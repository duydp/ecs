﻿namespace Company.Interface.Report.SXXK
{
    partial class rpGDN
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpGDN));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl21 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl23 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl25 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl26 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl11 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCuc1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNPL = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSokien = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTrongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThanhTien = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrLabel47,
            this.xrLabel39,
            this.lbl22,
            this.xrLabel37,
            this.xrLabel36,
            this.xrLabel35,
            this.lbl24,
            this.xrLabel33,
            this.lbl21,
            this.lbl20,
            this.lbl23,
            this.lbl17,
            this.xrLabel28,
            this.lbl15,
            this.xrLabel26,
            this.lbl19,
            this.xrLabel24,
            this.xrLabel23,
            this.lbl14,
            this.lbl25,
            this.lbl26,
            this.lbl16,
            this.xrLabel45,
            this.lbl18,
            this.xrLabel30,
            this.lbl3,
            this.lbl2,
            this.lbl1,
            this.lbl4,
            this.xrLabel2,
            this.xrLabel3,
            this.lbl5,
            this.xrLabel5,
            this.lbl6,
            this.xrLabel10,
            this.xrLabel11,
            this.lbl7,
            this.lbl8,
            this.xrLabel14,
            this.lbl9,
            this.xrLabel6,
            this.lbl10,
            this.xrLabel8,
            this.lbl11,
            this.lbl12,
            this.lbl13,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLabel16,
            this.xrTable3});
            this.Detail.Height = 770;
            this.Detail.Name = "Detail";
            // 
            // xrLabel47
            // 
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel47.Location = new System.Drawing.Point(425, 542);
            this.xrLabel47.Multiline = true;
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.ParentStyleUsing.UseFont = false;
            this.xrLabel47.Size = new System.Drawing.Size(108, 25);
            this.xrLabel47.Text = ", ngày invoice:";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel39.Location = new System.Drawing.Point(450, 617);
            this.xrLabel39.Multiline = true;
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.ParentStyleUsing.UseFont = false;
            this.xrLabel39.Size = new System.Drawing.Size(75, 25);
            this.xrLabel39.Text = "Đến cảng:";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl22
            // 
            this.lbl22.BorderColor = System.Drawing.Color.Black;
            this.lbl22.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl22.Location = new System.Drawing.Point(350, 617);
            this.lbl22.Name = "lbl22";
            this.lbl22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl22.ParentStyleUsing.UseBackColor = false;
            this.lbl22.ParentStyleUsing.UseBorderColor = false;
            this.lbl22.ParentStyleUsing.UseBorders = false;
            this.lbl22.ParentStyleUsing.UseFont = false;
            this.lbl22.Size = new System.Drawing.Size(100, 25);
            this.lbl22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl22.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel37
            // 
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel37.Location = new System.Drawing.Point(292, 617);
            this.xrLabel37.Multiline = true;
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.ParentStyleUsing.UseFont = false;
            this.xrLabel37.Size = new System.Drawing.Size(58, 25);
            this.xrLabel37.Text = "Số B/L:";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel36.Location = new System.Drawing.Point(75, 617);
            this.xrLabel36.Multiline = true;
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.ParentStyleUsing.UseFont = false;
            this.xrLabel36.Size = new System.Drawing.Size(66, 25);
            this.xrLabel36.Text = "Tên tàu:";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel35.Location = new System.Drawing.Point(75, 642);
            this.xrLabel35.Multiline = true;
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.ParentStyleUsing.UseFont = false;
            this.xrLabel35.Size = new System.Drawing.Size(175, 25);
            this.xrLabel35.Text = "Dự kiến ngày nhận hàng:";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl24
            // 
            this.lbl24.BorderColor = System.Drawing.Color.Black;
            this.lbl24.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl24.Location = new System.Drawing.Point(250, 642);
            this.lbl24.Name = "lbl24";
            this.lbl24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl24.ParentStyleUsing.UseBackColor = false;
            this.lbl24.ParentStyleUsing.UseBorderColor = false;
            this.lbl24.ParentStyleUsing.UseBorders = false;
            this.lbl24.ParentStyleUsing.UseFont = false;
            this.lbl24.Size = new System.Drawing.Size(458, 25);
            this.lbl24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl24.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel33.Location = new System.Drawing.Point(75, 592);
            this.xrLabel33.Multiline = true;
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.ParentStyleUsing.UseFont = false;
            this.xrLabel33.Size = new System.Drawing.Size(166, 25);
            this.xrLabel33.Text = "Biên bản kiểm hàng số:";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl21
            // 
            this.lbl21.BorderColor = System.Drawing.Color.Black;
            this.lbl21.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl21.Location = new System.Drawing.Point(142, 617);
            this.lbl21.Name = "lbl21";
            this.lbl21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl21.ParentStyleUsing.UseBackColor = false;
            this.lbl21.ParentStyleUsing.UseBorderColor = false;
            this.lbl21.ParentStyleUsing.UseBorders = false;
            this.lbl21.ParentStyleUsing.UseFont = false;
            this.lbl21.Size = new System.Drawing.Size(150, 25);
            this.lbl21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl21.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl20
            // 
            this.lbl20.BorderColor = System.Drawing.Color.Black;
            this.lbl20.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl20.Location = new System.Drawing.Point(242, 592);
            this.lbl20.Name = "lbl20";
            this.lbl20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl20.ParentStyleUsing.UseBackColor = false;
            this.lbl20.ParentStyleUsing.UseBorderColor = false;
            this.lbl20.ParentStyleUsing.UseBorders = false;
            this.lbl20.ParentStyleUsing.UseFont = false;
            this.lbl20.Size = new System.Drawing.Size(466, 25);
            this.lbl20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl20.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl23
            // 
            this.lbl23.BorderColor = System.Drawing.Color.Black;
            this.lbl23.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl23.Location = new System.Drawing.Point(525, 617);
            this.lbl23.Name = "lbl23";
            this.lbl23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl23.ParentStyleUsing.UseBackColor = false;
            this.lbl23.ParentStyleUsing.UseBorderColor = false;
            this.lbl23.ParentStyleUsing.UseBorders = false;
            this.lbl23.ParentStyleUsing.UseFont = false;
            this.lbl23.Size = new System.Drawing.Size(183, 25);
            this.lbl23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl23.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl17
            // 
            this.lbl17.BorderColor = System.Drawing.Color.Black;
            this.lbl17.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl17.Location = new System.Drawing.Point(242, 542);
            this.lbl17.Name = "lbl17";
            this.lbl17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl17.ParentStyleUsing.UseBackColor = false;
            this.lbl17.ParentStyleUsing.UseBorderColor = false;
            this.lbl17.ParentStyleUsing.UseBorders = false;
            this.lbl17.ParentStyleUsing.UseFont = false;
            this.lbl17.Size = new System.Drawing.Size(183, 25);
            this.lbl17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl17.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel28
            // 
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel28.Location = new System.Drawing.Point(75, 492);
            this.xrLabel28.Multiline = true;
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.ParentStyleUsing.UseFont = false;
            this.xrLabel28.Size = new System.Drawing.Size(108, 25);
            this.xrLabel28.Text = "Nhà cung cấp:";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl15
            // 
            this.lbl15.BorderColor = System.Drawing.Color.Black;
            this.lbl15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl15.Location = new System.Drawing.Point(233, 517);
            this.lbl15.Name = "lbl15";
            this.lbl15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl15.ParentStyleUsing.UseBackColor = false;
            this.lbl15.ParentStyleUsing.UseBorderColor = false;
            this.lbl15.ParentStyleUsing.UseBorders = false;
            this.lbl15.ParentStyleUsing.UseFont = false;
            this.lbl15.Size = new System.Drawing.Size(191, 25);
            this.lbl15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl15.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel26
            // 
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel26.Location = new System.Drawing.Point(75, 517);
            this.xrLabel26.Multiline = true;
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.ParentStyleUsing.UseFont = false;
            this.xrLabel26.Size = new System.Drawing.Size(158, 25);
            this.xrLabel26.Text = "Hợp đồng mua bán số:";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl19
            // 
            this.lbl19.BorderColor = System.Drawing.Color.Black;
            this.lbl19.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl19.Location = new System.Drawing.Point(225, 567);
            this.lbl19.Name = "lbl19";
            this.lbl19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl19.ParentStyleUsing.UseBackColor = false;
            this.lbl19.ParentStyleUsing.UseBorderColor = false;
            this.lbl19.ParentStyleUsing.UseBorders = false;
            this.lbl19.ParentStyleUsing.UseFont = false;
            this.lbl19.Size = new System.Drawing.Size(483, 25);
            this.lbl19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl19.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel24.Location = new System.Drawing.Point(75, 567);
            this.xrLabel24.Multiline = true;
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.ParentStyleUsing.UseFont = false;
            this.xrLabel24.Size = new System.Drawing.Size(150, 25);
            this.xrLabel24.Text = "Tờ khai Hải Quan số:";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.Location = new System.Drawing.Point(75, 542);
            this.xrLabel23.Multiline = true;
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.ParentStyleUsing.UseFont = false;
            this.xrLabel23.Size = new System.Drawing.Size(167, 25);
            this.xrLabel23.Text = "Invoice/packing list số:";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl14
            // 
            this.lbl14.BorderColor = System.Drawing.Color.Black;
            this.lbl14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl14.Location = new System.Drawing.Point(183, 492);
            this.lbl14.Name = "lbl14";
            this.lbl14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl14.ParentStyleUsing.UseBackColor = false;
            this.lbl14.ParentStyleUsing.UseBorderColor = false;
            this.lbl14.ParentStyleUsing.UseBorders = false;
            this.lbl14.ParentStyleUsing.UseFont = false;
            this.lbl14.Size = new System.Drawing.Size(525, 25);
            this.lbl14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl14.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl25
            // 
            this.lbl25.BorderColor = System.Drawing.Color.Black;
            this.lbl25.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl25.Location = new System.Drawing.Point(125, 683);
            this.lbl25.Name = "lbl25";
            this.lbl25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl25.ParentStyleUsing.UseBackColor = false;
            this.lbl25.ParentStyleUsing.UseBorderColor = false;
            this.lbl25.ParentStyleUsing.UseBorders = false;
            this.lbl25.ParentStyleUsing.UseFont = false;
            this.lbl25.Size = new System.Drawing.Size(267, 25);
            this.lbl25.Text = "Kho ký nhận";
            this.lbl25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lbl25.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl26
            // 
            this.lbl26.BorderColor = System.Drawing.Color.Black;
            this.lbl26.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl26.Location = new System.Drawing.Point(458, 683);
            this.lbl26.Name = "lbl26";
            this.lbl26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl26.ParentStyleUsing.UseBackColor = false;
            this.lbl26.ParentStyleUsing.UseBorderColor = false;
            this.lbl26.ParentStyleUsing.UseBorders = false;
            this.lbl26.ParentStyleUsing.UseFont = false;
            this.lbl26.Size = new System.Drawing.Size(250, 25);
            this.lbl26.Text = "Người giao hàng";
            this.lbl26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lbl26.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl16
            // 
            this.lbl16.BorderColor = System.Drawing.Color.Black;
            this.lbl16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl16.Location = new System.Drawing.Point(550, 517);
            this.lbl16.Name = "lbl16";
            this.lbl16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl16.ParentStyleUsing.UseBackColor = false;
            this.lbl16.ParentStyleUsing.UseBorderColor = false;
            this.lbl16.ParentStyleUsing.UseBorders = false;
            this.lbl16.ParentStyleUsing.UseFont = false;
            this.lbl16.Size = new System.Drawing.Size(158, 25);
            this.lbl16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl16.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel45
            // 
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel45.Location = new System.Drawing.Point(425, 517);
            this.xrLabel45.Multiline = true;
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.ParentStyleUsing.UseFont = false;
            this.xrLabel45.Size = new System.Drawing.Size(125, 25);
            this.xrLabel45.Text = ", ngày hợp đồng:";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl18
            // 
            this.lbl18.BorderColor = System.Drawing.Color.Black;
            this.lbl18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl18.Location = new System.Drawing.Point(533, 542);
            this.lbl18.Name = "lbl18";
            this.lbl18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl18.ParentStyleUsing.UseBackColor = false;
            this.lbl18.ParentStyleUsing.UseBorderColor = false;
            this.lbl18.ParentStyleUsing.UseBorders = false;
            this.lbl18.ParentStyleUsing.UseFont = false;
            this.lbl18.Size = new System.Drawing.Size(175, 25);
            this.lbl18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl18.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel30.Location = new System.Drawing.Point(50, 458);
            this.xrLabel30.Multiline = true;
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.ParentStyleUsing.UseFont = false;
            this.xrLabel30.Size = new System.Drawing.Size(658, 25);
            this.xrLabel30.Text = "Chứng từ kèm theo:";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl3
            // 
            this.lbl3.BorderColor = System.Drawing.Color.Black;
            this.lbl3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl3.Location = new System.Drawing.Point(42, 75);
            this.lbl3.Name = "lbl3";
            this.lbl3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl3.ParentStyleUsing.UseBackColor = false;
            this.lbl3.ParentStyleUsing.UseBorderColor = false;
            this.lbl3.ParentStyleUsing.UseBorders = false;
            this.lbl3.ParentStyleUsing.UseFont = false;
            this.lbl3.Size = new System.Drawing.Size(666, 25);
            this.lbl3.Text = "(Cho hàng nhập khẩu nước ngoài)";
            this.lbl3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lbl3.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl2
            // 
            this.lbl2.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl2.Location = new System.Drawing.Point(42, 50);
            this.lbl2.Multiline = true;
            this.lbl2.Name = "lbl2";
            this.lbl2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl2.ParentStyleUsing.UseFont = false;
            this.lbl2.Size = new System.Drawing.Size(666, 25);
            this.lbl2.Text = "GIẤY ĐỀ NGHỊ NHẬP NGUYÊN PHỤ LIỆU";
            this.lbl2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lbl1
            // 
            this.lbl1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.Location = new System.Drawing.Point(42, 0);
            this.lbl1.Multiline = true;
            this.lbl1.Name = "lbl1";
            this.lbl1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lbl1.ParentStyleUsing.UseFont = false;
            this.lbl1.Size = new System.Drawing.Size(259, 50);
            this.lbl1.Text = "CTY SX & XNK VINATEX\r\nXN MAY MẶC HÀNG XNK";
            this.lbl1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lbl1.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl4
            // 
            this.lbl4.BorderColor = System.Drawing.Color.Black;
            this.lbl4.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl4.Location = new System.Drawing.Point(125, 117);
            this.lbl4.Name = "lbl4";
            this.lbl4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl4.ParentStyleUsing.UseBackColor = false;
            this.lbl4.ParentStyleUsing.UseBorderColor = false;
            this.lbl4.ParentStyleUsing.UseBorders = false;
            this.lbl4.ParentStyleUsing.UseFont = false;
            this.lbl4.Size = new System.Drawing.Size(233, 25);
            this.lbl4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl4.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.Location = new System.Drawing.Point(50, 117);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.ParentStyleUsing.UseFont = false;
            this.xrLabel2.Size = new System.Drawing.Size(75, 25);
            this.xrLabel2.Text = "Tên tôi là:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.Location = new System.Drawing.Point(358, 117);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.ParentStyleUsing.UseFont = false;
            this.xrLabel3.Size = new System.Drawing.Size(58, 25);
            this.xrLabel3.Text = "Đơn vị:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl5
            // 
            this.lbl5.BorderColor = System.Drawing.Color.Black;
            this.lbl5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl5.Location = new System.Drawing.Point(417, 117);
            this.lbl5.Name = "lbl5";
            this.lbl5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl5.ParentStyleUsing.UseBackColor = false;
            this.lbl5.ParentStyleUsing.UseBorderColor = false;
            this.lbl5.ParentStyleUsing.UseBorders = false;
            this.lbl5.ParentStyleUsing.UseFont = false;
            this.lbl5.Size = new System.Drawing.Size(292, 25);
            this.lbl5.Text = "Phòng Xuất Nhập Khẩu";
            this.lbl5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl5.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel5.Location = new System.Drawing.Point(50, 142);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.ParentStyleUsing.UseFont = false;
            this.xrLabel5.Size = new System.Drawing.Size(658, 25);
            this.xrLabel5.Text = "Đề nghị nhập nguyên phụ liệu để sản xuất cho:";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl6
            // 
            this.lbl6.BorderColor = System.Drawing.Color.Black;
            this.lbl6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl6.Location = new System.Drawing.Point(233, 167);
            this.lbl6.Name = "lbl6";
            this.lbl6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl6.ParentStyleUsing.UseBackColor = false;
            this.lbl6.ParentStyleUsing.UseBorderColor = false;
            this.lbl6.ParentStyleUsing.UseBorders = false;
            this.lbl6.ParentStyleUsing.UseFont = false;
            this.lbl6.Size = new System.Drawing.Size(475, 25);
            this.lbl6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl6.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel10.Location = new System.Drawing.Point(50, 167);
            this.xrLabel10.Multiline = true;
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.ParentStyleUsing.UseFont = false;
            this.xrLabel10.Size = new System.Drawing.Size(183, 25);
            this.xrLabel10.Text = "Hợp đồng xuất khẩu số:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.Location = new System.Drawing.Point(50, 192);
            this.xrLabel11.Multiline = true;
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.ParentStyleUsing.UseFont = false;
            this.xrLabel11.Size = new System.Drawing.Size(75, 25);
            this.xrLabel11.Text = "Là hàng:";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl7
            // 
            this.lbl7.BorderColor = System.Drawing.Color.Black;
            this.lbl7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl7.Location = new System.Drawing.Point(125, 192);
            this.lbl7.Name = "lbl7";
            this.lbl7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl7.ParentStyleUsing.UseBackColor = false;
            this.lbl7.ParentStyleUsing.UseBorderColor = false;
            this.lbl7.ParentStyleUsing.UseBorders = false;
            this.lbl7.ParentStyleUsing.UseFont = false;
            this.lbl7.Size = new System.Drawing.Size(141, 25);
            this.lbl7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl7.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl8
            // 
            this.lbl8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl8.Location = new System.Drawing.Point(267, 192);
            this.lbl8.Multiline = true;
            this.lbl8.Name = "lbl8";
            this.lbl8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl8.ParentStyleUsing.UseFont = false;
            this.lbl8.Size = new System.Drawing.Size(116, 25);
            this.lbl8.Text = "* GIA CÔNG";
            this.lbl8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl8.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel14
            // 
            this.xrLabel14.BorderColor = System.Drawing.Color.Black;
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.Location = new System.Drawing.Point(383, 192);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.ParentStyleUsing.UseBackColor = false;
            this.xrLabel14.ParentStyleUsing.UseBorderColor = false;
            this.xrLabel14.ParentStyleUsing.UseBorders = false;
            this.xrLabel14.ParentStyleUsing.UseFont = false;
            this.xrLabel14.Size = new System.Drawing.Size(108, 25);
            this.xrLabel14.Text = "Số:";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lbl9
            // 
            this.lbl9.BorderColor = System.Drawing.Color.Black;
            this.lbl9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl9.Location = new System.Drawing.Point(492, 192);
            this.lbl9.Name = "lbl9";
            this.lbl9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl9.ParentStyleUsing.UseBackColor = false;
            this.lbl9.ParentStyleUsing.UseBorderColor = false;
            this.lbl9.ParentStyleUsing.UseBorders = false;
            this.lbl9.ParentStyleUsing.UseFont = false;
            this.lbl9.Size = new System.Drawing.Size(217, 25);
            this.lbl9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl9.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel6.Location = new System.Drawing.Point(50, 225);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.ParentStyleUsing.UseFont = false;
            this.xrLabel6.Size = new System.Drawing.Size(100, 25);
            this.xrLabel6.Text = "Thanh toán:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl10
            // 
            this.lbl10.BorderColor = System.Drawing.Color.Black;
            this.lbl10.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl10.Location = new System.Drawing.Point(292, 250);
            this.lbl10.Name = "lbl10";
            this.lbl10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl10.ParentStyleUsing.UseBackColor = false;
            this.lbl10.ParentStyleUsing.UseBorderColor = false;
            this.lbl10.ParentStyleUsing.UseBorders = false;
            this.lbl10.ParentStyleUsing.UseFont = false;
            this.lbl10.Size = new System.Drawing.Size(417, 25);
            this.lbl10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl10.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.Location = new System.Drawing.Point(150, 250);
            this.xrLabel8.Multiline = true;
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.ParentStyleUsing.UseFont = false;
            this.xrLabel8.Size = new System.Drawing.Size(141, 25);
            this.xrLabel8.Text = "Thanh toán toàn bộ:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lbl11
            // 
            this.lbl11.BorderColor = System.Drawing.Color.Black;
            this.lbl11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl11.Location = new System.Drawing.Point(350, 275);
            this.lbl11.Name = "lbl11";
            this.lbl11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl11.ParentStyleUsing.UseBackColor = false;
            this.lbl11.ParentStyleUsing.UseBorderColor = false;
            this.lbl11.ParentStyleUsing.UseBorders = false;
            this.lbl11.ParentStyleUsing.UseFont = false;
            this.lbl11.Size = new System.Drawing.Size(358, 25);
            this.lbl11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl11.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl12
            // 
            this.lbl12.BorderColor = System.Drawing.Color.Black;
            this.lbl12.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl12.Location = new System.Drawing.Point(325, 300);
            this.lbl12.Name = "lbl12";
            this.lbl12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl12.ParentStyleUsing.UseBackColor = false;
            this.lbl12.ParentStyleUsing.UseBorderColor = false;
            this.lbl12.ParentStyleUsing.UseBorders = false;
            this.lbl12.ParentStyleUsing.UseFont = false;
            this.lbl12.Size = new System.Drawing.Size(383, 25);
            this.lbl12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl12.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lbl13
            // 
            this.lbl13.BorderColor = System.Drawing.Color.Black;
            this.lbl13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl13.Location = new System.Drawing.Point(375, 325);
            this.lbl13.Name = "lbl13";
            this.lbl13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl13.ParentStyleUsing.UseBackColor = false;
            this.lbl13.ParentStyleUsing.UseBorderColor = false;
            this.lbl13.ParentStyleUsing.UseBorders = false;
            this.lbl13.ParentStyleUsing.UseFont = false;
            this.lbl13.Size = new System.Drawing.Size(333, 25);
            this.lbl13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lbl13.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.Location = new System.Drawing.Point(150, 325);
            this.xrLabel18.Multiline = true;
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.ParentStyleUsing.UseFont = false;
            this.xrLabel18.Size = new System.Drawing.Size(225, 25);
            this.xrLabel18.Text = "Thỏa thuận không thanh toán số:";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.Location = new System.Drawing.Point(150, 300);
            this.xrLabel19.Multiline = true;
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.ParentStyleUsing.UseFont = false;
            this.xrLabel19.Size = new System.Drawing.Size(175, 25);
            this.xrLabel19.Text = "Không thanh toán, lý do:";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.Location = new System.Drawing.Point(150, 275);
            this.xrLabel16.Multiline = true;
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.ParentStyleUsing.UseFont = false;
            this.xrLabel16.Size = new System.Drawing.Size(200, 25);
            this.xrLabel16.Text = "Thanh toán một phần số tiền:";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Location = new System.Drawing.Point(50, 375);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.ParentStyleUsing.UseBorders = false;
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.Size = new System.Drawing.Size(658, 25);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCuc1,
            this.xrTableCell1,
            this.xrTableCell3,
            this.xrTableCell5,
            this.xrTableCell7,
            this.xrTableCell9,
            this.xrTableCell11,
            this.xrTableCell13});
            this.xrTableRow3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.ParentStyleUsing.UseFont = false;
            this.xrTableRow3.Size = new System.Drawing.Size(658, 25);
            this.xrTableRow3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblCuc1
            // 
            this.lblCuc1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblCuc1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCuc1.Location = new System.Drawing.Point(0, 0);
            this.lblCuc1.Multiline = true;
            this.lblCuc1.Name = "lblCuc1";
            this.lblCuc1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCuc1.ParentStyleUsing.UseBorders = false;
            this.lblCuc1.ParentStyleUsing.UseFont = false;
            this.lblCuc1.Size = new System.Drawing.Size(42, 25);
            this.lblCuc1.Text = "STT";
            this.lblCuc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Location = new System.Drawing.Point(42, 0);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.ParentStyleUsing.UseBorders = false;
            this.xrTableCell1.ParentStyleUsing.UseFont = false;
            this.xrTableCell1.Size = new System.Drawing.Size(166, 25);
            this.xrTableCell1.Text = "Tên NPL";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell3.Location = new System.Drawing.Point(208, 0);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.ParentStyleUsing.UseBorders = false;
            this.xrTableCell3.ParentStyleUsing.UseFont = false;
            this.xrTableCell3.Size = new System.Drawing.Size(92, 25);
            this.xrTableCell3.Text = "S.lượng";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell5.Location = new System.Drawing.Point(300, 0);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.ParentStyleUsing.UseBorders = false;
            this.xrTableCell5.ParentStyleUsing.UseFont = false;
            this.xrTableCell5.Size = new System.Drawing.Size(42, 25);
            this.xrTableCell5.Text = "ĐVT";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell7.Location = new System.Drawing.Point(342, 0);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.ParentStyleUsing.UseBorders = false;
            this.xrTableCell7.ParentStyleUsing.UseFont = false;
            this.xrTableCell7.Size = new System.Drawing.Size(58, 25);
            this.xrTableCell7.Text = "Số kiện";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell9.Location = new System.Drawing.Point(400, 0);
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell9.ParentStyleUsing.UseBorders = false;
            this.xrTableCell9.ParentStyleUsing.UseFont = false;
            this.xrTableCell9.Size = new System.Drawing.Size(92, 25);
            this.xrTableCell9.Text = "Trọng lượng";
            this.xrTableCell9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell11.Location = new System.Drawing.Point(492, 0);
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell11.ParentStyleUsing.UseBorders = false;
            this.xrTableCell11.ParentStyleUsing.UseFont = false;
            this.xrTableCell11.Size = new System.Drawing.Size(91, 25);
            this.xrTableCell11.Text = "Th. Tiền";
            this.xrTableCell11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell13.Location = new System.Drawing.Point(583, 0);
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell13.ParentStyleUsing.UseBorders = false;
            this.xrTableCell13.ParentStyleUsing.UseFont = false;
            this.xrTableCell13.Size = new System.Drawing.Size(75, 25);
            this.xrTableCell13.Text = "Ghi chú";
            this.xrTableCell13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(50, 400);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.ParentStyleUsing.UseBorders = false;
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.Size = new System.Drawing.Size(658, 25);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTT,
            this.lblTenNPL,
            this.lblSoLuong,
            this.lblDVT,
            this.lblSokien,
            this.lblTrongLuong,
            this.lblThanhTien,
            this.lblGhiChu});
            this.xrTableRow1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.ParentStyleUsing.UseFont = false;
            this.xrTableRow1.Size = new System.Drawing.Size(658, 25);
            this.xrTableRow1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSTT
            // 
            this.lblSTT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTT.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSTT.Location = new System.Drawing.Point(0, 0);
            this.lblSTT.Multiline = true;
            this.lblSTT.Name = "lblSTT";
            this.lblSTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTT.ParentStyleUsing.UseBorders = false;
            this.lblSTT.ParentStyleUsing.UseFont = false;
            this.lblSTT.Size = new System.Drawing.Size(42, 25);
            this.lblSTT.Text = "01";
            this.lblSTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSTT.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblTenNPL
            // 
            this.lblTenNPL.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTenNPL.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNPL.Location = new System.Drawing.Point(42, 0);
            this.lblTenNPL.Name = "lblTenNPL";
            this.lblTenNPL.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNPL.ParentStyleUsing.UseBorders = false;
            this.lblTenNPL.ParentStyleUsing.UseFont = false;
            this.lblTenNPL.Size = new System.Drawing.Size(166, 25);
            this.lblTenNPL.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTenNPL.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoLuong.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong.Location = new System.Drawing.Point(208, 0);
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuong.ParentStyleUsing.UseBorders = false;
            this.lblSoLuong.ParentStyleUsing.UseFont = false;
            this.lblSoLuong.Size = new System.Drawing.Size(92, 25);
            this.lblSoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSoLuong.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblDVT
            // 
            this.lblDVT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDVT.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVT.Location = new System.Drawing.Point(300, 0);
            this.lblDVT.Name = "lblDVT";
            this.lblDVT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDVT.ParentStyleUsing.UseBorders = false;
            this.lblDVT.ParentStyleUsing.UseFont = false;
            this.lblDVT.Size = new System.Drawing.Size(42, 25);
            this.lblDVT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblDVT.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblSokien
            // 
            this.lblSokien.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSokien.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSokien.Location = new System.Drawing.Point(342, 0);
            this.lblSokien.Name = "lblSokien";
            this.lblSokien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSokien.ParentStyleUsing.UseBorders = false;
            this.lblSokien.ParentStyleUsing.UseFont = false;
            this.lblSokien.Size = new System.Drawing.Size(58, 25);
            this.lblSokien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSokien.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblTrongLuong
            // 
            this.lblTrongLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTrongLuong.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrongLuong.Location = new System.Drawing.Point(400, 0);
            this.lblTrongLuong.Name = "lblTrongLuong";
            this.lblTrongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTrongLuong.ParentStyleUsing.UseBorders = false;
            this.lblTrongLuong.ParentStyleUsing.UseFont = false;
            this.lblTrongLuong.Size = new System.Drawing.Size(92, 25);
            this.lblTrongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblTrongLuong.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblThanhTien
            // 
            this.lblThanhTien.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblThanhTien.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThanhTien.Location = new System.Drawing.Point(492, 0);
            this.lblThanhTien.Name = "lblThanhTien";
            this.lblThanhTien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThanhTien.ParentStyleUsing.UseBorders = false;
            this.lblThanhTien.ParentStyleUsing.UseFont = false;
            this.lblThanhTien.Size = new System.Drawing.Size(91, 25);
            this.lblThanhTien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblThanhTien.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblGhiChu
            // 
            this.lblGhiChu.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblGhiChu.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGhiChu.Location = new System.Drawing.Point(583, 0);
            this.lblGhiChu.Name = "lblGhiChu";
            this.lblGhiChu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGhiChu.ParentStyleUsing.UseBorders = false;
            this.lblGhiChu.ParentStyleUsing.UseFont = false;
            this.lblGhiChu.Size = new System.Drawing.Size(75, 25);
            this.lblGhiChu.Text = "Hàng rời";
            this.lblGhiChu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblGhiChu.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.Location = new System.Drawing.Point(125, 325);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.Size = new System.Drawing.Size(16, 25);
            // 
            // rpGDN
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Margins = new System.Drawing.Printing.Margins(32, 32, 70, 70);
            this.PageHeight = 910;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.Custom;
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRLabel lbl1;
        private DevExpress.XtraReports.UI.XRLabel lbl2;
        private DevExpress.XtraReports.UI.XRLabel lbl5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lbl4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel lbl8;
        private DevExpress.XtraReports.UI.XRLabel lbl7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lbl6;
        private DevExpress.XtraReports.UI.XRLabel lbl12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel lbl13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lbl11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lbl10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel lbl9;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblCuc1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel lbl17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel lbl15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel lbl19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel lbl14;
        private DevExpress.XtraReports.UI.XRLabel lbl23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel lbl22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel lbl24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel lbl21;
        private DevExpress.XtraReports.UI.XRLabel lbl20;
        private DevExpress.XtraReports.UI.XRLabel lbl26;
        private DevExpress.XtraReports.UI.XRLabel lbl25;
        private DevExpress.XtraReports.UI.XRLabel lbl3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel lbl18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel lbl16;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNPL;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong;
        private DevExpress.XtraReports.UI.XRTableCell lblDVT;
        private DevExpress.XtraReports.UI.XRTableCell lblSokien;
        private DevExpress.XtraReports.UI.XRTableCell lblTrongLuong;
        private DevExpress.XtraReports.UI.XRTableCell lblThanhTien;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChu;
    }
}
