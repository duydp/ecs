﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.BLL.KDT;
using Janus.Windows.GridEX;
using Company.BLL;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class ChungTuKemForm : Company.Interface.BaseForm
    {
        public ToKhaiMauDich TKMD;
        public ChungTuKem CTK = new ChungTuKem();
        public List<ChungTuKemChiTiet> ListCTDK = new List<ChungTuKemChiTiet>();
        public bool isKhaiBoSung = false;
        public bool isAddNew = true;

        string filebase64 = "";
        long filesize = 0;
        public ChungTuKemForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Lay tong dung luong cac file dinh kem
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private void HienThiTongDungLuong(List<ChungTuKemChiTiet> list)
        {
            long size = 0;

            for (int i = 0; i < list.Count; i++)
            {
                size += Convert.ToInt64(list[i].FileSize);
            }


            //hien thi tong dung luong file
            lblTongDungLuong.Text = Convert.ToString(size / (1024 * 1024)) + " MB (" + size.ToString() + " Bytes)";

            lblLuuY.Text = Convert.ToString(GlobalSettings.FileSize / (1024 * 1024)) + " MB";
        }

        private void BindData()
        {
            dgList.DataSource = ListCTDK;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }

            //Cap nhat thong tin tong dung luong file.
            HienThiTongDungLuong(ListCTDK);

        }

        private void GiayPhepForm_Load(object sender, EventArgs e)
        {
            //Ngay chung tu mac dinh
            ccNgayChungTu.Text = DateTime.Today.ToShortDateString();

            //Thiet lap nut Khai bao, Phan hoi mac dinh
            btnKhaiBao.Enabled = true;
            btnLayPhanHoi.Enabled = false;

            //Load danh sach Loai chung tu
            DataView dvLCT = BLL.DuLieuChuan.LoaiChungTu.SelectAll().Tables[0].DefaultView;
            dvLCT.Sort = "ID ASC";
            cbLoaiCT.Items.Clear();
            for (int i = 0; i < dvLCT.Count; i++)
            {
                cbLoaiCT.Items.Add(dvLCT[i]["Ten"].ToString(), (int)dvLCT[i]["ID"]);
            }
            cbLoaiCT.SelectedIndex = cbLoaiCT.Items.Count > 0 ? 0 : -1;

            if (CTK.ID > 0)
            {
                ccNgayChungTu.Text = CTK.NGAY_CT.ToShortDateString();
                txtThongTinKhac.Text = CTK.DIENGIAI;
                txtSoChungTu.Text = CTK.SO_CT;
                cbLoaiCT.SelectedValue = CTK.MA_LOAI_CT;

                ListCTDK = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(CTK.ID);

                BindData();
            }
            else
            {
                //Cap nhat thong tin tong dung luong file.
                HienThiTongDungLuong(ListCTDK);
            }

            //if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO && !isKhaiBoSung)
            //{
            if (TKMD.ID <= 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            else if (CTK.SOTN > 0 && CTK.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET.ToString())
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                ccNgayTiepNhan.Text = CTK.NGAYTN.ToShortDateString();
                txtSoTiepNhan.Text = CTK.SOTN + "";
            }
            else if (CTK.SOTN > 0 && CTK.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET.ToString())
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
                ccNgayTiepNhan.Text = CTK.NGAYTN.ToShortDateString();
                txtSoTiepNhan.Text = CTK.SOTN + "";
            }
            else if (TKMD.SoToKhai > 0 && int.Parse(TKMD.PhanLuong != "" ? TKMD.PhanLuong : "0") == 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKMD.PhanLuong != "")
            {
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //}

            txtSoChungTu.Focus();

            // Thiết lập trạng thái các nút trên form.
            // HUNGTQ, Update 07/06/2010.
            SetButtonStateCHUNGTUKEM(TKMD, isKhaiBoSung, CTK);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<ChungTuKemChiTiet> HangGiayPhepDetailCollection = new List<ChungTuKemChiTiet>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuKemChiTiet hgpDetail = new ChungTuKemChiTiet();
                        hgpDetail = (ChungTuKemChiTiet)i.GetRow().DataRow;

                        if (hgpDetail == null) continue;

                        //Detele from DB.
                        if (hgpDetail.ID > 0)
                            hgpDetail.Delete();

                        //Remove out Collction
                        ListCTDK.Remove(hgpDetail);
                    }
                }
            }

            BindData();
        }

        private bool checkSoCTK(string soCT)
        {
            int cnt = 0;

            foreach (ChungTuKem gpTMP in TKMD.ChungTuKemCollection)
            {
                if (gpTMP.SO_CT.Trim().ToUpper() == soCT.Trim().ToUpper())
                    cnt += 1;
            }

            if (isAddNew == true)//Neu la them moi chung tu, co 1 ket qua tra ve
                return cnt > 0;
            else
                return cnt > 1;//Neu la chinh sua chung tu, co hon 1 ket qua tra ve la 2, tuc bo qua chung tu dang sua.
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;

            if (TKMD.ID <= 0)
            {
                ShowMessage("Vui long nhap to khai truoc", false);
                return;
            }
            if (ListCTDK.Count == 0)
            {
                ShowMessage("Chưa chọn file đính kèm", false);
                btnAddNew.Focus();
                return;
            }
            //Cap nhat lai danh sach file dinh kem chi tiet
            CTK.listCTChiTiet = ListCTDK;

            //TKMD.ChungTuKemCollection.Remove(chungTuKem);

            if (checkSoCTK(txtSoChungTu.Text))
            {
                ShowMessage("Chứng từ đã tồn tại", false);
                return;
            }

            CTK.SO_CT = txtSoChungTu.Text.Trim();
            CTK.DIENGIAI = txtThongTinKhac.Text;
            CTK.MA_LOAI_CT = cbLoaiCT.SelectedValue.ToString();
            CTK.NGAY_CT = ccNgayChungTu.Value;
            CTK.TKMDID = TKMD.ID;
            CTK.GUIDSTR = Guid.NewGuid().ToString();
            //Khai bo sung
            if (isKhaiBoSung)
                CTK.LoaiKB = "1";
            else
                CTK.LoaiKB = "0";

            try
            {
                CTK.InsertUpdateFull(ListCTDK);

                if (isAddNew == true)
                    TKMD.ChungTuKemCollection.Add(CTK);

                BindData();

                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
                return;
            }

        }

        static public string EncodeTo64(string toEncode)
        {

            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(toEncode);

            string returnValue = System.Convert.ToBase64String(toEncodeAsBytes);

            return returnValue;

        }

        static public string DecodeFrom64(string encodedData)
        {

            byte[] encodedDataAsBytes

                = System.Convert.FromBase64String(encodedData);

            string returnValue =

               System.Text.ASCIIEncoding.ASCII.GetString(encodedDataAsBytes);

            return returnValue;

        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = "";
            openFileDialog1.Filter = "JPEG (*.jpg; *.jpeg)|*.jpg;*.jpeg|TIFF (*.tif; *.tiff)|*.tif;*.tiff";
            openFileDialog1.Multiselect = true;
            //List<ChungTuKem> ctkemList = ChungTuKem.SelectCollectionBy_TKMDID(this.TKMD.ID);
            //long sizeExist = 0;
            //if (ctkemList.Count > 0)
            //{
            //    foreach (ChungTuKem ctk in ctkemList)
            //    {
            //        List<ChungTuKemChiTiet> ctKemChiTietList = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(ctk.ID);
            //        if (ctKemChiTietList.Count > 0) {
            //            foreach (ChungTuKemChiTiet ctkChiTiet in ctKemChiTietList) {
            //                sizeExist += Convert.ToInt64(ctkChiTiet.FileSize);
            //            }
            //        }
            //    }
            //}
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    for (int k = 0; k < openFileDialog1.FileNames.Length; k++)
                    {
                        System.IO.FileInfo fin = new System.IO.FileInfo(openFileDialog1.FileNames[k]);
                        System.IO.FileStream fs = new System.IO.FileStream(openFileDialog1.FileNames[k], System.IO.FileMode.Open, System.IO.FileAccess.Read);

                        //Cap nhat tong dung luong file.
                        long size = 0;

                        for (int i = 0; i < ListCTDK.Count; i++)
                        {
                            size += Convert.ToInt64(ListCTDK[i].FileSize);
                        }

                        //+ them dung luong file moi chuan bi them vao danh sach
                        size += fs.Length;
                        //+ them dung luong file trong danh sach san co
                        //size += sizeExist;
                        //Kiem tra dung luong file
                        if (size > GlobalSettings.FileSize)
                        {
                            this.ShowMessage("Tổng dung lượng các file đính kèm vượt quá dung lượng cho phép.", false);
                            return;
                        }

                        byte[] data = new byte[fs.Length];
                        fs.Read(data, 0, data.Length);
                        filebase64 = System.Convert.ToBase64String(data);
                        filesize = fs.Length;

                        /*
                         * truoc khi them moi file dinh kem vao danh sach, phai kiem tra tong dung luong co hop len khong?.
                         * Neu > dung luong choh phep -> Hien thi thong bao va khong them vao danh sach.
                         */
                        ChungTuKemChiTiet ctctiet = new ChungTuKemChiTiet();
                        ctctiet.ChungTuKemID = CTK.ID;
                        ctctiet.FileName = fin.Name;
                        ctctiet.FileSize = filesize;
                        ctctiet.NoiDung = data;

                        ListCTDK.Add(ctctiet);
                    }

                    dgList.DataSource = ListCTDK;
                    try
                    {
                        dgList.Refetch();
                    }
                    catch { dgList.Refresh(); }

                    HienThiTongDungLuong(ListCTDK);

                }
            }
            catch (Exception ex) { }
        }

        private void btnChonGP_Click(object sender, EventArgs e)
        {
            //kiem tra thu da co muc temp?. Neu chua co -> tao 1 thu muc temp de chua cac file tam thoi.
            string path = Application.StartupPath + "\\Temp";

            if (System.IO.Directory.Exists(path) == false)
            {
                System.IO.Directory.CreateDirectory(path);
            }

            //Giai ma basecode64 -> file & luu tam vao thu muc Temp moi tao.
            if (dgList.RecordCount>0)
            {
                ChungTuKemChiTiet fileData = (ChungTuKemChiTiet)dgList.GetRow().DataRow;
                string fileName = path + "\\" + fileData.FileName;

                //Ghi file
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }

                System.IO.FileStream fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                fs.Write(fileData.NoiDung, 0, fileData.NoiDung.Length);
                fs.Close();

                System.Diagnostics.Process.Start(fileName);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            this.KhaiBaoChungTuKem();
        }

        private void KhaiBaoChungTuKem()
        {
            if (CTK.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                
                bool thanhcong = CTK.WSKhaiBaoChungTuDinhKem(password, TKMD.MaHaiQuan, (long)TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK, TKMD.ID, TKMD.MaDoanhNghiep, ListCTDK, MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao);
                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        /// <summary>
        /// Lấy phản hồi sau khi khai báo. Mục đích là lấy số tiếp nhận điện tử.
        /// </summary>
        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = CTK.WSLaySoTiepNhan(password, EntityBase.GetPathProram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.CTK.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungChungTuDinhKemThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.CTK.SOTN.ToString("N0");
                    ccNgayTiepNhan.Value = this.CTK.NGAYTN;
                    ccNgayTiepNhan.Text = this.CTK.NGAYTN.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        /// <summary>
        /// Lấy thông tin phản hồi sau khi đã khai báo có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = CTK.WSLayPhanHoi(password, EntityBase.GetPathProram(), GlobalSettings.MA_HAI_QUAN, this.MaDoanhNghiep);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.CTK.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungChungTuDinhKemDuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.CTK.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                    }
                    else {
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                    }

                    this.ShowMessage(message, false);
                    
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        /// <summary>
        /// Lấy các thông tin phản hồi.
        /// </summary>
        private void LayThongTinPhanHoi()
        {
            string password = string.Empty;
            WSForm form = new WSForm();
            if (GlobalSettings.PassWordDT == "")
            {
                form.ShowDialog(this);
                if (!form.IsReady) return;
            }
            if (GlobalSettings.PassWordDT != "")
                password = GlobalSettings.PassWordDT;
            else
                password = form.txtMatKhau.Text.Trim();

            if (this.CTK.SOTN == 0)
            {
                this.LayPhanHoiKhaiBao(password);
            }
            else if (this.CTK.SOTN > 0)
            {
                this.LayPhanHoiDuyet(password);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            LayThongTinPhanHoi();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void btnXoaChungTu_Click(object sender, EventArgs e)
        {
            if (CTK.ID > 0)
            {
                if (ShowMessage("Bạn có muốn xóa chứng từ này không?", true) == "Yes")
                {

                    //Xoa chung tu chi tiet
                    CTK.LoadListCTChiTiet();

                    for (int i = 0; i < CTK.listCTChiTiet.Count; i++)
                    {
                        CTK.listCTChiTiet[i].Delete();
                    }

                    //Xoa chung tu trong DB
                    CTK.Delete();

                    //Xoa chung tu trong Collection
                    TKMD.ChungTuKemCollection.Remove(CTK);

                    //Load lai danh sach chung tu kem cho TK
                    TKMD.LoadChungTuKem();
                }
            }

            //Dong form sau khi xoa chung tu.
            this.Close();
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form CHUNGTUKEM.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateCHUNGTUKEM(BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.ChungTuKem chungTuKem)
        {
            if (chungTuKem == null)
                return false;

            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.SoTiepNhan == 0
                            || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                            || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET);

                btnXoa.Enabled = status;
                btnXoaChungTu.Enabled = status;
                btnAddNew.Enabled = status;
                //btnXemFile.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXyLy.Enabled = false;

                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkmd.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    if (chungTuKem.SOTN > 0)
                        status = false;
                    else
                        status = true;

                    btnXoa.Enabled = status;
                    btnXoaChungTu.Enabled = status;
                    btnAddNew.Enabled = status;
                    //btnXemFile.Enabled = status;
                    btnGhi.Enabled = status;
                    btnKetQuaXyLy.Enabled = true;

                    //Neu hop dong chua co so tiep nhan -> phai khai bao
                    if (chungTuKem.SOTN == 0)
                    {
                        btnKhaiBao.Enabled = true;
                        btnLayPhanHoi.Enabled = false;
                    }
                    //Neu hop dong da co so tiep nhan -> co the lay phan hoi
                    else
                    {
                        btnKhaiBao.Enabled = false;
                        //btnLayPhanHoi.Enabled = true;
                    }
                }
            }

            return true;
        }

        #endregion

        private void btnKetQuaXyLy_Click(object sender, EventArgs e)
        {
            if (CTK.GUIDSTR != null && CTK.GUIDSTR != "")
                Globals.ShowKetQuaXuLyBoSung(CTK.GUIDSTR);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }
    }
}

