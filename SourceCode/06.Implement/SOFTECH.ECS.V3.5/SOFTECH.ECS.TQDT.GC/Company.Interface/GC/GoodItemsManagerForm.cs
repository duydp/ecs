﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.GC.BLL.GC;
namespace Company.Interface.GC
{
    public partial class GoodItemsManagerForm : BaseForm
    {
        public string where = "";
        public GoodItemsManagerForm()
        {
            InitializeComponent();
        }
        private void GoodItemsManagerForm_Load(object sender, EventArgs e)
        {
            ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            ctrCoQuanHaiQuan.BackColor = Color.Transparent;
            cbStatus.SelectedValue = 0;
            txtNamQT.Text = (DateTime.Now.Year - 1).ToString();
            //BindData();
            btnSearch_Click(null,null);
        }
        public void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = KDT_VNACCS_BaoCaoQuyetToan_NPLSP.SelectCollectionDynamic(GetSearchWhere(),"");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1=1";
                if (!String.IsNullOrEmpty(txtSoTiepNhan.Text))
                    where += " AND SoTN LIKE '%" + txtSoTiepNhan.Text+"%'";
                if (!String.IsNullOrEmpty(txtNamQT.Text))
                    where += " AND NamQuyetToan = " + txtNamQT.Text;
                if (!String.IsNullOrEmpty(cbStatus.SelectedValue.ToString()))
                    where += " AND TrangThaiXuLy =" + cbStatus.SelectedValue;
                if (!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Code))
                    where += " AND MaHQ = '" + ctrCoQuanHaiQuan.Code + "'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
                return null;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = KDT_VNACCS_BaoCaoQuyetToan_NPLSP.SelectCollectionDynamic(GetSearchWhere(), "ID");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP();
                goodItem = (KDT_VNACCS_BaoCaoQuyetToan_NPLSP)e.Row.DataRow;
                //if (GlobalSettings.MA_DON_VI == "4000395355")
                //{
                    BaoCaoQuyetToanHopDong f = new BaoCaoQuyetToanHopDong();
                    f.IsActive = true;
                    f.goodItem = goodItem;
                    f.ShowDialog(this);
                //}
                //else
                //{
                //    XuatNhapTonForm f = new XuatNhapTonForm();
                //    f.isDaQuyetToan = true;
                //    f.goodItem = goodItem;
                //    f.ShowDialog(this);                
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    int LoaiHinh = Convert.ToInt32(e.Row.Cells["LoaiHinhBaoCao"].Value);
                    int DVT = Convert.ToInt32(e.Row.Cells["DVTBaoCao"].Value);
                    //if (LoaiHinh == 1)
                    e.Row.Cells["LoaiHinhBaoCao"].Text = "Gia công";
                    //e.Row.Cells["LoaiHinhBaoCao"].Text = "SẢN XUẤT XUẤT KHẨU";
                    if (DVT == 1)
                    {
                        e.Row.Cells["DVTBaoCao"].Text = "Trị giá";
                    }
                    else
                    {
                        e.Row.Cells["DVTBaoCao"].Text = "Số lượng";
                    }

                    string TrangThai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                    switch (TrangThai)
                    {
                        case "0":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                            break;
                        case "-1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                            break;
                        case "1":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                            break;
                        case "2":
                            e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                            break;

                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP();
                            goodItem = (KDT_VNACCS_BaoCaoQuyetToan_NPLSP)i.GetRow().DataRow;
                            goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail.SelectCollectionBy_GoodItem_ID(goodItem.ID);
                            if (!String.IsNullOrEmpty(goodItem.GuidStr))
                            {
                                ShowMessage("Báo cáo quyết toán này đã gửi lên HQ nên không được xóa !", false);
                            }
                            else
                            {
                                goodItem.DeleteFull();
                                HoSoQuyetToan_DSHopDong.DeleteDynamic("Master_ID =" + goodItem.ID);
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    //BindData();
                    btnSearch_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách báo cáo quyết toán_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (ShowMessage("Bạn có muốn xuất kèm theo danh sách hàng hóa không ?", true) == "No")
                {
                    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
                else
                {
                    if (sfNPL.ShowDialog(this) == DialogResult.Yes || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        dgList.Tables[0].Columns.Add(new GridEXColumn("STT", ColumnType.Text, EditType.NoEdit) { Caption = "STT" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("LoaiHangHoa", ColumnType.Text, EditType.NoEdit) { Caption = "Loại hàng hóa" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TaiKhoan", ColumnType.Text, EditType.NoEdit) { Caption = "Tài khoản" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TenHangHoa", ColumnType.Text, EditType.NoEdit) { Caption = "Tên hàng hóa" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("MaHangHoa", ColumnType.Text, EditType.NoEdit) { Caption = "Mã HS" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DVT", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TonDauKy", ColumnType.Text, EditType.NoEdit) { Caption = "Tồn đầu kỳ" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("NhapTrongKy", ColumnType.Text, EditType.NoEdit) { Caption = "Nhập trong kỳ" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("XuatTrongKy", ColumnType.Text, EditType.NoEdit) { Caption = "Xuất trong kỳ" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TonCuoiKy", ColumnType.Text, EditType.NoEdit) { Caption = "Tồn cuối kỳ" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("GhiChu", ColumnType.Text, EditType.NoEdit) { Caption = "Ghi chú" });

                        dgList.DataSource = KDT_VNACCS_BaoCaoQuyetToan_NPLSP.SelectDynamicFull(GetSearchWhere(), "").Tables[0];
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        dgList.Tables[0].Columns.Remove("STT");
                        dgList.Tables[0].Columns.Remove("LoaiHangHoa");
                        dgList.Tables[0].Columns.Remove("TaiKhoan");
                        dgList.Tables[0].Columns.Remove("TenHangHoa");
                        dgList.Tables[0].Columns.Remove("MaHangHoa");
                        dgList.Tables[0].Columns.Remove("DVT");
                        dgList.Tables[0].Columns.Remove("TonDauKy");
                        dgList.Tables[0].Columns.Remove("NhapTrongKy");
                        dgList.Tables[0].Columns.Remove("XuatTrongKy");
                        dgList.Tables[0].Columns.Remove("TonCuoiKy");
                        dgList.Tables[0].Columns.Remove("GhiChu");

                        dgList.Refresh();
                        btnSearch_Click(null, null);
                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xử lý :\r\n " + ex.Message, false);
            }
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }


    }
}
