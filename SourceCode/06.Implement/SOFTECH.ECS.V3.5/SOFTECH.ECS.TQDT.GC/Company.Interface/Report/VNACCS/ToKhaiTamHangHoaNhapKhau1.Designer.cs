﻿namespace Company.Interface.Report.VNACCS
{
    partial class ToKhaiTamHangHoaNhapKhau1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiTamHangHoaNhapKhau1));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiDauTien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoNhanhToKhaiChiaNho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoToKhaiChiaNho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiTamNhapTaiXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiKiemTra = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHieuPhuongThucVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiCaNhanToChuc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSoHangHoaDaiDienToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCoQuanHaiQuanTiepNhanToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBoPhanXuLyToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayThayDoiDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioThayDoiDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoiHanTaiNhapTaiXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBieuThiTHHetHan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBuuChinhNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDienThoaiNguoiNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiUyThacNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiUyThacNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiXuatKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiXuatKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell103 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBuuChinhXuatKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiXuatKhau1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiXuatKhau2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiXuatKhau3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiXuatKhau4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNuoc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNguoiUyThacXuatKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDaiLyHaiQuan = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDaiLyHaiQuan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell129 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNhanVienHaiQuan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemLuuKhoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDiaDiemLuuKhoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoVanDon1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemDoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDiaDiemDoHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoVanDon2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemXepHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDiaDiemXepHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoVanDon3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoVanDon4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell167 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhuongTienVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenPhuongTienVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoVanDon5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHangDen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell159 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDonViTinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTrongLuongHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell187 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDonViTinhTrongLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongContainer = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell194 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblKyHieuSoHieu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell195 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDuocPhepNhapKho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVanBanPhapQuyKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVanBanPhapQuyKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVanBanPhapQuyKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVanBanPhapQuyKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaVanBanPhapQuyKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell214 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiHinhThucHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell218 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTiepNhanHoaDonDienTu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell224 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayPhatHanh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongThucThanhToan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell235 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDieuKienGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDongTienHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell237 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTriGiaTinhThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell243 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongHeSoPhanBoTriGia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiNhapLieu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell249 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaKetQuaKiemTraNoiDung = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell261 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell267 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiGiayPhepNhapKhau1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoGiayPhep1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell279 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiGiayPhepNhapKhau2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoGiayPhep2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiGiayPhepNhapKhau3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoGiayPhep3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiGiayPhepNhapKhau4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoGiayPhep4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiGiayPhepNhapKhau5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoGiayPhep5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell295 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiKhaiTriGia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell291 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTiepNhanKhaiTriGiaTongHop = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiCongThucChuan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiDieuChinhTriGia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell319 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhuongPhapDieuChinhTriGia = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTienTe = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiaCoSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell297 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell299 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell301 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiPhiVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell321 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTienTePhiVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhiVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell303 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell305 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiBaoHiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTienTeCuaTienBaoHiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhiBaoHiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDangKyBaoHiemTongHop = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell309 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell332 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell348 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow54 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell344 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTenKhoanDieuChinh1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiDieuChinh1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDongTienDieuChinhTriGia1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell346 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaKhoanDieuChinh1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow55 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell349 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell350 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTenKhoanDieuChinh2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell352 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiDieuChinh2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell354 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDongTienDieuChinhTriGia2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell356 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaKhoanDieuChinh2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell358 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell360 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow56 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell361 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell362 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTenKhoanDieuChinh3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell364 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiDieuChinh3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell366 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDongTienDieuChinhTriGia3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell368 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaKhoanDieuChinh3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell370 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell372 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow57 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell373 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell374 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTenKhoanDieuChinh4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell376 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiDieuChinh4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell378 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDongTienDieuChinhTriGia4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell380 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaKhoanDieuChinh4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell382 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell384 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow58 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell385 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell386 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaTenKhoanDieuChinh5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell388 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiDieuChinh5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell390 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDongTienDieuChinhTriGia5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaKhoanDieuChinh5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell394 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell396 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow59 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell397 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell400 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow60 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChiTietKhaiTriGia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow61 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell401 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell404 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell405 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell403 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell406 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell422 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell402 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow62 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell407 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell408 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThue1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThue1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell411 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThue1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell413 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDongTong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell419 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell420 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell417 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThuePhaiNop = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell421 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell412 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow63 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell414 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell423 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThue2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThue2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell426 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThue2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell428 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDongTong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell430 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell437 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell438 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienBaoLanh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell440 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell441 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow64 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell442 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell443 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThue3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThue3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell446 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThue3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell448 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDongTong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell450 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell451 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell452 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDongTienTyGiaTinhThue1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell454 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTyGiaTinhThue1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow69 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell498 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell499 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThue4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThue4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell502 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThue4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell504 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDongTong4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell506 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell507 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell508 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDongTienTyGiaTinhThue2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell510 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTyGiaTinhThue2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow68 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell484 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell485 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThue5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThue5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell488 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThue5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell490 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDongTong5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell492 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell493 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell494 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDongTienTyGiaTinhThue3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell496 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTyGiaTinhThue3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow67 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell470 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell471 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThue6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThue6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell474 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongTienThue6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell476 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDongTong6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell478 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell479 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell480 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaXacDinhThoiHanNopThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell482 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell456 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNguoiNopThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow66 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell431 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell432 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell433 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell434 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell435 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaLyDoDeNghi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell457 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell459 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiNopThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow65 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell460 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell461 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell462 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell463 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell464 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoTrangToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell466 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell467 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoDongHang = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2,
            this.xrTable3,
            this.xrTable4,
            this.xrTable1});
            this.Detail.HeightF = 850F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 22F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable5,
            this.lblTenThongTinXuat});
            this.ReportHeader.HeightF = 35F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(162.4302F, 3.75F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(491.875F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "Tờ khai tạm hàng hóa nhập khẩu (thông báo kết quả phân luồng)";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Arial", 9F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(764.9302F, 3.749974F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable5.SizeF = new System.Drawing.SizeF(47.06976F, 21.25001F);
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoTrang});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.6;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoTrang.StylePriority.UsePadding = false;
            this.lblSoTrang.Text = "1/3";
            this.lblSoTrang.Weight = 3;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 12.5F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29});
            this.xrTable1.SizeF = new System.Drawing.SizeF(796.0001F, 370F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell6,
            this.xrTableCell1,
            this.xrTableCell2,
            this.lblSoToKhai,
            this.xrTableCell4,
            this.xrTableCell8,
            this.xrTableCell9,
            this.lblSoToKhaiDauTien,
            this.xrTableCell11,
            this.lblSoNhanhToKhaiChiaNho,
            this.xrTableCell12,
            this.lblTongSoToKhaiChiaNho});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseTextAlignment = false;
            this.xrTableRow1.Weight = 0.44000000000000006;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 0.22150664915059459;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.StylePriority.UseTextAlignment = false;
            this.xrTableCell1.Text = "Số tờ khai";
            this.xrTableCell1.Weight = 0.74516030105603948;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 0.1000000610159798;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.Text = "NNNNNNNNN1NE";
            this.lblSoToKhai.Weight = 1.1358326073077036;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 0.13791746139519587;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "Số tờ khai đầu tiên";
            this.xrTableCell8.Weight = 1.1215622138977288;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Weight = 0.11286464691162101;
            // 
            // lblSoToKhaiDauTien
            // 
            this.lblSoToKhaiDauTien.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSoToKhaiDauTien.Name = "lblSoToKhaiDauTien";
            this.lblSoToKhaiDauTien.StylePriority.UseFont = false;
            this.lblSoToKhaiDauTien.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiDauTien.Weight = 1.2274480819702147;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "-";
            this.xrTableCell11.Weight = 0.13369716644287111;
            // 
            // lblSoNhanhToKhaiChiaNho
            // 
            this.lblSoNhanhToKhaiChiaNho.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoNhanhToKhaiChiaNho.Name = "lblSoNhanhToKhaiChiaNho";
            this.lblSoNhanhToKhaiChiaNho.StylePriority.UseFont = false;
            this.lblSoNhanhToKhaiChiaNho.Text = "NE";
            this.lblSoNhanhToKhaiChiaNho.Weight = 0.25869808197021493;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "/";
            this.xrTableCell12.Weight = 0.12328121185302754;
            // 
            // lblTongSoToKhaiChiaNho
            // 
            this.lblTongSoToKhaiChiaNho.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongSoToKhaiChiaNho.Name = "lblTongSoToKhaiChiaNho";
            this.lblTongSoToKhaiChiaNho.StylePriority.UseFont = false;
            this.lblTongSoToKhaiChiaNho.Text = "NE";
            this.lblTongSoToKhaiChiaNho.Weight = 2.642031823730445;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell17,
            this.lblSoToKhaiTamNhapTaiXuat});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.Weight = 0.44000000000000006;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Weight = 0.22150619138694019;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            this.xrTableCell14.Weight = 1.9809938467600321;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.13791687011718773;
            // 
            // lblSoToKhaiTamNhapTaiXuat
            // 
            this.lblSoToKhaiTamNhapTaiXuat.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiTamNhapTaiXuat.Name = "lblSoToKhaiTamNhapTaiXuat";
            this.lblSoToKhaiTamNhapTaiXuat.StylePriority.UseFont = false;
            this.lblSoToKhaiTamNhapTaiXuat.Text = "NNNNNNNNN1NE";
            this.lblSoToKhaiTamNhapTaiXuat.Weight = 5.6195833984374763;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.lblMaPhanLoaiKiemTra,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.lblMaLoaiHinh,
            this.lblMaPhanLoaiHangHoa,
            this.lblMaHieuPhuongThucVanChuyen,
            this.xrTableCell35,
            this.lblPhanLoaiCaNhanToChuc,
            this.xrTableCell19,
            this.xrTableCell21,
            this.xrTableCell18,
            this.xrTableCell22,
            this.lblMaSoHangHoaDaiDienToKhai});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseTextAlignment = false;
            this.xrTableRow3.Weight = 0.43999999999999995;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 0.221505962505113;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Mã phân loại kiểm tra";
            this.xrTableCell26.Weight = 1.1741212571555952;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 0.12353910221354357;
            // 
            // lblMaPhanLoaiKiemTra
            // 
            this.lblMaPhanLoaiKiemTra.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiKiemTra.Name = "lblMaPhanLoaiKiemTra";
            this.lblMaPhanLoaiKiemTra.StylePriority.UseFont = false;
            this.lblMaPhanLoaiKiemTra.Text = "XX E";
            this.lblMaPhanLoaiKiemTra.Weight = 0.68333367812574852;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 0.13791687011718762;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Text = "Mã loại hình";
            this.xrTableCell30.Weight = 0.78822906494140621;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 0.10244812011718735;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.StylePriority.UseFont = false;
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.Weight = 0.34375;
            // 
            // lblMaPhanLoaiHangHoa
            // 
            this.lblMaPhanLoaiHangHoa.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiHangHoa.Name = "lblMaPhanLoaiHangHoa";
            this.lblMaPhanLoaiHangHoa.StylePriority.UseFont = false;
            this.lblMaPhanLoaiHangHoa.Text = "X";
            this.lblMaPhanLoaiHangHoa.Weight = 0.20489528656005862;
            // 
            // lblMaHieuPhuongThucVanChuyen
            // 
            this.lblMaHieuPhuongThucVanChuyen.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaHieuPhuongThucVanChuyen.Name = "lblMaHieuPhuongThucVanChuyen";
            this.lblMaHieuPhuongThucVanChuyen.StylePriority.UseFont = false;
            this.lblMaHieuPhuongThucVanChuyen.Text = "X";
            this.lblMaHieuPhuongThucVanChuyen.Weight = 0.23786464691162124;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.Text = "[";
            this.xrTableCell35.Weight = 0.11286464691162124;
            // 
            // lblPhanLoaiCaNhanToChuc
            // 
            this.lblPhanLoaiCaNhanToChuc.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiCaNhanToChuc.Name = "lblPhanLoaiCaNhanToChuc";
            this.lblPhanLoaiCaNhanToChuc.StylePriority.UseFont = false;
            this.lblPhanLoaiCaNhanToChuc.Text = "X";
            this.lblPhanLoaiCaNhanToChuc.Weight = 0.15994142055511468;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.Text = "]";
            this.xrTableCell19.Weight = 0.11827485561370854;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Weight = 0.13910798549652093;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Mã số hàng hóa đại diện của tờ khai";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell18.Weight = 1.8370245504379272;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Weight = 0.13238280296325689;
            // 
            // lblMaSoHangHoaDaiDienToKhai
            // 
            this.lblMaSoHangHoaDaiDienToKhai.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSoHangHoaDaiDienToKhai.Name = "lblMaSoHangHoaDaiDienToKhai";
            this.lblMaSoHangHoaDaiDienToKhai.StylePriority.UseFont = false;
            this.lblMaSoHangHoaDaiDienToKhai.Text = "XXXE";
            this.lblMaSoHangHoaDaiDienToKhai.Weight = 1.4428000560760264;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.xrTableCell39,
            this.lblTenCoQuanHaiQuanTiepNhanToKhai,
            this.xrTableCell41,
            this.xrTableCell47,
            this.xrTableCell23,
            this.lblMaBoPhanXuLyToKhai});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.44000000000000017;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 0.22150588621117057;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            this.xrTableCell38.Weight = 2.1189112732455566;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Weight = 0.10000000435370748;
            // 
            // lblTenCoQuanHaiQuanTiepNhanToKhai
            // 
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Name = "lblTenCoQuanHaiQuanTiepNhanToKhai";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.StylePriority.UseFont = false;
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Text = "XXXXXXXXXE";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Weight = 1.968267910042105;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.13910766601562485;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell47.StylePriority.UsePadding = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "Mã bộ phận xử lý tờ khai";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell47.Weight = 1.8370246505737304;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.13238281249999989;
            // 
            // lblMaBoPhanXuLyToKhai
            // 
            this.lblMaBoPhanXuLyToKhai.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBoPhanXuLyToKhai.Name = "lblMaBoPhanXuLyToKhai";
            this.lblMaBoPhanXuLyToKhai.StylePriority.UseFont = false;
            this.lblMaBoPhanXuLyToKhai.Text = "XE";
            this.lblMaBoPhanXuLyToKhai.Weight = 1.442800103759742;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.lblNgayDangKy,
            this.xrTableCell15,
            this.lblGioDangKy,
            this.xrTableCell54,
            this.xrTableCell55,
            this.lblNgayThayDoiDangKy,
            this.lblGioThayDoiDangKy,
            this.xrTableCell58,
            this.xrTableCell59,
            this.lblThoiHanTaiNhapTaiXuat,
            this.xrTableCell42,
            this.lblBieuThiTHHetHan});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.Weight = 0.43999999999999984;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.221505962505113;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "Ngày đăng ký";
            this.xrTableCell50.Weight = 0.74516097816477034;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.099999984722052071;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Text = "dd/MM/yyyy";
            this.lblNgayDangKy.Weight = 0.66083307460806506;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 0.050000000000000044;
            // 
            // lblGioDangKy
            // 
            this.lblGioDangKy.Name = "lblGioDangKy";
            this.lblGioDangKy.Text = "hh:mm:ss";
            this.lblGioDangKy.Weight = 0.56291641235351564;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "Ngày thay đổi đăng ký";
            this.xrTableCell54.Weight = 1.161460095717697;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Weight = 0.10000028618597634;
            // 
            // lblNgayThayDoiDangKy
            // 
            this.lblNgayThayDoiDangKy.Name = "lblNgayThayDoiDangKy";
            this.lblNgayThayDoiDangKy.Text = "dd/MM/yyyy";
            this.lblNgayThayDoiDangKy.Weight = 0.8068084010849248;
            // 
            // lblGioThayDoiDangKy
            // 
            this.lblGioThayDoiDangKy.Name = "lblGioThayDoiDangKy";
            this.lblGioThayDoiDangKy.Text = "hh:mm:ss";
            this.lblGioThayDoiDangKy.Weight = 0.52730399380583526;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell58.StylePriority.UsePadding = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell58.Weight = 1.4488280868530274;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 0.13238277435302748;
            // 
            // lblThoiHanTaiNhapTaiXuat
            // 
            this.lblThoiHanTaiNhapTaiXuat.Name = "lblThoiHanTaiNhapTaiXuat";
            this.lblThoiHanTaiNhapTaiXuat.Text = "dd/MM/yyyy";
            this.lblThoiHanTaiNhapTaiXuat.Weight = 0.81098310470581036;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "-";
            this.xrTableCell42.Weight = 0.17111817359924308;
            // 
            // lblBieuThiTHHetHan
            // 
            this.lblBieuThiTHHetHan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBieuThiTHHetHan.Name = "lblBieuThiTHHetHan";
            this.lblBieuThiTHHetHan.StylePriority.UseFont = false;
            this.lblBieuThiTHHetHan.Text = "X";
            this.lblBieuThiTHHetHan.Weight = 0.46069897804257914;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell44});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.Weight = 0.44000000000000006;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Weight = 0.221506343974825;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Text = "Người nhập khẩu";
            this.xrTableCell44.Weight = 7.7384939627268121;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell62,
            this.xrTableCell61,
            this.lblMaNguoiNhapKhau});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.44000000000000006;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Weight = 0.3499999961853027;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Text = "Mã";
            this.xrTableCell62.Weight = 0.764582670291292;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 0.099999010006606048;
            // 
            // lblMaNguoiNhapKhau
            // 
            this.lblMaNguoiNhapKhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiNhapKhau.Name = "lblMaNguoiNhapKhau";
            this.lblMaNguoiNhapKhau.StylePriority.UseFont = false;
            this.lblMaNguoiNhapKhau.Text = "XXXXXXXXX1-XXE";
            this.lblMaNguoiNhapKhau.Weight = 6.7454186302184365;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.lblTenNguoiNhapKhau});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1.12;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.3499999961853027;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Text = "Tên";
            this.xrTableCell64.Weight = 0.764582670291292;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Weight = 0.099999620358145275;
            // 
            // lblTenNguoiNhapKhau
            // 
            this.lblTenNguoiNhapKhau.Font = new System.Drawing.Font("Arial", 8F);
            this.lblTenNguoiNhapKhau.Name = "lblTenNguoiNhapKhau";
            this.lblTenNguoiNhapKhau.StylePriority.UseFont = false;
            this.lblTenNguoiNhapKhau.StylePriority.UsePadding = false;
            this.lblTenNguoiNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblTenNguoiNhapKhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiNhapKhau.Weight = 6.7454180198668974;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.xrTableCell68,
            this.xrTableCell69,
            this.lblMaBuuChinhNhapKhau});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.44000000000000017;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Weight = 0.3499999961853027;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Text = "Mã bưu chính";
            this.xrTableCell68.Weight = 0.764582670291292;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Weight = 0.099999620358145275;
            // 
            // lblMaBuuChinhNhapKhau
            // 
            this.lblMaBuuChinhNhapKhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBuuChinhNhapKhau.Name = "lblMaBuuChinhNhapKhau";
            this.lblMaBuuChinhNhapKhau.StylePriority.UseFont = false;
            this.lblMaBuuChinhNhapKhau.Text = "XXXXXXE";
            this.lblMaBuuChinhNhapKhau.Weight = 6.7454180198668974;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71,
            this.xrTableCell72,
            this.xrTableCell73,
            this.lblDiaChiNguoiNhapKhau});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1.12;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Weight = 0.3499999961853027;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Text = "Địa chỉ";
            this.xrTableCell72.Weight = 0.764582670291292;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Weight = 0.099999620358145275;
            // 
            // lblDiaChiNguoiNhapKhau
            // 
            this.lblDiaChiNguoiNhapKhau.Font = new System.Drawing.Font("Arial", 8F);
            this.lblDiaChiNguoiNhapKhau.Name = "lblDiaChiNguoiNhapKhau";
            this.lblDiaChiNguoiNhapKhau.StylePriority.UseFont = false;
            this.lblDiaChiNguoiNhapKhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiNguoiNhapKhau.Weight = 6.7454180198668974;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell75,
            this.xrTableCell76,
            this.xrTableCell77,
            this.lblSoDienThoaiNguoiNhapKhau});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.43999999999999984;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Weight = 0.3499999961853027;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Text = "Số điện thoại";
            this.xrTableCell76.Weight = 0.764582670291292;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Weight = 0.099999010006606048;
            // 
            // lblSoDienThoaiNguoiNhapKhau
            // 
            this.lblSoDienThoaiNguoiNhapKhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDienThoaiNguoiNhapKhau.Name = "lblSoDienThoaiNguoiNhapKhau";
            this.lblSoDienThoaiNguoiNhapKhau.StylePriority.UseFont = false;
            this.lblSoDienThoaiNguoiNhapKhau.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoDienThoaiNguoiNhapKhau.Weight = 6.7454186302184365;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell79,
            this.xrTableCell80});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseTextAlignment = false;
            this.xrTableRow12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.xrTableRow12.Weight = 0.44000000000000017;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Weight = 0.22150573362328574;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Text = "Người ủy thác nhập khẩu";
            this.xrTableCell80.Weight = 7.7384945730783512;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.xrTableCell84,
            this.xrTableCell85,
            this.lblMaNguoiUyThacNhapKhau});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.43999999999999995;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 0.3499999961853027;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Text = "Mã";
            this.xrTableCell84.Weight = 0.764582670291292;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 0.1000002307096845;
            // 
            // lblMaNguoiUyThacNhapKhau
            // 
            this.lblMaNguoiUyThacNhapKhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiUyThacNhapKhau.Name = "lblMaNguoiUyThacNhapKhau";
            this.lblMaNguoiUyThacNhapKhau.StylePriority.UseFont = false;
            this.lblMaNguoiUyThacNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblMaNguoiUyThacNhapKhau.Text = "XXXXXXXXX1-XXE";
            this.lblMaNguoiUyThacNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            this.lblMaNguoiUyThacNhapKhau.Weight = 6.7454174095153583;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell87,
            this.xrTableCell88,
            this.xrTableCell89,
            this.lblTenNguoiUyThacNhapKhau});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1.1199999999999997;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.Weight = 0.3499999961853027;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Text = "Tên";
            this.xrTableCell88.Weight = 0.764582670291292;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 0.1000002307096845;
            // 
            // lblTenNguoiUyThacNhapKhau
            // 
            this.lblTenNguoiUyThacNhapKhau.Font = new System.Drawing.Font("Arial", 8F);
            this.lblTenNguoiUyThacNhapKhau.Name = "lblTenNguoiUyThacNhapKhau";
            this.lblTenNguoiUyThacNhapKhau.StylePriority.UseFont = false;
            this.lblTenNguoiUyThacNhapKhau.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiUyThacNhapKhau.Weight = 6.7454174095153583;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell81,
            this.xrTableCell82});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 0.44000000000000017;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Weight = 0.22150573362328574;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Text = "Người xuất khẩu";
            this.xrTableCell82.Weight = 7.7384945730783512;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95,
            this.lblMaNguoiXuatKhau});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.44000000000000039;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Weight = 0.3499999961853027;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Text = "Mã";
            this.xrTableCell94.Weight = 0.61666672134399414;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Weight = 0.24791617965698237;
            // 
            // lblMaNguoiXuatKhau
            // 
            this.lblMaNguoiXuatKhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiXuatKhau.Name = "lblMaNguoiXuatKhau";
            this.lblMaNguoiXuatKhau.StylePriority.UseFont = false;
            this.lblMaNguoiXuatKhau.Text = "XXXXXXXXX1-XXE";
            this.lblMaNguoiXuatKhau.Weight = 6.7454174095153583;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.xrTableCell98,
            this.xrTableCell99,
            this.lblTenNguoiXuatKhau});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.44000000000000017;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 0.3499999961853027;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Text = "Tên";
            this.xrTableCell98.Weight = 0.61666672134399414;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Weight = 0.24791617965698237;
            // 
            // lblTenNguoiXuatKhau
            // 
            this.lblTenNguoiXuatKhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNguoiXuatKhau.Name = "lblTenNguoiXuatKhau";
            this.lblTenNguoiXuatKhau.StylePriority.UseFont = false;
            this.lblTenNguoiXuatKhau.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblTenNguoiXuatKhau.Weight = 6.7454174095153583;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101,
            this.xrTableCell102,
            this.xrTableCell103,
            this.lblMaBuuChinhXuatKhau});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.44000000000000017;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 0.3499999961853027;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Text = "Mã bưu chính";
            this.xrTableCell102.Weight = 0.76458235377374728;
            // 
            // xrTableCell103
            // 
            this.xrTableCell103.Name = "xrTableCell103";
            this.xrTableCell103.Weight = 0.10000054722722929;
            // 
            // lblMaBuuChinhXuatKhau
            // 
            this.lblMaBuuChinhXuatKhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBuuChinhXuatKhau.Name = "lblMaBuuChinhXuatKhau";
            this.lblMaBuuChinhXuatKhau.StylePriority.UseFont = false;
            this.lblMaBuuChinhXuatKhau.Text = "XXXXXXXXE";
            this.lblMaBuuChinhXuatKhau.Weight = 6.7454174095153583;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell107,
            this.lblDiaChiNguoiXuatKhau1,
            this.lblDiaChiNguoiXuatKhau2});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.43999999999999995;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Weight = 0.3499999961853027;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Text = "Địa chỉ";
            this.xrTableCell106.Weight = 0.61666672134399414;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 0.24791648483275197;
            // 
            // lblDiaChiNguoiXuatKhau1
            // 
            this.lblDiaChiNguoiXuatKhau1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiNguoiXuatKhau1.Name = "lblDiaChiNguoiXuatKhau1";
            this.lblDiaChiNguoiXuatKhau1.StylePriority.UseFont = false;
            this.lblDiaChiNguoiXuatKhau1.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiaChiNguoiXuatKhau1.Weight = 3.2977082481384397;
            // 
            // lblDiaChiNguoiXuatKhau2
            // 
            this.lblDiaChiNguoiXuatKhau2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiNguoiXuatKhau2.Name = "lblDiaChiNguoiXuatKhau2";
            this.lblDiaChiNguoiXuatKhau2.StylePriority.UseFont = false;
            this.lblDiaChiNguoiXuatKhau2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiaChiNguoiXuatKhau2.Weight = 3.4477088562011486;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell109,
            this.xrTableCell110,
            this.xrTableCell111,
            this.lblDiaChiNguoiXuatKhau3,
            this.lblDiaChiNguoiXuatKhau4});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.43999999999999995;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Weight = 0.3499999961853027;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Weight = 0.61666672134399414;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Weight = 0.24791648483275197;
            // 
            // lblDiaChiNguoiXuatKhau3
            // 
            this.lblDiaChiNguoiXuatKhau3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiNguoiXuatKhau3.Name = "lblDiaChiNguoiXuatKhau3";
            this.lblDiaChiNguoiXuatKhau3.StylePriority.UseFont = false;
            this.lblDiaChiNguoiXuatKhau3.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiaChiNguoiXuatKhau3.Weight = 3.2977082481384397;
            // 
            // lblDiaChiNguoiXuatKhau4
            // 
            this.lblDiaChiNguoiXuatKhau4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiNguoiXuatKhau4.Name = "lblDiaChiNguoiXuatKhau4";
            this.lblDiaChiNguoiXuatKhau4.StylePriority.UseFont = false;
            this.lblDiaChiNguoiXuatKhau4.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblDiaChiNguoiXuatKhau4.Weight = 3.4477088562011486;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell115,
            this.lblMaNuoc});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.43999999999999995;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 0.3499999961853027;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Text = "Mã nước";
            this.xrTableCell114.Weight = 0.61666672134399414;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Weight = 0.24791495895390392;
            // 
            // lblMaNuoc
            // 
            this.lblMaNuoc.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNuoc.Name = "lblMaNuoc";
            this.lblMaNuoc.StylePriority.UseFont = false;
            this.lblMaNuoc.Text = "XE";
            this.lblMaNuoc.Weight = 6.7454186302184365;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.lblNguoiUyThacXuatKhau});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.43999999999999995;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Weight = 0.22150573362328577;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Text = "Người ủy thác xuất khẩu";
            this.xrTableCell118.Weight = 1.349999907192321;
            // 
            // lblNguoiUyThacXuatKhau
            // 
            this.lblNguoiUyThacXuatKhau.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoiUyThacXuatKhau.Name = "lblNguoiUyThacXuatKhau";
            this.lblNguoiUyThacXuatKhau.StylePriority.UseFont = false;
            this.lblNguoiUyThacXuatKhau.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXXE";
            this.lblNguoiUyThacXuatKhau.Weight = 6.3884946658860322;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell123,
            this.lblMaDaiLyHaiQuan,
            this.lblTenDaiLyHaiQuan,
            this.xrTableCell129,
            this.lblMaNhanVienHaiQuan});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.StylePriority.UseBorders = false;
            this.xrTableRow23.Weight = 0.44000000000000061;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Weight = 0.22150565732934335;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Text = "Đại lý Hải quan";
            this.xrTableCell122.Weight = 0.89307693285331191;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Weight = 0.099999315182366988;
            // 
            // lblMaDaiLyHaiQuan
            // 
            this.lblMaDaiLyHaiQuan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDaiLyHaiQuan.Name = "lblMaDaiLyHaiQuan";
            this.lblMaDaiLyHaiQuan.StylePriority.UseFont = false;
            this.lblMaDaiLyHaiQuan.Text = "XXXXE";
            this.lblMaDaiLyHaiQuan.Weight = 0.56291841310257151;
            // 
            // lblTenDaiLyHaiQuan
            // 
            this.lblTenDaiLyHaiQuan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDaiLyHaiQuan.Name = "lblTenDaiLyHaiQuan";
            this.lblTenDaiLyHaiQuan.StylePriority.UseFont = false;
            this.lblTenDaiLyHaiQuan.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXXE";
            this.lblTenDaiLyHaiQuan.Weight = 4.3217184792161216;
            // 
            // xrTableCell129
            // 
            this.xrTableCell129.Name = "xrTableCell129";
            this.xrTableCell129.Text = "Mã nhân viên Hải quan";
            this.xrTableCell129.Weight = 1.2289648342132571;
            // 
            // lblMaNhanVienHaiQuan
            // 
            this.lblMaNhanVienHaiQuan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNhanVienHaiQuan.Name = "lblMaNhanVienHaiQuan";
            this.lblMaNhanVienHaiQuan.StylePriority.UseFont = false;
            this.lblMaNhanVienHaiQuan.Text = "XXXXE";
            this.lblMaNhanVienHaiQuan.Weight = 0.63181667480466408;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell125,
            this.xrTableCell126,
            this.xrTableCell127,
            this.xrTableCell131,
            this.xrTableCell134,
            this.lblMaDiaDiemLuuKhoHang,
            this.xrTableCell135,
            this.lblTenDiaDiemLuuKhoHang});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.43999999999999995;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 0.22150603879905537;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Text = "Số vận đơn";
            this.xrTableCell126.Weight = 2.9071399639029414;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.StylePriority.UseBorders = false;
            this.xrTableCell127.Weight = 0.1024478149155037;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Text = "Địa điểm lưu kho";
            this.xrTableCell131.Weight = 0.99937503116560411;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Weight = 0.099999074805913982;
            // 
            // lblMaDiaDiemLuuKhoHang
            // 
            this.lblMaDiaDiemLuuKhoHang.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemLuuKhoHang.Name = "lblMaDiaDiemLuuKhoHang";
            this.lblMaDiaDiemLuuKhoHang.StylePriority.UseFont = false;
            this.lblMaDiaDiemLuuKhoHang.Text = "XXXXXXE";
            this.lblMaDiaDiemLuuKhoHang.Weight = 0.60552147026714942;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Weight = 0.1563030889014016;
            // 
            // lblTenDiaDiemLuuKhoHang
            // 
            this.lblTenDiaDiemLuuKhoHang.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDiaDiemLuuKhoHang.Name = "lblTenDiaDiemLuuKhoHang";
            this.lblTenDiaDiemLuuKhoHang.StylePriority.UseFont = false;
            this.lblTenDiaDiemLuuKhoHang.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblTenDiaDiemLuuKhoHang.Weight = 2.8677078239440696;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell136,
            this.xrTableCell145,
            this.lblSoVanDon1,
            this.xrTableCell138,
            this.xrTableCell139,
            this.xrTableCell140,
            this.lblMaDiaDiemDoHang,
            this.xrTableCell142,
            this.lblTenDiaDiemDoHang});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.44000000000000039;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Weight = 0.35000005245208521;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Text = "1";
            this.xrTableCell145.Weight = 0.14999999032221509;
            // 
            // lblSoVanDon1
            // 
            this.lblSoVanDon1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoVanDon1.Name = "lblSoVanDon1";
            this.lblSoVanDon1.StylePriority.UseFont = false;
            this.lblSoVanDon1.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3";
            this.lblSoVanDon1.Weight = 2.6286459599276966;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.StylePriority.UseBorders = false;
            this.xrTableCell138.Weight = 0.1024478149155037;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Text = "Địa điểm dỡ hàng";
            this.xrTableCell139.Weight = 0.99937411563836553;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Weight = 0.10000090586039123;
            // 
            // lblMaDiaDiemDoHang
            // 
            this.lblMaDiaDiemDoHang.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemDoHang.Name = "lblMaDiaDiemDoHang";
            this.lblMaDiaDiemDoHang.StylePriority.UseFont = false;
            this.lblMaDiaDiemDoHang.Text = "XXXXXE";
            this.lblMaDiaDiemDoHang.Weight = 0.60552055473991073;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Weight = 0.1563030889014016;
            // 
            // lblTenDiaDiemDoHang
            // 
            this.lblTenDiaDiemDoHang.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDiaDiemDoHang.Name = "lblTenDiaDiemDoHang";
            this.lblTenDiaDiemDoHang.StylePriority.UseFont = false;
            this.lblTenDiaDiemDoHang.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTenDiaDiemDoHang.Weight = 2.8677078239440696;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell144,
            this.xrTableCell146,
            this.lblSoVanDon2,
            this.xrTableCell148,
            this.xrTableCell149,
            this.xrTableCell150,
            this.lblMaDiaDiemXepHang,
            this.xrTableCell152,
            this.lblTenDiaDiemXepHang});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.43999999999999995;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Weight = 0.3500000905990564;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Text = "2";
            this.xrTableCell146.Weight = 0.14999977097713063;
            // 
            // lblSoVanDon2
            // 
            this.lblSoVanDon2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoVanDon2.Name = "lblSoVanDon2";
            this.lblSoVanDon2.StylePriority.UseFont = false;
            this.lblSoVanDon2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3";
            this.lblSoVanDon2.Weight = 2.6286461411258095;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.StylePriority.UseBorders = false;
            this.xrTableCell148.Weight = 0.1024478149155037;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Text = "Địa điểm xếp hàng";
            this.xrTableCell149.Weight = 0.99937381046261931;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Weight = 0.10000090586039122;
            // 
            // lblMaDiaDiemXepHang
            // 
            this.lblMaDiaDiemXepHang.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDiaDiemXepHang.Name = "lblMaDiaDiemXepHang";
            this.lblMaDiaDiemXepHang.StylePriority.UseFont = false;
            this.lblMaDiaDiemXepHang.Text = "XXXXE";
            this.lblMaDiaDiemXepHang.Weight = 0.605520859915657;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Weight = 0.1563030889014016;
            // 
            // lblTenDiaDiemXepHang
            // 
            this.lblTenDiaDiemXepHang.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenDiaDiemXepHang.Name = "lblTenDiaDiemXepHang";
            this.lblTenDiaDiemXepHang.StylePriority.UseFont = false;
            this.lblTenDiaDiemXepHang.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTenDiaDiemXepHang.Weight = 2.8677078239440696;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell154,
            this.xrTableCell155,
            this.lblSoVanDon3,
            this.xrTableCell157,
            this.xrTableCell158});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.44000000000000039;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Weight = 0.35000001430511407;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Text = "3";
            this.xrTableCell155.Weight = 0.1499998472710731;
            // 
            // lblSoVanDon3
            // 
            this.lblSoVanDon3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoVanDon3.Name = "lblSoVanDon3";
            this.lblSoVanDon3.StylePriority.UseFont = false;
            this.lblSoVanDon3.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3";
            this.lblSoVanDon3.Weight = 2.6286461411258095;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.StylePriority.UseBorders = false;
            this.xrTableCell157.Weight = 0.1024478149155037;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Text = "Phương tiện vận chuyển";
            this.xrTableCell158.Weight = 4.7289064890841388;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell163,
            this.xrTableCell164,
            this.lblSoVanDon4,
            this.xrTableCell166,
            this.xrTableCell167,
            this.lblMaPhuongTienVanChuyen,
            this.xrTableCell170,
            this.lblTenPhuongTienVanChuyen});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.44000000000000039;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Weight = 0.35000009059905646;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Text = "4";
            this.xrTableCell164.Weight = 0.14999961838924589;
            // 
            // lblSoVanDon4
            // 
            this.lblSoVanDon4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoVanDon4.Name = "lblSoVanDon4";
            this.lblSoVanDon4.StylePriority.UseFont = false;
            this.lblSoVanDon4.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3";
            this.lblSoVanDon4.Weight = 2.6286462937136945;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.StylePriority.UseBorders = false;
            this.xrTableCell166.Weight = 0.1024478149155037;
            // 
            // xrTableCell167
            // 
            this.xrTableCell167.Name = "xrTableCell167";
            this.xrTableCell167.Weight = 0.90107143832726921;
            // 
            // lblMaPhuongTienVanChuyen
            // 
            this.lblMaPhuongTienVanChuyen.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhuongTienVanChuyen.Name = "lblMaPhuongTienVanChuyen";
            this.lblMaPhuongTienVanChuyen.StylePriority.UseFont = false;
            this.lblMaPhuongTienVanChuyen.Text = "XXXXXXXXE";
            this.lblMaPhuongTienVanChuyen.Weight = 0.8038241379113984;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Weight = 0.15630308890140168;
            // 
            // lblTenPhuongTienVanChuyen
            // 
            this.lblTenPhuongTienVanChuyen.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenPhuongTienVanChuyen.Name = "lblTenPhuongTienVanChuyen";
            this.lblTenPhuongTienVanChuyen.StylePriority.UseFont = false;
            this.lblTenPhuongTienVanChuyen.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblTenPhuongTienVanChuyen.Weight = 2.8677078239440692;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell172,
            this.xrTableCell173,
            this.lblSoVanDon5,
            this.xrTableCell175,
            this.xrTableCell176,
            this.xrTableCell177,
            this.xrTableCell178,
            this.xrTableCell179,
            this.lblNgayHangDen});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 0.43999999999999995;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Weight = 0.35000001430511396;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Text = "5";
            this.xrTableCell173.Weight = 0.1499995420953035;
            // 
            // lblSoVanDon5
            // 
            this.lblSoVanDon5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoVanDon5.Name = "lblSoVanDon5";
            this.lblSoVanDon5.StylePriority.UseFont = false;
            this.lblSoVanDon5.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3";
            this.lblSoVanDon5.Weight = 2.6286464463015791;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.StylePriority.UseBorders = false;
            this.xrTableCell175.Weight = 0.1024478149155037;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Text = "Ngày hàng đến";
            this.xrTableCell176.Weight = 0.9010714383272691;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.Weight = 0.098303213184233865;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Weight = 0.70552092472716454;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Weight = 0.1563030889014016;
            // 
            // lblNgayHangDen
            // 
            this.lblNgayHangDen.Name = "lblNgayHangDen";
            this.lblNgayHangDen.Text = "dd/MM/yyyy";
            this.lblNgayHangDen.Weight = 2.8677078239440696;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(12.1355F, 387.5F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow30,
            this.xrTableRow32,
            this.xrTableRow33});
            this.xrTable2.SizeF = new System.Drawing.SizeF(312.8645F, 33F);
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UsePadding = false;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell159,
            this.xrTableCell160,
            this.lblSoLuong,
            this.xrTableCell183,
            this.lblMaDonViTinh});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 0.44000089518411295;
            // 
            // xrTableCell159
            // 
            this.xrTableCell159.Name = "xrTableCell159";
            this.xrTableCell159.Weight = 0.22122672682581474;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Text = "Số lượng";
            this.xrTableCell160.Weight = 0.99182792952088528;
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuong.StylePriority.UsePadding = false;
            this.lblSoLuong.StylePriority.UseTextAlignment = false;
            this.lblSoLuong.Text = "12.345.678";
            this.lblSoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuong.Weight = 1.4909384824735124;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Weight = 0.09987433292745497;
            // 
            // lblMaDonViTinh
            // 
            this.lblMaDonViTinh.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDonViTinh.Name = "lblMaDonViTinh";
            this.lblMaDonViTinh.StylePriority.UseFont = false;
            this.lblMaDonViTinh.Text = "XXE";
            this.lblMaDonViTinh.Weight = 0.32084634824230457;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell184,
            this.xrTableCell185,
            this.lblTongTrongLuongHang,
            this.xrTableCell187,
            this.lblMaDonViTinhTrongLuong});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 0.44000089518411284;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Weight = 0.22122657442966842;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Text = "Tổng trọng lượng hàng (Gross)";
            this.xrTableCell185.Weight = 1.6039771515410555;
            // 
            // lblTongTrongLuongHang
            // 
            this.lblTongTrongLuongHang.Name = "lblTongTrongLuongHang";
            this.lblTongTrongLuongHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongTrongLuongHang.StylePriority.UsePadding = false;
            this.lblTongTrongLuongHang.StylePriority.UseTextAlignment = false;
            this.lblTongTrongLuongHang.Text = "1.234.567.890";
            this.lblTongTrongLuongHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongTrongLuongHang.Weight = 0.87878941284948864;
            // 
            // xrTableCell187
            // 
            this.xrTableCell187.Name = "xrTableCell187";
            this.xrTableCell187.Weight = 0.099874332927455;
            // 
            // lblMaDonViTinhTrongLuong
            // 
            this.lblMaDonViTinhTrongLuong.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDonViTinhTrongLuong.Name = "lblMaDonViTinhTrongLuong";
            this.lblMaDonViTinhTrongLuong.StylePriority.UseFont = false;
            this.lblMaDonViTinhTrongLuong.Text = "XXE";
            this.lblMaDonViTinhTrongLuong.Weight = 0.32084634824230462;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell189,
            this.xrTableCell190,
            this.lblSoLuongContainer,
            this.xrTableCell192,
            this.xrTableCell193});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 0.44000089518411289;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Weight = 0.22122642203352214;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Text = "Số lượng container";
            this.xrTableCell190.Weight = 1.5041028795633677;
            // 
            // lblSoLuongContainer
            // 
            this.lblSoLuongContainer.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuongContainer.Name = "lblSoLuongContainer";
            this.lblSoLuongContainer.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuongContainer.StylePriority.UseFont = false;
            this.lblSoLuongContainer.StylePriority.UsePadding = false;
            this.lblSoLuongContainer.StylePriority.UseTextAlignment = false;
            this.lblSoLuongContainer.Text = "NNE";
            this.lblSoLuongContainer.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuongContainer.Weight = 0.97866383722332251;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.Weight = 0.099874332927455012;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Weight = 0.32084634824230462;
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(325F, 387.5F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31,
            this.xrTableRow34,
            this.xrTableRow35});
            this.xrTable3.SizeF = new System.Drawing.SizeF(483.1355F, 62F);
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UsePadding = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell194,
            this.xrTableCell162,
            this.xrTableCell168,
            this.lblKyHieuSoHieu});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 1.6000000000000003;
            // 
            // xrTableCell194
            // 
            this.xrTableCell194.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell194.Name = "xrTableCell194";
            this.xrTableCell194.StylePriority.UseBorders = false;
            this.xrTableCell194.Weight = 0.10244812003664847;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Text = "Ký hiệu và số hiệu";
            this.xrTableCell162.Weight = 0.94999995190761377;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.Weight = 0.049373993561997113;
            // 
            // lblKyHieuSoHieu
            // 
            this.lblKyHieuSoHieu.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKyHieuSoHieu.Name = "lblKyHieuSoHieu";
            this.lblKyHieuSoHieu.StylePriority.UseFont = false;
            this.lblKyHieuSoHieu.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXX6XXXXXXXXX7XXXXXXXXX8X" +
                "XXXXXXXX9XXXXXXXXX0XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXXE";
            this.lblKyHieuSoHieu.Weight = 3.7295324947542263;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell195,
            this.xrTableCell196,
            this.lblNgayDuocPhepNhapKho});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.44000000000000006;
            // 
            // xrTableCell195
            // 
            this.xrTableCell195.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell195.Name = "xrTableCell195";
            this.xrTableCell195.StylePriority.UseBorders = false;
            this.xrTableCell195.Weight = 0.10244781486090335;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.Text = "Ngày được phép nhập kho đầu tiên";
            this.xrTableCell196.Weight = 1.8611991857528067;
            // 
            // lblNgayDuocPhepNhapKho
            // 
            this.lblNgayDuocPhepNhapKho.Name = "lblNgayDuocPhepNhapKho";
            this.lblNgayDuocPhepNhapKho.Text = "dd/MM/yyyy";
            this.lblNgayDuocPhepNhapKho.Weight = 2.8677075596467754;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell199,
            this.xrTableCell200,
            this.xrTableCell201,
            this.lblMaVanBanPhapQuyKhac1,
            this.lblMaVanBanPhapQuyKhac2,
            this.lblMaVanBanPhapQuyKhac3,
            this.lblMaVanBanPhapQuyKhac4,
            this.lblMaVanBanPhapQuyKhac5});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 0.44000000000000006;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Borders = DevExpress.XtraPrinting.BorderSide.Left;
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.StylePriority.UseBorders = false;
            this.xrTableCell199.Weight = 0.10244781486090332;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Text = "Mã văn bản pháp quy khác";
            this.xrTableCell200.Weight = 1.4461976574738316;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Weight = 0.12500060781508968;
            // 
            // lblMaVanBanPhapQuyKhac1
            // 
            this.lblMaVanBanPhapQuyKhac1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaVanBanPhapQuyKhac1.Name = "lblMaVanBanPhapQuyKhac1";
            this.lblMaVanBanPhapQuyKhac1.StylePriority.UseFont = false;
            this.lblMaVanBanPhapQuyKhac1.Text = "XE";
            this.lblMaVanBanPhapQuyKhac1.Weight = 0.2999999606675598;
            // 
            // lblMaVanBanPhapQuyKhac2
            // 
            this.lblMaVanBanPhapQuyKhac2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaVanBanPhapQuyKhac2.Name = "lblMaVanBanPhapQuyKhac2";
            this.lblMaVanBanPhapQuyKhac2.StylePriority.UseFont = false;
            this.lblMaVanBanPhapQuyKhac2.Text = "XE";
            this.lblMaVanBanPhapQuyKhac2.Weight = 0.29999996066745566;
            // 
            // lblMaVanBanPhapQuyKhac3
            // 
            this.lblMaVanBanPhapQuyKhac3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaVanBanPhapQuyKhac3.Name = "lblMaVanBanPhapQuyKhac3";
            this.lblMaVanBanPhapQuyKhac3.StylePriority.UseFont = false;
            this.lblMaVanBanPhapQuyKhac3.Text = "XE";
            this.lblMaVanBanPhapQuyKhac3.Weight = 0.29999996066745549;
            // 
            // lblMaVanBanPhapQuyKhac4
            // 
            this.lblMaVanBanPhapQuyKhac4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaVanBanPhapQuyKhac4.Name = "lblMaVanBanPhapQuyKhac4";
            this.lblMaVanBanPhapQuyKhac4.StylePriority.UseFont = false;
            this.lblMaVanBanPhapQuyKhac4.Text = "XE";
            this.lblMaVanBanPhapQuyKhac4.Weight = 0.29999996066745127;
            // 
            // lblMaVanBanPhapQuyKhac5
            // 
            this.lblMaVanBanPhapQuyKhac5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaVanBanPhapQuyKhac5.Name = "lblMaVanBanPhapQuyKhac5";
            this.lblMaVanBanPhapQuyKhac5.StylePriority.UseFont = false;
            this.lblMaVanBanPhapQuyKhac5.Text = "XE";
            this.lblMaVanBanPhapQuyKhac5.Weight = 1.9577086374407393;
            // 
            // xrTable4
            // 
            this.xrTable4.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 450F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow37,
            this.xrTableRow38,
            this.xrTableRow39,
            this.xrTableRow40,
            this.xrTableRow41,
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow44,
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47,
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50,
            this.xrTableRow51,
            this.xrTableRow52,
            this.xrTableRow53,
            this.xrTableRow54,
            this.xrTableRow55,
            this.xrTableRow56,
            this.xrTableRow57,
            this.xrTableRow58,
            this.xrTableRow59,
            this.xrTableRow60,
            this.xrTableRow61,
            this.xrTableRow62,
            this.xrTableRow63,
            this.xrTableRow64,
            this.xrTableRow69,
            this.xrTableRow68,
            this.xrTableRow67,
            this.xrTableRow66,
            this.xrTableRow65});
            this.xrTable4.SizeF = new System.Drawing.SizeF(795.9999F, 387.5F);
            this.xrTable4.StylePriority.UseFont = false;
            this.xrTable4.StylePriority.UsePadding = false;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell212,
            this.xrTableCell213,
            this.xrTableCell214,
            this.lblPhanLoaiHinhThucHoaDon,
            this.xrTableCell232,
            this.lblSoHoaDon});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.StylePriority.UseBorders = false;
            this.xrTableRow37.Weight = 0.44000069814465004;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Weight = 0.079748249638059043;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Text = "Số hoá đơn";
            this.xrTableCell213.Weight = 0.5422014471430372;
            // 
            // xrTableCell214
            // 
            this.xrTableCell214.Name = "xrTableCell214";
            this.xrTableCell214.StylePriority.UseTextAlignment = false;
            this.xrTableCell214.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell214.Weight = 0.018001427894171862;
            // 
            // lblPhanLoaiHinhThucHoaDon
            // 
            this.lblPhanLoaiHinhThucHoaDon.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiHinhThucHoaDon.Name = "lblPhanLoaiHinhThucHoaDon";
            this.lblPhanLoaiHinhThucHoaDon.StylePriority.UseFont = false;
            this.lblPhanLoaiHinhThucHoaDon.Text = "X";
            this.lblPhanLoaiHinhThucHoaDon.Weight = 0.069171549007943867;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Text = "-";
            this.xrTableCell232.Weight = 0.08384062126199146;
            // 
            // lblSoHoaDon
            // 
            this.lblSoHoaDon.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHoaDon.Name = "lblSoHoaDon";
            this.lblSoHoaDon.StylePriority.UseFont = false;
            this.lblSoHoaDon.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXE";
            this.lblSoHoaDon.Weight = 2.0728652935165548;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell217,
            this.xrTableCell218,
            this.xrTableCell219,
            this.lblSoTiepNhanHoaDonDienTu,
            this.xrTableCell221});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 0.44000069814464993;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Weight = 0.079748222121823753;
            // 
            // xrTableCell218
            // 
            this.xrTableCell218.Name = "xrTableCell218";
            this.xrTableCell218.Text = "Số tiếp nhận hóa đơn điện tử";
            this.xrTableCell218.Weight = 0.54220142075666034;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.StylePriority.UseTextAlignment = false;
            this.xrTableCell219.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell219.Weight = 0.018001509264798488;
            // 
            // lblSoTiepNhanHoaDonDienTu
            // 
            this.lblSoTiepNhanHoaDonDienTu.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTiepNhanHoaDonDienTu.Name = "lblSoTiepNhanHoaDonDienTu";
            this.lblSoTiepNhanHoaDonDienTu.StylePriority.UseFont = false;
            this.lblSoTiepNhanHoaDonDienTu.Text = "NNNNNNNNN1NE";
            this.lblSoTiepNhanHoaDonDienTu.Weight = 1.9218194968130162;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Weight = 0.30405793950545912;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell222,
            this.xrTableCell223,
            this.xrTableCell224,
            this.lblNgayPhatHanh,
            this.xrTableCell226});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 0.44000065999761762;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Weight = 0.079748139765997164;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Text = "Ngày phát hành";
            this.xrTableCell223.Weight = 0.54220168033249749;
            // 
            // xrTableCell224
            // 
            this.xrTableCell224.Name = "xrTableCell224";
            this.xrTableCell224.StylePriority.UseTextAlignment = false;
            this.xrTableCell224.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell224.Weight = 0.018001328055567756;
            // 
            // lblNgayPhatHanh
            // 
            this.lblNgayPhatHanh.Name = "lblNgayPhatHanh";
            this.lblNgayPhatHanh.Text = "dd/MM/yyyy";
            this.lblNgayPhatHanh.Weight = 1.9218195008022367;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Weight = 0.30405793950545912;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell227,
            this.xrTableCell228,
            this.xrTableCell229,
            this.lblPhuongThucThanhToan,
            this.xrTableCell231});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 0.4400006669360626;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Weight = 0.079748112249761888;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Text = "Phương thức thanh toán";
            this.xrTableCell228.Weight = 0.54220159797667511;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.Weight = 0.018001441916845495;
            // 
            // lblPhuongThucThanhToan
            // 
            this.lblPhuongThucThanhToan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhuongThucThanhToan.Name = "lblPhuongThucThanhToan";
            this.lblPhuongThucThanhToan.StylePriority.UseFont = false;
            this.lblPhuongThucThanhToan.Text = "XXXXXXE";
            this.lblPhuongThucThanhToan.Weight = 1.9218194968130162;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Weight = 0.30405793950545912;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell233,
            this.xrTableCell234,
            this.xrTableCell235,
            this.lblMaPhanLoaiGiaHoaDon,
            this.xrTableCell254,
            this.lblMaDieuKienGiaHoaDon,
            this.xrTableCell253,
            this.lblMaDongTienHoaDon,
            this.xrTableCell236,
            this.lblTongTriGiaHoaDon,
            this.xrTableCell237});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 0.44000066693606243;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Weight = 0.079748112249761888;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Text = "Tổng trị giá hóa đơn";
            this.xrTableCell234.Weight = 0.54220159797667511;
            // 
            // xrTableCell235
            // 
            this.xrTableCell235.Name = "xrTableCell235";
            this.xrTableCell235.Weight = 0.018001441916845495;
            // 
            // lblMaPhanLoaiGiaHoaDon
            // 
            this.lblMaPhanLoaiGiaHoaDon.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiGiaHoaDon.Name = "lblMaPhanLoaiGiaHoaDon";
            this.lblMaPhanLoaiGiaHoaDon.StylePriority.UseFont = false;
            this.lblMaPhanLoaiGiaHoaDon.Text = "X";
            this.lblMaPhanLoaiGiaHoaDon.Weight = 0.069171668323088048;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Text = "-";
            this.xrTableCell254.Weight = 0.042595320591126029;
            // 
            // lblMaDieuKienGiaHoaDon
            // 
            this.lblMaDieuKienGiaHoaDon.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDieuKienGiaHoaDon.Name = "lblMaDieuKienGiaHoaDon";
            this.lblMaDieuKienGiaHoaDon.StylePriority.UseFont = false;
            this.lblMaDieuKienGiaHoaDon.Text = "XXE";
            this.lblMaDieuKienGiaHoaDon.Weight = 0.13195011741885915;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Text = "-";
            this.xrTableCell253.Weight = 0.039803334265818613;
            // 
            // lblMaDongTienHoaDon
            // 
            this.lblMaDongTienHoaDon.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDongTienHoaDon.Name = "lblMaDongTienHoaDon";
            this.lblMaDongTienHoaDon.StylePriority.UseFont = false;
            this.lblMaDongTienHoaDon.Text = "XXE";
            this.lblMaDongTienHoaDon.Weight = 0.12128767739092039;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Text = "-";
            this.xrTableCell236.Weight = 0.03663977204548674;
            // 
            // lblTongTriGiaHoaDon
            // 
            this.lblTongTriGiaHoaDon.Name = "lblTongTriGiaHoaDon";
            this.lblTongTriGiaHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongTriGiaHoaDon.StylePriority.UsePadding = false;
            this.lblTongTriGiaHoaDon.StylePriority.UseTextAlignment = false;
            this.lblTongTriGiaHoaDon.Text = "12.345.678.901.234.567.890";
            this.lblTongTriGiaHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongTriGiaHoaDon.Weight = 0.55593722210233887;
            // 
            // xrTableCell237
            // 
            this.xrTableCell237.Name = "xrTableCell237";
            this.xrTableCell237.Weight = 1.2284923241808374;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell238,
            this.xrTableCell239,
            this.xrTableCell240,
            this.xrTableCell241,
            this.lblTongTriGiaTinhThue,
            this.xrTableCell242});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.Weight = 0.44000066693606249;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.Weight = 0.079748112249761888;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.Text = "Tổng trị giá tính thuế";
            this.xrTableCell239.Weight = 0.54220159797667511;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Weight = 0.018001441916845495;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Weight = 0.44144778016324127;
            // 
            // lblTongTriGiaTinhThue
            // 
            this.lblTongTriGiaTinhThue.Name = "lblTongTriGiaTinhThue";
            this.lblTongTriGiaTinhThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongTriGiaTinhThue.StylePriority.UsePadding = false;
            this.lblTongTriGiaTinhThue.StylePriority.UseTextAlignment = false;
            this.lblTongTriGiaTinhThue.Text = "1.234.567.890.123.456.789";
            this.lblTongTriGiaTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongTriGiaTinhThue.Weight = 0.55593744184645422;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.Weight = 1.2284922143087798;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell243,
            this.xrTableCell244,
            this.xrTableCell245,
            this.lblTongHeSoPhanBoTriGia,
            this.xrTableCell260,
            this.lblMaPhanLoaiNhapLieu});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 0.44000066693606277;
            // 
            // xrTableCell243
            // 
            this.xrTableCell243.Name = "xrTableCell243";
            this.xrTableCell243.Weight = 0.079748112249761888;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Text = "Tổng hệ số phân bổ trị giá";
            this.xrTableCell244.Weight = 0.54220159797667511;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Weight = 0.018001441916845495;
            // 
            // lblTongHeSoPhanBoTriGia
            // 
            this.lblTongHeSoPhanBoTriGia.Name = "lblTongHeSoPhanBoTriGia";
            this.lblTongHeSoPhanBoTriGia.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongHeSoPhanBoTriGia.StylePriority.UsePadding = false;
            this.lblTongHeSoPhanBoTriGia.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGia.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongHeSoPhanBoTriGia.Weight = 0.94730219461686149;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.StylePriority.UseTextAlignment = false;
            this.xrTableCell260.Text = "-";
            this.xrTableCell260.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell260.Weight = 0.050083082328862882;
            // 
            // lblMaPhanLoaiNhapLieu
            // 
            this.lblMaPhanLoaiNhapLieu.Name = "lblMaPhanLoaiNhapLieu";
            this.lblMaPhanLoaiNhapLieu.Text = "X";
            this.lblMaPhanLoaiNhapLieu.Weight = 1.2284921593727509;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell248,
            this.xrTableCell249,
            this.xrTableCell250,
            this.lblMaKetQuaKiemTraNoiDung,
            this.xrTableCell252});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 0.44000067091238271;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Weight = 0.079748112249761888;
            // 
            // xrTableCell249
            // 
            this.xrTableCell249.Name = "xrTableCell249";
            this.xrTableCell249.Text = "Mã kết quả kiểm tra nội dung";
            this.xrTableCell249.Weight = 0.54220159797667511;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.Weight = 0.018001441916845495;
            // 
            // lblMaKetQuaKiemTraNoiDung
            // 
            this.lblMaKetQuaKiemTraNoiDung.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaKetQuaKiemTraNoiDung.Name = "lblMaKetQuaKiemTraNoiDung";
            this.lblMaKetQuaKiemTraNoiDung.StylePriority.UseFont = false;
            this.lblMaKetQuaKiemTraNoiDung.Text = "X";
            this.lblMaKetQuaKiemTraNoiDung.Weight = 1.9218194968130162;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Weight = 0.30405793950545912;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell261,
            this.xrTableCell262,
            this.xrTableCell263,
            this.xrTableCell264,
            this.xrTableCell265});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.StylePriority.UseBorders = false;
            this.xrTableRow45.Weight = 0.44000066703584029;
            // 
            // xrTableCell261
            // 
            this.xrTableCell261.Name = "xrTableCell261";
            this.xrTableCell261.Weight = 0.079748112249761888;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.Text = "Giấy phép nhập khẩu";
            this.xrTableCell262.Weight = 0.54220159797667511;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.Weight = 0.018001441916845495;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.Weight = 1.9218194968130162;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.Weight = 0.30405793950545912;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell266,
            this.xrTableCell267,
            this.lblPhanLoaiGiayPhepNhapKhau1,
            this.xrTableCell278,
            this.lblSoGiayPhep1,
            this.xrTableCell279,
            this.lblPhanLoaiGiayPhepNhapKhau2,
            this.xrTableCell280,
            this.lblSoGiayPhep2,
            this.xrTableCell269,
            this.lblPhanLoaiGiayPhepNhapKhau3,
            this.xrTableCell282,
            this.lblSoGiayPhep3});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 0.44000066703584495;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.Weight = 0.12601006527156411;
            // 
            // xrTableCell267
            // 
            this.xrTableCell267.Name = "xrTableCell267";
            this.xrTableCell267.Text = "1";
            this.xrTableCell267.Weight = 0.054004204783474889;
            // 
            // lblPhanLoaiGiayPhepNhapKhau1
            // 
            this.lblPhanLoaiGiayPhepNhapKhau1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiGiayPhepNhapKhau1.Name = "lblPhanLoaiGiayPhepNhapKhau1";
            this.lblPhanLoaiGiayPhepNhapKhau1.StylePriority.UseFont = false;
            this.lblPhanLoaiGiayPhepNhapKhau1.Text = "XXXE";
            this.lblPhanLoaiGiayPhepNhapKhau1.Weight = 0.16801363203172298;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.Text = "-";
            this.xrTableCell278.Weight = 0.053253892827404191;
            // 
            // lblSoGiayPhep1
            // 
            this.lblSoGiayPhep1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoGiayPhep1.Name = "lblSoGiayPhep1";
            this.lblSoGiayPhep1.StylePriority.UseFont = false;
            this.lblSoGiayPhep1.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoGiayPhep1.Weight = 0.64347731973089384;
            // 
            // xrTableCell279
            // 
            this.xrTableCell279.Name = "xrTableCell279";
            this.xrTableCell279.Text = "2";
            this.xrTableCell279.Weight = 0.054004311452862663;
            // 
            // lblPhanLoaiGiayPhepNhapKhau2
            // 
            this.lblPhanLoaiGiayPhepNhapKhau2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiGiayPhepNhapKhau2.Name = "lblPhanLoaiGiayPhepNhapKhau2";
            this.lblPhanLoaiGiayPhepNhapKhau2.StylePriority.UseFont = false;
            this.lblPhanLoaiGiayPhepNhapKhau2.Text = "XXXE";
            this.lblPhanLoaiGiayPhepNhapKhau2.Weight = 0.16201293901861388;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.Text = "-";
            this.xrTableCell280.Weight = 0.036002878062228169;
            // 
            // lblSoGiayPhep2
            // 
            this.lblSoGiayPhep2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoGiayPhep2.Name = "lblSoGiayPhep2";
            this.lblSoGiayPhep2.StylePriority.UseFont = false;
            this.lblSoGiayPhep2.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoGiayPhep2.Weight = 0.69098738034922835;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.Text = "3";
            this.xrTableCell269.Weight = 0.054004314886364493;
            // 
            // lblPhanLoaiGiayPhepNhapKhau3
            // 
            this.lblPhanLoaiGiayPhepNhapKhau3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiGiayPhepNhapKhau3.Name = "lblPhanLoaiGiayPhepNhapKhau3";
            this.lblPhanLoaiGiayPhepNhapKhau3.StylePriority.UseFont = false;
            this.lblPhanLoaiGiayPhepNhapKhau3.Text = "XXXE";
            this.lblPhanLoaiGiayPhepNhapKhau3.Weight = 0.16201294785510628;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.Text = "-";
            this.xrTableCell282.Weight = 0.03600287892822799;
            // 
            // lblSoGiayPhep3
            // 
            this.lblSoGiayPhep3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoGiayPhep3.Name = "lblSoGiayPhep3";
            this.lblSoGiayPhep3.StylePriority.UseFont = false;
            this.lblSoGiayPhep3.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoGiayPhep3.Weight = 0.6260418232640661;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell271,
            this.xrTableCell272,
            this.lblPhanLoaiGiayPhepNhapKhau4,
            this.xrTableCell286,
            this.lblSoGiayPhep4,
            this.xrTableCell284,
            this.lblPhanLoaiGiayPhepNhapKhau5,
            this.xrTableCell274,
            this.lblSoGiayPhep5});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 0.44000074332990957;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.Weight = 0.12601006870506618;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.Text = "4";
            this.xrTableCell272.Weight = 0.054004149898476822;
            // 
            // lblPhanLoaiGiayPhepNhapKhau4
            // 
            this.lblPhanLoaiGiayPhepNhapKhau4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiGiayPhepNhapKhau4.Name = "lblPhanLoaiGiayPhepNhapKhau4";
            this.lblPhanLoaiGiayPhepNhapKhau4.StylePriority.UseFont = false;
            this.lblPhanLoaiGiayPhepNhapKhau4.Text = "XXXE";
            this.lblPhanLoaiGiayPhepNhapKhau4.Weight = 0.16801363541419187;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.Text = "-";
            this.xrTableCell286.Weight = 0.053253999265961922;
            // 
            // lblSoGiayPhep4
            // 
            this.lblSoGiayPhep4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoGiayPhep4.Name = "lblSoGiayPhep4";
            this.lblSoGiayPhep4.StylePriority.UseFont = false;
            this.lblSoGiayPhep4.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoGiayPhep4.Weight = 0.6434772201593415;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.Text = "5";
            this.xrTableCell284.Weight = 0.0540044327289147;
            // 
            // lblPhanLoaiGiayPhepNhapKhau5
            // 
            this.lblPhanLoaiGiayPhepNhapKhau5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiGiayPhepNhapKhau5.Name = "lblPhanLoaiGiayPhepNhapKhau5";
            this.lblPhanLoaiGiayPhepNhapKhau5.StylePriority.UseFont = false;
            this.lblPhanLoaiGiayPhepNhapKhau5.Text = "XXXE";
            this.lblPhanLoaiGiayPhepNhapKhau5.Weight = 0.1620128542845577;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.Text = "-";
            this.xrTableCell274.Weight = 0.0360030085352967;
            // 
            // lblSoGiayPhep5
            // 
            this.lblSoGiayPhep5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoGiayPhep5.Name = "lblSoGiayPhep5";
            this.lblSoGiayPhep5.StylePriority.UseFont = false;
            this.lblSoGiayPhep5.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoGiayPhep5.Weight = 1.5690492194699506;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell288,
            this.xrTableCell289,
            this.xrTableCell295,
            this.lblMaPhanLoaiKhaiTriGia});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.StylePriority.UseBorders = false;
            this.xrTableRow48.Weight = 0.44000074332990968;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.Weight = 0.079748136332495362;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.Text = "Mã phân loại khai trị giá";
            this.xrTableCell289.Weight = 0.54220157389394164;
            // 
            // xrTableCell295
            // 
            this.xrTableCell295.Name = "xrTableCell295";
            this.xrTableCell295.Weight = 0.018001478212771427;
            // 
            // lblMaPhanLoaiKhaiTriGia
            // 
            this.lblMaPhanLoaiKhaiTriGia.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiKhaiTriGia.Name = "lblMaPhanLoaiKhaiTriGia";
            this.lblMaPhanLoaiKhaiTriGia.StylePriority.UseFont = false;
            this.lblMaPhanLoaiKhaiTriGia.Text = "X";
            this.lblMaPhanLoaiKhaiTriGia.Weight = 2.22587740002255;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell290,
            this.xrTableCell291,
            this.xrTableCell292,
            this.lblSoTiepNhanKhaiTriGiaTongHop,
            this.xrTableCell320,
            this.lblPhanLoaiCongThucChuan,
            this.xrTableCell314,
            this.lblMaPhanLoaiDieuChinhTriGia,
            this.xrTableCell319,
            this.lblPhuongPhapDieuChinhTriGia,
            this.lblMaTienTe,
            this.xrTableCell318,
            this.lblGiaCoSo});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 0.44000074332990979;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.Weight = 0.1260100961730819;
            // 
            // xrTableCell291
            // 
            this.xrTableCell291.Name = "xrTableCell291";
            this.xrTableCell291.Text = "Khai trị giá tổng hợp";
            this.xrTableCell291.Weight = 0.49593961405335513;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.Weight = 0.018001478212771427;
            // 
            // lblSoTiepNhanKhaiTriGiaTongHop
            // 
            this.lblSoTiepNhanKhaiTriGiaTongHop.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTiepNhanKhaiTriGiaTongHop.Name = "lblSoTiepNhanKhaiTriGiaTongHop";
            this.lblSoTiepNhanKhaiTriGiaTongHop.StylePriority.UseFont = false;
            this.lblSoTiepNhanKhaiTriGiaTongHop.Text = "XXXXXXXXE";
            this.lblSoTiepNhanKhaiTriGiaTongHop.Weight = 0.3347892703181487;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.Weight = 0.018001440412620542;
            // 
            // lblPhanLoaiCongThucChuan
            // 
            this.lblPhanLoaiCongThucChuan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiCongThucChuan.Name = "lblPhanLoaiCongThucChuan";
            this.lblPhanLoaiCongThucChuan.StylePriority.UseFont = false;
            this.lblPhanLoaiCongThucChuan.Text = "X";
            this.lblPhanLoaiCongThucChuan.Weight = 0.052017170232964732;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.Text = "-";
            this.xrTableCell314.Weight = 0.036002880825240946;
            // 
            // lblMaPhanLoaiDieuChinhTriGia
            // 
            this.lblMaPhanLoaiDieuChinhTriGia.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiDieuChinhTriGia.Name = "lblMaPhanLoaiDieuChinhTriGia";
            this.lblMaPhanLoaiDieuChinhTriGia.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinhTriGia.Text = "XE";
            this.lblMaPhanLoaiDieuChinhTriGia.Weight = 0.082524921215223923;
            // 
            // xrTableCell319
            // 
            this.xrTableCell319.Name = "xrTableCell319";
            this.xrTableCell319.Text = "-";
            this.xrTableCell319.Weight = 0.036002871473242104;
            // 
            // lblPhuongPhapDieuChinhTriGia
            // 
            this.lblPhuongPhapDieuChinhTriGia.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhuongPhapDieuChinhTriGia.Name = "lblPhuongPhapDieuChinhTriGia";
            this.lblPhuongPhapDieuChinhTriGia.StylePriority.UseFont = false;
            this.lblPhuongPhapDieuChinhTriGia.Text = "XXXXXXXXX1XXXXXXXXX2XE";
            this.lblPhuongPhapDieuChinhTriGia.Weight = 0.852086644215339;
            // 
            // lblMaTienTe
            // 
            this.lblMaTienTe.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaTienTe.Name = "lblMaTienTe";
            this.lblMaTienTe.StylePriority.UseFont = false;
            this.lblMaTienTe.Text = "XXE";
            this.lblMaTienTe.Weight = 0.15240769755953174;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.Text = "-";
            this.xrTableCell318.Weight = 0.036002647347118444;
            // 
            // lblGiaCoSo
            // 
            this.lblGiaCoSo.Name = "lblGiaCoSo";
            this.lblGiaCoSo.Text = "12.345.678.901.234.567.890";
            this.lblGiaCoSo.Weight = 0.62604185642311949;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell294,
            this.xrTableCell297,
            this.xrTableCell298,
            this.xrTableCell299});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 0.44000074332990957;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.Weight = 0.12601009622129855;
            // 
            // xrTableCell297
            // 
            this.xrTableCell297.Name = "xrTableCell297";
            this.xrTableCell297.Text = "Các khoản điều chỉnh";
            this.xrTableCell297.Weight = 0.49593961400513842;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.Weight = 0.018001478212771427;
            // 
            // xrTableCell299
            // 
            this.xrTableCell299.Name = "xrTableCell299";
            this.xrTableCell299.Weight = 2.22587740002255;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell300,
            this.xrTableCell301,
            this.xrTableCell302,
            this.lblMaPhanLoaiPhiVanChuyen,
            this.xrTableCell321,
            this.lblMaTienTePhiVanChuyen,
            this.xrTableCell324,
            this.lblPhiVanChuyen,
            this.xrTableCell303});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.Weight = 0.44000074332990979;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.Weight = 0.1696825357324423;
            // 
            // xrTableCell301
            // 
            this.xrTableCell301.Name = "xrTableCell301";
            this.xrTableCell301.Text = "Phí vận chuyển";
            this.xrTableCell301.Weight = 0.45226717449399473;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.Weight = 0.018001478212771427;
            // 
            // lblMaPhanLoaiPhiVanChuyen
            // 
            this.lblMaPhanLoaiPhiVanChuyen.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiPhiVanChuyen.Name = "lblMaPhanLoaiPhiVanChuyen";
            this.lblMaPhanLoaiPhiVanChuyen.StylePriority.UseFont = false;
            this.lblMaPhanLoaiPhiVanChuyen.Text = "X";
            this.lblMaPhanLoaiPhiVanChuyen.Weight = 0.0691715295190029;
            // 
            // xrTableCell321
            // 
            this.xrTableCell321.Name = "xrTableCell321";
            this.xrTableCell321.Text = "-";
            this.xrTableCell321.Weight = 0.0425954564671851;
            // 
            // lblMaTienTePhiVanChuyen
            // 
            this.lblMaTienTePhiVanChuyen.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaTienTePhiVanChuyen.Name = "lblMaTienTePhiVanChuyen";
            this.lblMaTienTePhiVanChuyen.StylePriority.UseFont = false;
            this.lblMaTienTePhiVanChuyen.Text = "XXE";
            this.lblMaTienTePhiVanChuyen.Weight = 0.13195006101881734;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.Text = "-";
            this.xrTableCell324.Weight = 0.039803231693764429;
            // 
            // lblPhiVanChuyen
            // 
            this.lblPhiVanChuyen.Name = "lblPhiVanChuyen";
            this.lblPhiVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblPhiVanChuyen.StylePriority.UsePadding = false;
            this.lblPhiVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblPhiVanChuyen.Text = "123.456.789.012.345.678";
            this.lblPhiVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblPhiVanChuyen.Weight = 0.49369003507942982;
            // 
            // xrTableCell303
            // 
            this.xrTableCell303.Name = "xrTableCell303";
            this.xrTableCell303.Weight = 1.44866708624435;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell304,
            this.xrTableCell305,
            this.xrTableCell306,
            this.lblMaPhanLoaiBaoHiem,
            this.xrTableCell327,
            this.lblMaTienTeCuaTienBaoHiem,
            this.xrTableCell325,
            this.lblPhiBaoHiem,
            this.xrTableCell329,
            this.lblSoDangKyBaoHiemTongHop});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 0.44000074332990979;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.Weight = 0.1696825357324423;
            // 
            // xrTableCell305
            // 
            this.xrTableCell305.Name = "xrTableCell305";
            this.xrTableCell305.Text = "Phí bảo hiểm";
            this.xrTableCell305.Weight = 0.45226717449399473;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.Weight = 0.018001478212771427;
            // 
            // lblMaPhanLoaiBaoHiem
            // 
            this.lblMaPhanLoaiBaoHiem.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiBaoHiem.Name = "lblMaPhanLoaiBaoHiem";
            this.lblMaPhanLoaiBaoHiem.StylePriority.UseFont = false;
            this.lblMaPhanLoaiBaoHiem.Text = "X";
            this.lblMaPhanLoaiBaoHiem.Weight = 0.069171487728998962;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.Text = "-";
            this.xrTableCell327.Weight = 0.042595304805123468;
            // 
            // lblMaTienTeCuaTienBaoHiem
            // 
            this.lblMaTienTeCuaTienBaoHiem.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaTienTeCuaTienBaoHiem.Name = "lblMaTienTeCuaTienBaoHiem";
            this.lblMaTienTeCuaTienBaoHiem.StylePriority.UseFont = false;
            this.lblMaTienTeCuaTienBaoHiem.Text = "XXE";
            this.lblMaTienTeCuaTienBaoHiem.Weight = 0.13195021742341132;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.Text = "-";
            this.xrTableCell325.Weight = 0.039803240097256762;
            // 
            // lblPhiBaoHiem
            // 
            this.lblPhiBaoHiem.Name = "lblPhiBaoHiem";
            this.lblPhiBaoHiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblPhiBaoHiem.StylePriority.UsePadding = false;
            this.lblPhiBaoHiem.StylePriority.UseTextAlignment = false;
            this.lblPhiBaoHiem.Text = "1.234.567.890.123.456";
            this.lblPhiBaoHiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblPhiBaoHiem.Weight = 0.49369001696341441;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.StylePriority.UseTextAlignment = false;
            this.xrTableCell329.Text = "-";
            this.xrTableCell329.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell329.Weight = 0.054004315319364421;
            // 
            // lblSoDangKyBaoHiemTongHop
            // 
            this.lblSoDangKyBaoHiemTongHop.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDangKyBaoHiemTongHop.Name = "lblSoDangKyBaoHiemTongHop";
            this.lblSoDangKyBaoHiemTongHop.StylePriority.UseFont = false;
            this.lblSoDangKyBaoHiemTongHop.Text = "XXXXXE";
            this.lblSoDangKyBaoHiemTongHop.Weight = 1.3946628176849802;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell308,
            this.xrTableCell309,
            this.xrTableCell310,
            this.xrTableCell333,
            this.xrTableCell332,
            this.xrTableCell334,
            this.xrTableCell335,
            this.xrTableCell348,
            this.xrTableCell311});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.StylePriority.UseBorders = false;
            this.xrTableRow53.Weight = 0.44000074332990979;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.Weight = 0.20568538411967721;
            // 
            // xrTableCell309
            // 
            this.xrTableCell309.Name = "xrTableCell309";
            this.xrTableCell309.StylePriority.UseTextAlignment = false;
            this.xrTableCell309.Text = "Mã tên";
            this.xrTableCell309.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell309.Weight = 0.1423424234415383;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.Weight = 0.053253997245599163;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.StylePriority.UseTextAlignment = false;
            this.xrTableCell333.Text = "Mã phân loại";
            this.xrTableCell333.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell333.Weight = 0.23866931116833187;
            // 
            // xrTableCell332
            // 
            this.xrTableCell332.Name = "xrTableCell332";
            this.xrTableCell332.Weight = 0.24371713699710043;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.StylePriority.UseTextAlignment = false;
            this.xrTableCell334.Text = "Trị giá khoản điều chỉnh";
            this.xrTableCell334.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell334.Weight = 0.63942227971850341;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.Weight = 0.057583284292200831;
            // 
            // xrTableCell348
            // 
            this.xrTableCell348.Name = "xrTableCell348";
            this.xrTableCell348.StylePriority.UseTextAlignment = false;
            this.xrTableCell348.Text = "Tổng hệ số phân bổ";
            this.xrTableCell348.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell348.Weight = 0.60966993536471326;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.Weight = 0.6754848361140936;
            // 
            // xrTableRow54
            // 
            this.xrTableRow54.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell336,
            this.xrTableCell344,
            this.lblMaTenKhoanDieuChinh1,
            this.xrTableCell338,
            this.lblMaPhanLoaiDieuChinh1,
            this.xrTableCell339,
            this.lblMaDongTienDieuChinhTriGia1,
            this.xrTableCell346,
            this.lblTriGiaKhoanDieuChinh1,
            this.xrTableCell342,
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1,
            this.xrTableCell343});
            this.xrTableRow54.Name = "xrTableRow54";
            this.xrTableRow54.Weight = 0.44000070518287715;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.Weight = 0.16968250821339317;
            // 
            // xrTableCell344
            // 
            this.xrTableCell344.Name = "xrTableCell344";
            this.xrTableCell344.Text = "1";
            this.xrTableCell344.Weight = 0.036002890076622891;
            // 
            // lblMaTenKhoanDieuChinh1
            // 
            this.lblMaTenKhoanDieuChinh1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaTenKhoanDieuChinh1.Name = "lblMaTenKhoanDieuChinh1";
            this.lblMaTenKhoanDieuChinh1.StylePriority.UseFont = false;
            this.lblMaTenKhoanDieuChinh1.StylePriority.UseTextAlignment = false;
            this.lblMaTenKhoanDieuChinh1.Text = "X";
            this.lblMaTenKhoanDieuChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaTenKhoanDieuChinh1.Weight = 0.14234254661127682;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.Text = "-";
            this.xrTableCell338.Weight = 0.053253832437506358;
            // 
            // lblMaPhanLoaiDieuChinh1
            // 
            this.lblMaPhanLoaiDieuChinh1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiDieuChinh1.Name = "lblMaPhanLoaiDieuChinh1";
            this.lblMaPhanLoaiDieuChinh1.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinh1.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiDieuChinh1.Text = "XXE";
            this.lblMaPhanLoaiDieuChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaPhanLoaiDieuChinh1.Weight = 0.22066803994580714;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.Text = "-";
            this.xrTableCell339.Weight = 0.054004202013804965;
            // 
            // lblMaDongTienDieuChinhTriGia1
            // 
            this.lblMaDongTienDieuChinhTriGia1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDongTienDieuChinhTriGia1.Name = "lblMaDongTienDieuChinhTriGia1";
            this.lblMaDongTienDieuChinhTriGia1.StylePriority.UseFont = false;
            this.lblMaDongTienDieuChinhTriGia1.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienDieuChinhTriGia1.Text = "XXE";
            this.lblMaDongTienDieuChinhTriGia1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaDongTienDieuChinhTriGia1.Weight = 0.16666305951810631;
            // 
            // xrTableCell346
            // 
            this.xrTableCell346.Name = "xrTableCell346";
            this.xrTableCell346.Text = "-";
            this.xrTableCell346.Weight = 0.041051055519655016;
            // 
            // lblTriGiaKhoanDieuChinh1
            // 
            this.lblTriGiaKhoanDieuChinh1.Name = "lblTriGiaKhoanDieuChinh1";
            this.lblTriGiaKhoanDieuChinh1.StylePriority.UseTextAlignment = false;
            this.lblTriGiaKhoanDieuChinh1.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaKhoanDieuChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTriGiaKhoanDieuChinh1.Weight = 0.6394223983545777;
            // 
            // xrTableCell342
            // 
            this.xrTableCell342.Name = "xrTableCell342";
            this.xrTableCell342.Weight = 0.057583284292200831;
            // 
            // lblTongHeSoPhanBoTriGiaKhoanDieuChinh1
            // 
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Name = "lblTongHeSoPhanBoTriGiaKhoanDieuChinh1";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh1.Weight = 0.60966993536471326;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.Weight = 0.6754848361140936;
            // 
            // xrTableRow55
            // 
            this.xrTableRow55.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell349,
            this.xrTableCell350,
            this.lblMaTenKhoanDieuChinh2,
            this.xrTableCell352,
            this.lblMaPhanLoaiDieuChinh2,
            this.xrTableCell354,
            this.lblMaDongTienDieuChinhTriGia2,
            this.xrTableCell356,
            this.lblTriGiaKhoanDieuChinh2,
            this.xrTableCell358,
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2,
            this.xrTableCell360});
            this.xrTableRow55.Name = "xrTableRow55";
            this.xrTableRow55.Weight = 0.44000062888881253;
            // 
            // xrTableCell349
            // 
            this.xrTableCell349.Name = "xrTableCell349";
            this.xrTableCell349.Weight = 0.16968250821339317;
            // 
            // xrTableCell350
            // 
            this.xrTableCell350.Name = "xrTableCell350";
            this.xrTableCell350.Text = "2";
            this.xrTableCell350.Weight = 0.036002890076622891;
            // 
            // lblMaTenKhoanDieuChinh2
            // 
            this.lblMaTenKhoanDieuChinh2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaTenKhoanDieuChinh2.Name = "lblMaTenKhoanDieuChinh2";
            this.lblMaTenKhoanDieuChinh2.StylePriority.UseFont = false;
            this.lblMaTenKhoanDieuChinh2.StylePriority.UseTextAlignment = false;
            this.lblMaTenKhoanDieuChinh2.Text = "X";
            this.lblMaTenKhoanDieuChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaTenKhoanDieuChinh2.Weight = 0.14234254661127682;
            // 
            // xrTableCell352
            // 
            this.xrTableCell352.Name = "xrTableCell352";
            this.xrTableCell352.Text = "-";
            this.xrTableCell352.Weight = 0.053253832437506358;
            // 
            // lblMaPhanLoaiDieuChinh2
            // 
            this.lblMaPhanLoaiDieuChinh2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiDieuChinh2.Name = "lblMaPhanLoaiDieuChinh2";
            this.lblMaPhanLoaiDieuChinh2.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinh2.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiDieuChinh2.Text = "XXE";
            this.lblMaPhanLoaiDieuChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaPhanLoaiDieuChinh2.Weight = 0.22066803994580714;
            // 
            // xrTableCell354
            // 
            this.xrTableCell354.Name = "xrTableCell354";
            this.xrTableCell354.Text = "-";
            this.xrTableCell354.Weight = 0.054004202013804965;
            // 
            // lblMaDongTienDieuChinhTriGia2
            // 
            this.lblMaDongTienDieuChinhTriGia2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDongTienDieuChinhTriGia2.Name = "lblMaDongTienDieuChinhTriGia2";
            this.lblMaDongTienDieuChinhTriGia2.StylePriority.UseFont = false;
            this.lblMaDongTienDieuChinhTriGia2.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienDieuChinhTriGia2.Text = "XXE";
            this.lblMaDongTienDieuChinhTriGia2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaDongTienDieuChinhTriGia2.Weight = 0.16666305951810631;
            // 
            // xrTableCell356
            // 
            this.xrTableCell356.Name = "xrTableCell356";
            this.xrTableCell356.Text = "-";
            this.xrTableCell356.Weight = 0.041051055519655016;
            // 
            // lblTriGiaKhoanDieuChinh2
            // 
            this.lblTriGiaKhoanDieuChinh2.Name = "lblTriGiaKhoanDieuChinh2";
            this.lblTriGiaKhoanDieuChinh2.StylePriority.UseTextAlignment = false;
            this.lblTriGiaKhoanDieuChinh2.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaKhoanDieuChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTriGiaKhoanDieuChinh2.Weight = 0.6394223983545777;
            // 
            // xrTableCell358
            // 
            this.xrTableCell358.Name = "xrTableCell358";
            this.xrTableCell358.Weight = 0.057583284292200831;
            // 
            // lblTongHeSoPhanBoTriGiaKhoanDieuChinh2
            // 
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Name = "lblTongHeSoPhanBoTriGiaKhoanDieuChinh2";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh2.Weight = 0.60966993536471326;
            // 
            // xrTableCell360
            // 
            this.xrTableCell360.Name = "xrTableCell360";
            this.xrTableCell360.Weight = 0.6754848361140936;
            // 
            // xrTableRow56
            // 
            this.xrTableRow56.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell361,
            this.xrTableCell362,
            this.lblMaTenKhoanDieuChinh3,
            this.xrTableCell364,
            this.lblMaPhanLoaiDieuChinh3,
            this.xrTableCell366,
            this.lblMaDongTienDieuChinhTriGia3,
            this.xrTableCell368,
            this.lblTriGiaKhoanDieuChinh3,
            this.xrTableCell370,
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3,
            this.xrTableCell372});
            this.xrTableRow56.Name = "xrTableRow56";
            this.xrTableRow56.Weight = 0.44000074332990979;
            // 
            // xrTableCell361
            // 
            this.xrTableCell361.Name = "xrTableCell361";
            this.xrTableCell361.Weight = 0.16968250821339317;
            // 
            // xrTableCell362
            // 
            this.xrTableCell362.Name = "xrTableCell362";
            this.xrTableCell362.Text = "3";
            this.xrTableCell362.Weight = 0.036002890076622891;
            // 
            // lblMaTenKhoanDieuChinh3
            // 
            this.lblMaTenKhoanDieuChinh3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaTenKhoanDieuChinh3.Name = "lblMaTenKhoanDieuChinh3";
            this.lblMaTenKhoanDieuChinh3.StylePriority.UseFont = false;
            this.lblMaTenKhoanDieuChinh3.StylePriority.UseTextAlignment = false;
            this.lblMaTenKhoanDieuChinh3.Text = "X";
            this.lblMaTenKhoanDieuChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaTenKhoanDieuChinh3.Weight = 0.14234254661127682;
            // 
            // xrTableCell364
            // 
            this.xrTableCell364.Name = "xrTableCell364";
            this.xrTableCell364.Text = "-";
            this.xrTableCell364.Weight = 0.053253832437506358;
            // 
            // lblMaPhanLoaiDieuChinh3
            // 
            this.lblMaPhanLoaiDieuChinh3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiDieuChinh3.Name = "lblMaPhanLoaiDieuChinh3";
            this.lblMaPhanLoaiDieuChinh3.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinh3.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiDieuChinh3.Text = "XXE";
            this.lblMaPhanLoaiDieuChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaPhanLoaiDieuChinh3.Weight = 0.22066803994580714;
            // 
            // xrTableCell366
            // 
            this.xrTableCell366.Name = "xrTableCell366";
            this.xrTableCell366.Text = "-";
            this.xrTableCell366.Weight = 0.054004202013804965;
            // 
            // lblMaDongTienDieuChinhTriGia3
            // 
            this.lblMaDongTienDieuChinhTriGia3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDongTienDieuChinhTriGia3.Name = "lblMaDongTienDieuChinhTriGia3";
            this.lblMaDongTienDieuChinhTriGia3.StylePriority.UseFont = false;
            this.lblMaDongTienDieuChinhTriGia3.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienDieuChinhTriGia3.Text = "XXE";
            this.lblMaDongTienDieuChinhTriGia3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaDongTienDieuChinhTriGia3.Weight = 0.16666305951810631;
            // 
            // xrTableCell368
            // 
            this.xrTableCell368.Name = "xrTableCell368";
            this.xrTableCell368.Text = "-";
            this.xrTableCell368.Weight = 0.041051055519655016;
            // 
            // lblTriGiaKhoanDieuChinh3
            // 
            this.lblTriGiaKhoanDieuChinh3.Name = "lblTriGiaKhoanDieuChinh3";
            this.lblTriGiaKhoanDieuChinh3.StylePriority.UseTextAlignment = false;
            this.lblTriGiaKhoanDieuChinh3.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaKhoanDieuChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTriGiaKhoanDieuChinh3.Weight = 0.6394223983545777;
            // 
            // xrTableCell370
            // 
            this.xrTableCell370.Name = "xrTableCell370";
            this.xrTableCell370.Weight = 0.057583284292200831;
            // 
            // lblTongHeSoPhanBoTriGiaKhoanDieuChinh3
            // 
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Name = "lblTongHeSoPhanBoTriGiaKhoanDieuChinh3";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh3.Weight = 0.60966993536471326;
            // 
            // xrTableCell372
            // 
            this.xrTableCell372.Name = "xrTableCell372";
            this.xrTableCell372.Weight = 0.6754848361140936;
            // 
            // xrTableRow57
            // 
            this.xrTableRow57.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell373,
            this.xrTableCell374,
            this.lblMaTenKhoanDieuChinh4,
            this.xrTableCell376,
            this.lblMaPhanLoaiDieuChinh4,
            this.xrTableCell378,
            this.lblMaDongTienDieuChinhTriGia4,
            this.xrTableCell380,
            this.lblTriGiaKhoanDieuChinh4,
            this.xrTableCell382,
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4,
            this.xrTableCell384});
            this.xrTableRow57.Name = "xrTableRow57";
            this.xrTableRow57.Weight = 0.44000074332990957;
            // 
            // xrTableCell373
            // 
            this.xrTableCell373.Name = "xrTableCell373";
            this.xrTableCell373.Weight = 0.16968250821339317;
            // 
            // xrTableCell374
            // 
            this.xrTableCell374.Name = "xrTableCell374";
            this.xrTableCell374.Text = "4";
            this.xrTableCell374.Weight = 0.036002890076622891;
            // 
            // lblMaTenKhoanDieuChinh4
            // 
            this.lblMaTenKhoanDieuChinh4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaTenKhoanDieuChinh4.Name = "lblMaTenKhoanDieuChinh4";
            this.lblMaTenKhoanDieuChinh4.StylePriority.UseFont = false;
            this.lblMaTenKhoanDieuChinh4.StylePriority.UseTextAlignment = false;
            this.lblMaTenKhoanDieuChinh4.Text = "X";
            this.lblMaTenKhoanDieuChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaTenKhoanDieuChinh4.Weight = 0.14234254661127682;
            // 
            // xrTableCell376
            // 
            this.xrTableCell376.Name = "xrTableCell376";
            this.xrTableCell376.Text = "-";
            this.xrTableCell376.Weight = 0.053253832437506358;
            // 
            // lblMaPhanLoaiDieuChinh4
            // 
            this.lblMaPhanLoaiDieuChinh4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiDieuChinh4.Name = "lblMaPhanLoaiDieuChinh4";
            this.lblMaPhanLoaiDieuChinh4.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinh4.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiDieuChinh4.Text = "XXE";
            this.lblMaPhanLoaiDieuChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaPhanLoaiDieuChinh4.Weight = 0.22066803994580714;
            // 
            // xrTableCell378
            // 
            this.xrTableCell378.Name = "xrTableCell378";
            this.xrTableCell378.Text = "-";
            this.xrTableCell378.Weight = 0.054004202013804965;
            // 
            // lblMaDongTienDieuChinhTriGia4
            // 
            this.lblMaDongTienDieuChinhTriGia4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDongTienDieuChinhTriGia4.Name = "lblMaDongTienDieuChinhTriGia4";
            this.lblMaDongTienDieuChinhTriGia4.StylePriority.UseFont = false;
            this.lblMaDongTienDieuChinhTriGia4.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienDieuChinhTriGia4.Text = "XXE";
            this.lblMaDongTienDieuChinhTriGia4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaDongTienDieuChinhTriGia4.Weight = 0.16666305951810631;
            // 
            // xrTableCell380
            // 
            this.xrTableCell380.Name = "xrTableCell380";
            this.xrTableCell380.Text = "-";
            this.xrTableCell380.Weight = 0.041051055519655016;
            // 
            // lblTriGiaKhoanDieuChinh4
            // 
            this.lblTriGiaKhoanDieuChinh4.Name = "lblTriGiaKhoanDieuChinh4";
            this.lblTriGiaKhoanDieuChinh4.StylePriority.UseTextAlignment = false;
            this.lblTriGiaKhoanDieuChinh4.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaKhoanDieuChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTriGiaKhoanDieuChinh4.Weight = 0.6394223983545777;
            // 
            // xrTableCell382
            // 
            this.xrTableCell382.Name = "xrTableCell382";
            this.xrTableCell382.Weight = 0.057583284292200831;
            // 
            // lblTongHeSoPhanBoTriGiaKhoanDieuChinh4
            // 
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Name = "lblTongHeSoPhanBoTriGiaKhoanDieuChinh4";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh4.Weight = 0.60966993536471326;
            // 
            // xrTableCell384
            // 
            this.xrTableCell384.Name = "xrTableCell384";
            this.xrTableCell384.Weight = 0.6754848361140936;
            // 
            // xrTableRow58
            // 
            this.xrTableRow58.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell385,
            this.xrTableCell386,
            this.lblMaTenKhoanDieuChinh5,
            this.xrTableCell388,
            this.lblMaPhanLoaiDieuChinh5,
            this.xrTableCell390,
            this.lblMaDongTienDieuChinhTriGia5,
            this.xrTableCell392,
            this.lblTriGiaKhoanDieuChinh5,
            this.xrTableCell394,
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5,
            this.xrTableCell396});
            this.xrTableRow58.Name = "xrTableRow58";
            this.xrTableRow58.Weight = 0.44000074332991;
            // 
            // xrTableCell385
            // 
            this.xrTableCell385.Name = "xrTableCell385";
            this.xrTableCell385.Weight = 0.16968250821339317;
            // 
            // xrTableCell386
            // 
            this.xrTableCell386.Name = "xrTableCell386";
            this.xrTableCell386.Text = "5";
            this.xrTableCell386.Weight = 0.036002890076622891;
            // 
            // lblMaTenKhoanDieuChinh5
            // 
            this.lblMaTenKhoanDieuChinh5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaTenKhoanDieuChinh5.Name = "lblMaTenKhoanDieuChinh5";
            this.lblMaTenKhoanDieuChinh5.StylePriority.UseFont = false;
            this.lblMaTenKhoanDieuChinh5.StylePriority.UseTextAlignment = false;
            this.lblMaTenKhoanDieuChinh5.Text = "X";
            this.lblMaTenKhoanDieuChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaTenKhoanDieuChinh5.Weight = 0.14234254661127682;
            // 
            // xrTableCell388
            // 
            this.xrTableCell388.Name = "xrTableCell388";
            this.xrTableCell388.Text = "-";
            this.xrTableCell388.Weight = 0.053253887373537291;
            // 
            // lblMaPhanLoaiDieuChinh5
            // 
            this.lblMaPhanLoaiDieuChinh5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiDieuChinh5.Name = "lblMaPhanLoaiDieuChinh5";
            this.lblMaPhanLoaiDieuChinh5.StylePriority.UseFont = false;
            this.lblMaPhanLoaiDieuChinh5.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiDieuChinh5.Text = "XXE";
            this.lblMaPhanLoaiDieuChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaPhanLoaiDieuChinh5.Weight = 0.22066798500977622;
            // 
            // xrTableCell390
            // 
            this.xrTableCell390.Name = "xrTableCell390";
            this.xrTableCell390.Text = "-";
            this.xrTableCell390.Weight = 0.054004202013804965;
            // 
            // lblMaDongTienDieuChinhTriGia5
            // 
            this.lblMaDongTienDieuChinhTriGia5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDongTienDieuChinhTriGia5.Name = "lblMaDongTienDieuChinhTriGia5";
            this.lblMaDongTienDieuChinhTriGia5.StylePriority.UseFont = false;
            this.lblMaDongTienDieuChinhTriGia5.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienDieuChinhTriGia5.Text = "XXE";
            this.lblMaDongTienDieuChinhTriGia5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaDongTienDieuChinhTriGia5.Weight = 0.16666305951810631;
            // 
            // xrTableCell392
            // 
            this.xrTableCell392.Name = "xrTableCell392";
            this.xrTableCell392.Text = "-";
            this.xrTableCell392.Weight = 0.041051055519655016;
            // 
            // lblTriGiaKhoanDieuChinh5
            // 
            this.lblTriGiaKhoanDieuChinh5.Name = "lblTriGiaKhoanDieuChinh5";
            this.lblTriGiaKhoanDieuChinh5.StylePriority.UseTextAlignment = false;
            this.lblTriGiaKhoanDieuChinh5.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaKhoanDieuChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTriGiaKhoanDieuChinh5.Weight = 0.6394223983545777;
            // 
            // xrTableCell394
            // 
            this.xrTableCell394.Name = "xrTableCell394";
            this.xrTableCell394.Weight = 0.057583284292200831;
            // 
            // lblTongHeSoPhanBoTriGiaKhoanDieuChinh5
            // 
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Name = "lblTongHeSoPhanBoTriGiaKhoanDieuChinh5";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.StylePriority.UseTextAlignment = false;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Text = "12.345.678.901.234.567.890";
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongHeSoPhanBoTriGiaKhoanDieuChinh5.Weight = 0.60966993536471326;
            // 
            // xrTableCell396
            // 
            this.xrTableCell396.Name = "xrTableCell396";
            this.xrTableCell396.Weight = 0.6754848361140936;
            // 
            // xrTableRow59
            // 
            this.xrTableRow59.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell397,
            this.xrTableCell400});
            this.xrTableRow59.Name = "xrTableRow59";
            this.xrTableRow59.Weight = 0.44000074332990979;
            // 
            // xrTableCell397
            // 
            this.xrTableCell397.Name = "xrTableCell397";
            this.xrTableCell397.Weight = 0.079748081396464443;
            // 
            // xrTableCell400
            // 
            this.xrTableCell400.Name = "xrTableCell400";
            this.xrTableCell400.Text = "Chi tiết khai trị giá";
            this.xrTableCell400.Weight = 2.786080507065293;
            // 
            // xrTableRow60
            // 
            this.xrTableRow60.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell398,
            this.lblChiTietKhaiTriGia});
            this.xrTableRow60.Name = "xrTableRow60";
            this.xrTableRow60.Weight = 1.4200022134040591;
            // 
            // xrTableCell398
            // 
            this.xrTableCell398.Name = "xrTableCell398";
            this.xrTableCell398.Weight = 0.0797487406288357;
            // 
            // lblChiTietKhaiTriGia
            // 
            this.lblChiTietKhaiTriGia.Font = new System.Drawing.Font("Arial", 7.5F);
            this.lblChiTietKhaiTriGia.Name = "lblChiTietKhaiTriGia";
            this.lblChiTietKhaiTriGia.StylePriority.UseFont = false;
            this.lblChiTietKhaiTriGia.Text = resources.GetString("lblChiTietKhaiTriGia.Text");
            this.lblChiTietKhaiTriGia.Weight = 2.7860798478329216;
            // 
            // xrTableRow61
            // 
            this.xrTableRow61.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow61.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell401,
            this.xrTableCell404,
            this.xrTableCell405,
            this.xrTableCell403,
            this.xrTableCell406,
            this.xrTableCell422,
            this.xrTableCell402});
            this.xrTableRow61.Name = "xrTableRow61";
            this.xrTableRow61.StylePriority.UseBorders = false;
            this.xrTableRow61.Weight = 0.44000070889321596;
            // 
            // xrTableCell401
            // 
            this.xrTableCell401.Name = "xrTableCell401";
            this.xrTableCell401.Weight = 0.16968257555416144;
            // 
            // xrTableCell404
            // 
            this.xrTableCell404.Name = "xrTableCell404";
            this.xrTableCell404.StylePriority.UseTextAlignment = false;
            this.xrTableCell404.Text = "Tên sắc thuế";
            this.xrTableCell404.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell404.Weight = 0.539440112807337;
            // 
            // xrTableCell405
            // 
            this.xrTableCell405.Name = "xrTableCell405";
            this.xrTableCell405.Weight = 0.042595311163201907;
            // 
            // xrTableCell403
            // 
            this.xrTableCell403.Name = "xrTableCell403";
            this.xrTableCell403.StylePriority.UseTextAlignment = false;
            this.xrTableCell403.Text = "Tổng tiền thuế";
            this.xrTableCell403.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell403.Weight = 0.37468463677400321;
            // 
            // xrTableCell406
            // 
            this.xrTableCell406.Name = "xrTableCell406";
            this.xrTableCell406.Weight = 0.12000951943155536;
            // 
            // xrTableCell422
            // 
            this.xrTableCell422.Name = "xrTableCell422";
            this.xrTableCell422.StylePriority.UseTextAlignment = false;
            this.xrTableCell422.Text = "Số dòng tổng";
            this.xrTableCell422.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell422.Weight = 0.33426166297496956;
            // 
            // xrTableCell402
            // 
            this.xrTableCell402.Name = "xrTableCell402";
            this.xrTableCell402.Weight = 1.2851547697565287;
            // 
            // xrTableRow62
            // 
            this.xrTableRow62.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell407,
            this.xrTableCell408,
            this.lblMaSacThue1,
            this.lblTenSacThue1,
            this.xrTableCell411,
            this.lblTongTienThue1,
            this.xrTableCell413,
            this.lblSoDongTong1,
            this.xrTableCell419,
            this.xrTableCell420,
            this.xrTableCell417,
            this.lblTongTienThuePhaiNop,
            this.xrTableCell421,
            this.xrTableCell412});
            this.xrTableRow62.Name = "xrTableRow62";
            this.xrTableRow62.Weight = 0.44000070889321663;
            // 
            // xrTableCell407
            // 
            this.xrTableCell407.Name = "xrTableCell407";
            this.xrTableCell407.Weight = 0.16968252886543894;
            // 
            // xrTableCell408
            // 
            this.xrTableCell408.Name = "xrTableCell408";
            this.xrTableCell408.Text = "1";
            this.xrTableCell408.Weight = 0.036002916685695642;
            // 
            // lblMaSacThue1
            // 
            this.lblMaSacThue1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSacThue1.Name = "lblMaSacThue1";
            this.lblMaSacThue1.StylePriority.UseFont = false;
            this.lblMaSacThue1.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue1.Text = "X";
            this.lblMaSacThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaSacThue1.Weight = 0.12434100350822452;
            // 
            // lblTenSacThue1
            // 
            this.lblTenSacThue1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenSacThue1.Name = "lblTenSacThue1";
            this.lblTenSacThue1.StylePriority.UseFont = false;
            this.lblTenSacThue1.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue1.Text = "WWWWWWWWE";
            this.lblTenSacThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTenSacThue1.Weight = 0.37909623206355697;
            // 
            // xrTableCell411
            // 
            this.xrTableCell411.Name = "xrTableCell411";
            this.xrTableCell411.Weight = 0.042595589091060024;
            // 
            // lblTongTienThue1
            // 
            this.lblTongTienThue1.Name = "lblTongTienThue1";
            this.lblTongTienThue1.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue1.Text = "12.345.678.901";
            this.lblTongTienThue1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue1.Weight = 0.37468442146110648;
            // 
            // xrTableCell413
            // 
            this.xrTableCell413.Name = "xrTableCell413";
            this.xrTableCell413.Text = "VND";
            this.xrTableCell413.Weight = 0.12000950850491057;
            // 
            // lblSoDongTong1
            // 
            this.lblSoDongTong1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDongTong1.Name = "lblSoDongTong1";
            this.lblSoDongTong1.StylePriority.UseFont = false;
            this.lblSoDongTong1.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong1.Text = "NNE";
            this.lblSoDongTong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong1.Weight = 0.33426158974643405;
            // 
            // xrTableCell419
            // 
            this.xrTableCell419.Name = "xrTableCell419";
            this.xrTableCell419.Weight = 0.056662447818540733;
            // 
            // xrTableCell420
            // 
            this.xrTableCell420.Name = "xrTableCell420";
            this.xrTableCell420.Text = "Tổng tiền thuế phải nộp";
            this.xrTableCell420.Weight = 0.46034941817171193;
            // 
            // xrTableCell417
            // 
            this.xrTableCell417.Name = "xrTableCell417";
            this.xrTableCell417.Weight = 0.043843125818321949;
            // 
            // lblTongTienThuePhaiNop
            // 
            this.lblTongTienThuePhaiNop.Name = "lblTongTienThuePhaiNop";
            this.lblTongTienThuePhaiNop.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTongTienThuePhaiNop.StylePriority.UsePadding = false;
            this.lblTongTienThuePhaiNop.StylePriority.UseTextAlignment = false;
            this.lblTongTienThuePhaiNop.Text = "12.345.678.901";
            this.lblTongTienThuePhaiNop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTongTienThuePhaiNop.Weight = 0.36633609874118855;
            // 
            // xrTableCell421
            // 
            this.xrTableCell421.Name = "xrTableCell421";
            this.xrTableCell421.Weight = 0.053905646053519995;
            // 
            // xrTableCell412
            // 
            this.xrTableCell412.Name = "xrTableCell412";
            this.xrTableCell412.Text = "VND";
            this.xrTableCell412.Weight = 0.30405806193204687;
            // 
            // xrTableRow63
            // 
            this.xrTableRow63.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell414,
            this.xrTableCell423,
            this.lblMaSacThue2,
            this.lblTenSacThue2,
            this.xrTableCell426,
            this.lblTongTienThue2,
            this.xrTableCell428,
            this.lblSoDongTong2,
            this.xrTableCell430,
            this.xrTableCell437,
            this.xrTableCell438,
            this.lblSoTienBaoLanh,
            this.xrTableCell440,
            this.xrTableCell441});
            this.xrTableRow63.Name = "xrTableRow63";
            this.xrTableRow63.Weight = 0.44000070889321563;
            // 
            // xrTableCell414
            // 
            this.xrTableCell414.Name = "xrTableCell414";
            this.xrTableCell414.Weight = 0.16968252886543894;
            // 
            // xrTableCell423
            // 
            this.xrTableCell423.Name = "xrTableCell423";
            this.xrTableCell423.Text = "2";
            this.xrTableCell423.Weight = 0.036002889217680169;
            // 
            // lblMaSacThue2
            // 
            this.lblMaSacThue2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSacThue2.Name = "lblMaSacThue2";
            this.lblMaSacThue2.StylePriority.UseFont = false;
            this.lblMaSacThue2.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue2.Text = "X";
            this.lblMaSacThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaSacThue2.Weight = 0.12434097604020905;
            // 
            // lblTenSacThue2
            // 
            this.lblTenSacThue2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenSacThue2.Name = "lblTenSacThue2";
            this.lblTenSacThue2.StylePriority.UseFont = false;
            this.lblTenSacThue2.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue2.Text = "WWWWWWWWE";
            this.lblTenSacThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTenSacThue2.Weight = 0.37909625953157244;
            // 
            // xrTableCell426
            // 
            this.xrTableCell426.Name = "xrTableCell426";
            this.xrTableCell426.Weight = 0.04259561655907549;
            // 
            // lblTongTienThue2
            // 
            this.lblTongTienThue2.Name = "lblTongTienThue2";
            this.lblTongTienThue2.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue2.Text = "12.345.678.901";
            this.lblTongTienThue2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue2.Weight = 0.37468442146110648;
            // 
            // xrTableCell428
            // 
            this.xrTableCell428.Name = "xrTableCell428";
            this.xrTableCell428.Text = "VND";
            this.xrTableCell428.Weight = 0.12000950850491057;
            // 
            // lblSoDongTong2
            // 
            this.lblSoDongTong2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDongTong2.Name = "lblSoDongTong2";
            this.lblSoDongTong2.StylePriority.UseFont = false;
            this.lblSoDongTong2.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong2.Text = "NNE";
            this.lblSoDongTong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong2.Weight = 0.33426158974643405;
            // 
            // xrTableCell430
            // 
            this.xrTableCell430.Name = "xrTableCell430";
            this.xrTableCell430.Weight = 0.056662447818540733;
            // 
            // xrTableCell437
            // 
            this.xrTableCell437.Name = "xrTableCell437";
            this.xrTableCell437.Text = "Số tiền bảo lãnh";
            this.xrTableCell437.Weight = 0.46034941817171193;
            // 
            // xrTableCell438
            // 
            this.xrTableCell438.Name = "xrTableCell438";
            this.xrTableCell438.Weight = 0.043843125818321949;
            // 
            // lblSoTienBaoLanh
            // 
            this.lblSoTienBaoLanh.Name = "lblSoTienBaoLanh";
            this.lblSoTienBaoLanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienBaoLanh.StylePriority.UsePadding = false;
            this.lblSoTienBaoLanh.StylePriority.UseTextAlignment = false;
            this.lblSoTienBaoLanh.Text = "12.345.678.901";
            this.lblSoTienBaoLanh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienBaoLanh.Weight = 0.36633609874118855;
            // 
            // xrTableCell440
            // 
            this.xrTableCell440.Name = "xrTableCell440";
            this.xrTableCell440.Weight = 0.053905646053519995;
            // 
            // xrTableCell441
            // 
            this.xrTableCell441.Name = "xrTableCell441";
            this.xrTableCell441.Text = "VND";
            this.xrTableCell441.Weight = 0.30405806193204687;
            // 
            // xrTableRow64
            // 
            this.xrTableRow64.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell442,
            this.xrTableCell443,
            this.lblMaSacThue3,
            this.lblTenSacThue3,
            this.xrTableCell446,
            this.lblTongTienThue3,
            this.xrTableCell448,
            this.lblSoDongTong3,
            this.xrTableCell450,
            this.xrTableCell451,
            this.xrTableCell452,
            this.lblMaDongTienTyGiaTinhThue1,
            this.xrTableCell454,
            this.lblTyGiaTinhThue1});
            this.xrTableRow64.Name = "xrTableRow64";
            this.xrTableRow64.Weight = 0.44000070889321607;
            // 
            // xrTableCell442
            // 
            this.xrTableCell442.Name = "xrTableCell442";
            this.xrTableCell442.Weight = 0.16968252886543894;
            // 
            // xrTableCell443
            // 
            this.xrTableCell443.Name = "xrTableCell443";
            this.xrTableCell443.Text = "3";
            this.xrTableCell443.Weight = 0.036002889217680169;
            // 
            // lblMaSacThue3
            // 
            this.lblMaSacThue3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSacThue3.Name = "lblMaSacThue3";
            this.lblMaSacThue3.StylePriority.UseFont = false;
            this.lblMaSacThue3.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue3.Text = "X";
            this.lblMaSacThue3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaSacThue3.Weight = 0.12434097604020905;
            // 
            // lblTenSacThue3
            // 
            this.lblTenSacThue3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenSacThue3.Name = "lblTenSacThue3";
            this.lblTenSacThue3.StylePriority.UseFont = false;
            this.lblTenSacThue3.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue3.Text = "WWWWWWWWE";
            this.lblTenSacThue3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTenSacThue3.Weight = 0.37909625953157244;
            // 
            // xrTableCell446
            // 
            this.xrTableCell446.Name = "xrTableCell446";
            this.xrTableCell446.Weight = 0.04259561655907549;
            // 
            // lblTongTienThue3
            // 
            this.lblTongTienThue3.Name = "lblTongTienThue3";
            this.lblTongTienThue3.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue3.Text = "12.345.678.901";
            this.lblTongTienThue3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue3.Weight = 0.37468442146110648;
            // 
            // xrTableCell448
            // 
            this.xrTableCell448.Name = "xrTableCell448";
            this.xrTableCell448.Text = "VND";
            this.xrTableCell448.Weight = 0.12000950850491057;
            // 
            // lblSoDongTong3
            // 
            this.lblSoDongTong3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDongTong3.Name = "lblSoDongTong3";
            this.lblSoDongTong3.StylePriority.UseFont = false;
            this.lblSoDongTong3.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong3.Text = "NNE";
            this.lblSoDongTong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong3.Weight = 0.33426158974643405;
            // 
            // xrTableCell450
            // 
            this.xrTableCell450.Name = "xrTableCell450";
            this.xrTableCell450.Weight = 0.056662447818540733;
            // 
            // xrTableCell451
            // 
            this.xrTableCell451.Name = "xrTableCell451";
            this.xrTableCell451.Text = "Tỷ giá tính thuế";
            this.xrTableCell451.Weight = 0.46034941817171193;
            // 
            // xrTableCell452
            // 
            this.xrTableCell452.Name = "xrTableCell452";
            this.xrTableCell452.Weight = 0.043843125818321949;
            // 
            // lblMaDongTienTyGiaTinhThue1
            // 
            this.lblMaDongTienTyGiaTinhThue1.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDongTienTyGiaTinhThue1.Name = "lblMaDongTienTyGiaTinhThue1";
            this.lblMaDongTienTyGiaTinhThue1.StylePriority.UseFont = false;
            this.lblMaDongTienTyGiaTinhThue1.Text = "XXE";
            this.lblMaDongTienTyGiaTinhThue1.Weight = 0.13182866730155843;
            // 
            // xrTableCell454
            // 
            this.xrTableCell454.Name = "xrTableCell454";
            this.xrTableCell454.Text = "-";
            this.xrTableCell454.Weight = 0.036002873233676846;
            // 
            // lblTyGiaTinhThue1
            // 
            this.lblTyGiaTinhThue1.Name = "lblTyGiaTinhThue1";
            this.lblTyGiaTinhThue1.Text = "123.456.789";
            this.lblTyGiaTinhThue1.Weight = 0.55646826619152023;
            // 
            // xrTableRow69
            // 
            this.xrTableRow69.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell498,
            this.xrTableCell499,
            this.lblMaSacThue4,
            this.lblTenSacThue4,
            this.xrTableCell502,
            this.lblTongTienThue4,
            this.xrTableCell504,
            this.lblSoDongTong4,
            this.xrTableCell506,
            this.xrTableCell507,
            this.xrTableCell508,
            this.lblMaDongTienTyGiaTinhThue2,
            this.xrTableCell510,
            this.lblTyGiaTinhThue2});
            this.xrTableRow69.Name = "xrTableRow69";
            this.xrTableRow69.Weight = 0.44000070889321563;
            // 
            // xrTableCell498
            // 
            this.xrTableCell498.Name = "xrTableCell498";
            this.xrTableCell498.Weight = 0.16968252886543894;
            // 
            // xrTableCell499
            // 
            this.xrTableCell499.Name = "xrTableCell499";
            this.xrTableCell499.Text = "4";
            this.xrTableCell499.Weight = 0.036002889217680169;
            // 
            // lblMaSacThue4
            // 
            this.lblMaSacThue4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSacThue4.Name = "lblMaSacThue4";
            this.lblMaSacThue4.StylePriority.UseFont = false;
            this.lblMaSacThue4.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue4.Text = "X";
            this.lblMaSacThue4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaSacThue4.Weight = 0.12434097604020905;
            // 
            // lblTenSacThue4
            // 
            this.lblTenSacThue4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenSacThue4.Name = "lblTenSacThue4";
            this.lblTenSacThue4.StylePriority.UseFont = false;
            this.lblTenSacThue4.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue4.Text = "WWWWWWWWE";
            this.lblTenSacThue4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTenSacThue4.Weight = 0.37909625953157244;
            // 
            // xrTableCell502
            // 
            this.xrTableCell502.Name = "xrTableCell502";
            this.xrTableCell502.Weight = 0.04259561655907549;
            // 
            // lblTongTienThue4
            // 
            this.lblTongTienThue4.Name = "lblTongTienThue4";
            this.lblTongTienThue4.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue4.Text = "12.345.678.901";
            this.lblTongTienThue4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue4.Weight = 0.37468442146110648;
            // 
            // xrTableCell504
            // 
            this.xrTableCell504.Name = "xrTableCell504";
            this.xrTableCell504.Text = "VND";
            this.xrTableCell504.Weight = 0.12000950850491057;
            // 
            // lblSoDongTong4
            // 
            this.lblSoDongTong4.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDongTong4.Name = "lblSoDongTong4";
            this.lblSoDongTong4.StylePriority.UseFont = false;
            this.lblSoDongTong4.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong4.Text = "NNE";
            this.lblSoDongTong4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong4.Weight = 0.33426158974643405;
            // 
            // xrTableCell506
            // 
            this.xrTableCell506.Name = "xrTableCell506";
            this.xrTableCell506.Weight = 0.056662447818540733;
            // 
            // xrTableCell507
            // 
            this.xrTableCell507.Name = "xrTableCell507";
            this.xrTableCell507.Weight = 0.46034941817171193;
            // 
            // xrTableCell508
            // 
            this.xrTableCell508.Name = "xrTableCell508";
            this.xrTableCell508.Weight = 0.043843125818321949;
            // 
            // lblMaDongTienTyGiaTinhThue2
            // 
            this.lblMaDongTienTyGiaTinhThue2.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDongTienTyGiaTinhThue2.Name = "lblMaDongTienTyGiaTinhThue2";
            this.lblMaDongTienTyGiaTinhThue2.StylePriority.UseFont = false;
            this.lblMaDongTienTyGiaTinhThue2.Text = "XXE";
            this.lblMaDongTienTyGiaTinhThue2.Weight = 0.13182844755744311;
            // 
            // xrTableCell510
            // 
            this.xrTableCell510.Name = "xrTableCell510";
            this.xrTableCell510.Text = "-";
            this.xrTableCell510.Weight = 0.036003092977792139;
            // 
            // lblTyGiaTinhThue2
            // 
            this.lblTyGiaTinhThue2.Name = "lblTyGiaTinhThue2";
            this.lblTyGiaTinhThue2.Text = "123.456.789";
            this.lblTyGiaTinhThue2.Weight = 0.55646826619152023;
            // 
            // xrTableRow68
            // 
            this.xrTableRow68.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell484,
            this.xrTableCell485,
            this.lblMaSacThue5,
            this.lblTenSacThue5,
            this.xrTableCell488,
            this.lblTongTienThue5,
            this.xrTableCell490,
            this.lblSoDongTong5,
            this.xrTableCell492,
            this.xrTableCell493,
            this.xrTableCell494,
            this.lblMaDongTienTyGiaTinhThue3,
            this.xrTableCell496,
            this.lblTyGiaTinhThue3});
            this.xrTableRow68.Name = "xrTableRow68";
            this.xrTableRow68.Weight = 0.44000070889321574;
            // 
            // xrTableCell484
            // 
            this.xrTableCell484.Name = "xrTableCell484";
            this.xrTableCell484.Weight = 0.16968252886543894;
            // 
            // xrTableCell485
            // 
            this.xrTableCell485.Name = "xrTableCell485";
            this.xrTableCell485.Text = "5";
            this.xrTableCell485.Weight = 0.036002889217680169;
            // 
            // lblMaSacThue5
            // 
            this.lblMaSacThue5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSacThue5.Name = "lblMaSacThue5";
            this.lblMaSacThue5.StylePriority.UseFont = false;
            this.lblMaSacThue5.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue5.Text = "X";
            this.lblMaSacThue5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaSacThue5.Weight = 0.12434097604020905;
            // 
            // lblTenSacThue5
            // 
            this.lblTenSacThue5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenSacThue5.Name = "lblTenSacThue5";
            this.lblTenSacThue5.StylePriority.UseFont = false;
            this.lblTenSacThue5.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue5.Text = "WWWWWWWWE";
            this.lblTenSacThue5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTenSacThue5.Weight = 0.37909625953157244;
            // 
            // xrTableCell488
            // 
            this.xrTableCell488.Name = "xrTableCell488";
            this.xrTableCell488.Weight = 0.04259561655907549;
            // 
            // lblTongTienThue5
            // 
            this.lblTongTienThue5.Name = "lblTongTienThue5";
            this.lblTongTienThue5.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue5.Text = "12.345.678.901";
            this.lblTongTienThue5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue5.Weight = 0.37468442146110648;
            // 
            // xrTableCell490
            // 
            this.xrTableCell490.Name = "xrTableCell490";
            this.xrTableCell490.Text = "VND";
            this.xrTableCell490.Weight = 0.12000950850491057;
            // 
            // lblSoDongTong5
            // 
            this.lblSoDongTong5.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDongTong5.Name = "lblSoDongTong5";
            this.lblSoDongTong5.StylePriority.UseFont = false;
            this.lblSoDongTong5.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong5.Text = "NNE";
            this.lblSoDongTong5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong5.Weight = 0.33426158974643405;
            // 
            // xrTableCell492
            // 
            this.xrTableCell492.Name = "xrTableCell492";
            this.xrTableCell492.Weight = 0.056662447818540733;
            // 
            // xrTableCell493
            // 
            this.xrTableCell493.Name = "xrTableCell493";
            this.xrTableCell493.Weight = 0.46034941817171193;
            // 
            // xrTableCell494
            // 
            this.xrTableCell494.Name = "xrTableCell494";
            this.xrTableCell494.Weight = 0.043843125818321949;
            // 
            // lblMaDongTienTyGiaTinhThue3
            // 
            this.lblMaDongTienTyGiaTinhThue3.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaDongTienTyGiaTinhThue3.Name = "lblMaDongTienTyGiaTinhThue3";
            this.lblMaDongTienTyGiaTinhThue3.StylePriority.UseFont = false;
            this.lblMaDongTienTyGiaTinhThue3.Text = "XXE";
            this.lblMaDongTienTyGiaTinhThue3.Weight = 0.13182844755744311;
            // 
            // xrTableCell496
            // 
            this.xrTableCell496.Name = "xrTableCell496";
            this.xrTableCell496.Text = "-";
            this.xrTableCell496.Weight = 0.036003092977792139;
            // 
            // lblTyGiaTinhThue3
            // 
            this.lblTyGiaTinhThue3.Name = "lblTyGiaTinhThue3";
            this.lblTyGiaTinhThue3.Text = "123.456.789";
            this.lblTyGiaTinhThue3.Weight = 0.55646826619152023;
            // 
            // xrTableRow67
            // 
            this.xrTableRow67.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell470,
            this.xrTableCell471,
            this.lblMaSacThue6,
            this.lblTenSacThue6,
            this.xrTableCell474,
            this.lblTongTienThue6,
            this.xrTableCell476,
            this.lblSoDongTong6,
            this.xrTableCell478,
            this.xrTableCell479,
            this.xrTableCell480,
            this.lblMaXacDinhThoiHanNopThue,
            this.xrTableCell482,
            this.xrTableCell456,
            this.lblNguoiNopThue});
            this.xrTableRow67.Name = "xrTableRow67";
            this.xrTableRow67.Weight = 0.44000070889321663;
            // 
            // xrTableCell470
            // 
            this.xrTableCell470.Name = "xrTableCell470";
            this.xrTableCell470.Weight = 0.16968252886543891;
            // 
            // xrTableCell471
            // 
            this.xrTableCell471.Name = "xrTableCell471";
            this.xrTableCell471.Text = "6";
            this.xrTableCell471.Weight = 0.036002889217680183;
            // 
            // lblMaSacThue6
            // 
            this.lblMaSacThue6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSacThue6.Name = "lblMaSacThue6";
            this.lblMaSacThue6.StylePriority.UseFont = false;
            this.lblMaSacThue6.StylePriority.UseTextAlignment = false;
            this.lblMaSacThue6.Text = "X";
            this.lblMaSacThue6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblMaSacThue6.Weight = 0.12434097604020905;
            // 
            // lblTenSacThue6
            // 
            this.lblTenSacThue6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenSacThue6.Name = "lblTenSacThue6";
            this.lblTenSacThue6.StylePriority.UseFont = false;
            this.lblTenSacThue6.StylePriority.UseTextAlignment = false;
            this.lblTenSacThue6.Text = "WWWWWWWWE";
            this.lblTenSacThue6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTenSacThue6.Weight = 0.37909625953157244;
            // 
            // xrTableCell474
            // 
            this.xrTableCell474.Name = "xrTableCell474";
            this.xrTableCell474.Weight = 0.0425956165590755;
            // 
            // lblTongTienThue6
            // 
            this.lblTongTienThue6.Name = "lblTongTienThue6";
            this.lblTongTienThue6.StylePriority.UseTextAlignment = false;
            this.lblTongTienThue6.Text = "12.345.678.901";
            this.lblTongTienThue6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTongTienThue6.Weight = 0.37468442146110648;
            // 
            // xrTableCell476
            // 
            this.xrTableCell476.Name = "xrTableCell476";
            this.xrTableCell476.Text = "VND";
            this.xrTableCell476.Weight = 0.12000950850491057;
            // 
            // lblSoDongTong6
            // 
            this.lblSoDongTong6.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDongTong6.Name = "lblSoDongTong6";
            this.lblSoDongTong6.StylePriority.UseFont = false;
            this.lblSoDongTong6.StylePriority.UseTextAlignment = false;
            this.lblSoDongTong6.Text = "NNE";
            this.lblSoDongTong6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDongTong6.Weight = 0.33426158974643405;
            // 
            // xrTableCell478
            // 
            this.xrTableCell478.Name = "xrTableCell478";
            this.xrTableCell478.Weight = 0.056662447818540733;
            // 
            // xrTableCell479
            // 
            this.xrTableCell479.Name = "xrTableCell479";
            this.xrTableCell479.Text = "Mã xác định thời hạn nộp thuế";
            this.xrTableCell479.Weight = 0.5530076282851627;
            // 
            // xrTableCell480
            // 
            this.xrTableCell480.Name = "xrTableCell480";
            this.xrTableCell480.Weight = 0.049442865109211517;
            // 
            // lblMaXacDinhThoiHanNopThue
            // 
            this.lblMaXacDinhThoiHanNopThue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaXacDinhThoiHanNopThue.Name = "lblMaXacDinhThoiHanNopThue";
            this.lblMaXacDinhThoiHanNopThue.StylePriority.UseFont = false;
            this.lblMaXacDinhThoiHanNopThue.Text = "X";
            this.lblMaXacDinhThoiHanNopThue.Weight = 0.0695738409645982;
            // 
            // xrTableCell482
            // 
            this.xrTableCell482.Name = "xrTableCell482";
            this.xrTableCell482.Weight = 0.078272521641893217;
            // 
            // xrTableCell456
            // 
            this.xrTableCell456.Name = "xrTableCell456";
            this.xrTableCell456.Text = "Người nộp thuế";
            this.xrTableCell456.Weight = 0.37837079854809968;
            // 
            // lblNguoiNopThue
            // 
            this.lblNguoiNopThue.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguoiNopThue.Name = "lblNguoiNopThue";
            this.lblNguoiNopThue.StylePriority.UseFont = false;
            this.lblNguoiNopThue.Text = "X";
            this.lblNguoiNopThue.Weight = 0.099824696167823912;
            // 
            // xrTableRow66
            // 
            this.xrTableRow66.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell431,
            this.xrTableCell432,
            this.xrTableCell433,
            this.xrTableCell434,
            this.xrTableCell435,
            this.lblMaLyDoDeNghi,
            this.xrTableCell457,
            this.xrTableCell459,
            this.lblPhanLoaiNopThue});
            this.xrTableRow66.Name = "xrTableRow66";
            this.xrTableRow66.Weight = 0.44000070889321563;
            // 
            // xrTableCell431
            // 
            this.xrTableCell431.Name = "xrTableCell431";
            this.xrTableCell431.Weight = 0.043745863393282874;
            // 
            // xrTableCell432
            // 
            this.xrTableCell432.Name = "xrTableCell432";
            this.xrTableCell432.Weight = 0.30428201777057945;
            // 
            // xrTableCell433
            // 
            this.xrTableCell433.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell433.Name = "xrTableCell433";
            this.xrTableCell433.StylePriority.UseFont = false;
            this.xrTableCell433.Weight = 1.2893083730244483;
            // 
            // xrTableCell434
            // 
            this.xrTableCell434.Name = "xrTableCell434";
            this.xrTableCell434.Text = "Mã lý do đề nghị BP";
            this.xrTableCell434.Weight = 0.55300759065855065;
            // 
            // xrTableCell435
            // 
            this.xrTableCell435.Name = "xrTableCell435";
            this.xrTableCell435.Weight = 0.049442679656233161;
            // 
            // lblMaLyDoDeNghi
            // 
            this.lblMaLyDoDeNghi.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaLyDoDeNghi.Name = "lblMaLyDoDeNghi";
            this.lblMaLyDoDeNghi.StylePriority.UseFont = false;
            this.lblMaLyDoDeNghi.Text = "X";
            this.lblMaLyDoDeNghi.Weight = 0.069574058087140012;
            // 
            // xrTableCell457
            // 
            this.xrTableCell457.Name = "xrTableCell457";
            this.xrTableCell457.Weight = 0.0782726288923774;
            // 
            // xrTableCell459
            // 
            this.xrTableCell459.Name = "xrTableCell459";
            this.xrTableCell459.Text = "Phân loại nộp thuế";
            this.xrTableCell459.Weight = 0.37837057487162407;
            // 
            // lblPhanLoaiNopThue
            // 
            this.lblPhanLoaiNopThue.Name = "lblPhanLoaiNopThue";
            this.lblPhanLoaiNopThue.Text = "X";
            this.lblPhanLoaiNopThue.Weight = 0.099824802107521232;
            // 
            // xrTableRow65
            // 
            this.xrTableRow65.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow65.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell460,
            this.xrTableCell461,
            this.xrTableCell462,
            this.xrTableCell463,
            this.xrTableCell464,
            this.lblTongSoTrangToKhai,
            this.xrTableCell466,
            this.xrTableCell467,
            this.lblTongSoDongHang});
            this.xrTableRow65.Name = "xrTableRow65";
            this.xrTableRow65.StylePriority.UseBorders = false;
            this.xrTableRow65.Weight = 0.44000017483476173;
            // 
            // xrTableCell460
            // 
            this.xrTableCell460.Name = "xrTableCell460";
            this.xrTableCell460.Weight = 0.043745863393282874;
            // 
            // xrTableCell461
            // 
            this.xrTableCell461.Name = "xrTableCell461";
            this.xrTableCell461.Weight = 0.30428201777057945;
            // 
            // xrTableCell462
            // 
            this.xrTableCell462.Name = "xrTableCell462";
            this.xrTableCell462.Weight = 1.1750626383613003;
            // 
            // xrTableCell463
            // 
            this.xrTableCell463.Name = "xrTableCell463";
            this.xrTableCell463.Text = "Tổng số trang của tờ khai";
            this.xrTableCell463.Weight = 0.46467605977185417;
            // 
            // xrTableCell464
            // 
            this.xrTableCell464.Name = "xrTableCell464";
            this.xrTableCell464.Weight = 0.054004093667182493;
            // 
            // lblTongSoTrangToKhai
            // 
            this.lblTongSoTrangToKhai.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongSoTrangToKhai.Name = "lblTongSoTrangToKhai";
            this.lblTongSoTrangToKhai.StylePriority.UseFont = false;
            this.lblTongSoTrangToKhai.Text = "NE";
            this.lblTongSoTrangToKhai.Weight = 0.099758116635522961;
            // 
            // xrTableCell466
            // 
            this.xrTableCell466.Name = "xrTableCell466";
            this.xrTableCell466.Weight = 0.036003097223222258;
            // 
            // xrTableCell467
            // 
            this.xrTableCell467.Name = "xrTableCell467";
            this.xrTableCell467.Text = "Tổng số dòng hàng của tờ khai";
            this.xrTableCell467.Weight = 0.58847189953129142;
            // 
            // lblTongSoDongHang
            // 
            this.lblTongSoDongHang.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongSoDongHang.Name = "lblTongSoDongHang";
            this.lblTongSoDongHang.StylePriority.UseFont = false;
            this.lblTongSoDongHang.Text = "NE";
            this.lblTongSoDongHang.Weight = 0.099824802107521232;
            // 
            // ToKhaiTamHangHoaNhapKhau1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader});
            this.Margins = new System.Drawing.Printing.Margins(12, 16, 22, 100);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTrang;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell159;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDonViTinh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTrongLuongHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell187;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDonViTinhTrongLuong;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongContainer;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell194;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell lblKyHieuSoHieu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell195;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDuocPhepNhapKho;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell lblMaVanBanPhapQuyKhac1;
        private DevExpress.XtraReports.UI.XRTableCell lblMaVanBanPhapQuyKhac2;
        private DevExpress.XtraReports.UI.XRTableCell lblMaVanBanPhapQuyKhac3;
        private DevExpress.XtraReports.UI.XRTableCell lblMaVanBanPhapQuyKhac4;
        private DevExpress.XtraReports.UI.XRTableCell lblMaVanBanPhapQuyKhac5;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell214;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiHinhThucHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell lblSoHoaDon;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell218;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTiepNhanHoaDonDienTu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell224;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayPhatHanh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongThucThanhToan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell235;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDieuKienGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDongTienHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell237;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTriGiaTinhThue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell243;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTableCell lblTongHeSoPhanBoTriGia;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiNhapLieu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell249;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableCell lblMaKetQuaKiemTraNoiDung;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell261;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell267;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiGiayPhepNhapKhau1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableCell lblSoGiayPhep1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell279;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiGiayPhepNhapKhau2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell lblSoGiayPhep2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiGiayPhepNhapKhau3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell lblSoGiayPhep3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiGiayPhepNhapKhau4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell lblSoGiayPhep4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiGiayPhepNhapKhau5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableCell lblSoGiayPhep5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell295;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiKhaiTriGia;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell291;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTiepNhanKhaiTriGiaTongHop;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiCongThucChuan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiDieuChinhTriGia;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell319;
        private DevExpress.XtraReports.UI.XRTableCell lblPhuongPhapDieuChinhTriGia;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTienTe;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableCell lblGiaCoSo;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell297;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell299;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell301;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiPhiVanChuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell321;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTienTePhiVanChuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRTableCell lblPhiVanChuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell303;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell305;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiBaoHiem;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTienTeCuaTienBaoHiem;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRTableCell lblPhiBaoHiem;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDangKyBaoHiemTongHop;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell309;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell332;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell348;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell344;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTenKhoanDieuChinh1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiDieuChinh1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDongTienDieuChinhTriGia1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell346;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaKhoanDieuChinh1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
        private DevExpress.XtraReports.UI.XRTableCell lblTongHeSoPhanBoTriGiaKhoanDieuChinh1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell349;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell350;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTenKhoanDieuChinh2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell352;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiDieuChinh2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell354;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDongTienDieuChinhTriGia2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell356;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaKhoanDieuChinh2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell358;
        private DevExpress.XtraReports.UI.XRTableCell lblTongHeSoPhanBoTriGiaKhoanDieuChinh2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell360;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow56;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell361;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell362;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTenKhoanDieuChinh3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell364;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiDieuChinh3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell366;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDongTienDieuChinhTriGia3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell368;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaKhoanDieuChinh3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell370;
        private DevExpress.XtraReports.UI.XRTableCell lblTongHeSoPhanBoTriGiaKhoanDieuChinh3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell372;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow57;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell373;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell374;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTenKhoanDieuChinh4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell376;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiDieuChinh4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell378;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDongTienDieuChinhTriGia4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell380;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaKhoanDieuChinh4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell382;
        private DevExpress.XtraReports.UI.XRTableCell lblTongHeSoPhanBoTriGiaKhoanDieuChinh4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell384;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell385;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell386;
        private DevExpress.XtraReports.UI.XRTableCell lblMaTenKhoanDieuChinh5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell388;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiDieuChinh5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell390;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDongTienDieuChinhTriGia5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaKhoanDieuChinh5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell394;
        private DevExpress.XtraReports.UI.XRTableCell lblTongHeSoPhanBoTriGiaKhoanDieuChinh5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell396;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow59;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell397;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell400;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
        private DevExpress.XtraReports.UI.XRTableCell lblChiTietKhaiTriGia;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow61;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell401;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell404;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell405;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell403;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell406;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell422;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell402;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell407;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell408;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThue1;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThue1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell411;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThue1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell413;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDongTong1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell419;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell420;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell417;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThuePhaiNop;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell421;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell412;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell414;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell423;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThue2;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThue2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell426;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThue2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell428;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDongTong2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell430;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell437;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell438;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienBaoLanh;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell440;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell441;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell442;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell443;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThue3;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThue3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell446;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThue3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell448;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDongTong3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell450;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell451;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell452;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDongTienTyGiaTinhThue1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell454;
        private DevExpress.XtraReports.UI.XRTableCell lblTyGiaTinhThue1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell498;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell499;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThue4;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThue4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell502;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThue4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell504;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDongTong4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell506;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell507;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell508;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDongTienTyGiaTinhThue2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell510;
        private DevExpress.XtraReports.UI.XRTableCell lblTyGiaTinhThue2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell484;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell485;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThue5;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThue5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell488;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThue5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell490;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDongTong5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell492;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell493;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell494;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDongTienTyGiaTinhThue3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell496;
        private DevExpress.XtraReports.UI.XRTableCell lblTyGiaTinhThue3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell470;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell471;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThue6;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThue6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell474;
        private DevExpress.XtraReports.UI.XRTableCell lblTongTienThue6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell476;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDongTong6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell478;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell479;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell480;
        private DevExpress.XtraReports.UI.XRTableCell lblMaXacDinhThoiHanNopThue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell482;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell456;
        private DevExpress.XtraReports.UI.XRTableCell lblNguoiNopThue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell431;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell432;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell433;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell434;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell435;
        private DevExpress.XtraReports.UI.XRTableCell lblMaLyDoDeNghi;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell457;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell459;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiNopThue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell460;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell461;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell462;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell463;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell464;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoTrangToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell466;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell467;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoDongHang;
        internal DevExpress.XtraReports.UI.XRTable xrTable1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiDauTien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell lblSoNhanhToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoToKhaiChiaNho;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiTamNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiKiemTra;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiHangHoa;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHieuPhuongThucVanChuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiCaNhanToChuc;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSoHangHoaDaiDienToKhai;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCoQuanHaiQuanTiepNhanToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBoPhanXuLyToKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell lblGioDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRTableCell lblGioThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell lblThoiHanTaiNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell lblBieuThiTHHetHan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiNhapKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiNhapKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinhNhapKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiNhapKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDienThoaiNguoiNhapKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiUyThacNhapKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiUyThacNhapKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiXuatKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiXuatKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell103;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBuuChinhXuatKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiXuatKhau1;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiXuatKhau2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiXuatKhau3;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiXuatKhau4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNuoc;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell lblNguoiUyThacXuatKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDaiLyHaiQuan;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDaiLyHaiQuan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell129;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNhanVienHaiQuan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemLuuKhoHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDiaDiemLuuKhoHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell lblSoVanDon1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell lblSoVanDon2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDiaDiemXepHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDiaDiemXepHang;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell lblSoVanDon3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell lblSoVanDon4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell167;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhuongTienVanChuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableCell lblTenPhuongTienVanChuyen;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell lblSoVanDon5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHangDen;
    }
}
