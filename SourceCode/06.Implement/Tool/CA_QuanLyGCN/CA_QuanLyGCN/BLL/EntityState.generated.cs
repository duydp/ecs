using System;
using System.Collections.Generic;
using System.Text;

namespace CA_QuanLyGCN
{
    /// <summary>
    /// 0=Unchanged, 1=Added, 2=Updated, 3=Deleted
    /// </summary>
    public enum EntityState
    {
        Unchanged = 0,
        Added = 1,
        Updated = 2,
        Deleted = 3
    }
}