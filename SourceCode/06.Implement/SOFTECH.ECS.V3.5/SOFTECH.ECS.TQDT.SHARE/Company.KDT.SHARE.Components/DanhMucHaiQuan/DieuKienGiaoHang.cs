﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
    public partial class DieuKienGiaoHang
    {
        public static List<DieuKienGiaoHang> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<DieuKienGiaoHang> collection = new List<DieuKienGiaoHang>();

            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                DieuKienGiaoHang entity = new DieuKienGiaoHang();
                entity.ID = dataSet.Tables[0].Rows[i]["ID"].ToString();
                entity.MoTa = dataSet.Tables[0].Rows[i]["MoTa"].ToString();
                entity.DateCreated = dataSet.Tables[0].Rows[i]["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateCreated"]) : DateTime.Today;
                entity.DateModified = dataSet.Tables[0].Rows[i]["DateModified"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateModified"]) : DateTime.Today;
                collection.Add(entity);
            }

            return collection;
        }
    }
}
