﻿namespace Company.Interface
{
    partial class CauHinhToKhaiForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem5 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem6 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CauHinhToKhaiForm));
            this.label1 = new System.Windows.Forms.Label();
            this.txtTenDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ctrNuocXK = new Company.Interface.Controls.NuocHControl();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.ctrNguyenTe = new Company.Interface.Controls.NguyenTeControl();
            this.cbPTVT = new Janus.Windows.EditControls.UIComboBox();
            this.cbDKGH = new Janus.Windows.EditControls.UIComboBox();
            this.cbPTTT = new Janus.Windows.EditControls.UIComboBox();
            this.ctrCuaKhauXuatHang = new Company.Interface.Controls.CuaKhauHControl();
            this.label10 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbTinhThue = new Janus.Windows.EditControls.UIComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMienThueXNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this._lblPhuongThucTT = new System.Windows.Forms.Label();
            this.cbPTTinhThue = new Janus.Windows.EditControls.UIComboBox();
            this.cbMaHTS = new Janus.Windows.EditControls.UIComboBox();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.label6 = new System.Windows.Forms.Label();
            this.ctrDiaDiemDoHang = new Company.Interface.Controls.CuaKhauHControl();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.txtTieuDeDM = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(644, 417);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(8, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên đối tác";
            // 
            // txtTenDoiTac
            // 
            this.txtTenDoiTac.Location = new System.Drawing.Point(142, 20);
            this.txtTenDoiTac.Multiline = true;
            this.txtTenDoiTac.Name = "txtTenDoiTac";
            this.txtTenDoiTac.Size = new System.Drawing.Size(166, 45);
            this.txtTenDoiTac.TabIndex = 1;
            this.txtTenDoiTac.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(12, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nước xuất khẩu";
            // 
            // ctrNuocXK
            // 
            this.ctrNuocXK.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXK.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrNuocXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXK.Location = new System.Drawing.Point(137, 26);
            this.ctrNuocXK.Ma = "";
            this.ctrNuocXK.Name = "ctrNuocXK";
            this.ctrNuocXK.ReadOnly = false;
            this.ctrNuocXK.Size = new System.Drawing.Size(329, 22);
            this.ctrNuocXK.TabIndex = 8;
            this.ctrNuocXK.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(8, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Điều kiện giao hàng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(317, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Phương thức thanh toán";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(317, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(121, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Phương thức vận tải";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(12, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Cửa khẩu xuất hàng";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(8, 109);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(127, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Đồng tiền thanh toán";
            // 
            // ctrNguyenTe
            // 
            this.ctrNguyenTe.BackColor = System.Drawing.Color.Transparent;
            this.ctrNguyenTe.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.ctrNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNguyenTe.Location = new System.Drawing.Point(142, 104);
            this.ctrNguyenTe.Ma = "";
            this.ctrNguyenTe.Name = "ctrNguyenTe";
            this.ctrNguyenTe.ReadOnly = false;
            this.ctrNguyenTe.Size = new System.Drawing.Size(180, 22);
            this.ctrNguyenTe.TabIndex = 5;
            this.ctrNguyenTe.VisualStyleManager = this.vsmMain;
            // 
            // cbPTVT
            // 
            this.cbPTVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTVT.DisplayMember = "Ten";
            this.cbPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTVT.Location = new System.Drawing.Point(471, 169);
            this.cbPTVT.Name = "cbPTVT";
            this.cbPTVT.Size = new System.Drawing.Size(110, 21);
            this.cbPTVT.TabIndex = 6;
            this.cbPTVT.ValueMember = "ID";
            this.cbPTVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbPTVT.VisualStyleManager = this.vsmMain;
            // 
            // cbDKGH
            // 
            this.cbDKGH.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDKGH.DisplayMember = "ID";
            this.cbDKGH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDKGH.Location = new System.Drawing.Point(142, 73);
            this.cbDKGH.Name = "cbDKGH";
            this.cbDKGH.Size = new System.Drawing.Size(166, 21);
            this.cbDKGH.TabIndex = 3;
            this.cbDKGH.ValueMember = "ID";
            this.cbDKGH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbDKGH.VisualStyleManager = this.vsmMain;
            // 
            // cbPTTT
            // 
            this.cbPTTT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTTT.DisplayMember = "Name";
            this.cbPTTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTTT.Location = new System.Drawing.Point(471, 139);
            this.cbPTTT.Name = "cbPTTT";
            this.cbPTTT.Size = new System.Drawing.Size(111, 21);
            this.cbPTTT.TabIndex = 4;
            this.cbPTTT.ValueMember = "ID";
            this.cbPTTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbPTTT.VisualStyleManager = this.vsmMain;
            // 
            // ctrCuaKhauXuatHang
            // 
            this.ctrCuaKhauXuatHang.BackColor = System.Drawing.Color.Transparent;
            this.ctrCuaKhauXuatHang.ErrorMessage = "\"Cửa khẩu\" không được bỏ trống.";
            this.ctrCuaKhauXuatHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrCuaKhauXuatHang.Location = new System.Drawing.Point(137, 20);
            this.ctrCuaKhauXuatHang.Ma = "";
            this.ctrCuaKhauXuatHang.Name = "ctrCuaKhauXuatHang";
            this.ctrCuaKhauXuatHang.ReadOnly = false;
            this.ctrCuaKhauXuatHang.Size = new System.Drawing.Size(253, 25);
            this.ctrCuaKhauXuatHang.TabIndex = 10;
            this.ctrCuaKhauXuatHang.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(417, 25);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Mã HTS";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.cbTinhThue);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.txtTieuDeDM);
            this.uiGroupBox1.Controls.Add(this.txtMienThueXNK);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtTenDoiTac);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.cbDKGH);
            this.uiGroupBox1.Controls.Add(this._lblPhuongThucTT);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.cbPTTinhThue);
            this.uiGroupBox1.Controls.Add(this.cbPTTT);
            this.uiGroupBox1.Controls.Add(this.ctrNguyenTe);
            this.uiGroupBox1.Controls.Add(this.cbPTVT);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(588, 199);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Tờ khai chung";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // cbTinhThue
            // 
            this.cbTinhThue.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Không";
            uiComboBoxItem3.Value = 0;
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Có";
            uiComboBoxItem4.Value = 1;
            this.cbTinhThue.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbTinhThue.Location = new System.Drawing.Point(142, 139);
            this.cbTinhThue.Name = "cbTinhThue";
            this.cbTinhThue.Size = new System.Drawing.Size(166, 21);
            this.cbTinhThue.TabIndex = 7;
            this.cbTinhThue.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(8, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(127, 43);
            this.label9.TabIndex = 12;
            this.label9.Text = "Tự động phân bổ các phí vào trị giá tính thuế";
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(317, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(89, 30);
            this.label11.TabIndex = 2;
            this.label11.Text = "Thông tư miễn thuế XNK";
            // 
            // txtMienThueXNK
            // 
            this.txtMienThueXNK.Location = new System.Drawing.Point(408, 20);
            this.txtMienThueXNK.Multiline = true;
            this.txtMienThueXNK.Name = "txtMienThueXNK";
            this.txtMienThueXNK.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtMienThueXNK.Size = new System.Drawing.Size(174, 45);
            this.txtMienThueXNK.TabIndex = 2;
            this.txtMienThueXNK.VisualStyleManager = this.vsmMain;
            // 
            // _lblPhuongThucTT
            // 
            this._lblPhuongThucTT.AutoSize = true;
            this._lblPhuongThucTT.BackColor = System.Drawing.Color.Transparent;
            this._lblPhuongThucTT.Location = new System.Drawing.Point(8, 175);
            this._lblPhuongThucTT.Name = "_lblPhuongThucTT";
            this._lblPhuongThucTT.Size = new System.Drawing.Size(133, 13);
            this._lblPhuongThucTT.TabIndex = 6;
            this._lblPhuongThucTT.Text = "Phương thức tính thuế";
            // 
            // cbPTTinhThue
            // 
            this.cbPTTinhThue.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem5.FormatStyle.Alpha = 0;
            uiComboBoxItem5.IsSeparator = false;
            uiComboBoxItem5.Text = "Dựa trên đơn giá NT";
            uiComboBoxItem5.Value = "0";
            uiComboBoxItem6.FormatStyle.Alpha = 0;
            uiComboBoxItem6.IsSeparator = false;
            uiComboBoxItem6.Text = "Dựa trên trị giá NT";
            uiComboBoxItem6.Value = "1";
            this.cbPTTinhThue.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem5,
            uiComboBoxItem6});
            this.cbPTTinhThue.Location = new System.Drawing.Point(142, 169);
            this.cbPTTinhThue.Name = "cbPTTinhThue";
            this.cbPTTinhThue.Size = new System.Drawing.Size(111, 21);
            this.cbPTTinhThue.TabIndex = 4;
            this.cbPTTinhThue.ValueMember = "ID";
            this.cbPTTinhThue.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            // 
            // cbMaHTS
            // 
            this.cbMaHTS.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Không";
            uiComboBoxItem1.Value = 0;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Có";
            uiComboBoxItem2.Value = 1;
            this.cbMaHTS.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbMaHTS.Location = new System.Drawing.Point(472, 20);
            this.cbMaHTS.Name = "cbMaHTS";
            this.cbMaHTS.Size = new System.Drawing.Size(97, 21);
            this.cbMaHTS.TabIndex = 1;
            this.cbMaHTS.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(208, 384);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 23);
            this.btnSave.TabIndex = 12;
            this.btnSave.Text = "Lưu cấu hình";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(321, 384);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(12, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(105, 13);
            this.label6.TabIndex = 3;
            this.label6.Text = "Địa điểm dỡ hàng";
            // 
            // ctrDiaDiemDoHang
            // 
            this.ctrDiaDiemDoHang.BackColor = System.Drawing.Color.Transparent;
            this.ctrDiaDiemDoHang.ErrorMessage = "\"Cửa khẩu\" không được bỏ trống.";
            this.ctrDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDiaDiemDoHang.Location = new System.Drawing.Point(137, 54);
            this.ctrDiaDiemDoHang.Ma = "";
            this.ctrDiaDiemDoHang.Name = "ctrDiaDiemDoHang";
            this.ctrDiaDiemDoHang.ReadOnly = false;
            this.ctrDiaDiemDoHang.Size = new System.Drawing.Size(318, 25);
            this.ctrDiaDiemDoHang.TabIndex = 9;
            this.ctrDiaDiemDoHang.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.ctrNuocXK);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.ctrDiaDiemDoHang);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 217);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(588, 96);
            this.uiGroupBox2.TabIndex = 20;
            this.uiGroupBox2.Text = "Tờ khai nhập";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.ctrCuaKhauXuatHang);
            this.uiGroupBox3.Controls.Add(this.cbMaHTS);
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(12, 319);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(588, 59);
            this.uiGroupBox3.TabIndex = 21;
            this.uiGroupBox3.Text = "Tờ khai xuất";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // txtTieuDeDM
            // 
            this.txtTieuDeDM.Location = new System.Drawing.Point(408, 81);
            this.txtTieuDeDM.Multiline = true;
            this.txtTieuDeDM.Name = "txtTieuDeDM";
            this.txtTieuDeDM.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtTieuDeDM.Size = new System.Drawing.Size(174, 45);
            this.txtTieuDeDM.TabIndex = 2;
            this.txtTieuDeDM.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTieuDeDM.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(317, 92);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 30);
            this.label12.TabIndex = 2;
            this.label12.Text = "Thông tư miễn thuế GTGT";
            // 
            // CauHinhToKhaiForm
            // 
            this.AcceptButton = this.btnSave;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(644, 417);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "CauHinhToKhaiForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cấu hình tham số mặc định của tờ khai nhập / xuất";
            this.Load += new System.EventHandler(this.CauHinhToKhaiForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoiTac;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Company.Interface.Controls.NuocHControl ctrNuocXK;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private Company.Interface.Controls.NguyenTeControl ctrNguyenTe;
        private Janus.Windows.EditControls.UIComboBox cbPTVT;
        private Janus.Windows.EditControls.UIComboBox cbDKGH;
        private Janus.Windows.EditControls.UIComboBox cbPTTT;
        private System.Windows.Forms.Label label10;
        private Company.Interface.Controls.CuaKhauHControl ctrCuaKhauXuatHang;
        private Janus.Windows.EditControls.UIComboBox cbMaHTS;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnSave;
        private System.Windows.Forms.Label label6;
        private Company.Interface.Controls.CuaKhauHControl ctrDiaDiemDoHang;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtMienThueXNK;
        private Janus.Windows.EditControls.UIComboBox cbTinhThue;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label _lblPhuongThucTT;
        private Janus.Windows.EditControls.UIComboBox cbPTTinhThue;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtTieuDeDM;
    }
}
