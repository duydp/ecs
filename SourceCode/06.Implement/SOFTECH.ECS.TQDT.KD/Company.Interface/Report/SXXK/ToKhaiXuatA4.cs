﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class ToKhaiXuatA4 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.Interface.Report.ReportViewTKXA4Form report;
        public bool BanLuuHaiQuan = false;
        public ToKhaiXuatA4()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            double tongTriGiaNT = 0;

            if (BanLuuHaiQuan)
            {
                lblBanLuuHaiQuan.Text = "Bản lưu Hải quan";
            }
            else
            {
                lblBanLuuHaiQuan.Text = "Bản lưu người khai Hải quan";
            }

            DateTime minDate = new DateTime(1900, 1, 1);
            lblMaHaiQuan.Text = GlobalSettings.MA_HAI_QUAN;
            lblChiCucHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN_NGAN.ToUpper();
            lblCucHaiQuan.Text = GlobalSettings.TEN_CUC_HAI_QUAN.ToUpper();
            if (this.TKMD.SoTiepNhan != 0)
                this.lblSoTiepNhan.Text = "Số TNDKDT: " + this.TKMD.SoTiepNhan;
            if (this.TKMD.SoToKhai > 0)
                lblSoToKhai.Text = this.TKMD.SoToKhai + "";
            if (this.TKMD.NgayDangKy > minDate)
                lblNgayDangKy.Text = this.TKMD.NgayDangKy.ToString("dd/MM/yyyy");
            if (this.TKMD.SoLuongPLTK > 0)
                lblSoPLTK.Text = this.TKMD.SoLuongPLTK.ToString();
            lblMaDoanhNghiep1.Text = this.ToStringForReport(this.TKMD.MaDoanhNghiep, "   ");
            lblTenDoanhNghiep1.Text = this.TKMD.TenDoanhNghiep.ToUpper() + "\n\r" + GlobalSettings.DIA_CHI.ToUpper();
            lblTenDoiTac.Text = this.TKMD.TenDonViDoiTac;
            lblNguoiUyThac.Text = "";
            lblMaNguoiUyThac.Text = "";
            lblMaDaiLyTTHQ.Text = this.TKMD.MaDaiLyTTHQ;
            lblTenDaiLyTTHQ.Text = this.TKMD.TenDaiLyTTHQ;
            lblSoGP.Text = "Số : " + this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep > minDate)
                lblNgayGP.Text = "Ngày: " + this.TKMD.NgayGiayPhep.ToString("dd/MM/yyyy");
            else
                lblNgayGP.Text = "Ngày: ";
            if (this.TKMD.NgayHetHanGiayPhep > minDate)
                lblNgayHHGP.Text = "Ngày hết hạn: " + this.TKMD.NgayHetHanGiayPhep.ToString("dd/MM/yyyy");
            else
                lblNgayHHGP.Text = "Ngày hết hạn: ";
            if (this.TKMD.SoHopDong.Length > 36)
                lblSoHopDong.Font = new Font("Times New Roman", 6.5f);
            lblSoHD.Text = "Số : " + this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong > minDate)
                lblNgayHD.Text = "Ngày: " + this.TKMD.NgayHopDong.ToString("dd/MM/yyyy");
            else
                lblNgayHD.Text = "Ngày: ";
            if (this.TKMD.NgayHetHanHopDong > minDate)
                lblNgayHHHD.Text = "Ngày hết hạn: " + this.TKMD.NgayHetHanHopDong.ToString("dd/MM/yyyy");
            else
                lblNgayHHHD.Text = "Ngày hết hạn: ";
            lblMaNuoc.Text = ToStringForReport(this.TKMD.NuocNK_ID, "      ");
            lblTenNuoc.Text = Nuoc.GetName(this.TKMD.NuocNK_ID).ToUpper();
            lblMaDiaDiemDoHang.Text = ToStringForReport(this.TKMD.CuaKhau_ID, "      ");
            lblDiaDiemDoHang.Text = CuaKhau.GetName(this.TKMD.CuaKhau_ID);
            lblDKGH.Text = this.TKMD.DKGH_ID;
            lblNgoaiTe.Text = ToStringForReport(this.TKMD.NguyenTe_ID, "      ");
            lblTyGiaTT.Text = this.TKMD.TyGiaTinhThue.ToString("G10");
            lblPTTT.Text = this.TKMD.PTTT_ID;
            if (TKMD.SoKien > 0)
                lblSoKienTrongLuong.Text = "         TC: " + TKMD.SoKien.ToString("n0") + " Kiện " + "    NW : " + this.TKMD.TrongLuongNet  + " Kg    GW : " + this.TKMD.TrongLuong + " Kg " ;
            if (this.TKMD.HMDCollection.Count <= 9)
            {
                for (int i = 0; i < this.TKMD.HMDCollection.Count; i++)
                {
                    XRControl control = new XRControl();
                    HangMauDich hmd = this.TKMD.HMDCollection[i];
                    control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                    control.Text = hmd.TenHang;
                    control = this.xrTable1.Rows[i].Controls["MaHS" + (i + 1)];
                    control.Text = hmd.MaHS;
                    control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                    control.Text = hmd.SoLuong.ToString("G20");
                    control = this.xrTable1.Rows[i].Controls["DVT" + (i + 1)];
                    control.Text = DonViTinh.GetName((object)hmd.DVT_ID);
                    control = this.xrTable1.Rows[i].Controls["DonGiaNT" + (i + 1)];
                    control.Text = hmd.DonGiaKB.ToString("G10");
                    control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                    control.Text = hmd.TriGiaKB.ToString("N2");
                    tongTriGiaNT += hmd.TriGiaKB;
                }
            }
            else
            {
                ArrayList arr = this.GetNhomHang();
                if (arr.Count == 1 && arr[0].ToString() == "")
                {
                    string nhomHang = arr[0].ToString();
                    XRControl control = new XRControl();
                    HangMauDichCollection col = GetHangCoCungNhom(nhomHang);
                    control = this.xrTable1.Rows[0].Controls["TenHang1"];
                    control.Text = "PHỤ LỤC ĐÍNH KÈM";
                    control = this.xrTable1.Rows[0].Controls["Luong1"];
                    control.Text = GetTongSoLuong(col).ToString("G15");
                    control = this.xrTable1.Rows[0].Controls["TriGiaNT1"];
                    control.Text = GetTongTriGiaNT(col).ToString("N2");

                }
                else
                {
                    for (int i = 0; i < arr.Count; i++)
                    {
                        string nhomHang = arr[i].ToString();
                        XRControl control = new XRControl();
                        HangMauDichCollection col = GetHangCoCungNhom(nhomHang);
                        control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                        control.Text = nhomHang;
                        control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                        control.Text = GetTongSoLuong(col).ToString("G15");
                        control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                        control.Text = GetTongTriGiaNT(col).ToString("N2");
                        
                    }
                }
                tongTriGiaNT = this.GetTongTriGiaNT(this.TKMD.HMDCollection);
            }
            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            XRControl control1 = new XRControl();
            int index = 3;
            foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
            {
                if (ct.LoaiCT == 1)
                {
                    control1 = this.Detail.Controls["lblSoBanChinh" + 1];
                    control1.Text = ct.SoBanChinh + "";
                    control1 = this.Detail.Controls["lblSoBanSao" + 1];
                    control1.Text = ct.SoBanSao + "";
                }
                else if (ct.LoaiCT == 3)
                {
                    control1 = this.Detail.Controls["lblSoBanChinh" + 2];
                    control1.Text = ct.SoBanChinh + "";
                    control1 = this.Detail.Controls["lblSoBanSao" + 2];
                    control1.Text = ct.SoBanSao + "";
                }
                else
                {
                    if (index == 7) return;
                    control1 = this.Detail.Controls["lblTenChungTu" + index];
                    control1.Text = ct.TenChungTu;
                    control1 = this.Detail.Controls["lblSoBanChinh" + index];
                    control1.Text = ct.SoBanChinh + "";
                    control1 = this.Detail.Controls["lblSoBanSao" + index];
                    control1.Text = ct.SoBanSao + "";
                    index++;
                }
            }
        }
        private string ToStringForReport(string s, string patern)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + patern;
            temp += s[s.Length - 1];
            return temp;
        }
        public void setVisibleImage(bool t)
        {
            //ptbImage.Visible = t;
        }
        private double GetTongTriGiaNT(HangMauDichCollection HMDCollection)
        {
            double d = 0;
            foreach (HangMauDich hmd in HMDCollection)
                d += hmd.TriGiaKB;
            return d;
        }
        private decimal GetTongSoLuong(HangMauDichCollection HMDCollection)
        {
            decimal d = 0;
            foreach (HangMauDich hmd in HMDCollection)
                d += hmd.SoLuong;
            return d;
        }
        private ArrayList GetNhomHang()
        {
            ArrayList arr = new ArrayList();
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (!CheckExitNhomHang(hmd.NhomHang, arr)) arr.Add(hmd.NhomHang);
            }
            return arr;
        }
        private bool CheckExitNhomHang(string nhomHang, ArrayList arr)
        {
            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i].ToString() == nhomHang) return true;
            }
            return false;
        }
        private HangMauDichCollection GetHangCoCungNhom(string nhomHang)
        {
            HangMauDichCollection col = new HangMauDichCollection();
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if (hmd.NhomHang == nhomHang) col.Add(hmd);
            return col;
        }
        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void xrCoThue_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            if (cell.Text.Trim() == "")
            cell.Text = "X";
            else
            cell.Text = "";
            this.CreateDocument();

        }


    }
}
