﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Janus.Windows.GridEX;
namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseRegisterManagementForm : Company.Interface.BaseForm
    {
        public T_KHOKETOAN_DANHSACHKHO kho = new T_KHOKETOAN_DANHSACHKHO();
        public bool isBrowser = false;
        public WareHouseRegisterManagementForm()
        {
            InitializeComponent();
        }

        private void WareHouseRegisterManagementForm_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string where = " 1=1 ";
                if(!String.IsNullOrEmpty(txtMaKho.Text))
                    where += " AND MAKHO LIKE N'%" + txtMaKho.Text.ToString() + "%'";
                if (!String.IsNullOrEmpty(txtTenKho.Text))
                    where += " AND TENKHO LIKE N'%" + txtTenKho.Text.ToString() + "%'";
                grList.Refetch();
                grList.DataSource = T_KHOKETOAN_DANHSACHKHO.SelectCollectionDynamic(where,"ID");
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<T_KHOKETOAN_DANHSACHKHO> ItemColl = new List<T_KHOKETOAN_DANHSACHKHO>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa Kho này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KHOKETOAN_DANHSACHKHO)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KHOKETOAN_DANHSACHKHO item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                    }
                }
                btnSearch_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType==RowType.Record)
                {
                    kho = new T_KHOKETOAN_DANHSACHKHO();
                    kho = (T_KHOKETOAN_DANHSACHKHO)e.Row.DataRow;
                    if (!isBrowser)
                    {
                        WareHouseRegisterForm f = new WareHouseRegisterForm();
                        f.kho = kho;
                        f.ShowDialog(this);
                        btnSearch_Click(null, null);   
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            WareHouseRegisterForm f = new WareHouseRegisterForm();
            f.ShowDialog(this);
            btnSearch_Click(null,null);
        }
    }
}
