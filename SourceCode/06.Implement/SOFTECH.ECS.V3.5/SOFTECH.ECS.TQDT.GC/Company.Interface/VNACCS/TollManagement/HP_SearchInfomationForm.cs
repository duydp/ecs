﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.VNACCS;
#if GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
#elif SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
#elif KD_V4
using Company.KD.BLL.KDT.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
#endif
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.Messages;

namespace Company.Interface.VNACCS.TollManagement
{
    public partial class HP_SearchInfomationForm : BaseFormHaveGuidPanel
    {
        private FeedBackContentPort feedbackContent = null;
        private string msgInfor = string.Empty;
        public T_KDT_THUPHI_BIENLAI BL = new T_KDT_THUPHI_BIENLAI();
        public HP_SearchInfomationForm()
        {
            InitializeComponent();
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(cbbLoaiHangHoa, errorProvider, "LOẠI HÀNG HÓA", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoBienLai, errorProvider, "SỐ BIÊN LAI", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        public string LoaiHangHoa;
        public string SoBienLai;
        public string SoToKhai;
        public string SoVanDon;
        public string SoContainer;
        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!ValidateForm(false))
                return;
            BL = new T_KDT_THUPHI_BIENLAI();
            LoaiHangHoa = cbbLoaiHangHoa.SelectedValue.ToString();
            SoBienLai = txtSoBienLai.Text.ToString();
            SoToKhai = txtSoToKhai.Text.ToString();
            SoVanDon = txtSoVanDon.Text.ToString();
            SoContainer = txtSoContainer.Text.ToString();
            Send();
        }
        private void Set()
        {
            try
            {
                txtMauBienLai.Text = BL.MauBienLai;
                txtSoBienLaiThu.Text = BL.SoBienLai;
                txtSoSerial.Text = BL.SoSerial;
                txtQuyenBienLai.Text = BL.QuyenBienLai;
                clcNgayBienLai.Value = BL.NgayBienLai;
                cbbTrangThai.SelectedValue = BL.TrangThaiBienLai;
                txtTongTien.Text = BL.TongTien.ToString();
                txtMaDoanhNghiep.Text = BL.MaDoanhNghiepNP;
                txtTenDoanhNghiep.Text = BL.TenDoanhNghiepNP;
                txtDiaChi.Text = BL.DiaChiDoanhNghiepNP;
                txtSoToKhai.Text = BL.SoToKhai;
                clcNgayToKhai.Value = Convert.ToDateTime(BL.NgayToKhai);
                ctrMaLoaiHinh.Code = BL.MaLoaiHinh;
                cbbNhomLoaiHinh.SelectedValue = BL.NhomLoaiHinh;
                ctrMaPhuongThucVT.Code = BL.MaPTVC.ToString();
                ctrMaDDLuuKho.Code = BL.MaDDLK;
                cbbChoPhep.SelectedValue = BL.ChoPhepLayHang;
                txtGhiChu.Text = BL.DienGiai;
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = BL.PhiCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Send()
        {
                try
                {
                    MsgSend sendXML = new MsgSend();
                    string returnMessage = string.Empty;
                    BL.GuidString = Guid.NewGuid().ToString();
                    TKNP_InfomationSearch search_Information = new TKNP_InfomationSearch();
                    search_Information = Mapper_V4.ToDataTransferSearchInformation(BL, GlobalSettings.TEN_DON_VI,LoaiHangHoa,SoBienLai,SoToKhai,SoVanDon,SoContainer);

                    ObjectSend msgSend = new ObjectSend(
                        new MessageHeader()
                        {
                            ProcedureType = "2",
                            Reference = new Reference()
                            {
                                Version = "3.00",
                                MessageID = BL.GuidString,
                            },
                            SendApplication = new SendApplication()
                            {
                                Name = "ECUS3",
                                Version = "3.00",
                                CompanyName = "ThaiSon",
                                CompanyIdentity = "0101300842",
                                CreateMessageIssue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                            },
                            From = new NameBase()
                            {
                                Name = "Chi cục HQ CK cảng Hải Phòng KV III",
                                Identity = "03TG",
                            },
                            To = new NameBase()
                            {
                                Name = "Chi cục HQ CK cảng Hải Phòng KV III",
                                Identity = "03TG",
                            },
                            Subject = new Subject()
                            {
                                Type = DeclarationIssuer.SearchInformation,
                                Function = DeclarationFunction.KHAI_BAO,
                                Reference = BL.GuidString,
                            }
                        },
                        new NameBase()
                        {
                            Name = "Chi cục HQ CK cảng Hải Phòng KV III",
                            Identity = "03TG",
                        },
                        new NameBase()
                        {
                            Name = "Chi cục HQ CK cảng Hải Phòng KV III",
                            Identity = "03TG",
                        },
                        new Subject()
                        {
                            Type = DeclarationIssuer.SearchInformation,
                            Function = DeclarationFunction.KHAI_BAO,
                            Reference = BL.GuidString,
                        },
                        search_Information
                        );
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    sendForm.IsPort = true;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(BL.ID, MessageTitle.SearchInformation);
                        sendXML.LoaiHS = LoaiKhaiBao.SearchInformaton;
                        sendXML.master_id = BL.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = Convert.ToInt32(search_Information.Function);
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            //ShowMessageTQDT(msgInfor, false);
                            Set();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.DUYET_LUONG_CHUNG_TU)
                        {
                            ShowMessageTQDT(msgInfor, false);
                            Set();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.SearchInformationSendHandler(BL, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void Feedback()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = BL.GuidString;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.SearchInformation,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.SearchInformation,
                };
                subjectBase.Type = DeclarationIssuer.SearchInformation;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = GlobalSettings.MA_DON_VI,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(GlobalSettings.MA_HAI_QUAN.Trim())),
                                                  Identity = GlobalSettings.MA_HAI_QUAN
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                sendForm.IsPort = true;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        Feedback();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    //int Loai = Convert.ToInt32(e.Row.Cells["Loai"].Value);
                    //int TinhChat = Convert.ToInt32(e.Row.Cells["TinhChat"].Value);
                    //switch (Loai)
                    //{
                    //    case 20:
                    //        e.Row.Cells["Loai"].Text = "Container 20 feet";
                    //        break;
                    //    case 40:
                    //        e.Row.Cells["Loai"].Text = "Container 40 feet";
                    //        break;
                    //    case 99:
                    //        e.Row.Cells["Loai"].Text = "Container loại khác";
                    //        break;
                    //    default:
                    //        break;
                    //}
                    //switch (TinhChat)
                    //{
                    //    case 0:
                    //        e.Row.Cells["TinhChat"].Text = "Container thường";
                    //        break;
                    //    case 1:
                    //        e.Row.Cells["TinhChat"].Text = "Container hàng khô";
                    //        break;
                    //    case 2:
                    //        e.Row.Cells["TinhChat"].Text = "Container hàng lạnh";
                    //        break;
                    //    default:
                    //        break;
                    //}
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbChoPhep_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
