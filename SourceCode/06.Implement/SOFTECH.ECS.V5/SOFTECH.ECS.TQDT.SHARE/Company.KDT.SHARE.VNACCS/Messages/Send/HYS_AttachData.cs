using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using System.IO;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class HYS_AttachData
    {
        public string FileName { get; set; }
        public byte[] Data { get; set; }
    }
}
