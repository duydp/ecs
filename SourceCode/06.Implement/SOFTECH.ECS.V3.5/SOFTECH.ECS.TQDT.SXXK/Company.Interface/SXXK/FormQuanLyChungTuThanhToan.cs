﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.QuanLyChungTu.CTTT;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK
{
    public partial class FormQuanLyChungTuThanhToan : Company.Interface.BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public bool IsOpenFromChayThanhLy = false;

        private CTTT_ChungTu objCTTT = null; //Dung luu tam

        public FormQuanLyChungTuThanhToan()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                FrmChungTuThanhToanChiTiet frm = new FrmChungTuThanhToanChiTiet();
                //frm.HSTL = IsOpenFromChayThanhLy ? HSTL : new HoSoThanhLyDangKy();
                //frm.IsOpenFromChayThanhLy = IsOpenFromChayThanhLy;
                frm.ChungTuThanhToan = new CTTT_ChungTu();
                frm.LoaiChungTu = cmbLoaiChungTu.SelectedValue.ToString();
                frm.ChungTuThanhToan.LoaiChungTu = cmbLoaiChungTu.SelectedValue.ToString();
                frm.ShowDialog(this);

                LoadData(string.Empty);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Janus.Windows.GridEX.GridEXSelectedItemCollection items = dgList.SelectedItems;

                if (items.Count > 0)
                {
                    if (MLMessages("Bạn có muốn xóa các chứng từ thanh toán này không?", "MSG_DEL01", "", true) == "Yes")
                    {
                        foreach (Janus.Windows.GridEX.GridEXSelectedItem i in items)
                        {
                            if (i.RowType == Janus.Windows.GridEX.RowType.Record)
                            {
                                //Get chung tu thanh toan
                                CTTT_ChungTu cttt = (CTTT_ChungTu)i.GetRow().DataRow;

                                //Delete chung tu thanh toan chi tiet
                                if (cttt.ChungTuChiTietCollections == null)
                                    cttt.LoadChungTuChiTiet();

                                foreach (CTTT_ChungTuChiTiet item in cttt.ChungTuChiTietCollections)
                                {
                                    item.Delete();
                                }

                                //Delete chung tu thanh toan
                                cttt.Delete();
                            }
                        }
                    }
                }

                //Reload data
                dgList.DataSource = CTTT_ChungTu.SelectCollectionAll();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void FormQuanLyChungTuThanhToan_Load(object sender, EventArgs e)
        {
            //if (IsOpenFromChayThanhLy)
            this.ContextMenuStrip = null;
            btnExportExcel.Visible = true;
            txtNamChungTu.Value = DateTime.Now.Year;
            txtNamChungTu.Text = DateTime.Now.Year.ToString();
            txtNamDKToKhai.Value = DateTime.Now.Year;
            txtNamDKToKhai.Text = DateTime.Now.Year.ToString();
            cmbLoaiChungTu.SelectedIndex = 0;
            //else
            //    this.ContextMenuStrip = contextMenuStrip1;

            //btnXemBangTongHopCTTT.Enabled = !IsOpenFromChayThanhLy;

            //LoadData(string.Empty);
            Seach();

        }

        private void LoadData(string where)
        {
            try
            {
                //if (HSTL.LanThanhLy > 0 && IsOpenFromChayThanhLy && !string.IsNullOrEmpty(where))
                //{
                //    dgList.DataSource = CTTT_ChungTu.SelectCollectionDynamic(where, "NgayChungTu");
                //}
                //else
                //    dgList.DataSource = CTTT_ChungTu.SelectCollectionAll();

                dgList.DataSource = CTTT_ChungTu.SelectCollectionDynamic(where, "NgayChungTu");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void Seach()
        {
            string where = "(1=1) ";
            if (System.Convert.ToDecimal(txtSoToKhai.Value) > 0)
            {
                decimal SoToKhai = System.Convert.ToDecimal(txtSoToKhai.Value);
                string loaiHinh = txtMaLoaiHinh.Text.ToString();
                int NamDKtoKhai = System.Convert.ToInt16(txtNamDKToKhai.Value);
                string sqlwhere = "";
                if (txtSoToKhai.Text.Length >= 6)
                {
                    sqlwhere = string.Format(@"select IDCT from t_CTTT_ChungTuChiTiet where SoToKhai = (Select top 1 SoTK  from t_VNACCS_CapSoToKhai where SoTKVNACCS like '%{0}%') ", SoToKhai);
                }
                else
                    sqlwhere = string.Format(@"select IDCT from t_CTTT_ChungTuChiTiet where SoToKhai = {0} ", SoToKhai);
                if (!string.IsNullOrEmpty(loaiHinh))
                    sqlwhere += string.Format(@" and MaLoaiHinh like '%{0}%'", loaiHinh);
                if(NamDKtoKhai > 0)
                    sqlwhere += string.Format(@" and year(NgayDangKy) = {0}", NamDKtoKhai);
                where += "AND ID in (" + sqlwhere + ")";
            }
            else
            {
                if (txtNamChungTu.Value != null)
                {
                    int NamChungTu = System.Convert.ToInt16(txtNamChungTu.Value);
                    if (NamChungTu > 0)
                    {
                        where += "AND year(NgayChungTu) = " + NamChungTu;
                    }
                }
                where += string.Format("AND LoaiChungTu = '{0}'", cmbLoaiChungTu.SelectedValue.ToString());

                if (chkHienThiSaiLech.Checked)
                {
                    where += "AND ConLai != 0"; 
                }
            }
            LoadData(where);
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                CTTT_ChungTu cttt = (CTTT_ChungTu)e.Row.DataRow;

                //Truong hop mo thong tin chung tu thanh toan tu Form Quan ly CTTT => thong tin HSTL khong co, can phai load thong tin HSTL
                //if (cttt.LanThanhLy > 0)
                //{
                //    //Load thong tin ho so thanh ly (Neu co)
                //    HSTL = new HoSoThanhLyDangKy();
                //    HSTL.LanThanhLy = cttt.LanThanhLy;
                //    HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //    //HSTL.GetIdByLanThanhLy(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                //    HSTL.GetIdByLanThanhLy();
                //    HSTL = HoSoThanhLyDangKy.Load(HSTL.ID);

                //    if (HSTL == null) HSTL = new HoSoThanhLyDangKy();
                //    HSTL.LoadBKCollection();

                //    //Kiem tra load bang ke to khai nhap/ xuat Neu chua co
                //    foreach (BangKeHoSoThanhLy bk in this.HSTL.BKCollection)
                //    {
                //        if ((bk.TenBangKe == "DTLTKN" && bk.bkTKNCollection.Count == 0)
                //            || (bk.TenBangKe == "DTLTKX" && bk.bkTKNCollection.Count == 0))
                //        {
                //            bk.LoadChiTietBangKe();
                //        }
                //    }
                //}
                //else
                //    HSTL = new HoSoThanhLyDangKy();

                FrmChungTuThanhToanChiTiet frm = new FrmChungTuThanhToanChiTiet();
                //frm.HSTL = HSTL;
                //frm.IsOpenFromChayThanhLy = IsOpenFromChayThanhLy;
                cttt.LoadChiPhiKhac();
                cttt.LoadChungTuChiTiet();
                frm.ChungTuThanhToan = cttt;
                frm.LoaiChungTu = cttt.LoaiChungTu;
                frm.ShowDialog(this);
                Seach();
                //LoadData(string.Empty);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { Cursor = Cursors.Default; }
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            LoadData(string.Empty);
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {
            //Lay thong tin to khai mau dich duoc chon tren luoi
            if (dgList.SelectedItems.Count > 0)
            {
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                    {
                        objCTTT = (CTTT_ChungTu)grItem.GetRow().DataRow;

                        if (objCTTT != null)
                        {
                            //Hien thi menu neu Lan thanh ly > 0
                            mniXemCTTT.Enabled = (objCTTT.LanThanhLy > 0) && !IsOpenFromChayThanhLy;
                        }
                    }
                }
            }
        }

        void mniXemCTTT_Click(object sender, EventArgs e)
        {
            XemBangTongHopCTTT();
        }

        private void XemBangTongHopCTTT()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                //Truong hop mo thong tin chung tu thanh toan tu Form Quan ly CTTT => thong tin HSTL khong co, can phai load thong tin HSTL
                if (objCTTT != null && objCTTT.LanThanhLy > 0)
                {
                    //Load thong tin ho so thanh ly (Neu co)
                    HSTL = new HoSoThanhLyDangKy();
                    HSTL.LanThanhLy = objCTTT.LanThanhLy;
                    HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    HSTL.GetIdByLanThanhLy();
                    HSTL = HoSoThanhLyDangKy.Load(HSTL.ID);

                    if (HSTL == null) HSTL = new HoSoThanhLyDangKy();
                    HSTL.LoadBKCollection();

                    //Kiem tra load bang ke to khai nhap/ xuat Neu chua co
                    foreach (BangKeHoSoThanhLy bk in this.HSTL.BKCollection)
                    {
                        if ((bk.TenBangKe == "DTLTKN" && bk.bkTKNCollection.Count == 0)
                            || (bk.TenBangKe == "DTLTKX" && bk.bkTKNCollection.Count == 0))
                        {
                            bk.LoadChiTietBangKe();
                        }
                    }

                    ChungTuThanhToanForm_Modified f = new ChungTuThanhToanForm_Modified();
                    f.LoaiChungTu = cmbLoaiChungTu.SelectedValue.ToString();
                    f.HSTL = HSTL;
                    f.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnXemBangTongHopCTTT_Click(object sender, EventArgs e)
        {
            //Lay thong tin to khai mau dich duoc chon tren luoi
            if (dgList.SelectedItems.Count > 0)
            {
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    if (grItem.RowType == RowType.Record)
                        objCTTT = (CTTT_ChungTu)grItem.GetRow().DataRow;
                }
            }

            XemBangTongHopCTTT();
        }

        private void xemBangTongHopChungTuThanhToan1_XemBangTongHopClick(object sender, EventArgs e)
        {
            try
            {
                if (xemBangTongHopChungTuThanhToan1.HoSoThanhLyDangKy != null && xemBangTongHopChungTuThanhToan1.HoSoThanhLyDangKy.ID > 0)
            {
                //Load thong tin ho so thanh ly (Neu co)
                //HSTL = new HoSoThanhLyDangKy();
                //HSTL.LanThanhLy = objCTTT.LanThanhLy;
                //HSTL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //HSTL.GetIdByLanThanhLy();
                HSTL = HoSoThanhLyDangKy.Load(xemBangTongHopChungTuThanhToan1.HoSoThanhLyDangKy.ID);

                if (HSTL == null) HSTL = new HoSoThanhLyDangKy();
                HSTL.LoadBKCollection();

                //Kiem tra load bang ke to khai nhap/ xuat Neu chua co
                foreach (BangKeHoSoThanhLy bk in this.HSTL.BKCollection)
                {
                    if ((bk.TenBangKe == "DTLTKN" && bk.bkTKNCollection.Count == 0)
                        || (bk.TenBangKe == "DTLTKX" && bk.bkTKNCollection.Count == 0))
                    {
                        bk.LoadChiTietBangKe();
                    }
                }

                ChungTuThanhToanForm_Modified f = new ChungTuThanhToanForm_Modified();
                f.HSTL = HSTL;
                f.LoaiChungTu = cmbLoaiChungTu.SelectedValue.ToString();
                f.ShowDialog();
            }
                else
                {
                    ShowMessage("Bạn chưa chọn hồ sơ thanh lý đ tổng hợp chứng từ thanh toán",false);
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
           
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (System.Convert.ToDecimal(e.Row.Cells["ConLai"].Value) > 0)
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Black;
                    e.Row.RowStyle.BackColor = Color.Yellow;
                }
                else if (System.Convert.ToDecimal(e.Row.Cells["ConLai"].Value) < 0)
                {
                    e.Row.RowStyle = new GridEXFormatStyle();
                    e.Row.RowStyle.ForeColor = Color.Red;
                }
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            Seach();
        }

        private void cmbLoaiChungTu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLoaiChungTu.SelectedValue.ToString() == enumLoaiChungTuTT.Nhap)
                txtMaLoaiHinh.Text = "E31";
            else
                txtMaLoaiHinh.Text = "E62";
               
        }
        private void btnExportExcel2_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "BangKeCTTT_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                sfNPL.ShowDialog(this);

                if (sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}
