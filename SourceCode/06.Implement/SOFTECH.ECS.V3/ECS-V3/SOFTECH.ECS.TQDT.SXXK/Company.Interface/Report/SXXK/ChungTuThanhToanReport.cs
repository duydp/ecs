using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SXXK;

namespace Company.Interface.Report.SXXK
{
    public partial class ChungTuThanhToanReport : DevExpress.XtraReports.UI.XtraReport
    {
        public int SoHSTK;
        public ChungTuThanhToanCollection CTCollection = new ChungTuThanhToanCollection();
        private int STT = 0;
        private string ToKhaiXuat = "";
        private DateTime NgayDangKyXuat = new DateTime(1900, 1, 1);
        public ChungTuThanhToanReport()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.DataSource = this.CTCollection;
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (SoHSTK > 0)
                lblSHSTK.Text = SoHSTK + "";
            lblToKhaiXuat.DataBindings.Add("Text", this.DataSource, "ToKhaiXuat");
            lblNgayDangKyXuat.DataBindings.Add("Text", this.DataSource, "NgayDangKyXuat","{0:dd/MM/yy}");
            lblSoNgayHopDong.DataBindings.Add("Text", this.DataSource, "SoNgayHopDong");
            lblMaHangXuat.DataBindings.Add("Text", this.DataSource, "MaHangXuat");
            lblTriGiaHopDong.DataBindings.Add("Text", this.DataSource, "TriGiaHopDong","{0:N2}");
            lblTriGiaHangThucXuat.DataBindings.Add("Text", this.DataSource, "TriGiaHangThucXuat", "{0:N2}");
            lblSoNgayChungTu.DataBindings.Add("Text", this.DataSource, "SoNgayChungTu");
            lblTriGiaCT.DataBindings.Add("Text", this.DataSource, "TriGiaCT", "{0:N2}");
            lblHinhThucThanhToan.DataBindings.Add("Text", this.DataSource, "HinhThucThanhToan");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, "GhiChu");
            xrLabel76.Text = xrLabel30.Text = GlobalSettings.TieudeNgay;
            
        }
        private bool KiemTraTrung()
        {
            if (ToKhaiXuat == GetCurrentColumnValue("ToKhaiXuat").ToString() && NgayDangKyXuat == Convert.ToDateTime(GetCurrentColumnValue("NgayDangKyXuat"))) return true;
            else return false;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (!KiemTraTrung())
            {
                STT++;
                lblSTT.Text = STT + "";
            }
            else
            {
                lblSTT.Text = "";
            }
            
        }

        private void lblToKhaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (KiemTraTrung())
            {
                XRControl control = (XRControl)sender;
                control.Text = "";   
            }
            
        }

        private void Detail_AfterPrint(object sender, EventArgs e)
        {
            ToKhaiXuat = GetCurrentColumnValue("ToKhaiXuat").ToString();
            NgayDangKyXuat = Convert.ToDateTime(GetCurrentColumnValue("NgayDangKyXuat"));

        }

    }
}
