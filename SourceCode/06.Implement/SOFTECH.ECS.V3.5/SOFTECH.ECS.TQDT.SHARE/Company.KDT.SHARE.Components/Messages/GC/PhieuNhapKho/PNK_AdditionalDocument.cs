﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho
{
   public  class PNK_AdditionalDocument
    {
        /// <summary>
        /// Số thứ tự 
        /// </summary>
        [XmlElement("sequence")]
        public string Sequence { get; set; }

        /// <summary>
        /// Số phiếu nhập
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }

        /// <summary>
        /// Ngày 
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Tên người giao hàng
        /// </summary>
        [XmlElement("nameConsignor")]
        public string NameConsignor { get; set; }

        /// <summary>
        /// Mã người giao hàng
        /// </summary>
        [XmlElement("identityConsignor")]
        public string IdentityConsignor { get; set; }

        /// <summary>
        /// Tên người giao hàng
        /// </summary>
        [XmlElement("nameConsignee")]
        public string NameConsignee { get; set; }

        /// <summary>
        /// Mã người giao hàng
        /// </summary>
        [XmlElement("identityConsignee")]
        public string IdentityConsignee { get; set; }



        /// <summary>
        /// Phiếu kho
        /// </summary>
        [XmlElement("CustomsGoodsItem")]
        public List<PNK_CustomsGoodsItem> CustomsGoodsItem { get; set; }

        /// <summary>
        /// Thông tin về chứng từ khác
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public PNK_AdditionalInformation AdditionalInformation { get; set; }
    }
}
