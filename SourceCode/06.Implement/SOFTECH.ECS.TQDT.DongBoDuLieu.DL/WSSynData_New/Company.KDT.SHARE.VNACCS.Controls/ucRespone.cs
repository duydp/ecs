﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using System.Net;
using System.IO;
using DevExpress.XtraGrid.Views.Card;
using Company.KDT.SHARE.Components;
using SignRemote;
using System.Threading;

namespace Company.KDT.SHARE.VNACCS.Controls
{
    public partial class ucRespone : DevExpress.XtraEditors.XtraUserControl
    {
        public string MA_DON_VI { get; set; }
        public string SERVER_NAME { get; set; }
        public string USER { get; set; }
        public string PASS { get; set; }
        public bool isError { get; set; }
        public bool ShowAll { get; set; }

        public ucRespone()
        {
            InitializeComponent();
        }

        public event EventHandler<ucResponeEventArgs> UcEvent;
        public event EventHandler<ucResponeEventArgs> HandlerID;
        public event EventHandler<ucResponeEventArgs> HandlerXuLyMSG;
        private void XuLyMsg(object DataTKMD, string status)
        {
            HandlerXuLyMSG(this, new ucResponeEventArgs(DataTKMD, status)); 
        }
        private void Setstatus(string exception, bool isError, bool isfinish, ETrangThaiPhanHoi TrangThai)
        {
           /* if (!string.IsNullOrEmpty(exception))*/
                UcEvent(this, new ucResponeEventArgs(exception, isError, isfinish, TrangThai));
        }
        private void HandlerMsgID(long ID, string MaNghiepVu)
        {
            HandlerID(this, new ucResponeEventArgs(MaNghiepVu, 0, null, ID));
        }
        #region Flag property
        public bool isAutoRequest;
        public bool isStop;
        //private int TimerRequestContinue = 0;
        //private int ThoiGianConLai = 0;
        #endregion

        public string PostMessages(string url, string msg)
        {
            MIME mess = new MIME();
            System.Security.Cryptography.X509Certificates.X509Certificate2 x509Certificate2 = null;
            if (Company.KDT.SHARE.Components.Globals.IsSignature)
            {
                try
                {
                    if (!string.IsNullOrEmpty(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName))
                    {
                        x509Certificate2 = Company.KDT.SHARE.Components.Cryptography.GetStoreX509Certificate2New(Company.KDT.SHARE.Components.Globals.GetX509CertificatedName); 
                    }
                    else
                    {
                        Setstatus("Chưa cấu hình chữ ký số", true, true,ETrangThaiPhanHoi.Error);
                        return "E";
                    }
                }
                catch (System.Exception ex)
                {
                    Setstatus("Lỗi load chữ ký số: " + ex.Message, true, true, ETrangThaiPhanHoi.Error);
                    return "E";
                }
                if (x509Certificate2 == null)
                {
                    Setstatus("Không tìm thấy chữ ký số ", true, true, ETrangThaiPhanHoi.Error);
                    return "E";
                }
                mess = HelperVNACCS.GetMIME(msg, x509Certificate2);
            }
            else if (Company.KDT.SHARE.Components.Globals.IsSignOnLan)
            {
                mess = HelperVNACCS.GetMIME(msg, GetSignatureOnLan(msg));
            }
            else if (Company.KDT.SHARE.Components.Globals.IsSignRemote)
            {
                mess = HelperVNACCS.GetMIME(msg, GetSignatureRemote(msg));
            }
            else
            {
                Setstatus("Chưa cấu hình chữ ký số", true, true, ETrangThaiPhanHoi.Error);
                return "E";
            }
           
            string boundary = mess.boundary;
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            if (string.IsNullOrEmpty(boundary))
            {
                Setstatus("Boundary bị rỗng", true, true, ETrangThaiPhanHoi.Error);
                return "E";
            }

            boundary = boundary.Substring(2, boundary.Length - 2);
            httpWebRequest.ContentType = "multipart/signed; micalg = \"sha1\";boundary=\"" + boundary + "\";protocol=\"application/x-pkcs7-signature\"";
            httpWebRequest.Method = "POST";
            httpWebRequest.KeepAlive = false;
            httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
            httpWebRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
            httpWebRequest.UserAgent = "SOFTECH";
            httpWebRequest.ProtocolVersion = HttpVersion.Version11;
            Stream memStream = new System.IO.MemoryStream();

            byte[] data = UTF8Encoding.UTF8.GetBytes(mess.GetMessages().ToString());
            memStream.Write(data, 0, data.Length);


            httpWebRequest.ContentLength = memStream.Length;
            Stream requestStream = httpWebRequest.GetRequestStream();
            memStream.Position = 0;
            byte[] tempBuffer = new byte[memStream.Length];
            memStream.Read(tempBuffer, 0, tempBuffer.Length);
            memStream.Close();
            requestStream.Write(tempBuffer, 0, tempBuffer.Length);
            requestStream.Close();
            try
            {
                WebResponse webResponse = httpWebRequest.GetResponse();
                Stream stream = webResponse.GetResponseStream();
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                string var = reader.ReadToEnd();
                if (var != EnumThongBao.KetThucPhanHoi)
                {
                    //MsgLog.SaveMessages(var, this.messagesSend.Header.MaNghiepVu.GetValue().ToString() + " Return", 0, EnumThongBao.ReturnMess, "", this.messagesSend.Header.MessagesTag.GetValue().ToString(), this.messagesSend.Header.InputMessagesID.GetValue().ToString(), this.messagesSend.Header.IndexTag.GetValue().ToString());
                    ResponseVNACCS feedback = new ResponseVNACCS(var);
                    string msgFeedBack = feedback.GetError();
                    if (feedback.Error != null && feedback.Error.Count > 0)
                    {
                        Setstatus(msgFeedBack, true, true, ETrangThaiPhanHoi.Error);
                        return "E";
                    }
                }
                return var;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Setstatus(ex.Message, true, true, ETrangThaiPhanHoi.Error);
                return "E";
            }
        }
        bool isWorking;
        public void DoWork(object obj)
        {
            isWorking = true;
            isError = false;
            //SetStatusImage(true);
            Setstatus("Đang nhận phản hồi từ HQ", false, false, ETrangThaiPhanHoi.Download);
            string msgError = string.Empty;
            try
            {
                A2 msgA2 = new A2();
                if (!string.IsNullOrEmpty(msgA2.RTPTag.GetValue().ToString().Trim()))
                {
                    string ResponseA2 = PostMessages(GlobalVNACC.Url, msgA2.A2BuidEdiMessages(new StringBuilder()).ToString());

                    if (ResponseA2 == "E")
                        isError = true;
                    else if (ProcessMSG(ResponseA2) || ResponseA2.Contains(EnumThongBao.KetThucPhanHoi))
                    {
                        if (ResponseA2.Contains(EnumThongBao.KetThucPhanHoi))
                        {
                            StartRequest();
                        }
                        else
                        {
                            ContinueRequest();
                        }
                    }
                }
                else
                {
                    StartRequest();
                }
                Setstatus("", isError, true, isError ? ETrangThaiPhanHoi.Error : ETrangThaiPhanHoi.Stop);
                GlobalVNACC.isResponding = false;
                isWorking = false;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                msgError = ex.Message;
                isError = true;
                Setstatus(ex.Message, isError, true, ETrangThaiPhanHoi.Error);
                isWorking = false;
                GlobalVNACC.isResponding = false;
            }
        }

        public void ContinueRequest()
        {
            bool isContinue = true;
            while (isContinue && !isStop && !GlobalVNACC.isStopRespone)
            {
                GlobalVNACC.isResponding = true;
                A2 msgA2 = new A2();
                string ResponseA2 = PostMessages(GlobalVNACC.Url, msgA2.A2BuidEdiMessages(new StringBuilder()).ToString());
                if (ResponseA2 == "E")
                {
                    isError = true;
                    return;
                }
                else
                    isContinue = ProcessMSG(ResponseA2);
            }


        }
        public void StartRequest()
        {
            try
            {
            
            GlobalVNACC.isResponding = true;
            Rep1 msgRep1 = new Rep1(GlobalVNACC.TerminalID, GlobalVNACC.TerminalAccessKey);
            MessagesSend msgSendRep = MessagesSend.Load(msgRep1);
            string postMessages = PostMessages(GlobalVNACC.Url, HelperVNACCS.BuildEdiMessages(msgSendRep).ToString()); // msgRep1.BuildEdiMessages<Rep1>(new StringBuilder(), true, GlobalVNACC.PathConfig, "Rep1").ToString());
            if (postMessages == "E")
            {
                isError = true;
                return;
            }
            else
            {
                if (ProcessMSG(postMessages))
                    ContinueRequest();
                else
                    return;
            }
            }
            catch (System.Exception ex)
            {
                isError = true;
                throw ex;
                
            }

        }
        public bool ProcessMSG(string MsgFeedBack)
        {
            MsgLog log = new MsgLog();
            try
            {

                if (!string.IsNullOrEmpty(MsgFeedBack) && !MsgFeedBack.Contains(EnumThongBao.KetThucPhanHoi))
                {
                    #region log

                    log.Log_Messages = MsgFeedBack;
                    log.MaNghiepVu = "Return";
                    log.MessagesTag = "";
                    log.NoiDungThongBao = "";
                    log.TieuDeThongBao = "Thông tin phản hồi từ HQ";
                    log.InputMessagesID = "";
                    log.Item_id = 0;
                    log.IndexTag = "";
                    log.CreatedTime = DateTime.Now;
                    #endregion

                    ReturnMessages msgReturn = new ReturnMessages(MsgFeedBack);
                    if (msgReturn == null)
                    {
                        long idLog = log.Insert();
                        if (idLog == 0)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(new Exception("Không lưu được messages phản hồi"));
                            return false;
                        }
                        Logger.LocalLogger.Instance().WriteMessage(new Exception("Không xử lý được messages có ID = " + idLog));
                        return false;
                    }
                    #region Lưu msg chờ xử lý

                    MsgPhanBo msgPB = new MsgPhanBo();
                    IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic("RTPTag = '" + msgReturn.GetRTPtag().Trim() + "'", null);
                    if (listPB != null && listPB.Count > 0)
                    {
                        msgPB = listPB[0];
                        msgPB.SoTiepNhan = msgReturn.GetSoTNHeader();
                        msgPB.MessagesInputID = msgReturn.header.InputMessagesID.GetValue().ToString();
                        msgPB.messagesTag = msgReturn.header.MessagesTag.GetValue().ToString();
                        msgPB.TerminalID = msgReturn.header.TerminalID.GetValue().ToString();
                        msgPB.Update();
                    }
                    else
                    {
                        long idLog = log.Insert();
                        if (idLog == 0)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(new Exception("Không lưu được messages phản hồi"));
                            return false;
                        }
                        msgPB.Master_ID = idLog;
                        msgPB.CreatedTime = DateTime.Now;
                        msgPB.SoTiepNhan = msgReturn.GetSoTNHeader();
                        msgPB.RTPTag = msgReturn.GetRTPtag();
                        msgPB.MessagesInputID = msgReturn.header.InputMessagesID.GetValue().ToString();
                        msgPB.messagesTag = msgReturn.header.MessagesTag.GetValue().ToString();
                        msgPB.TerminalID = msgReturn.header.TerminalID.GetValue().ToString();
                        msgPB.IndexTag = msgReturn.header.IndexTag.GetValue().ToString();
                        msgPB.TrangThai = EnumTrangThaiXuLyMessage.ChuaXuLy;
                        KDT_VNACC_ToKhaiMauDich TKMD = ProcessMessages.XuLyMsg(msgReturn);
                        if (TKMD != null)
                        {
                            //msgPB.TrangThai = EnumTrangThaiXuLyMessage.DaXem;
                            if (TKMD.TrangThaiXuLy.Contains("3") || TKMD.TrangThaiXuLy.Contains("2")) //Thong quan
                            {
                                //ProcessMessages.XuLyMsg(msgReturn);
                                XuLyMsg(TKMD, "TKMD");
                                
                            }
                        }
                        msgPB.MaNghiepVu = msgReturn.getMaNVPhanHoi();
                        msgPB.Insert();
                    }
                    #endregion

                    #region hiển thị thông báo
                    string ShowPopUpContent = ReturnMessages.GetThongBaoPhanHoi(msgPB.MaNghiepVu);
                    ShowPopUpContent += Environment.NewLine + "Số tờ khai: " + msgPB.SoTiepNhan;
                    ShowPopUpContent += Environment.NewLine + DateTime.Now.ToString("dd/MM/yyyy");
                    if (this.InvokeRequired)
                        this.Invoke(new MethodInvoker(delegate { bindingThongBao(); PopUpThongBao(ShowPopUpContent); }));
                    else
                    {
                        bindingThongBao();
                        PopUpThongBao(ShowPopUpContent);
                    }

                    #endregion

                    return true;
                }
                else
                    return false;
            }
            catch (System.Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
                if (log.ID > 0) Logger.LocalLogger.Instance().WriteMessage(new Exception("Không xử lý được messages có ID = " + log.ID));
                isError = true;
                throw ex;
            }
        }
        public string InputMsgID { get; set; }
        public string SoToKhai { get; set; }
        public void bindingThongBao()
        {
            try
            {
                List<MsgPhanBo> listPhanBo = new List<MsgPhanBo>();
                if (string.IsNullOrEmpty(InputMsgID))
                    listPhanBo = MsgPhanBo.GetMsgPhanBoChuaXuLy(GlobalVNACC.TerminalID);
                else
                    listPhanBo = MsgPhanBo.GetMsgPhanBoChuaXuLyInput(InputMsgID, SoToKhai);

                if (listPhanBo != null && listPhanBo.Count > 0)
                {
                    DataTable dtThongBao = new DataTable();
                    dtThongBao.Columns.Add("Master_ID", typeof(long));
                    dtThongBao.Columns.Add("NoiDung", typeof(string));
                    dtThongBao.Columns.Add("SoTiepNhan", typeof(string));
                    dtThongBao.Columns.Add("NgayTB", typeof(DateTime));
                    dtThongBao.Columns.Add("PhanLoai", typeof(string));
                    dtThongBao.Columns.Add("PhanLuong", typeof(string));
                    dtThongBao.Columns.Add("MaNghiepVu", typeof(string));
                    foreach (MsgPhanBo item in listPhanBo)
                    {
                        DataRow dr = dtThongBao.NewRow();
                        dr["NoiDung"] = ReturnMessages.GetThongBaoPhanHoi(item.MaNghiepVu);
                        dr["SoTiepNhan"] = item.SoTiepNhan;
                        dr["NgayTB"] = item.CreatedTime;
                        dr["PhanLoai"] = string.Empty;
                        dr["MaNghiepVu"] = item.MaNghiepVu;
                        if (KDT.SHARE.VNACCS.ClassVNACC.ProcessMessages.CheckInGroup(item.MaNghiepVu, EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiNhap_BanXacNhan) || KDT.SHARE.VNACCS.ClassVNACC.ProcessMessages.CheckInGroup(item.MaNghiepVu, EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiNhap_BanChapNhanThongQuan) || KDT.SHARE.VNACCS.ClassVNACC.ProcessMessages.CheckInGroup(item.MaNghiepVu, EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiXuat_BanXacNhan) || KDT.SHARE.VNACCS.ClassVNACC.ProcessMessages.CheckInGroup(item.MaNghiepVu, EnumNghiepVuPhanHoi.GroupPhanHoi_ToKhaiXuat_BanChapNhanThongQuan))
                        {
                            dr["PhanLuong"] = item.MaNghiepVu.Substring(3, 1);
                        }
                        dr["Master_ID"] = item.Master_ID;
                        dtThongBao.Rows.Add(dr);
                    }
                    //cardView1.FormatConditions
                    gridControl2.DataSource = dtThongBao;
                    gridControl2.Refresh();
                }
            }
            catch (System.Exception ex)
            {
                Setstatus(ex.Message, false, true, ETrangThaiPhanHoi.Error);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void PopUpThongBao(string contentThongBao)
        {
            popupNotifier1.TitleText = "Thông báo từ Hải Quan";
            popupNotifier1.ContentText = contentThongBao;
            popupNotifier1.ShowCloseButton = true;
            popupNotifier1.ShowOptionsButton = false;
            popupNotifier1.ShowGrip = false;
            popupNotifier1.Delay = 3000;
            popupNotifier1.AnimationInterval = 10;
            popupNotifier1.AnimationDuration = 1000;
            popupNotifier1.Popup();
            
        }
        private void ucRespone_Load(object sender, EventArgs e)
        {
            //if (!this.DesignMode)
            //    bindingThongBao();
        }

        private void cardView1_CustomDrawCardCaption(object sender, DevExpress.XtraGrid.Views.Card.CardCaptionCustomDrawEventArgs e)
        {
            DataTable dt = gridControl2.DataSource as DataTable;
            switch (dt.Rows[e.RowHandle]["PhanLuong"].ToString())
            {
                case "1":
                    e.Appearance.BackColor = Color.Green;
                    e.Appearance.BackColor2 = Color.LightGreen;
                    break;
                case "2":
                    e.Appearance.BackColor = Color.Yellow;
                    e.Appearance.BackColor2 = Color.LightGoldenrodYellow;
                    //e.Appearance.ForeColor = Color.Black;
                    break;
                case "3":
                    e.Appearance.BackColor = Color.Red;
                    e.Appearance.BackColor2 = Color.IndianRed;
                    break;
            }
        }

        private void cardView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                if (!isWorking)
                {
                    CardView card = (CardView)sender;
                    int indexrow = card.GetSelectedRows()[0];
                    DataTable dt = gridControl2.DataSource as DataTable;
                    long msgID = Convert.ToInt64(dt.Rows[indexrow]["Master_ID"]);
                    string MaNV = dt.Rows[indexrow]["MaNghiepVu"].ToString();
                    HandlerMsgID(msgID, MaNV);
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

 

        }


        #region ký số từ xa
        private int CountRequest(decimal len)
        {
            int timeRequest = 0;
            Random rnd1 = new Random();

            if (len < 1024)
                timeRequest = rnd1.Next(3, 7);
            else if (len > 1024 & len < 1048576)
                timeRequest = rnd1.Next(5, 9);
            else if (len > 1048576 & len < 107341824)
                timeRequest = rnd1.Next(10, 20);
            else timeRequest = rnd1.Next(30, 60);
            return timeRequest;
        }
        private string GetSignatureRemote(string ediMess)
        {
            ISignMessage signRemote = WebService.SignMessage();
            string guidStr = Guid.NewGuid().ToString();
            MessageSend msgSend = new MessageSend()
            {
                From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
                Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
                SendStatus = SendStatusInfo.SendVNACCS,
                Message = new SignRemote.Message()
                {
                    Content = Helpers.ConvertToBase64VNACCS(ediMess),
                    GuidStr = guidStr
                },
                To = MA_DON_VI.Trim()
            };

            string msg = Helpers.Serializer(msgSend, true, true);
            //setCaption("Gửi ký thông tin");
            string msgRet = signRemote.ClientSend(msg);
            msgSend = Helpers.Deserialize<MessageSend>(msgRet);
            if (msgSend.SendStatus == SendStatusInfo.Error)
            {
                throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
            }
            MessageSend msgFeedBack = new MessageSend()
            {
                From = Company.KDT.SHARE.Components.Globals.UserNameSignRemote,
                Password = Company.KDT.SHARE.Components.Globals.PasswordSignRemote,
                SendStatus = SendStatusInfo.Request,
                Message = new SignRemote.Message()
                {
                    GuidStr = guidStr
                },
                To = MA_DON_VI.Trim()
            };
            msg = Helpers.Serializer(msgFeedBack, true, true);

            bool isWait = true;
            int countSend = 1;
            int countRequest = CountRequest(msg.Length);
            while (isWait && countSend < 5)
            {
                int count = countRequest;
                //string title = this._title;
                string title = string.Format("Chờ phản hồi ký số lần thứ {0}", countSend);
                while (count > 0)
                {
                    //setCaption(string.Format(title + ": {0} giây", count));
                    Thread.Sleep(1000);
                    count--;
                }
                //this._title = title;
                msgRet = signRemote.ClientSend(msg);
                msgSend = Helpers.Deserialize<MessageSend>(msgRet);
                if (msgSend.SendStatus == SendStatusInfo.Error)
                {
                    throw new Exception("Thông báo từ hệ thống: " + msgSend.Warrning);
                }
                else if (msgSend.SendStatus == SendStatusInfo.Sign)
                {
                    return msgSend.Message.SignData;
                }
                else if (msgSend.SendStatus == SendStatusInfo.Wait)
                    countSend++;
            }
            throw new Exception(string.Format("ID={0}. Ký số từ xa thất bại\nMáy chủ đã không phản hồi thông tin trong thời gian giao dịch", guidStr));
        }
        private string GetSignatureOnLan(string content)
        {
            //try
            //{
                string guidStr = Guid.NewGuid().ToString();
                string cnnString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { SERVER_NAME, Company.KDT.SHARE.Components.Globals.DataSignLan, USER, PASS });
                string MSG_FROM = System.Environment.MachineName.ToString();
                INBOX.InsertSignContent(cnnString, guidStr, Helpers.ConvertToBase64VNACCS(content), SendStatusInfo.SendVNACCS,MSG_FROM);
                int count = 0;
                int timeWait = 15;
                while (count < timeWait)
                {
                    if (Company.KDT.SHARE.Components.Globals.MA_DON_VI !="4000395355")
                    {
                        OUTBOX outbox = OUTBOX.GetContentSignLocal(cnnString, guidStr);
                        if (outbox != null && outbox.MSG_SIGN_DATA != null)
                        {
                            return outbox.MSG_SIGN_DATA;
                        }
                        else
                        {
                            count++;
                            //setCaption(string.Format("Chờ phản hồi ký số : {0} giây", count));
                            Thread.Sleep(1000);
                        }
                    }
                    else
                    {
                        //OUTBOX outbox = OUTBOX.GetContentSignLocal(cnnString, guidStr);
                        List<OUTBOX> outBoxCollection = OUTBOX.GetCollectionContentSignLocal(cnnString, guidStr);
                        foreach (OUTBOX item in outBoxCollection)
                        {
                            if (item != null && item.MSG_SIGN_DATA != null)
                            {
                                return item.MSG_SIGN_DATA;
                            }
                            else
                            {
                                count++;
                                //setCaption(string.Format("Chờ phản hồi ký số : {0} giây", count));
                                Thread.Sleep(1000);
                            }
                        }
                    }
                }
                throw new Exception("Đã gửi yêu cầu ký để khai báo\r\nTuy nhiên máy chủ đã không ký thông tin yêu cầu\r\nVui lòng xem thiết lập chữ ký số trên máy chủ");
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    return null;
            //}


        }
        #endregion
    }

















    public class ucResponeEventArgs : EventArgs
    {
        public Exception Error { get; set; }
        public string Message { get; set; }
        public int Percent { get; set; }
        public object Data { get; set; }
        public bool isError { get; set; }
        public bool isFinish { get; set; }
        public bool isNewMsg { get; set; }
        public ETrangThaiPhanHoi TrangThaiPhanHoi { get; set; }
        public ucResponeEventArgs(string message, int percent, Exception ex, object data)
        {
            Message = message;
            Error = ex;
            Percent = percent;
            Data = data;
        }
        public ucResponeEventArgs(string message, int percent)
            : this(message, percent, null, null)
        {
        }
        public ucResponeEventArgs(Exception ex)
            : this(string.Empty, 0, ex, null)
        {

        }
        public ucResponeEventArgs(string error, bool isError, bool isfinish)
        {
            Message = error;
            this.isError = isError;
            this.isFinish = isfinish;
        }
        public ucResponeEventArgs(string error, bool isError, bool isfinish, ETrangThaiPhanHoi trangThai) :this(error, isError, isfinish, trangThai, false)
        {
            
        }
        public ucResponeEventArgs(string error, bool isError, bool isfinish, ETrangThaiPhanHoi trangThai, bool isnewMsg)
        {
            Message = error;
            this.isError = isError;
            this.isFinish = isfinish;
            TrangThaiPhanHoi = trangThai;
            this.isNewMsg = isnewMsg;
        }
        public ucResponeEventArgs(object _data,string status)
        {
            this.Data = _data;
            this.Message = status;

        }
    }
}
