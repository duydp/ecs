﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;

using System.Collections.Generic;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
namespace Company.Interface.GC
{
    public partial class HangMauDichRegisterForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        private decimal clg;
        private decimal dgnt;
        public Company.GC.BLL.KDT.GC.HopDong HD = new  Company.GC.BLL.KDT.GC.HopDong();
        public ToKhaiMauDich TKMD;
        private decimal luong;                
        private decimal st_clg;
        private bool edit = false;
        //-----------------------------------------------------------------------------------------				
        private decimal tgnt;
        private decimal tgtt_gtgt;

        private decimal tgtt_nk;
        private decimal tgtt_ttdb;
        private decimal tl_clg;
        private decimal tongTGKB;
        private decimal ts_gtgt;

        private decimal ts_nk;
        private decimal ts_ttdb;
        private decimal tt_gtgt;

        private decimal tt_nk;
        private decimal tt_ttdb;
        public decimal TyGiaTT;
  //      NguyenPhuLieuRegistedForm NPLRegistedForm;
//        SanPhamRegistedForm SPRegistedForm;
        private decimal LuongCon = 0;
        //-----------------------------------------------------------------------------------------

        public HangMauDichRegisterForm()
        {
            InitializeComponent();           
        }
        private void SetVisibleHTS(bool b)
        {
            txtMa_HTS.Visible = lblMa_HTS.Visible = txtSoLuong_HTS.Visible = lblSoLuong_HTS.Visible = cbbDVT_HTS.Visible = lblDVT_HTS.Visible = b;
            if (!b)
            {
                txtMaHS.Width = txtTenHang.Width = cbDonViTinh.Width = txtLuong.Width = txtMaHang.Width;
            }
        }
        private void khoitao_GiaoDien()
        {
            if (this.TKMD.MaLoaiHinh.StartsWith("NGC")) SetVisibleHTS(false);
            else
            {
                if (this.TKMD.LoaiHangHoa != "S" || TKMD.MaMid =="") SetVisibleHTS(false);
                label1.Text = setText("Lượng còn được xuất","Quantity can be exported");
            }
            if (this.TKMD.LoaiHangHoa == "N")
            {
                txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            }
            else
            {
                txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
            }
        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            cbbDVT_HTS.DataSource = this._DonViTinh;
            cbbDVT_HTS.SelectedValue = "15 ";
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
        }

        private decimal tinhthue2()
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Text);
            this.luong = Convert.ToDecimal(txtLuong.Text);
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;

            this.tgnt = this.dgnt * Convert.ToDecimal(this.luong);
            this.tgtt_nk = Convert.ToDecimal(txtTGTT_NK.Value);
            if (tgtt_nk == 0)
            {
                tgtt_nk = tgnt * TyGiaTT;
            }

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private decimal tinhthue()
        {
            this.tgnt = this.dgnt * Convert.ToDecimal(this.luong);
            this.tgtt_nk = this.tgnt * this.TyGiaTT;

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (hmd.MaPhu.Trim().ToUpper() == maHang.Trim().ToUpper()) return true;
            }
            return false;
        }
        
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            if (TKMD.MaLoaiHinh.StartsWith("N"))
            {
                if (PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap(TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.MaHaiQuan, (short)TKMD.NgayDangKy.Year,TKMD.IDHopDong))
                {
                    showMsg("MSG_ALL01");
                    //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                    return;
                }
            }
            else
            {
                if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                {
                    showMsg("MSG_ALL01");
                    //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                    return;
                }
            }
            KiemTraTonTaiHang();
            this.tinhthue();
            txtMaHang.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            //if (!MaHS.Validate(txtMaHS.Text, 10))
            //{
            //    epError.SetIconPadding(txtMaHS, -8);
            //    epError.SetError(txtMaHS, setText("Mã số HS không hợp lệ.", "This value is invalid"));
            //    return;
            //}

            if (checkMaHangExit(txtMaHang.Text))
            {
                
                showMsg("MSG_2702006");
                //ShowMessage("Mặt hàng này đã có rồi, bạn hãy chọn mặt hàng khác.", false);
                return;
            }
            
            HangMauDich hmd = new HangMauDich();
            hmd.SoThuTuHang = 0;
            hmd.MaHS = txtMaHS.Text;
            hmd.MaPhu = txtMaHang.Text;
            hmd.TenHang = txtTenHang.Text;
            hmd.NuocXX_ID = ctrNuocXX.Ma;
            hmd.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            hmd.SoLuong = Convert.ToDecimal(txtLuong.Value);
            hmd.DonGiaKB = Convert.ToDecimal(txtDGNT.Value);
            hmd.Ma_HTS = txtMa_HTS.Text;
            hmd.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
            hmd.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
            hmd.TriGiaKB = Convert.ToDecimal(txtTGNT.Value);
            hmd.TriGiaKB_VND = Convert.ToDecimal(txtTriGiaKB.Value);
            hmd.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Value);
            hmd.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Value);
            hmd.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Value);
            hmd.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Value);
            hmd.TyLeThuKhac = Convert.ToDecimal(txtTL_CLG.Value);
            hmd.ThueXNK = Math.Round(Convert.ToDecimal(txtTienThue_NK.Value), 0);
            hmd.ThueTTDB = Math.Round(Convert.ToDecimal(txtTienThue_TTDB.Value), 0);
            hmd.ThueGTGT = Math.Round(Convert.ToDecimal(txtTienThue_GTGT.Value), 0);
            hmd.DonGiaTT = hmd.TriGiaTT / Convert.ToDecimal(hmd.SoLuong);
            hmd.TriGiaThuKhac = Convert.ToDecimal(txtTien_CLG.Value);
            hmd.ThueSuatXNKGiam = txtTSXNKGiam.Text.Trim();
            hmd.ThueSuatTTDBGiam = txtTSTTDBGiam.Text.Trim();
            hmd.ThueSuatVATGiam = txtTSVatGiam.Text.Trim();
            if (chkMienThue.Checked)
                hmd.MienThue = 1;
            else
                hmd.MienThue = 0;
            this.TKMD.HMDCollection.Add(hmd);
            TinhTongTriGiaNT();
            reset();
        }
        private void reset()
        {
            try
            {
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
            txtMaHang.Text = "";
            txtTenHang.Text = "";
            txtMaHS.Text = "";
            txtLuong.Value = 0;
            txtDGNT.Value = 0;
            txtMa_HTS.Text = "";
            txtSoLuong_HTS.Value = 0;
            txtTGNT.Value = 0;
            txtTriGiaKB.Value = 0;
            
        }
        //-----------------------------------------------------------------------------------------
        private void txtMaNPL_Leave(object sender, EventArgs e)        
        {
            if (this.TKMD.LoaiHangHoa == "N")
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = txtMaHang.Text.Trim();
                if (npl.Load())
                {
                    txtMaHang.Text = npl.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = npl.MaHS;
                    txtTenHang.Text = npl.Ten;
                    cbDonViTinh.SelectedValue = npl.DVT_ID;
                    if (this.TKMD.MaLoaiHinh.StartsWith("NGC"))
                    {
                        LuongCon = npl.SoLuongDangKy - npl.SoLuongDaNhap-npl.SoLuongCungUng;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = npl.SoLuongDaNhap + npl.SoLuongCungUng - npl.SoLuongDaDung;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    epError.SetError(txtMaHang, null);
                    epError.SetError(txtTenHang, null);
                    epError.SetError(txtMaHS, null);
                    
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));
                    
                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";                   
                    return;
                }
            }
            else if (this.TKMD.LoaiHangHoa == "S")
            {
                SanPham sp = new SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = txtMaHang.Text.Trim();
                if (sp.Load())
                {
                    txtMaHang.Text = sp.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    cbDonViTinh.SelectedValue = sp.DVT_ID;
                    LuongCon = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                    txtSoLuongConDcNhap.Text = LuongCon.ToString();
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại sản phẩm này.", "This value is not exist"));

                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";                    
                    return;
                }
            }
            else if (this.TKMD.LoaiHangHoa == "T")
            {
                ThietBi tb = new ThietBi();
                tb.HopDong_ID = HD.ID;
                tb.Ma = txtMaHang.Text.Trim();
                if (tb.Load())
                {
                    txtMaHang.Text = tb.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = tb.MaHS;
                    txtTenHang.Text = tb.Ten;
                    cbDonViTinh.SelectedValue = tb.DVT_ID;
                    if (this.TKMD.MaLoaiHinh.StartsWith("NGC"))
                    {
                        LuongCon = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = tb.SoLuongDaNhap;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));

                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";                    
                    return;
                }
            }        
           
        }
        private void KiemTraTonTaiHang()
        {
            if (this.TKMD.LoaiHangHoa == "N")
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = txtMaHang.Text.Trim();
                if (npl.Load())
                {                    
                    epError.SetError(txtMaHang, null);
                    epError.SetError(txtTenHang, null);
                    epError.SetError(txtMaHS, null);

                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));
                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    return;
                }
            }
            else if (this.TKMD.LoaiHangHoa == "S")
            {
                SanPham sp = new SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = txtMaHang.Text.Trim();
                if (sp.Load())
                {
                    epError.SetError(txtMaHang, null);
                    epError.SetError(txtTenHang, null);
                    epError.SetError(txtMaHS, null);
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại sản phẩm này.", "This value is not exist"));
                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    return;
                }
            }
            else if (this.TKMD.LoaiHangHoa == "T")
            {
                ThietBi tb = new ThietBi();
                tb.HopDong_ID = HD.ID;
                tb.Ma = txtMaHang.Text.Trim();
                if (tb.Load())
                {
                    epError.SetError(txtMaHang, null);
                    epError.SetError(txtTenHang, null);
                    epError.SetError(txtMaHS, null);
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    return;
                }
            }

        }

        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();

            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.TKMD.NguyenTe_ID + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.TKMD.NguyenTe_ID);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.TKMD.NguyenTe_ID);

            this.tongTGKB = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {              
                this.tongTGKB += hmd.TriGiaKB;
            }
            dgList.DataSource = this.TKMD.HMDCollection;

            lblTongTGKB.Text = string.Format(setText("Tổng trị giá nguyên tệ:", "Total value in original currency") + " {0} ({1})", this.tongTGKB.ToString("N"), this.TKMD.NguyenTe_ID);
            lblTyGiaTT.Text = "Tỷ giá tính thuế: " + this.TyGiaTT.ToString("N");            
        }

        private void txtLuong_Leave(object sender, EventArgs e)        
        {            
            this.luong = Convert.ToDecimal(txtLuong.Value);
            LuongCon = Convert.ToDecimal(txtSoLuongConDcNhap.Text);
            txtSoLuong_HTS.Value = Math.Round(this.luong / 12);
            if (this.luong > LuongCon)
            {
                if (TKMD.MaLoaiHinh.Contains("N"))
                {
                    //if (!(MLMessages("Bạn đã nhập quá số lượng có thể nhập.Bạn có muốn tiếp tục không?", "MSG_WRN16", "", true) == "Yes"))
                    if (!(showMsg("MSG_WRN07", true) == "Yes"))
                    {
                        luong = 0;
                        txtLuong.Value = "0";
                    }
                }
                else
                {
                    //if (!(MLMessages("Bạn đã xuất quá số lượng có thể xuất.Bạn có muốn tiếp tục không?", "MSG_WRN16", "", true) == "Yes"))
                    if (!(showMsg("MSG_WRN08", true) == "Yes"))
                    {
                        luong = 0;
                        txtLuong.Value = "0";
                    }
                }
            }
            LuongCon = 0;
            this.tinhthue();
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Value);
            this.tinhthue();
        }
    

        //-----------------------------------------------------------------------------------------------       

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
        }
     
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            if (this.TKMD.LoaiHangHoa == "N")
            {                
                NguyenPhuLieuRegistedForm f2 = new NguyenPhuLieuRegistedForm();
                f2.NguyenPhuLieuSelected.HopDong_ID = this.HD.ID;
                f2.isBrower = true;
                f2.ShowDialog();
                if (f2.NguyenPhuLieuSelected.Ma != "")
                {
                    txtMaHang.Text = f2.NguyenPhuLieuSelected.Ma;
                    txtTenHang.Text = f2.NguyenPhuLieuSelected.Ten;
                    txtMaHS.Text = f2.NguyenPhuLieuSelected.MaHS;
                    cbDonViTinh.SelectedValue = f2.NguyenPhuLieuSelected.DVT_ID;
                    if (TKMD.MaLoaiHinh.StartsWith("NGC"))
                    {
                        LuongCon = f2.NguyenPhuLieuSelected.SoLuongDangKy - f2.NguyenPhuLieuSelected.SoLuongDaNhap - f2.NguyenPhuLieuSelected.SoLuongCungUng;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = f2.NguyenPhuLieuSelected.SoLuongDaNhap + f2.NguyenPhuLieuSelected.SoLuongCungUng - f2.NguyenPhuLieuSelected.SoLuongDaDung;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    epError.SetError(txtMaHang, null);
                    epError.SetError(txtTenHang, null);
                    epError.SetError(txtMaHS, null);
                }
            }
            else if (TKMD.LoaiHangHoa == "S")
            {              
                SanPhamRegistedForm f3 = new SanPhamRegistedForm();
                f3.isBrower = true;
                f3.SanPhamSelected.HopDong_ID = this.HD.ID;
                f3.ShowDialog();
                if (f3.SanPhamSelected.Ma != "")
                {
                    txtMaHang.Text = f3.SanPhamSelected.Ma;
                    txtTenHang.Text = f3.SanPhamSelected.Ten;
                    txtMaHS.Text = f3.SanPhamSelected.MaHS;
                    cbDonViTinh.SelectedValue = f3.SanPhamSelected.DVT_ID;
                    LuongCon = f3.SanPhamSelected.SoLuongDangKy - f3.SanPhamSelected.SoLuongDaXuat;
                    txtSoLuongConDcNhap.Text = LuongCon.ToString();
                }
            }
            else
            {                
                ThietBiRegistedForm ftb = new ThietBiRegistedForm();
                ftb.ThietBiSelected.HopDong_ID= this.HD.ID;
                ftb.isBrower = true;
                ftb.ShowDialog();
                if (ftb.ThietBiSelected.Ma != "")
                {
                    txtMaHang.Text = ftb.ThietBiSelected.Ma;
                    txtTenHang.Text = ftb.ThietBiSelected.Ten;
                    txtMaHS.Text = ftb.ThietBiSelected.MaHS;
                    cbDonViTinh.SelectedValue = ftb.ThietBiSelected.DVT_ID;
                    if (TKMD.MaLoaiHinh.StartsWith("NGC"))
                    {
                        LuongCon = ftb.ThietBiSelected.SoLuongDangKy - ftb.ThietBiSelected.SoLuongDaNhap;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = ftb.ThietBiSelected.SoLuongDaNhap;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                }
            }
        }
        private void TinhTongTriGiaNT()
        {
            this.tongTGKB = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                this.tongTGKB += hmd.TriGiaKB;
            }


            lblTongTGKB.Text = string.Format("Tổng trị giá khai khai báo : {0} ({1})", this.tongTGKB.ToString("N"), this.TKMD.NguyenTe_ID);
        }
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangMauDichRegisterEditForm f = new HangMauDichRegisterEditForm();
                    f.HMD = (HangMauDich)e.Row.DataRow;
                    f.HD.ID = this.TKMD.IDHopDong;                                                                                                    
                    f.TKMD = this.TKMD;
                    f.TyGiaTT = this.TyGiaTT;                   
                    f.ShowDialog();                                        
                    try
                    {
                        dgList.Refetch();
                    }
                    catch { dgList.Refresh(); }
                    this.tongTGKB = 0;
                    foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                    {
                        // Tính lại thuế.
                        //hmd.TinhThue(this.TyGiaTT);
                        // Tổng trị giá khai báo.
                        this.tongTGKB += hmd.TriGiaKB;
                    }
                    lblTongTGKB.Text = string.Format(setText("Tổng trị giá nguyên tệ:", "Total value in original currency") + " {0} ({1})", this.tongTGKB.ToString("N"), this.TKMD.NguyenTe_ID);
                }
                break;
            }
        }

    
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (TKMD.MaLoaiHinh.StartsWith("N"))
            {
                if (PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap(TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.MaHaiQuan, (short)TKMD.NgayDangKy.Year,TKMD.IDHopDong))
                {
                    showMsg("MSG_ALL01");
                    e.Cancel = true;
                    //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                    return;
                }
            }
            else
            {
                if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                {
                    showMsg("MSG_ALL01");
                    e.Cancel = true;
                    //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                    return;
                }
            }
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;
                        if (hmd.ID > 0)
                        {
                            hmd.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, TKMD.IDHopDong);
                            if (!TKMD.MaLoaiHinh.EndsWith("X") && TKMD.LoaiHangHoa == "N")
                            {
                                Company.GC.BLL.GC.NPLNhapTonThucTe npl = Company.GC.BLL.GC.NPLNhapTonThucTe.Load(TKMD.SoToKhai,TKMD.MaLoaiHinh, (short)TKMD.NgayDangKy.Year, TKMD.MaHaiQuan, hmd.MaPhu);
                                npl.Delete();
                            }
                        }
                    }
                }               
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void SaoChepCha_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            HangMauDich hmd = (HangMauDich)dgList.GetRow().DataRow;
            txtMaHang.Text = hmd.MaPhu;
            txtTenHang.Text = hmd.TenHang;
            txtMaHS.Text = hmd.MaHS;
            cbDonViTinh.SelectedValue = hmd.DVT_ID;
            ctrNuocXX.Ma = hmd.NuocXX_ID;
            txtDGNT.Value = this.dgnt = hmd.DonGiaKB;
            txtLuong.Value = this.luong = hmd.SoLuong;
            txtTGNT.Value = hmd.TriGiaKB;
            txtTriGiaKB.Value = hmd.TriGiaKB_VND;      
            txtLuong.Focus();
        }

        private void txtTGTT_NK_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã HS không hợp lệ.", "This value is invalid"));
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }
            string MoTa = MaHS.CheckExist(txtMaHS.Text);
            if (MoTa == "")
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã HS không có trong danh mục mã HS.", "This value is not exist"));
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }
        }

        private void lblMa_HTS_Click(object sender, EventArgs e)
        {

        }

        private void txtMa_HTS_TextChanged(object sender, EventArgs e)
        {

        }

        private void lblDVT_HTS_Click(object sender, EventArgs e)
        {

        }

        private void cbbDVT_HTS_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void cbDonViTinh_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void toolTip1_Popup(object sender, System.Windows.Forms.PopupEventArgs e)
        {

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (TKMD.MaLoaiHinh.StartsWith("N"))
            {
                if (PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap(TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.MaHaiQuan, (short)TKMD.NgayDangKy.Year,TKMD.IDHopDong))
                {
                    showMsg("MSG_ALL01");
                    //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                    return;
                }
            }
            else
            {
                if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                {
                    showMsg("MSG_ALL01");
                    //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                    return;
                }
            }
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            {
                List<HangMauDich> HMDTmpCollection = new List<HangMauDich>();
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;
                        if (hmd.ID > 0)
                        {
                            hmd.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, TKMD.IDHopDong);
                            if (!TKMD.MaLoaiHinh.EndsWith("X") && TKMD.LoaiHangHoa == "N")
                            {
                                Company.GC.BLL.GC.NPLNhapTonThucTe npl = Company.GC.BLL.GC.NPLNhapTonThucTe.Load(TKMD.SoToKhai,TKMD.MaLoaiHinh, (short)TKMD.NgayDangKy.Year, TKMD.MaHaiQuan, hmd.MaPhu);
                                npl.Delete();
                            }
                        }
                        HMDTmpCollection.Add(hmd);                        
                    }
                }
                foreach (HangMauDich HMD in HMDTmpCollection)
                    this.TKMD.HMDCollection.Remove(HMD);
                dgList.DataSource = this.TKMD.HMDCollection;
                try 
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
                TinhTongTriGiaNT();
            }
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            tinhthue2();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            tinhthue2();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            tinhthue2();
        }

        private void txtTL_CLG_Leave(object sender, EventArgs e)
        {
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;
            tinhthue2();
        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            this.tinhthue2();
        }
    }
}
