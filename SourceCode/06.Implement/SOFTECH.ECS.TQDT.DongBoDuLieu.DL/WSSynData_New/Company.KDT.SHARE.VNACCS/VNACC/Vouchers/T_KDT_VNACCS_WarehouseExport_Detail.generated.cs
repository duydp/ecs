﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class T_KDT_VNACCS_WarehouseExport_Detail : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long WarehouseExport_ID { set; get; }
		public decimal STT { set; get; }
		public string SoPhieuXuat { set; get; }
		public DateTime NgayPhieuXuat { set; get; }
		public string TenNguoiNhanHang { set; get; }
		public string MaNguoiNhanHang { set; get; }
        public List<T_KDT_VNACCS_WarehouseExport_GoodsDetail> Collection = new List<T_KDT_VNACCS_WarehouseExport_GoodsDetail>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KDT_VNACCS_WarehouseExport_Detail> ConvertToCollection(IDataReader reader)
		{
			List<T_KDT_VNACCS_WarehouseExport_Detail> collection = new List<T_KDT_VNACCS_WarehouseExport_Detail>();
			while (reader.Read())
			{
				T_KDT_VNACCS_WarehouseExport_Detail entity = new T_KDT_VNACCS_WarehouseExport_Detail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("WarehouseExport_ID"))) entity.WarehouseExport_ID = reader.GetInt64(reader.GetOrdinal("WarehouseExport_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetDecimal(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoPhieuXuat"))) entity.SoPhieuXuat = reader.GetString(reader.GetOrdinal("SoPhieuXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhieuXuat"))) entity.NgayPhieuXuat = reader.GetDateTime(reader.GetOrdinal("NgayPhieuXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KDT_VNACCS_WarehouseExport_Detail> collection, long id)
        {
            foreach (T_KDT_VNACCS_WarehouseExport_Detail item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KDT_VNACCS_WarehouseExport_Details VALUES(@WarehouseExport_ID, @STT, @SoPhieuXuat, @NgayPhieuXuat, @TenNguoiNhanHang, @MaNguoiNhanHang)";
            string update = "UPDATE T_KDT_VNACCS_WarehouseExport_Details SET WarehouseExport_ID = @WarehouseExport_ID, STT = @STT, SoPhieuXuat = @SoPhieuXuat, NgayPhieuXuat = @NgayPhieuXuat, TenNguoiNhanHang = @TenNguoiNhanHang, MaNguoiNhanHang = @MaNguoiNhanHang WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_VNACCS_WarehouseExport_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@WarehouseExport_ID", SqlDbType.BigInt, "WarehouseExport_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhieuXuat", SqlDbType.NVarChar, "SoPhieuXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhieuXuat", SqlDbType.DateTime, "NgayPhieuXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiNhanHang", SqlDbType.NVarChar, "MaNguoiNhanHang", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@WarehouseExport_ID", SqlDbType.BigInt, "WarehouseExport_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhieuXuat", SqlDbType.NVarChar, "SoPhieuXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhieuXuat", SqlDbType.DateTime, "NgayPhieuXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiNhanHang", SqlDbType.NVarChar, "MaNguoiNhanHang", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KDT_VNACCS_WarehouseExport_Details VALUES(@WarehouseExport_ID, @STT, @SoPhieuXuat, @NgayPhieuXuat, @TenNguoiNhanHang, @MaNguoiNhanHang)";
            string update = "UPDATE T_KDT_VNACCS_WarehouseExport_Details SET WarehouseExport_ID = @WarehouseExport_ID, STT = @STT, SoPhieuXuat = @SoPhieuXuat, NgayPhieuXuat = @NgayPhieuXuat, TenNguoiNhanHang = @TenNguoiNhanHang, MaNguoiNhanHang = @MaNguoiNhanHang WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_VNACCS_WarehouseExport_Details WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@WarehouseExport_ID", SqlDbType.BigInt, "WarehouseExport_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhieuXuat", SqlDbType.NVarChar, "SoPhieuXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhieuXuat", SqlDbType.DateTime, "NgayPhieuXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiNhanHang", SqlDbType.NVarChar, "MaNguoiNhanHang", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@WarehouseExport_ID", SqlDbType.BigInt, "WarehouseExport_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Decimal, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhieuXuat", SqlDbType.NVarChar, "SoPhieuXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhieuXuat", SqlDbType.DateTime, "NgayPhieuXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, "TenNguoiNhanHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiNhanHang", SqlDbType.NVarChar, "MaNguoiNhanHang", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KDT_VNACCS_WarehouseExport_Detail Load(long id)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KDT_VNACCS_WarehouseExport_Detail> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KDT_VNACCS_WarehouseExport_Detail> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KDT_VNACCS_WarehouseExport_Detail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<T_KDT_VNACCS_WarehouseExport_Detail> SelectCollectionBy_WarehouseExport_ID(long warehouseExport_ID)
		{
            IDataReader reader = SelectReaderBy_WarehouseExport_ID(warehouseExport_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_WarehouseExport_ID(long warehouseExport_ID)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectBy_WarehouseExport_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@WarehouseExport_ID", SqlDbType.BigInt, warehouseExport_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_WarehouseExport_ID(long warehouseExport_ID)
		{
			const string spName = "p_T_KDT_VNACCS_WarehouseExport_Detail_SelectBy_WarehouseExport_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@WarehouseExport_ID", SqlDbType.BigInt, warehouseExport_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KDT_VNACCS_WarehouseExport_Detail(long warehouseExport_ID, decimal sTT, string soPhieuXuat, DateTime ngayPhieuXuat, string tenNguoiNhanHang, string maNguoiNhanHang)
		{
			T_KDT_VNACCS_WarehouseExport_Detail entity = new T_KDT_VNACCS_WarehouseExport_Detail();	
			entity.WarehouseExport_ID = warehouseExport_ID;
			entity.STT = sTT;
			entity.SoPhieuXuat = soPhieuXuat;
			entity.NgayPhieuXuat = ngayPhieuXuat;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.MaNguoiNhanHang = maNguoiNhanHang;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@WarehouseExport_ID", SqlDbType.BigInt, WarehouseExport_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@SoPhieuXuat", SqlDbType.NVarChar, SoPhieuXuat);
			db.AddInParameter(dbCommand, "@NgayPhieuXuat", SqlDbType.DateTime, NgayPhieuXuat.Year <= 1753 ? DBNull.Value : (object) NgayPhieuXuat);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.NVarChar, MaNguoiNhanHang);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KDT_VNACCS_WarehouseExport_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseExport_Detail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KDT_VNACCS_WarehouseExport_Detail(long id, long warehouseExport_ID, decimal sTT, string soPhieuXuat, DateTime ngayPhieuXuat, string tenNguoiNhanHang, string maNguoiNhanHang)
		{
			T_KDT_VNACCS_WarehouseExport_Detail entity = new T_KDT_VNACCS_WarehouseExport_Detail();			
			entity.ID = id;
			entity.WarehouseExport_ID = warehouseExport_ID;
			entity.STT = sTT;
			entity.SoPhieuXuat = soPhieuXuat;
			entity.NgayPhieuXuat = ngayPhieuXuat;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.MaNguoiNhanHang = maNguoiNhanHang;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KDT_VNACCS_WarehouseExport_Detail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@WarehouseExport_ID", SqlDbType.BigInt, WarehouseExport_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@SoPhieuXuat", SqlDbType.NVarChar, SoPhieuXuat);
			db.AddInParameter(dbCommand, "@NgayPhieuXuat", SqlDbType.DateTime, NgayPhieuXuat.Year <= 1753 ? DBNull.Value : (object) NgayPhieuXuat);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.NVarChar, MaNguoiNhanHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KDT_VNACCS_WarehouseExport_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseExport_Detail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KDT_VNACCS_WarehouseExport_Detail(long id, long warehouseExport_ID, decimal sTT, string soPhieuXuat, DateTime ngayPhieuXuat, string tenNguoiNhanHang, string maNguoiNhanHang)
		{
			T_KDT_VNACCS_WarehouseExport_Detail entity = new T_KDT_VNACCS_WarehouseExport_Detail();			
			entity.ID = id;
			entity.WarehouseExport_ID = warehouseExport_ID;
			entity.STT = sTT;
			entity.SoPhieuXuat = soPhieuXuat;
			entity.NgayPhieuXuat = ngayPhieuXuat;
			entity.TenNguoiNhanHang = tenNguoiNhanHang;
			entity.MaNguoiNhanHang = maNguoiNhanHang;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@WarehouseExport_ID", SqlDbType.BigInt, WarehouseExport_ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Decimal, STT);
			db.AddInParameter(dbCommand, "@SoPhieuXuat", SqlDbType.NVarChar, SoPhieuXuat);
			db.AddInParameter(dbCommand, "@NgayPhieuXuat", SqlDbType.DateTime, NgayPhieuXuat.Year <= 1753 ? DBNull.Value : (object) NgayPhieuXuat);
			db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
			db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.NVarChar, MaNguoiNhanHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KDT_VNACCS_WarehouseExport_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseExport_Detail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KDT_VNACCS_WarehouseExport_Detail(long id)
		{
			T_KDT_VNACCS_WarehouseExport_Detail entity = new T_KDT_VNACCS_WarehouseExport_Detail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_WarehouseExport_ID(long warehouseExport_ID)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_DeleteBy_WarehouseExport_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@WarehouseExport_ID", SqlDbType.BigInt, warehouseExport_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Detail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KDT_VNACCS_WarehouseExport_Detail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseExport_Detail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}