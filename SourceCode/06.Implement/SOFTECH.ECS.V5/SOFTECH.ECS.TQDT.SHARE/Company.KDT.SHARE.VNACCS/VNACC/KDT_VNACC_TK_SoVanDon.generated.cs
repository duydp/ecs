using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_TK_SoVanDon : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public int SoTT { set; get; }
		public string SoVanDon { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		public string SoVanDonChu { set; get; }
		public string NamVanDonChu { set; get; }
		public string LoaiDinhDanh { set; get; }
		public DateTime NgayVanDon { set; get; }
		public string SoDinhDanh { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_TK_SoVanDon> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_TK_SoVanDon> collection = new List<KDT_VNACC_TK_SoVanDon>();
			while (reader.Read())
			{
				KDT_VNACC_TK_SoVanDon entity = new KDT_VNACC_TK_SoVanDon();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTT"))) entity.SoTT = reader.GetInt32(reader.GetOrdinal("SoTT"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDonChu"))) entity.SoVanDonChu = reader.GetString(reader.GetOrdinal("SoVanDonChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamVanDonChu"))) entity.NamVanDonChu = reader.GetString(reader.GetOrdinal("NamVanDonChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiDinhDanh"))) entity.LoaiDinhDanh = reader.GetString(reader.GetOrdinal("LoaiDinhDanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDinhDanh"))) entity.SoDinhDanh = reader.GetString(reader.GetOrdinal("SoDinhDanh"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_TK_SoVanDon> collection, long id)
        {
            foreach (KDT_VNACC_TK_SoVanDon item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_TK_SoVanDon VALUES(@TKMD_ID, @SoTT, @SoVanDon, @InputMessageID, @MessageTag, @IndexTag, @SoVanDonChu, @NamVanDonChu, @LoaiDinhDanh, @NgayVanDon, @SoDinhDanh)";
            string update = "UPDATE t_KDT_VNACC_TK_SoVanDon SET TKMD_ID = @TKMD_ID, SoTT = @SoTT, SoVanDon = @SoVanDon, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, SoVanDonChu = @SoVanDonChu, NamVanDonChu = @NamVanDonChu, LoaiDinhDanh = @LoaiDinhDanh, NgayVanDon = @NgayVanDon, SoDinhDanh = @SoDinhDanh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TK_SoVanDon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTT", SqlDbType.Int, "SoTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDonChu", SqlDbType.VarChar, "SoVanDonChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamVanDonChu", SqlDbType.VarChar, "NamVanDonChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiDinhDanh", SqlDbType.VarChar, "LoaiDinhDanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDinhDanh", SqlDbType.VarChar, "SoDinhDanh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTT", SqlDbType.Int, "SoTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDonChu", SqlDbType.VarChar, "SoVanDonChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamVanDonChu", SqlDbType.VarChar, "NamVanDonChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiDinhDanh", SqlDbType.VarChar, "LoaiDinhDanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDinhDanh", SqlDbType.VarChar, "SoDinhDanh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_TK_SoVanDon VALUES(@TKMD_ID, @SoTT, @SoVanDon, @InputMessageID, @MessageTag, @IndexTag, @SoVanDonChu, @NamVanDonChu, @LoaiDinhDanh, @NgayVanDon, @SoDinhDanh)";
            string update = "UPDATE t_KDT_VNACC_TK_SoVanDon SET TKMD_ID = @TKMD_ID, SoTT = @SoTT, SoVanDon = @SoVanDon, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, SoVanDonChu = @SoVanDonChu, NamVanDonChu = @NamVanDonChu, LoaiDinhDanh = @LoaiDinhDanh, NgayVanDon = @NgayVanDon, SoDinhDanh = @SoDinhDanh WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TK_SoVanDon WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTT", SqlDbType.Int, "SoTT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDonChu", SqlDbType.VarChar, "SoVanDonChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamVanDonChu", SqlDbType.VarChar, "NamVanDonChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiDinhDanh", SqlDbType.VarChar, "LoaiDinhDanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDinhDanh", SqlDbType.VarChar, "SoDinhDanh", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTT", SqlDbType.Int, "SoTT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDonChu", SqlDbType.VarChar, "SoVanDonChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamVanDonChu", SqlDbType.VarChar, "NamVanDonChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiDinhDanh", SqlDbType.VarChar, "LoaiDinhDanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayVanDon", SqlDbType.DateTime, "NgayVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDinhDanh", SqlDbType.VarChar, "SoDinhDanh", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_TK_SoVanDon Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_SoVanDon_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_TK_SoVanDon> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_TK_SoVanDon> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_TK_SoVanDon> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TK_SoVanDon_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TK_SoVanDon_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_TK_SoVanDon(long tKMD_ID, int soTT, string soVanDon, string inputMessageID, string messageTag, string indexTag, string soVanDonChu, string namVanDonChu, string loaiDinhDanh, DateTime ngayVanDon, string soDinhDanh)
		{
			KDT_VNACC_TK_SoVanDon entity = new KDT_VNACC_TK_SoVanDon();	
			entity.TKMD_ID = tKMD_ID;
			entity.SoTT = soTT;
			entity.SoVanDon = soVanDon;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.SoVanDonChu = soVanDonChu;
			entity.NamVanDonChu = namVanDonChu;
			entity.LoaiDinhDanh = loaiDinhDanh;
			entity.NgayVanDon = ngayVanDon;
			entity.SoDinhDanh = soDinhDanh;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_TK_SoVanDon_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoTT", SqlDbType.Int, SoTT);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@SoVanDonChu", SqlDbType.VarChar, SoVanDonChu);
			db.AddInParameter(dbCommand, "@NamVanDonChu", SqlDbType.VarChar, NamVanDonChu);
			db.AddInParameter(dbCommand, "@LoaiDinhDanh", SqlDbType.VarChar, LoaiDinhDanh);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@SoDinhDanh", SqlDbType.VarChar, SoDinhDanh);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_TK_SoVanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_SoVanDon item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_TK_SoVanDon(long id, long tKMD_ID, int soTT, string soVanDon, string inputMessageID, string messageTag, string indexTag, string soVanDonChu, string namVanDonChu, string loaiDinhDanh, DateTime ngayVanDon, string soDinhDanh)
		{
			KDT_VNACC_TK_SoVanDon entity = new KDT_VNACC_TK_SoVanDon();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoTT = soTT;
			entity.SoVanDon = soVanDon;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.SoVanDonChu = soVanDonChu;
			entity.NamVanDonChu = namVanDonChu;
			entity.LoaiDinhDanh = loaiDinhDanh;
			entity.NgayVanDon = ngayVanDon;
			entity.SoDinhDanh = soDinhDanh;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_TK_SoVanDon_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoTT", SqlDbType.Int, SoTT);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@SoVanDonChu", SqlDbType.VarChar, SoVanDonChu);
			db.AddInParameter(dbCommand, "@NamVanDonChu", SqlDbType.VarChar, NamVanDonChu);
			db.AddInParameter(dbCommand, "@LoaiDinhDanh", SqlDbType.VarChar, LoaiDinhDanh);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@SoDinhDanh", SqlDbType.VarChar, SoDinhDanh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_TK_SoVanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_SoVanDon item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_TK_SoVanDon(long id, long tKMD_ID, int soTT, string soVanDon, string inputMessageID, string messageTag, string indexTag, string soVanDonChu, string namVanDonChu, string loaiDinhDanh, DateTime ngayVanDon, string soDinhDanh)
		{
			KDT_VNACC_TK_SoVanDon entity = new KDT_VNACC_TK_SoVanDon();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.SoTT = soTT;
			entity.SoVanDon = soVanDon;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.SoVanDonChu = soVanDonChu;
			entity.NamVanDonChu = namVanDonChu;
			entity.LoaiDinhDanh = loaiDinhDanh;
			entity.NgayVanDon = ngayVanDon;
			entity.SoDinhDanh = soDinhDanh;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_SoVanDon_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@SoTT", SqlDbType.Int, SoTT);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@SoVanDonChu", SqlDbType.VarChar, SoVanDonChu);
			db.AddInParameter(dbCommand, "@NamVanDonChu", SqlDbType.VarChar, NamVanDonChu);
			db.AddInParameter(dbCommand, "@LoaiDinhDanh", SqlDbType.VarChar, LoaiDinhDanh);
			db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object) NgayVanDon);
			db.AddInParameter(dbCommand, "@SoDinhDanh", SqlDbType.VarChar, SoDinhDanh);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_TK_SoVanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_SoVanDon item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_TK_SoVanDon(long id)
		{
			KDT_VNACC_TK_SoVanDon entity = new KDT_VNACC_TK_SoVanDon();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_SoVanDon_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TK_SoVanDon_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_TK_SoVanDon> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TK_SoVanDon item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}