using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAL0020 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute JNO { get; set; }
public PropertiesAttribute TSB { get; set; }
public PropertiesAttribute TNM { get; set; }
public PropertiesAttribute CH { get; set; }
public PropertiesAttribute CHB { get; set; }
public PropertiesAttribute YMD { get; set; }
public PropertiesAttribute SNM { get; set; }
public PropertiesAttribute SAD { get; set; }
public PropertiesAttribute STL { get; set; }
public PropertiesAttribute REF { get; set; }
public PropertiesAttribute KIJ { get; set; }

public VAL0020()
        {
JNO = new PropertiesAttribute(12, typeof(int));
TSB = new PropertiesAttribute(3, typeof(string));
TNM = new PropertiesAttribute(210, typeof(string));
CH = new PropertiesAttribute(6, typeof(string));
CHB = new PropertiesAttribute(2, typeof(string));
YMD = new PropertiesAttribute(8, typeof(DateTime));
SNM = new PropertiesAttribute(300, typeof(string));
SAD = new PropertiesAttribute(300, typeof(string));
STL = new PropertiesAttribute(20, typeof(string));
REF = new PropertiesAttribute(20, typeof(string));
KIJ = new PropertiesAttribute(996, typeof(string));
TongSoByte = 1899;
}

}public partial class EnumGroupID
    {
}

}
