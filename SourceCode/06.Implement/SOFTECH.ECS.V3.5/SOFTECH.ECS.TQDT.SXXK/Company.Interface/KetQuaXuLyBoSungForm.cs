﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class KetQuaXuLyBoSungForm : Company.Interface.BaseForm
    {
        public long ItemID { set; get; }
        public string refId { set; get; }
        public KetQuaXuLyBoSungForm()
        {
            InitializeComponent();
        }

        private void KetQuaXuLyBoSung_Load(object sender, EventArgs e)
        {
            dgList.DataSource = Company.KDT.SHARE.Components.Message.SelectCollectionBy_ReferenceID(new Guid(refId));
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                int id = Convert.ToInt32(e.Row.Cells["ID"].Value);
                Company.KDT.SHARE.Components.Message msg = Company.KDT.SHARE.Components.Message.Load(id);
                this.ShowMessageTQDT("ID: [" + msg.ReferenceID + "]\r\n" + msg.NoiDungThongBao, false);
            }
        }
    }
}
