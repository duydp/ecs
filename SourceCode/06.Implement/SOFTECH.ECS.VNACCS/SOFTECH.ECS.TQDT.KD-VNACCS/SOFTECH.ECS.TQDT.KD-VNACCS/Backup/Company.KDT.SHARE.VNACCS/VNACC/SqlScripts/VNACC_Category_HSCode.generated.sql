-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_HSCode_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Insert]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@HSCode varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
INSERT INTO [dbo].[t_VNACC_Category_HSCode]
(
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES
(
	@ResultCode,
	@PGNumber,
	@TableID,
	@ProcessClassification,
	@CreatorClassification,
	@NumberOfKeyItems,
	@HSCode,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Update]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@HSCode varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_VNACC_Category_HSCode]
SET
	[ResultCode] = @ResultCode,
	[PGNumber] = @PGNumber,
	[TableID] = @TableID,
	[ProcessClassification] = @ProcessClassification,
	[CreatorClassification] = @CreatorClassification,
	[NumberOfKeyItems] = @NumberOfKeyItems,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[HSCode] = @HSCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_InsertUpdate]
	@ResultCode varchar(50),
	@PGNumber int,
	@TableID varchar(10),
	@ProcessClassification varchar(5),
	@CreatorClassification int,
	@NumberOfKeyItems varchar(5),
	@HSCode varchar(10),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [HSCode] FROM [dbo].[t_VNACC_Category_HSCode] WHERE [HSCode] = @HSCode)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_HSCode] 
		SET
			[ResultCode] = @ResultCode,
			[PGNumber] = @PGNumber,
			[TableID] = @TableID,
			[ProcessClassification] = @ProcessClassification,
			[CreatorClassification] = @CreatorClassification,
			[NumberOfKeyItems] = @NumberOfKeyItems,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[HSCode] = @HSCode
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_VNACC_Category_HSCode]
	(
			[ResultCode],
			[PGNumber],
			[TableID],
			[ProcessClassification],
			[CreatorClassification],
			[NumberOfKeyItems],
			[HSCode],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
	)
	VALUES
	(
			@ResultCode,
			@PGNumber,
			@TableID,
			@ProcessClassification,
			@CreatorClassification,
			@NumberOfKeyItems,
			@HSCode,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Delete]
	@HSCode varchar(10)
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_HSCode]
WHERE
	[HSCode] = @HSCode

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_HSCode] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_Load]
	@HSCode varchar(10)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_HSCode]
WHERE
	[HSCode] = @HSCode
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_VNACC_Category_HSCode] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_HSCode_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, September 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_HSCode_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ResultCode],
	[PGNumber],
	[TableID],
	[ProcessClassification],
	[CreatorClassification],
	[NumberOfKeyItems],
	[HSCode],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_VNACC_Category_HSCode]	

GO

