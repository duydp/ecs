﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
//
//
using eDeclaration;
using Janus.Windows.EditControls;
using Janus.Windows.ExplorerBar;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;

namespace Company.Interface
{
    partial class MainForm
    {
        private UICommandManager cmMain;
        private UIRebar TopRebar1;
        private UICommand cmdHeThong;
        private UICommand cmdHeThong1;
        private UICommand cmdThoat;
        private UICommand cmdThoat1;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private UIPanelManager pmMain;
        private UICommandBar cmbMenu;
        private UICommand cmdLoaiHinh;
        private UIPanel pnlSXXK;
        private UIPanelInnerContainer pnlSXXKContainer;
        private UIPanel pnlGiaCong;
        private UIPanelInnerContainer pnlGiaCongContainer;
        private UIPanel pnlKinhDoanh;
        private UIPanelInnerContainer pnlKinhDoanhContainer;
        private UIPanel pnlDauTu;
        internal ImageList ilSmall;
        internal ImageList ilMedium;
        internal ImageList ilLarge;
        private UICommand cmdReceiveAll;
        private Janus.Windows.ExplorerBar.ExplorerBar expSXXK;
        private Janus.Windows.ExplorerBar.ExplorerBar expGiaCong;
        private Janus.Windows.ExplorerBar.ExplorerBar expKD;
        private Janus.Windows.UI.CommandBars.UICommand cmdThoat2;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup1 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem1 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem2 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem3 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup2 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem4 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem5 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem6 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup3 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem7 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem8 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem9 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup4 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem10 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem11 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem12 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem13 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup5 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem14 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem15 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem16 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem17 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup6 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem18 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem19 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem20 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup7 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem21 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem22 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem23 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem24 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup8 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem25 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem26 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup9 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem27 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem28 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem29 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup10 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem30 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem31 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem32 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup11 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem33 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem34 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem35 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup12 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem36 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem37 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem38 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem39 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem40 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup13 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem41 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem42 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem43 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem44 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup14 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem45 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem46 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem47 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup15 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem48 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem49 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem50 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup16 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem51 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem52 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem53 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup17 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem54 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem55 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup18 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem56 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem57 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem58 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup19 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem59 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem60 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup20 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem61 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem62 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup21 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem63 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem64 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup22 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem65 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem66 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup23 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem67 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem68 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup24 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem69 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem70 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel1 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel2 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel3 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbMenu = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdHeThong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.cmdNhapXuat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXuat");
            this.Command11 = new Janus.Windows.UI.CommandBars.UICommand("Command1");
            this.cmdDanhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDanhMuc");
            this.Command01 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.TraCuuMaHS1 = new Janus.Windows.UI.CommandBars.UICommand("TraCuuMaHS");
            this.QuanTri1 = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.cmdHeThong = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.cmdTLTTDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTLTTDN");
            this.cmdDataVersion1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDataVersion");
            this.cmdBackUp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBackUp");
            this.cmdCauHinh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.QuanLyMess1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyMess");
            this.mnuQuerySQL1 = new Janus.Windows.UI.CommandBars.UICommand("mnuQuerySQL");
            this.cmdLog1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.LoginUser1 = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.cmdThoat2 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdThoat = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdLoaiHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiHinh");
            this.cmdReceiveAll = new Janus.Windows.UI.CommandBars.UICommand("cmdReceiveAll");
            this.NhacNho = new Janus.Windows.UI.CommandBars.UICommand("NhacNho");
            this.DongBoDuLieu = new Janus.Windows.UI.CommandBars.UICommand("DongBoDuLieu");
            this.cmdImport = new Janus.Windows.UI.CommandBars.UICommand("cmdImport");
            this.cmdImportNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.cmdImportNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportTTDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportTTDM");
            this.cmdImportToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.Command1 = new Janus.Windows.UI.CommandBars.UICommand("Command1");
            this.cmd20071 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd20031 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.cmdVN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdEng1 = new Janus.Windows.UI.CommandBars.UICommand("cmdEng");
            this.cmd2007 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd2003 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.Command0 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.cmdHelp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHelp");
            this.cmdAutoUpdate1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdActivate1 = new Janus.Windows.UI.CommandBars.UICommand("cmdActivate");
            this.cmdAbout1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdHelp = new Janus.Windows.UI.CommandBars.UICommand("cmdHelp");
            this.cmdAbout = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdNPLNhapTon = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLNhapTon");
            this.cmdDanhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdDanhMuc");
            this.cmdHaiQuan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNuoc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdMaHS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNguyenTe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.cmdDMSPGC1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDMSPGC");
            this.cmdMaHS = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNuoc = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdHaiQuan = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNguyenTe = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.cmdBackUp = new Janus.Windows.UI.CommandBars.UICommand("cmdBackUp");
            this.cmdRestore = new Janus.Windows.UI.CommandBars.UICommand("cmdRestore");
            this.ThongSoKetNoi = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdThietLapIn = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapIn");
            this.cmdExportExccel = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdImportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdDongBoPhongKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdDongBoPhongKhai");
            this.cmdImportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdExportExccel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdCauHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.ThongSoKetNoi1 = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ1 = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdCauHinhToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinhToKhai");
            this.cmdThietLapIn1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapIn");
            this.cmdCauHinhToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinhToKhai");
            this.QuanTri = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.QuanLyNguoiDung1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNhom1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNhom");
            this.QuanLyNguoiDung = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNhom = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNhom");
            this.LoginUser = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.TraCuuMaHS = new Janus.Windows.UI.CommandBars.UICommand("TraCuuMaHS");
            this.cmdAutoUpdate = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.NhapXML = new Janus.Windows.UI.CommandBars.UICommand("NhapXML");
            this.NhapHDXML = new Janus.Windows.UI.CommandBars.UICommand("NhapHDXML");
            this.NhapDMXML = new Janus.Windows.UI.CommandBars.UICommand("NhapDMXML");
            this.NhapPKXML = new Janus.Windows.UI.CommandBars.UICommand("NhapPKXML");
            this.XuatTKXML = new Janus.Windows.UI.CommandBars.UICommand("XuatTKXML");
            this.XuatTKMD1 = new Janus.Windows.UI.CommandBars.UICommand("XuatTKMD");
            this.XuatTKGCCT1 = new Janus.Windows.UI.CommandBars.UICommand("XuatTKGCCT");
            this.XuatTKMD = new Janus.Windows.UI.CommandBars.UICommand("XuatTKMD");
            this.XuatTKGCCT = new Janus.Windows.UI.CommandBars.UICommand("XuatTKGCCT");
            this.NhapTKXML = new Janus.Windows.UI.CommandBars.UICommand("NhapTKXML");
            this.NhapTKMD1 = new Janus.Windows.UI.CommandBars.UICommand("NhapTKMD");
            this.NhapTKGCCT1 = new Janus.Windows.UI.CommandBars.UICommand("NhapTKGCCT");
            this.NhapTKMD = new Janus.Windows.UI.CommandBars.UICommand("NhapTKMD");
            this.NhapTKGCCT = new Janus.Windows.UI.CommandBars.UICommand("NhapTKGCCT");
            this.cmdEnglish = new Janus.Windows.UI.CommandBars.UICommand("cmdEng");
            this.cmdVN = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdNhapXuat = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXuat");
            this.NhapXML2 = new Janus.Windows.UI.CommandBars.UICommand("NhapXML");
            this.cmdXuatDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDN");
            this.cmdXuatDuLieuPhongKHai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuPhongKHai");
            this.cmdNhapDuLieuTuDoangNghiep1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDuLieuTuDoangNghiep");
            this.cmdXuatDuLieuPhongKHai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuPhongKHai");
            this.cmdXuatHopDong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatHopDong");
            this.cmdXuatDinhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDinhMuc");
            this.cmdXuatPhuKien1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatPhuKien");
            this.cmdXuatToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.cmdXuatHopDong = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatHopDong");
            this.cmdXuatDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDinhMuc");
            this.cmdXuatPhuKien = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatPhuKien");
            this.cmdXuatToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.XuatTKGCCT2 = new Janus.Windows.UI.CommandBars.UICommand("XuatTKGCCT");
            this.XuatTKMD2 = new Janus.Windows.UI.CommandBars.UICommand("XuatTKMD");
            this.cmdActivate = new Janus.Windows.UI.CommandBars.UICommand("cmdActivate");
            this.cmdDMSPGC = new Janus.Windows.UI.CommandBars.UICommand("cmdDMSPGC");
            this.cmdNhapDuLieuDaiLy = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDuLieuDaiLy");
            this.cmdCloseMe = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseMe");
            this.cmdCloseAllButMe = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButMe");
            this.cmdCloseAll = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.cmdXuatDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDN");
            this.QuanLyMess = new Janus.Windows.UI.CommandBars.UICommand("QuanLyMess");
            this.cmdNhapDuLieuTuDoangNghiep = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDuLieuTuDoangNghiep");
            this.cmdNhapHopDong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapHopDong");
            this.cmdNhapDinhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDinhMuc");
            this.cmdNhapPhuKien1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapPhuKien");
            this.cmdNhapToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhai");
            this.cmdNhapHopDong = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapHopDong");
            this.cmdNhapDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDinhMuc");
            this.cmdNhapPhuKien = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapPhuKien");
            this.cmdNhapToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhai");
            this.cmdNhapToKhaiGCCT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiGCCT");
            this.cmdNhapToKhaiMauDich1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiMauDich");
            this.cmdNhapToKhaiMauDich = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiMauDich");
            this.cmdNhapToKhaiGCCT = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhaiGCCT");
            this.cmdTLTTDN = new Janus.Windows.UI.CommandBars.UICommand("cmdTLTTDN");
            this.mnuQuerySQL = new Janus.Windows.UI.CommandBars.UICommand("mnuQuerySQL");
            this.cmdLog = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.cmdDataVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdDataVersion");
            this.mnuRightClick = new Janus.Windows.UI.CommandBars.UIContextMenu();
            this.cmdCloseMe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseMe");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdCloseAllButMe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButMe");
            this.cmdCloseAll1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.ilSmall = new System.Windows.Forms.ImageList(this.components);
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdThoat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.pmMain = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBar1 = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel2 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel2Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBar2 = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSXXK = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSXXKContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expSXXK = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlGiaCong = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlGiaCongContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expGiaCong = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlKinhDoanh = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlKinhDoanhContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expKD = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlDauTu = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlDauTuContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expDT = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSend = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSendContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expKhaiBao_TheoDoi = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.ilMedium = new System.Windows.Forms.ImageList(this.components);
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.statusBar = new Janus.Windows.UI.StatusBar.UIStatusBar();
            this.cmdThoat3 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuRightClick)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).BeginInit();
            this.uiPanel2.SuspendLayout();
            this.uiPanel2Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).BeginInit();
            this.pnlSXXK.SuspendLayout();
            this.pnlSXXKContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expSXXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).BeginInit();
            this.pnlGiaCong.SuspendLayout();
            this.pnlGiaCongContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).BeginInit();
            this.pnlKinhDoanh.SuspendLayout();
            this.pnlKinhDoanhContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).BeginInit();
            this.pnlDauTu.SuspendLayout();
            this.pnlDauTuContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).BeginInit();
            this.pnlSend.SuspendLayout();
            this.pnlSendContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expKhaiBao_TheoDoi)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.BackColor = System.Drawing.SystemColors.Control;
            this.grbMain.Location = new System.Drawing.Point(202, 29);
            this.grbMain.Size = new System.Drawing.Size(485, 391);
            this.grbMain.Visible = false;
            this.grbMain.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // vsmMain
            // 
            this.vsmMain.DefaultColorScheme = "Office2007";
            // 
            // cmMain
            // 
            this.cmMain.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowMerge = false;
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong,
            this.cmdThoat,
            this.cmdLoaiHinh,
            this.cmdReceiveAll,
            this.NhacNho,
            this.DongBoDuLieu,
            this.cmdImport,
            this.cmdImportNPL,
            this.cmdImportSP,
            this.cmdImportDM,
            this.cmdImportTTDM,
            this.cmdImportToKhai,
            this.cmdImportHangHoa,
            this.Command1,
            this.cmd2007,
            this.cmd2003,
            this.Command0,
            this.cmdHelp,
            this.cmdAbout,
            this.cmdNPLNhapTon,
            this.cmdDanhMuc,
            this.cmdMaHS,
            this.cmdNuoc,
            this.cmdHaiQuan,
            this.cmdNguyenTe,
            this.cmdDVT,
            this.cmdPTTT,
            this.cmdPTVT,
            this.cmdDKGH,
            this.cmdCuaKhau,
            this.cmdBackUp,
            this.cmdRestore,
            this.ThongSoKetNoi,
            this.TLThongTinDNHQ,
            this.cmdThietLapIn,
            this.cmdExportExccel,
            this.cmdImportExcel,
            this.cmdDongBoPhongKhai,
            this.cmdCauHinh,
            this.cmdCauHinhToKhai,
            this.QuanTri,
            this.QuanLyNguoiDung,
            this.QuanLyNhom,
            this.LoginUser,
            this.cmdChangePass,
            this.TraCuuMaHS,
            this.cmdAutoUpdate,
            this.NhapXML,
            this.NhapHDXML,
            this.NhapDMXML,
            this.NhapPKXML,
            this.XuatTKXML,
            this.XuatTKMD,
            this.XuatTKGCCT,
            this.NhapTKXML,
            this.NhapTKMD,
            this.NhapTKGCCT,
            this.cmdEnglish,
            this.cmdVN,
            this.cmdNhapXuat,
            this.cmdXuatDuLieuPhongKHai,
            this.cmdXuatHopDong,
            this.cmdXuatDinhMuc,
            this.cmdXuatPhuKien,
            this.cmdXuatToKhai,
            this.cmdActivate,
            this.cmdDMSPGC,
            this.cmdNhapDuLieuDaiLy,
            this.cmdCloseMe,
            this.cmdCloseAllButMe,
            this.cmdCloseAll,
            this.cmdXuatDN,
            this.QuanLyMess,
            this.cmdNhapDuLieuTuDoangNghiep,
            this.cmdNhapHopDong,
            this.cmdNhapDinhMuc,
            this.cmdNhapPhuKien,
            this.cmdNhapToKhai,
            this.cmdNhapToKhaiMauDich,
            this.cmdNhapToKhaiGCCT,
            this.cmdTLTTDN,
            this.mnuQuerySQL,
            this.cmdLog,
            this.cmdDataVersion});
            this.cmMain.ContainerControl = this;
            this.cmMain.ContextMenus.AddRange(new Janus.Windows.UI.CommandBars.UIContextMenu[] {
            this.mnuRightClick});
            this.cmMain.Id = new System.Guid("eae49f54-3bfa-4a6a-8b46-89b443ba80cd");
            this.cmMain.ImageList = this.ilSmall;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbMenu
            // 
            this.cmbMenu.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowMerge = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.CommandBarType = Janus.Windows.UI.CommandBars.CommandBarType.Menu;
            this.cmbMenu.CommandManager = this.cmMain;
            this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong1,
            this.cmdNhapXuat1,
            this.Command11,
            this.cmdDanhMuc1,
            this.Command01,
            this.TraCuuMaHS1,
            this.QuanTri1});
            this.cmbMenu.Key = "cmbMenu";
            this.cmbMenu.Location = new System.Drawing.Point(0, 0);
            this.cmbMenu.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbMenu.MergeRowOrder = 0;
            this.cmbMenu.Name = "cmbMenu";
            this.cmbMenu.RowIndex = 0;
            this.cmbMenu.Size = new System.Drawing.Size(690, 26);
            this.cmbMenu.Text = "cmbMenu";
            this.cmbMenu.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMenu_CommandClick);
            // 
            // cmdHeThong1
            // 
            this.cmdHeThong1.Key = "cmdHeThong";
            this.cmdHeThong1.Name = "cmdHeThong1";
            this.cmdHeThong1.Text = "Hệ thốn&g";
            // 
            // cmdNhapXuat1
            // 
            this.cmdNhapXuat1.ImageIndex = 14;
            this.cmdNhapXuat1.Key = "cmdNhapXuat";
            this.cmdNhapXuat1.Name = "cmdNhapXuat1";
            // 
            // Command11
            // 
            this.Command11.Key = "Command1";
            this.Command11.Name = "Command11";
            this.Command11.Text = "Gia&o diện";
            // 
            // cmdDanhMuc1
            // 
            this.cmdDanhMuc1.Key = "cmdDanhMuc";
            this.cmdDanhMuc1.Name = "cmdDanhMuc1";
            this.cmdDanhMuc1.Text = "&Danh mục";
            // 
            // Command01
            // 
            this.Command01.Key = "Command0";
            this.Command01.Name = "Command01";
            this.Command01.Text = "&Trợ giúp";
            // 
            // TraCuuMaHS1
            // 
            this.TraCuuMaHS1.Key = "TraCuuMaHS";
            this.TraCuuMaHS1.Name = "TraCuuMaHS1";
            this.TraCuuMaHS1.Text = "Tra cứu &mã HS";
            // 
            // QuanTri1
            // 
            this.QuanTri1.Key = "QuanTri";
            this.QuanTri1.Name = "QuanTri1";
            this.QuanTri1.Text = "&Quản trị";
            // 
            // cmdHeThong
            // 
            this.cmdHeThong.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdTLTTDN1,
            this.cmdDataVersion1,
            this.cmdBackUp1,
            this.cmdCauHinh1,
            this.QuanLyMess1,
            this.mnuQuerySQL1,
            this.cmdLog1,
            this.LoginUser1,
            this.cmdChangePass1,
            this.cmdThoat2});
            this.cmdHeThong.Key = "cmdHeThong";
            this.cmdHeThong.Name = "cmdHeThong";
            this.cmdHeThong.Text = "&Hệ thống";
            // 
            // cmdTLTTDN1
            // 
            this.cmdTLTTDN1.ImageIndex = 6;
            this.cmdTLTTDN1.Key = "cmdTLTTDN";
            this.cmdTLTTDN1.Name = "cmdTLTTDN1";
            // 
            // cmdDataVersion1
            // 
            this.cmdDataVersion1.ImageIndex = 8;
            this.cmdDataVersion1.Key = "cmdDataVersion";
            this.cmdDataVersion1.Name = "cmdDataVersion1";
            // 
            // cmdBackUp1
            // 
            this.cmdBackUp1.Key = "cmdBackUp";
            this.cmdBackUp1.Name = "cmdBackUp1";
            // 
            // cmdCauHinh1
            // 
            this.cmdCauHinh1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCauHinh1.Icon")));
            this.cmdCauHinh1.Key = "cmdCauHinh";
            this.cmdCauHinh1.Name = "cmdCauHinh1";
            // 
            // QuanLyMess1
            // 
            this.QuanLyMess1.ImageIndex = 12;
            this.QuanLyMess1.Key = "QuanLyMess";
            this.QuanLyMess1.Name = "QuanLyMess1";
            // 
            // mnuQuerySQL1
            // 
            this.mnuQuerySQL1.ImageIndex = 8;
            this.mnuQuerySQL1.Key = "mnuQuerySQL";
            this.mnuQuerySQL1.Name = "mnuQuerySQL1";
            // 
            // cmdLog1
            // 
            this.cmdLog1.ImageIndex = 7;
            this.cmdLog1.Key = "cmdLog";
            this.cmdLog1.Name = "cmdLog1";
            // 
            // LoginUser1
            // 
            this.LoginUser1.Icon = ((System.Drawing.Icon)(resources.GetObject("LoginUser1.Icon")));
            this.LoginUser1.Key = "LoginUser";
            this.LoginUser1.Name = "LoginUser1";
            // 
            // cmdChangePass1
            // 
            this.cmdChangePass1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdChangePass1.Icon")));
            this.cmdChangePass1.Key = "cmdChangePass";
            this.cmdChangePass1.Name = "cmdChangePass1";
            // 
            // cmdThoat2
            // 
            this.cmdThoat2.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThoat2.Icon")));
            this.cmdThoat2.Key = "cmdThoat";
            this.cmdThoat2.Name = "cmdThoat2";
            // 
            // cmdThoat
            // 
            this.cmdThoat.Key = "cmdThoat";
            this.cmdThoat.Name = "cmdThoat";
            this.cmdThoat.Text = "Thoát";
            // 
            // cmdLoaiHinh
            // 
            this.cmdLoaiHinh.Key = "cmdLoaiHinh";
            this.cmdLoaiHinh.Name = "cmdLoaiHinh";
            this.cmdLoaiHinh.Text = "Loại hình";
            // 
            // cmdReceiveAll
            // 
            this.cmdReceiveAll.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdReceiveAll.Icon")));
            this.cmdReceiveAll.Key = "cmdReceiveAll";
            this.cmdReceiveAll.Name = "cmdReceiveAll";
            this.cmdReceiveAll.Text = "Cập nhật thông tin";
            // 
            // NhacNho
            // 
            this.NhacNho.Key = "NhacNho";
            this.NhacNho.Name = "NhacNho";
            this.NhacNho.Text = "Nhắc nhở";
            // 
            // DongBoDuLieu
            // 
            this.DongBoDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("DongBoDuLieu.Icon")));
            this.DongBoDuLieu.Key = "DongBoDuLieu";
            this.DongBoDuLieu.Name = "DongBoDuLieu";
            this.DongBoDuLieu.Text = "Đồng bộ dữ liệu";
            // 
            // cmdImport
            // 
            this.cmdImport.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportNPL1,
            this.cmdImportSP1,
            this.cmdImportDM1,
            this.cmdImportToKhai1,
            this.cmdImportHangHoa1});
            this.cmdImport.Key = "cmdImport";
            this.cmdImport.Name = "cmdImport";
            this.cmdImport.Text = "Import from Excel";
            // 
            // cmdImportNPL1
            // 
            this.cmdImportNPL1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportNPL1.Icon")));
            this.cmdImportNPL1.Key = "cmdImportNPL";
            this.cmdImportNPL1.Name = "cmdImportNPL1";
            this.cmdImportNPL1.Text = "Nguyên phụ liệu";
            // 
            // cmdImportSP1
            // 
            this.cmdImportSP1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportSP1.Icon")));
            this.cmdImportSP1.Key = "cmdImportSP";
            this.cmdImportSP1.Name = "cmdImportSP1";
            this.cmdImportSP1.Text = "Sản phẩm";
            // 
            // cmdImportDM1
            // 
            this.cmdImportDM1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportDM1.Icon")));
            this.cmdImportDM1.Key = "cmdImportDM";
            this.cmdImportDM1.Name = "cmdImportDM1";
            this.cmdImportDM1.Text = "Định mức";
            // 
            // cmdImportToKhai1
            // 
            this.cmdImportToKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportToKhai1.Icon")));
            this.cmdImportToKhai1.Key = "cmdImportToKhai";
            this.cmdImportToKhai1.Name = "cmdImportToKhai1";
            this.cmdImportToKhai1.Text = "Tờ khai";
            // 
            // cmdImportHangHoa1
            // 
            this.cmdImportHangHoa1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportHangHoa1.Icon")));
            this.cmdImportHangHoa1.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa1.Name = "cmdImportHangHoa1";
            this.cmdImportHangHoa1.Text = "Hàng của tờ khai";
            // 
            // cmdImportNPL
            // 
            this.cmdImportNPL.Key = "cmdImportNPL";
            this.cmdImportNPL.Name = "cmdImportNPL";
            this.cmdImportNPL.Text = "Import NPL";
            // 
            // cmdImportSP
            // 
            this.cmdImportSP.Key = "cmdImportSP";
            this.cmdImportSP.Name = "cmdImportSP";
            this.cmdImportSP.Text = "Import Sản phẩm";
            // 
            // cmdImportDM
            // 
            this.cmdImportDM.Key = "cmdImportDM";
            this.cmdImportDM.Name = "cmdImportDM";
            this.cmdImportDM.Text = "Import Định mức";
            // 
            // cmdImportTTDM
            // 
            this.cmdImportTTDM.Key = "cmdImportTTDM";
            this.cmdImportTTDM.Name = "cmdImportTTDM";
            this.cmdImportTTDM.Text = "Import thông tin định mức";
            // 
            // cmdImportToKhai
            // 
            this.cmdImportToKhai.Key = "cmdImportToKhai";
            this.cmdImportToKhai.Name = "cmdImportToKhai";
            this.cmdImportToKhai.Text = "Import tờ khai";
            // 
            // cmdImportHangHoa
            // 
            this.cmdImportHangHoa.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa.Name = "cmdImportHangHoa";
            this.cmdImportHangHoa.Text = "Import hàng tờ khai";
            // 
            // Command1
            // 
            this.Command1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmd20071,
            this.cmd20031,
            this.cmdVN1,
            this.cmdEng1});
            this.Command1.Key = "Command1";
            this.Command1.Name = "Command1";
            this.Command1.Text = "Giao diện";
            // 
            // cmd20071
            // 
            this.cmd20071.Key = "cmd2007";
            this.cmd20071.Name = "cmd20071";
            // 
            // cmd20031
            // 
            this.cmd20031.Key = "cmd2003";
            this.cmd20031.Name = "cmd20031";
            // 
            // cmdVN1
            // 
            this.cmdVN1.ImageIndex = 11;
            this.cmdVN1.Key = "cmdVN";
            this.cmdVN1.Name = "cmdVN1";
            // 
            // cmdEng1
            // 
            this.cmdEng1.ImageIndex = 10;
            this.cmdEng1.Key = "cmdEng";
            this.cmdEng1.Name = "cmdEng1";
            // 
            // cmd2007
            // 
            this.cmd2007.Key = "cmd2007";
            this.cmd2007.Name = "cmd2007";
            this.cmd2007.Text = "Office 2007";
            // 
            // cmd2003
            // 
            this.cmd2003.Key = "cmd2003";
            this.cmd2003.Name = "cmd2003";
            this.cmd2003.Text = "Office 2003";
            // 
            // Command0
            // 
            this.Command0.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHelp1,
            this.cmdAutoUpdate1,
            this.Separator1,
            this.cmdActivate1,
            this.cmdAbout1});
            this.Command0.Key = "Command0";
            this.Command0.Name = "Command0";
            this.Command0.Text = "Trợ giúp";
            // 
            // cmdHelp1
            // 
            this.cmdHelp1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdHelp1.Icon")));
            this.cmdHelp1.Key = "cmdHelp";
            this.cmdHelp1.Name = "cmdHelp1";
            this.cmdHelp1.Text = "&Hướng dẫn sử dụng";
            // 
            // cmdAutoUpdate1
            // 
            this.cmdAutoUpdate1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAutoUpdate1.Icon")));
            this.cmdAutoUpdate1.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate1.Name = "cmdAutoUpdate1";
            this.cmdAutoUpdate1.Text = "&Cập nhật chương trình";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdActivate1
            // 
            this.cmdActivate1.ImageIndex = 9;
            this.cmdActivate1.Key = "cmdActivate";
            this.cmdActivate1.Name = "cmdActivate1";
            // 
            // cmdAbout1
            // 
            this.cmdAbout1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAbout1.Icon")));
            this.cmdAbout1.Key = "cmdAbout";
            this.cmdAbout1.Name = "cmdAbout1";
            this.cmdAbout1.Text = "&Thông tin sản phẩm";
            // 
            // cmdHelp
            // 
            this.cmdHelp.Key = "cmdHelp";
            this.cmdHelp.Name = "cmdHelp";
            this.cmdHelp.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.cmdHelp.Text = "Hướng dẫn sử dụng";
            // 
            // cmdAbout
            // 
            this.cmdAbout.Key = "cmdAbout";
            this.cmdAbout.Name = "cmdAbout";
            this.cmdAbout.Text = "Thông tin sản phẩm";
            // 
            // cmdNPLNhapTon
            // 
            this.cmdNPLNhapTon.Key = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Name = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Text = "NPL nhập tồn";
            // 
            // cmdDanhMuc
            // 
            this.cmdDanhMuc.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHaiQuan1,
            this.cmdNuoc1,
            this.cmdMaHS1,
            this.cmdNguyenTe1,
            this.cmdDVT1,
            this.cmdPTTT1,
            this.cmdPTVT1,
            this.cmdDKGH1,
            this.cmdCuaKhau1,
            this.cmdDMSPGC1});
            this.cmdDanhMuc.Key = "cmdDanhMuc";
            this.cmdDanhMuc.Name = "cmdDanhMuc";
            this.cmdDanhMuc.Text = "DanhMuc";
            // 
            // cmdHaiQuan1
            // 
            this.cmdHaiQuan1.ImageIndex = 5;
            this.cmdHaiQuan1.Key = "cmdHaiQuan";
            this.cmdHaiQuan1.Name = "cmdHaiQuan1";
            // 
            // cmdNuoc1
            // 
            this.cmdNuoc1.ImageIndex = 5;
            this.cmdNuoc1.Key = "cmdNuoc";
            this.cmdNuoc1.Name = "cmdNuoc1";
            // 
            // cmdMaHS1
            // 
            this.cmdMaHS1.ImageIndex = 5;
            this.cmdMaHS1.Key = "cmdMaHS";
            this.cmdMaHS1.Name = "cmdMaHS1";
            // 
            // cmdNguyenTe1
            // 
            this.cmdNguyenTe1.ImageIndex = 5;
            this.cmdNguyenTe1.Key = "cmdNguyenTe";
            this.cmdNguyenTe1.Name = "cmdNguyenTe1";
            // 
            // cmdDVT1
            // 
            this.cmdDVT1.ImageIndex = 5;
            this.cmdDVT1.Key = "cmdDVT";
            this.cmdDVT1.Name = "cmdDVT1";
            // 
            // cmdPTTT1
            // 
            this.cmdPTTT1.ImageIndex = 5;
            this.cmdPTTT1.Key = "cmdPTTT";
            this.cmdPTTT1.Name = "cmdPTTT1";
            // 
            // cmdPTVT1
            // 
            this.cmdPTVT1.ImageIndex = 5;
            this.cmdPTVT1.Key = "cmdPTVT";
            this.cmdPTVT1.Name = "cmdPTVT1";
            // 
            // cmdDKGH1
            // 
            this.cmdDKGH1.ImageIndex = 5;
            this.cmdDKGH1.Key = "cmdDKGH";
            this.cmdDKGH1.Name = "cmdDKGH1";
            // 
            // cmdCuaKhau1
            // 
            this.cmdCuaKhau1.ImageIndex = 5;
            this.cmdCuaKhau1.Key = "cmdCuaKhau";
            this.cmdCuaKhau1.Name = "cmdCuaKhau1";
            // 
            // cmdDMSPGC1
            // 
            this.cmdDMSPGC1.ImageIndex = 5;
            this.cmdDMSPGC1.Key = "cmdDMSPGC";
            this.cmdDMSPGC1.Name = "cmdDMSPGC1";
            // 
            // cmdMaHS
            // 
            this.cmdMaHS.Key = "cmdMaHS";
            this.cmdMaHS.Name = "cmdMaHS";
            this.cmdMaHS.Text = "Mã HS";
            // 
            // cmdNuoc
            // 
            this.cmdNuoc.Key = "cmdNuoc";
            this.cmdNuoc.Name = "cmdNuoc";
            this.cmdNuoc.Text = "Nước";
            // 
            // cmdHaiQuan
            // 
            this.cmdHaiQuan.Key = "cmdHaiQuan";
            this.cmdHaiQuan.Name = "cmdHaiQuan";
            this.cmdHaiQuan.Text = "Đơn vị Hải quan";
            // 
            // cmdNguyenTe
            // 
            this.cmdNguyenTe.Key = "cmdNguyenTe";
            this.cmdNguyenTe.Name = "cmdNguyenTe";
            this.cmdNguyenTe.Text = "Nguyên tệ";
            // 
            // cmdDVT
            // 
            this.cmdDVT.Key = "cmdDVT";
            this.cmdDVT.Name = "cmdDVT";
            this.cmdDVT.Text = "Đơn vị tính";
            // 
            // cmdPTTT
            // 
            this.cmdPTTT.Key = "cmdPTTT";
            this.cmdPTTT.Name = "cmdPTTT";
            this.cmdPTTT.Text = "Phương thức thanh toán";
            // 
            // cmdPTVT
            // 
            this.cmdPTVT.Key = "cmdPTVT";
            this.cmdPTVT.Name = "cmdPTVT";
            this.cmdPTVT.Text = "Phương thức vận tải";
            // 
            // cmdDKGH
            // 
            this.cmdDKGH.Key = "cmdDKGH";
            this.cmdDKGH.Name = "cmdDKGH";
            this.cmdDKGH.Text = "Điều kiện giao hàng";
            // 
            // cmdCuaKhau
            // 
            this.cmdCuaKhau.Key = "cmdCuaKhau";
            this.cmdCuaKhau.Name = "cmdCuaKhau";
            this.cmdCuaKhau.Text = "Cửa khẩu";
            // 
            // cmdBackUp
            // 
            this.cmdBackUp.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBackUp.Icon")));
            this.cmdBackUp.Key = "cmdBackUp";
            this.cmdBackUp.Name = "cmdBackUp";
            this.cmdBackUp.Text = "Sao lưu dữ liệu";
            // 
            // cmdRestore
            // 
            this.cmdRestore.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdRestore.Icon")));
            this.cmdRestore.Key = "cmdRestore";
            this.cmdRestore.Name = "cmdRestore";
            this.cmdRestore.Text = "Phục hồi dữ liệu";
            // 
            // ThongSoKetNoi
            // 
            this.ThongSoKetNoi.Icon = ((System.Drawing.Icon)(resources.GetObject("ThongSoKetNoi.Icon")));
            this.ThongSoKetNoi.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi.Name = "ThongSoKetNoi";
            this.ThongSoKetNoi.Text = "Thiết lập thông số kết nối";
            // 
            // TLThongTinDNHQ
            // 
            this.TLThongTinDNHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("TLThongTinDNHQ.Icon")));
            this.TLThongTinDNHQ.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Name = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Text = "Thiết lập thông tin doanh nghiệp và hải quan";
            // 
            // cmdThietLapIn
            // 
            this.cmdThietLapIn.Key = "cmdThietLapIn";
            this.cmdThietLapIn.Name = "cmdThietLapIn";
            this.cmdThietLapIn.Text = "Thiết lập thông số in báo cáo";
            // 
            // cmdExportExccel
            // 
            this.cmdExportExccel.Key = "cmdExportExccel";
            this.cmdExportExccel.Name = "cmdExportExccel";
            this.cmdExportExccel.Text = "Xuất dữ liệu ra file Excel";
            // 
            // cmdImportExcel
            // 
            this.cmdImportExcel.Key = "cmdImportExcel";
            this.cmdImportExcel.Name = "cmdImportExcel";
            this.cmdImportExcel.Text = "Nhập dữ liêu từ file Excel";
            // 
            // cmdDongBoPhongKhai
            // 
            this.cmdDongBoPhongKhai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportExcel1,
            this.cmdExportExccel1});
            this.cmdDongBoPhongKhai.Key = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Name = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Text = "Đồng bộ dữ liệu với phòng khai";
            // 
            // cmdImportExcel1
            // 
            this.cmdImportExcel1.Key = "cmdImportExcel";
            this.cmdImportExcel1.Name = "cmdImportExcel1";
            // 
            // cmdExportExccel1
            // 
            this.cmdExportExccel1.Key = "cmdExportExccel";
            this.cmdExportExccel1.Name = "cmdExportExccel1";
            // 
            // cmdCauHinh
            // 
            this.cmdCauHinh.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThongSoKetNoi1,
            this.TLThongTinDNHQ1,
            this.cmdCauHinhToKhai1,
            this.cmdThietLapIn1});
            this.cmdCauHinh.Key = "cmdCauHinh";
            this.cmdCauHinh.Name = "cmdCauHinh";
            this.cmdCauHinh.Text = "Cấu hình hệ thống";
            // 
            // ThongSoKetNoi1
            // 
            this.ThongSoKetNoi1.Icon = ((System.Drawing.Icon)(resources.GetObject("ThongSoKetNoi1.Icon")));
            this.ThongSoKetNoi1.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi1.Name = "ThongSoKetNoi1";
            this.ThongSoKetNoi1.Text = "Cấu hình thông số kết nối";
            // 
            // TLThongTinDNHQ1
            // 
            this.TLThongTinDNHQ1.Icon = ((System.Drawing.Icon)(resources.GetObject("TLThongTinDNHQ1.Icon")));
            this.TLThongTinDNHQ1.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ1.Name = "TLThongTinDNHQ1";
            this.TLThongTinDNHQ1.Text = "Cấu hình thông tin doanh nghiệp và hải quan";
            // 
            // cmdCauHinhToKhai1
            // 
            this.cmdCauHinhToKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCauHinhToKhai1.Icon")));
            this.cmdCauHinhToKhai1.Key = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai1.Name = "cmdCauHinhToKhai1";
            // 
            // cmdThietLapIn1
            // 
            this.cmdThietLapIn1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThietLapIn1.Icon")));
            this.cmdThietLapIn1.Key = "cmdThietLapIn";
            this.cmdThietLapIn1.Name = "cmdThietLapIn1";
            this.cmdThietLapIn1.Text = "Cấu hình thông số in báo cáo";
            // 
            // cmdCauHinhToKhai
            // 
            this.cmdCauHinhToKhai.Key = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Name = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Text = "Cấu hình tham số mặc định của tờ khai";
            // 
            // QuanTri
            // 
            this.QuanTri.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.QuanLyNguoiDung1,
            this.QuanLyNhom1});
            this.QuanTri.Key = "QuanTri";
            this.QuanTri.Name = "QuanTri";
            this.QuanTri.Text = "Quản trị";
            // 
            // QuanLyNguoiDung1
            // 
            this.QuanLyNguoiDung1.Icon = ((System.Drawing.Icon)(resources.GetObject("QuanLyNguoiDung1.Icon")));
            this.QuanLyNguoiDung1.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung1.Name = "QuanLyNguoiDung1";
            // 
            // QuanLyNhom1
            // 
            this.QuanLyNhom1.Icon = ((System.Drawing.Icon)(resources.GetObject("QuanLyNhom1.Icon")));
            this.QuanLyNhom1.Key = "QuanLyNhom";
            this.QuanLyNhom1.Name = "QuanLyNhom1";
            // 
            // QuanLyNguoiDung
            // 
            this.QuanLyNguoiDung.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Name = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Text = "Quản lý người dùng";
            // 
            // QuanLyNhom
            // 
            this.QuanLyNhom.Key = "QuanLyNhom";
            this.QuanLyNhom.Name = "QuanLyNhom";
            this.QuanLyNhom.Text = "Quản lý nhóm người dùng";
            // 
            // LoginUser
            // 
            this.LoginUser.Icon = ((System.Drawing.Icon)(resources.GetObject("LoginUser.Icon")));
            this.LoginUser.Key = "LoginUser";
            this.LoginUser.Name = "LoginUser";
            this.LoginUser.Text = "Đăng nhập người dùng khác";
            // 
            // cmdChangePass
            // 
            this.cmdChangePass.Key = "cmdChangePass";
            this.cmdChangePass.Name = "cmdChangePass";
            this.cmdChangePass.Text = "Đổi mật khẩu";
            // 
            // TraCuuMaHS
            // 
            this.TraCuuMaHS.Key = "TraCuuMaHS";
            this.TraCuuMaHS.Name = "TraCuuMaHS";
            this.TraCuuMaHS.Text = "Tra cứu mã HS";
            // 
            // cmdAutoUpdate
            // 
            this.cmdAutoUpdate.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate.Name = "cmdAutoUpdate";
            this.cmdAutoUpdate.Text = "Cập nhật chương trình";
            // 
            // NhapXML
            // 
            this.NhapXML.Key = "NhapXML";
            this.NhapXML.Name = "NhapXML";
            this.NhapXML.Text = "Nhập dữ liệu từ đại lý";
            // 
            // NhapHDXML
            // 
            this.NhapHDXML.Key = "NhapHDXML";
            this.NhapHDXML.Name = "NhapHDXML";
            this.NhapHDXML.Text = "Hợp đồng";
            // 
            // NhapDMXML
            // 
            this.NhapDMXML.Key = "NhapDMXML";
            this.NhapDMXML.Name = "NhapDMXML";
            this.NhapDMXML.Text = "Định mức";
            // 
            // NhapPKXML
            // 
            this.NhapPKXML.Key = "NhapPKXML";
            this.NhapPKXML.Name = "NhapPKXML";
            this.NhapPKXML.Text = "Phụ kiện";
            // 
            // XuatTKXML
            // 
            this.XuatTKXML.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.XuatTKMD1,
            this.XuatTKGCCT1});
            this.XuatTKXML.Key = "XuatTKXML";
            this.XuatTKXML.Name = "XuatTKXML";
            this.XuatTKXML.Text = "Tờ khai";
            // 
            // XuatTKMD1
            // 
            this.XuatTKMD1.Key = "XuatTKMD";
            this.XuatTKMD1.Name = "XuatTKMD1";
            // 
            // XuatTKGCCT1
            // 
            this.XuatTKGCCT1.Key = "XuatTKGCCT";
            this.XuatTKGCCT1.Name = "XuatTKGCCT1";
            // 
            // XuatTKMD
            // 
            this.XuatTKMD.Key = "XuatTKMD";
            this.XuatTKMD.Name = "XuatTKMD";
            this.XuatTKMD.Text = "Tờ khai mậu dịch";
            // 
            // XuatTKGCCT
            // 
            this.XuatTKGCCT.Key = "XuatTKGCCT";
            this.XuatTKGCCT.Name = "XuatTKGCCT";
            this.XuatTKGCCT.Text = "Tờ khai GCCT";
            // 
            // NhapTKXML
            // 
            this.NhapTKXML.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.NhapTKMD1,
            this.NhapTKGCCT1});
            this.NhapTKXML.Key = "NhapTKXML";
            this.NhapTKXML.Name = "NhapTKXML";
            this.NhapTKXML.Text = "Tờ khai";
            // 
            // NhapTKMD1
            // 
            this.NhapTKMD1.Key = "NhapTKMD";
            this.NhapTKMD1.Name = "NhapTKMD1";
            // 
            // NhapTKGCCT1
            // 
            this.NhapTKGCCT1.Key = "NhapTKGCCT";
            this.NhapTKGCCT1.Name = "NhapTKGCCT1";
            // 
            // NhapTKMD
            // 
            this.NhapTKMD.Key = "NhapTKMD";
            this.NhapTKMD.Name = "NhapTKMD";
            this.NhapTKMD.Text = "Tờ khai mậu dịch";
            // 
            // NhapTKGCCT
            // 
            this.NhapTKGCCT.Key = "NhapTKGCCT";
            this.NhapTKGCCT.Name = "NhapTKGCCT";
            this.NhapTKGCCT.Text = "Tờ khai GCCT";
            // 
            // cmdEnglish
            // 
            this.cmdEnglish.Key = "cmdEng";
            this.cmdEnglish.Name = "cmdEnglish";
            this.cmdEnglish.Text = "Tiếng Anh";
            // 
            // cmdVN
            // 
            this.cmdVN.Key = "cmdVN";
            this.cmdVN.Name = "cmdVN";
            this.cmdVN.Text = "Tiếng Việt";
            // 
            // cmdNhapXuat
            // 
            this.cmdNhapXuat.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.NhapXML2,
            this.cmdXuatDN1,
            this.cmdXuatDuLieuPhongKHai1,
            this.cmdNhapDuLieuTuDoangNghiep1});
            this.cmdNhapXuat.Key = "cmdNhapXuat";
            this.cmdNhapXuat.Name = "cmdNhapXuat";
            this.cmdNhapXuat.Text = "Nhập xuất";
            // 
            // NhapXML2
            // 
            this.NhapXML2.ImageIndex = 13;
            this.NhapXML2.Key = "NhapXML";
            this.NhapXML2.Name = "NhapXML2";
            // 
            // cmdXuatDN1
            // 
            this.cmdXuatDN1.ImageIndex = 14;
            this.cmdXuatDN1.Key = "cmdXuatDN";
            this.cmdXuatDN1.Name = "cmdXuatDN1";
            // 
            // cmdXuatDuLieuPhongKHai1
            // 
            this.cmdXuatDuLieuPhongKHai1.ImageIndex = 14;
            this.cmdXuatDuLieuPhongKHai1.Key = "cmdXuatDuLieuPhongKHai";
            this.cmdXuatDuLieuPhongKHai1.Name = "cmdXuatDuLieuPhongKHai1";
            // 
            // cmdNhapDuLieuTuDoangNghiep1
            // 
            this.cmdNhapDuLieuTuDoangNghiep1.ImageIndex = 13;
            this.cmdNhapDuLieuTuDoangNghiep1.Key = "cmdNhapDuLieuTuDoangNghiep";
            this.cmdNhapDuLieuTuDoangNghiep1.Name = "cmdNhapDuLieuTuDoangNghiep1";
            // 
            // cmdXuatDuLieuPhongKHai
            // 
            this.cmdXuatDuLieuPhongKHai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdXuatHopDong1,
            this.cmdXuatDinhMuc1,
            this.cmdXuatPhuKien1,
            this.cmdXuatToKhai1});
            this.cmdXuatDuLieuPhongKHai.Key = "cmdXuatDuLieuPhongKHai";
            this.cmdXuatDuLieuPhongKHai.Name = "cmdXuatDuLieuPhongKHai";
            this.cmdXuatDuLieuPhongKHai.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // cmdXuatHopDong1
            // 
            this.cmdXuatHopDong1.ImageIndex = 14;
            this.cmdXuatHopDong1.Key = "cmdXuatHopDong";
            this.cmdXuatHopDong1.Name = "cmdXuatHopDong1";
            // 
            // cmdXuatDinhMuc1
            // 
            this.cmdXuatDinhMuc1.ImageIndex = 14;
            this.cmdXuatDinhMuc1.Key = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc1.Name = "cmdXuatDinhMuc1";
            // 
            // cmdXuatPhuKien1
            // 
            this.cmdXuatPhuKien1.ImageIndex = 14;
            this.cmdXuatPhuKien1.Key = "cmdXuatPhuKien";
            this.cmdXuatPhuKien1.Name = "cmdXuatPhuKien1";
            // 
            // cmdXuatToKhai1
            // 
            this.cmdXuatToKhai1.ImageIndex = 14;
            this.cmdXuatToKhai1.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai1.Name = "cmdXuatToKhai1";
            // 
            // cmdXuatHopDong
            // 
            this.cmdXuatHopDong.Key = "cmdXuatHopDong";
            this.cmdXuatHopDong.Name = "cmdXuatHopDong";
            this.cmdXuatHopDong.Text = "Hợp đồng";
            // 
            // cmdXuatDinhMuc
            // 
            this.cmdXuatDinhMuc.Key = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc.Name = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc.Text = "Định mức";
            // 
            // cmdXuatPhuKien
            // 
            this.cmdXuatPhuKien.Key = "cmdXuatPhuKien";
            this.cmdXuatPhuKien.Name = "cmdXuatPhuKien";
            this.cmdXuatPhuKien.Text = "Phụ kiện";
            // 
            // cmdXuatToKhai
            // 
            this.cmdXuatToKhai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.XuatTKGCCT2,
            this.XuatTKMD2});
            this.cmdXuatToKhai.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai.Name = "cmdXuatToKhai";
            this.cmdXuatToKhai.Text = "Tờ khai";
            // 
            // XuatTKGCCT2
            // 
            this.XuatTKGCCT2.ImageIndex = 14;
            this.XuatTKGCCT2.Key = "XuatTKGCCT";
            this.XuatTKGCCT2.Name = "XuatTKGCCT2";
            // 
            // XuatTKMD2
            // 
            this.XuatTKMD2.ImageIndex = 14;
            this.XuatTKMD2.Key = "XuatTKMD";
            this.XuatTKMD2.Name = "XuatTKMD2";
            // 
            // cmdActivate
            // 
            this.cmdActivate.Key = "cmdActivate";
            this.cmdActivate.Name = "cmdActivate";
            this.cmdActivate.Shortcut = System.Windows.Forms.Shortcut.CtrlF10;
            this.cmdActivate.Text = "Kích hoạt phần mềm";
            // 
            // cmdDMSPGC
            // 
            this.cmdDMSPGC.Key = "cmdDMSPGC";
            this.cmdDMSPGC.Name = "cmdDMSPGC";
            this.cmdDMSPGC.Text = "Danh mục sản phẩm gia công";
            // 
            // cmdNhapDuLieuDaiLy
            // 
            this.cmdNhapDuLieuDaiLy.Key = "cmdNhapDuLieuDaiLy";
            this.cmdNhapDuLieuDaiLy.Name = "cmdNhapDuLieuDaiLy";
            this.cmdNhapDuLieuDaiLy.Text = "Nhập dữ liệu từ đại lý";
            // 
            // cmdCloseMe
            // 
            this.cmdCloseMe.Key = "cmdCloseMe";
            this.cmdCloseMe.Name = "cmdCloseMe";
            this.cmdCloseMe.Text = "Đóng cửa sổ này";
            // 
            // cmdCloseAllButMe
            // 
            this.cmdCloseAllButMe.Key = "cmdCloseAllButMe";
            this.cmdCloseAllButMe.Name = "cmdCloseAllButMe";
            this.cmdCloseAllButMe.Text = "Đóng các cửa sổ khác";
            // 
            // cmdCloseAll
            // 
            this.cmdCloseAll.Key = "cmdCloseAll";
            this.cmdCloseAll.Name = "cmdCloseAll";
            this.cmdCloseAll.Text = "Đóng hết các cửa sổ";
            // 
            // cmdXuatDN
            // 
            this.cmdXuatDN.Key = "cmdXuatDN";
            this.cmdXuatDN.Name = "cmdXuatDN";
            this.cmdXuatDN.Text = "Xuất dữ liệu cho doanh nghiệp";
            // 
            // QuanLyMess
            // 
            this.QuanLyMess.Key = "QuanLyMess";
            this.QuanLyMess.Name = "QuanLyMess";
            this.QuanLyMess.Text = "Quản lý message khai báo";
            // 
            // cmdNhapDuLieuTuDoangNghiep
            // 
            this.cmdNhapDuLieuTuDoangNghiep.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNhapHopDong1,
            this.cmdNhapDinhMuc1,
            this.cmdNhapPhuKien1,
            this.cmdNhapToKhai1});
            this.cmdNhapDuLieuTuDoangNghiep.Key = "cmdNhapDuLieuTuDoangNghiep";
            this.cmdNhapDuLieuTuDoangNghiep.Name = "cmdNhapDuLieuTuDoangNghiep";
            this.cmdNhapDuLieuTuDoangNghiep.Text = "Nhập dữ liệu từ Doanh Nghiệp";
            // 
            // cmdNhapHopDong1
            // 
            this.cmdNhapHopDong1.ImageIndex = 13;
            this.cmdNhapHopDong1.Key = "cmdNhapHopDong";
            this.cmdNhapHopDong1.Name = "cmdNhapHopDong1";
            // 
            // cmdNhapDinhMuc1
            // 
            this.cmdNhapDinhMuc1.ImageIndex = 13;
            this.cmdNhapDinhMuc1.Key = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc1.Name = "cmdNhapDinhMuc1";
            // 
            // cmdNhapPhuKien1
            // 
            this.cmdNhapPhuKien1.ImageIndex = 13;
            this.cmdNhapPhuKien1.Key = "cmdNhapPhuKien";
            this.cmdNhapPhuKien1.Name = "cmdNhapPhuKien1";
            // 
            // cmdNhapToKhai1
            // 
            this.cmdNhapToKhai1.ImageIndex = 13;
            this.cmdNhapToKhai1.Key = "cmdNhapToKhai";
            this.cmdNhapToKhai1.Name = "cmdNhapToKhai1";
            // 
            // cmdNhapHopDong
            // 
            this.cmdNhapHopDong.Key = "cmdNhapHopDong";
            this.cmdNhapHopDong.Name = "cmdNhapHopDong";
            this.cmdNhapHopDong.Text = "Hợp đồng";
            // 
            // cmdNhapDinhMuc
            // 
            this.cmdNhapDinhMuc.Key = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc.Name = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc.Text = "Định mức";
            // 
            // cmdNhapPhuKien
            // 
            this.cmdNhapPhuKien.Key = "cmdNhapPhuKien";
            this.cmdNhapPhuKien.Name = "cmdNhapPhuKien";
            this.cmdNhapPhuKien.Text = "Phụ kiện";
            // 
            // cmdNhapToKhai
            // 
            this.cmdNhapToKhai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNhapToKhaiGCCT1,
            this.cmdNhapToKhaiMauDich1});
            this.cmdNhapToKhai.Key = "cmdNhapToKhai";
            this.cmdNhapToKhai.Name = "cmdNhapToKhai";
            this.cmdNhapToKhai.Text = "Tờ khai";
            // 
            // cmdNhapToKhaiGCCT1
            // 
            this.cmdNhapToKhaiGCCT1.ImageIndex = 13;
            this.cmdNhapToKhaiGCCT1.Key = "cmdNhapToKhaiGCCT";
            this.cmdNhapToKhaiGCCT1.Name = "cmdNhapToKhaiGCCT1";
            // 
            // cmdNhapToKhaiMauDich1
            // 
            this.cmdNhapToKhaiMauDich1.ImageIndex = 13;
            this.cmdNhapToKhaiMauDich1.Key = "cmdNhapToKhaiMauDich";
            this.cmdNhapToKhaiMauDich1.Name = "cmdNhapToKhaiMauDich1";
            // 
            // cmdNhapToKhaiMauDich
            // 
            this.cmdNhapToKhaiMauDich.Key = "cmdNhapToKhaiMauDich";
            this.cmdNhapToKhaiMauDich.Name = "cmdNhapToKhaiMauDich";
            this.cmdNhapToKhaiMauDich.Text = "Tờ khai mậu dịch";
            // 
            // cmdNhapToKhaiGCCT
            // 
            this.cmdNhapToKhaiGCCT.Key = "cmdNhapToKhaiGCCT";
            this.cmdNhapToKhaiGCCT.Name = "cmdNhapToKhaiGCCT";
            this.cmdNhapToKhaiGCCT.Text = "Tờ khai gia công chuyển tiếp";
            // 
            // cmdTLTTDN
            // 
            this.cmdTLTTDN.Key = "cmdTLTTDN";
            this.cmdTLTTDN.Name = "cmdTLTTDN";
            this.cmdTLTTDN.Text = "Thiêt lập thông tin Doanh Nghiệp";
            // 
            // mnuQuerySQL
            // 
            this.mnuQuerySQL.Key = "mnuQuerySQL";
            this.mnuQuerySQL.Name = "mnuQuerySQL";
            this.mnuQuerySQL.Text = "Thực hiện truy vấn SQL";
            // 
            // cmdLog
            // 
            this.cmdLog.Key = "cmdLog";
            this.cmdLog.Name = "cmdLog";
            this.cmdLog.Text = "Nhật ký chương trình";
            // 
            // cmdDataVersion
            // 
            this.cmdDataVersion.Key = "cmdDataVersion";
            this.cmdDataVersion.Name = "cmdDataVersion";
            this.cmdDataVersion.Text = "Dữ liệu phiên bản: ?";
            // 
            // mnuRightClick
            // 
            this.mnuRightClick.CommandManager = this.cmMain;
            this.mnuRightClick.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCloseMe1,
            this.Separator2,
            this.cmdCloseAllButMe1,
            this.cmdCloseAll1});
            this.mnuRightClick.Key = "mnuRightClick";
            // 
            // cmdCloseMe1
            // 
            this.cmdCloseMe1.Key = "cmdCloseMe";
            this.cmdCloseMe1.Name = "cmdCloseMe1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdCloseAllButMe1
            // 
            this.cmdCloseAllButMe1.Key = "cmdCloseAllButMe";
            this.cmdCloseAllButMe1.Name = "cmdCloseAllButMe1";
            // 
            // cmdCloseAll1
            // 
            this.cmdCloseAll1.Key = "cmdCloseAll";
            this.cmdCloseAll1.Name = "cmdCloseAll1";
            // 
            // ilSmall
            // 
            this.ilSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmall.ImageStream")));
            this.ilSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmall.Images.SetKeyName(0, "explorerBarItem1.Icon.ico");
            this.ilSmall.Images.SetKeyName(1, "explorerBarItem2.Icon.ico");
            this.ilSmall.Images.SetKeyName(2, "explorerBarItem3.Icon.ico");
            this.ilSmall.Images.SetKeyName(3, "explorerBarItem10.Icon.ico");
            this.ilSmall.Images.SetKeyName(4, "explorerBarItem11.Icon.ico");
            this.ilSmall.Images.SetKeyName(5, "folder_page.png");
            this.ilSmall.Images.SetKeyName(6, "shell32_279.ico");
            this.ilSmall.Images.SetKeyName(7, "shell32_21.ico");
            this.ilSmall.Images.SetKeyName(8, "RightDatabase32.gif");
            this.ilSmall.Images.SetKeyName(9, "key.png");
            this.ilSmall.Images.SetKeyName(10, "en-US.gif");
            this.ilSmall.Images.SetKeyName(11, "vi-VN.gif");
            this.ilSmall.Images.SetKeyName(12, "application_view_tile.png");
            this.ilSmall.Images.SetKeyName(13, "import.png");
            this.ilSmall.Images.SetKeyName(14, "export.png");
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 24);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 494);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbMenu);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(690, 26);
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdExportExcel1.Icon")));
            this.cmdExportExcel1.Key = "cmdExportExccel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // cmdThoat1
            // 
            this.cmdThoat1.Key = "cmdThoat";
            this.cmdThoat1.Name = "cmdThoat1";
            // 
            // pmMain
            // 
            this.pmMain.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(247)))));
            this.pmMain.ContainerControl = this;
            this.pmMain.DefaultPanelSettings.ActiveCaptionMode = Janus.Windows.UI.Dock.ActiveCaptionMode.Never;
            this.pmMain.DefaultPanelSettings.CaptionDisplayMode = Janus.Windows.UI.Dock.PanelCaptionDisplayMode.Text;
            this.pmMain.DefaultPanelSettings.CaptionHeight = 30;
            this.pmMain.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.pmMain.DefaultPanelSettings.DarkCaptionFormatStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.pmMain.DefaultPanelSettings.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.Window;
            this.pmMain.DefaultPanelSettings.TabStateStyles.FormatStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pmMain.TabbedMdi = true;
            this.pmMain.TabbedMdiSettings.TabStateStyles.SelectedFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.pmMain.Tag = null;
            this.pmMain.MdiTabMouseDown += new Janus.Windows.UI.Dock.MdiTabMouseEventHandler(this.pmMain_MdiTabMouseDown);
            this.uiPanel0.Id = new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("61981317-cb9d-40d9-9bdb-f3341c4a539c");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.uiPanel2.Id = new System.Guid("5671da6c-3bdd-401e-9876-b3de34ebbd02");
            this.uiPanel0.Panels.Add(this.uiPanel2);
            this.pmMain.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.pmMain.BeginPanelInfo();
            this.pmMain.AddDockPanelInfo(new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, Janus.Windows.UI.Dock.PanelDockStyle.Left, true, new System.Drawing.Size(199, 391), true);
            this.pmMain.AddDockPanelInfo(new System.Guid("61981317-cb9d-40d9-9bdb-f3341c4a539c"), new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("5671da6c-3bdd-401e-9876-b3de34ebbd02"), new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), -1, true);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("821fe19b-f1a8-4d50-828d-141a7ec30bdc"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("61981317-cb9d-40d9-9bdb-f3341c4a539c"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("5671da6c-3bdd-401e-9876-b3de34ebbd02"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.GroupStyle = Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator;
            this.uiPanel0.Location = new System.Drawing.Point(3, 29);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.SelectedPanel = this.uiPanel1;
            this.uiPanel0.Size = new System.Drawing.Size(199, 391);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Panel 0";
            this.uiPanel0.SelectedPanelChanged += new Janus.Windows.UI.Dock.PanelActionEventHandler(this.uiPanel0_SelectedPanelChanged);
            // 
            // uiPanel1
            // 
            this.uiPanel1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiPanel1.Icon")));
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(195, 289);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Loại hình GC";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.explorerBar1);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(193, 258);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // explorerBar1
            // 
            this.explorerBar1.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar1.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar1.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBar1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem1.ImageIndex = 0;
            explorerBarItem1.Key = "hdgcNhap";
            explorerBarItem1.Text = "Khai báo";
            explorerBarItem2.ImageIndex = 1;
            explorerBarItem2.Key = "hdgcManage";
            explorerBarItem2.Text = "Theo dõi";
            explorerBarItem3.ImageIndex = 2;
            explorerBarItem3.Key = "hdgcRegisted";
            explorerBarItem3.Text = "Đã đăng ký";
            explorerBarGroup1.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem1,
            explorerBarItem2,
            explorerBarItem3});
            explorerBarGroup1.Key = "grpHopDong";
            explorerBarGroup1.Text = "Hợp đồng";
            explorerBarItem4.ImageIndex = 0;
            explorerBarItem4.Key = "dmSend";
            explorerBarItem4.Text = "Khai báo";
            explorerBarItem5.ImageIndex = 1;
            explorerBarItem5.Key = "dmManage";
            explorerBarItem5.Text = "Theo dõi";
            explorerBarItem6.ImageIndex = 2;
            explorerBarItem6.Key = "dmRegisted";
            explorerBarItem6.Text = "Đã đăng ký";
            explorerBarGroup2.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem4,
            explorerBarItem5,
            explorerBarItem6});
            explorerBarGroup2.Key = "grpDinhMuc";
            explorerBarGroup2.Text = "Định mức";
            explorerBarItem7.ImageIndex = 0;
            explorerBarItem7.Key = "pkgcNhap";
            explorerBarItem7.Text = "Khai báo";
            explorerBarItem8.ImageIndex = 1;
            explorerBarItem8.Key = "pkgcManage";
            explorerBarItem8.Text = "Theo dõi";
            explorerBarItem9.ImageIndex = 2;
            explorerBarItem9.Key = "pkgcRegisted";
            explorerBarItem9.Text = "Đã đăng ký";
            explorerBarGroup3.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem7,
            explorerBarItem8,
            explorerBarItem9});
            explorerBarGroup3.Key = "grpPhuKien";
            explorerBarGroup3.Text = "Phụ kiện";
            explorerBarItem10.ImageIndex = 3;
            explorerBarItem10.Key = "tkNhapKhau_GC";
            explorerBarItem10.Text = "Nhập khẩu";
            explorerBarItem11.ImageIndex = 4;
            explorerBarItem11.Key = "tkXuatKhau_GC";
            explorerBarItem11.Text = "Xuất khẩu";
            explorerBarItem12.ImageIndex = 1;
            explorerBarItem12.Key = "tkTheoDoi";
            explorerBarItem12.Text = "Theo dõi";
            explorerBarItem13.ImageIndex = 2;
            explorerBarItem13.Key = "tkDaDangKy";
            explorerBarItem13.Text = "Đã đăng ký";
            explorerBarGroup4.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem10,
            explorerBarItem11,
            explorerBarItem12,
            explorerBarItem13});
            explorerBarGroup4.Key = "grpToKhai";
            explorerBarGroup4.Text = "Tờ khai có Hợp đồng";
            explorerBarItem14.ImageIndex = 3;
            explorerBarItem14.Key = "tkGCCTNhap";
            explorerBarItem14.Text = "Tờ khai nhập";
            explorerBarItem15.ImageIndex = 4;
            explorerBarItem15.Key = "tkGCCTXuat";
            explorerBarItem15.Text = "Tờ khai xuất";
            explorerBarItem16.ImageIndex = 1;
            explorerBarItem16.Key = "theodoiTKCT";
            explorerBarItem16.Text = "Theo dõi";
            explorerBarItem17.ImageIndex = 2;
            explorerBarItem17.Key = "tkGCCTDaDangKy";
            explorerBarItem17.Text = "Đã đăng ký";
            explorerBarGroup5.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem14,
            explorerBarItem15,
            explorerBarItem16,
            explorerBarItem17});
            explorerBarGroup5.Key = "grpGCCT";
            explorerBarGroup5.Text = "Tờ khai Gia công chuyển tiếp";
            explorerBarItem18.ImageIndex = 0;
            explorerBarItem18.Key = "dmCUKhaibao";
            explorerBarItem18.Text = "Khai báo";
            explorerBarItem19.ImageIndex = 1;
            explorerBarItem19.Key = "dmCUTheoDoi";
            explorerBarItem19.Text = "Theo dõi";
            explorerBarItem20.ImageIndex = 2;
            explorerBarItem20.Key = "dmCUDaDangKy";
            explorerBarItem20.Text = "Đã đăng ký";
            explorerBarGroup6.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem18,
            explorerBarItem19,
            explorerBarItem20});
            explorerBarGroup6.Key = "DinhMucCU";
            explorerBarGroup6.Text = "NPL tự cung ứng";
            explorerBarGroup7.Expanded = false;
            explorerBarItem21.ImageIndex = 3;
            explorerBarItem21.Key = "tkNhapKoHD";
            explorerBarItem21.Text = "Tờ khai nhập";
            explorerBarItem22.ImageIndex = 4;
            explorerBarItem22.Key = "tkXuatKoHD";
            explorerBarItem22.Text = "Tờ khai xuất";
            explorerBarItem23.ImageIndex = 1;
            explorerBarItem23.Key = "tkKoHDManage";
            explorerBarItem23.Text = "Theo dõi";
            explorerBarItem24.ImageIndex = 2;
            explorerBarItem24.Key = "tkKoHDRegisted";
            explorerBarItem24.Text = "Đã đăng ký";
            explorerBarGroup7.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem21,
            explorerBarItem22,
            explorerBarItem23,
            explorerBarItem24});
            explorerBarGroup7.Key = "grpToKhaiKoCoHD";
            explorerBarGroup7.Text = "Tờ khai không có HĐ";
            explorerBarGroup7.Visible = false;
            this.explorerBar1.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup1,
            explorerBarGroup2,
            explorerBarGroup3,
            explorerBarGroup4,
            explorerBarGroup5,
            explorerBarGroup6,
            explorerBarGroup7});
            this.explorerBar1.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar1.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBar1.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.explorerBar1.ImageList = this.ilSmall;
            this.explorerBar1.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBar1.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBar1.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBar1.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBar1.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBar1.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar1.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBar1.Location = new System.Drawing.Point(0, 0);
            this.explorerBar1.Name = "explorerBar1";
            this.explorerBar1.Size = new System.Drawing.Size(193, 258);
            this.explorerBar1.TabIndex = 0;
            this.explorerBar1.Text = "explorerBar2";
            this.explorerBar1.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2003;
            this.explorerBar1.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expGiaCong_ItemClick);
            // 
            // uiPanel2
            // 
            this.uiPanel2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiPanel2.Icon")));
            this.uiPanel2.InnerContainer = this.uiPanel2Container;
            this.uiPanel2.Location = new System.Drawing.Point(0, 0);
            this.uiPanel2.Name = "uiPanel2";
            this.uiPanel2.Size = new System.Drawing.Size(195, 289);
            this.uiPanel2.TabIndex = 4;
            this.uiPanel2.Text = "Theo dõi phân bổ";
            // 
            // uiPanel2Container
            // 
            this.uiPanel2Container.Controls.Add(this.explorerBar2);
            this.uiPanel2Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel2Container.Name = "uiPanel2Container";
            this.uiPanel2Container.Size = new System.Drawing.Size(193, 258);
            this.uiPanel2Container.TabIndex = 0;
            // 
            // explorerBar2
            // 
            this.explorerBar2.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar2.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar2.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.explorerBar2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.explorerBar2.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem25.Key = "ThucHienPhanBo";
            explorerBarItem25.Text = "Phân bổ tờ khai xuất";
            explorerBarItem26.Key = "TheoDoiPhanBo";
            explorerBarItem26.Text = "Theo dõi phân bổ";
            explorerBarItem26.Visible = false;
            explorerBarGroup8.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem25,
            explorerBarItem26});
            explorerBarGroup8.Key = "grpTKLoaiHinhKhac";
            explorerBarGroup8.Text = "Quản lý phân bổ tờ khai xuất";
            this.explorerBar2.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup8});
            this.explorerBar2.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.explorerBar2.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.explorerBar2.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.explorerBar2.ImageSize = new System.Drawing.Size(16, 16);
            this.explorerBar2.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.explorerBar2.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.explorerBar2.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.explorerBar2.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.explorerBar2.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.explorerBar2.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.explorerBar2.Location = new System.Drawing.Point(0, 0);
            this.explorerBar2.Name = "explorerBar2";
            this.explorerBar2.Size = new System.Drawing.Size(193, 258);
            this.explorerBar2.TabIndex = 2;
            this.explorerBar2.Text = "explorerBar1";
            this.explorerBar2.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2003;
            this.explorerBar2.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expKhaiBao_TheoDoi_ItemClick);
            // 
            // pnlSXXK
            // 
            this.pnlSXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSXXK.Icon")));
            this.pnlSXXK.InnerContainer = this.pnlSXXKContainer;
            this.pnlSXXK.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXK.Name = "pnlSXXK";
            this.pnlSXXK.Size = new System.Drawing.Size(209, 239);
            this.pnlSXXK.TabIndex = 4;
            this.pnlSXXK.Text = "Loại hình SXXK";
            // 
            // pnlSXXKContainer
            // 
            this.pnlSXXKContainer.Controls.Add(this.expSXXK);
            this.pnlSXXKContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXKContainer.Name = "pnlSXXKContainer";
            this.pnlSXXKContainer.Size = new System.Drawing.Size(209, 239);
            this.pnlSXXKContainer.TabIndex = 0;
            // 
            // expSXXK
            // 
            this.expSXXK.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expSXXK.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expSXXK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expSXXK.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarGroup9.Expanded = false;
            explorerBarItem27.Key = "nplKhaiBao";
            explorerBarItem27.Text = "Khai báo ";
            explorerBarItem28.Key = "nplTheoDoi";
            explorerBarItem28.Text = "Theo dõi";
            explorerBarItem29.Key = "nplDaDangKy";
            explorerBarItem29.Text = "Đã đăng ký";
            explorerBarGroup9.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem27,
            explorerBarItem28,
            explorerBarItem29});
            explorerBarGroup9.Key = "grpNguyenPhuLieu";
            explorerBarGroup9.Text = "Nguyên phụ liệu";
            explorerBarGroup10.Expanded = false;
            explorerBarItem30.Key = "spKhaiBao";
            explorerBarItem30.Text = "Khai báo";
            explorerBarItem31.Key = "spTheoDoi";
            explorerBarItem31.Text = "Theo dõi";
            explorerBarItem32.Key = "spDaDangKy";
            explorerBarItem32.Text = "Đã đăng ký";
            explorerBarGroup10.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem30,
            explorerBarItem31,
            explorerBarItem32});
            explorerBarGroup10.Key = "grpSanPham";
            explorerBarGroup10.Text = "Sản phẩm";
            explorerBarItem33.Key = "dmKhaiBao";
            explorerBarItem33.Text = "Khai báo";
            explorerBarItem34.Key = "dmTheoDoi";
            explorerBarItem34.Text = "Theo dõi";
            explorerBarItem35.Key = "dmDaDangKy";
            explorerBarItem35.Text = "Đã đăng ký";
            explorerBarGroup11.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem33,
            explorerBarItem34,
            explorerBarItem35});
            explorerBarGroup11.Key = "grpDinhMuc";
            explorerBarGroup11.Text = "Định mức";
            explorerBarItem36.Key = "tkNhapKhau";
            explorerBarItem36.Text = "Nhập khẩu";
            explorerBarItem37.Key = "tkXuatKhau";
            explorerBarItem37.Text = "Xuất khẩu";
            explorerBarItem38.Key = "TheoDoiTKSXXK";
            explorerBarItem38.Text = "Theo dõi ";
            explorerBarItem39.Key = "ToKhaiSXXKDangKy";
            explorerBarItem39.Text = "Đã đăng ký";
            explorerBarItem40.Key = "tkHetHan";
            explorerBarItem40.Text = "Săp hết hạn TK";
            explorerBarGroup12.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem36,
            explorerBarItem37,
            explorerBarItem38,
            explorerBarItem39,
            explorerBarItem40});
            explorerBarGroup12.Key = "grpToKhai";
            explorerBarGroup12.Text = "Tờ khai";
            explorerBarItem41.Key = "AddHSTL";
            explorerBarItem41.Text = "Tạo mới";
            explorerBarItem42.Key = "UpdateHSTL";
            explorerBarItem42.Text = "Cập nhật";
            explorerBarItem43.Key = "HSTLManage";
            explorerBarItem43.Text = "Theo dõi";
            explorerBarItem44.Key = "HSTLClosed";
            explorerBarItem44.Text = "Đã đóng";
            explorerBarGroup13.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem41,
            explorerBarItem42,
            explorerBarItem43,
            explorerBarItem44});
            explorerBarGroup13.Key = "grpThanhLy";
            explorerBarGroup13.Text = "Hồ sơ thanh lý";
            this.expSXXK.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup9,
            explorerBarGroup10,
            explorerBarGroup11,
            explorerBarGroup12,
            explorerBarGroup13});
            this.expSXXK.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expSXXK.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expSXXK.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expSXXK.ImageSize = new System.Drawing.Size(16, 16);
            this.expSXXK.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expSXXK.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expSXXK.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expSXXK.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expSXXK.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expSXXK.Location = new System.Drawing.Point(0, 0);
            this.expSXXK.Name = "expSXXK";
            this.expSXXK.Size = new System.Drawing.Size(209, 239);
            this.expSXXK.TabIndex = 0;
            this.expSXXK.Text = "explorerBar1";
            this.expSXXK.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expSXXK.VisualStyleManager = this.vsmMain;
            this.expSXXK.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expSXXK_ItemClick);
            // 
            // pnlGiaCong
            // 
            this.pnlGiaCong.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlGiaCong.Icon")));
            this.pnlGiaCong.InnerContainer = this.pnlGiaCongContainer;
            this.pnlGiaCong.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCong.Name = "pnlGiaCong";
            this.pnlGiaCong.Size = new System.Drawing.Size(209, 239);
            this.pnlGiaCong.TabIndex = 4;
            this.pnlGiaCong.Text = "Loại hình gia công";
            // 
            // pnlGiaCongContainer
            // 
            this.pnlGiaCongContainer.Controls.Add(this.expGiaCong);
            this.pnlGiaCongContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCongContainer.Name = "pnlGiaCongContainer";
            this.pnlGiaCongContainer.Size = new System.Drawing.Size(209, 239);
            this.pnlGiaCongContainer.TabIndex = 0;
            // 
            // expGiaCong
            // 
            this.expGiaCong.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expGiaCong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expGiaCong.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem45.Key = "hdgcNhap";
            explorerBarItem45.Text = "Khai báo";
            explorerBarItem46.Key = "hdgcManage";
            explorerBarItem46.Text = "Theo dõi";
            explorerBarItem47.Key = "hdgcRegisted";
            explorerBarItem47.Text = "Đã đăng ký";
            explorerBarGroup14.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem45,
            explorerBarItem46,
            explorerBarItem47});
            explorerBarGroup14.Key = "grpHopDong";
            explorerBarGroup14.Text = "Hợp đồng";
            explorerBarItem48.Key = "dmSend";
            explorerBarItem48.Text = "Khai báo";
            explorerBarItem49.Key = "dmManage";
            explorerBarItem49.Text = "Theo dõi";
            explorerBarItem50.Key = "dmRegisted";
            explorerBarItem50.Text = "Đã đăng ký";
            explorerBarGroup15.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem48,
            explorerBarItem49,
            explorerBarItem50});
            explorerBarGroup15.Key = "grpDinhMuc";
            explorerBarGroup15.Text = "Định mức";
            explorerBarItem51.Key = "pkgcNhap";
            explorerBarItem51.Text = "Khai báo";
            explorerBarItem52.Key = "pkgcManage";
            explorerBarItem52.Text = "Theo dõi";
            explorerBarItem53.Key = "pkgcRegisted";
            explorerBarItem53.Text = "Đã đăng ký";
            explorerBarGroup16.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem51,
            explorerBarItem52,
            explorerBarItem53});
            explorerBarGroup16.Key = "grpPhuKien";
            explorerBarGroup16.Text = "Phụ kiện";
            explorerBarItem54.Key = "tkNhapKhau_GC";
            explorerBarItem54.Text = "Nhập khẩu";
            explorerBarItem55.Key = "tkXuatKhau_GC";
            explorerBarItem55.Text = "Xuất khẩu";
            explorerBarGroup17.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem54,
            explorerBarItem55});
            explorerBarGroup17.Key = "grpToKhai";
            explorerBarGroup17.Text = "Tờ khai";
            explorerBarItem56.Key = "tkGCCTNhap";
            explorerBarItem56.Text = "Tờ khai GCCT nhập";
            explorerBarItem57.Key = "tkGCCTXuat";
            explorerBarItem57.Text = "Tờ khai GCCT xuất";
            explorerBarItem58.Key = "theodoiTKCT";
            explorerBarItem58.Text = "Theo dõi";
            explorerBarGroup18.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem56,
            explorerBarItem57,
            explorerBarItem58});
            explorerBarGroup18.Key = "grpGCCT";
            explorerBarGroup18.Text = "Tờ khai GCCT";
            this.expGiaCong.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup14,
            explorerBarGroup15,
            explorerBarGroup16,
            explorerBarGroup17,
            explorerBarGroup18});
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expGiaCong.ImageSize = new System.Drawing.Size(16, 16);
            this.expGiaCong.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expGiaCong.Location = new System.Drawing.Point(0, 0);
            this.expGiaCong.Name = "expGiaCong";
            this.expGiaCong.Size = new System.Drawing.Size(209, 239);
            this.expGiaCong.TabIndex = 1;
            this.expGiaCong.Text = "explorerBar2";
            this.expGiaCong.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expGiaCong.VisualStyleManager = this.vsmMain;
            this.expGiaCong.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expGiaCong_ItemClick);
            // 
            // pnlKinhDoanh
            // 
            this.pnlKinhDoanh.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlKinhDoanh.Icon")));
            this.pnlKinhDoanh.InnerContainer = this.pnlKinhDoanhContainer;
            this.pnlKinhDoanh.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanh.Name = "pnlKinhDoanh";
            this.pnlKinhDoanh.Size = new System.Drawing.Size(209, 239);
            this.pnlKinhDoanh.TabIndex = 4;
            this.pnlKinhDoanh.Text = "Loại hình kinh doanh";
            // 
            // pnlKinhDoanhContainer
            // 
            this.pnlKinhDoanhContainer.Controls.Add(this.expKD);
            this.pnlKinhDoanhContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanhContainer.Name = "pnlKinhDoanhContainer";
            this.pnlKinhDoanhContainer.Size = new System.Drawing.Size(209, 239);
            this.pnlKinhDoanhContainer.TabIndex = 0;
            // 
            // expKD
            // 
            this.expKD.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expKD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expKD.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem59.Key = "tkNhapKhau_KD";
            explorerBarItem59.Text = "Nhập khẩu";
            explorerBarItem60.Key = "tkXuatKhau_KD";
            explorerBarItem60.Text = "Xuất khẩu";
            explorerBarGroup19.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem59,
            explorerBarItem60});
            explorerBarGroup19.Key = "grpToKhai";
            explorerBarGroup19.Text = "Tờ khai";
            this.expKD.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup19});
            this.expKD.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expKD.ImageSize = new System.Drawing.Size(16, 16);
            this.expKD.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expKD.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expKD.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expKD.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expKD.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expKD.Location = new System.Drawing.Point(0, 0);
            this.expKD.Name = "expKD";
            this.expKD.Size = new System.Drawing.Size(209, 239);
            this.expKD.TabIndex = 1;
            this.expKD.Text = "explorerBar1";
            this.expKD.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expKD.VisualStyleManager = this.vsmMain;
            // 
            // pnlDauTu
            // 
            this.pnlDauTu.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlDauTu.Icon")));
            this.pnlDauTu.InnerContainer = this.pnlDauTuContainer;
            this.pnlDauTu.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTu.Name = "pnlDauTu";
            this.pnlDauTu.Size = new System.Drawing.Size(209, 239);
            this.pnlDauTu.TabIndex = 4;
            this.pnlDauTu.Text = "Loại hình đầu tư";
            // 
            // pnlDauTuContainer
            // 
            this.pnlDauTuContainer.Controls.Add(this.expDT);
            this.pnlDauTuContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTuContainer.Name = "pnlDauTuContainer";
            this.pnlDauTuContainer.Size = new System.Drawing.Size(209, 239);
            this.pnlDauTuContainer.TabIndex = 0;
            // 
            // expDT
            // 
            this.expDT.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expDT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expDT.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem61.Key = "tkNhapKhau_DT";
            explorerBarItem61.Text = "Nhập khẩu";
            explorerBarItem62.Key = "tkXuatKhau_DT";
            explorerBarItem62.Text = "Xuất khẩu";
            explorerBarGroup20.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem61,
            explorerBarItem62});
            explorerBarGroup20.Key = "grpToKhai";
            explorerBarGroup20.Text = "Tờ khai";
            this.expDT.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup20});
            this.expDT.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expDT.ImageSize = new System.Drawing.Size(16, 16);
            this.expDT.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expDT.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expDT.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expDT.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expDT.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expDT.Location = new System.Drawing.Point(0, 0);
            this.expDT.Name = "expDT";
            this.expDT.Size = new System.Drawing.Size(209, 239);
            this.expDT.TabIndex = 2;
            this.expDT.Text = "explorerBar1";
            this.expDT.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expDT.VisualStyleManager = this.vsmMain;
            // 
            // pnlSend
            // 
            this.pnlSend.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSend.Icon")));
            this.pnlSend.InnerContainer = this.pnlSendContainer;
            this.pnlSend.Location = new System.Drawing.Point(0, 0);
            this.pnlSend.Name = "pnlSend";
            this.pnlSend.Size = new System.Drawing.Size(209, 239);
            this.pnlSend.TabIndex = 4;
            this.pnlSend.Text = "Khai báo / Theo dõi tờ khai";
            // 
            // pnlSendContainer
            // 
            this.pnlSendContainer.Controls.Add(this.expKhaiBao_TheoDoi);
            this.pnlSendContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSendContainer.Name = "pnlSendContainer";
            this.pnlSendContainer.Size = new System.Drawing.Size(209, 239);
            this.pnlSendContainer.TabIndex = 0;
            // 
            // expKhaiBao_TheoDoi
            // 
            this.expKhaiBao_TheoDoi.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKhaiBao_TheoDoi.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expKhaiBao_TheoDoi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expKhaiBao_TheoDoi.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem63.Key = "tkManage";
            explorerBarItem63.Text = "Theo dõi tờ khai";
            explorerBarItem64.Key = "tkDaDangKy";
            explorerBarItem64.Text = "Tờ khai đã đăng ký";
            explorerBarGroup21.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem63,
            explorerBarItem64});
            explorerBarGroup21.Key = "grpToKhai";
            explorerBarGroup21.Text = "Tờ khai";
            explorerBarItem65.Key = "tdNPLton";
            explorerBarItem65.Text = "Theo dõi NPL Tồn";
            explorerBarItem66.Key = "tkNPLton";
            explorerBarItem66.Text = "Thống kê NPL tồn";
            explorerBarGroup22.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem65,
            explorerBarItem66});
            explorerBarGroup22.Key = "grpNPLTon";
            explorerBarGroup22.Text = "Nguyên Phụ Liệu Tồn";
            explorerBarItem67.Key = "tkNhap";
            explorerBarItem67.Text = "Theo tờ khai nhập";
            explorerBarItem68.Key = "tkXuat";
            explorerBarItem68.Text = "Theo tờ khai xuất";
            explorerBarGroup23.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem67,
            explorerBarItem68});
            explorerBarGroup23.Key = "grpPhanBo";
            explorerBarGroup23.Text = "Theo dõi phân bổ NPL";
            explorerBarGroup23.Visible = false;
            explorerBarItem69.Key = "tkLHKNhap";
            explorerBarItem69.Text = "Tờ Khai Nhập";
            explorerBarItem70.Key = "tkLHKXuat";
            explorerBarItem70.Text = "Tờ Khai Xuất";
            explorerBarGroup24.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem69,
            explorerBarItem70});
            explorerBarGroup24.Key = "grpTKLoaiHinhKhac";
            explorerBarGroup24.Text = "Tờ khai nhập từ hệ thống khác";
            this.expKhaiBao_TheoDoi.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup21,
            explorerBarGroup22,
            explorerBarGroup23,
            explorerBarGroup24});
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expKhaiBao_TheoDoi.ImageSize = new System.Drawing.Size(16, 16);
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expKhaiBao_TheoDoi.Location = new System.Drawing.Point(0, 0);
            this.expKhaiBao_TheoDoi.Name = "expKhaiBao_TheoDoi";
            this.expKhaiBao_TheoDoi.Size = new System.Drawing.Size(209, 239);
            this.expKhaiBao_TheoDoi.TabIndex = 1;
            this.expKhaiBao_TheoDoi.Text = "explorerBar1";
            this.expKhaiBao_TheoDoi.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2007;
            this.expKhaiBao_TheoDoi.VisualStyleManager = this.vsmMain;
            this.expKhaiBao_TheoDoi.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expKhaiBao_TheoDoi_ItemClick);
            // 
            // ilMedium
            // 
            this.ilMedium.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilMedium.ImageSize = new System.Drawing.Size(24, 24);
            this.ilMedium.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // ilLarge
            // 
            this.ilLarge.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilLarge.ImageSize = new System.Drawing.Size(24, 24);
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 423);
            this.statusBar.Name = "statusBar";
            uiStatusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel1.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel1.Key = "DoanhNghiep";
            uiStatusBarPanel1.ProgressBarValue = 0;
            uiStatusBarPanel1.Width = 283;
            uiStatusBarPanel2.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel2.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel2.Key = "HaiQuan";
            uiStatusBarPanel2.ProgressBarValue = 0;
            uiStatusBarPanel2.ToggleKeyValue = Janus.Windows.UI.StatusBar.ToggleKeyValue.NumLock;
            uiStatusBarPanel2.Width = 282;
            uiStatusBarPanel3.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel3.Key = "System";
            uiStatusBarPanel3.PanelType = Janus.Windows.UI.StatusBar.StatusBarPanelType.ProgressBar;
            uiStatusBarPanel3.ProgressBarValue = 0;
            this.statusBar.Panels.AddRange(new Janus.Windows.UI.StatusBar.UIStatusBarPanel[] {
            uiStatusBarPanel1,
            uiStatusBarPanel2,
            uiStatusBarPanel3});
            this.statusBar.PanelsBorderColor = System.Drawing.SystemColors.ControlDark;
            this.statusBar.Size = new System.Drawing.Size(690, 23);
            this.statusBar.TabIndex = 7;
            this.statusBar.VisualStyleManager = this.vsmMain;
            // 
            // cmdThoat3
            // 
            this.cmdThoat3.Key = "cmdThoat";
            this.cmdThoat3.Name = "cmdThoat3";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(690, 446);
            this.Controls.Add(this.uiPanel0);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông quan điện tử - Gia công";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.statusBar, 0);
            this.Controls.SetChildIndex(this.uiPanel0, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mnuRightClick)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).EndInit();
            this.uiPanel2.ResumeLayout(false);
            this.uiPanel2Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).EndInit();
            this.pnlSXXK.ResumeLayout(false);
            this.pnlSXXKContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expSXXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).EndInit();
            this.pnlGiaCong.ResumeLayout(false);
            this.pnlGiaCongContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).EndInit();
            this.pnlKinhDoanh.ResumeLayout(false);
            this.pnlKinhDoanhContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).EndInit();
            this.pnlDauTu.ResumeLayout(false);
            this.pnlDauTuContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).EndInit();
            this.pnlSend.ResumeLayout(false);
            this.pnlSendContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expKhaiBao_TheoDoi)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UICommand cmdThoat3;
        private UIPanelInnerContainer pnlDauTuContainer;
        private ExplorerBar expDT;
        private UIPanel pnlSend;
        private UIPanelInnerContainer pnlSendContainer;
        private ExplorerBar expKhaiBao_TheoDoi;
        private NotifyIcon notifyIcon1;
        private UICommand NhacNho;
        private UICommand DongBoDuLieu;
        private UICommand cmdImport;
        private UICommand cmdImportNPL1;
        private UICommand cmdImportSP1;
        private UICommand cmdImportNPL;
        private UICommand cmdImportSP;
        private UICommand cmdImportDM1;
        private UICommand cmdImportDM;
        private UICommand cmdImportToKhai1;
        private UICommand cmdImportTTDM;
        private UICommand cmdImportToKhai;
        private UICommand cmdImportHangHoa1;
        private UICommand cmdImportHangHoa;
        private UICommand Command01;
        private UICommand Command0;
        private UICommand cmdHelp1;
        private UICommand cmdAbout1;
        private UICommand cmdHelp;
        private UICommand cmdAbout;
        private UICommand Command11;
        private UICommand Command1;
        private UICommand cmd20071;
        private UICommand cmd2007;
        private UICommand cmd2003;
        private UICommand cmd20031;
        private UICommand cmdNPLNhapTon;
        private UICommand cmdDanhMuc1;
        private UICommand cmdDanhMuc;
        private UICommand cmdMaHS;
        private UICommand cmdHaiQuan1;
        private UICommand cmdNuoc1;
        private UICommand cmdMaHS1;
        private UICommand cmdNguyenTe1;
        private UICommand cmdDVT1;
        private UICommand cmdPTTT1;
        private UICommand cmdPTVT1;
        private UICommand cmdDKGH1;
        private UICommand cmdCuaKhau1;
        private UICommand cmdNuoc;
        private UICommand cmdHaiQuan;
        private UICommand cmdNguyenTe;
        private UICommand cmdDVT;
        private UICommand cmdPTTT;
        private UICommand cmdPTVT;
        private UICommand cmdDKGH;
        private UICommand cmdCuaKhau;
        private UICommand cmdBackUp1;
        private UICommand cmdBackUp;
        private UICommand cmdRestore;
        private UICommand ThongSoKetNoi;
        private UICommand TLThongTinDNHQ;
        private UICommand cmdThietLapIn;
        private UICommand cmdExportExccel;
        private UICommand cmdExportExcel1;
        private UICommand cmdImportExcel;
        private UICommand cmdDongBoPhongKhai;
        private UICommand cmdImportExcel1;
        private UICommand cmdExportExccel1;
        private UICommand cmdCauHinh1;
        private UICommand cmdCauHinh;
        private UICommand ThongSoKetNoi1;
        private UICommand TLThongTinDNHQ1;
        private UICommand cmdCauHinhToKhai1;
        private UICommand cmdThietLapIn1;
        private UICommand cmdCauHinhToKhai;
        public Janus.Windows.UI.StatusBar.UIStatusBar statusBar;
        private UICommand QuanTri1;
        private UICommand QuanTri;
        private UICommand QuanLyNguoiDung1;
        private UICommand QuanLyNhom1;
        private UICommand QuanLyNguoiDung;
        private UICommand QuanLyNhom;
        private UICommand LoginUser1;
        private UICommand LoginUser;
        private UICommand cmdChangePass1;
        private UICommand cmdChangePass;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private ExplorerBar explorerBar1;
        private UIPanel uiPanel2;
        private UIPanelInnerContainer uiPanel2Container;
        private ExplorerBar explorerBar2;
        private UICommand TraCuuMaHS1;
        private UICommand TraCuuMaHS;
        private UICommand cmdAutoUpdate1;
        private UICommand cmdAutoUpdate;
        private UICommand NhapXML;
        private UICommand NhapHDXML;
        private UICommand NhapDMXML;
        private UICommand NhapPKXML;
        private UICommand XuatTKXML;
        private UICommand XuatTKMD1;
        private UICommand XuatTKGCCT1;
        private UICommand XuatTKMD;
        private UICommand XuatTKGCCT;
        private UICommand NhapTKXML;
        private UICommand NhapTKMD1;
        private UICommand NhapTKGCCT1;
        private UICommand NhapTKMD;
        private UICommand NhapTKGCCT;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private UICommand cmdVN1;
        private UICommand cmdEnglish;
        private UICommand cmdVN;
        private UICommand cmdEng1;
        private UICommand cmdNhapXuat1;
        private UICommand cmdNhapXuat;
        private UICommand NhapXML2;
        private UICommand cmdXuatDuLieuPhongKHai1;
        private UICommand cmdXuatDuLieuPhongKHai;
        private UICommand cmdXuatHopDong1;
        private UICommand cmdXuatDinhMuc1;
        private UICommand cmdXuatPhuKien1;
        private UICommand cmdXuatToKhai1;
        private UICommand cmdXuatHopDong;
        private UICommand cmdXuatDinhMuc;
        private UICommand cmdXuatPhuKien;
        private UICommand cmdXuatToKhai;
        private UICommand Separator1;
        private UICommand cmdActivate1;
        private UICommand cmdActivate;
        private UICommand XuatTKGCCT2;
        private UICommand XuatTKMD2;
        private UICommand cmdDMSPGC1;
        private UICommand cmdDMSPGC;
        private UICommand cmdNhapDuLieuDaiLy;
        private UICommand cmdCloseMe;
        private UICommand cmdCloseAllButMe;
        private UICommand cmdCloseAll;
        private UIContextMenu mnuRightClick;
        private UICommand cmdCloseMe1;
        private UICommand Separator2;
        private UICommand cmdCloseAllButMe1;
        private UICommand cmdCloseAll1;
        private BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Timer timer1;
        private UICommand cmdXuatDN1;
        private UICommand cmdXuatDN;
        private UICommand QuanLyMess1;
        private UICommand cmdNhapDuLieuTuDoangNghiep1;
        private UICommand QuanLyMess;
        private UICommand cmdNhapDuLieuTuDoangNghiep;
        private UICommand cmdNhapHopDong1;
        private UICommand cmdNhapDinhMuc1;
        private UICommand cmdNhapPhuKien1;
        private UICommand cmdNhapToKhai1;
        private UICommand cmdNhapHopDong;
        private UICommand cmdNhapDinhMuc;
        private UICommand cmdNhapPhuKien;
        private UICommand cmdNhapToKhai;
        private UICommand cmdNhapToKhaiGCCT1;
        private UICommand cmdNhapToKhaiMauDich1;
        private UICommand cmdNhapToKhaiMauDich;
        private UICommand cmdNhapToKhaiGCCT;
        private UICommand cmdTLTTDN1;
        private UICommand cmdTLTTDN;
        private UICommand mnuQuerySQL;
        private UICommand mnuQuerySQL1;
        private UICommand cmdLog1;
        private UICommand cmdLog;
        private UICommand cmdDataVersion1;
        private UICommand cmdDataVersion;
    }
}
