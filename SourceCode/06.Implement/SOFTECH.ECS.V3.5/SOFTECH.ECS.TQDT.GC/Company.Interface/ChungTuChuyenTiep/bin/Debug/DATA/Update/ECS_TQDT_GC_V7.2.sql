--USE [ECS_TQDT_GC_V4]
--GO

/****** Object:  StoredProcedure [dbo].[p_GC_SelectHang]    Script Date: 02/21/2013 14:35:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_GC_SelectHang]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_SelectHang]
GO

--USE [ECS_TQDT_GC_V4]
--GO

/****** Object:  StoredProcedure [dbo].[p_GC_SelectHang]    Script Date: 02/21/2013 14:35:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[p_GC_SelectHang]      
(      
 @MaHaiQuan VARCHAR(10),      
 @MaDoanhNghiep VARCHAR(20),      
 @MaLoaiHinh VARCHAR(5)      
)      
AS      
      
/*Updateed by Hungtq, 08/09/2012*/      
      
DECLARE @sql VARCHAR(max), @TableName VARCHAR(50)     

/*TO KHAI GIA CONG*/
      
SET @sql =      
  ' SELECT distinct t.MaHaiQuan, t.MaDoanhNghiep, h.MaPhu AS Ma, h.TenHang AS Ten, h.MaHS, h.DVT_ID '      
+ ' FROM dbo.t_KDT_ToKhaiMauDich t INNER JOIN dbo.t_KDT_HangMauDich h '      
+ ' ON t.ID = h.TKMD_ID '      
+ ' WHERE t.MaHaiQuan = ''' + @MaHaiQuan + ''' AND t.MaDoanhNghiep = ''' + @MaDoanhNghiep + ''''      
+ ' AND t.TrangThaiXuLy = 1 AND t.MaLoaiHinh LIKE ''' + @MaLoaiHinh + '%'''      
    
SET @sql = @sql + ' UNION '      
    
IF(@MaLoaiHinh = 'N')        
 SET @TableName = 't_GC_NguyenPhuLieu'    
IF(@MaLoaiHinh = 'X')        
 SET @TableName = 't_GC_SanPham'    
IF(@MaLoaiHinh = 'T')        
 SET @TableName = 't_GC_ThietBi'    
    
SET @sql = @sql     
+ ' SELECT DISTINCT MaHaiQuan, MaDoanhNghiep, Ma, Ten, MaHS, DVT_ID '    
+ ' FROM dbo.' + @TableName + ' d INNER JOIN dbo.t_KDT_GC_HopDong h ON d.HopDong_ID = h.ID '
+ ' WHERE MaHaiQuan = ''' + @MaHaiQuan + ''' AND MaDoanhNghiep = ''' + @MaDoanhNghiep + ''''    

/*TO KHAI GIA CONG CHUYEN TIEP*/

SET @sql = @sql + ' UNION '      

SET @sql = @sql     
+ ' SELECT t.MaHaiQuanTiepNhan AS MaHaiQuan, t.MaDoanhNghiep, h.MaHang AS Ma, h.TenHang AS Ten, h.MaHS, h.ID_DVT AS DVT_ID '      
+ ' FROM dbo.t_KDT_GC_ToKhaiChuyenTiep t INNER JOIN dbo.t_KDT_GC_HangChuyenTiep h '      
+ ' ON t.ID = h.Master_ID '      
+ ' WHERE t.MaHaiQuanTiepNhan = ''' + @MaHaiQuan + ''' AND t.MaDoanhNghiep = ''' + @MaDoanhNghiep + ''''      
+ ' AND t.TrangThaiXuLy = 1 AND t.MaLoaiHinh LIKE ''' + @MaLoaiHinh + '%'''      
    
SET @sql = 'SELECT distinct MaHaiQuan, MaDoanhNghiep, Ma, Ten, MaHS, DVT_ID from (' + @sql + ') as v'    

--PRINT @sql      
EXEC (@sql)      
      
--exec p_GC_SelectHang 'C34C', '0100101308', 'N'      

GO


UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '7.2'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO	