﻿namespace Company.Interface
{
    partial class VNACC_ChuyenCuaKhau_Container
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ChuyenCuaKhau_Container));
            this.grList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnThoat = new Janus.Windows.EditControls.UIButton();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnLuu = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoSeal6 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoSeal4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoSeal2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoDongHangTrenTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoSeal5 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoSeal3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoSeal1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHieuContainer = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 326), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 326);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 302);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 302);
            // 
            // grbMain
            // 
            this.grbMain.AutoScroll = true;
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.grList);
            this.grbMain.Size = new System.Drawing.Size(466, 326);
            this.grbMain.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbMain.VisualStyleManager = null;
            // 
            // grList
            // 
            this.grList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            grList_DesignTimeLayout.LayoutString = resources.GetString("grList_DesignTimeLayout.LayoutString");
            this.grList.DesignTimeLayout = grList_DesignTimeLayout;
            this.grList.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.grList.GroupByBoxVisible = false;
            this.grList.Location = new System.Drawing.Point(0, 188);
            this.grList.Name = "grList";
            this.grList.RecordNavigator = true;
            this.grList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.grList.Size = new System.Drawing.Size(466, 138);
            this.grList.TabIndex = 0;
            this.grList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grList.VisualStyleManager = this.vsmMain;
            this.grList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grList_RowDoubleClick);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.btnThoat);
            this.uiGroupBox1.Controls.Add(this.btnXoa);
            this.uiGroupBox1.Controls.Add(this.btnLuu);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 145);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(466, 43);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // btnThoat
            // 
            this.btnThoat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnThoat.Image = ((System.Drawing.Image)(resources.GetObject("btnThoat.Image")));
            this.btnThoat.ImageSize = new System.Drawing.Size(20, 20);
            this.btnThoat.Location = new System.Drawing.Point(383, 11);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(75, 23);
            this.btnThoat.TabIndex = 4;
            this.btnThoat.Text = "Đóng";
            this.btnThoat.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(304, 11);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 4;
            this.btnXoa.Text = "Xóa";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLuu.Image = ((System.Drawing.Image)(resources.GetObject("btnLuu.Image")));
            this.btnLuu.ImageSize = new System.Drawing.Size(20, 20);
            this.btnLuu.Location = new System.Drawing.Point(224, 11);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(75, 23);
            this.btnLuu.TabIndex = 3;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label9);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.txtSoSeal6);
            this.uiGroupBox2.Controls.Add(this.txtSoSeal4);
            this.uiGroupBox2.Controls.Add(this.txtSoSeal2);
            this.uiGroupBox2.Controls.Add(this.txtSoDongHangTrenTK);
            this.uiGroupBox2.Controls.Add(this.txtSoSeal5);
            this.uiGroupBox2.Controls.Add(this.txtSoSeal3);
            this.uiGroupBox2.Controls.Add(this.txtSoSeal1);
            this.uiGroupBox2.Controls.Add(this.txtSoHieuContainer);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(466, 145);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(213, 96);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Số Seal 6";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(213, 71);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Số Seal 5";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(213, 42);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Số Seal 4";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(5, 96);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Số Seal 3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(5, 69);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Số Seal 2";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(5, 47);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Số Seal 1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(213, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Số dòng hàng trên tờ khai";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Số hiệu";
            // 
            // txtSoSeal6
            // 
            this.txtSoSeal6.Location = new System.Drawing.Point(283, 96);
            this.txtSoSeal6.Name = "txtSoSeal6";
            this.txtSoSeal6.Size = new System.Drawing.Size(143, 21);
            this.txtSoSeal6.TabIndex = 7;
            this.txtSoSeal6.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoSeal4
            // 
            this.txtSoSeal4.Location = new System.Drawing.Point(283, 42);
            this.txtSoSeal4.Name = "txtSoSeal4";
            this.txtSoSeal4.Size = new System.Drawing.Size(143, 21);
            this.txtSoSeal4.TabIndex = 3;
            this.txtSoSeal4.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoSeal2
            // 
            this.txtSoSeal2.Location = new System.Drawing.Point(62, 69);
            this.txtSoSeal2.Name = "txtSoSeal2";
            this.txtSoSeal2.Size = new System.Drawing.Size(143, 21);
            this.txtSoSeal2.TabIndex = 4;
            this.txtSoSeal2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoDongHangTrenTK
            // 
            this.txtSoDongHangTrenTK.Location = new System.Drawing.Point(353, 13);
            this.txtSoDongHangTrenTK.Name = "txtSoDongHangTrenTK";
            this.txtSoDongHangTrenTK.Size = new System.Drawing.Size(73, 21);
            this.txtSoDongHangTrenTK.TabIndex = 1;
            this.txtSoDongHangTrenTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoSeal5
            // 
            this.txtSoSeal5.Location = new System.Drawing.Point(283, 69);
            this.txtSoSeal5.Name = "txtSoSeal5";
            this.txtSoSeal5.Size = new System.Drawing.Size(143, 21);
            this.txtSoSeal5.TabIndex = 5;
            this.txtSoSeal5.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoSeal3
            // 
            this.txtSoSeal3.Location = new System.Drawing.Point(62, 96);
            this.txtSoSeal3.Name = "txtSoSeal3";
            this.txtSoSeal3.Size = new System.Drawing.Size(143, 21);
            this.txtSoSeal3.TabIndex = 6;
            this.txtSoSeal3.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoSeal1
            // 
            this.txtSoSeal1.Location = new System.Drawing.Point(62, 42);
            this.txtSoSeal1.Name = "txtSoSeal1";
            this.txtSoSeal1.Size = new System.Drawing.Size(143, 21);
            this.txtSoSeal1.TabIndex = 2;
            this.txtSoSeal1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHieuContainer
            // 
            this.txtSoHieuContainer.Location = new System.Drawing.Point(62, 13);
            this.txtSoHieuContainer.Name = "txtSoHieuContainer";
            this.txtSoHieuContainer.Size = new System.Drawing.Size(143, 21);
            this.txtSoHieuContainer.TabIndex = 0;
            this.txtSoHieuContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // VNACC_ChuyenCuaKhau_Container
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(672, 332);
            this.helpProvider1.SetHelpKeyword(this, "BaseFormHaveGuidPanel.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ChuyenCuaKhau_Container";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Danh sách Container";
            this.Load += new System.EventHandler(this.VNACC_ChuyenCuaKhau_Container_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX grList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSeal6;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSeal4;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSeal2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDongHangTrenTK;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSeal5;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSeal3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoSeal1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHieuContainer;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnLuu;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnThoat;
    }
}