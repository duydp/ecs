﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
	public partial class T_GC_SANPHAM_QUYETOAN : ICloneable
	{
		#region Properties.
		
		public string MASP { set; get; }
		public string TENSP { set; get; }
		public string DVT { set; get; }
		public decimal LUONGTONDK { set; get; }
		public decimal TRIGIATONDK { set; get; }
		public decimal LUONGNHAPTK { set; get; }
		public decimal TRIGIANHAPTK { set; get; }
		public decimal LUONGXUATTK { set; get; }
		public decimal TRIGIAXUATTK { set; get; }
		public decimal LUONGTONCK { set; get; }
		public decimal TRIGIATONCK { set; get; }
		public long HOPDONG_ID { set; get; }
		public int NAMQUYETTOAN { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_GC_SANPHAM_QUYETOAN> ConvertToCollection(IDataReader reader)
		{
			List<T_GC_SANPHAM_QUYETOAN> collection = new List<T_GC_SANPHAM_QUYETOAN>();
			while (reader.Read())
			{
				T_GC_SANPHAM_QUYETOAN entity = new T_GC_SANPHAM_QUYETOAN();
				if (!reader.IsDBNull(reader.GetOrdinal("MASP"))) entity.MASP = reader.GetString(reader.GetOrdinal("MASP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENSP"))) entity.TENSP = reader.GetString(reader.GetOrdinal("TENSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGTONDK"))) entity.LUONGTONDK = reader.GetDecimal(reader.GetOrdinal("LUONGTONDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIATONDK"))) entity.TRIGIATONDK = reader.GetDecimal(reader.GetOrdinal("TRIGIATONDK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGNHAPTK"))) entity.LUONGNHAPTK = reader.GetDecimal(reader.GetOrdinal("LUONGNHAPTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIANHAPTK"))) entity.TRIGIANHAPTK = reader.GetDecimal(reader.GetOrdinal("TRIGIANHAPTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGXUATTK"))) entity.LUONGXUATTK = reader.GetDecimal(reader.GetOrdinal("LUONGXUATTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIAXUATTK"))) entity.TRIGIAXUATTK = reader.GetDecimal(reader.GetOrdinal("TRIGIAXUATTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGTONCK"))) entity.LUONGTONCK = reader.GetDecimal(reader.GetOrdinal("LUONGTONCK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIATONCK"))) entity.TRIGIATONCK = reader.GetDecimal(reader.GetOrdinal("TRIGIATONCK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HOPDONG_ID"))) entity.HOPDONG_ID = reader.GetInt64(reader.GetOrdinal("HOPDONG_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NAMQUYETTOAN"))) entity.NAMQUYETTOAN = reader.GetInt32(reader.GetOrdinal("NAMQUYETTOAN"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_GC_SANPHAM_QUYETOAN VALUES(@MASP, @TENSP, @DVT, @LUONGTONDK, @TRIGIATONDK, @LUONGNHAPTK, @TRIGIANHAPTK, @LUONGXUATTK, @TRIGIAXUATTK, @LUONGTONCK, @TRIGIATONCK, @HOPDONG_ID, @NAMQUYETTOAN)";
            string update = "UPDATE T_GC_SANPHAM_QUYETOAN SET MASP = @MASP, TENSP = @TENSP, DVT = @DVT, LUONGTONDK = @LUONGTONDK, TRIGIATONDK = @TRIGIATONDK, LUONGNHAPTK = @LUONGNHAPTK, TRIGIANHAPTK = @TRIGIANHAPTK, LUONGXUATTK = @LUONGXUATTK, TRIGIAXUATTK = @TRIGIAXUATTK, LUONGTONCK = @LUONGTONCK, TRIGIATONCK = @TRIGIATONCK, HOPDONG_ID = @HOPDONG_ID, NAMQUYETTOAN = @NAMQUYETTOAN WHERE ID = @ID";
            string delete = "DELETE FROM T_GC_SANPHAM_QUYETOAN WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MASP", SqlDbType.VarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONDK", SqlDbType.Decimal, "LUONGTONDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONDK", SqlDbType.Decimal, "TRIGIATONDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGNHAPTK", SqlDbType.Decimal, "LUONGNHAPTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, "TRIGIANHAPTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGXUATTK", SqlDbType.Decimal, "LUONGXUATTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, "TRIGIAXUATTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONCK", SqlDbType.Decimal, "LUONGTONCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONCK", SqlDbType.Decimal, "TRIGIATONCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MASP", SqlDbType.VarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONDK", SqlDbType.Decimal, "LUONGTONDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONDK", SqlDbType.Decimal, "TRIGIATONDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGNHAPTK", SqlDbType.Decimal, "LUONGNHAPTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, "TRIGIANHAPTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGXUATTK", SqlDbType.Decimal, "LUONGXUATTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, "TRIGIAXUATTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONCK", SqlDbType.Decimal, "LUONGTONCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONCK", SqlDbType.Decimal, "TRIGIATONCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MASP", SqlDbType.VarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_GC_SANPHAM_QUYETOAN VALUES(@MASP, @TENSP, @DVT, @LUONGTONDK, @TRIGIATONDK, @LUONGNHAPTK, @TRIGIANHAPTK, @LUONGXUATTK, @TRIGIAXUATTK, @LUONGTONCK, @TRIGIATONCK, @HOPDONG_ID, @NAMQUYETTOAN)";
            string update = "UPDATE T_GC_SANPHAM_QUYETOAN SET MASP = @MASP, TENSP = @TENSP, DVT = @DVT, LUONGTONDK = @LUONGTONDK, TRIGIATONDK = @TRIGIATONDK, LUONGNHAPTK = @LUONGNHAPTK, TRIGIANHAPTK = @TRIGIANHAPTK, LUONGXUATTK = @LUONGXUATTK, TRIGIAXUATTK = @TRIGIAXUATTK, LUONGTONCK = @LUONGTONCK, TRIGIATONCK = @TRIGIATONCK, HOPDONG_ID = @HOPDONG_ID, NAMQUYETTOAN = @NAMQUYETTOAN WHERE ID = @ID";
            string delete = "DELETE FROM T_GC_SANPHAM_QUYETOAN WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MASP", SqlDbType.VarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONDK", SqlDbType.Decimal, "LUONGTONDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONDK", SqlDbType.Decimal, "TRIGIATONDK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGNHAPTK", SqlDbType.Decimal, "LUONGNHAPTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, "TRIGIANHAPTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGXUATTK", SqlDbType.Decimal, "LUONGXUATTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, "TRIGIAXUATTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONCK", SqlDbType.Decimal, "LUONGTONCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONCK", SqlDbType.Decimal, "TRIGIATONCK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MASP", SqlDbType.VarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENSP", SqlDbType.NVarChar, "TENSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONDK", SqlDbType.Decimal, "LUONGTONDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONDK", SqlDbType.Decimal, "TRIGIATONDK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGNHAPTK", SqlDbType.Decimal, "LUONGNHAPTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, "TRIGIANHAPTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGXUATTK", SqlDbType.Decimal, "LUONGXUATTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, "TRIGIAXUATTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONCK", SqlDbType.Decimal, "LUONGTONCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONCK", SqlDbType.Decimal, "TRIGIATONCK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MASP", SqlDbType.VarChar, "MASP", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@NAMQUYETTOAN", SqlDbType.Int, "NAMQUYETTOAN", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_GC_SANPHAM_QUYETOAN Load(string mASP, long hOPDONG_ID, int nAMQUYETTOAN)
		{
			const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.VarChar, mASP);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, hOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, nAMQUYETTOAN);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_GC_SANPHAM_QUYETOAN> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_GC_SANPHAM_QUYETOAN> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_GC_SANPHAM_QUYETOAN> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
        public static DataSet SelectDynamicNew(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectDynamicNew]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }
        public DataSet SelectSumSPXuat(long HopDong_ID, int NamQuyetToan)
        {
            const string spName = "[dbo].[p_T_GC_SANPHAM_SelectSumSPXuatBy_HD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
            db.AddInParameter(dbCommand, "@NamQuyetToan", SqlDbType.Int, NamQuyetToan);
            dbCommand.CommandTimeout = 600;

            return db.ExecuteDataSet(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertT_GC_SANPHAM_QUYETOAN(string mASP, string tENSP, string dVT, decimal lUONGTONDK, decimal tRIGIATONDK, decimal lUONGNHAPTK, decimal tRIGIANHAPTK, decimal lUONGXUATTK, decimal tRIGIAXUATTK, decimal lUONGTONCK, decimal tRIGIATONCK, long hOPDONG_ID, int nAMQUYETTOAN)
		{
			T_GC_SANPHAM_QUYETOAN entity = new T_GC_SANPHAM_QUYETOAN();	
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVT = dVT;
			entity.LUONGTONDK = lUONGTONDK;
			entity.TRIGIATONDK = tRIGIATONDK;
			entity.LUONGNHAPTK = lUONGNHAPTK;
			entity.TRIGIANHAPTK = tRIGIANHAPTK;
			entity.LUONGXUATTK = lUONGXUATTK;
			entity.TRIGIAXUATTK = tRIGIAXUATTK;
			entity.LUONGTONCK = lUONGTONCK;
			entity.TRIGIATONCK = tRIGIATONCK;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.NAMQUYETTOAN = nAMQUYETTOAN;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MASP", SqlDbType.VarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGTONDK", SqlDbType.Decimal, LUONGTONDK);
			db.AddInParameter(dbCommand, "@TRIGIATONDK", SqlDbType.Decimal, TRIGIATONDK);
			db.AddInParameter(dbCommand, "@LUONGNHAPTK", SqlDbType.Decimal, LUONGNHAPTK);
			db.AddInParameter(dbCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, TRIGIANHAPTK);
			db.AddInParameter(dbCommand, "@LUONGXUATTK", SqlDbType.Decimal, LUONGXUATTK);
			db.AddInParameter(dbCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, TRIGIAXUATTK);
			db.AddInParameter(dbCommand, "@LUONGTONCK", SqlDbType.Decimal, LUONGTONCK);
			db.AddInParameter(dbCommand, "@TRIGIATONCK", SqlDbType.Decimal, TRIGIATONCK);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_GC_SANPHAM_QUYETOAN> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_GC_SANPHAM_QUYETOAN item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_GC_SANPHAM_QUYETOAN(string mASP, string tENSP, string dVT, decimal lUONGTONDK, decimal tRIGIATONDK, decimal lUONGNHAPTK, decimal tRIGIANHAPTK, decimal lUONGXUATTK, decimal tRIGIAXUATTK, decimal lUONGTONCK, decimal tRIGIATONCK, long hOPDONG_ID, int nAMQUYETTOAN)
		{
			T_GC_SANPHAM_QUYETOAN entity = new T_GC_SANPHAM_QUYETOAN();			
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVT = dVT;
			entity.LUONGTONDK = lUONGTONDK;
			entity.TRIGIATONDK = tRIGIATONDK;
			entity.LUONGNHAPTK = lUONGNHAPTK;
			entity.TRIGIANHAPTK = tRIGIANHAPTK;
			entity.LUONGXUATTK = lUONGXUATTK;
			entity.TRIGIAXUATTK = tRIGIAXUATTK;
			entity.LUONGTONCK = lUONGTONCK;
			entity.TRIGIATONCK = tRIGIATONCK;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.NAMQUYETTOAN = nAMQUYETTOAN;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_GC_SANPHAM_QUYETOAN_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MASP", SqlDbType.VarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGTONDK", SqlDbType.Decimal, LUONGTONDK);
			db.AddInParameter(dbCommand, "@TRIGIATONDK", SqlDbType.Decimal, TRIGIATONDK);
			db.AddInParameter(dbCommand, "@LUONGNHAPTK", SqlDbType.Decimal, LUONGNHAPTK);
			db.AddInParameter(dbCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, TRIGIANHAPTK);
			db.AddInParameter(dbCommand, "@LUONGXUATTK", SqlDbType.Decimal, LUONGXUATTK);
			db.AddInParameter(dbCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, TRIGIAXUATTK);
			db.AddInParameter(dbCommand, "@LUONGTONCK", SqlDbType.Decimal, LUONGTONCK);
			db.AddInParameter(dbCommand, "@TRIGIATONCK", SqlDbType.Decimal, TRIGIATONCK);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_GC_SANPHAM_QUYETOAN> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_GC_SANPHAM_QUYETOAN item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_GC_SANPHAM_QUYETOAN(string mASP, string tENSP, string dVT, decimal lUONGTONDK, decimal tRIGIATONDK, decimal lUONGNHAPTK, decimal tRIGIANHAPTK, decimal lUONGXUATTK, decimal tRIGIAXUATTK, decimal lUONGTONCK, decimal tRIGIATONCK, long hOPDONG_ID, int nAMQUYETTOAN)
		{
			T_GC_SANPHAM_QUYETOAN entity = new T_GC_SANPHAM_QUYETOAN();			
			entity.MASP = mASP;
			entity.TENSP = tENSP;
			entity.DVT = dVT;
			entity.LUONGTONDK = lUONGTONDK;
			entity.TRIGIATONDK = tRIGIATONDK;
			entity.LUONGNHAPTK = lUONGNHAPTK;
			entity.TRIGIANHAPTK = tRIGIANHAPTK;
			entity.LUONGXUATTK = lUONGXUATTK;
			entity.TRIGIAXUATTK = tRIGIAXUATTK;
			entity.LUONGTONCK = lUONGTONCK;
			entity.TRIGIATONCK = tRIGIATONCK;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.NAMQUYETTOAN = nAMQUYETTOAN;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MASP", SqlDbType.VarChar, MASP);
			db.AddInParameter(dbCommand, "@TENSP", SqlDbType.NVarChar, TENSP);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGTONDK", SqlDbType.Decimal, LUONGTONDK);
			db.AddInParameter(dbCommand, "@TRIGIATONDK", SqlDbType.Decimal, TRIGIATONDK);
			db.AddInParameter(dbCommand, "@LUONGNHAPTK", SqlDbType.Decimal, LUONGNHAPTK);
			db.AddInParameter(dbCommand, "@TRIGIANHAPTK", SqlDbType.Decimal, TRIGIANHAPTK);
			db.AddInParameter(dbCommand, "@LUONGXUATTK", SqlDbType.Decimal, LUONGXUATTK);
			db.AddInParameter(dbCommand, "@TRIGIAXUATTK", SqlDbType.Decimal, TRIGIAXUATTK);
			db.AddInParameter(dbCommand, "@LUONGTONCK", SqlDbType.Decimal, LUONGTONCK);
			db.AddInParameter(dbCommand, "@TRIGIATONCK", SqlDbType.Decimal, TRIGIATONCK);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_GC_SANPHAM_QUYETOAN> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_GC_SANPHAM_QUYETOAN item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_GC_SANPHAM_QUYETOAN(string mASP, long hOPDONG_ID, int nAMQUYETTOAN)
		{
			T_GC_SANPHAM_QUYETOAN entity = new T_GC_SANPHAM_QUYETOAN();
			entity.MASP = mASP;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.NAMQUYETTOAN = nAMQUYETTOAN;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MASP", SqlDbType.VarChar, MASP);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------

        public int DeleteBy_HD_ID(long HOPDONG_ID, int NAMQUYETTOAN)
        {
            const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_Delete_By_HD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
            db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);

            return db.ExecuteNonQuery(dbCommand);
        }
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_GC_SANPHAM_QUYETOAN_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_GC_SANPHAM_QUYETOAN> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_GC_SANPHAM_QUYETOAN item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}