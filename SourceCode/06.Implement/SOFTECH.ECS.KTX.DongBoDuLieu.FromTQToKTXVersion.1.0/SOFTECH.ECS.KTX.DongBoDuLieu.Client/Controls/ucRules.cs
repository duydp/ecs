using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DongBo.Controls
{
    public partial class ucRules : ucDesign
    {
        frmMain frmParent = null;
        public ucRules()
        {
            InitializeComponent();
            frmParent = (frmMain)Application.OpenForms["frmMain"];
        }

        private void ucRules_Load(object sender, EventArgs e)
        {
            frmParent.btnPrev.Enabled = true;
            this.validControl();
        }

        private void validControl()
        {
            if (chkAgree1.Checked && chkAgree2.Checked)
            {
                frmParent.btnNext.Enabled = true;
            }
            else
            {
                frmParent.btnNext.Enabled = false;
            }
        }

        private void chkAgree1_CheckedChanged(object sender, EventArgs e)
        {
            this.validControl();
        }

        private void chkAgree2_CheckedChanged(object sender, EventArgs e)
        {
            this.validControl();
        }
    }
}
