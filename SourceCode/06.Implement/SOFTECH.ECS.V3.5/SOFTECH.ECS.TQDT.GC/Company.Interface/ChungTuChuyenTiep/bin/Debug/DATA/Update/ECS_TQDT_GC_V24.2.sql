
GO
ALTER VIEW [dbo].[t_View_KDT_GC_HMD_HD]  
AS  
SELECT DISTINCT   
                      dbo.t_KDT_ToKhaiMauDich.IDHopDong, dbo.t_KDT_HangMauDich.MaPhu, dbo.t_KDT_HangMauDich.TenHang,dbo.t_KDT_HangMauDich.SoThuTuHang, dbo.t_KDT_HangMauDich.DVT_ID,   
                      dbo.t_KDT_ToKhaiMauDich.MaLoaiHinh, dbo.t_KDT_ToKhaiMauDich.TrangThaiXuLy, dbo.t_KDT_ToKhaiMauDich.LoaiHangHoa  
FROM         dbo.t_KDT_ToKhaiMauDich INNER JOIN  
                      dbo.t_KDT_HangMauDich ON dbo.t_KDT_ToKhaiMauDich.ID = dbo.t_KDT_HangMauDich.TKMD_ID  
  
  
GO
  
 ALTER VIEW [dbo].[t_View_KDT_GC_HCT_HD]  
AS  
SELECT DISTINCT   
                      dbo.t_KDT_GC_ToKhaiChuyenTiep.IDHopDong, dbo.t_KDT_GC_HangChuyenTiep.MaHang AS MaPhu, dbo.t_KDT_GC_HangChuyenTiep.TenHang,  
                       dbo.t_KDT_GC_HangChuyenTiep.SoThuTuHang,  
                      dbo.t_KDT_GC_HangChuyenTiep.ID_DVT, dbo.t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy, dbo.t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh  
FROM         dbo.t_KDT_GC_HangChuyenTiep INNER JOIN  
                      dbo.t_KDT_GC_ToKhaiChuyenTiep ON dbo.t_KDT_GC_HangChuyenTiep.Master_ID = dbo.t_KDT_GC_ToKhaiChuyenTiep.ID  
  

    
  
GO
  
ALTER VIEW [dbo].[v_KDT_SP_Xuat_HD]  
AS  
SELECT     IDHopDong, MaPhu,SoThuTuHang , ID_DVT AS DVT_ID  
FROM         dbo.t_View_KDT_GC_HCT_HD  
WHERE     (MaLoaiHinh = 'PHSPX' OR MaLoaiHinh = 'XGC19' OR MaLoaiHinh = 'XVE54'  ) AND (TrangThaiXuLy = 1)  
UNION  
SELECT     IDHopDong, MaPhu,SoThuTuHang, DVT_ID  
FROM         dbo.t_View_KDT_GC_HMD_HD  
WHERE     (MaLoaiHinh LIKE 'X%') AND (LoaiHangHoa = 'S') AND (TrangThaiXuLy = 1)  

GO
  
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_GC_XuatNhapTon_SP_DM_NPL_TT38]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_XuatNhapTon_SP_DM_NPL_TT38]
GO  
CREATE PROC [dbo].[p_GC_XuatNhapTon_SP_DM_NPL_TT38]  
    @HopDong_ID NVARCHAR(MAX)  
AS  
    BEGIN  
        SELECT DISTINCT  
                ToKhai.IDHopDong AS HopDong_ID ,  
                ToKhai.SoHopDong ,  
                CONVERT(VARCHAR(50), ( CASE WHEN ToKhai.MaLoaiHinh LIKE '%V%'  
                                            THEN ( SELECT TOP 1  
                                                            SoTKVNACCS  
                                                   FROM     t_VNACCS_CapSoToKhai  
                                                   WHERE    SoTK = ToKhai.SoToKhai  
                                                 )  
                                            ELSE ToKhai.SoToKhai  
                                       END )) AS SoToKhai ,  
                ToKhai.MaLoaiHinh ,  
                ToKhai.NgayDangKy ,  
                v_KDT_SP_Xuat_HD.MaPhu AS MaHang ,  
                v_KDT_SP_Xuat_HD.SoThuTuHang ,  
                ToKhai.TenHang ,  
                ToKhai.SoLuong ,  
                DM.MaNguyenPhuLieu ,  
                CONVERT(DECIMAL(16, 6), DM.DinhMucSuDung) AS DinhMucSuDung ,  
                CONVERT(DECIMAL(16, 6), DM.TyLeHaoHut) AS TyLeHaoHut ,  
                CONVERT(DECIMAL(24, 6), ToKhai.SoLuong * DM.DinhMucSuDung) AS LuongNPLChuaHH ,  
                ToKhai.SoLuong * ( DM.DinhMucSuDung / 100 * DM.TyLeHaoHut  
                                   + DM.DinhMucSuDung ) AS LuongNPLCoHH  
        FROM    v_KDT_SP_Xuat_HD  
                INNER JOIN ( SELECT t_KDT_ToKhaiMauDich.SoToKhai ,  
                                    t_KDT_ToKhaiMauDich.MaLoaiHinh ,  
                                    t_KDT_ToKhaiMauDich.NgayDangKy ,  
                                    t_KDT_ToKhaiMauDich.IDHopDong ,  
                                    t_KDT_ToKhaiMauDich.SoHopDong ,  
                                    t_KDT_ToKhaiMauDich.MaHaiQuan ,  
                                    t_KDT_HangMauDich.MaPhu AS MaHang ,  
                                    t_KDT_HangMauDich.TenHang ,  
                                    t_KDT_HangMauDich.SoLuong ,  
                                    t_KDT_HangMauDich.SoThuTuHang ,  
                                    t_KDT_ToKhaiMauDich.MaDoanhNghiep  
                             FROM   t_KDT_ToKhaiMauDich  
                                    INNER JOIN t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID  
                             WHERE  ( t_KDT_ToKhaiMauDich.TrangThaiXuLy IN ( 0,  
                                                              1 ) )  
                                    AND ( t_KDT_ToKhaiMauDich.IDHopDong IN (  
                                          @HopDong_ID ) )  
                                    AND ( t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%' )  
                                    AND ( t_KDT_ToKhaiMauDich.MaLoaiHinh NOT LIKE 'XVB13' )  
                                    AND ( t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' )  
                             UNION ALL  
                             SELECT t_KDT_GC_ToKhaiChuyenTiep.SoToKhai ,  
                                    t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,  
                                    t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,  
                                    t_KDT_GC_ToKhaiChuyenTiep.IDHopDong ,  
                                    t_KDT_GC_ToKhaiChuyenTiep.SoHopDongDV AS SoHopDong ,  
                                    t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan ,  
                                    t_KDT_GC_HangChuyenTiep.MaHang ,  
                                    t_KDT_GC_HangChuyenTiep.TenHang ,  
                                    t_KDT_GC_HangChuyenTiep.SoLuong ,  
                                    t_KDT_GC_HangChuyenTiep.SoThuTuHang ,  
                                    t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep  
                             FROM   t_KDT_GC_ToKhaiChuyenTiep  
                                    INNER JOIN t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID  
                             WHERE  ( t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy IN (  
                                      0, 1 ) )  
                                    AND ( t_KDT_GC_ToKhaiChuyenTiep.IDHopDong IN (  
                                          @HopDong_ID ) )  
                                    AND ( t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX'  
                                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19'  
                                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XVE54'  
                                        )  
                           ) AS ToKhai ON v_KDT_SP_Xuat_HD.IDHopDong = ToKhai.IDHopDong  
                                          AND v_KDT_SP_Xuat_HD.MaPhu = ToKhai.MaHang  
                                          AND v_KDT_SP_Xuat_HD.SoThuTuHang = ToKhai.SoThuTuHang  
                INNER JOIN t_HaiQuan_DonViTinh ON v_KDT_SP_Xuat_HD.DVT_ID = t_HaiQuan_DonViTinh.ID  
                INNER JOIN t_HaiQuan_LoaiHinhMauDich ON ToKhai.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID  
                INNER JOIN dbo.t_GC_DinhMuc AS DM ON ToKhai.MaHang = DM.MaSanPham  
                                                     AND ToKhai.IDHopDong = DM.HopDong_ID  
        WHERE   ( v_KDT_SP_Xuat_HD.IDHopDong IN ( @HopDong_ID ) )  
        ORDER BY ToKhai.IDHopDong ,  
                MaNguyenPhuLieu ,  
                ToKhai.NgayDangKy ,  
                MaHang                   
    END  
                                  
								                                                                               
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_GC_XuatNhapTon_TT38]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_GC_XuatNhapTon_TT38]  
GO 
CREATE PROC [dbo].[p_GC_XuatNhapTon_TT38]  
    @HopDong_ID NVARCHAR(MAX)  
AS  
    BEGIN  
        SELECT  0 AS STT ,  
                152 AS SoTaiKhoan ,  
                n.OldHD_ID AS [HopDong_ID] ,  
                hd.SoHopDong ,  
                t.SoToKhai ,  
                t.SoToKhaiVnacc ,  
                t.NgayDangKy ,  
                t.MaLoaiHinh ,  
                [Ma] ,  
                [Ten] ,  
                '' AS [MaHS] ,  
                n.DVT AS DVT_ID ,  
                TongLuongNK AS [SoLuongDangKy] ,  
                TongLuongNK AS [SoLuongDaNhap] ,  
                TongLuongXK AS [SoLuongDaDung] ,  
                TongLuongCU AS [SoLuongCungUng] ,  
                ( TongLuongNK + TongLuongCU ) - TongLuongXK AS LuongTon ,  
                t.SoLuong ,  
                t.SoLuong AS LuongSD_TK ,  
                t.SoLuong AS LuongTon_TK ,  
                CASE WHEN t.TyGiaTT > 0  
                     THEN CONVERT(DECIMAL(24, 6), ( t.DonGiaTT / t.TyGiaTT ))  
                     ELSE 0  
                END AS DonGiaKB ,  
                CONVERT(DECIMAL(24, 6), t.DonGiaTT) AS DonGiaTT ,  
                CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TriGiaTT ,  
                CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TriGiaSuDung ,  
                CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TonTriGia ,  
                t.TyGiaTT ,  
                t.MaDVT  
        FROM    [dbo].[t_GC_ThanhKhoanHDGC] n  
                JOIN ( SELECT   dk.HopDong_ID AS IDHopDong ,  
                                dk.SoTiepNhan AS SoToKhai ,  
                                dk.SoTiepNhan AS SoToKhaiVnacc ,  
                                dk.NgayTiepNhan AS NgayDangKy ,  
                                'NLHUY' AS MaLoaiHinh ,  
                                cu.MaNPL AS MaPhu ,  
                                cu.TenNPL AS TenHang ,  
                                cu.LuongNPLHuy AS SoLuong ,  
                                1 AS DonGiaTT ,  
                                1 AS TriGiaTT ,  
                                1 AS TyGiaTT ,  
                                'VND' AS MaDVT  
                       FROM     t_KDT_GC_NPLHuyDangKy AS dk  
                                JOIN dbo.t_KDT_GC_NPLHuy AS cu ON dk.ID = cu.Master_ID  
                       WHERE    dk.HopDong_ID IN ( @HopDong_ID )  
                       UNION ALL  
                       SELECT   dk.HopDong_ID AS IDHopDong ,  
                                dk.SoTiepNhan AS SoToKhai ,  
                                dk.SoTiepNhan AS SoToKhaiVnacc ,  
                                dk.NgayTiepNhan AS NgayDangKy ,  
                                'NPLCU' AS MaLoaiHinh ,  
                                cu.MaNPL AS MaPhu ,  
                                cu.TenNPL AS TenHang ,  
                                cu.LuongCungUng AS SoLuong ,  
                                1 AS DonGiaTT ,  
                                1 AS TriGiaTT ,  
                                1 AS TyGiaTT ,  
                                'VND' AS MaDVT  
                       FROM     t_KDT_GC_CungUngDangKy AS dk  
                                JOIN dbo.t_KDT_GC_CungUng AS cu ON dk.ID = cu.Master_ID  
                       WHERE    dk.HopDong_ID IN ( @HopDong_ID )  
                       UNION ALL  
                       SELECT   tkmd.IDHopDong ,  
                                tkmd.SoToKhai ,  
                                tkmd.Huongdan_PL AS SoToKhaiVnacc ,  
                                tkmd.NgayDangKy ,  
                                tkmd.MaLoaiHinh ,  
                                hmd.MaHang AS MaPhu ,  
                                hmd.TenHang ,  
                                hmd.SoLuong ,  
                                hmd.DonGiaTT ,  
                                hmd.TriGiaTT ,  
                                CASE WHEN tkmd.Huongdan_PL <> ''  
                                     THEN ( SELECT TOP 1  
                                                    g.TyGiaTinhThue  
                                FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g  
                                            WHERE   Master_ID = ( SELECT TOP 1  
                                                              ID  
                                                              FROM  
                                                              t_KDT_VNACC_ToKhaiMauDich t  
                                                              WHERE  
                                                              tkmd.SoToKhai = ( SELECT TOP 1  
                                                              SoTK  
                                                              FROM  
                                                              dbo.t_VNACCS_CapSoToKhai  
                                                              WHERE  
                                                              SoTKVNACCS = t.SoToKhai  
                                                              )  
                                                              )  
                                          )  
                                     ELSE 0  
                                END AS TyGiaTT ,  
                                CASE WHEN tkmd.Huongdan_PL <> ''  
                                     THEN ( SELECT TOP 1  
                                                    g.MaTTTyGiaTinhThue  
                                            FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g  
                                            WHERE   Master_ID = ( SELECT TOP 1  
                                                              ID  
                                                              FROM  
                                                              t_KDT_VNACC_ToKhaiMauDich t  
                                                              WHERE  
                                                              tkmd.SoToKhai = ( SELECT TOP 1  
                                                              SoTK  
                                                              FROM  
                                                              dbo.t_VNACCS_CapSoToKhai  
                                                              WHERE  
                                                              SoTKVNACCS = t.SoToKhai  
                                                              )  
                                                              )  
                                          )  
                                     ELSE ''  
                                END AS MaDVT  
                       FROM     t_KDT_GC_HangChuyenTiep hmd  
                                INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tkmd ON hmd.Master_ID = tkmd.ID  
                       WHERE    tkmd.IDHopDong IN ( @HopDong_ID )  
                                AND MaLoaiHinh LIKE '%NV%'  
                                AND LoaiHangHoa = 'N'  
                                AND tkmd.TrangThaiXuLy = 1  
                       UNION ALL  
                       SELECT   tkmd.IDHopDong ,  
                                tkmd.SoToKhai ,  
                                tkmd.Huongdan_PL AS SoToKhaiVnacc ,  
                                tkmd.NgayDangKy ,  
                                tkmd.MaLoaiHinh ,  
                                hmd.MaHang AS MaPhu ,  
                                hmd.TenHang ,  
                                hmd.SoLuong ,  
                                hmd.DonGiaTT ,  
                                hmd.TriGiaTT ,  
                                CASE WHEN tkmd.Huongdan_PL <> ''  
                                     THEN ( SELECT TOP 1  
                                                    g.TyGiaTinhThue  
                                            FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g  
                                            WHERE   Master_ID = ( SELECT TOP 1  
                                                              ID  
                         FROM  
                                                              t_KDT_VNACC_ToKhaiMauDich t  
                                                              WHERE  
                                                              tkmd.SoToKhai = ( SELECT TOP 1  
                                                              SoTK  
                                                              FROM  
                                                              dbo.t_VNACCS_CapSoToKhai  
                                                              WHERE  
                                                              SoTKVNACCS = t.SoToKhai  
                                                              )  
                                                              )  
                                          )  
                                     ELSE 0  
                                END AS TyGiaTT ,  
                                CASE WHEN tkmd.Huongdan_PL <> ''  
                                     THEN ( SELECT TOP 1  
                                                    g.MaTTTyGiaTinhThue  
                                            FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g  
                                            WHERE   Master_ID = ( SELECT TOP 1  
                                                              ID  
                                                              FROM  
                                                              t_KDT_VNACC_ToKhaiMauDich t  
                                                              WHERE  
                                                              tkmd.SoToKhai = ( SELECT TOP 1  
                                                              SoTK  
                                                              FROM  
                                                              dbo.t_VNACCS_CapSoToKhai  
                                                              WHERE  
                                                              SoTKVNACCS = t.SoToKhai  
                                                              )  
                                                              )  
                                          )  
                                     ELSE ''  
                                END AS MaDVT  
                       FROM     t_KDT_GC_HangChuyenTiep hmd  
                                INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tkmd ON hmd.Master_ID = tkmd.ID  
                       WHERE    tkmd.IDHopDong IN ( @HopDong_ID )  
                                AND MaLoaiHinh LIKE '%XV%'  
                                AND LoaiHangHoa = 'N'  
                                AND tkmd.TrangThaiXuLy = 1  
                       UNION ALL  
                       SELECT   tkmd.IDHopDong ,  
                                tkmd.SoToKhai ,  
                                tkmd.LoaiVanDon AS SoToKhaiVnacc ,  
                                tkmd.NgayDangKy ,  
                                tkmd.MaLoaiHinh ,  
                                hmd.MaPhu ,  
                                hmd.TenHang ,  
                                hmd.SoLuong ,  
                                hmd.DonGiaTT ,  
                                hmd.TriGiaTT ,  
                                CASE WHEN tkmd.LoaiVanDon <> ''  
                                     THEN ( SELECT TOP 1  
                                                    g.TyGiaTinhThue  
                                            FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g  
                                            WHERE   Master_ID = ( SELECT TOP 1  
                                                              ID  
                                                              FROM  
                                                              t_KDT_VNACC_ToKhaiMauDich t  
                                                              WHERE  
                                                              tkmd.SoToKhai = ( SELECT TOP 1  
                                                              SoTK  
                                                              FROM  
                                                              dbo.t_VNACCS_CapSoToKhai  
                                                              WHERE  
                                                              SoTKVNACCS = t.SoToKhai  
                                                              )  
                                                              )  
                                          )  
                                     ELSE 0  
                                END AS TyGiaTT ,  
                                CASE WHEN tkmd.LoaiVanDon <> ''  
                                     THEN ( SELECT TOP 1  
                                                    g.MaTTTyGiaTinhThue  
                                            FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g  
                                            WHERE   Master_ID = ( SELECT TOP 1  
                                                              ID  
                                                              FROM  
                                                              t_KDT_VNACC_ToKhaiMauDich t  
                                                              WHERE  
                                                              tkmd.SoToKhai = ( SELECT TOP 1  
                                                              SoTK  
                                                              FROM  
                                                              dbo.t_VNACCS_CapSoToKhai  
                                                              WHERE  
                                                              SoTKVNACCS = t.SoToKhai  
                                                              )  
                                                              )  
                                          )  
                                     ELSE ''  
                                END AS MaDVT  
                       FROM     t_KDT_HangMauDich hmd  
                                INNER JOIN t_KDT_ToKhaiMauDich tkmd ON hmd.TKMD_ID = tkmd.ID  
                       WHERE    tkmd.IDHopDong IN ( @HopDong_ID )  
                                AND MaLoaiHinh LIKE '%NV%'  
                                AND LoaiHangHoa = 'N'  
                                AND tkmd.TrangThaiXuLy = 1  
                     ) t ON n.OldHD_ID = t.IDHopDong  
                            AND n.Ma = t.MaPhu  
                JOIN t_KDT_GC_HopDong hd ON hd.ID = n.OldHD_ID  
        WHERE   hd.ID IN ( @HopDong_ID )  
        UNION ALL  
        SELECT  0 AS STT ,  
                155 AS SoTaiKhoan ,  
                [HopDong_ID] ,  
                hd.SoHopDong ,  
                t.SoToKhai ,  
                t.SoToKhaiVnacc ,  
                t.NgayDangKy ,  
                t.MaLoaiHinh ,  
                [Ma] ,  
                [Ten] ,  
                n.[MaHS] ,  
                CASE WHEN DVT_ID <> '' THEN ( SELECT    Ten  
                                              FROM      t_HaiQuan_DonViTinh  
                                              WHERE     ID = n.DVT_ID  
                                            )  
                     ELSE ''  
                END AS DVT_ID ,  
                [SoLuongDangKy] ,  
                SoLuongDaXuat AS [SoLuongDaNhap] ,  
                SoLuongDaXuat AS [SoLuongDaDung] ,  
                0 AS [SoLuongCungUng] ,  
                0 AS LuongTon ,  
                t.SoLuong ,  
                t.SoLuong AS LuongSD_TK ,  
                0 AS LuongTon_TK ,  
                CASE WHEN t.TyGiaTT > 0  
                     THEN CONVERT(DECIMAL(24, 6), ( t.DonGiaTT / t.TyGiaTT ))  
                     ELSE 0  
                END AS DonGiaKB ,  
                CONVERT(DECIMAL(24, 6), t.DonGiaTT) AS DonGiaTT ,  
                CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TriGiaTT ,  
                CONVERT(DECIMAL(24, 0), t.TriGiaTT) AS TriGiaSuDung ,  
                0 AS TonTriGia ,  
                t.TyGiaTT ,  
                t.MaDVT  
        FROM    [dbo].[t_GC_SanPham] n  
                JOIN ( SELECT   tkmd.IDHopDong ,  
                                tkmd.SoToKhai ,  
                                tkmd.Huongdan_PL AS SoToKhaiVnacc ,  
                                tkmd.NgayDangKy ,  
                                tkmd.MaLoaiHinh ,  
                                hmd.MaHang AS MaPhu ,  
                                hmd.TenHang ,  
                                hmd.SoLuong ,  
                                hmd.DonGiaTT ,  
                                hmd.TriGiaTT ,  
                                CASE WHEN tkmd.Huongdan_PL <> ''  
                                     THEN ( SELECT TOP 1  
                                                    g.TyGiaTinhThue  
                                            FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g  
                                            WHERE   Master_ID = ( SELECT TOP 1  
                                                              ID  
                                                              FROM  
                                                              t_KDT_VNACC_ToKhaiMauDich t  
                                                              WHERE  
                                                              tkmd.SoToKhai = ( SELECT TOP 1  
                                                              SoTK  
                                                              FROM  
                                                              dbo.t_VNACCS_CapSoToKhai  
                                                              WHERE  
                                                              SoTKVNACCS = t.SoToKhai  
                                                              )  
                                                              )  
                                          )  
                                     ELSE 0  
                                END AS TyGiaTT ,  
                                CASE WHEN tkmd.Huongdan_PL <> ''  
                                     THEN ( SELECT TOP 1  
                                                    g.MaTTTyGiaTinhThue  
                                            FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g  
                                            WHERE   Master_ID = ( SELECT TOP 1  
                                                              ID  
                                                              FROM  
                                                              t_KDT_VNACC_ToKhaiMauDich t  
                                                              WHERE  
                                                              tkmd.SoToKhai = ( SELECT TOP 1  
                                                              SoTK  
                                                              FROM  
                                                              dbo.t_VNACCS_CapSoToKhai  
                                                              WHERE  
                                                              SoTKVNACCS = t.SoToKhai  
                                                              )  
                                                              )  
                                          )  
                                     ELSE ''  
                                END AS MaDVT  
                       FROM     t_KDT_GC_HangChuyenTiep hmd  
                                INNER JOIN t_KDT_GC_ToKhaiChuyenTiep tkmd ON hmd.Master_ID = tkmd.ID  
                       WHERE    tkmd.IDHopDong IN ( @HopDong_ID )  
                                AND MaLoaiHinh LIKE '%V%'  
                                AND LoaiHangHoa = 'S'  
                                AND tkmd.TrangThaiXuLy = 1  
                       UNION ALL  
                       SELECT   tkmd.IDHopDong ,  
                                tkmd.SoToKhai ,  
                                tkmd.LoaiVanDon AS SoToKhaiVnacc ,  
                                tkmd.NgayDangKy ,  
                                tkmd.MaLoaiHinh ,  
                                hmd.MaPhu ,  
                                hmd.TenHang ,  
                                hmd.SoLuong ,  
                                hmd.DonGiaTT ,  
                                hmd.TriGiaTT ,  
                                CASE WHEN tkmd.LoaiVanDon <> ''  
                                     THEN ( SELECT TOP 1  
                                                    g.TyGiaTinhThue  
                                            FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g  
                                            WHERE   Master_ID = ( SELECT TOP 1  
                                                              ID  
                                                              FROM  
                                                              t_KDT_VNACC_ToKhaiMauDich t  
                                                              WHERE  
                                                              tkmd.SoToKhai = ( SELECT TOP 1  
                                                              SoTK  
                                                              FROM  
                                                              dbo.t_VNACCS_CapSoToKhai  
                                                              WHERE  
                                                              SoTKVNACCS = t.SoToKhai  
                                                              )  
                                                              )  
                                          )  
                                     ELSE 0  
                                END AS TyGiaTT ,  
                                CASE WHEN tkmd.LoaiVanDon <> ''  
                                     THEN ( SELECT TOP 1  
                                                    g.MaTTTyGiaTinhThue  
                                            FROM    t_KDT_VNACC_TK_PhanHoi_TyGia g  
                                            WHERE   Master_ID = ( SELECT TOP 1  
                                                              ID  
                                                              FROM  
                                                              t_KDT_VNACC_ToKhaiMauDich t  
                                                              WHERE  
                                                              tkmd.SoToKhai = ( SELECT TOP 1  
                                                              SoTK  
                                                              FROM  
                                                              dbo.t_VNACCS_CapSoToKhai  
                                                              WHERE  
                                                              SoTKVNACCS = t.SoToKhai  
                                                              )  
                                                              )  
                                          )  
                                     ELSE ''  
                                END AS MaDVT  
                       FROM     t_KDT_HangMauDich hmd  
                                INNER JOIN t_KDT_ToKhaiMauDich tkmd ON hmd.TKMD_ID = tkmd.ID  
                       WHERE    tkmd.IDHopDong IN ( @HopDong_ID )  
                                AND ( MaLoaiHinh LIKE '%XVB13%'  
                                      OR MaLoaiHinh LIKE '%NVA31%'  
                                    )  
                                AND LoaiHangHoa = 'S'  
                                AND tkmd.TrangThaiXuLy = 1  
                       UNION ALL  
                       SELECT   tkmd.IDHopDong ,  
                                tkmd.SoToKhai ,  
                                CONVERT(VARCHAR(18), tkmd.LoaiVanDon) AS SoToKhaiVnacc ,  
                                tkmd.NgayDangKy ,  
                   tkmd.MaLoaiHinh ,  
                                hmd.MaPhu ,  
                                hmd.TenHang ,  
                                hmd.SoLuong ,  
                                hmd.DonGiaTT ,  
                                hmd.TriGiaTT ,  
                                tkmd.TyGiaTinhThue AS TyGiaTT ,  
                                tkmd.NguyenTe_ID AS MaDVT  
                       FROM     t_KDT_HangMauDich hmd  
                                INNER JOIN t_KDT_ToKhaiMauDich tkmd ON hmd.TKMD_ID = tkmd.ID  
                       WHERE    tkmd.IDHopDong IN ( @HopDong_ID )  
                                AND MaLoaiHinh LIKE '%V%'  
                                AND LoaiHangHoa = 'S'  
                                AND TrangThaiXuLy = 1  
                       UNION ALL  
                       SELECT   tkmd.IDHopDong ,  
                                tkmd.SoToKhai ,  
                                CONVERT(VARCHAR(18), tkmd.LoaiVanDon) AS SoToKhaiVnacc ,  
                                tkmd.NgayDangKy ,  
                                tkmd.MaLoaiHinh ,  
                                dbo.t_GC_DinhMuc.MaNguyenPhuLieu AS MaPhu ,  
                                dbo.t_GC_DinhMuc.TenNPL AS TenHang ,  
                                hmd.SoLuong * ( dbo.t_GC_DinhMuc.DinhMucSuDung  
                                                / 100  
                                                * dbo.t_GC_DinhMuc.TyLeHaoHut  
                                                + dbo.t_GC_DinhMuc.DinhMucSuDung ) AS SoLuong ,  
                                hmd.DonGiaTT ,  
                                hmd.TriGiaTT ,  
                                tkmd.TyGiaTinhThue AS TyGiaTT ,  
                                tkmd.NguyenTe_ID AS MaDVT  
                       FROM     dbo.t_KDT_ToKhaiMauDich tkmd  
                                INNER JOIN dbo.t_KDT_HangMauDich hmd ON tkmd.ID = hmd.TKMD_ID  
                                INNER JOIN dbo.t_GC_DinhMuc ON dbo.t_GC_DinhMuc.MaSanPham = hmd.MaPhu  
                                                              AND dbo.t_GC_DinhMuc.HopDong_ID = tkmd.IDHopDong  
                                INNER JOIN dbo.t_GC_NguyenPhuLieu ON dbo.t_GC_DinhMuc.HopDong_ID = dbo.t_GC_NguyenPhuLieu.HopDong_ID  
                                                              AND dbo.t_GC_DinhMuc.MaNguyenPhuLieu = dbo.t_GC_NguyenPhuLieu.Ma  
                       WHERE    tkmd.IDHopDong IN ( @HopDong_ID )  
                                AND ( MaLoaiHinh LIKE '%XVB13%'  
                                      OR MaLoaiHinh LIKE '%NVA31%'  
                                    )  
                                AND LoaiHangHoa = 'S'  
                                AND tkmd.TrangThaiXuLy = 1  
                     ) t ON n.HopDong_ID = t.IDHopDong  
                            AND n.Ma = t.MaPhu  
                JOIN t_KDT_GC_HopDong hd ON hd.ID = n.HopDong_ID  
        WHERE   hd.ID IN ( @HopDong_ID )  
        ORDER BY SoTaiKhoan ASC ,  
                HopDong_ID ,  
                n.Ma ,  
                NgayDangKy  
    END               
	
	GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_HoSoQuyetToan_DSHopDong_Load]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HoSoQuyetToan_DSHopDong_Load]
GO  
	------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_HoSoQuyetToan_DSHopDong_Load]    
-- Database: ECS_TQDT_GC_V4    
-- Author: Ngo Thanh Tung    
-- Time created: Friday, February 26, 2016    
------------------------------------------------------------------------------------------------------------------------    
   
CREATE PROCEDURE [dbo].[p_HoSoQuyetToan_DSHopDong_Load]    
 @Master_ID bigint    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
SELECT    
 [ID],    
 [Master_ID],    
 [HopDong_ID],    
 [SoHopDong],    
 [NgayKy],    
 [NgayHetHan]    
FROM    
 [dbo].[t_KDT_GC_HoSoQuyetToan_DSHopDong]    
WHERE    
 [Master_ID] = @Master_ID 

GO  
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_KDT_Message_Load]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_Message_Load]  
GO  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_Message_Load]  
-- Database: ECS.TQDT.KD  
-- Author: Ngo Thanh Tung  
-- Time created: Monday, May 24, 2010  
------------------------------------------------------------------------------------------------------------------------  
CREATE PROCEDURE [dbo].[p_KDT_Message_Load]  
 @ID  BIGINT 
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
SELECT  
 [ID],  
 [ReferenceID],  
 [ItemID],  
 [MessageFrom],  
 [MessageTo],  
 [MessageType],  
 [MessageFunction],  
 [MessageContent],  
 [CreatedTime],  
 [TieuDeThongBao],  
 [NoiDungThongBao]  
FROM  
 [dbo].[t_KDT_Messages]  
WHERE  
 [ID] = @ID                                  

 GO
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '24.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('24.2',GETDATE(), N'CẬP NHẬT KHAI BÁO BÁO CÁO QUYẾT TOÁN')
END