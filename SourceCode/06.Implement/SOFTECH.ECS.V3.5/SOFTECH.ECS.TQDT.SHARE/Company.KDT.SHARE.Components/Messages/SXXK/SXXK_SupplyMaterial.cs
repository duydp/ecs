﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.SXXK
{
    public class SXXK_SupplyMaterial
    {

        [XmlElement("Material")]
        public List<SXXK_MaterialSupply_Material> Materials { get; set; }



    }
}
