﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Vouchers;
using Company.KDT.SHARE.Components.Messages.Send;
#if GC_V4
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
#endif
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho;
using Company.Interface.GC.WareHouse;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;
#if SXXK_V4
using Company.BLL.KDT.SXXK;
using Company.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho;
using Company.Interface.GC.WareHouse;
#endif

namespace Company.Interface.VNACCS.WareHouse
{
    public partial class VNACC_WareHouseExportForm : BaseFormHaveGuidPanel
    {
        public T_KDT_VNACCS_WarehouseExport wareHouseExport = new T_KDT_VNACCS_WarehouseExport();
        public T_KDT_VNACCS_WarehouseExport_Detail wareHouseDetail;
        public T_KDT_VNACCS_WarehouseExport_GoodsDetail wareHouseGoodDetial;
        public List<T_KDT_VNACCS_WarehouseExport_Detail> wareHouseDetailCollection = new List<T_KDT_VNACCS_WarehouseExport_Detail>();
        public List<T_KDT_VNACCS_WarehouseExport_GoodsDetail> wareHouseGoodDetaiCollection = new List<T_KDT_VNACCS_WarehouseExport_GoodsDetail>();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        KDT_VNACC_ToKhaiMauDich TKMD;
#if GC_V4
        public HopDong HD;
#endif
        public VNACC_WareHouseExportForm()
        {
            InitializeComponent();
        }

        private void VNACC_WareHouseExportForm_Load(object sender, EventArgs e)
        {
            try
            {
                cbbLoaiCT.SelectedIndex = 0;
#if GC_V4
                BindHopDong();
#elif SXXK_V4
                grbHopDong.Visible = false;
#endif
                if (wareHouseExport.ID == 0)
                {
                    wareHouseExport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    txtTenDN.Text = GlobalSettings.TEN_DON_VI;
                    txtMaDN.Text = GlobalSettings.MA_DON_VI;
                }
                else
                {
                    wareHouseExport = T_KDT_VNACCS_WarehouseExport.Load(wareHouseExport.ID);
                    wareHouseExport.WarehouseExportCollection = T_KDT_VNACCS_WarehouseExport_Detail.SelectCollectionBy_WarehouseExport_ID(wareHouseExport.ID);
                    SetwareHouseExport();
                }
                BindData();
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void BindHopDong()
        {
#if GC_V4
            HopDong hd = new HopDong();
            hd.SoHopDong = "";
            hd.ID = 0;
            List<HopDong> collection;
            {
                string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                try
                {
                    collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
                    collection.Add(hd);
                    cbbSoHD.DataSource = collection;
                    cbbSoHD.DisplayMember = "SoHopDong";
                    cbbSoHD.ValueMember = "ID";
                    cbbSoHD.SelectedIndex = collection.Count - 1;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
#endif

        }
        private void SetCommandStatus()
        {
            if (wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = wareHouseExport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseExport.NgayTN;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                dtpNgayTN.Value = DateTime.Now;
                lblTrangThai.Text = wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = wareHouseExport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseExport.NgayTN;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO || wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = setText("Đã khai báo", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = wareHouseExport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseExport.NgayTN;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAddExcel.Enabled = cmdAddExcel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = wareHouseExport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseExport.NgayTN;
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled = cmdSendEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTN.Text = wareHouseExport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseExport.NgayTN;
                lblTrangThai.Text = setText("Chờ duyệt", "Wait approved");
                this.OpenType = OpenFormType.View;
            }

        }
        private void cmMainPXK_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdAdd":
                    this.Add();
                    break;
                case "cmdSelect":
                    this.SelectWareHouseExport();
                    break;
                case "cmdAddExcel":
                    this.ImportExcel();
                    break;
                case "cmdSend":
                    this.SendV5("Send");
                    break;
                case "cmdSendEdit":
                    this.SendV5("Edit");
                    break;
                case "cmdCancel":
                    if (ShowMessage("DOANH NGHIỆP CÓ CHẮC CHẮN MUỐN KHAI BÁO HỦY PHIẾU XUẤT KHO NÀY ĐẾN HQ KHÔNG ? ", true) == "Yes")
                    {
                        wareHouseExport.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        wareHouseExport.Update();
                        SetCommandStatus();
                        this.SendV5("Cancel");
                    }
                    break;
                case "cmdFeedback":
                    this.FeedbackV5();
                    break;
                case "cmdResult":
                    this.Result();
                    break;
                case "cmdEdit":
                    this.Edit();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGuidString();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
                default:
                    break;
            }
        }
        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = wareHouseExport.ID;
                f.loaiKhaiBao = LoaiKhaiBao.WareHouseExport;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", wareHouseExport.ID, LoaiKhaiBao.WareHouseExport), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageWarehouseExport(wareHouseExport);
                    wareHouseExport.InsertUpdateFull();
                    ShowMessage("Cập nhật thông tin thành công .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SelectWareHouseExport()
        {
            try
            {
                string whereCondition = "";
                string LoaiChungTu = "X";
                WareHouseManager f = new WareHouseManager();
                f.IsShowChonPXNK = true;
                f.whereCondition = whereCondition;
                f.LoaiChungTu = LoaiChungTu;
#if GC_V4
                if (HD != null)
                {
                    f.hd = HD;
                }
#endif
                f.ShowDialog(this);
                if (f.PhieuXNKhoCollectionSelected.Count >=1)
                {
                    foreach (T_KHOKETOAN_PHIEUXNKHO PhieuXNK in f.PhieuXNKhoCollectionSelected)
                    {
                        if (PhieuXNK.LOAIHANGHOA == "N")
                        {
                            // Phiếu xuất kho NPL
                            T_KDT_VNACCS_WarehouseExport_Detail wareHouseDetail = new T_KDT_VNACCS_WarehouseExport_Detail();
                            wareHouseDetail.SoPhieuXuat = PhieuXNK.SOCT;
                            wareHouseDetail.NgayPhieuXuat = PhieuXNK.NGAYCT;
                            wareHouseDetail.MaNguoiNhanHang = PhieuXNK.MADN;
                            wareHouseDetail.TenNguoiNhanHang = PhieuXNK.TENDN;
                            foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in PhieuXNK.HangCollection)
                            {
                                T_KDT_VNACCS_WarehouseExport_GoodsDetail wareHouseGoodDetail = new T_KDT_VNACCS_WarehouseExport_GoodsDetail();
                                wareHouseGoodDetail.MaHangHoa = item.MAHANGMAP;
                                wareHouseGoodDetail.TenHangHoa = item.TENHANGMAP;
                                wareHouseGoodDetail.DVT = item.DVTMAP.ToString();
                                //wareHouseGoodDetail.MaDinhDanhSX = item.MaDinhDanhSX;
                                wareHouseGoodDetail.LoaiHangHoa = 1;
                                wareHouseGoodDetail.MucDichSuDung = 3;
                                wareHouseGoodDetail.SoLuongDuKienXuat = Convert.ToDecimal(item.SOLUONG);
                                wareHouseGoodDetail.SoLuongThucXuat = Convert.ToDecimal(item.SOLUONG);
                                wareHouseDetail.Collection.Add(wareHouseGoodDetail);
                            }
                            wareHouseExport.WarehouseExportCollection.Add(wareHouseDetail);

                        }
                        else if (PhieuXNK.LOAIHANGHOA == "S")
                        {
                            // Phiếu xuất kho SP
                            T_KDT_VNACCS_WarehouseExport_Detail wareHouseDetail = new T_KDT_VNACCS_WarehouseExport_Detail();
                            wareHouseDetail.SoPhieuXuat = PhieuXNK.SOCT;
                            wareHouseDetail.NgayPhieuXuat = PhieuXNK.NGAYCT;
                            wareHouseDetail.MaNguoiNhanHang = PhieuXNK.MADN;
                            wareHouseDetail.TenNguoiNhanHang = PhieuXNK.TENDN;
                            foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA item in PhieuXNK.HangCollection)
                            {
                                T_KDT_VNACCS_WarehouseExport_GoodsDetail wareHouseGoodDetail = new T_KDT_VNACCS_WarehouseExport_GoodsDetail();
                                wareHouseGoodDetail.MaHangHoa = item.MAHANGMAP;
                                wareHouseGoodDetail.TenHangHoa = item.TENHANGMAP;
                                wareHouseGoodDetail.DVT = item.DVTMAP.ToString();
                                //wareHouseGoodDetail.MaDinhDanhSX = item.MaDinhDanhSX;
                                wareHouseGoodDetail.LoaiHangHoa = 2;
                                wareHouseGoodDetail.MucDichSuDung = 1;
                                wareHouseGoodDetail.SoLuongDuKienXuat = Convert.ToDecimal(item.SOLUONG);
                                wareHouseGoodDetail.SoLuongThucXuat = Convert.ToDecimal(item.SOLUONG);
                                wareHouseDetail.Collection.Add(wareHouseGoodDetail);
                            }
                            wareHouseExport.WarehouseExportCollection.Add(wareHouseDetail);
                        }   
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateGuidString()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_VNACCS_WarehouseExport", "", Convert.ToInt32(wareHouseExport.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void Edit()
        {
            try
            {
                if (ShowMessage("DOANH NGHIỆP CÓ CHẮC CHẮN MUỐN CHUYỂN PHIẾU XUẤT KHO NÀY SANG SỬA KHÔNG ? ", true) == "Yes")
                {
                    wareHouseExport.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    wareHouseExport.Update();
                    SetCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMaDN, errorProvider, "MÃ DOANH NGHIỆP ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenDN, errorProvider, "TÊN DOANH NGHIỆP ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaKho, errorProvider, "MÃ KHO", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenKho, errorProvider, "TÊN KHO", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiCT, errorProvider, "LOẠI CHỨNG TỪ XNK / TỜ KHAI ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoTKCT, errorProvider, "SỐ CHỨNG TỪ XNK / TỜ KHAI", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void Save()
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (wareHouseExport.WarehouseExportCollection.Count == 0)
                {
                    ShowMessage("DOANH NGHIỆP CHƯA THÊM DANH SÁCH PHIẾU NHẬP KHO ", false);
                    return;
                }
                GetwareHouseExport();
                wareHouseExport.InsertUpdateFull();
                BindData();
                ShowMessage("Lưu thành công .", false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Thông báo lỗi\r\n" + ex, false);
            }
        }
        private void GetwareHouseExport()
        {
            try
            {
                wareHouseExport.SoTN = Convert.ToInt32(txtSoTN.Text);
                wareHouseExport.NgayTN = dtpNgayTN.Value;
                wareHouseExport.MaHQ = GlobalSettings.MA_HAI_QUAN;
                wareHouseExport.MaDoanhNghiep = txtMaDN.Text;
                wareHouseExport.TenDoanhNghiep = txtTenDN.Text;
                wareHouseExport.NgayBatDauBC = dtpNgayBatDau.Value;
                wareHouseExport.NgayKetthucBC = dtpNgayKetThuc.Value;
                wareHouseExport.MaKho = txtMaKho.Text;
                wareHouseExport.TenKho = txtTenKho.Text;
                wareHouseExport.Loai = Convert.ToDecimal(cbbLoaiCT.SelectedValue.ToString());
                wareHouseExport.SoTKChungTu = txtSoTKCT.Text;
                wareHouseExport.SoHopDong = cbbSoHD.Text;
                wareHouseExport.NgayHopDong = clcNgayHD.Value;
                wareHouseExport.NgayHetHanHD = clcNgayHH.Value;
                wareHouseExport.MaHQTiepNhanHD = ctrCoQuanHQ.Ma;
                wareHouseExport.GhiChuKhac = txtGhiChu.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void SetwareHouseExport()
        {
            try
            {
                txtSoTN.Text = wareHouseExport.SoTN.ToString();
                dtpNgayTN.Value = wareHouseExport.NgayTN;
                txtMaDN.Text = wareHouseExport.MaDoanhNghiep;
                txtTenDN.Text = wareHouseExport.TenDoanhNghiep;
                dtpNgayBatDau.Value = wareHouseExport.NgayBatDauBC;
                dtpNgayKetThuc.Value = wareHouseExport.NgayKetthucBC;
                txtMaKho.Text = wareHouseExport.MaKho;
                txtTenKho.Text = wareHouseExport.TenKho;
                cbbLoaiCT.SelectedValue = wareHouseExport.Loai.ToString();
                txtSoTKCT.Text = wareHouseExport.SoTKChungTu;
                ctrCoQuanHQ.Ma = wareHouseExport.MaHQTiepNhanHD;
                cbbSoHD.Text = wareHouseExport.SoHopDong;
                clcNgayHD.Value = wareHouseExport.NgayHopDong;
                clcNgayHH.Value = wareHouseExport.NgayHetHanHD;
                txtGhiChu.Text = wareHouseExport.GhiChuKhac;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void Add()
        {
            try
            {
                VNACC_WareHouseExportGoodsForm f = new VNACC_WareHouseExportGoodsForm();
                f.TKMD = TKMD;
#if GC_V4
                f.HD = HD;
#endif
                f.wareHouseExport = wareHouseExport;
                f.wareHouseDetailCollection = wareHouseDetailCollection;
                f.OpenType = this.OpenType;
                f.ShowDialog(this);
                //int STT = 1;
                //foreach (T_KDT_VNACCS_WarehouseExport_Detail item in wareHouseExport.WarehouseExportCollection)
                //{
                //    item.STT = STT;
                //    STT++;
                //}
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = wareHouseExport.WarehouseExportCollection;
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void ImportExcel()
        {
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }
        private void SendV5(string Status)
        {
            if (ShowMessage("Doanh nghiệp có muốn khai phụ kiện này đến HQ không ?", true) == "Yes")
            {
                if (wareHouseExport.ID == 0)
                {
                    ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo ", false);
                    return;
                }
                else
                {
                    wareHouseExport = T_KDT_VNACCS_WarehouseExport.Load(wareHouseExport.ID);
                    wareHouseExport.WarehouseExportCollection = T_KDT_VNACCS_WarehouseExport_Detail.SelectCollectionBy_WarehouseExport_ID(wareHouseExport.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.WareHouseExport;
                sendXML.master_id = wareHouseExport.ID;

                if (sendXML.Load())
                {
                    if (Status=="Send")
                    {
                        if (wareHouseExport.TrangThaiXuLy==TrangThaiXuLy.CHO_DUYET)
                        {
                            ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                            cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            return;
                        }
                    }
                }
                try
                {
                    string returnMessage = string.Empty;
                    wareHouseExport.GuidStr = Guid.NewGuid().ToString();
                    GC_ImportWareHouse exportWareHouse = new GC_ImportWareHouse();
                    exportWareHouse = Mapper_V4.ToDataTransferExportWareHouseNew(wareHouseExport, wareHouseDetail, wareHouseGoodDetial, GlobalSettings.TEN_DON_VI ,Status);

                    ObjectSend msgSend = new ObjectSend(
                         new NameBase()
                         {
                             Name = GlobalSettings.TEN_DON_VI,
                             Identity = wareHouseExport.MaDoanhNghiep
                         },
                          new NameBase()
                          {
                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(wareHouseExport.MaHQ)),
                              Identity = wareHouseExport.MaHQ
                          },
                          new SubjectBase()
                          {

                              Type = exportWareHouse.Issuer,
                              //Function = DeclarationFunction.SUA,
                              Reference = wareHouseExport.GuidStr,
                          },
                          exportWareHouse
                        );
                    if (Status == "Send")
                    {
                        msgSend.Subject.Function = DeclarationFunction.KHAI_BAO;
                    }
                    else if (Status == "Edit")
                    {
                        msgSend.Subject.Function = DeclarationFunction.SUA;
                    }
                    else
                    {
                        msgSend.Subject.Function = DeclarationFunction.HUY;
                    }
                    wareHouseExport.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    SendMessageForm sendForm = new SendMessageForm();
                    sendForm.Send += SendMessage;
                    bool isSend = sendForm.DoSend(msgSend);
                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(wareHouseExport.ID, MessageTitle.RegisterWareHouseExport);
                        cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        sendXML.LoaiHS = LoaiKhaiBao.WareHouseExport;
                        sendXML.master_id = wareHouseExport.ID;
                        sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                        {
                            SetCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                            wareHouseExport.Update();
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            FeedbackV5();
                            SetCommandStatus();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        sendForm.Message.XmlSaveMessage(wareHouseExport.ID, MessageTitle.RegisterWareHouseExport);
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandStatus();
                    }
                }
                catch (Exception ex)
                {

                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        private void FeedbackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                string reference = wareHouseExport.GuidStr;
                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.EXportWareHouse,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.EXportWareHouse,
                };
                subjectBase.Type = DeclarationIssuer.EXportWareHouse;

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = wareHouseExport.MaDoanhNghiep,

                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(wareHouseExport.MaHQ.Trim())),
                                                  Identity = wareHouseExport.MaHQ
                                              }, subjectBase, null);
                if (wareHouseExport.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(wareHouseExport.MaHQ));
                    msgSend.To.Identity = GlobalSettings.TEN_CUC_HAI_QUAN;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        wareHouseExport.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        wareHouseExport.Update();
                        isFeedBack = false;
                        SetCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.ExportWareHouseSendHandler(wareHouseExport, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = wareHouseExport.ID;
                form.DeclarationIssuer = DeclarationIssuer.EXportWareHouse;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void InPhieuTN()
        {
            try
            {
                if (this.wareHouseExport.SoTN == 0) return;
#if GC_V4
                Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
#elif SXXK_V4
                Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
#endif
                phieuTN.phieu = "PHIẾU XUẤT KHO";
                phieuTN.soTN = wareHouseExport.SoTN.ToString();
                phieuTN.ngayTN = wareHouseExport.NgayTN.ToString("dd/MM/yyyy");
                phieuTN.maHaiQuan = wareHouseExport.MaHQ;
                phieuTN.BindReport();
                phieuTN.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (dgList.GetRows().Length < 0) return;

                T_KDT_VNACCS_WarehouseExport_Detail wareHouseDetail = new T_KDT_VNACCS_WarehouseExport_Detail();
                wareHouseDetail = (T_KDT_VNACCS_WarehouseExport_Detail)dgList.CurrentRow.DataRow;
                VNACC_WareHouseExportGoodsForm f = new VNACC_WareHouseExportGoodsForm();
                f.wareHouseExport = wareHouseExport;
                f.wareHouseDetail = wareHouseDetail;
                f.wareHouseDetailCollection = wareHouseDetailCollection;
                f.ShowDialog(this);
                BindData();
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
                //
            }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {

        }

        private void txtSoTKCT_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                if (cbbLoaiCT.SelectedValue.ToString() == "1")
                {
                    VNACC_ToKhaiManager f = new VNACC_ToKhaiManager();
                    f.IsShowChonToKhai = true;
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.OK)
                    {
                        TKMD = f.TKMDDuocChon;
                        txtSoTKCT.Text = TKMD.SoToKhai.ToString();
#if GC_V4
                        HopDong HD = HopDong.Load(TKMD.HopDong_ID);
                        ctrCoQuanHQ.Ma = HD.MaHaiQuan;
                        cbbSoHD.Text = HD.SoHopDong;
                        clcNgayHD.Value = HD.NgayDangKy;
                        clcNgayHH.Value = HD.NgayHetHan;
#endif
                    }
                }
                else
                {
                    ShowMessageTQDT("DOANH NGHIỆP CHƯA CHỌN LOẠI CHỨNG TỪ LÀ TỜ KHAI XNK.", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void cbbSoHD_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
#if GC_V4
                HD = HopDong.Load(Convert.ToInt64(cbbSoHD.SelectedValue.ToString()));
                clcNgayHD.Value = HD.NgayDangKy;
                clcNgayHH.Value = HD.NgayHetHan;
#endif
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaKho_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                WareHouseRegisterManagementForm f = new WareHouseRegisterManagementForm();
                f.isBrowser = true;
                f.ShowDialog(this);
                if (f.kho != null)
                {
                    txtMaKho.Text = f.kho.MAKHO;
                    txtTenKho.Text = f.kho.TENKHO;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
