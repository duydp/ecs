﻿namespace Company.Interface
{
    partial class SendmsgVNACCFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblStatus = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grbRequest = new Janus.Windows.EditControls.UIGroupBox();
            this.ucRespone1 = new Company.KDT.SHARE.VNACCS.Controls.ucRespone();
            this.grbSend = new Janus.Windows.EditControls.UIGroupBox();
            this.pbRequest = new System.Windows.Forms.PictureBox();
            this.pbSend = new System.Windows.Forms.PictureBox();
            this.galleryControlGallery1 = new DevExpress.XtraBars.Ribbon.Gallery.GalleryControlGallery();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbRequest)).BeginInit();
            this.grbRequest.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbSend)).BeginInit();
            this.grbSend.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRequest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSend)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(530, 433);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.Location = new System.Drawing.Point(158, 70);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(35, 13);
            this.lblStatus.TabIndex = 1;
            this.lblStatus.Text = "label1";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.grbRequest);
            this.uiGroupBox1.Controls.Add(this.grbSend);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(530, 433);
            this.uiGroupBox1.TabIndex = 2;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // grbRequest
            // 
            this.grbRequest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grbRequest.Controls.Add(this.ucRespone1);
            this.grbRequest.Location = new System.Drawing.Point(6, 121);
            this.grbRequest.Name = "grbRequest";
            this.grbRequest.Size = new System.Drawing.Size(518, 309);
            this.grbRequest.TabIndex = 5;
            this.grbRequest.Text = "Thông báo từ hệ thống VNACCS";
            this.grbRequest.VisualStyleManager = this.vsmMain;
            // 
            // ucRespone1
            // 
            this.ucRespone1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucRespone1.InputMsgID = null;
            this.ucRespone1.isError = false;
            this.ucRespone1.Location = new System.Drawing.Point(3, 17);
            this.ucRespone1.MA_DON_VI = null;
            this.ucRespone1.Name = "ucRespone1";
            this.ucRespone1.PASS = null;
            this.ucRespone1.SERVER_NAME = null;
            this.ucRespone1.Size = new System.Drawing.Size(512, 289);
            this.ucRespone1.TabIndex = 3;
            this.ucRespone1.USER = null;
            // 
            // grbSend
            // 
            this.grbSend.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grbSend.Controls.Add(this.pbRequest);
            this.grbSend.Controls.Add(this.lblStatus);
            this.grbSend.Controls.Add(this.pbSend);
            this.grbSend.Location = new System.Drawing.Point(6, 11);
            this.grbSend.Name = "grbSend";
            this.grbSend.Size = new System.Drawing.Size(518, 104);
            this.grbSend.TabIndex = 4;
            this.grbSend.VisualStyleManager = this.vsmMain;
            // 
            // pbRequest
            // 
            this.pbRequest.Image = global::Company.Interface.Properties.Resources.SEND_FORM_2;
            this.pbRequest.Location = new System.Drawing.Point(98, 12);
            this.pbRequest.Name = "pbRequest";
            this.pbRequest.Size = new System.Drawing.Size(306, 55);
            this.pbRequest.TabIndex = 2;
            this.pbRequest.TabStop = false;
            // 
            // pbSend
            // 
            this.pbSend.Image = global::Company.Interface.Properties.Resources.SEND_FORM_1;
            this.pbSend.Location = new System.Drawing.Point(98, 12);
            this.pbSend.Name = "pbSend";
            this.pbSend.Size = new System.Drawing.Size(306, 55);
            this.pbSend.TabIndex = 2;
            this.pbSend.TabStop = false;
            // 
            // SendmsgVNACCFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(530, 433);
            this.ControlBox = false;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SendmsgVNACCFrm";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gửi và nhận thông điệp EDI";
            this.Load += new System.EventHandler(this.SendVNACCFrm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbRequest)).EndInit();
            this.grbRequest.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbSend)).EndInit();
            this.grbSend.ResumeLayout(false);
            this.grbSend.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbRequest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbSend)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblStatus;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.PictureBox pbSend;
        private DevExpress.XtraBars.Ribbon.Gallery.GalleryControlGallery galleryControlGallery1;
        private System.Windows.Forms.PictureBox pbRequest;
        private Company.KDT.SHARE.VNACCS.Controls.ucRespone ucRespone1;
        private Janus.Windows.EditControls.UIGroupBox grbRequest;
        private Janus.Windows.EditControls.UIGroupBox grbSend;



    }
}