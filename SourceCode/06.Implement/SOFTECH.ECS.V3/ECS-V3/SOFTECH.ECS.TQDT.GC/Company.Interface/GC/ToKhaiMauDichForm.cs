﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Drawing;

using Company.Interface.KDT.GC;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.Interface.Report;
using System.IO;
using Company.GC.BLL.KDT.SXXK;
using System.Net.Mail;
using System.Data;
using System.Threading;
using System.Collections.Generic;
using System.Diagnostics;
using System.Collections;
using Company.KDT.SHARE.Components;
namespace Company.Interface.GC
{
    public partial class ToKhaiMauDichForm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public Company.GC.BLL.KDT.GC.HopDong HDGC = new Company.GC.BLL.KDT.GC.HopDong();
        public string NhomLoaiHinh = string.Empty;
        private string xmlCurrent = "";
        public ToKhaiMauDichForm()
        {
            InitializeComponent();
        }
        private bool KiemTraLuongTon()
        {
            if (radSP.Checked)
            {
                DataSet dsLuongTon = NguyenPhuLieu.GetLuongNguyenPhuLieuTon(this.TKMD.IDHopDong);
                foreach (HangMauDich hangMD in this.TKMD.HMDCollection)
                {
                    if (hangMD.ID > 0)
                    {
                        DataTable dtLuongNPLTheoDM = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMD.MaPhu, hangMD.SoLuong, this.TKMD.IDHopDong).Tables[0];
                        foreach (DataRow row in dtLuongNPLTheoDM.Rows)
                        {
                            DataRow rowTon = dsLuongTon.Tables[0].Select("MaNguyenPhuLieu='" + row["MaNguyenPhuLieu"] + "'")[0];

                            rowTon["LuongTon"] = Convert.ToDecimal(rowTon["LuongTon"]) + Convert.ToDecimal(row["LuongCanDung"]);

                        }
                    }
                }
                TKMD.HMDCollection.Sort(
                                    delegate(HangMauDich hang1, HangMauDich hang2)
                                    {
                                        return hang1.MaPhu.CompareTo(hang2.MaPhu);
                                    }
                    );
                string TenHangBiAm = "";
                foreach (HangMauDich hangMD in this.TKMD.HMDCollection)
                {
                    DataSet dsLuongNPLTheoDM = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(hangMD.MaPhu, hangMD.SoLuong, this.TKMD.IDHopDong);
                    foreach (DataRow rowNPLTon in dsLuongTon.Tables[0].Rows)
                    {
                        bool ok = false;
                        foreach (DataRow rowNPLTheoDM in dsLuongNPLTheoDM.Tables[0].Rows)
                        {
                            if (rowNPLTon["MaNguyenPhuLieu"].ToString().Trim() == rowNPLTheoDM["MaNguyenPhuLieu"].ToString().Trim())
                            {
                                rowNPLTon["LuongTon"] = Convert.ToDecimal(rowNPLTon["LuongTon"]) - Convert.ToDecimal(rowNPLTheoDM["LuongCanDung"]);
                                if (Convert.ToDecimal(rowNPLTon["LuongTon"]) < 0)
                                {
                                    if (TenHangBiAm.IndexOf(hangMD.TenHang) < 0)
                                    {
                                        if (TenHangBiAm == "")
                                            TenHangBiAm += hangMD.TenHang;
                                        else
                                            TenHangBiAm += " ," + hangMD.TenHang;
                                    }
                                    ok = true;
                                    break;
                                }
                            }
                        }
                        if (ok)
                            break;
                    }
                }
                if (TenHangBiAm != "")
                {
                    if (showMsg("MSG_WRN01", TenHangBiAm, true) != "Yes")
                    //if (ShowMessage("Nguyên phụ liệu tồn trong hệ thống không đủ để sản xuất các hàng sau : "+TenHangBiAm+". Bạn có muốn lưu không?", true) != "Yes")
                    {
                        return false;
                    }
                    else
                        return true;
                }
            }
            return true;

        }
        private void loadTKMDData()
        {
            txtSoTiepNhan.Text = TKMD.SoTiepNhan + "";
            ccNgayDangKy.Text = TKMD.NgayDangKy.ToShortDateString();
            txtSoLuongPLTK.Value = this.TKMD.SoLuongPLTK;

            if (this.TKMD.NgayDangKy.Year > 1900)
                ccNgayDangKy.Text = this.TKMD.NgayDangKy.ToShortDateString();
            else
                ccNgayDangKy.Text = "";

            // Doanh nghiệp.
            txtMaDonVi.Text = this.TKMD.MaDoanhNghiep;
            txtTenDonVi.Text = this.TKMD.TenDoanhNghiep;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.Text = this.TKMD.TenDonViDoiTac;

            // Đại lý TTHQ.
            txtMaDaiLy.Text = this.TKMD.MaDaiLyTTHQ;
            txtTenDaiLy.Text = this.TKMD.TenDaiLyTTHQ;

            //Don vi uy thac
            txtMaDonViUyThac.Text = this.TKMD.MaDonViUT;
            txtTenDonViUyThac.Text = this.TKMD.MaDonViUT;

            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Ma = this.TKMD.MaLoaiHinh;

            // Phương tiện vận tải.
            cbPTVT.SelectedValue = this.TKMD.PTVT_ID;
            txtSoHieuPTVT.Text = this.TKMD.SoHieuPTVT;
            if (TKMD.NgayDenPTVT != null || TKMD.NgayDenPTVT.Year > 1900)
                ccNgayDen.Text = this.TKMD.NgayDenPTVT.ToShortDateString();
            else
                ccNgayDen.Text = "";
            // Nước.
            if (this.TKMD.MaLoaiHinh.Substring(0, 1) == "N")
                ctrNuocXuatKhau.Ma = this.TKMD.NuocXK_ID;
            else
                ctrNuocXuatKhau.Ma = this.TKMD.NuocNK_ID;

            // ĐKGH.
            cbDKGH.SelectedValue = this.TKMD.DKGH_ID;

            // PTTT.
            cbPTTT.SelectedValue = this.TKMD.PTTT_ID;

            // Giấy phép.
            txtSoGiayPhep.Text = this.TKMD.SoGiayPhep;
            if (this.TKMD.NgayGiayPhep.Year > 1900) ccNgayGiayPhep.Text = this.TKMD.NgayGiayPhep.ToShortDateString();
            else ccNgayGiayPhep.Text = "";
            if (this.TKMD.NgayHetHanGiayPhep.Year > 1900) ccNgayHHGiayPhep.Text = this.TKMD.NgayHetHanGiayPhep.ToShortDateString();
            else ccNgayHHGiayPhep.Text = "";
            //if (this.TKMD.SoGiayPhep.Length > 0) chkGiayPhep.Checked = true;

            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.Text = this.TKMD.SoHoaDonThuongMai;
            if (this.TKMD.NgayHoaDonThuongMai.Year > 1900) ccNgayHDTM.Text = this.TKMD.NgayHoaDonThuongMai.ToShortDateString();
            else ccNgayHDTM.Text = "";
            //if (this.TKMD.SoHoaDonThuongMai.Length > 0) chkHoaDonThuongMai.Checked = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.Ma = this.TKMD.CuaKhau_ID;

            // Nguyên tệ.
            ctrNguyenTe.Ma = this.TKMD.NguyenTe_ID;
            txtTyGiaTinhThue.Value = this.TKMD.TyGiaTinhThue;
            txtTyGiaUSD.Value = this.TKMD.TyGiaUSD;

            // Hợp đồng.
            txtSoHopDong.Text = this.TKMD.SoHopDong;
            if (this.TKMD.NgayHopDong.Year > 1900) ccNgayHopDong.Text = this.TKMD.NgayHopDong.ToShortDateString();
            else ccNgayHopDong.Text = "";
            if (this.TKMD.NgayHetHanHopDong.Year > 1900) ccNgayHHHopDong.Text = this.TKMD.NgayHetHanHopDong.ToShortDateString();
            else ccNgayHHHopDong.Text = "";
            //if (this.TKMD.SoHopDong.Length > 0) chkHopDong.Checked = true;

            // Vận tải đơn.
            txtSoVanTaiDon.Text = this.TKMD.SoVanDon;
            if (this.TKMD.NgayVanDon.Year > 1900) ccNgayVanTaiDon.Value = this.TKMD.NgayVanDon;
            else ccNgayVanTaiDon.Text = "";
            // if (this.TKMD.SoVanDon.Length > 0) chkVanTaiDon.Checked = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.Text = this.TKMD.DiaDiemXepHang;
            //if (this.TKMD.DiaDiemXepHang.Length > 0) chkDiaDiemXepHang.Checked = true;

            // Tên chủ hàng.
            txtTenChuHang.Text = this.TKMD.TenChuHang;

            // Container 20.
            txtSoContainer20.Value = this.TKMD.SoContainer20;

            // Container 40.
            txtSoContainer40.Value = this.TKMD.SoContainer40;

            // Số kiện hàng.
            txtSoKien.Value = this.TKMD.SoKien;

            // Trọng lượng.
            txtTrongLuong.Value = this.TKMD.TrongLuong;

            // Lệ phí HQ.
            txtLePhiHQ.Value = this.TKMD.LePhiHaiQuan;

            // Phí BH.
            txtPhiBaoHiem.Value = this.TKMD.PhiBaoHiem;

            // Phí VC.
            txtPhiVanChuyen.Value = this.TKMD.PhiVanChuyen;

            //Phí ngân hàng
            txtPhiNganHang.Value = this.TKMD.PhiKhac;

            txtTrongLuongTinh.Value = this.TKMD.TrongLuongNet;
            //MaMid
            txtMaMid.Text = this.TKMD.MaMid;

            if (this.TKMD.ID > 0)
            {
                if (this.TKMD.HMDCollection == null || this.TKMD.HMDCollection.Count == 0)
                    this.TKMD.LoadHMDCollection();
                if (this.TKMD.ChungTuTKCollection == null || this.TKMD.ChungTuTKCollection.Count == 0)
                    this.TKMD.LoadChungTuTKCollection();
                HDGC.ID = TKMD.IDHopDong;
                HDGC = Company.GC.BLL.KDT.GC.HopDong.Load(HDGC.ID);

            }
            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Refetch();
            gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            if (this.TKMD.LoaiHangHoa == "S")
            {
                radSP.Checked = true;
                radNPL.Checked = false;
                radTB.Checked = false;
            }
            else if (this.TKMD.LoaiHangHoa == "N")
            {
                radSP.Checked = false;
                radNPL.Checked = true;
                radTB.Checked = false;
            }
            else if (this.TKMD.LoaiHangHoa == "T")
            {
                radSP.Checked = false;
                radNPL.Checked = false;
                radTB.Checked = true;
            }
            this.radTB.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            this.radNPL.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            this.radSP.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);

            //chung tu kem
            txtChungTu.Text = this.TKMD.GiayTo;
            //so to khai 
            if (this.TKMD.SoToKhai > 0)
                txtSoToKhai.Text = this.TKMD.SoToKhai.ToString();

            if (this.TKMD.SoTiepNhan > 0)
                txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                lblTrangThai.Text = setText("Chờ duyệt", "Wai for approval");
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                lblTrangThai.Text = setText("Không được phê duyệt", "Not approved");
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                if (TKMD.PhanLuong == "")
                    lblTrangThai.Text = setText("Đã duyệt", "Approved");
                else
                {
                    //xanh
                    if (TKMD.PhanLuong == "1" || TKMD.PhanLuong == "3" || TKMD.PhanLuong == "5")
                        lblTrangThai.Text = "Tờ khai đã được phân luồng xanh";
                    else if (TKMD.PhanLuong == "2" || TKMD.PhanLuong == "4" || TKMD.PhanLuong == "6" || TKMD.PhanLuong == "10")
                        lblTrangThai.Text = "Tờ khai đã được phân luồng đỏ";
                    else if (TKMD.PhanLuong == "8" || TKMD.PhanLuong == "12")
                        lblTrangThai.Text = "Tờ khai đã được phân luồng vàng";
                }

            }
        }

        private void khoitao_DuLieuChuan()
        {
            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.Nhom = this.NhomLoaiHinh;
            ctrLoaiHinhMauDich.Ma = this.NhomLoaiHinh + "01";

            // Phương thức thanh toán.
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;

            // Điều kiện giao hàng.
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;

            // Phương tiện vận tải.
            cbPTVT.DataSource = PhuongThucVanTai.SelectAll();
            cbPTTT.DisplayMember = "Ten";
            cbPTTT.ValueMember = "ID";
            cbPTVT.SelectedValue = GlobalSettings.PTVT_MAC_DINH;

            if (this.NhomLoaiHinh.StartsWith("N"))
                ctrCuaKhau.Ma = GlobalSettings.DIA_DIEM_DO_HANG;
            else
                ctrCuaKhau.Ma = GlobalSettings.CUA_KHAU;
        }

        private void khoitao_GiaoDienToKhai()
        {
            this.NhomLoaiHinh = this.NhomLoaiHinh.Substring(0, 3);
            string loaiToKhai = this.NhomLoaiHinh.Substring(0, 1);
            //if (NhomLoaiHinh != "NSX" && NhomLoaiHinh != "XSX" && NhomLoaiHinh != "NGC" && NhomLoaiHinh != "XGC")             
            //grbHopDong.Enabled = chkHopDong.Enabled = true;
            //chkGiayPhep.Enabled = gbGiayPhep.Enabled = true;
            if (loaiToKhai == "N")
            {
                // Tiêu đề.
                dgList.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                this.Text = setText("Tờ khai nhập khẩu", "Import declaration") + "(" + this.NhomLoaiHinh + ")";
                uiGroupBox11.Visible = true;
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = true;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = true;
                //chkHoaDonThuongMai.Enabled = grbHoaDonThuongMai.Enabled = true;
                // chkVanTaiDon.Enabled = grbVanTaiDon.Enabled = true;
                // chkDiaDiemXepHang.Enabled = grbDiaDiemXepHang.Enabled = true;
                //grbDiaDiemXepHang.Visible = chkDiaDiemXepHang.Visible = true;                
                txtMaMid.Visible = lblMaMid.Visible = false;
                CanDoiNhapXuat.Visible = Janus.Windows.UI.InheritableBoolean.False;
                PhanBo.Visible = Janus.Windows.UI.InheritableBoolean.False;
            }
            else
            {
                // Tiêu đề.
                this.Text = setText("Tờ khai nhập khẩu", "Export declaration") + "(" + this.NhomLoaiHinh + ")";
                this.uiGroupBox1.BackColor = System.Drawing.Color.FromArgb(255, 228, 225);
                // Người XK / Người NK.
                grbNguoiNK.Text = setText("Người xuất khẩu", "Exporter");
                grbNguoiXK.Text = setText("Người nhập khẩu", "Importer");

                // Phương tiện vận tải.              
                //uiGroupBox11.Enabled = false;
                lblSoHieuPTVT.Visible = txtSoHieuPTVT.Visible = false;
                lblNgayDenPTVT.Visible = ccNgayDen.Visible = false;
                //chkHoaDonThuongMai.Visible =
                grbHoaDonThuongMai.Enabled = false;
                //chkVanTaiDon.Visible = 
                grbVanTaiDon.Enabled = false;

                // Vận tải đơn.


                // Nước XK.
                grbNuocXK.Text = setText("Nước nhập khẩu", "National Import");

                // Địa điểm xếp hàng / Địa điểm dỡ hàng.
                grbDiaDiemDoHang.Text = setText("Cửa khẩu xuất hàng", "Out-border gate");
                //chkDiaDiemXepHang.Enabled = 
                grbDiaDiemXepHang.Enabled = false;
                //grbDiaDiemXepHang.Visible = 
                // chkDiaDiemXepHang.Visible = false;
            }

            // Loại hình gia công.
            if (this.NhomLoaiHinh == "NGC" || this.NhomLoaiHinh == "XGC")
            {
                //chkHopDong.Checked = true;
                //chkHopDong.Visible = false;
                //txtSoHopDong.BackColor = ccNgayHopDong.BackColor = ccNgayHHHopDong.BackColor = Color.FromArgb(255, 255, 192);
                //txtSoHopDong.ButtonStyle = EditButtonStyle.Ellipsis;
                if (this.NhomLoaiHinh == "NGC")
                {
                    radNPL.Checked = true;
                    radSP.Visible = false;
                    radTB.Checked = false;
                }
            }

        }
        //-----------------------------------------------------------------------------------------

        private void ToKhaiNhapKhauSXXK_Form_Load(object sender, EventArgs e)
        {
            this.radTB.CheckedChanged -= new System.EventHandler(this.radSP_CheckedChanged);
            this.radNPL.CheckedChanged -= new System.EventHandler(this.radSP_CheckedChanged);
            this.radSP.CheckedChanged -= new System.EventHandler(this.radSP_CheckedChanged);
            // Người nhập khẩu / Người xuất khẩu.
            txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
            txtTenDonVi.Text = GlobalSettings.TEN_DON_VI;
            txtTenDonViDoiTac.Text = HDGC.DonViDoiTac + "\r\n" + HDGC.DiaChiDoiTac;
            txtTyGiaUSD.Value = GlobalSettings.TY_GIA_USD;
            ccNgayHHHopDong.Text = HDGC.NgayHetHan.ToShortDateString();
            ccNgayHopDong.Text = HDGC.NgayKy.ToShortDateString();
            txtSoHopDong.Text = HDGC.SoHopDong;
            this.TKMD.IDHopDong = HDGC.ID;
            this.TKMD.TrangThaiXuLy = 1;
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDienToKhai();
            switch (this.OpenType)
            {
                case OpenFormType.View:
                    this.loadTKMDData();
                    this.ViewTKMD();
                    break;
                case OpenFormType.Edit:
                    this.loadTKMDData();

                    break;
            }
            //this.NhomLoaiHinh = this.TKMD.MaLoaiHinh.Substring(0, 3);

            //setCommandStatus();
            this.radTB.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            this.radNPL.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            this.radSP.CheckedChanged += new System.EventHandler(this.radSP_CheckedChanged);
            //if (TKMD.ID > 0)
            //{
            //    MsgSend sendXML = new MsgSend();
            //    sendXML.LoaiHS = "TK";
            //    sendXML.master_id = TKMD.ID;
            //    if (sendXML.Load())
            //    {
            //        lblTrangThai.Text = "Đang chờ xác nhận hải quan";
            //        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //        XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //        Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //        cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    }
            //}
            //else
            //{                
            //    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //}
            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            cmdPrint.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            if (GlobalSettings.TuDongTinhThue == "0")
                cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;

        }

        private void ViewTKMD()
        {
            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            cmdTinhLaiThue.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            ChungTuKemTheo.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            txtSoLuongPLTK.ReadOnly = true;
            cmdReadExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            // Doanh nghiệp.
            txtMaDonVi.ReadOnly = true;
            txtTenDonVi.ReadOnly = true;

            // Đơn vị đối tác.
            txtTenDonViDoiTac.ReadOnly = true;
            ccNgayDangKy.ReadOnly = true;
            // Đại lý TTHQ.
            txtMaDaiLy.ReadOnly = true;
            txtTenDaiLy.ReadOnly = true;

            // Loại hình mậu dịch.
            ctrLoaiHinhMauDich.ReadOnly = true;

            // Phương tiện vận tải.
            cbPTVT.ReadOnly = true;
            txtSoHieuPTVT.ReadOnly = true;
            ccNgayDen.ReadOnly = true;

            // Nước.
            ctrNuocXuatKhau.ReadOnly = true;

            // ĐKGH.
            cbDKGH.ReadOnly = true;

            // PTTT.
            cbPTTT.ReadOnly = true;

            // Giấy phép.
            txtSoGiayPhep.ReadOnly = true;
            ccNgayGiayPhep.ReadOnly = true;
            ccNgayHHGiayPhep.ReadOnly = true;


            // Hóa đơn thương mại.
            txtSoHoaDonThuongMai.ReadOnly = true;
            ccNgayHDTM.ReadOnly = true;

            // Địa điểm dỡ hàng.
            ctrCuaKhau.ReadOnly = true;

            // Nguyên tệ.
            ctrNguyenTe.ReadOnly = true;
            txtTyGiaTinhThue.ReadOnly = true;
            txtTyGiaUSD.ReadOnly = true;

            // Hợp đồng.
            txtSoHopDong.ReadOnly = true;
            ccNgayHopDong.ReadOnly = true;
            ccNgayHHHopDong.ReadOnly = true;

            // Vận tải đơn.
            txtSoVanTaiDon.ReadOnly = true;
            ccNgayVanTaiDon.ReadOnly = true;

            // Địa điểm xếp hàng.
            txtDiaDiemXepHang.ReadOnly = true;

            // Tên chủ hàng.
            txtTenChuHang.ReadOnly = true;

            // Container 20.
            txtSoContainer20.ReadOnly = true;

            // Container 40.
            txtSoContainer40.ReadOnly = true;

            // Số kiện hàng.
            txtSoKien.ReadOnly = true;

            // Trọng lượng.
            txtTrongLuong.ReadOnly = true;

            // Lệ phí HQ.
            txtLePhiHQ.ReadOnly = true;

            // Phí BH.
            txtPhiBaoHiem.ReadOnly = true;

            // Phí VC.
            txtPhiVanChuyen.ReadOnly = true;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHoaDonThuongMai_CheckedChanged(object sender, EventArgs e)
        {
            //grbHoaDonThuongMai.Enabled = chkHoaDonThuongMai.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkGiayPhep_CheckedChanged(object sender, EventArgs e)
        {
            //gbGiayPhep.Enabled = chkGiayPhep.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkHopDong_CheckedChanged(object sender, EventArgs e)
        {
            //grbHopDong.Enabled = chkHopDong.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkVanTaiDon_CheckedChanged(object sender, EventArgs e)
        {
            // grbVanTaiDon.Enabled = chkVanTaiDon.Checked;
        }

        //-----------------------------------------------------------------------------------------
        private void chkDiaDiemXepHang_CheckedChanged(object sender, EventArgs e)
        {
            // grbDiaDiemXepHang.Enabled = chkDiaDiemXepHang.Checked;
        }
        //-----------------------------------------------------------------------------------------
        //-----------------------------------------------------------------------------------------
        private void ReadExcel()
        {
            bool valid = false;
            if (this.NhomLoaiHinh.Substring(0, 1) == "N")
            {
                cvError.ContainerToValidate = this;
                cvError.Validate();
                valid = cvError.IsValid;
            }
            else
            {
                cvError.ContainerToValidate = this.txtTenDonViDoiTac;
                cvError.Validate();
                bool valid1 = cvError.IsValid;
                cvError.ContainerToValidate = this.grbNguyenTe;
                cvError.Validate();
                bool valid2 = cvError.IsValid;
                valid = valid1 && valid2;
            }
            if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
            {
                epError.SetError(txtTyGiaTinhThue, setText("Tỷ giá tính thuế không hợp lệ.", "This value is invalid"));
                epError.SetIconPadding(txtTyGiaTinhThue, -8);
                return;

            }
            ReadExcelForm reform = new ReadExcelForm();
            reform.TiGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            reform.TKMD = this.TKMD;
            reform.ShowDialog();
            try
            {
                dgList.DataSource = this.TKMD.HMDCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        //-----------------------------------------------------------------------------------------

        private void themHang()
        {
            AddDataToToKhai();
            if (this.NhomLoaiHinh == "NGC" || this.NhomLoaiHinh == "XGC")
            {
                if (txtSoHopDong.Text == "")
                {
                    epError.SetError(txtSoHopDong, setText("Số hợp đồng không được rỗng.", "This VietNam must be filled"));
                    epError.SetIconPadding(txtSoHopDong, -8);
                    return;
                }
                else epError.Clear();
            }
            cvError.ContainerToValidate = grbNguyenTe;
            cvError.Validate();
            if (!cvError.IsValid) return;

            HangMauDichRegisterForm f = new HangMauDichRegisterForm();
            f.OpenType = OpenFormType.Edit;
            f.HD = this.HDGC;
            f.TKMD = this.TKMD;
            f.MaHaiQuan = HDGC.MaHaiQuan;
            f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
            this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
            f.ShowDialog();
            try
            {
                dgList.DataSource = TKMD.HMDCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void AddDataToToKhai()
        {
            bool valid = false;
            if (this.NhomLoaiHinh.Substring(0, 1) == "N")
            {
                cvError.ContainerToValidate = this;
                cvError.Validate();
                valid = cvError.IsValid;
            }
            else
            {
                cvError.ContainerToValidate = this.txtTenDonViDoiTac;
                cvError.Validate();
                bool valid1 = cvError.IsValid;
                cvError.ContainerToValidate = this.grbNguyenTe;
                cvError.Validate();
                bool valid2 = cvError.IsValid;
                valid = valid1 && valid2;
            }
            if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
            {
                epError.SetError(txtTyGiaTinhThue, setText("Tỷ giá tính thuế không hợp lệ.", "This value is invalid"));
                epError.SetIconPadding(txtTyGiaTinhThue, -8);
                return;

            }

            this.TKMD.SoTiepNhan = this.TKMD.SoTiepNhan;
            this.TKMD.MaHaiQuan = HDGC.MaHaiQuan;
            this.TKMD.SoHang = (short)this.TKMD.HMDCollection.Count;
            this.TKMD.SoLuongPLTK = Convert.ToInt16(txtSoLuongPLTK.Text);

            // Doanh nghiệp.
            this.TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            this.TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

            // Đơn vị đối tác.
            this.TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;


            this.TKMD.MaDonViUT = txtMaDonViUyThac.Text;
            this.TKMD.TenDonViUT = txtTenDonViUyThac.Text;

            // Đại lý TTHQ.
            this.TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
            this.TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

            // Loại hình mậu dịch.
            this.TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

            // Giấy phép.
            if (txtSoGiayPhep.Text.Trim().Length > 0)
            {
                this.TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                if (!ccNgayGiayPhep.IsNullDate)
                    this.TKMD.NgayGiayPhep = ccNgayGiayPhep.Value;
                else
                    this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                if (!ccNgayHHGiayPhep.IsNullDate)
                    this.TKMD.NgayHetHanGiayPhep = ccNgayHHGiayPhep.Value;
                else
                    this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
            }
            else
            {
                this.TKMD.SoGiayPhep = "";
                this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
            }

            // 7. Hợp đồng.
            if (txtSoHopDong.Text.Trim().Length > 0)
            {
                this.TKMD.SoHopDong = txtSoHopDong.Text;
                // Ngày HD.
                if (ccNgayHopDong.IsNullDate)
                    this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                else
                    this.TKMD.NgayHopDong = ccNgayHopDong.Value;
                // Ngày hết hạn HD.
                if (ccNgayHHHopDong.IsNullDate)
                    this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                else
                    this.TKMD.NgayHetHanHopDong = ccNgayHHHopDong.Value;
            }
            else
            {
                this.TKMD.SoHopDong = "";
                this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
            }

            // Hóa đơn thương mại.
            if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
            {
                this.TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                if (ccNgayHDTM.IsNullDate)
                    this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                else
                    this.TKMD.NgayHoaDonThuongMai = ccNgayHDTM.Value;
            }
            else
            {
                this.TKMD.SoHoaDonThuongMai = "";
                this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
            }
            // Phương tiện vận tải.
            this.TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
            this.TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
            if (ccNgayDen.IsNullDate)
                this.TKMD.NgayDenPTVT = DateTime.Parse("01/01/1900");
            else
                this.TKMD.NgayDenPTVT = ccNgayDen.Value;

            // Vận tải đơn.
            if (txtSoVanTaiDon.Text.Trim().Length > 0)
            {
                this.TKMD.SoVanDon = txtSoVanTaiDon.Text;
                if (ccNgayVanTaiDon.IsNullDate)
                    this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                else
                    this.TKMD.NgayVanDon = ccNgayVanTaiDon.Value;
            }
            else
            {
                this.TKMD.SoVanDon = "";
                this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
            }
            // Nước.
            if (this.NhomLoaiHinh.Substring(0, 1) == "N")
            {
                this.TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                this.TKMD.NuocNK_ID = "VN".PadRight(3);
            }
            else
            {
                this.TKMD.NuocXK_ID = "VN".PadRight(3);
                this.TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma;
            }

            // Địa điểm xếp hàng.
            //if (chkDiaDiemXepHang.Checked)
            this.TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
            // else
            //    this.TKMD.DiaDiemXepHang = "";

            // Địa điểm dỡ hàng.
            this.TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

            // Điều kiện giao hàng.
            this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

            // Đồng tiền thanh toán.
            this.TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
            this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            this.TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

            // Phương thức thanh toán.
            this.TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

            //
            this.TKMD.TenChuHang = txtTenChuHang.Text;
            this.TKMD.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
            this.TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
            this.TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
            this.TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

            // Tổng trị giá khai báo (Chưa tính).
            this.TKMD.TongTriGiaKhaiBao = 0;
            //
            this.TKMD.LoaiToKhaiGiaCong = "NPL";
            //
            this.TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
            this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
            this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
            this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
            this.TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

            //this.tkmd.NgayGui = DateTime.Parse("01/01/1900");
            if (radNPL.Checked) this.TKMD.LoaiHangHoa = "N";
            if (radSP.Checked) this.TKMD.LoaiHangHoa = "S";
            if (radTB.Checked) this.TKMD.LoaiHangHoa = "T";

            this.TKMD.GiayTo = txtChungTu.Text;
            this.TKMD.MaMid = txtMaMid.Text.Trim();
        }
        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin tờ khai.
        /// </summary>
        public void Save()
        {
            if (TKMD.MaLoaiHinh.StartsWith("N"))
            {
                if (PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap(TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.MaHaiQuan, (short)TKMD.NgayDangKy.Year, TKMD.IDHopDong))
                {
                    showMsg("MSG_ALL01");
                    //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                    return;
                }
            }
            else
            {
                if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                {
                    showMsg("MSG_ALL01");
                    //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                    return;
                }
            }
            bool valid = false;
            if (this.NhomLoaiHinh.Substring(0, 1) == "N")
            {
                cvError.ContainerToValidate = this;
                cvError.Validate();
                valid = cvError.IsValid;
            }
            else
            {
                cvError.ContainerToValidate = this.txtTenDonViDoiTac;
                cvError.Validate();
                bool valid1 = cvError.IsValid;
                cvError.ContainerToValidate = this.grbNguyenTe;
                cvError.Validate();
                bool valid2 = cvError.IsValid;
                valid = valid1 && valid2;
            }
            if (valid)
            {
                if (this.TKMD.HMDCollection.Count == 0)
                {
                    showMsg("MSG_SEN08");
                    //ShowMessage("Chưa nhập thông tin hàng của tờ khai", false);
                    return;
                }

                if (Convert.ToDecimal(txtTyGiaTinhThue.Value) == 0)
                {
                    epError.SetError(txtTyGiaTinhThue, setText("Tỷ giá tính thuế không hợp lệ.", "This value is invalid"));
                    epError.SetIconPadding(txtTyGiaTinhThue, -8);
                    return;

                }
                //if (!KiemTraLuongTon())
                //    return;
                HDGC =Company.GC.BLL.KDT.GC.HopDong.Load(HDGC.ID);
                this.TKMD.NgayTiepNhan = this.TKMD.NgayDangKy = ccNgayDangKy.Value;
                this.TKMD.SoToKhai = Convert.ToInt32(txtSoToKhai.Text);
                if (txtSoTiepNhan.Text.Trim() != "")
                    this.TKMD.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text);
                this.TKMD.MaHaiQuan = HDGC.MaHaiQuan;
                // this.TKMD.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;

                this.TKMD.SoHang = (short)this.TKMD.HMDCollection.Count;
                this.TKMD.SoLuongPLTK = Convert.ToInt16(txtSoLuongPLTK.Text);

                // Doanh nghiệp.
                this.TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                this.TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                // Đơn vị đối tác.
                this.TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;


                this.TKMD.MaDonViUT = txtMaDonViUyThac.Text;
                this.TKMD.TenDonViUT = txtTenDonViUyThac.Text;

                // Đại lý TTHQ.
                this.TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
                this.TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

                // Loại hình mậu dịch.
                this.TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

                // Giấy phép.
                if (txtSoGiayPhep.Text.Trim().Length > 0)
                {
                    this.TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                    if (!ccNgayGiayPhep.IsNullDate)
                        this.TKMD.NgayGiayPhep = ccNgayGiayPhep.Value;
                    else
                        this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                    if (!ccNgayHHGiayPhep.IsNullDate)
                        this.TKMD.NgayHetHanGiayPhep = ccNgayHHGiayPhep.Value;
                    else
                        this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                }
                else
                {
                    this.TKMD.SoGiayPhep = "";
                    this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                    this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                }

                // 7. Hợp đồng.
                if (txtSoHopDong.Text.Trim().Length > 0)
                {
                    this.TKMD.SoHopDong = txtSoHopDong.Text;
                    // Ngày HD.
                    if (ccNgayHopDong.IsNullDate)
                        this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHopDong = ccNgayHopDong.Value;
                    // Ngày hết hạn HD.
                    if (ccNgayHHHopDong.IsNullDate)
                        this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHetHanHopDong = ccNgayHHHopDong.Value;
                }
                else
                {
                    this.TKMD.SoHopDong = "";
                    this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                    this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                }

                // Hóa đơn thương mại.
                if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                {
                    this.TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                    if (ccNgayHDTM.IsNullDate)
                        this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayHoaDonThuongMai = ccNgayHDTM.Value;
                }
                else
                {
                    this.TKMD.SoHoaDonThuongMai = "";
                    this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                }
                // Phương tiện vận tải.
                this.TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
                this.TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                if (ccNgayDen.IsNullDate)
                    this.TKMD.NgayDenPTVT = DateTime.Parse("01/01/1900");
                else
                    this.TKMD.NgayDenPTVT = ccNgayDen.Value;

                // Vận tải đơn.
                if (txtSoVanTaiDon.Text.Trim().Length > 0)
                {
                    this.TKMD.SoVanDon = txtSoVanTaiDon.Text;
                    if (ccNgayVanTaiDon.IsNullDate)
                        this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                    else
                        this.TKMD.NgayVanDon = ccNgayVanTaiDon.Value;
                }
                else
                {
                    this.TKMD.SoVanDon = "";
                    this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                }
                // Nước.
                if (this.NhomLoaiHinh.Substring(0, 1) == "N")
                {
                    this.TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                    this.TKMD.NuocNK_ID = "VN".PadRight(3);
                }
                else
                {
                    this.TKMD.NuocXK_ID = "VN".PadRight(3);
                    this.TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma;
                }

                // Địa điểm xếp hàng.
                //if (chkDiaDiemXepHang.Checked)
                this.TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                // else
                //    this.TKMD.DiaDiemXepHang = "";

                // Địa điểm dỡ hàng.
                this.TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

                // Điều kiện giao hàng.
                this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

                // Đồng tiền thanh toán.
                this.TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
                this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                this.TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

                // Phương thức thanh toán.
                this.TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

                //
                this.TKMD.TenChuHang = txtTenChuHang.Text;
                this.TKMD.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                this.TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                this.TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
                this.TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

                // Tổng trị giá khai báo (Chưa tính).
                if (GlobalSettings.TuDongTinhThue == "1" && TKMD.MaLoaiHinh.Contains("N"))
                    this.tinhLaiThue();
                else
                {
                    tinhTongTriGiaKhaiBao();
                }
                //
                this.TKMD.TrangThaiXuLy = 1;
                this.TKMD.LoaiToKhaiGiaCong = "NPL";
                //
                this.TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
                this.TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

                //this.tkmd.NgayGui = DateTime.Parse("01/01/1900");
                if (radNPL.Checked) this.TKMD.LoaiHangHoa = "N";
                if (radSP.Checked) this.TKMD.LoaiHangHoa = "S";
                if (radTB.Checked) this.TKMD.LoaiHangHoa = "T";

                this.TKMD.GiayTo = txtChungTu.Text;

                this.TKMD.MaMid = txtMaMid.Text.Trim();

                long ret;
                try
                {
                    if (TKMD.MaLoaiHinh.Contains("N"))
                        NPLNhapTonThucTe.UpdateNguyenPhuLieuTonThucTeByToKhaiAndHMD(this.TKMD);
                    else
                        this.TKMD.InsertUpdateFull();
                    this.TKMD.LoadChungTuTKCollection();
                    this.TKMD.LoadHMDCollection();
                    dgList.DataSource = this.TKMD.HMDCollection;
                    dgList.Refetch();
                    gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
                    gridEX1.Refetch();
                    showMsg("MSG_SAV02");
                    //ShowMessage("Lưu tờ khai thành công.", false);
                }
                catch (Exception ex)
                {
                    showMsg("MSG_2702004", ex.Message);
                    //ShowMessage(ex.Message, false);
                }
            }
        }
        private void tinhTongTriGiaKhaiBao()
        {
            this.TKMD.TongTriGiaKhaiBao = 0;
            this.TKMD.TongTriGiaTinhThue = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                this.TKMD.TongTriGiaKhaiBao += hmd.TriGiaKB;
                this.TKMD.TongTriGiaTinhThue += hmd.TriGiaTT;
            }
        }
        private void tinhLaiThue()
        {
            this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            this.TKMD.TongTriGiaKhaiBao = 0;
            decimal Phi = Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
            decimal TongTriGiaHang = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                TongTriGiaHang += hmd.TriGiaKB;
                this.TKMD.TongTriGiaKhaiBao += Convert.ToDecimal(hmd.TriGiaKB);
            }
            decimal TriGiaTTMotDong = 0;
            if (TongTriGiaHang == 0) return;
            TriGiaTTMotDong = (1 + Phi / TongTriGiaHang) * Convert.ToDecimal(TKMD.TyGiaTinhThue);
            this.TKMD.TongTriGiaTinhThue = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                hmd.DonGiaTT = TriGiaTTMotDong * hmd.DonGiaKB;
                hmd.TriGiaTT = hmd.DonGiaTT * (hmd.SoLuong);
                this.TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);
                hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
                hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, MidpointRounding.AwayFromZero);
                hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB) * hmd.ThueSuatGTGT / 100, MidpointRounding.AwayFromZero);
                hmd.TriGiaThuKhac = Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, MidpointRounding.AwayFromZero);
            }
            dgList.DataSource = this.TKMD.HMDCollection;
            dgList.Refetch();
        }

        //-----------------------------------------------------------------------------------------

        private void inToKhai()
        {
            if (this.TKMD.HMDCollection.Count == 0) return;
            if (this.TKMD.ID == 0)
            {
                showMsg("MSG_PRI01");
                //ShowMessage("Bạn hãy lưu trước khi in.",false);
                return;
            }
            if (this.TKMD.TrangThaiXuLy != 1)
            {
                //if (ShowMessage("Tờ khai chưa được duyệt, bạn có muốn in hay không?", true) != "Yes") return;
                if (showMsg("MSG_PRI02", true) != "Yes") return;
            }
            switch (this.TKMD.MaLoaiHinh.Substring(0, 1))
            {
                case "N":
                    ReportViewTKNForm f = new ReportViewTKNForm();
                    f.TKMD = this.TKMD;
                    f.ShowDialog();
                    break;

                case "X":
                    ReportViewTKXForm f1 = new ReportViewTKXForm();
                    f1.TKMD = this.TKMD;
                    f1.ShowDialog();
                    break;
            }

        }
        private void setCommandStatus()
        {
            if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lblTrangThai.Text = setText("Không phê duyệt", "Not approved");
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
            }
            else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lblTrangThai.Text = setText("Chờ duyệt", "Wai for approval");
            }
        }
        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdTinhLaiThue":
                    this.tinhLaiThue();
                    showMsg("MSG_WRN36", this.TKMD.TongTriGiaKhaiBao);
                    //ShowMessage("Tổng trị giá khai báo là : " + this.TKMD.TongTriGiaKhaiBao, false);
                    break;
                case "cmdReadExcel":
                    this.ReadExcel();
                    break;
                case "cmdThemHang":
                    this.themHang();
                    break;
                case "ThemHang":
                    this.themHang();
                    break;
                case "cmdPrint":
                    this.inToKhai();
                    break;
                case "cmdSend":
                    this.Send();
                    break;
                case "ChungTuKemTheo":
                    this.AddChungTu();
                    break;
                case "NhanDuLieu":
                    this.NhanDuLieuToKhai();
                    break;
                case "XacNhan":
                    this.LaySoTiepNhanDT();
                    break;
                case "FileDinhKem":
                    this.GetFileDinhKem();
                    break;
                case "Huy":
                    this.HuyToKhai();
                    break;
                case "CanDoiNhapXuat":
                    this.ShowCanDoiNhapXuat();
                    break;
                case "XoaPhanBo":
                    this.XoaPhanBo();
                    break;
                case "PhanBo":
                    this.ThucHienPhanBoToKhai();
                    break;
                case "cmdXuatExcelHang":
                    this.XuatExcelHang();
                    break;
            }
        }
        private void XuatExcelHang()
        {
            SaveFileDialog sfNPL = new SaveFileDialog();
            sfNPL.RestoreDirectory = true;
            sfNPL.InitialDirectory = Application.StartupPath;
            sfNPL.Filter = "Excel files| *.xls";
            sfNPL.ShowDialog();
            if (sfNPL.FileName != "")
            {
                Stream str = sfNPL.OpenFile();
                gridEXExporterHangToKhai.Export(str);
                str.Close();
                if (showMsg("MSG_MAL08", true) == "Yes")
                //if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                {
                    Process.Start(sfNPL.FileName);
                }
            }
        }
        public void ThucHienPhanBoToKhai()
        {
            try
            {
                if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                {
                    showMsg("MSG_ALL03");
                    //ShowMessage("Tờ khai này đã được phân bổ.", false);
                    return;
                }
                if (this.TKMD.LoaiHangHoa == "S")
                {
                    PhanBoToKhaiXuat.PhanBoChoToKhaiXuatGC(this.TKMD, GlobalSettings.SoThapPhan.LuongNPL, GlobalSettings.SoThapPhan.DinhMuc);
                    showMsg("MSG_ALL04");
                    //ShowMessage("Thực hiện phân bổ thành công.", false);
                }
                else if (this.TKMD.LoaiHangHoa == "N")
                {
                    SelectNPLTaiXuatForm f = new SelectNPLTaiXuatForm();
                    f.TKMD = TKMD;
                    TKMD.PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
                    foreach (HangMauDich HMD in TKMD.HMDCollection)
                    {
                        PhanBoToKhaiXuat pbToKhaiTaiXuat = new PhanBoToKhaiXuat();
                        pbToKhaiTaiXuat.ID_TKMD = TKMD.ID;
                        pbToKhaiTaiXuat.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        pbToKhaiTaiXuat.MaSP = HMD.MaPhu;
                        pbToKhaiTaiXuat.SoLuongXuat = HMD.SoLuong;
                        TKMD.PhanBoToKhaiXuatCollection.Add(pbToKhaiTaiXuat);
                    }
                    f.pbToKhaiXuatCollection = TKMD.PhanBoToKhaiXuatCollection;
                    f.ShowDialog();
                    if (f.isPhanBo)
                        showMsg("MSG_ALL04");
                    //ShowMessage("Thực hiện phân bổ thành công.", false);
                }


            }
            catch (Exception ex)
            {
                showMsg("MSG_ALL05", ex.Message);
                //ShowMessage("Có lỗi khi phân bổ : " + ex.Message, false);
            }
        }
        private void XoaPhanBo()
        {
            //try
            //{
            //    ToKhaiMauDich TKMDXuat = new ToKhaiMauDich();
            //    TKMD.LoadHMDCollection();
            //    if (TKMD.MaLoaiHinh.StartsWith("X") && TKMD.TrangThai == 0)
            //    {
            //        TKMD.LoadPhanBo();
            //        if (ShowMessage("Bạn có muốn xóa phân bổ của tờ khai này không?", true) != "Yes")
            //            return;
            //    }
            //    else if (TKMD.MaLoaiHinh.StartsWith("X") && TKMD.TrangThai == 1)
            //    {
            //        ShowMessage("Tờ khai này chưa được phân bổ.", false);
            //        return;
            //    }
            //    else if (TKMD.MaLoaiHinh.StartsWith("X") && TKMD.TrangThai == 2)
            //    {
            //        ShowMessage("Tờ khai này không được phân bổ.", false);
            //        return;
            //    }
            //    else if (TKMD.MaLoaiHinh.StartsWith("N"))
            //    {
            //        TKMDXuat.SoToKhai = pbTKX.SoToKhaiXuat;
            //        TKMDXuat.NamDangKy = pbTKX.NamDangKyXuat;
            //        TKMDXuat.MaHaiQuan = pbTKX.MaHaiQuanXuat;
            //        TKMDXuat.MaLoaiHinh = pbTKX.MaLoaiHinhXuat;
            //        TKMDXuat.Load();
            //        TKMDXuat.LoadHMDCollection();
            //        TKMDXuat.LoadPhanBo();
            //        if (ShowMessage("Nếu chỉnh sửa tờ khai nhập này bạn sẽ phải phân bổ lại từ tờ khai xuất : " + TKMDXuat.SoToKhai + ". Bạn có muốn tiếp tục không?", true) != "Yes")
            //            return;
            //    }


            //    if (TKMD.MaLoaiHinh.StartsWith("X") && TKMD.TrangThai == 0)
            //        TKMD.DeletePhanBo();
            //    else if (TKMD.MaLoaiHinh.StartsWith("N"))
            //    {
            //        TKMDXuat.DeletePhanBo();
            //    }
            //    ShowMessage("Xóa phân bổ tờ khai thành công.", false);
            //}
            //catch (Exception ex)
            //{
            //    ShowMessage("Có lỗi khi xóa phân bổ : " + ex.Message, false);
            //}
            showMsg("MSG_WRN30");
            //ShowMessage("Chức năng này đang được xây dựng.", false);
        }
        private void ShowCanDoiNhapXuat()
        {
            //CanDoiNhapXuatForm f = new CanDoiNhapXuatForm();
            //f.TKMD = this.TKMD;
            //f.tableCanDoi = GetTableCanDoi();
            //if (f.tableCanDoi == null)
            //    return;
            //f.ShowDialog();
        }
        private void GetFileDinhKem()
        {
            SendMailChungTuForm f = new SendMailChungTuForm();
            f.TKMD = this.TKMD;
            f.ShowDialog();
        }
        private void HuyToKhai()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;
            string password = "";
            if (sendXML.Load())
            {
                //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                showMsg("MSG_WRN05");
                return;
            }
            try
            {
                int ttxl = TKMD.TrangThaiXuLy;
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = TKMD.WSCancelXMLGC(password);
                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK"; ;
                sendXML.master_id = TKMD.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 3;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void NhanDuLieuToKhai()
        {
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;
            string password = "";
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }
            try
            {
                int ttxl = TKMD.TrangThaiXuLy;
                string stPhanLuong = TKMD.PhanLuong;
                WSForm wsForm = new WSForm();
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.WaitCursor;
                xmlCurrent = TKMD.WSRequestXMLGC(password);
                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK"; ;
                sendXML.master_id = TKMD.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 2;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;

                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi nhận dữ liệu khai báo tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void AddChungTu()
        {
            ChungTuForm f = new ChungTuForm();
            f.OpenType = OpenFormType.Insert;
            f.collection = this.TKMD.ChungTuTKCollection;
            f.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;
            f.ShowDialog();
            this.TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            setCommandStatus();

        }

        private void Send()
        {
            bool ok = false;
            string password = "";
            this.Cursor = Cursors.Default;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                //ShowMessage("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", false);
                return;
            }

            WSForm wsForm = new WSForm();
            try
            {

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                {
                    if (TKMD.MaLoaiHinh.StartsWith("NGC"))
                    {
                        xmlCurrent = TKMD.WSSendXMLNHAP(password);
                    }
                    else if (TKMD.MaLoaiHinh.StartsWith("XGC"))
                    {
                        xmlCurrent = TKMD.WSSendXMLXuat(password, GlobalSettings.MaMID);
                    }
                }
                this.Cursor = Cursors.Default;
                sendXML = new MsgSend();
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.msg = xmlCurrent;
                sendXML.func = 1;
                xmlCurrent = "";
                sendXML.InsertUpdate();
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = InheritableBoolean.False;
                LayPhanHoi(password);
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.KHAI_BAO;
                            //    hd.PassWord = password;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                            showMsg("MSG_WRN13", ex.Message);
                        //ShowMessage("Xảy ra lỗi không xác định.", false);
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                        setCommandStatus();
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();

            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void LaySoTiepNhanDT()
        {
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            string password = "";
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                if (!sendXML.Load())
                {
                    showMsg("MSG_STN01");
                    //ShowMessage("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", false);
                    return;
                }

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }

                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = TKMD.LayPhanHoiGC(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    //string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                    string kq = showMsg("MSG_STN02", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(password);
                    }
                    else
                    {
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        lblTrangThai.Text = setText("Chờ xác nhận ", "Wai for comfirmation");
                    }
                    return;
                }
                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", TKMD.SoTiepNhan);
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + TKMD.SoTiepNhan, false);
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wai for approval");
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    dgList.AllowDelete = InheritableBoolean.False;
                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702024");
                    //ShowMessage("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
                    cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    dgList.AllowDelete = InheritableBoolean.True;
                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        showMsg("MSG_SEN11");
                        //this.ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKMD.SoToKhai.ToString(), false);
                        lblTrangThai.Text = setText("Đã duyệt", "Approved");
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        showMsg("MSG_SEN04");
                        //this.ShowMessage("Hải quan chưa duyệt danh sách này", false);
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //this.ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                        lblTrangThai.Text = setText("Không phê duyệt", "Not approved");
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        dgList.AllowDelete = InheritableBoolean.False;
                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void sendMailChungTu()
        {
            this.Cursor = Cursors.WaitCursor;
            if (GlobalSettings.MailDoanhNghiep == "" || GlobalSettings.MailHaiQuan == "")
            {
                if (GlobalSettings.MailDoanhNghiep == "")
                    showMsg("MSG_MAL01");
                //ShowMessage("Chưa cấu hình mail của doanh nghiệp", false);
                if (GlobalSettings.MailHaiQuan == "")
                    showMsg("MSG_MAL02");
                //ShowMessage("Chưa cấu hình mail của hải quan tiếp nhận", false);
                return;
            }
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(GlobalSettings.MailDoanhNghiep, GlobalSettings.TEN_DON_VI, System.Text.Encoding.UTF8);
            string[] mailArray = GlobalSettings.MailHaiQuan.Split(';');
            for (int k = 0; k < mailArray.Length; ++k)
                if (mailArray[k] != "")
                    mail.To.Add(mailArray[k]);
            mail.Subject = "Khai báo điện tử - " + GlobalSettings.MA_DON_VI + "- Tờ khai có số tiếp nhận : " + this.TKMD.SoTiepNhan.ToString();
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = "Hải quan điện tử - Duyệt tờ khai - Chứng từ ";
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;
            mail.Priority = MailPriority.High;

            int i = 0;
            foreach (ChungTu ct in TKMD.ChungTuTKCollection)
            {
                try
                {
                    if (ct.FileUpLoad != "")
                    {
                        //FileStream file = File.Open(ct.FileUpLoad, FileMode.Open);
                        Attachment att = new Attachment(ct.FileUpLoad);
                        mail.Attachments.Add(att);
                        mail.Body += "\n" + ct.TenChungTu + " ";
                        if (ct.SoBanChinh > 0)
                            mail.Body += "Số bản chính : " + ct.SoBanChinh;
                        if (ct.SoBanSao > 0)
                            mail.Body += "Số bản sao : " + ct.SoBanSao;
                        ++i;
                    }
                }
                catch
                {
                }
            }
            try
            {
                if (i > 0)
                {
                    this.Cursor = Cursors.WaitCursor;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Port = 587;
                    smtp.Host = "smtp.gmail.com";
                    smtp.EnableSsl = true;
                    smtp.Credentials = new System.Net.NetworkCredential("haiquandientu@gmail.com", "haiquandientudanang");
                    smtp.Send(mail);
                    //ShowMessage("Gửi mail thành công.", false);
                    this.Cursor = Cursors.Default;
                    this.Close();
                }
                this.Cursor = Cursors.WaitCursor;
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                showMsg("MSG_MAL06");
                //ShowMessage("Không gửi chứng từ tới hải quan được.", false);
            }
        }
        private void LayPhanHoi(string pass)
        {
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;

                xmlCurrent = TKMD.LayPhanHoiGC(pass, sendXML.msg);

                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  

                if (xmlCurrent != "")
                {
                    //string kq = ShowMessage("Chưa có phản hồi từ hải quan.\nBạn có muốn tiếp tục lấy xác nhận thông tin không?", true);
                    string kq = showMsg("MSG_STN02", true);
                    if (kq == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    else
                    {
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        ThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        ThemHang2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        lblTrangThai.Text = setText("Chờ xác nhận", "Wai for confirmation");
                    }
                    return;
                }
                if (sendXML.func == 1)
                {
                    sendMailChungTu();
                    showMsg("MSG_SEN02", TKMD.SoTiepNhan);
                    //ShowMessage("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + TKMD.SoTiepNhan, false);
                    txtSoTiepNhan.Text = this.TKMD.SoTiepNhan.ToString();
                    lblTrangThai.Text = setText("Chờ duyệt chính thức", "Wai for approval");


                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702024");
                    //ShowMessage("Hủy khai báo thành công", false);
                    txtSoTiepNhan.Text = "";
                    lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");

                }
                else if (sendXML.func == 2)
                {
                    if (TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        showMsg("MSG_SEN11", TKMD.SoToKhai);
                        //this.ShowMessage("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : "+TKMD.SoToKhai.ToString(), false);
                        lblTrangThai.Text = setText("Đã duyệt", "Approved");
                        txtSoToKhai.Text = TKMD.SoToKhai.ToString();
                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        showMsg("MSG_SEN04");
                        //this.ShowMessage("Hải quan chưa duyệt danh sách này", false);

                    }
                    else if (TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        showMsg("MSG_SEN05");
                        //this.ShowMessage("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.", false);
                        lblTrangThai.Text = setText("Không phê duyệt", "Not approved");

                    }
                }
                //xoa thông tin msg nay trong database
                sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKMD.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKMD.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định.", false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi khai báo danh sách Tờ khai. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        //-----------------------------------------------------------------------------------------

        //-----------------------------------------------------------------------------------------
        private void txtSoHopDong_ButtonClick(object sender, EventArgs e)
        {
            switch (this.NhomLoaiHinh)
            {
                case "NGC":
                    HopDongManageForm f = new HopDongManageForm();
                    f.IsBrowseForm = true;
                    f.IsDaDuyet = true;
                    f.ShowDialog();
                    if (f.HopDongSelected.ID > 0)
                    {
                        if (this.HDGC.ID != f.HopDongSelected.ID)
                        {
                            if (this.TKMD.HMDCollection.Count > 0)
                            {
                                //if (ShowMessage("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", true) == "Yes")
                                if (showMsg("MSG_SAV05", true) == "Yes")
                                {
                                    HangMauDich hang = new HangMauDich();
                                    HangMauDich.DeleteCollection((IList<HangMauDich>)TKMD.HMDCollection);
                                    TKMD.HMDCollection.Clear();
                                }
                                else
                                    return;
                            }
                            this.HDGC = f.HopDongSelected;
                            txtSoHopDong.Text = f.HopDongSelected.SoHopDong;
                            ccNgayHopDong.Value = f.HopDongSelected.NgayKy;
                            ccNgayHHHopDong.Value = f.HopDongSelected.NgayHetHan;
                            txtTenDonViDoiTac.Text = f.HopDongSelected.DonViDoiTac;
                            this.TKMD.IDHopDong = this.HDGC.ID;
                        }
                    }
                    break;
                case "XGC":
                    HopDongManageForm f1 = new HopDongManageForm();
                    f1.IsBrowseForm = true;
                    f1.IsDaDuyet = true;
                    f1.ShowDialog();
                    if (f1.HopDongSelected.SoHopDong != "")
                    {
                        if (this.HDGC.ID != f1.HopDongSelected.ID)
                        {
                            if (this.TKMD.HMDCollection.Count > 0)
                            {
                                //if (ShowMessage("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", true) == "Yes")
                                if (showMsg("MSG_SAV05", true) == "Yes")
                                {
                                    HangMauDich hang = new HangMauDich();
                                    HangMauDich.DeleteCollection((IList<HangMauDich>)TKMD.HMDCollection);
                                    TKMD.HMDCollection.Clear();
                                }
                                else
                                    return;
                            }
                            this.HDGC = f1.HopDongSelected;
                            txtSoHopDong.Text = f1.HopDongSelected.SoHopDong;
                            ccNgayHopDong.Value = f1.HopDongSelected.NgayKy;
                            ccNgayHHHopDong.Value = f1.HopDongSelected.NgayHetHan;
                            this.TKMD.IDHopDong = this.HDGC.ID;
                        }
                    }
                    break;
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangMauDichRegisterEditForm f = new HangMauDichRegisterEditForm();
                    f.HMD = this.TKMD.HMDCollection[i.Position];
                    this.HDGC.ID = this.TKMD.IDHopDong;
                    f.HD = this.HDGC;
                    f.TKMD = this.TKMD;
                    if (radNPL.Checked) this.TKMD.LoaiHangHoa = "N";
                    if (radSP.Checked) this.TKMD.LoaiHangHoa = "S";
                    if (radTB.Checked) this.TKMD.LoaiHangHoa = "T";
                    f.LoaiHangHoa = this.TKMD.LoaiHangHoa;
                    f.NhomLoaiHinh = this.NhomLoaiHinh;
                    f.MaHaiQuan = this.TKMD.MaHaiQuan;
                    f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
                    this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Value);
                    this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
                    this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Value);
                    f.ShowDialog();
                    try
                    {
                        dgList.Refetch();
                    }
                    catch { dgList.Refresh(); }
                }
                break;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text.ToString());
        }

        private void gridEX1_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChungTuForm f = new ChungTuForm();
            if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            ChungTu ctDetail = new ChungTu();
            ctDetail.ID = Convert.ToInt64(e.Row.Cells["ID"].Text);
            ctDetail.TenChungTu = e.Row.Cells["TenChungTu"].Text;
            ctDetail.SoBanChinh = Convert.ToInt16(e.Row.Cells["SoBanChinh"].Text);
            ctDetail.SoBanSao = Convert.ToInt16(e.Row.Cells["SoBanSao"].Text);
            ctDetail.STTHang = Convert.ToInt16(e.Row.Cells["STTHang"].Text);
            ctDetail.Master_ID = Convert.ToInt64(e.Row.Cells["Master_ID"].Text);
            ctDetail.FileUpLoad = e.Row.Cells["FileUpLoad"].Text;
            f.ctDetail = ctDetail;
            int i = 0;
            foreach (ChungTu ct in this.TKMD.ChungTuTKCollection)
            {
                if (ct.TenChungTu.ToUpper() == ctDetail.TenChungTu.ToUpper())
                {
                    this.TKMD.ChungTuTKCollection.RemoveAt(i);
                    break;
                }
                ++i;
            }
            f.MaLoaiHinh = this.TKMD.MaLoaiHinh;
            f.collection = this.TKMD.ChungTuTKCollection;
            f.ShowDialog();
            this.TKMD.ChungTuTKCollection = f.collection;
            gridEX1.DataSource = this.TKMD.ChungTuTKCollection;
            gridEX1.Refetch();
            setCommandStatus();
        }

        private void gridEX1_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (this.TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                showMsg("MSG_2702027");
                //ShowMessage("Tờ khai đã được duyệt không được sửa",false);
                e.Cancel = true;
                return;
            }
            ChungTu ctDetail = new ChungTu();
            ctDetail.ID = Convert.ToInt64(e.Row.Cells["ID"].Text);
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa không ?", true) == "Yes")
            {
                if (ctDetail.ID > 0)
                    ctDetail.Delete();
                this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            }
            else
                e.Cancel = true;
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (TKMD.MaLoaiHinh.StartsWith("N"))
            {
                if (PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap(TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.MaHaiQuan, (short)TKMD.NgayDangKy.Year, TKMD.IDHopDong))
                {
                    showMsg("MSG_ALL01");
                    //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                    e.Cancel = true;
                    return;
                }
            }
            else
            {
                if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                {
                    showMsg("MSG_ALL01");
                    //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                    e.Cancel = true;
                    return;
                }
            }
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;
                        if (hmd.ID > 0)
                        {
                            hmd.Delete(TKMD.LoaiHangHoa, TKMD.MaLoaiHinh, TKMD.IDHopDong);
                            if (!TKMD.MaLoaiHinh.EndsWith("X") && TKMD.LoaiHangHoa == "N")
                            {
                                Company.GC.BLL.GC.NPLNhapTonThucTe npl = Company.GC.BLL.GC.NPLNhapTonThucTe.Load(TKMD.SoToKhai, TKMD.MaLoaiHinh, (short)TKMD.NgayDangKy.Year, TKMD.MaHaiQuan, hmd.MaPhu);
                                npl.Delete();
                            }
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
            //setCommandStatus();
        }

        private void cmbToolBar_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            //test
            //ReadExcelForm reform = new ReadExcelForm();
            //reform.ShowDialog();
        }

        private void editBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void radSP_CheckedChanged(object sender, EventArgs e)
        {
            this.TKMD.HMDCollection.Clear();
            dgList.DataSource = this.TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void txtChungTu_ButtonClick(object sender, EventArgs e)
        {
            GiayToForm f = new GiayToForm();
            f.collection = this.TKMD.ChungTuTKCollection;
            if (TKMD.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO)
                f.OpenType = OpenFormType.View;
            else
                f.OpenType = OpenFormType.Edit;
            f.ShowDialog();
            this.TKMD.ChungTuTKCollection = f.collection;
            try
            {
                gridEX1.DataSource = TKMD.ChungTuTKCollection;
                gridEX1.Refetch();
            }
            catch
            {
                gridEX1.Refresh();
            }
            txtChungTu.Text = f.GiayTo;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void ToKhaiMauDichForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool ok = false;
            if (TKMD.MaLoaiHinh.StartsWith("N"))
            {
                //if (ToKhaiMauDichForm.isTKNhap)
                //    ok = true;
            }
            else
            {
                //if (ToKhaiMauDichForm.isTKXuat)
                //    ok = true;
            }
            ok = false;
            try
            {
                if (ok)
                {
                    if (showMsg("MSG_SAV03", true) == "Yes")
                    //if (ShowMessage("Dữ liệu có sự thay đổi. Bạn có muốn cập nhật lại không?", true) == "Yes")
                    {
                        this.TKMD.SoTiepNhan = this.TKMD.SoTiepNhan;
                        this.TKMD.MaHaiQuan = HDGC.MaHaiQuan;
                        this.TKMD.NgayTiepNhan = DateTime.Parse("01/01/1900");
                        this.TKMD.NgayDangKy = DateTime.Parse("01/01/1900");
                        this.TKMD.SoHang = (short)this.TKMD.HMDCollection.Count;
                        this.TKMD.SoLuongPLTK = Convert.ToInt16(txtSoLuongPLTK.Text);

                        // Doanh nghiệp.
                        this.TKMD.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        this.TKMD.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;

                        // Đơn vị đối tác.
                        this.TKMD.TenDonViDoiTac = txtTenDonViDoiTac.Text;


                        this.TKMD.MaDonViUT = txtMaDonViUyThac.Text;
                        this.TKMD.TenDonViUT = txtTenDonViUyThac.Text;

                        // Đại lý TTHQ.
                        this.TKMD.MaDaiLyTTHQ = txtMaDaiLy.Text;
                        this.TKMD.TenDaiLyTTHQ = txtTenDaiLy.Text;

                        // Loại hình mậu dịch.
                        this.TKMD.MaLoaiHinh = ctrLoaiHinhMauDich.Ma;

                        // Giấy phép.
                        if (txtSoGiayPhep.Text.Trim().Length > 0)
                        {
                            this.TKMD.SoGiayPhep = txtSoGiayPhep.Text;
                            if (!ccNgayGiayPhep.IsNullDate)
                                this.TKMD.NgayGiayPhep = ccNgayGiayPhep.Value;
                            else
                                this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                            if (!ccNgayHHGiayPhep.IsNullDate)
                                this.TKMD.NgayHetHanGiayPhep = ccNgayHHGiayPhep.Value;
                            else
                                this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                        }
                        else
                        {
                            this.TKMD.SoGiayPhep = "";
                            this.TKMD.NgayGiayPhep = DateTime.Parse("01/01/1900");
                            this.TKMD.NgayHetHanGiayPhep = DateTime.Parse("01/01/1900");
                        }

                        // 7. Hợp đồng.
                        if (txtSoHopDong.Text.Trim().Length > 0)
                        {
                            this.TKMD.SoHopDong = txtSoHopDong.Text;
                            // Ngày HD.
                            if (ccNgayHopDong.IsNullDate)
                                this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                            else
                                this.TKMD.NgayHopDong = ccNgayHopDong.Value;
                            // Ngày hết hạn HD.
                            if (ccNgayHHHopDong.IsNullDate)
                                this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                            else
                                this.TKMD.NgayHetHanHopDong = ccNgayHHHopDong.Value;
                        }
                        else
                        {
                            this.TKMD.SoHopDong = "";
                            this.TKMD.NgayHopDong = DateTime.Parse("01/01/1900");
                            this.TKMD.NgayHetHanHopDong = DateTime.Parse("01/01/1900");
                        }

                        // Hóa đơn thương mại.
                        if (txtSoHoaDonThuongMai.Text.Trim().Length > 0)
                        {
                            this.TKMD.SoHoaDonThuongMai = txtSoHoaDonThuongMai.Text;
                            if (ccNgayHDTM.IsNullDate)
                                this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                            else
                                this.TKMD.NgayHoaDonThuongMai = ccNgayHDTM.Value;
                        }
                        else
                        {
                            this.TKMD.SoHoaDonThuongMai = "";
                            this.TKMD.NgayHoaDonThuongMai = DateTime.Parse("01/01/1900");
                        }
                        // Phương tiện vận tải.
                        this.TKMD.PTVT_ID = cbPTVT.SelectedValue.ToString();
                        this.TKMD.SoHieuPTVT = txtSoHieuPTVT.Text;
                        if (ccNgayDen.IsNullDate)
                            this.TKMD.NgayDenPTVT = DateTime.Parse("01/01/1900");
                        else
                            this.TKMD.NgayDenPTVT = ccNgayDen.Value;

                        // Vận tải đơn.
                        if (txtSoVanTaiDon.Text.Trim().Length > 0)
                        {
                            this.TKMD.SoVanDon = txtSoVanTaiDon.Text;
                            if (ccNgayVanTaiDon.IsNullDate)
                                this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                            else
                                this.TKMD.NgayVanDon = ccNgayVanTaiDon.Value;
                        }
                        else
                        {
                            this.TKMD.SoVanDon = "";
                            this.TKMD.NgayVanDon = DateTime.Parse("01/01/1900");
                        }
                        // Nước.
                        if (this.NhomLoaiHinh.Substring(0, 1) == "N")
                        {
                            this.TKMD.NuocXK_ID = ctrNuocXuatKhau.Ma;
                            this.TKMD.NuocNK_ID = "VN".PadRight(3);
                        }
                        else
                        {
                            this.TKMD.NuocXK_ID = "VN".PadRight(3);
                            this.TKMD.NuocNK_ID = ctrNuocXuatKhau.Ma;
                        }

                        // Địa điểm xếp hàng.
                        //if (chkDiaDiemXepHang.Checked)
                        this.TKMD.DiaDiemXepHang = txtDiaDiemXepHang.Text;
                        // else
                        //    this.TKMD.DiaDiemXepHang = "";

                        // Địa điểm dỡ hàng.
                        this.TKMD.CuaKhau_ID = ctrCuaKhau.Ma;

                        // Điều kiện giao hàng.
                        this.TKMD.DKGH_ID = cbDKGH.SelectedValue.ToString();

                        // Đồng tiền thanh toán.
                        this.TKMD.NguyenTe_ID = ctrNguyenTe.Ma;
                        this.TKMD.TyGiaTinhThue = Convert.ToDecimal(txtTyGiaTinhThue.Text);
                        this.TKMD.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Text);

                        // Phương thức thanh toán.
                        this.TKMD.PTTT_ID = cbPTTT.SelectedValue.ToString();

                        //
                        this.TKMD.TenChuHang = txtTenChuHang.Text;
                        this.TKMD.SoContainer20 = Convert.ToDecimal(txtSoContainer20.Text);
                        this.TKMD.SoContainer40 = Convert.ToDecimal(txtSoContainer40.Text);
                        this.TKMD.SoKien = Convert.ToDecimal(txtSoKien.Text);
                        this.TKMD.TrongLuong = Convert.ToDecimal(txtTrongLuong.Text);

                        // Tổng trị giá khai báo (Chưa tính).

                        TKMD.TongTriGiaKhaiBao = 0;
                        TKMD.TongTriGiaTinhThue = 0;
                        foreach (HangMauDich hang in TKMD.HMDCollection)
                        {
                            TKMD.TongTriGiaKhaiBao += hang.TriGiaKB;
                            TKMD.TongTriGiaTinhThue += hang.TriGiaTT;
                        }
                        this.TKMD.TongTriGiaKhaiBao = 0;
                        //
                        this.TKMD.TrangThaiXuLy = -1;
                        this.TKMD.LoaiToKhaiGiaCong = "NPL";
                        //
                        this.TKMD.LePhiHaiQuan = Convert.ToDecimal(txtLePhiHQ.Text);
                        this.TKMD.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
                        this.TKMD.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
                        this.TKMD.PhiKhac = Convert.ToDecimal(txtPhiNganHang.Value);
                        this.TKMD.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);

                        //this.tkmd.NgayGui = DateTime.Parse("01/01/1900");
                        if (radNPL.Checked) this.TKMD.LoaiHangHoa = "N";
                        if (radSP.Checked) this.TKMD.LoaiHangHoa = "S";
                        if (radTB.Checked) this.TKMD.LoaiHangHoa = "T";

                        this.TKMD.GiayTo = txtChungTu.Text;

                        this.TKMD.MaMid = txtMaMid.Text.Trim();
                        TKMD.InsertUpdateFull();
                    }
                }
                //if (TKMD.MaLoaiHinh.StartsWith("N"))
                //    isTKNhap = false;
                //else
                //    isTKXuat = false;
            }
            catch (Exception ex)
            {
                //if (ShowMessage("Dữ liệu cập nhật có lỗi :" + ex.Message + ". Bạn có muốn thoát không?", true) != "Yes")
                if (showMsg("MSG_2702028", true) != "Yes")
                    e.Cancel = true;
                else
                {
                    //if (TKMD.MaLoaiHinh.StartsWith("N"))
                    //    isTKNhap = false;
                    //else
                    //    isTKXuat = false;
                }
            }
        }
    }
}
