﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServerRemote.Mapper
{
    class ItemSign
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public override string ToString()
        {
            return Name;
        }
    }
}
