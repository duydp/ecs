﻿namespace CA_QuanLyGCN
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup1 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem1 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem2 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem3 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem4 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            this.explorerBar1 = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.uiCommandManager1 = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdCauHinh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.cmdCauHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel0Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            this.uiPanel0Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // explorerBar1
            // 
            this.explorerBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem1.Key = "DM_NganhNghe";
            explorerBarItem1.Text = "Danh mục ngành nghề";
            explorerBarItem2.Key = "Nhap_DoanhNghiep";
            explorerBarItem2.Text = "Nhập mới giấy chứng nhận";
            explorerBarItem3.Key = "Nhap_QuyetDinhPhat";
            explorerBarItem3.Text = "Nhập quyết định phạt";
            explorerBarItem4.Key = "TraCuu";
            explorerBarItem4.Text = "Tra cứu thông tin";
            explorerBarGroup1.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem1,
            explorerBarItem2,
            explorerBarItem3,
            explorerBarItem4});
            explorerBarGroup1.Key = "Group1";
            explorerBarGroup1.Text = "Chức năng";
            this.explorerBar1.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup1});
            this.explorerBar1.Location = new System.Drawing.Point(0, 0);
            this.explorerBar1.Name = "explorerBar1";
            this.explorerBar1.Size = new System.Drawing.Size(194, 526);
            this.explorerBar1.TabIndex = 0;
            this.explorerBar1.VisualStyleManager = this.visualStyleManager1;
            this.explorerBar1.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBar1_ItemClick);
            // 
            // visualStyleManager1
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "Office2007";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme1.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme1);
            // 
            // uiCommandManager1
            // 
            this.uiCommandManager1.BottomRebar = this.BottomRebar1;
            this.uiCommandManager1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmdMain});
            this.uiCommandManager1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCauHinh});
            this.uiCommandManager1.ContainerControl = this;
            this.uiCommandManager1.Id = new System.Guid("2a390378-683f-4c2a-aecc-ca5ac8681e16");
            this.uiCommandManager1.LeftRebar = this.LeftRebar1;
            this.uiCommandManager1.RightRebar = this.RightRebar1;
            this.uiCommandManager1.TopRebar = this.TopRebar1;
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.uiCommandManager1;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 584);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(842, 0);
            // 
            // cmdMain
            // 
            this.cmdMain.CommandManager = this.uiCommandManager1;
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCauHinh1});
            this.cmdMain.Key = "CommandBar1";
            this.cmdMain.Location = new System.Drawing.Point(0, 0);
            this.cmdMain.Name = "cmdMain";
            this.cmdMain.RowIndex = 0;
            this.cmdMain.Size = new System.Drawing.Size(76, 28);
            this.cmdMain.Text = "CommandBar1";
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // cmdCauHinh1
            // 
            this.cmdCauHinh1.Key = "cmdCauHinh";
            this.cmdCauHinh1.Name = "cmdCauHinh1";
            // 
            // cmdCauHinh
            // 
            this.cmdCauHinh.Key = "cmdCauHinh";
            this.cmdCauHinh.Name = "cmdCauHinh";
            this.cmdCauHinh.Text = "Cấu hình";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.uiCommandManager1;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 556);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.uiCommandManager1;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(842, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 556);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmdMain});
            this.TopRebar1.CommandManager = this.uiCommandManager1;
            this.TopRebar1.Controls.Add(this.cmdMain);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(842, 28);
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.TabbedMdi = true;
            this.uiPanel0.Id = new System.Guid("2a879f8e-da1e-49d8-84ba-c6a67de8a026");
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("2a879f8e-da1e-49d8-84ba-c6a67de8a026"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 550), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("2a879f8e-da1e-49d8-84ba-c6a67de8a026"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.InnerContainer = this.uiPanel0Container;
            this.uiPanel0.Location = new System.Drawing.Point(3, 31);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(200, 550);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "QUẢN LÝ DOANH NGHIỆP";
            // 
            // uiPanel0Container
            // 
            this.uiPanel0Container.Controls.Add(this.explorerBar1);
            this.uiPanel0Container.Location = new System.Drawing.Point(1, 23);
            this.uiPanel0Container.Name = "uiPanel0Container";
            this.uiPanel0Container.Size = new System.Drawing.Size(194, 526);
            this.uiPanel0Container.TabIndex = 0;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(203, 31);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(636, 550);
            this.uiGroupBox1.TabIndex = 8;
            this.uiGroupBox1.Visible = false;
            this.uiGroupBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 584);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.uiPanel0);
            this.Controls.Add(this.LeftRebar1);
            this.Controls.Add(this.RightRebar1);
            this.Controls.Add(this.TopRebar1);
            this.Controls.Add(this.BottomRebar1);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MainForm";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            this.uiPanel0Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.ExplorerBar.ExplorerBar explorerBar1;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private Janus.Windows.UI.CommandBars.UICommandManager uiCommandManager1;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.Dock.UIPanel uiPanel0;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel0Container;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.Dock.UIPanelManager uiPanelManager1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.UI.CommandBars.UICommandBar cmdMain;
        private Janus.Windows.UI.CommandBars.UICommand cmdCauHinh1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCauHinh;
    }
}