﻿using System.Configuration;
using System.Windows.Forms;
using System;
using System.Xml;
using System.Collections.Generic;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public class GlobalSettings
    {
        //Dong bo du lieu
        public static bool SuDungSoTK;
        public static int SoTK;
        public static bool DongBoChungTuDinhKem;

        public static string DATETIME_FORMAT_VN = "dd/MM/yyyy";
        public static string USERNAME_DONGBO;
        public static string PASSWOR_DONGBO;
        public static int SOTOKHAI_DONGBO;
        public static bool ISKHAIBAO = true;

        /// <summary>
        /// KhanhHN - 21/06/2012
        /// Cấu hình tính thuế dựa trên Trị giá / Đơn giá
        /// </summary>
        public static string TinhThueTGNT;
        /// <summary>
        /// In định mức theo TT 196 - Khanhhn 20/05/2013
        /// </summary>
        public static bool InDinhMucTT196;
        /// <summary>
        /// Nội dung tiêu đề số hồ sơ thanh khoản. Hồ sơ thuộc loại xin hoàn thuế hay không thu thuế.
        /// </summary>
        public static string NoiDungSoHoSoThanhKhoan = string.Empty;

        public static bool ThanhKhoanNhieuChiCuc;
        public static bool inNKvaND;
        public static bool SuDungChuKySo;
        public static string DiaChiWS;
        public static string DiaChiWS_Host;
        public static string DiaChiWS_Name;
        public static string ActiveStatus;
        public static int HanThanhKhoan;
        public static int ThongBaoThanhKhoan;
        public static string UserLog;
        public static string PassLog;
        public static int ChayToKhaiNKD;
        public static int TKX;
        public static int AmTKTiep;
        public static int ThongBaoHetHan;
        public static decimal SoTienKhoanTKN;
        public static decimal SoTienKhoanTKX;
        public static bool Import;
        public static string NGON_NGU;
        public static string NGAYSAOLUU;
        public static string NHAC_NHO_SAO_LUU;
        public static string LastBackup;
        public static string PathBackup;
        public static bool NgayHeThong;
        public static string DiaPhuong;
        public static string TieudeNgay;
        public static string CHO_TIEP_NHAN_COLOR;
        public static string CHUA_DUYET_COLOR;
        public static string CHUA_KHAI_BAO_COLOR;
        public static string CUA_KHAU;
        public static string LOAI_HINH;
        public static string NHOM_LOAI_HINH;
        public static string NHOM_LOAI_HINH_KHAC_XUAT;
        public static string LOAI_HINH_KHAC_XUAT;
        public static string NHOM_LOAI_HINH_KHAC_NHAP;
        public static string LOAI_HINH_KHAC_NHAP;
        public static string DA_DUYET_COLOR;
        public static bool DAI_LY_TTHQ;
        public static string DKGH_MAC_DINH;
        public static string DVT_MAC_DINH;
        public static string MA_DAI_LY;
        public static string TEN_DAI_LY;
        public static string NUOC;
        public static string NUOCNK;
        public static string SoDienThoaiDN;
        public static string SoFaxDN;
        public static string NGUYEN_TE_MAC_DINH;
        public static string PTTT_MAC_DINH;
        public static string PTVT_MAC_DINH;
        public static string TEN_DOI_TAC;
        public static decimal TY_GIA_USD;
        public static string CHENHLECH_THN_THX;
        public static string LoaiWS;
        public static string DATABASE_NAME;
        public static string USER;
        public static string PASS;
        public static string SERVER_NAME;
        public static string MA_CUC_HAI_QUAN;
        public static string TEN_CUC_HAI_QUAN;
        public static string MA_DON_VI;
        public static string DIA_CHI;
        public static string MA_HAI_QUAN;
        public static string MA_HAI_QUAN_VNACCS;
        public static bool M07DMDK;
        public static string TEN_HAI_QUAN;
        public static string TEN_HAI_QUAN_NGAN;
        public static string TEN_DON_VI;

        public static string PassWordDT = "";
        public static string UserId = "";
        public static bool IsRemember = false;

        public static string MailDoanhNghiep;
        public static string DienThoai;
        public static string Fax;
        public static string NguoiLienHe;
        public static string ChucVu;
        public static string MailHaiQuan;
        public static string MaMID;
        public static int MaHTS;
        public static string DIA_DIEM_DO_HANG;
        public static string CUA_KHAU_XUAT_HANG;
        public static string TieuDeInDinhMuc;
        public static string TuDongTinhThue;
        public static System.Drawing.Printing.Margins MarginTKN;
        public static System.Drawing.Printing.Margins MarginTKX;
        public static System.Drawing.Printing.Margins MarginPhuLuc;
        public static System.Drawing.Printing.Margins MarginTKCT;
        public static long FileSize;
        public static string MienThueGTGT;
        public static List<string> ListTableNameSource = new List<string>();
        public static int ToKhaiKoTK;
        public static string HienThiHD;
        public static int ChiPhiKhac;
        public static float FontDongHang;
        public static float FontToKhai;

        //minhnd fontsize prop 06/10/2014
        public static string fontsize { get; set; }

        public static int TKhoanKCX;
        public static float FontBCXNT;
        public static bool IsUseProxy;
        public static string Host;
        public static string Port;
        public static int TriGiaNT = 4;
        public static int DonGiaNT = 6;
        public static int TongTriGiaNT = 2;
        public static int SoThapPhan_BaoCaoThanhKhoan = 0;
        public static bool IsDaiLy { get; set; }
        public static bool IsOnlyMe = false;
        public static bool iSignRemote;
        public static string userNameSign;
        public static string passwordSign;
        //
        public static bool WordWrap;
        public static bool SendV4 = false;
        public static bool IsKhongDungBangKeCont;
        public static string LoaihinhMD_Nhap;
        public static string LoaihinhMD_Xuat;
        public static bool IsDelete;
        public static bool IsSignOnLan;
        public static string DataSignLan;

        //minhnd 29/09/2015 kiểm tra chữ ký số
        public static double CheckChuKySo()
        {
            TimeSpan ts = new TimeSpan();
            List<System.Security.Cryptography.X509Certificates.X509Certificate2> items = Cryptography.GetX509CertificatedNames();
            foreach (System.Security.Cryptography.X509Certificates.X509Certificate2 item in items)
            {
                string name = item.GetName();
                if (name.Contains(GlobalSettings.MA_DON_VI))
                {
                    DateTime date = DateTime.Parse(item.GetExpirationDateString());
                    ts = date - DateTime.Now;
                    return Math.Round(ts.TotalDays);
                    //if(ts.TotalDays<=30)
                    //    MessageBox.Show("Chữ ký số có MST:" + GlobalSettings.MA_DON_VI+" còn "+Math.Round(ts.TotalDays)+" nữa hết hạn sử dụng.", "Expiration Date Certificate", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            return -1;
        }
        public static void KhoiTao_GiaTriMacDinh()
        {
            try
            {
                string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
                ApplicationSettings settings = new ApplicationSettings();
                settings.ReadXml(settingFileName);

                NguyenPhuLieu.Ma = settings.NguyenPhuLieu[0].Ma;
                NguyenPhuLieu.Ten = settings.NguyenPhuLieu[0].Ten;
                NguyenPhuLieu.MaHS = settings.NguyenPhuLieu[0].MaHS;
                NguyenPhuLieu.DVT = settings.NguyenPhuLieu[0].DVT;

                SanPham.Ma = settings.SanPham[0].Ma;
                SanPham.Ten = settings.SanPham[0].Ten;
                SanPham.MaHS = settings.SanPham[0].MaHS;
                SanPham.DVT = settings.SanPham[0].DVT;

                DinhMuc.MaSP = settings.DinhMuc[0].MaSP;
                DinhMuc.MaNPL = settings.DinhMuc[0].MaNPL;
                DinhMuc.DinhMucSuDung = settings.DinhMuc[0].DinhMucSuDung;
                DinhMuc.DinhMucChung = settings.DinhMuc[0].DinhMucChung;
                DinhMuc.TyLeHH = settings.DinhMuc[0].TyLeHH;

                ThongTinDinhMuc.MaSP = settings.ThongTinDinhMuc[0].MaSP;
                ThongTinDinhMuc.SoDinhMuc = settings.ThongTinDinhMuc[0].SoDinhMuc;
                ThongTinDinhMuc.NgayDangKy = settings.ThongTinDinhMuc[0].NgayDangKy;
                ThongTinDinhMuc.NgayApDung = settings.ThongTinDinhMuc[0].NgayApDung;

                GiaoDien.Id = settings.GiaoDien[0].Id;

                SoThapPhan.DinhMuc = Convert.ToInt32(settings.SoThapPhan[0].DinhMuc);
                SoThapPhan.LuongNPL = Convert.ToInt32(settings.SoThapPhan[0].LuongNPL);
                SoThapPhan.LuongSP = Convert.ToInt32(settings.SoThapPhan[0].LuongSP);
                SoThapPhan.SapXepTheoTK = Convert.ToInt32(settings.SoThapPhan[0].SapXepTheoTK);
                SoThapPhan.NPLKoTK = Convert.ToInt32(settings.SoThapPhan[0].NPLKoTK);
                SoThapPhan.MauBC01 = Convert.ToInt32(settings.SoThapPhan[0].MauBC1);
                SoThapPhan.TachLam2 = Convert.ToInt32(settings.SoThapPhan[0].TachLam2);
                SoThapPhan.MauBC04 = Convert.ToInt32(settings.SoThapPhan[0].MauBC4);
                SoThapPhan.TLHH = Convert.ToInt32(settings.SoThapPhan[0].TLHH);
                InToKhai.SoTNDKDT = Convert.ToInt32(settings.InToKhai[0].SoTNDT);
                InToKhai.TriGiTT = Convert.ToInt32(settings.InToKhai[0].TriGiaTT);
                InToKhai.LoaiHinh = Convert.ToInt32(settings.InToKhai[0].MaLoaiHinh);
                InToKhai.DVTCon = Convert.ToInt32(settings.InToKhai[0].DVTCon);
            }
            catch (Exception ex)
            {
                try
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                catch { }
            }
        }
        public static void Luu_NPL(string ma, string ten, string maHS, string dvt)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.NguyenPhuLieu[0].Ma = ma;
            settings.NguyenPhuLieu[0].Ten = ten;
            settings.NguyenPhuLieu[0].MaHS = maHS;
            settings.NguyenPhuLieu[0].DVT = dvt;
            settings.WriteXml(settingFileName);
        }
        public static void Luu_SP(string ma, string ten, string maHS, string dvt)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.SanPham[0].Ma = ma;
            settings.SanPham[0].Ten = ten;
            settings.SanPham[0].MaHS = maHS;
            settings.SanPham[0].DVT = dvt;
            settings.WriteXml(settingFileName);
        }

        public static void Luu_DM(string maSP, string maNPL, string DMSD, string TLHH, string DMC)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.DinhMuc[0].MaSP = maSP;
            settings.DinhMuc[0].MaNPL = maNPL;
            settings.DinhMuc[0].DinhMucSuDung = DMSD;
            settings.DinhMuc[0].DinhMucChung = DMC;
            settings.DinhMuc[0].TyLeHH = TLHH;
            settings.WriteXml(settingFileName);
        }

        public static void Luu_TTDM(string maSP, string soDinhMuc, string ngayDangKy, string ngayApDung)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.ThongTinDinhMuc[0].MaSP = maSP;
            settings.ThongTinDinhMuc[0].SoDinhMuc = soDinhMuc;
            settings.ThongTinDinhMuc[0].NgayDangKy = ngayDangKy;
            settings.ThongTinDinhMuc[0].NgayApDung = ngayApDung;
            settings.WriteXml(settingFileName);
        }
        public static void Luu_GiaoDien(string id)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.GiaoDien[0].Id = id;
            settings.WriteXml(settingFileName);
        }
        public static void Luu_SoThapPhan(int dinhMuc, int luongNPL, int luongSP, int sapXepTheoTK, int nplKoTK, int mauBC01, int tachLam2, int mauBC4, int tyLeHH)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.SoThapPhan[0].DinhMuc = dinhMuc + "";
            settings.SoThapPhan[0].LuongSP = luongSP + "";
            settings.SoThapPhan[0].LuongNPL = luongNPL + "";
            settings.SoThapPhan[0].TLHH = tyLeHH + "";
            settings.SoThapPhan[0].SapXepTheoTK = sapXepTheoTK + "";
            settings.SoThapPhan[0].NPLKoTK = nplKoTK + "";
            settings.SoThapPhan[0].MauBC1 = mauBC01 + "";
            settings.SoThapPhan[0].TachLam2 = tachLam2 + "";
            settings.SoThapPhan[0].MauBC4 = mauBC4 + "";
            //settings.SoThapPhan[0].TLHH = TLHH + "";
            settings.WriteXml(settingFileName);
        }
        public static void Luu_InToKhai(int sotn, int trigiatt, int loaihinh, int condvt)
        {
            string settingFileName = Application.StartupPath + "\\ApplicationSettings.xml";
            ApplicationSettings settings = new ApplicationSettings();
            settings.ReadXml(settingFileName);
            settings.InToKhai[0].SoTNDT = sotn + "";
            settings.InToKhai[0].TriGiaTT = trigiatt + "";
            settings.InToKhai[0].MaLoaiHinh = loaihinh + "";
            settings.InToKhai[0].DVTCon = condvt + "";
            settings.WriteXml(settingFileName);
        }

        #region Nested type: NguyenPhuLieu

        public struct NguyenPhuLieu
        {
            public static string Ma;
            public static string Ten;
            public static string MaHS;
            public static string DVT;
        }

        #endregion

        #region Nested type: SanPham

        public struct SanPham
        {
            public static string Ma;
            public static string Ten;
            public static string MaHS;
            public static string DVT;
        }

        #endregion

        #region Nested type: DinhMuc

        public struct DinhMuc
        {
            public static string MaSP;
            public static string MaNPL;
            public static string DinhMucSuDung;
            public static string TyLeHH;
            public static string DinhMucChung;
            public static int TLHH;
        }

        #endregion

        #region Nested type: ThongTinDinhMuc

        public struct ThongTinDinhMuc
        {
            public static string MaSP;
            public static string SoDinhMuc;
            public static string NgayDangKy;
            public static string NgayApDung;
        }

        #endregion

        #region Nested type: GiaoDien

        public struct GiaoDien
        {
            public static string Id;
        }

        #endregion

        #region Nested type: SoThapPhan

        public struct SoThapPhan
        {
            public static int DinhMuc = 5;
            public static int LuongNPL = 5;
            public static int LuongSP = 0;
            public static int SapXepTheoTK = 1;
            public static int NPLKoTK = 0;
            public static int MauBC01 = 1;
            public static int TachLam2 = 1;
            public static int MauBC04 = 1;
            public static int TLHH;
        }
        public struct InToKhai
        {
            public static int SoTNDKDT;
            public static int TriGiTT;
            public static int LoaiHinh;
            public static int DVTCon;

        }

        #endregion

        public static void RefreshKey()
        {
            string tmpMaDoanhNghiep = MA_DON_VI;

            try
            {
                ActiveStatus = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ActiveStatus");
                HanThanhKhoan = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("HanThanhKhoan"));
                ThongBaoThanhKhoan = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ThongBaoThanhKhoan"));
                UserLog = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("UserLog");
                PassLog = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PassLog");
                MA_DAI_LY = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MA_DAI_LY");
                ChayToKhaiNKD = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ChayToKhaiNKD"));
                TKX = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TKX"));
                ToKhaiKoTK = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ToKhaiKoTK"));
                HienThiHD = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("VisibleHD");
                AmTKTiep = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("AmTKTiep"));
                ThongBaoHetHan = Convert.ToInt32(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ThongBaoHetHan"));
                SoTienKhoanTKN = decimal.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTienKhoanTKN"));
                SoTienKhoanTKX = decimal.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTienKhoanTKX"));
                TieudeNgay = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TieudeNgay");
                Import = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Import"));
                NGON_NGU = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ngonngu");
                NGAYSAOLUU = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NgaySaoLuu");
                NHAC_NHO_SAO_LUU = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHAC_NHO_SAO_LUU");
                LastBackup = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LastBackup");
                PathBackup = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PathBackup");
                NgayHeThong = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NgayHeThong"));
                DiaPhuong = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DiaPhuong");
                CUA_KHAU = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("CUA_KHAU");
                LOAI_HINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LOAI_HINH");
                NHOM_LOAI_HINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHOM_LOAI_HINH");
                //NHOM_LOAI_HINH_KHAC_XUAT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHOM_LOAI_HINH_KHAC_XUAT");
                //LOAI_HINH_KHAC_XUAT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LOAI_HINH_KHAC_XUAT");
                NHOM_LOAI_HINH_KHAC_NHAP = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHOM_LOAI_HINH_KHAC_NHAP");
                //LOAI_HINH_KHAC_NHAP = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LOAI_HINH_KHAC_NHAP");
                DKGH_MAC_DINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DKGH_MAC_DINH");
                DVT_MAC_DINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DVT_MAC_DINH");
                MA_DAI_LY = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MA_DAI_LY");
                NUOC = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NUOC");
                NUOCNK = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NUOCNK");
                NGUYEN_TE_MAC_DINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NGUYEN_TE_MAC_DINH");
                PTTT_MAC_DINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PTTT_MAC_DINH");
                PTVT_MAC_DINH = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PTVT_MAC_DINH");
                TEN_DOI_TAC = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DOI_TAC");
                TY_GIA_USD = decimal.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TY_GIA_USD"));
                CHENHLECH_THN_THX = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("CL_THN_THX");
                LoaiWS = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LoaiWS");
                DiaChiWS = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DiaChiWS");
                DATABASE_NAME = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DATABASE_NAME");
                USER = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("user");
                PASS = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("pass");
                SERVER_NAME = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ServerName");
                //MA_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MA_CUC_HAI_QUAN");
                //TEN_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_CUC_HAI_QUAN");
                //MA_DON_VI = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MA_DON_VI");
                //DIA_CHI = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DIA_CHI");
                //MA_HAI_QUAN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MA_HAI_QUAN");
                //TEN_HAI_QUAN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_HAI_QUAN");
                //TEN_HAI_QUAN_NGAN = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_HAI_QUAN_NGAN");
                //TEN_DON_VI = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI");
                //PassWordDT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PassWordDT");
                //MailDoanhNghiep = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MailDoanhNghiep");
                //MailHaiQuan = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MailHaiQuan");
                //MaMID = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MaMID");
                MaHTS = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MA_HTS"));
                DIA_DIEM_DO_HANG = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DIA_DIEM_DO_HANG");
                CUA_KHAU_XUAT_HANG = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("CUA_KHAU_XUAT_HANG");
                TieuDeInDinhMuc = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TieuDeInDinhMuc");
                TuDongTinhThue = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TuDongTinhThue");
                //KhanhHN - 21/06/2012 - lưu cấu hình tình thuế theo trị giá/ đơn giá
                TinhThueTGNT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TinhThueTGNT");

                MarginTKN = Company.KDT.SHARE.Components.Globals.GetMargin(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MarginTKN"));
                MarginTKX = Company.KDT.SHARE.Components.Globals.GetMargin(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MarginTKX"));
                MarginPhuLuc = Company.KDT.SHARE.Components.Globals.GetMargin(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MarginPhuLuc"));
                MarginTKCT = Company.KDT.SHARE.Components.Globals.GetMargin(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MarginTKCT"));
                ChiPhiKhac = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ChiPhiKhac"));
                FontDongHang = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang"));
                TKhoanKCX = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TKhoanKCX"));
                FontBCXNT = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontBCXNT"));
                //phiph 25/10/2012
                FontToKhai = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontToKhai"));

                //Minhnd get fontsize 06/10/2014
                //fontsize = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontHuongDan", "10");

                /*DATLMQ update Đọc dữ liệu từ file Config 19/01/2011.*/
                XmlDocument doc = new XmlDocument();
                string path = Company.BLL.EntityBase.GetPathProgram() + "\\ConfigDoanhNghiep";
                //Hungtq update 28/01/2011.
                string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);

                doc.Load(fileName);
                //Minhnd get fontsize 06/10/2014
                fontsize = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontHuongDan", "10");

                //HUNGTQ Updated 07/06/2011
                //Get thong tin WebService
                DiaChiWS = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS").InnerText;
                DiaChiWS_Host = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Host").InnerText;
                DiaChiWS_Name = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Name").InnerText;

                Host = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Host").InnerText;
                Port = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Port").InnerText;

                SoDienThoaiDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DienThoai").InnerText;
                SoFaxDN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Fax").InnerText;
                //Get thong tin Server
                SERVER_NAME = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Server").InnerText;
                //Get thong tin Database
                DATABASE_NAME = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Database").InnerText;
                //Get thong tin UserName
                USER = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "UserName").InnerText;
                //Get thong tin PassWord
                PASS = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Password").InnerText;

                //Get thông tin MaCucHQ
                MA_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaCucHQ").InnerText;
                //Get thông tin TenCucHQ
                TEN_CUC_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenCucHQ").InnerText;
                //Get thông tin MaChiCucHQ
                MA_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaChiCucHQ").InnerText.Trim();
                //Get thông tin TenChiCucHQ
                TEN_HAI_QUAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenChiCucHQ").InnerText;
                //Get thông tin TenNganChiCucHQ
                TEN_HAI_QUAN_NGAN = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenNganChiCucHQ").InnerText;
                //Get thông tin MailHQ
                MailHaiQuan = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailHQ").InnerText;
                //Get thông tin MaDoanhNghiep
                MA_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaDoanhNghiep").InnerText.Trim();
                //Get thông tin TenDoanhNghiep
                TEN_DON_VI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "TenDoanhNghiep").InnerText;
                //Get thông tin DiaChiDoanhNghiep
                DIA_CHI = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DiaChiDoanhNghiep").InnerText;
                //Get thông tin MaMid
                MaMID = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MaMID").InnerText;
                IsOnlyMe = bool.Parse(Company.KDT.SHARE.Components.Globals.GetConfig(doc, "OnlyMe", "False"));
                MailDoanhNghiep = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "MailDoanhNghiep").InnerText;
                DienThoai = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "DienThoai").InnerText;
                Fax = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Fax").InnerText;
                NguoiLienHe = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "NguoiLienHe").InnerText;
                ChucVu = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "ChucVu").InnerText;

                FileSize = long.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FileSize"));
                MienThueGTGT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("MienThueGTGT");

                M07DMDK = bool.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("07DMDK"));
                //xuống dòng thông tin địa chỉ
                WordWrap = bool.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("WordWrap"));

                //Thong tin cau hinh proxy
                IsUseProxy = Host != "";

                TriGiaNT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TriGiaNT") != "" ? int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TriGiaNT")) : 4;
                DonGiaNT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DonGiaNT") != "" ? int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DonGiaNT")) : 6;
                TongTriGiaNT = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TongTriGiaNT") != "" ? int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TongTriGiaNT")) : 2;
                SoThapPhan_BaoCaoThanhKhoan = int.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("BCTK", "5"));

                SuDungChuKySo = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SuDungChuKySo"));
                try
                {
                    SendV4 = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SendV4"));
                }
                catch (System.Exception ex)
                {
                    SendV4 = false;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                InDinhMucTT196 = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("InDinhMucTT196", "false"));
                NoiDungSoHoSoThanhKhoan = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NoiDungSoHSTK", "");
                ThanhKhoanNhieuChiCuc = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ThanhKhoanNhieuChiCuc", "False"));
                //phi 12/08/2013
                //LoaihinhMD_Nhap = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "LoaiHinhMD_Nhap").InnerText;
                //LoaihinhMD_Xuat = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "LoaiHinhMD_Xuat").InnerText;
                LoaihinhMD_Nhap = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LoaiHinhMD_Nhap", "");
                LoaihinhMD_Xuat = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("LoaiHinhMD_Xuat", "");

                // load chu ky so cho ky tư xa
                iSignRemote = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsSignRemote", "False"));
                userNameSign = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("UserNameSignRemote", "");
                passwordSign = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("PasswordSignRemote", "");

                IsDelete = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsDelete", "False"));
                //TODO: VNACCS---------------------------------------------------------------------------------------------
                GlobalVNACC.PathConfig = Application.StartupPath + "\\Config";
                GlobalVNACC.TimerRequest = 5;
                GlobalVNACC.isStopRespone = false;
                GlobalVNACC.NguoiSuDung_DiaChi = string.Empty;
                ////TODO: Dua lay thong tin cau hinh cua VNACC vao Form Login.
                //GlobalVNACC.NguoiSuDung_Ma = "E8400";
                //GlobalVNACC.NguoiSuDung_ID = "001";
                //GlobalVNACC.TerminalID = "AAA17K"; //"BAAA5C";
                //GlobalVNACC.NguoiSuDung_Pass = "CTVNACCS";
                //GlobalVNACC.Url = "http://localhost:32333/test.aspx"; //"https://ediconn.vnaccs.customs.gov.vn/test";
                //GlobalVNACC.TerminalAccessKey = "9999CTVNACCS";
                IsKhongDungBangKeCont = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsKhongDungBangKeCont", "false"));
                SuDungSoTK = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SuDungSoTK") == "True" ? true : false;
                SoTK = Convert.ToInt32(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTK"));
                DongBoChungTuDinhKem = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("CHUNGTUDINHKEM") == "True" ? true : false;
                GlobalVNACC.IsGenerateTextDataVNACC = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsGenerateTextDataVNACC", "false").ToLower() == "true" ? true : false;
                IsSignOnLan = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsSignOnLan") == "True" ? true : false;
                DataSignLan = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DataSignLan");
                 
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            try
            {
                if (IsDaiLy && !string.IsNullOrEmpty(tmpMaDoanhNghiep))
                {

                    Company.Interface.TTDaiLy.Login lg = new Company.Interface.TTDaiLy.Login();
                    lg.GetValueConfig(tmpMaDoanhNghiep);
                }
            }
            catch (Exception ex)
            {

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            //if (!Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS) Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS = Company.KDT.SHARE.Components.Globals.KiemTraKBVNACC(GlobalSettings.MA_HAI_QUAN);
            MA_HAI_QUAN_VNACCS = Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACC(MA_HAI_QUAN, "MaHQ");
        }
    }
}
