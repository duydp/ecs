﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
    public partial class MucDichSuDung : BaseClass
    {
        public static string GetID(string name)
        {
            //string query = string.Format("SELECT ID FROM t_HaiQuan_MucDichSuDung WHERE [TenLoaiMucDich] = N'{0}'", name.Trim());
            //DbCommand dbCommand = db.GetSqlStringCommand(query);
            //IDataReader reader = db.ExecuteReader(dbCommand);
            //if (reader.Read())
            //{
            //    return reader["ID"].ToString();
            //}
            //return string.Empty;

            try
            {
                string query = string.Format("TenLoaiMucDich = '{0}'", name.Trim());

                DataRow[] filters = Globals.GlobalDanhMucChuanHQ.Tables["MucDichSuDung"].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["ID"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }

        public static string GetName(string id)
        {
            try
            {
                string query = string.Format("ID = '{0}'", id.Trim());

                DataRow[] filters = MucDichSuDung.SelectAll().Tables[0].Select(query);

                if (filters.Length > 0)
                {
                    return filters[0]["TenLoaiMucDich"].ToString();
                }
            }
            catch (System.Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return string.Empty;
        }

    }
}
