

/****** Object:  Table [dbo].[t_KDT_DonViTinhQuyDoi]    Script Date: 03/14/2013 14:59:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_DonViTinhQuyDoi]') AND type in (N'U'))
DROP TABLE [dbo].[t_KDT_DonViTinhQuyDoi]
GO


/****** Object:  Table [dbo].[t_KDT_DonViTinhQuyDoi]    Script Date: 03/14/2013 14:59:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_DonViTinhQuyDoi](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[Type] [nvarchar](10) NULL,
	[DVT_ID] [char](3) NULL,
	[TyLeQuyDoi] [float] NULL,
	[Temp1] [nvarchar](50) NULL,
	[Temp2] [nvarchar](50) NULL,
 CONSTRAINT [PK_t_KDT_DonViTinhQuyDoi] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


/****** Object:  Table [dbo].[t_KDT_ChungTuHQTruocDo]    Script Date: 03/14/2013 14:59:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_ChungTuHQTruocDo]') AND type in (N'U'))
DROP TABLE [dbo].[t_KDT_ChungTuHQTruocDo]
GO


/****** Object:  Table [dbo].[t_KDT_ChungTuHQTruocDo]    Script Date: 03/14/2013 14:59:16 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_ChungTuHQTruocDo](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[Type] [varchar](10) NULL,
	[LoaiChungTu] [bigint] NULL,
	[NgayKhaiCT] [datetime] NULL,
	[SoDangKyCT] [bigint] NULL,
	[NgayDangKyCT] [datetime] NULL,
	[MaHaiQuan] [varchar](8) NULL,
	[MaNguoiKhaiHQ] [nvarchar](35) NULL,
	[TenNguoiKhaiHQ] [nvarchar](255) NULL,
	[SoChungTu] [nvarchar](255) NULL,
	[NgayChungTu] [datetime] NULL,
	[NgayHHChungTu] [datetime] NULL,
	[MaNguoiPhatHanh] [nvarchar](35) NULL,
	[TenNguoiPhatHanh] [nvarchar](255) NULL,
	[MaNguoiDuocCap] [nvarchar](35) NULL,
	[TenNguoiDuocCap] [nvarchar](255) NULL,
	[GhiChu] [nvarchar](1000) NULL,
	[XinNo] [bit] NULL,
	[ThoiHanNop] [datetime] NULL,
	[Temp1] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_KDT_ChungTuHQTruocDo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_ChungTuHQTruocDo_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungTuHQTruocDo_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungTuHQTruocDo_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungTuHQTruocDo_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungTuHQTruocDo_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungTuHQTruocDo_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungTuHQTruocDo_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ChungTuHQTruocDo_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuHQTruocDo_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_Insert]
	@Master_ID bigint,
	@Type varchar(10),
	@LoaiChungTu bigint,
	@NgayKhaiCT datetime,
	@SoDangKyCT bigint,
	@NgayDangKyCT datetime,
	@MaHaiQuan varchar(8),
	@MaNguoiKhaiHQ nvarchar(35),
	@TenNguoiKhaiHQ nvarchar(255),
	@SoChungTu nvarchar(255),
	@NgayChungTu datetime,
	@NgayHHChungTu datetime,
	@MaNguoiPhatHanh nvarchar(35),
	@TenNguoiPhatHanh nvarchar(255),
	@MaNguoiDuocCap nvarchar(35),
	@TenNguoiDuocCap nvarchar(255),
	@GhiChu nvarchar(1000),
	@XinNo bit,
	@ThoiHanNop datetime,
	@Temp1 nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ChungTuHQTruocDo]
(
	[Master_ID],
	[Type],
	[LoaiChungTu],
	[NgayKhaiCT],
	[SoDangKyCT],
	[NgayDangKyCT],
	[MaHaiQuan],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[SoChungTu],
	[NgayChungTu],
	[NgayHHChungTu],
	[MaNguoiPhatHanh],
	[TenNguoiPhatHanh],
	[MaNguoiDuocCap],
	[TenNguoiDuocCap],
	[GhiChu],
	[XinNo],
	[ThoiHanNop],
	[Temp1]
)
VALUES 
(
	@Master_ID,
	@Type,
	@LoaiChungTu,
	@NgayKhaiCT,
	@SoDangKyCT,
	@NgayDangKyCT,
	@MaHaiQuan,
	@MaNguoiKhaiHQ,
	@TenNguoiKhaiHQ,
	@SoChungTu,
	@NgayChungTu,
	@NgayHHChungTu,
	@MaNguoiPhatHanh,
	@TenNguoiPhatHanh,
	@MaNguoiDuocCap,
	@TenNguoiDuocCap,
	@GhiChu,
	@XinNo,
	@ThoiHanNop,
	@Temp1
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuHQTruocDo_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_Update]
	@ID bigint,
	@Master_ID bigint,
	@Type varchar(10),
	@LoaiChungTu bigint,
	@NgayKhaiCT datetime,
	@SoDangKyCT bigint,
	@NgayDangKyCT datetime,
	@MaHaiQuan varchar(8),
	@MaNguoiKhaiHQ nvarchar(35),
	@TenNguoiKhaiHQ nvarchar(255),
	@SoChungTu nvarchar(255),
	@NgayChungTu datetime,
	@NgayHHChungTu datetime,
	@MaNguoiPhatHanh nvarchar(35),
	@TenNguoiPhatHanh nvarchar(255),
	@MaNguoiDuocCap nvarchar(35),
	@TenNguoiDuocCap nvarchar(255),
	@GhiChu nvarchar(1000),
	@XinNo bit,
	@ThoiHanNop datetime,
	@Temp1 nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_ChungTuHQTruocDo]
SET
	[Master_ID] = @Master_ID,
	[Type] = @Type,
	[LoaiChungTu] = @LoaiChungTu,
	[NgayKhaiCT] = @NgayKhaiCT,
	[SoDangKyCT] = @SoDangKyCT,
	[NgayDangKyCT] = @NgayDangKyCT,
	[MaHaiQuan] = @MaHaiQuan,
	[MaNguoiKhaiHQ] = @MaNguoiKhaiHQ,
	[TenNguoiKhaiHQ] = @TenNguoiKhaiHQ,
	[SoChungTu] = @SoChungTu,
	[NgayChungTu] = @NgayChungTu,
	[NgayHHChungTu] = @NgayHHChungTu,
	[MaNguoiPhatHanh] = @MaNguoiPhatHanh,
	[TenNguoiPhatHanh] = @TenNguoiPhatHanh,
	[MaNguoiDuocCap] = @MaNguoiDuocCap,
	[TenNguoiDuocCap] = @TenNguoiDuocCap,
	[GhiChu] = @GhiChu,
	[XinNo] = @XinNo,
	[ThoiHanNop] = @ThoiHanNop,
	[Temp1] = @Temp1
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuHQTruocDo_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@Type varchar(10),
	@LoaiChungTu bigint,
	@NgayKhaiCT datetime,
	@SoDangKyCT bigint,
	@NgayDangKyCT datetime,
	@MaHaiQuan varchar(8),
	@MaNguoiKhaiHQ nvarchar(35),
	@TenNguoiKhaiHQ nvarchar(255),
	@SoChungTu nvarchar(255),
	@NgayChungTu datetime,
	@NgayHHChungTu datetime,
	@MaNguoiPhatHanh nvarchar(35),
	@TenNguoiPhatHanh nvarchar(255),
	@MaNguoiDuocCap nvarchar(35),
	@TenNguoiDuocCap nvarchar(255),
	@GhiChu nvarchar(1000),
	@XinNo bit,
	@ThoiHanNop datetime,
	@Temp1 nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ChungTuHQTruocDo] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ChungTuHQTruocDo] 
		SET
			[Master_ID] = @Master_ID,
			[Type] = @Type,
			[LoaiChungTu] = @LoaiChungTu,
			[NgayKhaiCT] = @NgayKhaiCT,
			[SoDangKyCT] = @SoDangKyCT,
			[NgayDangKyCT] = @NgayDangKyCT,
			[MaHaiQuan] = @MaHaiQuan,
			[MaNguoiKhaiHQ] = @MaNguoiKhaiHQ,
			[TenNguoiKhaiHQ] = @TenNguoiKhaiHQ,
			[SoChungTu] = @SoChungTu,
			[NgayChungTu] = @NgayChungTu,
			[NgayHHChungTu] = @NgayHHChungTu,
			[MaNguoiPhatHanh] = @MaNguoiPhatHanh,
			[TenNguoiPhatHanh] = @TenNguoiPhatHanh,
			[MaNguoiDuocCap] = @MaNguoiDuocCap,
			[TenNguoiDuocCap] = @TenNguoiDuocCap,
			[GhiChu] = @GhiChu,
			[XinNo] = @XinNo,
			[ThoiHanNop] = @ThoiHanNop,
			[Temp1] = @Temp1
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ChungTuHQTruocDo]
		(
			[Master_ID],
			[Type],
			[LoaiChungTu],
			[NgayKhaiCT],
			[SoDangKyCT],
			[NgayDangKyCT],
			[MaHaiQuan],
			[MaNguoiKhaiHQ],
			[TenNguoiKhaiHQ],
			[SoChungTu],
			[NgayChungTu],
			[NgayHHChungTu],
			[MaNguoiPhatHanh],
			[TenNguoiPhatHanh],
			[MaNguoiDuocCap],
			[TenNguoiDuocCap],
			[GhiChu],
			[XinNo],
			[ThoiHanNop],
			[Temp1]
		)
		VALUES 
		(
			@Master_ID,
			@Type,
			@LoaiChungTu,
			@NgayKhaiCT,
			@SoDangKyCT,
			@NgayDangKyCT,
			@MaHaiQuan,
			@MaNguoiKhaiHQ,
			@TenNguoiKhaiHQ,
			@SoChungTu,
			@NgayChungTu,
			@NgayHHChungTu,
			@MaNguoiPhatHanh,
			@TenNguoiPhatHanh,
			@MaNguoiDuocCap,
			@TenNguoiDuocCap,
			@GhiChu,
			@XinNo,
			@ThoiHanNop,
			@Temp1
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuHQTruocDo_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ChungTuHQTruocDo]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuHQTruocDo_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ChungTuHQTruocDo] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuHQTruocDo_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Type],
	[LoaiChungTu],
	[NgayKhaiCT],
	[SoDangKyCT],
	[NgayDangKyCT],
	[MaHaiQuan],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[SoChungTu],
	[NgayChungTu],
	[NgayHHChungTu],
	[MaNguoiPhatHanh],
	[TenNguoiPhatHanh],
	[MaNguoiDuocCap],
	[TenNguoiDuocCap],
	[GhiChu],
	[XinNo],
	[ThoiHanNop],
	[Temp1]
FROM
	[dbo].[t_KDT_ChungTuHQTruocDo]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuHQTruocDo_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[Type],
	[LoaiChungTu],
	[NgayKhaiCT],
	[SoDangKyCT],
	[NgayDangKyCT],
	[MaHaiQuan],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[SoChungTu],
	[NgayChungTu],
	[NgayHHChungTu],
	[MaNguoiPhatHanh],
	[TenNguoiPhatHanh],
	[MaNguoiDuocCap],
	[TenNguoiDuocCap],
	[GhiChu],
	[XinNo],
	[ThoiHanNop],
	[Temp1]
FROM [dbo].[t_KDT_ChungTuHQTruocDo] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ChungTuHQTruocDo_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ChungTuHQTruocDo_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Type],
	[LoaiChungTu],
	[NgayKhaiCT],
	[SoDangKyCT],
	[NgayDangKyCT],
	[MaHaiQuan],
	[MaNguoiKhaiHQ],
	[TenNguoiKhaiHQ],
	[SoChungTu],
	[NgayChungTu],
	[NgayHHChungTu],
	[MaNguoiPhatHanh],
	[TenNguoiPhatHanh],
	[MaNguoiDuocCap],
	[TenNguoiDuocCap],
	[GhiChu],
	[XinNo],
	[ThoiHanNop],
	[Temp1]
FROM
	[dbo].[t_KDT_ChungTuHQTruocDo]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_DonViTinhQuyDoi_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_DonViTinhQuyDoi_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_DonViTinhQuyDoi_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_DonViTinhQuyDoi_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_DonViTinhQuyDoi_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_DonViTinhQuyDoi_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_DonViTinhQuyDoi_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_DonViTinhQuyDoi_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DonViTinhQuyDoi_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_Insert]
	@Master_ID bigint,
	@Type nvarchar(10),
	@DVT_ID char(3),
	@TyLeQuyDoi float,
	@Temp1 nvarchar(50),
	@Temp2 nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_DonViTinhQuyDoi]
(
	[Master_ID],
	[Type],
	[DVT_ID],
	[TyLeQuyDoi],
	[Temp1],
	[Temp2]
)
VALUES 
(
	@Master_ID,
	@Type,
	@DVT_ID,
	@TyLeQuyDoi,
	@Temp1,
	@Temp2
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DonViTinhQuyDoi_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_Update]
	@ID bigint,
	@Master_ID bigint,
	@Type nvarchar(10),
	@DVT_ID char(3),
	@TyLeQuyDoi float,
	@Temp1 nvarchar(50),
	@Temp2 nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_DonViTinhQuyDoi]
SET
	[Master_ID] = @Master_ID,
	[Type] = @Type,
	[DVT_ID] = @DVT_ID,
	[TyLeQuyDoi] = @TyLeQuyDoi,
	[Temp1] = @Temp1,
	[Temp2] = @Temp2
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DonViTinhQuyDoi_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@Type nvarchar(10),
	@DVT_ID char(3),
	@TyLeQuyDoi float,
	@Temp1 nvarchar(50),
	@Temp2 nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_DonViTinhQuyDoi] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_DonViTinhQuyDoi] 
		SET
			[Master_ID] = @Master_ID,
			[Type] = @Type,
			[DVT_ID] = @DVT_ID,
			[TyLeQuyDoi] = @TyLeQuyDoi,
			[Temp1] = @Temp1,
			[Temp2] = @Temp2
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_DonViTinhQuyDoi]
		(
			[Master_ID],
			[Type],
			[DVT_ID],
			[TyLeQuyDoi],
			[Temp1],
			[Temp2]
		)
		VALUES 
		(
			@Master_ID,
			@Type,
			@DVT_ID,
			@TyLeQuyDoi,
			@Temp1,
			@Temp2
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DonViTinhQuyDoi_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_DonViTinhQuyDoi]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DonViTinhQuyDoi_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_DonViTinhQuyDoi] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DonViTinhQuyDoi_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Type],
	[DVT_ID],
	[TyLeQuyDoi],
	[Temp1],
	[Temp2]
FROM
	[dbo].[t_KDT_DonViTinhQuyDoi]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DonViTinhQuyDoi_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[Type],
	[DVT_ID],
	[TyLeQuyDoi],
	[Temp1],
	[Temp2]
FROM [dbo].[t_KDT_DonViTinhQuyDoi] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_DonViTinhQuyDoi_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_DonViTinhQuyDoi_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Type],
	[DVT_ID],
	[TyLeQuyDoi],
	[Temp1],
	[Temp2]
FROM
	[dbo].[t_KDT_DonViTinhQuyDoi]	

GO


/****** Object:  Table [dbo].[t_KDT_ThuTucHQTruocDo]    Script Date: 03/14/2013 15:20:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_ThuTucHQTruocDo]') AND type in (N'U'))
DROP TABLE [dbo].[t_KDT_ThuTucHQTruocDo]
GO


/****** Object:  Table [dbo].[t_KDT_ThuTucHQTruocDo]    Script Date: 03/14/2013 15:20:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_ThuTucHQTruocDo](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[Type] [varchar](8) NULL,
	[NoiDungThuTuc] [nvarchar](max) NULL,
 CONSTRAINT [PK_t_KDT_ThuTucHQTruocDo] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_ThuTucHQTruocDo_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_ThuTucHQTruocDo_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_ThuTucHQTruocDo_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_ThuTucHQTruocDo_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_ThuTucHQTruocDo_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ThuTucHQTruocDo_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_ThuTucHQTruocDo_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_ThuTucHQTruocDo_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Insert]
	@Master_ID bigint,
	@Type varchar(8),
	@NoiDungThuTuc nvarchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ThuTucHQTruocDo]
(
	[Master_ID],
	[Type],
	[NoiDungThuTuc]
)
VALUES 
(
	@Master_ID,
	@Type,
	@NoiDungThuTuc
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Update]
	@ID bigint,
	@Master_ID bigint,
	@Type varchar(8),
	@NoiDungThuTuc nvarchar(max)
AS

UPDATE
	[dbo].[t_KDT_ThuTucHQTruocDo]
SET
	[Master_ID] = @Master_ID,
	[Type] = @Type,
	[NoiDungThuTuc] = @NoiDungThuTuc
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@Type varchar(8),
	@NoiDungThuTuc nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ThuTucHQTruocDo] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ThuTucHQTruocDo] 
		SET
			[Master_ID] = @Master_ID,
			[Type] = @Type,
			[NoiDungThuTuc] = @NoiDungThuTuc
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ThuTucHQTruocDo]
		(
			[Master_ID],
			[Type],
			[NoiDungThuTuc]
		)
		VALUES 
		(
			@Master_ID,
			@Type,
			@NoiDungThuTuc
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ThuTucHQTruocDo]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ThuTucHQTruocDo] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Type],
	[NoiDungThuTuc]
FROM
	[dbo].[t_KDT_ThuTucHQTruocDo]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[Type],
	[NoiDungThuTuc]
FROM [dbo].[t_KDT_ThuTucHQTruocDo] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Type],
	[NoiDungThuTuc]
FROM
	[dbo].[t_KDT_ThuTucHQTruocDo]	

GO

/*
Run this script on:

        192.168.72.100.ECS_TQDT_KD_V4_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_KD_V4

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 03/18/2013 10:54:25 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [dbo].[t_KDT_ThuTucHQTruocDo]'
GO
CREATE TABLE [dbo].[t_KDT_ThuTucHQTruocDo]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[Master_ID] [bigint] NULL,
[Type] [varchar] (8) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoiDungThuTuc] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_ThuTucHQTruocDo] on [dbo].[t_KDT_ThuTucHQTruocDo]'
GO
ALTER TABLE [dbo].[t_KDT_ThuTucHQTruocDo] ADD CONSTRAINT [PK_t_KDT_ThuTucHQTruocDo] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ThuTucHQTruocDo_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Insert]
	@Master_ID bigint,
	@Type varchar(8),
	@NoiDungThuTuc nvarchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_ThuTucHQTruocDo]
(
	[Master_ID],
	[Type],
	[NoiDungThuTuc]
)
VALUES 
(
	@Master_ID,
	@Type,
	@NoiDungThuTuc
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ThuTucHQTruocDo_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Update]
	@ID bigint,
	@Master_ID bigint,
	@Type varchar(8),
	@NoiDungThuTuc nvarchar(max)
AS

UPDATE
	[dbo].[t_KDT_ThuTucHQTruocDo]
SET
	[Master_ID] = @Master_ID,
	[Type] = @Type,
	[NoiDungThuTuc] = @NoiDungThuTuc
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ThuTucHQTruocDo_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@Type varchar(8),
	@NoiDungThuTuc nvarchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_ThuTucHQTruocDo] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_ThuTucHQTruocDo] 
		SET
			[Master_ID] = @Master_ID,
			[Type] = @Type,
			[NoiDungThuTuc] = @NoiDungThuTuc
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_ThuTucHQTruocDo]
		(
			[Master_ID],
			[Type],
			[NoiDungThuTuc]
		)
		VALUES 
		(
			@Master_ID,
			@Type,
			@NoiDungThuTuc
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ThuTucHQTruocDo_Delete]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_Delete]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_ThuTucHQTruocDo]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ThuTucHQTruocDo_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Type],
	[NoiDungThuTuc]
FROM
	[dbo].[t_KDT_ThuTucHQTruocDo]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ThuTucHQTruocDo_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[Type],
	[NoiDungThuTuc]
FROM
	[dbo].[t_KDT_ThuTucHQTruocDo]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_GiayPhep]'
GO
ALTER TABLE [dbo].[t_KDT_GiayPhep] ADD
[LoaiGiayPhep] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[HinhThucTruLui] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_Insert]'
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Insert]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_Insert]
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@LoaiGiayPhep varchar(10),
	@HinhThucTruLui nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GiayPhep]
(
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
)
VALUES 
(
	@SoGiayPhep,
	@NgayGiayPhep,
	@NgayHetHan,
	@NguoiCap,
	@NoiCap,
	@MaDonViDuocCap,
	@TenDonViDuocCap,
	@MaCoQuanCap,
	@TenQuanCap,
	@ThongTinKhac,
	@MaDoanhNghiep,
	@TKMD_ID,
	@GuidStr,
	@LoaiKB,
	@SoTiepNhan,
	@NgayTiepNhan,
	@TrangThai,
	@NamTiepNhan,
	@LoaiGiayPhep,
	@HinhThucTruLui
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_Update]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Update]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_Update]
	@ID bigint,
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@LoaiGiayPhep varchar(10),
	@HinhThucTruLui nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_GiayPhep]
SET
	[SoGiayPhep] = @SoGiayPhep,
	[NgayGiayPhep] = @NgayGiayPhep,
	[NgayHetHan] = @NgayHetHan,
	[NguoiCap] = @NguoiCap,
	[NoiCap] = @NoiCap,
	[MaDonViDuocCap] = @MaDonViDuocCap,
	[TenDonViDuocCap] = @TenDonViDuocCap,
	[MaCoQuanCap] = @MaCoQuanCap,
	[TenQuanCap] = @TenQuanCap,
	[ThongTinKhac] = @ThongTinKhac,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TKMD_ID] = @TKMD_ID,
	[GuidStr] = @GuidStr,
	[LoaiKB] = @LoaiKB,
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[TrangThai] = @TrangThai,
	[NamTiepNhan] = @NamTiepNhan,
	[LoaiGiayPhep] = @LoaiGiayPhep,
	[HinhThucTruLui] = @HinhThucTruLui
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_InsertUpdate]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_InsertUpdate]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_InsertUpdate]
	@ID bigint,
	@SoGiayPhep varchar(100),
	@NgayGiayPhep datetime,
	@NgayHetHan datetime,
	@NguoiCap nvarchar(100),
	@NoiCap nvarchar(100),
	@MaDonViDuocCap varchar(50),
	@TenDonViDuocCap nvarchar(100),
	@MaCoQuanCap varchar(50),
	@TenQuanCap nvarchar(100),
	@ThongTinKhac nvarchar(500),
	@MaDoanhNghiep varchar(50),
	@TKMD_ID bigint,
	@GuidStr varchar(500),
	@LoaiKB int,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@TrangThai int,
	@NamTiepNhan int,
	@LoaiGiayPhep varchar(10),
	@HinhThucTruLui nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GiayPhep] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GiayPhep] 
		SET
			[SoGiayPhep] = @SoGiayPhep,
			[NgayGiayPhep] = @NgayGiayPhep,
			[NgayHetHan] = @NgayHetHan,
			[NguoiCap] = @NguoiCap,
			[NoiCap] = @NoiCap,
			[MaDonViDuocCap] = @MaDonViDuocCap,
			[TenDonViDuocCap] = @TenDonViDuocCap,
			[MaCoQuanCap] = @MaCoQuanCap,
			[TenQuanCap] = @TenQuanCap,
			[ThongTinKhac] = @ThongTinKhac,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TKMD_ID] = @TKMD_ID,
			[GuidStr] = @GuidStr,
			[LoaiKB] = @LoaiKB,
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[TrangThai] = @TrangThai,
			[NamTiepNhan] = @NamTiepNhan,
			[LoaiGiayPhep] = @LoaiGiayPhep,
			[HinhThucTruLui] = @HinhThucTruLui
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GiayPhep]
		(
			[SoGiayPhep],
			[NgayGiayPhep],
			[NgayHetHan],
			[NguoiCap],
			[NoiCap],
			[MaDonViDuocCap],
			[TenDonViDuocCap],
			[MaCoQuanCap],
			[TenQuanCap],
			[ThongTinKhac],
			[MaDoanhNghiep],
			[TKMD_ID],
			[GuidStr],
			[LoaiKB],
			[SoTiepNhan],
			[NgayTiepNhan],
			[TrangThai],
			[NamTiepNhan],
			[LoaiGiayPhep],
			[HinhThucTruLui]
		)
		VALUES 
		(
			@SoGiayPhep,
			@NgayGiayPhep,
			@NgayHetHan,
			@NguoiCap,
			@NoiCap,
			@MaDonViDuocCap,
			@TenDonViDuocCap,
			@MaCoQuanCap,
			@TenQuanCap,
			@ThongTinKhac,
			@MaDoanhNghiep,
			@TKMD_ID,
			@GuidStr,
			@LoaiKB,
			@SoTiepNhan,
			@NgayTiepNhan,
			@TrangThai,
			@NamTiepNhan,
			@LoaiGiayPhep,
			@HinhThucTruLui
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_Load]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_Load]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM
	[dbo].[t_KDT_GiayPhep]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM
	[dbo].[t_KDT_GiayPhep]
WHERE
	[TKMD_ID] = @TKMD_ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_SelectAll]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectAll]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM
	[dbo].[t_KDT_GiayPhep]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GiayPhep] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GiayPhep_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GiayPhep_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Monday, March 11, 2013
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GiayPhep_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoGiayPhep],
	[NgayGiayPhep],
	[NgayHetHan],
	[NguoiCap],
	[NoiCap],
	[MaDonViDuocCap],
	[TenDonViDuocCap],
	[MaCoQuanCap],
	[TenQuanCap],
	[ThongTinKhac],
	[MaDoanhNghiep],
	[TKMD_ID],
	[GuidStr],
	[LoaiKB],
	[SoTiepNhan],
	[NgayTiepNhan],
	[TrangThai],
	[NamTiepNhan],
	[LoaiGiayPhep],
	[HinhThucTruLui]
FROM [dbo].[t_KDT_GiayPhep] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ThuTucHQTruocDo_DeleteDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_ThuTucHQTruocDo] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_KDT_ThuTucHQTruocDo_SelectDynamic]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ThuTucHQTruocDo_SelectDynamic]
-- Database: ECS_TQDT_KD_V4
-- Author: Ngo Thanh Tung
-- Time created: Thursday, March 07, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_ThuTucHQTruocDo_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[Type],
	[NoiDungThuTuc]
FROM [dbo].[t_KDT_ThuTucHQTruocDo] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

UPDATE dbo.t_HaiQuan_Version SET [Version] = '7.4', [Date] = GETDATE(), Notes = N''

