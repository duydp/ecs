﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.KDT.SXXK;
using System.Data;

namespace Company.Interface.Report.SXXK
{
    public partial class ReportDinhMuc_TT196_2013 : DevExpress.XtraReports.UI.XtraReport
    {
        public delegate void GetObjectInfo(string hodong, string tokhai);

        public string MaSP;
        public string TenSP;
        public string DVT;
        public string HopDong;
        public DinhMucDangKy DMDangKy;
        private int STT = 0;
        public GetObjectInfo GetText;
        public bool inNKvaND = false;

        public ReportDinhMuc_TT196_2013()
        {
            InitializeComponent();
        }

        public void BindReport()
        {
            try
            {
                lblNgayDangKy.Text = string.Format(" {0}/ {1}/ {2}",
                    new object[] { GlobalSettings.NgayHeThong == true? DateTime.Now.Day.ToString("00") : "..", 
                                GlobalSettings.NgayHeThong == true? DateTime.Now.Month.ToString("00"):"..", 
                                GlobalSettings.NgayHeThong == true? DateTime.Now.Year.ToString("0000"):"...." });

                lblMaHQ.Text = GlobalSettings.MA_HAI_QUAN;
                lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                lblDiaChi.Text = GlobalSettings.DIA_CHI;
                lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;

                if (!string.IsNullOrEmpty(HopDong))
                {
                    // lblHopDong.Text = this.HopDong;
                    //lblHopDong.Visible = true;
                    //lblHopDongLable.Visible = true;
                }

                //else
                //{
                //lblHopDong.Visible = false;
                //lblHopDongLable.Visible = false;
                //lblSanPham.Location = lblHopDongLable.Location;
                //}
                //if (this.DMDangKy != null && this.DMDangKy.SoTiepNhan > 0)
                //    lblSoTN.Text = "STNĐM: " + this.DMDangKy.SoTiepNhan;
                lblMaSanPham.Text = this.MaSP;
                lblTenSP.Text = this.TenSP;
                lblDonViTinh.Text = this.DVT;

                if (this.DMDangKy != null)
                    this.DataSource = DMDangKy.getDinhMucOfSanPham(this.MaSP, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                else
                    this.DataSource = BLL.SXXK.DinhMuc.getDinhMucOfSanPham(this.MaSP, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                lblMaNPL.DataBindings.Add("Text", this.DataSource, "MaNguyenPhuLieu");
                lblDVT.DataBindings.Add("Text", this.DataSource, "DVT_ID");
                // Chua co du lieu dua vao
                lblDMTieuHao.Text = "";

                lblDMSuDung.DataBindings.Add("Text", this.DataSource, "DinhMucSuDung", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
                lblTyLeHH.DataBindings.Add("Text", this.DataSource, "TyLeHaoHut", "{0:n0}");
                lblDMChung.DataBindings.Add("Text", this.DataSource, "DinhMucChung", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
                lblTenNPL.DataBindings.Add("Text", this.DataSource, "TenNPL");
                lblGhiChu.DataBindings.Add("Text", this.DataSource, "GhiChu");
                if (!inNKvaND)
                    lblNguonCung.DataBindings.Add("Text", this.DataSource, "FromVietNam");
                else
                    lblNguonCung.Text = "Nhập khẩu và mua tại VN";
                lblNgay.Text = string.Format("{0}, ngày .. tháng .. năm ....", string.IsNullOrEmpty(GlobalSettings.DiaPhuong) ? "............" : GlobalSettings.DiaPhuong);
                lblNgay2.Text = string.Format("{0}, ngày {1} tháng {2} năm {3}",
                    new object[] { string.IsNullOrEmpty(GlobalSettings.DiaPhuong) ? "............" : GlobalSettings.DiaPhuong, 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Day.ToString("00") : "..", 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Month.ToString("00"):"..", 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Year.ToString("0000"):"...." });
                //lbTitleSoToKhai.Visible = lbSoToKhai.Visible = GlobalSettings.M07DMDK;
                //lbSoToKhai.Text = string.Format("{0}/XK/SXXK/{1} ngày {2}",
                // new object[] { ".......", GlobalSettings.MA_HAI_QUAN, ".../.../....." });
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        public void BindReportDinhMucDaDangKy()
        {
            lblNgayDangKy.Text = string.Format(" {0}/ {1}/ {2}",
                new object[] { GlobalSettings.NgayHeThong == true? DateTime.Now.Day.ToString("00") : "..", 
                                GlobalSettings.NgayHeThong == true? DateTime.Now.Month.ToString("00"):"..", 
                                GlobalSettings.NgayHeThong == true? DateTime.Now.Year.ToString("0000"):"...." });

            lblMaHQ.Text = GlobalSettings.MA_HAI_QUAN;
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;

            lblMaSanPham.Text = this.MaSP;
            lblTenSP.Text = this.TenSP;
            lblDonViTinh.Text = this.DVT;

            DataSet ds = BLL.SXXK.DinhMuc.getDinhMucOfSanPham(this.MaSP, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            if (ds.Tables.Count == 0) return;
            ds.Tables[0].Columns.Add("STTHang");
            int i = 1;
            foreach (DataRow row in ds.Tables[0].Rows)
                row["STTHang"] = i++;
            this.DataSource = ds.Tables[0];
            lblMaNPL.DataBindings.Add("Text", this.DataSource, "MaNguyenPhuLieu");
            lblSTT.DataBindings.Add("Text", this.DataSource, "STTHang");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT_ID");
            // Chua co du lieu dua vao
            lblDMTieuHao.Text = "";

            lblDMSuDung.DataBindings.Add("Text", this.DataSource, "DinhMucSuDung", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTyLeHH.DataBindings.Add("Text", this.DataSource, "TyLeHaoHut", "{0:n0}");
            lblDMChung.DataBindings.Add("Text", this.DataSource, "DinhMucChung", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, "TenNPL");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, "GhiChu");
            lblNgay.Text = string.Format("{0}, ngày .. tháng .. năm ....", string.IsNullOrEmpty(GlobalSettings.DiaPhuong) ? "............" : GlobalSettings.DiaPhuong);
            lblNgay2.Text = string.Format("{0}, ngày {1} tháng {2} năm {3}",
                new object[] { string.IsNullOrEmpty(GlobalSettings.DiaPhuong) ? "............" : GlobalSettings.DiaPhuong, 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Day.ToString("00") : "..", 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Month.ToString("00"):"..", 
                    GlobalSettings.NgayHeThong == true? DateTime.Now.Year.ToString("0000"):"...." });
            if (!inNKvaND)
                lblNguonCung.DataBindings.Add("Text", this.DataSource, "FromVietNam");
            else
                lblNguonCung.Text = "Nhập khẩu và mua tại VN";

            //lbTitleSoToKhai.Visible = lbSoToKhai.Visible = GlobalSettings.M07DMDK;

            //lbSoToKhai.Text = string.Format("{0}/XK/SXXK/{1} ngày {2}",
            //                new object[] { ".......", GlobalSettings.MA_HAI_QUAN, ".../.../....." });

        }

        private void lblTenNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
        }

        private void lblTyLeHH_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void ReportDinhMuc_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        public void SetText(string hopdong, string tokhai)
        {
            //if (!string.IsNullOrEmpty(hopdong))
            //    lblHopDong.Text = hopdong;
            //if (!string.IsNullOrEmpty(tokhai))
            //{
            //    lbSoToKhai.Text = tokhai;
            //}
            this.CreateDocument();
        }

        //private void lbSoToKhai_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        //{
        //    if (GetText != null)
        //        GetText(lblHopDong.Text, lbSoToKhai.Text);
        //}

    }
}
