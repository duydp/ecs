﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using System.IO;
using System.Data;

namespace KDTService
{
    public class Helpers
    {
        public static  void SaveMessage(string MsgXML, string passWord, string ghichu)
        {
            //Logger.LocalLogger.Instance().WriteMessage(new Exception(MsgXML));
            if (string.IsNullOrEmpty(MsgXML)) return;
            try
            {
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                string query = "insert into MSG (msg, pass,GhiCHu) VALUES (@value, @pass,@GhiCHu)";
                SqlCommand cmd = (SqlCommand)db.GetSqlStringCommand(query);
                db.AddInParameter(cmd, "@value", SqlDbType.NVarChar, MsgXML);
                db.AddInParameter(cmd, "@pass", SqlDbType.NVarChar, passWord);
                db.AddInParameter(cmd, "@GhiCHu", SqlDbType.NVarChar, DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss  ") + ghichu);
                db.ExecuteNonQuery(cmd);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex.Message, new Exception(MsgXML));
            }
        }
        public static void SaveMessage(string MsgXML, string passWord)
        {
            SaveMessage(MsgXML, passWord, string.Empty);
        }
    }
}
