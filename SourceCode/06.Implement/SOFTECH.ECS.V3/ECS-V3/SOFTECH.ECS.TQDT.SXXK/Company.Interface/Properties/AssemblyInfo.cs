﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SOFTECH ECS TQDT SXXK v4.0")]
[assembly: AssemblyDescription("Phần mềm Khai báo Hải quan điện tử - Sản xuất xuất khẩu")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Công ty Cổ phần SOFTECH")]
[assembly: AssemblyProduct("SOFTECH ECS TQDT SXXK v4.0")]
[assembly: AssemblyCopyright("Copyright © Softech 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("AB08870F-2EF4-4BB2-9612-5F52812E7E2B")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("3.0.2013.1202")]
[assembly: AssemblyFileVersion("3.0.2013.1202")]
[assembly: NeutralResourcesLanguageAttribute("")]
