using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Security;
using System.IO;

namespace Company.KDT.SHARE.Components
{

    public class RSACryptographyHelper
    {

        private System.Security.Cryptography.X509Certificates.X509Certificate2 cert;

        public string Issuer
        {
            get
            {
                return cert.Issuer;
            }
        }

        public System.DateTime NotAfter
        {
            get
            {
                return cert.NotAfter;
            }
        }

        public System.DateTime NotBefore
        {
            get
            {
                return cert.NotBefore;
            }
        }

        public string RawData
        {
            get
            {
                return System.Convert.ToBase64String(cert.RawData);
            }
        }

        public string SerialNumber
        {
            get
            {
                return cert.SerialNumber;
            }
        }

        public RSACryptographyHelper(string certificated)
        {
            if (System.String.IsNullOrEmpty(certificated))
                throw new System.Security.Cryptography.CryptographicException("Thông tư chứng thư không được rỗng.");
            byte[] bArr = System.Convert.FromBase64String(certificated);
            cert = new System.Security.Cryptography.X509Certificates.X509Certificate2();
            cert.Import(bArr);
            ValidateCert();
        }

        public RSACryptographyHelper(System.Security.Cryptography.X509Certificates.X509Certificate2 certificated)
        {
            if (certificated == null)
                throw new System.Security.Cryptography.CryptographicException("Thông tư chứng thư không được rỗng.");
            cert = certificated;
            ValidateCert();
        }

        public RSACryptographyHelper(byte[] certificated)
        {
            if (certificated == null)
                throw new System.Security.Cryptography.CryptographicException("Thông tư chứng thư không được rỗng.");
            cert = new System.Security.Cryptography.X509Certificates.X509Certificate2();
            cert.Import(certificated);
            ValidateCert();
        }

        public System.Security.Cryptography.HashAlgorithm GetHashAlgorithm()
        {
            if (cert.SignatureAlgorithm.FriendlyName.ToUpper().Contains("MD5"))
                return GetHashAlgorithm(HashAlgorithmName.MD5);
            if (cert.SignatureAlgorithm.FriendlyName.ToUpper().Contains("SHA1"))
                return GetHashAlgorithm(HashAlgorithmName.SHA1);
            if (cert.SignatureAlgorithm.FriendlyName.ToUpper().Contains("SHA256"))
                return GetHashAlgorithm(HashAlgorithmName.SHA256);
            if (cert.SignatureAlgorithm.FriendlyName.ToUpper().Contains("SHA384"))
                return GetHashAlgorithm(HashAlgorithmName.SHA384);
            if (cert.SignatureAlgorithm.FriendlyName.ToUpper().Contains("SHA512"))
                return GetHashAlgorithm(HashAlgorithmName.SHA512);
            throw new System.Security.Cryptography.CryptographicException("Unknown hash algorithm!");
        }

        private System.Security.Cryptography.HashAlgorithm GetHashAlgorithm(HashAlgorithmName name)
        {
            switch (name)
            {
                case HashAlgorithmName.MD5:
                    return new System.Security.Cryptography.MD5CryptoServiceProvider();

                case HashAlgorithmName.SHA1:
                    return new System.Security.Cryptography.SHA1Managed();

                case HashAlgorithmName.SHA256:
                    return new System.Security.Cryptography.SHA256Managed();

                case HashAlgorithmName.SHA384:
                    return new System.Security.Cryptography.SHA384Managed();

                case HashAlgorithmName.SHA512:
                    return new System.Security.Cryptography.SHA512Managed();
            }
            throw new System.Security.Cryptography.CryptographicException("Unknown hash algorithm!");
        }

        public string GetHashAlgorithmName()
        {
            if (cert.SignatureAlgorithm.FriendlyName.ToUpper().Contains("MD5"))
                return 1.ToString();
            if (cert.SignatureAlgorithm.FriendlyName.ToUpper().Contains("SHA1"))
                return "SHA1";//2.ToString();//            
            return System.String.Empty;
        }

        public CspParameters GetCspParameters(string password)
        {
            if (!cert.HasPrivateKey)
                throw new System.Security.Cryptography.CryptographicException("Không có khóa bí mật trong Certificate!");
            CspKeyContainerInfo keyContainerInfo = ((RSACryptoServiceProvider)cert.PrivateKey).CspKeyContainerInfo;
            CspParameters parameters = new CspParameters();
            parameters.ProviderName = keyContainerInfo.ProviderName;
            parameters.ProviderType = keyContainerInfo.ProviderType;
            parameters.Flags = CspProviderFlags.UseExistingKey | CspProviderFlags.NoPrompt;           
            parameters.KeyContainerName = keyContainerInfo.KeyContainerName;
            parameters.KeyNumber = (int)keyContainerInfo.KeyNumber;
            parameters.KeyPassword = new SecureString();
            //Set password
            foreach (char c in password.ToCharArray())
                parameters.KeyPassword.AppendChar(c);
            return parameters;

        }
        #region Ký theo tùy chọn
        public byte[] SignHash(byte[] data, string password)
        {
            #region Test password LanNT
            //CspParameters parameters = GetCspParameters("MKkb1980");
            //RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider(parameters);
            // byte[]arByTmp= cryptoServiceProvider.SignHash(data, System.Security.Cryptography.CryptoConfig.MapNameToOID(GetHashAlgorithmName()));            
            #endregion            
            CspParameters parameters = GetCspParameters(password);            
            RSACryptoServiceProvider cryptoServiceProvider = new RSACryptoServiceProvider(parameters);
            string strAlgorithmOID = System.Security.Cryptography.CryptoConfig.MapNameToOID(GetHashAlgorithmName());
            byte[] bArr= cryptoServiceProvider.SignHash(data,strAlgorithmOID);           
            return bArr;
        }
        public string SignHash(string data, string password)
        {
            byte[] bArr1 = System.Text.Encoding.UTF8.GetBytes(data);
            System.Security.Cryptography.HashAlgorithm hashAlgorithm = GetHashAlgorithm();
            byte[] bArr2 = SignHash(hashAlgorithm.ComputeHash(bArr1), password);
            return System.Convert.ToBase64String(bArr2);
        }
        #endregion


        #region Ký theo thông tin nhà cung cấp
        public byte[] SignHash(byte[] data)
        {
            if (!cert.HasPrivateKey)
                throw new System.Security.Cryptography.CryptographicException("Không có khóa bí mật trong Certificate!");
            System.Security.Cryptography.RSACryptoServiceProvider rsacryptoServiceProvider = cert.PrivateKey as System.Security.Cryptography.RSACryptoServiceProvider;
            return rsacryptoServiceProvider.SignHash(data, System.Security.Cryptography.CryptoConfig.MapNameToOID(GetHashAlgorithmName()));
        }

        public string SignHash(string data)
        {
            byte[] bArr1 = System.Text.Encoding.UTF8.GetBytes(data);
            System.Security.Cryptography.HashAlgorithm hashAlgorithm = GetHashAlgorithm();
            //Nghị định 87
            string str = Convert.ToBase64String(hashAlgorithm.ComputeHash(bArr1));
            bArr1 = Convert.FromBase64String(str);

            byte[] bArr2 = SignHash(bArr1);
            return System.Convert.ToBase64String(bArr2);
        }
        #endregion

        private void ValidateCert()
        {
            System.DateTime dateTime = System.DateTime.Now;
            if (dateTime > NotAfter || dateTime < NotBefore)
                throw new System.Security.Cryptography.CryptographicException("Certificate không còn giá trị để sử dụng");
            string sr = SerialNumber;
        }
        #region Export import
        private void Export(RSACryptoServiceProvider cryptoServiceProvider)
        {

            using (FileStream fs = new FileStream(AppDomain.CurrentDomain.BaseDirectory + "Cer.xml", FileMode.OpenOrCreate))
            {
                byte[] blob = cryptoServiceProvider.ExportCspBlob(true);
                fs.Write(blob, 0, blob.Length);
            }
        }

        #endregion
    }

}

