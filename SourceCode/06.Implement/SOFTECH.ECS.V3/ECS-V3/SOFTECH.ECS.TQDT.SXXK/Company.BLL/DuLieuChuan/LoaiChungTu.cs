﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.BLL.DuLieuChuan
{
    public class LoaiChungTu : BaseClass
    {
        public static DataSet SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_LoaiChungTu ORDER BY Ten";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }

        public static string getName(string id)
        {
            string query = "SELECT Ten FROM t_HaiQuan_LoaiChungTu where id='"+id+"'";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteScalar(dbCommand).ToString();
        }
        public static void Update(DataSet ds)
        {
            string insert = "INSERT INTO t_HaiQuan_LoaiChungTu VALUES(@Id,@Ten)";
            string update = "UPDATE t_HaiQuan_LoaiChungTu SET Ten = @Ten WHERE Id=@Id";
            string delete = "DELETE FROM t_HaiQuan_LoaiChungTu WHERE ID = @Id";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.VarChar, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
    }
}