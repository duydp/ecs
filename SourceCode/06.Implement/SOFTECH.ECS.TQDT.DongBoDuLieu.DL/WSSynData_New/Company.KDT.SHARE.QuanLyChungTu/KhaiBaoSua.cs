﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Company.KDT.SHARE.Components;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class KhaiBaoSua
    {
        public static long SoTNSua(long sotn, int namdk, string maloaihinh,
            string mahaiquan, string madoanhnghiep, string loaikhaibao)
        {
            return SoTNSua(sotn, 0, namdk, maloaihinh, maloaihinh, madoanhnghiep, loaikhaibao);
        }
        public static long SoTNSua(long sotn, long sotk, int namdk, string maloaihinh,
            string mahaiquan, string madoanhnghiep, string loaikhaibao)
        {

            try
            {
                long value = sotn;
                string sfmt = string.Format(@"SoToKhai={0} and NamDK={1} 
                                              and MaLoaiHinh='{2}' and MaHaiQuan='{3}' and MaDoanhNghiep='{4}' and F1='{5}'",
                new object[] { sotk, namdk, maloaihinh.Trim(), mahaiquan.Trim(), madoanhnghiep.Trim(), loaikhaibao });

                List<KhaiBaoSua> kbsItems = KhaiBaoSua.SelectCollectionDynamic(sfmt, "");
                if (kbsItems.Count == 0)
                {
                    KhaiBaoSua kbs = new KhaiBaoSua()
                    {
                        SoTiepNhan = sotn,
                        SoToKhai = sotk,
                        NamDK = namdk,
                        MaLoaiHinh = maloaihinh.Trim(),
                        MaHaiQuan = mahaiquan.Trim(),
                        MaDoanhNghiep = madoanhnghiep.Trim(),
                        F1 = loaikhaibao
                    };
                    kbs.Insert();
                }
                else
                {
                    KhaiBaoSua kbs = kbsItems[0];
                    value = kbs.SoTiepNhan;
                }
                return value;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return sotn;
            }
        }
        public static void XoaSoTNCu(long sotk, int namdk, string maloaihinh,
            string mahaiquan, string madoanhnghiep, string loaikhaibao)
        {
            try
            {
                string sfmt = string.Format(@"SoToKhai={0} and NamDK={1} 
                                              and MaLoaiHinh='{2}' and MaHaiQuan='{3}' and MaDoanhNghiep='{4}' and F1='{5}'",
                new object[] { sotk, namdk, maloaihinh.Trim(), mahaiquan.Trim(), madoanhnghiep.Trim(), loaikhaibao });
                KhaiBaoSua.DeleteDynamic(sfmt);
            }
            catch { }
        }
    }
}
