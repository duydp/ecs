﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using System.Globalization;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class BaseClass
    {

        public static HangMauDich GetHangMauDichByIDHMD(long HMD_ID, List<HangMauDich> HMDCollection)
        {
            foreach (HangMauDich HMD in HMDCollection)
            {
                if (HMD.ID == HMD_ID)
                    return HMD;
            }
            return null;
        }

        public static GCCT.HangChuyenTiep GetHangChuyenTiepByIDHCT(long HCT_ID, List<GCCT.HangChuyenTiep> HCTCollection)
        {
            foreach (GCCT.HangChuyenTiep HCT in HCTCollection)
            {
                if (HCT.ID == HCT_ID)
                    return HCT;
            }
            return null;
        }

        public static string GetPathProgram()
        {
            FileInfo fileInfo = new FileInfo(Application.ExecutablePath);
            return fileInfo.DirectoryName;
        }

        /// <summary>
        /// Làm tròn giá trị
        /// </summary>
        /// <param name="value">Giá trị</param>
        /// <param name="fractionDigits">Số thập phân làm tròn số</param>
        /// <returns></returns>
        public static string Round(decimal value, int fractionDigits)
        {
            return Round(Convert.ToDouble(value), fractionDigits);
        }

        /// <summary>
        /// Làm tròn giá trị
        /// </summary>
        /// <param name="value">Giá trị</param>
        /// <param name="fractionDigits">Số thập phân làm tròn số</param>
        /// <returns></returns>
        public static string Round(double value, int fractionDigits)
        {
            CultureInfo culture = new CultureInfo("en-US");
            NumberFormatInfo f = new NumberFormatInfo();

            return System.Math.Round(value, fractionDigits).ToString(f);
        }

    }

}