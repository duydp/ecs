﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.VNACCS;
#if GC_V4
using Company.GC.BLL.KDT;
#endif
#if SXXK_V4
using Company.BLL.KDT;
#endif

namespace Company.Interface.VNACCS.ExportTotalData
{
    public partial class V4_ThongKeTKXNKForm : BaseForm
    {
        public V4_ThongKeTKXNKForm()
        {
            InitializeComponent();
        }

        private void V4_ThongKeTKXNKForm_Load(object sender, EventArgs e)
        {
            clcDenNgay.Value = clcTuNgay.Value.AddDays(30);
            cbbLoaiBC.SelectedIndex = 0;
            btnTimKiem_Click(null, null);
        }
        private string GetSeachWhere()
        {
            try
            {

                //this.Cursor = Cursors.WaitCursor;

                string where = String.Empty;
                where = "TKMD.MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND TKMD.TrangThaiXuLy IN (1,10) ";
                if (!String.IsNullOrEmpty(txtSoHoaDon.Text.Trim()))
                    where = where + " AND TKMD.SoHoaDonThuongMai LIKE '%" + txtSoHoaDon.Text.Trim() + "%' ";
#if GC_V4
                if (!String.IsNullOrEmpty(txtSoHopDong.Text.Trim()))
                    where = where + " AND TKMD.SoHopDong LIKE '%" + txtSoHopDong.Text.Trim() + "%'";
#endif
                if (!string.IsNullOrEmpty(txtMaLoaiHinh.Text))
                {
                    if (txtMaLoaiHinh.Text.Contains(";"))
                    {
                        string[] listMlh = txtMaLoaiHinh.Text.Split(';');
                        string seachMlh = string.Empty;
                        foreach (string item in listMlh)
                        {
                            seachMlh += "'" + item.Trim() + "'" + ",";
                        }
                        seachMlh = seachMlh.Remove(seachMlh.Length - 1);
                        where = where + " AND TKMD.MaLoaiHinh in (" + seachMlh + ") ";
                    }
                    else
                    {
                        where = where + " AND TKMD.MaLoaiHinh = '" + txtMaLoaiHinh.Text.Trim() + "' ";
                    }
                }
                if (!String.IsNullOrEmpty(txtTenDoiTac.Text))
                    where = where + " AND TKMD.TenDonViDoiTac LIKE '%" + txtTenDoiTac.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(cbbLoaiBC.SelectedValue.ToString()))
                {
                    string LoaiBaoCao = cbbLoaiBC.SelectedValue.ToString();
                    switch (LoaiBaoCao)
                    {
                        case "V":
                            where = where + " AND SUBSTRING(TKMD.MaLoaiHinh,1,1) IN ('N','X') ";
                            break;
                        case "NV":
                            where = where + " AND SUBSTRING(TKMD.MaLoaiHinh,1,1) IN ('N') ";
                            break;
                        case "XV":
                            where = where + " AND SUBSTRING(TKMD.MaLoaiHinh,1,1) IN ('X') ";
                            break;
                    }
                }

                DateTime fromDate = clcTuNgay.Value;
                DateTime toDate = clcDenNgay.Value;
                where = where + " AND (TKMD.NgayDangKy Between '" + fromDate.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + toDate.ToString("yyyy-MM-dd 23:59:59") + "')";

                if (!String.IsNullOrEmpty(txtMaHangHoa.Text.Trim()))
                    where = where + " AND HMD.MaPhu LIKE '%" + txtMaHangHoa.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(txtTenHangHoa.Text.Trim()))
                    where = where + " AND HMD.TenHang LIKE '%" + txtTenHangHoa.Text.Trim() + "%' ";
                if (!String.IsNullOrEmpty(txtMaHS.Text.Trim()))
                    where = where + " AND HMD.MaHS LIKE '%" + txtMaHS.Text.Trim() + "%' ";

                return where;
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1 ";
            }

        }
        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                grList.Refresh();
                grList.DataSource = ToKhaiMauDich.ThongKeToKhaiXNK(GetSeachWhere(), "").Tables[0];
                grList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Bảng Thống kê Tờ khai xuất nhập khẩu V4  Từ ngày_" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + " Đến ngày _" + clcDenNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = grList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbGroupBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnTimKiem_Click(null, null);
                grList.RootTable.Groups.Clear();
                grList.RootTable.GroupHeaderTotals.Clear();

                GridEXGroup group = new GridEXGroup();
                GridEXColumn column = new GridEXColumn();
                column = grList.RootTable.Columns["" + cbbGroupBy.SelectedValue.ToString() + ""];
                group = new GridEXGroup(column, Janus.Windows.GridEX.SortOrder.Ascending);
                grList.RootTable.Groups.Add(group);

                column = grList.RootTable.Columns["SoToKhai"];
                GridEXGroupHeaderTotal groupHeaderNPL = new GridEXGroupHeaderTotal();
                groupHeaderNPL.AggregateFunction = AggregateFunction.Count;
                groupHeaderNPL.Column = column;
                groupHeaderNPL.TotalPrefix = "( Tổng số : ";
                groupHeaderNPL.TotalSuffix = " TK)";
                grList.RootTable.GroupHeaderTotals.Add(groupHeaderNPL);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
