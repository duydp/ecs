﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class BranchDetail
    {
        /// <summary>
        /// Số thứ tự vận đơn nhánh
        /// </summary>
        [XmlElement("sequence")]
        public string Sequence { get; set; }

        /// <summary>
        /// Số vận đơn nhánh
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Tên người gửi hàng của vận đơn nhánh
        /// </summary>
        [XmlElement("shipperName")]
        public string ShipperName { get; set; }

        /// <summary>
        /// Địa chỉ người gửi hàng của vận đơn nhánh
        /// </summary>
        [XmlElement("shipperAddress")]
        public string ShipperAddress { get; set; }

        /// <summary>
        /// Tên người nhận hàng của vận đơn nhánh
        /// </summary>
        [XmlElement("consigneeName")]
        public string ConsigneeName { get; set; }

        /// <summary>
        /// Địa chỉ người nhận hàng của vận đơn nhánh
        /// </summary>
        [XmlElement("consigneeAddress")]
        public string ConsigneeAddress { get; set; }

        /// <summary>
        /// Tổng số lượng container của vận đơn nhánh
        /// </summary>
        [XmlElement("numberOfCont")]
        public string NumberOfCont { get; set; }

        /// <summary>
        /// Số lượng hàng
        /// </summary>
        [XmlElement("cargoPiece")]
        public string CargoPiece { get; set; }

        /// <summary>
        /// ĐVT số lượng hàng
        /// </summary>
        [XmlElement("pieceUnitCode")]
        public string PieceUnitCode { get; set; }

        /// <summary>
        /// Tổng trọng lượng hàng
        /// </summary>
        [XmlElement("cargoWeight")]
        public string CargoWeight { get; set; }

        /// <summary>
        /// ĐVT tổng trọng lượng hàng
        /// </summary>
        [XmlElement("weightUnitCode")]
        public string WeightUnitCode { get; set; }

        /// <summary>
        /// Thông tin về container
        /// </summary>
        [XmlElement("TransportEquipments")]
        public BranchDetail_Containers TransportEquipments { get; set; }
    }
}
