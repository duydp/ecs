﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class UnloadingLocation : LocationNameBase
    {
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("arrival")]
        public string Arrival { get; set; }
    }
}
