﻿namespace Company.Interface
{
    partial class FrmQuerySQLExtend
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmQuerySQLExtend));
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tabQuery = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.grtop = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNgayDangKy = new System.Windows.Forms.DateTimePicker();
            this.txtNgayTiepNhan = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtReferenceid = new System.Windows.Forms.TextBox();
            this.txtTT_XuLy = new System.Windows.Forms.TextBox();
            this.txtSoToKhai = new System.Windows.Forms.TextBox();
            this.txtSTN = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbtnSoTK = new System.Windows.Forms.RadioButton();
            this.rbtnSoTN = new System.Windows.Forms.RadioButton();
            this.rbtnID = new System.Windows.Forms.RadioButton();
            this.comboBoxTable = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grdResult = new System.Windows.Forms.DataGridView();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.tabControlKetQua = new System.Windows.Forms.TabControl();
            this.tabPageGrid = new System.Windows.Forms.TabPage();
            this.tabPageMessage = new System.Windows.Forms.TabPage();
            this.txtMessage = new System.Windows.Forms.RichTextBox();
            this.btnClose = new System.Windows.Forms.Button();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnCmd_Delete = new System.Windows.Forms.Button();
            this.btnCmd_Insert = new System.Windows.Forms.Button();
            this.bntCmd_Update = new System.Windows.Forms.Button();
            this.btnCmd_Select = new System.Windows.Forms.Button();
            this.btnQuery = new System.Windows.Forms.Button();
            this.btnTabDelete = new System.Windows.Forms.Button();
            this.btnTabNew = new System.Windows.Forms.Button();
            this.btnSelect = new System.Windows.Forms.Button();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.groupBox3.SuspendLayout();
            this.tabQuery.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grtop.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdResult)).BeginInit();
            this.tabControlKetQua.SuspendLayout();
            this.tabPageGrid.SuspendLayout();
            this.tabPageMessage.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tabQuery);
            this.groupBox3.Controls.Add(this.panel1);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox3.Location = new System.Drawing.Point(0, 118);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(885, 229);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            // 
            // tabQuery
            // 
            this.tabQuery.Controls.Add(this.tabPage1);
            this.tabQuery.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabQuery.Location = new System.Drawing.Point(3, 45);
            this.tabQuery.Name = "tabQuery";
            this.tabQuery.SelectedIndex = 0;
            this.tabQuery.Size = new System.Drawing.Size(879, 181);
            this.tabQuery.TabIndex = 1;
            this.tabQuery.KeyUp += new System.Windows.Forms.KeyEventHandler(this.tabQuery_KeyUp);
            // 
            // tabPage1
            // 
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(871, 155);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Cmd_SQL 1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnQuery);
            this.panel1.Controls.Add(this.btnCmd_Select);
            this.panel1.Controls.Add(this.bntCmd_Update);
            this.panel1.Controls.Add(this.btnCmd_Insert);
            this.panel1.Controls.Add(this.btnTabNew);
            this.panel1.Controls.Add(this.btnTabDelete);
            this.panel1.Controls.Add(this.btnCmd_Delete);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(879, 29);
            this.panel1.TabIndex = 0;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "exit.png");
            this.imageList1.Images.SetKeyName(1, "bt_play.png");
            this.imageList1.Images.SetKeyName(2, "erase.png");
            // 
            // grtop
            // 
            this.grtop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.grtop.Controls.Add(this.groupBox2);
            this.grtop.Controls.Add(this.groupBox1);
            this.grtop.Dock = System.Windows.Forms.DockStyle.Top;
            this.grtop.Location = new System.Drawing.Point(0, 0);
            this.grtop.Name = "grtop";
            this.grtop.Size = new System.Drawing.Size(885, 118);
            this.grtop.TabIndex = 0;
            this.grtop.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnUpdate);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtNgayDangKy);
            this.groupBox2.Controls.Add(this.txtNgayTiepNhan);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.txtReferenceid);
            this.groupBox2.Controls.Add(this.txtTT_XuLy);
            this.groupBox2.Controls.Add(this.txtSoToKhai);
            this.groupBox2.Controls.Add(this.txtSTN);
            this.groupBox2.Location = new System.Drawing.Point(215, 11);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(601, 104);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Cập nhật";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(143, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 13);
            this.label2.TabIndex = 25;
            this.label2.Text = "Ngày đăng ký:";
            // 
            // txtNgayDangKy
            // 
            this.txtNgayDangKy.CustomFormat = "yyyy/MM/dd";
            this.txtNgayDangKy.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtNgayDangKy.Location = new System.Drawing.Point(228, 43);
            this.txtNgayDangKy.Name = "txtNgayDangKy";
            this.txtNgayDangKy.Size = new System.Drawing.Size(93, 20);
            this.txtNgayDangKy.TabIndex = 4;
            this.txtNgayDangKy.Value = new System.DateTime(2012, 7, 31, 12, 53, 7, 0);
            // 
            // txtNgayTiepNhan
            // 
            this.txtNgayTiepNhan.CustomFormat = "yyyy/MM/dd";
            this.txtNgayTiepNhan.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.txtNgayTiepNhan.Location = new System.Drawing.Point(228, 18);
            this.txtNgayTiepNhan.Name = "txtNgayTiepNhan";
            this.txtNgayTiepNhan.Size = new System.Drawing.Size(93, 20);
            this.txtNgayTiepNhan.TabIndex = 1;
            this.txtNgayTiepNhan.Value = new System.DateTime(2012, 7, 31, 12, 53, 7, 0);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(326, 46);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(27, 13);
            this.label12.TabIndex = 22;
            this.label12.Text = "Ref:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(328, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(82, 13);
            this.label13.TabIndex = 21;
            this.label13.Text = "Trạng thái xữ lý:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 46);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Số tờ khai:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(70, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "Số tiếp nhận:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(143, 21);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "Ngày tiếp nhận:";
            // 
            // txtReferenceid
            // 
            this.txtReferenceid.Location = new System.Drawing.Point(357, 43);
            this.txtReferenceid.Name = "txtReferenceid";
            this.txtReferenceid.Size = new System.Drawing.Size(238, 20);
            this.txtReferenceid.TabIndex = 5;
            this.txtReferenceid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtTT_XuLy
            // 
            this.txtTT_XuLy.Location = new System.Drawing.Point(421, 16);
            this.txtTT_XuLy.Name = "txtTT_XuLy";
            this.txtTT_XuLy.Size = new System.Drawing.Size(46, 20);
            this.txtTT_XuLy.TabIndex = 2;
            this.txtTT_XuLy.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Location = new System.Drawing.Point(78, 43);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(60, 20);
            this.txtSoToKhai.TabIndex = 3;
            this.txtSoToKhai.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSTN
            // 
            this.txtSTN.Location = new System.Drawing.Point(78, 19);
            this.txtSTN.Name = "txtSTN";
            this.txtSTN.Size = new System.Drawing.Size(60, 20);
            this.txtSTN.TabIndex = 0;
            this.txtSTN.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnSelect);
            this.groupBox1.Controls.Add(this.rbtnSoTK);
            this.groupBox1.Controls.Add(this.rbtnSoTN);
            this.groupBox1.Controls.Add(this.rbtnID);
            this.groupBox1.Controls.Add(this.comboBoxTable);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtID);
            this.groupBox1.Location = new System.Drawing.Point(6, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(203, 103);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tìm";
            // 
            // rbtnSoTK
            // 
            this.rbtnSoTK.AutoSize = true;
            this.rbtnSoTK.Location = new System.Drawing.Point(129, 51);
            this.rbtnSoTK.Name = "rbtnSoTK";
            this.rbtnSoTK.Size = new System.Drawing.Size(55, 17);
            this.rbtnSoTK.TabIndex = 3;
            this.rbtnSoTK.Text = "Số TK";
            this.rbtnSoTK.UseVisualStyleBackColor = true;
            // 
            // rbtnSoTN
            // 
            this.rbtnSoTN.AutoSize = true;
            this.rbtnSoTN.Location = new System.Drawing.Point(67, 51);
            this.rbtnSoTN.Name = "rbtnSoTN";
            this.rbtnSoTN.Size = new System.Drawing.Size(56, 17);
            this.rbtnSoTN.TabIndex = 2;
            this.rbtnSoTN.Text = "Số TN";
            this.rbtnSoTN.UseVisualStyleBackColor = true;
            // 
            // rbtnID
            // 
            this.rbtnID.AutoSize = true;
            this.rbtnID.Checked = true;
            this.rbtnID.Location = new System.Drawing.Point(17, 51);
            this.rbtnID.Name = "rbtnID";
            this.rbtnID.Size = new System.Drawing.Size(36, 17);
            this.rbtnID.TabIndex = 1;
            this.rbtnID.TabStop = true;
            this.rbtnID.Text = "ID";
            this.rbtnID.UseVisualStyleBackColor = true;
            // 
            // comboBoxTable
            // 
            this.comboBoxTable.FormattingEnabled = true;
            this.comboBoxTable.Location = new System.Drawing.Point(44, 19);
            this.comboBoxTable.Name = "comboBoxTable";
            this.comboBoxTable.Size = new System.Drawing.Size(146, 21);
            this.comboBoxTable.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Table";
            // 
            // txtID
            // 
            this.txtID.Location = new System.Drawing.Point(11, 74);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(112, 20);
            this.txtID.TabIndex = 4;
            this.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtID.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            this.txtID.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtID_KeyUp);
            // 
            // editBox1
            // 
            this.editBox1.Location = new System.Drawing.Point(6, 40);
            this.editBox1.Multiline = true;
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(561, 109);
            this.editBox1.TabIndex = 0;
            // 
            // grdResult
            // 
            this.grdResult.AllowUserToAddRows = false;
            this.grdResult.AllowUserToDeleteRows = false;
            this.grdResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdResult.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdResult.Location = new System.Drawing.Point(3, 3);
            this.grdResult.Name = "grdResult";
            this.grdResult.Size = new System.Drawing.Size(871, 116);
            this.grdResult.TabIndex = 0;
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.AutoScroll = true;
            this.uiTabPage4.Key = "tabPageGrid";
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(571, 215);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Kết quả";
            // 
            // tabControlKetQua
            // 
            this.tabControlKetQua.Controls.Add(this.tabPageGrid);
            this.tabControlKetQua.Controls.Add(this.tabPageMessage);
            this.tabControlKetQua.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlKetQua.Location = new System.Drawing.Point(0, 357);
            this.tabControlKetQua.Name = "tabControlKetQua";
            this.tabControlKetQua.SelectedIndex = 0;
            this.tabControlKetQua.Size = new System.Drawing.Size(885, 148);
            this.tabControlKetQua.TabIndex = 3;
            // 
            // tabPageGrid
            // 
            this.tabPageGrid.Controls.Add(this.grdResult);
            this.tabPageGrid.Location = new System.Drawing.Point(4, 22);
            this.tabPageGrid.Name = "tabPageGrid";
            this.tabPageGrid.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGrid.Size = new System.Drawing.Size(877, 122);
            this.tabPageGrid.TabIndex = 0;
            this.tabPageGrid.Text = "Kết quả";
            this.tabPageGrid.UseVisualStyleBackColor = true;
            // 
            // tabPageMessage
            // 
            this.tabPageMessage.Controls.Add(this.txtMessage);
            this.tabPageMessage.Location = new System.Drawing.Point(4, 22);
            this.tabPageMessage.Name = "tabPageMessage";
            this.tabPageMessage.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageMessage.Size = new System.Drawing.Size(1302, 122);
            this.tabPageMessage.TabIndex = 1;
            this.tabPageMessage.Text = "Thông báo";
            this.tabPageMessage.UseVisualStyleBackColor = true;
            // 
            // txtMessage
            // 
            this.txtMessage.BackColor = System.Drawing.Color.White;
            this.txtMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMessage.Location = new System.Drawing.Point(3, 3);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ReadOnly = true;
            this.txtMessage.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtMessage.Size = new System.Drawing.Size(1276, 113);
            this.txtMessage.TabIndex = 1;
            this.txtMessage.Text = "";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.ImageIndex = 0;
            this.btnClose.ImageList = this.imageList1;
            this.btnClose.Location = new System.Drawing.Point(783, 5);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Đóng";
            this.btnClose.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // splitter1
            // 
            this.splitter1.BackColor = System.Drawing.SystemColors.Highlight;
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 347);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(885, 10);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 505);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(885, 33);
            this.panel2.TabIndex = 4;
            // 
            // btnCmd_Delete
            // 
            this.btnCmd_Delete.AutoSize = true;
            this.btnCmd_Delete.Location = new System.Drawing.Point(257, 3);
            this.btnCmd_Delete.Name = "btnCmd_Delete";
            this.btnCmd_Delete.Size = new System.Drawing.Size(25, 23);
            this.btnCmd_Delete.TabIndex = 6;
            this.btnCmd_Delete.Text = "D";
            this.btnCmd_Delete.UseVisualStyleBackColor = true;
            this.btnCmd_Delete.Click += new System.EventHandler(this.btnCmd_Delete_Click);
            // 
            // btnCmd_Insert
            // 
            this.btnCmd_Insert.AutoSize = true;
            this.btnCmd_Insert.Location = new System.Drawing.Point(224, 3);
            this.btnCmd_Insert.Name = "btnCmd_Insert";
            this.btnCmd_Insert.Size = new System.Drawing.Size(25, 23);
            this.btnCmd_Insert.TabIndex = 5;
            this.btnCmd_Insert.Text = "I";
            this.btnCmd_Insert.UseVisualStyleBackColor = true;
            this.btnCmd_Insert.Click += new System.EventHandler(this.btnCmd_Insert_Click);
            // 
            // bntCmd_Update
            // 
            this.bntCmd_Update.AutoSize = true;
            this.bntCmd_Update.Location = new System.Drawing.Point(191, 3);
            this.bntCmd_Update.Name = "bntCmd_Update";
            this.bntCmd_Update.Size = new System.Drawing.Size(25, 23);
            this.bntCmd_Update.TabIndex = 4;
            this.bntCmd_Update.Text = "U";
            this.bntCmd_Update.UseVisualStyleBackColor = true;
            this.bntCmd_Update.Click += new System.EventHandler(this.bntCmd_Update_Click);
            // 
            // btnCmd_Select
            // 
            this.btnCmd_Select.AutoSize = true;
            this.btnCmd_Select.Location = new System.Drawing.Point(158, 3);
            this.btnCmd_Select.Name = "btnCmd_Select";
            this.btnCmd_Select.Size = new System.Drawing.Size(25, 23);
            this.btnCmd_Select.TabIndex = 3;
            this.btnCmd_Select.Text = "S";
            this.btnCmd_Select.UseVisualStyleBackColor = true;
            this.btnCmd_Select.Click += new System.EventHandler(this.btnCmd_Select_Click);
            // 
            // btnQuery
            // 
            this.btnQuery.AutoSize = true;
            this.btnQuery.Location = new System.Drawing.Point(87, 3);
            this.btnQuery.Name = "btnQuery";
            this.btnQuery.Size = new System.Drawing.Size(65, 23);
            this.btnQuery.TabIndex = 2;
            this.btnQuery.Text = "Thực hiện";
            this.btnQuery.UseVisualStyleBackColor = true;
            this.btnQuery.Click += new System.EventHandler(this.btnQuery_Click);
            // 
            // btnTabDelete
            // 
            this.btnTabDelete.AutoSize = true;
            this.btnTabDelete.Location = new System.Drawing.Point(42, 3);
            this.btnTabDelete.Name = "btnTabDelete";
            this.btnTabDelete.Size = new System.Drawing.Size(27, 23);
            this.btnTabDelete.TabIndex = 1;
            this.btnTabDelete.Text = "T-";
            this.btnTabDelete.UseVisualStyleBackColor = true;
            this.btnTabDelete.Click += new System.EventHandler(this.btnTabDelete_Click);
            // 
            // btnTabNew
            // 
            this.btnTabNew.AutoSize = true;
            this.btnTabNew.Location = new System.Drawing.Point(4, 3);
            this.btnTabNew.Name = "btnTabNew";
            this.btnTabNew.Size = new System.Drawing.Size(30, 23);
            this.btnTabNew.TabIndex = 0;
            this.btnTabNew.Text = "T+";
            this.btnTabNew.UseVisualStyleBackColor = true;
            this.btnTabNew.Click += new System.EventHandler(this.btnTabNew_Click);
            // 
            // btnSelect
            // 
            this.btnSelect.AutoSize = true;
            this.btnSelect.ImageIndex = 1;
            this.btnSelect.ImageList = this.imageList1;
            this.btnSelect.Location = new System.Drawing.Point(132, 72);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(65, 23);
            this.btnSelect.TabIndex = 5;
            this.btnSelect.Text = "Tìm";
            this.btnSelect.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnSelect.UseVisualStyleBackColor = true;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // btnUpdate
            // 
            this.btnUpdate.AutoSize = true;
            this.btnUpdate.ImageList = this.imageList1;
            this.btnUpdate.Location = new System.Drawing.Point(9, 72);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(65, 23);
            this.btnUpdate.TabIndex = 6;
            this.btnUpdate.Text = "Cập nhật";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // FrmQuerySQLExtend
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(885, 538);
            this.Controls.Add(this.tabControlKetQua);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.grtop);
            this.Name = "FrmQuerySQLExtend";
            this.Text = "Thực hiện truy vấn SQL";
            this.Load += new System.EventHandler(this.FrmQuerySQL_Load);
            this.groupBox3.ResumeLayout(false);
            this.tabQuery.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.grtop.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdResult)).EndInit();
            this.tabControlKetQua.ResumeLayout(false);
            this.tabPageGrid.ResumeLayout(false);
            this.tabPageMessage.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.DataGridView grdResult;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private System.Windows.Forms.ComboBox comboBoxTable;
        private System.Windows.Forms.TextBox txtSTN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtReferenceid;
        private System.Windows.Forms.TextBox txtTT_XuLy;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DateTimePicker txtNgayTiepNhan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker txtNgayDangKy;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton rbtnSoTK;
        private System.Windows.Forms.RadioButton rbtnSoTN;
        private System.Windows.Forms.RadioButton rbtnID;
        private System.Windows.Forms.GroupBox grtop;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSoToKhai;
        private System.Windows.Forms.TabControl tabQuery;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabControl tabControlKetQua;
        private System.Windows.Forms.TabPage tabPageGrid;
        private System.Windows.Forms.TabPage tabPageMessage;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.RichTextBox txtMessage;
        private System.Windows.Forms.Button bntCmd_Update;
        private System.Windows.Forms.Button btnCmd_Insert;
        private System.Windows.Forms.Button btnCmd_Delete;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnQuery;
        private System.Windows.Forms.Button btnCmd_Select;
        private System.Windows.Forms.Button btnTabNew;
        private System.Windows.Forms.Button btnTabDelete;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnSelect;
    }
}