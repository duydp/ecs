﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Data.Common;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.GC.BLL.KDT.GC
{
    public partial class SanPhanCungUng
    {
        private NguyenPhuLieuCungUngCollection _NPLCUCollection = new NguyenPhuLieuCungUngCollection();

        public NguyenPhuLieuCungUngCollection NPLCungUngCollection
        {
            set { _NPLCUCollection = value; }
            get { return _NPLCUCollection; }
        }

        public void LoadNPLCungUngCollection()
        {
            NguyenPhuLieuCungUng npl = new NguyenPhuLieuCungUng();
            npl.Master_ID = this.ID;
            this._NPLCUCollection = npl.SelectCollectionBy_Master_ID();
        }
        public void LoadNPLCungUngCollection(string database)
        {
            NguyenPhuLieuCungUng npl = new NguyenPhuLieuCungUng();
            npl.SetDabaseMoi(database);
            npl.Master_ID = this.ID;
            this._NPLCUCollection = npl.SelectCollectionBy_Master_ID();
        }
        public void LoadNPLCungUngCollection(SqlTransaction transaction)
        {
            NguyenPhuLieuCungUng npl = new NguyenPhuLieuCungUng();
            npl.Master_ID = this.ID;
            this._NPLCUCollection = npl.SelectCollectionBy_Master_ID(transaction);
        }
        public void LoadNPLCungUngCollection(SqlTransaction transaction, string dbName)
        {
            NguyenPhuLieuCungUng npl = new NguyenPhuLieuCungUng();
            npl.Master_ID = this.ID;
            this._NPLCUCollection = npl.SelectCollectionBy_Master_ID(transaction, dbName);
        }
        public SanPhanCungUngCollection SelectCollectionBy_Master_ID(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_SPLuongCungUng_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            SanPhanCungUngCollection collection = new SanPhanCungUngCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand, transaction);
            while (reader.Read())
            {
                SanPhanCungUng entity = new SanPhanCungUng();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCUSanPham"))) entity.LuongCUSanPham = reader.GetDecimal(reader.GetOrdinal("LuongCUSanPham"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public SanPhanCungUngCollection SelectCollectionBy_Master_ID(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_SPLuongCungUng_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            SanPhanCungUngCollection collection = new SanPhanCungUngCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                SanPhanCungUng entity = new SanPhanCungUng();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCUSanPham"))) entity.LuongCUSanPham = reader.GetDecimal(reader.GetOrdinal("LuongCUSanPham"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public void Delete(HopDong HD)
        {
            if (this.NPLCungUngCollection.Count == 0)
            {
                this.LoadNPLCungUngCollection();
            }
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    foreach (NguyenPhuLieuCungUng nplCungUng in this.NPLCungUngCollection)
                    {
                        if (nplCungUng.ID > 0)
                            nplCungUng.Delete(HD, transaction, this);
                    }
                    this.DeleteTransaction(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public void Delete(HopDong HD, SqlTransaction transaction)
        {
            foreach (NguyenPhuLieuCungUng nplCungUng in this.NPLCungUngCollection)
            {
                nplCungUng.Delete(HD, transaction, this);
            }
            this.DeleteTransaction(transaction);
        }

        public long InsertTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_SPLuongCungUng_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@LuongCUSanPham", SqlDbType.Decimal, this._LuongCUSanPham);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        public int UpdateTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_SPLuongCungUng_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@LuongCUSanPham", SqlDbType.Decimal, this._LuongCUSanPham);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_SPLuongCungUng_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);
            this.db.AddInParameter(dbCommand, "@LuongCUSanPham", SqlDbType.Decimal, this._LuongCUSanPham);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public bool Load(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_SPLuongCungUng_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, this._MaSanPham);

            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) this._Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) this._MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCUSanPham"))) this._LuongCUSanPham = reader.GetDecimal(reader.GetOrdinal("LuongCUSanPham"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
    }
}
