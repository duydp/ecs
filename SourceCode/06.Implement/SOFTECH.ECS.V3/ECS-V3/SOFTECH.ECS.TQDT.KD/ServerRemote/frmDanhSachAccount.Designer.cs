﻿namespace ServerRemote
{
    partial class frmDanhSachAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDanhSachAccount));
            this.listView1 = new System.Windows.Forms.ListView();
            this.HO_TEN = new System.Windows.Forms.ColumnHeader();
            this.USER_NAME = new System.Windows.Forms.ColumnHeader();
            this.MO_TA = new System.Windows.Forms.ColumnHeader();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tlsDoiMatKhau = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.HO_TEN,
            this.USER_NAME,
            this.MO_TA});
            this.listView1.ContextMenuStrip = this.contextMenuStrip1;
            this.listView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listView1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listView1.FullRowSelect = true;
            this.listView1.GridLines = true;
            this.listView1.Location = new System.Drawing.Point(0, 0);
            this.listView1.MultiSelect = false;
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(1037, 438);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // HO_TEN
            // 
            this.HO_TEN.Tag = "";
            this.HO_TEN.Text = "Tên Đại Lý";
            this.HO_TEN.Width = 72;
            // 
            // USER_NAME
            // 
            this.USER_NAME.Tag = "";
            this.USER_NAME.Text = "Account";
            this.USER_NAME.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.USER_NAME.Width = 93;
            // 
            // MO_TA
            // 
            this.MO_TA.Tag = "";
            this.MO_TA.Text = "Mô tả đại lý";
            this.MO_TA.Width = 523;
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tlsDoiMatKhau});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(158, 26);
            // 
            // tlsDoiMatKhau
            // 
            this.tlsDoiMatKhau.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.tlsDoiMatKhau.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlsDoiMatKhau.ForeColor = System.Drawing.Color.White;
            this.tlsDoiMatKhau.Name = "tlsDoiMatKhau";
            this.tlsDoiMatKhau.Size = new System.Drawing.Size(157, 22);
            this.tlsDoiMatKhau.Text = "Đổi mật khẩu";
            this.tlsDoiMatKhau.Click += new System.EventHandler(this.tlsDoiMatKhau_Click);
            // 
            // frmDanhSachAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1037, 438);
            this.Controls.Add(this.listView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmDanhSachAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Danh sách account đại lý đang khai báo cho doanh nghiệp";
            this.Load += new System.EventHandler(this.frmDanhSachAccount_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.ColumnHeader HO_TEN;
        private System.Windows.Forms.ColumnHeader USER_NAME;
        private System.Windows.Forms.ColumnHeader MO_TA;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tlsDoiMatKhau;
    }
}