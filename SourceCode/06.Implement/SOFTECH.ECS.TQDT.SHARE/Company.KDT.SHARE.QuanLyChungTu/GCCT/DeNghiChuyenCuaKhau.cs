﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Nodes.GC;
using Company.KDT.SHARE.Components.Utils;

namespace Company.KDT.SHARE.QuanLyChungTu.GCCT
{
    public partial class DeNghiChuyenCuaKhau
    {
        public static XmlNode ConvertCollectionDeNghiChuyenCuaKhauToXML(XmlDocument doc, List<DeNghiChuyenCuaKhau> ChuyenCuaKhauCollection, long TKMD_ID)
        {
            if (ChuyenCuaKhauCollection == null || ChuyenCuaKhauCollection.Count == 0)
            {
                ChuyenCuaKhauCollection = (List<DeNghiChuyenCuaKhau>)DeNghiChuyenCuaKhau.SelectCollectionBy_ID_TK(TKMD_ID);
            }
            XmlElement CHUNG_TU_DENGHICHUYENCK = doc.CreateElement("CHUNG_TU_DENGHICHUYENCK");
            foreach (DeNghiChuyenCuaKhau deNghichuyen in ChuyenCuaKhauCollection)
            {
                XmlElement CHUNG_TU_DENGHICHUYENCKItem = doc.CreateElement("CHUNG_TU_DENGHICHUYENCK.ITEM");
                CHUNG_TU_DENGHICHUYENCK.AppendChild(CHUNG_TU_DENGHICHUYENCKItem);

                XmlElement DoanhNghiep = doc.CreateElement("DoanhNghiep");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(DoanhNghiep);

                XmlAttribute MaDN = doc.CreateAttribute("MaDN");
                MaDN.Value = deNghichuyen.MaDoanhNghiep;
                DoanhNghiep.Attributes.Append(MaDN);

                XmlAttribute TenDN = doc.CreateAttribute("TenDN");
                TenDN.Value = FontConverter.Unicode2TCVN(deNghichuyen.MaDoanhNghiep);
                DoanhNghiep.Attributes.Append(TenDN);

                XmlElement VanDon = doc.CreateElement("VanDon");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(VanDon);

                XmlAttribute SoVanDon = doc.CreateAttribute("SoVanDon");
                SoVanDon.Value = FontConverter.Unicode2TCVN(deNghichuyen.SoVanDon);
                VanDon.Attributes.Append(SoVanDon);

                XmlAttribute NgayVanDon = doc.CreateAttribute("NgayVanDon");
                NgayVanDon.Value = deNghichuyen.NgayVanDon.ToString("yyyy-MM-dd");
                VanDon.Attributes.Append(NgayVanDon);

                XmlElement NoiDung = doc.CreateElement("NoiDung");
                NoiDung.InnerText = FontConverter.Unicode2TCVN(deNghichuyen.ThongTinKhac);
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(NoiDung);

                XmlElement DiaDiemKiemTra = doc.CreateElement("DiaDiemKiemTra");
                DiaDiemKiemTra.InnerText = FontConverter.Unicode2TCVN(deNghichuyen.DiaDiemKiemTra);
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(DiaDiemKiemTra);

                XmlElement ThoiGianDen = doc.CreateElement("ThoiGianDen");
                ThoiGianDen.InnerText = deNghichuyen.ThoiGianDen.ToString("yyyy-MM-dd");
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(ThoiGianDen);

                XmlElement TuyenDuong = doc.CreateElement("TuyenDuong");
                TuyenDuong.InnerText = FontConverter.Unicode2TCVN(deNghichuyen.TuyenDuong);
                CHUNG_TU_DENGHICHUYENCKItem.AppendChild(TuyenDuong);

            }
            if (CHUNG_TU_DENGHICHUYENCK.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_DENGHICHUYENCK;

        }

        public string WSSend(string password, string maHQ, long soTK, string maLH, int namDK, long TKMD_ID)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\KhaiBoSung.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            if (this.GuidStr == "")
            {
                this.GuidStr = (System.Guid.NewGuid().ToString()); ;

                this.Update();
            }
            nodeReference.InnerText = this.GuidStr;
            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;


            XmlNode nodeTO_KHAI = doc.GetElementsByTagName("TO_KHAI")[0];
            nodeTO_KHAI.Attributes["MA_LH"].Value = maLH;
            nodeTO_KHAI.Attributes["MA_HQ"].Value = maHQ;
            nodeTO_KHAI.Attributes["SOTK"].Value = soTK + "";
            nodeTO_KHAI.Attributes["NAMDK"].Value = namDK + "";


            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            List<DeNghiChuyenCuaKhau> listDeNghiChuyenCuaKhau = new List<DeNghiChuyenCuaKhau>();
            listDeNghiChuyenCuaKhau.Add(this);
            nodeDulieu.AppendChild(DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(doc, listDeNghiChuyenCuaKhau, TKMD_ID));


            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        public bool WSKhaiBaoChuyenCK(string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long TKMD_ID, string maDoanhNghiep, List<DeNghiChuyenCuaKhau> list, MessageTypes messageType, MessageFunctions messageFunction)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep;
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep;

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan;
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan;

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];
            GuidStr = nodeReference.InnerText = System.Guid.NewGuid().ToString();
            this.Update();
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep;
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep;

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan;
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan;

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";

            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<DeNghiChuyenCuaKhau> listDeNghiChuyenCuaKhau = new List<DeNghiChuyenCuaKhau>();
            listDeNghiChuyenCuaKhau.Add(this);
            xmlNodeDulieu.AppendChild(DeNghiChuyenCuaKhau.ConvertCollectionDeNghiChuyenCuaKhauToXML(xmlDocument, listDeNghiChuyenCuaKhau, TKMD_ID));

            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                // Lưu message.
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ToKhaiNhap;
                message.MessageFunction = MessageFunctions.KhaiBao;
                message.MessageFrom = maDoanhNghiep;
                message.MessageTo = maHaiQuan.Trim();
                message.MessageContent = xmlDocument.InnerXml;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungDeNghiChuyenCK;
                message.NoiDungThongBao = string.Empty;
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Khai báo.
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    string noiDung = FontConverter.TCVN2Unicode(docResult.GetElementsByTagName("Root")[0].InnerText.Trim());
                    XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);

                    // Lưu message.
                    Message message = new Message();
                    message.ItemID = this.ID;
                    message.ReferenceID = new Guid(this.GuidStr);
                    message.MessageType = MessageTypes.ThongTin;
                    message.MessageFunction = MessageFunctions.LayPhanHoi;
                    message.MessageFrom = maHaiQuan.Trim();
                    message.MessageTo = maDoanhNghiep;
                    message.MessageContent = xmlDocument.InnerXml;
                    message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungDeNghiChuyenCK;
                    message.NoiDungThongBao = noiDung;// string.Empty;
                    message.CreatedTime = DateTime.Now;
                    message.Insert();

                    return true;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return false;

        }

        public bool WSLaySoTiepNhan(string password)
        {
            KDTService kdt = WebService.GetWS();

            //BaseClass.LoadConfigure("WS_URL");
            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungDeNghiChuyenCKThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }

            return false;
        }

        public bool WSLayPhanHoi(string password)
        {
            KDTService kdt = WebService.GetWS();

            // Đọc lại thông tin trả về từ Hải quan.
            string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            XmlDocument xmlDocumentRequest = new XmlDocument();
            xmlDocumentRequest.LoadXml(xmlRequest);

            string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocumentRequest.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.TuChoiTiepNhan;
                string lydo = FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), lydo);
                message.CreatedTime = DateTime.Now;
                message.Insert();

                // Cập lại số tiếp nhận
                this.SoTiepNhan = 0;
                this.NgayTiepNhan = new DateTime(1900, 1, 1);
                this.Update();

                return true;
            }
            else if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungDeNghiChuyenCKThanhCong;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();
                return true;
            }
            else if (xmlNodeResult.Attributes["SOTN"] != null)
            {
                Message message = new Message();
                message.ItemID = this.ID;
                message.ReferenceID = new Guid(this.GuidStr);
                message.MessageType = MessageTypes.ThongTin;
                message.MessageFunction = MessageFunctions.LayPhanHoi;
                message.MessageFrom = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText.Trim();
                message.MessageTo = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText.Trim();
                message.MessageContent = kq;
                message.TieuDeThongBao = MessageTitle.KhaiBaoBoSungDeNghiChuyenCKDuocChapNhan;
                message.NoiDungThongBao = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã được Hải quan chấp nhận.", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString());
                message.CreatedTime = DateTime.Now;
                message.Insert();

                return true;
            }

            return false;
        }

        public static XmlNode ConvertCollectionDeNghiChuyenCKToXML(XmlDocument doc, List<DeNghiChuyenCuaKhau> ChuyenCuaKhauCollection, long TKMD_ID)
        {
            if (ChuyenCuaKhauCollection == null || ChuyenCuaKhauCollection.Count == 0)
            {
                ChuyenCuaKhauCollection = (List<DeNghiChuyenCuaKhau>)DeNghiChuyenCuaKhau.SelectCollectionBy_ID_TK(TKMD_ID);
            }
            XmlElement CustomsOfficeChangedRequests = doc.CreateElement(NodeDeNghiChuyenCK.CustomsOfficeChangedRequests);
            foreach (DeNghiChuyenCuaKhau deNghichuyen in ChuyenCuaKhauCollection)
            {
                XmlElement CustomsOfficeChangedRequest = doc.CreateElement(NodeDeNghiChuyenCK.CustomsOfficeChangedRequest);
                CustomsOfficeChangedRequests.AppendChild(CustomsOfficeChangedRequest);
                //
                XmlElement AdditionalDocument = doc.CreateElement(NodeDeNghiChuyenCK.AdditionalDocument);
                CustomsOfficeChangedRequest.AppendChild(AdditionalDocument);
                XmlElement reference = doc.CreateElement(NodeDeNghiChuyenCK.reference);
                AdditionalDocument.AppendChild(reference);
                reference.InnerText = deNghichuyen.SoVanDon;
                XmlElement issue = doc.CreateElement(NodeDeNghiChuyenCK.issue);
                AdditionalDocument.AppendChild(issue);
                issue.InnerText = deNghichuyen.NgayVanDon.ToString(NgayThang.yyyyMMdd);
                //
                XmlElement AdditionalInformation = doc.CreateElement(NodeDeNghiChuyenCK.AdditionalInformation);
                CustomsOfficeChangedRequest.AppendChild(AdditionalInformation);
                XmlElement content = doc.CreateElement(NodeDeNghiChuyenCK.content);
                AdditionalInformation.AppendChild(content);
                content.InnerText = deNghichuyen.ThongTinKhac;
                XmlElement examinationPlace = doc.CreateElement(NodeDeNghiChuyenCK.examinationPlace);
                AdditionalInformation.AppendChild(examinationPlace);
                examinationPlace.InnerText = deNghichuyen.DiaDiemKiemTra;
                XmlElement time = doc.CreateElement(NodeDeNghiChuyenCK.time);
                AdditionalInformation.AppendChild(time);
                time.InnerText = deNghichuyen.ThoiGianDen.ToString(NgayThang.yyyyMMdd);
                XmlElement route = doc.CreateElement(NodeDeNghiChuyenCK.route);
                AdditionalInformation.AppendChild(route);
                route.InnerText = deNghichuyen.TuyenDuong;
            }
            if (CustomsOfficeChangedRequests.ChildNodes.Count == 0)
                return null;
            return CustomsOfficeChangedRequests;

        }


    }
}