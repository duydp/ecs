﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.BLL.DuLieuChuan;
using Company.Interface.DanhMucChuan;
using System.Windows.Forms;
using Company.BLL;
using Company.GC.BLL.DuLieuChuan;
using System.Data;
using System.IO;
using Company.KDT.SHARE.Components;
using System.Xml;
using Logger;
using Company.KDT.SHARE.Components.Utils;

namespace Company.Interface
{
    public class Globals
    {
        #region MESSAGE
        private static Company.Controls.KDTMessageBoxControl _KDTMsgBox;
        private static Company.Controls.MessageBoxControl _MsgBox;

        public static string ShowMessageTQDT(string messageHQ, string messageContent, bool showYesNoButton)
        {
            _KDTMsgBox = new Company.Controls.KDTMessageBoxControl();
            _KDTMsgBox.ShowYesNoButton = showYesNoButton;
            _KDTMsgBox.HQMessageString = messageHQ;
            _KDTMsgBox.MessageString = messageContent;
            _KDTMsgBox.ShowDialog();
            string st = _KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
        public static string ShowMessageTQDT(string messageContent, bool showYesNoButton)
        {
            string messageHQ = "Thông báo trả về từ hệ thống Hải quan";
            _KDTMsgBox = new Company.Controls.KDTMessageBoxControl();
            _KDTMsgBox.ShowYesNoButton = showYesNoButton;
            _KDTMsgBox.HQMessageString = messageHQ;
            _KDTMsgBox.MessageString = messageContent;
            _KDTMsgBox.ShowDialog();
            string st = _KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
        public static string ShowMessage(string message, bool showYesNoButton)
        {
            _MsgBox = new Company.Controls.MessageBoxControl();
            _MsgBox.ShowYesNoButton = showYesNoButton;
            _MsgBox.MessageString = message;
            _MsgBox.ShowDialog();
            string st = _MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }

        #endregion

        #region DANH MUC DOI TAC
        /// <summary>
        /// Tra ve 1 Object Doi tac
        /// </summary>
        /// <returns></returns>
        public static DoiTac GetMaDonViObject()
        {
            DoiTac dt = new DoiTac();

            try
            {
                DonViDoiTacForm f = new DonViDoiTacForm();
                f.isBrower = true;
                f.ShowDialog();

                if (f.doiTac != null && f.doiTac.TenCongTy != "")
                {
                    dt = f.doiTac;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return dt;
        }

        /// <summary>
        /// Tra ve chuoi: ten va dia chi doi tac.
        /// </summary>
        /// <returns></returns>
        public static string GetMaDonViString()
        {
            try
            {
                DonViDoiTacForm f = new DonViDoiTacForm();
                f.isBrower = true;
                f.ShowDialog();
                if (f.doiTac != null && f.doiTac.TenCongTy != "")
                {
                    return f.doiTac.TenCongTy + ". " + f.doiTac.DiaChi;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return "";
        }
        #endregion

        #region VALIDATE

        /// <summary>
        /// Kiểm tra thông tin rỗng.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNull(Company.Interface.Controls.DonViHaiQuanControl control, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";

            err.SetError(control, "");

            if (control.Ma.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(control, msgErr);
                control.Focus();
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin rỗng.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNull(Janus.Windows.GridEX.EditControls.EditBox control, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";

            err.SetError(control, "");

            if (control.Text.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(control, msgErr);
                control.Focus();
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin rỗng.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNull(Janus.Windows.EditControls.UIComboBox control, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";

            err.SetError(control, "");

            if (control.Text.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(control, msgErr);
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra thông tin rỗng.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateNull(Janus.Windows.CalendarCombo.CalendarCombo control, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' không được để trống.";

            err.SetError(control, "");

            if (control.Text.Trim().Length == 0)
            {
                isValid = false;
                err.SetError(control, msgErr);
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra độ dài ký tự cho phép.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateLength(Janus.Windows.EditControls.UIComboBox control, int lengthLimit, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' vượt quá ký tự cho phép, phải <= '" + lengthLimit + "' ký tự.";

            err.SetError(control, "");

            if (control.SelectedValue != null)
                isValid = ValidateLength(control.SelectedValue.ToString().Trim(), lengthLimit);
            else
                isValid = false;

            if (isValid == false)
            {
                err.SetError(control, msgErr);
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra độ dài ký tự cho phép.
        /// </summary>
        /// <param name="txtControl"></param>
        /// <param name="err"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static bool ValidateLength(Janus.Windows.GridEX.EditControls.EditBox control, int lengthLimit, ErrorProvider err, string fieldName)
        {
            bool isValid = true;
            string msgErr = "Thông tin '" + fieldName + "' vượt quá ký tự cho phép, phải <= '" + lengthLimit + "' ký tự.";

            err.SetError(control, "");

            isValid = ValidateLength(control.Text, lengthLimit);

            if (isValid == false)
            {
                err.SetError(control, msgErr);
            }

            return isValid;
        }

        /// <summary>
        /// Kiểm tra độ dài ký tự cho phép.
        /// </summary>
        /// <param name="valueString"></param>
        /// <param name="lengthLimit"></param>
        /// <returns></returns>
        private static bool ValidateLength(string valueString, int lengthLimit)
        {
            return valueString.Length <= lengthLimit;
        }

        #endregion

        #region Ket qua xu ly
        public static void KetQuaXuLy(Company.GC.BLL.KDT.ToKhaiMauDich tkmd)
        {
            KetQuaXuLyForm form = new KetQuaXuLyForm();
            form.ItemID = tkmd.ID;
            form.ShowDialog();
        }
        #endregion

        #region Ket qua xu ly bo sung

        public static void ShowKetQuaXuly(long itemID)
        {
            KetQuaXuLyBoSungForm form = new KetQuaXuLyBoSungForm();
            form.ItemID = itemID;
            form.ShowDialog();
        }
        public static void ShowKetQuaXuLyBoSung(string guidStr)
        {
            KetQuaXuLyBoSungForm form = new KetQuaXuLyBoSungForm();
            form.refId = guidStr;
            form.ShowDialog();
        }
        #endregion

        #region VALUE LIST

        private static System.Data.DataTable dtHS;
        private static System.Data.DataTable dtNguyenTe;
        private static System.Data.DataTable dtDonViTinh;
        private static System.Data.DataTable dtNuoc;

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillHSValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;

                valueList = col.ValueList;

                if (dtHS == null)
                    dtHS = Company.GC.BLL.Utils.MaHS.SelectAll();

                System.Data.DataView view = dtHS.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["HS10So"], (string)row["HS10So"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillNguyenTeValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;
                valueList = col.ValueList;

                if (dtNguyenTe == null)
                    dtNguyenTe = Company.GC.BLL.DuLieuChuan.NguyenTe.SelectAll();

                System.Data.DataView view = dtNguyenTe.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"], (string)row["Ten"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillDonViTinhValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;
                valueList = col.ValueList;

                if (dtDonViTinh == null)
                    dtDonViTinh = Company.GC.BLL.DuLieuChuan.DonViTinh.SelectAll();

                System.Data.DataView view = dtDonViTinh.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"], (string)row["Ten"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        public static Janus.Windows.GridEX.GridEXValueListItemCollection FillNuocXXValueList(Janus.Windows.GridEX.GridEXColumn col)
        {
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = new Janus.Windows.GridEX.GridEXValueListItemCollection();

            try
            {
                col.EditType = Janus.Windows.GridEX.EditType.Combo;
                col.HasValueList = true;
                valueList = col.ValueList;

                if (dtNuoc == null)
                    dtNuoc = Company.GC.BLL.DuLieuChuan.Nuoc.SelectAll();

                System.Data.DataView view = dtNuoc.DefaultView;

                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"], (string)row["Ten"]));
                }
            }
            catch (Exception ex) { }

            return valueList;
        }

        #endregion

        #region File, Folder

        /// <summary>
        /// Đọc file
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns>byte[]</returns>
        public static byte[] ReadFile(string filePath)
        {
            byte[] filedata = new byte[0];

            try
            {
                // provide read access to the file
                FileStream fs = new FileStream(filePath, FileMode.Open, FileAccess.Read);

                // Create a byte array of file stream length
                filedata = new byte[fs.Length];

                //Read block of bytes from stream into the byte array
                fs.Read(filedata, 0, System.Convert.ToInt32(fs.Length));

                //Close the File Stream
                fs.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return filedata;
        }

        /// <summary>
        /// Ghi file
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="data"></param>
        public static bool WriteFile(string filePath, byte[] data)
        {
            try
            {
                // provide read access to the file
                FileStream fs = new FileStream(filePath, FileMode.Create, FileAccess.ReadWrite);

                //Read block of bytes from stream into the byte array
                fs.Write(data, 0, data.Length);

                //Close the File Stream
                fs.Close();

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }

        }
        #endregion

        public static string ConfigPhongBiPhanHoi(int type, int function, string maHaiQuan, string maDoanhNghiep, string GUIDSTR)
        {
            XmlDocument doc = new XmlDocument();
            string path = Company.GC.BLL.EntityBase.GetPathProram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = FontConverter.Unicode2TCVN(Company.GC.BLL.DuLieuChuan.DonViHaiQuan.GetName(maHaiQuan));
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHaiQuan.Trim();

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = GUIDSTR.ToUpper();

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[0].InnerText = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TEN_DON_VI"));
            nodeFrom.ChildNodes[1].InnerText = maDoanhNghiep.Trim();

            return doc.InnerXml;
        }

        //DATLMQ Export Excel Thông báo Thuế 06/08/2011
        public static void ExportExcelThongBaoThue(Company.GC.BLL.KDT.ToKhaiMauDich TKMD, string _soQD, DateTime _ngayQD, DateTime _ngayHH, string _TKKB, string _TenKB, string _chuongThueXNK,
                                                    string _loaiThueXNK, string _khoanThueXNK, string _mucThueXNK, string _tieuMucThueXNK, double _soTienThueXNK, string _chuongThueVAT,
                                                    string _loaiThueVAT, string _khoanThueVAT, string _mucThueVAT, string _tieuMucThueVAT, double _soTienThueVAT, string _chuongThueTTDB,
                                                    string _loaiThueTTDB, string _khoanThueTTDB, string _mucThueTTDB, string _tieuMucThueTTDB, double _soTienThueTTDB, string _chuongThueTVCBPG,
                                                    string _loaiThueTVCBPG, string _khoanThueTVCBPG, string _mucThueTVCBPG, string _tieuMucThueTVCBPG, double _soTienThueTVCBPG)
        {
            //Lấy đường dẫn file gốc
            string destFile = "";
            string fileName = "Thong_Bao_Thue.xls";

            string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

            //Kiem tra file Excel mau co ton tai?
            if (!System.IO.File.Exists(sourcePath))
            {
                ShowMessage("Không tồn tại file mẫu Excel.", false);
                return;
            }

            //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
            try
            {
                Infragistics.Excel.Workbook workBook = Infragistics.Excel.Workbook.Load(sourcePath);
                Infragistics.Excel.Worksheet workSheet = workBook.Worksheets[0];

                //Nội dung xuất Excel
                //1. Tên chi cục Hải quan: A2
                workSheet.GetCell("A2").Value = GlobalSettings.TEN_HAI_QUAN;
                //2. Số QD: D3
                workSheet.GetCell("D3").Value = TKMD.SoQD + "/TBT";
                //3. Người Xuất khẩu/Nhập khẩu: F9
                workSheet.GetCell("F9").Value = TKMD.TenDoanhNghiep;
                //4. Mã số thuế: F10
                workSheet.GetCell("F10").Value = TKMD.MaDoanhNghiep;
                //5. Người khai Hải quan: F11
                workSheet.GetCell("F11").Value = TKMD.TenDoanhNghiep;
                //6. Mã số thuế: F12
                workSheet.GetCell("F12").Value = TKMD.MaDoanhNghiep;
                //7. Số tờ khai: C13
                workSheet.GetCell("C13").Value = TKMD.SoToKhai.ToString();
                //8. Mã loại hình: I13
                workSheet.GetCell("I13").Value = TKMD.MaLoaiHinh;
                //9. Ngày đăng ký: O13
                workSheet.GetCell("O13").Value = TKMD.NgayDangKy.ToShortDateString();
                //10. Số Vận Đơn: D15
                workSheet.GetCell("D15").Value = TKMD.SoVanDon;
                //11. Ngày Vận Đơn: J15
                workSheet.GetCell("J15").Value = TKMD.NgayVanDon.ToShortDateString();
                //12. Số Hóa Đơn Thương Mại: F16
                workSheet.GetCell("F16").Value = TKMD.SoHoaDonThuongMai;
                //13. Ngày Hóa Đơn Thương Mại: J16
                workSheet.GetCell("J16").Value = TKMD.NgayHoaDonThuongMai.ToShortDateString();
                //14. Kết quả xử lý: E18
                string tenLuong = "";
                if (TKMD.PhanLuong == "1")
                    tenLuong = "Xanh";
                else if (TKMD.PhanLuong == "2")
                    tenLuong = "Vàng";
                else
                    tenLuong = "Đỏ";
                workSheet.GetCell("E18").Value = "Tờ khai được phân luồng " + tenLuong;
                //15. Chi tiết Thuế
                //Thuế XNK: Chương: F24
                workSheet.GetCell("F24").Value = _chuongThueXNK;
                //Loại: H24
                workSheet.GetCell("H24").Value = _loaiThueXNK;
                //Khoản: J24
                workSheet.GetCell("J24").Value = _khoanThueXNK;
                //Mục: L24
                workSheet.GetCell("L24").Value = _mucThueXNK;
                //Tiểu Mục: N24
                workSheet.GetCell("N24").Value = _tieuMucThueXNK;
                //Số Tiền: P24
                workSheet.GetCell("P24").Value = _soTienThueXNK.ToString("N4");
                //Thuế VAT: Chương: F26
                workSheet.GetCell("F26").Value = _chuongThueVAT;
                //Loại: H24
                workSheet.GetCell("H26").Value = _loaiThueVAT;
                //Khoản: J24
                workSheet.GetCell("J26").Value = _khoanThueVAT;
                //Mục: L24
                workSheet.GetCell("L26").Value = _mucThueVAT;
                //Tiểu Mục: N24
                workSheet.GetCell("N26").Value = _tieuMucThueVAT;
                //Số Tiền: P24
                workSheet.GetCell("P26").Value = _soTienThueVAT.ToString("N4");
                //Thuế TTDB: Chương: F28
                workSheet.GetCell("F28").Value = _chuongThueTTDB;
                //Loại: H24
                workSheet.GetCell("H28").Value = _loaiThueTTDB;
                //Khoản: J24
                workSheet.GetCell("J28").Value = _khoanThueTTDB;
                //Mục: L24
                workSheet.GetCell("L28").Value = _mucThueTTDB;
                //Tiểu Mục: N24
                workSheet.GetCell("N28").Value = _tieuMucThueTTDB;
                //Số Tiền: P24
                workSheet.GetCell("P28").Value = _soTienThueTTDB.ToString("N4");
                //Thuế TVCBPG: Chương: F30
                workSheet.GetCell("F30").Value = _chuongThueTVCBPG;
                //Loại: H24
                workSheet.GetCell("H30").Value = _loaiThueTVCBPG;
                //Khoản: J24
                workSheet.GetCell("J30").Value = _khoanThueTVCBPG;
                //Mục: L24
                workSheet.GetCell("L30").Value = _mucThueTVCBPG;
                //Tiểu Mục: N24
                workSheet.GetCell("N30").Value = _tieuMucThueTVCBPG;
                //Số Tiền: P24
                workSheet.GetCell("P30").Value = _soTienThueTVCBPG.ToString("N4");
                //Tổng số Tiền Thuế: P32
                double tongTienThue = _soTienThueXNK + _soTienThueVAT + _soTienThueTTDB + _soTienThueTVCBPG;
                workSheet.GetCell("P32").Value = tongTienThue.ToString("N4");
                //Bằng chữ: C35
                workSheet.GetCell("C35").Value = Company.GC.BLL.Utils.VNCurrency.ToString(tongTienThue);
                //Thời hạn: D37
                workSheet.GetCell("D37").Value = _ngayHH.Subtract(_ngayQD).TotalDays;
                //Kể từ ngày: H37
                workSheet.GetCell("H37").Value = _ngayQD.ToShortDateString();
                //Tài khoản Kho Bạc: L38
                workSheet.GetCell("L38").Value = _TKKB;
                //Tên Kho Bạc: A39
                workSheet.GetCell("A39").Value = _TenKB;

                //Chọn đường dẫn file cần lưu
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                string dateNow = DateTime.Now.ToShortDateString().Replace("/", ""); // +"_" + DateTime.Now.Hour + "h" + DateTime.Now.Minute + "p" + DateTime.Now.Second + "s";
                saveFileDialog1.FileName = "ThongBaoThue_TK_" + TKMD.SoToKhai + "_" + TKMD.MaLoaiHinh + "_" + GlobalSettings.MA_HAI_QUAN + "_" + TKMD.NamDK + "_" + dateNow;
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    destFile = saveFileDialog1.FileName;
                    //Ghi nội dung file gốc vào file cần lưu
                    try
                    {
                        byte[] sourceFile = Globals.ReadFile(sourcePath);
                        Globals.WriteFile(destFile, sourceFile);
                        workBook.Save(destFile);
                        ShowMessage("Lưu file thành công", false);
                        System.Diagnostics.Process.Start(destFile);
                    }
                    catch (Exception ex)
                    {
                        ShowMessage(ex.Message, false);
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Logger.LocalLogger.Instance().WriteMessage(e);
                ShowMessage("Có lỗi trong quá trình Export thông tin ra file Excel.\nChi tiết lỗi:\n" + e.Message, false);
                return;
            }
        }
    }
}
