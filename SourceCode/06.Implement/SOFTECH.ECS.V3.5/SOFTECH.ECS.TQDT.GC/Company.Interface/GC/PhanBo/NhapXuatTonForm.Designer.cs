﻿namespace Company.Interface.GC.PhanBo
{
    partial class NhapXuatTonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout cbHopDong_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NhapXuatTonForm));
            Janus.Windows.GridEX.GridEXLayout dgListNPLNhap_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListNPLXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dtpDateTo = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.dtpDateFrom = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.cbHopDong = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpNPLTon = new System.Windows.Forms.TabPage();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.mnuExportExcel = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuExport = new System.Windows.Forms.ToolStripMenuItem();
            this.tpNPLNhap = new System.Windows.Forms.TabPage();
            this.dgListNPLNhap = new Janus.Windows.GridEX.GridEX();
            this.tpSPXuat = new System.Windows.Forms.TabPage();
            this.dgListNPLXuat = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpNPLTon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.mnuExportExcel.SuspendLayout();
            this.tpNPLNhap.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLNhap)).BeginInit();
            this.tpSPXuat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLXuat)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.groupBox2);
            this.grbMain.Controls.Add(this.groupBox1);
            this.grbMain.Size = new System.Drawing.Size(1167, 484);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.dtpDateTo);
            this.groupBox1.Controls.Add(this.dtpDateFrom);
            this.groupBox1.Controls.Add(this.cbHopDong);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox1.Location = new System.Drawing.Point(0, 449);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox1.Size = new System.Drawing.Size(1167, 35);
            this.groupBox1.TabIndex = 86;
            this.groupBox1.TabStop = false;
            // 
            // dtpDateTo
            // 
            this.dtpDateTo.Location = new System.Drawing.Point(899, 9);
            this.dtpDateTo.Name = "dtpDateTo";
            this.dtpDateTo.ReadOnly = false;
            this.dtpDateTo.Size = new System.Drawing.Size(106, 21);
            this.dtpDateTo.TabIndex = 87;
            this.dtpDateTo.TagName = "";
            this.dtpDateTo.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpDateTo.WhereCondition = "";
            // 
            // dtpDateFrom
            // 
            this.dtpDateFrom.Location = new System.Drawing.Point(704, 9);
            this.dtpDateFrom.Name = "dtpDateFrom";
            this.dtpDateFrom.ReadOnly = false;
            this.dtpDateFrom.Size = new System.Drawing.Size(106, 21);
            this.dtpDateFrom.TabIndex = 87;
            this.dtpDateFrom.TagName = "";
            this.dtpDateFrom.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.dtpDateFrom.WhereCondition = "";
            // 
            // cbHopDong
            // 
            cbHopDong_DesignTimeLayout.LayoutString = resources.GetString("cbHopDong_DesignTimeLayout.LayoutString");
            this.cbHopDong.DesignTimeLayout = cbHopDong_DesignTimeLayout;
            this.cbHopDong.Location = new System.Drawing.Point(81, 9);
            this.cbHopDong.Name = "cbHopDong";
            this.cbHopDong.SelectedIndex = -1;
            this.cbHopDong.SelectedItem = null;
            this.cbHopDong.Size = new System.Drawing.Size(503, 21);
            this.cbHopDong.TabIndex = 86;
            this.cbHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.cbHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.cbHopDong.ValueChanged += new System.EventHandler(this.cbHopDong_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(825, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 85;
            this.label3.Text = "ĐẾN NGÀY :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(625, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 85;
            this.label2.Text = "TỪ NGÀY :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 13);
            this.label1.TabIndex = 85;
            this.label1.Text = "HỢP ĐỒNG :";
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(122)))), ((int)(((byte)(183)))));
            this.btnSearch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSearch.Font = new System.Drawing.Font("Tahoma", 9F);
            this.btnSearch.ForeColor = System.Drawing.Color.White;
            this.btnSearch.Location = new System.Drawing.Point(1029, 7);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(125, 25);
            this.btnSearch.TabIndex = 84;
            this.btnSearch.Text = "XEM DỮ LIỆU";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.tabControl1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox2.Size = new System.Drawing.Size(1167, 449);
            this.groupBox2.TabIndex = 87;
            this.groupBox2.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpNPLTon);
            this.tabControl1.Controls.Add(this.tpNPLNhap);
            this.tabControl1.Controls.Add(this.tpSPXuat);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 14);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.Padding = new System.Drawing.Point(0, 0);
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1167, 435);
            this.tabControl1.TabIndex = 1;
            this.tabControl1.Selected += new System.Windows.Forms.TabControlEventHandler(this.tabControl1_Selected);
            // 
            // tpNPLTon
            // 
            this.tpNPLTon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(218)))), ((int)(((byte)(250)))));
            this.tpNPLTon.Controls.Add(this.dgList);
            this.tpNPLTon.Location = new System.Drawing.Point(4, 22);
            this.tpNPLTon.Margin = new System.Windows.Forms.Padding(0);
            this.tpNPLTon.Name = "tpNPLTon";
            this.tpNPLTon.Size = new System.Drawing.Size(1159, 409);
            this.tpNPLTon.TabIndex = 0;
            this.tpNPLTon.Text = "DANH SÁCH NGUYÊN PHỤ LIỆU TỒN";
            this.tpNPLTon.UseVisualStyleBackColor = true;
            // 
            // dgList
            // 
            this.dgList.ColumnAutoResize = true;
            this.dgList.ContextMenuStrip = this.mnuExportExcel;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Margin = new System.Windows.Forms.Padding(0);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.Size = new System.Drawing.Size(1159, 409);
            this.dgList.TabIndex = 85;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // mnuExportExcel
            // 
            this.mnuExportExcel.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuExport});
            this.mnuExportExcel.Name = "mnuExportExcel";
            this.mnuExportExcel.Size = new System.Drawing.Size(141, 26);
            // 
            // mnuExport
            // 
            this.mnuExport.Name = "mnuExport";
            this.mnuExport.Size = new System.Drawing.Size(140, 22);
            this.mnuExport.Text = "XUẤT EXCEL";
            this.mnuExport.Click += new System.EventHandler(this.mnuExport_Click);
            // 
            // tpNPLNhap
            // 
            this.tpNPLNhap.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(218)))), ((int)(((byte)(250)))));
            this.tpNPLNhap.Controls.Add(this.dgListNPLNhap);
            this.tpNPLNhap.Location = new System.Drawing.Point(4, 22);
            this.tpNPLNhap.Margin = new System.Windows.Forms.Padding(0);
            this.tpNPLNhap.Name = "tpNPLNhap";
            this.tpNPLNhap.Size = new System.Drawing.Size(447, 120);
            this.tpNPLNhap.TabIndex = 1;
            this.tpNPLNhap.Text = "DANH SÁCH NGUYÊN PHỤ LIỆU NHẬP";
            this.tpNPLNhap.UseVisualStyleBackColor = true;
            // 
            // dgListNPLNhap
            // 
            this.dgListNPLNhap.ColumnAutoResize = true;
            this.dgListNPLNhap.ContextMenuStrip = this.mnuExportExcel;
            dgListNPLNhap_DesignTimeLayout.LayoutString = resources.GetString("dgListNPLNhap_DesignTimeLayout.LayoutString");
            this.dgListNPLNhap.DesignTimeLayout = dgListNPLNhap_DesignTimeLayout;
            this.dgListNPLNhap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPLNhap.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPLNhap.GroupByBoxVisible = false;
            this.dgListNPLNhap.Location = new System.Drawing.Point(0, 0);
            this.dgListNPLNhap.Margin = new System.Windows.Forms.Padding(0);
            this.dgListNPLNhap.Name = "dgListNPLNhap";
            this.dgListNPLNhap.RecordNavigator = true;
            this.dgListNPLNhap.Size = new System.Drawing.Size(447, 120);
            this.dgListNPLNhap.TabIndex = 86;
            this.dgListNPLNhap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tpSPXuat
            // 
            this.tpSPXuat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(196)))), ((int)(((byte)(218)))), ((int)(((byte)(250)))));
            this.tpSPXuat.Controls.Add(this.dgListNPLXuat);
            this.tpSPXuat.Location = new System.Drawing.Point(4, 22);
            this.tpSPXuat.Margin = new System.Windows.Forms.Padding(0);
            this.tpSPXuat.Name = "tpSPXuat";
            this.tpSPXuat.Size = new System.Drawing.Size(447, 120);
            this.tpSPXuat.TabIndex = 2;
            this.tpSPXuat.Text = "DANH SÁCH NGUYÊN PHỤ LIỆU XUẤT";
            this.tpSPXuat.UseVisualStyleBackColor = true;
            // 
            // dgListNPLXuat
            // 
            this.dgListNPLXuat.ColumnAutoResize = true;
            this.dgListNPLXuat.ContextMenuStrip = this.mnuExportExcel;
            dgListNPLXuat_DesignTimeLayout.LayoutString = resources.GetString("dgListNPLXuat_DesignTimeLayout.LayoutString");
            this.dgListNPLXuat.DesignTimeLayout = dgListNPLXuat_DesignTimeLayout;
            this.dgListNPLXuat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPLXuat.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPLXuat.GroupByBoxVisible = false;
            this.dgListNPLXuat.Location = new System.Drawing.Point(0, 0);
            this.dgListNPLXuat.Margin = new System.Windows.Forms.Padding(0);
            this.dgListNPLXuat.Name = "dgListNPLXuat";
            this.dgListNPLXuat.RecordNavigator = true;
            this.dgListNPLXuat.Size = new System.Drawing.Size(447, 120);
            this.dgListNPLXuat.TabIndex = 86;
            this.dgListNPLXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // NhapXuatTonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1167, 484);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "NhapXuatTonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "NHẬP XUẤT TỒN HỢP ĐỒNG";
            this.Load += new System.EventHandler(this.NhapXuatTonForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbHopDong)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tpNPLTon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.mnuExportExcel.ResumeLayout(false);
            this.tpNPLNhap.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLNhap)).EndInit();
            this.tpSPXuat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLXuat)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpNPLTon;
        private Janus.Windows.GridEX.GridEX dgList;
        private System.Windows.Forms.TabPage tpNPLNhap;
        private System.Windows.Forms.TabPage tpSPXuat;
        private System.Windows.Forms.GroupBox groupBox1;
        public Janus.Windows.GridEX.EditControls.MultiColumnCombo cbHopDong;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSearch;
        private Janus.Windows.GridEX.GridEX dgListNPLNhap;
        private Janus.Windows.GridEX.GridEX dgListNPLXuat;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dtpDateTo;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar dtpDateFrom;
        private System.Windows.Forms.ContextMenuStrip mnuExportExcel;
        private System.Windows.Forms.ToolStripMenuItem mnuExport;



    }
}