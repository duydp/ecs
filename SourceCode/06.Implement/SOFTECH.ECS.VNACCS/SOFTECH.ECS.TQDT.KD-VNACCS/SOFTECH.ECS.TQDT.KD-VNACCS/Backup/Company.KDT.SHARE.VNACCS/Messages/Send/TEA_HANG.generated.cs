using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class TEA_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute CMN { get; set; }
public PropertiesAttribute QT { get; set; }
public PropertiesAttribute QTU { get; set; }
public PropertiesAttribute UT { get; set; }
public PropertiesAttribute UTU { get; set; }
public PropertiesAttribute VA { get; set; }
public PropertiesAttribute EVA { get; set; }

public TEA_HANG()
        {
CMN = new PropertiesAttribute(600, typeof(string));
QT = new PropertiesAttribute(15, typeof(int));
QTU = new PropertiesAttribute(4, typeof(string));
UT = new PropertiesAttribute(15, typeof(int));
UTU = new PropertiesAttribute(4, typeof(string));
VA = new PropertiesAttribute(20, typeof(string));
EVA = new PropertiesAttribute(20, typeof(string));
TongSoByte = 692;
}

}public partial class EnumGroupID
    {
}

}
