﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace LicenseCode
{
    public class TypeKey
    {
        /// <summary>
        /// Thời hạn của key
        /// </summary>
        [XmlElement("trail")]
        public List<int> Days;
    }
}
