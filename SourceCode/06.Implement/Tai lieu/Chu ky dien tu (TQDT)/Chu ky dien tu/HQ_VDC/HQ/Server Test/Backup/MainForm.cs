using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using VNPT_CA.CryptokiLib;

namespace Server_Test
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

       

        private void btnEncrypt_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Certificate file (*.crt; *.cer)|*.crt ;*.cer";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                UnicodeEncoding encoding = new UnicodeEncoding();
                string filename = openFileDialog1.FileName;
                byte[] data = encoding.GetBytes(txtData.Text);
                byte[] encdata = ServerSide.HexToBytes(txtSignature.Text);
                txtEnc.Text= ServerSide.BytesToHex( ServerSide.Encrypt(data, filename));
            }

        }

        private void btnDescrypt_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Certificate file (*.p12,*.pfx)|*.p12;*.pfx";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                UnicodeEncoding encoding = new UnicodeEncoding();
                string filename = openFileDialog1.FileName;
                byte[] encdata = ServerSide.HexToBytes(txtEnc.Text);
                byte[] decdata = ServerSide.Decrypt(encdata, filename, txtPassword.Text);
                txtDec.Text = ServerSide.BytesToString(decdata, "Unicode");
            }

        }

        private void btnSign_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Certificate file (*.p12,*.pfx)|*.p12;*.pfx";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                UnicodeEncoding encoding = new UnicodeEncoding();
                string filename = openFileDialog1.FileName;
                byte[] data = encoding.GetBytes(txtData_Sign.Text);
                txtSignature.Text = ServerSide.BytesToHex(ServerSide.Sign(data, filename, txtPIN.Text));
            }
        }

        private void btnVerify_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Certificate file (*.crt; *.cer)|*.crt ;*.cer";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                UnicodeEncoding encoding = new UnicodeEncoding();
                string filename = openFileDialog1.FileName;
                byte[] data = encoding.GetBytes(txtData_Sign.Text);
                byte[] signature = ServerSide.HexToBytes(txtSignature.Text);
                if (ServerSide.Verify(data, signature, filename)) MessageBox.Show("Valid!");
                else MessageBox.Show("Invalid!");
            }
        }

        private void btnCertInfo_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Certificate file (*.crt; *.cer)|*.crt ;*.cer";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                txtCertFileName.Text = openFileDialog1.FileName;
                CertificateInfo certInfo = new CertificateInfo(txtCertFileName.Text);
                lblSerialNumber.Text = certInfo.SerialNumber;
                lblSubject.Text = certInfo.Subject;
                lblIssuer.Text = certInfo.Issuer;
                lblFrom.Text = certInfo.NotBefore.ToString();
                lblTo.Text = certInfo.NotAfter.ToString();
            }

        }

        

        private void btnTrustCertVerify_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Certificate file (*.crt; *.cer)|*.crt ;*.cer";
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                if(ServerSide.VerifyCertificate(txtCertFileName.Text,true))
                    MessageBox.Show("Certificate is valid");
                else
                    MessageBox.Show("Certificate is not valid");
           
                txtTrustCertFileName.Text = openFileDialog1.FileName;
                if (ServerSide.VerifyTrustCertificate(txtCertFileName.Text, txtTrustCertFileName.Text))
                    MessageBox.Show("Certificate is trust");
                else
                    MessageBox.Show("Certificate is not trust");
            }
        }
    }
}