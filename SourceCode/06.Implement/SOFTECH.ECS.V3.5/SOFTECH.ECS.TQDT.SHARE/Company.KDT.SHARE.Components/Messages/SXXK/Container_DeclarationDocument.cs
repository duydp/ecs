﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.SXXK
{
   public class Container_DeclarationDocument
    {
        /// <summary>
        /// Số tờ khai
        /// 
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Ngày tờ khai
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        ///<summary>
        ///Mã loại hình
        ///</summary>
        [XmlElement("natureOfTransaction")]
        public string NatureOfTransaction { set; get; }

        /// <summary>
        /// Đơn vị HQ khai báo
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }

    }
}
