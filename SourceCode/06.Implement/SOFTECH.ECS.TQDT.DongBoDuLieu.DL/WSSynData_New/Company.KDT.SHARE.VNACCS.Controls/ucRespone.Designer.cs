﻿namespace Company.KDT.SHARE.VNACCS.Controls
{
    partial class ucRespone
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition1 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition2 = new DevExpress.XtraGrid.StyleFormatCondition();
            DevExpress.XtraGrid.StyleFormatCondition styleFormatCondition3 = new DevExpress.XtraGrid.StyleFormatCondition();
            this.PhanLuong = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.cardView1 = new DevExpress.XtraGrid.Views.Card.CardView();
            this.NoiDung = new DevExpress.XtraGrid.Columns.GridColumn();
            this.SoTiepNhan = new DevExpress.XtraGrid.Columns.GridColumn();
            this.NgayTB = new DevExpress.XtraGrid.Columns.GridColumn();
            this.popupNotifier1 = new NotificationWindow.PopupNotifier();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).BeginInit();
            this.SuspendLayout();
            // 
            // PhanLuong
            // 
            this.PhanLuong.Name = "PhanLuong";
            this.PhanLuong.Visible = true;
            this.PhanLuong.VisibleIndex = 3;
            // 
            // gridControl2
            // 
            this.gridControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl2.Location = new System.Drawing.Point(0, 0);
            this.gridControl2.MainView = this.cardView1;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.Size = new System.Drawing.Size(640, 380);
            this.gridControl2.TabIndex = 1;
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.cardView1});
            // 
            // cardView1
            // 
            this.cardView1.Appearance.Card.BackColor = System.Drawing.Color.Transparent;
            this.cardView1.Appearance.CardButton.BackColor = System.Drawing.Color.Yellow;
            this.cardView1.Appearance.FieldCaption.BackColor = System.Drawing.Color.Transparent;
            this.cardView1.Appearance.FieldCaption.Options.UseBackColor = true;
            this.cardView1.Appearance.FieldValue.BackColor = System.Drawing.Color.Transparent;
            this.cardView1.Appearance.FieldValue.BackColor2 = System.Drawing.Color.Transparent;
            this.cardView1.Appearance.FieldValue.Options.UseBackColor = true;
            this.cardView1.Appearance.FocusedCardCaption.BackColor = System.Drawing.Color.Transparent;
            this.cardView1.Appearance.FocusedCardCaption.BorderColor = System.Drawing.Color.Black;
            this.cardView1.Appearance.FocusedCardCaption.Options.UseBackColor = true;
            this.cardView1.Appearance.FocusedCardCaption.Options.UseBorderColor = true;
            this.cardView1.Appearance.HideSelectionCardCaption.BackColor = System.Drawing.Color.Transparent;
            this.cardView1.Appearance.HideSelectionCardCaption.Options.UseBackColor = true;
            this.cardView1.Appearance.SelectedCardCaption.BackColor = System.Drawing.Color.Transparent;
            this.cardView1.Appearance.SelectedCardCaption.Options.UseBackColor = true;
            this.cardView1.CardCaptionFormat = "Thông báo thứ {0}";
            this.cardView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.NoiDung,
            this.SoTiepNhan,
            this.NgayTB,
            this.PhanLuong});
            this.cardView1.FocusedCardTopFieldIndex = 0;
            styleFormatCondition1.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            styleFormatCondition1.Appearance.BackColor2 = System.Drawing.Color.Lime;
            styleFormatCondition1.Appearance.BorderColor = System.Drawing.Color.Black;
            styleFormatCondition1.Appearance.Options.UseBackColor = true;
            styleFormatCondition1.Appearance.Options.UseBorderColor = true;
            styleFormatCondition1.ApplyToRow = true;
            styleFormatCondition1.Column = this.PhanLuong;
            styleFormatCondition1.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition1.Expression = "\"[PhanLuong] = 1\"";
            styleFormatCondition2.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            styleFormatCondition2.Appearance.BackColor2 = System.Drawing.Color.Yellow;
            styleFormatCondition2.Appearance.Options.UseBackColor = true;
            styleFormatCondition2.ApplyToRow = true;
            styleFormatCondition2.Column = this.PhanLuong;
            styleFormatCondition2.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition2.Expression = "\"[PhanLuong] = 2\"";
            styleFormatCondition3.Appearance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            styleFormatCondition3.Appearance.BackColor2 = System.Drawing.Color.Red;
            styleFormatCondition3.Appearance.Options.UseBackColor = true;
            styleFormatCondition3.ApplyToRow = true;
            styleFormatCondition3.Column = this.PhanLuong;
            styleFormatCondition3.Condition = DevExpress.XtraGrid.FormatConditionEnum.Expression;
            styleFormatCondition3.Expression = "\"[PhanLuong] = 3\"";
            this.cardView1.FormatConditions.AddRange(new DevExpress.XtraGrid.StyleFormatCondition[] {
            styleFormatCondition1,
            styleFormatCondition2,
            styleFormatCondition3});
            this.cardView1.GridControl = this.gridControl2;
            this.cardView1.Name = "cardView1";
            this.cardView1.OptionsBehavior.AutoHorzWidth = true;
            this.cardView1.OptionsBehavior.Editable = false;
            this.cardView1.OptionsFind.AllowFindPanel = false;
            this.cardView1.OptionsView.AnimationType = DevExpress.XtraGrid.Views.Base.GridAnimationType.AnimateAllContent;
            this.cardView1.OptionsView.ShowQuickCustomizeButton = false;
            this.cardView1.PaintStyleName = "Skin";
            this.cardView1.DoubleClick += new System.EventHandler(this.cardView1_DoubleClick);
            this.cardView1.CustomDrawCardCaption += new DevExpress.XtraGrid.Views.Card.CardCaptionCustomDrawEventHandler(this.cardView1_CustomDrawCardCaption);
            // 
            // NoiDung
            // 
            this.NoiDung.Caption = "Thông báo";
            this.NoiDung.FieldName = "NoiDung";
            this.NoiDung.Name = "NoiDung";
            this.NoiDung.Visible = true;
            this.NoiDung.VisibleIndex = 0;
            this.NoiDung.Width = 150;
            // 
            // SoTiepNhan
            // 
            this.SoTiepNhan.Caption = "Số tiếp nhận/Số TK";
            this.SoTiepNhan.FieldName = "SoTiepNhan";
            this.SoTiepNhan.Name = "SoTiepNhan";
            this.SoTiepNhan.Visible = true;
            this.SoTiepNhan.VisibleIndex = 1;
            // 
            // NgayTB
            // 
            this.NgayTB.Caption = "Ngày cập nhật";
            this.NgayTB.FieldName = "NgayTB";
            this.NgayTB.Name = "NgayTB";
            this.NgayTB.OptionsColumn.AllowGroup = DevExpress.Utils.DefaultBoolean.True;
            this.NgayTB.Visible = true;
            this.NgayTB.VisibleIndex = 2;
            // 
            // popupNotifier1
            // 
            this.popupNotifier1.BodyColor = System.Drawing.SystemColors.Highlight;
            this.popupNotifier1.ContentFont = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.popupNotifier1.ContentText = null;
            this.popupNotifier1.Image = global::Company.KDT.SHARE.VNACCS.Controls.Properties.Resources._30412012114150logoHQ1;
            this.popupNotifier1.ImageSize = new System.Drawing.Size(48, 50);
            this.popupNotifier1.OptionsMenu = null;
            this.popupNotifier1.Size = new System.Drawing.Size(400, 100);
            this.popupNotifier1.TitleColor = System.Drawing.Color.White;
            this.popupNotifier1.TitleFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.popupNotifier1.TitlePadding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.popupNotifier1.TitleText = null;
            // 
            // ucRespone
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl2);
            this.Name = "ucRespone";
            this.Size = new System.Drawing.Size(640, 380);
            this.Load += new System.EventHandler(this.ucRespone_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cardView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Card.CardView cardView1;
        private DevExpress.XtraGrid.Columns.GridColumn NoiDung;
        private DevExpress.XtraGrid.Columns.GridColumn SoTiepNhan;
        private DevExpress.XtraGrid.Columns.GridColumn NgayTB;
        private DevExpress.XtraGrid.Columns.GridColumn PhanLuong;
        private NotificationWindow.PopupNotifier popupNotifier1;

    }
}
