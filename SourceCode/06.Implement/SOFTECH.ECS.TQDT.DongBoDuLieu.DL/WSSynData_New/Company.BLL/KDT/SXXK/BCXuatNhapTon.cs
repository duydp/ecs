using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
    public partial class BCXuatNhapTon
    {
        public int UpdateNgayThucNhapXuatTransaction(int soToKhai, string maLoaiHinh, DateTime ngayDangKy, DateTime ngayThuc, DateTime ngayhoanthanh, SqlTransaction transaction)
        {
            string sql = "";
            if (maLoaiHinh.Contains("NSX"))
                sql = "UPDATE t_KDT_SXXK_BCXuatNhapTon SET NgayHoanThanhNhap = @NgayHoanThanh WHERE SoToKhaiNhap = @SoToKhai AND MaLoaiHinhNhap = @MaLoaiHinh AND NgayDangKyNhap = @NgayDangKy";
            else
                sql = "UPDATE t_KDT_SXXK_BCXuatNhapTon SET NgayHoanThanhXuat = @NgayHoanThanh, NgayThucXuat=@NgayThuc WHERE SoToKhaiXuat = @SoToKhai AND MaLoaiHinhXuat = @MaLoaiHinh AND NgayDangKyXuat = @NgayDangKy";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, ngayDangKy);
            db.AddInParameter(dbCommand, "@NgayThuc", SqlDbType.DateTime, ngayThuc);
            db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, ngayhoanthanh);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public DataTable GetBCNXT929(string MaDoanhNghiep, int LanThanhLy)
        {
            string spName = "p_KDT_SXXK_BCXuatNhapTon_GetBCXNT929";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public DataTable GetBCXNTByLanThanhLy(string MaDoanhNghiep, int LanThanhLy)
        {
            string spName = "[p_KDT_SXXK_BCXuatNhapTon_GetBCXNTByLanThanhLy]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public DataTable GetBCXNTByLanThanhLyAllNPL(string MaDoanhNghiep, int LanThanhLy)
        {
            string spName = "[p_KDT_SXXK_BCXuatNhapTonAllNPL_GetBCXNTByLanThanhLy]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public DataTable GetBC07_KCX_XNTByLanThanhLy(string MaDoanhNghiep, int LanThanhLy, string MaHaiQuan, int BangKeHSTL_ID)
        {
            string spName = "[p_KDT_SXXK_BC07_GetBCXNTByLanThanhLy]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
            db.AddInParameter(dbCommand, "@BangKeHoSoThanhLy_ID", SqlDbType.Int, BangKeHSTL_ID);


            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
        public DataSet GetMaNPL(string maDN, int lanThanhLy)
        {
            string spName = "select distinct(MaNPL) from t_KDT_SXXK_BCXuatNhapTon where MaDoanhNghiep =@MaDoanhNghiep And LanThanhLy=@LanThanhLy";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDN);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, lanThanhLy);
            return db.ExecuteDataSet(dbCommand);
        }
        public static DataTable SelectNPLXuatDetailByYear(int NAMQUYETTOAN)
        {
            string spName = "p_KDT_SXXK_BCXuatNhapTon_SelectNPLXuatDetailByYear";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@NAMQUYETTOAN", SqlDbType.Int, NAMQUYETTOAN);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
    }
}