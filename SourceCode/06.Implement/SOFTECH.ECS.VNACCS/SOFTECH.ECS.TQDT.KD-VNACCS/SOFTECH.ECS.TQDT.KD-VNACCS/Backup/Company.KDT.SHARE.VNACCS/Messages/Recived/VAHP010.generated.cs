using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAHP010 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute A01 { get; set; }
public PropertiesAttribute A02 { get; set; }
public PropertiesAttribute A08 { get; set; }
public PropertiesAttribute A06 { get; set; }
public PropertiesAttribute A07 { get; set; }
public PropertiesAttribute A09 { get; set; }
public PropertiesAttribute A03 { get; set; }
public PropertiesAttribute A04 { get; set; }
public PropertiesAttribute A05 { get; set; }
public PropertiesAttribute A10 { get; set; }
public PropertiesAttribute A11 { get; set; }
public PropertiesAttribute A13 { get; set; }
public PropertiesAttribute A14 { get; set; }
public PropertiesAttribute A15 { get; set; }
public PropertiesAttribute A16 { get; set; }
public PropertiesAttribute A18 { get; set; }
public PropertiesAttribute A12 { get; set; }
public PropertiesAttribute A17 { get; set; }
public PropertiesAttribute A19 { get; set; }
public PropertiesAttribute A20 { get; set; }
public PropertiesAttribute A21 { get; set; }
public PropertiesAttribute A22 { get; set; }
public PropertiesAttribute A23 { get; set; }
public PropertiesAttribute A24 { get; set; }
public PropertiesAttribute A25 { get; set; }
public PropertiesAttribute A26 { get; set; }
public PropertiesAttribute MTN { get; set; }
public PropertiesAttribute B05 { get; set; }
public PropertiesAttribute B06 { get; set; }
public PropertiesAttribute A27 { get; set; }
public PropertiesAttribute A28 { get; set; }
public PropertiesAttribute AD { get; set; }
public PropertiesAttribute A30 { get; set; }
public PropertiesAttribute A29 { get; set; }

public VAHP010()
        {
A01 = new PropertiesAttribute(13, typeof(string));
A02 = new PropertiesAttribute(300, typeof(string));
A08 = new PropertiesAttribute(12, typeof(int));
A06 = new PropertiesAttribute(1, typeof(int));
A07 = new PropertiesAttribute(4, typeof(string));
A09 = new PropertiesAttribute(1, typeof(string));
A03 = new PropertiesAttribute(8, typeof(DateTime));
A04 = new PropertiesAttribute(6, typeof(string));
A05 = new PropertiesAttribute(300, typeof(string));
A10 = new PropertiesAttribute(13, typeof(string));
A11 = new PropertiesAttribute(300, typeof(string));
A13 = new PropertiesAttribute(17, typeof(string));
A14 = new PropertiesAttribute(17, typeof(string));
A15 = new PropertiesAttribute(35, typeof(string));
A16 = new PropertiesAttribute(8, typeof(DateTime));
A18 = new PropertiesAttribute(7, typeof(string));
A12 = new PropertiesAttribute(300, typeof(string));
A17 = new PropertiesAttribute(2, typeof(string));
A19 = new PropertiesAttribute(20, typeof(string));
A20 = new PropertiesAttribute(20, typeof(string));
A21 = new PropertiesAttribute(210, typeof(string));
A22 = new PropertiesAttribute(17, typeof(string));
A23 = new PropertiesAttribute(8, typeof(DateTime));
A24 = new PropertiesAttribute(17, typeof(string));
A25 = new PropertiesAttribute(8, typeof(DateTime));
A26 = new PropertiesAttribute(2, typeof(string));
MTN = new PropertiesAttribute(75, typeof(string));
B05 = new PropertiesAttribute(6, typeof(string));
B06 = new PropertiesAttribute(35, typeof(string));
A27 = new PropertiesAttribute(8, typeof(DateTime));
A28 = new PropertiesAttribute(8, typeof(DateTime));
AD = new PropertiesAttribute(750, typeof(string));
A30 = new PropertiesAttribute(996, typeof(string));
A29 = new PropertiesAttribute(300, typeof(string));
TongSoByte = 3892;
}

}public partial class EnumGroupID
    {
}

}
