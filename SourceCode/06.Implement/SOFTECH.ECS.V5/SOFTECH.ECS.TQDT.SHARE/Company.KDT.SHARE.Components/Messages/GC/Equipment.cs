﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class Equipment : Product
    {
        /// <summary>
        /// Nguyên tệ
        /// </summary>
        [XmlElement("CurrencyExchange")]
        public CurrencyExchange CurrencyExchange { get; set; }
        /// <summary>
        /// Nước sản suất
        /// </summary>
        [XmlElement("Origin")]
        public Origin Origin { get; set; }
        /// <summary>
        /// Trị giá
        /// </summary>
        [XmlElement("CustomsValue")]
        public CustomsValue CustomsValue { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

    }
}
