﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;

namespace Company.BLL.KDT.ExportToExcel
{
	public partial class ToKhaiMauDich : EntityBase
	{
		#region Private members.
		
		protected string _MaHaiQuan = String.Empty;
		protected int _SoToKhai;
		protected string _MaLoaiHinh = String.Empty;
		protected short _NamDangKy;
		protected DateTime _NgayDangKy = new DateTime(1900, 01, 01);
		protected string _MaDoanhNghiep = String.Empty;
		protected string _TenDoanhNghiep = String.Empty;
		protected string _MaDaiLyTTHQ = String.Empty;
		protected string _TenDaiLyTTHQ = String.Empty;
		protected string _TenDonViDoiTac = String.Empty;
		protected string _ChiTietDonViDoiTac = String.Empty;
		protected string _SoGiayPhep = String.Empty;
		protected DateTime _NgayGiayPhep = new DateTime(1900, 01, 01);
		protected DateTime _NgayHetHanGiayPhep = new DateTime(1900, 01, 01);
		protected string _SoHopDong = String.Empty;
		protected DateTime _NgayHopDong = new DateTime(1900, 01, 01);
		protected DateTime _NgayHetHanHopDong = new DateTime(1900, 01, 01);
		protected string _SoHoaDonThuongMai = String.Empty;
		protected DateTime _NgayHoaDonThuongMai = new DateTime(1900, 01, 01);
		protected string _PTVT_ID = String.Empty;
		protected string _SoHieuPTVT = String.Empty;
		protected DateTime _NgayDenPTVT = new DateTime(1900, 01, 01);
		protected string _QuocTichPTVT_ID = String.Empty;
		protected string _LoaiVanDon = String.Empty;
		protected string _SoVanDon = String.Empty;
		protected DateTime _NgayVanDon = new DateTime(1900, 01, 01);
		protected string _NuocXK_ID = String.Empty;
		protected string _NuocNK_ID = String.Empty;
		protected string _DiaDiemXepHang = String.Empty;
		protected string _CuaKhau_ID = String.Empty;
		protected string _DKGH_ID = String.Empty;
		protected string _NguyenTe_ID = String.Empty;
		protected decimal _TyGiaTinhThue;
		protected decimal _TyGiaUSD;
		protected string _PTTT_ID = String.Empty;
		protected short _SoHang;
		protected short _SoLuongPLTK;
		protected string _TenChuHang = String.Empty;
		protected decimal _SoContainer20;
		protected decimal _SoContainer40;
		protected decimal _SoKien;
		protected decimal _TrongLuong;
		protected decimal _TongTriGiaKhaiBao;
		protected decimal _TongTriGiaTinhThue;
		protected string _LoaiToKhaiGiaCong = String.Empty;
		protected decimal _LePhiHaiQuan;
		protected decimal _PhiBaoHiem;
		protected decimal _PhiVanChuyen;
		protected decimal _PhiXepDoHang;
		protected decimal _PhiKhac;
		protected string _Xuat_NPL_SP = String.Empty;
		protected string _ThanhLy = String.Empty;
		protected DateTime _NGAY_THN_THX = new DateTime(1900, 01, 01);
        protected DateTime _NGayHoanThanh = new DateTime(1900, 01, 01);
		protected string _MaDonViUT = String.Empty;
        private HangMauDichCollection _HMDCollection = new HangMauDichCollection();
        protected string _TrangThaiThanhKhoan = String.Empty;
        protected string _ChungTu = String.Empty;
        protected string _PhanLuong = String.Empty;
        protected int _TrangThai;
		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.

        public DateTime NgayHoanThanh
        {
            set { this._NGayHoanThanh = value; }
            get { return this._NGayHoanThanh; }
        }


        public string PhanLuong
        {
            set { this._PhanLuong = value; }
            get { return this._PhanLuong; }
        }

        public string ChungTu
        {
            set { this._ChungTu = value; }
            get { return this._ChungTu; }
        }
        public HangMauDichCollection HMDCollection
        {
            set { this._HMDCollection = value; }
            get { return this._HMDCollection; }
        }

		public string MaHaiQuan
		{
			set {this._MaHaiQuan = value;}
			get {return this._MaHaiQuan;}
		}
		public int SoToKhai
		{
			set {this._SoToKhai = value;}
			get {return this._SoToKhai;}
		}
		public string MaLoaiHinh
		{
			set {this._MaLoaiHinh = value;}
			get {return this._MaLoaiHinh;}
		}
		public short NamDangKy
		{
			set {this._NamDangKy = value;}
			get {return this._NamDangKy;}
		}
		public DateTime NgayDangKy
		{
			set {this._NgayDangKy = value;}
			get {return this._NgayDangKy;}
		}
		public string MaDoanhNghiep
		{
			set {this._MaDoanhNghiep = value;}
			get {return this._MaDoanhNghiep;}
		}
		public string TenDoanhNghiep
		{
			set {this._TenDoanhNghiep = value;}
			get {return this._TenDoanhNghiep;}
		}
		public string MaDaiLyTTHQ
		{
			set {this._MaDaiLyTTHQ = value;}
			get {return this._MaDaiLyTTHQ;}
		}
		public string TenDaiLyTTHQ
		{
			set {this._TenDaiLyTTHQ = value;}
			get {return this._TenDaiLyTTHQ;}
		}
		public string TenDonViDoiTac
		{
			set {this._TenDonViDoiTac = value;}
			get {return this._TenDonViDoiTac;}
		}
		public string ChiTietDonViDoiTac
		{
			set {this._ChiTietDonViDoiTac = value;}
			get {return this._ChiTietDonViDoiTac;}
		}
		public string SoGiayPhep
		{
			set {this._SoGiayPhep = value;}
			get {return this._SoGiayPhep;}
		}
		public DateTime NgayGiayPhep
		{
			set {this._NgayGiayPhep = value;}
			get {return this._NgayGiayPhep;}
		}
		public DateTime NgayHetHanGiayPhep
		{
			set {this._NgayHetHanGiayPhep = value;}
			get {return this._NgayHetHanGiayPhep;}
		}
		public string SoHopDong
		{
			set {this._SoHopDong = value;}
			get {return this._SoHopDong;}
		}
		public DateTime NgayHopDong
		{
			set {this._NgayHopDong = value;}
			get {return this._NgayHopDong;}
		}
		public DateTime NgayHetHanHopDong
		{
			set {this._NgayHetHanHopDong = value;}
			get {return this._NgayHetHanHopDong;}
		}
		public string SoHoaDonThuongMai
		{
			set {this._SoHoaDonThuongMai = value;}
			get {return this._SoHoaDonThuongMai;}
		}
		public DateTime NgayHoaDonThuongMai
		{
			set {this._NgayHoaDonThuongMai = value;}
			get {return this._NgayHoaDonThuongMai;}
		}
		public string PTVT_ID
		{
			set {this._PTVT_ID = value;}
			get {return this._PTVT_ID;}
		}
		public string SoHieuPTVT
		{
			set {this._SoHieuPTVT = value;}
			get {return this._SoHieuPTVT;}
		}
		public DateTime NgayDenPTVT
		{
			set {this._NgayDenPTVT = value;}
			get {return this._NgayDenPTVT;}
		}
		public string QuocTichPTVT_ID
		{
			set {this._QuocTichPTVT_ID = value;}
			get {return this._QuocTichPTVT_ID;}
		}
		public string LoaiVanDon
		{
			set {this._LoaiVanDon = value;}
			get {return this._LoaiVanDon;}
		}
		public string SoVanDon
		{
			set {this._SoVanDon = value;}
			get {return this._SoVanDon;}
		}
		public DateTime NgayVanDon
		{
			set {this._NgayVanDon = value;}
			get {return this._NgayVanDon;}
		}
		public string NuocXK_ID
		{
			set {this._NuocXK_ID = value;}
			get {return this._NuocXK_ID;}
		}
		public string NuocNK_ID
		{
			set {this._NuocNK_ID = value;}
			get {return this._NuocNK_ID;}
		}
		public string DiaDiemXepHang
		{
			set {this._DiaDiemXepHang = value;}
			get {return this._DiaDiemXepHang;}
		}
		public string CuaKhau_ID
		{
			set {this._CuaKhau_ID = value;}
			get {return this._CuaKhau_ID;}
		}
		public string DKGH_ID
		{
			set {this._DKGH_ID = value;}
			get {return this._DKGH_ID;}
		}
		public string NguyenTe_ID
		{
			set {this._NguyenTe_ID = value;}
			get {return this._NguyenTe_ID;}
		}
		public decimal TyGiaTinhThue
		{
			set {this._TyGiaTinhThue = value;}
			get {return this._TyGiaTinhThue;}
		}
		public decimal TyGiaUSD
		{
			set {this._TyGiaUSD = value;}
			get {return this._TyGiaUSD;}
		}
		public string PTTT_ID
		{
			set {this._PTTT_ID = value;}
			get {return this._PTTT_ID;}
		}
		public short SoHang
		{
			set {this._SoHang = value;}
			get {return this._SoHang;}
		}
		public short SoLuongPLTK
		{
			set {this._SoLuongPLTK = value;}
			get {return this._SoLuongPLTK;}
		}
		public string TenChuHang
		{
			set {this._TenChuHang = value;}
			get {return this._TenChuHang;}
		}
		public decimal SoContainer20
		{
			set {this._SoContainer20 = value;}
			get {return this._SoContainer20;}
		}
		public decimal SoContainer40
		{
			set {this._SoContainer40 = value;}
			get {return this._SoContainer40;}
		}
		public decimal SoKien
		{
			set {this._SoKien = value;}
			get {return this._SoKien;}
		}
		public decimal TrongLuong
		{
			set {this._TrongLuong = value;}
			get {return this._TrongLuong;}
		}
		public decimal TongTriGiaKhaiBao
		{
			set {this._TongTriGiaKhaiBao = value;}
			get {return this._TongTriGiaKhaiBao;}
		}
		public decimal TongTriGiaTinhThue
		{
			set {this._TongTriGiaTinhThue = value;}
			get {return this._TongTriGiaTinhThue;}
		}
		public string LoaiToKhaiGiaCong
		{
			set {this._LoaiToKhaiGiaCong = value;}
			get {return this._LoaiToKhaiGiaCong;}
		}
		public decimal LePhiHaiQuan
		{
			set {this._LePhiHaiQuan = value;}
			get {return this._LePhiHaiQuan;}
		}
		public decimal PhiBaoHiem
		{
			set {this._PhiBaoHiem = value;}
			get {return this._PhiBaoHiem;}
		}
		public decimal PhiVanChuyen
		{
			set {this._PhiVanChuyen = value;}
			get {return this._PhiVanChuyen;}
		}
		public decimal PhiXepDoHang
		{
			set {this._PhiXepDoHang = value;}
			get {return this._PhiXepDoHang;}
		}
		public decimal PhiKhac
		{
			set {this._PhiKhac = value;}
			get {return this._PhiKhac;}
		}
		public string Xuat_NPL_SP
		{
			set {this._Xuat_NPL_SP = value;}
			get {return this._Xuat_NPL_SP;}
		}
		public string ThanhLy
		{
			set {this._ThanhLy = value;}
			get {return this._ThanhLy;}
		}
		public DateTime NGAY_THN_THX
		{
			set {this._NGAY_THN_THX = value;}
			get {return this._NGAY_THN_THX;}
		}
		public string MaDonViUT
		{
			set {this._MaDonViUT = value;}
			get {return this._MaDonViUT;}
		}
        public string TrangThaiThanhKhoan
        {
            set { this._TrangThaiThanhKhoan= value; }
            get { return this._TrangThaiThanhKhoan; }
        }
       //Bổ sung
        public int  TrangThai
        {
            set { this._TrangThai   = value; }
            get { return this._TrangThai; }
        }
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
        public void LoadHMDCollection()
        {
            HangMauDich hmd = new HangMauDich();
            hmd.MaHaiQuan = this.MaHaiQuan;
            hmd.MaLoaiHinh = this.MaLoaiHinh;
            hmd.SoToKhai = this.SoToKhai;
            hmd.NamDangKy = this.NamDangKy;
            this.HMDCollection = hmd.SelectCollectionBy_MaHaiQuan_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy();
        }
		public bool Load()
		{
			string spName = "p_SXXK_ToKhaiMauDich_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) this._NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) this._TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) this._MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) this._TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) this._TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) this._ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) this._SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) this._NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) this._NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) this._SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) this._NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) this._NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) this._SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) this._NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) this._PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) this._SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) this._NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) this._QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) this._LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) this._SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) this._NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) this._NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) this._NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) this._DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) this._CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this._DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) this._TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) this._TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this._PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) this._SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) this._SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) this._TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) this._SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) this._SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) this._SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this._TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) this._TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) this._TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) this._LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) this._LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) this._PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) this._PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) this._PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) this._PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) this._Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) this._NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) this._MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) this._ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) this.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) this.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) this.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) this.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this.TrangThai  = reader.GetInt16 (reader.GetOrdinal("TrangThai"));
                
                return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------

        public DataSet GetThongBao(uint hanthanhkhoan,uint thoigiantk)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();
            string sql = "SELECT *, (@HanThanhKhoan - DATEDIFF(day, NgayDangKy, GETDATE())) as songay " +
                        "FROM         t_SXXK_ToKhaiMauDich " +
                        "WHERE     (275 - DATEDIFF(day, NgayDangKy, GETDATE()) <=@ThoiGianTK) AND ThanhLy!='H' AND" +
                        " maloaihinh like 'NSX%'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HanThanhKhoan",SqlDbType.Int, hanthanhkhoan);
            db.AddInParameter(dbCommand, "@ThoiGianTK", SqlDbType.Int, thoigiantk);
            return this.db.ExecuteDataSet(dbCommand);              
        }

		public DataSet SelectAll()
        {
            string spName = "p_SXXK_ToKhaiMauDich_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_ToKhaiMauDich_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
        //public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        //{
        //    string spName = "p_SXXK_ToKhaiMauDich_SelectDynamic";
        //    SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

        //    this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
        //    this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
        //    return this.db.ExecuteDataSet(dbCommand);        				
        //}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_ToKhaiMauDich_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
        public IDataReader SelectReaderDSTKNChuaThanhLy(string maDoanhNghiep,string  maHaiQuan, DateTime maxDate)
        {
            string spName = "p_SXXK_GetDSTKNChuaThanhLy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@maxDate", SqlDbType.DateTime, maxDate);

            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKXChuaThanhLy(string maDoanhNghiep, string maHaiQuan)
        {
            string spName = "p_SXXK_GetDSTKXChuaThanhLy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKXChuaThanhLyMinDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate)
        {
            string spName = "p_SXXK_GetDSTKXChuaThanhLyMinDate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@MinDate", SqlDbType.DateTime, minDate);

            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKXChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            string spName = "p_SXXK_GetDSTKXChuaThanhLyDate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, minDate);
            this.db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, maxDate);
            
            return this.db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDSTKNChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            string spName = "p_SXXK_GetDSTKNChuaThanhLyDate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            this.db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, minDate);
            this.db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, maxDate);

            return this.db.ExecuteReader(dbCommand);
        }
		//---------------------------------------------------------------------------------------------
		
		public ToKhaiMauDichCollection SelectCollectionAll()
		{
			ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				ToKhaiMauDich entity = new ToKhaiMauDich();
				
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) entity.Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public ToKhaiMauDichCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				ToKhaiMauDich entity = new ToKhaiMauDich();
				
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChiTietDonViDoiTac"))) entity.ChiTietDonViDoiTac = reader.GetString(reader.GetOrdinal("ChiTietDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayPhep"))) entity.SoGiayPhep = reader.GetString(reader.GetOrdinal("SoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiayPhep"))) entity.NgayGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanGiayPhep"))) entity.NgayHetHanGiayPhep = reader.GetDateTime(reader.GetOrdinal("NgayHetHanGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHopDong"))) entity.NgayHetHanHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDonThuongMai"))) entity.SoHoaDonThuongMai = reader.GetString(reader.GetOrdinal("SoHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDonThuongMai"))) entity.NgayHoaDonThuongMai = reader.GetDateTime(reader.GetOrdinal("NgayHoaDonThuongMai"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTVT_ID"))) entity.PTVT_ID = reader.GetString(reader.GetOrdinal("PTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT_ID"))) entity.QuocTichPTVT_ID = reader.GetString(reader.GetOrdinal("QuocTichPTVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiVanDon"))) entity.LoaiVanDon = reader.GetString(reader.GetOrdinal("LoaiVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTinhThue"))) entity.TyGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TyGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHang"))) entity.SoHang = reader.GetInt16(reader.GetOrdinal("SoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongPLTK"))) entity.SoLuongPLTK = reader.GetInt16(reader.GetOrdinal("SoLuongPLTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChuHang"))) entity.TenChuHang = reader.GetString(reader.GetOrdinal("TenChuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer20"))) entity.SoContainer20 = reader.GetDecimal(reader.GetOrdinal("SoContainer20"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer40"))) entity.SoContainer40 = reader.GetDecimal(reader.GetOrdinal("SoContainer40"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaKhaiBao"))) entity.TongTriGiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("TongTriGiaKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiToKhaiGiaCong"))) entity.LoaiToKhaiGiaCong = reader.GetString(reader.GetOrdinal("LoaiToKhaiGiaCong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LePhiHaiQuan"))) entity.LePhiHaiQuan = reader.GetDecimal(reader.GetOrdinal("LePhiHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiXepDoHang"))) entity.PhiXepDoHang = reader.GetDecimal(reader.GetOrdinal("PhiXepDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiKhac"))) entity.PhiKhac = reader.GetDecimal(reader.GetOrdinal("PhiKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("Xuat_NPL_SP"))) entity.Xuat_NPL_SP = reader.GetString(reader.GetOrdinal("Xuat_NPL_SP"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThanhLy"))) entity.ThanhLy = reader.GetString(reader.GetOrdinal("ThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetString(reader.GetOrdinal("TrangThaiThanhKhoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanh"))) entity.NgayHoanThanh = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanh"));
                //Bổ sung thêm
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt16(reader.GetOrdinal("TrangThai"));

                collection.Add(entity);
			}
			return collection;			
		}
        public ToKhaiMauDichCollection GetDSTKNChuaThanhLy(string maDoanhNghiep, string maHaiQuan, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKNChuaThanhLy(maDoanhNghiep, maHaiQuan, maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKXChuaThanhLy(string maDoanhNghiep, string maHaiQuan)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXChuaThanhLy(maDoanhNghiep, maHaiQuan);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKXChuaThanhLyMinDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXChuaThanhLyMinDate(maDoanhNghiep, maHaiQuan, minDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKXChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKXChuaThanhLyByDate(maDoanhNghiep, maHaiQuan, minDate,maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            return collection;
        }
        public ToKhaiMauDichCollection GetDSTKNChuaThanhLyByDate(string maDoanhNghiep, string maHaiQuan, DateTime minDate, DateTime maxDate)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDSTKNChuaThanhLyByDate(maDoanhNghiep, maHaiQuan, minDate, maxDate);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_THN_THX"))) entity.NGAY_THN_THX = reader.GetDateTime(reader.GetOrdinal("NGAY_THN_THX"));
                collection.Add(entity);
            }
            return collection;
        }
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ToKhaiMauDich_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
			this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
			this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
			this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
			this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
			this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
			this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
			this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
			this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
			this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
			this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
			this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
			this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
			this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
			this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
			this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
			this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
			this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
			this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
			this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
			this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
			this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
			this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
			this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
			this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
			this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
			this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
			this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
			this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
			this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
			this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
			this.db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
			this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
			this.db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
			this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            this.db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
			if (transaction != null)
			{
				return this.db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return this.db.ExecuteNonQuery(dbCommand);
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(ToKhaiMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ToKhaiMauDich item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, ToKhaiMauDichCollection collection)
        {
            foreach (ToKhaiMauDich item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.

        //**************** InsertUpdateFull() ******************
        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                  // this.InsertUpdateTransaction(transaction);
                    //this.UpdateTransaction(transaction);
                    BLL.SXXK.ToKhai.ToKhaiMauDich tkMD = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                    tkMD.MaDoanhNghiep = this.MaDoanhNghiep;
                    tkMD.PhanLuong = "";
                    tkMD.MaHaiQuan = this.MaHaiQuan;
                    tkMD.MaLoaiHinh = this.MaLoaiHinh;
                    tkMD.NamDangKy = (short)this.NgayDangKy.Year;
                    //tkMD.NamDangKy = this.NamDangKy ;
                    tkMD.NgayDangKy = this.NgayDangKy;
                    tkMD.SoToKhai = this.SoToKhai;
                    tkMD.TenDoanhNghiep = this.TenDoanhNghiep;//
                    tkMD.TenDaiLyTTHQ = this.TenDaiLyTTHQ;//
                    tkMD.MaDaiLyTTHQ = this.MaDaiLyTTHQ; //
                    tkMD.TenDonViDoiTac = this.TenDonViDoiTac;
                    tkMD.ChiTietDonViDoiTac = this.ChiTietDonViDoiTac;//
                    tkMD.NgayGiayPhep = this.NgayGiayPhep;
                    tkMD.NgayHetHanGiayPhep = this.NgayHetHanGiayPhep;
                    tkMD.SoHopDong = this.SoHopDong;
                    tkMD.NgayHopDong = this.NgayHopDong;
                    tkMD.NgayHetHanHopDong = this.NgayHetHanHopDong;
                    tkMD.SoHoaDonThuongMai = this.SoHoaDonThuongMai;
                    tkMD.NgayHoaDonThuongMai = this.NgayHoaDonThuongMai;
                    tkMD.PTVT_ID = this.PTVT_ID;
                    tkMD.SoHieuPTVT = this.SoHieuPTVT;
                    tkMD.NgayDenPTVT = this.NgayDenPTVT;
                    tkMD.SoVanDon = this.SoVanDon; //
                    tkMD.LoaiVanDon = this.LoaiVanDon;
                    tkMD.NgayVanDon = this.NgayVanDon;
                    tkMD.NuocXK_ID = this.NuocXK_ID;
                    tkMD.NuocNK_ID = this.NuocNK_ID;
                    tkMD.DiaDiemXepHang = this.DiaDiemXepHang;
                    tkMD.DKGH_ID = this.DKGH_ID;
                    tkMD.CuaKhau_ID = this.CuaKhau_ID;
                    tkMD.NguyenTe_ID = this.NguyenTe_ID;
                    tkMD.TyGiaTinhThue = this.TyGiaTinhThue;
                    tkMD.TyGiaUSD = this.TyGiaUSD;
                    tkMD.PTTT_ID = this.PTTT_ID;
                    tkMD.SoHang = this.SoHang;
                    tkMD.SoLuongPLTK = this.SoLuongPLTK;
                    tkMD.TenChuHang = this.TenChuHang;
                    tkMD.SoContainer20 = this.SoContainer20;
                    tkMD.SoContainer40 = this.SoContainer40;
                    tkMD.SoKien = this.SoKien;
                    tkMD.TrongLuong = this.TrongLuong;
                    tkMD.TongTriGiaKhaiBao = this.TongTriGiaKhaiBao;
                    tkMD.TongTriGiaTinhThue = this.TongTriGiaTinhThue;
                    tkMD.LoaiToKhaiGiaCong = this.LoaiToKhaiGiaCong;
                    tkMD.LePhiHaiQuan = this.LePhiHaiQuan;
                    tkMD.PhiKhac = this.PhiKhac;//
                    tkMD.PhiBaoHiem = this.PhiBaoHiem;
                    tkMD.PhiVanChuyen = this.PhiVanChuyen;
                    tkMD.ThanhLy = "";
                    tkMD.Xuat_NPL_SP = this.Xuat_NPL_SP ;//edit
                    tkMD.ChungTu = this.ChungTu ;       // edit
                    tkMD.NGAY_THN_THX = this.NGAY_THN_THX;//
                    tkMD.TrangThaiThanhKhoan = "D";                   
                    tkMD.MaDonViUT = this.MaDonViUT;//
                    tkMD.NgayHoanThanh = this.NgayHoanThanh;//
                    tkMD.TrangThai = 1;
                    tkMD.InsertUpdateTransaction(transaction);
                    if (this.HMDCollection.Count == 0)
                        this.LoadHMDCollection();
                    foreach (HangMauDich hmd in this.HMDCollection)
                    {
                        BLL.SXXK.ToKhai.HangMauDich hmdTK = new Company.BLL.SXXK.ToKhai.HangMauDich();
                        hmdTK.DonGiaKB = hmd.DonGiaKB;
                        hmdTK.DonGiaTT = hmd.DonGiaTT;
                        hmdTK.DVT_ID = hmd.DVT_ID;
                        hmdTK.MaHaiQuan = this.MaHaiQuan;
                        hmdTK.MaHS = hmd.MaHS;
                        hmdTK.MaLoaiHinh = this.MaLoaiHinh;
                        hmdTK.MaPhu = hmd.MaPhu;
                        hmdTK.MienThue = hmd.MienThue;
                        hmdTK.NamDangKy = (short)this.NgayDangKy.Year;
                        // hmdTK.NamDangKy = this.NamDangKy ;
                        hmdTK.NuocXX_ID = hmd.NuocXX_ID;
                        hmdTK.PhuThu = hmd.PhuThu;
                        hmdTK.SoLuong = hmd.SoLuong;
                        hmdTK.SoThuTuHang = (short)hmd.SoThuTuHang;
                        hmdTK.SoToKhai = this.SoToKhai;
                        hmdTK.TenHang = hmd.TenHang;
                        hmdTK.ThueGTGT = hmd.ThueGTGT;
                        hmdTK.ThueSuatGTGT = hmd.ThueSuatGTGT;
                        hmdTK.ThueSuatTTDB = hmd.ThueSuatTTDB;
                        hmdTK.ThueSuatXNK = hmd.ThueSuatXNK;
                        hmdTK.ThueTTDB = hmd.ThueTTDB;
                        hmdTK.ThueXNK = hmd.ThueXNK;
                        hmdTK.TriGiaKB = hmd.TriGiaKB;
                        hmdTK.TriGiaKB_VND = hmd.TriGiaKB_VND;
                        hmdTK.TriGiaThuKhac = hmd.TriGiaThuKhac;
                        hmdTK.TriGiaTT = hmd.TriGiaTT;
                        hmdTK.TyLeThuKhac = hmd.TyLeThuKhac;

                        hmdTK.InsertUpdateTransaction(transaction);
                        if (tkMD.MaLoaiHinh.StartsWith("N") || tkMD.MaLoaiHinh.StartsWith("X"))
                       { 
                            //cap nhat vao bang thong tin dieu chinh
                            BLL.SXXK.ToKhai.ThongTinDieuChinh ttDC = new Company.BLL.SXXK.ToKhai.ThongTinDieuChinh();
                            ttDC.MaHaiQuan = tkMD.MaHaiQuan = this.MaHaiQuan;
                            ttDC.MaLoaiHinh = tkMD.MaLoaiHinh = this.MaLoaiHinh;
                            ttDC.NamDangKy = tkMD.NamDangKy = (short)this.NgayDangKy.Year;
                            //ttDC.SoChungTu  = ;
                            ttDC.NGAY_HL = tkMD.NgayDangKy = this.NgayDangKy;
                            ttDC.SoToKhai = tkMD.SoToKhai = this.SoToKhai;
                            //ttDC.LanDieuChinh = 0; //


                            ttDC.InsertUpdateTransaction(transaction);

                            //cap nhat vao hang dieu chinh
                            BLL.SXXK.ToKhai.HangMauDichDieuChinh hmdDC = new Company.BLL.SXXK.ToKhai.HangMauDichDieuChinh();

                            hmdDC.SoToKhai = hmdTK.SoToKhai;
                            hmdDC.NamDangKy =hmdTK.NamDangKy ;
                            hmdDC.MaHaiQuan = hmdTK.MaHaiQuan;
                            hmdDC.MaHang = hmd.MaPhu;
                            hmdDC.MaLoaiHinh = hmdTK.MaLoaiHinh;
                            hmdDC.LanDieuChinh = ttDC.LanDieuChinh;
                            hmdDC.MA_NPL_SP = hmd.MaPhu; //
                            hmdDC.MaHSKB = hmdTK.MaHS;
                            hmdDC.MaHS = hmdTK.MaHS ;

                            hmdDC.DGIA_KB = (double)hmdTK.DonGiaKB;
                            hmdDC.DGIA_TT = (double)hmdTK.DonGiaTT;
                            hmdDC.DVT_ID = hmdTK.DVT_ID;                          
                            hmdDC.NuocXX_ID = hmdTK.NuocXX_ID;                          
                            hmdDC.Luong = hmdTK.SoLuong;
                            hmdDC.STTHang = hmdTK.SoThuTuHang;                           
                            hmdDC.TenHang = hmdTK.TenHang;

                            hmdDC.THUE_VAT = (double)hmdTK.ThueGTGT;
                            hmdDC.TS_VAT = hmdTK.ThueSuatGTGT;
                            hmdDC.TS_TTDB = hmdTK.ThueSuatTTDB;
                            hmdDC.TS_XNK = hmdTK.ThueSuatXNK;                           
                            hmdTK.ThueXNK = hmd.ThueXNK;                           
                            hmdDC.TRIGIA_KB = (double)hmdTK.TriGiaKB;
                            hmdDC.TGKB_VND = (double)hmdTK.TriGiaKB_VND;                          
                            hmdDC.TRIGIA_TT = (double)hmdTK.TriGiaTT;
                            hmdDC.TYLE_THUKHAC = hmdTK.TyLeThuKhac;                           
                            hmdDC.DinhMuc = 0;
                            hmdDC.MA_DG = "HD";
                            hmdDC.LOAITSXNK = 0;
                            hmdDC.MA_THKE = "";
                            hmdDC.TL_QUYDOI = 0;

                            hmdDC.InsertUpdateTransaction(transaction);
                           
                            //cap nhat vao hang NPL nhap ton
                            BLL.SXXK.ThanhKhoan.NPLNhapTon nplNhapTon = new Company.BLL.SXXK.ThanhKhoan.NPLNhapTon();
                            nplNhapTon.SoToKhai = tkMD.SoToKhai;
                            nplNhapTon.MaLoaiHinh = tkMD.MaLoaiHinh;
                            nplNhapTon.NamDangKy = tkMD.NamDangKy;
                            nplNhapTon.MaHaiQuan = tkMD.MaHaiQuan;
                            nplNhapTon.MaNPL = hmdTK.MaPhu;
                            nplNhapTon.MaDoanhNghiep = tkMD.MaDoanhNghiep;
                            nplNhapTon.Luong = hmdTK.SoLuong;
                            nplNhapTon.Ton = hmd.Ton  ; //
                            nplNhapTon.ThueXNK = Convert.ToDouble(hmdTK.ThueXNK);
                            nplNhapTon.ThueTTDB = Convert.ToDouble(hmdTK.ThueTTDB);
                            nplNhapTon.ThueVAT = Convert.ToDouble(hmdTK.ThueGTGT);
                            nplNhapTon.PhuThu = Convert.ToDouble(hmdTK.PhuThu);
                            nplNhapTon.ThueCLGia = Convert.ToDouble(hmdTK.TriGiaThuKhac); 
                          
                            nplNhapTon.InsertUpdateTransaction(transaction);

                       }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        //***********************************
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ToKhaiMauDich_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
			this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
			this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
			this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
			this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
			this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
			this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
			this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
			this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
			this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
			this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
			this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
			this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
			this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
			this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
			this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
			this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
			this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
			this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
			this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
			this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
			this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
			this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
			this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
			this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
			this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
			this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
			this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
			this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
			this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
			this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
			this.db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
			this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
			this.db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
			this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            this.db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(ToKhaiMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ToKhaiMauDich item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ToKhaiMauDich_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, this._NgayDangKy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, this._TenDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@MaDaiLyTTHQ", SqlDbType.VarChar, this._MaDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDaiLyTTHQ", SqlDbType.NVarChar, this._TenDaiLyTTHQ);
			this.db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, this._TenDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@ChiTietDonViDoiTac", SqlDbType.NVarChar, this._ChiTietDonViDoiTac);
			this.db.AddInParameter(dbCommand, "@SoGiayPhep", SqlDbType.NVarChar, this._SoGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayGiayPhep", SqlDbType.DateTime, this._NgayGiayPhep);
			this.db.AddInParameter(dbCommand, "@NgayHetHanGiayPhep", SqlDbType.DateTime, this._NgayHetHanGiayPhep);
			this.db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, this._SoHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, this._NgayHopDong);
			this.db.AddInParameter(dbCommand, "@NgayHetHanHopDong", SqlDbType.DateTime, this._NgayHetHanHopDong);
			this.db.AddInParameter(dbCommand, "@SoHoaDonThuongMai", SqlDbType.NVarChar, this._SoHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@NgayHoaDonThuongMai", SqlDbType.DateTime, this._NgayHoaDonThuongMai);
			this.db.AddInParameter(dbCommand, "@PTVT_ID", SqlDbType.VarChar, this._PTVT_ID);
			this.db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.NVarChar, this._SoHieuPTVT);
			this.db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, this._NgayDenPTVT);
			this.db.AddInParameter(dbCommand, "@QuocTichPTVT_ID", SqlDbType.Char, this._QuocTichPTVT_ID);
			this.db.AddInParameter(dbCommand, "@LoaiVanDon", SqlDbType.NVarChar, this._LoaiVanDon);
			this.db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.NVarChar, this._SoVanDon);
			this.db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, this._NgayVanDon);
			this.db.AddInParameter(dbCommand, "@NuocXK_ID", SqlDbType.Char, this._NuocXK_ID);
			this.db.AddInParameter(dbCommand, "@NuocNK_ID", SqlDbType.Char, this._NuocNK_ID);
			this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, this._DiaDiemXepHang);
			this.db.AddInParameter(dbCommand, "@CuaKhau_ID", SqlDbType.Char, this._CuaKhau_ID);
			this.db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, this._DKGH_ID);
			this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
			this.db.AddInParameter(dbCommand, "@TyGiaTinhThue", SqlDbType.Money, this._TyGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, this._TyGiaUSD);
			this.db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, this._PTTT_ID);
			this.db.AddInParameter(dbCommand, "@SoHang", SqlDbType.SmallInt, this._SoHang);
			this.db.AddInParameter(dbCommand, "@SoLuongPLTK", SqlDbType.SmallInt, this._SoLuongPLTK);
			this.db.AddInParameter(dbCommand, "@TenChuHang", SqlDbType.NVarChar, this._TenChuHang);
			this.db.AddInParameter(dbCommand, "@SoContainer20", SqlDbType.Decimal, this._SoContainer20);
			this.db.AddInParameter(dbCommand, "@SoContainer40", SqlDbType.Decimal, this._SoContainer40);
			this.db.AddInParameter(dbCommand, "@SoKien", SqlDbType.Decimal, this._SoKien);
			this.db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, this._TrongLuong);
			this.db.AddInParameter(dbCommand, "@TongTriGiaKhaiBao", SqlDbType.Money, this._TongTriGiaKhaiBao);
			this.db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Money, this._TongTriGiaTinhThue);
			this.db.AddInParameter(dbCommand, "@LoaiToKhaiGiaCong", SqlDbType.VarChar, this._LoaiToKhaiGiaCong);
			this.db.AddInParameter(dbCommand, "@LePhiHaiQuan", SqlDbType.Money, this._LePhiHaiQuan);
			this.db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Money, this._PhiBaoHiem);
			this.db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Money, this._PhiVanChuyen);
			this.db.AddInParameter(dbCommand, "@PhiXepDoHang", SqlDbType.Money, this._PhiXepDoHang);
			this.db.AddInParameter(dbCommand, "@PhiKhac", SqlDbType.Money, this._PhiKhac);
			this.db.AddInParameter(dbCommand, "@Xuat_NPL_SP", SqlDbType.Char, this._Xuat_NPL_SP);
			this.db.AddInParameter(dbCommand, "@ThanhLy", SqlDbType.Char, this._ThanhLy);
			this.db.AddInParameter(dbCommand, "@NGAY_THN_THX", SqlDbType.DateTime, this._NGAY_THN_THX);
			this.db.AddInParameter(dbCommand, "@MaDonViUT", SqlDbType.VarChar, this._MaDonViUT);
            this.db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Char, this.TrangThaiThanhKhoan);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, this.ChungTu);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, this.PhanLuong);
            this.db.AddInParameter(dbCommand, "@NgayHoanThanh", SqlDbType.DateTime, this.NgayHoanThanh);
            //Bổ sung thêm
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this.TrangThai);
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(ToKhaiMauDichCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiMauDich item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ToKhaiMauDich_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(ToKhaiMauDichCollection collection, SqlTransaction transaction)
        {
            foreach (ToKhaiMauDich item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(ToKhaiMauDichCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (ToKhaiMauDich item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion

      
        public ToKhaiMauDichCollection SelectCollectionNhacNhoDynamic(string whereCondition, string orderByExpression)
        {
            ToKhaiMauDichCollection collection = new ToKhaiMauDichCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiMauDich entity = new ToKhaiMauDich();

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));                
                collection.Add(entity);
            }
            return collection;
        }
        public static void getDanhSachNhacNho()
        {
            string where = "";
        }
        public void UpdateMaHang(string maMoi, string maCu)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    string sql = "UPDATE t_SXXK_HangMauDich SET MaPhu = '" + maMoi + "' WHERE " +
                                 "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                                 "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + this.MaHaiQuan + "' AND MaPhu = '" + maCu +"'";
                    SqlCommand sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.ExecuteNonQuery(sqlCmd, transaction);

                    sql = "UPDATE t_SXXK_HangMauDichDieuChinh SET Ma_NPL_SP = '" + maMoi + "' WHERE " +
                         "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                         "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + this.MaHaiQuan + "' AND Ma_NPL_SP = '" + maCu + "'";
                    sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.ExecuteNonQuery(sqlCmd, transaction);

                    sql = "UPDATE t_SXXK_ThanhLy_NPLNhapTon SET MaNPL = '" + maMoi + "' WHERE " +
                          "SoToKhai = " + this.SoToKhai + " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' " +
                          "AND NamDangKy = " + this.NamDangKy + " AND MaHaiQuan = '" + this.MaHaiQuan + "' AND MaNPL = '" + maCu + "'";
                    sqlCmd = (SqlCommand)db.GetSqlStringCommand(sql);
                    db.ExecuteNonQuery(sqlCmd, transaction);
                    transaction.Commit();
                }
                catch
                {
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
        }
	}	
}

 