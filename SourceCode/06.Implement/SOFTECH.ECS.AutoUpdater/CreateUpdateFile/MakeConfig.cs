﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
namespace CreateUpdateFile
{
    public class MakeConfig
    {
        #region private
        private string group;
        private string version;
        private string folderDatabase;
        private string url;
        private string title;
        private string description;
        private string header;
        private string footer;
        private string dateRelease;
        private NeedRestartFile needRestart = new NeedRestartFile();
        private string clientPath;
        #endregion

        public string Group { get { return group; } set { group = value; } }
        public string Version { get { return version; } set { version = value; } }
        public string FolderDatabase { get { return folderDatabase; } set { folderDatabase = value; } }
        public string Url { get { return url; } set { url = value; } }
        public string Header { get { return header; } set { header = value; } }
        public string Description { get { return description; } set { description = value; } }
        public string Footer { get { return footer; } set { footer = value; } }
        public string Title { get { return title; } set { title = value; } }
        public string DateRelease { get { return dateRelease; } set { dateRelease = value; } }
        public NeedRestartFile NeedRestartFile { get { return needRestart; } set { needRestart = value; } }
        public string ClientPath { get { return clientPath; } set { clientPath = value; } }
        #region Property

        #endregion
        public MakeConfig()
        {
        }
        private static MakeConfig install;
        public static MakeConfig Install
        {
            get
            {
                return install;
            }
        }
        public static bool Save()
        {
            try
            {
                XmlSerializer xs = new XmlSerializer(typeof(MakeConfig));
                StreamWriter sw = new StreamWriter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MakeConfig.cfg"));
                xs.Serialize(sw, install);
                sw.Close();
                return true;
            }
            catch
            {
                return false;
            }
        }
        static MakeConfig()
        {
            if (install == null)
            {
                XmlSerializer xs = new XmlSerializer(typeof(MakeConfig));
                StreamReader sr = new StreamReader(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "MakeConfig.cfg"));
                install = xs.Deserialize(sr) as MakeConfig;
                sr.Close();
            }
        }
    }
}
