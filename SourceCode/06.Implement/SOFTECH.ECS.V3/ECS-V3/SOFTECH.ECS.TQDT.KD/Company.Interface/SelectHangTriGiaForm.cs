﻿using System;
using System.Drawing;
//using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL.KDT;
//using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL;
//using Company.KD.BLL.Utils;
using System.Data;
using System.Collections.Generic;
#if GC_V3
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif
namespace Company.Interface
{
    public partial class SelectHangTriGiaForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------

        public ToKhaiMauDich TKMD;
        public ToKhaiTriGia TKTG;
        public HangMauDich HMD;
        //-----------------------------------------------------------------------------------------

        public SelectHangTriGiaForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(int STT)
        {
            foreach (HangTriGia hmd in TKTG.HTGCollection)
            {
                if (hmd.STTHang == STT) return true;
            }
            return false;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            double PhiBaoHiem = Math.Round(Convert.ToDouble(TKMD.PhiBaoHiem) / Convert.ToDouble(TKMD.TongTriGiaKhaiBao), 8);
            double PhiVanChuyen = 0;
            if (TKMD.PhiVanChuyen > 0)
                PhiVanChuyen = Math.Round(Convert.ToDouble(TKMD.PhiVanChuyen / TKMD.TongTriGiaKhaiBao), 8);
            double PhiKhac = 0;
            if (TKMD.PhiKhac > 0) PhiKhac = Math.Round(Convert.ToDouble(TKMD.PhiKhac / TKMD.TongTriGiaKhaiBao), 8);
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {

                if (i.RowType == RowType.Record)
                {
                    HangMauDich hangMD = (HangMauDich)i.GetRow().DataRow;
                    if (hangMD.DonGiaKB == 0)
                    {
                        continue;
                    }
                    HangTriGia hangTG = new HangTriGia();
                    hangTG.GiaTrenHoaDon = Math.Round(Convert.ToDouble(hangMD.DonGiaKB), 8);
                    hangTG.STTHang = hangMD.SoThuTuHang;
                    hangTG.TenHang = hangMD.TenHang;
                    hangTG.TriGiaNguyenTe = Math.Round(Convert.ToDouble(hangMD.DonGiaKB), 8);
                    hangTG.TriGiaVND = Math.Round(hangTG.GiaTrenHoaDon * Convert.ToDouble(TKMD.TyGiaTinhThue), 8);
                    if (GlobalSettings.TuDongTinhThue == "1")
                    {
                        hangTG.PhiBaoHiem = Math.Round(PhiBaoHiem * hangTG.GiaTrenHoaDon, 8);
                        hangTG.TienThue = Math.Round(PhiKhac * hangTG.GiaTrenHoaDon, 8);
                        hangTG.ChiPhiVanChuyen = Math.Round(PhiVanChuyen * hangTG.GiaTrenHoaDon, 8);
                        hangTG.TriGiaNguyenTe += Math.Round((hangTG.PhiBaoHiem + hangTG.ChiPhiVanChuyen), 8);
                        hangTG.TriGiaVND = Math.Round(hangTG.TriGiaNguyenTe * Convert.ToDouble(TKMD.TyGiaTinhThue), 8);
                    }

                    if (checkMaHangExit(hangTG.STTHang))
                    {
                        if (MLMessages("Hàng này đã có trong danh sách nên được bỏ qua. Bạn có muốn tiếp tục không?", "MSG_SEL01", "", true) != "Yes")
                        {
                            break;
                        }
                    }
                    else
                        TKTG.HTGCollection.Add(hangTG);
                }
            }
            this.Close();
        }



        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            double PhiBaoHiem = Math.Round(Convert.ToDouble(TKMD.PhiBaoHiem) / Convert.ToDouble(TKMD.TongTriGiaKhaiBao), 8);
            double PhiVanChuyen = Math.Round(Convert.ToDouble(TKMD.PhiVanChuyen / TKMD.TongTriGiaKhaiBao), 8);
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    HangTKTGForm f = new HangTKTGForm();
                    HangMauDich HMD = this.TKMD.HMDCollection[i.Position];
                    if (HMD.DonGiaKB == 0)
                    {
                        ShowMessage("Đây là hàng có đơn giá = 0 không khai báo trong tờ khai trị giá", false);
                        return;
                    }
                    f.TyGiaTT = TKMD.TyGiaTinhThue;
                    if (checkMaHangExit(HMD.SoThuTuHang))
                    {
                        MLMessages("Mặt hàng này đã được chọn.", "MSG_SEL02", "", false);
                        return;
                    }
                    f.TKTG = this.TKTG;
                    f.HangTG.TenHang = HMD.TenHang;
                    f.HangTG.STTHang = HMD.SoThuTuHang;
                    f.HangTG.GiaTrenHoaDon = Math.Round(Convert.ToDouble(HMD.DonGiaKB), 8);
                    f.HangTG.TriGiaNguyenTe = Math.Round(Convert.ToDouble(HMD.DonGiaKB), 8);
#if KD_V3
                    f.HangTG.TriGiaVND = Math.Round(HMD.DonGiaKB * Convert.ToDouble(TKMD.TyGiaTinhThue), 8);
#elif GC_V3
                    f.HangTG.TriGiaVND = Convert.ToDouble(Math.Round(HMD.DonGiaKB * TKMD.TyGiaTinhThue, 8));
#endif
                    if (GlobalSettings.TuDongTinhThue == "1")
                    {
                        f.HangTG.PhiBaoHiem = Math.Round(PhiBaoHiem * f.HangTG.GiaTrenHoaDon, 8);
                        f.HangTG.ChiPhiVanChuyen = Math.Round(PhiVanChuyen * f.HangTG.GiaTrenHoaDon, 8);
                        f.HangTG.TriGiaNguyenTe += Math.Round((f.HangTG.PhiBaoHiem + f.HangTG.ChiPhiVanChuyen), 8);
                        f.HangTG.TriGiaVND = Math.Round(f.HangTG.TriGiaNguyenTe * Convert.ToDouble(TKMD.TyGiaTinhThue), 8);
                    }

                    if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO ||
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET ||
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET ||
                TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                        f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
                    else
                        f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
                    f.ShowDialog(this);
                    this.Close();
                }
                break;
            }
        }

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            List<HangMauDich> hmdtemcol = new List<HangMauDich>();
            foreach (HangMauDich hmd1 in TKMD.HMDCollection)
            {
                if (hmd1.DonGiaKB <= 0) hmdtemcol.Add(hmd1);
            }
            foreach (HangMauDich hmd2 in hmdtemcol)
            {
                TKMD.HMDCollection.Remove(hmd2);
            }
            dgList.DataSource = TKMD.HMDCollection;
            if (TKMD.TrangThaiXuLy == 1)
            {

            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            double PhiBaoHiem = Math.Round(Convert.ToDouble(TKMD.PhiBaoHiem) / Convert.ToDouble(TKMD.TongTriGiaKhaiBao), 8);
            double PhiVanChuyen = Math.Round(Convert.ToDouble(TKMD.PhiVanChuyen / TKMD.TongTriGiaKhaiBao), 8);
            double PhiKhac = Math.Round(Convert.ToDouble(TKMD.PhiKhac / TKMD.TongTriGiaKhaiBao), 8);
            GridEXRow[] items = dgList.GetRows();
            foreach (GridEXRow i in items)
            {

                if (i.RowType == RowType.Record)
                {
                    HangMauDich hangMD = (HangMauDich)i.DataRow;
                    if (hangMD.DonGiaKB == 0)
                    {
                        continue;
                    }
                    HangTriGia hangTG = new HangTriGia();
                    hangTG.GiaTrenHoaDon = Math.Round(Convert.ToDouble(hangMD.DonGiaKB), 8);
                    hangTG.STTHang = hangMD.SoThuTuHang;
                    hangTG.TenHang = hangMD.TenHang;
                    hangTG.TriGiaNguyenTe = Math.Round(Convert.ToDouble(hangMD.DonGiaKB), 8);
                    hangTG.TriGiaVND = Math.Round(hangTG.GiaTrenHoaDon * Convert.ToDouble(TKMD.TyGiaTinhThue), 8);
                    if (GlobalSettings.TuDongTinhThue == "1")
                    {
                        hangTG.TienThue = Math.Round(PhiKhac * hangTG.GiaTrenHoaDon, 8);
                        hangTG.PhiBaoHiem = Math.Round(PhiBaoHiem * hangTG.GiaTrenHoaDon, 8);
                        hangTG.ChiPhiVanChuyen = Math.Round(PhiVanChuyen * hangTG.GiaTrenHoaDon, 8);
                        hangTG.TriGiaNguyenTe += Math.Round((hangTG.PhiBaoHiem + hangTG.ChiPhiVanChuyen), 8);
                        hangTG.TriGiaVND = Math.Round(hangTG.TriGiaNguyenTe * Convert.ToDouble(TKMD.TyGiaTinhThue), 8);
                    }

                    if (checkMaHangExit(hangTG.STTHang))
                    {
                        if (MLMessages("Hàng này đã có trong danh sách nên được bỏ qua. Bạn có muốn tiếp tục không?", "MSG_SEL01", "", true) != "Yes")
                        {
                            break;
                        }
                    }
                    else
                        TKTG.HTGCollection.Add(hangTG);
                }
            }
            this.Close();
        }


    }
}
