﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Messages.SmartCA
{
    public class ResponseMessage
    {
        //Chuỗi định danh response dùng để kiểm tra xem có khớp với request không
        public Guid ResponseID { get; set; }

        //Mã kết quả trả về
        public int ResponseCode { get; set; }

        //Mô tả mã kết quả
        public string ResponseContent { get; set; }

        //Dữ liệu trả về cho từ hàm định dạng json object hoặc json array.
        public object Content { get; set; }

    }
    public class ResponseAccessToken
    {
        //Giá trị access_token định dạng JWT
        public string Access_Token { get; set; }

        //Giá trị refresh_token dùng để yêu cầu access_token mới khi hết hạn
        public string Refresh_Token { get; set; }

        //Định dạng token. Mặc định là bearer
        public string Token_Type { get; set; }

        //Thời gian hiện lực của access_token tính bằng giây
        public string Expires_in { get; set; }

        //Thông tin  các access mà access token này được cấp
        public string Scope { get; set; }

        //Personal ID đã gửi vào trong email người dùng
        public string Email { get; set; }

        public string Id { get; set; }
    }
    public class CertificateSmartCAResponse
    {
        public CertRespone Cert { get; set; }
        public KeyRespone Key { get; set; }
        public string AuthMode { get; set; }
        public string Scal { get; set; }
        public string Multisign { get; set; }
        public string Status { get; set; }
    }
    public class ContenSignHash
    {
        public string tranId { get; set; }
    }
    public class CertRespone
    {
        public string Status { get; set; }
        public string SerialNumber { get; set; }
        public string SubjectDN { get; set; }
        public List<string> Certificates { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }

    }
    public class KeyRespone
    {
        public string Status { get; set; }
        public object Alg { get; set; }
        public string Len { get; set; }

    }
    public class Userinfo
    {
        //        Loại thuê bao
        //0: Khách hàng cá nhân
        //1: Khách hàng doanh nghiệp
        //2: Cá nhân trong doanh nghiệp 3: Onetime CA
        public int AccType { get; set; }

        //Loại giấy tờ (CMND, CCCD, …)
        public string UidPre { get; set; }

        //Số CMND, CCCD
        public string Uid { get; set; }

        //Địa chỉ email của thuê bao
        public string Email { get; set; }

        //Số điện thoại của thuê bao
        public string Phone { get; set; }

        public string FullName { get; set; }
        public int Gender { get; set; }
        public string Address { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Status { get; set; }
        public string StatusDesc { get; set; }

    }
    public class CredentialSmartCAResponse
    {
        public string Code { get; set; }
        public string CodeDesc { get; set; }
        public string Message { get; set; }
        public List<string> Content { get; set; }
    }
    public class Account
    {
        public string Email { get; set; }
        public string PhoneNumber { get; set; }

        public List<Group> Groups { get; set; }
    }

    public class TranInfoSmartCAResp
    {
        public int Code { get; set; }
        public string CodeDesc { get; set; }
        public string Message { get; set; }
        public TranInfoSmartCARespContent Content { get; set; }
    }
    public class SignHashSmartCAResponse
    {
        public int Code { get; set; }
        public string CodeDesc { get; set; }
        public string Message { get; set; }
        public ContenSignHash Content { get; set; }
    }
    public class TranInfoSmartCARespContent
    {
        public string refTranId { get; set; }
        public List<Documents> Documents { get; set; }
        public string TranId { get; set; }
        public string Sub { get; set; }
        public string CredentialId { get; set; }
        public string TranType { get; set; }
        public string TranTypeDesc { get; set; }
        public string TranStatus { get; set; }
        public string TranStatusDesc { get; set; }
        public DateTime ReqTime { get; set; }
    }
    public class Documents
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Size { get; set; }
        public string Data { get; set; }
        public string Hash { get; set; }
        public string Sig { get; set; }
        public string DataSigned { get; set; }
        public string Url { get; set; }
    }
    public class Group
    {
        public string ID { get; set; }
        public string AdminEmail { get; set; }
        public string TenNhom { get; set; }
    }
}
