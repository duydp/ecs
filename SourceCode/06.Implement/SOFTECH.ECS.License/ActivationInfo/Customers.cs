﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivationInfo
{
    public class CustomersInfor
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string CompanyOrDepartment { get; set; }
        public string BillingAddress { get; set; }
        public string City { get; set; }
        public string StateOrProvince { get; set; }
        public string PostalCode { get; set; }
        public string ContactTitle { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
        public string MaDN { get; set; }
    }
}
