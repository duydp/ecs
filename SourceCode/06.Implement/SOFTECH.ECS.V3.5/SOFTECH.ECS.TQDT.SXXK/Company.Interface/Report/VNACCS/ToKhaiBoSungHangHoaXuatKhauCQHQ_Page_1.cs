﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report.VNACCS
{
    public partial class ToKhaiBoSungHangHoaXuatKhauCQHQ_Page_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiBoSungHangHoaXuatKhauCQHQ_Page_1()
        {
            InitializeComponent();
        }
        public void BindReport(VAE2LY0 Vae)
        {
            KDT_VNACC_ToKhaiMauDich TKMD = new KDT_VNACC_ToKhaiMauDich();


            //lblSoTrang.Text = "1/" + Vae.K69.GetValue().ToString();
            int HangHoa = Vae.HangMD.Count;
            if (HangHoa <= 2)
            {
                lblSoTrang.Text = "1/" + System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblSoTrang.Text = "1/" + System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 2, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblSotokhai.Text = Vae.ECN.GetValue().ToString();
            lblSotokhaidautien.Text = Vae.FIC.GetValue().ToString();
            lblSonhanhtokhaichianho.Text = Vae.BNO.GetValue().ToString();
            lblTongsotokhaichianho.Text = Vae.DNO.GetValue().ToString();
            lblSotokhaitamnhaptaixuat.Text = Vae.TDN.GetValue().ToString();
            lblMaphanloaikiemtra.Text = Vae.K07.GetValue().ToString();
            lblMaloaihinh.Text = Vae.ECB.GetValue().ToString();
            lblMaphanloaihanghoa.Text = Vae.CCC.GetValue().ToString();
            lblMahieuphuongthucvanchuyen.Text = Vae.MTC.GetValue().ToString();
            lblMasothuedaidien.Text = Vae.K01.GetValue().ToString();
            lblTencoquanhaiquantiepnhantokhai.Text = Vae.K08.GetValue().ToString();
            lblNhomxulyhoso.Text = Vae.CHB.GetValue().ToString();
            if
             (Convert.ToDateTime(Vae.K10.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaydangky.Text = Convert.ToDateTime(Vae.K10.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaydangky.Text = "";
            }

            if (Vae.AD1.GetValue().ToString() != "" && Vae.AD1.GetValue().ToString() != "0")
            {
                lblGiodangky.Text = Vae.AD1.GetValue().ToString().Substring(0, 2) + ":" + Vae.AD1.GetValue().ToString().Substring(2, 2) + ":" + Vae.AD1.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiodangky.Text = "";
            }
            if
                (Convert.ToDateTime(Vae.AD2.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaythaydoidangky.Text = Convert.ToDateTime(Vae.AD2.GetValue()).ToString("dd/MM/yyyy");

            }
            else
            {
                lblNgaythaydoidangky.Text = "";
            }

            if (Vae.AD3.GetValue().ToString() != "" && Vae.AD3.GetValue().ToString() != "0")
            {
                lblGiothaydoidangky.Text = Vae.AD3.GetValue().ToString().Substring(0, 2) + ":" + Vae.AD3.GetValue().ToString().Substring(2, 2) + ":" + Vae.AD3.GetValue().ToString().Substring(4, 2);
            }
            else
            {
                lblGiothaydoidangky.Text = "";
            }
            if
                (Convert.ToDateTime(Vae.RID.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoihantainhapxuat.Text = Convert.ToDateTime(Vae.RID.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoihantainhapxuat.Text = "";
            }
            lblBieuthitruonghophethan.Text = Vae.AAA.GetValue().ToString();
            lblMaxuatkhau.Text = Vae.EPC.GetValue().ToString();
            lblTenxuatkhau.Text = Vae.EPN.GetValue().ToString();
            lblMaBuuChinhXK.Text = Vae.EPP.GetValue().ToString();
            lblDiachinguoixuat.Text = Vae.EPA.GetValue().ToString();
            lblSodtnguoixuat.Text = Vae.EPT.GetValue().ToString();
            lblManguoiuythacxuatkhau.Text = Vae.EXC.GetValue().ToString();
            lblTennguoiuythacxuatkhau.Text = Vae.EXN.GetValue().ToString();
            lblManguoinhapkhau.Text = Vae.CGC.GetValue().ToString();
            lblTennguoinhapkhau.Text = Vae.CGN.GetValue().ToString();
            lblMabuuchinh.Text = Vae.CGP.GetValue().ToString();
            lblDiachi1.Text = Vae.CGA.GetValue().ToString();
            lblDiachi2.Text = Vae.CAT.GetValue().ToString();
            lblDiachi3.Text = Vae.CAC.GetValue().ToString();
            lblDiachi4.Text = Vae.CAS.GetValue().ToString();
            lblManuoc.Text = Vae.CGK.GetValue().ToString();
            lblMadailyhaiquan.Text = Vae.K32.GetValue().ToString();
            lblTendailyhaiquan.Text = Vae.K33.GetValue().ToString();
            lblManhanvienhaiquan.Text = Vae.K34.GetValue().ToString();
            lblSovandon.Text = Vae.EKN.GetValue().ToString();
            lblSoluong.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(Vae.NO.GetValue().ToString().Replace(".", ",")));
            lblMadonvitinh.Text = Vae.NOT.GetValue().ToString();
            lblTongtrongluonghang.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(3, false), Convert.ToDecimal(Vae.GW.GetValue().ToString().Replace(".", ",")));
            lblMadonvitinhtrongluong.Text = Vae.GWT.GetValue().ToString();
            lblMadiadiemluukho.Text = Vae.ST.GetValue().ToString();
            lblTendiadiemluukho.Text = Vae.K42.GetValue().ToString();
            lblMadiadiemnhanhangcuoi.Text = Vae.DSC.GetValue().ToString();
            lblTendiadiemnhanhangcuoi.Text = Vae.DSN.GetValue().ToString();
            lblMadiadiemxephang.Text = Vae.PSC.GetValue().ToString();
            lblTendiadiemxephang.Text = Vae.PSN.GetValue().ToString();
            lblMaphuongtienvanchuyen.Text = Vae.VSC.GetValue().ToString();
            lblTenphuongtienvanchuyen.Text = Vae.VSN.GetValue().ToString();
            if
            (Convert.ToDateTime(Vae.SYM.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayhangdidukien.Text = Convert.ToDateTime(Vae.SYM.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayhangdidukien.Text = "";
            }
            lblKyhieusohieu.Text = Vae.MRK.GetValue().ToString();
            for (int i = 0; i < Vae.SS_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:

                        lblPhanloaigiayphepxuatkhau1.Text = Vae.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep1.Text = Vae.SS_.listAttribute[1].GetValueCollection(i).ToString();

                        break;
                    case 1:

                        lblPhanloaigiayphepxuatkhau2.Text = Vae.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep2.Text = Vae.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 2:

                        lblPhanloaigiayphepxuatkhau3.Text = Vae.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep3.Text = Vae.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 3:

                        lblPhanloaigiayphepxuatkhau4.Text = Vae.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep4.Text = Vae.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 4:

                        lblPhanloaigiayphepxuatkhau5.Text = Vae.SS_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSogiayphep5.Text = Vae.SS_.listAttribute[1].GetValueCollection(i).ToString();
                        break;

                }

            }
            lblPhanloaihinhthuchoadon.Text = Vae.IV1.GetValue().ToString();

            lblSohoadon.Text = Vae.IV3.GetValue().ToString();
            lblSotiepnhanhoadondientu.Text = Vae.IV2.GetValue().ToString();
            if
                (Convert.ToDateTime(Vae.IVD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayphathanh.Text = Convert.ToDateTime(Vae.IVD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayphathanh.Text = "";
            }
            lblPhuongthucthanhtoan.Text = Vae.IVP.GetValue().ToString();
            lblMadkgiahoadon.Text = Vae.IP2.GetValue().ToString();
            lblMadongtienhoadon.Text = Vae.IP3.GetValue().ToString();
            decimal Tongtrigiahoadon = Convert.ToDecimal(Vae.IP4.GetValue().ToString().Replace(".", ","));
            decimal Tygiatinhthue = Convert.ToDecimal(Vae.RC_.listAttribute[1].GetValueCollection(0).ToString().Replace(".", ","));
            decimal Tongthue = Tongtrigiahoadon * Tygiatinhthue;
            lblTongtrigiahoadon.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae.IP4.GetValue().ToString().Replace(".", ",")));
            lblMaphanloaigiahoadon.Text = Vae.IP1.GetValue().ToString();
            //lblMadongtientongthue.Text = Vae.FCD.GetValue().ToString();
            lblMadongtientongthue.Text = "VND";
            //lblTongthue.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae.FKK.GetValue().ToString().Replace(".", ",")));
            lblTongthue.Text = String.Format("{0:N2}", Tongthue);
            for (int i = 0; i < Vae.RC_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblMadongtientygiathue1.Text = Vae.RC_.listAttribute[0].GetValueCollection(i).ToString();
                        lblTygiatinhthue1.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae.RC_.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;
                    case 1:
                        lblMadongtientygiathue2.Text = Vae.RC_.listAttribute[0].GetValueCollection(i).ToString();
                        lblTygiatinhthue2.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae.RC_.listAttribute[1].GetValueCollection(i).ToString().Replace(".", ",")));
                        break;


                }

            }

            //minhnd Fix Trị giá Tờ khai
            TKMD = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(lblSotokhai.Text);
            decimal soToKhai = Convert.ToDecimal(TKMD.SoToKhai.ToString().Substring(11, 1));
            if (TKMD.TrangThaiXuLy == "1")
            {
                lblTenThongTinXuat.Text = "Tờ khai tạm hàng hóa xuất khẩu";
            }
            if (TKMD.TrangThaiXuLy == "2" && soToKhai >= 1)
            {
                lblTenThongTinXuat.Text = "Tờ khai bổ sung hàng hóa xuất khẩu (người khai Hải quan thực hiện)";
            }
            if (TKMD.TrangThaiXuLy == "3" && soToKhai >= 1)
            {
                lblTenThongTinXuat.Text = "Tờ khai bổ sung hàng hóa xuất khẩu (người khai Hải quan thực hiện)";
            }
            decimal triGiaHang = 0;
            TKMD.LoadFull();
            foreach (KDT_VNACC_HangMauDich hmd in TKMD.HangCollection)
            {
                triGiaHang += hmd.TriGiaHoaDon;
            }
            lblTonghesophanbothue.Text = triGiaHang.ToString("#,#.0000#;(#,#.0000#)");
            //minhnd Fix Trị giá Tờ khai
            //lblTonghesophanbothue.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae.TP.GetValue().ToString().Replace(".", ",")));

            lblMaphanloainhaplieutonggia.Text = Vae.K68.GetValue().ToString();
            lblPhanloaikhongcanquydoi.Text = Vae.CNV.GetValue().ToString();

            for (int i = 0; i < Vae.EA_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblPhanloaidinhkemkhaibaodientu1.Text = Vae.EA_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSodinhkemkhaibao1.Text = Vae.EA_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 1:
                        lblPhanloaidinhkemkhaibaodientu2.Text = Vae.EA_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSodinhkemkhaibao2.Text = Vae.EA_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                    case 2:
                        lblPhanloaidinhkemkhaibaodientu3.Text = Vae.EA_.listAttribute[0].GetValueCollection(i).ToString();
                        lblSodinhkemkhaibao3.Text = Vae.EA_.listAttribute[1].GetValueCollection(i).ToString();
                        break;
                }

            }

            lblNguoinopthue.Text = Vae.TPM.GetValue().ToString();
            lblMaxacdinhthoihannopthue.Text = Vae.ENC.GetValue().ToString();
            lblPhanloainopthue.Text = Vae.PAY.GetValue().ToString();
            //lblTongsotienthuexuatkhau.Text = Vae.ETA.GetValue().ToString();
            lblMatientetongtienthuexuat.Text = Vae.AD7.GetValue().ToString();
            lblTongsotienthuexuatkhau.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae.ETA.GetValue().ToString().Replace(".", ",")));
            lblTongsotienlephi.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae.TCO.GetValue()));
            //lblTongsotienbaolanh.Text = Vae.SAM.GetValue().ToString();
            lblMatientetienbaolanh.Text = Vae.AD8.GetValue().ToString();
            lblTongsotienbaolanh.Text = String.Format(Company.KDT.SHARE.Components.Globals.FormatNumber(4, false), Convert.ToDecimal(Vae.SAM.GetValue().ToString().Replace(".", ",")));
            lblTongsotokhaicuatrang.Text = Vae.K69.GetValue().ToString();
            lblTongsodonghangtokhai.Text = Vae.K70.GetValue().ToString();
            lblPhanghichu.Text = Vae.NT2.GetValue().ToString();
            lblSoquanlynoibodoanhnghiep.Text = Vae.REF.GetValue().ToString();
            lblSoquanlynguoisudung.Text = Vae.K82.GetValue().ToString();
            if
                (Convert.ToDateTime(Vae.DPD.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaykhoihanh.Text = Convert.ToDateTime(Vae.DPD.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaykhoihanh.Text = "";
            }
            for (int i = 0; i < Vae.ST_.listAttribute[0].ListValue.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        lblDiadiem.Text = Vae.ST_.listAttribute[0].GetValueCollection(i).ToString();
                        if (Convert.ToDateTime(Vae.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaydukiendentrungchuyen.Text = Convert.ToDateTime(Vae.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaydukiendentrungchuyen.Text = "";
                        }

                        if (Convert.ToDateTime(Vae.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaykhoihanhvanchuyen.Text = Convert.ToDateTime(Vae.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaykhoihanhvanchuyen.Text = "";
                        }
                        break;
                    case 1:
                        lblDiadiem2.Text = Vae.ST_.listAttribute[0].GetValueCollection(i).ToString();
                        if (Convert.ToDateTime(Vae.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaydukiendentrungchuyen2.Text = Convert.ToDateTime(Vae.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaydukiendentrungchuyen2.Text = "";
                        }
                        if (Convert.ToDateTime(Vae.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaykhoihanhvanchuyen2.Text = Convert.ToDateTime(Vae.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaykhoihanhvanchuyen2.Text = "";
                        }
                        break;
                    case 2:
                        lblDiadiem3.Text = Vae.ST_.listAttribute[0].GetValueCollection(i).ToString();
                        if (Convert.ToDateTime(Vae.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaydukiendentrungchuyen3.Text = Convert.ToDateTime(Vae.ST_.listAttribute[1].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaydukiendentrungchuyen3.Text = "";
                        }
                        if (Convert.ToDateTime(Vae.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy") != "01/01/1900")
                        {
                            lblNgaykhoihanhvanchuyen3.Text = Convert.ToDateTime(Vae.ST_.listAttribute[2].GetValueCollection(i)).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            lblNgaykhoihanhvanchuyen3.Text = "";
                        }
                        break;


                }

            }

            lblDiadiemdich.Text = Vae.ARP.GetValue().ToString();
            if
                (Convert.ToDateTime(Vae.ADT.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgaydukienden.Text = Convert.ToDateTime(Vae.ADT.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgaydukienden.Text = "";
            }





        }

        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0" || lbl.Text.Trim() == "0,00" || lbl.Text.Trim() == "0,0000")
                lbl.Text = "";
        }
    }
}
