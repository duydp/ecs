﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.DuLieuChuan ;
using System.Security.Cryptography;
using Company.Interface.Classes;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL.KDT;
namespace Company.Interface.PhongKhai
{
    public partial class CreatAccountForm : Company.Interface.BaseForm
    {
        private HeThongPhongKhai hqch = new HeThongPhongKhai();
        private HeThongPhongKhaiCollection hqpkCollection = new HeThongPhongKhaiCollection();
        private HeThongPhongKhaiCollection hqpkCollectionAll = new HeThongPhongKhaiCollection();
        //private HeThongPhongKhaiCollection hqpkCountRecord;
        private DataRowCollection dtb;
        private string _PassTemp;
        public CreatAccountForm()
        {
            InitializeComponent();
        }
        public bool IsRegister;
        private long t =0;
       // private DataRowCollection dtb;
        private HeThongPhongKhai haiquanpkch = new HeThongPhongKhai();

     
        private void Register(string strMa, string strTen, string strPass)
        {

            try
            {               
                    strMa = txtMaDoanhNghiep.Text.Trim();
                    strTen = txtTenDoanhNghiep.Text.Trim();
                    strPass = this.GetMD5Value(txtRePass.Text.Trim());
                    //Tên doanh Nghiệp
                    haiquanpkch.MaDoanhNghiep = strMa;
                    haiquanpkch.TenDoanhNghiep = strTen;
                    haiquanpkch.PassWord = strPass;
                    haiquanpkch.Key_Config = "CauHinh";
                    haiquanpkch.Value_Config = "0";
                    haiquanpkch.Role = 0; //quyền doanh nghiệp
                    t = haiquanpkch.Insert();
                    dgList.DataSource = haiquanpkch.SelectCollectionByRoleDoanhNghiep();
                    dgList.Refetch();
                                 
            }
            catch
            {
                ShowMessage("Phát hiện lỗi trong quá trình tạo tài khoản,vui lòng kiểm tra lại", false);
            }

        }
        //
        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();

        }

        //
        private void ShowLoginForm()
        {         
            LoginForm loginForm = new LoginForm();            
            loginForm.ShowDialog();
        }
              

       

        private void btnCancel_Click(object sender, EventArgs e)
        {           
                this.Close();
                     
        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void DangKyForm_Load(object sender, EventArgs e)
        {
            btnEdit.Enabled = false;
            btnCreatAccount.Enabled = true;
            btnEditAccount.Enabled = true;
            cmdDelDN.Enabled = true;
            try
            {
                hqpkCollection = new HeThongPhongKhai().SelectCollectionByRoleDoanhNghiep();
                dgList.DataSource = hqpkCollection;
                dgList.Refetch();
               

            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
          
          
           
        }

        private void DangKyForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            //this.ShowLoginForm();
            //this.Close();
        }

        private void btnCreatAccount_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            string strMa = txtMaDoanhNghiep.Text.Trim();
            string strTen = txtTenDoanhNghiep.Text.Trim();
            string strPass = txtPass.Text.Trim();
            string strRePass = txtRePass.Text.Trim();
            if (strPass.Length > 0)
            {
                if (strPass != strRePass)
                {
                    ShowMessage("Mật khẩu chưa trùng khớp !", false);                  
                    return;
                }
            }
            else
            {
                ShowMessage("Mật khẩu không được rỗng !", false);
                txtPass.Focus() ;
                return;
            }
            try
            {
                dtb = hqch.SelectLogin().Tables[0].Rows;
                string strMaRow = "";
               // string strPassRow = "";
               // int intRole = 0;
                foreach (DataRow dr in dtb)
                {
                    strMaRow = dr["MadoanhNghiep"].ToString().Trim();
                    //strPassRow = dr["PassWord"].ToString().Trim();
                    if (strMa == strMaRow )
                    {
                        ShowMessage("Tài khoản của Doanh Nghiệp này đã được đăng ký !", false);
                        return;

                    }
                    
                }
           
            }
            catch
            {

                ShowMessage("lỗi!", false);
                return;
            }
            cvError.Validate();
            if (!cvError.IsValid) return;
            this.Cursor = Cursors.WaitCursor;
            this.btnCreatAccount.Enabled = false; 
            this.Register(strMa, strTen, strPass);
            if (t > 0)
            {
                 ShowMessage("Tài khoản của doanh nghiệp đã được tạo!", false);
                 txtMaDoanhNghiep.Text = "";
                 txtPass.Text = "";
                 txtTenDoanhNghiep.Text = "";
                 txtRePass.Text = "";
            }
            else
            {
                dgList.Refresh();
                ShowMessage("Tạo Tài khoản không thành công !", false);
               

            }
            this.Cursor = Cursors.Arrow ;
            this.btnCreatAccount.Enabled = true ;
           
        }

        private void btnEditAccount_Click(object sender, EventArgs e)
        {
            AdminAccountForm f = new AdminAccountForm();
            f.ShowDialog();
        }

        private void DeleteAccountDN()
        {
            if (ShowMessage("Bạn có chắc chắn là xóa tài khoản này không ?", true) == "Yes")
            {
               
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                int count = 0;
                int t = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        try
                        {
                            HeThongPhongKhai hqchpk = (HeThongPhongKhai)i.GetRow().DataRow;
                            string maDN = hqchpk.MaDoanhNghiep;
                           // t = hqchpk.Delete();
                            t = hqchpk.DeleteALL();
                            count++;
                        }
                        catch (Exception ex)
                        {
                            ShowMessage("Lỗi : " + ex.Message, false);
                        }
                    }

                 }
                if (t > 0)
                    ShowMessage(count.ToString() + " Tài khoản đã được xóa ", false);
                else
                    ShowMessage("Không thể xóa tài khoản này ", false);
               }
            
            dgList.DataSource = haiquanpkch.SelectCollectionByRoleDoanhNghiep();
           dgList.Refetch();
        }

        private void cmdDelDN_Click(object sender, EventArgs e)
        {
            this.DeleteAccountDN();
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            this.DeleteAccountDN();
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            btnEdit.Enabled = true ;
            btnCreatAccount.Enabled = false ;
            btnEditAccount.Enabled = false ;
            cmdDelDN.Enabled = false ;
            txtMaDoanhNghiep.ReadOnly = true;
            HeThongPhongKhai hqpkcauhinh = new HeThongPhongKhai();
            GridEXSelectedItemCollection gridEXSelectedCollection = dgList.SelectedItems; ;
            foreach (GridEXSelectedItem gridEXSelectedItem in gridEXSelectedCollection)
            {
                if (gridEXSelectedItem.RowType == RowType.Record)
                {
                    hqpkcauhinh = (HeThongPhongKhai)dgList.GetRow().DataRow;
                    txtMaDoanhNghiep.Text = hqpkcauhinh.MaDoanhNghiep;
                    txtMaDoanhNghiep.Text = hqpkcauhinh.MaDoanhNghiep;
                    _PassTemp = hqpkcauhinh.PassWord;
                    txtPass.Text = "";
                    txtRePass.Text = "";
                    txtTenDoanhNghiep.Text = hqpkcauhinh.TenDoanhNghiep;
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            string strPassPre = txtPass.Text.Trim();
            string strPass = this.GetMD5Value(txtPass.Text.Trim());
            string strRepass = txtRePass.Text;
            txtMaDoanhNghiep.ReadOnly = false;
             //Kiểm tra xem mật khẩu có nhập không
            if (txtPass.Text.Trim().Length > 0)
            {
                strPass = this.GetMD5Value(txtPass.Text.Trim());
                strRepass = this.GetMD5Value(txtRePass.Text.Trim());
                if (strPass != strRepass)
                {
                    MessageBox.Show("Mật khẩu không trùng khớp, yêu cầu kiểm tra lại !");
                    return;
                }                
            }         
           
            
            btnCreatAccount.Enabled = true;
            btnEditAccount.Enabled = true;
            cmdDelDN.Enabled = true;
            HeThongPhongKhai hqpk = new HeThongPhongKhai();

            try
            {
                hqpk.MaDoanhNghiep = txtMaDoanhNghiep.Text;
                hqpk.TenDoanhNghiep = txtTenDoanhNghiep.Text;
                hqpk.Key_Config = "CauHinh";
                hqpk.Value_Config = "0";
                if (txtPass.Text.Length > 0)
                {
                    hqpk.PassWord = strPass;
                }
                else
                    hqpk.PassWord = _PassTemp;
                hqpk.Role = 0;
                int t = hqpk.Update();
                if (t > 0)
                {
                    ShowMessage("Tài khoản đã được cập nhật!", false);
                    btnEdit.Enabled = false;
                }
                else
                    ShowMessage("Cập nhật thông tin tài khoản không thành công!", false);

                dgList.DataSource = haiquanpkch.SelectCollectionByRoleDoanhNghiep();
                dgList.Refetch();
            }
            catch (Exception ex1)
            {
                ShowMessage(" Lỗi : " + ex1.Message, false);
            }
            txtTenDoanhNghiep.Text = "";
            txtMaDoanhNghiep.Text = "";
            txtPass.Text = "";
            txtPass.Text = "";
            txtRePass.Text = "";
        }

        //private void uiCommandBar1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        //{
        //    switch ( e.Command.Key )
        //    {
        //        case "cmdCauHinhCSDLDN":
        //            {
        //                this.ShowFormDN();
        //                break;
        //            }
        //        case "cmdCauHinhCSDLDBHQ":
        //            {
        //                this.ShowFormDBHQ();
        //                break;
        //            }
        //    }
        //}
        private void ShowFormDN()
        {
            ThietLapThongSoKBForm thietlapForm = new ThietLapThongSoKBForm();
            thietlapForm.ShowDialog();
        }
        private void ShowFormDBHQ()
        {
            CauHinhCSDLHaiQuanForm cauhinhDNForm = new CauHinhCSDLHaiQuanForm();
            cauhinhDNForm.ShowDialog();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            ThongTinDNAndHQForm f = new ThongTinDNAndHQForm();
            f.ShowDialog();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            ChangePINForm f = new ChangePINForm();
            f.ShowDialog();
        }
    }
}

