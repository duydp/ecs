﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
using Logger;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.Report.SXXK.TT38
{
    public partial class ToKhaiNhapXinHoan_TT38 : BaseForm
    {
        public int LanThanhLy;
        public int NamThanhLy;
        public bool Hoan_KhongThu = true;
        private DataTable tb;
        private DataTable tb_Hoan;
        HoSoThanhLyDangKy HSTL;
        string SoToKhai = "";
        //private List<String> listTK = null;
        public ToKhaiNhapXinHoan_TT38()
        {
            InitializeComponent();
        }
        private DataTable GetDataSource()
        {
            try
            {
                string spName = "[p_SXXK_BKToKhaiNhap_TT38]";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
                db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.BigInt, LanThanhLy);
                db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, NamThanhLy);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
                db.AddInParameter(dbCommand, "@Distinct", SqlDbType.Int, 1);
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void ToKhaiNhapXinHoan_TT38_Load(object sender, EventArgs e)
        {
            
            tb = GetDataSource();
            tb_Hoan = GetDataSource();
            gridTKNK.DataSource = tb;
            tb_Hoan.Rows.Clear();
            HSTL = HoSoThanhLyDangKy.LoadByLanThanhLy(LanThanhLy, NamThanhLy, MaDoanhNghiep);
            DataTable tbTam = tb;
            if (Hoan_KhongThu==false)
            {
                if (!string.IsNullOrEmpty(HSTL.ListTKNK_KhongThu))
                {
                    HSTL.ListTKNK_Hoan = HSTL.ListTKNK_Hoan.Replace("'", "");
                    string[] list = HSTL.ListTKNK_KhongThu.Split(',');
                    for (int i = 0; i < list.Length; i++)
                    {
                        DataRow[] _temp = tbTam.Select("SoToKhaiNhap ='" + list[i] + "'");

                        if (_temp.Length > 0)
                        {
                            tb_Hoan.ImportRow(_temp[0]);
                            tb.Rows.Remove(_temp[0]);
                        }

                    }
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(HSTL.ListTKNK_Hoan))
                {
                    HSTL.ListTKNK_Hoan = HSTL.ListTKNK_Hoan.Replace("'", "");
                    string[] list = HSTL.ListTKNK_Hoan.Split(',');
                    for (int i = 0; i < list.Length; i++)
                    {
                        DataRow[] _temp = tbTam.Select("SoToKhaiNhap ='" + list[i] + "'");

                        if (_temp.Length > 0)
                        {
                            tb_Hoan.ImportRow(_temp[0]);
                            tb.Rows.Remove(_temp[0]);
                        }

                    }
                }
            }
            
            gridTKNK_Hoan.DataSource = tb_Hoan;
            gridTKNK.DataSource = tb;
        }

        private bool CheckRowExist(string SoToKhai) 
        {
            foreach (Janus.Windows.GridEX.GridEXRow dr in gridTKNK_Hoan.GetRows()) 
            {
                if (dr.Cells[2].Value.ToString() == SoToKhai) 
                {
                    return true;
                }
            }
            return false;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            foreach (Janus.Windows.GridEX.GridEXRow dr in gridTKNK.GetRows())
            //foreach (DataRow dr in tb.Rows)
            {
                string stk = dr.Cells[2].Value.ToString();
                //string stk = dr[2].ToString();
                //bool flag = Convert.ToBoolean(dr[1]);
                bool flag = Convert.ToBoolean(dr.Cells[1].Value);
                if (flag) 
                //if(dr.Cells[0])
                {
                    if(!CheckRowExist(stk))
                    {
                        try
                        {
                            
                            //gridTKNK_Hoan.AddItem(dr);
                            DataRow d = tb_Hoan.NewRow();
                            d =  tb.Rows[dr.RowIndex];
                            tb_Hoan.ImportRow(d);
                            tb.Rows.RemoveAt(dr.RowIndex);
                            //gridTKNK.GetRow(dr).Delete();
                            //tb.Rows.Remove(dr);
                        }
                        catch (Exception)
                        {

                            throw;
                        }
                        
                    }
                }
            }
            gridTKNK_Hoan.DataSource = tb_Hoan;
            gridTKNK.DataSource = tb;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            foreach (Janus.Windows.GridEX.GridEXRow dr in gridTKNK_Hoan.GetRows())
            {
                string stk = dr.Cells[2].Value.ToString();
                //string stk = dr[2].ToString();
                //bool flag = Convert.ToBoolean(dr[1]);
                bool flag = Convert.ToBoolean(dr.Cells[1].Value);
                if (flag)
                {
                    try
                    {
                        DataRow d = tb.NewRow();
                        d = tb_Hoan.Rows[dr.RowIndex];
                        tb.ImportRow(d);
                        tb_Hoan.Rows.RemoveAt(dr.RowIndex);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    
                }
            }
            gridTKNK_Hoan.DataSource = tb_Hoan;
            gridTKNK.DataSource = tb;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            SoToKhai = " SoToKhaiNhap in (";
            foreach (Janus.Windows.GridEX.GridEXRow dr in gridTKNK_Hoan.GetRows())
            {
                SoToKhai += "'"+dr.Cells[2].Value.ToString()+"',";
            }
            SoToKhai += " '')";
            BKToKhaiNhapXinHoan_TT38 bk = new BKToKhaiNhapXinHoan_TT38();
            bk.LanThanhLy = LanThanhLy;
            bk.NamThanhLy = NamThanhLy;
            bk.MaDoanhNghiep = MaDoanhNghiep;
            bk.WhereCondition = SoToKhai;
            if (Hoan_KhongThu == true)
            {
                bk.bk_Hoan_KhongThu = true;
            }
            else
            {
                bk.bk_Hoan_KhongThu = false;
            }
            bk.BindReport("");
            bk.ShowPreview();
        }

        private void btnFindTKNK_Click(object sender, EventArgs e)
        {
            if (numTKNK.Text != "0") 
            {
                
                if(gridTKNK.FindAll(gridTKNK.Tables[0].Columns[2],ConditionOperator.Contains,numTKNK.Text.Trim())>0)
                {
                        foreach (GridEXSelectedItem item in gridTKNK.SelectedItems)
                        item.GetRow().IsChecked = true;
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                SoToKhai = "";
                foreach (Janus.Windows.GridEX.GridEXRow dr in gridTKNK_Hoan.GetRows())
                {
                    SoToKhai += dr.Cells[2].Value.ToString() + ",";
                }
                if(Hoan_KhongThu==false)
                    HSTL.ListTKNK_KhongThu = SoToKhai;
                else
                    HSTL.ListTKNK_Hoan = SoToKhai;
                HSTL.Update();
                MessageBox.Show("Lưu thành công.", "Thông Báo", MessageBoxButtons.OK);
            }
            catch (Exception)
            {

                //throw;
            }
            
        }
    }
}
