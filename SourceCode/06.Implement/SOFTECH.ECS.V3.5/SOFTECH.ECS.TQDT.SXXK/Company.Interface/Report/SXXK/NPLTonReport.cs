﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.BLL.SXXK.ThanhKhoan;

namespace Company.Interface.Report.SXXK
{
    public partial class NPLTonReport : DevExpress.XtraReports.UI.XtraReport
    {
        public NPLTonReport()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.PrintingSystem.ShowMarginsWarning = false;             
            TKNPLTon tknhapton = new TKNPLTon();
            DataTable dt = new DataTable();
            dt = tknhapton.TongTon();            
            dt.TableName = "v_ThongKeNPLTon";
            this.DataSource = dt;
            lblMNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenHang");
            lblTongTon.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TotalNPL", "{0:n0}");          

        }

        
    }
}
