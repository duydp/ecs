﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Net;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.Maper;
using System.Security.Cryptography.X509Certificates;
using Company.KDT.SHARE.Components;

namespace TestVNACC
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            progressBar1.MarqueeAnimationSpeed = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "Text File (*.txt)|*.txt";
            DialogResult result =  f.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtFile.Text = f.FileName;
                loadFile(f.FileName);
            }

        }
        private void loadFile(string path)
        {
            StreamReader streamReader = new StreamReader(path);
            txtInfo.Text = streamReader.ReadToEnd();
            streamReader.Close();

        }
        public string PostMultipleFiles(string url, string mess)
        {
            //string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            //boundary = mime.Boundary;
            //mess = mime.GetMime();
            //mess = mime.GetEntireBody();
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            //WebRequest httpWebRequest = WebRequest.Create(url);
           // boundary = "------739a2e7e-94f1-4c62-aaed-2e4e1f0a4aa0";
            boundary = boundary.Substring(2, boundary.Length - 2);
            httpWebRequest.ContentType = "multipart/signed; micalg = \"sha1\";boundary=\""  + boundary + "\";protocol=\"application/x-pkcs7-signature\"";
            httpWebRequest.Method = "POST";
            httpWebRequest.KeepAlive = false;
            httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
            httpWebRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
            httpWebRequest.UserAgent = "E8200001";
            httpWebRequest.ProtocolVersion = HttpVersion.Version10;
            Stream memStream = new System.IO.MemoryStream();
            
            byte[] data = UTF8Encoding.UTF8.GetBytes(mess);
            memStream.Write(data, 0, data.Length);
            

            httpWebRequest.ContentLength = memStream.Length;
            Stream requestStream = httpWebRequest.GetRequestStream();
            memStream.Position = 0;
            byte[] tempBuffer = new byte[memStream.Length];
            memStream.Read(tempBuffer, 0, tempBuffer.Length);
            memStream.Close();
            requestStream.Write(tempBuffer, 0, tempBuffer.Length);
            requestStream.Close();
            try
            {
                WebResponse webResponse = httpWebRequest.GetResponse();
                Stream stream = webResponse.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                string var = reader.ReadToEnd();
                return var;
            }
            catch (Exception ex)
            {
                throw ex;
                textBox2.Text = ex.Message;
            }
            //httpWebRequest = null;
        }
        #region     Send messages
        private byte[] postData;
        public void SendMessEdi(string url, string mess)
        {
            //string boundary = "----------------------------" + DateTime.Now.Ticks.ToString("x");
            //boundary = mime.Boundary;
            //mess = mime.GetMime();
            //mess = mime.GetEntireBody();
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            //WebRequest httpWebRequest = WebRequest.Create(url);
            // boundary = "------739a2e7e-94f1-4c62-aaed-2e4e1f0a4aa0";
            if (string.IsNullOrEmpty(boundary))
            {
                boundary = textBox2.Text;
            }
            if (string.IsNullOrEmpty(boundary))
            {
                MessageBox.Show("Boundary rỗng");
                return;
            }
            boundary = boundary.Substring(2, boundary.Length - 2);
            httpWebRequest.ContentType = "multipart/signed; micalg = \"sha1\";boundary=\"" + boundary + "\";protocol=\"application/x-pkcs7-signature\"";
            httpWebRequest.Method = "POST";
            httpWebRequest.KeepAlive = false;
            httpWebRequest.Credentials = System.Net.CredentialCache.DefaultCredentials;
            httpWebRequest.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
            httpWebRequest.UserAgent = "SOFTECH";
            httpWebRequest.ProtocolVersion = HttpVersion.Version10;
            postData = UTF8Encoding.UTF8.GetBytes(mess);
            httpWebRequest.BeginGetRequestStream(new AsyncCallback(GetRequestStreamCallback), httpWebRequest);
            button1.Enabled = false;
            LoadingProcess();
        }
        private  void GetRequestStreamCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

                // End the operation
                Stream postStream = request.EndGetRequestStream(asynchronousResult);

                //Console.WriteLine("Please enter the input data to be posted:");
                //string postData = Console.ReadLine();

                // Convert the string into a byte array. 
                // byte[] byteArray = Encoding.UTF8.GetBytes(postData);

                // Write to the request stream.
                postStream.Write(postData, 0, postData.Length);
                postStream.Close();

                // Start the asynchronous operation to get the response
                request.BeginGetResponse(new AsyncCallback(GetResponseCallback), request);
            }
            catch (Exception e)
            {
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { button1.Enabled = true; MessageBox.Show(e.Message); UnLoadingProcess(); }));
                }
                
            }
            
        }

        private  void GetResponseCallback(IAsyncResult asynchronousResult)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)asynchronousResult.AsyncState;

                // End the operation
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(asynchronousResult);
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                string responseString = streamRead.ReadToEnd();
                Company.KDT.SHARE.VNACCS.HelperVNACCS.SaveFileEdi(Application.StartupPath, new StringBuilder(responseString), "Return" + DateTime.Now.ToString("yyyyMMddhhmmss"));
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { txtResult.Text = responseString; UnLoadingProcess(); }));
                }
                else
                    textBox2.Text = responseString;
                //Console.WriteLine(responseString);
                // Close the stream object
                streamResponse.Close();
                streamRead.Close();

                // Release the HttpWebResponse
                response.Close();
                //allDone.Set();
            }
            catch (Exception e)
            {
                if (InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { button1.Enabled = true; MessageBox.Show(e.Message); UnLoadingProcess(); }));
                }
                
            }
            if (InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { button1.Enabled = true; UnLoadingProcess(); }));
            }
            else
                button1.Enabled = true;

        }
#endregion
      

        private string MessagesSend;
        private string boundary;
        //private X509Certificate2 cert;
        private void button1_Click(object sender, EventArgs e)
        {
            txtResult.Text = "";
            string url = "https://ediconn.vnaccs.customs.gov.vn/test";
            //WebClient client = new WebClient();
            //client.Headers.Add(HttpRequestHeader.ContentType, "multipart/signed");

            //client.UploadProgressChanged += new UploadProgressChangedEventHandler(client_UploadProgressChanged);
            //client.UploadStringCompleted += new UploadStringCompletedEventHandler(client_UploadStringCompleted);
            ////byte[] data = System.Text.Encoding.ASCII.GetBytes(MessagesSend);
            //client.UploadStringAsync(new Uri("https://ediconn.vnaccs.customs.gov.vn/test"), MessagesSend);
            //HttpWebRequest httprequest = (HttpWebRequest)WebRequest.Create(url);
            ///httprequest.con
            //string st = sendRequest(url, "POST", this.MessagesSend);
            ////string st2 = PostMultipleFiles(url, txtInfo.Text);
            //string st2 = 
            SendMessEdi(url, txtInfo.Text);
            //txtResult.Text = st2;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Globals.GetX509CertificatedName))
            {
                MessageBox.Show("Chưa chọn chữ kí số");
                return;
            }
            X509Certificate2 cert = Cryptography.GetStoreX509Certificate2(Globals.GetX509CertificatedName);
            try
            {
                //MessagesSend = HelperVNACCS.GetMIME(txtInfo.Text, Globals.GetX509CertificatedName).GetMessages();
                txtInfo.Text = MessagesSend;
                textBox2.Text = boundary;
            }
            catch (Exception ex)
            {

                txtInfo.Text = ex.Message;
            }

        }

        private void Form1_Load(object sender, EventArgs e)
        {
           
        }
        private string sendRequest(string url, string method, string postdata)
        {
            WebRequest rqst = HttpWebRequest.Create(url);

            // only needed, if you use HTTP AUTH
            //CredentialCache creds = new CredentialCache();
            //creds.Add(new Uri(url), "Basic", new NetworkCredential(this.Uname, this.Pwd));
            //rqst.Credentials = creds;
            rqst.Method = method;
            if (!String.IsNullOrEmpty(postdata))
            {
                //rqst.ContentType = "application/xml";
                rqst.ContentType = textBox2.Text;
                rqst.CachePolicy = new System.Net.Cache.RequestCachePolicy(System.Net.Cache.RequestCacheLevel.NoCacheNoStore);
                byte[] byteData = UTF8Encoding.UTF8.GetBytes(postdata);
                rqst.ContentLength = byteData.Length;
                using (Stream postStream = rqst.GetRequestStream())
                {
                    postStream.Write(byteData, 0, byteData.Length);
                    postStream.Close();
                }
            }
            ((HttpWebRequest)rqst).KeepAlive = false;
            StreamReader rsps = new StreamReader(rqst.GetResponse().GetResponseStream());
            string strRsps = rsps.ReadToEnd();
            return strRsps;

        }

        private void button4_Click(object sender, EventArgs e)
        {
            frmChonChuKySo f = new frmChonChuKySo();
            f.ShowDialog();
        }

        private void LoadingProcess()
        {
            progressBar1.MarqueeAnimationSpeed = 50;
        }
        private void UnLoadingProcess()
        {
            progressBar1.MarqueeAnimationSpeed = 0;
            progressBar1.Value = 0;
            progressBar1.Refresh();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SaveFileDialog f = new SaveFileDialog();
            DialogResult result = f.ShowDialog();
            if (result == DialogResult.OK)
            {
                HelperVNACCS.SaveFileEdi(f.FileName, new StringBuilder(txtResult.Text));

            }
            MessageBox.Show("Save successfull");
        }
    }
}
