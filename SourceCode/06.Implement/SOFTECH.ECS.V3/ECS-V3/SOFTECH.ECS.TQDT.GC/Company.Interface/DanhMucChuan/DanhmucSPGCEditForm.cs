﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.DanhMucChuan
{
    public partial class DanhmucSPGCEditForm : BaseForm
    {
        public DanhMucSanPhamGiaCongCollection dmSPGCCollection = new DanhMucSanPhamGiaCongCollection();
       
        public DanhmucSPGCEditForm()
        {
            InitializeComponent();
        }

        private void PTVTForm_Load(object sender, EventArgs e)
        {
            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cboDonViTinh.DataSource = this._DonViTinh;
            cboDonViTinh.DisplayMember = "Ten";
            cboDonViTinh.ValueMember = "ID";
            cboDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
        }
        private void uiButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool CheckMaSP(string masp)
        {
            foreach (DanhMucSanPhamGiaCong dmtemp in dmSPGCCollection)
            {
                if (dmtemp.MaSanPham.ToUpper().Trim() == masp.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            try
            {
                Company.KDT.SHARE.Components.DuLieuChuan.DanhMucSanPhamGiaCong dm = new Company.KDT.SHARE.Components.DuLieuChuan.DanhMucSanPhamGiaCong();
                if (CheckMaSP(txtMa.Text.Trim()))
                {
                    //ShowMessage("Loại sản phẩm này đã tồn tại trên lưới", false);
                        showMsg("MSG_0203080");
                    return;
                }
                dm.MaSanPham  = txtMa.Text.Trim(); 
                dm.MaDuKien  = txtMaDK.Text.Trim();
                dm.TenSanPham = txtTen.Text.Trim();
               // dm.DVT_ID = txtDVT.Text.Trim();
                dm.DVT_ID = cboDonViTinh.SelectedValue.ToString() ;
                dm.Insert();
                //ShowMessage("Lưu thành công.", false);             
                 showMsg("MSG_SAV02");
                this.Close();
                
            }
            catch (Exception ex)
            {
                ShowMessage(setText("Lỗi: ","Error") + ex.Message, false);
                //MLMessages("Lưu không thành công", "MSG_SAV01", "", false); 
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            try
            {
                Company.KDT.SHARE.Components.DuLieuChuan.DanhMucSanPhamGiaCong dm = new Company.KDT.SHARE.Components.DuLieuChuan.DanhMucSanPhamGiaCong();
                if (CheckMaSP(txtMa.Text.Trim()))
                {
                    //ShowMessage("Loại sản phẩm này đã tồn tại trên lưới", false);
                    showMsg("MSG_0203080");
                    return;
                }
                dm.MaSanPham = txtMa.Text.Trim();
                dm.MaDuKien = txtMaDK.Text.Trim();
                dm.TenSanPham = txtTen.Text.Trim();
                //dm.DVT_ID = txtDVT.Text.Trim();
                dm.DVT_ID =  cboDonViTinh.SelectedValue.ToString();
                //ShowMessage("Lưu thành công.", false);
                showMsg("MSG_SAV02"); 
                txtMaDK.Clear();                               
                txtMa.Text = "";
                txtMa.Focus();             
                txtTen.Text = "";
                dm.Insert();
                
            }
            catch (Exception ex)
            {
               ShowMessage(setText("Lỗi: ","Error") + ex.Message, false);
                //MLMessages("Lưu không thành công", "MSG_SAV01", "", false); 
            }
        }

       

       
    }
}

