﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
using Company.GC.BLL;
using System.Text;
using System.Security.Cryptography;

namespace Company.Interface
{
    public partial class WSForm : BaseForm
    {
        public bool IsReady = false;
        public WSForm()
        {
            InitializeComponent();
        }

        private void WSForm_Load(object sender, EventArgs e)
        {
            txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {            
            if (!ccError.IsValid)
                return;
            IsReady = true;
            txtMatKhau.Text = GetMD5Value(txtMaDoanhNghiep.Text.Trim() + "+" + txtMatKhau.Text.Trim());
            if (ckLuu.Checked)
            {
                GlobalSettings.PassWordDT = txtMatKhau.Text;
            }
            this.Close();
            
        }
        public string GetMD5Value(string data)
        {

            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            //return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper(); //TQDT Version
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper().ToLower(); //TNTT Version
        }
      
    }
}