﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.GC;
using System.Linq;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class PhuKienChung
    {
        /// <summary>
        /// ConfigPhuKien
        /// </summary> 
        /// <returns>XElement</returns>
        public static XElement ConfigPhuKien(XElement Root, PhuKienDangKy PKDK, int issuer, int function, int status)
        { 
            if (PKDK != null && Root != null)
            {
                HopDong HD = HopDong.Load(PKDK.HopDong_ID);
                CuaKhau CK = CuaKhau.Load(PKDK.MaHaiQuan);
                DonViHaiQuan DVHQ = DonViHaiQuan.Load(PKDK.MaHaiQuan);
                IList<LoaiPhuKien> lLPK = LoaiPhuKien.SelectCollectionBy_Master_ID(PKDK.ID);
                if (Root != null && Root.Elements().Count() > 0)
                {        
                    foreach (XElement element in Root.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.issuer.ToUpper()))
                            element.SetValue(issuer.ToString());
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.reference.ToUpper()))
                            element.SetValue(PKDK.ID);
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.issue.ToUpper()))
                            element.SetValue("");    
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.function.ToUpper()))
                            element.SetValue(function);    
                        if(CK!=null)
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.issueLocation.ToUpper()))
                            element.SetValue(CK.Ten);    
                        }                                                        
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.status.ToUpper()))
                            element.SetValue(status);
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.customsReference.ToUpper()))
                            element.SetValue(PKDK.GUIDSTR);        
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.acceptance.ToUpper()))
                            element.SetValue(PKDK.NgayTiepNhan);
                        if(DVHQ!=null)
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.declarationOffice.ToUpper()))
                            element.SetValue(DVHQ.Ten);
                        }
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.Agent.ToUpper()))
                        {
                            foreach(XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.name.ToUpper()))
                                    elementChild.SetValue("");
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.identity.ToUpper()))
                                    elementChild.SetValue("");
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.status.ToUpper()))
                                    elementChild.SetValue("");
                            }
                        }                                
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.Importer.ToUpper()))
                        {
                            foreach(XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.name.ToUpper()))
                                    elementChild.SetValue("");
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.identity.ToUpper()))
                                    elementChild.SetValue("");                                    
                            }
                        }
                        if (HD != null)
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.customsReference.ToUpper()))
                            {
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.reference.ToUpper()))
                                        elementChild.SetValue(HD.SoHopDong);
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.issue.ToUpper()))
                                        elementChild.SetValue(HD.NgayTiepNhan);
                                    DVHQ = DonViHaiQuan.Load(HD.MaHaiQuan);
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.declarationOffice.ToUpper()))
                                        elementChild.SetValue(DVHQ.ID);
                                }
                            }
                        }
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.SubContract.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.reference.ToUpper()))
                                    elementChild.SetValue(PKDK.SoPhuKien);
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.issue.ToUpper()))
                                    elementChild.SetValue(PKDK.NgayPhuKien);
                                if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.description.ToUpper()))
                                    elementChild.SetValue("");
                            }
                        }
                        if (element.Name.ToString().ToUpper().Equals(NodePhuKien.AdditionalInformation.ToUpper()))
                        {
                            foreach (XElement elementChild in element.Elements())
                            {
                                if (lLPK != null && lLPK.Count > 0)
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.statement.ToUpper()))
                                        elementChild.SetValue(lLPK[0].MaPhuKien);
                                }                                    
                            }
                        }
                    }
                }
            }
            return Root;
        }
    }
}
