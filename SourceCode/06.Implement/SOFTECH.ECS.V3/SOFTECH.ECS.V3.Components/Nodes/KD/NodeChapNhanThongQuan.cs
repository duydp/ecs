﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.KD
{
    class NodeChapNhanThongQuan
    {

        /// <summary>
        /// Tờ khai
        /// </summary>
        public const string Declaration = "Declaration";
        /// <summary>
        /// Loại chứng từ (Nhập khẩu=929, Xuất khẩu=930)
        /// </summary>
        public const string issuer = "issuer";
        /// <summary>
        /// Số tham chiếu tờ khai
        /// </summary>
        public const string reference = "reference";
        /// <summary>
        /// Ngày khai báo
        /// </summary>
        public const string issue = "issue";
        /// <summary>
        /// Chức năng (= 32)
        /// </summary>
        public const string function = "function";
        /// <summary>
        /// Nơi khai báo
        /// </summary>
        public const string issueLocation = "issueLocation";
        /// <summary>
        /// Trạng thái đại lý
        /// </summary>
        public const string status = "status";
        /// <summary>
        /// Số tờ khai
        /// </summary>
        public const string customsReference = "customsReference";
        /// <summary>
        /// Ngày đăng ký
        /// </summary>
        public const string acceptance = "acceptance";
        /// <summary>
        /// Đơn vị HQ khai báo
        /// </summary>
        public const string declarationOffice = "declarationOffice";
        /// <summary>
        /// Người khai HQ
        /// </summary>
        public const string Agent = "Agent";
        /// <summary>
        /// Tên người khai hải quan || Tên doanh nghiệp
        /// </summary>
        public const string name = "name";
        /// <summary>
        /// Mã người khai hải quan || Mã doanh nghiệp
        /// </summary>
        public const string identity = "identity";
        /// <summary>
        /// Doanh nghiệp XNK
        /// </summary>
        public const string Importer = "Importer";        
        /// <summary>
        /// Mã loại hình
        /// </summary>
        public const string natureOfTransaction = "natureOfTransaction";        
        /// <summary>
        /// Thông tin
        /// </summary>
        public const string AdditionalInformation = "AdditionalInformation";
        /// <summary>
        /// Thông tin chấp nhận thông quan
        /// </summary>
        public const string content = "content";
        /// <summary>
        /// Ghi chu mo ta seqquen
        /// </summary>
        public const string sequence = "sequence";        
        /// <summary>
        /// Mã nội dung phản hồi
        /// </summary>
        public const string statement = "statement";        

    }
}
