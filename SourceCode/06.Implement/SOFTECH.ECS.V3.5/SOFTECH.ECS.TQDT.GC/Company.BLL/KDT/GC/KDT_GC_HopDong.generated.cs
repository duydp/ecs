﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_HopDong : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long SoTiepNhan { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string SoHopDong { set; get; }
		public string MaHaiQuan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string MaDaiLy { set; get; }
		public DateTime NgayKy { set; get; }
		public DateTime NgayDangKy { set; get; }
		public DateTime NgayHetHan { set; get; }
		public DateTime NgayGiaHan { set; get; }
		public string NuocThue_ID { set; get; }
		public string NguyenTe_ID { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public string DonViDoiTac { set; get; }
		public string DiaChiDoiTac { set; get; }
		public string CanBoTheoDoi { set; get; }
		public string CanBoDuyet { set; get; }
		public int TrangThaiThanhKhoan { set; get; }
		public string GUIDSTR { set; get; }
		public string DeXuatKhac { set; get; }
		public string LyDoSua { set; get; }
		public short ActionStatus { set; get; }
		public string GuidReference { set; get; }
		public int NamTN { set; get; }
		public string HUONGDAN { set; get; }
		public string PhanLuong { set; get; }
		public string HuongdanPL { set; get; }
		public string DiaChiDoanhNghiep { set; get; }
		public string TenDonViDoiTac { set; get; }
		public string TenDoanhNghiep { set; get; }
		public string GhiChu { set; get; }
		public string PTTT_ID { set; get; }
		public double TongTriGiaSP { set; get; }
		public double TongTriGiaTienCong { set; get; }
		public int IsGiaCongNguoc { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_HopDong> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_HopDong> collection = new List<KDT_GC_HopDong>();
			while (reader.Read())
			{
				KDT_GC_HopDong entity = new KDT_GC_HopDong();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) entity.NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHan"))) entity.NgayHetHan = reader.GetDateTime(reader.GetOrdinal("NgayHetHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiaHan"))) entity.NgayGiaHan = reader.GetDateTime(reader.GetOrdinal("NgayGiaHan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocThue_ID"))) entity.NuocThue_ID = reader.GetString(reader.GetOrdinal("NuocThue_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViDoiTac"))) entity.DonViDoiTac = reader.GetString(reader.GetOrdinal("DonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac"))) entity.DiaChiDoiTac = reader.GetString(reader.GetOrdinal("DiaChiDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("CanBoTheoDoi"))) entity.CanBoTheoDoi = reader.GetString(reader.GetOrdinal("CanBoTheoDoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("CanBoDuyet"))) entity.CanBoDuyet = reader.GetString(reader.GetOrdinal("CanBoDuyet"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiThanhKhoan"))) entity.TrangThaiThanhKhoan = reader.GetInt32(reader.GetOrdinal("TrangThaiThanhKhoan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt16(reader.GetOrdinal("ActionStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamTN"))) entity.NamTN = reader.GetInt32(reader.GetOrdinal("NamTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("HuongdanPL"))) entity.HuongdanPL = reader.GetString(reader.GetOrdinal("HuongdanPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoanhNghiep"))) entity.DiaChiDoanhNghiep = reader.GetString(reader.GetOrdinal("DiaChiDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaSP"))) entity.TongTriGiaSP = reader.GetDouble(reader.GetOrdinal("TongTriGiaSP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTienCong"))) entity.TongTriGiaTienCong = reader.GetDouble(reader.GetOrdinal("TongTriGiaTienCong"));
				if (!reader.IsDBNull(reader.GetOrdinal("IsGiaCongNguoc"))) entity.IsGiaCongNguoc = reader.GetInt32(reader.GetOrdinal("IsGiaCongNguoc"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_HopDong> collection, long id)
        {
            foreach (KDT_GC_HopDong item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_HopDong VALUES(@SoTiepNhan, @TrangThaiXuLy, @SoHopDong, @MaHaiQuan, @MaDoanhNghiep, @MaDaiLy, @NgayKy, @NgayDangKy, @NgayHetHan, @NgayGiaHan, @NuocThue_ID, @NguyenTe_ID, @NgayTiepNhan, @DonViDoiTac, @DiaChiDoiTac, @CanBoTheoDoi, @CanBoDuyet, @TrangThaiThanhKhoan, @GUIDSTR, @DeXuatKhac, @LyDoSua, @ActionStatus, @GuidReference, @NamTN, @HUONGDAN, @PhanLuong, @HuongdanPL, @DiaChiDoanhNghiep, @TenDonViDoiTac, @TenDoanhNghiep, @GhiChu, @PTTT_ID, @TongTriGiaSP, @TongTriGiaTienCong, @IsGiaCongNguoc)";
            string update = "UPDATE t_KDT_GC_HopDong SET SoTiepNhan = @SoTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, SoHopDong = @SoHopDong, MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, MaDaiLy = @MaDaiLy, NgayKy = @NgayKy, NgayDangKy = @NgayDangKy, NgayHetHan = @NgayHetHan, NgayGiaHan = @NgayGiaHan, NuocThue_ID = @NuocThue_ID, NguyenTe_ID = @NguyenTe_ID, NgayTiepNhan = @NgayTiepNhan, DonViDoiTac = @DonViDoiTac, DiaChiDoiTac = @DiaChiDoiTac, CanBoTheoDoi = @CanBoTheoDoi, CanBoDuyet = @CanBoDuyet, TrangThaiThanhKhoan = @TrangThaiThanhKhoan, GUIDSTR = @GUIDSTR, DeXuatKhac = @DeXuatKhac, LyDoSua = @LyDoSua, ActionStatus = @ActionStatus, GuidReference = @GuidReference, NamTN = @NamTN, HUONGDAN = @HUONGDAN, PhanLuong = @PhanLuong, HuongdanPL = @HuongdanPL, DiaChiDoanhNghiep = @DiaChiDoanhNghiep, TenDonViDoiTac = @TenDonViDoiTac, TenDoanhNghiep = @TenDoanhNghiep, GhiChu = @GhiChu, PTTT_ID = @PTTT_ID, TongTriGiaSP = @TongTriGiaSP, TongTriGiaTienCong = @TongTriGiaTienCong, IsGiaCongNguoc = @IsGiaCongNguoc WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_HopDong WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLy", SqlDbType.VarChar, "MaDaiLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayGiaHan", SqlDbType.DateTime, "NgayGiaHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocThue_ID", SqlDbType.Char, "NuocThue_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViDoiTac", SqlDbType.NVarChar, "DonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CanBoTheoDoi", SqlDbType.VarChar, "CanBoTheoDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CanBoDuyet", SqlDbType.VarChar, "CanBoDuyet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, "TrangThaiThanhKhoan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActionStatus", SqlDbType.SmallInt, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamTN", SqlDbType.Int, "NamTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuongdanPL", SqlDbType.NVarChar, "HuongdanPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, "DiaChiDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, "TenDonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PTTT_ID", SqlDbType.NVarChar, "PTTT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaSP", SqlDbType.Float, "TongTriGiaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaTienCong", SqlDbType.Float, "TongTriGiaTienCong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsGiaCongNguoc", SqlDbType.Int, "IsGiaCongNguoc", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLy", SqlDbType.VarChar, "MaDaiLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayGiaHan", SqlDbType.DateTime, "NgayGiaHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocThue_ID", SqlDbType.Char, "NuocThue_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViDoiTac", SqlDbType.NVarChar, "DonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CanBoTheoDoi", SqlDbType.VarChar, "CanBoTheoDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CanBoDuyet", SqlDbType.VarChar, "CanBoDuyet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, "TrangThaiThanhKhoan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActionStatus", SqlDbType.SmallInt, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamTN", SqlDbType.Int, "NamTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuongdanPL", SqlDbType.NVarChar, "HuongdanPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, "DiaChiDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, "TenDonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PTTT_ID", SqlDbType.NVarChar, "PTTT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaSP", SqlDbType.Float, "TongTriGiaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaTienCong", SqlDbType.Float, "TongTriGiaTienCong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsGiaCongNguoc", SqlDbType.Int, "IsGiaCongNguoc", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_HopDong VALUES(@SoTiepNhan, @TrangThaiXuLy, @SoHopDong, @MaHaiQuan, @MaDoanhNghiep, @MaDaiLy, @NgayKy, @NgayDangKy, @NgayHetHan, @NgayGiaHan, @NuocThue_ID, @NguyenTe_ID, @NgayTiepNhan, @DonViDoiTac, @DiaChiDoiTac, @CanBoTheoDoi, @CanBoDuyet, @TrangThaiThanhKhoan, @GUIDSTR, @DeXuatKhac, @LyDoSua, @ActionStatus, @GuidReference, @NamTN, @HUONGDAN, @PhanLuong, @HuongdanPL, @DiaChiDoanhNghiep, @TenDonViDoiTac, @TenDoanhNghiep, @GhiChu, @PTTT_ID, @TongTriGiaSP, @TongTriGiaTienCong, @IsGiaCongNguoc)";
            string update = "UPDATE t_KDT_GC_HopDong SET SoTiepNhan = @SoTiepNhan, TrangThaiXuLy = @TrangThaiXuLy, SoHopDong = @SoHopDong, MaHaiQuan = @MaHaiQuan, MaDoanhNghiep = @MaDoanhNghiep, MaDaiLy = @MaDaiLy, NgayKy = @NgayKy, NgayDangKy = @NgayDangKy, NgayHetHan = @NgayHetHan, NgayGiaHan = @NgayGiaHan, NuocThue_ID = @NuocThue_ID, NguyenTe_ID = @NguyenTe_ID, NgayTiepNhan = @NgayTiepNhan, DonViDoiTac = @DonViDoiTac, DiaChiDoiTac = @DiaChiDoiTac, CanBoTheoDoi = @CanBoTheoDoi, CanBoDuyet = @CanBoDuyet, TrangThaiThanhKhoan = @TrangThaiThanhKhoan, GUIDSTR = @GUIDSTR, DeXuatKhac = @DeXuatKhac, LyDoSua = @LyDoSua, ActionStatus = @ActionStatus, GuidReference = @GuidReference, NamTN = @NamTN, HUONGDAN = @HUONGDAN, PhanLuong = @PhanLuong, HuongdanPL = @HuongdanPL, DiaChiDoanhNghiep = @DiaChiDoanhNghiep, TenDonViDoiTac = @TenDonViDoiTac, TenDoanhNghiep = @TenDoanhNghiep, GhiChu = @GhiChu, PTTT_ID = @PTTT_ID, TongTriGiaSP = @TongTriGiaSP, TongTriGiaTienCong = @TongTriGiaTienCong, IsGiaCongNguoc = @IsGiaCongNguoc WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_HopDong WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDaiLy", SqlDbType.VarChar, "MaDaiLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayGiaHan", SqlDbType.DateTime, "NgayGiaHan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocThue_ID", SqlDbType.Char, "NuocThue_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViDoiTac", SqlDbType.NVarChar, "DonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CanBoTheoDoi", SqlDbType.VarChar, "CanBoTheoDoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CanBoDuyet", SqlDbType.VarChar, "CanBoDuyet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, "TrangThaiThanhKhoan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ActionStatus", SqlDbType.SmallInt, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamTN", SqlDbType.Int, "NamTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HuongdanPL", SqlDbType.NVarChar, "HuongdanPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, "DiaChiDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, "TenDonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PTTT_ID", SqlDbType.NVarChar, "PTTT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaSP", SqlDbType.Float, "TongTriGiaSP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaTienCong", SqlDbType.Float, "TongTriGiaTienCong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IsGiaCongNguoc", SqlDbType.Int, "IsGiaCongNguoc", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHaiQuan", SqlDbType.Char, "MaHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDaiLy", SqlDbType.VarChar, "MaDaiLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKy", SqlDbType.DateTime, "NgayKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHan", SqlDbType.DateTime, "NgayHetHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayGiaHan", SqlDbType.DateTime, "NgayGiaHan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocThue_ID", SqlDbType.Char, "NuocThue_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguyenTe_ID", SqlDbType.Char, "NguyenTe_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViDoiTac", SqlDbType.NVarChar, "DonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, "DiaChiDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CanBoTheoDoi", SqlDbType.VarChar, "CanBoTheoDoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CanBoDuyet", SqlDbType.VarChar, "CanBoDuyet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, "TrangThaiThanhKhoan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GUIDSTR", SqlDbType.NVarChar, "GUIDSTR", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ActionStatus", SqlDbType.SmallInt, "ActionStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidReference", SqlDbType.NVarChar, "GuidReference", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamTN", SqlDbType.Int, "NamTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HUONGDAN", SqlDbType.NVarChar, "HUONGDAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLuong", SqlDbType.VarChar, "PhanLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HuongdanPL", SqlDbType.NVarChar, "HuongdanPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, "DiaChiDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, "TenDonViDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PTTT_ID", SqlDbType.NVarChar, "PTTT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaSP", SqlDbType.Float, "TongTriGiaSP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaTienCong", SqlDbType.Float, "TongTriGiaTienCong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IsGiaCongNguoc", SqlDbType.Int, "IsGiaCongNguoc", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_HopDong Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_HopDong_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_HopDong> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_HopDong> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_HopDong> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_HopDong_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_HopDong_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_HopDong_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_HopDong_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_HopDong(long soTiepNhan, int trangThaiXuLy, string soHopDong, string maHaiQuan, string maDoanhNghiep, string maDaiLy, DateTime ngayKy, DateTime ngayDangKy, DateTime ngayHetHan, DateTime ngayGiaHan, string nuocThue_ID, string nguyenTe_ID, DateTime ngayTiepNhan, string donViDoiTac, string diaChiDoiTac, string canBoTheoDoi, string canBoDuyet, int trangThaiThanhKhoan, string gUIDSTR, string deXuatKhac, string lyDoSua, short actionStatus, string guidReference, int namTN, string hUONGDAN, string phanLuong, string huongdanPL, string diaChiDoanhNghiep, string tenDonViDoiTac, string tenDoanhNghiep, string ghiChu, string pTTT_ID, double tongTriGiaSP, double tongTriGiaTienCong, int isGiaCongNguoc)
		{
			KDT_GC_HopDong entity = new KDT_GC_HopDong();	
			entity.SoTiepNhan = soTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoHopDong = soHopDong;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaDaiLy = maDaiLy;
			entity.NgayKy = ngayKy;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayHetHan = ngayHetHan;
			entity.NgayGiaHan = ngayGiaHan;
			entity.NuocThue_ID = nuocThue_ID;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.DonViDoiTac = donViDoiTac;
			entity.DiaChiDoiTac = diaChiDoiTac;
			entity.CanBoTheoDoi = canBoTheoDoi;
			entity.CanBoDuyet = canBoDuyet;
			entity.TrangThaiThanhKhoan = trangThaiThanhKhoan;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamTN = namTN;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.HuongdanPL = huongdanPL;
			entity.DiaChiDoanhNghiep = diaChiDoanhNghiep;
			entity.TenDonViDoiTac = tenDonViDoiTac;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.GhiChu = ghiChu;
			entity.PTTT_ID = pTTT_ID;
			entity.TongTriGiaSP = tongTriGiaSP;
			entity.TongTriGiaTienCong = tongTriGiaTienCong;
			entity.IsGiaCongNguoc = isGiaCongNguoc;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_HopDong_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year <= 1753 ? DBNull.Value : (object) NgayKy);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@NgayGiaHan", SqlDbType.DateTime, NgayGiaHan.Year <= 1753 ? DBNull.Value : (object) NgayGiaHan);
			db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
			db.AddInParameter(dbCommand, "@CanBoTheoDoi", SqlDbType.VarChar, CanBoTheoDoi);
			db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.VarChar, CanBoDuyet);
			db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamTN", SqlDbType.Int, NamTN);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, HuongdanPL);
			db.AddInParameter(dbCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, DiaChiDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.NVarChar, PTTT_ID);
			db.AddInParameter(dbCommand, "@TongTriGiaSP", SqlDbType.Float, TongTriGiaSP);
			db.AddInParameter(dbCommand, "@TongTriGiaTienCong", SqlDbType.Float, TongTriGiaTienCong);
			db.AddInParameter(dbCommand, "@IsGiaCongNguoc", SqlDbType.Int, IsGiaCongNguoc);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_HopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_HopDong item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_HopDong(long id, long soTiepNhan, int trangThaiXuLy, string soHopDong, string maHaiQuan, string maDoanhNghiep, string maDaiLy, DateTime ngayKy, DateTime ngayDangKy, DateTime ngayHetHan, DateTime ngayGiaHan, string nuocThue_ID, string nguyenTe_ID, DateTime ngayTiepNhan, string donViDoiTac, string diaChiDoiTac, string canBoTheoDoi, string canBoDuyet, int trangThaiThanhKhoan, string gUIDSTR, string deXuatKhac, string lyDoSua, short actionStatus, string guidReference, int namTN, string hUONGDAN, string phanLuong, string huongdanPL, string diaChiDoanhNghiep, string tenDonViDoiTac, string tenDoanhNghiep, string ghiChu, string pTTT_ID, double tongTriGiaSP, double tongTriGiaTienCong, int isGiaCongNguoc)
		{
			KDT_GC_HopDong entity = new KDT_GC_HopDong();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoHopDong = soHopDong;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaDaiLy = maDaiLy;
			entity.NgayKy = ngayKy;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayHetHan = ngayHetHan;
			entity.NgayGiaHan = ngayGiaHan;
			entity.NuocThue_ID = nuocThue_ID;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.DonViDoiTac = donViDoiTac;
			entity.DiaChiDoiTac = diaChiDoiTac;
			entity.CanBoTheoDoi = canBoTheoDoi;
			entity.CanBoDuyet = canBoDuyet;
			entity.TrangThaiThanhKhoan = trangThaiThanhKhoan;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamTN = namTN;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.HuongdanPL = huongdanPL;
			entity.DiaChiDoanhNghiep = diaChiDoanhNghiep;
			entity.TenDonViDoiTac = tenDonViDoiTac;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.GhiChu = ghiChu;
			entity.PTTT_ID = pTTT_ID;
			entity.TongTriGiaSP = tongTriGiaSP;
			entity.TongTriGiaTienCong = tongTriGiaTienCong;
			entity.IsGiaCongNguoc = isGiaCongNguoc;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_HopDong_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year <= 1753 ? DBNull.Value : (object) NgayKy);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@NgayGiaHan", SqlDbType.DateTime, NgayGiaHan.Year <= 1753 ? DBNull.Value : (object) NgayGiaHan);
			db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
			db.AddInParameter(dbCommand, "@CanBoTheoDoi", SqlDbType.VarChar, CanBoTheoDoi);
			db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.VarChar, CanBoDuyet);
			db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamTN", SqlDbType.Int, NamTN);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, HuongdanPL);
			db.AddInParameter(dbCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, DiaChiDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.NVarChar, PTTT_ID);
			db.AddInParameter(dbCommand, "@TongTriGiaSP", SqlDbType.Float, TongTriGiaSP);
			db.AddInParameter(dbCommand, "@TongTriGiaTienCong", SqlDbType.Float, TongTriGiaTienCong);
			db.AddInParameter(dbCommand, "@IsGiaCongNguoc", SqlDbType.Int, IsGiaCongNguoc);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_HopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_HopDong item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_HopDong(long id, long soTiepNhan, int trangThaiXuLy, string soHopDong, string maHaiQuan, string maDoanhNghiep, string maDaiLy, DateTime ngayKy, DateTime ngayDangKy, DateTime ngayHetHan, DateTime ngayGiaHan, string nuocThue_ID, string nguyenTe_ID, DateTime ngayTiepNhan, string donViDoiTac, string diaChiDoiTac, string canBoTheoDoi, string canBoDuyet, int trangThaiThanhKhoan, string gUIDSTR, string deXuatKhac, string lyDoSua, short actionStatus, string guidReference, int namTN, string hUONGDAN, string phanLuong, string huongdanPL, string diaChiDoanhNghiep, string tenDonViDoiTac, string tenDoanhNghiep, string ghiChu, string pTTT_ID, double tongTriGiaSP, double tongTriGiaTienCong, int isGiaCongNguoc)
		{
			KDT_GC_HopDong entity = new KDT_GC_HopDong();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoHopDong = soHopDong;
			entity.MaHaiQuan = maHaiQuan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaDaiLy = maDaiLy;
			entity.NgayKy = ngayKy;
			entity.NgayDangKy = ngayDangKy;
			entity.NgayHetHan = ngayHetHan;
			entity.NgayGiaHan = ngayGiaHan;
			entity.NuocThue_ID = nuocThue_ID;
			entity.NguyenTe_ID = nguyenTe_ID;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.DonViDoiTac = donViDoiTac;
			entity.DiaChiDoiTac = diaChiDoiTac;
			entity.CanBoTheoDoi = canBoTheoDoi;
			entity.CanBoDuyet = canBoDuyet;
			entity.TrangThaiThanhKhoan = trangThaiThanhKhoan;
			entity.GUIDSTR = gUIDSTR;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ActionStatus = actionStatus;
			entity.GuidReference = guidReference;
			entity.NamTN = namTN;
			entity.HUONGDAN = hUONGDAN;
			entity.PhanLuong = phanLuong;
			entity.HuongdanPL = huongdanPL;
			entity.DiaChiDoanhNghiep = diaChiDoanhNghiep;
			entity.TenDonViDoiTac = tenDonViDoiTac;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.GhiChu = ghiChu;
			entity.PTTT_ID = pTTT_ID;
			entity.TongTriGiaSP = tongTriGiaSP;
			entity.TongTriGiaTienCong = tongTriGiaTienCong;
			entity.IsGiaCongNguoc = isGiaCongNguoc;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_HopDong_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, MaHaiQuan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
			db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year <= 1753 ? DBNull.Value : (object) NgayKy);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@NgayHetHan", SqlDbType.DateTime, NgayHetHan.Year <= 1753 ? DBNull.Value : (object) NgayHetHan);
			db.AddInParameter(dbCommand, "@NgayGiaHan", SqlDbType.DateTime, NgayGiaHan.Year <= 1753 ? DBNull.Value : (object) NgayGiaHan);
			db.AddInParameter(dbCommand, "@NuocThue_ID", SqlDbType.Char, NuocThue_ID);
			db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@DonViDoiTac", SqlDbType.NVarChar, DonViDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac", SqlDbType.NVarChar, DiaChiDoiTac);
			db.AddInParameter(dbCommand, "@CanBoTheoDoi", SqlDbType.VarChar, CanBoTheoDoi);
			db.AddInParameter(dbCommand, "@CanBoDuyet", SqlDbType.VarChar, CanBoDuyet);
			db.AddInParameter(dbCommand, "@TrangThaiThanhKhoan", SqlDbType.Int, TrangThaiThanhKhoan);
			db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NVarChar, GUIDSTR);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.SmallInt, ActionStatus);
			db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
			db.AddInParameter(dbCommand, "@NamTN", SqlDbType.Int, NamTN);
			db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
			db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
			db.AddInParameter(dbCommand, "@HuongdanPL", SqlDbType.NVarChar, HuongdanPL);
			db.AddInParameter(dbCommand, "@DiaChiDoanhNghiep", SqlDbType.NVarChar, DiaChiDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDonViDoiTac", SqlDbType.NVarChar, TenDonViDoiTac);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.NVarChar, PTTT_ID);
			db.AddInParameter(dbCommand, "@TongTriGiaSP", SqlDbType.Float, TongTriGiaSP);
			db.AddInParameter(dbCommand, "@TongTriGiaTienCong", SqlDbType.Float, TongTriGiaTienCong);
			db.AddInParameter(dbCommand, "@IsGiaCongNguoc", SqlDbType.Int, IsGiaCongNguoc);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_HopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_HopDong item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_HopDong(long id)
		{
			KDT_GC_HopDong entity = new KDT_GC_HopDong();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_HopDong_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_HopDong_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_HopDong> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_HopDong item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}