﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.DanhMucChuan
{
    public partial class DoiTacEditForm : Company.Interface.BaseForm
    {
        private DoiTac _doiTacDoanhNghiep;
        private bool _isEdit;

        public bool IsEdit
        {
            get { return _isEdit; }
            set { _isEdit = value; }
        }

        public DoiTac DoiTacDoanhNghiep
        {
            get { return _doiTacDoanhNghiep; }
            set { _doiTacDoanhNghiep = value; }
        }

        public DoiTacEditForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            try
            {
                if (!IsEdit)
                {
                    DoiTac dt = new DoiTac();
                    dt.DiaChi = txtDiaChi.Text.Trim(); ;
                    dt.DienThoai = txtDienThoai.Text.Trim(); ;
                    dt.Email = txtMail.Text.Trim(); ;
                    dt.Fax = txtFax.Text.Trim(); ;
                    dt.GhiChu = txtGhiChu.Text.Trim(); ;
                    dt.MaCongTy = txtMa.Text.Trim(); ;
                    dt.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    dt.TenCongTy = txtTenCongTy.Text.Trim();
                    dt.Insert();
                }
                else
                {
                    DoiTacDoanhNghiep.DiaChi = txtDiaChi.Text.Trim(); ;
                    DoiTacDoanhNghiep.DienThoai = txtDienThoai.Text.Trim(); ;
                    DoiTacDoanhNghiep.Email = txtMail.Text.Trim(); ;
                    DoiTacDoanhNghiep.Fax = txtFax.Text.Trim(); ;
                    DoiTacDoanhNghiep.GhiChu = txtGhiChu.Text.Trim(); ;
                    DoiTacDoanhNghiep.MaCongTy = txtMa.Text.Trim(); ;
                    DoiTacDoanhNghiep.TenCongTy = txtTenCongTy.Text.Trim();
                    DoiTacDoanhNghiep.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    DoiTacDoanhNghiep.Update();
                }

                ShowMessage(setText("Cập nhật thành công.", "Save successfully"), false);
                this.Close();

            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void btnSaveAndNew_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            try
            {
                if (!IsEdit)
                {
                    DoiTac dt = new DoiTac();
                    dt.DiaChi = txtDiaChi.Text.Trim(); ;
                    dt.DienThoai = txtDiaChi.Text.Trim(); ;
                    dt.Email = txtMail.Text.Trim(); ;
                    dt.Fax = txtFax.Text.Trim(); ;
                    dt.GhiChu = txtGhiChu.Text.Trim(); ;
                    dt.MaCongTy = txtMa.Text.Trim(); ;
                    dt.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    dt.TenCongTy = txtTenCongTy.Text.Trim();                    
                    dt.Insert();
                }
                else
                {
                    DoiTacDoanhNghiep.DiaChi = txtDiaChi.Text.Trim(); ;
                    DoiTacDoanhNghiep.DienThoai = txtDienThoai.Text.Trim(); ;
                    DoiTacDoanhNghiep.Email = txtMail.Text.Trim(); ;
                    DoiTacDoanhNghiep.Fax = txtFax.Text.Trim(); ;
                    DoiTacDoanhNghiep.GhiChu = txtGhiChu.Text.Trim(); ;
                    DoiTacDoanhNghiep.MaCongTy = txtMa.Text.Trim(); ;
                    DoiTacDoanhNghiep.TenCongTy = txtTenCongTy.Text.Trim();
                    DoiTacDoanhNghiep.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    DoiTacDoanhNghiep.Update();
                }

                ShowMessage(setText("Cập nhật thành công.", "Save successfully"), false);
                
                txtDiaChi.Clear();
                txtDienThoai.Clear();
                txtFax.Clear();
                txtGhiChu.Clear();
                txtMa.Text = "";
                txtMa.Focus();
                txtMail.Text = "";
                txtTenCongTy.Text = "";
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void DoiTacEditForm_Load(object sender, EventArgs e)
        {
            if (IsEdit)
            {
                if (DoiTacDoanhNghiep != null)
                {
                    txtMa.Text = DoiTacDoanhNghiep.MaCongTy;
                    txtTenCongTy.Text = DoiTacDoanhNghiep.TenCongTy;
                    txtDiaChi.Text = DoiTacDoanhNghiep.DiaChi;
                    txtDienThoai.Text = DoiTacDoanhNghiep.DienThoai;
                    txtMail.Text = DoiTacDoanhNghiep.Email;
                    txtFax.Text = DoiTacDoanhNghiep.Fax;
                    txtGhiChu.Text = DoiTacDoanhNghiep.GhiChu;
                }
            }

            txtMa.Focus();
        }
    }
}

