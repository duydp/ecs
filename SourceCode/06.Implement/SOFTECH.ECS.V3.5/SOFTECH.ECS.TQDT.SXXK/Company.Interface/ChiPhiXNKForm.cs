﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ToKhai;
using Company.Interface.Report.SXXK;

namespace Company.Interface
{
    public partial class ChiPhiXNKForm : BaseForm
    {
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        public ChiPhiXNKForm()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            search();
        }

        private void search()
        {
            string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "'";
            if (txtNamTiepNhan.TextLength > 0)
            {
                where += " AND NamDangKy = " + txtNamTiepNhan.Value;
            }
            if (Convert.ToInt16(txtThangDK.Value) > 0)
            {
                where += " AND month(NgayDangKy)=" + Convert.ToInt16(txtThangDK.Value);
            }

            if (txtTenChuHang.Text.Trim().Length > 0)
            {
                if (txtTenChuHang.Text.Trim() != "*")
                    where += " AND TenChuHang LIKE '%" + txtTenChuHang.Text + "%'";
            }
            else
            {
                where += " AND TenChuHang LIKE ''";
            }

            // Thực hiện tìm kiếm.            
            this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "MaLoaiHinh, SoToKhai");
            foreach (ToKhaiMauDich tkmd in this.tkmdCollection)
            {
                if (tkmd.MaLoaiHinh.Contains("XSX"))
                {
                    if (tkmd.SoTienKhoan == 0) tkmd.SoTienKhoan = GlobalSettings.SoTienKhoanTKX;
                }
                else
                {
                    if (tkmd.SoTienKhoan == 0)
                        tkmd.SoTienKhoan = GlobalSettings.SoTienKhoanTKN;
                }
            }
            TinhTongSoTienKhoan();
            dgList.DataSource = this.tkmdCollection; 
        }

        private void ChiPhiXNKForm_Load(object sender, EventArgs e)
        {
            txtNamTiepNhan.Value = DateTime.Today.Year;
            txtThangDK.Value = DateTime.Today.Month;
            search();
        }
        private void TinhTongSoTienKhoan()
        {
            decimal TongSoTienKhoan = 0;
            foreach (ToKhaiMauDich tkmd in this.tkmdCollection)
            {
                TongSoTienKhoan += tkmd.SoTienKhoan;
            }
            if (GlobalSettings.NGON_NGU == "0")
            {
                lblTongSoTienKhoan.Text = "Tổng số tiền khoán: " + TongSoTienKhoan.ToString("n0") + " (VNĐ)";
            }
            else
            {
                lblTongSoTienKhoan.Text = "Total amount of money: " + TongSoTienKhoan.ToString("n0") + " (VND)";
            }
        }
        private void uiButton3_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.tkmdCollection.Count == 0) return;
                new ToKhaiMauDich().UpdateCollection(this.tkmdCollection);
                TinhTongSoTienKhoan();
                //ShowMessage("Lưu thành công.", false);
                MLMessages("Lưu thành công.", "MSG_SAV03", "", false);
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            ChiPhiXNKReport report = new ChiPhiXNKReport();
            report.TKMDCollection = this.tkmdCollection;
            report.BindReport();
            report.ShowPreview();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtThangDK_TextChanged(object sender, EventArgs e)
        {
            if (Convert.ToInt32(txtThangDK.Value) < 0) txtThangDK.Value = 0;
            if (Convert.ToInt32(txtThangDK.Value) > 12) txtThangDK.Value = 12;
        }
    }
}