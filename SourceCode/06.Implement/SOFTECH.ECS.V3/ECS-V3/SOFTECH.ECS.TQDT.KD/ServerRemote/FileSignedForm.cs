﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using SignRemote;
using Janus.Windows.GridEX;

namespace ServerRemote
{
    public partial class FileSignedForm : Company.Interface.BaseForm
    {
        public FileSignedForm()
        {
            InitializeComponent();
        }
        private string GetSearchWhere()
        {
            string where = " 1 = 1";
            if (!String.IsNullOrEmpty(txtNguoiGui.Text.ToString().Trim()))
                where += " AND MSG_FROM LIKE '%" + txtNguoiGui.Text.ToString().Trim() + "%'";
            DateTime dateFrom = clcTuNgay.Value;
            DateTime dateTo = clcDenNgay.Value;

            where += " AND CREATE_TIME BETWEEN '" + dateFrom.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + dateTo.ToString("yyyy-MM-dd 23:59:59") + "'";
            return where;
        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = MSG_FILE_OUTBOX.SelectCollectionDynamic(GetSearchWhere(), "ID");
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            BindData();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<MSG_FILE_OUTBOX> ItemColl = new List<MSG_FILE_OUTBOX>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ItemColl.Add((MSG_FILE_OUTBOX)i.GetRow().DataRow);
                    }

                }
                foreach (MSG_FILE_OUTBOX item in ItemColl)
                {
                    if (item.ID > 0)
                        item.Delete();
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    // Check File Open Another Program
                    MSG_FILE_OUTBOX File = new MSG_FILE_OUTBOX();
                    File = (MSG_FILE_OUTBOX)grList.GetRow().DataRow;
                    string path = Application.StartupPath + "\\Temp";
                    if (System.IO.Directory.Exists(path) == false)
                    {
                        System.IO.Directory.CreateDirectory(path);
                    }
                    string fileName = "";
                    System.IO.FileStream fs;
                    byte[] bytes;
                    fileName = path + "\\" + File.FILE_NAME;
                    //Process[] processes = Process.GetProcesses();

                    //foreach (Process process in processes)
                    //{
                    //    string filePath = process.MainModule.FileName;
                    //    if (filePath == fileName)
                    //    {
                    //        process.Kill();
                    //    }
                    //}
                    if (System.IO.File.Exists(fileName))
                    {
                        System.IO.File.Delete(fileName);
                    }
                    fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                    bytes = System.Convert.FromBase64String(File.FILE_DATA);
                    fs.Write(bytes, 0, bytes.Length);
                    fs.Close();
                    System.Diagnostics.Process.Start(fileName);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_ColumnButtonClick(object sender, Janus.Windows.GridEX.ColumnActionEventArgs e)
        {
            try
            {
                // Check File Open Another Program
                MSG_FILE_OUTBOX File = new MSG_FILE_OUTBOX();
                File = (MSG_FILE_OUTBOX)grList.GetRow().DataRow;
                string path = Application.StartupPath + "\\Temp";
                if (System.IO.Directory.Exists(path) == false)
                {
                    System.IO.Directory.CreateDirectory(path);
                }
                string fileName = "";
                System.IO.FileStream fs;
                byte[] bytes;
                fileName = path + "\\" + File.FILE_NAME;
                //Process[] processes = Process.GetProcesses();

                //foreach (Process process in processes)
                //{
                //    string filePath = process.MainModule.FileName;
                //    if (filePath == fileName)
                //    {
                //        process.Kill();
                //    }
                //}
                if (System.IO.File.Exists(fileName))
                {
                    System.IO.File.Delete(fileName);
                }
                fs = new System.IO.FileStream(fileName, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write);
                bytes = System.Convert.FromBase64String(File.FILE_DATA);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
                System.Diagnostics.Process.Start(fileName);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void FileSignedForm_Load(object sender, EventArgs e)
        {
            clcTuNgay.Value = DateTime.Now;
            clcDenNgay.Value = DateTime.Now;
            BindData();
        }
    }
}
