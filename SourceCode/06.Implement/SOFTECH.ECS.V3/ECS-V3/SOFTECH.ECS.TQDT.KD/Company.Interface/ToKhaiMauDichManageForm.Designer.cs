﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    partial class ToKhaiMauDichManageForm
    {
        private UIGroupBox uiGroupBox1;
        private GridEX dgList;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiMauDichManageForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.timToKhai1 = new Company.Interface.Controls.TimToKhai();
            this.label6 = new System.Windows.Forms.Label();
            this.ctrDonViHaiQuan = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label1 = new System.Windows.Forms.Label();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuKhaiBao = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuNhanDuLieu = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHuyKhaiBao = new System.Windows.Forms.ToolStripMenuItem();
            this.SaoChepCha = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepTK = new System.Windows.Forms.ToolStripMenuItem();
            this.saoChepHH = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuToKhaiChungTu = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuCSDaDuyet = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuSuaToKhai = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuHuyToKhai = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInToKhai = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInToKhaiTQDT = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInToKhaiTQDT_TT15 = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInToKhaiSuaDoiBoSung = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInToKhaiTriGiaPP1 = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInBangKeContainer = new System.Windows.Forms.ToolStripMenuItem();
            this.mniInAnDinhThue = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdDelete1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDelete");
            this.Export2 = new Janus.Windows.UI.CommandBars.UICommand("Export");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdHistory1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdSingleDownload = new Janus.Windows.UI.CommandBars.UICommand("cmdSingleDownload");
            this.cmdCancel = new Janus.Windows.UI.CommandBars.UICommand("cmdCancel");
            this.SaoChep = new Janus.Windows.UI.CommandBars.UICommand("SaoChep");
            this.SaoChepToKhaiHang1 = new Janus.Windows.UI.CommandBars.UICommand("SaoChepToKhaiHang");
            this.SaoChepALL1 = new Janus.Windows.UI.CommandBars.UICommand("SaoChepALL");
            this.SaoChepALL = new Janus.Windows.UI.CommandBars.UICommand("SaoChepALL");
            this.SaoChepToKhaiHang = new Janus.Windows.UI.CommandBars.UICommand("SaoChepToKhaiHang");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.DongBoDuLieu = new Janus.Windows.UI.CommandBars.UICommand("DongBoDuLieu");
            this.Export1 = new Janus.Windows.UI.CommandBars.UICommand("Export");
            this.Import1 = new Janus.Windows.UI.CommandBars.UICommand("Import");
            this.Export = new Janus.Windows.UI.CommandBars.UICommand("Export");
            this.Import = new Janus.Windows.UI.CommandBars.UICommand("Import");
            this.cmdXuatDuLieuChoPhongKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuChoPhongKhai");
            this.cmdCSDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdCSDaDuyet");
            this.cmdNhanPhanLuong = new Janus.Windows.UI.CommandBars.UICommand("cmdNhanPhanLuong");
            this.cmdLayPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoi");
            this.cmdHuyToKhaiDaDuyet = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyToKhaiDaDuyet");
            this.cmdSuaToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaToKhai");
            this.cmdDelete = new Janus.Windows.UI.CommandBars.UICommand("cmdDelete");
            this.cmdHistory = new Janus.Windows.UI.CommandBars.UICommand("cmdHistory");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(821, 402);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.timToKhai1);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.ctrDonViHaiQuan);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 28);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(821, 402);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // timToKhai1
            // 
            this.timToKhai1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.timToKhai1.BackColor = System.Drawing.Color.Transparent;
            this.timToKhai1.Location = new System.Drawing.Point(42, 30);
            this.timToKhai1.Name = "timToKhai1";
            this.timToKhai1.Size = new System.Drawing.Size(776, 24);
            this.timToKhai1.TabIndex = 313;
            this.timToKhai1.Search += new System.EventHandler<Company.Interface.Controls.StringEventArgs>(this.timToKhai1_Search);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Blue;
            this.label6.Location = new System.Drawing.Point(550, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(204, 13);
            this.label6.TabIndex = 295;
            this.label6.Text = "Hướng dẫn: Kích đôi để xem chi tiết";
            // 
            // ctrDonViHaiQuan
            // 
            this.ctrDonViHaiQuan.BackColor = System.Drawing.Color.Transparent;
            this.ctrDonViHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDonViHaiQuan.Location = new System.Drawing.Point(122, 3);
            this.ctrDonViHaiQuan.Ma = "";
            this.ctrDonViHaiQuan.MaCuc = "";
            this.ctrDonViHaiQuan.Name = "ctrDonViHaiQuan";
            this.ctrDonViHaiQuan.ReadOnly = false;
            this.ctrDonViHaiQuan.Size = new System.Drawing.Size(426, 22);
            this.ctrDonViHaiQuan.TabIndex = 1;
            this.ctrDonViHaiQuan.VisualStyleManager = this.vsmMain;
            this.ctrDonViHaiQuan.ValueChanged += new Company.Interface.Controls.DonViHaiQuanControl.ValueChangedEventHandler(this.ctrDonViHaiQuan_ValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hải quan tiếp nhận";
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingRowFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ContextMenuStrip = this.contextMenuStrip1;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(1, 61);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(818, 338);
            this.dgList.TabIndex = 17;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.SelectionChanged += new System.EventHandler(this.dgList_SelectionChanged);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuKhaiBao,
            this.mnuNhanDuLieu,
            this.mnuHuyKhaiBao,
            this.SaoChepCha,
            this.mnuCSDaDuyet,
            this.mnuSuaToKhai,
            this.mnuHuyToKhai,
            this.mniInToKhai});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(179, 180);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // mnuKhaiBao
            // 
            this.mnuKhaiBao.Image = global::Company.Interface.Properties.Resources.Send16;
            this.mnuKhaiBao.Name = "mnuKhaiBao";
            this.mnuKhaiBao.Size = new System.Drawing.Size(178, 22);
            this.mnuKhaiBao.Text = "Khai báo";
            this.mnuKhaiBao.Click += new System.EventHandler(this.mnuKhaiBao_Click);
            // 
            // mnuNhanDuLieu
            // 
            this.mnuNhanDuLieu.Image = global::Company.Interface.Properties.Resources.Reload02_16;
            this.mnuNhanDuLieu.Name = "mnuNhanDuLieu";
            this.mnuNhanDuLieu.Size = new System.Drawing.Size(178, 22);
            this.mnuNhanDuLieu.Text = "Nhận phản hồi";
            this.mnuNhanDuLieu.Click += new System.EventHandler(this.mnuNhanDuLieu_Click);
            // 
            // mnuHuyKhaiBao
            // 
            this.mnuHuyKhaiBao.Image = global::Company.Interface.Properties.Resources.cmdCancel_Icon;
            this.mnuHuyKhaiBao.Name = "mnuHuyKhaiBao";
            this.mnuHuyKhaiBao.Size = new System.Drawing.Size(178, 22);
            this.mnuHuyKhaiBao.Text = "Hủy khai báo";
            this.mnuHuyKhaiBao.Click += new System.EventHandler(this.mnuHuyKhaiBao_Click);
            // 
            // SaoChepCha
            // 
            this.SaoChepCha.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.saoChepTK,
            this.saoChepHH,
            this.mnuToKhaiChungTu});
            this.SaoChepCha.Image = global::Company.Interface.Properties.Resources.copy;
            this.SaoChepCha.Name = "SaoChepCha";
            this.SaoChepCha.Size = new System.Drawing.Size(178, 22);
            this.SaoChepCha.Text = "Sao chép";
            // 
            // saoChepTK
            // 
            this.saoChepTK.Name = "saoChepTK";
            this.saoChepTK.Size = new System.Drawing.Size(237, 22);
            this.saoChepTK.Text = "Tờ khai";
            this.saoChepTK.Click += new System.EventHandler(this.saoChepTK_Click);
            // 
            // saoChepHH
            // 
            this.saoChepHH.Name = "saoChepHH";
            this.saoChepHH.Size = new System.Drawing.Size(237, 22);
            this.saoChepHH.Text = "Tờ khai + Hàng ";
            this.saoChepHH.Click += new System.EventHandler(this.saoChepHH_Click);
            // 
            // mnuToKhaiChungTu
            // 
            this.mnuToKhaiChungTu.Name = "mnuToKhaiChungTu";
            this.mnuToKhaiChungTu.Size = new System.Drawing.Size(237, 22);
            this.mnuToKhaiChungTu.Text = "Tờ khai + Hàng + Chứng từ";
            this.mnuToKhaiChungTu.Click += new System.EventHandler(this.mnuToKhaiChungTu_Click);
            // 
            // mnuCSDaDuyet
            // 
            this.mnuCSDaDuyet.Image = global::Company.Interface.Properties.Resources.apply2;
            this.mnuCSDaDuyet.Name = "mnuCSDaDuyet";
            this.mnuCSDaDuyet.Size = new System.Drawing.Size(178, 22);
            this.mnuCSDaDuyet.Text = "Chuyển trạng thái";
            this.mnuCSDaDuyet.Click += new System.EventHandler(this.mnuCSDaDuyet_Click);
            // 
            // mnuSuaToKhai
            // 
            this.mnuSuaToKhai.Image = global::Company.Interface.Properties.Resources.app_edit;
            this.mnuSuaToKhai.Name = "mnuSuaToKhai";
            this.mnuSuaToKhai.Size = new System.Drawing.Size(178, 22);
            this.mnuSuaToKhai.Text = "Sửa tờ khai";
            this.mnuSuaToKhai.Click += new System.EventHandler(this.mnuSuaToKhai_Click);
            // 
            // mnuHuyToKhai
            // 
            this.mnuHuyToKhai.Image = global::Company.Interface.Properties.Resources.delete;
            this.mnuHuyToKhai.Name = "mnuHuyToKhai";
            this.mnuHuyToKhai.Size = new System.Drawing.Size(178, 22);
            this.mnuHuyToKhai.Text = "Hủy tờ khai";
            this.mnuHuyToKhai.Click += new System.EventHandler(this.mnuHuyToKhai_Click);
            // 
            // mniInToKhai
            // 
            this.mniInToKhai.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mniInToKhaiTQDT,
            this.mniInToKhaiTQDT_TT15,
            this.mniInToKhaiSuaDoiBoSung,
            this.mniInToKhaiTriGiaPP1,
            this.mniInBangKeContainer,
            this.mniInAnDinhThue});
            this.mniInToKhai.Image = ((System.Drawing.Image)(resources.GetObject("mniInToKhai.Image")));
            this.mniInToKhai.Name = "mniInToKhai";
            this.mniInToKhai.Size = new System.Drawing.Size(178, 22);
            this.mniInToKhai.Text = "In tờ khai";
            // 
            // mniInToKhaiTQDT
            // 
            this.mniInToKhaiTQDT.Image = ((System.Drawing.Image)(resources.GetObject("mniInToKhaiTQDT.Image")));
            this.mniInToKhaiTQDT.Name = "mniInToKhaiTQDT";
            this.mniInToKhaiTQDT.Size = new System.Drawing.Size(283, 22);
            this.mniInToKhaiTQDT.Text = "In tờ khai thông quan";
            this.mniInToKhaiTQDT.Click += new System.EventHandler(this.mniInToKhaiTQDT_Click);
            // 
            // mniInToKhaiTQDT_TT15
            // 
            this.mniInToKhaiTQDT_TT15.Image = ((System.Drawing.Image)(resources.GetObject("mniInToKhaiTQDT_TT15.Image")));
            this.mniInToKhaiTQDT_TT15.Name = "mniInToKhaiTQDT_TT15";
            this.mniInToKhaiTQDT_TT15.Size = new System.Drawing.Size(283, 22);
            this.mniInToKhaiTQDT_TT15.Text = "In tờ khai thông quan (Thông tư 196)";
            this.mniInToKhaiTQDT_TT15.Click += new System.EventHandler(this.mniInToKhaiTQDT_TT15_Click);
            // 
            // mniInToKhaiSuaDoiBoSung
            // 
            this.mniInToKhaiSuaDoiBoSung.Image = ((System.Drawing.Image)(resources.GetObject("mniInToKhaiSuaDoiBoSung.Image")));
            this.mniInToKhaiSuaDoiBoSung.Name = "mniInToKhaiSuaDoiBoSung";
            this.mniInToKhaiSuaDoiBoSung.Size = new System.Drawing.Size(283, 22);
            this.mniInToKhaiSuaDoiBoSung.Text = "In tờ khai điện tử sửa đổi bổ sung";
            this.mniInToKhaiSuaDoiBoSung.Click += new System.EventHandler(this.mniInToKhaiSuaDoiBoSung_Click);
            // 
            // mniInToKhaiTriGiaPP1
            // 
            this.mniInToKhaiTriGiaPP1.Image = ((System.Drawing.Image)(resources.GetObject("mniInToKhaiTriGiaPP1.Image")));
            this.mniInToKhaiTriGiaPP1.Name = "mniInToKhaiTriGiaPP1";
            this.mniInToKhaiTriGiaPP1.Size = new System.Drawing.Size(283, 22);
            this.mniInToKhaiTriGiaPP1.Text = "In tờ khai trị giá (Phương pháp 1)";
            this.mniInToKhaiTriGiaPP1.Click += new System.EventHandler(this.mniInToKhaiTriGiaPP1_Click);
            // 
            // mniInBangKeContainer
            // 
            this.mniInBangKeContainer.Image = ((System.Drawing.Image)(resources.GetObject("mniInBangKeContainer.Image")));
            this.mniInBangKeContainer.Name = "mniInBangKeContainer";
            this.mniInBangKeContainer.Size = new System.Drawing.Size(283, 22);
            this.mniInBangKeContainer.Text = "In bảng kê Container";
            this.mniInBangKeContainer.Click += new System.EventHandler(this.mniInBangKeContainer_Click);
            // 
            // mniInAnDinhThue
            // 
            this.mniInAnDinhThue.Image = ((System.Drawing.Image)(resources.GetObject("mniInAnDinhThue.Image")));
            this.mniInAnDinhThue.Name = "mniInAnDinhThue";
            this.mniInAnDinhThue.Size = new System.Drawing.Size(283, 22);
            this.mniInAnDinhThue.Text = "In ấn định thuế tờ khai";
            this.mniInAnDinhThue.Click += new System.EventHandler(this.mniInAnDinhThue_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            this.ImageList1.Images.SetKeyName(4, "page_excel.png");
            this.ImageList1.Images.SetKeyName(5, "report.png");
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend,
            this.cmdSingleDownload,
            this.cmdCancel,
            this.SaoChep,
            this.SaoChepALL,
            this.SaoChepToKhaiHang,
            this.cmdPrint,
            this.XacNhan,
            this.DongBoDuLieu,
            this.Export,
            this.Import,
            this.cmdXuatDuLieuChoPhongKhai,
            this.cmdCSDaDuyet,
            this.cmdNhanPhanLuong,
            this.cmdLayPhanHoi,
            this.cmdHuyToKhaiDaDuyet,
            this.cmdSuaToKhai,
            this.cmdDelete,
            this.cmdHistory});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdDelete1,
            this.Export2,
            this.Separator1,
            this.cmdHistory1,
            this.Separator2});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.ImageList = this.ImageList1;
            this.uiCommandBar1.Key = "cmdBar";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(821, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdDelete1
            // 
            this.cmdDelete1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdDelete1.Icon")));
            this.cmdDelete1.Key = "cmdDelete";
            this.cmdDelete1.Name = "cmdDelete1";
            // 
            // Export2
            // 
            this.Export2.ImageIndex = 4;
            this.Export2.Key = "Export";
            this.Export2.Name = "Export2";
            this.Export2.Text = "Xuất dữ liệu";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdHistory1
            // 
            this.cmdHistory1.ImageIndex = 5;
            this.cmdHistory1.Key = "cmdHistory";
            this.cmdHistory1.Name = "cmdHistory1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdSend
            // 
            this.cmdSend.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend.Icon")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdSingleDownload
            // 
            this.cmdSingleDownload.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSingleDownload.Icon")));
            this.cmdSingleDownload.Key = "cmdSingleDownload";
            this.cmdSingleDownload.Name = "cmdSingleDownload";
            this.cmdSingleDownload.Shortcut = System.Windows.Forms.Shortcut.CtrlD;
            this.cmdSingleDownload.Text = "Nhận dữ liệu";
            this.cmdSingleDownload.ToolTipText = "Nhận dữ liệu mới của tờ khai đang chọn (Ctrl + D)";
            // 
            // cmdCancel
            // 
            this.cmdCancel.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCancel.Icon")));
            this.cmdCancel.Key = "cmdCancel";
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdCancel.Text = "Hủy khai báo";
            this.cmdCancel.ToolTipText = "Hủy khai báo (Ctrl + S)";
            // 
            // SaoChep
            // 
            this.SaoChep.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.SaoChepToKhaiHang1,
            this.SaoChepALL1});
            this.SaoChep.Icon = ((System.Drawing.Icon)(resources.GetObject("SaoChep.Icon")));
            this.SaoChep.Key = "SaoChep";
            this.SaoChep.Name = "SaoChep";
            this.SaoChep.Text = "Sao chép";
            // 
            // SaoChepToKhaiHang1
            // 
            this.SaoChepToKhaiHang1.Icon = ((System.Drawing.Icon)(resources.GetObject("SaoChepToKhaiHang1.Icon")));
            this.SaoChepToKhaiHang1.Key = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang1.Name = "SaoChepToKhaiHang1";
            this.SaoChepToKhaiHang1.Text = "Tờ khai";
            // 
            // SaoChepALL1
            // 
            this.SaoChepALL1.Icon = ((System.Drawing.Icon)(resources.GetObject("SaoChepALL1.Icon")));
            this.SaoChepALL1.Key = "SaoChepALL";
            this.SaoChepALL1.Name = "SaoChepALL1";
            this.SaoChepALL1.Text = "Tờ khai+hàng";
            // 
            // SaoChepALL
            // 
            this.SaoChepALL.Key = "SaoChepALL";
            this.SaoChepALL.Name = "SaoChepALL";
            this.SaoChepALL.Text = "Sao chép tờ khai";
            // 
            // SaoChepToKhaiHang
            // 
            this.SaoChepToKhaiHang.Key = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang.Name = "SaoChepToKhaiHang";
            this.SaoChepToKhaiHang.Text = "Sao chép cả hàng";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "In tờ khai";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận thông tin";
            // 
            // DongBoDuLieu
            // 
            this.DongBoDuLieu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.Export1,
            this.Import1});
            this.DongBoDuLieu.Key = "DongBoDuLieu";
            this.DongBoDuLieu.Name = "DongBoDuLieu";
            this.DongBoDuLieu.Text = "Đồng bộ dữ liệu";
            // 
            // Export1
            // 
            this.Export1.Icon = ((System.Drawing.Icon)(resources.GetObject("Export1.Icon")));
            this.Export1.Key = "Export";
            this.Export1.Name = "Export1";
            // 
            // Import1
            // 
            this.Import1.Icon = ((System.Drawing.Icon)(resources.GetObject("Import1.Icon")));
            this.Import1.Key = "Import";
            this.Import1.Name = "Import1";
            // 
            // Export
            // 
            this.Export.Key = "Export";
            this.Export.Name = "Export";
            this.Export.Text = "Export dữ liệu";
            // 
            // Import
            // 
            this.Import.Key = "Import";
            this.Import.Name = "Import";
            this.Import.Text = "Import dữ liệu";
            // 
            // cmdXuatDuLieuChoPhongKhai
            // 
            this.cmdXuatDuLieuChoPhongKhai.Key = "cmdXuatDuLieuChoPhongKhai";
            this.cmdXuatDuLieuChoPhongKhai.Name = "cmdXuatDuLieuChoPhongKhai";
            this.cmdXuatDuLieuChoPhongKhai.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // cmdCSDaDuyet
            // 
            this.cmdCSDaDuyet.Image = ((System.Drawing.Image)(resources.GetObject("cmdCSDaDuyet.Image")));
            this.cmdCSDaDuyet.Key = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Name = "cmdCSDaDuyet";
            this.cmdCSDaDuyet.Shortcut = System.Windows.Forms.Shortcut.CtrlT;
            this.cmdCSDaDuyet.Text = "Chuyển Trạng Thái";
            // 
            // cmdNhanPhanLuong
            // 
            this.cmdNhanPhanLuong.Key = "cmdNhanPhanLuong";
            this.cmdNhanPhanLuong.Name = "cmdNhanPhanLuong";
            this.cmdNhanPhanLuong.Shortcut = System.Windows.Forms.Shortcut.CtrlL;
            this.cmdNhanPhanLuong.Text = "Nhận trạng thái phân luồn";
            // 
            // cmdLayPhanHoi
            // 
            this.cmdLayPhanHoi.Key = "cmdLayPhanHoi";
            this.cmdLayPhanHoi.Name = "cmdLayPhanHoi";
            this.cmdLayPhanHoi.Text = "Lấy phản hồi";
            // 
            // cmdHuyToKhaiDaDuyet
            // 
            this.cmdHuyToKhaiDaDuyet.Key = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Name = "cmdHuyToKhaiDaDuyet";
            this.cmdHuyToKhaiDaDuyet.Text = "Hủy tờ khai đã duyệt";
            // 
            // cmdSuaToKhai
            // 
            this.cmdSuaToKhai.Key = "cmdSuaToKhai";
            this.cmdSuaToKhai.Name = "cmdSuaToKhai";
            this.cmdSuaToKhai.Text = "Sửa tờ khai";
            // 
            // cmdDelete
            // 
            this.cmdDelete.Key = "cmdDelete";
            this.cmdDelete.Name = "cmdDelete";
            this.cmdDelete.Text = "Xóa tờ khai";
            // 
            // cmdHistory
            // 
            this.cmdHistory.Key = "cmdHistory";
            this.cmdHistory.Name = "cmdHistory";
            this.cmdHistory.Text = "Kết quả xử lý";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(821, 28);
            // 
            // uiRebar1
            // 
            this.uiRebar1.CommandManager = this.cmMain;
            this.uiRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiRebar1.Location = new System.Drawing.Point(0, 0);
            this.uiRebar1.Name = "uiRebar1";
            this.uiRebar1.Size = new System.Drawing.Size(810, 0);
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            this.saveFileDialog1.RestoreDirectory = true;
            // 
            // ToKhaiMauDichManageForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(821, 430);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiMauDichManageForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Tag = "ToKhaiMauDichManageForm";
            this.Text = "Theo dõi thông tin tờ khai";
            this.Load += new System.EventHandler(this.ToKhaiMauDichManageForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiRebar1)).EndInit();
            this.ResumeLayout(false);

        }

       
        #endregion

        private Company.Interface.Controls.DonViHaiQuanControl ctrDonViHaiQuan;
        private Label label1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar uiRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdSingleDownload;
        private Janus.Windows.UI.CommandBars.UICommand cmdCancel;
        private Janus.Windows.UI.CommandBars.UICommand SaoChep;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepToKhaiHang1;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepALL1;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepALL;
        private Janus.Windows.UI.CommandBars.UICommand SaoChepToKhaiHang;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem SaoChepCha;
        private ToolStripMenuItem saoChepTK;
        private ToolStripMenuItem saoChepHH;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint;
        private Janus.Windows.UI.CommandBars.UICommand cmdPrint1;
        private Janus.Windows.UI.CommandBars.UICommand XacNhan;
        private Janus.Windows.UI.CommandBars.UICommand DongBoDuLieu;
        private Janus.Windows.UI.CommandBars.UICommand Export1;
        private Janus.Windows.UI.CommandBars.UICommand Import1;
        private Janus.Windows.UI.CommandBars.UICommand Export;
        private Janus.Windows.UI.CommandBars.UICommand Import;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private Janus.Windows.UI.CommandBars.UICommand cmdXuatDuLieuChoPhongKhai;
        private Janus.Windows.UI.CommandBars.UICommand cmdCSDaDuyet;
        private ToolStripMenuItem mnuCSDaDuyet;
        private Janus.Windows.UI.CommandBars.UICommand cmdNhanPhanLuong;
        private ToolStripMenuItem mnuToKhaiChungTu;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayPhanHoi;
        private ToolStripMenuItem mnuSuaToKhai;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuyToKhaiDaDuyet;
        private Janus.Windows.UI.CommandBars.UICommand cmdSuaToKhai;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private ToolStripMenuItem mnuHuyToKhai;
        private ToolStripMenuItem mnuKhaiBao;
        private ToolStripMenuItem mnuNhanDuLieu;
        private ToolStripMenuItem mnuHuyKhaiBao;
        private ImageList ImageList1;
        private Label label6;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDelete1;
        private Janus.Windows.UI.CommandBars.UICommand cmdDelete;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHistory;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private Janus.Windows.UI.CommandBars.UICommand Export2;
        private Company.Interface.Controls.TimToKhai timToKhai1;
        private ToolStripMenuItem mniInToKhai;
        private ToolStripMenuItem mniInToKhaiTQDT;
        private ToolStripMenuItem mniInToKhaiSuaDoiBoSung;
        private ToolStripMenuItem mniInBangKeContainer;
        private ToolStripMenuItem mniInAnDinhThue;
        private ToolStripMenuItem mniInToKhaiTQDT_TT15;
        private ToolStripMenuItem mniInToKhaiTriGiaPP1;
    }
}
