using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Globalization;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.Common;
using System.Data.SqlClient;
using Logger;

namespace Company.Interface.Report.SXXK
{
    public partial class BCThueXNK_TT196 : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 0;
        private decimal TongTienThueTKTiep = 0;
        private decimal TongTienThueHoan = 0;
        private decimal TongLuongSuDung = 0;
        private decimal TongTienThueHoanTatCa = 0;
        private decimal TongTienThueHoanCuaToKhai = 0;
        private decimal TongThueNKPhaiNopCuaToKhai = 0;
        private decimal TongTienThueTKTiepCuaToKhai = 0;
        public int SoHoSo;
        public bool isHoanThue = false;
        public int NamThanhLy;
        public string MaDoanhNghiep;

        public BCThueXNK_TT196()
        {
            InitializeComponent();
            lblTongTienThueTKTiep.AfterPrint += new EventHandler(lblTongTienThueTKTiep_AfterPrint);
            lblTongThueKhongThu.BeforePrint += new System.Drawing.Printing.PrintEventHandler(lblTongThueKhongThu_BeforePrint);
            lblTongThueLanSauTK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(lblTongThueLanSauTK_BeforePrint);
        }

        void lblTongThueLanSauTK_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongThueLanSauTK.Text = TongTienThueTKTiep.ToString("N0");
        }

        void lblTongThueKhongThu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongThueKhongThu.Text = TongTienThueHoanTatCa.ToString("N0");
        }

        void lblTongTienThueTKTiep_AfterPrint(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(((DevExpress.XtraReports.UI.XRTableCell)sender).Text))
                TongTienThueTKTiep = TongTienThueTKTiep + System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);//(decimal)GetCurrentColumnValue("ThuePhaiThu");
        }
        public void BindReport(string where)
        {
            DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontBCXNT);
            DataTable dataSource = new DataTable();
            dataSource = GetDataSource();
            DetailReport.DataSource = dataSource;
            lblSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "TenNPL");
            lblTenDVT_NPL.DataBindings.Add("Text", DetailReport.DataSource, "DVT");
            lblSoToKhaiNhap.DataBindings.Add("Text", DetailReport.DataSource, "SoToKhai");
            lblTongTienThueHoan.DataBindings.Add("Text", DetailReport.DataSource, "TienThueHoan",Company.KDT.SHARE.Components.Globals.FormatNumber(0));
            lblTongTienThueTKTiep.DataBindings.Add("Text", DetailReport.DataSource, "ThuePhaiThu", Company.KDT.SHARE.Components.Globals.FormatNumber(0));
//             lbl.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
//             lblLuongNPLSuDung.DataBindings.Add("Text", DetailReport.DataSource, "LuongSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
//             lblLuongNPLTaiXuat.DataBindings.Add("Text", DetailReport.DataSource, "LuongTaiXuat", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
//             lblLuongTonCuoi.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonCuoi", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
//             lblXuLy.DataBindings.Add("Text", DetailReport.DataSource, "XuLy");


            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            if (SoHoSo > 0)
            {
                // khanhhn 20/02/2012 - Xóa Từ Xin Hoàn (Theo yêu cầu của Grozbecker)
                lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
                lblSHSTK.Width = 200;
            }
        
            //HQ
            xrLabel76.Text = Properties.Settings.Default.TieudeNgay;
            //DN
            xrLabel5.Text = GlobalSettings.TieudeNgay;
        }
        private DataTable GetDataSource()
        {
            try
            {

                string spName = "[p_SXXK_BCThueXNK_TT196]";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
                db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.BigInt, LanThanhLy);
                db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, NamThanhLy);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
                db.AddInParameter(dbCommand, "@SapXepNgayDangKyTKN", SqlDbType.VarChar, GlobalSettings.SoThapPhan.SapXepTheoTK == 0 ? 0 : 1);
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        private void lblTongTienThueHoan_AfterPrint(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(((DevExpress.XtraReports.UI.XRTableCell)sender).Text))
                TongTienThueHoanTatCa = TongTienThueHoanTatCa + System.Convert.ToDecimal(((DevExpress.XtraReports.UI.XRTableCell)sender).Text);
        }

        
      

       
       
    }
}
