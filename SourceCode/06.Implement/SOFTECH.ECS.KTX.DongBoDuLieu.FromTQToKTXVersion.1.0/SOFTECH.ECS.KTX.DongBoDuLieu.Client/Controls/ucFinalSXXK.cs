﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace DongBo.Controls
{
    public partial class ucFinalSXXK : ucDesign
    {
        frmMain frmParent = null;
        public ucFinalSXXK()
        {
            InitializeComponent();
            frmParent = (frmMain)Application.OpenForms["frmMain"];
        }

        private void chkTiepTuc_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox obj = (CheckBox)sender;
            if (obj.Checked)
            {
                frmParent.btnNext.Text = "Tiếp tục";
            }
            else
            {
                frmParent.btnNext.Text = "Hoàn Thành";
            }
        }

        private void ucFinalSXXK_Load(object sender, EventArgs e)
        {
            frmParent.btnNext.Text = "Hoàn Thành";
        }
    }
}
