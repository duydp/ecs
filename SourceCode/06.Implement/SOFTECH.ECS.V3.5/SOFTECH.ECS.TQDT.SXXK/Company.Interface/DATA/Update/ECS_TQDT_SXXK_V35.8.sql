

DELETE FROM [t_VNACC_Category_Common] WHERE ReferenceDB='A519'

ALTER TABLE t_VNACC_Category_Common
ALTER COLUMN Notes NVARCHAR(MAX) NULL

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WA',N'58/2003/NĐ-CP',N'Quy định về kiểm soát NK, XK, vận chuyển quá cảnh lãnh thổ VN chất ma túy, tiền chất, thuôc gây nghiện, hướng thần')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WY',N'38/2014/NĐ-CP',N'Về quản lý hóa chất thuộc diện kiểm soát của công ước cấm phát triển, sản xuất, tàng trữ, sử dụng và phá hủy vũ khí hóa học')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WC',N'32/2006/NĐ-CP',N'Quản lý động vật, thực vật rừng nguy cấp, quý hiếm ')  
  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WE',N'39/2009/NĐ-CP ',N'Về vật liệu nổ công nghiệp')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WF',N'109/2010/NĐ-CP',N'Về kinh doanh XK gạo')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WG',N'2239/TTg-KTN',N'Vv khai thác, vận chuyển, tàng trữ, tiêu thụ, xuất, nhập khẩu cây cảnh, cây bóng mát, cây cổ thụ từ rừng tự nhiên')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WJ',N'24/2012/NĐ-CP',N'Về quản lý kinh doanh vàng')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WK',N'38/2012/NĐ-CP',N'Quy định chi tiết Luật ATTP quy định chức năng của 3 Bộ quản lý ATTP theo nhóm hàng (các Bộ sẽ ban hành Danh mục hh thay thế các văn bản nêu trên)')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WL',N'39/2012/QĐ-TTg',N'Quyết định ban hành quy chế quản lý cây cảnh, cây bóng mát, cây cổ thụ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WM',N'94/2012/NĐ-CP',N'Quy định về sản xuất kinh doanh rượu')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WN',N'11/2013/QĐ-TTg',N'Cấm XNK, mua bán mẫu vật một số loài động vật hoang dã thuộc các Phụ lục công ước quốc tế về buôn bán quốc tế các loài động vật, thực vật hoang dã nguy cấp')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WP',N'67/2013/NĐ-CP',N'Quy định chi tiết một số điều về Luật thi hành phòng, chống tác hại của thuốc lá và kinh doanh thuốc lá')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WQ',N'82/2013/NĐ-CP',N'Danh mục chất ma túy và tiền chất')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WR',N'17/CT-TTg',N'Về việc tăng cường quản lý, kiểm soát việc nhập khẩu công nghệ, máy móc thiết bị của doanh nghiệp')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WS',N'202/2013/NĐ-CP',N'Về quản lý phân bón')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','WT',N'78/2013/QĐ-TTg',N'Về việc ban hành danh mục và lộ trình phương tiện, thiết bị sử dụng năng lượng phải loại bỏ và các tổ máy phát điện hiệu suất thấp không được xây dựng mới.')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YA',N'187/2013/NĐ-CP',N'Quy định chi tiết thi hành Luật Thương mại về hoạt động mua bán hàng hóa quốc tế và các hoạt động đại lý mua, bán, gia công và quá cảnh hàng hóa với nước ngoài')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YB',N'60/2014/NĐ-CP',N'Quy định về hoạt động in')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YC',N'73/2014/QĐ-TTg',N'Quy định Danh mục phế liệu được phép nhập khẩu từ nước ngoài làm nguyên liệu sản xuất')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YD',N'111/2014/NĐ-CP',N'Quy định niên hạn sử dụng của phương tiện thủy nội địa và niên hạn của phương tiện thủy được phép nhập khẩu')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YE',N'114/2014/NĐ-CP',N'Quy định đối tượng, điều kiện được phép nhập khẩu phá dỡ tàu biển đã qua sử dụng')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YJ',N'35/2016/NĐ-CP',N'Quy định chi tiết một số điều Luật Thú y')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YK',N'132/2008/NĐ-CP',N'Quy định chi tiết thi hành một số Điều của Luật chất lượng sản phẩm hàng hóa ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YL',N'08/2010/ NĐ-CP ',N'Quản lý thức ăn chăn nuôi')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YM',N'58/2016/NĐ-CP',N'Quy định chiết tiết về kinh doanh sản phẩm dịch vụ mật mã dân sự và xuất khẩu, nhập khẩu sản phẩm mật mã dân sự')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YN',N'25/2012/NĐ-CP',N'Quy định chi tiết thi hành một số điều Pháp lệnh quản lý, sử dụng vũ khí, vật liệu nổ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YP',N'126/015/NĐ-CP ',N'Sửa đổi Danh mục các chất ma túy và tiền chất kèm theo Nghị định 82/2013/NĐ-CP về danh mục chất ma túy và tiền chất')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YQ',N'82/2006/NĐ-CP',N'Quản lý hoạt động xuất khẩu, nhập khẩu, tái xuất khẩu, nhập nội từ biển, quá cảnh, nuôi sinh sản, nuôi sinh trưởng và trồng cấy nhân tạo các loài động vật, thực vật hoang nguy cấp, quý hiếm ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YR',N'151/2007/QĐ-TTg',N'Quy định về việc nhập khẩu thuốc chưa có số đăng ký tại Việt Nam')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YS',N'42/2013/QĐ-TTg',N'Quy định về quản lý thuốc dùng cho người theo được xuất khẩu, nhập khẩu phi mậu dịch và sửa đổi bổ sung một số điều của quy định về việc nhập khẩu thuốc chưa có số đăng ký tại Việt Nam ban hành kèm Quyết định số 151/2007/QĐ-TTg')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YV',N'87/2016/NĐ-CP',N'Quy định về điều kiện kinh doanh mũ bảo hiểm cho người đi mô tô, xe máy')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YW',N'91/2016/NĐ-CP',N'Về quản lý hóa chất,chế phẩm diệt côn trùng, diệt khuẩn dùng trong lĩnh vực gia dụng và y tế')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YX',N'04/2017/QĐ-TTg',N'Quy định danh mục phương tiện thiết bị phải dán nhãn năng lượng, áp dụng mức hiệu suất năng lượng tối thiểu và lộ trình thực hiện')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YY',N'39/2017/NĐ-CP',N'Về quản lý thức ăn chăn nuôi, thủy sản')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','ZA',N'36/2016/NĐ-CP',N'Quản lý trang thiết bị Y tế')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','YZ',N'108/2017/NĐ-CP',N'Về quản lý phân bón')  
  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','ZB',N'113/2017/NĐ-CP',N'Hướng dẫn Luật chất lượng')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AB',N'06/2006/QĐ-BCN',N'V/v Công bố DM hàng cấm nhập khẩu theo quy định tại NĐ số 12/2006/NĐ-CP ngày 23/01/2006 của CP')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AC',N'24/2006/QĐ-BTM',N'Về việc tạm ngừng TNTX đồ gỗ thành phẩm quan VN sang Hoa Ký và quy định việc TNTX tinh dầu xá xị phải có GP của Bộ Thương mại')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AD',N'01/2006/TT-BCN  ',N'Về việc hướng dẫn quản lý xuất khẩu, nhập khẩu hóa chất độc và sản phẩm có hóa chất độc hại, tiền chất ma túy, hóa chất theo tiêu chuẩn kỹ thuật thuộc dạng quản lý chuyên ngành của Bộ Công nghiệp')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AE',N'40/2006/QĐ-BCN ',N'Bổ sung Danh mục hoá chất cấm xuất khẩu, cấm nhập khẩu (Ban hành kèm theo QĐ số 05/2006/QĐ-BCN ngày 7/4/2006 của Bộ CN về việc công bố DM hoá chất cấm xuất khẩu cấm nhập khẩu)')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AF',N'06/2007/TT- BTM ',N'Hướng dẫn việc nhập khẩu xe gắn máy phân khối lớn từ 175 cm3 trở lên')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AG',N'23/2009/TT-BCT',N'Quy định chi tiết một số điều của Nghị định số 39/2009/NĐ-CP ngày 23 tháng 4 năm 2009 của Chính phủ về vật liệu nổ công nghiệp')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AH',N'20/2011/TT-BCT',N'Quy định bổ sung thủ tục nhập khẩu xe ô tô chở người loại từ 09 chỗ ngồi trở xuống')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AJ',N'23/2012/TT-BCT',N'Quy định việc áp dụng chế độ cấp giấy phép nhập  khẩu tự động đối với một số sản phẩm thép ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AK',N'26/2012/TT-BCT ',N'v/v sửa đổi Thông tư 23/2009/TT-BCT hướng dẫn Nghị định 39/2009/NĐ-CP về vật liệu nổ công nghiệp')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AL',N'41/2012/TT-BCT',N'Quy định về xuất khẩu khoáng sản')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AM',N'15/2013/TT-BCT',N'Hướng dẫn xuất khẩu than')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BJ',N'49/2014/TT-BCT',N'Quy định việc nhập khẩu thuốc lá nguyên liệu nhập khẩu theo hạn ngạch thuế quan năm 2014')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AP',N'37/2013/TT-BCT',N'Quy định nhập khẩu thuốc là điếu, xì gà')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AQ',N'04/VBHN-BCT',N'Văn bản hợp nhất số 04/VBHN-BCT ngày 23/01/2014 của Bộ Công thương về việc hợp nhất Thông tư hướng dẫn việc nhập khẩu xe gắn máy phân khối lớn từ 175 cm3 trở lên')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AR',N'05/VBHN-BCT',N'Về việc tạm ngừng TNTX đồ gỗ thành phẩm quan VN sang Hoa Ký và quy định việc TNTX tinh dầu xá xị phải có GP của Bộ Thương mại')  
  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AS',N'04/2014/TT-BCT',N'Hướng dẫn một số nội dung tại NĐ 187/2013/NĐ-CP của CP quy định chi tiết thi hành Luật TM về hoạt động mua bán hàng hóa quốc tế và các hoạt động đại lý mua, bán, gia công và quá cảnh hàng hóa với nước ngoài')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AV',N'09/2014/TT-BCT',N'Quy định việc nhập khẩu theo hạn ngạch thuế quan năm 2014 và năm 2015 với thuế suất nhập khẩu 0% đối với hàng hóa có xuất xứ từ Campuchia')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BV',N'07/2017/TT-BCT',N'Quy định cửa khẩu nhập khẩu một số mặt hàng phân bón')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','AY',N'29/2010/TT-BCT',N'Về việc nhập khẩu ô tô chưa qua sử dụng bị đục sửa số khung, máy')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BR',N'41/2015/TT-BCT',N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công thương do Bộ trưởng Bộ Công thương ban hành')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BA',N'14/2013/TT-BCT',N'Quy định về điều kiện kinh doanh than')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BB',N'18/2013/TT-BCT',N'Ban hành quy chuẩn kỹ thuật quốc gia về an toàn chai chứa khí dầu mỏ hoá lỏng bằng thép')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BC',N'28/2013/TT-BCT',N'Quy định kiểm tra nhà nước về an toàn thực phẩm đối với thực phẩm nhập khẩu thuộc trách nhiệm quản lý của Bộ Công Thương')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BD',N'42/2013/TT-BCT',N'Quy định quản lý, kiểm soát tiền chất trong lĩnh vực công nghiệp')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BF',N'35/2014/TT-BCT',N'Quy định việc áp dụng chế độ cấp giấy phép nhập khẩu tự động đối với một số mặt hàng phân bón')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BG',N'37/2014/TT-BCT',N'Quy định việc tạm ngừng kinh doanh TNTX gỗ trò, gỗ xẻ từ rừng tự nhiên từ Lào và Campuchia')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BH',N'29/2014/TT-BCT',N'Quy định cụ thể và hướng dẫn thực hiện một số điều về phân bón vô cơ, hướng dẫn việc cấp phép sản xuất phân bón vô cơ đồng thời sản xuất phân bón hữu cơ và phân bón khác tại Nghị định số 202/2013/NĐ-CP ngày 27/11/2013 của Chính phủ về quản lý phân bón')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BK',N'02/2015/TT-BCT',N'Quy định việc nhập khẩu theo hạn ngạch thuế quan năm 2015 với thuế suất 0% đối với hàng hóa có xuất xứ từ CHDCND Lào')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BL',N'02/2016/TT-BCT',N'Quy định về nguyên tắc điều hành hạn ngạch thuế quan nhập khẩu đối với mặt hàng trứng muối, trứng gia cầm năm 2016')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BQ',N'12/2015/TTBCT',N'Quy định việc áp dụng chế độ cấp giấy phép nhập khẩu tự động đối với một số sản phẩm thép')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BS',N'12/2016/TT-BCT',N'Sửa đổi, bổ sung một số Điều của Thông tư 41/2012/TT-BCT quy định về xuất khẩu khoáng sản')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BT',N'29/2016/TT-BCT',N'Danh mục sản phẩm, hàng hóa có khả năng gây mấy an toàn')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BU',N'36/2016/TT-BCT',N'Quy định dán nhãn năng lượng')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','BW',N'11/2017/TT-BCT',N'Quy định về hoạt động tạm nhập, tái xuất, tạm xuất, tái nhập và chuyển khẩu hàng hóa')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DD',N'85/2009/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DE',N'40/2010/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DF',N'49/2010/TT-BNNPTNT',N'Danh mục bổ sung giống cây trồng, phân bón được phép sản xuất, kinh doanh và sử dụng ở Việt Nam')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DG',N'65/2010/TT-BNNPTNT',N'Danh mục bổ sung giống cây trồng, phân bón được phép sản xuất kinh doanh, sử dụng và danh mục thuốc thú y, vắc xin, chế phẩm sinh học, vi sinh vật, hóa chất dùng trong thú y được phép lưu hành tại Việt Nam') 

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DH',N'70/2010/TT-BNNPTNT',N'Danh mục bổ sung giống cây trồng, phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DJ',N'29/2011/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam ')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DK',N'42/2011/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DL',N'59/2011/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DM',N'86/2011/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DN',N'13/2012/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DP',N'31/2012/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DQ',N'45/2012/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DR',N'64/2012/TT-BNNPTNT',N'Danh mục bổ sung phân bón được phép sản xuất kinh doanh và sử dụng tại Việt Nam')
  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DS',N'38/2008/QĐ-BNNPTNT',N'Danh mục áp mã số HS hàng hoá xuất khẩu, nhập khẩu chuyên ngành thủy sản')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DU',N'39/2013/TT-BNNPTNT',N'Ban hành Danh mục bổ sung, sửa đổi thức ăn thủy sản; sản phẩm xử lý, cải tạo môi trường nuôi trồng thuỷ sản được phép lưu hành tại Việt Nam')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','GD',N'13/QD-BNN-TCTS',N'Công bố mã HS đối với các Danh mục quản lý hàng hóa chuyên ngành thủy sản tại Việt Nam')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DW',N'26/2012/TT-BNNPTNT',N'Danh mục tạm thời thức ăn chăn nuôi gia súc gia cầm được phép lưu hành tại Việt nam')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FG',N'50/2014/TT-BNNPTNT',N'Sửa đổi, bổ sung một số điều của Thông tư số 66/2011/TT-BNNPTNT ngày 10/10/2011 Quy định chi tiết một số điều Nghị định số 08/2011/NĐ-CP về quản lý thức ăn chăn nuôi')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FV',N'01/2017/TT-BNNPTNT',N'Bổ sung Danh mục hóa chất, kháng sinh cấm nhập khẩu, sản xuất, kinh doanh và sử dụng trong thức ăn chăn nuôi gia súc, gia cầm tại Việt Nam')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DX',N'15/2009/TT-BNNPTNT',N'Ban hành Danh mục thuốc, hóa chất, kháng sinh cấm sử dụng, hạn chế sử dụng ')  
  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','DY',N'29/2009/TT-BNNPTNT',N'Bổ sung, sửa đổi Thông tư số 15/2009/TT-BNN ngày 17/3/2009 của Bộ trưởng Bộ Nông nghiệp và PTNT ban hành Danh mục thuốc, hóa chất, kháng sinh cấm sử dụng, hạn chế sử dụng')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FK',N'42/2015/TT-BNNPTNN',N'Ban hành Danh mục bổ sung hóa chất, kháng sinh cấm nhập khẩu, sản xuất, kinh doanh và sử dụng trong thức ăn chăn nuôi gia súc, gia cầm tại Việt Nam ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FZ',N'06/2017/TT-BNNPTNT',N'Sửa đổi, bổ sung một số nội dung của Thông tư 03/2016/TT-BNNPTNT ngày 2/4/2016 của Bộ trưởng Bộ NNPTNT về ban hành DM thuốc BVTV được phép sử dụng, cấm sử dụng tại Việt Nam; công bố mã HS đối với thuốc BVTV được phép sử dụng, cấm sử dụng tại Việt Nam')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FY',N'03/2016/TT-BNNPTNT',N'Ban hành danh mục thuốc BVTV được phép sử dụng, cấm sử dụng tại Việt Nam; công bố mã HS đối với thuốc BVTV được phép sử dụng, cấm sử dụng tại Việt Nam')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EB',N'69/2004/QĐ-BNNPTNT',N'Ban hành Danh mục giống cây trồng quý hiếm cấm xuất khẩu')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EC',N'79 /2005/QĐ-BNNPTNT',N'Quy định về trao đổi quốc tế nguồn gen cây trồng quý hiếm')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FP',N'01/2015/TT-BNNPTNT',N'Ban hành danh mục bổ sung giống cây trồng được phép sản xuất, kinh doanh ở Việt Nam')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FR',N'28/2016/TT-BNNPTNT',N'Ban hành danh mục bổ sung giống cây trồng được phép sản xuất, kinh doanh ở Việt Nam')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FS',N'24/2016/TT-BNNPTNT',N'Ban hành Danh mục và công bố mã HS đối với hàng hóa cấm xuất khẩu là Gỗ tròn, gỗ xẻ các loại từ gỗ rừng tự nhiên trong nước và hàng hóa xuất khẩu theo giấy phép là củi, than làm từ gỗ hoặc củi có nguồn gỗ từ gỗ rừng tự nhiên trong nước')
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FW',N'558/QĐ-BNN-BVTV',N'Tạm ngừng nhập khẩu các mặt hàng có nguy cơ cao mang theo mọt lạc serratus Ấn Độ')  
  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','ED',N'78/2004/QĐ-BNNPTNT',N'Ban hành Danh mục giống vật nuôi quý hiếm cấm xuất khẩu  ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EE',N'01/2010/TT-BNNPTNT',N'Ban hành Danh mục bổ sung giống vật nuôi được phép sản xuất kinh doanh ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EH',N'06/2012/TT-BNNPTNT',N'Ban hành Danh mục bổ sung nguồn gen vật nuôi quý hiếm cần được bảo tồn  ')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EL',N'25/2012/TT-BNNPTNT',N'Thuốc thú y, chế phẩm sinh học, vi sinh vật, hóa chất dùng trong thú y thủy sản được phép lưu hành tại Việt Nam')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EM',N'28/2013/TT-BNNPTNT',N'Ban hành Danh mục thuốc thú y được phép lưu hành tại Việt Nam; Danh mục vắc xin, chế phẩm sinh học, vi sinh vật, hóa chất dùng trong thú y được phép lưu hành tại Việt Nam')
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EN',N'45/2005/QĐ-BNNPTNT',N'Danh mục đối tượng kiểm dịch động vật, sản phẩm động vật; danh mục động vật, sản phẩm động vật thuộc diện phải kiểm dịch')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EP',N'46/2005/QĐ-BNNPTNT',N'Danh mục đối tượng kiểm tra vệ sinh thú y; danh mục đối tượng thuộc diện phải kiểm tra vệ sinh thú y; danh mục đối tượng thuộc diện phải kiểm tra vệ sinh thú y bắt buộc áp dụng tiêu chuẩn vệ sinh thú ý')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EQ',N'50/2009/TT-BNNPTNT',N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ NN & PTNT')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','ER',N'13/2011/TT-BNNPTNT',N'Hướng dẫn việc kiểm tra an toàn thực phẩm hàng hóa có nguồn gốc thực vật nhập khẩu')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EW',N'01/2012/TT/BNNPTNT',N'Quy định hồ sơ lâm sản hợp pháp và kiểm tra nguồn gốc  lâm sản')
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','EX',N'32/2012/TT-BNNPTNT',N'Danh mục đối tượng kiểm dịch thủy sản, sản phẩm thủy sản; Danh mục thủy sản, sản phẩm thủy sản thuộc diện phải kiểm dịch')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FA',N'66/2011/TT-BNNPTNT',N'Quy định chi tiết một số điều Nghị định số 08/2010/NĐ-CP ngày 05/02/2010 của Chính phủ về quản lý thức ăn chăn nuôi')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FD',N'28/2014/TT-BNNPTNT',N'Danh mục hóa chất, kháng sinh cấm nhập khẩu, sản xuất, kinh doanh và sử dụng trong thức ăn chăn nuôi gia súc, gia cầm tại Việt Nam')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FE',N'34/2014/TT-BNNPTNT',N'Thông tư hướng dẫn kiểm tra chât lượng muối nhập khẩu')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FF',N'30/2014/TT-BNNPTNT',N'')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FG',N'50/2014/TT-BNNPTNT',N'Sửa đổi, bổ sung một số điều của Thông tư số 66/2011/TT-BNNPTNT ngày 10 tháng 10 năm 2011 quy định chi tiết một số điều Nghị định số 08/2010/NĐ-CP ngày 05 tháng 02 năm 2010 của Chính phủ về quản lý thức ăn chăn nuôi')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FH',N'04/2015/TT-BNNPTNT',N'Thông tư hướng dẫn thực hiện một số nội dung của Nghị định số 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ quy định chi tiết thi hành Luật Thương mại về hoạt động mua bán hàng hóa quốc tế và các hoạt động đại lý, mua, bán, gia công và quá cảnh hàng hóa với nước ngoài trong lĩnh vực nông nghiệp, lâm nghiệp và thủy sản')  
  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FJ',N'41/2014/TT-BNNPTNT',N'Thông tư hướng dẫm một số điều của Nghị định số 202/2013/NĐ-CP ngày 27/11/2013 của Chính phủ về quản lý phân bón thuộc trách nhiệm quản lý nhà nước của Bộ Nông nghiệp và phát triển nông thôn')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FK',N'42/2015/TT-BNNPTNT',N'Ban hành Danh mục bổ sung hóa chất, kháng sinh cấm nhập khẩu, sản xuất, kinh doanh và sử dụng trong thức ăn chăn nuôi gia súc, gia cầm tại Việt Nam')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FN',N'21/2015/TT-BNNPTNT',N'Quản lý thuốc bảo vệ thực vật')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FQ',N'26/2016/TT-BNNPTNT',N'Quy định về kiểm dịch động vật, sản phẩm động vật thủy sản')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FT',N'25/2016/TT-BNNPTNT',N'Quy định về kiểm dịch động vật, sản phẩm động vật trên cạn')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','FX',N'50/2010/TT-BNNPTNT',N'Sửa đổi, bổ sung danh mục sản phẩm, hàng hóa nhóm 2 ban hành kèm Thông tư số 50/2009/TT-BNNPTNT ngày 18/08/2009 của Bộ trưởng Bộ NNPTNT')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','GA',N'04/2017/TT-BNNPTNT',N'Ban hành Danh mục các loài động vật, thực vật hoang dã quy định trong các phụ lục của công ước về buôn bán quốc tế các loài động vật, thực vật hoang dã nguy cấp. ')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','GB',N'924/QD-BNN-TCLN',N'Ban hành bảng mã HS đối với hàng hóa là động vật, thực vật hoang dã nguy cấp, quý, hiếm')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','GE',N'4069/QD-BNN-QLCL',N'Công bố Danh mục hàng hóa nhập khẩu phải kiểm tra ATTP trước khi thông quan thuộc trách nhiệm quản lý của Bộ NN& PTNT')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HW',N'40/2016/TT-BYT',N'Ban hành Danh mục thực phẩm, phụ gia thực phẩm, chất hỗ trợ chế biến thực phẩm và dụng cụ, vật liệu bao gói, chứa đựng thực phẩm theo mã số HS trong biểu thuế xuất khẩu, thuế nhập khẩu thuộc diện quản lý chuyên ngành của Bộ Y tế')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','TB',N'18/2014/TT-NHNN',N'Hướng dẫn hoạt động nhập khẩu hàng hóa thuộc diện quản lý chuyên ngành của Ngân hàng Nhà nước Việt Nam')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','TC',N'16/2012/TT-NHNN',N'Hướng dẫn Nghị định 24/2012/NĐ-CP về quản lý hoạt động kinh doanh vàng do Ngân hàng Nhà nước Việt Nam ban hành')  
  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','VA',N'03/2010/TT-BLDTBXH',N'Ban hành DM sản phẩm hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ LĐTBXH')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LA',N'14/2011/TT-BTTTT  ',N'Quy định chi tiết thi hành nghị định số 12/2002/NĐ-CP')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LJ',N'31/2015/TT-BTTTT',N'Hướng dẫn một số điều của Nghị định 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ đối với hoạt động xuất, nhập khẩu sản phẩm công nghệ thông tin đã qua sử dụng')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LE',N'18/2014/TT-BTTTT',N'Quy định chi tiết thi hành Nghị định số 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ đối với việc cấp giấy phép nhập khẩu thiết bị phát, thu phát sóng vô tuyến điện')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LF',N'26/2014/TT-BTTTT',N'Quy định chi tiết thi hành Nghị định số 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ đối với việc nhập khẩu tem bưu chính')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LG',N'23/2014/TT-BTTTT ',N'Quy định chi tiết và hướng dẫn thi hành một số điều của Luật Xuất bản và Nghị định số 195/2013/NĐ-CP ngày 21/11/2013 của Chính phủ quy định chi tiết một số điều và biện pháp thi hành Luật xuất bản')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LH',N'03/2015/TT-BTTTT',N'Quy định chi tiết và hướng dẫn thi hành một số điều, khoản của Nghị định số 60/2014/NĐ-CP ngày 19/6/2014 của Chính phủ quy định về hoạt động in')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LK',N'43/2016/TT-BTTTT',N'Danh mục thông tin số')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LM',N'41/2016/TT-BTTTT',N'Sửa đổi bổ sung một số Điều của Thông tư 16/2015/TT-BTTT về xuất nhập khẩu hàng hóa trong lĩnh vực in, phát hành xuất bản phẩm ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','LN',N'42/2016/TT-BTTTT',N'Quy định Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ TTTT')  

 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','MA',N'QĐ 15/2006/QĐ-BTNMT',N'V/v Ban hành DM thiết bị làm lạnh sử dụng môi chất lạnh CFC cấm nhập khẩu')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','MB',N'01/2013/TT-BTNMT',N'Quy định về phế liệu được phép nhập khẩu làm nguyên liệu sản xuất')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','MC',N'41/2015/TT-BTNMT',N'Thông tư về bảo vệ môi trường trong nhập khẩu phế liệu làm nguyên liệu sản xuất')  
  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','NA',N'03/2012/TT-BXD',N'Vv công bố danh mục và mã số HS vật liệu amiăng thuộc nhóm amfibole cấm nhập khẩu ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','NB',N'11/2009/TT-BXD',N'Quản lý chất lượng kính xây dựng thuộc trách nhiệm quản lý của Bộ Xây dựng')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','NC',N'01/2010/TT-BXD',N'Quản lý chất lượng clanhke xi măng pooc lăng thuộc trách nhiệm của Bộ Xây dựng')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','ND',N'14/2010/TT-BXD',N'Quản lý chất lượng gạch ốp lát thuộc trách nhiệm quản lý của Bộ Xây dựng.')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','NE',N'04/2012/TT-BXD',N'Hướng dẫn xuất khẩu khoáng sản làm vật liệu xây dựng')  
  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','NF',N'25/2016/TT-BXD',N'Về việc công bố Danh mục và mã số HS vật liệu amiăng thuộc nhóm amfibole cấm nhập khẩu')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','NG',N'09/2017/TT-BXD',N'Hướng dẫn xuất khảu vôi, đô lô mit ning')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','PC',N'28/2014/TT-BVHTTDL',N'Quy định về quản lý hoạt động mua bán hàng hóa quốc tế thuộc diện quản lý chuyên ngành văn hóa của Bộ Văn hóa, Thể thao và Du lịch')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','QB',N'22/2013/TT-BQP',N'Quy định về quản lý hoạt động vật liệu nổ công nghiệp trong quân đội')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','QC',N'40/2017/TT-BQP',N'Công bố Danh mục cụ thể hàng  hóa cấm xuất nhập khẩu thuộc diện quản lý chuyên ngành của Bộ Quốc phòng theo quy định tại Nghị định số 187/2013/NĐ-Cp ngày 20/11/2013 của Chính phủ')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HA',N'1064/2001/QĐ-BYT',N'Công bố 24 hóa chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong lĩnh vực gia dụng và y tế được cấp giấy chứng nhận đăng ký lưu hành tại Việt Nam')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HB',N'674/QĐ-BYT',N'Về việc ban hành Danh mục vắc xin, sinh phẩm y tế và danh mục hoá chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong gia dụng và y tế được nhập khẩu theo nhu cầu ')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HC',N'41/2007/QĐ-BYT',N'Quyết định ban hành Danh mục thuộc dành cho người và mỹ phẩm nhập khẩu vào VN đã được xác định mã số hàng hóa theo Danh mục hàng hóa xuất khẩu, nhập khẩu và Biểu thuế nhập khẩu ưu đãi')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HD',N'09/2010/TT-BYT',N'Hướng dẫn việc quản lý chất lượng thuốc')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HG',N'47/2010/TT-BYT',N'Hướng dẫn hoạt động xuất khẩu, nhập khẩu thuốc và bao bì tiếp xúc trực tiếp với thuốc')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HU',N'03/2016/TT-BYT',N'Quy định về hoạt động kinh doanh dược liệu ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HJ',N'25/2011/TT-BYT',N'Ban hành Danh mục hoá chất, chế phẩm diệt côn trùng, diệt khuẩn được phép đăng ký để sử dụng, được phép đăng ký nhưng hạn chế sử dụng và cấm sử dụng trong lĩnh vực gia dụng và y tế tại Việt Nam')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HV',N'12/2016/TT-BYT',N'Ban hành Danh mục hóa chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong gia dụng  và y tế theo mã số HS trong biểu thuế xuất khẩu, thuế nhập khẩu thuộc diện quản lý chuyên ngành của Bộ Y tế')

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HK',N'818/2007/QĐ-BYT',N'Danh mục hàng hóa NK phải kiểm tra về VSATTP theo mã số HS')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HL',N'06/2011/TT-BYT',N'Quy định về quản lý mỹ phẩm')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HM',N'29/2011/TT-BYT',N'Quy định về quản lý hóa chất, chế phẩm diệt côn trùng, diệt khuẩn dùng trong lĩnh vực gia dụng và y tế')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HN',N'44/2011/TT-BYT',N'Ban hành Danh mục hàng hóa nhóm 2 thuộc trách nhiệm của Bộ Y tế')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HP',N'39/2013/TT-BYT',N'Quy định về quản lý thuốc chữa bệnh cho người theo đường xuất khẩu, nhập khẩu phi mậu dịch')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HQ',N'38/2013/TT-BYT',N'Sửa đổi Thông tư 47/2010/TT-BYT hướng dẫn hoạt động xuất, nhập khẩu thuốc và bao bì tiếp xúc trực tiếp với thuốc ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HR',N'19/2014/TT-BYT',N'Quy định quản lý thuốc gây nghiện, thuốc hướng tâm thần và tiền chất dùng làm thuốc ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HS',N'43/2011/TT-BYT',N'Quy định chế độ quản lý mẫu bệnh phẩm truyền nhiễm')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','HT',N'30/2015/TT-BYT',N'Quy định việc nhập khẩu trang thiết bị y tế')  
 
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KD',N'31/2011/TT-BGTVT',N'Quy định về kiểm tra chất lượng an toàn kỹ thuật và bảo vệ môi trường xe cơ giới nhập khẩu')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KE',N'63/2011/TT-BGTVT',N'Ban hành danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý nhà nước của Bộ Giao thông Vận tải')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KH',N'04/2014/TT-BGTVT',N'Quy định điều kiện và thủ tục cấp giấy phép nhập khẩu pháo hiệu cho an toàn hàng hải')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KJ',N'19/2014/TT-BGTVT',N'Thông tư sửa dổi Thông tư 23/2014/TT-BGTVT')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KK',N'39/2016/TT-BGTVT',N'Ban hành Danh mục hàng hóa có khả năng gây mất an toàn')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KL',N'89/2015/TT-BGTVT',N'Quyết định về kiểm tra chất lượng an toàn kỹ thuật và bảo vệ môi trường xe máy chuyên dùng')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','KM',N'13/2015/TT-BGTVT',N'Công bố Danh mục hàng hóa nhập khẩu thuộc diện quản lý chuyên ngành của Bộ GTVT theo quyết định tại  NĐ 187/2013/NĐ-CP ngày 20/11/2013 của Chính phủ')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','RA',N'01/2009/TT-BKHCN',N'Danh mục hàng hóa nhóm 2 thuộc trách nhiệm quản lý của Bộ KH&CN.')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','RD',N'1171/QĐ-BKHCN',N'Về việc công bố Danh mục hàng hóa nhập khẩu phải kiểm tra chất lượng theo quy chuẩn kỹ thuật quốc gia trước khi thông quan thuộc trách nhiệm quản lý của Bộ KHCN')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','RE',N'23/2015/TT-BKHCN',N'Quy định việc nhập khẩu thiết bị, dây chuyền công nghệ đã qua sử dụng KHCN')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','RF',N'07/2017/TT-BKHCN',N'Kiểm tra chất lượng hàng hóa nhập khẩu thuộc quản lý của Bộ KHCN')  

INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','UA',N'14/2012/TT-BCA',N'Danh mục sản phẩm, hàng hóa có khả năng gây mất an toàn thuộc trách nhiệm quản lý của Bộ Công an')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','UB',N'57/2012/TT-BCA',N'Quy định chi tiết thi hành việc nhập khẩu, quản lý, sử dụng, tiêu hủy mẫu các chất ma túy vì mục đích quốc phòng, an ninh')  
  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','SA',N'111/2012/TT-BTC ',N'Danh mục hàng hóa và thuế suất thuế nhập khẩu để áp dụng hạn ngạch thuế quan')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','SD',N'69/2016/TT-BTC',N'Quy định thủ tục hải quan đối với xăng dầu, hóa chất, khí xuất khẩu, nhập khẩu, tạm nhập tái xuất, chuyển khẩu, quá cảnh; nguyên liệu nhập khẩu để sản xuất và pha chế hoặc gia công xuất khẩu xăng dầu, khí; dầu thô xuất khẩu, nhập khẩu; hàng hóa xuất khẩu, nhập khẩu phục vụ hoạt động dầu khí.')  
  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XA',N'14/2009/TTLT-BCT-BTC',N'Hướng dẫn việc cấp chứng nhận và thủ tục nhập khẩu, xuất khẩu kim cương thô nhằm thực thi các quy định của quy chế chứng nhận quy trình KIMBERLEY')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XB',N'01/2012/TTLT-BCT-BTC',N'Sửa đổi bổ sung TT 14/2009/TTLT-BCT-BTC ngày 23/6/2009')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XD',N'03/2006/TTLT-BTM-BGTVT-BTC-BCA',N'Hướng dẫn việc nhập khẩu ô tô dưới 16 chỗ ngồi đã qua sử dụng theo Nghị định 12/2006/NĐ-CP ngày 23 tháng 01 năm 2006 của Chính phủ')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XE',N'25 /2010 /TTLT-BCT-BGTVT-BTC',N'Quy định việc nhập khẩu ô tô chở người dưới 16 chỗ ngồi, loại mới (chưa qua sử dụng)')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XF',N'47/2011/TTLT-BCT-BTNM',N'Quy định việc quản lý nhập khẩu, xuất khẩu, tạm nhập tái xuất các chất làm suy giảm tầng ô zôn theo quy định của nghị định thư montreal vê các chất làm suy giảm o- zon')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XG',N'06/2013/TTLT-BKHCN-BCA-BGTVT',N'Hướng dẫn việc nhập khẩu mũ bảo hiểm cho người đi xe mô tô, xe gắn máy, xe đạp máy')  
INSERT INTO [t_VNACC_Category_Common](ReferenceDB,Code,Name_VN,Notes) VALUES('A519','XJ',N'58/2015/TTLT-BCT-BKHCN',N'Quy định về quản lý chất lượng thép sản xuất trong nước và thép nhập khẩu')  

GO
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '35.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('35.8',GETDATE(), N'CẬP NHẬT MÃ VĂN BẢN PHÁP QUY')
END




