IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_SXXK_ThanhLy_NPLNhapTon_Insert') AND type IN ('P','U'))
	DROP PROCEDURE p_SXXK_ThanhLy_NPLNhapTon_Insert
GO
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_SXXK_ThanhLy_NPLNhapTon_Insert]  
-- Database: HaiQuanLuoi  
-- Author: Ngo Thanh Tung  
-- Time created: Thursday, August 28, 2008  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_SXXK_ThanhLy_NPLNhapTon_Insert]  
 @SoToKhai int,  
 @MaLoaiHinh char(5),  
 @NamDangKy smallint,  
 @MaHaiQuan char(6),  
 @MaNPL varchar(30),  
 @MaDoanhNghiep varchar(14),  
 @Luong numeric(18, 8),  
 @Ton numeric(18, 8),  
 @ThueXNK float,  
 @ThueTTDB float,  
 @ThueVAT float,  
 @PhuThu float,  
 @ThueCLGia float,  
 @ThueXNKTon float ,
 @SoThuTuHang INT
AS  
INSERT INTO [dbo].[t_SXXK_ThanhLy_NPLNhapTon]  
(  
 [SoToKhai],  
 [MaLoaiHinh],  
 [NamDangKy],  
 [MaHaiQuan],  
 [MaNPL],  
 [MaDoanhNghiep],  
 [Luong],  
 [Ton],  
 [ThueXNK],  
 [ThueTTDB],  
 [ThueVAT],  
 [PhuThu],  
 [ThueCLGia],  
 [ThueXNKTon] ,
 SoThuTuHang 
)  
VALUES  
(  
 @SoToKhai,  
 @MaLoaiHinh,  
 @NamDangKy,  
 @MaHaiQuan,  
 @MaNPL,  
 @MaDoanhNghiep,  
 @Luong,  
 @Ton,  
 @ThueXNK,  
 @ThueTTDB,  
 @ThueVAT,  
 @PhuThu,  
 @ThueCLGia,  
 @ThueXNKTon  ,
 @SoThuTuHang
)  
  
  GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_ToKhaiMauDich_SelectDynamicAndHMD') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_ToKhaiMauDich_SelectDynamicAndHMD
GO
  ------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]    
-- Database: ECS_SXXK    
-- Author: Dang Phuoc Duy    
-- Time created: Tuesday, October 20, 2015    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL nvarchar(3250)    
    
SET @SQL = 'SELECT      
 tkmd.ID,    
 [SoTiepNhan],    
 [NgayTiepNhan],    
 [MaHaiQuan],    
 [SoToKhai],    
 [MaLoaiHinh],    
 [NgayDangKy],    
 [MaDoanhNghiep],    
 [TenDoanhNghiep],    
 [MaDaiLyTTHQ],    
 [TenDaiLyTTHQ],    
 [TenDonViDoiTac],    
 [ChiTietDonViDoiTac],    
 [SoGiayPhep],    
 [NgayGiayPhep],    
 [NgayHetHanGiayPhep],    
 [SoHopDong],    
 [NgayHopDong],    
 [NgayHetHanHopDong],    
 [SoHoaDonThuongMai],    
 [NgayHoaDonThuongMai],    
 [PTVT_ID],    
 [SoHieuPTVT],    
 [NgayDenPTVT],    
 [QuocTichPTVT_ID],    
 [LoaiVanDon],    
 [SoVanDon],    
 [NgayVanDon],    
 [NuocXK_ID],    
 [NuocNK_ID],    
 [DiaDiemXepHang],    
 [CuaKhau_ID],    
 [DKGH_ID],    
 [NguyenTe_ID],    
 [TyGiaTinhThue],    
 [TyGiaUSD],    
 [PTTT_ID],    
 [SoHang],    
 [SoLuongPLTK],    
 [TenChuHang],    
 [ChucVu],    
 [SoContainer20],    
 [SoContainer40],    
 [SoKien],    
 [TongTriGiaKhaiBao],    
 [TongTriGiaTinhThue],    
 [LoaiToKhaiGiaCong],    
 [LePhiHaiQuan],    
 [PhiBaoHiem],    
 [PhiVanChuyen],    
 [PhiXepDoHang],    
 [PhiKhac],    
 [CanBoDangKy],    
 [QuanLyMay],    
 [TrangThaiXuLy],    
 [LoaiHangHoa],    
 [GiayTo],    
 [PhanLuong],    
 [MaDonViUT],    
 [TenDonViUT],    
 [TrongLuongNet],    
 [SoTienKhoan],    
 [HeSoNhan],    
 [GUIDSTR],    
 [DeXuatKhac],    
 [LyDoSua],    
 [ActionStatus],    
 [GuidReference],    
 [NamDK],    
 [HUONGDAN],    
 hmd.SoThuTuHang,
 hmd.MaPhu,    
 hmd.MaHS,    
 hmd.TenHang,    
 hmd.NuocXX_ID,    
 hmd.SoLuong,    
 hmd.DonGiaKB,    
 hmd.TriGiaKB,    
 hmd.DonGiaTT,    
 hmd.TriGiaTT,    
 CASE WHEN tkmd.ID <>0 THEN (SELECT dvt.Ten FROM dbo.t_HaiQuan_DonViTinh dvt WHERE dvt.ID=hmd.DVT_ID) ELSE NULL END AS DVT    
 FROM [dbo].[t_KDT_ToKhaiMauDich] tkmd INNER JOIN dbo.t_KDT_HangMauDich hmd ON hmd.TKMD_ID = tkmd.ID WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL    
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('t_KDT_SXXK_BCXuatNhapTon_SelectNPLNhapDetailByTime') AND type IN ('P','U'))
	DROP PROCEDURE t_KDT_SXXK_BCXuatNhapTon_SelectNPLNhapDetailByTime
GO
CREATE PROCEDURE t_KDT_SXXK_BCXuatNhapTon_SelectNPLNhapDetailByTime  
@dateFrom NVARCHAR(50),  
@dateTo NVARCHAR(50)  
AS  
BEGIN  
SELECT   
TEMP.MaNPL,  
TEMP.TenNPL,  
TEMP.TenDVT_NPL,  
SUM(TEMP.LuongTonDau) AS LuongNhap  
FROM  
(  
 SELECT DISTINCT  
CASE WHEN SoToKhaiNhap = (SELECT TOP 1 SoTK FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhaiNhap AND NamDangKy = YEAR(NgayDangKyNhap)) THEN  (SELECT TOP 1 SoTKVNACCS FROM dbo.t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhaiNhap AND NamDangKy = YEAR(NgayDangKyNhap)) ELSE SoToKhaiNhap END AS SoTKNVNACCS,  
NgayHoanThanhNhap,MaNPL,TenNPL,TenDVT_NPL,LuongTonDau  
FROM dbo.t_KDT_SXXK_BCXuatNhapTon WHERE NgayHoanThanhNhap BETWEEN @dateFrom AND @dateTo AND NgayHoanThanhXuat BETWEEN @dateFrom AND @dateTo ) TEMP GROUP BY TEMP.MaNPL,TEMP.TenNPL,TEMP.TenDVT_NPL ORDER BY TEMP.MaNPL  
END  
  
GO
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('t_KDT_SXXK_BCXuatNhapTon_SelectNPLXuatDetailByTime') AND type IN ('P','U'))
	DROP PROCEDURE t_KDT_SXXK_BCXuatNhapTon_SelectNPLXuatDetailByTime
GO
CREATE PROCEDURE t_KDT_SXXK_BCXuatNhapTon_SelectNPLXuatDetailByTime  
@dateFrom NVARCHAR(50),  
@dateTo NVARCHAR(50)  
AS  
BEGIN  
SELECT  
MaNPL,  
TenNPL,  
TenDVT_NPL,  
SUM(LuongNPLSuDung) AS LuongXuat  
FROM dbo.t_KDT_SXXK_BCXuatNhapTon WHERE NgayHoanThanhNhap BETWEEN @dateFrom AND @dateTo AND NgayHoanThanhXuat BETWEEN @dateFrom AND @dateTo GROUP BY MaNPL,TenNPL,TenDVT_NPL ORDER BY MaNPL,TenNPL,TenDVT_NPL  
END

GO
  
  
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_ToKhaiMauDich_SelectDynamicAndHMD') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_ToKhaiMauDich_SelectDynamicAndHMD
GO
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]    
-- Database: ECS_GC    
-- Author: Dang Phuoc Duy  
-- Time created: Tuesday, October 20, 2015  
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_SelectDynamicAndHMD]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL nvarchar(3250)    
    
SET @SQL = 'SELECT    
  tkmd.ID,    
 [SoTiepNhan],    
 [NgayTiepNhan],    
 [MaHaiQuan],    
 [SoToKhai],    
 [MaLoaiHinh],    
 [NgayDangKy],    
 [MaDoanhNghiep],    
 [TenDoanhNghiep],    
 [MaDaiLyTTHQ],    
 [TenDaiLyTTHQ],    
 [TenDonViDoiTac],    
 [ChiTietDonViDoiTac],    
 [SoGiayPhep],    
 [NgayGiayPhep],    
 [NgayHetHanGiayPhep],    
 [SoHopDong],    
 [NgayHopDong],    
 [NgayHetHanHopDong],    
 [SoHoaDonThuongMai],    
 [NgayHoaDonThuongMai],    
 [PTVT_ID],    
 [SoHieuPTVT],    
 [NgayDenPTVT],    
 [QuocTichPTVT_ID],    
 [LoaiVanDon],    
 [SoVanDon],    
 [NgayVanDon],    
 [NuocXK_ID],    
 [NuocNK_ID],    
 [DiaDiemXepHang],    
 [CuaKhau_ID],    
 [DKGH_ID],    
 [NguyenTe_ID],    
 [TyGiaTinhThue],    
 [TyGiaUSD],    
 [PTTT_ID],    
 [SoHang],    
 [SoLuongPLTK],    
 [TenChuHang],    
 [ChucVu],    
 [SoContainer20],    
 [SoContainer40],    
 [SoKien],    
 [TongTriGiaKhaiBao],    
 [TongTriGiaTinhThue],    
 [LoaiToKhaiGiaCong],    
 [LePhiHaiQuan],    
 [PhiBaoHiem],    
 [PhiVanChuyen],    
 [PhiXepDoHang],    
 [PhiKhac],    
 [CanBoDangKy],    
 [QuanLyMay],    
 [TrangThaiXuLy],    
 [LoaiHangHoa],    
 [GiayTo],    
 [PhanLuong],    
 [MaDonViUT],    
 [TenDonViUT],    
 [TrongLuongNet],    
 [SoTienKhoan],    
 [IDHopDong],    
 [MaMid],    
 [Ngay_THN_THX],    
 [TrangThaiPhanBo],    
 [GUIDSTR],    
 [DeXuatKhac],    
 [LyDoSua],    
 [ActionStatus],    
 [GuidReference],    
 [NamDK],    
 [HUONGDAN] ,  
 hmd.SoThuTuHang,  
 hmd.MaPhu,
 hmd.MaHS,  
 hmd.TenHang,  
 hmd.NuocXX_ID,  
 hmd.SoLuong,  
 hmd.DonGiaKB,  
 hmd.TriGiaKB,  
 hmd.DonGiaTT,  
 hmd.TriGiaTT,  
 CASE WHEN tkmd.ID <>0 THEN (SELECT dvt.Ten FROM dbo.t_HaiQuan_DonViTinh dvt WHERE dvt.ID=hmd.DVT_ID) ELSE NULL END AS DVT  
 FROM [dbo].[t_KDT_ToKhaiMauDich] tkmd INNER JOIN dbo.t_KDT_HangMauDich hmd ON hmd.TKMD_ID = tkmd.ID  WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL    
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_SXXK_NguyenPhuLieuDangKy_SelectDynamicAll') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_SXXK_NguyenPhuLieuDangKy_SelectDynamicAll
GO
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieuDangKy_SelectDynamicAll]    
-- Database: ECS_TQDT_SXXK_V4    
-- Author: Ngo Thanh Tung    
-- Time created: Monday, January 21, 2013    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieuDangKy_SelectDynamicAll]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT t_KDT_SXXK_NguyenPhuLieuDangKy.ID ,  
       SoTiepNhan ,  
       NgayTiepNhan ,  
       MaHaiQuan ,  
       MaDoanhNghiep ,  
       MaDaiLy ,  
       TrangThaiXuLy ,  
       GUIDSTR ,  
       DeXuatKhac ,  
       LyDoSua ,  
       ActionStatus ,  
       GuidReference ,  
       NamDK ,  
       HUONGDAN ,  
       PhanLuong ,  
       Huongdan_PL ,  
       Ma ,  
       Ten ,  
       MaHS ,  
       CASE WHEN DVT_ID = (SELECT TOP 1 ID FROM dbo.t_HaiQuan_DonViTinh WHERE ID=DVT_ID) THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID=DVT_ID) ELSE NULL END AS DVT ,  
       STTHang ,  
       Master_ID FROM dbo.t_KDT_SXXK_NguyenPhuLieuDangKy INNER JOIN dbo.t_KDT_SXXK_NguyenPhuLieu ON t_KDT_SXXK_NguyenPhuLieu.Master_ID = t_KDT_SXXK_NguyenPhuLieuDangKy.ID  
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL    
  
GO
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_SXXK_SanPhamDangKy_SelectDynamicAll') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_SXXK_SanPhamDangKy_SelectDynamicAll
GO
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPhamDangKy_SelectDynamicAll]      
-- Database: ECS_TQ_SXXK      
-- Author: Ngo Thanh Tung      
-- Time created: Monday, March 22, 2010      
------------------------------------------------------------------------------------------------------------------------      
      
CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPhamDangKy_SelectDynamicAll]      
 @WhereCondition nvarchar(500),      
 @OrderByExpression nvarchar(250) = NULL      
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
DECLARE @SQL nvarchar(3250)      
      
SET @SQL = ' SELECT t_KDT_SXXK_SanPhamDangKy.ID ,    
        SoTiepNhan ,    
        NgayTiepNhan ,    
        MaHaiQuan ,    
        MaDoanhNghiep ,    
        MaDaiLy ,    
        TrangThaiXuLy ,    
        GUIDSTR ,    
        DeXuatKhac ,    
        LyDoSua ,    
        ActionStatus ,    
        GuidReference ,    
        NamDK ,    
        HUONGDAN ,    
        PhanLuong ,    
        Huongdan_PL ,    
        Ma ,    
        Ten ,    
        MaHS ,    
  CASE WHEN Ma IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID= (SELECT TOP 1 DVT_ID FROM dbo.t_SXXK_SanPham WHERE Ma = Ma)) ELSE DVT_ID END AS DVT    
        FROM [dbo].[t_KDT_SXXK_SanPhamDangKy] INNER JOIN dbo.t_KDT_SXXK_SanPham ON t_KDT_SXXK_SanPham.Master_ID = t_KDT_SXXK_SanPhamDangKy.ID WHERE ' + @WhereCondition      
      
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0      
BEGIN      
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression      
END      
      
EXEC sp_executesql @SQL      
GO
IF EXISTS (SELECT * FROM sys.objects WHERE object_id= OBJECT_ID('p_KDT_SXXK_DinhMucDangKy_DinhMucChung') AND type IN ('P','U'))
	DROP PROCEDURE p_KDT_SXXK_DinhMucDangKy_DinhMucChung
GO
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMucDangKy_DinhMucChung]      
-- Database: ECS_TQ_SXXK      
-- Author: Ngo Thanh Tung      
-- Time created: Monday, March 22, 2010      
------------------------------------------------------------------------------------------------------------------------      
      
CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMucDangKy_DinhMucChung]      
 @WhereCondition nvarchar(500),      
 @OrderByExpression nvarchar(250) = NULL      
       
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
DECLARE @SQL nvarchar(3250)      
      
SET @SQL = ' SELECT DMDK.ID ,    
        DMDK.NgayTiepNhan ,    
        DM.MaSanPham ,    
  CASE WHEN DM.MaSanPham IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_SXXK_SanPham WHERE DM.MaSanPham = Ma ) ELSE NULL END AS TenSanPham,    
    CASE WHEN DM.MaSanPham IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID= (SELECT TOP 1 DVT_ID FROM dbo.t_SXXK_SanPham WHERE DM.MaSanPham = Ma)) ELSE DM.DVT_ID END AS DVT_SP,    
        DM.MaNguyenPhuLieu AS MaNPL ,    
  CASE WHEN DM.MaNguyenPhuLieu IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_SXXK_NguyenPhuLieu WHERE DM.MaNguyenPhuLieu = Ma ) ELSE NULL END AS TenNPL,    
  CASE WHEN DM.MaNguyenPhuLieu IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID= (SELECT TOP 1 DVT_ID FROM dbo.t_SXXK_NguyenPhuLieu WHERE DM.MaNguyenPhuLieu = Ma)) ELSE DM.DVT_ID END AS DVT,    
        DM.DinhMucSuDung ,    
        DM.TyLeHaoHut ,    
  DM.DinhMucSuDung + DM.DinhMucSuDung*DM.TyLeHaoHut/100 AS DinhMucChung,    
               CASE WHEN IsFromVietNam = 1 THEN N''Có''    
      ELSE ''Không'' END AS MuaVN    
   FROM dbo.t_KDT_SXXK_DinhMucDangKy DMDK INNER JOIN t_kdt_SXXK_DinhMuc DM ON  DM.Master_ID = DMDK.ID WHERE      
'+ @WhereCondition      
      
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0      
BEGIN      
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression      
END      
      
EXEC sp_executesql @SQL      
  
GO
 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '35.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('35.0',GETDATE(), N'CẬP NHẬT  PROCEDURE XUẤT EXCEL SẢN PHẨM ,NGUYÊN PHỤ LIỆU , ĐỊNH MỨC')
END
