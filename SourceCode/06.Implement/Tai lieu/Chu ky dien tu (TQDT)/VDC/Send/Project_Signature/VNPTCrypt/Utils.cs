using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;

namespace VNPTCrypt
{
    public class Utils
    {
        public static string ArrayFormatString(byte[] arrayByte)
        {
            return BitConverter.ToString(arrayByte).Replace("-", string.Empty);
        }
        public static byte[] StringFormatArray(string str)
        {
            int NumberChars = str.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(str.Substring(i, 2), 16);
            return bytes;
        }

        public static byte[] ConvertStringToByte(string strOrigin)
        {
            System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();
            byte[] pByte = enc.GetBytes(strOrigin);
            return pByte;
        }

        public static string ConvertByteToString(byte[] byteOrigin)
        {
            System.Text.UTF8Encoding enc = new System.Text.UTF8Encoding();
            string str = enc.GetString(byteOrigin);
            return str;
        }
    }
}
