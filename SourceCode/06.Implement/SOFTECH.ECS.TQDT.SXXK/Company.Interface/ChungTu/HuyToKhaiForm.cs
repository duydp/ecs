﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Company.BLL;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Company.BLL.KDT;
using Company.BLL.KDT.SXXK;



namespace Company.Interface
{
    public partial class HuyToKhaiForm : BaseForm
    {
        public bool isKhaiBoSung = false;

        public ToKhaiMauDich TKMD;
        public BLL.SXXK.ToKhai.ToKhaiMauDich TKMDDangKy;
        public Company.KDT.SHARE.QuanLyChungTu.HuyToKhai huyTK = null;
        public HuyToKhaiForm()
        {
            InitializeComponent();
        }

        private void HuyToKhaiForm_Load(object sender, EventArgs e)
        {
            getThongTinHuy();
        }

        private void getThongTinHuy()
        {
            List<HuyToKhai> listHuyTK = Company.KDT.SHARE.QuanLyChungTu.HuyToKhai.SelectCollectionBy_TKMD_ID(TKMD.ID);
            if (listHuyTK.Count > 0)
                huyTK = listHuyTK[0];
            //huyTK.TrangThai = 0;
            //huyTK.Update();
            //TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
            //TKMD.ActionStatus = (int)1;
            //TKMD.Update();
            if (huyTK != null)
            {
                txtLyDoHuy.Text = huyTK.LyDoHuy;
                if (huyTK.SoTiepNhan > 0)
                {
                    txtSoTiepNhan.Text = huyTK.SoTiepNhan.ToString();
                    ccNgayTiepNhan.Text = huyTK.NgayTiepNhan.ToShortDateString();
                }
                if (huyTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY)
                {
                    btnKhaiBao.Enabled = false;
                    btnGhi.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    lblTrangThai.Text = Company.KDT.SHARE.Components.TrangThaiXuLy.strCHODUYET;
                }
                else if (huyTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY)
                {
                    lblTrangThai.Text = Company.KDT.SHARE.Components.TrangThaiXuLy.strDAHUY;
                }
                else if (huyTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                {
                    lblTrangThai.Text = Company.KDT.SHARE.Components.TrangThaiXuLy.strCHUAKHAIBAO;
                }
            }
            else
            {
                lblTrangThai.Text = Company.KDT.SHARE.Components.TrangThaiXuLy.strCHUAKHAIBAO;
                huyTK = new HuyToKhai();
                btnKhaiBao.Enabled = false;
                btnGhi.Enabled = true;
                btnLayPhanHoi.Enabled = false;
            }
            lblSoTK.Text = TKMD.SoToKhai.ToString();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (txtLyDoHuy.Text.Trim() == "")
            {
                ShowMessage("Phải nhập lý do hủy!", false);
                return;
            }
            try
            {
                huyTK.LyDoHuy = txtLyDoHuy.Text.Trim();
                huyTK.TKMD_ID = TKMD.ID;
                huyTK.Guid = TKMD.GUIDSTR;
                if (huyTK.ID == 0)
                    huyTK.Insert();
                else
                    huyTK.Update();
                ShowMessage("Lưu thành công.", false);
                btnKhaiBao.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowMessage("Xảy ra lỗi: " + ex.ToString(), false);
            }
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            HuyToKhai(this.TKMD);
        }

        private void HuyToKhai(ToKhaiMauDich tkmd)
        {
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            string password = string.Empty;

            sendXML.LoaiHS = "TK";
            sendXML.master_id = TKMD.ID;
            if (sendXML.Load())
            {
                MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.\nHãy chọn chức năng \"Nhận dữ liệu\" để lấy thông tin phản hồi.", "MSG_SEN03", "", false);

                return;
            }

            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                this.Cursor = Cursors.Default;
                //KHAI BÁO HỦY TỜ KHAI
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                sendXML.Load();
                string xmlCurrent = string.Empty;
                xmlCurrent = tkmd.WSHuyToKhai(password);

                if (!string.IsNullOrEmpty(xmlCurrent))
                {
                    sendXML.LoaiHS = "TK";
                    sendXML.master_id = TKMD.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 1;
                    sendXML.InsertUpdate();

                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                    btnGhi.Enabled = false;
                    lblTrangThai.Text = Company.KDT.SHARE.Components.TrangThaiXuLy.strCHODUYET;

                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                    btnGhi.Enabled = true;
                    lblTrangThai.Text = Company.KDT.SHARE.Components.TrangThaiXuLy.strCHUAKHAIBAO;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong khi thực hiện khai báo hủy tờ khai ", false, true, ex.ToString());
            }

        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            MsgSend sendXML = new MsgSend();
            try
            {
                sendXML.LoaiHS = "TK";
                sendXML.master_id = TKMD.ID;
                if (!sendXML.Load())
                {
                    MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.", "MSG_TK06", "", false);
                    btnKhaiBao.Enabled = true;
                    return;
                }

                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                string xmlCurrent = string.Empty;
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = TKMD.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                if (string.IsNullOrEmpty(xmlCurrent))
                    getThongTinHuy();
                else
                {
                    MLMessages("Chưa có thông tin phản hồi từ hệ thống hải quan.", "MSG_TK06", "", false);
                }
            }
            catch (Exception ex)
            {
                //ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
                ShowMessage("Xảy ra lỗi : " + ex.ToString(), false, true, string.Empty);
            }
        }

        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                if (huyTK.Guid == "")
                {
                    huyTK.Guid = TKMD.GUIDSTR;
                    huyTK.Update();
                }
                bool thanhcong = huyTK.WSLaySoTiepNhan(password, TKMD.MaHaiQuan, TKMD.MaDoanhNghiep);



                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;

                }
                else
                {
                    //datlmq update 24/07/2010; Bổ sung History Kết quả xử lý
                    Company.KD.BLL.KDT.KetQuaXuLy kqxl = new KD.BLL.KDT.KetQuaXuLy();
                    kqxl.ItemID = this.TKMD.ID;
                    kqxl.ReferenceID = new Guid(this.TKMD.GUIDSTR);
                    kqxl.LoaiChungTu = Company.KD.BLL.KDT.KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = "Khai báo hủy tờ khai thành công";//Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                    kqxl.NoiDung = string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", huyTK.SoTiepNhan, huyTK.NgayTiepNhan.ToShortDateString()); ;
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();

                    TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_HUY;
                    TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy;
                    TKMD.Update();

                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.huyTK.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHuyTKThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.huyTK.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.huyTK.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.huyTK.NgayTiepNhan.ToShortDateString();
                }
                lblTrangThai.Text = Company.KDT.SHARE.Components.TrangThaiXuLy.strCHODUYET;

            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false, true, ex.ToString());
            }
        }

        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = huyTK.WSLayPhanHoi(password, TKMD.SoToKhai, TKMD.MaHaiQuan, TKMD.MaDoanhNghiep);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                    lblTrangThai.Text = Company.KDT.SHARE.Components.TrangThaiXuLy.strCHODUYET;
                }
                else// CÓ PHẢN HỒI
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.huyTK.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHuyTKDuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.huyTK.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                        lblTrangThai.Text = Company.KDT.SHARE.Components.TrangThaiXuLy.strTUCHOI;

                        //datlmq update 24/07/2010; Bổ sung History Kết quả xử lý
                        Company.KD.BLL.KDT.KetQuaXuLy kqxl = new KD.BLL.KDT.KetQuaXuLy();
                        kqxl.ItemID = this.TKMD.ID;
                        kqxl.ReferenceID = new Guid(this.TKMD.GUIDSTR);
                        kqxl.LoaiChungTu = Company.KD.BLL.KDT.KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan;//Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày tiếp nhận: {1}", TKMD.SoToKhai, TKMD.NgayTiepNhan.ToShortDateString()); ;
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();

                        TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                        TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy;
                        TKMD.Update();
                    }
                    else// TỜ KHAI HỦY ĐƯỢC CHẤP NHẬN
                    {
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                        TKMD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY;
                        TKMD.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiDaHuy;
                        TKMD.Update();
                        lblTrangThai.Text = Company.KDT.SHARE.Components.TrangThaiXuLy.strDAHUY;

                        //datlmq update 24/07/2010; Bổ sung History Kết quả xử lý
                        Company.KD.BLL.KDT.KetQuaXuLy kqxl = new KD.BLL.KDT.KetQuaXuLy();
                        kqxl.ItemID = this.TKMD.ID;
                        kqxl.ReferenceID = new Guid(this.TKMD.GUIDSTR);
                        kqxl.LoaiChungTu = Company.KD.BLL.KDT.KetQuaXuLy.LoaiChungTu_ToKhai;
                        kqxl.LoaiThongDiep = Company.KDT.SHARE.Components.MessageTitle.HuyToKhaiThanhCong;//Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_ToKhaiDuocCapSo;
                        kqxl.NoiDung = string.Format("Số tờ khai: {0}\r\nNgày tiếp nhận: {1}", TKMD.SoToKhai, TKMD.NgayTiepNhan.ToShortDateString()); ;
                        kqxl.Ngay = DateTime.Now;
                        kqxl.Insert();

                        Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMDDangKy = new Company.BLL.SXXK.ToKhai.ToKhaiMauDich();
                        TKMDDangKy.LoadBy(TKMD.MaHaiQuan, TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK);
                        if (TKMDDangKy != null)
                        {
                            TKMDDangKy.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY;
                            TKMDDangKy.Update();
                        }
                    }
                    this.ShowMessage(message, false);

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false, true, ex.ToString());
            }
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (huyTK.Guid != null && huyTK.Guid != "")
                Globals.ShowKetQuaXuLyBoSung(huyTK.Guid);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

    }
}

