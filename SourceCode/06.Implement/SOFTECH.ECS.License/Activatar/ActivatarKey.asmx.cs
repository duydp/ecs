﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;
using KeyServer;

namespace Activation
{
    /// <summary>
    /// Summary description for Service1
    /// </summary>
    [WebService(Namespace = "http://Softech.VN/", Description = "http://softech.vn")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class ActivationKey : System.Web.Services.WebService
    {

        [WebMethod]
        public string Active(string key)
        {
            KeyActive keyActive = new KeyActive();
            return keyActive.Active(key);
        }
        [WebMethod]
        public string CheckInfo(string key)
        {
            KeyActive keyActive = new KeyActive();
            return keyActive.CheckInfo(key);
        }

        [WebMethod]
        public string Ecs_Update_Info(string messages)
        {
            ECS_UpdateInfo ecs_update = new ECS_UpdateInfo();
            return ecs_update.Ecs_Update_Info(messages);
        }
    }
}
