﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
    public partial class HangMau
    {
        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

        public int DeleteBy_HopDong_ID(SqlTransaction transaction)
        {
            string spName = "p_GC_HangMau_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        public string GetTenHangMau(long IDHopDong, string MaHS, string Ma)
        {
            string sql = " SELECT Ten "
          + " FROM t_GC_HangMau  "
          + " Where HopDong_ID = @HopDong_ID AND Ma=@Ma AND MaHS=@MaHS ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, IDHopDong);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, IDHopDong);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return null;
            else
                return (String)obj;
        }
    }
}