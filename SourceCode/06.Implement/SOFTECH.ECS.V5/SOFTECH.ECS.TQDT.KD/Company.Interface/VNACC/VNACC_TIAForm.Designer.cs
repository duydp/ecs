﻿namespace Company.Interface
{
    partial class VNACC_TIAForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grListHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_TIAForm));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.clcNgayHangDen = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaNguoiXuatNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiXuatNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.grbDonVi = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaNguoiKhaiDauTien = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiKhaiDauTien = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.grListHang = new Janus.Windows.GridEX.GridEX();
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).BeginInit();
            this.grbDonVi.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(0, 28);
            this.grbMain.Size = new System.Drawing.Size(739, 386);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.clcNgayHangDen);
            this.uiGroupBox1.Controls.Add(this.editBox1);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.grbDonVi);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(739, 149);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // clcNgayHangDen
            // 
            // 
            // 
            // 
            this.clcNgayHangDen.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.clcNgayHangDen.DropDownCalendar.Name = "";
            this.clcNgayHangDen.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.clcNgayHangDen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.clcNgayHangDen.IsNullDate = true;
            this.clcNgayHangDen.Location = new System.Drawing.Point(354, 15);
            this.clcNgayHangDen.Name = "clcNgayHangDen";
            this.clcNgayHangDen.Nullable = true;
            this.clcNgayHangDen.NullButtonText = "Xóa";
            this.clcNgayHangDen.ShowNullButton = true;
            this.clcNgayHangDen.Size = new System.Drawing.Size(89, 21);
            this.clcNgayHangDen.TabIndex = 1;
            this.clcNgayHangDen.TodayButtonText = "Hôm nay";
            this.clcNgayHangDen.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // editBox1
            // 
            this.editBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.editBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.editBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editBox1.Location = new System.Drawing.Point(93, 15);
            this.editBox1.MaxLength = 13;
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(114, 21);
            this.editBox1.TabIndex = 0;
            this.editBox1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.editBox1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(30, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Số tờ khai";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(229, 19);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Thời hạn tái xuất/nhập";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.txtMaNguoiXuatNhapKhau);
            this.uiGroupBox4.Controls.Add(this.txtTenNguoiXuatNhapKhau);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Location = new System.Drawing.Point(370, 42);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(358, 99);
            this.uiGroupBox4.TabIndex = 3;
            this.uiGroupBox4.Text = "Người xuất nhập khẩu";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtMaNguoiXuatNhapKhau
            // 
            this.txtMaNguoiXuatNhapKhau.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNguoiXuatNhapKhau.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNguoiXuatNhapKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiXuatNhapKhau.Location = new System.Drawing.Point(30, 17);
            this.txtMaNguoiXuatNhapKhau.MaxLength = 13;
            this.txtMaNguoiXuatNhapKhau.Name = "txtMaNguoiXuatNhapKhau";
            this.txtMaNguoiXuatNhapKhau.Size = new System.Drawing.Size(166, 21);
            this.txtMaNguoiXuatNhapKhau.TabIndex = 0;
            this.txtMaNguoiXuatNhapKhau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiXuatNhapKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNguoiXuatNhapKhau
            // 
            this.txtTenNguoiXuatNhapKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiXuatNhapKhau.Location = new System.Drawing.Point(30, 46);
            this.txtTenNguoiXuatNhapKhau.MaxLength = 100;
            this.txtTenNguoiXuatNhapKhau.Multiline = true;
            this.txtTenNguoiXuatNhapKhau.Name = "txtTenNguoiXuatNhapKhau";
            this.txtTenNguoiXuatNhapKhau.Size = new System.Drawing.Size(320, 40);
            this.txtTenNguoiXuatNhapKhau.TabIndex = 1;
            this.txtTenNguoiXuatNhapKhau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiXuatNhapKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 33;
            this.label3.Text = "Mã";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(25, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "Tên";
            // 
            // grbDonVi
            // 
            this.grbDonVi.BackColor = System.Drawing.Color.Transparent;
            this.grbDonVi.Controls.Add(this.txtMaNguoiKhaiDauTien);
            this.grbDonVi.Controls.Add(this.txtTenNguoiKhaiDauTien);
            this.grbDonVi.Controls.Add(this.label1);
            this.grbDonVi.Controls.Add(this.label2);
            this.grbDonVi.Location = new System.Drawing.Point(6, 42);
            this.grbDonVi.Name = "grbDonVi";
            this.grbDonVi.Size = new System.Drawing.Size(358, 99);
            this.grbDonVi.TabIndex = 2;
            this.grbDonVi.Text = "Người khai đầu tiên";
            this.grbDonVi.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtMaNguoiKhaiDauTien
            // 
            this.txtMaNguoiKhaiDauTien.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNguoiKhaiDauTien.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNguoiKhaiDauTien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNguoiKhaiDauTien.Location = new System.Drawing.Point(30, 17);
            this.txtMaNguoiKhaiDauTien.MaxLength = 13;
            this.txtMaNguoiKhaiDauTien.Name = "txtMaNguoiKhaiDauTien";
            this.txtMaNguoiKhaiDauTien.Size = new System.Drawing.Size(166, 21);
            this.txtMaNguoiKhaiDauTien.TabIndex = 0;
            this.txtMaNguoiKhaiDauTien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNguoiKhaiDauTien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNguoiKhaiDauTien
            // 
            this.txtTenNguoiKhaiDauTien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNguoiKhaiDauTien.Location = new System.Drawing.Point(30, 46);
            this.txtTenNguoiKhaiDauTien.MaxLength = 100;
            this.txtTenNguoiKhaiDauTien.Multiline = true;
            this.txtTenNguoiKhaiDauTien.Name = "txtTenNguoiKhaiDauTien";
            this.txtTenNguoiKhaiDauTien.Size = new System.Drawing.Size(320, 40);
            this.txtTenNguoiKhaiDauTien.TabIndex = 1;
            this.txtTenNguoiKhaiDauTien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNguoiKhaiDauTien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenNguoiKhaiDauTien.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(21, 13);
            this.label1.TabIndex = 33;
            this.label1.Text = "Mã";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Tên";
            // 
            // grListHang
            // 
            this.grListHang.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHang.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHang.ColumnAutoResize = true;
            grListHang_DesignTimeLayout.LayoutString = resources.GetString("grListHang_DesignTimeLayout.LayoutString");
            this.grListHang.DesignTimeLayout = grListHang_DesignTimeLayout;
            this.grListHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListHang.GroupByBoxVisible = false;
            this.grListHang.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListHang.Location = new System.Drawing.Point(3, 17);
            this.grListHang.Name = "grListHang";
            this.grListHang.RecordNavigator = true;
            this.grListHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grListHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListHang.Size = new System.Drawing.Size(733, 217);
            this.grListHang.TabIndex = 0;
            this.grListHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grListHang.VisualStyleManager = this.vsmMain;
            this.grListHang.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.grListHang_DeletingRecord);
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdLuu,
            this.cmdKhaiBao});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.TopRebar1;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdLuu1,
            this.cmdKhaiBao1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(150, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLuu.Icon")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdKhaiBao.Icon")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(739, 28);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.grListHang);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 149);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(739, 237);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.Text = "Hàng tạm nhập tái xuất";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // VNACC_TIAForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(739, 414);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "VNACC_TIAForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đăng ký hàng tạm nhập tái xuất";
            this.Load += new System.EventHandler(this.VNACC_TIAForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDonVi)).EndInit();
            this.grbDonVi.ResumeLayout(false);
            this.grbDonVi.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grListHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX grListHang;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox grbDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiKhaiDauTien;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiKhaiDauTien;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiXuatNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiXuatNhapKhau;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private Janus.Windows.CalendarCombo.CalendarCombo clcNgayHangDen;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}