﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ECS.AutoUpdate
{
    internal interface IUnZip
    {
        bool UnZip(DownloadFileInfo fileInfo);
        void BeforeUnzip();
        void AfterUnzip();
    }
}
