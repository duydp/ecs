﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseDinhMucMaper : BaseFormHaveGuidPanel
    {
        public T_KHOKETOAN_SANPHAM SP = new T_KHOKETOAN_SANPHAM();
        public T_KHOKETOAN_NGUYENPHULIEU NPL = new T_KHOKETOAN_NGUYENPHULIEU();
        public T_KHOKETOAN_DINHMUC DM = new T_KHOKETOAN_DINHMUC();
        public bool IsAdd = true;
        public WareHouseDinhMucMaper()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinhSP.DataSource = this._DonViTinh;
            txtDonViTinhSP.DisplayMember = "Ten";
            txtDonViTinhSP.ValueMember = "ID";
            txtDonViTinhSP.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            txtDonViTinhNPL.DataSource = this._DonViTinh;
            txtDonViTinhNPL.DisplayMember = "Ten";
            txtDonViTinhNPL.ValueMember = "ID";
            txtDonViTinhNPL.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
        }

        private void WareHouseDinhMucMaper_Load(object sender, EventArgs e)
        {
            if (DM.ID > 0)
            {
                txtMaSP.Text = DM.MASP;
                txtTenSP.Text = DM.TENSP;
                txtDonViTinhSP.Text = DM.DVTSP;
            }
            BindData();
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = T_KHOKETOAN_DINHMUC.SelectCollectionDynamic("MASP = '" + txtMaSP.Text.ToString() + "'", "ID");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }  
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtMaNPL, errorProvider, "Mã nguyên phụ liệu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPL, errorProvider, "Tên/Mô tả nguyên phụ liệu", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHSNPL, errorProvider, "Mã HS", isOnlyWarning);
                //isValid &= ValidateControl.ValidateChoose(txtDonViTinhNPL, errorProvider, "Đơn vị tính", isOnlyWarning);

                isValid &= ValidateControl.ValidateNull(txtMaSP, errorProvider, "Mã sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenSP, errorProvider, "Tên/Mô tả sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHSSP, errorProvider, "Mã HS", isOnlyWarning);
                //isValid &= ValidateControl.ValidateChoose(txtDonViTinhSP, errorProvider, "Đơn vị tính", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDinhMuc, errorProvider, "Định mức", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        private void GetDinhMuc()
        {
            try
            {
                if (IsAdd)
                    DM = new T_KHOKETOAN_DINHMUC();
                DM.MASP = txtMaSP.Text;
                DM.TENSP = txtTenSP.Text;
                DM.DVTSP = txtDonViTinhSP.Text;
                DM.MANPL = txtMaNPL.Text;
                DM.TENNPL = txtTenNPL.Text;
                DM.DVTNPL = txtDonViTinhNPL.Text;
                DM.DMSD = Convert.ToDecimal(txtDinhMuc.Text.ToString());
                DM.TLHH = Convert.ToDecimal(txtTyLeHH.Text.ToString());
                DM.DMCHUNG = DM.DMSD + (DM.DMSD * DM.TLHH) / 100;
                DM.GHICHU = txtGhiChu.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }     
        }
        private void SetDinhMuc()
        {
            try
            {
                txtMaSP.Text = DM.MASP;
                txtTenSP.Text = DM.TENSP;
                txtDonViTinhSP.Text = DM.DVTSP;
                txtMaNPL.Text = DM.MANPL;
                txtTenNPL.Text = DM.TENNPL;
                txtDonViTinhNPL.Text = DM.DVTNPL;
                txtDinhMuc.Text = DM.DMSD.ToString();
                txtTyLeHH.Text = DM.TLHH.ToString();
                txtGhiChu.Text = DM.GHICHU;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }  
        }
        private void ResetText()
        {
            try
            {
                txtMaNPL.Text = String.Empty;
                txtTenNPL.Text = String.Empty;
                txtDonViTinhNPL.Text = String.Empty;
                txtDinhMuc.Text = String.Empty;
                txtTyLeHH.Text = String.Empty;
                txtGhiChu.Text = String.Empty;
                DM = new T_KHOKETOAN_DINHMUC();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetDinhMuc();
                List<T_KHOKETOAN_DINHMUC> DMCollection = T_KHOKETOAN_DINHMUC.SelectCollectionDynamic("MASP = '" + txtMaSP.Text.ToString() + "'", "ID");
                foreach (T_KHOKETOAN_DINHMUC item in DMCollection)
                {
                    if (IsAdd)
                    {
                        if (item.MANPL == DM.MANPL)
                        {
                            errorProvider.SetError(txtMaNPL, setText("Mã nguyên phụ liệu này đã có ở danh sách định mức phía dưới .", "This value is exist"));
                            return;
                        }
                    }
                }
                DM.InsertUpdate();
                BindData();
                ResetText();
                //ShowMessage("Lưu thành công ",false);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }  
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<T_KHOKETOAN_DINHMUC> ItemColl = new List<T_KHOKETOAN_DINHMUC>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KHOKETOAN_DINHMUC)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_KHOKETOAN_DINHMUC item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }  
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    DM = new T_KHOKETOAN_DINHMUC();
                    DM = (T_KHOKETOAN_DINHMUC)e.Row.DataRow;
                    SetDinhMuc();
                    IsAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }  
        }

        private void txtMaSP_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                WareHouseSPRegistedForm f = new WareHouseSPRegistedForm();
                f.ShowDialog();
                if (f.SPSelected != null)
                {
                    txtMaSP.Text = f.SPSelected.MASP;
                    txtTenSP.Text = f.SPSelected.TENSP.Trim();
                    txtMaHSSP.Text = f.SPSelected.MAHS.Trim();
                    txtDonViTinhSP.SelectedValue = f.SPSelected.DVT;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }  
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                WareHouseNPLRegistedForm f = new WareHouseNPLRegistedForm();
                f.ShowDialog();
                if (f.NPLSelected != null)
                {
                    txtMaNPL.Text = f.NPLSelected.MANPL.Trim();
                    txtTenNPL.Text = f.NPLSelected.TENNPL.Trim();
                    txtMaHSNPL.Text = f.NPLSelected.MAHS.Trim();
                    txtDonViTinhNPL.SelectedValue = f.NPLSelected.DVT;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }  
        }

        private void txtMaSP_Leave(object sender, EventArgs e)
        {
            if (txtMaSP.Text.Trim() == "")
                return;
            SP = new T_KHOKETOAN_SANPHAM();
            SP.MASP = txtMaSP.Text.Trim();
            List<T_KHOKETOAN_SANPHAM> SPCollection = new List<T_KHOKETOAN_SANPHAM>();
            SPCollection = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic("MASP ='" + SP.MASP + "'", "");
            if (SPCollection.Count >=1)
            {
                txtMaSP.Text = SPCollection[0].MASP;
                txtTenSP.Text = SPCollection[0].TENSP.Trim();
                txtMaHSSP.Text = SPCollection[0].MAHS.Trim();
                txtDonViTinhSP.SelectedValue = SPCollection[0].DVT;
                //txtMaSP.Focus();
                BindData();
                errorProvider.Clear();
            }
            else
            {
                errorProvider.SetError(txtMaSP, setText("Không tồn tại sản phẩm này.", "This value is not exist"));
                txtMaSP.Clear();
                txtMaSP.Focus();
                return;
            }
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (txtMaNPL.Text.Trim() == "")
                return;
            NPL = new T_KHOKETOAN_NGUYENPHULIEU();
            NPL.MANPL = txtMaNPL.Text.Trim();
            List<T_KHOKETOAN_NGUYENPHULIEU> NPLCollection = new List<T_KHOKETOAN_NGUYENPHULIEU>();
            NPLCollection = T_KHOKETOAN_NGUYENPHULIEU.SelectCollectionDynamic("MANPL ='" + NPL.MANPL + "'", "");
            if (NPLCollection.Count >=1)
            {
                txtMaNPL.Text = NPLCollection[0].MANPL;
                txtTenNPL.Text = NPLCollection[0].TENNPL.Trim();
                txtMaHSNPL.Text = NPLCollection[0].MAHS.Trim();
                txtDonViTinhNPL.SelectedValue = NPLCollection[0].DVT;
                //txtMaNPL.Focus();
                errorProvider.Clear();
            }
            else
            {
                errorProvider.SetError(txtMaNPL, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));
                txtMaNPL.Clear();
                txtMaNPL.Focus();
                return;
            }
            if (txtMaNPL.Text.Trim() == "")
                return;
        }
    }
}
