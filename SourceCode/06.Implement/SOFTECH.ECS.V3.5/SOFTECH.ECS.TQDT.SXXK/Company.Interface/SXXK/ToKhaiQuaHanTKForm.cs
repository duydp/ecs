using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ToKhai;
using Company.Interface.Report.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK
{
    public partial class ToKhaiQuaHanTKForm : Company.Interface.BaseForm
    {
        private  DataTable dt = new DataTable();
        private ToKhaiMauDichCollection tkmdColl = new ToKhaiMauDichCollection();
        public ToKhaiQuaHanTKForm()
        {
            InitializeComponent();
        }

        private void ToKhaiSapHetHanForm_Load(object sender, EventArgs e)
        {
            int hanThanhKhoan = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["HanThanhKhoan"]);
            //int thoiGianTK = GlobalSettings.ThongBaoHetHan;
            this.dt = new ToKhaiMauDich().GetToKhaiQuaHanTK(hanThanhKhoan,GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN);
            dgList.DataSource = this.dt;
            dgList.Tables[0].Columns["Luong"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            dgList.Tables[0].Columns["Ton"].FormatString = "n" + GlobalSettings.SoThapPhan.LuongNPL;
            //foreach (DataRow dr in dt.Rows)
            //{
            //    ToKhaiMauDich tkmd = new ToKhaiMauDich();
            //    tkmd.SoToKhai = Convert.ToInt32(dr["SoToKhai"]);
            //}
        }


        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (this.dt.Rows.Count > 0)
            {
                if (this.dt.Rows.Count > 0)
                {
                    //BCKhaiBoSungThue bcThueBoSung = new BCKhaiBoSungThue();
                    //bcThueBoSung.dt = this.dt;
                    //bcThueBoSung.BindReport();
                    //bcThueBoSung.ShowPreview();
                }
            }
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void XemBaoCaoThue_Click(object sender, EventArgs e)
        {
            //if (this.dt.Rows.Count > 0)
            //{
            //    BCKhaiBoSungThue bcThueBoSung = new BCKhaiBoSungThue();
            //    bcThueBoSung.dt = this.dt;
            //    bcThueBoSung.BindReport();
            //    bcThueBoSung.ShowPreview();
            //}
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            
            Janus.Windows.GridEX.GridEXSelectedItemCollection grdColl = dgList.SelectedItems;
            int hanThanhKhoan = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["HanThanhKhoan"]);
            foreach (Janus.Windows.GridEX.GridEXSelectedItem grdSle in grdColl)
            {
                if (grdSle.RowType == RowType.Record)
                {
                    DataTable dtbaocao = new ToKhaiMauDich().GetToKhaiBaoCaoThueTon(hanThanhKhoan, Convert.ToInt32( grdSle.GetRow().Cells["SoToKhai"].Value),GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN); 
                    BCKhaiBoSungThue bcThueBoSung = new BCKhaiBoSungThue();
                    bcThueBoSung.dt = dtbaocao;
                    bcThueBoSung.BindReport();
                    bcThueBoSung.ShowPreview();
                }
            }
           // if(e.Row.RowType  == ro
        }

    }
}

