using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace CA_QuanLyGCN.Report
{
    public partial class GiayChungNhanReport : DevExpress.XtraReports.UI.XtraReport
    {
        public bool printImage = false;
        private DataTable dt = new DataTable();
        public ChungNhan gcn = new ChungNhan();
        public GiayChungNhanReport()
        {

            InitializeComponent();
        }

        public void BindReport()
        {
            SetFont();
            xrPictureBox1.Visible = printImage;
            lblTieuDe_TenCQ.Text = CauHinh.Load("CoQuan").KeyValue.ToString();
            lblTieuDe_TenPhongBan.Text = CauHinh.Load("Phong").KeyValue.ToString();
            lblFullName.Text = CauHinh.Load("FullName").KeyValue.ToString().ToUpper();
            lblTieuDe_SoGiayCN.Text = gcn.SoChungNhan;
            lblCoSoKD_Ten.Text = gcn.TenCoSo.ToUpper();
            lblNganhNgheKD.Text = NganhNghe.Load(Convert.ToInt32(gcn.IdNganhNghe)).TenNganhNghe;
            lblCoSoKD_DiaChi.Text = gcn.DiaChi;

            
            
            if (System.Convert.ToDateTime(gcn.NgayChungNhan).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgay.Text = System.Convert.ToDateTime(gcn.NgayChungNhan).ToString("dd/MM/yyyy").Substring(0, 2);
                lblThang.Text = System.Convert.ToDateTime(gcn.NgayChungNhan).ToString("dd/MM/yyyy").Substring(3, 2);
                lblNam.Text = System.Convert.ToDateTime(gcn.NgayChungNhan).ToString("dd/MM/yyyy").Substring(6, 4);
            }

            #region DanhNghiep
            
            DoanhNghiep dn = new DoanhNghiep();
            dn = DoanhNghiep.Load(Convert.ToInt32(gcn.DoanhNghiepId));
            //lblHoSo1.Text = dn.Tendn;
            if (dn.Tendn != null && dn.Tendn.Length > 45)
            {
                int slip = 0;
                for (int a = 45; a <= 45; a--)
                {
                    if (dn.Tendn.Substring(a, 1) == " " || dn.Tendn.Substring(a, 1) == ",")
                    {
                        slip = a;
                        break;
                    }
                    if (a == 0)
                        break;
                }
                lblHoSo1.Text = dn.Tendn.Substring(0, slip).ToUpper();
                lblHoSo2.Text = dn.Tendn.Substring(slip, dn.Tendn.Length - slip).ToUpper();

            }
            else
            {
                lblHoSo1.Text = dn.Tendn.ToUpper();
                lblHoSo2.Text = "";
            }
            //Tru so tai
            if (dn.DiaChi != null && dn.DiaChi.Length > 85)
            {
                int slip = 0;
                for (int a = 85; a <= 85; a--)
                {
                    if (dn.DiaChi.Substring(a, 1) == " " || dn.DiaChi.Substring(a, 1) == ",")
                    {
                        slip = a;
                        break;
                    }
                    if (a == 0)
                        break;
                }
                lblTruSo1.Text = dn.DiaChi.Substring(0, slip);
                lblTruSo2.Text = dn.DiaChi.Substring(slip, dn.Tendn.Length - slip);

            }
            else
            {
                lblTruSo1.Text = dn.DiaChi;
                lblTruSo2.Text = "";
            }
           
            lblMaDoanhNghiep.Text = dn.Mst;
            lblCoQuanCapMaDoanhNghiep.Text = dn.CoQuanCapMst;
            if (System.Convert.ToDateTime(dn.NgayCapMst).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayChungNhan.Text = System.Convert.ToDateTime(dn.NgayCapMst).ToString("dd/MM/yyyy").Substring(0, 2);
                lblThangChungNhan.Text = System.Convert.ToDateTime(dn.NgayCapMst).ToString("dd/MM/yyyy").Substring(3, 2);
                lblNamChungNhan.Text = System.Convert.ToDateTime(dn.NgayCapMst).ToString("dd/MM/yyyy").Substring(6, 4);
            }
            lblNguoDaiDien.Text = dn.TenNguoiDaiDien.ToUpper();
            lblQuocTich.Text = dn.QuocTichNguoiDaiDien;
            lblChucDanh.Text = dn.ChucDanhNguoiDaiDien;
            lblSoCMND_HC.Text = dn.CmndNguoiDaiDien;
            if (System.Convert.ToDateTime(dn.NgayCapCmnd).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayCMND.Text = System.Convert.ToDateTime(dn.NgayCapCmnd).ToString("dd/MM/yyyy").Substring(0, 2);
                lblThangCMND.Text = System.Convert.ToDateTime(dn.NgayCapCmnd).ToString("dd/MM/yyyy").Substring(3, 2);
                lblNamCMND.Text = System.Convert.ToDateTime(dn.NgayCapCmnd).ToString("dd/MM/yyyy").Substring(6, 4);
            }
            lblCoQuanCapCMND.Text = dn.CoQuanCapCmnd;
            if (dn.DiaChiNguoiDaiDien != null || dn.DiaChiNguoiDaiDien.Length > 40)
            {
                int slip = 0;
                for (int a = 40; a <= 40; a--)
                {
                    if (dn.DiaChiNguoiDaiDien.Substring(a, 1) == " " || dn.DiaChiNguoiDaiDien.Substring(a, 1) == ",")
                    {
                        slip = a;
                        break;
                    }
                    if (a == 0)
                        break;
                }
                lblHoKhau1.Text = dn.DiaChiNguoiDaiDien.Substring(0, slip);
                lblHoKhau2.Text = dn.DiaChiNguoiDaiDien.Substring(slip, dn.DiaChiNguoiDaiDien.Length - slip);

            }
            else
            {
                lblHoKhau1.Text = dn.DiaChiNguoiDaiDien;
                lblHoKhau2.Text = "";
            }
            #endregion

        }

        private void GiayChungNhanReport_PrintProgress(object sender, DevExpress.XtraPrinting.PrintProgressEventArgs e)
        {
            //printImage = false;
            //BindReport();
            //xrPictureBox1.Image = null;
            //this.OnPrintOnPage()
            //xrPictureBox1.Visible = false;
            //this.CreateDocument();
        }
        private void SetFont()
        {
            lblHoSo1.Font = NewFont(Global.sizeFont.TenDoanhNghiep, Global.sizeFont.TenDoanhNghiepBold, Global.sizeFont.TenDoanhNghiepItalic);
            lblHoSo2.Font = NewFont(Global.sizeFont.TenDoanhNghiep, Global.sizeFont.TenDoanhNghiepBold, Global.sizeFont.TenDoanhNghiepItalic);
            lblTruSo1.Font = NewFont(Global.sizeFont.DiaChiDoanhNghiep, Global.sizeFont.DiaChiDoanhNghiepBold, Global.sizeFont.DiaChiDoanhNghiepItalic);
            lblTruSo2.Font = NewFont(Global.sizeFont.DiaChiDoanhNghiep, Global.sizeFont.DiaChiDoanhNghiepBold, Global.sizeFont.DiaChiDoanhNghiepItalic);
            lblMaDoanhNghiep.Font = NewFont(Global.sizeFont.MaDoanhNghiep, Global.sizeFont.MaDoanhNghiepBold, Global.sizeFont.MaDoanhNghiepItalic);
            lblCoQuanCapMaDoanhNghiep.Font = NewFont(Global.sizeFont.CoQuanCapMaDoanhNghiep, Global.sizeFont.CoQuanCapMaDoanhNghiepBold, Global.sizeFont.CoQuanCapMaDoanhNghiepItalic);
            lblNgayChungNhan.Font = NewFont(Global.sizeFont.NgayCapMST, Global.sizeFont.NgayCapMSTBold, Global.sizeFont.NgayCapMSTItalic);
            lblThangChungNhan.Font = NewFont(Global.sizeFont.NgayCapMST, Global.sizeFont.NgayCapMSTBold, Global.sizeFont.NgayCapMSTItalic);
            lblNamChungNhan.Font = NewFont(Global.sizeFont.NgayCapMST, Global.sizeFont.NgayCapMSTBold, Global.sizeFont.NgayCapMSTItalic);
            lblNguoDaiDien.Font = NewFont(Global.sizeFont.TenNguoiDaiDien, Global.sizeFont.TenNguoiDaiDienBold, Global.sizeFont.TenNguoiDaiDienItalic);
            lblQuocTich.Font = NewFont(Global.sizeFont.QuocTich, Global.sizeFont.QuocTichBold, Global.sizeFont.QuocTichItalic);
            lblChucDanh.Font = NewFont(Global.sizeFont.ChucDanh, Global.sizeFont.ChucDanhBold, Global.sizeFont.ChucDanhItalic);
            lblSoCMND_HC.Font = NewFont(Global.sizeFont.SoCMND, Global.sizeFont.SoCMNDBold, Global.sizeFont.SoCMNDItalic);
            lblNgayCMND.Font = NewFont(Global.sizeFont.NgayCapCMND, Global.sizeFont.NgayCapCMNDBold, Global.sizeFont.NgayCapCMNDItalic);
            lblThangCMND.Font = NewFont(Global.sizeFont.NgayCapCMND, Global.sizeFont.NgayCapCMNDBold, Global.sizeFont.NgayCapCMNDItalic);
            lblNamCMND.Font = NewFont(Global.sizeFont.NgayCapCMND, Global.sizeFont.NgayCapCMNDBold, Global.sizeFont.NgayCapCMNDItalic);
            lblCoQuanCapCMND.Font = NewFont(Global.sizeFont.CoQuanCapCMND, Global.sizeFont.CoQuanCapCMNDBold, Global.sizeFont.CoQuanCapCMNDItalic);
            lblHoKhau1.Font = NewFont(Global.sizeFont.HoKhau, Global.sizeFont.HoKhauBold, Global.sizeFont.HoKhauItalic);
            lblHoKhau2.Font = NewFont(Global.sizeFont.HoKhau, Global.sizeFont.HoKhauBold, Global.sizeFont.HoKhauItalic);
            lblTieuDe_TenCQ.Font = NewFont(Global.sizeFont.TenCoQuan, Global.sizeFont.TenCoQuanBold, Global.sizeFont.TenCoQuanItalic);
            lblTieuDe_TenPhongBan.Font = NewFont(Global.sizeFont.TenPhongBan, Global.sizeFont.TenPhongBanBold, Global.sizeFont.TenPhongBanItalic);
            lblFullName.Font = NewFont(Global.sizeFont.TenDayDuPhongBan, Global.sizeFont.TenDayDuPhongBanBold, Global.sizeFont.TenDayDuPhongBanItalic);
            lblTieuDe_SoGiayCN.Font = NewFont(Global.sizeFont.SoGiayCN, Global.sizeFont.SoGiayCNBold, Global.sizeFont.SoGiayCNItalic);
            lblCoSoKD_Ten.Font = NewFont(Global.sizeFont.TenCoSoKD, Global.sizeFont.TenCoSoKDBold, Global.sizeFont.TenCoSoKDItalic);
            lblNganhNgheKD.Font = NewFont(Global.sizeFont.NganhNgheKD, Global.sizeFont.NganhNgheKDBold, Global.sizeFont.NganhNgheKDItalic);
            lblCoSoKD_DiaChi.Font = NewFont(Global.sizeFont.DiaChiCoSoKinhDoanh, Global.sizeFont.DiaChiCoSoKinhDoanhBold, Global.sizeFont.DiaChiCoSoKinhDoanhItalic);
            lblNgay.Font = NewFont(Global.sizeFont.NgayChungNhan, Global.sizeFont.NgayChungNhanBold, Global.sizeFont.NgayChungNhanItalic);
            lblThang.Font = NewFont(Global.sizeFont.NgayChungNhan, Global.sizeFont.NgayChungNhanBold, Global.sizeFont.NgayChungNhanItalic);
            lblNam.Font = NewFont(Global.sizeFont.NgayChungNhan, Global.sizeFont.NgayChungNhanBold, Global.sizeFont.NgayChungNhanItalic);
        }
        public void GetDefaultFont()
        {
            Global.sizeFont.TenDoanhNghiep = lblHoSo1.Font.Size;
            Global.sizeFont.TenDoanhNghiep = lblHoSo2.Font.Size;
            Global.sizeFont.DiaChiDoanhNghiep = lblTruSo1.Font.Size;
            Global.sizeFont.DiaChiDoanhNghiep = lblTruSo2.Font.Size;
            Global.sizeFont.MaDoanhNghiep = lblMaDoanhNghiep.Font.Size;
            Global.sizeFont.CoQuanCapMaDoanhNghiep = lblCoQuanCapMaDoanhNghiep.Font.Size;
            Global.sizeFont.NgayCapMST = lblNgayChungNhan.Font.Size;
            Global.sizeFont.NgayCapMST = lblThangChungNhan.Font.Size;
            Global.sizeFont.NgayCapMST = lblNamChungNhan.Font.Size;
            Global.sizeFont.TenNguoiDaiDien = lblNguoDaiDien.Font.Size;
            Global.sizeFont.QuocTich = lblQuocTich.Font.Size;
            Global.sizeFont.ChucDanh = lblChucDanh.Font.Size;
            Global.sizeFont.SoCMND = lblSoCMND_HC.Font.Size;
            Global.sizeFont.NgayCapCMND = lblNgayCMND.Font.Size;
            Global.sizeFont.NgayCapCMND = lblThangCMND.Font.Size;
            Global.sizeFont.NgayCapCMND = lblNamCMND.Font.Size;
            Global.sizeFont.CoQuanCapCMND = lblCoQuanCapCMND.Font.Size;
            Global.sizeFont.HoKhau = lblHoKhau1.Font.Size;
            Global.sizeFont.HoKhau = lblHoKhau2.Font.Size;
            Global.sizeFont.TenCoQuan = lblTieuDe_TenCQ.Font.Size;
            Global.sizeFont.TenPhongBan = lblTieuDe_TenPhongBan.Font.Size;
            Global.sizeFont.TenDayDuPhongBan = lblFullName.Font.Size;
            Global.sizeFont.SoGiayCN = lblTieuDe_SoGiayCN.Font.Size;
            Global.sizeFont.TenCoSoKD = lblCoSoKD_Ten.Font.Size;
            Global.sizeFont.NganhNgheKD = lblNganhNgheKD.Font.Size;
            Global.sizeFont.DiaChiCoSoKinhDoanh = lblCoSoKD_DiaChi.Font.Size;
            Global.sizeFont.NgayChungNhan = lblNgay.Font.Size;
            Global.sizeFont.NgayChungNhan = lblThang.Font.Size;
            Global.sizeFont.NgayChungNhan = lblNam.Font.Size;


            Global.sizeFont.TenDoanhNghiepItalic = lblHoSo1.Font.Italic;
            Global.sizeFont.DiaChiDoanhNghiepItalic = lblTruSo1.Font.Italic;
            Global.sizeFont.MaDoanhNghiepItalic = lblMaDoanhNghiep.Font.Italic;
            Global.sizeFont.CoQuanCapMaDoanhNghiepItalic = lblCoQuanCapMaDoanhNghiep.Font.Italic;
            Global.sizeFont.NgayCapMSTItalic = lblNgayChungNhan.Font.Italic;
            Global.sizeFont.TenNguoiDaiDienItalic = lblNguoDaiDien.Font.Italic;
            Global.sizeFont.QuocTichItalic = lblQuocTich.Font.Italic;
            Global.sizeFont.ChucDanhItalic = lblChucDanh.Font.Italic;
            Global.sizeFont.SoCMNDItalic = lblSoCMND_HC.Font.Italic;
            Global.sizeFont.NgayCapCMNDItalic = lblNgayCMND.Font.Italic;
            Global.sizeFont.CoQuanCapCMNDItalic = lblCoQuanCapCMND.Font.Italic;
            Global.sizeFont.HoKhauItalic = lblHoKhau1.Font.Italic;
            Global.sizeFont.TenCoQuanItalic = lblTieuDe_TenCQ.Font.Italic;
            Global.sizeFont.TenPhongBanItalic = lblTieuDe_TenPhongBan.Font.Italic;
            Global.sizeFont.TenDayDuPhongBanItalic = lblFullName.Font.Italic;
            Global.sizeFont.SoGiayCNItalic = lblTieuDe_SoGiayCN.Font.Italic;
            Global.sizeFont.TenCoSoKDItalic = lblCoSoKD_Ten.Font.Italic;
            Global.sizeFont.NganhNgheKDItalic = lblNganhNgheKD.Font.Italic;
            Global.sizeFont.DiaChiCoSoKinhDoanhItalic = lblCoSoKD_DiaChi.Font.Italic;
            Global.sizeFont.NgayChungNhanItalic = lblNgay.Font.Italic;

            Global.sizeFont.TenDoanhNghiepBold = lblHoSo1.Font.Bold;
            Global.sizeFont.DiaChiDoanhNghiepBold = lblTruSo1.Font.Bold;
            Global.sizeFont.MaDoanhNghiepBold = lblMaDoanhNghiep.Font.Bold;
            Global.sizeFont.CoQuanCapMaDoanhNghiepBold = lblCoQuanCapMaDoanhNghiep.Font.Bold;
            Global.sizeFont.NgayCapMSTBold = lblNgayChungNhan.Font.Bold;
            Global.sizeFont.TenNguoiDaiDienBold = lblNguoDaiDien.Font.Bold;
            Global.sizeFont.QuocTichBold = lblQuocTich.Font.Bold;
            Global.sizeFont.ChucDanhBold = lblChucDanh.Font.Bold;
            Global.sizeFont.SoCMNDBold = lblSoCMND_HC.Font.Bold;
            Global.sizeFont.NgayCapCMNDBold = lblNgayCMND.Font.Bold;
            Global.sizeFont.CoQuanCapCMNDBold = lblCoQuanCapCMND.Font.Bold;
            Global.sizeFont.HoKhauBold = lblHoKhau1.Font.Bold;
            Global.sizeFont.TenCoQuanBold = lblTieuDe_TenCQ.Font.Bold;
            Global.sizeFont.TenPhongBanBold = lblTieuDe_TenPhongBan.Font.Bold;
            Global.sizeFont.TenDayDuPhongBanBold = lblFullName.Font.Bold;
            Global.sizeFont.SoGiayCNBold = lblTieuDe_SoGiayCN.Font.Bold;
            Global.sizeFont.TenCoSoKDBold = lblCoSoKD_Ten.Font.Bold;
            Global.sizeFont.NganhNgheKDBold = lblNganhNgheKD.Font.Bold;
            Global.sizeFont.DiaChiCoSoKinhDoanhBold = lblCoSoKD_DiaChi.Font.Bold;
            Global.sizeFont.NgayChungNhanBold = lblNgay.Font.Bold;

        }
        private Font NewFont(float size,bool Bold, bool Italic)
        {
            if (!Bold && !Italic)
                return new System.Drawing.Font("Times New Roman", size);
            else if (Bold && !Italic)
                return new System.Drawing.Font("Times New Roman", size, FontStyle.Bold);
            else if (!Bold && Italic)
                return new System.Drawing.Font("Times New Roman", size, FontStyle.Italic);
            else
                return new System.Drawing.Font("Times New Roman", size, FontStyle.Italic | FontStyle.Bold);
        }
    }
}
