﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT;
using Company.GC.BLL.DuLieuChuan;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report.GC
{
    public partial class PhuLucToKhaiCTTQ : DevExpress.XtraReports.UI.XtraReport
    {
        public HangChuyenTiepCollection HCTCollection = new HangChuyenTiepCollection();
        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        public long SoToKhai;
        public DateTime NgayDangKy;
       // public ToKhaiMauDich TKMD;
        public Company.Interface.Report.ReportViewGCCTTQForm report;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        public PhuLucToKhaiCTTQ()
        {
            InitializeComponent();
        }

        
        public void BindReport(string pls)
        {
            xrLabel3.Text = this.TKCT.MaLoaiHinh;//.Substring(1, 2);
            this.PrintingSystem.ShowMarginsWarning = false;
            decimal tongTriGiaNT = 0;
            decimal tongThueXNK = 0;
            decimal tongThueGTGT = 0;
            decimal tongTriGiaThuKhac = 0;
            decimal tongthuetatca = 0;

            //string MaLH = TKCT.MaLoaiHinh.Substring(0,1);
            //if (MaLH == "N")
            //{
            //    lblLoaiHinhKhai.Text = "Nhập khẩu";
            //}
            //else if(MaLH == "X")
            //{
            //    lblLoaiHinhKhai.Text = "Xuất khẩu";
            //}
            //else
            //    lblLoaiHinhKhai.Text = "";

            lblChiCucHaiQuan.Text = GlobalSettings.TEN_HAI_QUAN;
           
            if (TKCT.NgayDangKy > new DateTime(1900, 1, 1))
                lblNgayDangKy.Text = TKCT.NgayDangKy.ToString("dd/MM/yyyy");
            else
                lblNgayDangKy.Text = "";

            if (TKCT.SoToKhai != 0)
                lblSoToKhai.Text = TKCT.SoToKhai + "";
            else
                lblSoToKhai.Text = "";
            //mien thue           
            lblThongBaoMienThue.Visible = MienThue1;
            lblThongBaoMienThueGTGT.Visible = MienThue2;

            xrLabel1.Text = pls;
            for (int i = 0; i < this.HCTCollection.Count; i++)
            {
                XRControl control = new XRControl();
                HangChuyenTiep hmd = this.HCTCollection[i];
                control = this.xrTable1.Rows[i + 3].Controls["TenHang" + (i + 1)];//2
                //if(control !=null )
                if (!inMaHang)
                    control.Text = hmd.TenHang;
                else
                {
                    if (hmd.MaHang.Trim().Length > 0)
                        control.Text = hmd.TenHang + "/" + hmd.MaHang;
                    else
                        control.Text = hmd.TenHang;
                }

                try
                {
                    control.Font = new Font("Times New Roman", GlobalSettings.FontDongHang);
                }
                catch
                {
                    MessageBox.Show("File config thiếu key 'FontTenHang'", "Thông báo");
                    return;
                }

                control = this.xrTable1.Rows[i + 3].Controls["MaHS" + (i + 1)];
                control.Text = hmd.MaHS;

                control = this.xrTable1.Rows[i + 3].Controls["XuatXu" + (i + 1)];
                control.Text = hmd.ID_NuocXX;

                control = this.xrTable1.Rows[i + 3].Controls["Luong" + (i + 1)];
                control.Text = hmd.SoLuong.ToString("G20");

                control = this.xrTable1.Rows[i + 3].Controls["DVT" + (i + 1)];
                control.Text = DonViTinh.GetName((object)hmd.ID_DVT);
               
                
                control = this.xrTable1.Rows[i + 3].Controls["DonGiaNT" + (i + 1)];
                control.Text = hmd.DonGia.ToString("G10");

                control = this.xrTable1.Rows[i + 3].Controls["TriGiaNT" + (i + 1)];
                control.Text = hmd.TriGia.ToString("N2");
               
                //thue XNK
                if(hmd.ThueXNK.Equals(0))
                {
                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTT" + (i + 1)];
                    control.Text = "";
                }
                else
                { 
                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTT" + (i + 1)];
                    control.Text = hmd.TriGiaTT.ToString("N0");

                    control = this.xrTable2.Rows[i + 2].Controls["TienThueXNK" + (i + 1)];
                    control.Text = hmd.ThueXNK.ToString("N0");

                    control = this.xrTable2.Rows[i + 2].Controls["ThueSuatXNK" + (i + 1)];
                    control.Text = hmd.ThueSuatXNK.ToString("N0");
                }
                //thue thu khac
                if (hmd.TyLeThuKhac.Equals(0))
                {
                    control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];
                    control.Text = "";
                }
                else
                {
                    control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];
                    control.Text = hmd.TyLeThuKhac.ToString("N0");

                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];
                    control.Text = hmd.TriGiaThuKhac.ToString("N0");

                }
                //if (MienThue1) control.Text = "";
                //control = this.xrTable2.Rows[i + 2].Controls["TienThueXNK" + (i + 1)];
                //control.Text = hmd.ThueXNK.ToString("N0");
                //if (MienThue1) control.Text = "";
                //thue GTGT or thue TTDB
                if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                {
                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                    decimal TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                    control.Text = TriGiaTTGTGT.ToString("N0");
                    if (MienThue2) control.Text = "";

                    control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                    control.Text = hmd.ThueSuatGTGT.ToString("N0");
                    if (MienThue2) control.Text = "";
                    control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                    control.Text = hmd.ThueGTGT.ToString("N0");
                    if (MienThue2) control.Text = "";
                   
                  
                }
                else if ((hmd.ThueGTGT == 0 && hmd.ThueTTDB > 0)||(hmd.ThueGTGT > 0 && hmd.ThueTTDB > 0))
                {
                    decimal TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                    control.Text = TriGiaTTTTDB.ToString("N0");

                    control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                    control.Text = hmd.ThueSuatTTDB.ToString("N0");
                 
                    control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                    control.Text = hmd.ThueTTDB.ToString("N0");

                    
                }
               
                
                tongTriGiaNT += Math.Round(hmd.TriGia, 2, MidpointRounding.AwayFromZero);
                tongThueXNK += hmd.ThueXNK;
                if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                {

                    tongThueGTGT += hmd.ThueGTGT;
                    tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                }
                else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                {

                    tongThueGTGT += hmd.ThueTTDB;
                    tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                }
                else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT > 0)
                {
                    tongThueGTGT += hmd.ThueTTDB;
                    tongTriGiaThuKhac += hmd.ThueGTGT;
                }
                //}
            }
            tongthuetatca = tongThueXNK + tongTriGiaThuKhac + tongThueGTGT;
            lblTongbangso.Text = tongthuetatca.ToString("N0"); //chi tinh rieng phan hang tren phan phu luc, khong tinh tat va cac hang. //this.TinhTongThueHMD().ToString();
            string s = Company.GC.BLL.Utils.VNCurrency.ToString(tongthuetatca); //Company.BLL.Utils.VNCurrency.ToString((decimal)this.TinhTongThueHMD()).Trim();
            s = s[0].ToString().ToUpper() + s.Substring(1);
            lblTongThueXNKChu.Text = s.Replace("  ", " ");

            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N2");
            lblTongTienThueXNK.Text = tongThueXNK.ToString("N0");
            lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
            if (tongTriGiaThuKhac > 0)
                lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
            else
                lblTongTriGiaThuKhac.Text = "";
            if (MienThue1) lblTongTienThueXNK.Text = "";
            if (MienThue2) lblTongTienThueGTGT.Text = "";

        }
        public decimal TinhTongThueHMD()
        {
            decimal tong = 0;
            foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
            {
                //if (!hmd.FOC)
                tong += hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;
            }
            return tong;
        }
        public void setVisibleImage(bool t)
        {
            //xrPictureBox1.Visible = t;
        }
        private bool IsHaveTax()
        {
            foreach (HangChuyenTiep hmd in this.TKCT.HCTCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            // lblThongBaoMienThue.Visible = t;
            xrTable2.Visible = !t;
            lblTongTienThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = !t;
        }
        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void TenHang2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang4_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang5_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang6_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang7_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang8_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang9_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang1_PreviewClick_1(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        //private void TenHang1_PreviewClick_1(object sender, PreviewMouseEventArgs e)
        //{
        //    XRControl cell = (XRControl)sender;
        //    report.Cell = cell;
        //    report.txtTenNhomHang.Text = cell.Text;
        //    report.label3.Text = cell.Tag.ToString();
        //}
    }
}
