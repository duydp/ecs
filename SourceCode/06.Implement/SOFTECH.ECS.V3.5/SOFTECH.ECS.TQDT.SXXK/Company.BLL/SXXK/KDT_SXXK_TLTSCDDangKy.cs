﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.BLL.SXXK
{
    public partial class KDT_SXXK_TLTSCDDangKy
    {

        public List<KDT_SXXK_TLTSCD> HangThanhLyCollection { get; set; }

        public void LoadHangCollection()
        {
            if(this.ID > 0)
            {
                this.HangThanhLyCollection = new List<KDT_SXXK_TLTSCD>();
                this.HangThanhLyCollection = KDT_SXXK_TLTSCD.SelectCollectionDynamic("Master_ID = " + this.ID, null);
            }
        }
        public bool InsertUpdateFull()
        {
            bool re = true;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    //this.ID = this.InsertUpdate(transaction);
                    if (this.ID > 0)
                        this.Update(transaction);
                    else
                        this.ID = this.Insert(transaction);
                    if (this.ID > 0)
                    {
                        foreach (KDT_SXXK_TLTSCD item in this.HangThanhLyCollection)
                        {
                            item.Master_ID = this.ID;
                            item.InsertUpdate(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    re = false;
                    transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            return re;
        }
        public bool DeleteFull()
        {
            bool re = true;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID > 0)
                    {
                        if (this.HangThanhLyCollection == null || this.HangThanhLyCollection.Count == 0)
                            this.LoadHangCollection();
                        if (this.HangThanhLyCollection.Count > 0)
                            KDT_SXXK_TLTSCD.DeleteCollection(this.HangThanhLyCollection);

                        this.Delete(transaction);
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    re = false;
                    transaction.Rollback();
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                finally
                {
                    connection.Close();
                }
            }
            return re;
        }
    
    }
}
