namespace Company.Interface.SXXK
{
    partial class ImportTKForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportTKForm));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvTenSheet = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.txtSheet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.rfvFilePath = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.txtNgayVD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.txtPhanLuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label52 = new System.Windows.Forms.Label();
            this.txtNgayHoanThanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txtChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label54 = new System.Windows.Forms.Label();
            this.txtTTTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label45 = new System.Windows.Forms.Label();
            this.txtMaDVUT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txtNgayNhapXuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtThanhLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label48 = new System.Windows.Forms.Label();
            this.txtNL_SP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txtPhiVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtPhiBaoHiem = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label43 = new System.Windows.Forms.Label();
            this.txtLPHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtTongTGTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtTongTGKB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label38 = new System.Windows.Forms.Label();
            this.txtSoKien = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txtSoPLTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtContainer40 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.aaaaaaa = new System.Windows.Forms.Label();
            this.txtContainer20 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.adsaf = new System.Windows.Forms.Label();
            this.txtTenChuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtPTTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtTyGiaUSD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txtTyGiaTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtNguyenTe = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtDKGH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtDDXH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtCuaKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtNuocNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.txtNuocXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label22 = new System.Windows.Forms.Label();
            this.txtSoVD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtLoaiVD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtNgayDen = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtSoPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNgayHHHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNgayHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSoHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtNgayHDTM = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSoHDTM = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNgayHHGP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNgayGP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSoGP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDVDT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMaLH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNgayDangKy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.chkSaveSetting = new Janus.Windows.EditControls.UICheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.chkSaveSetting);
            this.grbMain.Controls.Add(this.uiTab1);
            this.helpProvider1.SetHelpKeyword(this.grbMain, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this.grbMain, System.Windows.Forms.HelpNavigator.Topic);
            this.helpProvider1.SetShowHelp(this.grbMain, true);
            this.grbMain.Size = new System.Drawing.Size(708, 442);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Excel 97 - 2003 files (*.xls)|*.xls";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this.grbMain;
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvTenSheet
            // 
            this.rfvTenSheet.ControlToValidate = this.txtSheet;
            this.rfvTenSheet.ErrorMessage = "\"Tên sheet\" bắt buộc phải nhập.";
            this.rfvTenSheet.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSheet.Icon")));
            // 
            // txtSheet
            // 
            this.txtSheet.Location = new System.Drawing.Point(134, 35);
            this.txtSheet.Name = "txtSheet";
            this.txtSheet.Size = new System.Drawing.Size(281, 21);
            this.txtSheet.TabIndex = 322;
            this.txtSheet.VisualStyleManager = this.vsmMain;
            // 
            // rfvFilePath
            // 
            this.rfvFilePath.ControlToValidate = this.txtFilePath;
            this.rfvFilePath.ErrorMessage = "\"Đường dẫn\" bắt buộc phải chọn.";
            this.rfvFilePath.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvFilePath.Icon")));
            // 
            // txtFilePath
            // 
            this.txtFilePath.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtFilePath.Location = new System.Drawing.Point(176, 316);
            this.txtFilePath.Multiline = true;
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(402, 23);
            this.txtFilePath.TabIndex = 188;
            this.txtFilePath.VisualStyleManager = this.vsmMain;
            this.txtFilePath.ButtonClick += new System.EventHandler(this.txtFilePath_ButtonClick);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiTab1
            // 
            this.uiTab1.BackColor = System.Drawing.Color.Transparent;
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(708, 411);
            this.uiTab1.TabIndex = 219;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2007;
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.txtNgayVD);
            this.uiTabPage1.Controls.Add(this.label29);
            this.uiTabPage1.Controls.Add(this.label50);
            this.uiTabPage1.Controls.Add(this.uiButton1);
            this.uiTabPage1.Controls.Add(this.txtFilePath);
            this.uiTabPage1.Controls.Add(this.txtPhanLuong);
            this.uiTabPage1.Controls.Add(this.label52);
            this.uiTabPage1.Controls.Add(this.txtNgayHoanThanh);
            this.uiTabPage1.Controls.Add(this.label53);
            this.uiTabPage1.Controls.Add(this.txtChungTu);
            this.uiTabPage1.Controls.Add(this.label54);
            this.uiTabPage1.Controls.Add(this.txtTTTK);
            this.uiTabPage1.Controls.Add(this.label45);
            this.uiTabPage1.Controls.Add(this.txtMaDVUT);
            this.uiTabPage1.Controls.Add(this.label46);
            this.uiTabPage1.Controls.Add(this.txtNgayNhapXuat);
            this.uiTabPage1.Controls.Add(this.label47);
            this.uiTabPage1.Controls.Add(this.txtThanhLy);
            this.uiTabPage1.Controls.Add(this.label48);
            this.uiTabPage1.Controls.Add(this.txtNL_SP);
            this.uiTabPage1.Controls.Add(this.label49);
            this.uiTabPage1.Controls.Add(this.txtPhiVC);
            this.uiTabPage1.Controls.Add(this.label42);
            this.uiTabPage1.Controls.Add(this.txtPhiBaoHiem);
            this.uiTabPage1.Controls.Add(this.label43);
            this.uiTabPage1.Controls.Add(this.txtLPHQ);
            this.uiTabPage1.Controls.Add(this.label44);
            this.uiTabPage1.Controls.Add(this.txtTongTGTT);
            this.uiTabPage1.Controls.Add(this.label36);
            this.uiTabPage1.Controls.Add(this.txtTongTGKB);
            this.uiTabPage1.Controls.Add(this.label37);
            this.uiTabPage1.Controls.Add(this.txtTrongLuong);
            this.uiTabPage1.Controls.Add(this.label38);
            this.uiTabPage1.Controls.Add(this.txtSoKien);
            this.uiTabPage1.Controls.Add(this.label39);
            this.uiTabPage1.Controls.Add(this.txtSoPLTK);
            this.uiTabPage1.Controls.Add(this.label34);
            this.uiTabPage1.Controls.Add(this.txtContainer40);
            this.uiTabPage1.Controls.Add(this.aaaaaaa);
            this.uiTabPage1.Controls.Add(this.txtContainer20);
            this.uiTabPage1.Controls.Add(this.adsaf);
            this.uiTabPage1.Controls.Add(this.txtTenChuHang);
            this.uiTabPage1.Controls.Add(this.label31);
            this.uiTabPage1.Controls.Add(this.txtPTTT);
            this.uiTabPage1.Controls.Add(this.label32);
            this.uiTabPage1.Controls.Add(this.txtTyGiaUSD);
            this.uiTabPage1.Controls.Add(this.label33);
            this.uiTabPage1.Controls.Add(this.txtTyGiaTT);
            this.uiTabPage1.Controls.Add(this.label24);
            this.uiTabPage1.Controls.Add(this.txtNguyenTe);
            this.uiTabPage1.Controls.Add(this.label25);
            this.uiTabPage1.Controls.Add(this.txtDKGH);
            this.uiTabPage1.Controls.Add(this.label26);
            this.uiTabPage1.Controls.Add(this.txtDDXH);
            this.uiTabPage1.Controls.Add(this.label27);
            this.uiTabPage1.Controls.Add(this.txtCuaKhau);
            this.uiTabPage1.Controls.Add(this.label28);
            this.uiTabPage1.Controls.Add(this.txtNuocNK);
            this.uiTabPage1.Controls.Add(this.label23);
            this.uiTabPage1.Controls.Add(this.txtNuocXK);
            this.uiTabPage1.Controls.Add(this.label22);
            this.uiTabPage1.Controls.Add(this.txtSoVD);
            this.uiTabPage1.Controls.Add(this.label21);
            this.uiTabPage1.Controls.Add(this.txtLoaiVD);
            this.uiTabPage1.Controls.Add(this.label20);
            this.uiTabPage1.Controls.Add(this.txtNgayDen);
            this.uiTabPage1.Controls.Add(this.label19);
            this.uiTabPage1.Controls.Add(this.txtSoPTVT);
            this.uiTabPage1.Controls.Add(this.label18);
            this.uiTabPage1.Controls.Add(this.txtPTVT);
            this.uiTabPage1.Controls.Add(this.label17);
            this.uiTabPage1.Controls.Add(this.txtNgayHHHD);
            this.uiTabPage1.Controls.Add(this.label1);
            this.uiTabPage1.Controls.Add(this.txtNgayHD);
            this.uiTabPage1.Controls.Add(this.label15);
            this.uiTabPage1.Controls.Add(this.txtSoHD);
            this.uiTabPage1.Controls.Add(this.label16);
            this.uiTabPage1.Controls.Add(this.txtNgayHDTM);
            this.uiTabPage1.Controls.Add(this.label13);
            this.uiTabPage1.Controls.Add(this.txtSoHDTM);
            this.uiTabPage1.Controls.Add(this.label12);
            this.uiTabPage1.Controls.Add(this.txtNgayHHGP);
            this.uiTabPage1.Controls.Add(this.label10);
            this.uiTabPage1.Controls.Add(this.txtNgayGP);
            this.uiTabPage1.Controls.Add(this.label9);
            this.uiTabPage1.Controls.Add(this.txtSoGP);
            this.uiTabPage1.Controls.Add(this.label8);
            this.uiTabPage1.Controls.Add(this.txtSoToKhai);
            this.uiTabPage1.Controls.Add(this.txtTenDVDT);
            this.uiTabPage1.Controls.Add(this.label7);
            this.uiTabPage1.Controls.Add(this.txtSheet);
            this.uiTabPage1.Controls.Add(this.txtMaLH);
            this.uiTabPage1.Controls.Add(this.txtNgayDangKy);
            this.uiTabPage1.Controls.Add(this.label6);
            this.uiTabPage1.Controls.Add(this.label14);
            this.uiTabPage1.Controls.Add(this.label11);
            this.uiTabPage1.Controls.Add(this.label3);
            this.uiTabPage1.Controls.Add(this.label4);
            this.uiTabPage1.Controls.Add(this.txtRow);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(706, 389);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thông số cấu hình";
            // 
            // txtNgayVD
            // 
            this.txtNgayVD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayVD.Location = new System.Drawing.Point(523, 143);
            this.txtNgayVD.MaxLength = 2;
            this.txtNgayVD.Name = "txtNgayVD";
            this.txtNgayVD.Size = new System.Drawing.Size(30, 21);
            this.txtNgayVD.TabIndex = 419;
            this.txtNgayVD.Text = "BV";
            this.txtNgayVD.VisualStyleManager = this.vsmMain;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(421, 149);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(76, 13);
            this.label29.TabIndex = 418;
            this.label29.Text = "Cột ngày VĐ";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(48, 322);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(121, 13);
            this.label50.TabIndex = 417;
            this.label50.Text = "Đường dẫn file Excel";
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(584, 316);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(105, 23);
            this.uiButton1.TabIndex = 216;
            this.uiButton1.Text = "Đọc file";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click_1);
            // 
            // txtPhanLuong
            // 
            this.txtPhanLuong.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPhanLuong.Location = new System.Drawing.Point(658, 278);
            this.txtPhanLuong.MaxLength = 2;
            this.txtPhanLuong.Name = "txtPhanLuong";
            this.txtPhanLuong.Size = new System.Drawing.Size(30, 21);
            this.txtPhanLuong.TabIndex = 416;
            this.txtPhanLuong.Text = "BA";
            this.txtPhanLuong.VisualStyleManager = this.vsmMain;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(559, 282);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(91, 13);
            this.label52.TabIndex = 415;
            this.label52.Text = "Cột phân luồng";
            // 
            // txtNgayHoanThanh
            // 
            this.txtNgayHoanThanh.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayHoanThanh.Location = new System.Drawing.Point(133, 279);
            this.txtNgayHoanThanh.MaxLength = 2;
            this.txtNgayHoanThanh.Name = "txtNgayHoanThanh";
            this.txtNgayHoanThanh.Size = new System.Drawing.Size(30, 21);
            this.txtNgayHoanThanh.TabIndex = 414;
            this.txtNgayHoanThanh.Text = "CF";
            this.txtNgayHoanThanh.VisualStyleManager = this.vsmMain;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(48, 281);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(75, 13);
            this.label53.TabIndex = 413;
            this.label53.Text = "Cột ngày HT";
            // 
            // txtChungTu
            // 
            this.txtChungTu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtChungTu.Location = new System.Drawing.Point(133, 252);
            this.txtChungTu.MaxLength = 2;
            this.txtChungTu.Name = "txtChungTu";
            this.txtChungTu.Size = new System.Drawing.Size(30, 21);
            this.txtChungTu.TabIndex = 412;
            this.txtChungTu.Text = "AF";
            this.txtChungTu.VisualStyleManager = this.vsmMain;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(47, 257);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(80, 13);
            this.label54.TabIndex = 411;
            this.label54.Text = "Cột chứng từ";
            // 
            // txtTTTK
            // 
            this.txtTTTK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTTTK.Location = new System.Drawing.Point(658, 35);
            this.txtTTTK.MaxLength = 2;
            this.txtTTTK.Name = "txtTTTK";
            this.txtTTTK.Size = new System.Drawing.Size(30, 21);
            this.txtTTTK.TabIndex = 410;
            this.txtTTTK.Text = "BF";
            this.txtTTTK.VisualStyleManager = this.vsmMain;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(559, 40);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(57, 13);
            this.label45.TabIndex = 409;
            this.label45.Text = "Cột TTTK";
            // 
            // txtMaDVUT
            // 
            this.txtMaDVUT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaDVUT.Location = new System.Drawing.Point(658, 251);
            this.txtMaDVUT.MaxLength = 2;
            this.txtMaDVUT.Name = "txtMaDVUT";
            this.txtMaDVUT.Size = new System.Drawing.Size(30, 21);
            this.txtMaDVUT.TabIndex = 408;
            this.txtMaDVUT.Text = "G";
            this.txtMaDVUT.VisualStyleManager = this.vsmMain;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(559, 256);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(81, 13);
            this.label46.TabIndex = 407;
            this.label46.Text = "Cột mã ĐVUT";
            // 
            // txtNgayNhapXuat
            // 
            this.txtNgayNhapXuat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayNhapXuat.Location = new System.Drawing.Point(523, 277);
            this.txtNgayNhapXuat.MaxLength = 2;
            this.txtNgayNhapXuat.Name = "txtNgayNhapXuat";
            this.txtNgayNhapXuat.Size = new System.Drawing.Size(30, 21);
            this.txtNgayNhapXuat.TabIndex = 406;
            this.txtNgayNhapXuat.Text = "BL";
            this.txtNgayNhapXuat.VisualStyleManager = this.vsmMain;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(421, 281);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(80, 13);
            this.label47.TabIndex = 405;
            this.label47.Text = "Cột ngày N/X";
            // 
            // txtThanhLy
            // 
            this.txtThanhLy.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThanhLy.Location = new System.Drawing.Point(385, 277);
            this.txtThanhLy.MaxLength = 2;
            this.txtThanhLy.Name = "txtThanhLy";
            this.txtThanhLy.Size = new System.Drawing.Size(30, 21);
            this.txtThanhLy.TabIndex = 404;
            this.txtThanhLy.Text = "CE";
            this.txtThanhLy.VisualStyleManager = this.vsmMain;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(284, 282);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(75, 13);
            this.label48.TabIndex = 403;
            this.label48.Text = "Cột thanh lý";
            // 
            // txtNL_SP
            // 
            this.txtNL_SP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNL_SP.Location = new System.Drawing.Point(249, 278);
            this.txtNL_SP.MaxLength = 2;
            this.txtNL_SP.Name = "txtNL_SP";
            this.txtNL_SP.Size = new System.Drawing.Size(30, 21);
            this.txtNL_SP.TabIndex = 402;
            this.txtNL_SP.Text = "CD";
            this.txtNL_SP.VisualStyleManager = this.vsmMain;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(172, 283);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(63, 13);
            this.label49.TabIndex = 401;
            this.label49.Text = "Cột NL_SP";
            // 
            // txtPhiVC
            // 
            this.txtPhiVC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPhiVC.Location = new System.Drawing.Point(523, 251);
            this.txtPhiVC.MaxLength = 2;
            this.txtPhiVC.Name = "txtPhiVC";
            this.txtPhiVC.Size = new System.Drawing.Size(30, 21);
            this.txtPhiVC.TabIndex = 396;
            this.txtPhiVC.Text = "AQ";
            this.txtPhiVC.VisualStyleManager = this.vsmMain;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(421, 256);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(63, 13);
            this.label42.TabIndex = 395;
            this.label42.Text = "Cột phí VC";
            // 
            // txtPhiBaoHiem
            // 
            this.txtPhiBaoHiem.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPhiBaoHiem.Location = new System.Drawing.Point(385, 251);
            this.txtPhiBaoHiem.MaxLength = 2;
            this.txtPhiBaoHiem.Name = "txtPhiBaoHiem";
            this.txtPhiBaoHiem.Size = new System.Drawing.Size(30, 21);
            this.txtPhiBaoHiem.TabIndex = 394;
            this.txtPhiBaoHiem.Text = "AP";
            this.txtPhiBaoHiem.VisualStyleManager = this.vsmMain;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(284, 256);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(64, 13);
            this.label43.TabIndex = 393;
            this.label43.Text = "Cột phí BH";
            // 
            // txtLPHQ
            // 
            this.txtLPHQ.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLPHQ.Location = new System.Drawing.Point(249, 251);
            this.txtLPHQ.MaxLength = 2;
            this.txtLPHQ.Name = "txtLPHQ";
            this.txtLPHQ.Size = new System.Drawing.Size(30, 21);
            this.txtLPHQ.TabIndex = 392;
            this.txtLPHQ.Text = "AD";
            this.txtLPHQ.VisualStyleManager = this.vsmMain;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(172, 257);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(58, 13);
            this.label44.TabIndex = 391;
            this.label44.Text = "Cột LPHQ";
            // 
            // txtTongTGTT
            // 
            this.txtTongTGTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTongTGTT.Location = new System.Drawing.Point(523, 223);
            this.txtTongTGTT.MaxLength = 2;
            this.txtTongTGTT.Name = "txtTongTGTT";
            this.txtTongTGTT.Size = new System.Drawing.Size(30, 21);
            this.txtTongTGTT.TabIndex = 388;
            this.txtTongTGTT.Text = "AU";
            this.txtTongTGTT.VisualStyleManager = this.vsmMain;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(421, 229);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(87, 13);
            this.label36.TabIndex = 387;
            this.label36.Text = "Cột tổng TGTT";
            // 
            // txtTongTGKB
            // 
            this.txtTongTGKB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTongTGKB.Location = new System.Drawing.Point(385, 224);
            this.txtTongTGKB.MaxLength = 2;
            this.txtTongTGKB.Name = "txtTongTGKB";
            this.txtTongTGKB.Size = new System.Drawing.Size(30, 21);
            this.txtTongTGKB.TabIndex = 386;
            this.txtTongTGKB.Text = "AT";
            this.txtTongTGKB.VisualStyleManager = this.vsmMain;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(284, 229);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(87, 13);
            this.label37.TabIndex = 385;
            this.label37.Text = "Cột tổng TGKB";
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTrongLuong.Location = new System.Drawing.Point(249, 224);
            this.txtTrongLuong.MaxLength = 2;
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.Size = new System.Drawing.Size(30, 21);
            this.txtTrongLuong.TabIndex = 384;
            this.txtTrongLuong.Text = "AV";
            this.txtTrongLuong.VisualStyleManager = this.vsmMain;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(172, 230);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(71, 13);
            this.label38.TabIndex = 383;
            this.label38.Text = "Cột TLượng";
            // 
            // txtSoKien
            // 
            this.txtSoKien.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoKien.Location = new System.Drawing.Point(134, 225);
            this.txtSoKien.MaxLength = 2;
            this.txtSoKien.Name = "txtSoKien";
            this.txtSoKien.Size = new System.Drawing.Size(30, 21);
            this.txtSoKien.TabIndex = 382;
            this.txtSoKien.Text = "AW";
            this.txtSoKien.VisualStyleManager = this.vsmMain;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(47, 230);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(69, 13);
            this.label39.TabIndex = 381;
            this.label39.Text = "Cột số kiện";
            // 
            // txtSoPLTK
            // 
            this.txtSoPLTK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoPLTK.Location = new System.Drawing.Point(523, 61);
            this.txtSoPLTK.MaxLength = 2;
            this.txtSoPLTK.Name = "txtSoPLTK";
            this.txtSoPLTK.Size = new System.Drawing.Size(30, 21);
            this.txtSoPLTK.TabIndex = 380;
            this.txtSoPLTK.Text = "CC";
            this.txtSoPLTK.VisualStyleManager = this.vsmMain;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(421, 66);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(72, 13);
            this.label34.TabIndex = 379;
            this.label34.Text = "Cột số PLTK";
            // 
            // txtContainer40
            // 
            this.txtContainer40.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtContainer40.Location = new System.Drawing.Point(658, 196);
            this.txtContainer40.MaxLength = 2;
            this.txtContainer40.Name = "txtContainer40";
            this.txtContainer40.Size = new System.Drawing.Size(30, 21);
            this.txtContainer40.TabIndex = 378;
            this.txtContainer40.Text = "BR";
            this.txtContainer40.VisualStyleManager = this.vsmMain;
            // 
            // aaaaaaa
            // 
            this.aaaaaaa.AutoSize = true;
            this.aaaaaaa.BackColor = System.Drawing.Color.Transparent;
            this.aaaaaaa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aaaaaaa.Location = new System.Drawing.Point(559, 200);
            this.aaaaaaa.Name = "aaaaaaa";
            this.aaaaaaa.Size = new System.Drawing.Size(98, 13);
            this.aaaaaaa.TabIndex = 377;
            this.aaaaaaa.Text = "Cột Container40";
            // 
            // txtContainer20
            // 
            this.txtContainer20.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtContainer20.Location = new System.Drawing.Point(523, 197);
            this.txtContainer20.MaxLength = 2;
            this.txtContainer20.Name = "txtContainer20";
            this.txtContainer20.Size = new System.Drawing.Size(30, 21);
            this.txtContainer20.TabIndex = 376;
            this.txtContainer20.Text = "AX";
            this.txtContainer20.VisualStyleManager = this.vsmMain;
            // 
            // adsaf
            // 
            this.adsaf.AutoSize = true;
            this.adsaf.BackColor = System.Drawing.Color.Transparent;
            this.adsaf.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.adsaf.Location = new System.Drawing.Point(421, 202);
            this.adsaf.Name = "adsaf";
            this.adsaf.Size = new System.Drawing.Size(98, 13);
            this.adsaf.TabIndex = 375;
            this.adsaf.Text = "Cột Container20";
            // 
            // txtTenChuHang
            // 
            this.txtTenChuHang.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenChuHang.Location = new System.Drawing.Point(385, 197);
            this.txtTenChuHang.MaxLength = 2;
            this.txtTenChuHang.Name = "txtTenChuHang";
            this.txtTenChuHang.Size = new System.Drawing.Size(30, 21);
            this.txtTenChuHang.TabIndex = 374;
            this.txtTenChuHang.Text = "AG";
            this.txtTenChuHang.VisualStyleManager = this.vsmMain;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(284, 202);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(102, 13);
            this.label31.TabIndex = 373;
            this.label31.Text = "Cột tên chủ hàng";
            // 
            // txtPTTT
            // 
            this.txtPTTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPTTT.Location = new System.Drawing.Point(249, 197);
            this.txtPTTT.MaxLength = 2;
            this.txtPTTT.Name = "txtPTTT";
            this.txtPTTT.Size = new System.Drawing.Size(30, 21);
            this.txtPTTT.TabIndex = 372;
            this.txtPTTT.Text = "AA";
            this.txtPTTT.VisualStyleManager = this.vsmMain;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(172, 203);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(57, 13);
            this.label32.TabIndex = 371;
            this.label32.Text = "Cột PTTT";
            // 
            // txtTyGiaUSD
            // 
            this.txtTyGiaUSD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTyGiaUSD.Location = new System.Drawing.Point(134, 198);
            this.txtTyGiaUSD.MaxLength = 2;
            this.txtTyGiaUSD.Name = "txtTyGiaUSD";
            this.txtTyGiaUSD.Size = new System.Drawing.Size(30, 21);
            this.txtTyGiaUSD.TabIndex = 370;
            this.txtTyGiaUSD.Text = "T";
            this.txtTyGiaUSD.VisualStyleManager = this.vsmMain;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(47, 203);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(87, 13);
            this.label33.TabIndex = 369;
            this.label33.Text = "Cột tỷ giá USD";
            // 
            // txtTyGiaTT
            // 
            this.txtTyGiaTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTyGiaTT.Location = new System.Drawing.Point(658, 170);
            this.txtTyGiaTT.MaxLength = 2;
            this.txtTyGiaTT.Name = "txtTyGiaTT";
            this.txtTyGiaTT.Size = new System.Drawing.Size(30, 21);
            this.txtTyGiaTT.TabIndex = 368;
            this.txtTyGiaTT.Text = "AC";
            this.txtTyGiaTT.VisualStyleManager = this.vsmMain;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(559, 176);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(78, 13);
            this.label24.TabIndex = 367;
            this.label24.Text = "Cột tỷ giá TT";
            // 
            // txtNguyenTe
            // 
            this.txtNguyenTe.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNguyenTe.Location = new System.Drawing.Point(523, 170);
            this.txtNguyenTe.MaxLength = 2;
            this.txtNguyenTe.Name = "txtNguyenTe";
            this.txtNguyenTe.Size = new System.Drawing.Size(30, 21);
            this.txtNguyenTe.TabIndex = 366;
            this.txtNguyenTe.Text = "AB";
            this.txtNguyenTe.VisualStyleManager = this.vsmMain;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(422, 176);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(86, 13);
            this.label25.TabIndex = 365;
            this.label25.Text = "Cột nguyên tệ";
            // 
            // txtDKGH
            // 
            this.txtDKGH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDKGH.Location = new System.Drawing.Point(658, 223);
            this.txtDKGH.MaxLength = 2;
            this.txtDKGH.Name = "txtDKGH";
            this.txtDKGH.Size = new System.Drawing.Size(30, 21);
            this.txtDKGH.TabIndex = 364;
            this.txtDKGH.Text = "Y";
            this.txtDKGH.VisualStyleManager = this.vsmMain;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(559, 228);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(61, 13);
            this.label26.TabIndex = 363;
            this.label26.Text = "Cột ĐKGH";
            // 
            // txtDDXH
            // 
            this.txtDDXH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDDXH.Location = new System.Drawing.Point(249, 171);
            this.txtDDXH.MaxLength = 2;
            this.txtDDXH.Name = "txtDDXH";
            this.txtDDXH.Size = new System.Drawing.Size(30, 21);
            this.txtDDXH.TabIndex = 362;
            this.txtDDXH.Text = "O";
            this.txtDDXH.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(172, 175);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(62, 13);
            this.label27.TabIndex = 361;
            this.label27.Text = "Cột ĐĐXH";
            // 
            // txtCuaKhau
            // 
            this.txtCuaKhau.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCuaKhau.Location = new System.Drawing.Point(385, 171);
            this.txtCuaKhau.MaxLength = 2;
            this.txtCuaKhau.Name = "txtCuaKhau";
            this.txtCuaKhau.Size = new System.Drawing.Size(30, 21);
            this.txtCuaKhau.TabIndex = 360;
            this.txtCuaKhau.Text = "N";
            this.txtCuaKhau.VisualStyleManager = this.vsmMain;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(284, 175);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(81, 13);
            this.label28.TabIndex = 359;
            this.label28.Text = "Cột cửa khẩu";
            // 
            // txtNuocNK
            // 
            this.txtNuocNK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNuocNK.Location = new System.Drawing.Point(134, 172);
            this.txtNuocNK.MaxLength = 2;
            this.txtNuocNK.Name = "txtNuocNK";
            this.txtNuocNK.Size = new System.Drawing.Size(30, 21);
            this.txtNuocNK.TabIndex = 358;
            this.txtNuocNK.Text = "X";
            this.txtNuocNK.VisualStyleManager = this.vsmMain;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(47, 176);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(74, 13);
            this.label23.TabIndex = 357;
            this.label23.Text = "Cột nước NK";
            // 
            // txtNuocXK
            // 
            this.txtNuocXK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNuocXK.Location = new System.Drawing.Point(658, 143);
            this.txtNuocXK.MaxLength = 2;
            this.txtNuocXK.Name = "txtNuocXK";
            this.txtNuocXK.Size = new System.Drawing.Size(30, 21);
            this.txtNuocXK.TabIndex = 356;
            this.txtNuocXK.Text = "W";
            this.txtNuocXK.VisualStyleManager = this.vsmMain;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(559, 148);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(74, 13);
            this.label22.TabIndex = 355;
            this.label22.Text = "Cột nước XK";
            // 
            // txtSoVD
            // 
            this.txtSoVD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoVD.Location = new System.Drawing.Point(385, 144);
            this.txtSoVD.MaxLength = 2;
            this.txtSoVD.Name = "txtSoVD";
            this.txtSoVD.Size = new System.Drawing.Size(30, 21);
            this.txtSoVD.TabIndex = 354;
            this.txtSoVD.Text = "M";
            this.txtSoVD.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(284, 149);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(61, 13);
            this.label21.TabIndex = 353;
            this.label21.Text = "Cột số VĐ";
            // 
            // txtLoaiVD
            // 
            this.txtLoaiVD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLoaiVD.Location = new System.Drawing.Point(249, 144);
            this.txtLoaiVD.MaxLength = 2;
            this.txtLoaiVD.Name = "txtLoaiVD";
            this.txtLoaiVD.Size = new System.Drawing.Size(30, 21);
            this.txtLoaiVD.TabIndex = 352;
            this.txtLoaiVD.Text = "M";
            this.txtLoaiVD.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(172, 150);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(68, 13);
            this.label20.TabIndex = 351;
            this.label20.Text = "Cột loại VĐ";
            // 
            // txtNgayDen
            // 
            this.txtNgayDen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayDen.Location = new System.Drawing.Point(134, 145);
            this.txtNgayDen.MaxLength = 2;
            this.txtNgayDen.Name = "txtNgayDen";
            this.txtNgayDen.Size = new System.Drawing.Size(30, 21);
            this.txtNgayDen.TabIndex = 350;
            this.txtNgayDen.Text = "L";
            this.txtNgayDen.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(47, 150);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(81, 13);
            this.label19.TabIndex = 349;
            this.label19.Text = "Cột ngày đến";
            // 
            // txtSoPTVT
            // 
            this.txtSoPTVT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoPTVT.Location = new System.Drawing.Point(658, 116);
            this.txtSoPTVT.MaxLength = 2;
            this.txtSoPTVT.Name = "txtSoPTVT";
            this.txtSoPTVT.Size = new System.Drawing.Size(30, 21);
            this.txtSoPTVT.TabIndex = 348;
            this.txtSoPTVT.Text = "J";
            this.txtSoPTVT.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(559, 121);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 347;
            this.label18.Text = "Cột số PTVT";
            // 
            // txtPTVT
            // 
            this.txtPTVT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPTVT.Location = new System.Drawing.Point(523, 116);
            this.txtPTVT.MaxLength = 2;
            this.txtPTVT.Name = "txtPTVT";
            this.txtPTVT.Size = new System.Drawing.Size(30, 21);
            this.txtPTVT.TabIndex = 346;
            this.txtPTVT.Text = "I";
            this.txtPTVT.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(421, 122);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(57, 13);
            this.label17.TabIndex = 345;
            this.label17.Text = "Cột PTVT";
            // 
            // txtNgayHHHD
            // 
            this.txtNgayHHHD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayHHHD.Location = new System.Drawing.Point(385, 117);
            this.txtNgayHHHD.MaxLength = 2;
            this.txtNgayHHHD.Name = "txtNgayHHHD";
            this.txtNgayHHHD.Size = new System.Drawing.Size(30, 21);
            this.txtNgayHHHD.TabIndex = 344;
            this.txtNgayHHHD.Text = "V";
            this.txtNgayHHHD.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(284, 122);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 343;
            this.label1.Text = "Cột ngày HHHĐ";
            // 
            // txtNgayHD
            // 
            this.txtNgayHD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayHD.Location = new System.Drawing.Point(249, 117);
            this.txtNgayHD.MaxLength = 2;
            this.txtNgayHD.Name = "txtNgayHD";
            this.txtNgayHD.Size = new System.Drawing.Size(30, 21);
            this.txtNgayHD.TabIndex = 342;
            this.txtNgayHD.Text = "U";
            this.txtNgayHD.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(172, 122);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(77, 13);
            this.label15.TabIndex = 341;
            this.label15.Text = "Cột ngày HĐ";
            // 
            // txtSoHD
            // 
            this.txtSoHD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoHD.Location = new System.Drawing.Point(134, 118);
            this.txtSoHD.MaxLength = 2;
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(30, 21);
            this.txtSoHD.TabIndex = 340;
            this.txtSoHD.Text = "S";
            this.txtSoHD.VisualStyleManager = this.vsmMain;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(47, 122);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 339;
            this.label16.Text = "Cột số HĐ";
            // 
            // txtNgayHDTM
            // 
            this.txtNgayHDTM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayHDTM.Location = new System.Drawing.Point(658, 89);
            this.txtNgayHDTM.MaxLength = 2;
            this.txtNgayHDTM.Name = "txtNgayHDTM";
            this.txtNgayHDTM.Size = new System.Drawing.Size(30, 21);
            this.txtNgayHDTM.TabIndex = 338;
            this.txtNgayHDTM.Text = "BU";
            this.txtNgayHDTM.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(559, 94);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(94, 13);
            this.label13.TabIndex = 337;
            this.label13.Text = "Cột ngày HĐTM";
            // 
            // txtSoHDTM
            // 
            this.txtSoHDTM.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoHDTM.Location = new System.Drawing.Point(523, 89);
            this.txtSoHDTM.MaxLength = 2;
            this.txtSoHDTM.Name = "txtSoHDTM";
            this.txtSoHDTM.Size = new System.Drawing.Size(30, 21);
            this.txtSoHDTM.TabIndex = 336;
            this.txtSoHDTM.Text = "BT";
            this.txtSoHDTM.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(421, 96);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 13);
            this.label12.TabIndex = 335;
            this.label12.Text = "Cột số HĐTM";
            // 
            // txtNgayHHGP
            // 
            this.txtNgayHHGP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayHHGP.Location = new System.Drawing.Point(385, 90);
            this.txtNgayHHGP.MaxLength = 2;
            this.txtNgayHHGP.Name = "txtNgayHHGP";
            this.txtNgayHHGP.Size = new System.Drawing.Size(30, 21);
            this.txtNgayHHGP.TabIndex = 334;
            this.txtNgayHHGP.Text = "R";
            this.txtNgayHHGP.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(284, 95);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 333;
            this.label10.Text = "Cột ngày HHGP";
            // 
            // txtNgayGP
            // 
            this.txtNgayGP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayGP.Location = new System.Drawing.Point(249, 90);
            this.txtNgayGP.MaxLength = 2;
            this.txtNgayGP.Name = "txtNgayGP";
            this.txtNgayGP.Size = new System.Drawing.Size(30, 21);
            this.txtNgayGP.TabIndex = 332;
            this.txtNgayGP.Text = "Q";
            this.txtNgayGP.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(172, 95);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(75, 13);
            this.label9.TabIndex = 331;
            this.label9.Text = "Cột ngày GP";
            // 
            // txtSoGP
            // 
            this.txtSoGP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoGP.Location = new System.Drawing.Point(134, 91);
            this.txtSoGP.MaxLength = 2;
            this.txtSoGP.Name = "txtSoGP";
            this.txtSoGP.Size = new System.Drawing.Size(30, 21);
            this.txtSoGP.TabIndex = 330;
            this.txtSoGP.Text = "P";
            this.txtSoGP.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(47, 95);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 329;
            this.label8.Text = "Cột số GP";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoToKhai.Location = new System.Drawing.Point(385, 62);
            this.txtSoToKhai.MaxLength = 2;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(30, 21);
            this.txtSoToKhai.TabIndex = 328;
            this.txtSoToKhai.Text = "A";
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDVDT
            // 
            this.txtTenDVDT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenDVDT.Location = new System.Drawing.Point(658, 62);
            this.txtTenDVDT.MaxLength = 2;
            this.txtTenDVDT.Name = "txtTenDVDT";
            this.txtTenDVDT.Size = new System.Drawing.Size(30, 21);
            this.txtTenDVDT.TabIndex = 327;
            this.txtTenDVDT.Text = "H";
            this.txtTenDVDT.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(559, 67);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(83, 13);
            this.label7.TabIndex = 326;
            this.label7.Text = "Cột tên ĐVĐT";
            // 
            // txtMaLH
            // 
            this.txtMaLH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaLH.Location = new System.Drawing.Point(249, 63);
            this.txtMaLH.MaxLength = 2;
            this.txtMaLH.Name = "txtMaLH";
            this.txtMaLH.Size = new System.Drawing.Size(30, 21);
            this.txtMaLH.TabIndex = 321;
            this.txtMaLH.Tag = "b";
            this.txtMaLH.Text = "B";
            this.txtMaLH.VisualStyleManager = this.vsmMain;
            // 
            // txtNgayDangKy
            // 
            this.txtNgayDangKy.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayDangKy.Location = new System.Drawing.Point(134, 64);
            this.txtNgayDangKy.MaxLength = 2;
            this.txtNgayDangKy.Name = "txtNgayDangKy";
            this.txtNgayDangKy.Size = new System.Drawing.Size(30, 21);
            this.txtNgayDangKy.TabIndex = 320;
            this.txtNgayDangKy.Text = "E";
            this.txtNgayDangKy.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(47, 41);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 318;
            this.label6.Text = "Tên Sheet";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(289, 67);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(59, 13);
            this.label14.TabIndex = 313;
            this.label14.Text = "Cột số TK";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(421, 36);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(60, 13);
            this.label11.TabIndex = 319;
            this.label11.Text = "Dòng đầu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(172, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 314;
            this.label3.Text = "Cột mã LH";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(47, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 315;
            this.label4.Text = "Cột ngày ĐK";
            // 
            // txtRow
            // 
            this.txtRow.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtRow.Location = new System.Drawing.Point(523, 35);
            this.txtRow.MaxLength = 4;
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(30, 21);
            this.txtRow.TabIndex = 317;
            this.txtRow.Text = "2";
            this.txtRow.Value = 2;
            this.txtRow.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtRow.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.dgList);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(706, 389);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Kết quả import";
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgList.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgList.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(706, 389);
            this.dgList.TabIndex = 219;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(457, 418);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(123, 23);
            this.btnSave.TabIndex = 221;
            this.btnSave.Text = "Lưu vào CSDL";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(586, 418);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 220;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkSaveSetting
            // 
            this.chkSaveSetting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkSaveSetting.BackColor = System.Drawing.Color.Transparent;
            this.chkSaveSetting.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkSaveSetting.Location = new System.Drawing.Point(41, 419);
            this.chkSaveSetting.Name = "chkSaveSetting";
            this.chkSaveSetting.Size = new System.Drawing.Size(104, 23);
            this.chkSaveSetting.TabIndex = 222;
            this.chkSaveSetting.Text = "Lưu cấu hình";
            this.chkSaveSetting.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.chkSaveSetting.VisualStyleManager = this.vsmMain;
            // 
            // ImportTKForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(708, 442);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.TableOfContents);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportTKForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Import danh sách tờ khai từ Excel";
            this.Load += new System.EventHandler(this.ImportTKForm_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage1.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvFilePath;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhanLuong;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayHoanThanh;
        private System.Windows.Forms.Label label53;
        private Janus.Windows.GridEX.EditControls.EditBox txtChungTu;
        private System.Windows.Forms.Label label54;
        private Janus.Windows.GridEX.EditControls.EditBox txtTTTK;
        private System.Windows.Forms.Label label45;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDVUT;
        private System.Windows.Forms.Label label46;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayNhapXuat;
        private System.Windows.Forms.Label label47;
        private Janus.Windows.GridEX.EditControls.EditBox txtThanhLy;
        private System.Windows.Forms.Label label48;
        private Janus.Windows.GridEX.EditControls.EditBox txtNL_SP;
        private System.Windows.Forms.Label label49;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhiVC;
        private System.Windows.Forms.Label label42;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhiBaoHiem;
        private System.Windows.Forms.Label label43;
        private Janus.Windows.GridEX.EditControls.EditBox txtLPHQ;
        private System.Windows.Forms.Label label44;
        private Janus.Windows.GridEX.EditControls.EditBox txtTongTGTT;
        private System.Windows.Forms.Label label36;
        private Janus.Windows.GridEX.EditControls.EditBox txtTongTGKB;
        private System.Windows.Forms.Label label37;
        private Janus.Windows.GridEX.EditControls.EditBox txtTrongLuong;
        private System.Windows.Forms.Label label38;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoKien;
        private System.Windows.Forms.Label label39;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoPLTK;
        private System.Windows.Forms.Label label34;
        private Janus.Windows.GridEX.EditControls.EditBox txtContainer40;
        private System.Windows.Forms.Label aaaaaaa;
        private Janus.Windows.GridEX.EditControls.EditBox txtContainer20;
        private System.Windows.Forms.Label adsaf;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenChuHang;
        private System.Windows.Forms.Label label31;
        private Janus.Windows.GridEX.EditControls.EditBox txtPTTT;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.GridEX.EditControls.EditBox txtTyGiaUSD;
        private System.Windows.Forms.Label label33;
        private Janus.Windows.GridEX.EditControls.EditBox txtTyGiaTT;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguyenTe;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.EditControls.EditBox txtDKGH;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.GridEX.EditControls.EditBox txtDDXH;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtCuaKhau;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.GridEX.EditControls.EditBox txtNuocNK;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.EditBox txtNuocXK;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVD;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiVD;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayDen;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoPTVT;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtPTVT;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayHHHD;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayHD;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHD;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayHDTM;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHDTM;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayHHGP;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayGP;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGP;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDVDT;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtSheet;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaLH;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayDangKy;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label label50;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UICheckBox chkSaveSetting;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayVD;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label52;
    }
}