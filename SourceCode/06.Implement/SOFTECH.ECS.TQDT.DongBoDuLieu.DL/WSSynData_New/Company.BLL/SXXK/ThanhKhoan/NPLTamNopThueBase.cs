using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.BLL.SXXK.ThanhKhoan
{
	public partial class NPLTamNopThue : EntityBase
	{
		#region Private members.
		
		protected long _ID;
		protected int _LanThanhLy;
		protected string _MaDoanhNghiep = String.Empty;
		protected int _SoToKhai;
		protected string _MaLoaiHinh = String.Empty;
		protected short _NamDangKy;
		protected string _MaHaiQuan = String.Empty;
		protected string _MaNPL = String.Empty;
		protected DateTime _NgayNopThue = new DateTime(1900, 01, 01);
		protected decimal _NopNK;
		protected decimal _NopVAT;
		protected decimal _NopTTDB;
		protected decimal _NopCLGia;
		protected decimal _DuNK;
		protected decimal _DuVAT;
		protected decimal _DuTTDB;
		protected decimal _DuCLGia;

		#endregion
		
		//---------------------------------------------------------------------------------------------

		#region Properties.
		
		public long ID
		{
			set {this._ID = value;}
			get {return this._ID;}
		}
		public int LanThanhLy
		{
			set {this._LanThanhLy = value;}
			get {return this._LanThanhLy;}
		}
		public string MaDoanhNghiep
		{
			set {this._MaDoanhNghiep = value;}
			get {return this._MaDoanhNghiep;}
		}
		public int SoToKhai
		{
			set {this._SoToKhai = value;}
			get {return this._SoToKhai;}
		}
		public string MaLoaiHinh
		{
			set {this._MaLoaiHinh = value;}
			get {return this._MaLoaiHinh;}
		}
		public short NamDangKy
		{
			set {this._NamDangKy = value;}
			get {return this._NamDangKy;}
		}
		public string MaHaiQuan
		{
			set {this._MaHaiQuan = value;}
			get {return this._MaHaiQuan;}
		}
		public string MaNPL
		{
			set {this._MaNPL = value;}
			get {return this._MaNPL;}
		}
		public DateTime NgayNopThue
		{
			set {this._NgayNopThue = value;}
			get {return this._NgayNopThue;}
		}
		public decimal NopNK
		{
			set {this._NopNK = value;}
			get {return this._NopNK;}
		}
		public decimal NopVAT
		{
			set {this._NopVAT = value;}
			get {return this._NopVAT;}
		}
		public decimal NopTTDB
		{
			set {this._NopTTDB = value;}
			get {return this._NopTTDB;}
		}
		public decimal NopCLGia
		{
			set {this._NopCLGia = value;}
			get {return this._NopCLGia;}
		}
		public decimal DuNK
		{
			set {this._DuNK = value;}
			get {return this._DuNK;}
		}
		public decimal DuVAT
		{
			set {this._DuVAT = value;}
			get {return this._DuVAT;}
		}
		public decimal DuTTDB
		{
			set {this._DuTTDB = value;}
			get {return this._DuTTDB;}
		}
		public decimal DuCLGia
		{
			set {this._DuCLGia = value;}
			get {return this._DuCLGia;}
		}
		
		//---------------------------------------------------------------------------------------------
        
		public bool IsExist
        {
            get 
            { 
                return this.Load();  
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public bool Load()
		{
			string spName = "p_SXXK_ThanhLy_NPLTamNopThue_Load";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNopThue"))) this._NgayNopThue = reader.GetDateTime(reader.GetOrdinal("NgayNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopNK"))) this._NopNK = reader.GetDecimal(reader.GetOrdinal("NopNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopVAT"))) this._NopVAT = reader.GetDecimal(reader.GetOrdinal("NopVAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopTTDB"))) this._NopTTDB = reader.GetDecimal(reader.GetOrdinal("NopTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopCLGia"))) this._NopCLGia = reader.GetDecimal(reader.GetOrdinal("NopCLGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuNK"))) this._DuNK = reader.GetDecimal(reader.GetOrdinal("DuNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuVAT"))) this._DuVAT = reader.GetDecimal(reader.GetOrdinal("DuVAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuTTDB"))) this._DuTTDB = reader.GetDecimal(reader.GetOrdinal("DuTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuCLGia"))) this._DuCLGia = reader.GetDecimal(reader.GetOrdinal("DuCLGia"));
				return true;
			}
			return false;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public NPLTamNopThueCollection SelectCollectionBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan()
		{
			string spName = "p_SXXK_ThanhLy_NPLTamNopThue_SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			
			NPLTamNopThueCollection collection = new NPLTamNopThueCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				NPLTamNopThue entity = new NPLTamNopThue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNopThue"))) entity.NgayNopThue = reader.GetDateTime(reader.GetOrdinal("NgayNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopNK"))) entity.NopNK = reader.GetDecimal(reader.GetOrdinal("NopNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopVAT"))) entity.NopVAT = reader.GetDecimal(reader.GetOrdinal("NopVAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopTTDB"))) entity.NopTTDB = reader.GetDecimal(reader.GetOrdinal("NopTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopCLGia"))) entity.NopCLGia = reader.GetDecimal(reader.GetOrdinal("NopCLGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuNK"))) entity.DuNK = reader.GetDecimal(reader.GetOrdinal("DuNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuVAT"))) entity.DuVAT = reader.GetDecimal(reader.GetOrdinal("DuVAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuTTDB"))) entity.DuTTDB = reader.GetDecimal(reader.GetOrdinal("DuTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuCLGia"))) entity.DuCLGia = reader.GetDecimal(reader.GetOrdinal("DuCLGia"));
				collection.Add(entity);
			}
			return collection;
		}

		//---------------------------------------------------------------------------------------------
		public NPLTamNopThueCollection SelectCollectionBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan()
		{
			string spName = "p_SXXK_ThanhLy_NPLTamNopThue_SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			
			NPLTamNopThueCollection collection = new NPLTamNopThueCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
			while (reader.Read())
			{
				NPLTamNopThue entity = new NPLTamNopThue();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNopThue"))) entity.NgayNopThue = reader.GetDateTime(reader.GetOrdinal("NgayNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopNK"))) entity.NopNK = reader.GetDecimal(reader.GetOrdinal("NopNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopVAT"))) entity.NopVAT = reader.GetDecimal(reader.GetOrdinal("NopVAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopTTDB"))) entity.NopTTDB = reader.GetDecimal(reader.GetOrdinal("NopTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopCLGia"))) entity.NopCLGia = reader.GetDecimal(reader.GetOrdinal("NopCLGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuNK"))) entity.DuNK = reader.GetDecimal(reader.GetOrdinal("DuNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuVAT"))) entity.DuVAT = reader.GetDecimal(reader.GetOrdinal("DuVAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuTTDB"))) entity.DuTTDB = reader.GetDecimal(reader.GetOrdinal("DuTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuCLGia"))) entity.DuCLGia = reader.GetDecimal(reader.GetOrdinal("DuCLGia"));
				collection.Add(entity);
			}
			return collection;
		}

		//---------------------------------------------------------------------------------------------

		public DataSet SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan()
		{
			string spName = "p_SXXK_ThanhLy_NPLTamNopThue_SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
						
            return this.db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------
		public DataSet SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan()
		{
			string spName = "p_SXXK_ThanhLy_NPLTamNopThue_SelectBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
						
            return this.db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public DataSet SelectAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLTamNopThue_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_ThanhLy_NPLTamNopThue_SelectAll";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_ThanhLy_NPLTamNopThue_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------

		public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            string spName = "p_SXXK_ThanhLy_NPLTamNopThue_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return this.db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		public NPLTamNopThueCollection SelectCollectionAll()
		{
			NPLTamNopThueCollection collection = new NPLTamNopThueCollection();

			IDataReader reader = this.SelectReaderAll();
			while (reader.Read())
			{
				NPLTamNopThue entity = new NPLTamNopThue();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNopThue"))) entity.NgayNopThue = reader.GetDateTime(reader.GetOrdinal("NgayNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopNK"))) entity.NopNK = reader.GetDecimal(reader.GetOrdinal("NopNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopVAT"))) entity.NopVAT = reader.GetDecimal(reader.GetOrdinal("NopVAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopTTDB"))) entity.NopTTDB = reader.GetDecimal(reader.GetOrdinal("NopTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopCLGia"))) entity.NopCLGia = reader.GetDecimal(reader.GetOrdinal("NopCLGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuNK"))) entity.DuNK = reader.GetDecimal(reader.GetOrdinal("DuNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuVAT"))) entity.DuVAT = reader.GetDecimal(reader.GetOrdinal("DuVAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuTTDB"))) entity.DuTTDB = reader.GetDecimal(reader.GetOrdinal("DuTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuCLGia"))) entity.DuCLGia = reader.GetDecimal(reader.GetOrdinal("DuCLGia"));
				collection.Add(entity);
			}
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public NPLTamNopThueCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			NPLTamNopThueCollection collection = new NPLTamNopThueCollection();

			IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				NPLTamNopThue entity = new NPLTamNopThue();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayNopThue"))) entity.NgayNopThue = reader.GetDateTime(reader.GetOrdinal("NgayNopThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopNK"))) entity.NopNK = reader.GetDecimal(reader.GetOrdinal("NopNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopVAT"))) entity.NopVAT = reader.GetDecimal(reader.GetOrdinal("NopVAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopTTDB"))) entity.NopTTDB = reader.GetDecimal(reader.GetOrdinal("NopTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("NopCLGia"))) entity.NopCLGia = reader.GetDecimal(reader.GetOrdinal("NopCLGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuNK"))) entity.DuNK = reader.GetDecimal(reader.GetOrdinal("DuNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuVAT"))) entity.DuVAT = reader.GetDecimal(reader.GetOrdinal("DuVAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuTTDB"))) entity.DuTTDB = reader.GetDecimal(reader.GetOrdinal("DuTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("DuCLGia"))) entity.DuCLGia = reader.GetDecimal(reader.GetOrdinal("DuCLGia"));
				collection.Add(entity);
			}
            reader.Close();
			return collection;			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public long Insert()
		{
			return this.InsertTransaction(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long InsertTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ThanhLy_NPLTamNopThue_Insert";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@NgayNopThue", SqlDbType.DateTime, this._NgayNopThue);
			this.db.AddInParameter(dbCommand, "@NopNK", SqlDbType.Money, this._NopNK);
			this.db.AddInParameter(dbCommand, "@NopVAT", SqlDbType.Money, this._NopVAT);
			this.db.AddInParameter(dbCommand, "@NopTTDB", SqlDbType.Money, this._NopTTDB);
			this.db.AddInParameter(dbCommand, "@NopCLGia", SqlDbType.Money, this._NopCLGia);
			this.db.AddInParameter(dbCommand, "@DuNK", SqlDbType.Money, this._DuNK);
			this.db.AddInParameter(dbCommand, "@DuVAT", SqlDbType.Money, this._DuVAT);
			this.db.AddInParameter(dbCommand, "@DuTTDB", SqlDbType.Money, this._DuTTDB);
			this.db.AddInParameter(dbCommand, "@DuCLGia", SqlDbType.Money, this._DuCLGia);
			
			if (transaction != null)
			{
				this.db.ExecuteNonQuery(dbCommand, transaction);
				this._ID = (long) this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}
            else
			{
				this.db.ExecuteNonQuery(dbCommand);
				this._ID = (long) this.db.GetParameterValue(dbCommand, "@ID");
				return this._ID;
			}			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool Insert(NPLTamNopThueCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLTamNopThue item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
						{							
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
                    	transaction.Commit();
                    	ret = true;
					}
					else
					{
                    	transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		
		//---------------------------------------------------------------------------------------------		
        
		public void InsertTransaction(SqlTransaction transaction, NPLTamNopThueCollection collection)
        {
            foreach (NPLTamNopThue item in collection)
            {
               	item.InsertTransaction(transaction);
            }
        }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		public int InsertUpdate()
		{
			return this.InsertUpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdateTransaction(SqlTransaction transaction)
		{			
			string spName = "p_SXXK_ThanhLy_NPLTamNopThue_InsertUpdate";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@NgayNopThue", SqlDbType.DateTime, this._NgayNopThue);
			this.db.AddInParameter(dbCommand, "@NopNK", SqlDbType.Money, this._NopNK);
			this.db.AddInParameter(dbCommand, "@NopVAT", SqlDbType.Money, this._NopVAT);
			this.db.AddInParameter(dbCommand, "@NopTTDB", SqlDbType.Money, this._NopTTDB);
			this.db.AddInParameter(dbCommand, "@NopCLGia", SqlDbType.Money, this._NopCLGia);
			this.db.AddInParameter(dbCommand, "@DuNK", SqlDbType.Money, this._DuNK);
			this.db.AddInParameter(dbCommand, "@DuVAT", SqlDbType.Money, this._DuVAT);
			this.db.AddInParameter(dbCommand, "@DuTTDB", SqlDbType.Money, this._DuTTDB);
			this.db.AddInParameter(dbCommand, "@DuCLGia", SqlDbType.Money, this._DuCLGia);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		
		public bool InsertUpdate(NPLTamNopThueCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLTamNopThue item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public int Update()
		{
			return this.UpdateTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int UpdateTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ThanhLy_NPLTamNopThue_Update";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);

			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
			this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
			this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
			this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
			this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
			this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
			this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
			this.db.AddInParameter(dbCommand, "@NgayNopThue", SqlDbType.DateTime, this._NgayNopThue);
			this.db.AddInParameter(dbCommand, "@NopNK", SqlDbType.Money, this._NopNK);
			this.db.AddInParameter(dbCommand, "@NopVAT", SqlDbType.Money, this._NopVAT);
			this.db.AddInParameter(dbCommand, "@NopTTDB", SqlDbType.Money, this._NopTTDB);
			this.db.AddInParameter(dbCommand, "@NopCLGia", SqlDbType.Money, this._NopCLGia);
			this.db.AddInParameter(dbCommand, "@DuNK", SqlDbType.Money, this._DuNK);
			this.db.AddInParameter(dbCommand, "@DuVAT", SqlDbType.Money, this._DuVAT);
			this.db.AddInParameter(dbCommand, "@DuTTDB", SqlDbType.Money, this._DuTTDB);
			this.db.AddInParameter(dbCommand, "@DuCLGia", SqlDbType.Money, this._DuCLGia);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public void UpdateCollection(NPLTamNopThueCollection collection, SqlTransaction transaction)
        {
            foreach (NPLTamNopThue item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int Delete()
		{
			return this.DeleteTransaction(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int DeleteTransaction(SqlTransaction transaction)
		{
			string spName = "p_SXXK_ThanhLy_NPLTamNopThue_Delete";		
			SqlCommand dbCommand = (SqlCommand) this.db.GetStoredProcCommand(spName);
			
			this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
			
			if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
        public void DeleteCollection(NPLTamNopThueCollection collection, SqlTransaction transaction)
        {
            foreach (NPLTamNopThue item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

		//---------------------------------------------------------------------------------------------
		
		public bool DeleteCollection(NPLTamNopThueCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
					bool ret01 = true;
                    foreach (NPLTamNopThue item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
						{
							ret01 = false;
							break;
						}
                    }
					if (ret01)
					{
						transaction.Commit();
						ret = true;
					}
					else
					{
						transaction.Rollback();
						ret = false;                    	
					}
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally 
                {
                    connection.Close();
                }
            }
            return ret;		
		}
		#endregion
        public NPLTamNopThue SelectEntityBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuanAndMaNPL(SqlTransaction transaction)
        {
            string spName = "[p_SXXK_ThanhLy_NPLTamNopThue_SelectBy_KeyPhu]";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this.MaNPL);

            NPLTamNopThue entity = null;
            IDataReader reader = null;
            if (transaction == null)
                reader = this.db.ExecuteReader(dbCommand);
            else
                reader = this.db.ExecuteReader(dbCommand, transaction);
            while (reader.Read())
            {
                entity = new NPLTamNopThue();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayNopThue"))) entity.NgayNopThue = reader.GetDateTime(reader.GetOrdinal("NgayNopThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("NopNK"))) entity.NopNK = reader.GetDecimal(reader.GetOrdinal("NopNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("NopVAT"))) entity.NopVAT = reader.GetDecimal(reader.GetOrdinal("NopVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NopTTDB"))) entity.NopTTDB = reader.GetDecimal(reader.GetOrdinal("NopTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("NopCLGia"))) entity.NopCLGia = reader.GetDecimal(reader.GetOrdinal("NopCLGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DuNK"))) entity.DuNK = reader.GetDecimal(reader.GetOrdinal("DuNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("DuVAT"))) entity.DuVAT = reader.GetDecimal(reader.GetOrdinal("DuVAT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DuTTDB"))) entity.DuTTDB = reader.GetDecimal(reader.GetOrdinal("DuTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DuCLGia"))) entity.DuCLGia = reader.GetDecimal(reader.GetOrdinal("DuCLGia"));                
            }
            reader.Close();
            return entity;
        }
        public void InsertUpdateTransaction2(SqlTransaction transaction)
        {
            NPLTamNopThue tmp = this.SelectEntityBy_LanThanhLy_AND_MaDoanhNghiep_AND_SoToKhai_AND_MaLoaiHinh_AND_NamDangKy_AND_MaHaiQuanAndMaNPL(transaction);
            if (tmp == null)
                this.InsertTransaction(transaction);
            else
            {
                this.ID = tmp.ID;
                this.UpdateTransaction(transaction);
            }
        }
        public static void updateDatabase(DataSet ds, SqlTransaction transaction)
        {
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                NPLTamNopThue npl = new NPLTamNopThue();
                try
                {
                    npl._DuCLGia = Convert.ToDecimal(row["DU_CLGIA"]);
                }
                catch { }
                try
                {
                    npl._DuNK = Convert.ToDecimal(row["DU_NK"]);
                }
                catch { }
                try
                {
                    npl._DuTTDB = Convert.ToDecimal(row["DU_TTDB"]);
                }
                catch { }
                try
                {
                    npl._DuVAT = Convert.ToDecimal(row["DU_VAT"]);
                }
                catch { }
               
               
              
                npl._LanThanhLy = Convert.ToInt32(row["LAN_TL"].ToString());
                npl._MaDoanhNghiep = (row["MA_DV"].ToString());
                npl._MaHaiQuan = (row["MA_HQ"].ToString());
                npl._MaLoaiHinh = (row["MA_LH"].ToString());
                npl._MaNPL = (row["MA_NPL"].ToString());
                npl._NamDangKy = Convert.ToInt16(row["NAMDK"].ToString());
                npl._NgayNopThue = Convert.ToDateTime(row["NGAY_NT"].ToString());
                npl._SoToKhai = Convert.ToInt32(row["SOTK"].ToString());
                npl._NopCLGia = Convert.ToDecimal(row["NOP_CLGIA"]);
                npl._NopNK = Convert.ToDecimal(row["NOP_NK"]);
                npl._NopTTDB = Convert.ToDecimal(row["NOP_TTDB"]);
                npl._NopVAT = Convert.ToDecimal(row["NOP_VAT"]);
                npl.InsertUpdateTransaction2(transaction);
            }
        }

	}	
}