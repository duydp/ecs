using System;
using System.Data;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Collections;
using Company.Interface.KDT.SXXK;
using System.Drawing;
using Company.KDT.SHARE.Components;
using Company.Interface.SXXK.BangKeThanhLy;
namespace Company.Interface.SXXK
{
    public partial class DinhMucRegistedForm : BaseForm
    {

        public DinhMucCollection dmCollection = new DinhMucCollection();
        public NguyenPhuLieuCollection nplcollection = new NguyenPhuLieuCollection();
        public SanPhamCollection sp = new SanPhamCollection();
        DinhMuc dm = new DinhMuc();
        public bool isBrower = false;
        NguyenPhuLieu nplupdate = new NguyenPhuLieu();
        public string maSP = "";
        string tenNPL = "";
        public DinhMucCollection dmUpdateCollection = new DinhMucCollection();
        public DinhMucRegistedForm()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            try
            {
                //DinhMucCollection dmCollection = new DinhMucCollection();

                dmCollection = dm.getSanPhamDinhMucCollection(ctrCoQuanHQ.Ma, GlobalSettings.MA_DON_VI, txtMaSP.Text.Trim(), txtMaNPL.Text.Trim());
                dgList.DataSource = dmCollection;

                lblTongSP.Text = dmCollection.Count.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            try
            {
                // Đơn vị tính.
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                // Đơn vị Hải quan.
                this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
                ctrCoQuanHQ.Ma = GlobalSettings.MA_HAI_QUAN;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Có lỗi trong quá trình xử lý :\r\n" + ex.Message, false);
            }

        }

        //-----------------------------------------------------------------------------------------

        private void DinhMucRegistedForm_Load(object sender, EventArgs e)
        {
            try
            {
                //dgList.GroupByBoxVisible = true;

                lblTongSP.Text = "0";
                //btnExportExcel.Visible = true;

                GlobalSettings.KhoiTao_GiaTriMacDinh();
                dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
                dgList.Tables[0].Columns["DinhMucChung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;
                dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
                this.khoitao_DuLieuChuan();
                this.setDataToComboUserKB();
                cbUserKB.SelectedIndex = 0;
                // Sản phẩm đã đăng ký.
                this.BindData();
                // Doanh nghiệp / Đại lý TTHQ.
                if (isBrower)
                {
                    btnClose.Visible = false;
                    //cmdAddNew.Visible = false;
                    //cmdSave.Visible = false;
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                }
                if (MainForm.versionHD == 0)
                {
                    if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.CapNhatDuLieu)))
                    {
                        dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                        //cmdAddNew.Visible = false;
                        //cmdSave.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        //-----------------------------------------------------------------------------------------

        private void btnGetListFromHQ_Click(object sender, EventArgs e)
        {

        }

        //-----------------------------------------------------------------------------------------


        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                Company.BLL.KDT.SXXK.KDT_LenhSanXuat lenhhXS = new Company.BLL.KDT.SXXK.KDT_LenhSanXuat();
                lenhhXS = Company.BLL.KDT.SXXK.KDT_LenhSanXuat.Load(Convert.ToInt64(e.Row.Cells["LenhSanXuat_ID"].Value));
                if (lenhhXS != null)
                    e.Row.Cells["LenhSanXuat_ID"].Text = lenhhXS.SoLenhSanXuat;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }
        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            //if (isBrower)
            //{
            //    if (e.Row.RowType == RowType.GroupHeader)
            //    {
            //        DinhMuc dmSelect = (DinhMuc)dgList.GetRow().GetChildRows()[0].DataRow;
            //        maSP = dmSelect.MaSanPHam;
            //        this.Close();
            //    }
            //}
            //minhnd
            //try
            //{
            //    frmThemTKBK01_HSTL obj = new frmThemTKBK01_HSTL();

            //    GridEXRow dr = dgList.SelectedItems[0].GetRow();
            //    obj.MaNPL = dr.Cells["MaNguyenPhuLieu"].Text;
            //    obj.lblMaNPL.Text = dr.Cells["MaNguyenPhuLieu"].Text;
            //    obj.lblTenNPL.Text = dr.Cells["TenNPL"].Text;
            //    //obj.lblLuongNhap.Text = dr.Cells["LuongNPLNhap"].Text;
            //    //obj.lblLuongXuat.Text = dr.Cells["LuongNPLXuat"].Text;
            //    //obj.lblLuongTon.Text = dr.Cells["LuongNPLTon"].Text;
            //    //obj.HSTL = this.HSTL;
            //    obj.ShowDialog(this);
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
            
        }

        private void cmdAddNew_Click(object sender, EventArgs e)
        {
            try
            {
                DinhMucEditForm f = new DinhMucEditForm();
                f.OpenType = OpenFormType.Insert;
                f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.maSP = this.maSP;
                // f.DMCollection = this.dmCollection ;      

                f.ShowDialog(this);
                this.maSP = f.DMDetail.MaSanPHam;
                f.DMDetail.MaSanPHam = f.maSP;
                // this.dmCollection = f.DMCollection;
                // dgList.DataSource =f.DMCollection  ; 
                // dgList.Refetch();
                this.BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (dm.InsertUpdateGrid(dmCollection))
                {
                    //ShowMessage("Cập nhật thành công !", false);
                    MLMessages("Cập nhật thành công !", "MSG_SAV02", "", false);
                    BindData();
                }
                else
                    MLMessages("Cập nhật không thành công !", "MSG_SAV01", "", false);
            }
            catch (Exception ex)
            {
                MLMessages("Cập nhật không thành công !" + ex.Message, "MSG_SAV01", "", false);
            }

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            this.Xoa();

        }

        // Deleting Method
        private void Xoa()
        {
            try
            {
                int count = 0;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count <= 0) return;
                //if (ShowMessage("Bạn có chắc chắn là xóa định mức này không ?", true) == "Yes")
                if (MLMessages("Bạn có chắc chắn là xóa định mức này không ?", "MSG_DEL01", "", true) == "Yes")
                {

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMuc dinhmuc = (DinhMuc)i.GetRow().DataRow;
                            dinhmuc.Delete();
                            count++;
                        }

                    }
                    if (count > 0)
                    {
                        if (GlobalSettings.NGON_NGU == "0")
                            ShowMessage(count.ToString() + " định mức đã được xóa ", false);
                        else
                            ShowMessage(count.ToString() + " norm has been deleted ", false);
                        ThongTinDinhMuc.Delete(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                    }
                    BindData();

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                int count = 0;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count <= 0) return;
                // if (ShowMessage("Bạn có chắc chắn là xóa các định mức này không ?", true) == "Yes")
                if (MLMessages("Bạn có chắc chắn là xóa định mức này không ?", "MSG_DEL01", "", true) == "Yes")
                {

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DinhMuc dinhmuc = (DinhMuc)i.GetRow().DataRow;
                            dinhmuc.Delete();
                            count++;
                        }

                    }
                    if (count > 0)
                    {
                        //ShowMessage(count.ToString() + " định mức đã được xóa ", false);
                        if (GlobalSettings.NGON_NGU == "0")
                            ShowMessage(count.ToString() + " định mức đã được xóa ", false);
                        else
                            ShowMessage(count.ToString() + " norm has been deleted ", false);
                        ThongTinDinhMuc.Delete(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                    }

                }
                else
                    e.Cancel = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            try
            {
                SelectSanPhamInDMForm f = new SelectSanPhamInDMForm();
                f.ShowDialog(this);
                if (f.spCollectionSelect.Count > 0)
                {
                    Report.ReportViewDinhMucForm fDM = new Company.Interface.Report.ReportViewDinhMucForm();
                    fDM.spCollection = f.spCollectionSelect;
                    fDM.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private DataTable dtDM;
        private void searchNguoiKhaiBao(string userName)
        {
            try
            {
                dmCollection = new DinhMuc().GetDMFromUserName(userName);
                dgList.DataSource = dmCollection;
            }
            catch (Exception ex)
            {
                ShowMessage("Có lỗi trong quá trình Tìm kiếm.\r\nChi tiết lỗi: " + ex.Message, false);
                return;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (!uiDataError.Checked)
                {
                    if (cbUserKB.SelectedValue != null && cbUserKB.SelectedIndex != 0)
                        searchNguoiKhaiBao(cbUserKB.SelectedItem.Value.ToString());
                    else
                        BindData();
                }
                else
                {
                    dmCollection = dm.getSanPhamDinhMucLoiCollection(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, txtMaSP.Text.Trim(), txtMaNPL.Text.Trim());
                    dgList.DataSource = dmCollection;
                }

                lblTongSP.Text = dmCollection.Count.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }           
        }

        private void txtMaSP_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dgList_UpdatingCell(object sender, UpdatingCellEventArgs e)
        {
            try
            {
                if (e.Column.Key == "DinhMucSuDung")
                {
                    if (Convert.ToDecimal(e.Value) > 1000000000)
                    {
                        //ShowMessage("Định mức sử dụng phải nhỏ hơn 1.000.000.000",false);
                        if (GlobalSettings.NGON_NGU == "0")
                            ShowMessage("Định mức sử dụng phải nhỏ hơn 1.000.000.000 ", false);
                        else
                            ShowMessage(" Used norm must be less than 1.000.000.000 ", false);
                        e.Cancel = true;
                    }
                }
                else
                {
                    if (Convert.ToDecimal(e.Value) > 100) e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            Xoa();
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (e.Row.Cells["TenNPL"].Text == "")
                    {
                        e.Row.RowStyle = new GridEXFormatStyle();
                        e.Row.RowStyle.ForeColor = Color.Red;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void setDataToComboUserKB()
        {
            try
            {
                DataTable dt = Company.QuanTri.User.SelectAll().Tables[0];
                DataRow dr = dt.NewRow();
                dr["USER_NAME"] = -1;
                dr["HO_TEN"] = "--Tất cả--";
                dt.Rows.InsertAt(dr, 0);
                cbUserKB.DataSource = dt;
                cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
                cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void cbUserKB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbUserKB.SelectedValue != null && cbUserKB.SelectedIndex != 0)
            {
                txtMaSP.Text = "";
                txtMaSP.Enabled = false;
            }
            else
            {
                txtMaSP.Text = "";
                txtMaSP.Enabled = true;
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DinhMucDaDangKy_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) == DialogResult.OK)
                {
                    if (sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xuất ra file Excel.", false);
            }
        }

        private void chkGroup_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (!chkGroup.Checked)
                {
                    //Removing any group present in the table

                    dgList.RootTable.Groups.Clear();

                    dgList.RootTable.Columns["MaSanPham"].Visible = true;
                }
                else
                {
                    //Grouping by columns ‘Country" and ‘City’ in the root table of a GridEX control.

                    GridEXGroup group = new GridEXGroup();

                    GridEXColumn column = new GridEXColumn();

                    //Removing any group present in the table

                    dgList.RootTable.Groups.Clear();

                    //get the column to be grouped

                    column = dgList.RootTable.Columns["MaSanPham"];

                    //create the group

                    group = new GridEXGroup(column, Janus.Windows.GridEX.SortOrder.Ascending);

                    //add the group to the Groups collection of the table

                    dgList.RootTable.Groups.Add(group);
                }
            }
            catch (Exception ex) 
            { 
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void cmMain_CommandClick_1(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdAdd":
                    cmdAddNew_Click(null, null);
                    break;
                case "cmdSave":
                    cmdSave_Click(null, null);
                    break;
                case "cmdExportExcel":
                    btnExportExcel_Click(null, null);
                    break;
                case "cmdDelete":
                    btnDelete_Click(null, null);
                    break;
                case "cmdPrint":
                    uiButton2_Click(null, null);
                    break;
            }
        }



    }
}