using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using System.Globalization;
using System.Data.Common;
using Logger;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report.GC
{
    public partial class BKToKhaiXuatTT38 : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HopDong = new HopDong();

        public string DSTK = "";

        public BKToKhaiXuatTT38()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            DienThongTinChung();
            DataTable tb= GetDataSource().Tables[0];
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("SoToKhai"),
                new DevExpress.XtraReports.UI.GroupField("MaLoaiHinh"),
                new DevExpress.XtraReports.UI.GroupField("NgayToKhai"),
                new DevExpress.XtraReports.UI.GroupField("MaHaiQuan"),
            });
            this.DetailReport1.DataSource = tb;
        }

        private void DienThongTinChung()
        {
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblSoHopDongDetail.Text = HopDong.SoHopDong;
            lblNgayHoSoThanhKhoan.Text = HopDong.NgayDangKy.ToShortDateString();
        }
        private DataSet GetDataSource()
        {
            try
            {
                string spName = "SELECT CASE WHEN b.MaLoaiHinh like '%V%' "
                                    + "THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = b.SoToKhai) "
                                    + "ELSE b.SoToKhai END AS SoToKhai, "
                                    + "RIGHT(b.MaLoaiHinh,3) AS MaLoaiHinh, "
                                    + "CONVERT(NVARCHAR(11),b.NgayDangKy,103) AS NgayToKhai, "
                                    + "b.MaHaiQuan, "
                                    + "c.MaPhu AS MaNPL, "
                                    + "c.TenHang AS TenNPL, "
                                    + "c.MaHS, "
                                    + "d.Ten AS DVT, "
                                    + "CAST(c.SoLuong AS FLOAT) AS LuongXuat, "
                                    + "CAST(c.DonGiaKB AS FLOAT) AS DonGiaUSD, "
                                    + "CAST(c.TriGiaKB AS FLOAT) AS TriGiaUSD, "
                                    + "c.NuocXX_ID AS XuatXu, "
                                    + "CAST(b.TyGiaTinhThue AS FLOAT) AS TyGia, "
                                    + "CAST(c.TriGiaTT AS DECIMAL(38,3)) AS TriGiaVND, "
                                    + "CAST(c.TriGiaKB*b.TyGiaTinhThue  AS DECIMAL(38,3))  "
                                + "FROM t_KDT_ToKhaiMauDich AS b LEFT JOIN t_KDT_HangMauDich AS c  ON b.ID = c.TKMD_ID "
                                                              + "JOIN t_HaiQuan_DonViTinh AS d ON c.DVT_ID = d.ID  "
                                + "WHERE b.IDHopDong =" + HopDong.ID + " AND b.MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "' AND b.MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "' AND b.MaLoaiHinh Like 'X%' AND b.SoToKhai IN (" + DSTK + ") "
                                + "UNION ALL "
                      + "SELECT CASE WHEN b.MaLoaiHinh like '%V%' "
                                            + "THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = b.SoToKhai) "
                                            + "ELSE b.SoToKhai END AS SoToKhai, "
                                    + "RIGHT(CAST(b.MaLoaiHinh AS varchar(5)),3) AS MaLoaiHinh, "
                                    + "CONVERT(NVARCHAR(11),b.NgayDangKy,103) AS NgayToKhai, "
                                    + "b.MaHaiQuanTiepNhan, "
                                    + "c.MaHang AS MaNPL, "
                                    + "c.TenHang AS TenNPL, "
                                    + "c.MaHS, "
                                    + "d.Ten AS DVT, "
                                    + "CAST(c.SoLuong AS FLOAT) AS LuongXuat, "
                                    + "CAST(c.DonGia AS FLOAT) AS DonGiaUSD, "
                                    + "CAST(c.TriGia AS FLOAT) AS TriGiaUSD, "
                                    + "c.ID_NuocXX AS XuatXu, "
                                    + "CASE WHEN EXISTS(SELECT * FROM t_KDT_VNACC_TK_PhanHoi_TyGia AS temp WHERE b.ID = temp.Master_ID) "
                                        + "THEN (SELECT TOP 1 temp.TyGiaTinhThue FROM t_KDT_VNACC_TK_PhanHoi_TyGia AS temp "
                                        + "WHERE temp.Master_ID = (SELECT TOP 1 ID "
                                                                + "FROM t_KDT_VNACC_ToKhaiMauDich AS tkvnacc "
                                                                + "WHERE tkvnacc.SoToKhai = b.HuongDan_PL)) "
                                        + "ELSE 0 END AS TyGiaTinhThue, "
                                    + "CAST(c.TriGiaTT AS DECIMAL(38,3)) AS TriGiaTinhThue, "
                                    
                                    + "CAST(c.TriGia * (CASE WHEN EXISTS(SELECT * FROM t_KDT_VNACC_TK_PhanHoi_TyGia AS temp WHERE b.ID = temp.Master_ID) "
                                        + "THEN (SELECT TOP 1 temp.TyGiaTinhThue FROM t_KDT_VNACC_TK_PhanHoi_TyGia AS temp "
                                                + "WHERE temp.Master_ID = (SELECT TOP 1 ID "
                                                                + "FROM t_KDT_VNACC_ToKhaiMauDich AS tkvnacc "
                                                                + "WHERE tkvnacc.SoToKhai = b.HuongDan_PL)) "
                                                                + "ELSE 0 END)  AS DECIMAL(38,3))  "
                                + "FROM t_KDT_GC_ToKhaiChuyenTiep AS b LEFT JOIN [t_KDT_GC_HangChuyenTiep] AS c  ON b.ID = c.Master_ID "
                                                                    + "JOIN t_HaiQuan_DonViTinh AS d ON c.ID_DVT = d.ID "
                                + "WHERE b.IDHopDong =" + HopDong.ID + " AND b.MaHaiQuanTiepNhan = '" + GlobalSettings.MA_HAI_QUAN + "' AND b.MaDoanhNghiep ='" + GlobalSettings.MA_DON_VI + "' AND b.MaLoaiHinh Like 'X%' AND b.SoToKhai IN (" + DSTK + ") ";

                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                DbCommand cmd = db.GetSqlStringCommand(spName);

                return db.ExecuteDataSet(cmd);
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }

        private void BKToKhaiXuatTT38_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
    }
}


