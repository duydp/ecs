using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhCapPhepNKThuocCoChuaThuocGayNghien_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public QuyetDinhCapPhepNKThuocCoChuaThuocGayNghien_2()
        {
            InitializeComponent();
        }
        public void BindingReport(VAGP130 vagp)
        {
            int HangHoa = vagp.HangHoa.Count;
            if (HangHoa <= 5)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa / 5, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            DataTable dt = new DataTable();
            if (vagp != null && vagp.HangHoa.Count > 0)
            {
                int STT = 0;
                dt.Columns.Add("STT", typeof(string));
                dt.Columns.Add("TenThuoc", typeof(string));
                dt.Columns.Add("MaSoHH", typeof(string));
                dt.Columns.Add("HoatChat", typeof(string));
                dt.Columns.Add("TieuChuanChatLuong", typeof(string));
                dt.Columns.Add("SoDangKy", typeof(string));
                dt.Columns.Add("HanDung", typeof(string));
                dt.Columns.Add("SoLuong", typeof(string));
                dt.Columns.Add("DVTSoLuong", typeof(string));
                dt.Columns.Add("TenHoatChatGayNghien", typeof(string));
                dt.Columns.Add("CongDung", typeof(string));
                dt.Columns.Add("TongSoKLHoatChatGayNghien", typeof(string));
                dt.Columns.Add("DVTKhoiLuong", typeof(string));
                for (int i = 0; i < vagp.HangHoa.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["STT"] = STT.ToString().Trim();
                    dr["TenThuoc"] = vagp.HangHoa[i].MDC.GetValue().ToString().Trim();
                    dr["MaSoHH"] = vagp.HangHoa[i].HSC.GetValue().ToString().Trim();
                    dr["HoatChat"] = vagp.HangHoa[i].ACE.GetValue().ToString().Trim();
                    dr["TieuChuanChatLuong"] = vagp.HangHoa[i].QSD.GetValue().ToString().Trim();
                    dr["SoDangKy"] = vagp.HangHoa[i].RNO.GetValue().ToString().Trim();
                    if (Convert.ToDateTime(vagp.HangHoa[i].EOM.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
                    {
                        dr["HanDung"] = Convert.ToDateTime(vagp.HangHoa[i].EOM.GetValue()).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        dr["HanDung"] = "";
                    }
                    dr["SoLuong"] = vagp.HangHoa[i].QTT.GetValue().ToString().Trim();
                    dr["DVTSoLuong"] = vagp.HangHoa[i].QTU.GetValue().ToString().Trim();
                    dr["TenHoatChatGayNghien"] = vagp.HangHoa[i].ACA.GetValue().ToString().Trim();
                    dr["CongDung"] = vagp.HangHoa[i].EFF.GetValue().ToString().Trim();
                    dr["TongSoKLHoatChatGayNghien"] = vagp.HangHoa[i].TWE.GetValue().ToString().Trim();
                    dr["DVTKhoiLuong"] = vagp.HangHoa[i].TWU.GetValue().ToString().Trim();
                    
           
                    dt.Rows.Add(dr);
                }
                BindingReportHang(dt);
            }
        }
        public void BindingReportHang(DataTable dt)
        {
            DetailReport.DataSource = dt;
            lblSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            lblTenThuoc.DataBindings.Add("Text", DetailReport.DataSource, "TenThuoc");
            lblMaSoHH.DataBindings.Add("Text", DetailReport.DataSource, "MaSoHH");
            lblHoatChat.DataBindings.Add("Text", DetailReport.DataSource, "HoatChat");
            lblTieuChuanChatLuong.DataBindings.Add("Text", DetailReport.DataSource, "TieuChuanChatLuong");
            lblSoDangKy.DataBindings.Add("Text", DetailReport.DataSource, "SoDangKy");
            lblHanDung.DataBindings.Add("Text", DetailReport.DataSource, "HanDung");
            lblSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "SoLuong");
            lblDVTSoLuong.DataBindings.Add("Text", DetailReport.DataSource, "DVTSoLuong");
            lblTenHoatChatGayNghien.DataBindings.Add("Text", DetailReport.DataSource, "TenHoatChatGayNghien");
            lblCongDung.DataBindings.Add("Text", DetailReport.DataSource, "CongDung");
            lblTongSoKLHoatChatGayNghien.DataBindings.Add("Text", DetailReport.DataSource, "TongSoKLHoatChatGayNghien");
            lblDVTKhoiLuong.DataBindings.Add("Text", DetailReport.DataSource, "DVTKhoiLuong");
        }

        private void lblSoTrang_PrintOnPage(object sender, PrintOnPageEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            cell.Text = (e.PageIndex + 2).ToString();
        }


        


    }
}
