﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.GC
{
    class NodeTKNhapChuyenTiep
    {
        /// <summary>
        /// Loại chứng từ (=986)	1	an..3	Danh mục chuẩn
        /// </summary>
        public const string issuer = "issuer";

        /// <summary>
        /// Số tham chiếu chứng từ 	1	an..35 ||Số hợp đồng
        /// </summary>
        public const string reference = "reference";

        /// <summary>
        /// Ngày khai chứng từ 	1	an19	YYYY-MM-DD HH:mm:ss || Ngày hợp đồng
        /// </summary>
        public const string issue = "issue";

        /// <summary>
        /// Chức năng của chứng từ (=8)	1	n..2	Danh mục chuẩn
        /// </summary>
        public const string function = "function";

        /// <summary>
        /// Nơi khai báo	0	an..60
        /// </summary>
        public const string issueLocation = "issueLocation";

        /// <summary>
        /// Trạng thái chứng từ 	1	an..3	Danh mục chuẩn || Trạng thái đại lý
        /// </summary>
        public const string status = "status";

        /// <summary>
        /// Số đăng ký tờ khai 		an..35
        /// </summary>
        public const string customsReference = "customsReference";

        /// <summary>
        /// Ngày đăng ký chứng từ 		an19	YYYY-MM-DD HH:mm:ss
        /// </summary>
        public const string acceptance = "acceptance";

        /// <summary>
        /// Mã hải quan 	1	an..6	Danh mục chuẩn
        /// </summary>
        public const string declarationOffice = "declarationOffice";

        /// <summary>
        /// Số lượng hàng	1	n..5
        /// </summary>
        public const string goodsItem = "goodsItem";

        /// <summary>
        /// Số lượng chứng từ, phụ lục đính kèm	1	n..5
        /// </summary>
        public const string loadingList = "loadingList";

        /// <summary>
        /// Trọng lượng (kg)	1	n..11,3	Tối đa 3 chữ số thập phân
        /// </summary>
        public const string totalGrossMass = "totalGrossMass";

        /// <summary>
        /// Mã loại hình	1	an..10	Danh mục chuẩn
        /// </summary>
        public const string natureOfTransaction = "natureOfTransaction";

        public const string Payment = "Payment";

        /// <summary>
        /// Mã phương thức thanh toán	1	a..10	Danh mục chuẩn ||Phương pháp xác định trị giá
        /// </summary>
        public const string Method = "Method";

        /// <summary>
        /// Đại lý
        /// </summary>
        public const string Agent = "Agent";

        /// <summary>
        /// Tên đại lý ||Hợp đồng ||Tên người xuất khẩu ||Tên ||Mã bên giao hàng ||Mã bên nhận hàng ||Mã bên chỉ định giao hàng ||Tên cửa khẩu xuất||Tên người nhập khẩu ||Tên hãng SX
        /// </summary>
        public const string name = "name";



        /// <summary>
        /// Mã đại lý ||Mã người xuất khẩu ||Bên giao hàng ||Bên nhận hàng ||Bên chỉ định giao hàng ||Mã người nhập khẩu ||Mã hãng SX
        /// </summary>
        public const string identity = "identity";

        /// <summary>
        /// Nguyên tệ
        /// </summary>
        public const string CurrencyExchange = "CurrencyExchange";

        /// <summary>
        /// Mã nguyên tệ	1	a3	Danh mục chuẩn
        /// </summary>
        public const string currencyType = "currencyType";

        /// <summary>
        /// Tỷ giá nguyên tệ	1	n..12	Do Hải quan quy định
        /// </summary>
        public const string rate = "rate";

        /// <summary>
        /// Số kiện
        /// </summary>
        public const string DeclarationPackaging = "DeclarationPackaging";

        /// <summary>
        /// Số kiện
        /// </summary>
        public const string quantity = "quantity";

        /// <summary>
        /// Hợp đồng giao ||Hợp đồng nhận
        /// </summary>
        public const string AdditionalDocument = "AdditionalDocument";

        /// <summary>
        /// Loại chứng từ (= 315)	1	an..3	Danh mục chuẩn ||Loại thuế
        /// </summary>
        public const string type = "type";

        public const string expire = "expire";

        /// <summary>
        /// Người nhập khẩu
        /// </summary>
        public const string Exporter = "Exporter";

        /// <summary>
        /// Người đại diện doanh nghiệp	1	none	Ký, đóng dấu trên tờ khai
        /// </summary>
        public const string RepresentativePerson = "RepresentativePerson";

        /// <summary>
        /// Chức vụ	1	a..35	Bắt buộc phải có
        /// </summary>
        public const string contactFunction = "contactFunction";

        /// <summary>
        /// Thông tin hàng hóa
        /// </summary>
        public const string GoodsShipment = "GoodsShipment";

        /// <summary>
        /// 
        /// </summary>
        public const string exportationCountry = "exportationCountry";

        /// <summary>
        /// Thông tin thương nhân giao hàng
        /// </summary>
        public const string Consignor = "Consignor";

        /// <summary>
        /// Thông tin bên nhận hàng
        /// </summary>
        public const string Consignee = "Consignee";

        /// <summary>
        /// Bên chỉ định giao hàng
        /// </summary>
        public const string NotifyParty = "NotifyParty";

        public const string DeliveryDestination = "DeliveryDestination";

        /// <summary>
        /// Địa điểm giao hàng
        /// </summary>
        public const string line = "line";

        /// <summary>
        /// Thời điểm giao hàng
        /// </summary>
        public const string time = "time";

        /// <summary>
        /// Cửa khẩu nhập
        /// </summary>
        public const string EntryCustomsOffice = "EntryCustomsOffice";

        /// <summary>
        /// Mã cửa khẩu nhập ||Mã cửa khẩu xuất
        /// </summary>
        public const string code = "code";

        /// <summary>
        /// Cửa khẩu xuất
        /// </summary>
        public const string ExitCustomsOffice = "ExitCustomsOffice";

        /// <summary>
        /// Người nhập khẩu
        /// </summary>
        public const string Importer = "Importer";

        /// <summary>
        /// Điều kiện giao hàng
        /// </summary>
        public const string TradeTerm = "TradeTerm";

        /// <summary>
        /// mã điều kiện giao hàng
        /// </summary>
        public const string condition = "condition";

        /// <summary>
        /// Hàng khai báo
        /// </summary>
        public const string CustomsGoodsItem = "CustomsGoodsItem";

        /// <summary>
        /// Trị giá khai báo (nguyên tệ) Tối đa 2 chữ số thập phân
        /// </summary>
        public const string customsValue = "customsValue";

        /// <summary>
        /// Số thứ tự hàng
        /// </summary>
        public const string sequence = "sequence";

        /// <summary>
        /// Trị giá tính thuế
        /// </summary>
        public const string statisticalValue = "statisticalValue";

        /// <summary>
        /// Đơn giá nguyên tệ
        /// </summary>
        public const string unitPrice = "unitPrice";

        /// <summary>
        /// Hãng sản xuất
        /// </summary>
        public const string Manufacturer = "Manufacturer";

        /// <summary>
        /// Xuất xứ
        /// </summary>
        public const string Origin = "Origin";

        /// <summary>
        /// Mã nước xuất xứ	1	a2	Danh mục chuẩn
        /// </summary>
        public const string originCountry = "originCountry";

        /// <summary>
        /// Chi tiết hàng, thuế
        /// </summary>
        public const string Commodity = "Commodity";

        /// <summary>
        /// Tên hàng
        /// </summary>
        public const string description = "description";

        /// <summary>
        /// Mã hàng do nhà sản xuất hoặc doanh nghiệp quy định	1	an..30	Nếu không có thì để xâu rỗng
        /// </summary>
        public const string identification = "identification";

        /// <summary>
        ///  Mã HS	1	n..12	Tối thiểu 8 số
        /// </summary>
        public const string tariffClassification = "tariffClassification";

        /// <summary>
        ///Mã HS mở rộng	0	n..12	Nếu không có thì để xâu rỗng
        /// </summary>
        public const string tariffClassificationExtension = "tariffClassificationExtension";

        /// <summary>
        /// Nhãn hiệu	0	an..256	Nếu không có thì để xâu rỗng
        /// </summary>
        public const string brand = "brand";

        /// <summary>
        /// Quy cách, phẩm chất	0	an..1000	Nếu không có thì để xâu rỗng
        /// </summary>
        public const string grade = "grade";

        /// <summary>
        /// Thành phần	0	an..100	Nếu không có thì để xâu rỗng
        /// </summary>
        public const string ingredients = "ingredients";

        /// <summary>
        /// model của hàng hóa	0	an..35	Nếu không có thì để xâu rỗng
        /// </summary>
        public const string modelNumber = "modelNumber";

        /// <summary>
        /// Thuế
        /// </summary>
        public const string DutyTaxFee = "DutyTaxFee";

        /// <summary>
        /// Số thuế tự tính	1	n..16	Tối đa 2 chữ số thập phân
        /// </summary>
        public const string adValoremTaxBase = "adValoremTaxBase";

        /// <summary>
        /// Thuế suất	1	n..4,1	Tối đa 1 chữ số thập phân
        /// </summary>
        public const string tax = "tax";

        public const string InvoiceLine = "InvoiceLine";

        public const string itemCharge = "itemCharge";

        public const string GoodsMeasure = "GoodsMeasure";

        /// <summary>
        /// Số lượng	1	n..14,3	Tối đa 3 chữ số thập phân
        /// </summary>
        public const string tariff = "tariff";

        /// <summary>
        /// Mã đơn vị tính	1	an..3	Danh mục chuẩn
        /// </summary>
        public const string measureUnit = "measureUnit";

        /// <summary>
        /// Tỷ lệ quy đổi so với đơn vị tính đăng ký	
        /// </summary>
        public const string conversionRate = "conversionRate";

        /// <summary>
        /// Trị giá Hải quan
        /// </summary>
        public const string CustomsValuation = "CustomsValuation";

        /// <summary>
        /// Tổng chi phí khác (vận tải, bảo hiểm và các chi phí khác từ cảng xuất đến cảng nhập)	1	n..16,2	Tối đa 2 chữ số thập phân
        /// </summary>
        public const string exitToEntryCharge = "exitToEntryCharge";

        /// <summary>
        /// Phí vận tải	1	n..16,2	Tối đa 2 chữ số thập phân
        /// </summary>
        public const string freightCharge = "freightCharge";

        /// <summary>
        /// Tổng các khoản phải cộng - tổng các khoản được trừ	1	n..16,2	Tối đa 2 chữ số thập phân. Nếu không có thì để giá trị 0
        /// </summary>
        public const string otherChargeDeduction = "otherChargeDeduction";

    }
}
