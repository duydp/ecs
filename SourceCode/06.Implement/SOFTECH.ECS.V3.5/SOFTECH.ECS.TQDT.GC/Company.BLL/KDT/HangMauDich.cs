using System.Data.SqlClient;
using System.Data;
using System;
using Company.GC.BLL.GC;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Collections.Generic;
using Company.KDT.SHARE.QuanLyChungTu;

namespace Company.GC.BLL.KDT
{
    public partial class HangMauDich
    {
        public string Ma { get; set; }
        public string Ten { get; set; }
        public string MaHSMoi { get; set; }
        public DonViTinhQuyDoi DVT_QuyDoi { get; set; }
        public ThuTucHQTruocDo ThuTucHQTruocDo { get; set; }
        List<MienGiamThue> _MienGiamThueCollection = new List<MienGiamThue>();
        public List<MienGiamThue> MienGiamThueCollection { get { return _MienGiamThueCollection; } set { _MienGiamThueCollection = value; } }
        public void LoadMienGiamThue()
        {
            if (this.ID > 0)
                _MienGiamThueCollection = (List<MienGiamThue>)MienGiamThue.SelectCollectionBy_HMD_ID(this.ID);


        }

        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        public string NhomHang { get; set; }

        public bool Load(SqlTransaction trans)
        {
            string spName = "p_KDT_HangMauDich_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand, trans);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) this.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) this.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) this.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) this.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) this.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) this.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) this.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) this.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) this.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) this.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) this.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) this.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) this.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) this.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) this.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) this.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) this.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) this.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) this.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) this.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) this.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) this.ThueSuatXNKGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) this.ThueSuatTTDBGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) this.ThueSuatVATGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatVATGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) this.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
                if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) this.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) this.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) this.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) this.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("Model"))) this.Model = reader.GetString(reader.GetOrdinal("Model"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) this.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) this.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) this.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) this.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueBVMT"))) this.ThueBVMT = reader.GetDecimal(reader.GetOrdinal("ThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMT"))) this.ThueSuatBVMT = reader.GetDecimal(reader.GetOrdinal("ThueSuatBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMTGiam"))) this.ThueSuatBVMTGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatBVMTGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueChongPhaGia"))) this.ThueChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGia"))) this.ThueSuatChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueSuatChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"))) this.ThueSuatChongPhaGiaGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("isHangCu"))) this.isHangCu = reader.GetBoolean(reader.GetOrdinal("isHangCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueXNK"))) this.BieuThueXNK = reader.GetString(reader.GetOrdinal("BieuThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueTTDB"))) this.BieuThueTTDB = reader.GetString(reader.GetOrdinal("BieuThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueBVMT"))) this.BieuThueBVMT = reader.GetString(reader.GetOrdinal("BieuThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueGTGT"))) this.BieuThueGTGT = reader.GetString(reader.GetOrdinal("BieuThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueCBPG"))) this.BieuThueCBPG = reader.GetString(reader.GetOrdinal("BieuThueCBPG"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) this.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public HangMauDich Copy()
        {
            HangMauDich entity = new HangMauDich();

            entity.ID = this.ID;
            entity.TKMD_ID = this.TKMD_ID;
            entity.SoThuTuHang = this.SoThuTuHang;
            entity.MaHS = this.MaHS;
            entity.MaPhu = this.MaPhu;
            entity.TenHang = this.TenHang;
            entity.NuocXX_ID = this.NuocXX_ID;
            entity.DVT_ID = this.DVT_ID;
            entity.SoLuong = this.SoLuong;
            entity.TrongLuong = this.TrongLuong;
            entity.DonGiaKB = this.DonGiaKB;
            entity.DonGiaTT = this.DonGiaTT;
            entity.TriGiaKB = this.TriGiaKB;
            entity.TriGiaTT = this.TriGiaTT;
            entity.TriGiaKB_VND = this.TriGiaKB_VND;
            entity.ThueSuatXNK = this.ThueSuatXNK;
            entity.ThueSuatTTDB = this.ThueSuatTTDB;
            entity.ThueSuatGTGT = this.ThueSuatGTGT;
            entity.ThueXNK = this.ThueXNK;
            entity.ThueTTDB = this.ThueTTDB;
            entity.ThueGTGT = this.ThueGTGT;
            entity.PhuThu = this.PhuThu;
            entity.TyLeThuKhac = this.TyLeThuKhac;
            entity.TriGiaThuKhac = this.TriGiaThuKhac;
            entity.MienThue = this.MienThue;
            entity.Ma_HTS = this.Ma_HTS;
            entity.DVT_HTS = this.DVT_HTS;
            entity.SoLuong_HTS = this.SoLuong_HTS;

            #region V3.5
            entity.ThueBVMT = this.ThueBVMT;
            entity.ThueSuatBVMT = this.ThueSuatBVMT;
            entity.ThueSuatBVMTGiam = this.ThueSuatBVMTGiam;
            entity.isHangCu = this.isHangCu;
            entity.ThueChongPhaGia = this.ThueChongPhaGia;
            entity.ThueSuatChongPhaGia = this.ThueSuatChongPhaGia;
            entity.ThueSuatChongPhaGiaGiam = this.ThueSuatChongPhaGiaGiam;
            entity.BieuThueXNK = this.BieuThueXNK;
            entity.BieuThueTTDB = this.BieuThueTTDB;
            entity.BieuThueGTGT = this.BieuThueGTGT;
            entity.BieuThueBVMT = this.BieuThueBVMT;
            entity.BieuThueCBPG = this.BieuThueCBPG;
            entity.ThongTinKhac = this.ThongTinKhac;
            #endregion
            return entity;
        }

        public List<HangMauDich> SelectCollectionBy_TKMD_ID(SqlTransaction trans)
        {
            string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);

            List<HangMauDich> collection = new List<HangMauDich>();
            IDataReader reader = this.db.ExecuteReader(dbCommand, trans);
            while (reader.Read())
            {
                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatVATGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) entity.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
                if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) entity.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) entity.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) entity.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("Model"))) entity.Model = reader.GetString(reader.GetOrdinal("Model"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) entity.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) entity.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueBVMT"))) entity.ThueBVMT = reader.GetDecimal(reader.GetOrdinal("ThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMT"))) entity.ThueSuatBVMT = reader.GetDecimal(reader.GetOrdinal("ThueSuatBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMTGiam"))) entity.ThueSuatBVMTGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatBVMTGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueChongPhaGia"))) entity.ThueChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGia"))) entity.ThueSuatChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueSuatChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"))) entity.ThueSuatChongPhaGiaGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("isHangCu"))) entity.isHangCu = reader.GetBoolean(reader.GetOrdinal("isHangCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueXNK"))) entity.BieuThueXNK = reader.GetString(reader.GetOrdinal("BieuThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueTTDB"))) entity.BieuThueTTDB = reader.GetString(reader.GetOrdinal("BieuThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueBVMT"))) entity.BieuThueBVMT = reader.GetString(reader.GetOrdinal("BieuThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueGTGT"))) entity.BieuThueGTGT = reader.GetString(reader.GetOrdinal("BieuThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueCBPG"))) entity.BieuThueCBPG = reader.GetString(reader.GetOrdinal("BieuThueCBPG"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));

                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public void Delete(string LoaiHangHoa, string MaLoaiHinh, long IDHopDong)
        {
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                try
                {
                    this.Delete(LoaiHangHoa, MaLoaiHinh.Trim(), IDHopDong, transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public void Delete(string LoaiHangHoa, string MaLoaiHinh, long IDHopDong, SqlTransaction transaction)
        {
            if (MaLoaiHinh.StartsWith("NGC"))
            {
                if (LoaiHangHoa == "N")
                {
                    #region Nhap NPL
                    NguyenPhuLieu npl = new NguyenPhuLieu();
                    npl.HopDong_ID = IDHopDong;
                    npl.Ma = this.MaPhu;
                    npl.Load(transaction);
                    npl.SoLuongDaNhap -= this.SoLuong;
                    npl.UpdateTransaction(transaction);
                    #endregion Nhap NPL
                }
                else
                {
                    #region Nhap Thiet BI
                    ThietBi tb = new ThietBi();
                    tb.HopDong_ID = IDHopDong;
                    tb.Ma = this.MaPhu;
                    tb.Load(transaction);
                    tb.SoLuongDaNhap -= this.SoLuong;
                    tb.UpdateTransaction(transaction);
                    #endregion Nhap Thiet BI
                }
            }
            else
            {
                if (LoaiHangHoa == "N")
                {
                    #region Xuat NPL
                    NguyenPhuLieu npl = new NguyenPhuLieu();
                    npl.HopDong_ID = IDHopDong;
                    npl.Ma = this.MaPhu;
                    npl.Load(transaction);
                    npl.SoLuongDaDung -= this.SoLuong;
                    npl.UpdateTransaction(transaction);
                    #endregion Xuat NPL
                }
                else if (LoaiHangHoa == "T")
                {
                    #region Xuat TB
                    ThietBi tb = new ThietBi();
                    tb.HopDong_ID = IDHopDong;
                    tb.Ma = this.MaPhu;
                    tb.Load(transaction);
                    tb.SoLuongDaNhap += this.SoLuong;
                    tb.UpdateTransaction(transaction);
                    #endregion Xuat Thiet bi
                }
                else
                {
                    SanPham sp = new SanPham();
                    sp.HopDong_ID = IDHopDong;
                    sp.Ma = this.MaPhu;
                    sp.Load(transaction);
                    sp.SoLuongDaXuat -= this.SoLuong;
                    sp.UpdateTransaction(transaction);
                    DataSet dsLuongNPL = NguyenPhuLieu.GetLuongNguyenPhuLieuTheoDM(sp.Ma, this.SoLuong, IDHopDong, transaction);
                    foreach (DataRow row in dsLuongNPL.Tables[0].Rows)
                    {
                        NguyenPhuLieu npl = new NguyenPhuLieu();
                        npl.HopDong_ID = IDHopDong;
                        npl.Ma = row["MaNguyenPhuLieu"].ToString();
                        npl.Load(transaction);
                        npl.SoLuongDaDung -= Convert.ToDecimal(row["LuongCanDung"]);
                        npl.UpdateTransaction(transaction);
                    }
                }

            }
            this.Delete(transaction);
        }
        public bool Load()
        {
            return HangMauDich.Load(this.ID) != null;
        }
        // Get so hang TK :
        public DataSet GetHangTK(long IDHopDong)
        {
            //string sql = " SELECT * " +
            //          " from   t_KDT_HangMauDich " +
            //          " where  TKMD_ID = @TKMD_ID ";

            string sql = " SELECT T.SoToKhai, T.MaLoaiHinh,T.ID, T.IDHopDong, H.TenHang, H.MaPhu, H.SoLuong,H.DVT_ID "
          + " FROM t_KDT_HangMauDich H  "
          + " inner join t_KDT_ToKhaiMauDich T "
          + " on  H.TKMD_ID = T.ID and T.IDHopDong = @IDHopDong";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        // Get Quantity :
        public DataSet GetSoluongHangTK(long SoTK, string maNPL, string maLoaiHinh, long IDHopDong)
        {
            string sql = " SELECT distinct T.SoToKhai, T.MaLoaiHinh,T.ID, T.IDHopDong, H.TenHang, H.MaPhu, H.SoLuong "
            + " FROM t_KDT_HangMauDich H  "
            + " inner join t_KDT_ToKhaiMauDich T "
            + " on  H.TKMD_ID = T.ID Where T.SoToKhai = @SoToKhai "
            + " And H.MaPhu =@MaPhu "
            + " And T.MaLoaiHinh=@MaLoaiHinh "
            + " And T.IDHopDong=@IDHopDong "
            + " and T.LoaiHangHoa='N'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoTK);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maNPL);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public decimal GetSoluongHangTKMD(long ID, string maPhu)
        {
            string sql = " SELECT SoLuong "
                         + " from   t_KDT_HangMauDich  "
                         + " where  TKMD_ID =@ID AND MaPhu =@MaPhu ";
            decimal temp = 0;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.Char, maPhu);
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            { }
            return temp;

        }
        public bool CheckMaHangToKhai()
        {
            string sql = " SELECT count(MaPhu) "
                         + " FROM   t_KDT_HangMauDich  "
                         + " WHERE  TKMD_ID =@ID AND MaPhu =@MaPhu ";
            decimal temp = 0;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this.TKMD_ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.Char, this.MaPhu);
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            { }
            return temp > 0;

        }
        public DataSet GetSoluongHangTKS(long SoTK, string maNPL, string maLoaiHinh, long IDHopDong)
        {
            string sql = " SELECT distinct T.SoToKhai, T.MaLoaiHinh,T.ID, T.IDHopDong, H.TenHang, H.MaPhu, H.SoLuong "
            + " FROM t_KDT_HangMauDich H  "
            + " inner join t_KDT_ToKhaiMauDich T "
            + " on  H.TKMD_ID = T.ID "
            + " Where T.SoToKhai = @SoToKhai "
            + " And H.MaPhu =@MaPhu "
            + " And T.MaLoaiHinh=@MaLoaiHinh "
            + " And T.IDHopDong=@IDHopDong "
            + " And T.LoaiHangHoa='S'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoTK);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maNPL);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public DataSet GetSoluongHangTKT(long SoTK, string maNPL, string maLoaiHinh, long IDHopDong)
        {
            string sql = " SELECT distinct T.SoToKhai, T.MaLoaiHinh,T.ID, T.IDHopDong, H.TenHang, H.MaPhu, H.SoLuong "
            + " FROM t_KDT_HangMauDich H  "
            + " inner join t_KDT_ToKhaiMauDich T "
            + " on  H.TKMD_ID = T.ID Where T.SoToKhai = @SoToKhai "
            + " And H.MaPhu =@MaPhu "
            + " And T.MaLoaiHinh=@MaLoaiHinh "
            + " And T.IDHopDong=@IDHopDong "
            + " and T.LoaiHangHoa='T'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoTK);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maNPL);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public decimal GetDonGiaHangTKMD(long ID, string maPhu)
        {
            string sql = " SELECT DonGiaKB "
                         + " from   t_KDT_HangMauDich  "
                         + " where  TKMD_ID =@ID AND MaPhu =@MaPhu ";
            decimal temp = 0;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.Char, maPhu);
            try
            {
                temp = Convert.ToDecimal(db.ExecuteScalar(dbCommand));
            }
            catch
            { }
            return temp;

        }
        public void SetDabaseMoi(string nameDatabase)
        {
            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameDatabase);
        }
        public long InsertTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_HangMauDich_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, TriGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
            db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.Decimal, ThueSuatXNKGiam);
            db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.Decimal, ThueSuatTTDBGiam);
            db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.Decimal, ThueSuatVATGiam);
            db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
            db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
            db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
            db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
            db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
            db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
            db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
            db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
            db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
            db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);
            db.AddInParameter(dbCommand, "@ThueBVMT", SqlDbType.Money, ThueBVMT);
            db.AddInParameter(dbCommand, "@ThueSuatBVMT", SqlDbType.Decimal, ThueSuatBVMT);
            db.AddInParameter(dbCommand, "@ThueSuatBVMTGiam", SqlDbType.Decimal, ThueSuatBVMTGiam);
            db.AddInParameter(dbCommand, "@ThueChongPhaGia", SqlDbType.Money, ThueChongPhaGia);
            db.AddInParameter(dbCommand, "@ThueSuatChongPhaGia", SqlDbType.Decimal, ThueSuatChongPhaGia);
            db.AddInParameter(dbCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Decimal, ThueSuatChongPhaGiaGiam);
            db.AddInParameter(dbCommand, "@isHangCu", SqlDbType.Bit, isHangCu);
            db.AddInParameter(dbCommand, "@BieuThueXNK", SqlDbType.NVarChar, BieuThueXNK);
            db.AddInParameter(dbCommand, "@BieuThueTTDB", SqlDbType.NVarChar, BieuThueTTDB);
            db.AddInParameter(dbCommand, "@BieuThueBVMT", SqlDbType.NVarChar, BieuThueBVMT);
            db.AddInParameter(dbCommand, "@BieuThueGTGT", SqlDbType.NVarChar, BieuThueGTGT);
            db.AddInParameter(dbCommand, "@BieuThueCBPG", SqlDbType.NVarChar, BieuThueCBPG);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_HangMauDich_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, TriGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
            db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.Decimal, ThueSuatXNKGiam);
            db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.Decimal, ThueSuatTTDBGiam);
            db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.Decimal, ThueSuatVATGiam);
            db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
            db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
            db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
            db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
            db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
            db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
            db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
            db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
            db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
            db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);
            db.AddInParameter(dbCommand, "@ThueBVMT", SqlDbType.Money, ThueBVMT);
            db.AddInParameter(dbCommand, "@ThueSuatBVMT", SqlDbType.Decimal, ThueSuatBVMT);
            db.AddInParameter(dbCommand, "@ThueSuatBVMTGiam", SqlDbType.Decimal, ThueSuatBVMTGiam);
            db.AddInParameter(dbCommand, "@ThueChongPhaGia", SqlDbType.Money, ThueChongPhaGia);
            db.AddInParameter(dbCommand, "@ThueSuatChongPhaGia", SqlDbType.Decimal, ThueSuatChongPhaGia);
            db.AddInParameter(dbCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Decimal, ThueSuatChongPhaGiaGiam);
            db.AddInParameter(dbCommand, "@isHangCu", SqlDbType.Bit, isHangCu);
            db.AddInParameter(dbCommand, "@BieuThueXNK", SqlDbType.NVarChar, BieuThueXNK);
            db.AddInParameter(dbCommand, "@BieuThueTTDB", SqlDbType.NVarChar, BieuThueTTDB);
            db.AddInParameter(dbCommand, "@BieuThueBVMT", SqlDbType.NVarChar, BieuThueBVMT);
            db.AddInParameter(dbCommand, "@BieuThueGTGT", SqlDbType.NVarChar, BieuThueGTGT);
            db.AddInParameter(dbCommand, "@BieuThueCBPG", SqlDbType.NVarChar, BieuThueCBPG);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_HangMauDich_InsertUpdateByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, DonGiaKB);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, TriGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, MienThue);
            db.AddInParameter(dbCommand, "@Ma_HTS", SqlDbType.VarChar, Ma_HTS);
            db.AddInParameter(dbCommand, "@DVT_HTS", SqlDbType.Char, DVT_HTS);
            db.AddInParameter(dbCommand, "@SoLuong_HTS", SqlDbType.Decimal, SoLuong_HTS);
            db.AddInParameter(dbCommand, "@ThueSuatXNKGiam", SqlDbType.Decimal, ThueSuatXNKGiam);
            db.AddInParameter(dbCommand, "@ThueSuatTTDBGiam", SqlDbType.Decimal, ThueSuatTTDBGiam);
            db.AddInParameter(dbCommand, "@ThueSuatVATGiam", SqlDbType.Decimal, ThueSuatVATGiam);
            db.AddInParameter(dbCommand, "@MaHSMoRong", SqlDbType.NVarChar, MaHSMoRong);
            db.AddInParameter(dbCommand, "@FOC", SqlDbType.Bit, FOC);
            db.AddInParameter(dbCommand, "@NhanHieu", SqlDbType.NVarChar, NhanHieu);
            db.AddInParameter(dbCommand, "@QuyCachPhamChat", SqlDbType.NVarChar, QuyCachPhamChat);
            db.AddInParameter(dbCommand, "@ThanhPhan", SqlDbType.NVarChar, ThanhPhan);
            db.AddInParameter(dbCommand, "@Model", SqlDbType.NVarChar, Model);
            db.AddInParameter(dbCommand, "@TenHangSX", SqlDbType.NVarChar, TenHangSX);
            db.AddInParameter(dbCommand, "@MaHangSX", SqlDbType.NVarChar, MaHangSX);
            db.AddInParameter(dbCommand, "@ThueTuyetDoi", SqlDbType.Bit, ThueTuyetDoi);
            db.AddInParameter(dbCommand, "@DonGiaTuyetDoi", SqlDbType.Float, DonGiaTuyetDoi);
            db.AddInParameter(dbCommand, "@ThueBVMT", SqlDbType.Money, ThueBVMT);
            db.AddInParameter(dbCommand, "@ThueSuatBVMT", SqlDbType.Decimal, ThueSuatBVMT);
            db.AddInParameter(dbCommand, "@ThueSuatBVMTGiam", SqlDbType.Decimal, ThueSuatBVMTGiam);
            db.AddInParameter(dbCommand, "@ThueChongPhaGia", SqlDbType.Money, ThueChongPhaGia);
            db.AddInParameter(dbCommand, "@ThueSuatChongPhaGia", SqlDbType.Decimal, ThueSuatChongPhaGia);
            db.AddInParameter(dbCommand, "@ThueSuatChongPhaGiaGiam", SqlDbType.Decimal, ThueSuatChongPhaGiaGiam);
            db.AddInParameter(dbCommand, "@isHangCu", SqlDbType.Bit, isHangCu);
            db.AddInParameter(dbCommand, "@BieuThueXNK", SqlDbType.NVarChar, BieuThueXNK);
            db.AddInParameter(dbCommand, "@BieuThueTTDB", SqlDbType.NVarChar, BieuThueTTDB);
            db.AddInParameter(dbCommand, "@BieuThueBVMT", SqlDbType.NVarChar, BieuThueBVMT);
            db.AddInParameter(dbCommand, "@BieuThueGTGT", SqlDbType.NVarChar, BieuThueGTGT);
            db.AddInParameter(dbCommand, "@BieuThueCBPG", SqlDbType.NVarChar, BieuThueCBPG);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public List<HangMauDich> SelectCollectionBy_TKMD_ID(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);

            List<HangMauDich> collection = new List<HangMauDich>();
            SqlDataReader reader = null;
            if (transaction != null)
                reader = (SqlDataReader)this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = (SqlDataReader)this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {

                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatVATGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) entity.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
                if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) entity.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) entity.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) entity.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("Model"))) entity.Model = reader.GetString(reader.GetOrdinal("Model"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) entity.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) entity.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueBVMT"))) entity.ThueBVMT = reader.GetDecimal(reader.GetOrdinal("ThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMT"))) entity.ThueSuatBVMT = reader.GetDecimal(reader.GetOrdinal("ThueSuatBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMTGiam"))) entity.ThueSuatBVMTGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatBVMTGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueChongPhaGia"))) entity.ThueChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGia"))) entity.ThueSuatChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueSuatChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"))) entity.ThueSuatChongPhaGiaGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("isHangCu"))) entity.isHangCu = reader.GetBoolean(reader.GetOrdinal("isHangCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueXNK"))) entity.BieuThueXNK = reader.GetString(reader.GetOrdinal("BieuThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueTTDB"))) entity.BieuThueTTDB = reader.GetString(reader.GetOrdinal("BieuThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueBVMT"))) entity.BieuThueBVMT = reader.GetString(reader.GetOrdinal("BieuThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueGTGT"))) entity.BieuThueGTGT = reader.GetString(reader.GetOrdinal("BieuThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueCBPG"))) entity.BieuThueCBPG = reader.GetString(reader.GetOrdinal("BieuThueCBPG"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                }
                catch { }
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public List<HangMauDich> SelectCollectionBy_TKMD_ID(string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_HangMauDich_SelectBy_TKMD_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);

            List<HangMauDich> collection = new List<HangMauDich>();
            SqlDataReader reader = (SqlDataReader)this.db.ExecuteReader(dbCommand);
            
            while (reader.Read())
            {

                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma_HTS"))) entity.Ma_HTS = reader.GetString(reader.GetOrdinal("Ma_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_HTS"))) entity.DVT_HTS = reader.GetString(reader.GetOrdinal("DVT_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong_HTS"))) entity.SoLuong_HTS = reader.GetDecimal(reader.GetOrdinal("SoLuong_HTS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNKGiam"))) entity.ThueSuatXNKGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNKGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDBGiam"))) entity.ThueSuatTTDBGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDBGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatVATGiam"))) entity.ThueSuatVATGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatVATGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHSMoRong"))) entity.MaHSMoRong = reader.GetString(reader.GetOrdinal("MaHSMoRong"));
                if (!reader.IsDBNull(reader.GetOrdinal("FOC"))) entity.FOC = reader.GetBoolean(reader.GetOrdinal("FOC"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhanHieu"))) entity.NhanHieu = reader.GetString(reader.GetOrdinal("NhanHieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuyCachPhamChat"))) entity.QuyCachPhamChat = reader.GetString(reader.GetOrdinal("QuyCachPhamChat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhPhan"))) entity.ThanhPhan = reader.GetString(reader.GetOrdinal("ThanhPhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("Model"))) entity.Model = reader.GetString(reader.GetOrdinal("Model"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangSX"))) entity.TenHangSX = reader.GetString(reader.GetOrdinal("TenHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangSX"))) entity.MaHangSX = reader.GetString(reader.GetOrdinal("MaHangSX"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTuyetDoi"))) entity.ThueTuyetDoi = reader.GetBoolean(reader.GetOrdinal("ThueTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTuyetDoi"))) entity.DonGiaTuyetDoi = reader.GetDouble(reader.GetOrdinal("DonGiaTuyetDoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueBVMT"))) entity.ThueBVMT = reader.GetDecimal(reader.GetOrdinal("ThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMT"))) entity.ThueSuatBVMT = reader.GetDecimal(reader.GetOrdinal("ThueSuatBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatBVMTGiam"))) entity.ThueSuatBVMTGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatBVMTGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueChongPhaGia"))) entity.ThueChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGia"))) entity.ThueSuatChongPhaGia = reader.GetDecimal(reader.GetOrdinal("ThueSuatChongPhaGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"))) entity.ThueSuatChongPhaGiaGiam = reader.GetDecimal(reader.GetOrdinal("ThueSuatChongPhaGiaGiam"));
                if (!reader.IsDBNull(reader.GetOrdinal("isHangCu"))) entity.isHangCu = reader.GetBoolean(reader.GetOrdinal("isHangCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueXNK"))) entity.BieuThueXNK = reader.GetString(reader.GetOrdinal("BieuThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueTTDB"))) entity.BieuThueTTDB = reader.GetString(reader.GetOrdinal("BieuThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueBVMT"))) entity.BieuThueBVMT = reader.GetString(reader.GetOrdinal("BieuThueBVMT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueGTGT"))) entity.BieuThueGTGT = reader.GetString(reader.GetOrdinal("BieuThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("BieuThueCBPG"))) entity.BieuThueCBPG = reader.GetString(reader.GetOrdinal("BieuThueCBPG"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                try
                {
                    if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                    if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                }
                catch { }
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<HangMauDich> SelectHang(string MaHQ, string maDN, string maLoaiHinh)
        {
            List<HangMauDich> collection = new List<HangMauDich>();

            try
            {
                const string spName = "p_GC_SelectHang";

                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

                db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHQ);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDN);
                db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, maLoaiHinh);

                SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
                while (reader.Read())
                {
                    HangMauDich entity = new HangMauDich();
                    if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                    if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                    if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                    if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                    entity.MaHSMoi = string.Empty;
                    collection.Add(entity);
                }
                reader.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return collection;
        }

    }
}