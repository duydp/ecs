﻿namespace Company.Interface
{
    partial class ThongTinDNAndHQForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongTinDNAndHQForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPageHQ = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.btnOpenWS = new System.Windows.Forms.Button();
            this.lblWS = new System.Windows.Forms.Label();
            this.txtTenDichVu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPort = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtHost = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtDiaChiHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtMaCuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMailHaiQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenCucHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenNganHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label7 = new System.Windows.Forms.Label();
            this.uiTabPageDN = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNguoiLienHeDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtSoFaxDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDienThoaiDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.opTiengAnh = new Janus.Windows.EditControls.UIRadioButton();
            this.opVietNam = new Janus.Windows.EditControls.UIRadioButton();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMaMid = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtMailDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMaDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.rfvDiaChi = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenNganHQ = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.containerValidator1 = new Company.Controls.CustomValidation.ContainerValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvTenCuc = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNguoiLienHe = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvEmailDN = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaCuc = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDiaChiHQ = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.cbFont = new Janus.Windows.EditControls.UIComboBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPageHQ.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.uiTabPageDN.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNganHQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenCuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiLienHe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvEmailDN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaCuc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChiHQ)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(395, 379);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.uiTab1);
            this.uiGroupBox1.Controls.Add(this.btnSave);
            this.uiGroupBox1.Controls.Add(this.uiButton2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(395, 379);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // uiTab1
            // 
            this.uiTab1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiTab1.BackColor = System.Drawing.SystemColors.Control;
            this.uiTab1.Location = new System.Drawing.Point(0, 3);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(395, 341);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPageHQ,
            this.uiTabPageDN});
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPageHQ
            // 
            this.uiTabPageHQ.Controls.Add(this.uiGroupBox5);
            this.uiTabPageHQ.Controls.Add(this.uiGroupBox3);
            this.uiTabPageHQ.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageHQ.Name = "uiTabPageHQ";
            this.uiTabPageHQ.Size = new System.Drawing.Size(393, 319);
            this.uiTabPageHQ.TabStop = true;
            this.uiTabPageHQ.Text = "Thông tin Hải quan";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.label15);
            this.uiGroupBox5.Controls.Add(this.label16);
            this.uiGroupBox5.Controls.Add(this.btnOpenWS);
            this.uiGroupBox5.Controls.Add(this.lblWS);
            this.uiGroupBox5.Controls.Add(this.txtTenDichVu);
            this.uiGroupBox5.Controls.Add(this.label17);
            this.uiGroupBox5.Controls.Add(this.label18);
            this.uiGroupBox5.Controls.Add(this.txtPort);
            this.uiGroupBox5.Controls.Add(this.txtHost);
            this.uiGroupBox5.Controls.Add(this.label19);
            this.uiGroupBox5.Controls.Add(this.txtDiaChiHQ);
            this.uiGroupBox5.Controls.Add(this.label20);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(4, 167);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(386, 149);
            this.uiGroupBox5.TabIndex = 2;
            this.uiGroupBox5.Text = "Thông số khai báo Hải quan";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.ForeColor = System.Drawing.Color.Red;
            this.label15.Location = new System.Drawing.Point(371, 52);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(14, 13);
            this.label15.TabIndex = 36;
            this.label15.Text = "*";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(371, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 13);
            this.label16.TabIndex = 37;
            this.label16.Text = "*";
            // 
            // btnOpenWS
            // 
            this.btnOpenWS.AutoSize = true;
            this.btnOpenWS.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btnOpenWS.Image = ((System.Drawing.Image)(resources.GetObject("btnOpenWS.Image")));
            this.btnOpenWS.Location = new System.Drawing.Point(365, 120);
            this.btnOpenWS.Name = "btnOpenWS";
            this.btnOpenWS.Size = new System.Drawing.Size(22, 22);
            this.btnOpenWS.TabIndex = 4;
            this.btnOpenWS.UseVisualStyleBackColor = true;
            this.btnOpenWS.Click += new System.EventHandler(this.btnOpenWS_Click);
            // 
            // lblWS
            // 
            this.lblWS.BackColor = System.Drawing.Color.Transparent;
            this.lblWS.ForeColor = System.Drawing.Color.Red;
            this.lblWS.Location = new System.Drawing.Point(19, 122);
            this.lblWS.Name = "lblWS";
            this.lblWS.Size = new System.Drawing.Size(346, 20);
            this.lblWS.TabIndex = 35;
            this.lblWS.Text = "http://www.dngcustoms.gov.vn/kdtservice/kdtservice.asmx";
            this.lblWS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtTenDichVu
            // 
            this.txtTenDichVu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDichVu.Location = new System.Drawing.Point(141, 47);
            this.txtTenDichVu.MaxLength = 200;
            this.txtTenDichVu.Name = "txtTenDichVu";
            this.txtTenDichVu.Size = new System.Drawing.Size(224, 21);
            this.txtTenDichVu.TabIndex = 1;
            this.txtTenDichVu.Text = "kdtservice/kdtservice.asmx";
            this.txtTenDichVu.VisualStyleManager = this.vsmMain;
            this.txtTenDichVu.TextChanged += new System.EventHandler(this.txtTenDichVu_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(16, 52);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(105, 13);
            this.label17.TabIndex = 34;
            this.label17.Text = "Tên dịch vụ khai báo";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(16, 103);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 13);
            this.label18.TabIndex = 33;
            this.label18.Text = "Port proxy";
            // 
            // txtPort
            // 
            this.txtPort.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPort.Location = new System.Drawing.Point(141, 98);
            this.txtPort.Name = "txtPort";
            this.txtPort.Size = new System.Drawing.Size(224, 21);
            this.txtPort.TabIndex = 3;
            this.txtPort.VisualStyleManager = this.vsmMain;
            // 
            // txtHost
            // 
            this.txtHost.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHost.Location = new System.Drawing.Point(141, 74);
            this.txtHost.Name = "txtHost";
            this.txtHost.Size = new System.Drawing.Size(224, 21);
            this.txtHost.TabIndex = 2;
            this.txtHost.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(16, 79);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(60, 13);
            this.label19.TabIndex = 32;
            this.label19.Text = "Host proxy";
            // 
            // txtDiaChiHQ
            // 
            this.txtDiaChiHQ.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiHQ.Location = new System.Drawing.Point(141, 20);
            this.txtDiaChiHQ.Name = "txtDiaChiHQ";
            this.txtDiaChiHQ.Size = new System.Drawing.Size(224, 21);
            this.txtDiaChiHQ.TabIndex = 0;
            this.txtDiaChiHQ.Text = "http://www.dngcustoms.gov.vn";
            this.txtDiaChiHQ.VisualStyleManager = this.vsmMain;
            this.txtDiaChiHQ.TextChanged += new System.EventHandler(this.txtDiaChiHQ_TextChanged);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(16, 28);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(83, 13);
            this.label20.TabIndex = 26;
            this.label20.Text = "Địa chỉ hải quan";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label10);
            this.uiGroupBox3.Controls.Add(this.txtMaCuc);
            this.uiGroupBox3.Controls.Add(this.txtMailHaiQuan);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.txtTenCucHQ);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtTenNganHQ);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiGroupBox3.Location = new System.Drawing.Point(4, 3);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(386, 158);
            this.uiGroupBox3.TabIndex = 1;
            this.uiGroupBox3.Text = "Chi cục Hải quan khai báo";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(16, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 13;
            this.label10.Text = "Mã cục hải quan:";
            // 
            // txtMaCuc
            // 
            this.txtMaCuc.Location = new System.Drawing.Point(141, 18);
            this.txtMaCuc.Name = "txtMaCuc";
            this.txtMaCuc.Size = new System.Drawing.Size(227, 21);
            this.txtMaCuc.TabIndex = 0;
            this.txtMaCuc.VisualStyleManager = this.vsmMain;
            // 
            // txtMailHaiQuan
            // 
            this.txtMailHaiQuan.Location = new System.Drawing.Point(141, 128);
            this.txtMailHaiQuan.Name = "txtMailHaiQuan";
            this.txtMailHaiQuan.Size = new System.Drawing.Size(227, 21);
            this.txtMailHaiQuan.TabIndex = 4;
            this.txtMailHaiQuan.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(16, 133);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Email Hải quan:";
            // 
            // txtTenCucHQ
            // 
            this.txtTenCucHQ.Location = new System.Drawing.Point(141, 73);
            this.txtTenCucHQ.Name = "txtTenCucHQ";
            this.txtTenCucHQ.Size = new System.Drawing.Size(227, 21);
            this.txtTenCucHQ.TabIndex = 2;
            this.txtTenCucHQ.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(16, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Tên cục hải quan:";
            // 
            // txtTenNganHQ
            // 
            this.txtTenNganHQ.Location = new System.Drawing.Point(141, 100);
            this.txtTenNganHQ.Name = "txtTenNganHQ";
            this.txtTenNganHQ.Size = new System.Drawing.Size(227, 21);
            this.txtTenNganHQ.TabIndex = 3;
            this.txtTenNganHQ.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(16, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Tên ngắn hải quan:";
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(141, 45);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(227, 22);
            this.donViHaiQuanControl1.TabIndex = 1;
            this.donViHaiQuanControl1.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(16, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Chọn hải quan khai báo:";
            // 
            // uiTabPageDN
            // 
            this.uiTabPageDN.Controls.Add(this.uiGroupBox4);
            this.uiTabPageDN.Location = new System.Drawing.Point(1, 21);
            this.uiTabPageDN.Name = "uiTabPageDN";
            this.uiTabPageDN.Size = new System.Drawing.Size(393, 319);
            this.uiTabPageDN.TabStop = true;
            this.uiTabPageDN.Text = "Thông tin Doanh nghiệp";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.cbFont);
            this.uiGroupBox4.Controls.Add(this.label28);
            this.uiGroupBox4.Controls.Add(this.label27);
            this.uiGroupBox4.Controls.Add(this.txtNguoiLienHeDN);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.txtSoFaxDN);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.txtDienThoaiDN);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label11);
            this.uiGroupBox4.Controls.Add(this.opTiengAnh);
            this.uiGroupBox4.Controls.Add(this.opVietNam);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtMaMid);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtMailDoanhNghiep);
            this.uiGroupBox4.Controls.Add(this.txtDiaChi);
            this.uiGroupBox4.Controls.Add(this.txtTenDN);
            this.uiGroupBox4.Controls.Add(this.label4);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.txtMaDN);
            this.uiGroupBox4.Controls.Add(this.label2);
            this.uiGroupBox4.FormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiGroupBox4.Location = new System.Drawing.Point(4, 0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(386, 316);
            this.uiGroupBox4.TabIndex = 0;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtNguoiLienHeDN
            // 
            this.txtNguoiLienHeDN.Location = new System.Drawing.Point(141, 129);
            this.txtNguoiLienHeDN.Name = "txtNguoiLienHeDN";
            this.txtNguoiLienHeDN.Size = new System.Drawing.Size(227, 21);
            this.txtNguoiLienHeDN.TabIndex = 5;
            this.txtNguoiLienHeDN.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(32, 136);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(73, 13);
            this.label14.TabIndex = 15;
            this.label14.Text = "Người liên hệ:";
            // 
            // txtSoFaxDN
            // 
            this.txtSoFaxDN.Location = new System.Drawing.Point(265, 99);
            this.txtSoFaxDN.Name = "txtSoFaxDN";
            this.txtSoFaxDN.Size = new System.Drawing.Size(103, 21);
            this.txtSoFaxDN.TabIndex = 4;
            this.txtSoFaxDN.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(237, 95);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(29, 13);
            this.label13.TabIndex = 14;
            this.label13.Text = "Fax:";
            // 
            // txtDienThoaiDN
            // 
            this.txtDienThoaiDN.Location = new System.Drawing.Point(141, 99);
            this.txtDienThoaiDN.Name = "txtDienThoaiDN";
            this.txtDienThoaiDN.Size = new System.Drawing.Size(90, 21);
            this.txtDienThoaiDN.TabIndex = 3;
            this.txtDienThoaiDN.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(32, 106);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(60, 13);
            this.label12.TabIndex = 11;
            this.label12.Text = "Điện thoại:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Location = new System.Drawing.Point(32, 270);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 13);
            this.label11.TabIndex = 13;
            this.label11.Text = "Ngôn ngữ hiển thị:";
            // 
            // opTiengAnh
            // 
            this.opTiengAnh.Location = new System.Drawing.Point(251, 265);
            this.opTiengAnh.Name = "opTiengAnh";
            this.opTiengAnh.Size = new System.Drawing.Size(104, 23);
            this.opTiengAnh.TabIndex = 9;
            this.opTiengAnh.Text = "Tiếng Anh";
            // 
            // opVietNam
            // 
            this.opVietNam.Checked = true;
            this.opVietNam.Location = new System.Drawing.Point(141, 265);
            this.opVietNam.Name = "opVietNam";
            this.opVietNam.Size = new System.Drawing.Size(104, 23);
            this.opVietNam.TabIndex = 8;
            this.opVietNam.TabStop = true;
            this.opVietNam.Text = "Tiếng Việt";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Location = new System.Drawing.Point(32, 245);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "Mã MID:";
            // 
            // txtMaMid
            // 
            this.txtMaMid.Location = new System.Drawing.Point(141, 238);
            this.txtMaMid.Name = "txtMaMid";
            this.txtMaMid.Size = new System.Drawing.Size(227, 21);
            this.txtMaMid.TabIndex = 7;
            this.txtMaMid.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Location = new System.Drawing.Point(32, 164);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 13);
            this.label8.TabIndex = 7;
            this.label8.Text = "Email doanh nghiệp:";
            // 
            // txtMailDoanhNghiep
            // 
            this.txtMailDoanhNghiep.Location = new System.Drawing.Point(141, 157);
            this.txtMailDoanhNghiep.Name = "txtMailDoanhNghiep";
            this.txtMailDoanhNghiep.Size = new System.Drawing.Size(227, 21);
            this.txtMailDoanhNghiep.TabIndex = 6;
            this.txtMailDoanhNghiep.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(141, 72);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(227, 21);
            this.txtDiaChi.TabIndex = 2;
            this.txtDiaChi.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(141, 45);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Size = new System.Drawing.Size(227, 21);
            this.txtTenDN.TabIndex = 1;
            this.txtTenDN.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(32, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Địa chỉ:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(32, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tên doanh nghiệp:";
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(141, 18);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Size = new System.Drawing.Size(227, 21);
            this.txtMaDN.TabIndex = 0;
            this.txtMaDN.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(32, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Mã doanh nghiệp:";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(125, 350);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 1;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.uiButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(206, 350);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 2;
            this.uiButton2.Text = "Đóng";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(200, 100);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "uiGroupBox2";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // rfvDiaChi
            // 
            this.rfvDiaChi.ControlToValidate = this.txtDiaChi;
            this.rfvDiaChi.ErrorMessage = "\"Địa chỉ\" không được để trống";
            this.rfvDiaChi.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaChi.Icon")));
            this.rfvDiaChi.Tag = "rfvDiaChi";
            // 
            // rfvMaDN
            // 
            this.rfvMaDN.ControlToValidate = this.txtMaDN;
            this.rfvMaDN.ErrorMessage = "\"Mã doanh nghiệp\" không được trống";
            this.rfvMaDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaDN.Icon")));
            this.rfvMaDN.Tag = "rfvMaDN";
            // 
            // rfvTenDN
            // 
            this.rfvTenDN.ControlToValidate = this.txtTenDN;
            this.rfvTenDN.ErrorMessage = "\"Tên doanh nghiệp \" không được trống";
            this.rfvTenDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDN.Icon")));
            this.rfvTenDN.Tag = "rfvTenDN";
            // 
            // rfvTenNganHQ
            // 
            this.rfvTenNganHQ.ControlToValidate = this.txtTenNganHQ;
            this.rfvTenNganHQ.ErrorMessage = "Chưa nhập tên ngắn hải quan";
            this.rfvTenNganHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenNganHQ.Icon")));
            this.rfvTenNganHQ.Tag = "rfvTenNganHQ";
            // 
            // containerValidator1
            // 
            this.containerValidator1.ContainerToValidate = this;
            this.containerValidator1.HostingForm = this;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // rfvTenCuc
            // 
            this.rfvTenCuc.ControlToValidate = this.txtTenCucHQ;
            this.rfvTenCuc.ErrorMessage = "Chưa nhập tên cục hải quan";
            this.rfvTenCuc.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenCuc.Icon")));
            this.rfvTenCuc.Tag = "rfvTenCuc";
            // 
            // rfvDT
            // 
            this.rfvDT.ControlToValidate = this.txtDienThoaiDN;
            this.rfvDT.ErrorMessage = "\"Điện thoại\" không được trống";
            this.rfvDT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDT.Icon")));
            this.rfvDT.Tag = "rfvDT";
            // 
            // rfvNguoiLienHe
            // 
            this.rfvNguoiLienHe.ControlToValidate = this.txtNguoiLienHeDN;
            this.rfvNguoiLienHe.ErrorMessage = "\"Người liên hệ\" không được trống";
            this.rfvNguoiLienHe.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiLienHe.Icon")));
            this.rfvNguoiLienHe.Tag = "rfvNguoiLienHe";
            // 
            // rfvEmailDN
            // 
            this.rfvEmailDN.ControlToValidate = this.txtMailDoanhNghiep;
            this.rfvEmailDN.ErrorMessage = "\"Email doanh nghiệp\" không được trống";
            this.rfvEmailDN.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvEmailDN.Icon")));
            this.rfvEmailDN.Tag = "rfvEmailDN";
            // 
            // rfvMaCuc
            // 
            this.rfvMaCuc.ControlToValidate = this.txtMaCuc;
            this.rfvMaCuc.ErrorMessage = "\"Mã cục Hải quan\" không được trống";
            this.rfvMaCuc.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaCuc.Icon")));
            this.rfvMaCuc.Tag = "rfvMaCuc";
            // 
            // rfvDiaChiHQ
            // 
            this.rfvDiaChiHQ.ControlToValidate = this.txtDiaChiHQ;
            this.rfvDiaChiHQ.ErrorMessage = "\"Địa chỉ hải quan\" không được để trống";
            this.rfvDiaChiHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDiaChiHQ.Icon")));
            this.rfvDiaChiHQ.Tag = "rfvDiaChiHQ";
            // 
            // cbFont
            // 
            this.cbFont.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbFont.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Unicode";
            uiComboBoxItem1.Value = "Unicode";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "TCVN3(ABC)";
            uiComboBoxItem2.Value = "TCVN3";
            this.cbFont.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbFont.Location = new System.Drawing.Point(141, 206);
            this.cbFont.Name = "cbFont";
            this.cbFont.Size = new System.Drawing.Size(83, 21);
            this.cbFont.TabIndex = 18;
            this.cbFont.Tag = "";
            this.cbFont.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbFont.VisualStyleManager = this.vsmMain;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Location = new System.Drawing.Point(225, 212);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(155, 13);
            this.label28.TabIndex = 17;
            this.label28.Text = "(Font hệ thống Hải quan dùng)";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Location = new System.Drawing.Point(32, 212);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(74, 13);
            this.label27.TabIndex = 16;
            this.label27.Text = "Font Hải quan";
            // 
            // ThongTinDNAndHQForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 379);
            this.Controls.Add(this.uiGroupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(403, 413);
            this.Name = "ThongTinDNAndHQForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin doanh nghiệp và hải quan";
            this.Load += new System.EventHandler(this.SendForm_Load);
            this.Controls.SetChildIndex(this.uiGroupBox2, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPageHQ.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.uiTabPageDN.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenNganHQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenCuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiLienHe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvEmailDN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaCuc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDiaChiHQ)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        protected Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDN;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDN;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaChi;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenNganHQ;
        private Company.Controls.CustomValidation.ContainerValidator containerValidator1;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label7;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganHQ;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCucHQ;
        private System.Windows.Forms.Label label5;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenCuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailHaiQuan;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtMailDoanhNghiep;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaMid;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaCuc;
        private Janus.Windows.EditControls.UIRadioButton opTiengAnh;
        private Janus.Windows.EditControls.UIRadioButton opVietNam;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoFaxDN;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtDienThoaiDN;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiLienHeDN;
        private System.Windows.Forms.Label label14;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNguoiLienHe;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvEmailDN;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaCuc;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageHQ;
        private Janus.Windows.UI.Tab.UITabPage uiTabPageDN;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnOpenWS;
        private System.Windows.Forms.Label lblWS;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDichVu;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtPort;
        private Janus.Windows.GridEX.EditControls.EditBox txtHost;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiHQ;
        private System.Windows.Forms.Label label20;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDiaChiHQ;
        private Janus.Windows.EditControls.UIComboBox cbFont;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;

    }
}