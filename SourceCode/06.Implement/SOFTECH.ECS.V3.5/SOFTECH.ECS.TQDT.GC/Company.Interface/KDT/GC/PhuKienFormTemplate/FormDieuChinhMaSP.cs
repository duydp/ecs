﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;


namespace Company.Interface.KDT.GC.PCTFormTemplate
{
    public partial class FormDieuChinhMaSP : BaseForm
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();        
        private string dvt = string.Empty;
        public bool boolFlag;
        public FormDieuChinhMaSP()
        {
            InitializeComponent();
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            if (txtMaNPL.Text.Trim() == "") return;
            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
            sp.HopDong_ID = this.HD.ID;
            sp.Ma = txtMaNPL.Text.Trim();
            if (sp.Load())
            {
                if (sp.TrangThai ==0)
                {
                    txtMaNPL.Text = sp.Ma.Trim();
                    txtTenNPL.Text = sp.Ten.Trim();
                    txtMaHSNPL.Text = sp.MaHS;
                    txtSoLuong.Text = sp.SoLuongDangKy.ToString();
                    cbDonViTinh.SelectedValue = (sp.DVT_ID);
                    cbNhomSP.SelectedValue = sp.NhomSanPham_ID;
                    txtMaSPMoi.Focus();                    
                }
                else if (sp.TrangThai != 0 && HangPK.ID>0)
                {
                    txtMaNPL.Text = sp.Ma.Trim();
                    txtTenNPL.Text = sp.Ten.Trim();
                    txtMaHSNPL.Text = sp.MaHS;
                    txtSoLuong.Text = sp.SoLuongDangKy.ToString();
                    cbDonViTinh.SelectedValue = (sp.DVT_ID);
                    cbNhomSP.SelectedValue = sp.NhomSanPham_ID;
                    txtMaSPMoi.Focus();
                }
                else
                {
                    error.SetError(txtMaNPL, setText("Sản phẩm đang được điều chỉnh nhưng chưa được hải quan duyệt nên không thể điều chỉnh mã sản phẩm được.", "This product have been updated and is pending approval so can not update code now"));
                    txtMaNPL.Focus();
                    return;
                }
            }
            else
            {
                error.SetError(txtMaNPL, setText("Không tồn tại sản phẩm này.","This value is not exist"));
                txtMaNPL.Focus();
                return;
            }
        }
       

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {

            Company.Interface.GC.SanPhamRegistedForm f = new Company.Interface.GC.SanPhamRegistedForm();
            f.SanPhamSelected.HopDong_ID = this.HD.ID;
            f.isDisplayAll = 2;
            f.ShowDialog();
            if (f.SanPhamSelected != null && f.SanPhamSelected.Ma != "")
            {
                txtMaNPL.Text = f.SanPhamSelected.Ma.Trim();
                txtTenNPL.Text = f.SanPhamSelected.Ten.Trim();
                txtMaHSNPL.Text = f.SanPhamSelected.MaHS;
                txtSoLuong.Text = f.SanPhamSelected.SoLuongDangKy.ToString();
                cbDonViTinh.SelectedValue = (f.SanPhamSelected.DVT_ID);
                cbNhomSP.SelectedValue = f.SanPhamSelected.NhomSanPham_ID;
                txtMaSPMoi.Focus();
            }
            
        }
        private bool CheckNPLExit(string maNPL)
        {
            foreach(HangPhuKien pk1 in LoaiPK.HPKCollection)
            {
                if (pk1.ThongTinCu == maNPL.Trim())
                {
                    return true;
                }
            }
            return false; ;
        }
             

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid) return;
                if (txtMaNPL.Text.Trim() == txtMaSPMoi.Text.Trim())
                {
                    showMsg("MSG_240219");
                    //ShowMessage("Mã sản phẩm mới phải khác mã sản phẩm cũ.", false);
                    return;
                }
                Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                sp.HopDong_ID = pkdk.HopDong_ID;
                sp.Ma = txtMaSPMoi.Text.Trim();
                if (sp.Load())
                {
                    showMsg("MSG_240220");
                    //ShowMessage("Mã sản phẩm này đã được khai báo.", false);
                    return;
                }                
                LoaiPK.HPKCollection.Remove(HangPK);
                if (CheckNPLExit(txtMaNPL.Text.Trim()))
                {
                    showMsg("MSG_240218");
                    //ShowMessage("Đã điều chỉnh sản phẩm này rồi", false);
                    return;
                }
                foreach (HangPhuKien p in LoaiPK.HPKCollection)
                    if (p.MaHang == txtMaSPMoi.Text.Trim())
                    {
                        showMsg("MSG_240221");
                        //ShowMessage("Đã có mã sản phẩm này trong danh sách điều chỉnh.", false);
                        if (HangPK.MaHang.Trim() != "")
                            LoaiPK.HPKCollection.Add(HangPK);
                        return;
                    }
                HangPK.MaHang = txtMaSPMoi.Text.Trim();
                HangPK.TenHang = txtTenNPL.Text.Trim();
                HangPK.SoLuong = Convert.ToDecimal(txtSoLuong.Text);
                HangPK.MaHS = txtMaHSNPL.Text.Trim();
                HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                if(HangPK.ThongTinCu.Trim()=="")
                    HangPK.ThongTinCu = txtMaNPL.Text.Trim();
                HangPK.NhomSP = cbNhomSP.SelectedValue.ToString();
                LoaiPK.HPKCollection.Add(HangPK);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                if( boolFlag == true )
                {
                    LoaiPK.InsertUpdateDieuChinhMaSPDaDangKy(pkdk.HopDong_ID);
                    boolFlag = false;
                }
                else
                {
                    LoaiPK.InsertUpdateDieuChinhMaSP(pkdk.HopDong_ID);
                }
                
                //PhuKienGCSendForm.isEdit = true;
                reset();


            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
        private void reset()
        {
            txtMaNPL.Text = "";
            txtTenNPL.Text = "";
            txtSoLuong.Text = "0.000";
            txtMaHSNPL.Text = "";            
            txtMaSPMoi.Text = "";
            cbDonViTinh.Text="";
            cbNhomSP.Text = "";                
            ReSetErro();
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
            HangPK = new HangPhuKien();
        }

        private void ReSetErro()
        {
            error.SetError(txtMaHSNPL, null);
        }
        private void DieuChinhSoLuongNPLForm_Load(object sender, EventArgs e)
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            
            Company.GC.BLL.GC.NhomSanPham nhomSP = new Company.GC.BLL.GC.NhomSanPham();
            nhomSP.HopDong_ID = this.HD.ID;
            cbNhomSP.DataSource = nhomSP.SelectCollectionBy_HopDong_ID();
            cbNhomSP.DisplayMember = "TenSanPham";
            cbNhomSP.ValueMember = "MaSanPham";   
 

            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == "S10")
                {
                    LoaiPK = pk;                    
                }               
            }
            if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                btnUpdate.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                LoaiPK.LoadCollection();
                btnXoa.Enabled = false;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ||pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                btnXoa.Enabled = true;
                btnUpdate.Enabled = true;
            }
            else if (pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                btnUpdate.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                btnXoa.Enabled = false;
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();
            //Registered
            if (boolFlag == true)
            {
                btnUpdate.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            LoaiPK.MaPhuKien = "S10";
            dgList.DataSource = LoaiPK.HPKCollection; 
        }
   
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                HangPK = (HangPhuKien)e.Row.DataRow;
                txtMaNPL.Text = HangPK.MaHang.Trim();
                txtTenNPL.Text = HangPK.TenHang.Trim();
                txtMaHSNPL.Text = HangPK.MaHS.Trim();
                txtSoLuong.Text = HangPK.SoLuong.ToString();
                cbDonViTinh.SelectedValue = HangPK.DVT_ID;
                cbNhomSP.SelectedValue = HangPK.NhomSP;
                txtMaSPMoi.Text = "";
            }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                e.Row.Cells["NhomSanPham_ID"].Text = Company.KDT.SHARE.Components.DuLieuChuan.NhomSanPham.getTenSanPham(e.Row.Cells["NhomSanPham_ID"].Text);
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa sản phẩm này không?", true) == "Yes")
            {

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            pkDelete.Delete(LoaiPK.MaPhuKien,pkdk.HopDong_ID);
                            //pkDelete.DeleteTransaction(null);
                        }
                    }
                }

            }
            else
            {
                e.Cancel = true;
            }
        }

        private void FormDieuChinhMaSP_FormClosing(object sender, System.Windows.Forms.FormClosingEventArgs e)
        {
            if (LoaiPK.HPKCollection.Count > 0)
            {
                LoaiPK.MaPhuKien = "S10";
                LoaiPK.NoiDung = LoaiPhuKien_GetName("S10");
                pkdk.PKCollection.Remove(LoaiPK);
                pkdk.PKCollection.Add(LoaiPK);
            }
            else
            {
                pkdk.PKCollection.Remove(LoaiPK);
                LoaiPK.Delete(pkdk.HopDong_ID); ;
                //LoaiPK.DeleteTransaction(null);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (LoaiPK.HPKCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count < 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            //if (ShowMessage("Bạn có muốn xóa sản phẩm này không?", true) == "Yes")
            {                
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        try 
                        {
                            if (pkDelete.ID > 0)
                            {
                                pkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                                //pkDelete.DeleteTransaction(null);
                            }
                             LoaiPK.HPKCollection.Remove(pkDelete);
                        }
                        catch { }
                    }
                }
                dgList.DataSource = LoaiPK.HPKCollection;
                this.reset();
                try { dgList.Refetch(); }
                catch { dgList.Refresh(); }
            }
        }

      

       
    }
}