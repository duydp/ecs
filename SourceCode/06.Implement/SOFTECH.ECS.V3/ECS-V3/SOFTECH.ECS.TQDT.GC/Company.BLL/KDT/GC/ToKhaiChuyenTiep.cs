﻿using System;
using System.Data.SqlClient;
using System.Data;
using System.Xml;
using System.Globalization;
using System.Collections.Generic;
using System.Data.Common;

using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using GiayPhep = Company.KDT.SHARE.QuanLyChungTu.GCCT.GiayPhep;
using System.Xml.Serialization;
namespace Company.GC.BLL.KDT.GC
{
    public partial class ToKhaiChuyenTiep
    {
        public void SetDabaseMoi(string nameDatabase)
        {
            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameDatabase);
        }
        //---------------------------------------------------------------------------------------------
        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
        
        private List<HangChuyenTiep> _HCTCollection = new List<HangChuyenTiep>();
        private List<PhanBoToKhaiXuat> _PhanBoToKhaiXuatCollection = new List<PhanBoToKhaiXuat>();
        public List<PhanBoToKhaiXuat> PhanBoToKhaiXuatCollection
        {
            set { _PhanBoToKhaiXuatCollection = value; }
            get { return _PhanBoToKhaiXuatCollection; }
        }
        public List<HangChuyenTiep> HCTCollection
        {
            set { _HCTCollection = value; }
            get { return _HCTCollection; }
        }
        public List<Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK> NoiDungDieuChinhTKCollection = new List<Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK>();
        #region ChungTuKem

        [XmlArray("GiayPheps")]
        [XmlArrayItem("GiayPhep")]
        public List<GiayPhep> GiayPhepCollection = new List<GiayPhep>();
        public List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon> HoaDonThuongMaiCollection = new List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon>();
        public List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong> HopDongThuongMaiCollection = new List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong>();
        public List<Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau> DeNghiChuyenCuaKhau = new List<Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau>();
        public List<Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh> AnhCollection = new List<Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh>();
        public List<Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuNo> ChungTuNoCollection = new List<Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuNo>();
        public List<Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng> NPLCungUngs = new List<Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng>();
        public void LoadChungTuKem()
        {
            //Xoa du lieu truoc khi load lai tranh truong hop trung du lieu.
            HoaDonThuongMaiCollection.Clear();
            HopDongThuongMaiCollection.Clear();
            GiayPhepCollection.Clear();
            DeNghiChuyenCuaKhau.Clear();
            AnhCollection.Clear();
            ChungTuNoCollection.Clear();
            //Hoa don thuong mai
            HoaDonThuongMaiCollection = (List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon>)Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon.SelectCollectionBy_ID_TK(this.ID);
            for (int j = 0; j < HoaDonThuongMaiCollection.Count; j++)
            {
                HoaDonThuongMaiCollection[j].LoadListHangDMOfHoaDon();
            }
            //Hop dong thuong mai
            HopDongThuongMaiCollection = (List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong>)Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong.SelectCollectionBy_ID_TK(this.ID);
            for (int i = 0; i < HopDongThuongMaiCollection.Count; i++)
            {
                HopDongThuongMaiCollection[i].LoadListHangDMOfHopDong();
            }
            //Giay phep
            GiayPhepCollection = (List<GiayPhep>)GiayPhep.SelectCollectionDynamic("TKCT_ID=" + this.ID, "");
            for (int m = 0; m < GiayPhepCollection.Count; m++)
            {
                GiayPhepCollection[m].LoadListHMDofGiayPhep();
            }
            //Chuyen cua khau
            DeNghiChuyenCuaKhau = (List<Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau>)Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau.SelectCollectionBy_ID_TK(this.ID);
            //Chung tu Anh
            AnhCollection = (List<Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh>)Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh.SelectCollectionBy_ID_TK(this.ID);
            for (int k = 0; k < AnhCollection.Count; k++)
            {
                AnhCollection[k].LoadListChungTuKemAnhChiTiet();
            }
            #region Chứng từ nợ
            ChungTuNoCollection = (List<Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuNo>)Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuNo.SelectCollectionDynamic("TKCTID=" + this.ID, "");
            #endregion
            NPLCungUngs = Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng.SelectCollectionDynamic("TKCTID=" + this.ID, "");
            foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng item in NPLCungUngs)
            {
                item.NPLCungUngDetails = Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail.SelectCollectionDynamic("Master_ID=" + item.ID, "");
            }
        }

        // Add extra :
        public List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HangChuyenTiep> ConvertHMDKDToHangMauDich()
        {
            List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HangChuyenTiep> listHang = new List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HangChuyenTiep>();
            foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep hmdKD in this.HCTCollection)
            {
                Company.KDT.SHARE.QuanLyChungTu.GCCT.HangChuyenTiep hang = new Company.KDT.SHARE.QuanLyChungTu.GCCT.HangChuyenTiep();
                hang.ID = hmdKD.ID;
                hang.DonGia = hmdKD.DonGia;
                hang.ID_DVT = hmdKD.ID_DVT;
                hang.MaHS = hmdKD.MaHS;
                hang.MaHang = hmdKD.MaHang;
                hang.ID_NuocXX = hmdKD.ID_NuocXX;
                hang.SoLuong = hmdKD.SoLuong;
                hang.SoThuTuHang = hmdKD.SoThuTuHang;
                hang.TenHang = hmdKD.TenHang;
                hang.TriGia = hmdKD.TriGia;
                hang.TriGiaKB_VND = hmdKD.TriGiaKB_VND;
                listHang.Add(hang);
            }
            return listHang;
        }

        #endregion ChungTuKem

        //-----------------------------------------------------------------------------------------

        public static long GetIDToKhaiChuyenTiepBySoToKhaiAndNamDangKy(string MaHQ, string MaLH, long SoToKhai, int NamDK)
        {
            string sql = "select ID from t_KDT_GC_ToKhaiChuyenTiep where SoToKhai=" + SoToKhai + " and year(NgayDangKy)=" + NamDK + " and MaHaiQuanTiepNhan='" + MaHQ + "' and MaLoaiHinh='" + MaLH + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            object id = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(id);
        }
        public static long GetIDToKhaiChuyenTiepBySoToKhaiAndNamDangKy(string MaHQ, string MaLH, long SoToKhai, int NamDK, string name)
        {
            string sql = "select ID from t_KDT_GC_ToKhaiChuyenTiep where SoToKhai=" + SoToKhai + " and year(NgayDangKy)=" + NamDK + " and MaHaiQuanTiepNhan='" + MaHQ + "' and MaLoaiHinh='" + MaLH + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(name);
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            object id = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(id);
        }
        public bool CheckPhanBo()
        {
            string sql = "SELECT count(*) FROM t_GC_PhanBoToKhaiXuat WHERE ID_TKMD =" + ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            int id = 0;
            try
            {
                id = Convert.ToInt32(db.ExecuteScalar(dbCommand));
            }
            catch
            { }
            return id > 0;
        }
        public DataSet GetToKhaiChuyenTiepXuatThietBi(long IDHopDong)
        {
            string sql = "Select ID,SoToKhai,MaLoaiHinh,NgayDangKy Where IDHopDong = @IDHopDong MaLoaiHinh LIKE 'PHTBX'"
                + " AND MaLoaiHinh LIKE 'XGC20'" //TODO: Hungtq update 22/02/2011
                + " AND TrangThaiXuLy = 1";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public void LoadHCTCollection()
        {
            HangChuyenTiep hct = new HangChuyenTiep();
            hct.Master_ID = this.ID;
            this.HCTCollection = hct.SelectCollectionBy_Master_ID();
        }

        public void LoadHCTCollection(string dbName)
        {
            HangChuyenTiep hct = new HangChuyenTiep();
            SetDabaseMoi(dbName);
            hct.Master_ID = this.ID;
            if (!dbName.Equals("MSSQL"))
                this.HCTCollection = hct.SelectCollectionBy_Master_ID();
            else
                this.HCTCollection = hct.SelectCollectionBy_Master_ID_KTX();
        }

        public void LoadHCTCollection(SqlTransaction trans, string dbName)
        {
            HangChuyenTiep hct = new HangChuyenTiep();
            hct.Master_ID = this.ID;
            this.HCTCollection = hct.SelectCollectionBy_Master_ID(trans, dbName);
        }

        public void LoadHCTCollectionKTX(SqlTransaction trans, string dbName)
        {
            HangChuyenTiep hct = new HangChuyenTiep();
            hct.Master_ID = this.ID;
            this.HCTCollection = hct.SelectCollectionBy_Master_IDKTX(trans, dbName);
        }

        #region ChungTuHaiQuan

        //public List<Company.KDT.SHARE.QuanLyChungTu.GCCT.NoiDungDieuChinhTK> NoiDungDieuChinhTKCollection = new List<Company.KDT.SHARE.QuanLyChungTu.GCCT.NoiDungDieuChinhTK>();

        public void LoadChungTuHaiQuan(string dbname)
        {
            if (!string.IsNullOrEmpty(dbname))
                SetDabaseMoi(dbname);

            //Xoa du lieu truoc khi load lai tranh truong hop trung du lieu.
            HoaDonThuongMaiCollection.Clear();
            HopDongThuongMaiCollection.Clear();
            GiayPhepCollection.Clear();
            DeNghiChuyenCuaKhau.Clear();
            AnhCollection.Clear();
            //NoiDungDieuChinhTKCollection.Clear();

            HoaDonThuongMaiCollection = (List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon>)Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon.SelectCollectionBy_ID_TK(this.ID);
            for (int j = 0; j < HoaDonThuongMaiCollection.Count; j++)
            {
                HoaDonThuongMaiCollection[j].LoadListHangDMOfHoaDon();
            }

            HopDongThuongMaiCollection = (List<Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong>)Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong.SelectCollectionBy_ID_TK(this.ID);
            for (int i = 0; i < HopDongThuongMaiCollection.Count; i++)
            {
                HopDongThuongMaiCollection[i].LoadListHangDMOfHopDong();
            }

            GiayPhepCollection = (List<GiayPhep>)GiayPhep.SelectCollectionDynamic("TKCT_ID=" + this.ID, "");
            for (int m = 0; m < GiayPhepCollection.Count; m++)
            {
                GiayPhepCollection[m].LoadListHMDofGiayPhep();
            }

            DeNghiChuyenCuaKhau = (List<Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau>)Company.KDT.SHARE.QuanLyChungTu.GCCT.DeNghiChuyenCuaKhau.SelectCollectionBy_ID_TK(this.ID);

            AnhCollection = (List<Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh>)Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnh.SelectCollectionBy_ID_TK(this.ID);
            for (int k = 0; k < AnhCollection.Count; k++)
            {
                AnhCollection[k].LoadListChungTuKemAnhChiTiet();
            }

            //NoiDungDieuChinhTKCollection = Company.KDT.SHARE.QuanLyChungTu.GCCT.NoiDungDieuChinhTK.SelectCollectionBy_TKMD_ID(this.ID);
            //for (int k = 0; k < NoiDungDieuChinhTKCollection.Count; k++)
            //{
            //    NoiDungDieuChinhTKCollection[k].LoadListNDDCTK();
            //}
        }

        #endregion ChungTuHaiQuan
        public bool Load(SqlTransaction trans)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Load";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            IDataReader reader = db.ExecuteReader(dbCommand, trans);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) this.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public ToKhaiChuyenTiep()
        {
            NgayBienLai = new DateTime(1900, 1, 1);
        }
        public bool Load(SqlTransaction trans, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_LoadBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            IDataReader reader = null;
            if (trans != null)
                reader = this.db.ExecuteReader(dbCommand, trans);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) this.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool LoadKTX(SqlTransaction trans, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_LoadByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            IDataReader reader = null;
            if (trans != null)
                reader = this.db.ExecuteReader(dbCommand, trans);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool LoadBySoToKhai()
        {
            string spName = "SELECT * FROM t_KDT_GC_ToKhaiChuyenTiep WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND Year(NgayDangKy) = @NamDangKy AND MaHaiQuanTiepNhan = @MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, this.NgayDangKy.Year);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuanTiepNhan);


            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) this.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public bool LoadBySoToKhaiKTX()
        {
            string spName = "SELECT * FROM t_KDT_GC_ToKhaiChuyenTiep WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND Year(NgayDangKy) = @NamDangKy AND MaHaiQuanTiepNhan = @MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, this.NgayDangKy.Year);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuanTiepNhan);


            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public bool LoadBySoToKhai(int namdk)
        {
            string spName = "SELECT * FROM t_KDT_GC_ToKhaiChuyenTiep WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND Year(NgayDangKy) = @NamDangKy AND MaHaiQuanTiepNhan = @MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, namdk);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuanTiepNhan);


            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) this.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool LoadBySoToKhai(SqlTransaction transaction, int NamDangKy, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "SELECT * FROM t_KDT_GC_ToKhaiChuyenTiep WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND Year(NgayDangKy) = @NamDangKy AND MaHaiQuanTiepNhan = @MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuanTiepNhan);

            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) this.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public bool LoadBySoToKhaiKTX(SqlTransaction transaction, int NamDangKy, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "SELECT * FROM t_KDT_GC_ToKhaiChuyenTiep WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND Year(NgayDangKy) = @NamDangKy AND MaHaiQuanTiepNhan = @MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuanTiepNhan);

            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public bool LoadBySoToKhaiKTX(int namdk)
        {
            string spName = "SELECT * FROM t_KDT_GC_ToKhaiChuyenTiep WHERE SoToKhai = @SoToKhai AND MaLoaiHinh = @MaLoaiHinh AND Year(NgayDangKy) = @NamDangKy AND MaHaiQuanTiepNhan = @MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this.SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this.MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, namdk);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this.MaHaiQuanTiepNhan);


            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public bool isHaveWith4Para()
        {
            string whereCond = "MaHaiQuanTiepNhan ='" + this.MaHaiQuanTiepNhan + "'";
            whereCond += " and MaLoaiHinh ='" + this.MaLoaiHinh + "'";
            whereCond += " and SoToKhai = " + this.SoToKhai + "";
            whereCond += " and year(NgayDangKy) = " + this.NgayDangKy.Year + "";
            string order = "ID";
            DataSet ds = ToKhaiChuyenTiep.SelectDynamic(whereCond, order);
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                //DateTime NgayDK = (DateTime)ds.Tables[0].Rows[i]["NgayDangKy"];
                //if (NgayDK.Year == this.NgayDangKy.Year)
                //{

                //}
                return true;
            }
            return false;
        }

        //-----------------------------------------------------------------------------------------

        public void DeleteHangHoa()
        {
            if (this.HCTCollection.Count == 0)
            {
                this.LoadHCTCollection();
            }
            string LoaiHangHoa = "N";
            if (this.MaLoaiHinh.IndexOf("SP") > 0 || this.MaLoaiHinh.IndexOf("19") > 0)
            {
                if (this.MaLoaiHinh.Trim().EndsWith("X") || MaLoaiHinh.Substring(0, 1).Equals("X")) LoaiHangHoa = "S";
            }
            else if (this.MaLoaiHinh.IndexOf("TB") > 0 || this.MaLoaiHinh.IndexOf("20") > 0)
                LoaiHangHoa = "T";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (HangChuyenTiep HCT in HCTCollection)
                    {
                        HCT.Delete(transaction, this.IDHopDong, LoaiHangHoa, this.MaLoaiHinh);
                    }
                    if (this.MaLoaiHinh.Substring(0, 1) == "N" && LoaiHangHoa == "N")
                        NPLNhapTonThucTe.DeleteNPLTonByToKhai((int)this.SoToKhai, (short)this.NgayDangKy.Year, this.MaLoaiHinh, this.MaHaiQuanTiepNhan, transaction);
                    this.Delete(transaction);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NChar, GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year <= 1753 ? DBNull.Value : (object)ThoiGianGiaoHang);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int InsertUpdateTransactionKTX(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_InsertUpdateByKTX";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }
        public bool InsertUpdateDongBoDuLieu()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert(transaction);
                    else
                        this.Update(transaction);
                    int i = 0;
                    foreach (HangChuyenTiep hct in this.HCTCollection)
                    {
                        if (id == 0)
                            hct.ID = 0;
                        hct.SoThuTuHang = ++i;
                        if (id == 0)
                        {
                            hct.ID = 0;
                        }

                        if (hct.ID == 0)
                        {
                            hct.Master_ID = this.ID;
                            hct.ID = hct.Insert(transaction);
                        }
                        else
                        {
                            hct.Update(transaction);
                        }

                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdateDongBoDuLieu(List<ToKhaiChuyenTiep> collection, HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    ToKhaiChuyenTiep tkctDelete = new ToKhaiChuyenTiep();
                    tkctDelete.IDHopDong = HD.ID;
                    tkctDelete.DeleteBy_IDHopDong(transaction);
                    foreach (ToKhaiChuyenTiep item in collection)
                    {
                        item.IDHopDong = HD.ID;
                        if (item.ID == 0)
                            item.ID = item.Insert(transaction);
                        else
                            item.Update(transaction);
                        int i = 0;
                        foreach (HangChuyenTiep hct in item.HCTCollection)
                        {
                            hct.SoThuTuHang = ++i;
                            hct.Master_ID = item.ID;
                            hct.ID = hct.Insert(transaction);

                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateDongBoDuLieu(List<ToKhaiChuyenTiep> collection, HopDong HD, string dbName)
        {
            SetDabaseMoi(dbName);

            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    //ToKhaiChuyenTiep tkctDelete = new ToKhaiChuyenTiep();
                    //tkctDelete.IDHopDong = HD.ID;
                    //tkctDelete.DeleteBy_IDHopDong(transaction, dbName);

                    foreach (ToKhaiChuyenTiep item in collection)
                    {
                        item.IDHopDong = HD.ID;
                        item.InsertUpdateTransaction(transaction, dbName);

                        ToKhaiChuyenTiep itemTemp = new ToKhaiChuyenTiep();
                        itemTemp.IDHopDong = HD.ID;
                        itemTemp.SoToKhai = item.SoToKhai;
                        itemTemp.NgayDangKy = item.NgayDangKy;
                        itemTemp.MaLoaiHinh = item.MaLoaiHinh;
                        itemTemp.MaHaiQuanTiepNhan = item.MaHaiQuanTiepNhan;
                        itemTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                        itemTemp.Load(transaction, dbName);

                        int i = 0;
                        foreach (HangChuyenTiep hct in item.HCTCollection)
                        {
                            hct.SoThuTuHang = ++i;
                            hct.Master_ID = itemTemp.ID;
                            hct.ID = hct.InsertUpdate(transaction, dbName);

                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public int DeleteBy_IDHopDong(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_DeleteBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }
        public bool InsertUpdateDongBoDuLieuKTX(List<ToKhaiChuyenTiep> collection, HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    ToKhaiChuyenTiep tkctDelete = new ToKhaiChuyenTiep();
                    tkctDelete.IDHopDong = HD.ID;
                    tkctDelete.DeleteBy_IDHopDong(transaction);
                    foreach (ToKhaiChuyenTiep item in collection)
                    {
                        item.IDHopDong = HD.ID;
                        if (item.ID == 0)
                            item.ID = item.InsertTransactionKTX(transaction);
                        else
                            item.UpdateTransactionKTX(transaction);
                        int i = 0;
                        foreach (HangChuyenTiep hct in item.HCTCollection)
                        {
                            hct.SoThuTuHang = ++i;
                            hct.Master_ID = item.ID;
                            hct.ID = hct.Insert(transaction);

                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdateDongBoDuLieuKTX(List<ToKhaiChuyenTiep> collection, HopDong HD, string dbName)
        {
            SetDabaseMoi(dbName);

            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    //ToKhaiChuyenTiep tkctDelete = new ToKhaiChuyenTiep();
                    //tkctDelete.IDHopDong = HD.ID;
                    //tkctDelete.DeleteBy_IDHopDong(transaction, dbName);

                    foreach (ToKhaiChuyenTiep item in collection)
                    {
                        item.ID = 0;
                        item.IDHopDong = HD.ID;
                        item.InsertUpdateTransactionKTX(transaction, dbName);

                        ToKhaiChuyenTiep itemTemp = new ToKhaiChuyenTiep();
                        itemTemp.IDHopDong = item.IDHopDong;
                        itemTemp.SoToKhai = item.SoToKhai;
                        itemTemp.NgayDangKy = item.NgayDangKy;
                        itemTemp.MaLoaiHinh = item.MaLoaiHinh;
                        itemTemp.MaHaiQuanTiepNhan = item.MaHaiQuanTiepNhan;
                        itemTemp.MaDoanhNghiep = item.MaDoanhNghiep;
                        itemTemp.LoadKTX(transaction, dbName);

                        int i = 0;
                        foreach (HangChuyenTiep hct in item.HCTCollection)
                        {
                            hct.SoThuTuHang = ++i;
                            hct.Master_ID = itemTemp.ID;
                            hct.ID = hct.InsertUpdateKTX(transaction, dbName);

                            //==================
                            HangChuyenTiep HCTCu = new HangChuyenTiep();
                            HCTCu = hct;
                            string LoaiHangHoa = "N";
                            if (item.MaLoaiHinh.IndexOf("SP") > 0 || item.MaLoaiHinh.IndexOf("19") > 0)
                            {
                                if (item.MaLoaiHinh.Trim().EndsWith("X") || MaLoaiHinh.Substring(0, 1).Equals("X")) LoaiHangHoa = "S";
                            }
                            else if (item.MaLoaiHinh.IndexOf("TB") > 0 || item.MaLoaiHinh.IndexOf("20") > 0)
                                LoaiHangHoa = "T";

                            hct.UpdateSoLuong(transaction, item.IDHopDong, LoaiHangHoa, item.MaLoaiHinh, HCTCu);
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NChar, GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year <= 1753 ? DBNull.Value : (object)ThoiGianGiaoHang);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        public long InsertTransactionKTX(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        public long InsertTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);
            this.db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.NChar, GUIDSTR);
            this.db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
            this.db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
            this.db.AddInParameter(dbCommand, "@ActionStatus", SqlDbType.Int, ActionStatus);
            this.db.AddInParameter(dbCommand, "@GuidReference", SqlDbType.NVarChar, GuidReference);
            this.db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK);
            this.db.AddInParameter(dbCommand, "@HUONGDAN", SqlDbType.NVarChar, HUONGDAN);
            this.db.AddInParameter(dbCommand, "@PhanLuong", SqlDbType.VarChar, PhanLuong);
            this.db.AddInParameter(dbCommand, "@Huongdan_PL", SqlDbType.NVarChar, Huongdan_PL);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.VarChar, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@ThoiGianGiaoHang", SqlDbType.DateTime, ThoiGianGiaoHang.Year <= 1753 ? DBNull.Value : (object)ThoiGianGiaoHang);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransactionKTX(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public int UpdateTransactionKTX(SqlTransaction transaction, string databaseName)
        {
            SetDabaseMoi(databaseName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            this.db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            this.db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan);
            this.db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, MaHaiQuanTiepNhan);
            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
            this.db.AddInParameter(dbCommand, "@CanBoDangKy", SqlDbType.NVarChar, CanBoDangKy);
            this.db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            this.db.AddInParameter(dbCommand, "@SoHopDongDV", SqlDbType.VarChar, SoHopDongDV);
            this.db.AddInParameter(dbCommand, "@NgayHDDV", SqlDbType.DateTime, NgayHDDV);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDDV", SqlDbType.DateTime, NgayHetHanHDDV);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhDV", SqlDbType.NVarChar, NguoiChiDinhDV);
            this.db.AddInParameter(dbCommand, "@MaKhachHang", SqlDbType.VarChar, MaKhachHang);
            this.db.AddInParameter(dbCommand, "@TenKH", SqlDbType.NVarChar, TenKH);
            this.db.AddInParameter(dbCommand, "@SoHDKH", SqlDbType.VarChar, SoHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHDKH", SqlDbType.DateTime, NgayHDKH);
            this.db.AddInParameter(dbCommand, "@NgayHetHanHDKH", SqlDbType.DateTime, NgayHetHanHDKH);
            this.db.AddInParameter(dbCommand, "@NguoiChiDinhKH", SqlDbType.NVarChar, NguoiChiDinhKH);
            this.db.AddInParameter(dbCommand, "@MaDaiLy", SqlDbType.VarChar, MaDaiLy);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.NChar, MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@DiaDiemXepHang", SqlDbType.NVarChar, DiaDiemXepHang);
            this.db.AddInParameter(dbCommand, "@TyGiaVND", SqlDbType.Money, TyGiaVND);
            this.db.AddInParameter(dbCommand, "@TyGiaUSD", SqlDbType.Money, TyGiaUSD);
            this.db.AddInParameter(dbCommand, "@LePhiHQ", SqlDbType.Money, LePhiHQ);
            this.db.AddInParameter(dbCommand, "@SoBienLai", SqlDbType.NChar, SoBienLai);
            this.db.AddInParameter(dbCommand, "@NgayBienLai", SqlDbType.DateTime, NgayBienLai);
            this.db.AddInParameter(dbCommand, "@ChungTu", SqlDbType.NVarChar, ChungTu);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@MaHaiQuanKH", SqlDbType.Char, MaHaiQuanKH);
            this.db.AddInParameter(dbCommand, "@ID_Relation", SqlDbType.BigInt, ID_Relation);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public List<ToKhaiChuyenTiep> SelectCollectionBy_IDHopDong(string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);

            List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public List<ToKhaiChuyenTiep> SelectCollectionBy_IDHopDong_KTX()
        {
            string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);

            List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public List<ToKhaiChuyenTiep> SelectCollectionBy_IDHopDong_KTX(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);

            List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public List<ToKhaiChuyenTiep> SelectCollectionBy_IDHopDong_KTX_KhongLayChuaKhaiBao(SqlTransaction transaction, string dbName)
        {
            List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();

            IDataReader reader = this.SelectReaderDynamic("IDHopDong = " + IDHopDong + " and TrangThaiXuLy != -1", "", null, dbName);

            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression, SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            return reader;
        }

        public int DeleteBy_MaDoanhNghiep(SqlTransaction transaction)
        {
            string spName = "delete from t_KDT_GC_ToKhaiChuyenTiep where MaDoanhNghiep='" + MaDoanhNghiep + "'";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        public bool InsertUpdateDongBoDuLieu(List<ToKhaiChuyenTiep> collection, string MaDoanhNghiep)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    ToKhaiChuyenTiep tkctDelete = new ToKhaiChuyenTiep();
                    tkctDelete.MaDoanhNghiep = MaDoanhNghiep;
                    tkctDelete.DeleteBy_MaDoanhNghiep(transaction);
                    foreach (ToKhaiChuyenTiep item in collection)
                    {

                        item.Insert(transaction);
                        int i = 0;
                        foreach (HangChuyenTiep hct in item.HCTCollection)
                        {
                            hct.SoThuTuHang = ++i;
                            hct.Master_ID = item.ID;
                            hct.ID = hct.Insert(transaction);

                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateDongBoDuLieuMoi(List<ToKhaiChuyenTiep> collection, HopDong HD)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    //ToKhaiChuyenTiep tkctDelete = new ToKhaiChuyenTiep();
                    //tkctDelete.IDHopDong = HD.ID;
                    //tkctDelete.DeleteBy_IDHopDong(transaction);
                    foreach (ToKhaiChuyenTiep item in collection)
                    {
                        item.IDHopDong = HD.ID;
                        if (item.ID == 0)
                            item.ID = item.Insert(transaction);
                        else
                            item.Update(transaction);
                        int i = 0;
                        foreach (HangChuyenTiep hct in item.HCTCollection)
                        {
                            hct.SoThuTuHang = ++i;
                            hct.Master_ID = item.ID;
                            hct.ID = hct.Insert(transaction);

                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateDongBoDuLieuMoiKTX(List<ToKhaiChuyenTiep> collection, HopDong HD, string databaseName)
        {
            SetDabaseMoi(databaseName);

            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    //ToKhaiChuyenTiep tkctDelete = new ToKhaiChuyenTiep();
                    //tkctDelete.IDHopDong = HD.ID;
                    //tkctDelete.DeleteBy_IDHopDong(transaction);
                    foreach (ToKhaiChuyenTiep item in collection)
                    {
                        item.IDHopDong = HD.ID;
                        if (item.ID == 0)
                            item.ID = item.InsertTransactionKTX(transaction, databaseName);
                        else
                            item.UpdateTransactionKTX(transaction, databaseName);

                        int i = 0;
                        foreach (HangChuyenTiep hct in item.HCTCollection)
                        {
                            hct.SoThuTuHang = ++i;
                            hct.Master_ID = item.ID;
                            hct.ID = hct.InsertTransactionKTX(transaction, databaseName);

                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch (Exception ex)
                {
                    ret = false;
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdateDongBoDuLieuMoi(List<ToKhaiChuyenTiep> collection)
        {
            bool ret;
            List<ToKhaiChuyenTiep> TKCTTMP = new List<ToKhaiChuyenTiep>();
            foreach (ToKhaiChuyenTiep tk in collection)
            {
                if (tk.CheckExistToKhaiChuyenTiep(tk.MaHaiQuanTiepNhan, tk.MaLoaiHinh, tk.SoToKhai, tk.NgayDangKy))
                    continue;
                TKCTTMP.Add(tk);
            }
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    //ToKhaiChuyenTiep tkctDelete = new ToKhaiChuyenTiep();
                    //tkctDelete.IDHopDong = HD.ID;
                    //tkctDelete.DeleteBy_IDHopDong(transaction);
                    foreach (ToKhaiChuyenTiep item in TKCTTMP)
                    {
                        if (item.CheckExistToKhaiChuyenTiep(item.MaHaiQuanTiepNhan, item.MaLoaiHinh, item.SoToKhai, item.NgayDangKy))
                            continue;
                        item.Insert(transaction);
                        int i = 0;
                        foreach (HangChuyenTiep hct in item.HCTCollection)
                        {
                            hct.SoThuTuHang = ++i;
                            hct.Master_ID = item.ID;
                            hct.ID = hct.Insert(transaction);

                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool InsertUpdateFull()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                long id = this.ID;
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert(transaction);
                    else
                        this.Update(transaction);
                    int i = 0;
                    foreach (HangChuyenTiep hct in this.HCTCollection)
                    {
                        hct.SoThuTuHang = ++i;
                        if (id == 0)
                        {
                            hct.ID = 0;
                        }
                        HangChuyenTiep HCTCu = new HangChuyenTiep();
                        if (hct.ID > 0)
                        {
                            HCTCu.ID = hct.ID;
                            HCTCu.Load(transaction);
                        }
                        if (hct.ID == 0)
                        {
                            hct.Master_ID = this.ID;
                            hct.ID = hct.Insert(transaction);
                        }
                        else
                        {
                            hct.Update(transaction);
                        }
                        HCTCu = hct;
                        string LoaiHangHoa = "N";
                        if (this.MaLoaiHinh.IndexOf("SP") > 0 || this.MaLoaiHinh.IndexOf("19") > 0)
                        {
                            if (this.MaLoaiHinh.Trim().EndsWith("X") || MaLoaiHinh.Substring(0, 1).Equals("X")) LoaiHangHoa = "S";
                        }
                        else if (this.MaLoaiHinh.IndexOf("TB") > 0 || this.MaLoaiHinh.IndexOf("20") > 0)
                            LoaiHangHoa = "T";

                        hct.UpdateSoLuong(transaction, this.IDHopDong, LoaiHangHoa, this.MaLoaiHinh, HCTCu);
                        
                    }
                    #region Chứng từ nợ
                    if (ChungTuNoCollection != null)
                    {
                        foreach (ChungTuNo item in ChungTuNoCollection)
                        {
                            item.TKCTID = this.ID;
                            if (item.ID == 0)
                                item.Insert(transaction);
                            else item.Update(transaction);
                        }
                    }
                    #endregion
                    #region Nguyên phụ liệu cung ứng
                    foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng nplCungUng in NPLCungUngs)
                    {
                        nplCungUng.TKCTID = this.ID;
                        if (nplCungUng.ID == 0)
                            nplCungUng.Insert(transaction);
                        else nplCungUng.Update(transaction);
                        foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail nplCungUngDetail in nplCungUng.NPLCungUngDetails)
                        {
                            nplCungUngDetail.Master_ID = nplCungUng.ID;
                            if (nplCungUngDetail.ID == 0)
                                nplCungUngDetail.Insert(transaction);
                            else nplCungUngDetail.Update(transaction);
                        }
                    }
                    #endregion
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = id;
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public void InsertUpdateFull(SqlTransaction transaction)
        {
            if (this.ID == 0)
                this.ID = this.Insert(transaction);
            else
                this.Update(transaction);
            int i = 0;
            foreach (HangChuyenTiep hctDetail in this.HCTCollection)
            {
                hctDetail.SoThuTuHang = ++i;
                if (hctDetail.ID == 0)
                {
                    hctDetail.Master_ID = this.ID;
                    hctDetail.ID = hctDetail.Insert(transaction);
                }
                else
                {
                    hctDetail.Update(transaction);
                }
            }
        }
        public void InsertUpdateToKhaiCTXuLyDuLieu(SqlTransaction transaction)
        {
            string LoaiHangHoa = "N";
            if (this.MaLoaiHinh.IndexOf("SP") > 0 || this.MaLoaiHinh.IndexOf("19") > 0)
            {
                if (this.MaLoaiHinh.Trim().EndsWith("X")
                    || this.MaLoaiHinh.Trim().Substring(0, 1).Equals("X")) //TODO: Hungtq update 22/02/2011
                    LoaiHangHoa = "S";
            }
            else if (this.MaLoaiHinh.IndexOf("TB") > 0 || this.MaLoaiHinh.IndexOf("20") > 0)
                LoaiHangHoa = "T";
            HangChuyenTiep HCTCu = new HangChuyenTiep();
            foreach (HangChuyenTiep hct in this.HCTCollection)
            {
                hct.UpdateSoLuong(transaction, this.IDHopDong, LoaiHangHoa, this.MaLoaiHinh, HCTCu);
            }
        }
        //tao moi to khai chuyen tiep doi xung
        public bool InsertUpdateFullCopy(ToKhaiChuyenTiep tkct)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.ID_Relation = tkct.ID;
                    if (this.ID == 0)
                        this.ID = this.Insert(transaction);
                    else
                        this.Update(transaction);
                    int i = 0;
                    tkct.ID_Relation = this.ID;
                    tkct.Update(transaction);
                    foreach (HangChuyenTiep hct in this.HCTCollection)
                    {
                        hct.SoThuTuHang = ++i;
                        if (hct.ID == 0)
                        {
                            hct.Master_ID = this.ID;
                            hct.ID = hct.Insert(transaction);
                        }
                        else
                        {
                            hct.Update(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //cap nhat to khai chuyen tiep doi xung
        public bool CapNhatFullCopy()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                        this.ID = this.Insert(transaction);
                    else
                        this.Update(transaction);
                    int i = 0;
                    //xoa het hang cu //cap nhat hang moi
                    HangChuyenTiep hangDel = new HangChuyenTiep();
                    hangDel.Master_ID = this.ID;
                    hangDel.DeleteBy_Master_ID(transaction);
                    foreach (HangChuyenTiep hct in this.HCTCollection)
                    {
                        hct.SoThuTuHang = ++i;
                        if (hct.ID == 0)
                        {
                            hct.Master_ID = this.ID;
                            hct.ID = hct.Insert(transaction);
                        }
                        else
                        {
                            hct.Update(transaction);
                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        //xoa to khai chuyen tiep doi xung
        public bool XoaFullCopy(ToKhaiChuyenTiep tkct)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    tkct.ID_Relation = 0;
                    tkct.Update(transaction);
                    this.Delete(transaction);
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #region Report
        public DataSet GetSoToKhaiGCCTNhapTheoHopDong(long IDHopDong, string maHaiQuanTN, string maDN)
        {
            string sql = " SELECT * " +
                       " from   t_KDT_GC_ToKhaiChuyenTiep " +
                       " where  IDHopDong = @IDHopDong AND MaHaiQuanTiepNhan =@MaHaiQuanTiepNhan "
                       + " AND MaDoanhNghiep =@MaDoanhNghiep "
                       + " AND TrangThaiXuLy =1";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            //db.AddInParameter(dbCommand, "@SoTK", SqlDbType.BigInt, soTK);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.Char, maHaiQuanTN);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDN);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        #endregion
        #region Web Services

        //-----------------------------------------------------------------------------------------
        private string ConfigPhongBi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuanTiepNhan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            this.GUIDSTR = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            this.Update();
            return doc.InnerXml;
        }
        private string ConfigPhongBiNew(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBiNew.xml");
            //XmlNode nodeSendApplication = doc.GetElementsByTagName("SendApplication")[0];
            XmlNode nodeCreateMessageIssue = doc.GetElementsByTagName("createMessageIssue")[0];
            nodeCreateMessageIssue.InnerText = DateTime.Now.ToString();

            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuanTiepNhan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            this.GUIDSTR = (System.Guid.NewGuid().ToString());
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            //nodeFrom.ChildNodes[0].InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DoiTac.GetName(this.MaDoanhNghiep);
            this.Update();
            return doc.InnerXml;
        }
        private string ConfigPhongBiPhanHoi(int type, int function)
        {
            XmlDocument doc = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            doc.Load(path + "\\B03GiaCong\\PhongBi.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(this.MaHaiQuanTiepNhan);
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = this.MaHaiQuanTiepNhan;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = type.ToString();

            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = function.ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GUIDSTR;

            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = (System.Guid.NewGuid().ToString());

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;

            return doc.InnerXml;
        }
        public string WSSendNhap(string pass)
        {
            XmlDocument doc = new XmlDocument();
            if (this.MaLoaiHinh.Trim().EndsWith("N") || this.MaLoaiHinh.Trim().Substring(0, 1).Equals("N"))
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiChuyenTiepNhap, (int)MessageFunctions.KhaiBao));
            else
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiChuyenTiepXuat, (int)MessageFunctions.KhaiBao));

            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXML());
            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";

        }


        public string WSSendNhapNew(string pass)
        {
            XmlDocument doc = new XmlDocument();
            if (this.MaLoaiHinh.Trim().EndsWith("N") || this.MaLoaiHinh.Trim().Substring(0, 1).Equals("N"))
                doc.LoadXml(ConfigPhongBiNew((int)MessgaseType.ToKhaiChuyenTiepNhap, (int)MessageFunctions.KhaiBao));
            else
                doc.LoadXml(ConfigPhongBiNew((int)MessgaseType.ToKhaiChuyenTiepXuat, (int)MessageFunctions.KhaiBao));

            XmlDocument docNPL = new XmlDocument();
            docNPL.LoadXml(ConvertCollectionToXML());
            //luu vao string
            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";

        }
        private string ConvertCollectionToXML()
        {
            if (this.HCTCollection == null || this.HCTCollection.Count == 0)
            {
                this.LoadHCTCollection();
            }

            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\B03GiaCong\\KhaiBaoToKhaiChuyenTiepNhapNguyenPhuLieu.XML");

            XmlNode nodeMaChuyenTiep = docNPL.SelectSingleNode("Root/DCTGC/Ma_CT");
            nodeMaChuyenTiep.InnerText = this.MaLoaiHinh;

            XmlNode nodeSoChuyenTiep = docNPL.SelectSingleNode("Root/DCTGC/So_CT");
            nodeSoChuyenTiep.InnerText = "0";

            XmlNode nodeNamChuyenTiep = docNPL.SelectSingleNode("Root/DCTGC/Nam_CT");
            nodeNamChuyenTiep.InnerText = DateTime.Now.Year.ToString();

            XmlNode nodeNgayChuyenTiep = docNPL.SelectSingleNode("Root/DCTGC/Ngay_CT");
            nodeNgayChuyenTiep.InnerText = "";

            XmlNode nodeDoanhNghiep = docNPL.SelectSingleNode("Root/DCTGC/DVGC");
            nodeDoanhNghiep.InnerText = this.MaDoanhNghiep;

            XmlNode nodeMaDoiTac = docNPL.SelectSingleNode("Root/DCTGC/Ma_DoiTac");
            nodeMaDoiTac.InnerText = this.MaKhachHang;

            XmlNode nodeTenDoiTac = docNPL.SelectSingleNode("Root/DCTGC/DoiTac");
            nodeTenDoiTac.InnerText = "";

            XmlNode nodeMaHaiQuan = docNPL.SelectSingleNode("Root/DCTGC/MA_HQ");
            nodeMaHaiQuan.InnerText = this.MaHaiQuanTiepNhan;

            XmlNode nodeSoHopDong = docNPL.SelectSingleNode("Root/DCTGC/So_HD");
            nodeSoHopDong.InnerText = this.SoHopDongDV;

            XmlNode nodeNgayKyHD = docNPL.SelectSingleNode("Root/DCTGC/Ngay_Ky");
            nodeNgayKyHD.InnerText = this.NgayHDDV.ToString("MM/dd/yyyy");

            XmlNode nodeSoHDGiao = docNPL.SelectSingleNode("Root/DCTGC/So_HDCh");
            nodeSoHDGiao.InnerText = this.SoHDKH;

            XmlNode nodeNgayKyHDChuyen = docNPL.SelectSingleNode("Root/DCTGC/Ngay_KyCh");
            nodeNgayKyHDChuyen.InnerText = this.NgayHDKH.ToString("MM/dd/yyyy");

            XmlNode nodeNguoiGiao = docNPL.SelectSingleNode("Root/DCTGC/NguoiGiao");
            if (this.NguoiChiDinhKH.Length <= 40)
                nodeNguoiGiao.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhKH);
            else
                nodeNguoiGiao.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhKH.Substring(0, 40));

            XmlNode nodeNgayGiao = docNPL.SelectSingleNode("Root/DCTGC/NgayGiao");
            nodeNgayGiao.InnerText = "";

            XmlNode nodeNguoiNhan = docNPL.SelectSingleNode("Root/DCTGC/NguoiNhan");
            if (this.NguoiChiDinhDV.Length <= 40)
                nodeNguoiNhan.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhDV);
            else
                nodeNguoiNhan.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhDV.Substring(0, 40));

            XmlNode nodeNgayNhan = docNPL.SelectSingleNode("Root/DCTGC/NgayNhan");
            nodeNgayNhan.InnerText = "";

            XmlNode nodeMa_HQKH = docNPL.SelectSingleNode("Root/DCTGC/Ma_HQKH");
            nodeMa_HQKH.InnerText = this.MaHaiQuanKH;

            XmlNode nodeNguyenTe = docNPL.SelectSingleNode("Root/DCTGC/Ma_NT");
            nodeNguyenTe.InnerText = this.NguyenTe_ID;

            XmlNode nodeTyGiaVND = docNPL.SelectSingleNode("Root/DCTGC/TyGiaVND");
            nodeTyGiaVND.InnerText = this.TyGiaVND.ToString(f);

            XmlNode nodeCTKT = docNPL.SelectSingleNode("Root/DCTGC/CTKT");
            if (this.ChungTu.Length <= 40)
                nodeCTKT.InnerText = FontConverter.Unicode2TCVN(this.ChungTu);
            else
                nodeCTKT.InnerText = FontConverter.Unicode2TCVN(this.ChungTu.Substring(0, 40));

            XmlNode nodeNguoiHQGiao = docNPL.SelectSingleNode("Root/DCTGC/NguoiHQGiao");
            if (this.MaLoaiHinh.Trim().EndsWith("N") || MaLoaiHinh.Substring(0, 1).Equals("N"))
            {
                if (this.NguoiChiDinhKH.Length <= 40)
                    nodeNguoiHQGiao.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhKH);
                else
                    nodeNguoiHQGiao.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhKH.Substring(0, 40));
            }
            else
            {
                if (this.DiaDiemXepHang.Length <= 40)
                    nodeNguoiHQGiao.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
                else
                    nodeNguoiHQGiao.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));
            }

            XmlNode nodeDiaDiemXepHang = docNPL.SelectSingleNode("Root/DCTGC/NguoiHQNhan");
            if (this.MaLoaiHinh.Trim().EndsWith("N") || MaLoaiHinh.Substring(0, 1).Equals("N"))
            {
                if (this.DiaDiemXepHang.Length <= 40)
                    nodeDiaDiemXepHang.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
                else
                    nodeDiaDiemXepHang.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));
            }
            else
                nodeDiaDiemXepHang.InnerText = "";
            XmlNode nodeHangCTS = docNPL.SelectSingleNode("Root/DHangCTGCs");

            foreach (HangChuyenTiep hct in this.HCTCollection)
            {
                XmlNode nodeHCT = docNPL.CreateElement("DHangCTGC");

                XmlNode nodeMaHang = docNPL.CreateElement("P_Code");
                if (this.MaLoaiHinh.IndexOf("SP") > 0 || this.MaLoaiHinh.IndexOf("19") > 0)
                {
                    if (this.MaLoaiHinh.Trim().EndsWith("N") || MaLoaiHinh.Substring(0, 1).Equals("N"))
                        nodeMaHang.InnerText = "N" + hct.MaHang;
                    else
                        nodeMaHang.InnerText = "S" + hct.MaHang;
                }
                else if (this.MaLoaiHinh.IndexOf("TB") > 0 || this.MaLoaiHinh.IndexOf("20") > 0)
                    nodeMaHang.InnerText = "T" + hct.MaHang;
                else
                    nodeMaHang.InnerText = "N" + hct.MaHang;
                XmlNode nodeMaHS = docNPL.CreateElement("HS_Code");
                nodeMaHS.InnerText = hct.MaHS;

                XmlNode nodeTenHang = docNPL.CreateElement("TenHang");
                nodeTenHang.InnerText = FontConverter.Unicode2TCVN(hct.TenHang);

                XmlNode nodeSoLuong = docNPL.CreateElement("SoLuong");
                nodeSoLuong.InnerText = hct.SoLuong.ToString(f); ;

                XmlNode nodeDVT_ID = docNPL.CreateElement("Ma_DVT");
                nodeDVT_ID.InnerText = hct.ID_DVT;

                XmlNode nodeGhiChu = docNPL.CreateElement("GhiChu");
                nodeGhiChu.InnerText = "";

                XmlNode nodeDonGia = docNPL.CreateElement("DonGia");
                nodeDonGia.InnerText = hct.DonGia.ToString(f);

                XmlNode nodeTriGia = docNPL.CreateElement("TriGia");
                nodeTriGia.InnerText = hct.TriGia.ToString(f); ;

                XmlNode nodeTriGia_VND = docNPL.CreateElement("TriGia_VND");
                nodeTriGia_VND.InnerText = "";

                XmlNode nodeNuocXX = docNPL.CreateElement("NuocXX");
                if (this.MaLoaiHinh.Trim().EndsWith("N") || MaLoaiHinh.Substring(0, 1).Equals("N"))
                    nodeNuocXX.InnerText = hct.ID_NuocXX;
                else
                    nodeNuocXX.InnerText = hct.ID_NuocXX;
                nodeHCT.AppendChild(nodeMaHang);
                nodeHCT.AppendChild(nodeMaHS);
                nodeHCT.AppendChild(nodeTenHang);
                nodeHCT.AppendChild(nodeSoLuong);
                nodeHCT.AppendChild(nodeDVT_ID);
                nodeHCT.AppendChild(nodeGhiChu);
                nodeHCT.AppendChild(nodeDonGia);
                nodeHCT.AppendChild(nodeTriGia);
                nodeHCT.AppendChild(nodeTriGia_VND);
                nodeHCT.AppendChild(nodeNuocXX);
                nodeHangCTS.AppendChild(nodeHCT);
            }
            return docNPL.InnerXml;
        }
        private string ConvertCollectionToXMLNew()
        {
            if (this.HCTCollection == null || this.HCTCollection.Count == 0)
            {
                this.LoadHCTCollection();
            }

            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\B03GiaCong\\KhaiBaoToKhaiChuyenTiepNhapNguyenPhuLieuEN.XML");

            XmlNode nodeIssue = docNPL.SelectSingleNode("Declaration/issue");
            nodeIssue.InnerText = DateTime.Now.ToShortDateString();

            XmlNode nodeReference = docNPL.SelectSingleNode("Declaration/reference");
            nodeIssue.InnerText = this.GUIDSTR;

            XmlNode nodeDeclarationOffice = docNPL.SelectSingleNode("Declaration/declarationOffice");
            nodeDeclarationOffice.InnerText = this.MaHaiQuanTiepNhan;

            XmlNode nodeNatureOfTransaction = docNPL.SelectSingleNode("Declaration/natureOfTransaction");
            nodeNatureOfTransaction.InnerText = this.MaLoaiHinh;

            XmlNode nodeMethod = docNPL.SelectSingleNode("Declaration/Payment/method");
            //nodeMethod.InnerText = ;

            #region cũ
            XmlNode nodeMaChuyenTiep = docNPL.SelectSingleNode("Root/DCTGC/Ma_CT");
            nodeMaChuyenTiep.InnerText = this.MaLoaiHinh;

            XmlNode nodeSoChuyenTiep = docNPL.SelectSingleNode("Root/DCTGC/So_CT");
            nodeSoChuyenTiep.InnerText = "0";

            XmlNode nodeNamChuyenTiep = docNPL.SelectSingleNode("Root/DCTGC/Nam_CT");
            nodeNamChuyenTiep.InnerText = DateTime.Now.Year.ToString();

            XmlNode nodeNgayChuyenTiep = docNPL.SelectSingleNode("Root/DCTGC/Ngay_CT");
            nodeNgayChuyenTiep.InnerText = "";

            XmlNode nodeDoanhNghiep = docNPL.SelectSingleNode("Root/DCTGC/DVGC");
            nodeDoanhNghiep.InnerText = this.MaDoanhNghiep;

            XmlNode nodeMaDoiTac = docNPL.SelectSingleNode("Root/DCTGC/Ma_DoiTac");
            nodeMaDoiTac.InnerText = this.MaKhachHang;

            XmlNode nodeTenDoiTac = docNPL.SelectSingleNode("Root/DCTGC/DoiTac");
            nodeTenDoiTac.InnerText = "";

            XmlNode nodeMaHaiQuan = docNPL.SelectSingleNode("Root/DCTGC/MA_HQ");
            nodeMaHaiQuan.InnerText = this.MaHaiQuanTiepNhan;

            XmlNode nodeSoHopDong = docNPL.SelectSingleNode("Root/DCTGC/So_HD");
            nodeSoHopDong.InnerText = this.SoHopDongDV;

            XmlNode nodeNgayKyHD = docNPL.SelectSingleNode("Root/DCTGC/Ngay_Ky");
            nodeNgayKyHD.InnerText = this.NgayHDDV.ToString("MM/dd/yyyy");

            XmlNode nodeSoHDGiao = docNPL.SelectSingleNode("Root/DCTGC/So_HDCh");
            nodeSoHDGiao.InnerText = this.SoHDKH;

            XmlNode nodeNgayKyHDChuyen = docNPL.SelectSingleNode("Root/DCTGC/Ngay_KyCh");
            nodeNgayKyHDChuyen.InnerText = this.NgayHDKH.ToString("MM/dd/yyyy");

            XmlNode nodeNguoiGiao = docNPL.SelectSingleNode("Root/DCTGC/NguoiGiao");
            if (this.NguoiChiDinhKH.Length <= 40)
                nodeNguoiGiao.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhKH);
            else
                nodeNguoiGiao.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhKH.Substring(0, 40));

            XmlNode nodeNgayGiao = docNPL.SelectSingleNode("Root/DCTGC/NgayGiao");
            nodeNgayGiao.InnerText = "";

            XmlNode nodeNguoiNhan = docNPL.SelectSingleNode("Root/DCTGC/NguoiNhan");
            if (this.NguoiChiDinhDV.Length <= 40)
                nodeNguoiNhan.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhDV);
            else
                nodeNguoiNhan.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhDV.Substring(0, 40));

            XmlNode nodeNgayNhan = docNPL.SelectSingleNode("Root/DCTGC/NgayNhan");
            nodeNgayNhan.InnerText = "";

            XmlNode nodeMa_HQKH = docNPL.SelectSingleNode("Root/DCTGC/Ma_HQKH");
            nodeMa_HQKH.InnerText = this.MaHaiQuanKH;

            XmlNode nodeNguyenTe = docNPL.SelectSingleNode("Root/DCTGC/Ma_NT");
            nodeNguyenTe.InnerText = this.NguyenTe_ID;

            XmlNode nodeTyGiaVND = docNPL.SelectSingleNode("Root/DCTGC/TyGiaVND");
            nodeTyGiaVND.InnerText = this.TyGiaVND.ToString(f);

            XmlNode nodeCTKT = docNPL.SelectSingleNode("Root/DCTGC/CTKT");
            if (this.ChungTu.Length <= 40)
                nodeCTKT.InnerText = FontConverter.Unicode2TCVN(this.ChungTu);
            else
                nodeCTKT.InnerText = FontConverter.Unicode2TCVN(this.ChungTu.Substring(0, 40));

            XmlNode nodeNguoiHQGiao = docNPL.SelectSingleNode("Root/DCTGC/NguoiHQGiao");
            if (this.MaLoaiHinh.Trim().EndsWith("N") || MaLoaiHinh.Substring(0, 1).Equals("N"))
            {
                if (this.NguoiChiDinhKH.Length <= 40)
                    nodeNguoiHQGiao.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhKH);
                else
                    nodeNguoiHQGiao.InnerText = FontConverter.Unicode2TCVN(this.NguoiChiDinhKH.Substring(0, 40));
            }
            else
            {
                if (this.DiaDiemXepHang.Length <= 40)
                    nodeNguoiHQGiao.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
                else
                    nodeNguoiHQGiao.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));
            }

            XmlNode nodeDiaDiemXepHang = docNPL.SelectSingleNode("Root/DCTGC/NguoiHQNhan");
            if (this.MaLoaiHinh.Trim().EndsWith("N") || MaLoaiHinh.Substring(0, 1).Equals("N"))
            {
                if (this.DiaDiemXepHang.Length <= 40)
                    nodeDiaDiemXepHang.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang);
                else
                    nodeDiaDiemXepHang.InnerText = FontConverter.Unicode2TCVN(this.DiaDiemXepHang.Substring(0, 40));
            }
            else
                nodeDiaDiemXepHang.InnerText = "";
            XmlNode nodeHangCTS = docNPL.SelectSingleNode("Root/DHangCTGCs");

            foreach (HangChuyenTiep hct in this.HCTCollection)
            {
                XmlNode nodeHCT = docNPL.CreateElement("DHangCTGC");

                XmlNode nodeMaHang = docNPL.CreateElement("P_Code");
                if (this.MaLoaiHinh.IndexOf("SP") > 0 || this.MaLoaiHinh.IndexOf("19") > 0)
                {
                    if (this.MaLoaiHinh.Trim().EndsWith("N") || MaLoaiHinh.Substring(0, 1).Equals("N"))
                        nodeMaHang.InnerText = "N" + hct.MaHang;
                    else
                        nodeMaHang.InnerText = "S" + hct.MaHang;
                }
                else if (this.MaLoaiHinh.IndexOf("TB") > 0 || this.MaLoaiHinh.IndexOf("20") > 0)
                    nodeMaHang.InnerText = "T" + hct.MaHang;
                else
                    nodeMaHang.InnerText = "N" + hct.MaHang;
                XmlNode nodeMaHS = docNPL.CreateElement("HS_Code");
                nodeMaHS.InnerText = hct.MaHS;

                XmlNode nodeTenHang = docNPL.CreateElement("TenHang");
                nodeTenHang.InnerText = FontConverter.Unicode2TCVN(hct.TenHang);

                XmlNode nodeSoLuong = docNPL.CreateElement("SoLuong");
                nodeSoLuong.InnerText = hct.SoLuong.ToString(f); ;

                XmlNode nodeDVT_ID = docNPL.CreateElement("Ma_DVT");
                nodeDVT_ID.InnerText = hct.ID_DVT;

                XmlNode nodeGhiChu = docNPL.CreateElement("GhiChu");
                nodeGhiChu.InnerText = "";

                XmlNode nodeDonGia = docNPL.CreateElement("DonGia");
                nodeDonGia.InnerText = hct.DonGia.ToString(f);

                XmlNode nodeTriGia = docNPL.CreateElement("TriGia");
                nodeTriGia.InnerText = hct.TriGia.ToString(f); ;

                XmlNode nodeTriGia_VND = docNPL.CreateElement("TriGia_VND");
                nodeTriGia_VND.InnerText = "";

                XmlNode nodeNuocXX = docNPL.CreateElement("NuocXX");
                if (this.MaLoaiHinh.Trim().EndsWith("N") || MaLoaiHinh.Substring(0, 1).Equals("N"))
                    nodeNuocXX.InnerText = hct.ID_NuocXX;
                else
                    nodeNuocXX.InnerText = hct.ID_NuocXX;
                nodeHCT.AppendChild(nodeMaHang);
                nodeHCT.AppendChild(nodeMaHS);
                nodeHCT.AppendChild(nodeTenHang);
                nodeHCT.AppendChild(nodeSoLuong);
                nodeHCT.AppendChild(nodeDVT_ID);
                nodeHCT.AppendChild(nodeGhiChu);
                nodeHCT.AppendChild(nodeDonGia);
                nodeHCT.AppendChild(nodeTriGia);
                nodeHCT.AppendChild(nodeTriGia_VND);
                nodeHCT.AppendChild(nodeNuocXX);
                nodeHangCTS.AppendChild(nodeHCT);
            #endregion

            }
            return docNPL.InnerXml;
        }

        #region Huy khai báo tờ khai

        public string WSCancel(string pass)
        {
            XmlDocument doc = new XmlDocument();
            if (this.MaLoaiHinh.StartsWith("N") || this.MaLoaiHinh.Substring(0, 1).Equals("N"))
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiChuyenTiepNhap, (int)MessageFunctions.HuyKhaiBao));
            else
                doc.LoadXml(ConfigPhongBi((int)MessgaseType.ToKhaiChuyenTiepXuat, (int)MessageFunctions.HuyKhaiBao));
            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\B03GiaCong\\HuyTKCT.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode("Root"), true);
            root.InnerText = this.SoTiepNhan.ToString();
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            string msgError = string.Empty;
            Company.KDT.SHARE.Components.MessageTypes msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiChuyenTiepNhap;
            if (MaLoaiHinh.Substring(0, 1).Equals("X"))
                msgType = Company.KDT.SHARE.Components.MessageTypes.ToKhaiChuyenTiepXuat;
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
                Company.KDT.SHARE.Components.Globals.SaveMessage(doc.InnerXml, ID,
                    Company.KDT.SHARE.Components.MessageTitle.HuyKhaiBaoToKhaiCT);

                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        return doc.InnerXml;
                    }
                }
                else
                {

                    msgError = "Hải quan trả về " + FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText);
                    Company.KDT.SHARE.Components.Globals.SaveMessage(
                        string.Empty, ID, GUIDSTR, msgType
                         ,
                         Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                         Company.KDT.SHARE.Components.MessageTitle.ChuyenTrangThaiTay);
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    throw ex;
                if (!string.IsNullOrEmpty(kq))
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                Logger.LocalLogger.Instance().WriteMessage(new Exception(doc.InnerXml));
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            return "";

        }



        #endregion Huy khai báo tờ khai

        #region Lấy phản hồi trạng thái

        public string WSRequest(string pass)
        {
            XmlDocument doc = new XmlDocument();
            if (this.MaLoaiHinh.StartsWith("N") || this.MaLoaiHinh.Substring(0, 1).Equals("N"))
                doc.LoadXml(ConfigPhongBiPhanHoi((int)MessgaseType.ToKhaiChuyenTiepNhap, (int)MessageFunctions.HoiTrangThai));
            else
                doc.LoadXml(ConfigPhongBiPhanHoi((int)MessgaseType.ToKhaiChuyenTiepXuat, (int)MessageFunctions.HoiTrangThai));

            XmlDocument docNPL = new XmlDocument();
            string path = EntityBase.GetPathProgram();
            docNPL.Load(path + "\\XMLChungTu\\LayPhanHoiDaDuyet.xml");

            XmlNode root = doc.ImportNode(docNPL.SelectSingleNode(@"Envelope/Body/Content/Root"), true);

            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = this.MaDoanhNghiep;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = this.MaHaiQuanTiepNhan;
            root.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = FontConverter.Unicode2TCVN(Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(MaHaiQuanTiepNhan));

            root.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            // root.InnerText = this.SoTiepNhan.ToString();
            XmlNode Content = doc.GetElementsByTagName("Content")[0];
            Content.AppendChild(root);

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, pass);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
            }
            return "";

        }

        #endregion Lấy phản hồi

        public string LayPhanHoi(string pass, string xml)
        {

            Company.KDT.SHARE.Components.WS.KDTService kdt = Company.KDT.SHARE.Components.WebService.GetWS();
            XmlDocument docNPL = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            int function = Convert.ToInt32(doc.GetElementsByTagName("function")[0].InnerText);
            doc.GetElementsByTagName("function")[0].InnerText = "13";
            doc.GetElementsByTagName("messageId")[0].InnerText = (System.Guid.NewGuid().ToString().ToUpper());

            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                string msgError = string.Empty;
                try
                {
                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, pass);
                    docNPL = new XmlDocument();
                    docNPL.LoadXml(kq);

                    Result = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/statement");
                    if (Result.InnerText != "27")
                    {
                        if (Result.InnerText == "12")
                            continue;
                        else
                            break;
                    }
                    else
                    {
                        foreach (XmlNode node in docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").ChildNodes)
                        {
                            msgError += node.InnerText + "; \n";
                        }

                        throw new Exception(msgError + "|" + "");
                    }
                }
                catch (Exception ex)
                {
                    if (msgError != string.Empty)
                    {
                        Company.KDT.SHARE.Components.Globals.SaveMessage(kq, ID,
                            Company.KDT.SHARE.Components.MessageTitle.Error, msgError);
                        throw ex;
                    }
                    else Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
            }

            if (i > 1)
                return doc.InnerXml;
            bool ok = false;

            try
            {
                XmlNode contentNode = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content");
                XmlNode issue = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/issue");
                XmlNode functionNode = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/function");
                XmlNode referenceNode = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/reference");
                if (functionNode.InnerText == "29")// Cấp STN
                {

                    string[] contentStr = contentNode.InnerText.Split('/');
                    this.SoTiepNhan = Convert.ToInt64(contentStr[0]);
                    this.NamDK = short.Parse(contentStr[1]);
                    this.NgayTiepNhan = Convert.ToDateTime(issue.InnerText);
                    this.TrangThaiXuLy = 0;
                    ok = true;
                    string sfmtMsg = string.Format("Số tiếp nhận {0},Ngày tiếp nhận {1},Năm đăng ký {2}", SoTiepNhan, NgayTiepNhan, NamDK);
                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, ID,
                            Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSoTiepNhan, sfmtMsg);

                }
                else if (functionNode.InnerText == "30")// Cấp số tờ khai
                {
                    string[] contentStr = contentNode.InnerText.Split('/');
                    this.SoToKhai = Convert.ToInt64(contentStr[0]);
                    this.NgayDangKy = Convert.ToDateTime(issue.InnerText);
                    this.TrangThaiXuLy = 1;

                    string sfmtMsg = string.Format("Số tờ khai {0},Ngày đăng ký {1}", SoToKhai, NgayDangKy);
                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, ID,
                            Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSo, sfmtMsg);
                    TransferToNPLTon();
                    ok = true;


                }
                else if (functionNode.InnerText == "31")// Duyệt luồng chứng từ
                {
                    string[] contentStr = contentNode.InnerText.Split('/');
                    if (contentStr[0].ToUpper().Equals("LUỒNG VÀNG"))
                        this.PhanLuong = TrangThaiPhanLuong.LUONG_VANG;
                    else if (contentStr[0].ToUpper().Equals("LUỒNG ĐỎ"))
                        this.PhanLuong = TrangThaiPhanLuong.LUONG_DO;
                    else
                        this.PhanLuong = TrangThaiPhanLuong.LUONG_XANH;
                    this.HUONGDAN = contentStr[1];

                    string sfmtMsg = string.Format("Tờ khai được phân luồng {0}, Hướng dẫn :{1} ", this.PhanLuong, this.HUONGDAN);
                    Company.KDT.SHARE.Components.Globals.SaveMessage(kq, ID,
                          Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocPhanLuong, sfmtMsg);
                    this.Update();

                }
                else if (functionNode.InnerText == "32")// Thông báo thông quan
                {

                }
                else if (functionNode.InnerText == "33")// Thông báo thực xuất
                {

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            if (ok)
                this.Update();

            return "";

        }
        public void TransferToNPLTon()
        {
            if (this.HCTCollection.Count == 0)
                this.LoadHCTCollection();
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.Update(transaction);
                    if (this.MaLoaiHinh.StartsWith("PHPLN") || this.MaLoaiHinh.StartsWith("PHSPN") || this.MaLoaiHinh.StartsWith("NGC18") || this.MaLoaiHinh.StartsWith("NGC19"))
                    {
                        foreach (HangChuyenTiep hmd in this.HCTCollection)
                        {

                            //CAP NHAT VAO LUONG TON THUC TE
                            NPLNhapTonThucTe nplNhapTonThucTe = new NPLNhapTonThucTe();
                            nplNhapTonThucTe.SoToKhai = Convert.ToInt32(this.SoToKhai);
                            nplNhapTonThucTe.MaLoaiHinh = this.MaLoaiHinh;
                            nplNhapTonThucTe.NamDangKy = (short)this.NgayDangKy.Year;
                            nplNhapTonThucTe.MaHaiQuan = this.MaHaiQuanTiepNhan;
                            nplNhapTonThucTe.MaNPL = hmd.MaHang;
                            nplNhapTonThucTe.MaDoanhNghiep = this.MaDoanhNghiep;
                            nplNhapTonThucTe.Luong = hmd.SoLuong;
                            nplNhapTonThucTe.Ton = hmd.SoLuong;
                            nplNhapTonThucTe.NgayDangKy = this.NgayDangKy;
                            nplNhapTonThucTe.ID_HopDong = this.IDHopDong;
                            nplNhapTonThucTe.Insert(transaction);
                        }
                    }
                    #region Đã kiểm tra bên trên
                    //if (this.MaLoaiHinh.StartsWith("NGC18") || this.MaLoaiHinh.StartsWith("NGC19"))
                    //{
                    //    foreach (HangChuyenTiep hmd in this.HCTCollection)
                    //    {

                    //        //CAP NHAT VAO LUONG TON THUC TE
                    //        NPLNhapTonThucTe nplNhapTonThucTe = new NPLNhapTonThucTe();
                    //        nplNhapTonThucTe.SoToKhai = Convert.ToInt32(this.SoToKhai);
                    //        nplNhapTonThucTe.MaLoaiHinh = this.MaLoaiHinh;
                    //        nplNhapTonThucTe.NamDangKy = (short)this.NgayDangKy.Year;
                    //        nplNhapTonThucTe.MaHaiQuan = this.MaHaiQuanTiepNhan;
                    //        nplNhapTonThucTe.MaNPL = hmd.MaHang;
                    //        nplNhapTonThucTe.MaDoanhNghiep = this.MaDoanhNghiep;
                    //        nplNhapTonThucTe.Luong = hmd.SoLuong;
                    //        nplNhapTonThucTe.Ton = hmd.SoLuong;
                    //        nplNhapTonThucTe.NgayDangKy = this.NgayDangKy;
                    //        nplNhapTonThucTe.ID_HopDong = this.IDHopDong;
                    //        nplNhapTonThucTe.Insert(transaction);
                    //    }
                    //}
                    #endregion
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
        private void TranferDataGC()
        {

        }

        #endregion

        public bool InsertDongBoDuLieu()
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    long ID = this.Insert(transaction);
                    int i = 0;
                    foreach (HangChuyenTiep hct in this.HCTCollection)
                    {
                        hct.SoThuTuHang = ++i;
                        hct.Master_ID = ID;
                        hct.ID = hct.Insert(transaction);
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool CheckExistToKhaiChuyenTiep(string MaHQ, string MaLH, long SoToKhai, DateTime NamDK)
        {
            string sql = "select * from t_KDT_GC_ToKhaiChuyenTiep where SoToKhai=" + SoToKhai + " and year(NgayDangKy) =@NamDK and MaHaiQuanTiepNhan='" + MaHQ + "' and MaLoaiHinh='" + MaLH + "'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@NamDK", SqlDbType.SmallInt, NamDK.Year);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null)
                return false;
            else return true;
        }

        public DataSet GetHangChuyenTiepNPL(long IDHopDong)
        {
            string sql = "Select * from t_View_KDT_GC_HCT where IDHopDong = @IDHopDong AND ( MaLoaiHinh = 'PHPLX' OR MaLoaiHinh = 'XGC18') AND TrangThaiXuLy = 1";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }
        public void LoadPhanBo()
        {
            PhanBoToKhaiXuatCollection = (List<PhanBoToKhaiXuat>)PhanBoToKhaiXuat.SelectPhanBoToKhaiXuatOfTKCT(this.ID);
        }
        public void LoadPhanBo(SqlTransaction transaction)
        {
            PhanBoToKhaiXuatCollection = (List<PhanBoToKhaiXuat>)PhanBoToKhaiXuat.SelectPhanBoToKhaiXuatOfTKCT(this.ID, transaction);
        }
        public void DeletePhanBo()
        {
            this.LoadPhanBo();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    string sql = "select * from  v_GC_ToKhaiXuatDaPhanBo WHERE STT > (SELECT STT FROM v_GC_ToKhaiXuatDaPhanBo WHERE ID = " + this.ID + ") AND IDHopDong =" + this.IDHopDong;
                    SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

                    DataSet ds = db.ExecuteDataSet(dbCommand, transaction);
                    foreach (PhanBoToKhaiXuat item in this.PhanBoToKhaiXuatCollection)
                    {
                        item.DeletePhanBo(transaction);
                    }
                    this.Update(transaction);
                    UpdateTrangThaiChuaPhanBo(transaction, ds);
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }

        public void UpdateTrangThaiChuaPhanBo(SqlTransaction transaction, DataSet ds)
        {


            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string maLoaiHinh = row["MaLoaiHinh"].ToString();
                int id = Convert.ToInt32(row["ID"]);
                if (maLoaiHinh.Contains("XGC"))
                {
                    ToKhaiMauDich TKMD = new ToKhaiMauDich();
                    TKMD.ID = id;
                    TKMD.Load(transaction);
                    bool ok = true;
                    if (TKMD.NgayDangKy == this.NgayDangKy)
                    {
                        if (TKMD.SoToKhai >= this.SoToKhai)
                        {
                            if (TKMD.SoToKhai == this.SoToKhai)
                            {
                                if (TKMD.MaLoaiHinh.CompareTo(this.MaLoaiHinh) > 0)
                                    ok = true;
                                else
                                    ok = false;
                            }
                            else
                                ok = true;
                        }
                        else
                            ok = false;
                    }
                    if (ok)
                    {

                        TKMD.LoadPhanBo(transaction);
                        TKMD.DeletePhanBo(transaction);
                        TKMD.TrangThaiPhanBo = 0;
                        TKMD.UpdateTransaction(transaction);
                    }
                }
                else
                {
                    ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                    TKCT.ID = id;
                    TKCT.Load(transaction);
                    bool ok = true;
                    if (TKCT.NgayDangKy == this.NgayDangKy)
                    {
                        if (TKCT.SoToKhai >= this.SoToKhai)
                        {
                            if (TKCT.SoToKhai == this.SoToKhai)
                            {
                                if (TKCT.MaLoaiHinh.CompareTo(this.MaLoaiHinh) > 0)
                                    ok = true;
                                else
                                    ok = false;
                            }
                            else
                                ok = true;
                        }
                        else
                            ok = false;
                    }
                    if (ok)
                    {

                        TKCT.LoadPhanBo(transaction);
                        TKCT.DeletePhanBo(transaction);
                        //TKCT.Update(transaction);
                    }
                }
            }
        }
        public void DeletePhanBo(SqlTransaction transaction)
        {
            if (this.MaLoaiHinh.StartsWith("PH"))
            {
                foreach (PhanBoToKhaiXuat item in this.PhanBoToKhaiXuatCollection)
                {
                    item.DeletePhanBo(transaction);
                }
            }
        }
        public decimal SelectDonGia(string maNPL)
        {
            string sql = "SELECT DonGia as DonGiaKB FROM v_GC_HangChuyenTiep WHERE MaHang = '" + maNPL + "' AND SoToKhai = " + this.SoToKhai +
                         " AND MaLoaiHinh = '" + this.MaLoaiHinh + "' AND NgayDangKy = @NgayDangKy";
            DbCommand command = db.GetSqlStringCommand(sql);
            db.AddInParameter(command, "@NgayDangKy", DbType.DateTime, this.NgayDangKy);
            return Convert.ToDecimal(db.ExecuteScalar(command));
        }
        public DataSet GetNPLCungUngTKCT(long TKCT_ID)
        {
            string sql = "SELECT * FROM t_View_KDT_NPLCungUng WHERE TKCT_ID = " + TKCT_ID;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            return db.ExecuteDataSet(dbCommand);
        }
        #region DongBoDuLieuPhongKhai

        public static void DongBoDuLieuKhaiDTByIDTKCTDK(ToKhaiChuyenTiep tkct, HopDong HD, string NameDatabase)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase(NameDatabase);
            string sql = "select * from DHangCTGC  where Ma_CT=@Ma_CT and CTID=@CTID and Nam_CT=@Nam_CT and Ma_HQ=@Ma_HQ";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@CTID", SqlDbType.Int, tkct.SoTiepNhan);
            db.AddInParameter(dbCommand, "@Ma_HQ", SqlDbType.VarChar, tkct.MaHaiQuanTiepNhan);
            db.AddInParameter(dbCommand, "@Nam_CT", SqlDbType.Int, tkct.NgayTiepNhan.Year);
            db.AddInParameter(dbCommand, "@Ma_CT", SqlDbType.Char, tkct.MaLoaiHinh);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            tkct.HCTCollection = new List<HangChuyenTiep>();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                HangChuyenTiep hangCT = new HangChuyenTiep();
                hangCT.DonGia = Convert.ToDecimal(row["DonGia"]);
                hangCT.ID_DVT = row["Ma_DVT"].ToString();
                hangCT.ID_NuocXX = row["NuocXX"].ToString();
                hangCT.MaHang = row["P_Code"].ToString().Substring(1).Trim();
                hangCT.MaHS = row["HS_Code"].ToString().Trim();
                hangCT.SoLuong = Convert.ToDecimal(row["SoLuong"]);
                hangCT.TenHang = FontConverter.TCVN2Unicode(row["TenHang"].ToString().Trim());
                hangCT.TriGia = Convert.ToDecimal(row["TriGia"]);
                tkct.HCTCollection.Add(hangCT);
            }
            try
            {
                tkct.InsertUpdateFull();
            }
            catch { }
        }


        public static void DongBoDuLieuKhaiDT(string MaDoanhNghiep, string MaHaiQuan, string NameDatabase)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlDelete = "delete t_KDT_GC_ToKhaiChuyenTiep where MaDoanhNghiep=@DVGC and TrangThaiXuLy!=-1 and MaHaiQuanTiepNhan=@Ma_HQHD";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlDelete);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);

            db.ExecuteNonQuery(dbCommandDelete);

            db = (SqlDatabase)DatabaseFactory.CreateDatabase(NameDatabase);
            string sql = "select *  from DCTGC  where DVGC=@DVGC and Ma_HQ=@Ma_HQHD and Nam_CT=year(getdate())";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                tkct.ChungTu = FontConverter.TCVN2Unicode(row["CTKT"].ToString());
                if (row["LePhiHQ"].ToString() != "")
                    tkct.LePhiHQ = Convert.ToDecimal(row["LePhiHQ"].ToString());
                tkct.MaDoanhNghiep = MaDoanhNghiep;
                tkct.MaLoaiHinh = row["Ma_CT"].ToString().Trim();
                string sohopdong = "";
                sohopdong = row["So_HD"].ToString().Trim();
                tkct.SoHopDongDV = row["So_HD"].ToString().Trim();
                tkct.SoHDKH = row["So_HDCh"].ToString().Trim();
                if (tkct.MaLoaiHinh.EndsWith("N") || tkct.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    tkct.DiaDiemXepHang = FontConverter.TCVN2Unicode(row["NguoiHQNhan"].ToString());

                }
                else
                {
                    tkct.DiaDiemXepHang = FontConverter.TCVN2Unicode(row["NguoiHQGiao"].ToString());
                }

                tkct.MaHaiQuanKH = row["Ma_HQKH"].ToString();
                tkct.MaHaiQuanTiepNhan = MaHaiQuan;
                tkct.MaKhachHang = row["Ma_DoiTac"].ToString();
                tkct.NgayDangKy = Convert.ToDateTime(row["Ngay_CT"]);

                HopDong hd = new HopDong();
                hd.ID = (new HopDong()).GetIDHopDongExit(sohopdong, MaHaiQuan, MaDoanhNghiep, Convert.ToDateTime(row["Ngay_Ky"]));
                hd = HopDong.Load(hd.ID);
                if (hd.ID > 0)
                {
                    tkct.IDHopDong = hd.ID;
                    tkct.NgayHDDV = Convert.ToDateTime(row["Ngay_Ky"].ToString());
                    tkct.NgayHDKH = Convert.ToDateTime(row["Ngay_KyCh"].ToString());
                    tkct.NgayHetHanHDDV = hd.NgayHetHan;
                    tkct.NguoiChiDinhDV = FontConverter.TCVN2Unicode(row["NguoiNhan"].ToString());
                    tkct.NguoiChiDinhKH = FontConverter.TCVN2Unicode(row["NguoiGiao"].ToString());
                    tkct.NgayTiepNhan = tkct.NgayDangKy;
                    tkct.NguyenTe_ID = row["Ma_NT"].ToString();
                    tkct.SoTiepNhan = Convert.ToInt64(row["CTID"].ToString());
                    if (row["SoCTDuocCap"].ToString() != "")
                        tkct.SoToKhai = Convert.ToInt32(row["SoCTDuocCap"].ToString());
                    if (row["TrangThai"].ToString() == "2")
                        tkct.TrangThaiXuLy = 1;
                    else
                        tkct.TrangThaiXuLy = 0;
                    if (row["TyGiaVND"].ToString() != "")
                        tkct.TyGiaVND = Convert.ToDecimal(row["TyGiaVND"].ToString());
                    tkct.ID = tkct.GetIDToKhaiBySoTiepNhanAndNamTiepNhanAndMaHaiQuan(tkct.SoTiepNhan, (short)tkct.NgayDangKy.Year, tkct.MaHaiQuanTiepNhan);
                    if (tkct.TrangThaiXuLy == 0)
                    {
                        HangChuyenTiep HCT = new HangChuyenTiep();
                        HCT.Master_ID = tkct.ID;
                        HCT.DeleteBy_Master_ID(null);
                        DongBoDuLieuKhaiDTByIDTKCTDK(tkct, hd, NameDatabase);
                    }
                    else
                    {
                        if (tkct.ID == 0)
                            DongBoDuLieuKhaiDTByIDTKCTDK(tkct, hd, NameDatabase);
                    }
                }
            }

        }
        public long GetIDToKhaiBySoTiepNhanAndNamTiepNhanAndMaHaiQuan(long SoTiepNhan, short NamTN, string MaHQ)
        {
            string sql = "select ID from t_KDT_GC_ToKhaiChuyenTiep where SoTiepNhan=@SoTiepNhan and MaHaiQuanTiepNhan=@MaHaiQuanTiepNhan and year(NgayTiepNhan)=@NamTN";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@MaHaiQuanTiepNhan", SqlDbType.VarChar, MaHQ);
            db.AddInParameter(dbCommand, "@NamTN", SqlDbType.SmallInt, NamTN);

            object o = db.ExecuteScalar(dbCommand);
            return Convert.ToInt64(o);
        }
        public static ToKhaiChuyenTiep DongBoDuLieuKhaiDT(string MaDoanhNghiep, string MaHaiQuan, string nameConnectKDT, long SoTiepNhan, string MaLH, short NamDK)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string sqlGet = "select id from  t_KDT_GC_ToKhaiChuyenTiep where MaDoanhNghiep=@DVGC and MaHaiQuanTiepNhan=@Ma_HQHD and SoTiepNhan=@SoTiepNhan and MaLoaiHinh=@MaLoaiHinh and year(NgayTiepNhan)=@NamDK";
            SqlCommand dbCommandDelete = (SqlCommand)db.GetSqlStringCommand(sqlGet);
            db.AddInParameter(dbCommandDelete, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommandDelete, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            db.AddInParameter(dbCommandDelete, "@SoTiepNhan", SqlDbType.Int, SoTiepNhan);
            db.AddInParameter(dbCommandDelete, "@MaLoaiHinh", SqlDbType.VarChar, MaLH);
            db.AddInParameter(dbCommandDelete, "@NamDK", SqlDbType.SmallInt, NamDK);
            object o = db.ExecuteScalar(dbCommandDelete);
            if (o != null)
            {
                ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
                TKCT = ToKhaiChuyenTiep.Load(Convert.ToInt64(o));
                return TKCT;
            }

            db = (SqlDatabase)DatabaseFactory.CreateDatabase(nameConnectKDT);
            string sql = "select *  from DCTGC  where DVGC=@DVGC and Ma_HQ=@Ma_HQHD and  CTID=@CTID";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@DVGC", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@Ma_HQHD", SqlDbType.VarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@CTID", SqlDbType.Int, SoTiepNhan);
            DataSet ds = db.ExecuteDataSet(dbCommand);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                tkct.ChungTu = FontConverter.TCVN2Unicode(row["CTKT"].ToString());
                if (row["LePhiHQ"].ToString() != "")
                    tkct.LePhiHQ = Convert.ToDecimal(row["LePhiHQ"].ToString());
                tkct.MaDoanhNghiep = MaDoanhNghiep;
                tkct.MaLoaiHinh = row["Ma_CT"].ToString().Trim();
                string sohopdong = "";
                sohopdong = row["So_HD"].ToString().Trim();
                tkct.SoHopDongDV = row["So_HD"].ToString().Trim();
                tkct.SoHDKH = row["So_HDCh"].ToString().Trim();
                if (tkct.MaLoaiHinh.EndsWith("N") || tkct.MaLoaiHinh.Substring(0, 1).Equals("N"))
                {
                    tkct.DiaDiemXepHang = FontConverter.TCVN2Unicode(row["NguoiHQNhan"].ToString());

                }
                else
                {
                    tkct.DiaDiemXepHang = FontConverter.TCVN2Unicode(row["NguoiHQGiao"].ToString());
                }

                tkct.MaHaiQuanKH = row["Ma_HQKH"].ToString();
                tkct.MaHaiQuanTiepNhan = MaHaiQuan;
                tkct.MaKhachHang = row["Ma_DoiTac"].ToString();
                tkct.NgayDangKy = Convert.ToDateTime(row["Ngay_CT"]);

                HopDong hd = new HopDong();
                hd.ID = (new HopDong()).GetIDHopDongExit(sohopdong, MaHaiQuan, MaDoanhNghiep, Convert.ToDateTime(row["Ngay_Ky"]));
                hd = HopDong.Load(hd.ID);
                if (hd.ID > 0)
                {
                    tkct.IDHopDong = hd.ID;
                    tkct.NgayHDDV = Convert.ToDateTime(row["Ngay_Ky"].ToString());
                    tkct.NgayHDKH = Convert.ToDateTime(row["Ngay_KyCh"].ToString());
                    tkct.NgayHetHanHDDV = hd.NgayHetHan;
                    tkct.NguoiChiDinhDV = FontConverter.TCVN2Unicode(row["NguoiNhan"].ToString());
                    tkct.NguoiChiDinhKH = FontConverter.TCVN2Unicode(row["NguoiGiao"].ToString());
                    tkct.NgayTiepNhan = tkct.NgayDangKy;
                    tkct.NguyenTe_ID = row["Ma_NT"].ToString();
                    tkct.SoTiepNhan = Convert.ToInt64(row["CTID"].ToString());
                    if (row["SoCTDuocCap"].ToString() != "")
                        tkct.SoToKhai = Convert.ToInt32(row["SoCTDuocCap"].ToString());
                    if (row["TrangThai"].ToString() == "2")
                        tkct.TrangThaiXuLy = 1;
                    else
                        tkct.TrangThaiXuLy = 0;
                    if (row["TyGiaVND"].ToString() != "")
                        tkct.TyGiaVND = Convert.ToDecimal(row["TyGiaVND"].ToString());
                    tkct.ID = tkct.GetIDToKhaiBySoTiepNhanAndNamTiepNhanAndMaHaiQuan(tkct.SoTiepNhan, (short)tkct.NgayDangKy.Year, tkct.MaHaiQuanTiepNhan);
                    if (tkct.TrangThaiXuLy == 0)
                        DongBoDuLieuKhaiDTByIDTKCTDK(tkct, hd, nameConnectKDT);
                    else
                    {
                        if (tkct.ID == 0)
                            DongBoDuLieuKhaiDTByIDTKCTDK(tkct, hd, nameConnectKDT);
                    }
                    return tkct;
                }
            }
            return null;
        }
        #endregion DongBoDuLieuPhongKhai

        public ToKhaiChuyenTiep LoadByID(long id)
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) this.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) this.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) this.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                reader.Close();
                return this;
            }
            reader.Close();
            return null;
        }

        public string PhanTichMSG(string xml)
        {


            XmlDocument docNPL = new XmlDocument();
            XmlNode Result = null;
            docNPL.LoadXml(xml);

            Result = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/statement");
            XmlNode errorNode = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content/XMLERRORINFORMATION");
            if (Result.InnerText != "27")
            {
                if (Result.InnerText == "12")
                    return docNPL.InnerXml;
            }
            else if (docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").InnerText == "")
            {
                //throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content/XMLERRORINFORMATION").InnerText + "|" + ""));
                string errorStr = "";
                foreach (XmlNode node in docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").ChildNodes)
                {
                    errorStr += node.InnerText + "; \n";
                }
                throw new Exception(errorStr + "|" + "");
            }
            else if (docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").InnerText != "" && docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").InnerText.Contains("lỗi"))
            {
                //throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content/XMLERRORINFORMATION").InnerText + "|" + ""));
                string errorStr = "";
                foreach (XmlNode node in docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content").ChildNodes)
                {
                    errorStr += node.InnerText + "; \n";
                }
                throw new Exception(errorStr + "|" + "");
            }
            else if (errorNode != null)
            {
                //throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content/XMLERRORINFORMATION").InnerText + "|" + ""));
                string errorStr = "";
                foreach (XmlNode node in errorNode.ChildNodes)
                {
                    errorStr += node.InnerText + "; \n";
                }
                throw new Exception(errorStr + "|" + "");
            }

            XmlNode contentNode = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/AdditionalInformation/content");
            XmlNode issue = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/issue");
            XmlNode functionNode = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/function");
            XmlNode referenceNode = docNPL.SelectSingleNode("Envelope/Body/Content/Declaration/reference");

            bool ok = false;
            if (functionNode.InnerText == "27")// Từ chối
            {
                if (errorNode == null)
                {
                    if (this.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy)
                    {
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                        ok = true;
                    }
                    else if (this.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                    {
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                        ok = true;
                    }
                    else if (this.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
                    {
                        this.SoTiepNhan = 0;
                        this.SoToKhai = 0;
                        this.NgayTiepNhan = new DateTime(1900, 1, 1);
                        this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET;
                        //this.PhanLuong = "";
                        this.HUONGDAN = contentNode.InnerText;
                        ok = true;
                    }
                    else if (this.SoTiepNhan == 0)
                    {
                        //this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        //ok = true;

                    }
                }
                else
                {

                }
                string sfmtMessage = string.Format("Lỗi hải quan trả về khi khai báo: {0}", contentNode.InnerText);
                string title = Company.KDT.SHARE.Components.MessageTitle.Error;
                if (this.SoTiepNhan != 0)
                {
                    sfmtMessage = string.Format("Từ chối tiếp nhận. Lý do: {0}", contentNode.InnerText);
                    title = Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan;
                }
                Company.KDT.SHARE.Components.Globals.SaveMessage(xml, ID, title, sfmtMessage);
                return "";
            }
            else if (functionNode.InnerText == "29")// Cấp STN
            {
                string[] contentStr = contentNode.InnerText.Split('/');
                this.SoTiepNhan = Convert.ToInt64(contentStr[0]);
                this.NamDK = short.Parse(contentStr[1]);
                this.NgayTiepNhan = Convert.ToDateTime(issue.InnerText);
                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                ok = true;
                string sfmtmsg = string.Format("Số tiếp nhận {0}, ngày tiếp nhận {1}, năm đăng ký {2}",
                    SoTiepNhan, NgayTiepNhan, NamDK);
                Company.KDT.SHARE.Components.Globals.SaveMessage(xml, ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSoTiepNhan,
                    sfmtmsg
                    );


            }
            else if (functionNode.InnerText == "30")// Cấp số tờ khai
            {
                string[] contentStr = contentNode.InnerText.Split('/');
                this.SoToKhai = Convert.ToInt64(contentStr[0]);
                this.NgayDangKy = Convert.ToDateTime(issue.InnerText);
                this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                this.GUIDSTR = referenceNode.InnerText;
                string sfmtmsg = string.Format("Số tờ khai {0}, ngày đăng ký {1}",
                                               SoToKhai, NgayDangKy);
                Company.KDT.SHARE.Components.Globals.SaveMessage(xml, ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocCapSo,
                    sfmtmsg
                    );
                TransferToNPLTon();
                ok = true;

            }
            else if (functionNode.InnerText == "31")// Duyệt luồng chứng từ
            {
                string[] contentStr = contentNode.InnerText.Split('/');
                if (contentStr[0].ToUpper().Equals("LUỒNG VÀNG"))
                    this.PhanLuong = TrangThaiPhanLuong.LUONG_VANG;
                else if (contentStr[0].ToUpper().Equals("LUỒNG ĐỎ"))
                    this.PhanLuong = TrangThaiPhanLuong.LUONG_DO;
                else
                    this.PhanLuong = TrangThaiPhanLuong.LUONG_XANH;
                this.HUONGDAN = contentStr[1];
                this.GUIDSTR = referenceNode.InnerText;
                this.Update();
                ok = true;

                string sfmtmsg = string.Format("Số tờ khai {0}, Phân luồng {1}, Hướng đẫn: {2}",
                                   SoToKhai, PhanLuong, HUONGDAN);
                Company.KDT.SHARE.Components.Globals.SaveMessage(xml, ID, Company.KDT.SHARE.Components.MessageTitle.ToKhaiDuocPhanLuong,
                    sfmtmsg
                    );
            }
            else if (functionNode.InnerText == "32")// Thông báo thông quan
            {
                if (this.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                {
                    //Đồng ý Sửa Tờ khai
                    this.GUIDSTR = referenceNode.InnerText;
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                    ok = true;
                    string sfmtmsg = string.Format("Tờ khai sửa được duyệt: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, issue.InnerText.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuanTiepNhan.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveMessage(xml, ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoToKhaiCTDuocDuyet,
                        sfmtmsg
                        );


                }
                else if (this.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy)
                {
                    //Đồng ý Hủy Tờ khai
                    this.GUIDSTR = referenceNode.InnerText;
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY;
                    //Chưa xử lý lại thông tin tồn
                    ok = true;
                    string sfmtmsg = string.Format("Hủy tờ khai: {0}\r\nNgày đăng ký: {1}\r\nLoại hình: {2}\r\nHải quan: {3}", this.SoToKhai, issue.InnerText.ToString(), this.MaLoaiHinh.Trim(), this.MaHaiQuanTiepNhan.Trim());
                    Company.KDT.SHARE.Components.Globals.SaveMessage(xml, ID, Company.KDT.SHARE.Components.MessageTitle.HuyToKhai,
                        sfmtmsg
                        );


                }
            }
            else if (functionNode.InnerText == "33")// Thông báo thực xuất
            {

            }

            if (ok)
                this.Update();

            return "";

        }
        public List<ToKhaiChuyenTiep> SelectCollectionBy_IDHopDong(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_KDT_GC_ToKhaiChuyenTiep_SelectBy_IDHopDong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.BigInt, IDHopDong);

            List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        #region Đồng bộ dữ liệu V3

        public string InsertFullFromISyncDaTa()
        {
            string error = string.Empty;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    List<Company.GC.BLL.KDT.GC.HopDong> hdDV = Company.GC.BLL.KDT.GC.HopDong.SelectCollectionDynamic("SoHopDong = '" + this.SoHopDongDV.Trim() + "'", null);
                    if (hdDV == null || hdDV.Count == 0)
                    {
                        transaction.Rollback();
                        this.ID = 0;
                        connection.Close();
                        return "Không tồn tại hợp đồng DV của tờ khai";
                    }
                    else
                    {
                        this.IDHopDong = hdDV[0].ID;

                    }
                    List<Company.GC.BLL.KDT.GC.HopDong> hdKH = Company.GC.BLL.KDT.GC.HopDong.SelectCollectionDynamic("SoHopDong = '" + this.SoHDKH.Trim() + "'", null);
                    if (hdKH == null || hdKH.Count == 0)
                    {
                        transaction.Rollback();
                        this.ID = 0;
                        connection.Close();
                        return "Không tồn tại hợp đồng KH của tờ khai";
                    }
                    else
                    {
                        this.ID_Relation = hdKH[0].ID;

                    }
                    this.ID = 0;
                    this.ID = this.Insert(transaction);
                    int i = 1;

                    #region hàng mậu dịch
                    foreach (HangChuyenTiep hmd in this.HCTCollection)
                    {
                        hmd.SoThuTuHang = i++;
                        hmd.Master_ID = this.ID;
                        hmd.ID = hmd.Insert(transaction);
                    }
                    #endregion
                    #region chứng từ hải quan
                    i = 1;
                    
                    foreach (ChungTuNo chungtuno in this.ChungTuNoCollection)
                    {
                        chungtuno.ID = 0;
                        chungtuno.TKCTID = this.ID;
                        chungtuno.Insert(transaction);
                    }
                    foreach (ChungTuKemAnh ctk in this.AnhCollection)
                    {
                        ctk.ID = 0;
                        ctk.ID_TK = this.ID;
                        ctk.ID = ctk.Insert(transaction);
                        foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.ChungTuKemAnhChiTiet item in ctk.ListChungTuKemAnhChiTiet)
                        {
                            item.ID = 0;
                            item.ChungTuKemAnhID = ctk.ID;
                            item.Insert(transaction);
                        }
                    }
                    foreach (GiayPhep gp in this.GiayPhepCollection)
                    {
                        gp.ID = 0;
                        gp.TKCT_ID = this.ID;
                        gp.ID = gp.Insert(transaction);
                        foreach (GiayPhepChiTiet item in gp.ListHMDofGiayPhep)
                        {
                            item.ID = 0;
                            item.GiayPhep_ID = gp.ID;
                            foreach (HangChuyenTiep hct in this.HCTCollection)
                                if (hct.MaHang == item.MaPhu)
                                {
                                    item.HCT_ID = hct.ID;
                                    item.SoThuTuHang = hct.SoThuTuHang;
                                    break;
                                }
                            item.ID  = item.Insert(transaction);
                        }
                    }
                    foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HoaDon hdtm in this.HoaDonThuongMaiCollection)
                    {
                        hdtm.ID = 0;
                        hdtm.ID_TK = this.ID;
                        hdtm.ID = hdtm.Insert(transaction);
                        foreach (HoaDonChiTiet item in hdtm.ListHangMDOfHoaDon)
                        {
                            item.ID = 0;
                            item.HoaDonTM_ID = hdtm.ID;
                            foreach (HangChuyenTiep hmd in this.HCTCollection)
                                if (hmd.MaHang == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.ID = item.Insert(transaction);
                        }
                    }
                    foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.HopDong hopdong in this.HopDongThuongMaiCollection)
                    {
                        hopdong.ID = 0;
                        hopdong.ID_TK = this.ID;
                        hopdong.ID = hopdong.Insert(transaction);
                        foreach (HopDongChiTiet item in hopdong.ListHangMDOfHopDong)
                        {
                            item.ID = 0;
                            item.HopDongTM_ID = hopdong.ID;
                            foreach (HangChuyenTiep hmd in this.HCTCollection)
                                if (hmd.MaHang == item.MaPhu)
                                {
                                    item.HMD_ID = hmd.ID;
                                    item.SoThuTuHang = hmd.SoThuTuHang;
                                    break;
                                }
                            item.ID  = item.Insert(transaction);
                        }
                    }
                    foreach (DeNghiChuyenCuaKhau denghi in this.DeNghiChuyenCuaKhau)
                    {
                        denghi.ID = 0;
                        denghi.ID_TK = this.ID;
                        denghi.ID = denghi.Insert(transaction);
                    }
                    foreach (Company.KDT.SHARE.QuanLyChungTu.NoiDungDieuChinhTK noidungsua in this.NoiDungDieuChinhTKCollection)
                    {
                        noidungsua.ID = 0;
                        noidungsua.TKMD_ID = this.ID;
                        noidungsua.Insert(transaction);
                    }
                    foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUng item in this.NPLCungUngs)
                    {
                        item.ID = 0;
                        item.TKCTID = this.ID;
                        item.ID = item.Insert(transaction);
                        foreach (Company.KDT.SHARE.QuanLyChungTu.GCCT.NPLCungUngDetail nplcu in item.NPLCungUngDetails)
                        {
                            nplcu.ID = 0;
                            nplcu.Master_ID = item.ID;
                            nplcu.ID = nplcu.Insert(transaction);
                        }
                    }

                    #endregion

                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    error = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }
            return error;
        }
        public  IDataReader SelectDynamicDongBoDuLieu(string whereCondition, string orderByExpression)
        {
            string spName = "[dbo].[p_KDT_GC_ToKhaiChuyenTiep_SelectDynamicDongBoDuLieu]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }
        public int TrangThaiDongBo;
        public  List<ToKhaiChuyenTiep> SelectCollectionDynamicDongBoDuLieu(string whereCondition, string orderByExpression)
        {
            List<ToKhaiChuyenTiep> collection = new List<ToKhaiChuyenTiep>();
            IDataReader reader = SelectDynamicDongBoDuLieu(whereCondition, orderByExpression);
            while (reader.Read())
            {
                ToKhaiChuyenTiep entity = new ToKhaiChuyenTiep();
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_STATUS"))) entity.TrangThaiDongBo = reader.GetInt32(reader.GetOrdinal("MSG_STATUS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("IDHopDong"))) entity.IDHopDong = reader.GetInt64(reader.GetOrdinal("IDHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanTiepNhan"))) entity.MaHaiQuanTiepNhan = reader.GetString(reader.GetOrdinal("MaHaiQuanTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("CanBoDangKy"))) entity.CanBoDangKy = reader.GetString(reader.GetOrdinal("CanBoDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDongDV"))) entity.SoHopDongDV = reader.GetString(reader.GetOrdinal("SoHopDongDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDDV"))) entity.NgayHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDDV"))) entity.NgayHetHanHDDV = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhDV"))) entity.NguoiChiDinhDV = reader.GetString(reader.GetOrdinal("NguoiChiDinhDV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaKhachHang"))) entity.MaKhachHang = reader.GetString(reader.GetOrdinal("MaKhachHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenKH"))) entity.TenKH = reader.GetString(reader.GetOrdinal("TenKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHDKH"))) entity.SoHDKH = reader.GetString(reader.GetOrdinal("SoHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHDKH"))) entity.NgayHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHDKH"))) entity.NgayHetHanHDKH = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHDKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiChiDinhKH"))) entity.NguoiChiDinhKH = reader.GetString(reader.GetOrdinal("NguoiChiDinhKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLy"))) entity.MaDaiLy = reader.GetString(reader.GetOrdinal("MaDaiLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemXepHang"))) entity.DiaDiemXepHang = reader.GetString(reader.GetOrdinal("DiaDiemXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaVND"))) entity.TyGiaVND = reader.GetDecimal(reader.GetOrdinal("TyGiaVND"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaUSD"))) entity.TyGiaUSD = reader.GetDecimal(reader.GetOrdinal("TyGiaUSD"));
                if (!reader.IsDBNull(reader.GetOrdinal("LePhiHQ"))) entity.LePhiHQ = reader.GetDecimal(reader.GetOrdinal("LePhiHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoBienLai"))) entity.SoBienLai = reader.GetString(reader.GetOrdinal("SoBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayBienLai"))) entity.NgayBienLai = reader.GetDateTime(reader.GetOrdinal("NgayBienLai"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChungTu"))) entity.ChungTu = reader.GetString(reader.GetOrdinal("ChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanKH"))) entity.MaHaiQuanKH = reader.GetString(reader.GetOrdinal("MaHaiQuanKH"));
                if (!reader.IsDBNull(reader.GetOrdinal("ID_Relation"))) entity.ID_Relation = reader.GetInt64(reader.GetOrdinal("ID_Relation"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
                if (!reader.IsDBNull(reader.GetOrdinal("ActionStatus"))) entity.ActionStatus = reader.GetInt32(reader.GetOrdinal("ActionStatus"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidReference"))) entity.GuidReference = reader.GetString(reader.GetOrdinal("GuidReference"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDK"))) entity.NamDK = reader.GetInt16(reader.GetOrdinal("NamDK"));
                if (!reader.IsDBNull(reader.GetOrdinal("HUONGDAN"))) entity.HUONGDAN = reader.GetString(reader.GetOrdinal("HUONGDAN"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhanLuong"))) entity.PhanLuong = reader.GetString(reader.GetOrdinal("PhanLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("Huongdan_PL"))) entity.Huongdan_PL = reader.GetString(reader.GetOrdinal("Huongdan_PL"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiaoHang"))) entity.ThoiGianGiaoHang = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiHangHoa"))) entity.LoaiHangHoa = reader.GetString(reader.GetOrdinal("LoaiHangHoa"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoKien"))) entity.SoKien = reader.GetDecimal(reader.GetOrdinal("SoKien"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongNet"))) entity.TrongLuongNet = reader.GetDouble(reader.GetOrdinal("TrongLuongNet"));
                if (!reader.IsDBNull(reader.GetOrdinal("DaiDienDoanhNghiep"))) entity.DaiDienDoanhNghiep = reader.GetString(reader.GetOrdinal("DaiDienDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViUT"))) entity.MaDonViUT = reader.GetString(reader.GetOrdinal("MaDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViUT"))) entity.TenDonViUT = reader.GetString(reader.GetOrdinal("TenDonViUT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDaiLyTTHQ"))) entity.MaDaiLyTTHQ = reader.GetString(reader.GetOrdinal("MaDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDaiLyTTHQ"))) entity.TenDaiLyTTHQ = reader.GetString(reader.GetOrdinal("TenDaiLyTTHQ"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViDoiTac"))) entity.TenDonViDoiTac = reader.GetString(reader.GetOrdinal("TenDonViDoiTac"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXK_ID"))) entity.NuocXK_ID = reader.GetString(reader.GetOrdinal("NuocXK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocNK_ID"))) entity.NuocNK_ID = reader.GetString(reader.GetOrdinal("NuocNK_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhau_ID"))) entity.CuaKhau_ID = reader.GetString(reader.GetOrdinal("CuaKhau_ID"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        #endregion
    }
}