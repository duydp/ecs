﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.SXXK.ToKhai
{
    public partial class HangMauDich : EntityBase
    {
        public DataTable GetHangMauDich(string MaHQ, string MaDN, string where)
        {
            string sql = "select * from t_View_HangMauDich where MaHaiQuan=@MaHaiQuan and MaDoanhNghiep=@MaDoanhNghiep ";
            sql += where;

            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHQ);
            this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDN);

            DataSet ds = this.db.ExecuteDataSet(dbCommand);
            ds.Tables[0].Columns.Add("GhiChu");
            DataTable dtDuLieuKoHopLe = new DataTable();

            foreach (DataColumn col in ds.Tables[0].Columns)
                dtDuLieuKoHopLe.Columns.Add(col.ColumnName, col.DataType);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                string maLoaiHinh = row["MaLoaiHinh"].ToString();
                bool ok = false;
                if (maLoaiHinh.Substring(0, 1) == "N")
                {
                    NguyenPhuLieu npl = new NguyenPhuLieu();
                    npl.Ma = row["MaPhu"].ToString();
                    npl.MaDoanhNghiep = MaDN;
                    npl.MaHaiQuan = MaHQ;
                    if (!npl.Load())
                    {
                        row["GhiChu"] = "Mã hàng của nguyên phụ liệu : " + npl.Ma + " không có trong danh mục nguyên phụ liệu đã đăng ký.";
                        ok = true;
                    }
                }
                else
                {
                    string loaiHH = row["Xuat_NPL_SP"].ToString();
                    if (loaiHH == "S")
                    {
                        SanPham sp = new SanPham();
                        sp.Ma = row["MaPhu"].ToString();
                        sp.MaDoanhNghiep = MaDN;
                        sp.MaHaiQuan = MaHQ;
                        if (!sp.Load())
                        {
                            row["GhiChu"] = "Mã hàng  : " + sp.Ma + " không có trong danh mục sản phẩm đã đăng ký.";
                            ok = true;
                        }
                        else
                        {
                            if (!DinhMuc.checkIsExist(MaHQ, MaDN, sp.Ma))
                            {
                                if (row["GhiChu"].ToString() != "")
                                    row["GhiChu"] += "\n";
                                row["GhiChu"] += "Sản phẩm : " + sp.Ma + " chưa có đăng ký định mức.";
                                ok = true;
                            }
                        }
                    }
                    else
                    {
                        NguyenPhuLieu npl = new NguyenPhuLieu();
                        npl.Ma = row["MaPhu"].ToString();
                        npl.MaDoanhNghiep = MaDN;
                        npl.MaHaiQuan = MaHQ;
                        if (!npl.Load())
                        {
                            row["GhiChu"] = "Mã hàng của nguyên phụ liệu : " + npl.Ma + " không có trong danh mục nguyên phụ liệu đã đăng ký.";
                            ok = true;
                        }
                    }
                }
                if (ok) dtDuLieuKoHopLe.ImportRow(row);
            }

            return (dtDuLieuKoHopLe);
        }

        public DataSet GetNPLFromTKXuat()
        {
            string query = "SELECT CASE WHEN t.MaLoaiHinh LIKE '%XV%' THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = t.SoToKhai) ELSE t.SoToKhai END AS SoToKhaiVNACCS, t.SoToKhai, t.MaLoaiHinh, t.MaHaiQuan, t.NamDangKy, h.SoThuTuHang, " +
                           "h.MaHS, h.MaPhu, h.TenHang, h.NuocXX_ID, h.DVT_ID, h.SoLuong, h.DonGiaKB, h.DonGiaTT, h.TriGiaKB_VND, " +
                           "h.ThueSuatXNK, h.ThueSuatTTDB, h.ThueSuatGTGT, h.ThueXNK, h.ThueGTGT, h.PhuThu, h.TyLeThuKhac, h.TriGiaThuKhac, h.MienThue, " +
                           "dm.MaNguyenPhuLieu, h.SoLuong * (dm.DinhMucSuDung / 100 * dm.TyLeHaoHut + dm.DinhMucSuDung) AS LuongNPL, " +
                           "t.MaDoanhNghiep, dm.DinhMucSuDung, dm.TyLeHaoHut " +
                           "FROM dbo.t_SXXK_ToKhaiMauDich t INNER JOIN   dbo.t_SXXK_HangMauDich h " +
                           "ON t.SoToKhai = h.SoToKhai AND t.MaLoaiHinh =h.MaLoaiHinh AND t.NamDangKy =h.NamDangKy AND t.MaHaiQuan =h.MaHaiQuan " +
                           "INNER JOIN dbo.t_SXXK_DinhMuc dm ON dm.MaSanPham = h.MaPhu INNER JOIN dbo.t_SXXK_NguyenPhuLieu npl ON dm.MaNguyenPhuLieu = npl.Ma " +
                           "WHERE (t.MaLoaiHinh LIKE  'X%')";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(query);
            return this.db.ExecuteDataSet(dbCommand);
        }

        public DataSet GetNPLFromTKXuat(int year, long SoToKhai)
        {
            string query = "SELECT CASE WHEN t.MaLoaiHinh LIKE '%XV%' THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = t.SoToKhai) ELSE t.SoToKhai END AS SoToKhaiVNACCS, t.SoToKhai, t.MaLoaiHinh, t.MaHaiQuan, t.NamDangKy, h.SoThuTuHang, " +
                           "h.MaHS, h.MaPhu, h.TenHang, h.NuocXX_ID, h.DVT_ID, h.SoLuong, h.DonGiaKB, h.DonGiaTT, h.TriGiaKB_VND, " +
                           "h.ThueSuatXNK, h.ThueSuatTTDB, h.ThueSuatGTGT, h.ThueXNK, h.ThueGTGT, h.PhuThu, h.TyLeThuKhac, h.TriGiaThuKhac, h.MienThue, " +
                           "dm.MaNguyenPhuLieu, h.SoLuong * (dm.DinhMucSuDung / 100 * dm.TyLeHaoHut + dm.DinhMucSuDung) AS LuongNPL, " +
                           "t.MaDoanhNghiep, dm.DinhMucSuDung, dm.TyLeHaoHut " +
                           "FROM dbo.t_SXXK_ToKhaiMauDich t INNER JOIN   dbo.t_SXXK_HangMauDich h " +
                           "ON t.SoToKhai = h.SoToKhai AND t.MaLoaiHinh =h.MaLoaiHinh AND t.NamDangKy =h.NamDangKy AND t.MaHaiQuan =h.MaHaiQuan " +
                           "INNER JOIN dbo.t_SXXK_DinhMuc dm ON dm.MaSanPham = h.MaPhu INNER JOIN dbo.t_SXXK_NguyenPhuLieu npl ON dm.MaNguyenPhuLieu = npl.Ma " +
                           "WHERE (t.MaLoaiHinh LIKE  'X%')  AND h.NamDangKy = " + year + (SoToKhai > 0 ? " AND t.SoToKhai = " + SoToKhai:"");
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(query);
            return this.db.ExecuteDataSet(dbCommand);
        }
        public DataSet GetNPLFromTKXuat(int year, string WhereSoToKhai)
        {
            string query = "SELECT CASE WHEN t.MaLoaiHinh LIKE '%XV%' THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = t.SoToKhai) ELSE t.SoToKhai END AS SoToKhaiVNACCS, t.SoToKhai, t.MaLoaiHinh, t.MaHaiQuan, t.NamDangKy, h.SoThuTuHang, " +
                           "h.MaHS, h.MaPhu, h.TenHang, h.NuocXX_ID, h.DVT_ID, h.SoLuong, h.DonGiaKB, h.DonGiaTT, h.TriGiaKB_VND, " +
                           "h.ThueSuatXNK, h.ThueSuatTTDB, h.ThueSuatGTGT, h.ThueXNK, h.ThueGTGT, h.PhuThu, h.TyLeThuKhac, h.TriGiaThuKhac, h.MienThue, " +
                           "dm.MaNguyenPhuLieu, h.SoLuong * (dm.DinhMucSuDung / 100 * dm.TyLeHaoHut + dm.DinhMucSuDung) AS LuongNPL, " +
                           "t.MaDoanhNghiep, dm.DinhMucSuDung, dm.TyLeHaoHut " +
                           "FROM dbo.t_SXXK_ToKhaiMauDich t INNER JOIN   dbo.t_SXXK_HangMauDich h " +
                           "ON t.SoToKhai = h.SoToKhai AND t.MaLoaiHinh =h.MaLoaiHinh AND t.NamDangKy =h.NamDangKy AND t.MaHaiQuan =h.MaHaiQuan " +
                           "INNER JOIN dbo.t_SXXK_DinhMuc dm ON dm.MaSanPham = h.MaPhu INNER JOIN dbo.t_SXXK_NguyenPhuLieu npl ON dm.MaNguyenPhuLieu = npl.Ma " +
                           "WHERE (t.MaLoaiHinh LIKE  'X%')  AND h.NamDangKy = " + year + (!string.IsNullOrEmpty(WhereSoToKhai) ? " AND t.SoToKhai in (" + WhereSoToKhai + ")" : "" + " AND t.MaLoaiHinh like '%V%'");
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(query);
            return this.db.ExecuteDataSet(dbCommand);
        }

        public DataSet GetTongLuongNPLFromTKXuat()
        {
            string query = " SELECT CASE WHEN t.MaLoaiHinh LIKE '%XV%' THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = t.SoToKhai) ELSE t.SoToKhai END AS SoToKhaiVNACCS, t.SoToKhai,t.MaLoaiHinh,t.MaHaiQuan,t.NamDangKy,h.NuocXX_ID,h.DVT_ID,dm.MaNguyenPhuLieu,SUM(h.SoLuong*(dm.DinhMucSuDung/100*dm.TyLeHaoHut+dm.DinhMucSuDung))AS LuongNPL,t.MaDoanhNghiep " +
                            " FROM    dbo.t_SXXK_ToKhaiMauDich t " +
                            " INNER JOIN dbo.t_SXXK_HangMauDich h ON t.SoToKhai = h.SoToKhai AND t.MaLoaiHinh = h.MaLoaiHinh AND t.NamDangKy = h.NamDangKy AND t.MaHaiQuan = h.MaHaiQuan " +
                            " INNER JOIN dbo.t_SXXK_DinhMuc dm ON dm.MaSanPham = h.MaPhu " +
                            " INNER JOIN dbo.t_SXXK_NguyenPhuLieu npl ON dm.MaNguyenPhuLieu = npl.Ma " +
                            " WHERE   ( t.MaLoaiHinh LIKE 'X%' ) " +
                            " GROUP BY t.SoToKhai,t.MaLoaiHinh,t.MaHaiQuan,t.MaDoanhNghiep,t.NamDangKy,h.NuocXX_ID,h.DVT_ID,dm.MaNguyenPhuLieu ";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(query);
            return this.db.ExecuteDataSet(dbCommand);
        }

        public DataSet GetTongLuongNPLFromTKXuat(int year)
        {
            string query = " SELECT CASE WHEN t.MaLoaiHinh LIKE '%XV%' THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = t.SoToKhai) ELSE t.SoToKhai END AS SoToKhaiVNACCS,  t.SoToKhai,t.MaLoaiHinh,t.MaHaiQuan,t.NamDangKy,h.NuocXX_ID,h.DVT_ID,dm.MaNguyenPhuLieu,SUM(h.SoLuong*(dm.DinhMucSuDung/100*dm.TyLeHaoHut+dm.DinhMucSuDung))AS LuongNPL,t.MaDoanhNghiep " +
                            " FROM    dbo.t_SXXK_ToKhaiMauDich t " +
                            " INNER JOIN dbo.t_SXXK_HangMauDich h ON t.SoToKhai = h.SoToKhai AND t.MaLoaiHinh = h.MaLoaiHinh AND t.NamDangKy = h.NamDangKy AND t.MaHaiQuan = h.MaHaiQuan " +
                            " INNER JOIN dbo.t_SXXK_DinhMuc dm ON dm.MaSanPham = h.MaPhu " +
                            " INNER JOIN dbo.t_SXXK_NguyenPhuLieu npl ON dm.MaNguyenPhuLieu = npl.Ma " +
                            " WHERE   ( t.MaLoaiHinh LIKE 'X%' ) AND h.NamDangKy = " + year +
                            " GROUP BY t.SoToKhai,t.MaLoaiHinh,t.MaHaiQuan,t.MaDoanhNghiep,t.NamDangKy,h.NuocXX_ID,h.DVT_ID,dm.MaNguyenPhuLieu ";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(query);
            return this.db.ExecuteDataSet(dbCommand);
        }

        public bool LoadByMaHang()
        {
            string spName = "select * from t_SXXK_HangMauDich where SoToKhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and NamDangKy=@NamDangKy and MaPhu=@MaPhu";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this.MaPhu);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) this._SoThuTuHang = reader.GetInt16(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) this._MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this._SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) this._DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) this._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) this._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) this._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) this._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) this._ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) this._ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) this._ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this._ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) this._ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) this._ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) this._PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) this._TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) this._TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) this._MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                this.MaTMP = this.MaPhu;
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool LoadByMaHang(SqlTransaction transaction)
        {
            string spName=string.Empty;
            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                spName = "select * from t_SXXK_HangMauDich where SoToKhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and NamDangKy=@NamDangKy and MaPhu=@MaPhu and TenHang = @TenHang and DonGiaTT = @DonGiaTT or ThueXNK = 0  and SoLuong=@SoLuong";
            else
                spName = "select * from t_SXXK_HangMauDich where SoToKhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and NamDangKy=@NamDangKy and MaPhu=@MaPhu and TenHang = @TenHang and DonGiaTT = @DonGiaTT and SoLuong=@SoLuong";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            this.db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            this.db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            this.db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this.MaPhu);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this.TenHang);
            this.db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, this.DonGiaTT);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this.SoLuong);

            IDataReader reader = this.db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) this._SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) this._MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) this._NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) this._SoThuTuHang = reader.GetInt16(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) this._MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this._SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) this._DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) this._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) this._TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) this._TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) this._TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) this._ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) this._ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) this._ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this._ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) this._ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) this._ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) this._PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) this._TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) this._TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) this._MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                this.MaTMP = this.MaPhu;
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public HangMauDich LoadByMaHang(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "select * from t_SXXK_HangMauDich where SoToKhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and NamDangKy=@NamDangKy and MaPhu=@MaPhu";
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this.MaPhu);

            IDataReader reader = db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                HangMauDich entity = new HangMauDich();
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt32(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKy"))) entity.NamDangKy = reader.GetInt16(reader.GetOrdinal("NamDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt16(reader.GetOrdinal("SoThuTuHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDecimal(reader.GetOrdinal("DonGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDecimal(reader.GetOrdinal("TriGiaKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTT"))) entity.TriGiaTT = reader.GetDecimal(reader.GetOrdinal("TriGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB_VND"))) entity.TriGiaKB_VND = reader.GetDecimal(reader.GetOrdinal("TriGiaKB_VND"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatXNK"))) entity.ThueSuatXNK = reader.GetDecimal(reader.GetOrdinal("ThueSuatXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatTTDB"))) entity.ThueSuatTTDB = reader.GetDecimal(reader.GetOrdinal("ThueSuatTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuatGTGT"))) entity.ThueSuatGTGT = reader.GetDecimal(reader.GetOrdinal("ThueSuatGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDecimal(reader.GetOrdinal("ThueTTDB"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueGTGT"))) entity.ThueGTGT = reader.GetDecimal(reader.GetOrdinal("ThueGTGT"));
                if (!reader.IsDBNull(reader.GetOrdinal("PhuThu"))) entity.PhuThu = reader.GetDecimal(reader.GetOrdinal("PhuThu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeThuKhac"))) entity.TyLeThuKhac = reader.GetDecimal(reader.GetOrdinal("TyLeThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGiaThuKhac"))) entity.TriGiaThuKhac = reader.GetDecimal(reader.GetOrdinal("TriGiaThuKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("MienThue"))) entity.MienThue = reader.GetByte(reader.GetOrdinal("MienThue"));
                entity.MaTMP = entity.MaPhu;
                reader.Close();
                return entity;
            }
            reader.Close();
            return null;
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_HangMauDich_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, this._SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, this._DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, this._TriGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, this._TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, this._TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);

            if (transaction != null)
            {
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            string spName = "p_SXXK_HangMauDich_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, this._SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, this._MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, this._NamDangKy);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.SmallInt, this._SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, this._MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Decimal, this._DonGiaKB);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, this._DonGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Decimal, this._TriGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaTT", SqlDbType.Decimal, this._TriGiaTT);
            db.AddInParameter(dbCommand, "@TriGiaKB_VND", SqlDbType.Decimal, this._TriGiaKB_VND);
            db.AddInParameter(dbCommand, "@ThueSuatXNK", SqlDbType.Decimal, this._ThueSuatXNK);
            db.AddInParameter(dbCommand, "@ThueSuatTTDB", SqlDbType.Decimal, this._ThueSuatTTDB);
            db.AddInParameter(dbCommand, "@ThueSuatGTGT", SqlDbType.Decimal, this._ThueSuatGTGT);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Money, this._ThueXNK);
            db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Money, this._ThueTTDB);
            db.AddInParameter(dbCommand, "@ThueGTGT", SqlDbType.Money, this._ThueGTGT);
            db.AddInParameter(dbCommand, "@PhuThu", SqlDbType.Money, this._PhuThu);
            db.AddInParameter(dbCommand, "@TyLeThuKhac", SqlDbType.Decimal, this._TyLeThuKhac);
            db.AddInParameter(dbCommand, "@TriGiaThuKhac", SqlDbType.Money, this._TriGiaThuKhac);
            db.AddInParameter(dbCommand, "@MienThue", SqlDbType.TinyInt, this._MienThue);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public int DeleteTransaction(SqlTransaction transaction, long soToKhai, string maLoaiHinh, int namDangKy, string maHaiQuan, string maHang)
        {
            string spName = "delete t_SXXK_HangMauDich where sotokhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaHaiQuan=@MaHaiQuan and MaPhu=@MaPhu and NamDangKy=@NamDangKy";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, soToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.Char, maLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, namDangKy);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, maHang);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public DataSet SelectHangTon(string Where)
        {
            string spName = @"SELECT CASE
           WHEN h.MaLoaiHinh LIKE '%E%' THEN
           (
               SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = h.SoToKhai
           )
           ELSE
               h.SoToKhai
       END AS SoToKhaiVNACCS,
       h.SoToKhai,
       h.MaLoaiHinh,
       h.MaHaiQuan,
       h.NamDangKy,
       h.SoThuTuHang,
       h.MaHS,
       h.MaPhu,
       h.TenHang,
       h.NuocXX_ID,
       h.DVT_ID,
       h.SoLuong,
       h.DonGiaKB,
       h.DonGiaTT,
       h.TriGiaKB,
       h.TriGiaTT,
       h.TriGiaKB_VND,
       h.ThueSuatXNK,
       h.ThueSuatTTDB,
       h.ThueSuatGTGT,
       h.ThueXNK,
       h.ThueTTDB,
       h.ThueGTGT,
       h.PhuThu,
       h.TyLeThuKhac,
       h.TriGiaThuKhac,
       h.MienThue,
	   (SELECT NgayDangKy FROM dbo.t_SXXK_ToKhaiMauDich WHERE MaHaiQuan = h.MaHaiQuan AND SoToKhai = h.SoToKhai AND MaLoaiHinh = h.MaLoaiHinh AND NamDangKy = h.NamDangKy) AS NgayDangKy
FROM dbo.t_SXXK_HangMauDich h WHERE ";

            spName = spName + Where;
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }
        public int UpdateByDonGiaTT(int SoToKhai, string MaLoaiHinh, int NamDK, string Mahang, double DonGiaTTKB, double donGiaTT, double ThueSuat)
        {
            System.Globalization.NumberFormatInfo provider = new System.Globalization.NumberFormatInfo();
            provider.NumberDecimalSeparator = ".";
            string spName = string.Format("update t_SXXK_HangMauDich set DonGiaTT = @DonGiaTT, ThueSuatXNK = @ThueSuat where sotokhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaPhu=@MaPhu and NamDangKy=@NamDangKy and   Convert(decimal(23,6),Round(DonGiaTT,0)) = Convert(decimal(23,6),Round({0},0))", DonGiaTTKB.ToString("0.######",provider));
            //string spName = "select * from t_SXXK_HangMauDich where sotokhai=@SoToKhai and MaLoaiHinh=@MaLoaiHinh and MaPhu=@MaPhu and NamDangKy=@NamDangKy and   Convert(decimal(23,6),Round(DonGiaTT,6)) = Convert(decimal(23,6),Round(@DonGiaTTKB,6))";
            //string spName = "select @DonGiaTTKB ";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, donGiaTT);
            //db.AddInParameter(dbCommand, "@DonGiaTTKB", SqlDbType.Decimal, DonGiaTTKB);
            db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, ThueSuat);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.SmallInt, NamDK);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, Mahang);

            return db.ExecuteNonQuery(dbCommand);
            //throw new Exception("Đơn giá : " + db.ExecuteScalar(dbCommand).ToString());

            
        }

    }
}