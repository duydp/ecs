﻿using System;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
using System.Threading;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Xml;
using System.Net;

namespace Company.Interface
{
    public partial class ThietLapThongSoKBForm : BaseForm
    {
        public ThietLapThongSoKBForm()
        {
            InitializeComponent();
        }

        private void SendForm_Load(object sender, EventArgs e)
        {
            /* Update by HungTQ, 14/01/2011.*/
            txtDiaChiHQ.Text = GlobalSettings.DiaChiWS_Host;
            txtTenDichVu.Text = GlobalSettings.DiaChiWS_Name;
            txtHost.Text = GlobalSettings.Host;
            txtPort.Text = GlobalSettings.Port;

            txtServerName.Text = GlobalSettings.SERVER_NAME;
            txtSa.Text = GlobalSettings.USER;
            txtPass.Text = GlobalSettings.PASS;

            SetListTable();
            cboDatabaseSource.Text = GlobalSettings.DATABASE_NAME;

        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            string MSSQLConnectionString = Company.KDT.SHARE.Components.Globals.ReadNodeXmlConnectionStrings2();
            //Kiem tra SQL co phai cau hinh ket noi truc tiep file DATABASE.
            bool isAttachFile = MSSQLConnectionString.Contains("AttachDbFilename");

            //Neu nguoi dung nhap vao [user, pass] -> khong su dung ket noi truc tiep den file DATABASE.
            isAttachFile = (txtSa.Text.Trim().Length == 0 && txtPass.Text.Trim().Length == 0);

            if (!isAttachFile)
            {
                containerValidator1.Validate();
                if (!containerValidator1.IsValid)
                    return;
            }

            if (cboDatabaseSource.Text == "")
            {
                errorProvider1.SetError(cboDatabaseSource, "\"Cơ sơ dữ liệu\" không được trống");
                return;
            }
            else
                errorProvider1.SetError(cboDatabaseSource, "");

            if (!isAttachFile)
            {
                //Cấu hình lại connectionString.
                SqlConnection con = new SqlConnection("Server=" + txtServerName.Text.Trim() + "; uid=" + txtSa.Text.Trim() + "; pwd=" + txtPass.Text.Trim() + "; database=" + cboDatabaseSource.Text.Trim());
                try
                {
                    con.Open();
                }
                catch
                {
                    if (MLMessages("Không kết nối được tới cơ sở dữ liệu trên máy chủ này.\r\nBạn có muốn lưu không?", "MSG_DAB01", "", true) != "Yes")
                        return;
                }

                //Hungtq 14/01/2011. Luu cau hinh
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("pass", txtPass.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("user", txtSa.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DATABASE_NAME", cboDatabaseSource.Text.Trim());
            }
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ServerName", txtServerName.Text.Trim());

            //HUNGTQ Updated 07/06/2011
            string URI = lblWS.Text.Trim();
            if (!URI.Contains("http:"))
                URI = "http://" + lblWS.Text.Trim();
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DiaChiWS", URI);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("Company_BLL_WS_KhaiDienTu_KDTService", URI);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlSetting("QuanLyChungTu_WS_KDTService", URI);
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("WS_URL", URI);

            //WebProxy proxy = (WebProxy)WebRequest.DefaultWebProxy;
            //if (proxy.Address.AbsoluteUri != string.Empty)
            //{
            //     proxy.Address.AbsoluteUri;
            //     wc.Proxy = proxy;
            //}


            /*DATLMQ update lưu Host và Port 13/05/2011*/
            if (txtHost.Text.Equals("") || txtPort.Text.Equals(""))
            {
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsUseProxy", "False");
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Host", "");
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Port", "");
                GlobalSettings.IsUseProxy = false;
                GlobalSettings.Host = "";
                GlobalSettings.Port = "";
            }
            else
            {
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsUseProxy", "True");
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Host", txtHost.Text.Trim());
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("Port", txtPort.Text.Trim());
                GlobalSettings.IsUseProxy = true;
                GlobalSettings.Host = txtHost.Text.Trim();
                GlobalSettings.Port = txtPort.Text.Trim();
            }

            /*DATLMQ update lưu cấu hình vào file config 18/01/2011.*/
            XmlDocument doc = new XmlDocument();
            string path = Company.BLL.EntityBase.GetPathProram() + "\\ConfigDoanhNghiep";
            //Hungtq update 28/01/2011.
            string fileName = Company.KDT.SHARE.Components.Globals.GetFileName(path);

            doc.Load(fileName);

            //HUNGTQ Updated 07/06/2011
            //Set thong tin WS_Host
            XmlNode nodeWS_Host = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Host");
            if (txtDiaChiHQ.Text.Contains("http:"))
                GlobalSettings.DiaChiWS_Host = nodeWS_Host.InnerText = txtDiaChiHQ.Text.Trim();
            else
                GlobalSettings.DiaChiWS_Host = nodeWS_Host.InnerText = "http://" + txtDiaChiHQ.Text.Trim();
            //Set thong tin WS_Name
            XmlNode nodeWS_Name = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS_Name");
            GlobalSettings.DiaChiWS_Name = nodeWS_Name.InnerText = txtTenDichVu.Text.Trim();
            //Set thong tin WebService
            XmlNode nodeWebService = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "WS");
            GlobalSettings.DiaChiWS = nodeWebService.InnerText = URI;

            //Set thong tin Server
            XmlNode nodeServerName = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "SERVER");
            GlobalSettings.SERVER_NAME = nodeServerName.InnerText = txtServerName.Text.Trim();

            //Set thong tin Host proxy, Port
            XmlNode nodeHostProxy = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Host");
            GlobalSettings.Host = nodeHostProxy.InnerText = txtHost.Text.Trim();

            XmlNode nodePort = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Port");
            GlobalSettings.Port = nodePort.InnerText = txtPort.Text.Trim();

            if (!isAttachFile)
            {
                //Set thong tin Database
                XmlNode nodeDatabase = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Database");
                GlobalSettings.DATABASE_NAME = nodeDatabase.InnerText = cboDatabaseSource.Text.Trim();
                //Set thong tin UserName
                XmlNode nodeUserName = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "UserName");
                GlobalSettings.USER = nodeUserName.InnerText = txtSa.Text.Trim();
                //Set thong tin Password
                XmlNode nodePassword = Company.KDT.SHARE.Components.Globals.GetNodeConfigDN(doc, "Password");
                GlobalSettings.PASS = nodePassword.InnerText = txtPass.Text.Trim();
            }

            doc.Save(fileName);
            ShowMessage("Lưu file cấu hình thành công.", false);

            if (!isAttachFile)
            {
                string mssql = "Server=" + txtServerName.Text.Trim() + "; database=" + cboDatabaseSource.Text.Trim() + "; uid=" + txtSa.Text.Trim() + "; pwd=" + txtPass.Text.Trim();
                Company.KDT.SHARE.Components.Globals.SaveNodeXmlConnectionStrings(mssql);
            }
            else
            {
                string MSSQLConnectionStringModified = "";
                string[] arr = MSSQLConnectionString.Split(new char[] { ';' });

                arr[0] = arr[0].Split(new char[] { '=' })[0] + "=" + txtServerName.Text.Trim().ToUpper();

                for (int i = 0; i < arr.Length; i++)
                {
                    MSSQLConnectionStringModified += arr[i];

                    if (i < arr.Length - 1)
                        MSSQLConnectionStringModified += ";";
                }

                Company.KDT.SHARE.Components.Globals.SaveNodeXmlConnectionStrings(MSSQLConnectionStringModified);
            }

            GlobalSettings.RefreshKey();

            this.Close();
            Application.Restart();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region SQLDMO

        //private SQLDMO.Application fApp = null;
        //private SQLDMO.SQLServerClass fServer = null;
        //private string gAppName = "Sql Server BackUp utility";

        private void GetListTableSource(Janus.Windows.EditControls.UIComboBox comboBox)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string strConnString = "server=" + txtServerName.Text;
                strConnString += ";User Id=" + txtSa.Text + ";Password=" + txtPass.Text;

                //fServer = new SQLDMO.SQLServerClass();
                //fServer.Connect(txtServerName.Text, txtSa.Text, txtPass.Text);

                Company.KDT.SHARE.Components.SQL.InitializeServer(strConnString);

                comboBox.Items.Clear();
                comboBox.Items.Add("(-Làm mới-)");

                //foreach (SQLDMO.Database db in fServer.Databases)
                //{
                //    comboBox.Items.Add(db.Name);
                //}

                foreach (string dbName in Company.KDT.SHARE.Components.SQL.ListDatabases())
                {
                    comboBox.Items.Add(dbName);
                }

                comboBox.SelectedIndexChanged -= new EventHandler(cboDatabaseSource_SelectedIndexChanged);
                comboBox.SelectedIndex = 0;
                comboBox.SelectedIndexChanged += new EventHandler(cboDatabaseSource_SelectedIndexChanged);

            }
            catch (Exception e1)
            {
                Globals.ShowMessage("Kết nối không thành công. Bạn hãy kiểm tra lại thông tin cấu hình.\r\n\nChi tiết: " + e1.Message, false);
                //MessageBox.Show(e1.ToString(), gAppName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.LocalLogger.Instance().WriteMessage(e1);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SetListTable()
        {
            if (GlobalSettings.ListTableNameSource.Count > 0)
            {
                cboDatabaseSource.Items.Clear();
                cboDatabaseSource.Items.Add("(-Làm mới-)");

                foreach (string item in GlobalSettings.ListTableNameSource)
                {
                    cboDatabaseSource.Items.Add(item);
                }

                cboDatabaseSource.SelectedItem.Value = GlobalSettings.DATABASE_NAME;
            }
        }

        private void cboDatabaseSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboDatabaseSource.SelectedIndex == 0)
            {
                GetListTableSource(cboDatabaseSource);
            }
        }

        private void cboDatabaseSource_DropDown(object sender, EventArgs e)
        {
            if (cboDatabaseSource.Items.Count <= 1)
                GetListTableSource(cboDatabaseSource);
        }

        private void cboDatabaseSource_Closed(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (cboDatabaseSource.SelectedIndex == 0)
                {
                    GetListTableSource(cboDatabaseSource);
                }

                GlobalSettings.ListTableNameSource.Clear();
                for (int i = 1; i < cboDatabaseSource.Items.Count; i++)
                {
                    GlobalSettings.ListTableNameSource.Add(cboDatabaseSource.Items[i].ToString());
                }

                Cursor = Cursors.Default;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        #endregion

        private void txtDiaChiHQ_TextChanged(object sender, EventArgs e)
        {
            lblWS.Text = txtDiaChiHQ.Text.Trim() + "/" + txtTenDichVu.Text.Trim();
        }

        private void txtTenDichVu_TextChanged(object sender, EventArgs e)
        {
            lblWS.Text = txtDiaChiHQ.Text.Trim() + "/" + txtTenDichVu.Text.Trim();
        }

        private void btnOpenWS_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();

            process.StartInfo.UseShellExecute = true;

            process.StartInfo.FileName = lblWS.Text.Trim();

            process.Start();
        }
    }
}