﻿using System.Data;
using System.Data.SqlClient;
using System;
using Company.GC.BLL.Utils;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
namespace Company.GC.BLL.GC
{
    public partial class ThietBi
    {
        //public static DataSet WS_GetDanhSachDaDangKy(string maHaiQuan, string maDoanhNghiep)
        //{
        //    GCService service = new GCService();
        //    return service.ThietBi_GetDanhSach(maHaiQuan, maDoanhNghiep);
        //}

        //public static bool UpdateRegistedToDatabase(string maHaiQuan, string maDoanhNghiep, SqlTransaction transaction)
        //{
        //    // Lấy danh sách từ WEB SERVICE.
        //    DataSet ds =WS_GetDanhSachDaDangKy(maHaiQuan, maDoanhNghiep);

        //    // Cập nhật vào CSDL.            
        //    foreach (DataRow row in ds.Tables[0].Rows)
        //    {
        //        ThietBi tb = new ThietBi();
        //        tb.SoHopDong = row["SoHopDong"].ToString();
        //        tb.MaHaiQuan = maHaiQuan;
        //        tb.MaDoanhNghiep = maDoanhNghiep;
        //        tb.NgayKy = Convert.ToDateTime(row["NgayKy"]);
        //        tb.Ma = row["Ma"].ToString().Substring(1);
        //        tb.Ten = FontConverter.TCVN2Unicode(row["Ten"].ToString());
        //        tb.MaHS = row["MaHS"].ToString();
        //        tb.DVT_ID = row["DVT_ID"].ToString();
        //        tb.SoLuongDangKy = Convert.ToDecimal(row["SoLuongDangKy"]);
        //        tb.NuocXX_ID = row["NuocXX_ID"].ToString();
        //        tb.TinhTrang = row["TinhTrang"].ToString();
        //        tb.DonGia = Convert.ToDecimal(row["DonGia"].ToString());
        //        tb.TriGia = Convert.ToDecimal(row["TriGia"].ToString());
        //        tb.NguyenTe_ID = row["NguyenTe_ID"].ToString();
        //        tb.InsertUpdateTransaction(transaction);               
        //    }
        //    return true;
        //}
        public bool Load(SqlTransaction tran)
        {
            string spName = "p_GC_ThietBi_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand, tran);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) this._SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) this._SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) this._TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this._DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) this._TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        public bool Load(string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_ThietBi_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) this._HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) this._Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) this._Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) this._SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) this._SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) this._TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this._DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) this._TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) this._TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }
        public ThietBi Load(string Ma, long HopDong_ID)
        {
            ThietBi entity = null;
            string spName = "p_GC_ThietBi_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt,HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar,Ma);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new ThietBi();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                reader.Close();
                return entity;
            }
            reader.Close();
            return entity;
        }
        public ThietBiCollection SelectCollectionDaDuyetBy_HopDong_ID()
        {
            string sql = "select * from t_GC_ThietBi where trangthai=0 and HopDong_ID=@HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            ThietBiCollection collection = new ThietBiCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ThietBi entity = new ThietBi();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public ThietBiCollection SelectCollectionDaDuyetAndBoSungBy_HopDong_ID()
        {
            string sql = "select * from t_GC_ThietBi where trangthai=0 and HopDong_ID=@HopDong_ID or  trangthai=2 and HopDong_ID=@HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(sql);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            ThietBiCollection collection = new ThietBiCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                ThietBi entity = new ThietBi();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public DataSet GetThietBi(long IDHopDong)
        {
            string sql = " SELECT * "
          + " FROM t_GC_ThietBi  "
          + " Where HopDong_ID = @HopDong_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            DataSet ds = new DataSet();
            ds = db.ExecuteDataSet(dbCommand);
            return ds;
        }

        public string GetTenThietBi(long IDHopDong,string MaHS ,string Ma)
        {
            string sql = " SELECT Ten "
          + " FROM t_GC_ThietBi  "
          + " Where HopDong_ID = @HopDong_ID AND Ma=@Ma AND MaHS=@MaHS ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, IDHopDong);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, IDHopDong);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, IDHopDong);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return null;
            else
                return (String)obj;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression, string databaseName)
        {
            string spName = "p_GC_ThietBi_SelectDynamic";

            //Updated by HUNGTQ, 06/05/2011.
            SetDabaseMoi(databaseName);

            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        public int DeleteBy_HopDong_ID(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_GC_ThietBi_DeleteBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            return this.db.ExecuteNonQuery(dbCommand, transaction);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, string databaseName)
        {
            //Updated by HUNGTQ, 09/05/2011.
            SetDabaseMoi(databaseName);

            string spName = "p_GC_ThietBi_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, this._SoLuongDaNhap);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, this._TinhTrang);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, this._DonGia);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, this._TriGia);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);

            if (transaction != null)
            {
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return this.db.ExecuteNonQuery(dbCommand);
            }
        }

        public int InsertUpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_ThietBi_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, this._SoLuongDaNhap);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, this._TinhTrang);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, this._DonGia);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, this._TriGia);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        public ThietBiCollection SelectCollectionBy_HopDong_ID(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_ThietBi_SelectBy_HopDong_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);

            ThietBiCollection collection = new ThietBiCollection();
            IDataReader reader = null;
            if (transaction != null)
                reader = this.db.ExecuteReader(dbCommand, transaction);
            else
                reader = this.db.ExecuteReader(dbCommand);

            while (reader.Read())
            {
                ThietBi entity = new ThietBi();
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ma"))) entity.Ma = reader.GetString(reader.GetOrdinal("Ma"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ten"))) entity.Ten = reader.GetString(reader.GetOrdinal("Ten"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDangKy"))) entity.SoLuongDangKy = reader.GetDecimal(reader.GetOrdinal("SoLuongDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDaNhap"))) entity.SoLuongDaNhap = reader.GetDecimal(reader.GetOrdinal("SoLuongDaNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public int UpdateTransaction(SqlTransaction transaction, string dbName)
        {
            SetDabaseMoi(dbName);

            string spName = "p_GC_ThietBi_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, this._HopDong_ID);
            this.db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, this._Ma);
            this.db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, this._Ten);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@SoLuongDangKy", SqlDbType.Decimal, this._SoLuongDangKy);
            this.db.AddInParameter(dbCommand, "@SoLuongDaNhap", SqlDbType.Decimal, this._SoLuongDaNhap);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, this._TinhTrang);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, this._DonGia);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, this._TriGia);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, this._TrangThai);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

    }
}