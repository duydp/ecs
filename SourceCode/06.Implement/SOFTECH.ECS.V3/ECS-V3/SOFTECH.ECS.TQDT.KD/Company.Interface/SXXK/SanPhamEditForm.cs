﻿using System;
using Company.KD.BLL;
using Company.KD.BLL.SXXK;
using Company.KD.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Data;
using System.Resources;
using Company.Controls.CustomValidation;
using System.Threading;
using System.Globalization;

namespace Company.Interface.SXXK
{
    public partial class SanPhamEditForm : BaseForm
    {
        public HangHoaXuat HangXuat;
        public SanPhamEditForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            // Kiểm tra tính hợp lệ của mã HS.

            if (!cvError.IsValid) return;

            Company.KD.BLL.SXXK.HangHoaXuat spSXXK = new Company.KD.BLL.SXXK.HangHoaXuat();
            spSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            spSXXK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            spSXXK.Ma = txtMa.Text.Trim();
            if (spSXXK.Load())
            {
                MLMessages("Hàng hóa xuất này đã được đăng ký.", "MSG_STN09", "", false);
                return;
            }
            spSXXK.Ten = txtTen.Text.Trim();
            spSXXK.MaHS = txtMaHS.Text;
            spSXXK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            try
            {
                if (HangXuat != null)
                    spSXXK.InsertUpdateFull(HangXuat.Ma);
                else
                    spSXXK.InsertUpdateFull("");
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
                return;
            }
            this.Close();
        }

        private void SanPhamEditForm_Load(object sender, EventArgs e)
        {
            revMaHS.ValidationExpression = @"\d{8,10}";

            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            if (HangXuat != null)
            {
                txtMa.Text = HangXuat.Ma;
                txtMaHS.Text = HangXuat.MaHS;
                txtTen.Text = HangXuat.Ten;
                cbDonViTinh.SelectedValue = HangXuat.DVT_ID;
            }
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            //if (!MaHS.Validate(txtMaHS.Text, 10))
            //{
            //    error.SetIconPadding(txtMaHS, -8);
            //    error.SetError(txtMaHS, "Mã HS không hợp lệ.");
            //}
            //else
            //{
            //    error.SetError(txtMaHS, string.Empty);
            //}

        }

    }
}