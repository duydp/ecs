﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
#if SXXK_V4
using Company.Interface.SXXK;
using Company.KDT.SHARE.QuanLyChungTu.CX;
#elif GC_V4
using Company.Interface.GC;
using Company.GC.BLL.GC;
#endif

namespace Company.Interface.VNACCS
{
    public partial class VNACC_DieuChinhHangMauDichForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        public KDT_VNACC_HangMauDich HMD ;
        public bool isAddNew = true;
        public string loaiHangHoa = "N";
#if SXXK_V4
        NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#elif GC_V4
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
#endif
        public VNACC_DieuChinhHangMauDichForm()
        {
            InitializeComponent();
        }

        private void VNACC_DieuChinhHangMauDichForm_Load(object sender, EventArgs e)
        {
            BindData();
        }
        private void BindData()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = TKMD.HangCollection;
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                ctrMaSoHang.SetValidate = !isOnlyWarning;
                ctrMaSoHang.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrMaSoHang.IsValidate; //"Mã số hàng hóa";
                //Đơn vị tính số lượng
                ctrDVTLuong1.SetValidate = !isOnlyWarning; ctrDVTLuong1.IsOnlyWarning = isOnlyWarning;
                isValid &= ctrDVTLuong1.IsValidate;

                isValid &= ValidateControl.ValidateNull(txtTenHang, errorProvider, "Tên hàng");
                isValid &= ValidateControl.ValidateNull(txtSoLuong1, errorProvider, "Số lượng ", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if (HMD == null)
                {
                    HMD = new KDT_VNACC_HangMauDich();
                    isAddNew = true;
                }
                GetHangMD();
                if (isAddNew)
                    TKMD.HangCollection.Add(HMD);
                BindData();
                TKMD.InsertUpdateFull();
#if GC_V4
                Company.GC.BLL.VNACC.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(TKMD, GlobalSettings.MA_DON_VI != "4000395355");
#elif SXXK_V4
                Company.BLL.VNACCS.ConvertFromVNACCS.InsertUpdateTKMDFromVNACCS(TKMD);
#endif
                HMD = new KDT_VNACC_HangMauDich();
                txtMaHangHoa.Text = String.Empty;
                txtTenHang.Text = String.Empty;
                ctrMaSoHang.Code = String.Empty;
                txtSoLuong1.Value = 0;
                ctrDVTLuong1.Code = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetHangMD()
        {
            HMD.MaHangHoa = txtMaHangHoa.Text;
            HMD.TenHang = txtTenHang.Text;
            HMD.MaSoHang = ctrMaSoHang.Code;
            HMD.SoLuong1 = Convert.ToDecimal(txtSoLuong1.Value);
            HMD.DVTLuong1 = ctrDVTLuong1.Code;
            HMD.TriGiaHoaDon = Convert.ToDecimal(txtTriGiaHoaDon.Value);
            HMD.DonGiaHoaDon = Convert.ToDecimal(txtDonGiaHoaDon.Value);
            HMD.NuocXuatXu = ctrNuocXuatXu.Code;

            HMD.TriGiaTinhThue = Convert.ToDecimal(txtTriGiaTinhThue.Value);
            HMD.MaTTTriGiaTinhThue = ctrMaTTTriGiaTinhThue.Code;
            HMD.DonGiaTinhThue = Convert.ToDecimal(txtDonGiaTinhThue.Value);
            HMD.DV_SL_TrongDonGiaTinhThue = ctrDV_SL_TrongDonGiaTinhThue.Code;
            HMD.TriGiaTinhThueS = Convert.ToDecimal(txtTriGiaTinhThueS.Text);
            HMD.SoLuongTinhThue = Convert.ToDecimal(txtSoLuongTinhThue.Value);
            HMD.MaDVTDanhThue = ctrMaDVTDanhThue.Code;
        }
        private void SetHangMD()
        {
            txtMaHangHoa.Text = HMD.MaHangHoa;
            txtTenHang.Text = HMD.TenHang;
            ctrMaSoHang.Code = HMD.MaSoHang;
            txtSoLuong1.Value = HMD.SoLuong1;
            ctrDVTLuong1.Code = HMD.DVTLuong1;
            ctrNuocXuatXu.Code = HMD.NuocXuatXu;
            txtTriGiaHoaDon.Text = HMD.TriGiaHoaDon.ToString();
            txtDonGiaHoaDon.Text = HMD.DonGiaHoaDon.ToString();
            ctrMaTTDonGia.Code = HMD.MaTTDonGia;

            txtTriGiaTinhThue.Text = HMD.TriGiaTinhThue.ToString();
            ctrMaTTTriGiaTinhThue.Code = HMD.MaTTTriGiaTinhThue;
            txtDonGiaTinhThue.Text = HMD.DonGiaTinhThue.ToString();
            ctrDV_SL_TrongDonGiaTinhThue.Code = HMD.DV_SL_TrongDonGiaTinhThue;
            txtTriGiaTinhThueS.Text = HMD.TriGiaTinhThueS.ToString();
            txtSoLuongTinhThue.Text = HMD.SoLuongTinhThue.ToString();
            ctrMaDVTDanhThue.Code = HMD.MaDVTDanhThue;
        }
        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACC_HangMauDich> ItemColl = new List<KDT_VNACC_HangMauDich>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACC_HangMauDich)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACC_HangMauDich item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TKMD.HangCollection.Remove(item);
                    }
                    TKMD.InsertUpdateFull();
                    BindData();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (grList.GetRows().Length < 0) return;

                HMD = (KDT_VNACC_HangMauDich)grList.CurrentRow.DataRow;
                SetHangMD();
                isAddNew = false;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaHangHoa_ButtonClick(object sender, EventArgs e)
        {
            string MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            string MaHQVNACC = GlobalSettings.MA_HAI_QUAN_VNACCS;//VNACCS_Mapper.GetCodeVNACC(MaHaiQuan, "MaHQ"); //MaHaiQuan.Substring(1, 2).ToUpper() + MaHaiQuan.Substring(0, 1).ToUpper() + MaHaiQuan.Substring(3, 1).ToUpper();
#if SXXK_V4
            if (this.loaiHangHoa == "T" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            {

                if (this.NPLRegistedForm_CX == null)
                    this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
                this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm_CX.LoaiNPL = "3";
                this.NPLRegistedForm_CX.ShowDialog(this);
                if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
                    txtTenHang.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
                    ctrMaSoHang.Code = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.MaHS;
                    ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

                }

            }
            else if (this.loaiHangHoa == "N")
            {
                if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                this.NPLRegistedForm.CalledForm = "HangMauDichForm";
                this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.NPLRegistedForm.isBrowser = true;
                this.NPLRegistedForm.ShowDialog(this);
                if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
                {
                    txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                    txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                    ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                    ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

                }
            }
            else
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                    txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                    ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                    ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                }
            }
#elif GC_V4
            switch (this.loaiHangHoa)

            {
                case "N":
                    //if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.isBrower = true;
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = TKMD.HopDong_ID;
                    this.NPLRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.NPLRegistedForm.NguyenPhuLieuSelected.Ma))
                    {
                        txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        ctrMaSoHang.Code = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
                    }
                    break;
                case "S":
                    //if (this.SPRegistedForm == null)
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.SanPhamSelected.HopDong_ID = TKMD.HopDong_ID;
                    this.SPRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                    {
                        txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        ctrMaSoHang.Code = this.SPRegistedForm.SanPhamSelected.MaHS;
                        ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
                    }
                    break;
                    
                case "T":
                    ThietBiRegistedForm ftb = new ThietBiRegistedForm();
                    ftb.ThietBiSelected.HopDong_ID = TKMD.HopDong_ID;
                    ftb.isBrower = true;
                    ftb.ShowDialog();
                    if (!string.IsNullOrEmpty(ftb.ThietBiSelected.Ma))
                    {
                        txtMaHangHoa.Text = ftb.ThietBiSelected.Ma;
                        txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        ctrMaSoHang.Code = ftb.ThietBiSelected.MaHS;
                        ctrDVTLuong1.Code = this.DVT_VNACC(ftb.ThietBiSelected.DVT_ID.PadRight(3));
                    }
                    break;
                case "H":
                    HangMauRegistedForm hangmauForm = new HangMauRegistedForm();
                    hangmauForm.isBrower = true;
                    hangmauForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    hangmauForm.HangMauSelected.HopDong_ID = TKMD.HopDong_ID;
                    hangmauForm.ShowDialog();
                    if (!string.IsNullOrEmpty(hangmauForm.HangMauSelected.Ma))
                    {
                        txtMaHangHoa.Text = hangmauForm.HangMauSelected.Ma;
                        txtTenHang.Text = hangmauForm.HangMauSelected.Ten;
                        ctrMaSoHang.Code = hangmauForm.HangMauSelected.MaHS;
                        ctrDVTLuong1.Code = this.DVT_VNACC(hangmauForm.HangMauSelected.DVT_ID.PadRight(3));
                    }
                    break;
            }
#endif
        }

        private void txtMaHangHoa_Leave(object sender, EventArgs e)
        {
#if GC_V4
            bool isNotFound = false;
            if (this.loaiHangHoa == "N")
            {

                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.HopDong_ID = TKMD.HopDong_ID;
                npl.Ma = txtMaHangHoa.Text.Trim();
                if (npl.Load())
                {
                    txtMaHangHoa.Text = npl.Ma;
                    ctrMaSoHang.Code = npl.MaHS;
                    txtTenHang.Text = npl.Ten;
                    ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));

                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.loaiHangHoa == "S")
            {
                SanPham sp = new SanPham();
                sp.HopDong_ID = TKMD.HopDong_ID;
                sp.Ma = txtMaHangHoa.Text.Trim();
                if (sp.Load())
                {
                    txtMaHangHoa.Text = sp.Ma;
                    ctrMaSoHang.Code = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.loaiHangHoa == "T")
            {
                ThietBi tb = new ThietBi();
                tb.HopDong_ID = TKMD.HopDong_ID;
                tb.Ma = txtMaHangHoa.Text.Trim();
                if (tb.Load())
                {
                    txtMaHangHoa.Text = tb.Ma;
                    ctrMaSoHang.Code = tb.MaHS;
                    txtTenHang.Text = tb.Ten;
                    ctrDVTLuong1.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.loaiHangHoa == "H")
            {
                HangMau hangmau = HangMau.Load(TKMD.HopDong_ID, txtMaHangHoa.Text.Trim());
                if (hangmau != null)
                {
                    txtMaHangHoa.Text = hangmau.Ma;
                    ctrMaSoHang.Code = hangmau.MaHS;
                    txtTenHang.Text = hangmau.Ten;
                    ctrDVTLuong1.Code = this.DVT_VNACC(hangmau.DVT_ID.PadRight(3));

                }
                else isNotFound = true;

            }
            if (isNotFound)
            {

                txtTenHang.Text = ctrMaSoHang.Code = string.Empty;
                return;
            }
#elif SXXK_V4

            if (loaiHangHoa == "T" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            {
                IList<HangDuaVao> listTB = HangDuaVao.SelectCollectionDynamic(string.Format("Ma = '{0}' and LoaiNPL = '3'", txtMaHangHoa.Text.Trim()), null);
                if (listTB != null && listTB.Count > 0)
                {
                    HangDuaVao tb = listTB[0];
                    txtTenHang.Text = tb.Ten;
                    ctrMaSoHang.Code = tb.MaHS;
                    ctrDVTLuong1.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
                }
                else
                {
                }
            }
            else if (loaiHangHoa == "N")
            {
                Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                npl.Ma = txtMaHangHoa.Text;
                if (npl.Load())
                {
                    txtMaHangHoa.Text = npl.Ma;
                    ctrMaSoHang.Code = npl.MaHS;
                    txtTenHang.Text = npl.Ten;
                    ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
                }


            }
            else if (loaiHangHoa == "S")
            {
                Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                sp.Ma = txtMaHangHoa.Text;
                sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (sp.Load())
                {
                    txtMaHangHoa.Text = sp.Ma;
                    ctrMaSoHang.Code = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));

                }
            }
#endif
        }
    }
}
