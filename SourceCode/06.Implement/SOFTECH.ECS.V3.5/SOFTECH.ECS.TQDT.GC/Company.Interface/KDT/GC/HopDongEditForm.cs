﻿using System;
using System.Windows.Forms;
using Company.Interface.KDT.GC;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.GC.BLL;
using System.IO;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KD.BLL;
using Company.GC.BLL.KDT.SXXK;
using System.Text.RegularExpressions;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.QuanTri;
namespace Company.Interface.KDT.GC
{
    public partial class HopDongEditForm : BaseFormHaveGuidPanel
    {
        public HopDong HD;
        private string xmlCurrent = "";
        string msgInfor = string.Empty;
        FeedBackContent feedbackContent = null;
        public bool isphukien = false;
        public LogHistory Log = new LogHistory();
        public String Caption;
        public bool IsChange;
        public HopDongEditForm()
        {
            InitializeComponent();
            // Phương thức thanh toán.
            nguyenTeControl1.ValueChanged -= new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
            nuocHControl1.ValueChanged -= new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);
        }

        private void HopDongEditForm_Load(object sender, EventArgs e)
        {
            try
            {
                cbPTTT.DataSource = Company.KDT.SHARE.Components.DuLieuChuan.PhuongThucThanhToan.SelectAll().Tables[0];
                cbPTTT.DisplayMember = "GhiChu";
                cbPTTT.ValueMember = "ID";

                cbPTTT.TextChanged -= new EventHandler(txt_TextChanged);
                cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;
                cbPTTT.TextChanged += new EventHandler(txt_TextChanged);

                Caption = this.Text;
                if (HD == null)
                    HD = new HopDong { TrangThaiXuLy = -1 };
                if (HD.ID > 0)
                {
                    HD = HopDong.Load(HD.ID);
                    HD.LoadCollection();
                    txtSoTiepNhan.Text = HD.SoTiepNhan.ToString();
                    donViHaiQuanControl1.Ma = HD.MaHaiQuan;
                    if (HD.NgayTiepNhan.Year <= 1900)
                    {
                        ccNgayTiepNhan.Text = "";
                    }
                    else ccNgayTiepNhan.Value = HD.NgayTiepNhan;
                    Company.GC.BLL.KDT.SXXK.MsgSend msg = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    msg.master_id = HD.ID;
                    msg.LoaiHS = "HD";
                    cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                    txtSoHopDong.TextChanged -= new EventHandler(txt_TextChanged);
                    txtSoHopDong.Text = HD.SoHopDong.Trim();
                    txtSoHopDong.TextChanged += new EventHandler(txt_TextChanged);

                    ccNgayKetThucHD.TextChanged -= new EventHandler(txt_TextChanged);
                    ccNgayKetThucHD.Value = HD.NgayHetHan;
                    ccNgayKetThucHD.TextChanged += new EventHandler(txt_TextChanged);

                    ccNgayKyHD.TextChanged -= new EventHandler(txt_TextChanged);
                    ccNgayKyHD.Value = HD.NgayKy;
                    ccNgayKyHD.TextChanged += new EventHandler(txt_TextChanged);

                    nguyenTeControl1.ValueChanged -= new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);
                    nguyenTeControl1.Ma = HD.NguyenTe_ID;
                    nguyenTeControl1.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(txt_TextChanged);

                    nuocHControl1.ValueChanged -= new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);
                    nuocHControl1.Ma = HD.NuocThue_ID;
                    nuocHControl1.ValueChanged += new Company.Interface.Controls.NuocHControl.ValueChangedEventHandler(txt_TextChanged);

                    cbPTTT.TextChanged -= new EventHandler(txt_TextChanged);
                    cbPTTT.SelectedValue = HD.PTTT_ID == null ? "" : HD.PTTT_ID;
                    cbPTTT.TextChanged += new EventHandler(txt_TextChanged);

                    cbbIsGC.TextChanged -= new EventHandler(txt_TextChanged);
                    cbbIsGC.SelectedValue = HD.IsGiaCongNguoc.ToString();
                    cbbIsGC.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenDoiTac.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenDoiTac.Text = HD.TenDonViDoiTac == null ? HD.DonViDoiTac.Trim() : HD.TenDonViDoiTac.Trim();
                    txtTenDoiTac.TextChanged += new EventHandler(txt_TextChanged);

                    txtDCDT.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDCDT.Text = HD.DiaChiDoiTac == null ? String.Empty : HD.DiaChiDoiTac.Trim();
                    txtDCDT.TextChanged += new EventHandler(txt_TextChanged);

                    txtDVDT.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDVDT.Text = HD.DonViDoiTac == null ? String.Empty : HD.DonViDoiTac.Trim();
                    txtDVDT.TextChanged += new EventHandler(txt_TextChanged);

                    txtMaBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaBenNhan.Text = HD.MaDoanhNghiep;
                    txtMaBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenBenNhan.Text = HD.TenDoanhNghiep;
                    txtTenBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    txtDiaChiBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDiaChiBenNhan.Text = HD.DiaChiDoanhNghiep;
                    txtDiaChiBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    txtTongGiaTriSP.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTongGiaTriSP.Value = HD.TongTriGiaSP;
                    txtTongGiaTriSP.TextChanged += new EventHandler(txt_TextChanged);

                    txtTongTriGiaTienCong.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTongTriGiaTienCong.Value = HD.TongTriGiaTienCong;
                    txtTongTriGiaTienCong.TextChanged += new EventHandler(txt_TextChanged);

                    txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                    txtGhiChu.Text = HD.GhiChu == null ? String.Empty : HD.GhiChu.Trim();
                    txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

                }
                else
                {
                    txtSoTiepNhan.Text = "1000000000000";
                    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    cbbIsGC.TextChanged -= new EventHandler(txt_TextChanged);
                    cbbIsGC.SelectedIndex = 0;
                    cbbIsGC.TextChanged += new EventHandler(txt_TextChanged);

                    txtMaBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtMaBenNhan.Text = GlobalSettings.MA_DON_VI;
                    txtMaBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    txtTenBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtTenBenNhan.Text = GlobalSettings.TEN_DON_VI;
                    txtTenBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    txtDiaChiBenNhan.TextChanged -= new EventHandler(txt_TextChanged);
                    txtDiaChiBenNhan.Text = GlobalSettings.DIA_CHI;
                    txtDiaChiBenNhan.TextChanged += new EventHandler(txt_TextChanged);

                    HD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                }                
                dgNhomSanPham.Tables[0].Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;

                dgNhomSanPham.DataSource = HD.NhomSPCollection;
                dgNguyenPhuLieu.DataSource = HD.NPLCollection;
                dgSanPham.DataSource = HD.SPCollection;
                dgThietBi.DataSource = HD.TBCollection;
                dgHangMau.DataSource = HD.HangMauCollection;

                if (HD.ID == 0)
                    ccNgayTiepNhan.Value = DateTime.Now;
                else
                    ccNgayTiepNhan.Value = HD.NgayTiepNhan;
            }                

            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            setCommandStatus();
        }
        private void setCommandStatus()
        {
            if (HD.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdEdit.Enabled = cmdEdit.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendEdit.Enabled =Janus.Windows.UI.InheritableBoolean.False;
                NhapHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                txtSoTiepNhan.Text = HD.SoTiepNhan.ToString();
                ccNgayTiepNhan.Value = HD.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (HD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || HD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY || HD.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
            {
                lblTrangThai.Text = HD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Wait for approval") :
                    HD.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY ? setText("Chờ hủy", "Wait for cancel") : setText("Đã khai báo", "Declare yet");
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                HuyKhaiBao.Enabled = HD.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? Janus.Windows.UI.InheritableBoolean.False :
                     Janus.Windows.UI.InheritableBoolean.False;
                NhapHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                txtSoTiepNhan.Text = HD.SoTiepNhan.ToString();
                ccNgayTiepNhan.Value = HD.NgayTiepNhan;
                this.OpenType = OpenFormType.View;
            }
            else if (HD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY )
            {
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                NhapHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                txtSoTiepNhan.Text = HD.SoTiepNhan.ToString();
                ccNgayTiepNhan.Value = HD.NgayTiepNhan;
                this.OpenType = OpenFormType.View;
            }
            else if (HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                lblTrangThai.Text = setText("Không phê duyệt", "Not approved");
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                NhapHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                this.OpenType = OpenFormType.Edit;

            }              
            else if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || HD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                lblTrangThai.Text = HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Not Declare yet") :
    setText("Đang sửa", "Wait Edit");
                cmdEdit.Enabled = cmdEdit.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieuHD.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgNguyenPhuLieu.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                dgNhomSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                dgSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                NhapHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCopyHD.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                this.OpenType = OpenFormType.Edit;
            } 
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            
            switch (e.Command.Key)
            {
                case "cmdAddLoaiSanPham":
                    showFormLoaiSanPham();
                    tabHopDong.SelectedIndex = 0;
                    break;
                case "cmdAddThietBi":
                    showFormThietBi();
                    tabHopDong.SelectedIndex = 3;
                    break;
                case "cmdAddNguyenPhuLieu":
                    showFormNguyenPhuLieu();
                    tabHopDong.SelectedIndex = 1;
                    break;
                case "cmdAddSanPham":
                    showFormSanPham();
                    tabHopDong.SelectedIndex = 2;
                    break;
                case "cmdAddHangMau":
                    showFormHangMau();
                    tabHopDong.SelectedIndex = 4;
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdSend":
                    SendV5(false);
                    break;
                case "cmdSendEdit":
                    this.SendV5(false);
                    break;
                case "HuyKhaiBao":
                    this.SendV5(true);
                    break;
                case "NhanDuLieuHD":
                    FeedBackV5();
                    break;
                case "cmdNPLExcel":
                    NguyenPhuLieuImportExcel();
                    tabHopDong.SelectedIndex = 1;
                    break;
                case "cmdSPExcel":
                    SanPhamImportExcel();
                    tabHopDong.SelectedIndex = 2;
                    break;
                case "cmdThietBi":
                    ThietBiImportExcel();
                    tabHopDong.SelectedIndex = 3;
                    break;
                case "cmdLoaiSPGCExcel":
                    LoaiSPGCImportExcel();
                    tabHopDong.SelectedIndex = 0;
                    break;
                case "cmdCopyHD":
                    showFormHopDongRegisted();
                    tabHopDong.SelectedIndex = 0;
                    break;
                case "cmdCopyDM":
                    showFormDangKyDM();
                    tabHopDong.SelectedIndex = 2;
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdEdit":
                    this.EditHopDong();
                    break;
                case "cmdResult":
                    Result();
                    break;
                case "cmdUpdateGuidString":
                    UpdateGUIDSTR();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
            }
        }
        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = HD.ID;
                f.loaiKhaiBao = LoaiKhaiBao.DinhMuc;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", HD.ID, LoaiKhaiBao.HopDong), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageHDGC(HD);
                    HD.LoadCollection(HD);
                    if (HD.InsertUpdateHopDong())
                    {
                        HD.InsertUpdate();
                        Log.LogHDGC(HD, MessageTitle.LuuThongTin, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        ShowMessage("Cập nhật thông tin thành công .", false);
                    }                   
                    HopDongEditForm_Load(null,null);                    
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_GC_HopDong", "", Convert.ToInt32(HD.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void inPhieuTN()
        {
            try
            {
                if (this.HD.SoTiepNhan == 0) return;
                Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
                phieuTN.phieu = "HỢP ĐỒNG";
                phieuTN.soTN = this.HD.SoTiepNhan.ToString();
                phieuTN.ngayTN = this.HD.NgayTiepNhan.ToString("dd/MM/yyyy");
                phieuTN.maHaiQuan = donViHaiQuanControl1.Ma;
                phieuTN.BindReport();
                phieuTN.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataLoaiSPGC()
        {
            try
            {
                dgNhomSanPham.Refresh();
                dgNhomSanPham.DataSource = HD.NhomSPCollection;
                dgNhomSanPham.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataSanPham()
        {
            try
            {
                dgSanPham.Refresh();
                dgSanPham.DataSource = HD.SPCollection;
                dgSanPham.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataNPL()
        {
            try
            {
                dgNguyenPhuLieu.Refresh();
                dgNguyenPhuLieu.DataSource = HD.NPLCollection;
                dgNguyenPhuLieu.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataTB()
        {
            try
            {
                dgThietBi.Refresh();
                dgThietBi.DataSource = HD.TBCollection;
                dgThietBi.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataHM()
        {
            try
            {
                dgHangMau.Refresh();
                dgHangMau.DataSource = HD.HangMauCollection;
                dgHangMau.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoaiSPGCImportExcel()
        {
            try
            {
                Company.Interface.KDT.SXXK.NguyenPhuLieuReadExcelForm f = new Company.Interface.KDT.SXXK.NguyenPhuLieuReadExcelForm();
                f.HD = this.HD;
                f.ShowDialog();
                this.SetChange(f.ImportExcelSucces);
                BindDataLoaiSPGC();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void NguyenPhuLieuImportExcel()
        {
            try
            {
                Company.Interface.KDT.SXXK.NguyenPhuLieuReadExcelForm f = new Company.Interface.KDT.SXXK.NguyenPhuLieuReadExcelForm();
                f.HD = this.HD;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                this.SetChange(f.ImportExcelSucces);
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void SanPhamImportExcel()
        {
            try
            {
                if (HD.NhomSPCollection.Count == 0)
                {
                    showMsg("MSG_2702047");
                    return;
                }
                Company.Interface.KDT.SXXK.SanPhamReadExcelForm f = new Company.Interface.KDT.SXXK.SanPhamReadExcelForm();
                f.HD = this.HD;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                this.SetChange(f.ImportExcelSucces);
                BindDataSanPham();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void ThietBiImportExcel()
        {
            try
            {
                Company.Interface.KDT.SXXK.ThietBiReadExcelForm f = new Company.Interface.KDT.SXXK.ThietBiReadExcelForm();
                f.HD = this.HD;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                this.SetChange(f.ImportExcelSucces);
                BindDataTB();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private static bool Check(string str)
        {
            Regex re = new Regex(@"^[a-zA-Z0-9\+\/\-\ \.\,]*$");
            if (re.IsMatch(str))
                return true;
            else
                return false;
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (HD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || HD.TrangThaiXuLy ==TrangThaiXuLy.SUATKDADUYET || HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";   
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtSoHopDong, errorProvider, "Số hợp đồng", isOnlyWarning);
                isValid &= ValidateControl.ValidateDate(ccNgayKyHD, errorProvider, "Ngày hợp đồng",isOnlyWarning);
                isValid &= ValidateControl.ValidateDate(ccNgayKetThucHD, errorProvider, "Ngày hết hạn hợp đồng", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbIsGC, errorProvider, "Có phải gia công ngược không", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbPTTT, errorProvider, "Mã phương thức thanh toán", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenBenNhan, errorProvider, "Tên người nhận gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaBenNhan, errorProvider, "Mã người nhận gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDiaChiBenNhan, errorProvider, "Địa chỉ người nhận gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenDoiTac, errorProvider, "Tên người thuê gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDVDT, errorProvider, "Mã người thuê gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDCDT, errorProvider, "Địa chỉ người thuê gia công", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtTongGiaTriSP, errorProvider, "Tổng trị giá sản phẩm");
                isValid &= ValidateControl.ValidateZero(txtTongTriGiaTienCong, errorProvider, "Tổng trị giá tiền công");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private bool Save()
        {
            try
            {
                error.Clear();
                if (!ValidateForm(false))
                    return false;
                if (!Check(txtSoHopDong.Text.ToString().Trim()))
                {
                    error.SetError(txtSoHopDong, "Số HĐGC đã nhập chứa ký tự đặc biệt hoặc là UNICODE");
                    return false;
                }
                if (!Check(txtTenDoiTac.Text.ToString().Trim()))
                {
                    error.SetError(txtTenDoiTac, "Tên đối tác đã nhập chứa ký tự đặc biệt hoặc là UNICODE");
                    return false;
                }
                if (ccNgayKyHD.Value >= ccNgayKetThucHD.Value)
                {
                    error.SetError(ccNgayKetThucHD, setText("Ngày kết thúc phải lớn hơn ngày ký.", "Date of ending contract must be greater than signing's date"));
                    return false;
                }
                String Notes = String.Empty;
                Notes = "#&" + txtSoHopDong.Text.ToString().Trim() + "#&" + ccNgayKyHD.Value.ToString("ddMMyyyy") + "#&" + ccNgayKetThucHD.Value.ToString("ddMMyyyy") + "#&" + txtTenDoiTac.Text.ToString().Trim() + "#&";
                if (Notes.Length >= 100)
                {
                    error.SetError(txtSoHopDong, "Số HĐGC đã nhập hoặc Tên đối tác đã nhập vượt quá ký tự cho phép (100 ký tự) .Đối với HĐGC khi Khai báo Tờ khai , phần Ghi chú trên Tờ khai sẽ bao gồm : #& Số HĐGC #& Ngày HĐGC #& Ngày hết hạn #& Tên đối tác #& Nội dung ghi chú .Nội dung Ghi chú trên tờ khai của doanh nghiệp đang nhập đang vượt quá độ dài cho phép là 100 ký tự.");
                    return false;
                }
                if (HD.NhomSPCollection.Count == 0)
                {
                    showMsg("MSG_2702049");
                    return false;
                }
                if (HD.SoHopDong!=null)
                {
                    if (HD.SoHopDong.ToUpper() != txtSoHopDong.Text.Trim().ToUpper())
                    {
                        if (HD.checkSoHopDongExit(txtSoHopDong.Text.Trim(), donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI))
                        {
                            showMsg("MSG_2702050");
                            return false;
                        }
                    }
                }
                else
                {
                    if (HD.checkSoHopDongExit(txtSoHopDong.Text.Trim(), donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI))
                    {
                        showMsg("MSG_2702050");
                        return false;
                    }
                }
                this.Cursor = Cursors.WaitCursor;
                HD.MaDoanhNghiep = txtMaBenNhan.Text.Trim();
                HD.PTTT_ID = cbPTTT.SelectedValue.ToString();
                HD.MaHaiQuan = donViHaiQuanControl1.Ma;
                HD.NgayGiaHan = new DateTime(1900, 1, 1);
                HD.NgayTiepNhan = ccNgayTiepNhan.Value;
                HD.NgayDangKy = ccNgayTiepNhan.Value;
                HD.SoTiepNhan =Convert.ToInt64(txtSoTiepNhan.Text);
                HD.NgayHetHan = Convert.ToDateTime(ccNgayKetThucHD.Text);
                HD.NgayKy = Convert.ToDateTime(ccNgayKyHD.Text);
                HD.NguyenTe_ID = nguyenTeControl1.Ma;
                HD.NuocThue_ID = nuocHControl1.Ma;
                HD.SoHopDong = txtSoHopDong.Text.Trim();
                HD.DonViDoiTac = txtDVDT.Text.Trim();
                HD.DiaChiDoiTac = txtDCDT.Text.Trim();
                HD.TenDonViDoiTac = txtTenDoiTac.Text.Trim();
                HD.GhiChu = txtGhiChu.Text.Trim();
                HD.TongTriGiaSP = Convert.ToDouble(txtTongGiaTriSP.Value);
                HD.TongTriGiaTienCong = Convert.ToDouble(txtTongTriGiaTienCong.Value);
                HD.DiaChiDoanhNghiep = txtDiaChiBenNhan.Text.Trim();
                HD.TenDoanhNghiep = txtTenBenNhan.Text.Trim();
                HD.IsGiaCongNguoc = Convert.ToInt32(cbbIsGC.SelectedValue);
                if (string.IsNullOrEmpty(HD.GUIDSTR))
                    HD.GUIDSTR = Guid.NewGuid().ToString();
                if (HD.InsertUpdateHopDong())
                {
                    HD.InsertUpdate();
                    Log.LogHDGC(HD,MessageTitle.LuuThongTin, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    showMsg("MSG_SAV02");                    
                }
                else
                    showMsg("MSG_2702002");
                setCommandStatus();
                this.SetChange(false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail("Lưu hợp đồng", HD.MaHaiQuan, new SendEventArgs(ex));
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
            return true;
        }
        private void showForm(string formName)
        {
            System.Windows.Forms.Form f = null;
            switch (formName)
            {
                case "LoaiSanPhamGCEditForm":
                    f = new LoaiSanPhamGCEditForm();
                    f.ShowDialog();
                    break;
                case "NguyenPhuLieuGCEditForm":
                    f = new NguyenPhuLieuGCEditForm();
                    f.ShowDialog();
                    break;
                case "SanPhamGCEditForm":
                    f = new SanPhamGCEditForm();
                    f.ShowDialog();
                    break;
                case "ThietBiGCEditForm":
                    f = new ThietBiGCEditForm();
                    f.ShowDialog();
                    break;
            }
        }
        private void showFormLoaiSanPham()
        {
            try
            {
                LoaiSanPhamGCEditForm f = null;
                f = new LoaiSanPhamGCEditForm();
                f.HD = HD;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                this.SetChange(f.IsChange);
                BindDataLoaiSPGC();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void showFormThietBi()
        {
            try
            {
                ThietBiGCEditForm f = null;
                f = new ThietBiGCEditForm();
                f.HD = HD;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                this.SetChange(f.IsChange);
                BindDataTB();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void showFormHangMau()
        {
            try
            {
                HangMauForm f = null;
                f = new HangMauForm();
                f.HD = HD;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                this.SetChange(f.IsChange);
                BindDataHM();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void showFormSanPham()
        {
            try
            {
                if (HD.NhomSPCollection.Count == 0)
                {
                    showMsg("MSG_2702047");
                    return;
                }
                SanPhamGCEditForm f = null;
                f = new SanPhamGCEditForm();
                f.HD = HD;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                this.SetChange(f.IsChange);
                BindDataSanPham();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }
        private void showFormNguyenPhuLieu()
        {
            try
            {
                NguyenPhuLieuGCEditForm npl = new NguyenPhuLieuGCEditForm();
                npl.HD = HD;
                npl.OpenType = this.OpenType;
                npl.ShowDialog();
                this.SetChange(npl.IsChange);
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        /*
         * Hiển thị form Hợp đồng đã đăng ký
         * 
         */
        private void showFormHopDongRegisted()
        {
            try
            {
                HopDongRegistedCopyForm registedHD = new HopDongRegistedCopyForm();
                registedHD.ShowDialog();
                this.HD = registedHD.HopDongSelected.copyHD();
                dgNhomSanPham.DataSource = this.HD.NhomSPCollection;
                dgSanPham.DataSource = this.HD.SPCollection;
                dgNguyenPhuLieu.DataSource = this.HD.NPLCollection;
                dgThietBi.DataSource = this.HD.TBCollection;
                dgHangMau.DataSource = this.HD.HangMauCollection;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void showFormDangKyDM()
        {
            try
            {
                DinhMucGCSendForm dm = new DinhMucGCSendForm();
                dm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgNhomSanPham_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                LoaiSanPhamGCEditForm f = null;
                f = new LoaiSanPhamGCEditForm();
                f.HD = HD;
                NhomSanPham nhom = (NhomSanPham)e.Row.DataRow;
                f.nhom = nhom;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                this.SetChange(f.IsChange);
                BindDataLoaiSPGC();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }            

        }
        
        private void dgNguyenPhuLieu_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                NguyenPhuLieu npl = (NguyenPhuLieu)e.Row.DataRow;
                NguyenPhuLieuGCEditForm nplForm = new NguyenPhuLieuGCEditForm();
                nplForm.HD = HD;
                nplForm.npl = npl;
                nplForm.OpenType = this.OpenType;
                nplForm.ShowDialog();
                this.SetChange(nplForm.IsChange);
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgSanPham_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["TenNhomSanPham"].Text = this.NhomSanPham_GetName(e.Row.Cells["NhomSanPham_ID"].Value);
                    e.Row.Cells["NhomSanPham_ID"].Text = e.Row.Cells["TenNhomSanPham"].Text;
                    e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgSanPham_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    SanPhamGCEditForm f = null;
                    f = new SanPhamGCEditForm();
                    SanPham SPDetail = (SanPham)e.Row.DataRow;
                    f.HD = HD;
                    f.SPDetail = SPDetail;
                    f.OpenType = this.OpenType;
                    f.ShowDialog();
                    this.SetChange(f.IsChange);
                }
                BindDataSanPham();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        
        private void dgThietBi_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                ThietBi tbDetail = (ThietBi)e.Row.DataRow;
                tbDetail.HopDong_ID = HD.ID;

                ThietBiGCEditForm f = null;
                f = new ThietBiGCEditForm();
                f.HD = HD;
                f.tbDetail = tbDetail;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                this.SetChange(f.IsChange);
                BindDataTB();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }            

        }

        private void dgThietBi_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    showMsg("MSG_2702051");
                    e.Cancel = true;
                    return;
                }
                if (e.Row.RowType == RowType.Record)
                {
                    if (showMsg("MSG_DEL01", true) == "Yes")
                    {
                        ThietBi tb = (ThietBi)e.Row.DataRow;
                        tb.Delete();
                        Company.GC.BLL.GC.GC_ThietBi.DeleteGC_ThietBi(HD.ID, tb.Ma);
                        HD.TBCollection.Remove(tb);
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                BindDataTB();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgThietBi_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
                e.Row.Cells["TenNguyenTe"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe_ID"].Value).Trim();
                e.Row.Cells["XuatXu"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value).Trim();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgNguyenPhuLieu_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
                
        private void dgNhomSanPham_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    showMsg("MSG_2702051");
                    e.Cancel = true;
                    return;
                }
                string st = "";
                GridEXSelectedItemCollection items = dgNhomSanPham.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                    foreach (SanPham sp in HD.SPCollection)
                    {
                        if (sp.NhomSanPham_ID.Trim().ToUpper() == nhom.MaSanPham.Trim().ToUpper())
                        {
                            if (st.IndexOf(sp.Ma) < 0)
                                st += sp.Ma + ";";
                        }
                    }
                }
                bool ok = false;
                if (st == "")
                {
                    if (showMsg("MSG_DEL01", true) == "Yes")
                    {
                        ok = true;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                else
                {
                    if (showMsg("MSG_240239", st, true) == "Yes")
                    {
                        ok = true;
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                if (ok)
                {
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                        if (HD.SPCollection.Count > 0)
                        {
                            SanPham sp = new SanPham();
                            sp.HopDong_ID = HD.ID;
                            sp.DeleteByNhomSanPhamTransaction(null, nhom.MaSanPham);
                            Company.GC.BLL.GC.GC_NhomSanPham.DeleteGC_NhomSanPham(HD.ID, nhom.MaSanPham);
                            SanPhamCollection SPCollectionTMP = new SanPhamCollection();
                            foreach (SanPham spDelete in HD.SPCollection)
                            {
                                if (spDelete.NhomSanPham_ID.Trim().ToUpper() == nhom.MaSanPham.Trim().ToUpper())
                                {
                                    SPCollectionTMP.Add(spDelete);
                                }
                            }
                            foreach (SanPham spDelete in SPCollectionTMP)
                            {
                                foreach (SanPham SPHopDong in HD.SPCollection)
                                {
                                    if (SPHopDong.Ma.Trim().ToUpper() == spDelete.Ma.Trim().ToUpper())
                                    {
                                        Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID, SPHopDong.Ma);
                                        KDT_GC_SanPham.DeleteKDT_GC_SanPham(HD.ID, SPHopDong.Ma);
                                        HD.SPCollection.Remove(SPHopDong);
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            nhom.Delete();
                            Company.GC.BLL.GC.GC_NhomSanPham.DeleteGC_NhomSanPham(HD.ID, nhom.MaSanPham);
                        }
                        HD.NhomSPCollection.Remove(nhom);
                        this.SetChange(true);
                    }
                    BindDataLoaiSPGC();
                    BindDataSanPham();
                }
                else e.Cancel = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
            
        }

        private void dgNguyenPhuLieu_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    showMsg("MSG_2702051");
                    e.Cancel = true;
                    return;
                }
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgNguyenPhuLieu.SelectedItems;
                    List<NguyenPhuLieu> ItemDelete = new List<NguyenPhuLieu>();
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.NguyenPhuLieu nplDelete = (Company.GC.BLL.KDT.GC.NguyenPhuLieu)row.GetRow().DataRow;
                        if (NguyenPhuLieu.CheckExitNPLinDinhMuc(nplDelete.Ma, donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI, this.HD.ID))
                        {
                            if (ShowMessage("Nguyên phụ liệu :  " + nplDelete.Ma + " đã khai định mức. Doanh nghiệp có muốn xóa không", true) == "Yes")
                            {
                                nplDelete.Delete();
                                Company.GC.BLL.GC.GC_NguyenPhuLieu.DeleteGC_NguyenPhuLieu(HD.ID,nplDelete.Ma);
                                HD.NPLCollection.Remove(nplDelete);
                                ItemDelete.Add(nplDelete);
                            }
                        }
                        else
                        {
                            nplDelete.Delete();
                            Company.GC.BLL.GC.GC_NguyenPhuLieu.DeleteGC_NguyenPhuLieu(HD.ID, nplDelete.Ma);
                            HD.NPLCollection.Remove(nplDelete);
                            ItemDelete.Add(nplDelete);
                        }
                    }
                    if (ItemDelete.Count>=1)
                        Log.LogHDGC(HD, ItemDelete, MessageTitle.XoaNguyenPhuLieu,((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    BindDataNPL();
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
            
        }

        private void dgThietBi_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    showMsg("MSG_2702051");
                    e.Cancel = true;
                    return;
                }
                {
                    if (showMsg("MSG_DEL01", true) == "Yes")
                    {
                        GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
                        List<ThietBi> ItemDelete = new List<ThietBi>();
                        foreach (GridEXSelectedItem row in items)
                        {
                            Company.GC.BLL.KDT.GC.ThietBi tbDelete = (Company.GC.BLL.KDT.GC.ThietBi)row.GetRow().DataRow;
                            tbDelete.Delete();
                            Company.GC.BLL.GC.GC_ThietBi.DeleteGC_ThietBi(HD.ID,tbDelete.Ma);
                            HD.TBCollection.Remove(tbDelete);
                            ItemDelete.Add(tbDelete);
                        }
                        if (ItemDelete.Count >= 1)
                            Log.LogHDGC(HD, ItemDelete, MessageTitle.XoaThietBi, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        this.SetChange(true);
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                    BindDataTB();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgSanPham_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    showMsg("MSG_2702051");
                    e.Cancel = true;
                    return;
                }
                if (showMsg("MSG_DEL01", true) == "Yes")
                {

                    GridEXSelectedItemCollection items = dgSanPham.SelectedItems;
                    SanPhamCollection ItemDelete = new SanPhamCollection();
                    foreach (GridEXSelectedItem row in items)
                    {
                        if (row.RowType == RowType.Record)
                        {
                            SanPham sp1 = (SanPham)row.GetRow().DataRow;
                            if (SanPham.CheckExitSPinDinhMuc(sp1.Ma, HD.MaHaiQuan, GlobalSettings.MA_DON_VI, HD.ID))
                            {
                                if (showMsg("MSG_0203076", sp1.Ma, true) == "Yes")
                                {
                                    sp1.Delete();
                                    Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID,sp1.Ma);
                                    HD.SPCollection.Remove(sp1);
                                    ItemDelete.Add(sp1);
                                }
                            }
                            else
                            {
                                sp1.Delete();
                                Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID, sp1.Ma);
                                HD.SPCollection.Remove(sp1);
                                ItemDelete.Add(sp1);
                            }
                        }
                    }
                    if (ItemDelete.Count >= 1)
                        Log.LogHDGC(HD, ItemDelete, MessageTitle.XoaSanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    BindDataSanPham();
                    this.SetChange(true);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }            
        }

        private void HopDongEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Hợp đồng Gia công có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.Save();
                }
            }
        }

        private void btnClickEvent(object sender, EventArgs e)
        {
            Janus.Windows.EditControls.UIButton button = (Janus.Windows.EditControls.UIButton)(sender);
            switch (button.Name)
            {
                case "btnThemSPGC":
                    showFormLoaiSanPham();
                    break;
                case "btnThemNguyenPhuLieu":
                    showFormNguyenPhuLieu();
                    break;
                case "btnThemSanPham":
                    showFormSanPham();
                    break;
                case "btnThemThietBi":
                    showFormThietBi();
                    break;
                case "btnImportNPL":
                    NguyenPhuLieuImportExcel();
                    break;
                case "btnImportSP":
                    SanPhamImportExcel();
                    break;
                case "btnImportTB":
                    ThietBiImportExcel();
                    break;
                case "btnThemHangMau":
                    showFormHangMau();
                    break;
            }
        }       

        private void btnXoaSP_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    showMsg("MSG_2702051");
                    return;
                }
                GridEXSelectedItemCollection items = dgSanPham.SelectedItems;
                SanPhamCollection SPCollectionTMP = new SanPhamCollection();
                if (items.Count < 0) return;
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    foreach (GridEXSelectedItem row in items)
                    {
                        if (row.RowType == RowType.Record)
                        {
                            SanPham sp1 = (SanPham)row.GetRow().DataRow;
                            if (SanPham.CheckExitSPinDinhMuc(sp1.Ma, donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI, HD.ID))
                            {
                                if (showMsg("MSG_0203076", sp1.Ma, true) == "Yes")
                                {
                                    sp1.Delete();
                                    Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID, sp1.Ma);
                                    SPCollectionTMP.Add(sp1);
                                }
                            }
                            else
                            {
                                sp1.Delete();
                                Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID, sp1.Ma);
                                SPCollectionTMP.Add(sp1);
                            }
                        }
                    }
                    foreach (SanPham spDelete in SPCollectionTMP)
                        HD.SPCollection.Remove(spDelete);
                    BindDataSanPham();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }            
        }
        
        private void btnXoa_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    showMsg("MSG_2702051");
                    return;
                }
                string st = "";
                GridEXSelectedItemCollection items = dgNhomSanPham.SelectedItems;
                if (items.Count < 0) return;
                foreach (GridEXSelectedItem row in items)
                {
                    Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                    foreach (SanPham sp in HD.SPCollection)
                    {
                        if (sp.NhomSanPham_ID.Trim().ToUpper() == nhom.MaSanPham.Trim().ToUpper())
                        {
                            if (st.IndexOf(sp.Ma) < 0)
                                st += sp.Ma + ";";
                        }
                    }
                }
                bool ok = false;
                if (st == "")
                {
                    if (showMsg("MSG_DEL01", true) == "Yes")
                    {
                        ok = true;
                    }
                }
                else
                {
                    if (showMsg("MSG_240239", st, true) == "Yes")
                    {
                        ok = true;
                    }

                }
                if (ok)
                {
                    List<NhomSanPham> NhomSPCollection = new List<NhomSanPham>();
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.NhomSanPham nhom = (Company.GC.BLL.KDT.GC.NhomSanPham)row.GetRow().DataRow;
                        NhomSPCollection.Add(nhom);
                        if (HD.SPCollection.Count > 0)
                        {
                            SanPham sp = new SanPham();
                            sp.HopDong_ID = HD.ID;
                            sp.DeleteByNhomSanPhamTransaction(null, nhom.MaSanPham);
                            Company.GC.BLL.GC.GC_NhomSanPham.DeleteGC_NhomSanPham(HD.ID, nhom.MaSanPham);
                            SanPhamCollection SPCollectionTMP = new SanPhamCollection();
                            foreach (SanPham spDelete in HD.SPCollection)
                            {
                                if (spDelete.NhomSanPham_ID.Trim().ToUpper() == nhom.MaSanPham.Trim().ToUpper())
                                {
                                    SPCollectionTMP.Add(spDelete);
                                }
                            }
                            foreach (SanPham spDelete in SPCollectionTMP)
                            {
                                foreach (SanPham SPHopDong in HD.SPCollection)
                                {
                                    if (SPHopDong.Ma.Trim().ToUpper() == spDelete.Ma.ToUpper().Trim())
                                    {
                                        Company.GC.BLL.GC.GC_SanPham.DeleteGC_SanPham(HD.ID, SPHopDong.Ma);
                                        KDT_GC_SanPham.DeleteKDT_GC_SanPham(HD.ID, SPHopDong.Ma);
                                        HD.SPCollection.Remove(SPHopDong);
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        {
                            nhom.Delete();
                            Company.GC.BLL.GC.GC_NhomSanPham.DeleteGC_NhomSanPham(HD.ID, nhom.MaSanPham);
                        }
                    }
                    if (NhomSPCollection.Count > 0)
                    {
                        foreach (NhomSanPham nhomSP in NhomSPCollection)
                        {
                            foreach (NhomSanPham nhomSPHopDong in HD.NhomSPCollection)
                            {
                                if (nhomSPHopDong.MaSanPham.Trim().ToUpper() == nhomSP.MaSanPham.Trim().ToUpper())
                                {
                                    HD.NhomSPCollection.Remove(nhomSPHopDong);
                                    break;
                                }
                            }

                        }
                    }
                    BindDataLoaiSPGC();
                    BindDataSanPham();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
            
        }

        private void btnXoaNPL_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    showMsg("MSG_2702051");
                    return;
                }
                GridEXSelectedItemCollection items = dgNguyenPhuLieu.SelectedItems;
                List<NguyenPhuLieu> NPLCollectionTMP = new List<NguyenPhuLieu>();
                if (items.Count > 0)
                {
                    if (showMsg("MSG_DEL01", true) == "Yes")
                    {

                        foreach (GridEXSelectedItem row in items)
                        {
                            Company.GC.BLL.KDT.GC.NguyenPhuLieu nplDelete = (Company.GC.BLL.KDT.GC.NguyenPhuLieu)row.GetRow().DataRow;
                            if (NguyenPhuLieu.CheckExitNPLinDinhMuc(nplDelete.Ma, donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI, this.HD.ID))
                            {
                                if (showMsg("MSG_0203077", nplDelete.Ma, true) == "Yes")
                                {
                                    nplDelete.Delete();
                                    Company.GC.BLL.GC.GC_NguyenPhuLieu.DeleteGC_NguyenPhuLieu(HD.ID, nplDelete.Ma);
                                    NPLCollectionTMP.Add(nplDelete);
                                }
                            }
                            else
                            {
                                nplDelete.Delete();
                                Company.GC.BLL.GC.GC_NguyenPhuLieu.DeleteGC_NguyenPhuLieu(HD.ID, nplDelete.Ma);
                                NPLCollectionTMP.Add(nplDelete);
                            }
                        }
                        foreach (NguyenPhuLieu NPL in NPLCollectionTMP)
                        {
                            HD.NPLCollection.Remove(NPL);
                        }
                        BindDataNPL();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }            

        }

        private void btnXoaTB_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    showMsg("MSG_2702051");
                    return;
                }
                List<ThietBi> tbColl = new List<ThietBi>();
                GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
                if (items.Count > 0)
                {
                    if (showMsg("MSG_DEL01", true) == "Yes")
                    {

                        foreach (GridEXSelectedItem row in items)
                        {
                            Company.GC.BLL.KDT.GC.ThietBi tbDelete = (Company.GC.BLL.KDT.GC.ThietBi)row.GetRow().DataRow;
                            tbDelete.Delete();
                            Company.GC.BLL.GC.GC_ThietBi.DeleteGC_ThietBi(HD.ID, tbDelete.Ma);
                            tbColl.Add(tbDelete);
                        }
                        foreach (Company.GC.BLL.KDT.GC.ThietBi tbt in tbColl)
                        {
                            HD.TBCollection.Remove(tbt);
                        }
                        BindDataTB();
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }           

        }

        private void btnXoaHangMau_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    showMsg("MSG_2702051");
                    return;
                }
                List<HangMau> tbColl = new List<HangMau>();
                GridEXSelectedItemCollection items = dgHangMau.SelectedItems;
                if (items.Count > 0)
                {
                    if (showMsg("Doanh nghiệp có muốn xóa hàng mẫu này không", true) == "Yes")
                    {

                        foreach (GridEXSelectedItem row in items)
                        {
                            HangMau tbDelete = (HangMau)row.GetRow().DataRow;
                            tbDelete.Delete();
                            Company.GC.BLL.GC.GC_HangMau.DeleteGC_HangMau(HD.ID,tbDelete.Ma);
                            tbColl.Add(tbDelete);
                        }
                        foreach (HangMau tbt in tbColl)
                        {
                            HD.HangMauCollection.Remove(tbt);
                        }
                        BindDataHM();
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }            

        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            try
            {
                TinhToanNhuCauNguyenPhuLieuForm f = new TinhToanNhuCauNguyenPhuLieuForm();
                f.HD = HD;
                f.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }
        //----------------------------------------------------------------------------------------------------------------
        #region Send V5 Create by LANNT
        private void SendV5(bool isCancel)
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.HopDong;
            sendXML.master_id = HD.ID;
            if (HD.ID == 0)
            {
                ShowMessage("Vui lòng lưu hợp đồng trước khi khai báo",false);
                return;
            }

            if (sendXML.Load())
            {
                if (HD.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống ", "Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    NhanDuLieuHD.Enabled = NhanDuLieuHD1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;   
                }
            }
            if (HD.NhomSPCollection.Count == 0)
            {
                ShowMessageTQDT(" Thông báo từ hệ thống ","Doanh nghiệp chưa nhập thông tin về nhóm sản phẩm.", false); 
                return;
            }
            try
            {

                HD.LoadHD(HD.ID);
                HD.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.GC_HopDong hd = Company.GC.BLL.DataTransferObjectMapper.Mapper.ToDataTransferObject_GC_HopDong(HD, GlobalSettings.DIA_CHI,isCancel);
                ObjectSend msgSend = new ObjectSend(
                               new Company.KDT.SHARE.Components.NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = HD.MaDoanhNghiep
                               }
                                 , new Company.KDT.SHARE.Components.NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HD.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(HD.MaHaiQuan).Trim() : HD.MaHaiQuan.Trim()
                                 }
                              ,
                                new Company.KDT.SHARE.Components.SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.DeclarationIssuer.HOP_DONG_GIA_CONG,
                                    Function = hd.Function,
                                    Reference = HD.GUIDSTR,
                                }
                                ,
                                hd);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.HopDong;
                    sendXML.master_id = HD.ID;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.func = Convert.ToInt32(hd.Function);
                    sendXML.InsertUpdate();
                    setCommandStatus();
                    if (feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        sendForm.Message.XmlSaveMessage(HD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHopDong);
                        NhanDuLieuHD.Enabled = NhanDuLieuHD1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN)
                        {
                            HD.Update();
                            setCommandStatus();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            setCommandStatus();
                            FeedBackV5();
                        }
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                    {
                        sendForm.Message.XmlSaveMessage(HD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHopDong);
                        sendForm.Message.XmlSaveMessage(HD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQLoiKetNoi, msgInfor);
                        ShowMessageTQDT(msgInfor, false);
                        setCommandStatus();
                    }
                }
                else
                {
                    sendForm.Message.XmlSaveMessage(HD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHQTuChoiHopDong);
                    ShowMessageTQDT(msgInfor, false);
                    setCommandStatus();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(HD.MaHaiQuan, new SendEventArgs(ex));
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void FeedBackV5()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.HopDong;
            sendXML.master_id = HD.ID;
            if (!sendXML.Load())
            {
                ShowMessageTQDT("Thông báo từ hệ thống", "Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
                return;
            }
            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.HOP_DONG_GIA_CONG,
                Reference = HD.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.HOP_DONG_GIA_CONG,

            };

            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = HD.TenDoanhNghiep,
                                            Identity = HD.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HD.MaHaiQuan.Trim()),
                                              Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(HD.MaHaiQuan).Trim() : HD.MaHaiQuan.Trim()
                                              //Identity = HD.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                    {
                        HD.Update();
                        isFeedBack = false;
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        HD.Update();
                        isFeedBack = false;
                        setCommandStatus();
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nDoanh nghiệp có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                }
            }


        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.HopDongSendHandler(HD, ref msgInfor, e);

        }
        /// <summary>
        /// Hủy Thông Tin đến Hải Quan
        /// </summary>
        private void CancelV5()
        {
            Company.KDT.SHARE.Components.DeclarationBase npl = Company.GC.BLL.DataTransferObjectMapper.Mapper.HuyKhaiBao(Company.KDT.SHARE.Components.DeclarationIssuer.HOP_DONG_GIA_CONG, HD.GUIDSTR, HD.SoTiepNhan, HD.MaHaiQuan, HD.NgayTiepNhan);
            ObjectSend msgSend = new ObjectSend(
                           new Company.KDT.SHARE.Components.NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = HD.MaDoanhNghiep
                           }
                             , new Company.KDT.SHARE.Components.NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(HD.MaHaiQuan),
                                 Identity = HD.MaHaiQuan
                             }
                          ,
                            new Company.KDT.SHARE.Components.SubjectBase()
                            {
                                Type = Company.KDT.SHARE.Components.DeclarationIssuer.HOP_DONG_GIA_CONG,
                                Function = Company.KDT.SHARE.Components.DeclarationFunction.HUY,
                                Reference = HD.GUIDSTR,
                            }
                            ,
                            npl);
            SendMessageForm sendForm = new SendMessageForm();
            HD.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.HUY_KHAI_BAO;
            sendForm.Send += SendMessage;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && HD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                sendForm.Message.XmlSaveMessage(HD.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoHuyHopDong);
                FeedBackV5();
                HD.Update();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }
        public void EditHopDong()
        {
            try
            {
                string msg = "";
                msg += "-------------Thông tin hợp đồng đã khai báo-------------";
                msg += "\nSố tiếp nhận : " + HD.SoTiepNhan.ToString();
                msg += "\nNgày tiếp nhận : " + HD.NgayTiepNhan.ToString("dd/MM/yy hh:mm:ss");
                msg += "\nHải quan tiếp nhận : " + HD.MaHaiQuan.ToString();
                msg += "\n--------------------Thông tin xác nhận--------------------";
                msg += "\nDoanh nghiệp có muốn chuyển sang Khai báo sửa không ?";
                if (ShowMessageTQDT(" Thông báo từ hệ thống ",msg, true) == "Yes")
                {
                    HD.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    HD.ActionStatus = (short)ActionStatus.HopDongSua;
                    HD.Update();
                    setCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        #endregion

        private void dgHangMau_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgHangMau_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                HangMau hangmau = (HangMau)e.Row.DataRow;
                hangmau.HopDong_ID = HD.ID;

                HangMauForm f = null;
                f = new HangMauForm();
                f.HD = HD;
                f.hmDetail = hangmau;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                this.SetChange(f.IsChange);
                BindDataHM();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }

        private void dgHangMau_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    showMsg("MSG_2702051");
                    e.Cancel = true;
                    return;
                }
                if (showMsg("MSG_DEL01", true) == "Yes")
                {

                    GridEXSelectedItemCollection items = dgHangMau.SelectedItems;
                    List<HangMau> ItemDelete = new List<HangMau>();
                    foreach (GridEXSelectedItem row in items)
                    {
                        if (row.RowType == RowType.Record)
                        {
                            HangMau hm = (HangMau)row.GetRow().DataRow;
                            hm.Delete();
                            Company.GC.BLL.GC.GC_HangMau.DeleteGC_HangMau(HD.ID,hm.Ma);
                            HD.HangMauCollection.Remove(hm);
                            ItemDelete.Add(hm);
                        }
                    }
                    if (ItemDelete.Count >= 1)
                        Log.LogHDGC(HD, ItemDelete, MessageTitle.XoaHangMau, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    BindDataHM();
                    this.SetChange(true);
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = HD.ID;
                form.DeclarationIssuer = DeclarationIssuer.HOP_DONG_GIA_CONG;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }

        private void btnChungTuTruoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (HD.ID == 0)
                    ShowMessage("Vui lòng lưu thông tin Hợp đồng trước khi thêm chứng từ HQ trước đó", false);
                else
                {
                    ListChungTuTruocDoForm f = new ListChungTuTruocDoForm();
                    f.Master_ID = HD.ID;
                    f.Type = "HDGC";
                    f.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void picHelps_Click(object sender, EventArgs e)
        {

        }

    }
}
