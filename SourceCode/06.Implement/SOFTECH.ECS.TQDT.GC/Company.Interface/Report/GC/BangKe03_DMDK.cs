﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.DuLieuChuan;
 

namespace Company.Interface.Report.GC
{
    public partial class BangKe03_DMDK : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        private int STT = 0;
        public string maSP;
        public DataTable dt = new DataTable();
        public BangKe03_DMDK()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamDangKyInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }

            Company.GC.BLL.KDT.GC.SanPham SP = new SanPham();
            SP.Ma = maSP;
            SP.HopDong_ID = this.HD.ID;
            SP.Load();

            lblMathang.Text = loaiSP;
            lblAddThue.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;

            lblMaHang.Text = SP.Ten + "  (Mã hàng: " + maSP + ")";
            lblSoLuongHang.Text = SP.SoLuongDangKy.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            lblDVTHang.Text = DonViTinh.GetName(SP.DVT_ID);
            this.DataSource = dt;

            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenNPL");
            lblMaNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".MaNPL");
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT");
            lblDMSD.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DMSD", "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTLHH.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TiLeHH", "{00:F00}");
            lblDMAll.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DMAll", "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblNguonNL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Nguon");
            xrLabel27.Text = GlobalSettings.TieudeNgay;

        }

    }
}
