﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components
{
    public partial class Track
    {
        public static Track Load(long soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep)
        {
            const string spName = "[dbo].[p_DongBoDuLieu_Track_LoadBy]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, soToKhai);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, namDangKy);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, maLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, maHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);

            IDataReader reader = db.ExecuteReader(dbCommand);
            IList<Track> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }	

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateBy()
        {
            return this.InsertUpdateBy(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateBy(SqlTransaction transaction)
        {
            const string spName = "p_DongBoDuLieu_Track_InsertUpdateBy";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@TKMD_GUIDSTR", SqlDbType.NVarChar, TKMD_GUIDSTR);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.VarChar, MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@IDUserDaiLy", SqlDbType.Int, IDUserDaiLy);
            db.AddInParameter(dbCommand, "@NgayDongBo", SqlDbType.DateTime, NgayDongBo.Year <= 1753 ? DBNull.Value : (object)NgayDongBo);
            db.AddInParameter(dbCommand, "@TrangThaiDongBoTaiLen", SqlDbType.Int, TrangThaiDongBoTaiLen);
            db.AddInParameter(dbCommand, "@TrangThaiDongBoTaiXuong", SqlDbType.Int, TrangThaiDongBoTaiXuong);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
            db.AddInParameter(dbCommand, "@UserName", SqlDbType.NVarChar, UserName);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
    }
}
