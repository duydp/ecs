﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace CA_QuanLyGCN
{
    public partial class ThongTinDoanhNghiepfrm : Form
    {
        public ThongTinDoanhNghiepfrm()
        {
            InitializeComponent();
        }
        public DoanhNghiep doanhnghiep = new DoanhNghiep();
        public string MaDoanhNghiep { get; set; }
        public TrangThai trangThai;
        public enum TrangThai { ThemGiayCN, ThemQDPhat, addNew }
        private void getThongTinDN()
        {
            doanhnghiep.Mst = txtMST.Text.Trim();
            doanhnghiep.Tendn = txtTenDN.Text.Trim();
            doanhnghiep.NgayCapMst = txtNgayCapMST.Value == null ? new DateTime(1900, 1, 1) : txtNgayCapMST.Value;
            doanhnghiep.DiaChi = txtDiaChiDN.Text.Trim();
            doanhnghiep.CoQuanCapMst = txtCoQuanCapMST.Text.Trim();
            doanhnghiep.TenNguoiDaiDien = txtHoTen.Text.Trim();
            doanhnghiep.QuocTichNguoiDaiDien = txtQuocTich.Text.Trim();
            doanhnghiep.ChucDanhNguoiDaiDien = txtChucDanh.Text.Trim();
            doanhnghiep.CmndNguoiDaiDien = txtSoCNMD.Text.Trim();
            doanhnghiep.NgayCapCmnd = txtNgayCapCMND.Value == null ? new DateTime(1900, 1, 1) : txtNgayCapCMND.Value;
            doanhnghiep.CoQuanCapCmnd = txtCoQuanCapCMND.Text.Trim();
            doanhnghiep.DiaChiNguoiDaiDien = txtHoKhauTT.Text.Trim();
            doanhnghiep.GhiChu = txtGhiChu.Text.Trim();
        }
        private void setThongTinDN()
        {
            if (doanhnghiep.Id > 0)
            {
                txtMST.Text = doanhnghiep.Mst;
                txtTenDN.Text = doanhnghiep.Tendn;
                txtNgayCapMST.Value = !doanhnghiep.NgayCapMst.HasValue ? new DateTime(1900,1,1) : doanhnghiep.NgayCapMst.Value;
                txtDiaChiDN.Text = doanhnghiep.DiaChi;
                txtCoQuanCapMST.Text = doanhnghiep.CoQuanCapMst;
                txtHoTen.Text = doanhnghiep.TenNguoiDaiDien;
                txtQuocTich.Text = doanhnghiep.QuocTichNguoiDaiDien;
                txtChucDanh.Text = doanhnghiep.ChucDanhNguoiDaiDien;
                txtSoCNMD.Text = doanhnghiep.CmndNguoiDaiDien;
                txtNgayCapCMND.Value = doanhnghiep.NgayCapCmnd.HasValue ? new DateTime(1900, 1, 1) : DateTime.Today;
                txtCoQuanCapCMND.Text = doanhnghiep.CoQuanCapCmnd;
                txtHoKhauTT.Text = doanhnghiep.DiaChiNguoiDaiDien;
                txtGhiChu.Text = doanhnghiep.GhiChu;

                if (txtNgayCapMST.Value != null && txtNgayCapMST.Value.Year > 1900)
                    //txtNgayCapMST.Text = txtNgayCapMST.Value.ToString("dd/MM/yyyy");
                    ;
                else
                    txtNgayCapMST.Text = string.Empty;
                if (txtNgayCapCMND.Value != null && txtNgayCapCMND.Value.Year > 1900)
                    //txtNgayCapCMND.Text = txtNgayCapCMND.Value.ToString("dd/MM/yyyy");
                    ;
                else
                    txtNgayCapCMND.Text = string.Empty;
            }
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                getThongTinDN();
                if (string.IsNullOrEmpty(doanhnghiep.Mst.Trim()))
                {
                    errorProvider1.SetError(txtMST, "Không được để trống");
                }
                if (doanhnghiep.Id == 0)
                    doanhnghiep.Insert();
                else
                    doanhnghiep.Update();

                setThongTinDN();
                MessageBox.Show("Lưu thông tin thành công !");
                BindingData();
                SetCommandStatus();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MessageBox.Show("Lưu thông lỗi:  " + ex.Message, "Thông báo lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ThongTinDoanhNghiepfrm_Load(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(this.MaDoanhNghiep))
                {
                    this.doanhnghiep = DoanhNghiep.LoadByMaDoanhNghiep(this.MaDoanhNghiep);
                }
                if (this.doanhnghiep.Id > 0)
                    setThongTinDN();
                else
                {
                    txtMST.Text = this.MaDoanhNghiep;
                }
                SetCommandStatus();
                BindingData();
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void SetCommandStatus()
        {
            if (this.doanhnghiep.Id > 0)
            {
                btnThemGiayCN.Enabled = true;
                btnThemQDPhat.Enabled = true;
                if (this.trangThai == TrangThai.ThemGiayCN)
                    lblStatus.Text = "* Nhấn vào 'Thêm CN' để thêm mới giấy chứng nhận";
                else
                    lblStatus.Text = "* Nhấn vào 'Thêm QĐ Phạt' để thêm mới QĐ Phạt";
            }
            else
            {
                btnThemGiayCN.Enabled = false;
                btnThemQDPhat.Enabled = false;
                lblStatus.Text = "* Lưu thông tin doanh nghiệp trước khi thêm giấy chứng nhận";
            }

        }

        private void btnThemGiayCN_Click(object sender, EventArgs e)
        {
            if (this.doanhnghiep.Id > 0)
            {
                CapGiayChungNhan f = new CapGiayChungNhan();
                f.doanhnghiep = this.doanhnghiep;
                f.ShowDialog(this);
                BindingData();
            }
            else
            {
                MessageBox.Show("Vui lòng lưu thông tin doanh nghiệp trước khi nhập giấy chứng nhận");
            }

        }

        private void btnThemQDPhat_Click(object sender, EventArgs e)
        {
            if (this.doanhnghiep.Id > 0)
            {
                QuyetDinhPhatFrm f = new QuyetDinhPhatFrm();
                f.dn = this.doanhnghiep;
                f.ShowDialog(this);
                BindingData();
            }
            else
            {
                MessageBox.Show("Vui lòng lưu thông tin doanh nghiệp trước khi nhập QĐ Phạt");
            }

        }
        private void BindingData()
        {
            try
            {
                this.doanhnghiep.LoadCollection();
                gridChungNhan.DataSource = this.doanhnghiep.ChungNhanColl;
                //gridChungNhan.SetDataBinding(this.doanhnghiep.ChungNhanColl, "");
                //gridChungNhan.BindingContext = new BindingContext();

               // gridChungNhan.SetDataBinding(this.doanhnghiep.ChungNhanColl, "RootTable");
               // gridChungNhan.RetrieveStructure();
                //gridChungNhan.DataSource = ChungNhan.ListAllDataSet().Tables[0];
                gridThongTinPhat.DataSource = this.doanhnghiep.LoiPhatColl;
                gridThongTinPhat.Refetch();
                gridChungNhan.Refetch();
            }
            catch (System.Exception ex)
            {
                gridThongTinPhat.Refetch();
                gridChungNhan.Refetch();
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void gridChungNhan_DeletingRecords(object sender, CancelEventArgs e)
        {
            try
            {

                if (MessageBox.Show("Bạn có muốn xóa giấy chứng nhận này danh mục này", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    Janus.Windows.GridEX.GridEXSelectedItemCollection list = gridChungNhan.SelectedItems;
                    if (list.Count > 0)
                    {
                        foreach (Janus.Windows.GridEX.GridEXSelectedItem item in list)
                        {
                            if (item.RowType == Janus.Windows.GridEX.RowType.Record)
                            {
                                //DataTable dt = gridEX1.DataSource as DataTable;
                                ChungNhan dr = (ChungNhan)item.GetRow().DataRow;
                                dr.Delete();
                                //Int32 id = System.Convert.ToInt32(dr["ID"]);
                                //NganhNghe nn = new NganhNghe();
                                //nn.Id = id;
                                //nn.Delete();
                            }
                        }
                        BindingData();
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void gridThongTinPhat_DeletingRecords(object sender, CancelEventArgs e)
        {
            try
            {

                if (MessageBox.Show("Bạn có muốn xóa giấy chứng nhận này danh mục này", "Thông báo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    Janus.Windows.GridEX.GridEXSelectedItemCollection list = gridThongTinPhat.SelectedItems;
                    if (list.Count > 0)
                    {
                        foreach (Janus.Windows.GridEX.GridEXSelectedItem item in list)
                        {
                            if (item.RowType == Janus.Windows.GridEX.RowType.Record)
                            {
                                //DataTable dt = gridEX1.DataSource as DataTable;
                                LoiPhat dr = (LoiPhat)item.GetRow().DataRow;
                                dr.Delete();
                                //Int32 id = System.Convert.ToInt32(dr["ID"]);
                                //NganhNghe nn = new NganhNghe();
                                //nn.Id = id;
                                //nn.Delete();
                            }
                        }
                        BindingData();
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void gridChungNhan_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
            {
                //DataTable dt = gridEX1.DataSource as DataTable;
                ChungNhan dr = (ChungNhan)e.Row.DataRow;
                CapGiayChungNhan f = new CapGiayChungNhan();
                f.doanhnghiep = this.doanhnghiep;
                f.chungNhan = dr;
                f.ShowDialog(this);
                BindingData();
            }
            
        }

        private void gridChungNhan_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
             if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
            {
                //DataTable dt = gridEX1.DataSource as DataTable;
                ChungNhan dr = (ChungNhan)e.Row.DataRow;
                e.Row.Cells["IDNganhNghe"].Text = NganhNghe.Load(System.Convert.ToInt32(e.Row.Cells["IDNganhNghe"].Value)).TenNganhNghe;
            }
        }

        private void gridThongTinPhat_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
            {
                //DataTable dt = gridEX1.DataSource as DataTable;
                LoiPhat dr = (LoiPhat)e.Row.DataRow;
                QuyetDinhPhatFrm f = new QuyetDinhPhatFrm();
                f.dn = this.doanhnghiep;
                f.loiphat = dr;
                f.ShowDialog(this);
                BindingData();
            }
        }

        
    }
}
