using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.BLL.KDT.ExportToExcel
{
	public partial class HangMauDich : EntityBase
	{
		
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public DataSet SelectDynamic2(string maHaiQuan)
		{
            //string spName = "SELECT * FROM t_SXXK_HangMauDichDieuChinh WHERE MaHaiQuan=@MaHaiQuan AND MaDoanhNghiep=@MaDoanhNghiep";
            string spName = "SELECT * FROM t_SXXK_HangMauDichDieuChinh WHERE MaHaiQuan=@MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand) this.db.GetSqlStringCommand(spName);

            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
           // this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);            
            return this.db.ExecuteDataSet(dbCommand);        				
		}
        public DataSet SelectDynamic(string maHaiQuan)
        {
            
            string spName = "SELECT * FROM t_SXXK_HangMauDich WHERE MaHaiQuan=@MaHaiQuan";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);                    
            return this.db.ExecuteDataSet(dbCommand);
        }

        public DataSet SelectDynamic1(string maHaiQuan)
        {
            //string spName = "SELECT * FROM t_SXXK_HangMauDichDieuChinh WHERE MaHaiQuan=@MaHaiQuan AND MaDoanhNghiep=@MaDoanhNghiep";
             //"SELECT * FROM t_SXXK_HangMauDichDieuChinh WHERE MaHaiQuan=@MaHaiQuan";
            string spName = " SELECT t_SXXK_HangMauDichDieuChinh.* , DonGiaKB,DonGiaTT,TriGiaKB,TriGiaTT,TriGiaKB_VND,ThueXNK,ThueTTDB,ThueGTGT,PhuThu,TriGiaThuKhac,TyLeThuKhac " +
                             " from  t_SXXK_HangMauDichDieuChinh  " +
                            " inner join  t_SXXK_HangMauDich    " +
                             " on t_SXXK_HangMauDich.Sotokhai =t_SXXK_HangMauDichDieuChinh.sotokhai " +
                             " and t_SXXK_HangMauDich.mahaiquan =t_SXXK_HangMauDichDieuChinh.mahaiquan " +
                            "  and t_SXXK_HangMauDich.maloaihinh =t_SXXK_HangMauDichDieuChinh.maloaihinh " +
                            "  and t_SXXK_HangMauDich.namdangky =t_SXXK_HangMauDichDieuChinh.namdangky " +
                            " and t_SXXK_HangMauDich.sothutuhang =t_SXXK_HangMauDichDieuChinh.stthang " +
                             " WHERE t_SXXK_HangMauDichDieuChinh.MaHaiQuan = @MaHaiQuan ";
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(spName);
            this.db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, maHaiQuan);
            // this.db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);            
            return this.db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------

		
		
		//---------------------------------------------------------------------------------------------
		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
	
	}	
}