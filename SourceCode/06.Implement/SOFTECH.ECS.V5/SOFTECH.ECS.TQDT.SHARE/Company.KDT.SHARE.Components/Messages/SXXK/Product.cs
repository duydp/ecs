﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class Product
    {
        /// <summary>
        /// Thông tin hàng
        /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }
        /// <summary>
        /// Số lượng và đơn vị tính
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }
        /// <summary>
        /// Mã sản phẩm khai báo sửa (NPL)
        /// </summary>
        [XmlElement("preIdentification")]
        public string PreIdentification { get; set; }

    }
}
