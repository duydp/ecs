using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using KeySecurity;

namespace Company.Interface.ProdInfo
{
    public partial class frmInfo : BaseForm
    {
#if KD_V3 || KD_V4
        string AsemblyName = "SOFTECH.ECS.TQDT.KD";
        string imageName = "KD";
#elif SXXK_V3 || SXXK_V4
        string AsemblyName = "SOFTECH.ECS.TQDT.SXXK";
        string imageName = "SXXK";
#elif GC_V3 || GC_V4
        string AsemblyName = "SOFTECH.ECS.TQDT.GC";
        string imageName = "GC";
#elif KD_V2
        string AsemblyName = "SOFTECH.ECS.TQDT.KD";
        string imageName = "KD";
#elif GC_V2
        string AsemblyName = "SOFTECH.ECS.TQDT.GC";
        string imageName = "GC";
#elif SXXK_V2
        string AsemblyName = "SOFTECH.ECS.TQDT.SXXK";
        string imageName = "SXXK";
#endif
        public bool register = false;
        public KeySecurity.License lic;
        public frmInfo()
        {
            InitializeComponent();
        }

        private void frmInfo_Load(object sender, EventArgs e)
        {
            #region Load image from  this form's resource

            // Gets a reference to the same assembly that 
            // contains the type that is creating the ResourceManager.
            System.Reflection.Assembly myAssembly;
            myAssembly = this.GetType().Assembly;

            // Gets a reference to a different assembly.
            System.Reflection.Assembly myOtherAssembly;
            myOtherAssembly = System.Reflection.Assembly.Load(AsemblyName);

            // Creates the ResourceManager.
            System.Resources.ResourceManager myManager = new
               System.Resources.ResourceManager("Company.Interface.ProdInfo.frmRegisteOnline",
               myAssembly);

            // Retrieves String and Image resources.
            System.Drawing.Image myImage;
            myImage = (System.Drawing.Image)myManager.GetObject(imageName);
            #endregion

            pictureBox1.Image = myImage;

            if (register) btnRegister.Enabled = false;
            lblVersion.Text = Application.ProductVersion;
            this.fillInfo();
            lblLicense.Text = lic != null? lic.KeyInfo.Key : Active.Install.License.KeyInfo.Key;
            string licenseInfo = setText("Được sử dụng ", "Can be used");
            licenseInfo += setText("1 máy", "computer(s)");
            //if (lic != null || !string.IsNullOrEmpty(Active.Install.License.KeyInfo.Key))
            //{
            //    btnRegister.Enabled = false;

            //}
            int i ;
            if (lic != null)
                i = lic.KeyInfo.TrialDays;
            else
                i = Active.Install.License.KeyInfo.TrialDays;
            licenseInfo += setText("\nNgày hết hạn: ", "Expire date:");
            licenseInfo += DateTime.Now.Date.AddDays(i).ToShortDateString();
            licenseInfo += " (" + i.ToString() + setText(" ngày)", "day(s))");

            lblLicenseInfo.Text = licenseInfo;
            lblLicenseInfo.ForeColor = Color.Blue;
        }

        private void fillInfo()
        {
            lblTenDN.Text = GlobalSettings.TEN_DON_VI;
            lblDiaChiDN.Text = GlobalSettings.DIA_CHI;
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            this.Hide();
            //this.Close();
            frmRegister obj = new frmRegister();
            obj.ShowDialog(this);
            

        }

        private void linkSoftech_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://softech.vn");
        }

        private void frmInfo_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (register)
            {
                ShowMessage("Bạn đã đăng ký thành công. Chương trình sẽ tự khởi động lại", false);
                Application.ExitThread();
                Application.Restart();
            }
        }
    }
}