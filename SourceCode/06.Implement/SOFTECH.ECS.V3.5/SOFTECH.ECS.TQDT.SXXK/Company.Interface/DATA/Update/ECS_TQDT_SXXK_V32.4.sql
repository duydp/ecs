IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDKI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG DK NG.KHOI (VT)' ,Notes=N'Các cảng dầu khí ngoài khơi (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNDKI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDKI','VN',N'CANG DK NG.KHOI (VT)',N'Các cảng dầu khí ngoài khơi (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNONN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG 19-9' ,Notes=N'Cảng 19-9' WHERE TableID='A016A' AND LOCODE='VNONN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNONN','VN',N'CANG 19-9',N'Cảng 19-9')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNBAN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG BA NGOI (K.HOA)' ,Notes=N'Cảng Ba Ngòi (Khánh Hòa)' WHERE TableID='A016A' AND LOCODE='VNBAN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNBAN','VN',N'CANG BA NGOI (K.HOA)',N'Cảng Ba Ngòi (Khánh Hòa)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVUT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG BA RIA VUNG TAU' ,Notes=N'Cảng Bà Rịa Vũng Tàu' WHERE TableID='A016A' AND LOCODE='VNVUT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVUT','VN',N'CANG BA RIA VUNG TAU',N'Cảng Bà Rịa Vũng Tàu')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNBMA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG BAO MAI (DT)' ,Notes=N'Cảng Bảo Mai' WHERE TableID='A016A' AND LOCODE='VNBMA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNBMA','VN',N'CANG BAO MAI (DT)',N'Cảng Bảo Mai')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNBDM')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG BEN DAM (VT)' ,Notes=N'Cảng Bến Đầm (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNBDM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNBDM','VN',N'CANG BEN DAM (VT)',N'Cảng Bến Đầm (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNBNG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG BEN NGHE (HCM)' ,Notes=N'Cảng Bến Nghé (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNBNG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNBNG','VN',N'CANG BEN NGHE (HCM)',N'Cảng Bến Nghé (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNBDC')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG BINH DUC (L.AN)' ,Notes=N'Cảng Bình Đức (Long An)' WHERE TableID='A016A' AND LOCODE='VNBDC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNBDC','VN',N'CANG BINH DUC (L.AN)',N'Cảng Bình Đức (Long An)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNBLG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG BINH LONG' ,Notes=N'Cảng Bình Long' WHERE TableID='A016A' AND LOCODE='VNBLG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNBLG','VN',N'CANG BINH LONG',N'Cảng Bình Long')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNBTI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG BINH TRI (KG)' ,Notes=N'Cảng Bình trị (Kiên Giang)' WHERE TableID='A016A' AND LOCODE='VNBTI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNBTI','VN',N'CANG BINH TRI (KG)',N'Cảng Bình trị (Kiên Giang)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCLN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CAI LAN (QNINH)' ,Notes=N'Cảng Cái Lân (Quảng Ninh)' WHERE TableID='A016A' AND LOCODE='VNCLN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCLN','VN',N'CANG CAI LAN (QNINH)',N'Cảng Cái Lân (Quảng Ninh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTCC')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'C CAI MEP TCCT (VT)' ,Notes=N'Cảng CÁI MÉP - TCCT (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNTCC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTCC','VN',N'C CAI MEP TCCT (VT)',N'Cảng CÁI MÉP - TCCT (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTCI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'C CAI MEP TCIT (VT)' ,Notes=N'Cảng CÁI MÉP - TCIT (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNTCI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTCI','VN',N'C CAI MEP TCIT (VT)',N'Cảng CÁI MÉP - TCIT (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCPH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CAM PHA (QN)' ,Notes=N'Cảng Cẩm Phả (Quảng Ninh)' WHERE TableID='A016A' AND LOCODE='VNCPH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCPH','VN',N'CANG CAM PHA (QN)',N'Cảng Cẩm Phả (Quảng Ninh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCPA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CAM PHA (VT)' ,Notes=N'Cảng Cẩm Phả (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNCPA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCPA','VN',N'CANG CAM PHA (VT)',N'Cảng Cẩm Phả (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCRB')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CAM RANH(K.HOA)' ,Notes=N'Cảng Cam Ranh (Khánh Hoà)' WHERE TableID='A016A' AND LOCODE='VNCRB'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCRB','VN',N'CANG CAM RANH(K.HOA)',N'Cảng Cam Ranh (Khánh Hoà)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVCT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CAN THO' ,Notes=N'Cảng Cần Thơ' WHERE TableID='A016A' AND LOCODE='VNVCT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVCT','VN',N'CANG CAN THO',N'Cảng Cần Thơ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCLI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CAT LAI (HCM)' ,Notes=N'Cảng Cát Lái (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNCLI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCLI','VN',N'CANG CAT LAI (HCM)',N'Cảng Cát Lái (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CAT LO (BRVT)' ,Notes=N'Cảng Cát Lở - (Bà Rịa - Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNCAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCAL','VN',N'CANG CAT LO (BRVT)',N'Cảng Cát Lở - (Bà Rịa - Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCMY')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CHAN MAY (HUE)' ,Notes=N'Cảng Chân Mây (Thừa Thiên-Huế)' WHERE TableID='A016A' AND LOCODE='VNCMY'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCMY','VN',N'CANG CHAN MAY (HUE)',N'Cảng Chân Mây (Thừa Thiên-Huế)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCKI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CTGK DKHI (VT)' ,Notes=N'Cảng Chế tạo giàn khoan Dầu khí (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNCKI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCKI','VN',N'CANG CTGK DKHI (VT)',N'Cảng Chế tạo giàn khoan Dầu khí (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCVE')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CHUA VE (HP)' ,Notes=N'Cảng Chùa vẽ (Hải phòng)' WHERE TableID='A016A' AND LOCODE='VNCVE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCVE','VN',N'CANG CHUA VE (HP)',N'Cảng Chùa vẽ (Hải phòng)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNXDT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG XD DONG THAP' ,Notes=N'Cảng chuyên dùng xăng dầu Đồng Tháp' WHERE TableID='A016A' AND LOCODE='VNXDT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNXDT','VN',N'CANG XD DONG THAP',N'Cảng chuyên dùng xăng dầu Đồng Tháp')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNXDG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG X.D.GA HOA LONG' ,Notes=N'Cảng chuyên dùng xếp dỡ xăng, dầu, gas hóa lỏng (Đình Vũ)' WHERE TableID='A016A' AND LOCODE='VNXDG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNXDG','VN',N'CANG X.D.GA HOA LONG',N'Cảng chuyên dùng xếp dỡ xăng, dầu, gas hóa lỏng (Đình Vũ)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCOP')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CONT. QT CAIMEP' ,Notes=N'Cảng Container quốc tế Cái Mép (ODA)' WHERE TableID='A016A' AND LOCODE='VNCOP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCOP','VN',N'CANG CONT. QT CAIMEP',N'Cảng Container quốc tế Cái Mép (ODA)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSSA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CONT QUOC TE SG' ,Notes=N'Cảng Container quốc tế Sài Gòn - SSA' WHERE TableID='A016A' AND LOCODE='VNSSA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSSA','VN',N'CANG CONT QUOC TE SG',N'Cảng Container quốc tế Sài Gòn - SSA')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCLO')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CUA LO (NG.AN)' ,Notes=N'Cảng Cửa Lò (Nghệ An)' WHERE TableID='A016A' AND LOCODE='VNCLO'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCLO','VN',N'CANG CUA LO (NG.AN)',N'Cảng Cửa Lò (Nghệ An)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCUV')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CUA VIET(Q.TRI)' ,Notes=N'Cảng Cửa Việt (Quảng Trị)' WHERE TableID='A016A' AND LOCODE='VNCUV'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCUV','VN',N'CANG CUA VIET(Q.TRI)',N'Cảng Cửa Việt (Quảng Trị)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDAN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG DA NANG' ,Notes=N'Cảng Đà Nẵng' WHERE TableID='A016A' AND LOCODE='VNDAN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDAN','VN',N'CANG DA NANG',N'Cảng Đà Nẵng')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDDN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG DIEM DIEN' ,Notes=N'Cảng Diêm Điền (Thuộc HQ Hải Phòng)' WHERE TableID='A016A' AND LOCODE='VNDDN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDDN','VN',N'CANG DIEM DIEN',N'Cảng Diêm Điền (Thuộc HQ Hải Phòng)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDVU')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG DINH VU - HP' ,Notes=N'Cảng Đình Vũ - Hải Phòng' WHERE TableID='A016A' AND LOCODE='VNDVU'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDVU','VN',N'CANG DINH VU - HP',N'Cảng Đình Vũ - Hải Phòng')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDXA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG DOAN XA - HP' ,Notes=N'Cảng Đoạn Xá - Hải Phòng' WHERE TableID='A016A' AND LOCODE='VNDXA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDXA','VN',N'CANG DOAN XA - HP',N'Cảng Đoạn Xá - Hải Phòng')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDNA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG DONG NAI' ,Notes=N'Cảng Đồng Nai' WHERE TableID='A016A' AND LOCODE='VNDNA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDNA','VN',N'CANG DONG NAI',N'Cảng Đồng Nai')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDTH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG DONG THAP' ,Notes=N'Cảng Đồng Tháp' WHERE TableID='A016A' AND LOCODE='VNDTH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDTH','VN',N'CANG DONG THAP',N'Cảng Đồng Tháp')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDDQ')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG DOOSAN D.QUAT' ,Notes=N'Cảng Doosan Dung Quất (Quảng Ngãi)' WHERE TableID='A016A' AND LOCODE='VNDDQ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDDQ','VN',N'CANG DOOSAN D.QUAT',N'Cảng Doosan Dung Quất (Quảng Ngãi)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDQT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG D.QUAT - B.SO 1' ,Notes=N'Cảng Dung Quất - Bến số 1 (Quảng Ngãi)' WHERE TableID='A016A' AND LOCODE='VNDQT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDQT','VN',N'CANG D.QUAT - B.SO 1',N'Cảng Dung Quất - Bến số 1 (Quảng Ngãi)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPDQ')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG DQUAT-BEN PHAO' ,Notes=N'Cảng Dung Quất-Bến phao& xuất SP(Q.Ngãi)' WHERE TableID='A016A' AND LOCODE='VNPDQ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPDQ','VN',N'CANG DQUAT-BEN PHAO',N'Cảng Dung Quất-Bến phao& xuất SP(Q.Ngãi)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDDG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG D.DONG-PQUOC' ,Notes=N'Cảng Dương Đông - Phú Quốc (Kiên Giang)' WHERE TableID='A016A' AND LOCODE='VNDDG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDDG','VN',N'CANG D.DONG-PQUOC',N'Cảng Dương Đông - Phú Quốc (Kiên Giang)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNELF')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG ELF GAZ DA NANG' ,Notes=N'Cảng ELF Gaz Đà Nẵng' WHERE TableID='A016A' AND LOCODE='VNELF'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNELF','VN',N'CANG ELF GAZ DA NANG',N'Cảng ELF Gaz Đà Nẵng')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNGAI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG GAS DAI HAI' ,Notes=N'Cảng Gas Đài Hải' WHERE TableID='A016A' AND LOCODE='VNGAI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNGAI','VN',N'CANG GAS DAI HAI',N'Cảng Gas Đài Hải')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNGDU')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG GO DAU (P.THAI)' ,Notes=N'Cảng Gò Dầu (Phước Thái - Đồng Nai)' WHERE TableID='A016A' AND LOCODE='VNGDU'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNGDU','VN',N'CANG GO DAU (P.THAI)',N'Cảng Gò Dầu (Phước Thái - Đồng Nai)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHLC')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG HA LOC (V.TAU)' ,Notes=N'Cảng Hà Lộc (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNHLC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHLC','VN',N'CANG HA LOC (V.TAU)',N'Cảng Hà Lộc (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVTH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG HA LUU PTSC(VT)' ,Notes=N'Cảng hạ lưu PTSC (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNVTH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVTH','VN',N'CANG HA LUU PTSC(VT)',N'Cảng hạ lưu PTSC (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHIA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG HAI AN' ,Notes=N'Cảng Hải An' WHERE TableID='A016A' AND LOCODE='VNHIA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHIA','VN',N'CANG HAI AN',N'Cảng Hải An')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHPH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG HAI PHONG' ,Notes=N'Cảng Hải Phòng' WHERE TableID='A016A' AND LOCODE='VNHPH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHPH','VN',N'CANG HAI PHONG',N'Cảng Hải Phòng')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHTH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG HAI THINH (ND)' ,Notes=N'Cảng Hải Thịnh (Nam Định)' WHERE TableID='A016A' AND LOCODE='VNHTH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHTH','VN',N'CANG HAI THINH (ND)',N'Cảng Hải Thịnh (Nam Định)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHPV')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VIETFRACHT (HP)' ,Notes=N'Cảng HAIPHONG-VIETFRACHT (Hải Phòng)' WHERE TableID='A016A' AND LOCODE='VNHPV'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHPV','VN',N'CANG VIETFRACHT (HP)',N'Cảng HAIPHONG-VIETFRACHT (Hải Phòng)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHPT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG TRANSVINA (HP)' ,Notes=N'Cảng HAPHONG-TRANSVINA (Hải Phòng)' WHERE TableID='A016A' AND LOCODE='VNHPT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHPT','VN',N'CANG TRANSVINA (HP)',N'Cảng HAPHONG-TRANSVINA (Hải Phòng)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSPC')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG HIEP PHUOC(HCM)' ,Notes=N'Cảng Hiệp Phước (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNSPC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSPC','VN',N'CANG HIEP PHUOC(HCM)',N'Cảng Hiệp Phước (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHLM')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG HOLCIM (V.TAU)' ,Notes=N'Cảng Holcim (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNHLM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHLM','VN',N'CANG HOLCIM (V.TAU)',N'Cảng Holcim (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHCH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG HON CHONG (KG)' ,Notes=N'Cảng Hòn Chông (Kiên Giang)' WHERE TableID='A016A' AND LOCODE='VNHCH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHCH','VN',N'CANG HON CHONG (KG)',N'Cảng Hòn Chông (Kiên Giang)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHON')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG HON GAI (Q.N)' ,Notes=N'Cảng Hòn Gai (Quảng Ninh)' WHERE TableID='A016A' AND LOCODE='VNHON'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHON','VN',N'CANG HON GAI (Q.N)',N'Cảng Hòn Gai (Quảng Ninh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHLA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG HON LA (Q.BINH)' ,Notes=N'Cảng Hòn La (Quảng Bình)' WHERE TableID='A016A' AND LOCODE='VNHLA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHLA','VN',N'CANG HON LA (Q.BINH)',N'Cảng Hòn La (Quảng Bình)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHTM')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG HON THOM (KG)' ,Notes=N'Cảng Hòn Thơm (Kiên Giang)' WHERE TableID='A016A' AND LOCODE='VNHTM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHTM','VN',N'CANG HON THOM (KG)',N'Cảng Hòn Thơm (Kiên Giang)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPLF')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG ICD PHUOCLONG 1' ,Notes=N'Cảng ICD Phước Long 1 (TP.HCM)' WHERE TableID='A016A' AND LOCODE='VNPLF'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPLF','VN',N'CANG ICD PHUOCLONG 1',N'Cảng ICD Phước Long 1 (TP.HCM)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPLS')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG ICD PHUOCLONG 2' ,Notes=N'Cảng ICD Phước Long 2 (TP.HCM)' WHERE TableID='A016A' AND LOCODE='VNPLS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPLS','VN',N'CANG ICD PHUOCLONG 2',N'Cảng ICD Phước Long 2 (TP.HCM)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPLT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG ICD PHUOCLONG 3' ,Notes=N'Cảng ICD Phước Long 3 (TP.HCM)' WHERE TableID='A016A' AND LOCODE='VNPLT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPLT','VN',N'CANG ICD PHUOCLONG 3',N'Cảng ICD Phước Long 3 (TP.HCM)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNIFR')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG INTERFLOUR (VT)' ,Notes=N'Cảng Interflour (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNIFR'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNIFR','VN',N'CANG INTERFLOUR (VT)',N'Cảng Interflour (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNKHI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG KHANH HOI (HCM)' ,Notes=N'Cảng Khánh Hội (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNKHI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNKHI','VN',N'CANG KHANH HOI (HCM)',N'Cảng Khánh Hội (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCLT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG XD CU LAO TAO' ,Notes=N'Cảng kho xăng dầu Cù Lao Tào (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNCLT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCLT','VN',N'CANG XD CU LAO TAO',N'Cảng kho xăng dầu Cù Lao Tào (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDXN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG KXD DONG XUYEN' ,Notes=N'Cảng kho xăng dầu Đông Xuyên (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNDXN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDXN','VN',N'CANG KXD DONG XUYEN',N'Cảng kho xăng dầu Đông Xuyên (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNKYH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG KY HA (Q.NAM)' ,Notes=N'Cảng Kỳ Hà (Quảng Nam)' WHERE TableID='A016A' AND LOCODE='VNKYH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNKYH','VN',N'CANG KY HA (Q.NAM)',N'Cảng Kỳ Hà (Quảng Nam)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNLIO')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG LISEMCO' ,Notes=N'Cảng Lisemco' WHERE TableID='A016A' AND LOCODE='VNLIO'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNLIO','VN',N'CANG LISEMCO',N'Cảng Lisemco')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNLOT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG LOTUS (HCM)' ,Notes=N'Cảng Lotus/Cảng Bông Sen (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNLOT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNLOT','VN',N'CANG LOTUS (HCM)',N'Cảng Lotus/Cảng Bông Sen (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNMCL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CATLAI OPENPORT(HCM)' ,Notes=N'Cảng Mở Cát Lái/ CatLai OpenPort (TP.HCM)' WHERE TableID='A016A' AND LOCODE='VNMCL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNMCL','VN',N'CATLAI OPENPORT(HCM)',N'Cảng Mở Cát Lái/ CatLai OpenPort (TP.HCM)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNMUT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG MY THO (L.AN)' ,Notes=N'Cảng Mỹ tho (Long An)' WHERE TableID='A016A' AND LOCODE='VNMUT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNMUT','VN',N'CANG MY THO (L.AN)',N'Cảng Mỹ tho (Long An)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNMTH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG MY THOI (AG)' ,Notes=N'Cảng Mỹ Thới (An Giang)' WHERE TableID='A016A' AND LOCODE='VNMTH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNMTH','VN',N'CANG MY THOI (AG)',N'Cảng Mỹ Thới (An Giang)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG NAM CAN' ,Notes=N'Cảng Năm Căn (Minh Hải)' WHERE TableID='A016A' AND LOCODE='VNNCN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCN','VN',N'CANG NAM CAN',N'Cảng Năm Căn (Minh Hải)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNGH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG NGHI SON(T.HOA)' ,Notes=N'Cảng Nghi Sơn (Thanh Hoá)' WHERE TableID='A016A' AND LOCODE='VNNGH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNGH','VN',N'CANG NGHI SON(T.HOA)',N'Cảng Nghi Sơn (Thanh Hoá)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBE')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG NHA BE (HCM)' ,Notes=N'Cảng Nhà bè (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNNBE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBE','VN',N'CANG NHA BE (HCM)',N'Cảng Nhà bè (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNRG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG NHA RONG (HCM)' ,Notes=N'Cảng Nhà rồng (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNNRG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNRG','VN',N'CANG NHA RONG (HCM)',N'Cảng Nhà rồng (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNHA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG NHA TRANG(KH)' ,Notes=N'Cảng Nha Trang (Khánh Hoà)' WHERE TableID='A016A' AND LOCODE='VNNHA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNHA','VN',N'CANG NHA TRANG(KH)',N'Cảng Nha Trang (Khánh Hoà)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNPC')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG NINH PHUC(NB)' ,Notes=N'Cảng Ninh Phúc - Ninh Bình' WHERE TableID='A016A' AND LOCODE='VNNPC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNPC','VN',N'CANG NINH PHUC(NB)',N'Cảng Ninh Phúc - Ninh Bình')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPHH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG PHU HUU' ,Notes=N'Cảng Phú Hữu' WHERE TableID='A016A' AND LOCODE='VNPHH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPHH','VN',N'CANG PHU HUU',N'Cảng Phú Hữu')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPHU')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG PHU MY (V.TAU)' ,Notes=N'Cảng Phú Mỹ (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNPHU'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPHU','VN',N'CANG PHU MY (V.TAU)',N'Cảng Phú Mỹ (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPMY')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'C PHU MY 2.1; 2.2-VT' ,Notes=N'Cảng Phú Mỹ 2.1; 2.2 (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNPMY'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPMY','VN',N'C PHU MY 2.1; 2.2-VT',N'Cảng Phú Mỹ 2.1; 2.2 (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPQY')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG PHU QUY (D.NAI)' ,Notes=N'Cảng Phú Quý (Đồng Nai)' WHERE TableID='A016A' AND LOCODE='VNPQY'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPQY','VN',N'CANG PHU QUY (D.NAI)',N'Cảng Phú Quý (Đồng Nai)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPOS')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG POSCO (VT)' ,Notes=N'Cảng POSCO (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNPOS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPOS','VN',N'CANG POSCO (VT)',N'Cảng POSCO (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVTP')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG PTSC (VUNG TAU)' ,Notes=N'Cảng PTSC (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNVTP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVTP','VN',N'CANG PTSC (VUNG TAU)',N'Cảng PTSC (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPVS')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG PV SHIPYARDS' ,Notes=N'Cảng PV ShipYards (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNPVS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPVS','VN',N'CANG PV SHIPYARDS',N'Cảng PV ShipYards (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVTG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG PV.GAS (V.TAU)' ,Notes=N'Cảng PV.Gas (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNVTG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVTG','VN',N'CANG PV.GAS (V.TAU)',N'Cảng PV.Gas (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPMS')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG PVC-MS (V.TAU)' ,Notes=N'Cảng PVC-MS (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNPMS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPMS','VN',N'CANG PVC-MS (V.TAU)',N'Cảng PVC-MS (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDQG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG QT GERMADEPT' ,Notes=N'Cảng Q.Tế Germadept Dung Quất (Q.Ngãi)' WHERE TableID='A016A' AND LOCODE='VNDQG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDQG','VN',N'CANG QT GERMADEPT',N'Cảng Q.Tế Germadept Dung Quất (Q.Ngãi)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNRRV')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG QLRR VUNG TAU' ,Notes=N'Cảng quản lý rủi ro Vũng Tàu' WHERE TableID='A016A' AND LOCODE='VNRRV'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNRRV','VN',N'CANG QLRR VUNG TAU',N'Cảng quản lý rủi ro Vũng Tàu')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNUIH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG QUI NHON(BDINH)' ,Notes=N'Cảng Qui Nhơn (Bình Định)' WHERE TableID='A016A' AND LOCODE='VNUIH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNUIH','VN',N'CANG QUI NHON(BDINH)',N'Cảng Qui Nhơn (Bình Định)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCMT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG QT CAI MEP' ,Notes=N'Cảng quốc tế Cái Mép (CMIT)' WHERE TableID='A016A' AND LOCODE='VNCMT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCMT','VN',N'CANG QT CAI MEP',N'Cảng quốc tế Cái Mép (CMIT)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSPT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG QT SP-SSA(SSIT)' ,Notes=N'Cảng Quốc tế SP-SSA (SSIT)' WHERE TableID='A016A' AND LOCODE='VNSPT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSPT','VN',N'CANG QT SP-SSA(SSIT)',N'Cảng Quốc tế SP-SSA (SSIT)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSGR')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG RAU QUA (HCM)' ,Notes=N'Cảng Rau Quả (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNSGR'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSGR','VN',N'CANG RAU QUA (HCM)',N'Cảng Rau Quả (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSAD')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG SA DEC (DT)' ,Notes=N'Cảng Sa Đéc' WHERE TableID='A016A' AND LOCODE='VNSAD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSAD','VN',N'CANG SA DEC (DT)',N'Cảng Sa Đéc')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSKY')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG SA KY (B.DINH)' ,Notes=N'Cảng Sa kỳ (Bình Định)' WHERE TableID='A016A' AND LOCODE='VNSKY'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSKY','VN',N'CANG SA KY (B.DINH)',N'Cảng Sa kỳ (Bình Định)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNOSP')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG SAI GON KV III' ,Notes=N'Cang Sài gòn KV III (cảng xăng dầu)' WHERE TableID='A016A' AND LOCODE='VNOSP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNOSP','VN',N'CANG SAI GON KV III',N'Cang Sài gòn KV III (cảng xăng dầu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSIT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG SITV (VUNG TAU)' ,Notes=N'Cảng SITV (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNSIT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSIT','VN',N'CANG SITV (VUNG TAU)',N'Cảng SITV (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSRA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG SOAI RAP (DT)' ,Notes=N'Cảng Soài Rạp - Hiệp Phước' WHERE TableID='A016A' AND LOCODE='VNSRA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSRA','VN',N'CANG SOAI RAP (DT)',N'Cảng Soài Rạp - Hiệp Phước')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSDG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG SON DUONG' ,Notes=N'Cảng Sơn Dương' WHERE TableID='A016A' AND LOCODE='VNSDG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSDG','VN',N'CANG SON DUONG',N'Cảng Sơn Dương')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSGH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG SONG GIANH' ,Notes=N'Cảng Sông Gianh - Quảng Bình' WHERE TableID='A016A' AND LOCODE='VNSGH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSGH','VN',N'CANG SONG GIANH',N'Cảng Sông Gianh - Quảng Bình')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSPA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG SP-PSA (V.TAU)' ,Notes=N'Cảng SP-PSA (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNSPA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSPA','VN',N'CANG SP-PSA (V.TAU)',N'Cảng SP-PSA (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSTX')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG STX VN OFFSHORE' ,Notes=N'Cảng STX Việt Nam Offshore (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNSTX'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSTX','VN',N'CANG STX VN OFFSHORE',N'Cảng STX Việt Nam Offshore (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTAP')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG TAM HIEP' ,Notes=N'Cảng Tam Hiệp' WHERE TableID='A016A' AND LOCODE='VNTAP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTAP','VN',N'CANG TAM HIEP',N'Cảng Tam Hiệp')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTCM')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG T.CANG -CAI MEP' ,Notes=N'Cảng Tân Cảng - Cái Mép (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNTCM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTCM','VN',N'CANG T.CANG -CAI MEP',N'Cảng Tân Cảng - Cái Mép (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTCG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG TAN CANG (HCM)' ,Notes=N'Cảng Tân cảng (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNTCG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTCG','VN',N'CANG TAN CANG (HCM)',N'Cảng Tân cảng (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTTS')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG TAN THUAN (HCM)' ,Notes=N'Cảng Tân Thuận (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNTTS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTTS','VN',N'CANG TAN THUAN (HCM)',N'Cảng Tân Thuận (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTTD')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG T.THUAN DONG' ,Notes=N'Cảng Tân Thuận Đông (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNTTD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTTD','VN',N'CANG T.THUAN DONG',N'Cảng Tân Thuận Đông (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTHS')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG THANG LONG GAS' ,Notes=N'Cảng Thăng Long Gas' WHERE TableID='A016A' AND LOCODE='VNTHS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTHS','VN',N'CANG THANG LONG GAS',N'Cảng Thăng Long Gas')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTHO')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG THANH HOA' ,Notes=N'Cảng Thanh Hoá' WHERE TableID='A016A' AND LOCODE='VNTHO'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTHO','VN',N'CANG THANH HOA',N'Cảng Thanh Hoá')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTMN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG THEP MIEN NAM' ,Notes=N'Cảng Thép miền Nam (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNTMN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTMN','VN',N'CANG THEP MIEN NAM',N'Cảng Thép miền Nam (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTHU')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG THUAN AN (HUE)' ,Notes=N'Cảng Thuận An (Huế)' WHERE TableID='A016A' AND LOCODE='VNTHU'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTHU','VN',N'CANG THUAN AN (HUE)',N'Cảng Thuận An (Huế)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVTT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG T.LUU PTSC (VT)' ,Notes=N'Cảng thượng lưu PTSC (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNVTT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVTT','VN',N'CANG T.LUU PTSC (VT)',N'Cảng thượng lưu PTSC (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTSN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG THUY SAN II' ,Notes=N'Cảng Thuỷ sản II' WHERE TableID='A016A' AND LOCODE='VNTSN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTSN','VN',N'CANG THUY SAN II',N'Cảng Thuỷ sản II')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDTS')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG TIEN SA(D.NANG)' ,Notes=N'Cảng Tiên sa (Đà Nẵng)' WHERE TableID='A016A' AND LOCODE='VNDTS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDTS','VN',N'CANG TIEN SA(D.NANG)',N'Cảng Tiên sa (Đà Nẵng)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNBDU')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG T.HOP B.DUONG' ,Notes=N'Cảng Tổng hợp Bình Dương' WHERE TableID='A016A' AND LOCODE='VNBDU'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNBDU','VN',N'CANG T.HOP B.DUONG',N'Cảng Tổng hợp Bình Dương')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTRG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG TRUONG THANH ' ,Notes=N'Cảng Trường Thành ' WHERE TableID='A016A' AND LOCODE='VNTRG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTRG','VN',N'CANG TRUONG THANH ',N'Cảng Trường Thành ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVAN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VAN AN (V.TAU)' ,Notes=N'Cảng Vạn An (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNVAN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVAN','VN',N'CANG VAN AN (V.TAU)',N'Cảng Vạn An (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVGA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VAN GIA (QN)' ,Notes=N'Cảng Vạn Gia (Quảng Ninh)' WHERE TableID='A016A' AND LOCODE='VNVGA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVGA','VN',N'CANG VAN GIA (QN)',N'Cảng Vạn Gia (Quảng Ninh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVPH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VAN PHONG' ,Notes=N'Cảng Văn Phong (Khánh Hoà)' WHERE TableID='A016A' AND LOCODE='VNVPH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVPH','VN',N'CANG VAN PHONG',N'Cảng Văn Phong (Khánh Hoà)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVAC')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VAT CACH (HP)' ,Notes=N'Cảng Vật Cách (Hải Phòng)' WHERE TableID='A016A' AND LOCODE='VNVAC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVAC','VN',N'CANG VAT CACH (HP)',N'Cảng Vật Cách (Hải Phòng)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVDN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VEDAN(DONG NAI)' ,Notes=N'Cảng Vêđan (Đồng Nai)' WHERE TableID='A016A' AND LOCODE='VNVDN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVDN','VN',N'CANG VEDAN(DONG NAI)',N'Cảng Vêđan (Đồng Nai)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVIC')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VICT' ,Notes=N'Cảng Vict' WHERE TableID='A016A' AND LOCODE='VNVIC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVIC','VN',N'CANG VICT',N'Cảng Vict')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVSP')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VIETSOV PETRO' ,Notes=N'Cảng Vietsov Petro (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNVSP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVSP','VN',N'CANG VIETSOV PETRO',N'Cảng Vietsov Petro (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVNO')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VINA OFFSHORE' ,Notes=N'Cảng Vina Offshore (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNVNO'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVNO','VN',N'CANG VINA OFFSHORE',N'Cảng Vina Offshore (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVTI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VINH THAI(CTHO)' ,Notes=N'Cảng Vĩnh Thái (Cần Thơ)' WHERE TableID='A016A' AND LOCODE='VNVTI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVTI','VN',N'CANG VINH THAI(CTHO)',N'Cảng Vĩnh Thái (Cần Thơ)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVAG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VUNG ANG(HTINH)' ,Notes=N'Cảng Vũng áng (Hà Tĩnh)' WHERE TableID='A016A' AND LOCODE='VNVAG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVAG','VN',N'CANG VUNG ANG(HTINH)',N'Cảng Vũng áng (Hà Tĩnh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVRO')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VUNG RO (P.YEN)' ,Notes=N'Cảng Vũng Rô (Phú Yên)' WHERE TableID='A016A' AND LOCODE='VNVRO'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVRO','VN',N'CANG VUNG RO (P.YEN)',N'Cảng Vũng Rô (Phú Yên)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNXHI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG XUAN HAI (HT)' ,Notes=N'Cảng Xuân Hải (Hà Tĩnh)' WHERE TableID='A016A' AND LOCODE='VNXHI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNXHI','VN',N'CANG XUAN HAI (HT)',N'Cảng Xuân Hải (Hà Tĩnh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSGZ')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG Z (HO CHI MINH)' ,Notes=N'Cảng Z (Hồ Chí Minh)' WHERE TableID='A016A' AND LOCODE='VNSGZ'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSGZ','VN',N'CANG Z (HO CHI MINH)',N'Cảng Z (Hồ Chí Minh)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHLH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'C.CANG DK HAI LINH' ,Notes=N'Cầu Cảng dầu khí Hải Linh' WHERE TableID='A016A' AND LOCODE='VNHLH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHLH','VN',N'C.CANG DK HAI LINH',N'Cầu Cảng dầu khí Hải Linh')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG CA HA LONG' ,Notes=N'Cảng cá Hạ Long' WHERE TableID='A016A' AND LOCODE='VNHAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHAL','VN',N'CANG CA HA LONG',N'Cảng cá Hạ Long')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCPD')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CP DINH VU' ,Notes=N'CP Đình Vũ' WHERE TableID='A016A' AND LOCODE='VNCPD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCPD','VN',N'CP DINH VU',N'CP Đình Vũ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNBDA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DONG TAU BACH DANG' ,Notes=N'CP đóng tàu Bạch Đằng' WHERE TableID='A016A' AND LOCODE='VNBDA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNBDA','VN',N'DONG TAU BACH DANG',N'CP đóng tàu Bạch Đằng')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCAM')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CUA CAM' ,Notes=N'Cửa Cấm' WHERE TableID='A016A' AND LOCODE='VNCAM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCAM','VN',N'CUA CAM',N'Cửa Cấm')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNBAI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CK BAC DAI (ANGIANG)' ,Notes=N'Cửa khẩu Bắc Đai' WHERE TableID='A016A' AND LOCODE='VNBAI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNBAI','VN',N'CK BAC DAI (ANGIANG)',N'Cửa khẩu Bắc Đai')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNKBH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CK KHANH BINH (AG)' ,Notes=N'Cửa khẩu Khánh Bình' WHERE TableID='A016A' AND LOCODE='VNKBH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNKBH','VN',N'CK KHANH BINH (AG)',N'Cửa khẩu Khánh Bình')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVXG')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CK QT VINH XUONG(AG)' ,Notes=N'Cửa khẩu Quốc tế Vĩnh Xương' WHERE TableID='A016A' AND LOCODE='VNVXG'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVXG','VN',N'CK QT VINH XUONG(AG)',N'Cửa khẩu Quốc tế Vĩnh Xương')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVHD')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CK VINH HOI DONG(AG)' ,Notes=N'Cửa khẩu Vĩnh Hội Đông' WHERE TableID='A016A' AND LOCODE='VNVHD'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVHD','VN',N'CK VINH HOI DONG(AG)',N'Cửa khẩu Vĩnh Hội Đông')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDAH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DAI HAI' ,Notes=N'Đài Hải' WHERE TableID='A016A' AND LOCODE='VNDAH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDAH','VN',N'DAI HAI',N'Đài Hải')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDAP')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DAP' ,Notes=N'DAP' WHERE TableID='A016A' AND LOCODE='VNDAP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDAP','VN',N'DAP',N'DAP')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDNH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DINH VU NAM HAI' ,Notes=N'Đình Vũ Nam Hải' WHERE TableID='A016A' AND LOCODE='VNDNH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDNH','VN',N'DINH VU NAM HAI',N'Đình Vũ Nam Hải')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDHA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DONG HAI' ,Notes=N'Đông Hải' WHERE TableID='A016A' AND LOCODE='VNDHA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDHA','VN',N'DONG HAI',N'Đông Hải')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPRU')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DONG TAU PHA RUNG' ,Notes=N'Đóng tàu Phà Rừng' WHERE TableID='A016A' AND LOCODE='VNPRU'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPRU','VN',N'DONG TAU PHA RUNG',N'Đóng tàu Phà Rừng')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNGEE')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'GREEN PORT (HP)' ,Notes=N'GREEN PORT (HAI PHONG)' WHERE TableID='A016A' AND LOCODE='VNGEE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNGEE','VN',N'GREEN PORT (HP)',N'GREEN PORT (HAI PHONG)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHDA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'HAI DANG  (CELL GAS)' ,Notes=N'Hải Đăng  (Cell gas)' WHERE TableID='A016A' AND LOCODE='VNHDA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHDA','VN',N'HAI DANG  (CELL GAS)',N'Hải Đăng  (Cell gas)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHDI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'HOANG DIEU (HP)' ,Notes=N'HOANG DIEU (Hải Phòng)' WHERE TableID='A016A' AND LOCODE='VNHDI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHDI','VN',N'HOANG DIEU (HP)',N'HOANG DIEU (Hải Phòng)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNKNN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'K 99' ,Notes=N'K 99' WHERE TableID='A016A' AND LOCODE='VNKNN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNKNN','VN',N'K 99',N'K 99')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNGDA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KHU TC GO DA (VT)' ,Notes=N'Khu trung chuyển Gò Da (Vũng Tàu)' WHERE TableID='A016A' AND LOCODE='VNGDA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNGDA','VN',N'KHU TC GO DA (VT)',N'Khu trung chuyển Gò Da (Vũng Tàu)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNLIL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'LILAMA' ,Notes=N'Lilama' WHERE TableID='A016A' AND LOCODE='VNLIL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNLIL','VN',N'LILAMA',N'Lilama')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNHC')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'NAM HAI' ,Notes=N'Nam Hai' WHERE TableID='A016A' AND LOCODE='VNNHC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNHC','VN',N'NAM HAI',N'Nam Hai')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNNI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'NAM NINH' ,Notes=N'Nam Ninh' WHERE TableID='A016A' AND LOCODE='VNNNI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNNI','VN',N'NAM NINH',N'Nam Ninh')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAU')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'NM D.TAU NAM TRIEU' ,Notes=N'Nhà máy đóng tàu Nam Triệu' WHERE TableID='A016A' AND LOCODE='VNNAU'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAU','VN',N'NM D.TAU NAM TRIEU',N'Nhà máy đóng tàu Nam Triệu')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPTE')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'PETEC' ,Notes=N'Petec' WHERE TableID='A016A' AND LOCODE='VNPTE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPTE','VN',N'PETEC',N'Petec')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPTS')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'PTSC DINH VU' ,Notes=N'PTSC Đình Vũ' WHERE TableID='A016A' AND LOCODE='VNPTS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPTS','VN',N'PTSC DINH VU',N'PTSC Đình Vũ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSDA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'SONG DA 12.4' ,Notes=N'Sông Đà 12.4' WHERE TableID='A016A' AND LOCODE='VNSDA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSDA','VN',N'SONG DA 12.4',N'Sông Đà 12.4')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTCN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TAN CANG (189)' ,Notes=N'Tân Cảng (189)' WHERE TableID='A016A' AND LOCODE='VNTCN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTCN','VN',N'TAN CANG (189)',N'Tân Cảng (189)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTCE')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TAN CANG 128' ,Notes=N'Tân Cảng 128' WHERE TableID='A016A' AND LOCODE='VNTCE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTCE','VN',N'TAN CANG 128',N'Tân Cảng 128')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHPN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TAN CANG HAI PHONG' ,Notes=N'Tân Cảng Hải Phòng (Tân Cảng Đình Vũ)' WHERE TableID='A016A' AND LOCODE='VNHPN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHPN','VN',N'TAN CANG HAI PHONG',N'Tân Cảng Hải Phòng (Tân Cảng Đình Vũ)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNGAS')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TOTAL GAS' ,Notes=N'Total gas' WHERE TableID='A016A' AND LOCODE='VNGAS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNGAS','VN',N'TOTAL GAS',N'Total gas')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHTC')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'VN HI-TECH TRANS. CO' ,Notes=N'Vietnam Hi-Tech Transportation Co' WHERE TableID='A016A' AND LOCODE='VNHTC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHTC','VN',N'VN HI-TECH TRANS. CO',N'Vietnam Hi-Tech Transportation Co')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNXDV')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'XANG DAU DINH VU' ,Notes=N'Xăng dầu Đình Vũ' WHERE TableID='A016A' AND LOCODE='VNXDV'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNXDV','VN',N'XANG DAU DINH VU',N'Xăng dầu Đình Vũ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNQDO')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'XD QUAN DOI (MIPEC)' ,Notes=N'Xăng dầu quân đội (MIPEC)' WHERE TableID='A016A' AND LOCODE='VNQDO'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNQDO','VN',N'XD QUAN DOI (MIPEC)',N'Xăng dầu quân đội (MIPEC)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTLY')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'XANG DAU THUONG LY' ,Notes=N'Xăng dầu Thượng Lý' WHERE TableID='A016A' AND LOCODE='VNTLY'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTLY','VN',N'XANG DAU THUONG LY',N'Xăng dầu Thượng Lý')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNITSL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'ICD TIEN SON BAC NINH ' ,Notes=N'ICD Tiên Sơn - Bắc Ninh' WHERE TableID='A016A' AND LOCODE='VNITSL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNITSL','VN',N'ICD TIEN SON BAC NINH ',N'ICD Tiên Sơn - Bắc Ninh')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDOC')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CK DONG DUC (AG)' ,Notes=N'Cửa khẩu Đồng Đức (An Giang)' WHERE TableID='A016A' AND LOCODE='VNDOC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDOC','VN',N'CK DONG DUC (AG)',N'Cửa khẩu Đồng Đức (An Giang)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTHC')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CK THUONG PHUOC (DT)' ,Notes=N'Cửa khẩu Thường Phước (Đồng Tháp)' WHERE TableID='A016A' AND LOCODE='VNTHC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTHC','VN',N'CK THUONG PHUOC (DT)',N'Cửa khẩu Thường Phước (Đồng Tháp)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNATH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG AN THOI' ,Notes=N'Cảng An Thới' WHERE TableID='A016A' AND LOCODE='VNATH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNATH','VN',N'CANG AN THOI',N'Cảng An Thới')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHTE')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KHU CT PD HA TIEN' ,Notes=N'Khu chuyển tải pháo đài Hà Tiên' WHERE TableID='A016A' AND LOCODE='VNHTE'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHTE','VN',N'KHU CT PD HA TIEN',N'Khu chuyển tải pháo đài Hà Tiên')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTVA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'BEN CANG TH THI VAI' ,Notes=N'Bến cảng Tổng hợp Thị Vải' WHERE TableID='A016A' AND LOCODE='VNTVA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTVA','VN',N'BEN CANG TH THI VAI',N'Bến cảng Tổng hợp Thị Vải')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNTCH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TAN CANG HIEP PHUOC' ,Notes=N'Cảng Tân cảng - Hiệp Phước' WHERE TableID='A016A' AND LOCODE='VNTCH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNTCH','VN',N'TAN CANG HIEP PHUOC',N'Cảng Tân cảng - Hiệp Phước')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDTV')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DUYEN HAI TRA VINH' ,Notes=N'Bến cảng thuộc dự án công trình cảng biển Trung tâm Điện lực Duyên Hải Trà Vinh' WHERE TableID='A016A' AND LOCODE='VNDTV'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDTV','VN',N'DUYEN HAI TRA VINH',N'Bến cảng thuộc dự án công trình cảng biển Trung tâm Điện lực Duyên Hải Trà Vinh')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNHUN')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CK HUNG DIEN (LA)' ,Notes=N'Cửa khẩu Hưng Điền (Long An)' WHERE TableID='A016A' AND LOCODE='VNHUN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNHUN','VN',N'CK HUNG DIEN (LA)',N'Cửa khẩu Hưng Điền (Long An)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNQPH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG QT ITC PHU HUU' ,Notes=N'Cảng Tổng hợp Quốc tế ITC Phú Hữu ' WHERE TableID='A016A' AND LOCODE='VNQPH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNQPH','VN',N'CANG QT ITC PHU HUU',N'Cảng Tổng hợp Quốc tế ITC Phú Hữu ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNVPK')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG VOPAK          ' ,Notes=N'VOPAK ( Cty TNHH Hóa Dầu AP)' WHERE TableID='A016A' AND LOCODE='VNVPK'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNVPK','VN',N'CANG VOPAK          ',N'VOPAK ( Cty TNHH Hóa Dầu AP)')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSCT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'SCT GAS VIET NAM    ' ,Notes=N'SCT GAS Việt Nam' WHERE TableID='A016A' AND LOCODE='VNSCT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSCT','VN',N'SCT GAS VIET NAM    ',N'SCT GAS Việt Nam')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNGHM')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'LPG HONG MOC        ' ,Notes=N'LPG Hồng Mộc' WHERE TableID='A016A' AND LOCODE='VNGHM'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNGHM','VN',N'LPG HONG MOC        ',N'LPG Hồng Mộc')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNSJH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'SANRIMJOHAP VINA    ' ,Notes=N'SANRIMJOHAP VINA' WHERE TableID='A016A' AND LOCODE='VNSJH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNSJH','VN',N'SANRIMJOHAP VINA    ',N'SANRIMJOHAP VINA')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDPH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG PHU DONG       ' ,Notes=N'Cảng Phú Đông' WHERE TableID='A016A' AND LOCODE='VNDPH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDPH','VN',N'CANG PHU DONG       ',N'Cảng Phú Đông')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNDPK')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'XANG DAU PHUOC KHANH' ,Notes=N'Xăng dầu Phước Khánh' WHERE TableID='A016A' AND LOCODE='VNDPK'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNDPK','VN',N'XANG DAU PHUOC KHANH',N'Xăng dầu Phước Khánh')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNLFA')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'LAFARGE XI MANG     ' ,Notes=N'LAFARGE Xi Măng' WHERE TableID='A016A' AND LOCODE='VNLFA'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNLFA','VN',N'LAFARGE XI MANG     ',N'LAFARGE Xi Măng')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNPPT')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'GAS PVC PHUOC THAI  ' ,Notes=N'GAS - PVC Phước Thái' WHERE TableID='A016A' AND LOCODE='VNPPT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNPPT','VN',N'GAS PVC PHUOC THAI  ',N'GAS - PVC Phước Thái')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNLTH')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG LONG THANH MN  ' ,Notes=N'Cảng Long Thành' WHERE TableID='A016A' AND LOCODE='VNLTH'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNLTH','VN',N'CANG LONG THANH MN  ',N'Cảng Long Thành')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNGHP')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'XANG DAU LPG HOA PHU' ,Notes=N'Bể cảng xăng dầu LPG Hòa Phú' WHERE TableID='A016A' AND LOCODE='VNGHP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNGHP','VN',N'XANG DAU LPG HOA PHU',N'Bể cảng xăng dầu LPG Hòa Phú')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNCXP')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG XANH VIP' ,Notes=N'Công ty cổ phần Cảng Xanh VIP' WHERE TableID='A016A' AND LOCODE='VNCXP'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNCXP','VN',N'CANG XANH VIP',N'Công ty cổ phần Cảng Xanh VIP')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNBDI')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CUA BIEN DE GI' ,Notes=N'Khu chuyển tải cửa Biển Đề Gi' WHERE TableID='A016A' AND LOCODE='VNBDI'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNBDI','VN',N'CUA BIEN DE GI',N'Khu chuyển tải cửa Biển Đề Gi')
END

IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNADTT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNADTT' , BorderGateName=N'CUA KHAU A DOT (THUA THIEN-HUE)' , Notes=N'Cửa khẩu A Đớt (Thừa Thiên-Huế)' WHERE TableID='A620A' AND BorderGateCode='VNADTT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNADTT',N'CUA KHAU A DOT (THUA THIEN-HUE)',N'Cửa khẩu A Đớt (Thừa Thiên-Huế)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBAAT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBAAT' , BorderGateName=N'CUA KHAU BAC HA (LAO CAI)' , Notes=N'Cửa khẩu Bắc Hà (Lao Cai)' WHERE TableID='A620A' AND BorderGateCode='VNBAAT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBAAT',N'CUA KHAU BAC HA (LAO CAI)',N'Cửa khẩu Bắc Hà (Lao Cai)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBATT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBATT' , BorderGateName=N'CUA KHAU BAT SAT (LAO CAI)' , Notes=N'Cửa khẩu Bát Sát (Lao Cai)' WHERE TableID='A620A' AND BorderGateCode='VNBATT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBATT',N'CUA KHAU BAT SAT (LAO CAI)',N'Cửa khẩu Bát Sát (Lao Cai)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBECT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBECT' , BorderGateName=N'HAI QUAN CUA KHAU BEN LUC' , Notes=N'Hải quan Cửa khẩu Bến Lức' WHERE TableID='A620A' AND BorderGateCode='VNBECT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBECT',N'HAI QUAN CUA KHAU BEN LUC',N'Hải quan Cửa khẩu Bến Lức') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBIAT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBIAT' , BorderGateName=N'CUA KHAU BI HA (CAO BANG)' , Notes=N'Cửa khẩu Bí Hà (Cao Bằng)' WHERE TableID='A620A' AND BorderGateCode='VNBIAT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBIAT',N'CUA KHAU BI HA (CAO BANG)',N'Cửa khẩu Bí Hà (Cao Bằng)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBIIT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBIIT' , BorderGateName=N'CUA KHAU BINH NGHI (LANG SON)' , Notes=N'Cửa khẩu Bình Nghi (Lạng Sơn)' WHERE TableID='A620A' AND BorderGateCode='VNBIIT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBIIT',N'CUA KHAU BINH NGHI (LANG SON)',N'Cửa khẩu Bình Nghi (Lạng Sơn)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBIPT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBIPT' , BorderGateName=N'CUA KHAU BINH HIEP (LONG AN)' , Notes=N'Cửa khẩu Bình Hiệp (Long An)' WHERE TableID='A620A' AND BorderGateCode='VNBIPT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBIPT',N'CUA KHAU BINH HIEP (LONG AN)',N'Cửa khẩu Bình Hiệp (Long An)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBNCT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBNCT' , BorderGateName=N'CUA KHAU BA NAM CUM (LAI CHAU)' , Notes=N'Cửa khẩu Ba Nậm Cúm (Lai Châu)' WHERE TableID='A620A' AND BorderGateCode='VNBNCT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBNCT',N'CUA KHAU BA NAM CUM (LAI CHAU)',N'Cửa khẩu Ba Nậm Cúm (Lai Châu)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBOYT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBOYT' , BorderGateName=N'CUA KHAU QUOC TE BO Y (KON TUM)' , Notes=N'Cửa khẩu Quốc tế Bờ Y (Kon Tum)' WHERE TableID='A620A' AND BorderGateCode='VNBOYT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBOYT',N'CUA KHAU QUOC TE BO Y (KON TUM)',N'Cửa khẩu Quốc tế Bờ Y (Kon Tum)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBPST')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBPST' , BorderGateName=N'CUA KHAU BAC PHONG SINH (QUANG NINH)' , Notes=N'Cửa khẩu Bắc Phong Sinh (Quảng Ninh)' WHERE TableID='A620A' AND BorderGateCode='VNBPST'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBPST',N'CUA KHAU BAC PHONG SINH (QUANG NINH)',N'Cửa khẩu Bắc Phong Sinh (Quảng Ninh)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBUGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBUGT' , BorderGateName=N'CUA KHAU BUPRANG (DAC LAC)' , Notes=N'Cửa khẩu BupRăng (Đắc Lắc)' WHERE TableID='A620A' AND BorderGateCode='VNBUGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBUGT',N'CUA KHAU BUPRANG (DAC LAC)',N'Cửa khẩu BupRăng (Đắc Lắc)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNCAGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNCAGT' , BorderGateName=N'CUA KHAU CA ROONG (QUANG BINH)' , Notes=N'Cửa khẩu Cà Roòng (Quảng Bình)' WHERE TableID='A620A' AND BorderGateCode='VNCAGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNCAGT',N'CUA KHAU CA ROONG (QUANG BINH)',N'Cửa khẩu Cà Roòng (Quảng Bình)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNCAOT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNCAOT' , BorderGateName=N'CUA KHAU CAU TREO (HA TINH)' , Notes=N'Cửa khẩu Cầu Treo (Hà Tĩnh)' WHERE TableID='A620A' AND BorderGateCode='VNCAOT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNCAOT',N'CUA KHAU CAU TREO (HA TINH)',N'Cửa khẩu Cầu Treo (Hà Tĩnh)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNCAUT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNCAUT' , BorderGateName=N'CUA KHAU CAO VEU' , Notes=N'Cửa khẩu Cao Vều' WHERE TableID='A620A' AND BorderGateCode='VNCAUT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNCAUT',N'CUA KHAU CAO VEU',N'Cửa khẩu Cao Vều') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNCHAT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNCHAT' , BorderGateName=N'CUA KHAU CHI MA (LANG SON)' , Notes=N'Cửa khẩu Chi Ma (Lạng Sơn)' WHERE TableID='A620A' AND BorderGateCode='VNCHAT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNCHAT',N'CUA KHAU CHI MA (LANG SON)',N'Cửa khẩu Chi Ma (Lạng Sơn)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNCHCT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNCHCT' , BorderGateName=N'CUA KHAU CHANG RIEC (TAY NINH)' , Notes=N'Cửa khẩu Chàng Riệc (Tây Ninh)' WHERE TableID='A620A' AND BorderGateCode='VNCHCT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNCHCT',N'CUA KHAU CHANG RIEC (TAY NINH)',N'Cửa khẩu Chàng Riệc (Tây Ninh)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNCHGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNCHGT' , BorderGateName=N'CUA KHAU CHIENG KHUONG (DIEN BIEN)' , Notes=N'Cửa khẩu Chiềng Khương (Điện Biên)' WHERE TableID='A620A' AND BorderGateCode='VNCHGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNCHGT',N'CUA KHAU CHIENG KHUONG (DIEN BIEN)',N'Cửa khẩu Chiềng Khương (Điện Biên)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNCHOT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNCHOT' , BorderGateName=N'CUA KHAU CHA LO (QUANG BINH)' , Notes=N'Cửa khẩu Cha Lo (Quảng Bình)' WHERE TableID='A620A' AND BorderGateCode='VNCHOT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNCHOT',N'CUA KHAU CHA LO (QUANG BINH)',N'Cửa khẩu Cha Lo (Quảng Bình)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNCOMT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNCOMT' , BorderGateName=N'CUA KHAU COC NAM (LANG SON)' , Notes=N'Cửa khẩu Cốc Nam (Lạng Sơn)' WHERE TableID='A620A' AND BorderGateCode='VNCOMT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNCOMT',N'CUA KHAU COC NAM (LANG SON)',N'Cửa khẩu Cốc Nam (Lạng Sơn)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNCOUT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNCOUT' , BorderGateName=N'CUA KHAU CO SAU' , Notes=N'Cửa khẩu Co Sâu' WHERE TableID='A620A' AND BorderGateCode='VNCOUT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNCOUT',N'CUA KHAU CO SAU',N'Cửa khẩu Co Sâu') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNDAKT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNDAKT' , BorderGateName=N'CUA KHAU DAK LONG (KONTUM)' , Notes=N'Cửa khẩu Đắk Long (Kontum)' WHERE TableID='A620A' AND BorderGateCode='VNDAKT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNDAKT',N'CUA KHAU DAK LONG (KONTUM)',N'Cửa khẩu Đắk Long (Kontum)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNDAOT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNDAOT' , BorderGateName=N'CUA KHAU DAKBLO (KONTUM)' , Notes=N'Cửa khẩu ĐăkBlô (Kontum)' WHERE TableID='A620A' AND BorderGateCode='VNDAOT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNDAOT',N'CUA KHAU DAKBLO (KONTUM)',N'Cửa khẩu ĐăkBlô (Kontum)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNDART')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNDART' , BorderGateName=N'CUA KHAU DAK PEUR (DAC LAK)' , Notes=N'Cửa khẩu Đắk Peur (Đắc Lắk)' WHERE TableID='A620A' AND BorderGateCode='VNDART'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNDART',N'CUA KHAU DAK PEUR (DAC LAK)',N'Cửa khẩu Đắk Peur (Đắc Lắk)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNDIAT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNDIAT' , BorderGateName=N'CUA KHAU DINH BA (DONG THAP)' , Notes=N'Cửa khẩu Dinh Bà (Đồng Tháp)' WHERE TableID='A620A' AND BorderGateCode='VNDIAT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNDIAT',N'CUA KHAU DINH BA (DONG THAP)',N'Cửa khẩu Dinh Bà (Đồng Tháp)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNGIHT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNGIHT' , BorderGateName=N'CUA KHAU GIANG THANH (KIEN GIANG)' , Notes=N'Cửa khẩu Giang Thành (Kiên Giang))' WHERE TableID='A620A' AND BorderGateCode='VNGIHT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNGIHT',N'CUA KHAU GIANG THANH (KIEN GIANG)',N'Cửa khẩu Giang Thành (Kiên Giang))') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNHOAT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNHOAT' , BorderGateName=N'CUA KHAU HOA LU (BINH PHUOC)' , Notes=N'Cửa khẩu Hoa Lư (Bình Phước)' WHERE TableID='A620A' AND BorderGateCode='VNHOAT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNHOAT',N'CUA KHAU HOA LU (BINH PHUOC)',N'Cửa khẩu Hoa Lư (Bình Phước)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNHOHT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNHOHT' , BorderGateName=N'CUA KHAU HONG LINH (Hà TINH)' , Notes=N'Cửa khẩu Hồng lĩnh (Hà Tĩnh)' WHERE TableID='A620A' AND BorderGateCode='VNHOHT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNHOHT',N'CUA KHAU HONG LINH (Hà TINH)',N'Cửa khẩu Hồng lĩnh (Hà Tĩnh)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNHOOT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNHOOT' , BorderGateName=N'CUA KHAU HOANH MO (QUANG NINH)' , Notes=N'Cửa khẩu Hoành Mô (Quảng Ninh)' WHERE TableID='A620A' AND BorderGateCode='VNHOOT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNHOOT',N'CUA KHAU HOANH MO (QUANG NINH)',N'Cửa khẩu Hoành Mô (Quảng Ninh)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNHOUT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNHOUT' , BorderGateName=N'CUA KHAU HOANG DIEU (BINH PHUOC)' , Notes=N'Cửa khẩu Hoàng Diệu (Bình Phước)' WHERE TableID='A620A' AND BorderGateCode='VNHOUT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNHOUT',N'CUA KHAU HOANG DIEU (BINH PHUOC)',N'Cửa khẩu Hoàng Diệu (Bình Phước)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNHOVT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNHOVT' , BorderGateName=N'CUA KHAU HONG VAN (THUA THIEN-HUE)' , Notes=N'Cửa khẩu Hồng Vân (Thừa Thiên-Huế)' WHERE TableID='A620A' AND BorderGateCode='VNHOVT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNHOVT',N'CUA KHAU HONG VAN (THUA THIEN-HUE)',N'Cửa khẩu Hồng Vân (Thừa Thiên-Huế)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNHTIT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNHTIT' , BorderGateName=N'CUA KHAU HA TIEN (KIEN GIANG)' , Notes=N'Cửa khẩu Hà Tiên (Kiên Giang)' WHERE TableID='A620A' AND BorderGateCode='VNHTIT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNHTIT',N'CUA KHAU HA TIEN (KIEN GIANG)',N'Cửa khẩu Hà Tiên (Kiên Giang)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNHUCT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNHUCT' , BorderGateName=N'CUA KHAU HUOI PUOC (DIEN BIEN)' , Notes=N'Cửa khẩu Huổi Puốc (Điện Biên)' WHERE TableID='A620A' AND BorderGateCode='VNHUCT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNHUCT',N'CUA KHAU HUOI PUOC (DIEN BIEN)',N'Cửa khẩu Huổi Puốc (Điện Biên)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNHUGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNHUGT' , BorderGateName=N'CUA KHAU HUU NGHI (LANG SON)' , Notes=N'Cửa khẩu Hữu Nghị (Lạng Sơn)' WHERE TableID='A620A' AND BorderGateCode='VNHUGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNHUGT',N'CUA KHAU HUU NGHI (LANG SON)',N'Cửa khẩu Hữu Nghị (Lạng Sơn)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNHUNT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNHUNT' , BorderGateName=N'CUA KHAU HUNG DIEN (LONG AN)' , Notes=N'Cửa khẩu Hưng Điền (Long An)' WHERE TableID='A620A' AND BorderGateCode='VNHUNT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNHUNT',N'CUA KHAU HUNG DIEN (LONG AN)',N'Cửa khẩu Hưng Điền (Long An)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNKAMT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNKAMT' , BorderGateName=N'CUA KHAU KATUM (TAY NINH)' , Notes=N'Cửa khẩu Katum (Tây Ninh)' WHERE TableID='A620A' AND BorderGateCode='VNKAMT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNKAMT',N'CUA KHAU KATUM (TAY NINH)',N'Cửa khẩu Katum (Tây Ninh)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNKHBT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNKHBT' , BorderGateName=N'CUA KHAU KHANH BINH (AN GIANG)' , Notes=N'Cửa khẩu Khánh Bình (An Giang)' WHERE TableID='A620A' AND BorderGateCode='VNKHBT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNKHBT',N'CUA KHAU KHANH BINH (AN GIANG)',N'Cửa khẩu Khánh Bình (An Giang)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNLAIT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNLAIT' , BorderGateName=N'CUA KHAU LAO CAI (LAO CAI)' , Notes=N'Cửa khẩu Lao Cai (Lao Cai)' WHERE TableID='A620A' AND BorderGateCode='VNLAIT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNLAIT',N'CUA KHAU LAO CAI (LAO CAI)',N'Cửa khẩu Lao Cai (Lao Cai)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNLAOT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNLAOT' , BorderGateName=N'CUA KHAU LAO BAO (QUANG TRI)' , Notes=N'Cửa khẩu Lao Bảo (Quảng Trị)' WHERE TableID='A620A' AND BorderGateCode='VNLAOT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNLAOT',N'CUA KHAU LAO BAO (QUANG TRI)',N'Cửa khẩu Lao Bảo (Quảng Trị)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNLAYT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNLAYT' , BorderGateName=N'CUA KHAU LA LAY (QUANG TRI)' , Notes=N'Cửa khẩu La Lay (Quảng Trị)' WHERE TableID='A620A' AND BorderGateCode='VNLAYT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNLAYT',N'CUA KHAU LA LAY (QUANG TRI)',N'Cửa khẩu La Lay (Quảng Trị)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNLEHT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNLEHT' , BorderGateName=N'CUA KHAU LE THANH (GIA LAI)' , Notes=N'Cửa khẩu Lệ Thanh (Gia Lai)' WHERE TableID='A620A' AND BorderGateCode='VNLEHT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNLEHT',N'CUA KHAU LE THANH (GIA LAI)',N'Cửa khẩu Lệ Thanh (Gia Lai)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNLOPT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNLOPT' , BorderGateName=N'CUA KHAU LONG SAP (DIEN BIEN)' , Notes=N'Cửa khẩu Lóng Sập (Điện Biên)' WHERE TableID='A620A' AND BorderGateCode='VNLOPT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNLOPT',N'CUA KHAU LONG SAP (DIEN BIEN)',N'Cửa khẩu Lóng Sập (Điện Biên)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNLYNT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNLYNT' , BorderGateName=N'CUA KHAU LY VAN (CAO BANG)' , Notes=N'Cửa khẩu Lý Vạn (Cao Bằng)' WHERE TableID='A620A' AND BorderGateCode='VNLYNT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNLYNT',N'CUA KHAU LY VAN (CAO BANG)',N'Cửa khẩu Lý Vạn (Cao Bằng)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNMLTT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNMLTT' , BorderGateName=N'CUA KHAU MA LU THANG (DIEN BIEN)' , Notes=N'Cửa khẩu Ma Lu Thàng (Điện Biên)' WHERE TableID='A620A' AND BorderGateCode='VNMLTT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNMLTT',N'CUA KHAU MA LU THANG (DIEN BIEN)',N'Cửa khẩu Ma Lu Thàng (Điện Biên)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNMOAT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNMOAT' , BorderGateName=N'CUA KHAU MOC HOA (LONG AN)' , Notes=N'Cửa khẩu Mộc Hóa (Long An)' WHERE TableID='A620A' AND BorderGateCode='VNMOAT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNMOAT',N'CUA KHAU MOC HOA (LONG AN)',N'Cửa khẩu Mộc Hóa (Long An)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNMOCT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNMOCT' , BorderGateName=N'CUA KHAU MONG CAI (QUANG NINH)' , Notes=N'Cửa khẩu Móng Cái (Quảng Ninh)' WHERE TableID='A620A' AND BorderGateCode='VNMOCT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNMOCT',N'CUA KHAU MONG CAI (QUANG NINH)',N'Cửa khẩu Móng Cái (Quảng Ninh)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNMOIT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNMOIT' , BorderGateName=N'CUA KHAU MOC BAI (TAY NINH)' , Notes=N'Cửa khẩu Mộc Bài (Tây Ninh)' WHERE TableID='A620A' AND BorderGateCode='VNMOIT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNMOIT',N'CUA KHAU MOC BAI (TAY NINH)',N'Cửa khẩu Mộc Bài (Tây Ninh)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNMQTT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNMQTT' , BorderGateName=N'CUA KHAU MY QUY TAY (LONG AN)' , Notes=N'Cửa khẩu Mỹ Quý Tây (Long An)' WHERE TableID='A620A' AND BorderGateCode='VNMQTT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNMQTT',N'CUA KHAU MY QUY TAY (LONG AN)',N'Cửa khẩu Mỹ Quý Tây (Long An)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNMUGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNMUGT' , BorderGateName=N'CUA KHAU MUONG KHUONG (LAO CAI)' , Notes=N'Cửa khẩu Mường Khương (Lao Cai)' WHERE TableID='A620A' AND BorderGateCode='VNMUGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNMUGT',N'CUA KHAU MUONG KHUONG (LAO CAI)',N'Cửa khẩu Mường Khương (Lao Cai)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNNAAT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNNAAT' , BorderGateName=N'CUA KHAU NA NUA (LANG SON)' , Notes=N'Cửa khẩu Na Nưa (Lạng Sơn)' WHERE TableID='A620A' AND BorderGateCode='VNNAAT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNNAAT',N'CUA KHAU NA NUA (LANG SON)',N'Cửa khẩu Na Nưa (Lạng Sơn)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNNACT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNNACT' , BorderGateName=N'CUA KHAU NAM CAN (NGHE AN)' , Notes=N'Cửa khẩu Nậm Cắn (Nghệ An)' WHERE TableID='A620A' AND BorderGateCode='VNNACT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNNACT',N'CUA KHAU NAM CAN (NGHE AN)',N'Cửa khẩu Nậm Cắn (Nghệ An)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNNAGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNNAGT' , BorderGateName=N'CUA KHAU NAM GIANG (QUANG NAM)' , Notes=N'Cửa khẩu Nam Giang (Quảng Nam)' WHERE TableID='A620A' AND BorderGateCode='VNNAGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNNAGT',N'CUA KHAU NAM GIANG (QUANG NAM)',N'Cửa khẩu Nam Giang (Quảng Nam)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNNAHT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNNAHT' , BorderGateName=N'CUA KHAU NA HINH (LANG SON)' , Notes=N'Cửa khẩu Na Hình (Lạng Sơn)' WHERE TableID='A620A' AND BorderGateCode='VNNAHT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNNAHT',N'CUA KHAU NA HINH (LANG SON)',N'Cửa khẩu Na Hình (Lạng Sơn)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNNALT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNNALT' , BorderGateName=N'CUA KHAU NA LAN (CAO BANG)' , Notes=N'Cửa khẩu Nà Lạn (Cao Bằng)' WHERE TableID='A620A' AND BorderGateCode='VNNALT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNNALT',N'CUA KHAU NA LAN (CAO BANG)',N'Cửa khẩu Nà Lạn (Cao Bằng)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNNAOT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNNAOT' , BorderGateName=N'CUA KHAU NA MOO (THANH HOO)' , Notes=N'Cửa khẩu Na Mèo (Thanh Hoá)' WHERE TableID='A620A' AND BorderGateCode='VNNAOT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNNAOT',N'CUA KHAU NA MOO (THANH HOO)',N'Cửa khẩu Na Mèo (Thanh Hoá)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNNGTT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNNGTT' , BorderGateName=N'CUA KHAU NGHIA THUAN (Hà GIANG)' , Notes=N'Cửa khẩu Nghĩa Thuận (Hà Giang)' WHERE TableID='A620A' AND BorderGateCode='VNNGTT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNNGTT',N'CUA KHAU NGHIA THUAN (Hà GIANG)',N'Cửa khẩu Nghĩa Thuận (Hà Giang)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNNTNT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNNTNT' , BorderGateName=N'CUA KHAU NINH THUAN (KHANH HUA)' , Notes=N'Cửa khẩu Ninh Thuận (Khánh Hòa)' WHERE TableID='A620A' AND BorderGateCode='VNNTNT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNNTNT',N'CUA KHAU NINH THUAN (KHANH HUA)',N'Cửa khẩu Ninh Thuận (Khánh Hòa)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNPAMT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNPAMT' , BorderGateName=N'CUA KHAU PA THOM (LAI CHAU)' , Notes=N'Cửa khẩu Pa Thơm (Lai Châu)' WHERE TableID='A620A' AND BorderGateCode='VNPAMT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNPAMT',N'CUA KHAU PA THOM (LAI CHAU)',N'Cửa khẩu Pa Thơm (Lai Châu)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNPBGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNPBGT' , BorderGateName=N'CUA KHAU PHO BANG (HA GIANG)' , Notes=N'Cửa khẩu Phó Bảng (Hà Giang)' WHERE TableID='A620A' AND BorderGateCode='VNPBGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNPBGT',N'CUA KHAU PHO BANG (HA GIANG)',N'Cửa khẩu Phó Bảng (Hà Giang)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNPHTT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNPHTT' , BorderGateName=N'CUA KHAU PHUOC TAN (TAY NINH)' , Notes=N'Cửa khẩu Phước Tân (Tây Ninh)' WHERE TableID='A620A' AND BorderGateCode='VNPHTT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNPHTT',N'CUA KHAU PHUOC TAN (TAY NINH)',N'Cửa khẩu Phước Tân (Tây Ninh)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNPOOT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNPOOT' , BorderGateName=N'CUA KHAU PO PEO (CAO BANG)' , Notes=N'Cửa khẩu Pò Peo (Cao Bằng)' WHERE TableID='A620A' AND BorderGateCode='VNPOOT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNPOOT',N'CUA KHAU PO PEO (CAO BANG)',N'Cửa khẩu Pò Peo (Cao Bằng)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNSOCT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNSOCT' , BorderGateName=N'CUA KHAU SONG DOC (MINH HAI)' , Notes=N'Cửa khẩu Sông Đốc (Minh Hải)' WHERE TableID='A620A' AND BorderGateCode='VNSOCT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNSOCT',N'CUA KHAU SONG DOC (MINH HAI)',N'Cửa khẩu Sông Đốc (Minh Hải)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNSOGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNSOGT' , BorderGateName=N'CUA KHAU SOC GIANG (CAO BANG)' , Notes=N'Cửa khẩu Sóc Giang (Cao Bằng)' WHERE TableID='A620A' AND BorderGateCode='VNSOGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNSOGT',N'CUA KHAU SOC GIANG (CAO BANG)',N'Cửa khẩu Sóc Giang (Cao Bằng)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNSOTT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNSOTT' , BorderGateName=N'CUA KHAU SO THUONG (DONG THAP)' , Notes=N'Cửa khẩu Sở Thượng (Đồng Tháp)' WHERE TableID='A620A' AND BorderGateCode='VNSOTT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNSOTT',N'CUA KHAU SO THUONG (DONG THAP)',N'Cửa khẩu Sở Thượng (Đồng Tháp)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTAGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTAGT' , BorderGateName=N'CUA KHAU TA LUNG (CAO BANG)' , Notes=N'Cửa khẩu Tà Lùng (Cao Bằng)' WHERE TableID='A620A' AND BorderGateCode='VNTAGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTAGT',N'CUA KHAU TA LUNG (CAO BANG)',N'Cửa khẩu Tà Lùng (Cao Bằng)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTATT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTATT' , BorderGateName=N'CUA KHAU TA VAT (BINH PHUOC)' , Notes=N'Cửa khẩu Tà Vát (Bình Phước)' WHERE TableID='A620A' AND BorderGateCode='VNTATT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTATT',N'CUA KHAU TA VAT (BINH PHUOC)',N'Cửa khẩu Tà Vát (Bình Phước)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTAYT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTAYT' , BorderGateName=N'CUA KHAU TAY TRANG (DIEN BIEN)' , Notes=N'Cửa khẩu Tây Trang (Điện Biên)' WHERE TableID='A620A' AND BorderGateCode='VNTAYT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTAYT',N'CUA KHAU TAY TRANG (DIEN BIEN)',N'Cửa khẩu Tây Trang (Điện Biên)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTHCT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTHCT' , BorderGateName=N'CUA KHAU THUONG PHUOC (DONG THAP)' , Notes=N'Cửa khẩu Thường Phước (Đồng Tháp)' WHERE TableID='A620A' AND BorderGateCode='VNTHCT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTHCT',N'CUA KHAU THUONG PHUOC (DONG THAP)',N'Cửa khẩu Thường Phước (Đồng Tháp)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTHHT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTHHT' , BorderGateName=N'CUA KHAU THONG BINH (DONG THAP)' , Notes=N'Cửa khẩu Thông Bình (Đồng Tháp)' WHERE TableID='A620A' AND BorderGateCode='VNTHHT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTHHT',N'CUA KHAU THONG BINH (DONG THAP)',N'Cửa khẩu Thông Bình (Đồng Tháp)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTHNT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTHNT' , BorderGateName=N'HAI QUAN THUY AN (HUE)' , Notes=N'Hải quan Thủy An (Huế)' WHERE TableID='A620A' AND BorderGateCode='VNTHNT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTHNT',N'HAI QUAN THUY AN (HUE)',N'Hải quan Thủy An (Huế)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTHTT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTHTT' , BorderGateName=N'CUA KHAU THONG THU' , Notes=N'Cửa khẩu Thông Thụ' WHERE TableID='A620A' AND BorderGateCode='VNTHTT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTHTT',N'CUA KHAU THONG THU',N'Cửa khẩu Thông Thụ') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTINT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTINT' , BorderGateName=N'CUA KHAU TINH BIEN (AN GIANG)' , Notes=N'Cửa khẩu Tịnh Biên (An Giang)' WHERE TableID='A620A' AND BorderGateCode='VNTINT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTINT',N'CUA KHAU TINH BIEN (AN GIANG)',N'Cửa khẩu Tịnh Biên (An Giang)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTRAT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTRAT' , BorderGateName=N'CUA KHAU TRA LINH (CAO BANG)' , Notes=N'Cửa khẩu Trà Lĩnh (Cao Bằng)' WHERE TableID='A620A' AND BorderGateCode='VNTRAT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTRAT',N'CUA KHAU TRA LINH (CAO BANG)',N'Cửa khẩu Trà Lĩnh (Cao Bằng)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTRHT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTRHT' , BorderGateName=N'CUA KHAU TRUNG KHANH (CAO BANG)' , Notes=N'Cửa khẩu Trùng Khánh (Cao Bằng)' WHERE TableID='A620A' AND BorderGateCode='VNTRHT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTRHT',N'CUA KHAU TRUNG KHANH (CAO BANG)',N'Cửa khẩu Trùng Khánh (Cao Bằng)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTTHT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTTHT' , BorderGateName=N'CUA KHAU TAN THANH (LANG SON)' , Notes=N'Cửa khẩu Tân Thanh (Lạng Sơn)' WHERE TableID='A620A' AND BorderGateCode='VNTTHT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTTHT',N'CUA KHAU TAN THANH (LANG SON)',N'Cửa khẩu Tân Thanh (Lạng Sơn)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTTYT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTTYT' , BorderGateName=N'CUA KHAU THANH THUY (HA GIANG)' , Notes=N'Cửa khẩu Thanh Thủy (Hà Giang)' WHERE TableID='A620A' AND BorderGateCode='VNTTYT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTTYT',N'CUA KHAU THANH THUY (HA GIANG)',N'Cửa khẩu Thanh Thủy (Hà Giang)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNVHDT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNVHDT' , BorderGateName=N'CUA KHAU VINH HOI DONG (AN GIANG)' , Notes=N'Cửa khẩu Vĩnh Hội Đông (An Giang)' WHERE TableID='A620A' AND BorderGateCode='VNVHDT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNVHDT',N'CUA KHAU VINH HOI DONG (AN GIANG)',N'Cửa khẩu Vĩnh Hội Đông (An Giang)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNVIGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNVIGT' , BorderGateName=N'CUA KHAU VINH XUONG (AN GIANG)' , Notes=N'Cửa khẩu Vĩnh Xương (An Giang)' WHERE TableID='A620A' AND BorderGateCode='VNVIGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNVIGT',N'CUA KHAU VINH XUONG (AN GIANG)',N'Cửa khẩu Vĩnh Xương (An Giang)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNXAMT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNXAMT' , BorderGateName=N'CUA KHAU XA MAT (TAY NINH)' , Notes=N'Cửa khẩu Xa Mát (Tây Ninh)' WHERE TableID='A620A' AND BorderGateCode='VNXAMT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNXAMT',N'CUA KHAU XA MAT (TAY NINH)',N'Cửa khẩu Xa Mát (Tây Ninh)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNXLOT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNXLOT' , BorderGateName=N'CUA KHAU BUU DIEN LONG XUYEN (AN GIANG)' , Notes=N'Cửa khẩu Bưu điện Long xuyên (An Giang)' WHERE TableID='A620A' AND BorderGateCode='VNXLOT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNXLOT',N'CUA KHAU BUU DIEN LONG XUYEN (AN GIANG)',N'Cửa khẩu Bưu điện Long xuyên (An Giang)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNXMNT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNXMNT' , BorderGateName=N'CUA KHAU XIN MAN (HA GIANG)' , Notes=N'Cửa khẩu Xín Mần (Hà Giang)' WHERE TableID='A620A' AND BorderGateCode='VNXMNT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNXMNT',N'CUA KHAU XIN MAN (HA GIANG)',N'Cửa khẩu Xín Mần (Hà Giang)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNXPNT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNXPNT' , BorderGateName=N'CUA KHAU XAM PUN (HA GIANG)' , Notes=N'Cửa khẩu Xăm Pun (Hà Giang)' WHERE TableID='A620A' AND BorderGateCode='VNXPNT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNXPNT',N'CUA KHAU XAM PUN (HA GIANG)',N'Cửa khẩu Xăm Pun (Hà Giang)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNLOHT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNLOHT' , BorderGateName=N'CUA KHAU CHINH LOC THINH' , Notes=N'Cửa khẩu chính Lộc Thịnh' WHERE TableID='A620A' AND BorderGateCode='VNLOHT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNLOHT',N'CUA KHAU CHINH LOC THINH',N'Cửa khẩu chính Lộc Thịnh') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTANT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTANT' , BorderGateName=N'CUA KHAU PHU TON TIEN' , Notes=N'Cửa khẩu phụ Tân Tiến' WHERE TableID='A620A' AND BorderGateCode='VNTANT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTANT',N'CUA KHAU PHU TON TIEN',N'Cửa khẩu phụ Tân Tiến') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNDAET')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNDAET' , BorderGateName=N'CUA KHAU DAK RUE' , Notes=N'Cửa khẩu Đắk Ruê' WHERE TableID='A620A' AND BorderGateCode='VNDAET'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNDAET',N'CUA KHAU DAK RUE',N'Cửa khẩu Đắk Ruê') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNMAGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNMAGT' , BorderGateName=N'CUA KHAU MA LU THANG' , Notes=N'Cửa khẩu Ma Lù Thàng' WHERE TableID='A620A' AND BorderGateCode='VNMAGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNMAGT',N'CUA KHAU MA LU THANG',N'Cửa khẩu Ma Lù Thàng') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNPTOT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNPTOT' , BorderGateName=N'LOI MO PO TO (MA LU THANG)' , Notes=N'Lối mở Pô Tô nằm trong khu kinh tế cửa khẩu Ma Lù Thàng' WHERE TableID='A620A' AND BorderGateCode='VNPTOT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNPTOT',N'LOI MO PO TO (MA LU THANG)',N'Lối mở Pô Tô nằm trong khu kinh tế cửa khẩu Ma Lù Thàng') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNNAIT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNNAIT' , BorderGateName=N'CUA KHAU PHU NA CAI' , Notes=N'Cửa khẩu phụ Nà Cài' WHERE TableID='A620A' AND BorderGateCode='VNNAIT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNNAIT',N'CUA KHAU PHU NA CAI',N'Cửa khẩu phụ Nà Cài') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBACT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBACT' , BorderGateName=N'CUA KHAU PHU BAN VUOC' , Notes=N'Cửa khẩu phụ Bản Vược' WHERE TableID='A620A' AND BorderGateCode='VNBACT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBACT',N'CUA KHAU PHU BAN VUOC',N'Cửa khẩu phụ Bản Vược') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTGGT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTGGT' , BorderGateName=N'CUA KHAU TAY GIANG' , Notes=N'Cửa khẩu Tây Giang' WHERE TableID='A620A' AND BorderGateCode='VNTGGT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTGGT',N'CUA KHAU TAY GIANG',N'Cửa khẩu Tây Giang') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNVAAT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNVAAT' , BorderGateName=N'CUA KHAU VAC SA' , Notes=N'Cửa khẩu Vạc Sa' WHERE TableID='A620A' AND BorderGateCode='VNVAAT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNVAAT',N'CUA KHAU VAC SA',N'Cửa khẩu Vạc Sa') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTLCT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTLCT' , BorderGateName=N'CUA KHAU TONG LE CHAN' , Notes=N'Cửa khẩu Tống Lê Chân' WHERE TableID='A620A' AND BorderGateCode='VNTLCT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTLCT',N'CUA KHAU TONG LE CHAN',N'Cửa khẩu Tống Lê Chân') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTHYT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTHYT' , BorderGateName=N'CK QUOC GIA THANH THUY (NGHE AN)' , Notes=N'Cửa khẩu quốc gia Thanh thủy (Nghệ An)' WHERE TableID='A620A' AND BorderGateCode='VNTHYT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTHYT',N'CK QUOC GIA THANH THUY (NGHE AN)',N'Cửa khẩu quốc gia Thanh thủy (Nghệ An)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNPTHT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNPTHT' , BorderGateName=N'CUA KHAU PHU THONG THU' , Notes=N'Cửa khẩu phụ Thông thụ' WHERE TableID='A620A' AND BorderGateCode='VNPTHT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNPTHT',N'CUA KHAU PHU THONG THU',N'Cửa khẩu phụ Thông thụ') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTAHT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTAHT' , BorderGateName=N'CUA KHAU PHU TAM HOP' , Notes=N'Cửa khẩu phụTam Hợp' WHERE TableID='A620A' AND BorderGateCode='VNTAHT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTAHT',N'CUA KHAU PHU TAM HOP',N'Cửa khẩu phụTam Hợp') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBUUT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBUUT' , BorderGateName=N'CUA KHAU PHU BUOC MU' , Notes=N'Cửa khẩu phụ Buộc Mú' WHERE TableID='A620A' AND BorderGateCode='VNBUUT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBUUT',N'CUA KHAU PHU BUOC MU',N'Cửa khẩu phụ Buộc Mú') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNCAVT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNCAVT' , BorderGateName=N'LOI MO CAO VEU' , Notes=N'Lối mở Cao Vều' WHERE TableID='A620A' AND BorderGateCode='VNCAVT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNCAVT',N'LOI MO CAO VEU',N'Lối mở Cao Vều') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNTAOT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNTAOT' , BorderGateName=N'LOI MO TA DO' , Notes=N'Lối mở Ta Đo' WHERE TableID='A620A' AND BorderGateCode='VNTAOT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNTAOT',N'LOI MO TA DO',N'Lối mở Ta Đo') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNXINT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNXINT' , BorderGateName=N'LOI MO XIENG TREN ( MY LY )' , Notes=N'Lối mở Xiềng Trên ( Mỹ Lý )' WHERE TableID='A620A' AND BorderGateCode='VNXINT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNXINT',N'LOI MO XIENG TREN ( MY LY )',N'Lối mở Xiềng Trên ( Mỹ Lý )') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNDGLS')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNDGLS' , BorderGateName=N'DONG DANG (LANG SON)               ' , Notes=N'Ga Đồng Đăng' WHERE TableID='A620A' AND BorderGateCode='VNDGLS'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNDGLS',N'DONG DANG (LANG SON)               ',N'Ga Đồng Đăng') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNLCLC')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNLCLC' , BorderGateName=N'LAO CAI                            ' , Notes=N'Ga Lào Cai' WHERE TableID='A620A' AND BorderGateCode='VNLCLC'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNLCLC',N'LAO CAI                            ',N'Ga Lào Cai') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNYVHN')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNYVHN' , BorderGateName=N'YEN VIEN (HA NOI)                  ' , Notes=N'Ga Yên Viên' WHERE TableID='A620A' AND BorderGateCode='VNYVHN'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNYVHN',N'YEN VIEN (HA NOI)                  ',N'Ga Yên Viên') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNLCHT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNLCHT' , BorderGateName=N'LOI MO MOC 238 LAO CHAI' , Notes=N'Lối mở mốc 238 Lao Chải' WHERE TableID='A620A' AND BorderGateCode='VNLCHT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNLCHT',N'LOI MO MOC 238 LAO CHAI',N'Lối mở mốc 238 Lao Chải') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNITSL')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNITSL' , BorderGateName=N'ICD TIEN SON BAC NINH' , Notes=N'ICD Tiên Sơn - Bắc Ninh' WHERE TableID='A620A' AND BorderGateCode='VNITSL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNITSL',N'ICD TIEN SON BAC NINH',N'ICD Tiên Sơn - Bắc Ninh') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNKCTL')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNKCTL' , BorderGateName=N'KHU KTCK CAU TREO' , Notes=N'Khu kinh tế cửa khẩu Cầu Treo' WHERE TableID='A620A' AND BorderGateCode='VNKCTL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNKCTL',N'KHU KTCK CAU TREO',N'Khu kinh tế cửa khẩu Cầu Treo') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNKLBL')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNKLBL' , BorderGateName=N'KHU TM LAO BAO' , Notes=N'Khu thương mại Lao Bảo' WHERE TableID='A620A' AND BorderGateCode='VNKLBL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNKLBL',N'KHU TM LAO BAO',N'Khu thương mại Lao Bảo') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNBUPT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNBUPT' , BorderGateName=N'CK PHU (LOI MO) 751 (DAK LAK)' , Notes=N'Cửa khẩu phụ (lối mở) 751 (Đắk Lắk)' WHERE TableID='A620A' AND BorderGateCode='VNBUPT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNBUPT',N'CK PHU (LOI MO) 751 (DAK LAK)',N'Cửa khẩu phụ (lối mở) 751 (Đắk Lắk)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNLTAT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNLTAT' , BorderGateName=N'LOI MO LOC TAN (BINH PHUOC)' , Notes=N'Lối mở Lộc Tấn (Bình Phước)' WHERE TableID='A620A' AND BorderGateCode='VNLTAT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNLTAT',N'LOI MO LOC TAN (BINH PHUOC)',N'Lối mở Lộc Tấn (Bình Phước)') 
END
IF EXISTS (SELECT * FROM t_VNACC_Category_BorderGate WHERE TableID ='A620A' AND BorderGateCode='VNAPIT')
BEGIN
UPDATE t_VNACC_Category_BorderGate SET BorderGateCode=N'VNAPIT' , BorderGateName=N'LOI MO A PA CHAI' , Notes=N'Lối mở A Pa Chải' WHERE TableID='A620A' AND BorderGateCode='VNAPIT'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_BorderGate(TableID,BorderGateCode,BorderGateName,Notes) VALUES ('A620A',N'VNAPIT',N'LOI MO A PA CHAI',N'Lối mở A Pa Chải') 
END


IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'NIPPON EXPRESS VN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAAL','VN',N'NIPPON EXPRESS VN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFABL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'PT HANG HAI HN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFABL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFABL','VN',N'PT HANG HAI HN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFACL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DLVT SAFI HA NOI' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFACL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFACL','VN',N'DLVT SAFI HA NOI',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFADL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'ICD TRANSIMEX SG' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFADL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFADL','VN',N'ICD TRANSIMEX SG',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAEL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TNHH CANG PHUOC LONG' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAEL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAEL','VN',N'TNHH CANG PHUOC LONG',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAFL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TNHH XNK TAY NAM' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAFL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAFL','VN',N'TNHH XNK TAY NAM',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAGL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CP TM XNK DONG TAY' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAGL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAGL','VN',N'CP TM XNK DONG TAY',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAHL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CFS SAFI HO CHI MINH' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAHL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAHL','VN',N'CFS SAFI HO CHI MINH',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAIL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KHO VAN BINH MINH' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAIL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAIL','VN',N'KHO VAN BINH MINH',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAJL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'SAGAWA EXPRESS VN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAJL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAJL','VN',N'SAGAWA EXPRESS VN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAKL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TIEP VAN SO 1' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAKL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAKL','VN',N'TIEP VAN SO 1',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFALL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DICH VU HANG HAI SG' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFALL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFALL','VN',N'DICH VU HANG HAI SG',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAML')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DV HH VINALINES HP' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAML'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAML','VN',N'DV HH VINALINES HP',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFANL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'QUOC TE NHAT VIET' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFANL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFANL','VN',N'QUOC TE NHAT VIET',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAOL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CONTAINER VIETNAM' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAOL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAOL','VN',N'CONTAINER VIETNAM',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAPL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'GEMADEPT HP' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAPL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAPL','VN',N'GEMADEPT HP',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAQL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'LDKHAITHAC CONTAINER' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAQL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAQL','VN',N'LDKHAITHAC CONTAINER',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFARL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CONTAINER MINH THANH' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFARL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFARL','VN',N'CONTAINER MINH THANH',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFASL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TIEP VAN NAM PHAT' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFASL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFASL','VN',N'TIEP VAN NAM PHAT',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFATL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'QUOC TE SAO DO' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFATL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFATL','VN',N'QUOC TE SAO DO',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAUL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'HANG HAI VN PHIA BAC' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAUL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAUL','VN',N'HANG HAI VN PHIA BAC',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAVL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'VAN TAI DUYEN HAI' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAVL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAVL','VN',N'VAN TAI DUYEN HAI',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAWL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'VAN TAI THUE TAU' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAWL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAWL','VN',N'VAN TAI THUE TAU',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAXL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TAN TIEN PHONG' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAXL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAXL','VN',N'TAN TIEN PHONG',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAYL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'VANTAI CONG NGHE CAO' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAYL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAYL','VN',N'VANTAI CONG NGHE CAO',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFAZL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'LOGISTICS XANH' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFAZL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFAZL','VN',N'LOGISTICS XANH',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'NAM LIEN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBAL','VN',N'NAM LIEN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBBL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TM DL BINH DUONG' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBBL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBBL','VN',N'TM DL BINH DUONG',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBCL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'ICD TANCANG SONGTHAN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBCL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBCL','VN',N'ICD TANCANG SONGTHAN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBDL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TAN HOAN CAU' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBDL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBDL','VN',N'TAN HOAN CAU',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBEL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG LS' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBEL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBEL','VN',N'CANG LS',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBFL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DAU TU BAC KY' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNFBFL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBFL','VN',N'DAU TU BAC KY',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBGL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCPXD GIAO THONG' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBGL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBGL','VN',N'CTCPXD GIAO THONG',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBHL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'INLACO LOGISTIC' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBHL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBHL','VN',N'INLACO LOGISTIC',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBIL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CFS CT LEN SAI GON' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBIL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBIL','VN',N'CFS CT LEN SAI GON',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBJL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT ISHENG ELECTRIC ' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBJL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBJL','VN',N'CT ISHENG ELECTRIC ',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBKL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'FUNING PRECISION' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBKL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBKL','VN',N'FUNING PRECISION',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBLL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT TENMA VIET NAM' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBLL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBLL','VN',N'CT TENMA VIET NAM',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBML')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT VS TECHNOLOGY' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBML'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBML','VN',N'CT VS TECHNOLOGY',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBNL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT VS INDUSTRY VN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBNL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBNL','VN',N'CT VS INDUSTRY VN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBOL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'EUNSUNG ELECTRONICS' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBOL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBOL','VN',N'EUNSUNG ELECTRONICS',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBPL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT MITAC COMPUTER' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBPL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBPL','VN',N'CT MITAC COMPUTER',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBQL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT MITAC PRECISION' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBQL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBQL','VN',N'CT MITAC PRECISION',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBRL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT AMSTRONG WESTON' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBRL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBRL','VN',N'CT AMSTRONG WESTON',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBSL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'HAYAKAWA ELECTRONICS' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBSL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBSL','VN',N'HAYAKAWA ELECTRONICS',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBTL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'SAMSUNG ELECTRONICS' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBTL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBTL','VN',N'SAMSUNG ELECTRONICS',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBUL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT SHIHEN VIETNAM' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBUL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBUL','VN',N'CT SHIHEN VIETNAM',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBVL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT NHUA KINH QUANG' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBVL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBVL','VN',N'CT NHUA KINH QUANG',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBWL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT MINH TRI' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBWL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBWL','VN',N'CT MINH TRI',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBYL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT KINYOSHA VN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBYL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBYL','VN',N'CT KINYOSHA VN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFBZL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT WINTEK VIET NAM' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFBZL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFBZL','VN',N'CT WINTEK VIET NAM',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'FUHONG PRECISION' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCAL','VN',N'FUHONG PRECISION',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCBL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT DOVAN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCBL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCBL','VN',N'CT DOVAN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCCL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT BAO BI HAO NHUE' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCCL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCCL','VN',N'CT BAO BI HAO NHUE',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCDL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT BIRZ VIET NAM' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCDL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCDL','VN',N'CT BIRZ VIET NAM',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCEL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT IN BAO BI SUNNY' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCEL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCEL','VN',N'CT IN BAO BI SUNNY',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCFL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KORETSUNE SEIKO VN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCFL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCFL','VN',N'KORETSUNE SEIKO VN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCGL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT STRONICS VN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCGL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCGL','VN',N'CT STRONICS VN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCHL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT UMEC VN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCHL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCHL','VN',N'CT UMEC VN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCIL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT ASEAN LINK' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCIL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCIL','VN',N'CT ASEAN LINK',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCJL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT DTU SUNG JIN VN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCJL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCJL','VN',N'CT DTU SUNG JIN VN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCKL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT DAINICHI COLOR' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCKL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCKL','VN',N'CT DAINICHI COLOR',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCLL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'SUMITOMO ELECTRIC' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCLL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCLL','VN',N'SUMITOMO ELECTRIC',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCML')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'HAYAKAWA ELECTRONICS' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCML'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCML','VN',N'HAYAKAWA ELECTRONICS',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCNL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TABUCHI ELECTRONIC' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCNL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCNL','VN',N'TABUCHI ELECTRONIC',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCOL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT TOHO PRECISION' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCOL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCOL','VN',N'CT TOHO PRECISION',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCPL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP SX SONG HONG' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCPL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCPL','VN',N'CTCP SX SONG HONG',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCQL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT JOYO MARK VN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCQL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCQL','VN',N'CT JOYO MARK VN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCRL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KURABE INDUSTRIAL BN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCRL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCRL','VN',N'KURABE INDUSTRIAL BN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCSL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DAIICHI DENSO BUHIN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCSL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCSL','VN',N'DAIICHI DENSO BUHIN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCTL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CONG NGHE MUTO HN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCTL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCTL','VN',N'CONG NGHE MUTO HN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCUL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT CANON VN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCUL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCUL','VN',N'CT CANON VN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCVL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT CANON VN QUE VO' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCVL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCVL','VN',N'CT CANON VN QUE VO',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCWL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT CANON VN TIEN SON' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCWL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCWL','VN',N'CT CANON VN TIEN SON',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCYL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CFS ICD TAN CANG LB' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCYL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCYL','VN',N'CFS ICD TAN CANG LB',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFCZL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT HANG HAI VIMADECO' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFCZL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFCZL','VN',N'CT HANG HAI VIMADECO',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'GN KV NGOAI THUONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAAL','VN',N'GN KV NGOAI THUONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNABL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TM DV XNK HAI PHONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNABL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNABL','VN',N'TM DV XNK HAI PHONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNACL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TM XD 5 HP' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNACL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNACL','VN',N'TM XD 5 HP',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNADL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TNHH THANH HUYEN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNADL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNADL','VN',N'TNHH THANH HUYEN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAEL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TM HOANG CAU' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAEL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAEL','VN',N'TM HOANG CAU',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAFL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'VAT LIEU DIEN HP' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAFL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAFL','VN',N'VAT LIEU DIEN HP',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAGL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CN LH VAN CHUYEN HP' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAGL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAGL','VN',N'CN LH VAN CHUYEN HP',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAHL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'VIJACO' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAHL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAHL','VN',N'VIJACO',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAIL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'XNK QUANG BINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAIL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAIL','VN',N'XNK QUANG BINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAJL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'BINH PHU' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAJL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAJL','VN',N'BINH PHU',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAKL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CAT VAN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAKL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAKL','VN',N'CAT VAN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNALL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'GIAONHAN NT HAIPHONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNALL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNALL','VN',N'GIAONHAN NT HAIPHONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAML')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ LOGISTICS XANH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAML'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAML','VN',N'KNQ LOGISTICS XANH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNANL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'VINALINE LOGISTIC VN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNANL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNANL','VN',N'VINALINE LOGISTIC VN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAOL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTY CP AB PLUS' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAOL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAOL','VN',N'CTY CP AB PLUS',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAPL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'MASCOT VN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAPL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAPL','VN',N'MASCOT VN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAQL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'VAN TAI VIET NHAT HD' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAQL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAQL','VN',N'VAN TAI VIET NHAT HD',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNARL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'SANKYULOGISTICS HD' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNARL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNARL','VN',N'SANKYULOGISTICS HD',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNASL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'YUSEN HA NOI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNASL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNASL','VN',N'YUSEN HA NOI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNATL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'GN KHO VAN HAI DUONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNATL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNATL','VN',N'GN KHO VAN HAI DUONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAVL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TIEP VAN THANG LONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAVL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAVL','VN',N'TIEP VAN THANG LONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAWL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'GEMADEPT SONGTHAN BD' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAWL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAWL','VN',N'GEMADEPT SONGTHAN BD',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAXL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'GEMADEPT AN THANH BD' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAXL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAXL','VN',N'GEMADEPT AN THANH BD',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAYL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TANCANG SONGTHAN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAYL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAYL','VN',N'TANCANG SONGTHAN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNAZL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'SWIRE COLD STORAGE' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNAZL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNAZL','VN',N'SWIRE COLD STORAGE',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'VAN TAI VIET NHAT' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBAL','VN',N'VAN TAI VIET NHAT',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBBL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KERRY I LOGISTICS BD' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBBL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBBL','VN',N'KERRY I LOGISTICS BD',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBCL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'GN VAN TAI UI D5' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBCL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBCL','VN',N'GN VAN TAI UI D5',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBDL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'C STEINWEG BD' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBDL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBDL','VN',N'C STEINWEG BD',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBFL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KHO VAN GIA DINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBFL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBFL','VN',N'KHO VAN GIA DINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBGL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'NIPPON EXPRESS' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBGL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBGL','VN',N'NIPPON EXPRESS',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBHL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'PETEC BD' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBHL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBHL','VN',N'PETEC BD',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBIL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'GN VAN TAI UI N2' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBIL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBIL','VN',N'GN VAN TAI UI N2',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBJL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'LIEN ANH BD' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBJL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBJL','VN',N'LIEN ANH BD',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBKL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DIA OC ARECO BD' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBKL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBKL','VN',N'DIA OC ARECO BD',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBLL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'NIPPON EXP SONG THAN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBLL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBLL','VN',N'NIPPON EXP SONG THAN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBML')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'GN VAN TAI UI N13' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBML'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBML','VN',N'GN VAN TAI UI N13',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBNL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TRANSIMEX BD' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBNL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBNL','VN',N'TRANSIMEX BD',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBOL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TMDV BINH DUONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBOL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBOL','VN',N'TMDV BINH DUONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBPL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'HOTRO TAINANG TRE VN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBPL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBPL','VN',N'HOTRO TAINANG TRE VN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBQL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TMDV XNK HAI PHONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBQL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBQL','VN',N'TMDV XNK HAI PHONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBRL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'QUANG PHAT' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBRL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBRL','VN',N'QUANG PHAT',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBSL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TAU BIEN QUANG NINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBSL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBSL','VN',N'TAU BIEN QUANG NINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBTL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'DTPT QUANG NINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBTL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBTL','VN',N'DTPT QUANG NINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBUL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'HANG HAI QUANG NINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBUL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBUL','VN',N'HANG HAI QUANG NINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBVL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CONG TY CO INDECO' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBVL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBVL','VN',N'CONG TY CO INDECO',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBWL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT CPSXTM HOANG TIEN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBWL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBWL','VN',N'CT CPSXTM HOANG TIEN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBXL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'C.TY TNHH SON TUNG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBXL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBXL','VN',N'C.TY TNHH SON TUNG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBYL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT TNHH TRUONG GIANG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBYL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBYL','VN',N'CT TNHH TRUONG GIANG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNBZL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT TNHH Q.TE SAO BAC' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNBZL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNBZL','VN',N'CT TNHH Q.TE SAO BAC',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'D.LY HH QUANG NINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCAL','VN',N'D.LY HH QUANG NINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCBL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP T.BIEN Q.NINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCBL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCBL','VN',N'CTCP T.BIEN Q.NINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCCL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP DT VA XNK QNINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCCL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCCL','VN',N'CTCP DT VA XNK QNINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCDL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTY CP TAXI MONG CAI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCDL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCDL','VN',N'CTY CP TAXI MONG CAI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCEL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CONG TY TNHH VU HAI ' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCEL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCEL','VN',N'CONG TY TNHH VU HAI ',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCFL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT KIMTHANH PHAT DAT' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCFL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCFL','VN',N'CT KIMTHANH PHAT DAT',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCGL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT MTV CANG SAI GON' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCGL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCGL','VN',N'CT MTV CANG SAI GON',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCHL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT VBDQ SAI GON SJC' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCHL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCHL','VN',N'CT VBDQ SAI GON SJC',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCIL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'LDXDKD KCX TAN THUAN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCIL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCIL','VN',N'LDXDKD KCX TAN THUAN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCJL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT GNKV NT SAI GON' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCJL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCJL','VN',N'CT GNKV NT SAI GON',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCKL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP Y D VIMEDIMEX' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCKL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCKL','VN',N'CTCP Y D VIMEDIMEX',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCLL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTY SX XNK TAY NAM' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCLL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCLL','VN',N'CTY SX XNK TAY NAM',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCML')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT KV M.NAM SOTRANS' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCML'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCML','VN',N'CT KV M.NAM SOTRANS',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCOL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP KTPT KV TAN TAO' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCOL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCOL','VN',N'CTCP KTPT KV TAN TAO',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCPL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTY SAGAWA EXPRESS' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCPL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCPL','VN',N'CTY SAGAWA EXPRESS',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCQL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP VINALINK' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCQL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCQL','VN',N'CTCP VINALINK',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCRL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP TMDV C SAI GON' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCRL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCRL','VN',N'CTCP TMDV C SAI GON',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCSL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTLD BONG SEN(LOTUS)' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCSL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCSL','VN',N'CTLD BONG SEN(LOTUS)',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCTL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CANG SAI GON' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCTL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCTL','VN',N'CANG SAI GON',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCUL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT TMDV XNK DONG TAY' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCUL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCUL','VN',N'CT TMDV XNK DONG TAY',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCVL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TNHH MTV C.SAI GON' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCVL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCVL','VN',N'TNHH MTV C.SAI GON',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCWL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTLD K.VAN BINH MINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCWL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCWL','VN',N'CTLD K.VAN BINH MINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCXL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTTNHH C.BEN NGHE' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCXL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCXL','VN',N'CTTNHH C.BEN NGHE',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCYL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP DVHH S.GON SCSC' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCYL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCYL','VN',N'CTCP DVHH S.GON SCSC',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNCZL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT DVHH TSN (TCS)' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNCZL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNCZL','VN',N'CT DVHH TSN (TCS)',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTLD VT HH VIET NHAT' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDAL','VN',N'CTLD VT HH VIET NHAT',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDBL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT T.VAN THANG LONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDBL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDBL','VN',N'CT T.VAN THANG LONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDCL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT N.LUC TMQT HA NOI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDCL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDCL','VN',N'CT N.LUC TMQT HA NOI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDDL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'NIPPONEXPRESS VN(HN)' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDDL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDDL','VN',N'NIPPONEXPRESS VN(HN)',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDEL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTTNHH Q.TE DUC YEN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDEL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDEL','VN',N'CTTNHH Q.TE DUC YEN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDFL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP TMXNK THANH LOI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDFL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDFL','VN',N'CTCP TMXNK THANH LOI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDGL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTY CP KHO VAN ALS' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDGL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDGL','VN',N'CTY CP KHO VAN ALS',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDHL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT NYK LOGISTICS VN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDHL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDHL','VN',N'CT NYK LOGISTICS VN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDIL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT PT D.THI KINH BAC' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDIL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDIL','VN',N'CT PT D.THI KINH BAC',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDJL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'SAGAWA EXPRESS (HN)' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDJL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDJL','VN',N'SAGAWA EXPRESS (HN)',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDKL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTY CP DAU TU BAC KY' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDKL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDKL','VN',N'CTY CP DAU TU BAC KY',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDLL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTYTNHH ALS BAC NINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDLL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDLL','VN',N'CTYTNHH ALS BAC NINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDML')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT GNVC INDO-TRAN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDML'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDML','VN',N'CT GNVC INDO-TRAN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDNL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'SAGAWA EXPRESS (HN)' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDNL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDNL','VN',N'SAGAWA EXPRESS (HN)',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDOL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTLOGISTIC TIN NGHIA' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDOL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDOL','VN',N'CTLOGISTIC TIN NGHIA',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDPL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTTNHH TV THANG LONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDPL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDPL','VN',N'CTTNHH TV THANG LONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDQL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'ICD T.CANG LONG BINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDQL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDQL','VN',N'ICD T.CANG LONG BINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDRL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CONG TY TNHH VOPAK' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDRL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDRL','VN',N'CONG TY TNHH VOPAK',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDSL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT TNHH CP HOA VIET' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDSL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDSL','VN',N'CT TNHH CP HOA VIET',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDTL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT TNHH XUAN CUONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDTL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDTL','VN',N'CT TNHH XUAN CUONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDUL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP TV THANH LONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDUL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDUL','VN',N'CTCP TV THANH LONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDVL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTTNHH KCTC VINA' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDVL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDVL','VN',N'CTTNHH KCTC VINA',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDWL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTY TNHH VINH CUONG.' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDWL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDWL','VN',N'CTY TNHH VINH CUONG.',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDXL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'XNK NLSAN PB BA RIA' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDXL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDXL','VN',N'XNK NLSAN PB BA RIA',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDYL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP THUONG CANG VT' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDYL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDYL','VN',N'CTCP THUONG CANG VT',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNDZL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CONG TY CP THANH CHI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNDZL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNDZL','VN',N'CONG TY CP THANH CHI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP HOA DAU VAN AN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEAL','VN',N'CTCP HOA DAU VAN AN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEBL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT C DVDK TH PHU MY' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEBL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEBL','VN',N'CT C DVDK TH PHU MY',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNECL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'THORESEN  VINAMA' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNECL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNECL','VN',N'THORESEN  VINAMA',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEDL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT VTAI BIEN V.NAM' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEDL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEDL','VN',N'CT VTAI BIEN V.NAM',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEEL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTLD KXD NQ V.PHONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEEL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEEL','VN',N'CTLD KXD NQ V.PHONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEFL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT L.THUC SONG HAU' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEFL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEFL','VN',N'CT L.THUC SONG HAU',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEGL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT CO QTE NAM SAO' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEGL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEGL','VN',N'CT CO QTE NAM SAO',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEHL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTY TMDV PHAN DUY' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEHL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEHL','VN',N'CTY TMDV PHAN DUY',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEIL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP CNP HANG HAI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEIL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEIL','VN',N'CTCP CNP HANG HAI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEJL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP XNK BINH DINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEJL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEJL','VN',N'CTCP XNK BINH DINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEKL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCPCN OTO T.CONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEKL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEKL','VN',N'CTCPCN OTO T.CONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNELL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CONG TY CP PHUC LOC' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNELL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNELL','VN',N'CONG TY CP PHUC LOC',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNELL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CONG TY CP PHUC LOC' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNELL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNELL','VN',N'CONG TY CP PHUC LOC',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEML')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTTNHH C LS NAM DINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEML'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEML','VN',N'CTTNHH C LS NAM DINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNENL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTTNHH DV HTANG KCN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNENL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNENL','VN',N'CTTNHH DV HTANG KCN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEOL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'VINALINESLOGISTIC VN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEOL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEOL','VN',N'VINALINESLOGISTIC VN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEPL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT C C.LAI TR.HAI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEPL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEPL','VN',N'CT C C.LAI TR.HAI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEQL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CONG TY TNHH MTV CT' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEQL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEQL','VN',N'CONG TY TNHH MTV CT',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNERL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTY CP XNK DAT PHAT' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNERL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNERL','VN',N'CTY CP XNK DAT PHAT',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNESL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KERRY I LOGISTICS HY' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNESL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNESL','VN',N'KERRY I LOGISTICS HY',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNETL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'ANH HUY BD' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNETL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNETL','VN',N'ANH HUY BD',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEUL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CONG TY CP PHUC LONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEUL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEUL','VN',N'CONG TY CP PHUC LONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEVL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'VINA COMMONDITIES' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEVL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEVL','VN',N'VINA COMMONDITIES',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEWL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ BARIA SERECE' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEWL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEWL','VN',N'KNQ BARIA SERECE',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEXL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ PTSC' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEXL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEXL','VN',N'KNQ PTSC',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEYL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ CANG VAN AN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEYL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEYL','VN',N'KNQ CANG VAN AN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNEZL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ CAN THO' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNEZL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNEZL','VN',N'KNQ CAN THO',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP CANG DONG NAI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFAL','VN',N'CTCP CANG DONG NAI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFBL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT VINH CUONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFBL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFBL','VN',N'CT VINH CUONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFCL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT DTU TM DV QTE' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFCL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFCL','VN',N'CT DTU TM DV QTE',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFDL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ CT LEN SAI GON' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFDL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFDL','VN',N'KNQ CT LEN SAI GON',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFEL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT CCI VIET NAM' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFEL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFEL','VN',N'CT CCI VIET NAM',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFFL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT YUSEN LOGISTICS' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFFL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFFL','VN',N'CT YUSEN LOGISTICS',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFGL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KHO NGOAI QUAN HUE' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFGL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFGL','VN',N'KHO NGOAI QUAN HUE',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFHL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CTCP TMDV MONG CAI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFHL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFHL','VN',N'CTCP TMDV MONG CAI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFIL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT TM VA GN QTE DN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFIL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFIL','VN',N'CT TM VA GN QTE DN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFDAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT LOGISTICS CANG DN' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFDAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFDAL','VN',N'CT LOGISTICS CANG DN',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFJL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT LOGISTICS CANG DN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFJL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFJL','VN',N'CT LOGISTICS CANG DN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFKL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'LOGISTIC TINNGHIA NT' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFKL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFKL','VN',N'LOGISTIC TINNGHIA NT',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFLL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ CHI MA BAC KINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFLL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFLL','VN',N'KNQ CHI MA BAC KINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFDBL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CFS MTV TAN CANG SG' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFDBL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFDBL','VN',N'CFS MTV TAN CANG SG',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFML')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ DV HH NOIBAI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFML'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFML','VN',N'KNQ DV HH NOIBAI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFNL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ CT PHUC DONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFNL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFNL','VN',N'KNQ CT PHUC DONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFOL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ CT THANH TRUNG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFOL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFOL','VN',N'KNQ CT THANH TRUNG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFPL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ CT ANPHA AG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFPL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFPL','VN',N'KNQ CT ANPHA AG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFQL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ MOLENBERGNATIE' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFQL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFQL','VN',N'KNQ MOLENBERGNATIE',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFRL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'NIPPON EXP BACNINH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFRL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFRL','VN',N'NIPPON EXP BACNINH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFSL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ CT INDO-TRAN' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFSL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFSL','VN',N'KNQ CT INDO-TRAN',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFUL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ CT TM KHO DEVYT' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFUL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFUL','VN',N'KNQ CT TM KHO DEVYT',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFVL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ CT SANKYU DNAI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFVL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFVL','VN',N'KNQ CT SANKYU DNAI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFXL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TCT TIN NGHIA TNHH' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFXL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFXL','VN',N'TCT TIN NGHIA TNHH',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNFYL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'TNHH CP XD CT GT CG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNFYL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNFYL','VN',N'TNHH CP XD CT GT CG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNGAL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ YUSEN HAI PHONG' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNGAL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNGAL','VN',N'KNQ YUSEN HAI PHONG',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNGBL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'KNQ NIPPON EXP DNAI' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNGBL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNGBL','VN',N'KNQ NIPPON EXP DNAI',N'KNQ')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFDDL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CSF YUSEN HAI PHONG' ,Notes=N'CSF' WHERE TableID='A016A' AND LOCODE='VNFDDL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFDDL','VN',N'CSF YUSEN HAI PHONG',N'CSF')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNFDCL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CFS INTERSERCO MD' ,Notes=N'CFS' WHERE TableID='A016A' AND LOCODE='VNFDCL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNFDCL','VN',N'CFS INTERSERCO MD',N'CFS')
END
IF EXISTS (SELECT * FROM  t_VNACC_Category_CityUNLOCODE WHERE TableID='A016A' AND LOCODE='VNNGCL')
BEGIN
UPDATE t_VNACC_Category_CityUNLOCODE SET CountryCode='VN' ,CityNameOrStateName=N'CT XEDAPDIEN LAMVIET' ,Notes=N'KNQ' WHERE TableID='A016A' AND LOCODE='VNNGCL'
END
ELSE
BEGIN
INSERT INTO t_VNACC_Category_CityUNLOCODE (TableID,LOCODE,CountryCode,CityNameOrStateName,Notes) VALUES ('A016A','VNNGCL','VN',N'CT XEDAPDIEN LAMVIET',N'KNQ')
END



GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '32.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('32.4',GETDATE(), N'Cập nhật địa điểm xếp hàng và dỡ hàng ')
END	