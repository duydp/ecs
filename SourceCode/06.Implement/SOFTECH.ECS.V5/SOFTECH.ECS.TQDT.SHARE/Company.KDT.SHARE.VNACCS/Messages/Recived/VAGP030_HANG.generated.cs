using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAGP030_HANG : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute B01 { get; set; }
public PropertiesAttribute B02 { get; set; }
public PropertiesAttribute B03 { get; set; }
public PropertiesAttribute B04 { get; set; }
public PropertiesAttribute B05 { get; set; }
public PropertiesAttribute B06 { get; set; }
public PropertiesAttribute B07 { get; set; }

public VAGP030_HANG()
        {
B01 = new PropertiesAttribute(768, typeof(string));
B02 = new PropertiesAttribute(12, typeof(string));
B03 = new PropertiesAttribute(2, typeof(string));
B04 = new PropertiesAttribute(8, typeof(int));
B05 = new PropertiesAttribute(3, typeof(string));
B06 = new PropertiesAttribute(10, typeof(int));
B07 = new PropertiesAttribute(3, typeof(string));
TongSoByte = 820;
}

}public partial class EnumGroupID
    {
}

}
