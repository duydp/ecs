namespace Company.Interface.Report.VNACCS
{
    partial class ThongBaoAnDinhThueHangHoaXNK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongBaoAnDinhThueHangHoaXNK));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblKyHieuChungTuBaoLanh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoChungTuBaoLanh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNHBaoLanh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNHBaoLanh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNHTraThay = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNHTraThay = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHieuHanMuc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblKyHieuChungTuHanMuc = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblLoaiBaoLanh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel38 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaBuuChinh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaChiNguoiXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDonViXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViXNK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDKToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDienThoai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoChungTu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenChiCucHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenCoQuanHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTenSacThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTieuMuc = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoThueKhaiBao = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoThueAnDinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoThueChenhLech = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel67 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenKhoBac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTKKhoBac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblLaiSuatPhatChamNop = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayPhatHanhChungTu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel50 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHetHieuLuc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoNgayAnHan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel54 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaTienTe = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoThueKhaiBao = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoThueAnDinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoThueChenhLech = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel14,
            this.xrLabel13,
            this.lblSoToKhai,
            this.xrLabel8,
            this.xrLabel4,
            this.lblKyHieuChungTuBaoLanh,
            this.xrLabel32,
            this.xrLabel35,
            this.lblSoChungTuBaoLanh,
            this.lblTenNHBaoLanh,
            this.xrLabel28,
            this.lblMaNHBaoLanh,
            this.xrLabel30,
            this.lblMaNHTraThay,
            this.lblTenNHTraThay,
            this.lblSoHieuHanMuc,
            this.lblKyHieuChungTuHanMuc,
            this.xrLabel37,
            this.lblLoaiBaoLanh,
            this.xrLabel39,
            this.xrLabel38,
            this.lblMaBuuChinh,
            this.xrLabel16,
            this.lblDiaChiNguoiXNK,
            this.xrLabel18,
            this.lblTenDonViXNK,
            this.lblMaDonViXNK,
            this.xrLabel12,
            this.xrLabel10,
            this.lblNgayDKToKhai,
            this.xrLabel24,
            this.lblMaLoaiHinh,
            this.xrLabel26,
            this.lblSoDienThoai,
            this.xrLabel20});
            this.Detail.HeightF = 223.9583F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(19.79679F, 211.8335F);
            this.xrLabel14.Multiline = true;
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(218.745F, 10.00001F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Tổng số tiền thuế ấn định:";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 211.8334F);
            this.xrLabel13.Multiline = true;
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(19.79665F, 10.00001F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "2.";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhai.LocationFloat = new DevExpress.Utils.PointFloat(238.5417F, 0F);
            this.lblSoToKhai.Multiline = true;
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.SizeF = new System.Drawing.SizeF(116.6633F, 10.00001F);
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoToKhai.Text = "NNNNNNNNN1NE";
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel8.Multiline = true;
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(19.79665F, 10.00001F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "1.";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(19.79664F, 0F);
            this.xrLabel4.Multiline = true;
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(218.745F, 10.00001F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Ấn định thuế đối với tờ khai Hải quan số:";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblKyHieuChungTuBaoLanh
            // 
            this.lblKyHieuChungTuBaoLanh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblKyHieuChungTuBaoLanh.LocationFloat = new DevExpress.Utils.PointFloat(137.5001F, 143.3334F);
            this.lblKyHieuChungTuBaoLanh.Multiline = true;
            this.lblKyHieuChungTuBaoLanh.Name = "lblKyHieuChungTuBaoLanh";
            this.lblKyHieuChungTuBaoLanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblKyHieuChungTuBaoLanh.SizeF = new System.Drawing.SizeF(116.6633F, 10.00001F);
            this.lblKyHieuChungTuBaoLanh.StylePriority.UseFont = false;
            this.lblKyHieuChungTuBaoLanh.StylePriority.UseTextAlignment = false;
            this.lblKyHieuChungTuBaoLanh.Text = "XXXXXXXXXE";
            this.lblKyHieuChungTuBaoLanh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(0F, 143.3334F);
            this.xrLabel32.Multiline = true;
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "Số chứng từ bảo lãnh:";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel35.LocationFloat = new DevExpress.Utils.PointFloat(370.8267F, 143.3334F);
            this.xrLabel35.Multiline = true;
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.SizeF = new System.Drawing.SizeF(96.87503F, 10F);
            this.xrLabel35.StylePriority.UseFont = false;
            this.xrLabel35.StylePriority.UseTextAlignment = false;
            this.xrLabel35.Text = "Loại bảo lãnh:";
            this.xrLabel35.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoChungTuBaoLanh
            // 
            this.lblSoChungTuBaoLanh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoChungTuBaoLanh.LocationFloat = new DevExpress.Utils.PointFloat(254.1634F, 143.3333F);
            this.lblSoChungTuBaoLanh.Multiline = true;
            this.lblSoChungTuBaoLanh.Name = "lblSoChungTuBaoLanh";
            this.lblSoChungTuBaoLanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoChungTuBaoLanh.SizeF = new System.Drawing.SizeF(116.6633F, 10.00001F);
            this.lblSoChungTuBaoLanh.StylePriority.UseFont = false;
            this.lblSoChungTuBaoLanh.StylePriority.UseTextAlignment = false;
            this.lblSoChungTuBaoLanh.Text = "XXXXXXXXXE";
            this.lblSoChungTuBaoLanh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenNHBaoLanh
            // 
            this.lblTenNHBaoLanh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenNHBaoLanh.LocationFloat = new DevExpress.Utils.PointFloat(137.5F, 106.9584F);
            this.lblTenNHBaoLanh.Multiline = true;
            this.lblTenNHBaoLanh.Name = "lblTenNHBaoLanh";
            this.lblTenNHBaoLanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNHBaoLanh.SizeF = new System.Drawing.SizeF(571.12F, 25.62501F);
            this.lblTenNHBaoLanh.StylePriority.UseFont = false;
            this.lblTenNHBaoLanh.StylePriority.UseTextAlignment = false;
            this.lblTenNHBaoLanh.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWWE";
            this.lblTenNHBaoLanh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel28
            // 
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(0F, 106.9584F);
            this.xrLabel28.Multiline = true;
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = "Ngân hàng bảo lãnh:";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaNHBaoLanh
            // 
            this.lblMaNHBaoLanh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaNHBaoLanh.LocationFloat = new DevExpress.Utils.PointFloat(137.5001F, 133.3333F);
            this.lblMaNHBaoLanh.Multiline = true;
            this.lblMaNHBaoLanh.Name = "lblMaNHBaoLanh";
            this.lblMaNHBaoLanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNHBaoLanh.SizeF = new System.Drawing.SizeF(116.6633F, 10.00001F);
            this.lblMaNHBaoLanh.StylePriority.UseFont = false;
            this.lblMaNHBaoLanh.StylePriority.UseTextAlignment = false;
            this.lblMaNHBaoLanh.Text = "XXXXXXXXX1E";
            this.lblMaNHBaoLanh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 133.3333F);
            this.xrLabel30.Multiline = true;
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "Mã ngân hàng:";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaNHTraThay
            // 
            this.lblMaNHTraThay.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaNHTraThay.LocationFloat = new DevExpress.Utils.PointFloat(137.5F, 184.3334F);
            this.lblMaNHTraThay.Multiline = true;
            this.lblMaNHTraThay.Name = "lblMaNHTraThay";
            this.lblMaNHTraThay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNHTraThay.SizeF = new System.Drawing.SizeF(116.6633F, 10.00001F);
            this.lblMaNHTraThay.StylePriority.UseFont = false;
            this.lblMaNHTraThay.StylePriority.UseTextAlignment = false;
            this.lblMaNHTraThay.Text = "XXXXXXXXX1E";
            this.lblMaNHTraThay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenNHTraThay
            // 
            this.lblTenNHTraThay.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenNHTraThay.LocationFloat = new DevExpress.Utils.PointFloat(137.5F, 158.3334F);
            this.lblTenNHTraThay.Multiline = true;
            this.lblTenNHTraThay.Name = "lblTenNHTraThay";
            this.lblTenNHTraThay.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNHTraThay.SizeF = new System.Drawing.SizeF(571.12F, 25.62501F);
            this.lblTenNHTraThay.StylePriority.UseFont = false;
            this.lblTenNHTraThay.StylePriority.UseTextAlignment = false;
            this.lblTenNHTraThay.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWWE";
            this.lblTenNHTraThay.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoHieuHanMuc
            // 
            this.lblSoHieuHanMuc.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoHieuHanMuc.LocationFloat = new DevExpress.Utils.PointFloat(254.1634F, 195.8334F);
            this.lblSoHieuHanMuc.Multiline = true;
            this.lblSoHieuHanMuc.Name = "lblSoHieuHanMuc";
            this.lblSoHieuHanMuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHieuHanMuc.SizeF = new System.Drawing.SizeF(116.6633F, 10.00001F);
            this.lblSoHieuHanMuc.StylePriority.UseFont = false;
            this.lblSoHieuHanMuc.StylePriority.UseTextAlignment = false;
            this.lblSoHieuHanMuc.Text = "XXXXXXXXXE";
            this.lblSoHieuHanMuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblKyHieuChungTuHanMuc
            // 
            this.lblKyHieuChungTuHanMuc.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblKyHieuChungTuHanMuc.LocationFloat = new DevExpress.Utils.PointFloat(137.5F, 195.8334F);
            this.lblKyHieuChungTuHanMuc.Multiline = true;
            this.lblKyHieuChungTuHanMuc.Name = "lblKyHieuChungTuHanMuc";
            this.lblKyHieuChungTuHanMuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblKyHieuChungTuHanMuc.SizeF = new System.Drawing.SizeF(116.6633F, 10.00001F);
            this.lblKyHieuChungTuHanMuc.StylePriority.UseFont = false;
            this.lblKyHieuChungTuHanMuc.StylePriority.UseTextAlignment = false;
            this.lblKyHieuChungTuHanMuc.Text = "XXXXXXXXXE";
            this.lblKyHieuChungTuHanMuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(0F, 158.3334F);
            this.xrLabel37.Multiline = true;
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "Ngân hàng trả thay:";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblLoaiBaoLanh
            // 
            this.lblLoaiBaoLanh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblLoaiBaoLanh.LocationFloat = new DevExpress.Utils.PointFloat(467.7017F, 143.3334F);
            this.lblLoaiBaoLanh.Multiline = true;
            this.lblLoaiBaoLanh.Name = "lblLoaiBaoLanh";
            this.lblLoaiBaoLanh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLoaiBaoLanh.SizeF = new System.Drawing.SizeF(240.9232F, 9.999969F);
            this.lblLoaiBaoLanh.StylePriority.UseFont = false;
            this.lblLoaiBaoLanh.StylePriority.UseTextAlignment = false;
            this.lblLoaiBaoLanh.Text = "WWWWWWWWW1WWWWWWWWWE";
            this.lblLoaiBaoLanh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel39
            // 
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 184.3334F);
            this.xrLabel39.Multiline = true;
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "Mã ngân hàng:";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel38
            // 
            this.xrLabel38.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel38.LocationFloat = new DevExpress.Utils.PointFloat(0F, 195.8334F);
            this.xrLabel38.Multiline = true;
            this.xrLabel38.Name = "xrLabel38";
            this.xrLabel38.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel38.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel38.StylePriority.UseFont = false;
            this.xrLabel38.StylePriority.UseTextAlignment = false;
            this.xrLabel38.Text = "Số chứng từ hạn mức:";
            this.xrLabel38.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaBuuChinh
            // 
            this.lblMaBuuChinh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaBuuChinh.LocationFloat = new DevExpress.Utils.PointFloat(137.5001F, 58.91667F);
            this.lblMaBuuChinh.Multiline = true;
            this.lblMaBuuChinh.Name = "lblMaBuuChinh";
            this.lblMaBuuChinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaBuuChinh.SizeF = new System.Drawing.SizeF(116.6633F, 10.00001F);
            this.lblMaBuuChinh.StylePriority.UseFont = false;
            this.lblMaBuuChinh.StylePriority.UseTextAlignment = false;
            this.lblMaBuuChinh.Text = "XXXXXXE";
            this.lblMaBuuChinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 58.91666F);
            this.xrLabel16.Multiline = true;
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Mã bưu chính:";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDiaChiNguoiXNK
            // 
            this.lblDiaChiNguoiXNK.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDiaChiNguoiXNK.LocationFloat = new DevExpress.Utils.PointFloat(137.5001F, 68.91668F);
            this.lblDiaChiNguoiXNK.Multiline = true;
            this.lblDiaChiNguoiXNK.Name = "lblDiaChiNguoiXNK";
            this.lblDiaChiNguoiXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaChiNguoiXNK.SizeF = new System.Drawing.SizeF(571.1248F, 26.04166F);
            this.lblDiaChiNguoiXNK.StylePriority.UseFont = false;
            this.lblDiaChiNguoiXNK.StylePriority.UseTextAlignment = false;
            this.lblDiaChiNguoiXNK.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiNguoiXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 68.91667F);
            this.xrLabel18.Multiline = true;
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Địa chỉ:";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenDonViXNK
            // 
            this.lblTenDonViXNK.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenDonViXNK.LocationFloat = new DevExpress.Utils.PointFloat(137.5001F, 20.00001F);
            this.lblTenDonViXNK.Multiline = true;
            this.lblTenDonViXNK.Name = "lblTenDonViXNK";
            this.lblTenDonViXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDonViXNK.SizeF = new System.Drawing.SizeF(571.1249F, 26.04167F);
            this.lblTenDonViXNK.StylePriority.UseFont = false;
            this.lblTenDonViXNK.StylePriority.UseTextAlignment = false;
            this.lblTenDonViXNK.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenDonViXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDonViXNK
            // 
            this.lblMaDonViXNK.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViXNK.LocationFloat = new DevExpress.Utils.PointFloat(137.5001F, 48.91667F);
            this.lblMaDonViXNK.Multiline = true;
            this.lblMaDonViXNK.Name = "lblMaDonViXNK";
            this.lblMaDonViXNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViXNK.SizeF = new System.Drawing.SizeF(147.9133F, 10.00001F);
            this.lblMaDonViXNK.StylePriority.UseFont = false;
            this.lblMaDonViXNK.StylePriority.UseTextAlignment = false;
            this.lblMaDonViXNK.Text = "XXXXXXXXX1-XXE";
            this.lblMaDonViXNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 48.91667F);
            this.xrLabel12.Multiline = true;
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Mã số thuế:";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(6.357829E-05F, 20.00001F);
            this.xrLabel10.Multiline = true;
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "Đơn vị xuất/nhập khẩu:";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayDKToKhai
            // 
            this.lblNgayDKToKhai.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblNgayDKToKhai.LocationFloat = new DevExpress.Utils.PointFloat(533.6666F, 0F);
            this.lblNgayDKToKhai.Multiline = true;
            this.lblNgayDKToKhai.Name = "lblNgayDKToKhai";
            this.lblNgayDKToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDKToKhai.SizeF = new System.Drawing.SizeF(89.25171F, 10.00001F);
            this.lblNgayDKToKhai.StylePriority.UseFont = false;
            this.lblNgayDKToKhai.StylePriority.UseTextAlignment = false;
            this.lblNgayDKToKhai.Text = "dd/MM/yyyy";
            this.lblNgayDKToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(399.29F, 0F);
            this.xrLabel24.Multiline = true;
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(134.3766F, 10.00001F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "Ngày đăng ký tờ khai:";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaLoaiHinh.LocationFloat = new DevExpress.Utils.PointFloat(137.5F, 10.00001F);
            this.lblMaLoaiHinh.Multiline = true;
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaLoaiHinh.SizeF = new System.Drawing.SizeF(31.245F, 9.999998F);
            this.lblMaLoaiHinh.StylePriority.UseFont = false;
            this.lblMaLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 10.00001F);
            this.xrLabel26.Multiline = true;
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "Mã loại hình:";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoDienThoai
            // 
            this.lblSoDienThoai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoDienThoai.LocationFloat = new DevExpress.Utils.PointFloat(137.5001F, 96.95834F);
            this.lblSoDienThoai.Multiline = true;
            this.lblSoDienThoai.Name = "lblSoDienThoai";
            this.lblSoDienThoai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDienThoai.SizeF = new System.Drawing.SizeF(217.705F, 10.00001F);
            this.lblSoDienThoai.StylePriority.UseFont = false;
            this.lblSoDienThoai.StylePriority.UseTextAlignment = false;
            this.lblSoDienThoai.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoDienThoai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 96.95834F);
            this.xrLabel20.Multiline = true;
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(132.2917F, 10.00001F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Số điện thoại:";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1,
            this.xrLabel2,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel9,
            this.lblSoChungTu,
            this.xrLabel5,
            this.xrLabel1,
            this.xrLabel3,
            this.lblTenChiCucHQ,
            this.lblTenCoQuanHQ});
            this.TopMargin.HeightF = 174F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(205.5833F, 143.165F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(350F, 2F);
            // 
            // xrLabel2
            // 
            this.xrLabel2.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(146.875F, 124.1033F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(476.0433F, 19.06166F);
            this.xrLabel2.StylePriority.UseBorders = false;
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Về việc ấn định thuế đối với hàng hóa xuất khẩu, nhập khẩu";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(132.2917F, 87.04166F);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(38.54665F, 10F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "/ TBT";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(0F, 87.04166F);
            this.xrLabel6.Multiline = true;
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(31.255F, 10F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Số:";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel9
            // 
            this.xrLabel9.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(146.875F, 105.0417F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(476.0433F, 19.06166F);
            this.xrLabel9.StylePriority.UseBorders = false;
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "THÔNG BÁO";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoChungTu
            // 
            this.lblSoChungTu.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoChungTu.LocationFloat = new DevExpress.Utils.PointFloat(31.255F, 87.04166F);
            this.lblSoChungTu.Multiline = true;
            this.lblSoChungTu.Name = "lblSoChungTu";
            this.lblSoChungTu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoChungTu.SizeF = new System.Drawing.SizeF(101.0367F, 10F);
            this.lblSoChungTu.StylePriority.UseFont = false;
            this.lblSoChungTu.StylePriority.UseTextAlignment = false;
            this.lblSoChungTu.Text = "NNNNNNNNN1NE";
            this.lblSoChungTu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(434.3767F, 38.04166F);
            this.xrLabel5.Multiline = true;
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(304.6634F, 34.48F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UsePadding = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM\r\nĐộc lập - Tự do - Hạnh phúc";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 63.04167F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(96.87669F, 10F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "CHI CỤC HQ";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 38.04167F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(96.87669F, 10F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "CQ HẢI QUAN";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenChiCucHQ
            // 
            this.lblTenChiCucHQ.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblTenChiCucHQ.LocationFloat = new DevExpress.Utils.PointFloat(96.8767F, 63.04166F);
            this.lblTenChiCucHQ.Multiline = true;
            this.lblTenChiCucHQ.Name = "lblTenChiCucHQ";
            this.lblTenChiCucHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenChiCucHQ.SizeF = new System.Drawing.SizeF(336.46F, 24F);
            this.lblTenChiCucHQ.StylePriority.UseFont = false;
            this.lblTenChiCucHQ.StylePriority.UseTextAlignment = false;
            this.lblTenChiCucHQ.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWWE";
            this.lblTenChiCucHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenCoQuanHQ
            // 
            this.lblTenCoQuanHQ.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblTenCoQuanHQ.LocationFloat = new DevExpress.Utils.PointFloat(96.8767F, 38.04166F);
            this.lblTenCoQuanHQ.Multiline = true;
            this.lblTenCoQuanHQ.Name = "lblTenCoQuanHQ";
            this.lblTenCoQuanHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenCoQuanHQ.SizeF = new System.Drawing.SizeF(336.46F, 24F);
            this.lblTenCoQuanHQ.StylePriority.UseFont = false;
            this.lblTenCoQuanHQ.StylePriority.UseTextAlignment = false;
            this.lblTenCoQuanHQ.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWWE";
            this.lblTenCoQuanHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportHeader,
            this.ReportFooter});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail1.HeightF = 11F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(19.79679F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.SizeF = new System.Drawing.SizeF(670.5F, 11F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTenSacThue,
            this.lblChuong,
            this.lblTieuMuc,
            this.lblSoThueKhaiBao,
            this.lblSoThueAnDinh,
            this.lblSoThueChenhLech});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.StylePriority.UseBorders = false;
            this.xrTableRow1.Weight = 1;
            // 
            // lblTenSacThue
            // 
            this.lblTenSacThue.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenSacThue.Name = "lblTenSacThue";
            this.lblTenSacThue.StylePriority.UseFont = false;
            this.lblTenSacThue.Text = "WWWWWWWWE";
            this.lblTenSacThue.Weight = 0.98000015258789053;
            // 
            // lblChuong
            // 
            this.lblChuong.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)));
            this.lblChuong.Name = "lblChuong";
            this.lblChuong.StylePriority.UseBorders = false;
            this.lblChuong.Weight = 0.553329404263554;
            // 
            // lblTieuMuc
            // 
            this.lblTieuMuc.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTieuMuc.Name = "lblTieuMuc";
            this.lblTieuMuc.StylePriority.UseFont = false;
            this.lblTieuMuc.Text = "NNNE";
            this.lblTieuMuc.Weight = 0.57975108677102127;
            // 
            // lblSoThueKhaiBao
            // 
            this.lblSoThueKhaiBao.Name = "lblSoThueKhaiBao";
            this.lblSoThueKhaiBao.Text = "12.345.678.901";
            this.lblSoThueKhaiBao.Weight = 1.0189447853863816;
            // 
            // lblSoThueAnDinh
            // 
            this.lblSoThueAnDinh.Name = "lblSoThueAnDinh";
            this.lblSoThueAnDinh.Text = "12.345.678.901";
            this.lblSoThueAnDinh.Weight = 1.1090349085017515;
            // 
            // lblSoThueChenhLech
            // 
            this.lblSoThueChenhLech.Name = "lblSoThueChenhLech";
            this.lblSoThueChenhLech.Text = "123.456.789.012";
            this.lblSoThueChenhLech.Weight = 1.2926559751368485;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.ReportHeader.HeightF = 33.33333F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(19.79679F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.SizeF = new System.Drawing.SizeF(670.495F, 33.33333F);
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UseTextAlignment = false;
            this.xrTable2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell5,
            this.xrTableCell2,
            this.xrTableCell6,
            this.xrTableCell1,
            this.xrTableCell4});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseBorders = false;
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.StylePriority.UseFont = false;
            this.xrTableCell3.Text = "Sắc thuế";
            this.xrTableCell3.Weight = 0.98000015258789053;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "Chương";
            this.xrTableCell5.Weight = 0.553329404263554;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "Tiểu mục";
            this.xrTableCell2.Weight = 0.57975108677102127;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "Số thuế khai báo";
            this.xrTableCell6.Weight = 1.0189447853863816;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Số thuế ấn định";
            this.xrTableCell1.Weight = 1.1090348008092046;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "Số thuế chênh lệch";
            this.xrTableCell4.Weight = 1.2926550920091038;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel23,
            this.xrLabel22,
            this.xrLabel21,
            this.xrLabel19,
            this.xrLabel67,
            this.xrLabel68,
            this.xrLabel69,
            this.xrLabel66,
            this.xrLabel63,
            this.xrLabel64,
            this.xrLabel65,
            this.xrLabel17,
            this.lblTenKhoBac,
            this.xrLabel57,
            this.lblSoTKKhoBac,
            this.lblLaiSuatPhatChamNop,
            this.xrLabel62,
            this.xrLabel61,
            this.lblNgayPhatHanhChungTu,
            this.xrLabel46,
            this.xrLabel50,
            this.lblNgayHetHieuLuc,
            this.lblSoNgayAnHan,
            this.xrLabel54,
            this.xrLabel53,
            this.xrLabel70,
            this.lblTyGia,
            this.xrLabel49,
            this.lblMaTienTe,
            this.xrTable3,
            this.xrLabel15});
            this.ReportFooter.HeightF = 372.9167F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 199.0417F);
            this.xrLabel23.Multiline = true;
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(195.5833F, 9.999969F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "- Lưu: VT, đơn vị soạn thảo (...b).";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 189.0417F);
            this.xrLabel22.Multiline = true;
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(170.8382F, 9.999985F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "- Cục HQ;";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 179.0417F);
            this.xrLabel21.Multiline = true;
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(170.8382F, 9.999985F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "- Tên người nộp thuế;";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(0F, 169.0417F);
            this.xrLabel19.Multiline = true;
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(96.87669F, 9.999969F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Nơi nhận:";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel67
            // 
            this.xrLabel67.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel67.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 280.2499F);
            this.xrLabel67.Multiline = true;
            this.xrLabel67.Name = "xrLabel67";
            this.xrLabel67.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel67.SizeF = new System.Drawing.SizeF(490.9617F, 9.999969F);
            this.xrLabel67.StylePriority.UseFont = false;
            this.xrLabel67.StylePriority.UseTextAlignment = false;
            this.xrLabel67.Text = "2007-Thu từ túi nilong; 2008-Thu từ thuốc diệt cỏ; 2009-Thu từ các sản phẩm, hàng" +
                " hóa khác";
            this.xrLabel67.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel68
            // 
            this.xrLabel68.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(0F, 260.25F);
            this.xrLabel68.Multiline = true;
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(398.6685F, 9.999969F);
            this.xrLabel68.StylePriority.UseFont = false;
            this.xrLabel68.StylePriority.UseTextAlignment = false;
            this.xrLabel68.Text = "(**): Tiểu mục thu thuế bảo vệ môi trường:";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel69
            // 
            this.xrLabel69.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(0F, 270.25F);
            this.xrLabel69.Multiline = true;
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(398.6685F, 9.999969F);
            this.xrLabel69.StylePriority.UseFont = false;
            this.xrLabel69.StylePriority.UseTextAlignment = false;
            this.xrLabel69.Text = "2005-Thu từ than đá; 2006-Thu từ dung dịch hydro, chloro, fluoro, carbon";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel66
            // 
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 250.25F);
            this.xrLabel66.Multiline = true;
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(398.2584F, 9.999969F);
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.StylePriority.UseTextAlignment = false;
            this.xrLabel66.Text = "1953: Thuế chống phân biệt đối xử đối với hàng hóa nhập khẩu vào Việt Nam";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel63
            // 
            this.xrLabel63.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(0.0001430511F, 220.2501F);
            this.xrLabel63.Multiline = true;
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(376.3834F, 9.999969F);
            this.xrLabel63.StylePriority.UseFont = false;
            this.xrLabel63.StylePriority.UseTextAlignment = false;
            this.xrLabel63.Text = "(*): Thuế bổ sung đối với hàng hóa nhập khẩu vào Việt Nam:";
            this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 230.2501F);
            this.xrLabel64.Multiline = true;
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(376.3834F, 9.999969F);
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "1951: Thuế chống bán phá giá đối với hàng hóa nhập khẩu vào Việt Nam";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel65
            // 
            this.xrLabel65.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(0F, 240.2501F);
            this.xrLabel65.Multiline = true;
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(376.3834F, 9.999969F);
            this.xrLabel65.StylePriority.UseFont = false;
            this.xrLabel65.StylePriority.UseTextAlignment = false;
            this.xrLabel65.Text = "1952: Thuế chống trợ cấp đối với hàng hóa nhập khẩu vào Việt Nam";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 79.9999F);
            this.xrLabel17.Multiline = true;
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(205.5846F, 9.999969F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "hoặc nộp trực tiếp cho cơ quan hải quan";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenKhoBac
            // 
            this.lblTenKhoBac.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenKhoBac.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 69.99992F);
            this.lblTenKhoBac.Multiline = true;
            this.lblTenKhoBac.Name = "lblTenKhoBac";
            this.lblTenKhoBac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenKhoBac.SizeF = new System.Drawing.SizeF(705F, 10F);
            this.lblTenKhoBac.StylePriority.UseFont = false;
            this.lblTenKhoBac.StylePriority.UseTextAlignment = false;
            this.lblTenKhoBac.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWWE";
            this.lblTenKhoBac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel57
            // 
            this.xrLabel57.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(0F, 59.99995F);
            this.xrLabel57.Multiline = true;
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(132.2917F, 9.999977F);
            this.xrLabel57.StylePriority.UseFont = false;
            this.xrLabel57.StylePriority.UseTextAlignment = false;
            this.xrLabel57.Text = "tại Kho bạc Nhà nước";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTKKhoBac
            // 
            this.lblSoTKKhoBac.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTKKhoBac.LocationFloat = new DevExpress.Utils.PointFloat(490.9619F, 49.99994F);
            this.lblSoTKKhoBac.Multiline = true;
            this.lblSoTKKhoBac.Name = "lblSoTKKhoBac";
            this.lblSoTKKhoBac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTKKhoBac.SizeF = new System.Drawing.SizeF(131.8348F, 10.00004F);
            this.lblSoTKKhoBac.StylePriority.UseFont = false;
            this.lblSoTKKhoBac.StylePriority.UseTextAlignment = false;
            this.lblSoTKKhoBac.Text = "XXXXXXXXX1XXXXE";
            this.lblSoTKKhoBac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblLaiSuatPhatChamNop
            // 
            this.lblLaiSuatPhatChamNop.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblLaiSuatPhatChamNop.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 91.99987F);
            this.lblLaiSuatPhatChamNop.Multiline = true;
            this.lblLaiSuatPhatChamNop.Name = "lblLaiSuatPhatChamNop";
            this.lblLaiSuatPhatChamNop.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLaiSuatPhatChamNop.SizeF = new System.Drawing.SizeF(705F, 52F);
            this.lblLaiSuatPhatChamNop.StylePriority.UseFont = false;
            this.lblLaiSuatPhatChamNop.StylePriority.UseTextAlignment = false;
            this.lblLaiSuatPhatChamNop.Text = resources.GetString("lblLaiSuatPhatChamNop.Text");
            this.lblLaiSuatPhatChamNop.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel62
            // 
            this.xrLabel62.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(515.2484F, 178.0417F);
            this.xrLabel62.Multiline = true;
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(177.2006F, 11F);
            this.xrLabel62.StylePriority.UseFont = false;
            this.xrLabel62.StylePriority.UseTextAlignment = false;
            this.xrLabel62.Text = "(Ký, ghi rõ họ tên chức vụ)";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel61
            // 
            this.xrLabel61.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(515.2484F, 166.0417F);
            this.xrLabel61.Multiline = true;
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(177.2006F, 11F);
            this.xrLabel61.StylePriority.UseFont = false;
            this.xrLabel61.StylePriority.UseTextAlignment = false;
            this.xrLabel61.Text = "NGƯỜI LẬP CHỨNG TỪ";
            this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayPhatHanhChungTu
            // 
            this.lblNgayPhatHanhChungTu.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblNgayPhatHanhChungTu.LocationFloat = new DevExpress.Utils.PointFloat(555.5833F, 153.5417F);
            this.lblNgayPhatHanhChungTu.Multiline = true;
            this.lblNgayPhatHanhChungTu.Name = "lblNgayPhatHanhChungTu";
            this.lblNgayPhatHanhChungTu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayPhatHanhChungTu.SizeF = new System.Drawing.SizeF(89.25171F, 10.00001F);
            this.lblNgayPhatHanhChungTu.StylePriority.UseFont = false;
            this.lblNgayPhatHanhChungTu.StylePriority.UseTextAlignment = false;
            this.lblNgayPhatHanhChungTu.Text = "dd/MM/yyyy";
            this.lblNgayPhatHanhChungTu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(515.2484F, 153.5417F);
            this.xrLabel46.Multiline = true;
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(40.33496F, 11F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "Ngày";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel50
            // 
            this.xrLabel50.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel50.LocationFloat = new DevExpress.Utils.PointFloat(19.79679F, 40F);
            this.xrLabel50.Multiline = true;
            this.xrLabel50.Name = "xrLabel50";
            this.xrLabel50.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel50.SizeF = new System.Drawing.SizeF(90.70322F, 9.999973F);
            this.xrLabel50.StylePriority.UseFont = false;
            this.xrLabel50.StylePriority.UseTextAlignment = false;
            this.xrLabel50.Text = "Trong thời hạn ";
            this.xrLabel50.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayHetHieuLuc
            // 
            this.lblNgayHetHieuLuc.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblNgayHetHieuLuc.LocationFloat = new DevExpress.Utils.PointFloat(250F, 40F);
            this.lblNgayHetHieuLuc.Multiline = true;
            this.lblNgayHetHieuLuc.Name = "lblNgayHetHieuLuc";
            this.lblNgayHetHieuLuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHetHieuLuc.SizeF = new System.Drawing.SizeF(89.25171F, 10.00001F);
            this.lblNgayHetHieuLuc.StylePriority.UseFont = false;
            this.lblNgayHetHieuLuc.StylePriority.UseTextAlignment = false;
            this.lblNgayHetHieuLuc.Text = "dd/MM/yyyy";
            this.lblNgayHetHieuLuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoNgayAnHan
            // 
            this.lblSoNgayAnHan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoNgayAnHan.LocationFloat = new DevExpress.Utils.PointFloat(112.5F, 40F);
            this.lblSoNgayAnHan.Multiline = true;
            this.lblSoNgayAnHan.Name = "lblSoNgayAnHan";
            this.lblSoNgayAnHan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoNgayAnHan.SizeF = new System.Drawing.SizeF(32.99832F, 10F);
            this.lblSoNgayAnHan.StylePriority.UseFont = false;
            this.lblSoNgayAnHan.StylePriority.UseTextAlignment = false;
            this.lblSoNgayAnHan.Text = "NNE";
            this.lblSoNgayAnHan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel54
            // 
            this.xrLabel54.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel54.LocationFloat = new DevExpress.Utils.PointFloat(0.0001271566F, 50F);
            this.xrLabel54.Multiline = true;
            this.xrLabel54.Name = "xrLabel54";
            this.xrLabel54.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel54.SizeF = new System.Drawing.SizeF(490.9617F, 9.999973F);
            this.xrLabel54.StylePriority.UseFont = false;
            this.xrLabel54.StylePriority.UseTextAlignment = false;
            this.xrLabel54.Text = "đơn vị/ngân hàng có trách nhiệm nộp đủ số tiền thuế chênh lệch nêu tại mục 2 vào " +
                "tài khoản số";
            this.xrLabel54.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(150F, 40F);
            this.xrLabel53.Multiline = true;
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(98.95512F, 9.999973F);
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.StylePriority.UseTextAlignment = false;
            this.xrLabel53.Text = "ngày kể từ ngày";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel70
            // 
            this.xrLabel70.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(428.2519F, 22.49998F);
            this.xrLabel70.Multiline = true;
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(54.04486F, 11F);
            this.xrLabel70.StylePriority.UseFont = false;
            this.xrLabel70.StylePriority.UseTextAlignment = false;
            this.xrLabel70.Text = "Tiền tệ:";
            this.xrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTyGia
            // 
            this.lblTyGia.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTyGia.LocationFloat = new DevExpress.Utils.PointFloat(596.5048F, 22.49998F);
            this.lblTyGia.Multiline = true;
            this.lblTyGia.Name = "lblTyGia";
            this.lblTyGia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGia.SizeF = new System.Drawing.SizeF(93.79199F, 11F);
            this.lblTyGia.StylePriority.UseFont = false;
            this.lblTyGia.StylePriority.UseTextAlignment = false;
            this.lblTyGia.Text = "123.456.789";
            this.lblTyGia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel49
            // 
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(533.6705F, 22.49998F);
            this.xrLabel49.Multiline = true;
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(62.83441F, 11F);
            this.xrLabel49.StylePriority.UseFont = false;
            this.xrLabel49.StylePriority.UseTextAlignment = false;
            this.xrLabel49.Text = "Tỷ giá:";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaTienTe
            // 
            this.lblMaTienTe.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaTienTe.LocationFloat = new DevExpress.Utils.PointFloat(482.2968F, 22.49998F);
            this.lblMaTienTe.Multiline = true;
            this.lblMaTienTe.Name = "lblMaTienTe";
            this.lblMaTienTe.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaTienTe.SizeF = new System.Drawing.SizeF(46.54F, 11F);
            this.lblMaTienTe.StylePriority.UseFont = false;
            this.lblMaTienTe.StylePriority.UseTextAlignment = false;
            this.lblMaTienTe.Text = "XXE";
            this.lblMaTienTe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(19.79679F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable3.SizeF = new System.Drawing.SizeF(670.5F, 11F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UseTextAlignment = false;
            this.xrTable3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell15,
            this.xrTableCell16,
            this.xrTableCell17,
            this.lblTongSoThueKhaiBao,
            this.lblTongSoThueAnDinh,
            this.lblTongSoThueChenhLech});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseBorders = false;
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Font = new System.Drawing.Font("Times New Roman", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.StylePriority.UseFont = false;
            this.xrTableCell15.StylePriority.UseTextAlignment = false;
            this.xrTableCell15.Text = "Tổng cộng";
            this.xrTableCell15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell15.Weight = 1.0054859064955133;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Weight = 0.56771923575476257;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.59481225148480543;
            // 
            // lblTongSoThueKhaiBao
            // 
            this.lblTongSoThueKhaiBao.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTongSoThueKhaiBao.Name = "lblTongSoThueKhaiBao";
            this.lblTongSoThueKhaiBao.StylePriority.UseFont = false;
            this.lblTongSoThueKhaiBao.Text = "12.345.678.901";
            this.lblTongSoThueKhaiBao.Weight = 1.0454590772755272;
            // 
            // lblTongSoThueAnDinh
            // 
            this.lblTongSoThueAnDinh.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTongSoThueAnDinh.Name = "lblTongSoThueAnDinh";
            this.lblTongSoThueAnDinh.StylePriority.UseFont = false;
            this.lblTongSoThueAnDinh.Text = "12.345.678.901";
            this.lblTongSoThueAnDinh.Weight = 1.1378756423526861;
            // 
            // lblTongSoThueChenhLech
            // 
            this.lblTongSoThueChenhLech.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblTongSoThueChenhLech.Name = "lblTongSoThueChenhLech";
            this.lblTongSoThueChenhLech.StylePriority.UseFont = false;
            this.lblTongSoThueChenhLech.Text = "123.456.789.012";
            this.lblTongSoThueChenhLech.Weight = 1.3262728899431457;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(0F, 39.99994F);
            this.xrLabel15.Multiline = true;
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(19.79665F, 10.00001F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "3.";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ThongBaoAnDinhThueHangHoaXNK
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(66, 44, 174, 100);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lblSoChungTu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblTenChiCucHQ;
        private DevExpress.XtraReports.UI.XRLabel lblTenCoQuanHQ;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblKyHieuChungTuBaoLanh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRLabel lblSoChungTuBaoLanh;
        private DevExpress.XtraReports.UI.XRLabel lblTenNHBaoLanh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel lblMaNHBaoLanh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel lblMaNHTraThay;
        private DevExpress.XtraReports.UI.XRLabel lblTenNHTraThay;
        private DevExpress.XtraReports.UI.XRLabel lblSoHieuHanMuc;
        private DevExpress.XtraReports.UI.XRLabel lblKyHieuChungTuHanMuc;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel lblLoaiBaoLanh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel xrLabel38;
        private DevExpress.XtraReports.UI.XRLabel lblMaBuuChinh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblDiaChiNguoiXNK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel lblTenDonViXNK;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViXNK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDKToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel lblSoDienThoai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThue;
        private DevExpress.XtraReports.UI.XRTableCell lblChuong;
        private DevExpress.XtraReports.UI.XRTableCell lblTieuMuc;
        private DevExpress.XtraReports.UI.XRTableCell lblSoThueKhaiBao;
        private DevExpress.XtraReports.UI.XRTableCell lblSoThueAnDinh;
        private DevExpress.XtraReports.UI.XRTableCell lblSoThueChenhLech;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel70;
        private DevExpress.XtraReports.UI.XRLabel lblTyGia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel lblMaTienTe;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoThueKhaiBao;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoThueAnDinh;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoThueChenhLech;
        private DevExpress.XtraReports.UI.XRLabel lblTenKhoBac;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel lblSoTKKhoBac;
        private DevExpress.XtraReports.UI.XRLabel lblLaiSuatPhatChamNop;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel lblNgayPhatHanhChungTu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel50;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHetHieuLuc;
        private DevExpress.XtraReports.UI.XRLabel lblSoNgayAnHan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel54;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel67;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
    }
}
