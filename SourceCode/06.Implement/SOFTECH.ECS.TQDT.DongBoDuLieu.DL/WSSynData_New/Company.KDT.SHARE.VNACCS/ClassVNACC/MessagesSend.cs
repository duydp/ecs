﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.VNACCS.ClassVNACC
{
    public class MessagesSend
    {
        public VNACCHeader Header { get; set; }
        public StringBuilder Body { get; set; }

        public static MessagesSend Load<T>(object obj, bool containHeader, string MaNghiepVuSua, string InputMessID)
        {
            MessagesSend msg = new MessagesSend() 
            {
                Body = HelperVNACCS.GetStrBody<T>(obj,MaNghiepVuSua)
            };
            if (containHeader)
                msg.Header = new VNACCHeader(string.IsNullOrEmpty(MaNghiepVuSua) ? HelperVNACCS.GetMaNghiepVu<T>() : MaNghiepVuSua, UTF8Encoding.UTF8.GetBytes(msg.Body.ToString()).Length, InputMessID);
            return msg;
        }
        public static MessagesSend Load<T>(object obj, string InputMSGID)
        {
            return Load<T>(obj, true, string.Empty, InputMSGID);
        }
        public static MessagesSend Load(Rep1 rep)
        {

            MessagesSend msg = new MessagesSend()
            {
                Body = HelperVNACCS.GetStrBody<Rep1>(rep, string.Empty),
            };
            msg.Header = new VNACCHeader("#REP1", UTF8Encoding.UTF8.GetBytes(msg.Body.ToString()).Length, string.Empty);
            return msg;
        }

        public static MessagesSend LoadHYS(HYS hys, string inputMsgID)
        {
            MessagesSend msgsend = new MessagesSend();
            msgsend.Header = new VNACCHeader("HYS", 1059, inputMsgID);
            msgsend.Body = hys.BuildEdiMessages(new StringBuilder(), GlobalVNACC.PathConfig);

            return msgsend;
        }
        public static MessagesSend LoadMSB(MSB msb, string inputMsgID)
        {
            MessagesSend msgsend = new MessagesSend();
            msgsend.Header = new VNACCHeader("MSB", 1236, inputMsgID);
            msgsend.Body = msb.BuildEdiMessages(new StringBuilder(), GlobalVNACC.PathConfig);

            return msgsend;
        }
    }
}
