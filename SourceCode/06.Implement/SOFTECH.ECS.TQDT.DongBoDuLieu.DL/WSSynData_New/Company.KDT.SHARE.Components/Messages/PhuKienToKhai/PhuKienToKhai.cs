﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    //Phụ kiện hợp đồng
    [XmlRoot("Declaration")]
    public class PhuKienToKhai : IssueBase
    {
        /// <summary>
        /// Số tờ khai 
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }
        /// <summary>
        /// Ngày đăng ký YYYY-MM-DD HH:mm:ss
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }
        /// <summary>
        /// Mã loại hình 
        /// </summary>
        [XmlElement("natureOfTransaction")]
        public string NatureOfTransaction { get; set; }
        /// <summary>
        /// Trạng thái
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }
        /// <summary>
        /// Đại lý
        /// </summary>
        [XmlElement("Agent")]
        public List<Agent> Agents { get; set; }
        /// <summary>
        /// Doanh nghiệp nhập khẩu
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { get; set; }
        //[XmlElement("ContractReference")]
        //public ContractDocument ContractReference { get; set; }
        [XmlElement("SubDeclaration")]
        public Subcontract SubContract { get; set; }
        [XmlElement("AdditionalInformation")]
        public List<PhuKienToKhai_AdditionalInformation> AdditionalInformations { get; set; }


    }

      public class PhuKienToKhai_AdditionalInformation
    {
        /// <summary>
        /// Mã Phụ kiện khai báo
        /// </summary>
        [XmlElement("statement")]
        public string Statement { get; set; }
          /// <summary>
        /// Nôi dung phụ kiện
          /// </summary>
        [XmlElement("Content")]
        public PhuKienToKhai_Content Content { get; set; }
    }
      public class PhuKienToKhai_Content
    {
        /// <summary>
        /// Phụ kiện tờ khai
        /// </summary>
        [XmlElement("Declaration")]
        public DeclarationPhuKienToKhai Declaration { get; set; }
    }
}
