-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VanDon_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VanDon_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VanDon_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VanDon_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VanDon_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VanDon_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VanDon_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VanDon_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VanDon_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VanDon_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VanDon_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VanDon_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VanDon_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VanDon_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VanDon_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VanDon_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VanDon_SelectBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VanDon_SelectBy_TKMD_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VanDon_DeleteBy_TKMD_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VanDon_DeleteBy_TKMD_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_Insert]
	@SoVanDon varchar(50),
	@NgayVanDon datetime,
	@HangRoi bit,
	@SoHieuPTVT varchar(50),
	@NgayDenPTVT datetime,
	@MaHangVT varchar(50),
	@TenHangVT nvarchar(100),
	@TenPTVT nvarchar(100),
	@QuocTichPTVT varchar(3),
	@NuocXuat_ID varchar(3),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang nvarchar(100),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang nvarchar(100),
	@CuaKhauNhap_ID varchar(4),
	@CuaKhauXuat nvarchar(100),
	@MaNguoiNhanHangTrungGian varchar(50),
	@TenNguoiNhanHangTrungGian nvarchar(100),
	@MaCangXepHang varchar(50),
	@TenCangXepHang nvarchar(100),
	@MaCangDoHang varchar(50),
	@TenCangDoHang nvarchar(100),
	@TKMD_ID bigint,
	@DKGH_ID varchar(7),
	@DiaDiemGiaoHang nvarchar(100),
	@NoiDi nvarchar(500),
	@SoHieuChuyenDi nvarchar(500),
	@NgayKhoiHanh datetime,
	@TongSoKien int,
	@LoaiKien nvarchar(5),
	@SoHieuKien nvarchar(35),
	@DiaDiemChuyenTai nvarchar(2048),
	@ID bigint OUTPUT
AS
INSERT INTO [dbo].[t_KDT_VanDon]
(
	[SoVanDon],
	[NgayVanDon],
	[HangRoi],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[MaHangVT],
	[TenHangVT],
	[TenPTVT],
	[QuocTichPTVT],
	[NuocXuat_ID],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[CuaKhauNhap_ID],
	[CuaKhauXuat],
	[MaNguoiNhanHangTrungGian],
	[TenNguoiNhanHangTrungGian],
	[MaCangXepHang],
	[TenCangXepHang],
	[MaCangDoHang],
	[TenCangDoHang],
	[TKMD_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[NoiDi],
	[SoHieuChuyenDi],
	[NgayKhoiHanh],
	[TongSoKien],
	[LoaiKien],
	[SoHieuKien],
	[DiaDiemChuyenTai]
)
VALUES 
(
	@SoVanDon,
	@NgayVanDon,
	@HangRoi,
	@SoHieuPTVT,
	@NgayDenPTVT,
	@MaHangVT,
	@TenHangVT,
	@TenPTVT,
	@QuocTichPTVT,
	@NuocXuat_ID,
	@MaNguoiNhanHang,
	@TenNguoiNhanHang,
	@MaNguoiGiaoHang,
	@TenNguoiGiaoHang,
	@CuaKhauNhap_ID,
	@CuaKhauXuat,
	@MaNguoiNhanHangTrungGian,
	@TenNguoiNhanHangTrungGian,
	@MaCangXepHang,
	@TenCangXepHang,
	@MaCangDoHang,
	@TenCangDoHang,
	@TKMD_ID,
	@DKGH_ID,
	@DiaDiemGiaoHang,
	@NoiDi,
	@SoHieuChuyenDi,
	@NgayKhoiHanh,
	@TongSoKien,
	@LoaiKien,
	@SoHieuKien,
	@DiaDiemChuyenTai
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_Update]
	@ID bigint,
	@SoVanDon varchar(50),
	@NgayVanDon datetime,
	@HangRoi bit,
	@SoHieuPTVT varchar(50),
	@NgayDenPTVT datetime,
	@MaHangVT varchar(50),
	@TenHangVT nvarchar(100),
	@TenPTVT nvarchar(100),
	@QuocTichPTVT varchar(3),
	@NuocXuat_ID varchar(3),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang nvarchar(100),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang nvarchar(100),
	@CuaKhauNhap_ID varchar(4),
	@CuaKhauXuat nvarchar(100),
	@MaNguoiNhanHangTrungGian varchar(50),
	@TenNguoiNhanHangTrungGian nvarchar(100),
	@MaCangXepHang varchar(50),
	@TenCangXepHang nvarchar(100),
	@MaCangDoHang varchar(50),
	@TenCangDoHang nvarchar(100),
	@TKMD_ID bigint,
	@DKGH_ID varchar(7),
	@DiaDiemGiaoHang nvarchar(100),
	@NoiDi nvarchar(500),
	@SoHieuChuyenDi nvarchar(500),
	@NgayKhoiHanh datetime,
	@TongSoKien int,
	@LoaiKien nvarchar(5),
	@SoHieuKien nvarchar(35),
	@DiaDiemChuyenTai nvarchar(2048)
AS

UPDATE
	[dbo].[t_KDT_VanDon]
SET
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[HangRoi] = @HangRoi,
	[SoHieuPTVT] = @SoHieuPTVT,
	[NgayDenPTVT] = @NgayDenPTVT,
	[MaHangVT] = @MaHangVT,
	[TenHangVT] = @TenHangVT,
	[TenPTVT] = @TenPTVT,
	[QuocTichPTVT] = @QuocTichPTVT,
	[NuocXuat_ID] = @NuocXuat_ID,
	[MaNguoiNhanHang] = @MaNguoiNhanHang,
	[TenNguoiNhanHang] = @TenNguoiNhanHang,
	[MaNguoiGiaoHang] = @MaNguoiGiaoHang,
	[TenNguoiGiaoHang] = @TenNguoiGiaoHang,
	[CuaKhauNhap_ID] = @CuaKhauNhap_ID,
	[CuaKhauXuat] = @CuaKhauXuat,
	[MaNguoiNhanHangTrungGian] = @MaNguoiNhanHangTrungGian,
	[TenNguoiNhanHangTrungGian] = @TenNguoiNhanHangTrungGian,
	[MaCangXepHang] = @MaCangXepHang,
	[TenCangXepHang] = @TenCangXepHang,
	[MaCangDoHang] = @MaCangDoHang,
	[TenCangDoHang] = @TenCangDoHang,
	[TKMD_ID] = @TKMD_ID,
	[DKGH_ID] = @DKGH_ID,
	[DiaDiemGiaoHang] = @DiaDiemGiaoHang,
	[NoiDi] = @NoiDi,
	[SoHieuChuyenDi] = @SoHieuChuyenDi,
	[NgayKhoiHanh] = @NgayKhoiHanh,
	[TongSoKien] = @TongSoKien,
	[LoaiKien] = @LoaiKien,
	[SoHieuKien] = @SoHieuKien,
	[DiaDiemChuyenTai] = @DiaDiemChuyenTai
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_InsertUpdate]
	@ID bigint,
	@SoVanDon varchar(50),
	@NgayVanDon datetime,
	@HangRoi bit,
	@SoHieuPTVT varchar(50),
	@NgayDenPTVT datetime,
	@MaHangVT varchar(50),
	@TenHangVT nvarchar(100),
	@TenPTVT nvarchar(100),
	@QuocTichPTVT varchar(3),
	@NuocXuat_ID varchar(3),
	@MaNguoiNhanHang varchar(50),
	@TenNguoiNhanHang nvarchar(100),
	@MaNguoiGiaoHang varchar(50),
	@TenNguoiGiaoHang nvarchar(100),
	@CuaKhauNhap_ID varchar(4),
	@CuaKhauXuat nvarchar(100),
	@MaNguoiNhanHangTrungGian varchar(50),
	@TenNguoiNhanHangTrungGian nvarchar(100),
	@MaCangXepHang varchar(50),
	@TenCangXepHang nvarchar(100),
	@MaCangDoHang varchar(50),
	@TenCangDoHang nvarchar(100),
	@TKMD_ID bigint,
	@DKGH_ID varchar(7),
	@DiaDiemGiaoHang nvarchar(100),
	@NoiDi nvarchar(500),
	@SoHieuChuyenDi nvarchar(500),
	@NgayKhoiHanh datetime,
	@TongSoKien int,
	@LoaiKien nvarchar(5),
	@SoHieuKien nvarchar(35),
	@DiaDiemChuyenTai nvarchar(2048)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VanDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VanDon] 
		SET
			[SoVanDon] = @SoVanDon,
			[NgayVanDon] = @NgayVanDon,
			[HangRoi] = @HangRoi,
			[SoHieuPTVT] = @SoHieuPTVT,
			[NgayDenPTVT] = @NgayDenPTVT,
			[MaHangVT] = @MaHangVT,
			[TenHangVT] = @TenHangVT,
			[TenPTVT] = @TenPTVT,
			[QuocTichPTVT] = @QuocTichPTVT,
			[NuocXuat_ID] = @NuocXuat_ID,
			[MaNguoiNhanHang] = @MaNguoiNhanHang,
			[TenNguoiNhanHang] = @TenNguoiNhanHang,
			[MaNguoiGiaoHang] = @MaNguoiGiaoHang,
			[TenNguoiGiaoHang] = @TenNguoiGiaoHang,
			[CuaKhauNhap_ID] = @CuaKhauNhap_ID,
			[CuaKhauXuat] = @CuaKhauXuat,
			[MaNguoiNhanHangTrungGian] = @MaNguoiNhanHangTrungGian,
			[TenNguoiNhanHangTrungGian] = @TenNguoiNhanHangTrungGian,
			[MaCangXepHang] = @MaCangXepHang,
			[TenCangXepHang] = @TenCangXepHang,
			[MaCangDoHang] = @MaCangDoHang,
			[TenCangDoHang] = @TenCangDoHang,
			[TKMD_ID] = @TKMD_ID,
			[DKGH_ID] = @DKGH_ID,
			[DiaDiemGiaoHang] = @DiaDiemGiaoHang,
			[NoiDi] = @NoiDi,
			[SoHieuChuyenDi] = @SoHieuChuyenDi,
			[NgayKhoiHanh] = @NgayKhoiHanh,
			[TongSoKien] = @TongSoKien,
			[LoaiKien] = @LoaiKien,
			[SoHieuKien] = @SoHieuKien,
			[DiaDiemChuyenTai] = @DiaDiemChuyenTai
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VanDon]
		(
			[SoVanDon],
			[NgayVanDon],
			[HangRoi],
			[SoHieuPTVT],
			[NgayDenPTVT],
			[MaHangVT],
			[TenHangVT],
			[TenPTVT],
			[QuocTichPTVT],
			[NuocXuat_ID],
			[MaNguoiNhanHang],
			[TenNguoiNhanHang],
			[MaNguoiGiaoHang],
			[TenNguoiGiaoHang],
			[CuaKhauNhap_ID],
			[CuaKhauXuat],
			[MaNguoiNhanHangTrungGian],
			[TenNguoiNhanHangTrungGian],
			[MaCangXepHang],
			[TenCangXepHang],
			[MaCangDoHang],
			[TenCangDoHang],
			[TKMD_ID],
			[DKGH_ID],
			[DiaDiemGiaoHang],
			[NoiDi],
			[SoHieuChuyenDi],
			[NgayKhoiHanh],
			[TongSoKien],
			[LoaiKien],
			[SoHieuKien],
			[DiaDiemChuyenTai]
		)
		VALUES 
		(
			@SoVanDon,
			@NgayVanDon,
			@HangRoi,
			@SoHieuPTVT,
			@NgayDenPTVT,
			@MaHangVT,
			@TenHangVT,
			@TenPTVT,
			@QuocTichPTVT,
			@NuocXuat_ID,
			@MaNguoiNhanHang,
			@TenNguoiNhanHang,
			@MaNguoiGiaoHang,
			@TenNguoiGiaoHang,
			@CuaKhauNhap_ID,
			@CuaKhauXuat,
			@MaNguoiNhanHangTrungGian,
			@TenNguoiNhanHangTrungGian,
			@MaCangXepHang,
			@TenCangXepHang,
			@MaCangDoHang,
			@TenCangDoHang,
			@TKMD_ID,
			@DKGH_ID,
			@DiaDiemGiaoHang,
			@NoiDi,
			@SoHieuChuyenDi,
			@NgayKhoiHanh,
			@TongSoKien,
			@LoaiKien,
			@SoHieuKien,
			@DiaDiemChuyenTai
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VanDon]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_DeleteBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_DeleteBy_TKMD_ID]
	@TKMD_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VanDon]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VanDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoVanDon],
	[NgayVanDon],
	[HangRoi],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[MaHangVT],
	[TenHangVT],
	[TenPTVT],
	[QuocTichPTVT],
	[NuocXuat_ID],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[CuaKhauNhap_ID],
	[CuaKhauXuat],
	[MaNguoiNhanHangTrungGian],
	[TenNguoiNhanHangTrungGian],
	[MaCangXepHang],
	[TenCangXepHang],
	[MaCangDoHang],
	[TenCangDoHang],
	[TKMD_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[NoiDi],
	[SoHieuChuyenDi],
	[NgayKhoiHanh],
	[TongSoKien],
	[LoaiKien],
	[SoHieuKien],
	[DiaDiemChuyenTai]
FROM
	[dbo].[t_KDT_VanDon]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_SelectBy_TKMD_ID]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_SelectBy_TKMD_ID]
	@TKMD_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoVanDon],
	[NgayVanDon],
	[HangRoi],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[MaHangVT],
	[TenHangVT],
	[TenPTVT],
	[QuocTichPTVT],
	[NuocXuat_ID],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[CuaKhauNhap_ID],
	[CuaKhauXuat],
	[MaNguoiNhanHangTrungGian],
	[TenNguoiNhanHangTrungGian],
	[MaCangXepHang],
	[TenCangXepHang],
	[MaCangDoHang],
	[TenCangDoHang],
	[TKMD_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[NoiDi],
	[SoHieuChuyenDi],
	[NgayKhoiHanh],
	[TongSoKien],
	[LoaiKien],
	[SoHieuKien],
	[DiaDiemChuyenTai]
FROM
	[dbo].[t_KDT_VanDon]
WHERE
	[TKMD_ID] = @TKMD_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoVanDon],
	[NgayVanDon],
	[HangRoi],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[MaHangVT],
	[TenHangVT],
	[TenPTVT],
	[QuocTichPTVT],
	[NuocXuat_ID],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[CuaKhauNhap_ID],
	[CuaKhauXuat],
	[MaNguoiNhanHangTrungGian],
	[TenNguoiNhanHangTrungGian],
	[MaCangXepHang],
	[TenCangXepHang],
	[MaCangDoHang],
	[TenCangDoHang],
	[TKMD_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[NoiDi],
	[SoHieuChuyenDi],
	[NgayKhoiHanh],
	[TongSoKien],
	[LoaiKien],
	[SoHieuKien],
	[DiaDiemChuyenTai]
FROM [dbo].[t_KDT_VanDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VanDon_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, November 30, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VanDon_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoVanDon],
	[NgayVanDon],
	[HangRoi],
	[SoHieuPTVT],
	[NgayDenPTVT],
	[MaHangVT],
	[TenHangVT],
	[TenPTVT],
	[QuocTichPTVT],
	[NuocXuat_ID],
	[MaNguoiNhanHang],
	[TenNguoiNhanHang],
	[MaNguoiGiaoHang],
	[TenNguoiGiaoHang],
	[CuaKhauNhap_ID],
	[CuaKhauXuat],
	[MaNguoiNhanHangTrungGian],
	[TenNguoiNhanHangTrungGian],
	[MaCangXepHang],
	[TenCangXepHang],
	[MaCangDoHang],
	[TenCangDoHang],
	[TKMD_ID],
	[DKGH_ID],
	[DiaDiemGiaoHang],
	[NoiDi],
	[SoHieuChuyenDi],
	[NgayKhoiHanh],
	[TongSoKien],
	[LoaiKien],
	[SoHieuKien],
	[DiaDiemChuyenTai]
FROM
	[dbo].[t_KDT_VanDon]	

GO

