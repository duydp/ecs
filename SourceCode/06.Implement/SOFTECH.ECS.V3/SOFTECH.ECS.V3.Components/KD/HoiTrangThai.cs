﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;

namespace SOFTECH.ECS.V3.Components.KD
{//AiNPV
    public partial class HoiTrangThai
    {
        public static string ConfigHoiTrangThai( ToKhaiMauDich tkmd, int issuer, int function)
        {
            string doc = "";
            string path = "../../../SOFTECH.ECS.V3.Components/XML/KD/HoiTrangThai.xml";
            XDocument docXML = XDocument.Load(path);
            if (docXML != null)
            {
                XElement Root = docXML.Root;
                if (Root != null && tkmd != null)
                {
                    Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, 0, 0);
                }
                else
                {
                    return doc; //return "":
                }
            }
           // docXML.Save(@"D:\temp.xml");
            doc = docXML.ToString();
            return doc;
        }
    }
}
