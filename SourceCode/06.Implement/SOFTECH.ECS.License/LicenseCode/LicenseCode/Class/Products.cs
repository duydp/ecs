﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace LicenseCode
{
    [XmlRoot("Products")]
    public class Products
    {
        /// <summary>
        /// Mã sản phẩm
        /// </summary>
        [XmlElement("id")]
        public string Id { get; set; }
        /// <summary>
        /// Tiền tố
        /// </summary>
        [XmlElement("prefix")]
        public List<string> Prefix { get; set; }
        /// <summary>
        /// Loại trail
        /// </summary>
        [XmlElement("Trial")]
        public TypeKey Trial { get; set; }
        /// <summary>
        /// Loại license
        /// </summary>
        [XmlElement("License")]
        public TypeKey License { get; set; }
    }
}
