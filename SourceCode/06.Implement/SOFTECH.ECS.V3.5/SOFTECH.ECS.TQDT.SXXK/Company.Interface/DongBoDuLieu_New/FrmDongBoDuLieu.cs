﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using Company.BLL.KDT.SXXK;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components;
using Janus.Windows.GridEX;
using System.Xml;

namespace Company.Interface.DongBoDuLieu_New
{
    public partial class FrmDongBoDuLieu : BaseForm
    {
        public ISyncData_SXXK myService =  IsyncDaTa_V3.SyncService();
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        string maHaiQuan = "";
        string maDoanhNghiep = "";
        string passwordLogin = "", userLogin = "";
        private static KetQuaXuLyForm _frmKetQuaXuLy = new KetQuaXuLyForm();
        private static int _trangThai = 0;
        public static string formType = FormType.CLIENT;
        private int countError = 0;
        delegate void SetProgessBarCallback(int percent);
        private void setProgessBar(int percent)
        {
            if (uiStatusBar.InvokeRequired)
            {
                SetProgessBarCallback d = new SetProgessBarCallback(setProgessBar);
                this.Invoke(d, new object[] { percent });
            }
            else
            {
                uiStatusBar.Panels["barProgress"].ProgressBarValue = percent;

                uiStatusBar.Panels["barProgress"].Text = percent + " %";
            }
        }
        delegate void SetStatusBarCallBack(string status);
        private void SetStatus(string status)
        {
            if (lbError.InvokeRequired)
            {
                SetStatusBarCallBack s = new SetStatusBarCallBack(SetStatus);
                this.Invoke(s, new object[] { status });
            }
            else
            {
                lbError.Text = status;
            }
        }



        public FrmDongBoDuLieu()
        {
            InitializeComponent();

            bw.WorkerReportsProgress = true;
            bw.WorkerSupportsCancellation = true;
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
            bw.ProgressChanged += new ProgressChangedEventHandler(bw_ProgressChanged);

            try
            {
                this.Cursor = Cursors.WaitCursor;
                maHaiQuan = GlobalSettings.MA_HAI_QUAN;
                maDoanhNghiep = GlobalSettings.MA_DON_VI;
                formType = GlobalSettings.IsDaiLy ? FormType.CLIENT : FormType.SERVER;

                if (formType == FormType.CLIENT)
                {
                    // Đại lý
                    tabDaiLy.TabVisible = false;
                }
                else
                {
                    // DN Mabuchi = server
                    tabDaiLy.TabVisible = true;

                  
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        delegate void SetErrorCallback(string msg);
        private void SetError(string msg)
        {
            timer1.Enabled = true;

            if (lbError.InvokeRequired)
            {
                SetErrorCallback d = new SetErrorCallback(SetError);
                this.Invoke(d, new object[] { msg });
            }
            else
            {
                lbError.Visible = true;
                lbError.Text = msg;
            }
        }

        private bool Login()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                WSForm_DBDL wsForm = new WSForm_DBDL();
                if (!Config.IsRemember)
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return false;
                }
                passwordLogin = Config.IsRemember ? Config.Pass : wsForm.txtMatKhau.Text.Trim();
                userLogin = Config.IsRemember ? Config.User : wsForm.txtMaDoanhNghiep.Text.Trim();
                string temp = myService.LoginDaiLy(userLogin, passwordLogin);
                // Đăng nhập sai user
                if (string.IsNullOrEmpty(temp))
                {
                    MLMessages("Người dùng không hợp lệ!", "", "", false);
                    Config.Pass = "";
                    Config.IsRemember = false;
                    return false;
                }
               Company.KDT.SHARE.Components.DaiLy userDaiLy = Helpers.Deserialize<DaiLy>((myService.LoginDaiLy(userLogin, passwordLogin)));
               // 1. Đăng nhập user của doanh nghiệp khác              ||
               // 2. Sử dụng đại lý nhưng đăng nhập User của Admin     ||    
               // 3. Sử dụng server nhưng đăng nhập user của Đại lý    ||
               if ((userDaiLy == null || userDaiLy.MaDoanhNghiep != GlobalSettings.MA_DON_VI) || (formType == FormType.CLIENT && userDaiLy.isAdmin) || (formType == FormType.SERVER && !userDaiLy.isAdmin))
               {
                   MLMessages("Người dùng không hợp lệ!", "", "", false);
                   Config.Pass = "";
                   Config.IsRemember = false;
                   return false;
               }
                    
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SetError(ex.Message);
                return false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnSynData_Click(object sender, EventArgs e)
        {
            try
            {
                setProgessBar(0);
                uiStatusBar.Panels["barTime"].Text = "00 : 00 : 00";

                if (!timePanel1.IsValidate)
                    return;

                if (btnSynData.Text == "Hủy")
                {
                    bw.CancelAsync();
                    timer.Stop();
                }
                else if (btnSynData.Text == "Thực hiện")
                {
                    if (!Login())
                        return;

                    if (bw.IsBusy != true)
                    {
                        timer.Enabled = true;

                        btnSynData.Text = "Hủy";
                        btnSynData.Enabled = true;
                        btnClose.Enabled = false;

                        bw.RunWorkerAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SetError(ex.Message);
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            _seconds += 1;

            uiStatusBar.Panels["barTime"].Text = GetTimeFrom();
            uiStatusBar.Refresh();
        }

        private int _seconds = 0;
        private int _minutes = 0;
        private int _hours = 0;

        private string GetTimeFrom()
        {
            _minutes += _seconds / 60;
            _hours += _minutes / 60;

            if (_seconds == 60)
                _seconds = 0;

            if (_minutes == 60)
                _minutes = 0;

            return (_hours.ToString().Length < 2 ? "0" + _hours : _hours.ToString())
                + " : " +
                (_minutes.ToString().Length < 2 ? "0" + _minutes : _minutes.ToString())
                + " : " +
                (_seconds.ToString().Length < 2 ? "0" + _seconds : _seconds.ToString());
        }

        private void FrmDongBoDuLieu_Load(object sender, EventArgs e)
        {
            try
            {
                if (!Login())
                {
                    ShowMessage("Sai tên đăng nhập hoặc mật khẩu", false);
                    this.BeginInvoke(new MethodInvoker(this.Close));
                    Config.IsRemember = false;
                }
                else
                {
                    //Load ds Đại lý từ WS
                    List<Company.KDT.SHARE.Components.DaiLy> daiLyCollection = Helpers.Deserialize<List<DaiLy>>(myService.SelectUserDaiLy(maDoanhNghiep,userLogin,passwordLogin));
                    dgDaiLy.DataSource = daiLyCollection;

                    timePanel1.ToDate = DateTime.Today;
                    timePanel1.FromDate = DateTime.Today.AddDays(-7);

                    this.uiStatusBar.Panels["barProgress"].Alignment = HorizontalAlignment.Left;

                    dgList.RootTable.Columns["TrangThaiDongBoTaiLen"].Visible = (FrmDongBoDuLieu.formType == FormType.CLIENT);
                    dgList.RootTable.Columns["TrangThaiDongBoTaiXuong"].Visible = (FrmDongBoDuLieu.formType == FormType.SERVER);
                }

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
//             if (!Login())
//                 return;

            if (formType == FormType.CLIENT)
                this.searchClient();    //Danh cho Client - Dai ly Upload
            else
                this.searchServer();    //Danh cho phia Server - Doanh nghiep Download

            tabControl.Select();
        }

        private void searchClient()
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = "1 = 1";
                where += string.Format(" AND [t_KDT_ToKhaiMauDich].MaDoanhNghiep = '{0}'", maDoanhNghiep);
                where += " AND NgayDangKy BETWEEN '" + timePanel1.FromDate.ToString("MM/dd/yyyy") + "' AND '" + timePanel1.ToDate.ToString("MM/dd/yyyy") + "'";
                where += " AND TrangThaiXuLy = 1 AND PhanLuong <> ''";
                // Thực hiện tìm kiếm.            
                this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "");

                //Update trang thai dong bo cua to khai tuong ung voi tren Server trung gian.
                if (!Login())
                    return;
                
                foreach (ToKhaiMauDich tk in tkmdCollection)
                {
                    Company.KDT.SHARE.Components.Track trackItem = null;
                    string result = myService.SelectTrack(userLogin, passwordLogin, tk.SoToKhai, tk.NgayDangKy.Year, tk.MaLoaiHinh, tk.MaHaiQuan, tk.MaDoanhNghiep);
                    if (!string.IsNullOrEmpty(result))
                    {
                        if (result == "MK")
                        {
                            ShowMessage("Sai thông tin mật khẩu hoặc user", false);
                            Config.IsRemember = false;
                            return;
                        }
                        else
                        {
                            trackItem = Helpers.Deserialize<Track>(result);
                        }
                    }
                    tk.TrangThaiDongBoTaiLen = (trackItem != null ? trackItem.TrangThaiDongBoTaiLen : 0);
                }

                dgList.DataSource = this.tkmdCollection;

                //Set column width
                dgList.RootTable.Columns["SoToKhai"].Width = 90;
                dgList.RootTable.Columns["MaLoaiHinh"].Width = 86;
                dgList.RootTable.Columns["MaHaiquan"].Width = 60;
                dgList.RootTable.Columns["NgayTiepNhan"].Width = 110;
                dgList.RootTable.Columns["SoTiepNhan"].Width = 102;
                dgList.RootTable.Columns["PhanLuong"].Width = 85;
                dgList.RootTable.Columns["TrangThaiDongBoTaiLen"].Width = 101;

                dgList.RootTable.Groups["MaLoaiHinh"].SortOrder = Janus.Windows.GridEX.SortOrder.Ascending;
                dgList.RootTable.Columns["ID"].SortIndicator = Janus.Windows.GridEX.SortIndicator.Ascending;

                dgList.RootTable.Columns["TenDaiLyKhaiBao"].Visible = false;

                dgList.MoveFirst();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void searchServer()
        {
            try
            {
                if (!Login())
                    return;

                //Lay danh sach To khai theo Dai ly duoc check tren luoi
                Company.BLL.KDT.ToKhaiMauDichCollection tkmdCollectionDownloaded = new ToKhaiMauDichCollection();
                

               // Company.BLL.KDT.ToKhaiMauDichCollection tkmdList = null;
                Company.KDT.SHARE.Components.DaiLy userDaiLy = new Company.KDT.SHARE.Components.DaiLy();
                //int cntCheck = 0;
                List<DaiLy> listDL = new List<DaiLy>();
                GridEXRow[] listRows = dgDaiLy.GetCheckedRows();
                foreach (GridEXRow item in listRows)
                {
                    listDL.Add((DaiLy)item.DataRow);
                }
                if (listDL.Count == 0)
                {
                    ShowMessage("Chưa chọn đại lý cần tìm kiếm", false);
                    return;
                }
                string resulst = myService.SeachTKMD_SXXK(this.userLogin, this.passwordLogin, Helpers.Serializer(listDL), maDoanhNghiep, timePanel1.FromDate, timePanel1.ToDate);
                if (!string.IsNullOrEmpty(resulst) && resulst != "MK")
                    tkmdCollectionDownloaded = Helpers.Deserialize<ToKhaiMauDichCollection>(resulst);
                else if (resulst == "MK")
                {
                    MessageBox.Show("Sai mật khẩu");
                    Config.IsRemember = false;
                }
                //                             tkmdList = myService.ReceivesByDaiLy_SXXK(Config.User, Config.Pass, maDoanhNghiep, timePanel1.FromDate, timePanel1.ToDate, userDaiLy.ID);
                // 
                //                             if (tkmdList != null)
                //                                 tkmdCollectionDownloaded.AddRange(tkmdList.ToArray());
//                 if (listTkmd != null)
//                     listTrack.AddRange(listTkmd);
                //                                 
                //                         }
                //                     }
                //                 }
// 
//                 if (cntCheck == 0)
//                 {
//                     MLMessages("Chưa chọn tên đại lý.", "", "", false);
//                     return;
//                 }

                if (tkmdCollectionDownloaded.Count == 0)
                {
                    MLMessages("Không tìm thấy thông tin tờ khai từ ngày \""
                        + timePanel1.FromDate.ToString("dd/MM/yyyy")
                        + "\" đến ngày \""
                        + timePanel1.ToDate.ToString("dd/MM/yyyy")
                        + "\".", "", "", false);
                    return;
                }

                //Update trang thai dong bo cua to khai tuong ung voi tren Server trung gian.
                //                 Company.KDT.SHARE.Components.Track trackItem = null;
                //                 foreach (ToKhaiMauDich tk in tkmdCollectionDownloaded)
                //                 {
                //                     trackItem = Helpers.Deserialize<Track>(myService.SelectTrack(Config.User, Config.Pass, tk.SoToKhai, tk.NamDK, tk.MaLoaiHinh, tk.MaHaiQuan, tk.MaDoanhNghiep));
                // 
                //                     tk.TrangThaiDongBoTaiLen = (trackItem != null ? trackItem.TrangThaiDongBoTaiLen : 0);
                //                 }

                // Thực hiện tìm kiếm.            
                //dgList.DataSource = tkmdCollectionDownloaded;

                //Set column width
                dgList.RootTable.Columns["SoToKhai"].Width = 90;
                dgList.RootTable.Columns["MaLoaiHinh"].Width = 86;
                dgList.RootTable.Columns["MaHaiquan"].Width = 60;
                dgList.RootTable.Columns["NgayTiepNhan"].Width = 110;
                dgList.RootTable.Columns["SoTiepNhan"].Width = 102;
                dgList.RootTable.Columns["PhanLuong"].Width = 85;
                dgList.RootTable.Columns["TrangThaiDongBoTaiXuong"].Width = 101;

                dgList.RootTable.Groups["MaLoaiHinh"].SortOrder = Janus.Windows.GridEX.SortOrder.Ascending;
                dgList.RootTable.Columns["SoToKhai"].SortIndicator = Janus.Windows.GridEX.SortIndicator.Ascending;

                dgList.RootTable.Columns["ID"].Visible = false;
                dgList.RootTable.Columns["TenDaiLyKhaiBao"].Visible = true;

                
                dgList.RootTable.Columns["TrangThaiDongBoTaiLen"].Visible = false;
//                 dgList.RootTable.Columns["NgayDangKy"].DataMember = "NgayDongBo";
//                 dgList.RootTable.Columns["NgayDangKy"].Caption = "Ngày đồng bộ";
                dgList.DataSource = tkmdCollectionDownloaded;
                dgList.Refresh();


                dgList.MoveFirst();

                if (dgList.RowCount > 0)
                    tabControl.SelectedTab = tabToKhai;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                switch (Convert.ToInt32(e.Row.Cells["PhanLuong"].Value))
                {
                    case 1:
                        e.Row.Cells["PhanLuong"].Text = "Xanh";
                        break;
                    case 2:
                        e.Row.Cells["PhanLuong"].Text = "Vàng";
                        break;
                    case 3:
                        e.Row.Cells["PhanLuong"].Text = "Đỏ";
                        break;
                }

                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    if (FrmDongBoDuLieu.formType == FormType.CLIENT)
                    {
                        _trangThai = Convert.ToInt32(e.Row.Cells["TrangThaiDongBoTaiLen"].Value);
                        e.Row.Cells["TrangThaiDongBoTaiLen"].Text = (_trangThai == 1 ? "Đã tải lên." : "Chưa tải lên.");
                    }
                    else if (FrmDongBoDuLieu.formType == FormType.SERVER)
                    {
                        _trangThai = Convert.ToInt32(e.Row.Cells["TrangThaiDongBoTaiXuong"].Value);
                        e.Row.Cells["TrangThaiDongBoTaiXuong"].Text = (_trangThai == 1 ? "Đã tải xuống." : "Chưa tải xuống.");
                    }

                    e.Row.Cells["TenDaiLyKhaiBao"].Text = (e.Row.Cells["TenDaiLyKhaiBao"].Text == "" ? "Không xác định" : e.Row.Cells["TenDaiLyKhaiBao"].Text);
                }

            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                _seconds = 0;
                this.uiStatusBar.Panels["barProgress"].Text = "";
                this.uiStatusBar.Panels["barProgress"].ToolTipText = "";

                //Lay danh sach To khai duoc check tren luoi
                Company.BLL.KDT.ToKhaiMauDichCollection tkmdCollectionSelect = new ToKhaiMauDichCollection();
                GridEXRow[] listrows = dgList.GetCheckedRows();
              
                    foreach (GridEXRow item in listrows)
                    {
                        tkmdCollectionSelect.Add((Company.BLL.KDT.ToKhaiMauDich)item.DataRow);
                    }
            


                
//                 for (int i = 0; i < dgList.RowCount; i++)
//                 {
//                     if (dgList.GetRow(i).RowType == Janus.Windows.GridEX.RowType.Record)
//                     {
//                         if (Convert.ToBoolean(dgList.GetRow(i).Cells["Select"].Value))
//                         {
//                             tkmdCollectionSelect.Add((Company.BLL.KDT.ToKhaiMauDich)dgList.GetRow(i).DataRow);
//                         }
//                     }
//                 }

                //Hien thi thong bao neu khong co to khai nao duoc chon
                if (tkmdCollectionSelect.Count == 0 )
                {
                    ShowMessage("Bạn chưa chọn tờ khai.", false);
                    e.Cancel = true;
                }



                string msg = "";
                for (int j = 0; j < tkmdCollectionSelect.Count; j++)
                {
                    if (!e.Cancel)
                    {
                        msg = "";
                        Company.BLL.KDT.ToKhaiMauDich tkmd = tkmdCollectionSelect[j];
                        string status = string.Format("Đang thực hiện đồng bộ tờ khai : {0}/{1}/{2}", tkmd.SoToKhai, tkmd.MaLoaiHinh, tkmd.NgayDangKy);
                        SetStatus(status);
                        if (tkmd.SoToKhai == 1254 && tkmd.MaLoaiHinh == "XSX01")
                        {

                        }
                        //Thuc hien gui thong tin to khai den Server
                        if (formType == FormType.CLIENT)
                        {
                            //Load thong tin hang mau dich cua to khai
                            tkmd.LoadHMDCollection();

                            //Load Chung tu kem lien quan cua To khai.
                            if (bool.Parse(WebService.LoadConfigure("CHUNGTUDINHKEM")))
                                tkmd.LoadChungTuHaiQuan();

                            msg = myService.Send_SXXK(userLogin, passwordLogin, Helpers.Serializer(tkmd));
                            if (!string.IsNullOrEmpty(msg))
                            {
                                if (msg == "MK")
                                {
                                    Config.IsRemember = false;
                                }
                                else
                                {

                                }
                            }
                        }
                        //Thuc hien lay thong tin to khai tu Server
                        else if (formType == FormType.SERVER)
                        {
                            string strtkmdServer = myService.Receive_SXXK(userLogin, passwordLogin, tkmd.SoToKhai,tkmd.NgayDangKy.Year, tkmd.MaLoaiHinh, tkmd.MaHaiQuan, tkmd.MaDoanhNghiep); 
                            //Lay thong tin to khai tu server trung gian
                            Company.BLL.KDT.ToKhaiMauDich tkmdServer = null;
                            if (!string.IsNullOrEmpty(strtkmdServer) && strtkmdServer != "MK")
                                tkmdServer = Helpers.Deserialize<Company.BLL.KDT.ToKhaiMauDich>(strtkmdServer);
                            //Company.BLL.KDT.ToKhaiMauDich tkmdServer = this.Receive(Config.User, Config.Pass, tkmd.SoToKhai, tkmd.NamDK, tkmd.MaLoaiHinh, tkmd.MaHaiQuan, tkmd.MaDoanhNghiep);

                            //Them moi/ Cap nhat thong tin to khai va cac thong tin lien quan khac vao CSDL.
                            if (tkmdServer != null)
                            {
                                try
                                {
                                    if (chkOverwrite.Checked)
                                    {
                                        //Ghi de du lieu to khai va cac thong tin lien quan khac.
                                        tkmdServer.InsertUpdateFullDaiLy();
                                        tkmdServer.ReSetSoLuong();
                                        BLL.SXXK.ToKhai.ToKhaiMauDich tkmddk = null;
                                        tkmddk.LoadBy(tkmdServer.MaHaiQuan, tkmdServer.SoToKhai, tkmdServer.MaLoaiHinh, tkmdServer.NamDK);
                                        if (tkmddk != null)
                                        {
                                            tkmdServer.CapNhatThongTinHangToKhaiSua();
                                        }
                                        else
                                            tkmdServer.TransgferDataToSXXK();

                                    }
                                    else
                                    {
                                        //Kiem tra thong tin to khai da ton tai?. Neu da ton tai -> ghi lai Track to khai do va thuc hien to khai tiep theo.
                                        if (Company.BLL.KDT.ToKhaiMauDich.IsExists(maHaiQuan, maDoanhNghiep, tkmdServer.SoToKhai, tkmdServer.NamDK, tkmdServer.MaLoaiHinh))
                                        {
                                            msg = "Thông tin tờ khai này đã tồn tại:" +
                                                "\r\nSố tờ khai:    " + tkmdServer.SoToKhai +
                                                "\r\nMã loại hình:  " + tkmdServer.MaLoaiHinh +
                                                "\r\nNgày đăng ký:  " + tkmdServer.NgayDangKy.ToString("dd/MM/yyyy");
                                        }
                                        else
                                        {
                                            tkmdServer.InsertUpdateFullDaiLy();

                                            tkmdServer.TransgferDataToSXXK();
                                        }
                                    }
                                }
                                catch (Exception ex) 
                                {
                                    msg = ex.Message;
                                    //Logger.LocalLogger.Instance().WriteMessage(ex);
                                }
                            }
                        }

                        //Track info
                        InsertTrack(msg, tkmd);

                        if (msg.Length > 0)
                            //throw new Exception(msg);
                            Logger.LocalLogger.Instance().WriteMessage(new Exception(msg));

                        bw.ReportProgress((int)(((double)(j + 1) / (double)tkmdCollectionSelect.Count) * 100));
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                e.Result = ex.Message;
                SetError(ex.Message);
                e.Cancel = true;
            }
        }

        private void bw_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            try
            {
                //this.uiStatusBar.Panels["barProgress"].ProgressBarValue = e.ProgressPercentage;

                //this.uiStatusBar.Panels["barProgress"].Text = e.ProgressPercentage.ToString() + " %";

                setProgessBar(e.ProgressPercentage);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                timer.Stop();

                btnSynData.Text = "Thực hiện";
                btnSynData.Enabled = true;
                btnClose.Enabled = true;

                //Neu khong xay ra loi nao
                if (e.Result != null && e.Result.ToString().Length == 0)
                    MLMessages("Đã hoàn thành!", "", "", false);

                btnSearch_Click(null, EventArgs.Empty);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        /// <summary>
        /// Ghi lai thong tin thuc hien vao bang t_DongBoDuLieu_Track
        /// </summary>
        /// <param name="msg"></param>
        /// <param name="tkmd"></param>
        private void InsertTrack(string msg, Company.BLL.KDT.ToKhaiMauDich tkmd)
        {
            Company.KDT.SHARE.Components.DaiLy userDaiLy = Helpers.Deserialize<DaiLy>(myService.LoginDaiLy(Config.User, Config.Pass));

            if (userDaiLy != null)
            {
                //Update Offline
                //Them vao bang t_DongBoDuLieu_Track
                Company.KDT.SHARE.Components.Track newTrack = new Company.KDT.SHARE.Components.Track();
                newTrack.MaDoanhNghiep = tkmd.MaDoanhNghiep;
                newTrack.MaHaiQuan = tkmd.MaHaiQuan;
                newTrack.SoToKhai = tkmd.SoToKhai;
                newTrack.NamDangKy = tkmd.NamDK;
                newTrack.MaLoaiHinh = tkmd.MaLoaiHinh;
                newTrack.TKMD_GUIDSTR = tkmd.GUIDSTR;
                //Ghi log Error neu co.
                newTrack.GhiChu = msg;
                //1: Thanh cong; 0: Khong thanh cong.
                if (formType == FormType.CLIENT)
                    newTrack.TrangThaiDongBoTaiLen = msg.Length == 0 ? 1 : 0;
                if (formType == FormType.SERVER)
                {
                    //Update TrangThai Dong bo server local.
                    newTrack.TrangThaiDongBoTaiXuong = msg.Length == 0 ? 1 : 0;
                }
                //ID user dai ly gui du lieu den Server.
                newTrack.IDUserDaiLy = userDaiLy != null ? userDaiLy.ID : 0;
                newTrack.UserName = userDaiLy != null ? userDaiLy.USER_NAME : "";
                newTrack.NgayDongBo = DateTime.Now;


                //Update Online
                //Load thong tin Track tren server traung gian
//                 string result = myService.SelectTrack(Config.User, Config.Pass, tkmd.SoToKhai, tkmd.NgayDangKy.Year, tkmd.MaLoaiHinh, tkmd.MaHaiQuan, tkmd.MaDoanhNghiep);
//                 Company.KDT.SHARE.Components.Track trackServer = null;
//                 if (!string.IsNullOrEmpty(result))
//                     trackServer = Helpers.Deserialize<Track>(result);
                if (formType == FormType.SERVER)
                {
                    //Ghi log Error neu co.
                    /*trackServer.GhiChu = msg;*/

                    //Update TrangThai Dong bo server.
//                     if (trackServer.TrangThaiDongBoTaiXuong == 0)
//                         trackServer.TrangThaiDongBoTaiXuong = msg.Length == 0 ? 1 : 0;
                    
                    string result = myService.UpdateTrack(userLogin, passwordLogin, tkmd.SoToKhai, tkmd.NgayDangKy.Year, tkmd.MaLoaiHinh, tkmd.MaHaiQuan, tkmd.MaDoanhNghiep, msg);
                    if (!string.IsNullOrEmpty(result))
                    {
                        newTrack.GhiChu = ", Không update được track trên server: " + result;
                    }
                }
                newTrack.InsertUpdateBy();

            }
        }

        /// <summary>
        /// Test method
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="TKMD"></param>
        /// <returns></returns>
        public string Send(string userNameLogin, string passWordLogin, ref Company.BLL.KDT.ToKhaiMauDich TKMD)
        {
            string error = "";
            //Neu login thanh cong

            Company.KDT.SHARE.Components.DaiLy userDaiLy = Helpers.Deserialize<DaiLy>(myService.LoginDaiLy(userNameLogin, passWordLogin));

            try
            {
                //Insert to khai va hang mau dich
                TKMD.InsertUpdateFullDaiLy();
                error = "";
            }
            catch (Exception ex) { error = ex.Message; }

            //Them vao bang t_DongBoDuLieu_Track
            Company.KDT.SHARE.Components.Track newTrack = new Company.KDT.SHARE.Components.Track();
            newTrack.MaDoanhNghiep = TKMD.MaDoanhNghiep;
            newTrack.MaHaiQuan = TKMD.MaHaiQuan;
            newTrack.SoToKhai = TKMD.SoToKhai;
            newTrack.NamDangKy = TKMD.NamDK;
            newTrack.MaLoaiHinh = TKMD.MaLoaiHinh;
            newTrack.TKMD_GUIDSTR = TKMD.GUIDSTR;
            //Ghi log Error neu co.
            newTrack.GhiChu = error;
            //1: Thanh cong; 0: Khong thanh cong.
            newTrack.TrangThaiDongBoTaiLen = error.Length == 0 ? 1 : 0;
            newTrack.TrangThaiDongBoTaiXuong = 0;
            //ID user dai ly gui du lieu den Server.
            newTrack.IDUserDaiLy = userDaiLy != null ? userDaiLy.ID : 0;
            newTrack.UserName = userDaiLy != null ? userDaiLy.USER_NAME : "";
            newTrack.NgayDongBo = DateTime.Now;

            newTrack.Insert();

            return error;
        }

        public Company.BLL.KDT.ToKhaiMauDich Receive(string userNameLogin, string passWordLogin, int soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep)
        {
            Company.KDT.SHARE.Components.DaiLy userDaiLy = Helpers.Deserialize<DaiLy>(myService.LoginDaiLy(userNameLogin, passWordLogin));

            //Neu login thanh cong
            if (userDaiLy != null)
            {
                Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();
                tkmd.Load(maHaiQuan, maDoanhNghiep, soToKhai, namDangKy, maLoaiHinh);

                //Load Hang hoa mau dich
                tkmd.LoadHMDCollection();

                //Load Chung tu kem khac...

                return tkmd;
            }

            return null;
        }

        private void btnResult_Click(object sender, EventArgs e)
        {
            if (!Login())
                return;

            Company.KDT.SHARE.Components.DaiLy userDaiLy = Helpers.Deserialize<DaiLy>((myService.LoginDaiLy(Config.User, Config.Pass)));

            string arrayUserDaiLy = "";
            if (userDaiLy != null)
            {
                if (formType == FormType.CLIENT)
                    _frmKetQuaXuLy.UserName = userDaiLy != null ? "'" + userDaiLy.USER_NAME + "'" : "";
                else if (formType == FormType.SERVER)
                {
                    for (int i = 0; i < dgDaiLy.RowCount; i++)
                    {
                        if (dgDaiLy.GetRow(i).RowType == Janus.Windows.GridEX.RowType.Record)
                        {
                            Company.KDT.SHARE.Components.DaiLy userDL = (Company.KDT.SHARE.Components.DaiLy)dgDaiLy.GetRow(i).DataRow;

                            if (userDL != null)
                            {
                                arrayUserDaiLy += "'" + userDL.USER_NAME + "'";
                            }

                            if (i < dgDaiLy.RowCount - 1)
                                arrayUserDaiLy += ", ";
                        }
                    }

                    _frmKetQuaXuLy.UserName = arrayUserDaiLy;
                }

                _frmKetQuaXuLy.ToDate = timePanel1.ToDate;
                _frmKetQuaXuLy.FromDate = timePanel1.FromDate;

                _frmKetQuaXuLy.ShowDialog();
            }
        }

        private void tabControl_SelectedTabChanged(object sender, Janus.Windows.UI.Tab.TabEventArgs e)
        {
            chkOverwrite.Checked = (e.Page.Key == "tabToKhai");
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            countError++;
            if (countError == 7)
            {
                countError = 0;
                lbError.Visible = false;
                timer1.Enabled = false;
            }
            if (lbError.BackColor == Color.Red)
                lbError.BackColor = Color.Black;
            else lbError.BackColor = Color.Red;

        }

        private void dgList_KeyDown(object sender, KeyEventArgs e)
        {
            CheckRows(e, dgList);
        }

        private static bool check = true;
        private void CheckRows(KeyEventArgs e, GridEX dataGrid)
        {
            if (e.KeyCode == Keys.Space)
            {
                foreach (GridEXSelectedItem item in dataGrid.SelectedItems)
                {
                    dataGrid.GetRow(item.Position).CheckState = (check == true ? RowCheckState.Checked : RowCheckState.Unchecked);
                }

                dataGrid.Refresh();

                check = !check;
            }
        }


        #region WebClient
        private static string CreateSoapEnvelope(string Name, string parameter)
        {
            string.Format("");
            XmlDocument soapEnvelop = new XmlDocument();
            string soapenvelop = string.Format(@"
<?xml version=""1.0"" encoding=""utf-8""?>
<soap:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap=""http://schemas.xmlsoap.org/soap/envelope/"">
  <soap:Body>
  <{0} xmlns=""http://tempuri.org/"">
      {1}
    </SeachTKMD_SXXK>
  </soap:Body>
</soap:Envelope>", Name, parameter);
            return soapenvelop;
        }
        private string seachFrocessTKMD(string listDL)
        {
            string soap = string.Format(@" <userNameLogin>{0}</userNameLogin>
                                           <passWordLogin>{1}</passWordLogin>
                                           <listDaiLy>{2}</listDaiLy>
                                           <MaDoanhNghiep>{3}</MaDoanhNghiep>
                                           <FromNgayDongBo>{4}</FromNgayDongBo>
                                           <ToNgayDongBo>{5}</ToNgayDongBo>", this.userLogin, this.passwordLogin, listDL, maDoanhNghiep, timePanel1.FromDate.ToString("yyyy-MM-dd"), timePanel1.ToDate.ToString("yyyy-MM-dd"));
            frmProcess f = new frmProcess();
            f.UrlWS = IsyncDaTa_V3.LoadConfigure("WS_SyncData");
            f.Tempuri = "SeachTKMD_SXXK";
            f.NoiDungQuerry = CreateSoapEnvelope("SeachTKMD_SXXK", soap);
            f.ShowDialog(this);
            return f.result;
        }


        #endregion
    }
}
