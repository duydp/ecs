﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class HoaDonThuongMaiDetail
    {
        public static int DeleteBy_HoaDonTM_ID(long hoaDonTM_ID, SqlTransaction tran)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteBy_HoaDonTM_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HoaDonTM_ID", SqlDbType.BigInt, hoaDonTM_ID);

            return db.ExecuteNonQuery(dbCommand, tran);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_HoaDonTM_ID(long hoaDonTM_ID, SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMaiDetail_DeleteBy_HoaDonTM_ID]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HoaDonTM_ID", SqlDbType.BigInt, hoaDonTM_ID);

            return db.ExecuteNonQuery(dbCommand, transaction);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_KDT_HoaDonThuongMaiDetail_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
            db.AddInParameter(dbCommand, "@HoaDonTM_ID", SqlDbType.BigInt, HoaDonTM_ID);
            db.AddInParameter(dbCommand, "@GiaTriDieuChinhTang", SqlDbType.Float, GiaTriDieuChinhTang);
            db.AddInParameter(dbCommand, "@GiaiTriDieuChinhGiam", SqlDbType.Float, GiaiTriDieuChinhGiam);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
            db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
            db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
            db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

    }
}
