﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.VNACCS
{
    public class VAL0940 : VAL0870
    {
        public static int TongSoByte { get { return 2966; } }

        public VAL0940(string strResult)
        {
            try
            {
                this.GetObject<VAL0940>(strResult, true, GlobalVNACC.PathConfig, "VAL0940", false, VAL0940.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAL0940.TongSoByte);
                while (true)
                {
                    IVA_HANGHOA ivaHang = new IVA_HANGHOA();
                    ivaHang.GetObject<IVA_HANGHOA>(strResult, true, GlobalVNACC.PathConfig, "IVA_HANGHOA", false, IVA_HANGHOA.TotalByte);
                    if (this.HangHoaTrongHoaDon == null) this.HangHoaTrongHoaDon = new List<IVA_HANGHOA>();
                    this.HangHoaTrongHoaDon.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > IVA_HANGHOA.TotalByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - IVA_HANGHOA.TotalByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, IVA_HANGHOA.TotalByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }
}
