using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
    public partial class BCXuatNhapTon
    {
        #region Private members.

        protected long _ID;
        protected long _STT;
        protected int _LanThanhLy;
        protected int _NamThanhLy;
        protected string _MaDoanhNghiep = String.Empty;
        protected string _MaNPL = String.Empty;
        protected string _TenNPL = String.Empty;
        protected int _SoToKhaiNhap;
        protected DateTime _NgayDangKyNhap = new DateTime(1900, 01, 01);
        protected DateTime _NgayHoanThanhNhap = new DateTime(1900, 01, 01);
        protected string _MaLoaiHinhNhap = String.Empty;
        protected decimal _LuongNhap;
        protected decimal _LuongTonDau;
        protected string _TenDVT_NPL = String.Empty;
        protected string _MaSP = String.Empty;
        protected string _TenSP = String.Empty;
        protected int _SoToKhaiXuat;
        protected DateTime _NgayDangKyXuat = new DateTime(1900, 01, 01);
        protected DateTime _NgayHoanThanhXuat = new DateTime(1900, 01, 01);
        protected string _MaLoaiHinhXuat = String.Empty;
        protected decimal _LuongSPXuat;
        protected string _TenDVT_SP = String.Empty;
        protected decimal _DinhMuc;
        protected decimal _LuongNPLSuDung;
        protected int _SoToKhaiTaiXuat;
        protected DateTime _NgayTaiXuat = new DateTime(1900, 01, 01);
        protected decimal _LuongNPLTaiXuat;
        protected decimal _LuongTonCuoi;
        protected string _ThanhKhoanTiep = String.Empty;
        protected string _ChuyenMucDichKhac = String.Empty;
        protected double _DonGiaTT;
        protected decimal _TyGiaTT;
        protected decimal _ThueSuat;
        protected double _ThueXNK;
        protected double _ThueXNKTon;
        protected DateTime _NgayThucXuat = new DateTime(1900, 01, 01);

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public long STT
        {
            set { this._STT = value; }
            get { return this._STT; }
        }
        public int LanThanhLy
        {
            set { this._LanThanhLy = value; }
            get { return this._LanThanhLy; }
        }
        public int NamThanhLy
        {
            set { this._NamThanhLy = value; }
            get { return this._NamThanhLy; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }
        public string MaNPL
        {
            set { this._MaNPL = value; }
            get { return this._MaNPL; }
        }
        public string TenNPL
        {
            set { this._TenNPL = value; }
            get { return this._TenNPL; }
        }
        public int SoToKhaiNhap
        {
            set { this._SoToKhaiNhap = value; }
            get { return this._SoToKhaiNhap; }
        }
        public DateTime NgayDangKyNhap
        {
            set { this._NgayDangKyNhap = value; }
            get { return this._NgayDangKyNhap; }
        }
        public DateTime NgayHoanThanhNhap
        {
            set { this._NgayHoanThanhNhap = value; }
            get { return this._NgayHoanThanhNhap; }
        }
        public string MaLoaiHinhNhap
        {
            set { this._MaLoaiHinhNhap = value; }
            get { return this._MaLoaiHinhNhap; }
        }
        public decimal LuongNhap
        {
            set { this._LuongNhap = value; }
            get { return this._LuongNhap; }
        }
        public decimal LuongTonDau
        {
            set { this._LuongTonDau = value; }
            get { return this._LuongTonDau; }
        }
        public string TenDVT_NPL
        {
            set { this._TenDVT_NPL = value; }
            get { return this._TenDVT_NPL; }
        }
        public string MaSP
        {
            set { this._MaSP = value; }
            get { return this._MaSP; }
        }
        public string TenSP
        {
            set { this._TenSP = value; }
            get { return this._TenSP; }
        }
        public int SoToKhaiXuat
        {
            set { this._SoToKhaiXuat = value; }
            get { return this._SoToKhaiXuat; }
        }
        public DateTime NgayDangKyXuat
        {
            set { this._NgayDangKyXuat = value; }
            get { return this._NgayDangKyXuat; }
        }
        public DateTime NgayHoanThanhXuat
        {
            set { this._NgayHoanThanhXuat = value; }
            get { return this._NgayHoanThanhXuat; }
        }
        public string MaLoaiHinhXuat
        {
            set { this._MaLoaiHinhXuat = value; }
            get { return this._MaLoaiHinhXuat; }
        }
        public decimal LuongSPXuat
        {
            set { this._LuongSPXuat = value; }
            get { return this._LuongSPXuat; }
        }
        public string TenDVT_SP
        {
            set { this._TenDVT_SP = value; }
            get { return this._TenDVT_SP; }
        }
        public decimal DinhMuc
        {
            set { this._DinhMuc = value; }
            get { return this._DinhMuc; }
        }
        public decimal LuongNPLSuDung
        {
            set { this._LuongNPLSuDung = value; }
            get { return this._LuongNPLSuDung; }
        }
        public int SoToKhaiTaiXuat
        {
            set { this._SoToKhaiTaiXuat = value; }
            get { return this._SoToKhaiTaiXuat; }
        }
        public DateTime NgayTaiXuat
        {
            set { this._NgayTaiXuat = value; }
            get { return this._NgayTaiXuat; }
        }
        public decimal LuongNPLTaiXuat
        {
            set { this._LuongNPLTaiXuat = value; }
            get { return this._LuongNPLTaiXuat; }
        }
        public decimal LuongTonCuoi
        {
            set { this._LuongTonCuoi = value; }
            get { return this._LuongTonCuoi; }
        }
        public string ThanhKhoanTiep
        {
            set { this._ThanhKhoanTiep = value; }
            get { return this._ThanhKhoanTiep; }
        }
        public string ChuyenMucDichKhac
        {
            set { this._ChuyenMucDichKhac = value; }
            get { return this._ChuyenMucDichKhac; }
        }
        public double DonGiaTT
        {
            set { this._DonGiaTT = value; }
            get { return this._DonGiaTT; }
        }
        public decimal TyGiaTT
        {
            set { this._TyGiaTT = value; }
            get { return this._TyGiaTT; }
        }
        public decimal ThueSuat
        {
            set { this._ThueSuat = value; }
            get { return this._ThueSuat; }
        }
        public double ThueXNK
        {
            set { this._ThueXNK = value; }
            get { return this._ThueXNK; }
        }
        public double ThueXNKTon
        {
            set { this._ThueXNKTon = value; }
            get { return this._ThueXNKTon; }
        }

        public DateTime NgayThucXuat
        {
            set { this._NgayThucXuat = value; }
            get { return this._NgayThucXuat; }
        }
        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_KDT_SXXK_BCXuatNhapTon_Load";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) this._STT = reader.GetInt64(reader.GetOrdinal("STT"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamThanhLy"))) this._NamThanhLy = reader.GetInt32(reader.GetOrdinal("NamThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) this._TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) this._SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) this._NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhNhap"))) this._NgayHoanThanhNhap = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) this._MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) this._LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) this._LuongTonDau = reader.GetDecimal(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) this._TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) this._MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSP"))) this._TenSP = reader.GetString(reader.GetOrdinal("TenSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) this._SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) this._NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhXuat"))) this._NgayHoanThanhXuat = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) this._MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongSPXuat"))) this._LuongSPXuat = reader.GetDecimal(reader.GetOrdinal("LuongSPXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_SP"))) this._TenDVT_SP = reader.GetString(reader.GetOrdinal("TenDVT_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) this._DinhMuc = reader.GetDecimal(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) this._LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiTaiXuat"))) this._SoToKhaiTaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTaiXuat"))) this._NgayTaiXuat = reader.GetDateTime(reader.GetOrdinal("NgayTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTaiXuat"))) this._LuongNPLTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongNPLTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) this._LuongTonCuoi = reader.GetDecimal(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhKhoanTiep"))) this._ThanhKhoanTiep = reader.GetString(reader.GetOrdinal("ThanhKhoanTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChuyenMucDichKhac"))) this._ChuyenMucDichKhac = reader.GetString(reader.GetOrdinal("ChuyenMucDichKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) this._DonGiaTT = reader.GetDouble(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTT"))) this._TyGiaTT = reader.GetDecimal(reader.GetOrdinal("TyGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuat"))) this._ThueSuat = reader.GetDecimal(reader.GetOrdinal("ThueSuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this._ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) this._ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucXuat"))) this._NgayThucXuat = reader.GetDateTime(reader.GetOrdinal("NgayThucXuat"));
                return true;
            }
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_BCXuatNhapTon_SelectAll";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_BCXuatNhapTon_SelectAll";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_BCXuatNhapTon_SelectDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_BCXuatNhapTon_SelectDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public BCXuatNhapTonCollection SelectCollectionAll()
        {
            BCXuatNhapTonCollection collection = new BCXuatNhapTonCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                BCXuatNhapTon entity = new BCXuatNhapTon();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt64(reader.GetOrdinal("STT"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamThanhLy"))) entity.NamThanhLy = reader.GetInt32(reader.GetOrdinal("NamThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhNhap"))) entity.NgayHoanThanhNhap = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) entity.LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDecimal(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) entity.TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSP"))) entity.TenSP = reader.GetString(reader.GetOrdinal("TenSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhXuat"))) entity.NgayHoanThanhXuat = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongSPXuat"))) entity.LuongSPXuat = reader.GetDecimal(reader.GetOrdinal("LuongSPXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_SP"))) entity.TenDVT_SP = reader.GetString(reader.GetOrdinal("TenDVT_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetDecimal(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) entity.LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiTaiXuat"))) entity.SoToKhaiTaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTaiXuat"))) entity.NgayTaiXuat = reader.GetDateTime(reader.GetOrdinal("NgayTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTaiXuat"))) entity.LuongNPLTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongNPLTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDecimal(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhKhoanTiep"))) entity.ThanhKhoanTiep = reader.GetString(reader.GetOrdinal("ThanhKhoanTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChuyenMucDichKhac"))) entity.ChuyenMucDichKhac = reader.GetString(reader.GetOrdinal("ChuyenMucDichKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDouble(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTT"))) entity.TyGiaTT = reader.GetDecimal(reader.GetOrdinal("TyGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuat"))) entity.ThueSuat = reader.GetDecimal(reader.GetOrdinal("ThueSuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) entity.ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucXuat"))) entity.NgayThucXuat = reader.GetDateTime(reader.GetOrdinal("NgayThucXuat"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public BCXuatNhapTonCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            BCXuatNhapTonCollection collection = new BCXuatNhapTonCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                BCXuatNhapTon entity = new BCXuatNhapTon();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt64(reader.GetOrdinal("STT"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamThanhLy"))) entity.NamThanhLy = reader.GetInt32(reader.GetOrdinal("NamThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhNhap"))) entity.NgayHoanThanhNhap = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) entity.LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDecimal(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) entity.TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSP"))) entity.TenSP = reader.GetString(reader.GetOrdinal("TenSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhXuat"))) entity.NgayHoanThanhXuat = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongSPXuat"))) entity.LuongSPXuat = reader.GetDecimal(reader.GetOrdinal("LuongSPXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_SP"))) entity.TenDVT_SP = reader.GetString(reader.GetOrdinal("TenDVT_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetDecimal(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) entity.LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiTaiXuat"))) entity.SoToKhaiTaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTaiXuat"))) entity.NgayTaiXuat = reader.GetDateTime(reader.GetOrdinal("NgayTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTaiXuat"))) entity.LuongNPLTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongNPLTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDecimal(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhKhoanTiep"))) entity.ThanhKhoanTiep = reader.GetString(reader.GetOrdinal("ThanhKhoanTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChuyenMucDichKhac"))) entity.ChuyenMucDichKhac = reader.GetString(reader.GetOrdinal("ChuyenMucDichKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDouble(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTT"))) entity.TyGiaTT = reader.GetDecimal(reader.GetOrdinal("TyGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuat"))) entity.ThueSuat = reader.GetDecimal(reader.GetOrdinal("ThueSuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDouble(reader.GetOrdinal("ThueXNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNKTon"))) entity.ThueXNKTon = reader.GetDouble(reader.GetOrdinal("ThueXNKTon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucNhap"))) entity.NgayThucXuat = reader.GetDateTime(reader.GetOrdinal("NgayThucNhap"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_BCXuatNhapTon_Insert";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, this._STT);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, this._NamThanhLy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, this._SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, this._NgayDangKyNhap);
            db.AddInParameter(dbCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, this._NgayHoanThanhNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, this._MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, this._LuongNhap);
            db.AddInParameter(dbCommand, "@LuongTonDau", SqlDbType.Decimal, this._LuongTonDau);
            db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, this._TenDVT_NPL);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, this._MaSP);
            db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, this._TenSP);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
            db.AddInParameter(dbCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, this._NgayHoanThanhXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@LuongSPXuat", SqlDbType.Decimal, this._LuongSPXuat);
            db.AddInParameter(dbCommand, "@TenDVT_SP", SqlDbType.VarChar, this._TenDVT_SP);
            db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, this._DinhMuc);
            db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, this._LuongNPLSuDung);
            db.AddInParameter(dbCommand, "@SoToKhaiTaiXuat", SqlDbType.Int, this._SoToKhaiTaiXuat);
            db.AddInParameter(dbCommand, "@NgayTaiXuat", SqlDbType.DateTime, this._NgayTaiXuat);
            db.AddInParameter(dbCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, this._LuongNPLTaiXuat);
            db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Decimal, this._LuongTonCuoi);
            db.AddInParameter(dbCommand, "@ThanhKhoanTiep", SqlDbType.NVarChar, this._ThanhKhoanTiep);
            db.AddInParameter(dbCommand, "@ChuyenMucDichKhac", SqlDbType.NVarChar, this._ChuyenMucDichKhac);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Float, this._DonGiaTT);
            db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Money, this._TyGiaTT);
            db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, this._ThueSuat);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, this._ThueXNK);
            db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, this._ThueXNKTon);
            db.AddInParameter(dbCommand, "@NgayThucXuat", SqlDbType.DateTime, this._NgayThucXuat);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(BCXuatNhapTonCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BCXuatNhapTon item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, BCXuatNhapTonCollection collection)
        {
            foreach (BCXuatNhapTon item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_BCXuatNhapTon_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, this._STT);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, this._NamThanhLy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, this._SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, this._NgayDangKyNhap);
            db.AddInParameter(dbCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, this._NgayHoanThanhNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, this._MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, this._LuongNhap);
            db.AddInParameter(dbCommand, "@LuongTonDau", SqlDbType.Decimal, this._LuongTonDau);
            db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, this._TenDVT_NPL);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, this._MaSP);
            db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, this._TenSP);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
            db.AddInParameter(dbCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, this._NgayHoanThanhXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@LuongSPXuat", SqlDbType.Decimal, this._LuongSPXuat);
            db.AddInParameter(dbCommand, "@TenDVT_SP", SqlDbType.VarChar, this._TenDVT_SP);
            db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, this._DinhMuc);
            db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, this._LuongNPLSuDung);
            db.AddInParameter(dbCommand, "@SoToKhaiTaiXuat", SqlDbType.Int, this._SoToKhaiTaiXuat);
            db.AddInParameter(dbCommand, "@NgayTaiXuat", SqlDbType.DateTime, this._NgayTaiXuat);
            db.AddInParameter(dbCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, this._LuongNPLTaiXuat);
            db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Decimal, this._LuongTonCuoi);
            db.AddInParameter(dbCommand, "@ThanhKhoanTiep", SqlDbType.NVarChar, this._ThanhKhoanTiep);
            db.AddInParameter(dbCommand, "@ChuyenMucDichKhac", SqlDbType.NVarChar, this._ChuyenMucDichKhac);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Float, this._DonGiaTT);
            db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Money, this._TyGiaTT);
            db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, this._ThueSuat);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, this._ThueXNK);
            db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, this._ThueXNKTon);
            db.AddInParameter(dbCommand, "@NgayThucXuat", SqlDbType.DateTime, this._NgayThucXuat);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(BCXuatNhapTonCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BCXuatNhapTon item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_BCXuatNhapTon_Update";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, this._STT);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, this._NamThanhLy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, this._SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, this._NgayDangKyNhap);
            db.AddInParameter(dbCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, this._NgayHoanThanhNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, this._MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, this._LuongNhap);
            db.AddInParameter(dbCommand, "@LuongTonDau", SqlDbType.Decimal, this._LuongTonDau);
            db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, this._TenDVT_NPL);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, this._MaSP);
            db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, this._TenSP);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
            db.AddInParameter(dbCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, this._NgayHoanThanhXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@LuongSPXuat", SqlDbType.Decimal, this._LuongSPXuat);
            db.AddInParameter(dbCommand, "@TenDVT_SP", SqlDbType.VarChar, this._TenDVT_SP);
            db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, this._DinhMuc);
            db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, this._LuongNPLSuDung);
            db.AddInParameter(dbCommand, "@SoToKhaiTaiXuat", SqlDbType.Int, this._SoToKhaiTaiXuat);
            db.AddInParameter(dbCommand, "@NgayTaiXuat", SqlDbType.DateTime, this._NgayTaiXuat);
            db.AddInParameter(dbCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, this._LuongNPLTaiXuat);
            db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Decimal, this._LuongTonCuoi);
            db.AddInParameter(dbCommand, "@ThanhKhoanTiep", SqlDbType.NVarChar, this._ThanhKhoanTiep);
            db.AddInParameter(dbCommand, "@ChuyenMucDichKhac", SqlDbType.NVarChar, this._ChuyenMucDichKhac);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Float, this._DonGiaTT);
            db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Money, this._TyGiaTT);
            db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, this._ThueSuat);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Float, this._ThueXNK);
            db.AddInParameter(dbCommand, "@ThueXNKTon", SqlDbType.Float, this._ThueXNKTon);
            db.AddInParameter(dbCommand, "@NgayThucXuat", SqlDbType.DateTime, this._NgayThucXuat);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(BCXuatNhapTonCollection collection, SqlTransaction transaction)
        {
            foreach (BCXuatNhapTon item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_BCXuatNhapTon_Delete";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(BCXuatNhapTonCollection collection, SqlTransaction transaction)
        {
            foreach (BCXuatNhapTon item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(BCXuatNhapTonCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BCXuatNhapTon item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public int DeleteDynamicTransaction(SqlTransaction transaction, int lanThanhLy, string MaDoanhNghiep)
        {
            string spName = "DELETE FROM t_KDT_SXXK_BCXuatNhapTon Where lanThanhLy =" + lanThanhLy + " AND MaDoanhNghiep ='"+ MaDoanhNghiep +"'";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        #endregion
    }
}