using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class HopDongXuatKhau
	{
		#region Properties.
		
		public int Id { set; get; }
		public long SoToKhai { set; get; }
		public string MaLoaiHinh { set; get; }
		public DateTime NgayDangKy { set; get; }
		public string SoHopDong { set; get; }
		public DateTime NgayKy { set; get; }
		public decimal TriGia { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HopDongXuatKhau Load(int id)
		{
			const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id", SqlDbType.Int, id);
			HopDongXuatKhau entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new HopDongXuatKhau();
				if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetInt32(reader.GetOrdinal("Id"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) entity.NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<HopDongXuatKhau> SelectCollectionAll()
		{
			List<HopDongXuatKhau> collection = new List<HopDongXuatKhau>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				HopDongXuatKhau entity = new HopDongXuatKhau();
				
				if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetInt32(reader.GetOrdinal("Id"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) entity.NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<HopDongXuatKhau> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<HopDongXuatKhau> collection = new List<HopDongXuatKhau>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				HopDongXuatKhau entity = new HopDongXuatKhau();
				
				if (!reader.IsDBNull(reader.GetOrdinal("Id"))) entity.Id = reader.GetInt32(reader.GetOrdinal("Id"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetInt64(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKy"))) entity.NgayKy = reader.GetDateTime(reader.GetOrdinal("NgayKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertHopDongXuatKhau(long soToKhai, string maLoaiHinh, DateTime ngayDangKy, string soHopDong, DateTime ngayKy, decimal triGia, string ghiChu)
		{
			HopDongXuatKhau entity = new HopDongXuatKhau();	
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayDangKy = ngayDangKy;
			entity.SoHopDong = soHopDong;
			entity.NgayKy = ngayKy;
			entity.TriGia = triGia;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@Id", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year <= 1753 ? DBNull.Value : (object) NgayKy);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				Id = (int) db.GetParameterValue(dbCommand, "@Id");
				return Id;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				Id = (int) db.GetParameterValue(dbCommand, "@Id");
				return Id;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HopDongXuatKhau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HopDongXuatKhau item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHopDongXuatKhau(int id, long soToKhai, string maLoaiHinh, DateTime ngayDangKy, string soHopDong, DateTime ngayKy, decimal triGia, string ghiChu)
		{
			HopDongXuatKhau entity = new HopDongXuatKhau();			
			entity.Id = id;
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayDangKy = ngayDangKy;
			entity.SoHopDong = soHopDong;
			entity.NgayKy = ngayKy;
			entity.TriGia = triGia;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_CTTT_HopDongXuatKhau_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Id", SqlDbType.Int, Id);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year == 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year == 1753 ? DBNull.Value : (object) NgayKy);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HopDongXuatKhau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HopDongXuatKhau item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHopDongXuatKhau(int id, long soToKhai, string maLoaiHinh, DateTime ngayDangKy, string soHopDong, DateTime ngayKy, decimal triGia, string ghiChu)
		{
			HopDongXuatKhau entity = new HopDongXuatKhau();			
			entity.Id = id;
			entity.SoToKhai = soToKhai;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.NgayDangKy = ngayDangKy;
			entity.SoHopDong = soHopDong;
			entity.NgayKy = ngayKy;
			entity.TriGia = triGia;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@Id", SqlDbType.Int, Id);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.BigInt, SoToKhai);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year == 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayKy", SqlDbType.DateTime, NgayKy.Year == 1753 ? DBNull.Value : (object) NgayKy);
			db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, TriGia);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HopDongXuatKhau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HopDongXuatKhau item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHopDongXuatKhau(int id)
		{
			HopDongXuatKhau entity = new HopDongXuatKhau();
			entity.Id = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Id", SqlDbType.Int, Id);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_CTTT_HopDongXuatKhau_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HopDongXuatKhau> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HopDongXuatKhau item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}