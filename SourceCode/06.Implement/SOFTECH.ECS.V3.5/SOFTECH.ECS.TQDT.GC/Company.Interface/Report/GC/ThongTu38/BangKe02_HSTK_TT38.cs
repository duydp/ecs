﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe02_HSTK_TT38 : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewBC02_HSTK_GC_TT117Form report;
        public HopDong HD = new HopDong();
        public bool First = false;
        public bool Last = false;
        decimal tongLuong = 0;
        public BangKe02_HSTK_TT38()
        {
            InitializeComponent();
        }

        public void BindReport(DateTime tuNgay,DateTime denNgay)
        {
            Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
            //DataSet dataSource = SP.BaoCaoBC02HSTK_GC_TT117(this.HD.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

            lblNgayKi.Text = GlobalSettings.TieudeNgay;
            this.PrintingSystem.ShowMarginsWarning = false;
            try
            {
                this.DataSource = SP.BaoCaoBC02HSTK_GC_TT38(this.HD.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI,tuNgay,denNgay);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
            

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }

            lblMathang.Text = loaiSP;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;

            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("MaHang"),
                });

            lblSTT.DataBindings.Add("Text", this.DataSource,"STT");
            lblToKhaiNhap.DataBindings.Add("Text", this.DataSource, "SoToKhai");
            lblNgayDK.DataBindings.Add("Text", this.DataSource, "NgayDangKy","{0:dd/MM/yyyy}");
            lblMaNLVT.DataBindings.Add("Text", this.DataSource, "MaHang");
            lblTenNguyenLieuVT.DataBindings.Add("Text", this.DataSource, "TenHang");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT");
            lblMaNLVT_F.DataBindings.Add("Text", this.DataSource, "MaHang");
            lblTenNguyenLieuVT_F.DataBindings.Add("Text", this.DataSource, "TenHang");
            lblDVT_F.DataBindings.Add("Text", this.DataSource, "DVT");
            lblLuonghang.DataBindings.Add("Text", this.DataSource, "SoLuong", "{0:N" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblTongLuong.DataBindings.Add("Text", this.DataSource, "TongCong", "{0:N" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, "GhiChu");

        }

        private void lblhopsoHDGC_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            //XRLabel label = (XRLabel)sender;  
            //report.txtName.Text = label.Text;
            //report.lblName.Text = label.Tag.ToString();
        }

        public void setText(XRLabel label, string text)
        {
            //label.Text = text;
        }

        private void lblTongLuong_AfterPrint(object sender, EventArgs e)
        {
            try
            {
                tongLuong += Convert.ToDecimal(lblTongLuong.Text);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //throw;
            }
        }

        private void lblTongLuong_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblTongLuong_F.Text = tongLuong.ToString("N5");
            tongLuong = 0;
        }
    }
}
