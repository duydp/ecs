﻿namespace Company.Interface
{
    partial class VNACC_ChungTuThanhToanForm_IBA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout grdHang_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ChungTuThanhToanForm_IBA));
            this.txtMaNganHangTraThay = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTenNganHangTraThay = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNamPhatHanh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtKiHieuChungTuPhatHanhHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dtNgayHetHieuLuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayBatDauHieuLuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label22 = new System.Windows.Forms.Label();
            this.txtSoDuHanMuc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienHanMucDangKi = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoThuTuHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonViSuDungHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtCoBaoVohieu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.ucMaTienTe = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtSoHieuPhatHanhHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTenDonViSuDungHanMuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.grdHang = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtCoQuanhaiQuanChiTiet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenCucHaiQuanChiTiet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label25 = new System.Windows.Forms.Label();
            this.dtThoiGianTaoDuLieu = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayDangKyToKhai = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayTaoDuLieu = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoToKhaiChiTiet = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoThuTuGiaoDich = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienTangLen = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoTienTruLui = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.txtMaLoaiHinhChiTiet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdLayPhanHoi1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoi");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdLayPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoi");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 539), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 31);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 539);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 515);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 515);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Location = new System.Drawing.Point(203, 31);
            this.grbMain.Size = new System.Drawing.Size(696, 539);
            // 
            // txtMaNganHangTraThay
            // 
            this.txtMaNganHangTraThay.Location = new System.Drawing.Point(121, 25);
            this.txtMaNganHangTraThay.Name = "txtMaNganHangTraThay";
            this.txtMaNganHangTraThay.Size = new System.Drawing.Size(100, 21);
            this.txtMaNganHangTraThay.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(16, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ngân hàng trả thay";
            // 
            // txtTenNganHangTraThay
            // 
            this.txtTenNganHangTraThay.Location = new System.Drawing.Point(227, 25);
            this.txtTenNganHangTraThay.Name = "txtTenNganHangTraThay";
            this.txtTenNganHangTraThay.Size = new System.Drawing.Size(458, 21);
            this.txtTenNganHangTraThay.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(16, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Năm phát hành";
            // 
            // txtNamPhatHanh
            // 
            this.txtNamPhatHanh.DecimalDigits = 0;
            this.txtNamPhatHanh.Location = new System.Drawing.Point(121, 100);
            this.txtNamPhatHanh.MaxLength = 4;
            this.txtNamPhatHanh.Name = "txtNamPhatHanh";
            this.txtNamPhatHanh.Size = new System.Drawing.Size(100, 21);
            this.txtNamPhatHanh.TabIndex = 4;
            this.txtNamPhatHanh.Text = "0";
            this.txtNamPhatHanh.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // txtKiHieuChungTuPhatHanhHanMuc
            // 
            this.txtKiHieuChungTuPhatHanhHanMuc.Location = new System.Drawing.Point(366, 100);
            this.txtKiHieuChungTuPhatHanhHanMuc.Name = "txtKiHieuChungTuPhatHanhHanMuc";
            this.txtKiHieuChungTuPhatHanhHanMuc.Size = new System.Drawing.Size(100, 21);
            this.txtKiHieuChungTuPhatHanhHanMuc.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(227, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(133, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "Ký hiệu chứng từ hạn mức";
            // 
            // uiTab1
            // 
            this.uiTab1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(696, 500);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2007;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dtNgayHetHieuLuc);
            this.uiTabPage1.Controls.Add(this.dtNgayBatDauHieuLuc);
            this.uiTabPage1.Controls.Add(this.label22);
            this.uiTabPage1.Controls.Add(this.txtSoDuHanMuc);
            this.uiTabPage1.Controls.Add(this.txtSoTienHanMucDangKi);
            this.uiTabPage1.Controls.Add(this.txtNamPhatHanh);
            this.uiTabPage1.Controls.Add(this.txtSoThuTuHanMuc);
            this.uiTabPage1.Controls.Add(this.txtMaDonViSuDungHanMuc);
            this.uiTabPage1.Controls.Add(this.txtCoBaoVohieu);
            this.uiTabPage1.Controls.Add(this.txtMaNganHangTraThay);
            this.uiTabPage1.Controls.Add(this.ucMaTienTe);
            this.uiTabPage1.Controls.Add(this.txtSoHieuPhatHanhHanMuc);
            this.uiTabPage1.Controls.Add(this.txtKiHieuChungTuPhatHanhHanMuc);
            this.uiTabPage1.Controls.Add(this.label5);
            this.uiTabPage1.Controls.Add(this.label4);
            this.uiTabPage1.Controls.Add(this.txtTenDonViSuDungHanMuc);
            this.uiTabPage1.Controls.Add(this.label24);
            this.uiTabPage1.Controls.Add(this.txtTenNganHangTraThay);
            this.uiTabPage1.Controls.Add(this.label3);
            this.uiTabPage1.Controls.Add(this.label20);
            this.uiTabPage1.Controls.Add(this.label1);
            this.uiTabPage1.Controls.Add(this.label19);
            this.uiTabPage1.Controls.Add(this.label23);
            this.uiTabPage1.Controls.Add(this.label17);
            this.uiTabPage1.Controls.Add(this.label21);
            this.uiTabPage1.Controls.Add(this.label2);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(694, 478);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thông tin chung";
            // 
            // dtNgayHetHieuLuc
            // 
            // 
            // 
            // 
            this.dtNgayHetHieuLuc.DropDownCalendar.Name = "";
            this.dtNgayHetHieuLuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayHetHieuLuc.Location = new System.Drawing.Point(585, 158);
            this.dtNgayHetHieuLuc.Name = "dtNgayHetHieuLuc";
            this.dtNgayHetHieuLuc.Size = new System.Drawing.Size(100, 21);
            this.dtNgayHetHieuLuc.TabIndex = 9;
            this.dtNgayHetHieuLuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtNgayBatDauHieuLuc
            // 
            // 
            // 
            // 
            this.dtNgayBatDauHieuLuc.DropDownCalendar.Name = "";
            this.dtNgayBatDauHieuLuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayBatDauHieuLuc.Location = new System.Drawing.Point(230, 158);
            this.dtNgayBatDauHieuLuc.Name = "dtNgayBatDauHieuLuc";
            this.dtNgayBatDauHieuLuc.Size = new System.Drawing.Size(130, 21);
            this.dtNgayBatDauHieuLuc.TabIndex = 8;
            this.dtNgayBatDauHieuLuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(16, 216);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(55, 13);
            this.label22.TabIndex = 1;
            this.label22.Text = "Mã tiền tệ";
            // 
            // txtSoDuHanMuc
            // 
            this.txtSoDuHanMuc.DecimalDigits = 0;
            this.txtSoDuHanMuc.Location = new System.Drawing.Point(585, 185);
            this.txtSoDuHanMuc.MaxLength = 13;
            this.txtSoDuHanMuc.Name = "txtSoDuHanMuc";
            this.txtSoDuHanMuc.Size = new System.Drawing.Size(100, 21);
            this.txtSoDuHanMuc.TabIndex = 11;
            this.txtSoDuHanMuc.Text = "0";
            this.txtSoDuHanMuc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // txtSoTienHanMucDangKi
            // 
            this.txtSoTienHanMucDangKi.DecimalDigits = 0;
            this.txtSoTienHanMucDangKi.Location = new System.Drawing.Point(227, 185);
            this.txtSoTienHanMucDangKi.MaxLength = 13;
            this.txtSoTienHanMucDangKi.Name = "txtSoTienHanMucDangKi";
            this.txtSoTienHanMucDangKi.Size = new System.Drawing.Size(133, 21);
            this.txtSoTienHanMucDangKi.TabIndex = 10;
            this.txtSoTienHanMucDangKi.Text = "0";
            this.txtSoTienHanMucDangKi.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            // 
            // txtSoThuTuHanMuc
            // 
            this.txtSoThuTuHanMuc.Location = new System.Drawing.Point(585, 212);
            this.txtSoThuTuHanMuc.Name = "txtSoThuTuHanMuc";
            this.txtSoThuTuHanMuc.Size = new System.Drawing.Size(100, 21);
            this.txtSoThuTuHanMuc.TabIndex = 13;
            // 
            // txtMaDonViSuDungHanMuc
            // 
            this.txtMaDonViSuDungHanMuc.Location = new System.Drawing.Point(121, 73);
            this.txtMaDonViSuDungHanMuc.Name = "txtMaDonViSuDungHanMuc";
            this.txtMaDonViSuDungHanMuc.Size = new System.Drawing.Size(100, 21);
            this.txtMaDonViSuDungHanMuc.TabIndex = 2;
            // 
            // txtCoBaoVohieu
            // 
            this.txtCoBaoVohieu.Location = new System.Drawing.Point(121, 127);
            this.txtCoBaoVohieu.Name = "txtCoBaoVohieu";
            this.txtCoBaoVohieu.Size = new System.Drawing.Size(44, 21);
            this.txtCoBaoVohieu.TabIndex = 7;
            // 
            // ucMaTienTe
            // 
            this.ucMaTienTe.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaTienTe.Appearance.Options.UseBackColor = true;
            this.ucMaTienTe.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.ucMaTienTe.Code = "";
            this.ucMaTienTe.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaTienTe.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaTienTe.IsValidate = true;
            this.ucMaTienTe.Location = new System.Drawing.Point(227, 209);
            this.ucMaTienTe.Name = "ucMaTienTe";
            this.ucMaTienTe.Name_VN = "";
            this.ucMaTienTe.ShowColumnCode = true;
            this.ucMaTienTe.ShowColumnName = false;
            this.ucMaTienTe.Size = new System.Drawing.Size(133, 26);
            this.ucMaTienTe.TabIndex = 12;
            this.ucMaTienTe.TagName = "";
            this.ucMaTienTe.WhereCondition = "";
            this.ucMaTienTe.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtSoHieuPhatHanhHanMuc
            // 
            this.txtSoHieuPhatHanhHanMuc.Location = new System.Drawing.Point(588, 100);
            this.txtSoHieuPhatHanhHanMuc.Name = "txtSoHieuPhatHanhHanMuc";
            this.txtSoHieuPhatHanhHanMuc.Size = new System.Drawing.Size(97, 21);
            this.txtSoHieuPhatHanhHanMuc.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(472, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Số hiệu hạn mức";
            // 
            // txtTenDonViSuDungHanMuc
            // 
            this.txtTenDonViSuDungHanMuc.Location = new System.Drawing.Point(227, 73);
            this.txtTenDonViSuDungHanMuc.Name = "txtTenDonViSuDungHanMuc";
            this.txtTenDonViSuDungHanMuc.Size = new System.Drawing.Size(458, 21);
            this.txtTenDonViSuDungHanMuc.TabIndex = 3;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(16, 131);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(64, 13);
            this.label24.TabIndex = 1;
            this.label24.Text = "Hết hiệu lực";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Location = new System.Drawing.Point(472, 163);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 1;
            this.label20.Text = "Ngày hết hiệu lực";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(472, 216);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Số thứ tự hạn mức";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(16, 163);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(112, 13);
            this.label19.TabIndex = 1;
            this.label19.Text = "Ngày bắt đầu hiệu lực";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Location = new System.Drawing.Point(472, 189);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 13);
            this.label23.TabIndex = 1;
            this.label23.Text = "Số dư hạn mức";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(16, 57);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(124, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Đơn vị sử dụng hạn mức";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Location = new System.Drawing.Point(16, 189);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(125, 13);
            this.label21.TabIndex = 1;
            this.label21.Text = "Số tiền hạn mức đăng ký";
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.grdHang);
            this.uiTabPage2.Controls.Add(this.uiGroupBox1);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(694, 478);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thông tin chi tiết";
            // 
            // grdHang
            // 
            this.grdHang.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.grdHang.CardColumnHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.ColumnAutoResize = true;
            grdHang_DesignTimeLayout.LayoutString = resources.GetString("grdHang_DesignTimeLayout.LayoutString");
            this.grdHang.DesignTimeLayout = grdHang_DesignTimeLayout;
            this.grdHang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.grdHang.GroupByBoxVisible = false;
            this.grdHang.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.Location = new System.Drawing.Point(0, 120);
            this.grdHang.Name = "grdHang";
            this.grdHang.RecordNavigator = true;
            this.grdHang.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.grdHang.RowHeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grdHang.RowHeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grdHang.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grdHang.Size = new System.Drawing.Size(694, 358);
            this.grdHang.TabIndex = 3;
            this.grdHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.grdHang.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtCoQuanhaiQuanChiTiet);
            this.uiGroupBox1.Controls.Add(this.txtTenCucHaiQuanChiTiet);
            this.uiGroupBox1.Controls.Add(this.label25);
            this.uiGroupBox1.Controls.Add(this.dtThoiGianTaoDuLieu);
            this.uiGroupBox1.Controls.Add(this.dtNgayDangKyToKhai);
            this.uiGroupBox1.Controls.Add(this.dtNgayTaoDuLieu);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhaiChiTiet);
            this.uiGroupBox1.Controls.Add(this.txtSoThuTuGiaoDich);
            this.uiGroupBox1.Controls.Add(this.txtSoTienTangLen);
            this.uiGroupBox1.Controls.Add(this.txtSoTienTruLui);
            this.uiGroupBox1.Controls.Add(this.label31);
            this.uiGroupBox1.Controls.Add(this.label29);
            this.uiGroupBox1.Controls.Add(this.label33);
            this.uiGroupBox1.Controls.Add(this.label32);
            this.uiGroupBox1.Controls.Add(this.label30);
            this.uiGroupBox1.Controls.Add(this.label28);
            this.uiGroupBox1.Controls.Add(this.txtMaLoaiHinhChiTiet);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(694, 120);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtCoQuanhaiQuanChiTiet
            // 
            this.txtCoQuanhaiQuanChiTiet.Location = new System.Drawing.Point(114, 92);
            this.txtCoQuanhaiQuanChiTiet.Name = "txtCoQuanhaiQuanChiTiet";
            this.txtCoQuanhaiQuanChiTiet.Size = new System.Drawing.Size(113, 21);
            this.txtCoQuanhaiQuanChiTiet.TabIndex = 8;
            // 
            // txtTenCucHaiQuanChiTiet
            // 
            this.txtTenCucHaiQuanChiTiet.Location = new System.Drawing.Point(233, 92);
            this.txtTenCucHaiQuanChiTiet.Name = "txtTenCucHaiQuanChiTiet";
            this.txtTenCucHaiQuanChiTiet.Size = new System.Drawing.Size(445, 21);
            this.txtTenCucHaiQuanChiTiet.TabIndex = 9;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(10, 96);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(92, 13);
            this.label25.TabIndex = 12;
            this.label25.Text = "Cơ quan Hải quan";
            // 
            // dtThoiGianTaoDuLieu
            // 
            this.dtThoiGianTaoDuLieu.CustomFormat = "hh:mm:ss";
            this.dtThoiGianTaoDuLieu.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtThoiGianTaoDuLieu.DropDownCalendar.Name = "";
            this.dtThoiGianTaoDuLieu.Location = new System.Drawing.Point(503, 11);
            this.dtThoiGianTaoDuLieu.Name = "dtThoiGianTaoDuLieu";
            this.dtThoiGianTaoDuLieu.Size = new System.Drawing.Size(74, 21);
            this.dtThoiGianTaoDuLieu.TabIndex = 2;
            // 
            // dtNgayDangKyToKhai
            // 
            // 
            // 
            // 
            this.dtNgayDangKyToKhai.DropDownCalendar.Name = "";
            this.dtNgayDangKyToKhai.Location = new System.Drawing.Point(384, 65);
            this.dtNgayDangKyToKhai.Name = "dtNgayDangKyToKhai";
            this.dtNgayDangKyToKhai.Size = new System.Drawing.Size(113, 21);
            this.dtNgayDangKyToKhai.TabIndex = 6;
            // 
            // dtNgayTaoDuLieu
            // 
            // 
            // 
            // 
            this.dtNgayTaoDuLieu.DropDownCalendar.Name = "";
            this.dtNgayTaoDuLieu.Location = new System.Drawing.Point(384, 11);
            this.dtNgayTaoDuLieu.Name = "dtNgayTaoDuLieu";
            this.dtNgayTaoDuLieu.Size = new System.Drawing.Size(113, 21);
            this.dtNgayTaoDuLieu.TabIndex = 1;
            // 
            // txtSoToKhaiChiTiet
            // 
            this.txtSoToKhaiChiTiet.DecimalDigits = 0;
            this.txtSoToKhaiChiTiet.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoToKhaiChiTiet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhaiChiTiet.Location = new System.Drawing.Point(114, 65);
            this.txtSoToKhaiChiTiet.MaxLength = 4;
            this.txtSoToKhaiChiTiet.Name = "txtSoToKhaiChiTiet";
            this.txtSoToKhaiChiTiet.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoToKhaiChiTiet.Size = new System.Drawing.Size(113, 21);
            this.txtSoToKhaiChiTiet.TabIndex = 5;
            this.txtSoToKhaiChiTiet.Text = "0";
            this.txtSoToKhaiChiTiet.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhaiChiTiet.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoToKhaiChiTiet.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoThuTuGiaoDich
            // 
            this.txtSoThuTuGiaoDich.DecimalDigits = 0;
            this.txtSoThuTuGiaoDich.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoThuTuGiaoDich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoThuTuGiaoDich.Location = new System.Drawing.Point(114, 11);
            this.txtSoThuTuGiaoDich.MaxLength = 4;
            this.txtSoThuTuGiaoDich.Name = "txtSoThuTuGiaoDich";
            this.txtSoThuTuGiaoDich.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoThuTuGiaoDich.Size = new System.Drawing.Size(52, 21);
            this.txtSoThuTuGiaoDich.TabIndex = 0;
            this.txtSoThuTuGiaoDich.Text = "0";
            this.txtSoThuTuGiaoDich.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoThuTuGiaoDich.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoThuTuGiaoDich.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienTangLen
            // 
            this.txtSoTienTangLen.DecimalDigits = 3;
            this.txtSoTienTangLen.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienTangLen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienTangLen.Location = new System.Drawing.Point(384, 38);
            this.txtSoTienTangLen.MaxLength = 13;
            this.txtSoTienTangLen.Name = "txtSoTienTangLen";
            this.txtSoTienTangLen.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienTangLen.Size = new System.Drawing.Size(113, 21);
            this.txtSoTienTangLen.TabIndex = 4;
            this.txtSoTienTangLen.Text = "0.000";
            this.txtSoTienTangLen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienTangLen.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoTienTangLen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTienTruLui
            // 
            this.txtSoTienTruLui.DecimalDigits = 3;
            this.txtSoTienTruLui.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoTienTruLui.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTienTruLui.Location = new System.Drawing.Point(114, 38);
            this.txtSoTienTruLui.MaxLength = 13;
            this.txtSoTienTruLui.Name = "txtSoTienTruLui";
            this.txtSoTienTruLui.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoTienTruLui.Size = new System.Drawing.Size(113, 21);
            this.txtSoTienTruLui.TabIndex = 3;
            this.txtSoTienTruLui.Text = "0.000";
            this.txtSoTienTruLui.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTienTruLui.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtSoTienTruLui.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(244, 43);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(82, 13);
            this.label31.TabIndex = 5;
            this.label31.Text = "Số tiền tăng lên";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(245, 16);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(86, 13);
            this.label29.TabIndex = 5;
            this.label29.Text = "Ngày tạo dữ liệu";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(244, 70);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(108, 13);
            this.label33.TabIndex = 5;
            this.label33.Text = "Ngày đăng ký tờ khai";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(10, 70);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(54, 13);
            this.label32.TabIndex = 5;
            this.label32.Text = "Số tờ khai";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(10, 43);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 13);
            this.label30.TabIndex = 5;
            this.label30.Text = "Số tiền trừ lùi";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(10, 16);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(98, 13);
            this.label28.TabIndex = 5;
            this.label28.Text = "Số thứ tự giao dịch";
            // 
            // txtMaLoaiHinhChiTiet
            // 
            this.txtMaLoaiHinhChiTiet.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaLoaiHinhChiTiet.Location = new System.Drawing.Point(572, 62);
            this.txtMaLoaiHinhChiTiet.MaxLength = 255;
            this.txtMaLoaiHinhChiTiet.Name = "txtMaLoaiHinhChiTiet";
            this.txtMaLoaiHinhChiTiet.Size = new System.Drawing.Size(106, 21);
            this.txtMaLoaiHinhChiTiet.TabIndex = 7;
            this.txtMaLoaiHinhChiTiet.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaLoaiHinhChiTiet.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaLoaiHinhChiTiet.VisualStyleManager = this.vsmMain;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(503, 67);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(63, 13);
            this.label27.TabIndex = 8;
            this.label27.Text = "Mã loại hình";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(612, 509);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 27;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdKhaiBao,
            this.cmdLayPhanHoi});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("64392c4c-08e0-49a5-ab35-32992c7e6756");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.TopRebar1;
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdKhaiBao1,
            this.cmdLayPhanHoi1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(193, 28);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // cmdLayPhanHoi1
            // 
            this.cmdLayPhanHoi1.Key = "cmdLayPhanHoi";
            this.cmdLayPhanHoi1.Name = "cmdLayPhanHoi1";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBao.Image")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdLayPhanHoi
            // 
            this.cmdLayPhanHoi.Image = ((System.Drawing.Image)(resources.GetObject("cmdLayPhanHoi.Image")));
            this.cmdLayPhanHoi.Key = "cmdLayPhanHoi";
            this.cmdLayPhanHoi.Name = "cmdLayPhanHoi";
            this.cmdLayPhanHoi.Text = "Lấy phản hồi";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmbMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(902, 28);
            // 
            // VNACC_ChungTuThanhToanForm_IBA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(902, 573);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ChungTuThanhToanForm_IBA";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Chứng từ thanh toán (IBA: Reference of Bank payment infomation)";
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage1.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdHang)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.EditControls.EditBox txtMaNganHangTraThay;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNganHangTraThay;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamPhatHanh;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHieuPhatHanhHanMuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtKiHieuChungTuPhatHanhHanMuc;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDonViSuDungHanMuc;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonViSuDungHanMuc;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayHetHieuLuc;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayBatDauHieuLuc;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienHanMucDangKi;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaTienTe;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoDuHanMuc;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.EditBox txtCoBaoVohieu;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaLoaiHinhChiTiet;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.CalendarCombo.CalendarCombo dtThoiGianTaoDuLieu;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayTaoDuLieu;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoThuTuGiaoDich;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienTruLui;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayDangKyToKhai;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoToKhaiChiTiet;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTienTangLen;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.GridEX.EditControls.EditBox txtCoQuanhaiQuanChiTiet;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCucHaiQuanChiTiet;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoThuTuHanMuc;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.GridEX grdHang;
        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayPhanHoi1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdLayPhanHoi;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
    }
}