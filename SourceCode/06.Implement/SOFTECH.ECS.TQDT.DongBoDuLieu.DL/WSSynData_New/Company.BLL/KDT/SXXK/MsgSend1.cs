﻿using System;
using System.Data;
using System.Data.SqlClient;
using Company.BLL.KDT.SXXK;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
    public partial class MsgSend : EntityBase
    {
        //---------------------------------------------------------------------------------------------

        public static long InsertTransaction(SqlTransaction transaction, SqlDatabase db, long masterId, int func, string loaiHS, string msg)
        {
            string spName = "p_KDT_SXXK_MsgSend_Insert";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, masterId);
            db.AddInParameter(dbCommand, "@func", SqlDbType.Int, func);
            db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, loaiHS);
            db.AddInParameter(dbCommand, "@msg", SqlDbType.NText, msg);

            if (transaction != null)
            {
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteTransaction(SqlTransaction transaction, SqlDatabase db, long masterId, string loaiHS)
        {
            string spName = "p_KDT_SXXK_MsgSend_Delete";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@master_id", SqlDbType.BigInt, masterId);
            db.AddInParameter(dbCommand, "@LoaiHS", SqlDbType.VarChar, loaiHS);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
    }
}
