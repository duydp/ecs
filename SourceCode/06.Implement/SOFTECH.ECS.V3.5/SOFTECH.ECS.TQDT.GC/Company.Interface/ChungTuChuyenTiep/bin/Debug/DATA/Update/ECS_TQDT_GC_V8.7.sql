

/****** Object:  StoredProcedure [dbo].[p_ViewToKhaiChuaPhanBo]    Script Date: 04/03/2013 11:56:50 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_ViewToKhaiChuaPhanBo]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_ViewToKhaiChuaPhanBo]
GO


/****** Object:  StoredProcedure [dbo].[p_ViewToKhaiChuaPhanBo]    Script Date: 04/03/2013 11:56:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Huynh Ngoc Khanh>
-- Create date: 04/02/2013
-- Description:	Chuc Nang Kiem Tra Cac To Khai Chua Phan Bo
-- =============================================
CREATE PROCEDURE [dbo].[p_ViewToKhaiChuaPhanBo] 
	-- Add the parameters for the stored procedure here
	@NamDangKy INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	-- Tờ khai GC theo hợp đồng
	SELECT ID, 0 AS IDTKCT,SoToKhai,MaLoaiHinh,MaHaiQuan,NgayDangKy,
			(case PhanLuong
			WHEN 1 THEN N'Luồng xanh'
			WHEN 2 THEN N'Luồng vàng'
			ELSE N'Luồng đỏ'
			END) AS PhanLuong  
	FROM 
	((SELECT ID,SoToKhai,MaLoaiHinh,MaHaiQuan,NgayDangKy,PhanLuong,TrangThaiXuLy,MaDoanhNghiep
	  FROM t_KDT_ToKhaiMauDich WHERE MaLoaiHinh LIKE 'N%' AND  YEAR(NgayDangKy) = @NamDangKy	AND TrangThaiXuLy = 1  ) as tkmd
	   LEFT JOIN (SELECT SoToKhai AS SoTk,MaLoaiHinh AS MLH,MaHaiQuan AS MHQ,MaDoanhNghiep AS MDN,NgayDangKy AS NDK FROM  t_GC_NPLNhapTonThucTe WHERE YEAR(NgayDangKy) = @NamDangKy) as nttt
	ON nttt.SoTK = tkmd.SoToKhai AND nttt.MLH = tkmd.MaLoaiHinh AND nttt.MHQ = tkmd.MaHaiQuan AND	nttt.MDN = tkmd.MaDoanhNghiep
	AND Year(nttt.NDK) = Year(tkmd.NgayDangKy))  
	WHERE SoTK IS NULL
   
   UNION

 --- Tờ khai gia công chuyển tiếp
  SELECT 0 as ID,IDTKCT,SoToKhai,MaLoaiHinh,MaHaiQuan,NgayDangKy,
			(case PhanLuong
			WHEN 1 THEN N'Luồng xanh'
			WHEN 2 THEN N'Luồng vàng'
			ELSE N'Luồng đỏ'
			END) AS PhanLuong  
	FROM 
	((SELECT ID as IDTKCT,SoToKhai,MaLoaiHinh,MaHaiQuanTiepNhan AS MaHaiQuan,NgayDangKy,PhanLuong ,TrangThaiXuLy,MaDoanhNghiep
	  FROM t_KDT_GC_ToKhaiChuyenTiep WHERE MaLoaiHinh LIKE 'N%' AND  YEAR(NgayDangKy) = @NamDangKy	AND TrangThaiXuLy = 1  ) as tkmd
	   LEFT JOIN (SELECT SoToKhai AS SoTk,MaLoaiHinh AS MLH,MaHaiQuan AS MHQ,MaDoanhNghiep AS MDN,NgayDangKy AS NDK FROM  t_GC_NPLNhapTonThucTe WHERE YEAR(NgayDangKy) = @NamDangKy) as nttt
	ON nttt.SoTK = tkmd.SoToKhai AND nttt.MLH = tkmd.MaLoaiHinh AND nttt.MHQ = tkmd.MaHaiQuan AND	nttt.MDN = tkmd.MaDoanhNghiep
	AND Year(nttt.NDK) = Year(tkmd.NgayDangKy))  
	WHERE SoTK IS NULL
   
   
END

GO



/****** Object:  StoredProcedure [dbo].[p_XemPhanBoToKhai]    Script Date: 04/03/2013 11:58:02 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_XemPhanBoToKhai]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_XemPhanBoToKhai]
GO


/****** Object:  StoredProcedure [dbo].[p_XemPhanBoToKhai]    Script Date: 04/03/2013 11:58:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Huynh Ngoc Khanh
-- Create date: 01/04/2013
-- Description:	Xem bang phan bo
-- =============================================
CREATE PROCEDURE [dbo].[p_XemPhanBoToKhai]
	-- Add the parameters for the stored procedure here
	@ID_TKX  BIGINT,
	@MaNPL	VARCHAR(30),
	@MaLoaiHinhXuat VARCHAR(5)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	IF(@MaLoaiHinhXuat LIKE '%18' OR @MaLoaiHinhXuat LIKE '%19' OR @MaLoaiHinhXuat LIKE '%20' OR @MaLoaiHinhXuat LIKE 'PH%')
	BEGIN
		select NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL, sum(LuongPhanBo) as LuongPhanBo
		FROM
		-- To Khai xuat GC Chuyen Tiep va To khai nhap GC Binh Thuong
(SELECT     pbtkx_1_1.MaSP, pbtkx_1_1.SoLuongXuat, pbtkx_1_1.MaDoanhNghiep, pbtkn_2.SoToKhaiNhap, pbtkn_2.MaLoaiHinhNhap, pbtkn_2.MaHaiQuanNhap, 
                      pbtkn_2.NamDangKyNhap, pbtkn_2.DinhMucChung, pbtkn_2.LuongTonDau, pbtkn_2.LuongPhanBo, pbtkn_2.LuongCungUng, pbtkn_2.LuongTonCuoi, 
                      pbtkn_2.TKXuat_ID, pbtkx_1_1.ID_TKMD, pbtkn_2.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
                      hangNhap.TenHang AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, hangNhap.DonGiaKB AS DonGiaTT, hangNhap.ThueSuatXNK AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.ID_DVT AS DVT_S, hangNhap.TyGiaTinhThue AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_2
                       WHERE    (t_GC_PhanBoToKhaiXuat_2.ID_TKMD = @ID_TKX) AND (t_GC_PhanBoToKhaiXuat_2.MaDoanhNghiep = @MaLoaiHinhXuat)) AS pbtkx_1_1 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_2
                            WHERE      (MaLoaiHinhNhap LIKE 'N%') AND t_GC_PhanBoToKhaiNhap_2.MaNPL = @MaNPL) AS pbtkn_2 ON pbtkx_1_1.ID = pbtkn_2.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangNhap ON pbtkn_2.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn_2.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
                      pbtkn_2.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn_2.MaNPL = hangNhap.MaPhu INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_1_1.ID_TKMD = hangXuat.ID AND pbtkx_1_1.MaSP = hangXuat.MaHang
UNION
-- Tờ khai Xuất GC Chuyển tiếp được cung ứng từ tờ khai nhập GC chuyển tiếp 
SELECT     pbtkx_2.MaSP, pbtkx_2.SoLuongXuat, pbtkx_2.MaDoanhNghiep, pbtkn_1_1.SoToKhaiNhap, pbtkn_1_1.MaLoaiHinhNhap, pbtkn_1_1.MaHaiQuanNhap, 
                      pbtkn_1_1.NamDangKyNhap, pbtkn_1_1.DinhMucChung, pbtkn_1_1.LuongTonDau, pbtkn_1_1.LuongPhanBo, pbtkn_1_1.LuongCungUng, 
                      pbtkn_1_1.LuongTonCuoi, pbtkn_1_1.TKXuat_ID, pbtkx_2.ID_TKMD, pbtkn_1_1.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, 
                      hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat, 
                      hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.TenHang AS TenNPL, hangNhap.ID_DVT AS DVT_NPL, hangNhap.DonGia AS DonGiaTT, 
                      0 AS ThueSuat, hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.ID_DVT AS DVT_SP, 
                      hangNhap.TyGiaVND AS TyGiaTT, hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      (t_GC_PhanBoToKhaiXuat_1.ID_TKMD = @ID_TKX) AND (t_GC_PhanBoToKhaiXuat_1.MaDoanhNghiep = @MaLoaiHinhXuat)) AS pbtkx_2 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      (MaLoaiHinhNhap LIKE 'PH%' OR MaLoaiHinhNhap LIKE '%19' OR MaLoaiHinhNhap LIKE '%18' OR MaLoaiHinhNhap LIKE '%20')AND t_GC_PhanBoToKhaiNhap_1.MaNPL = @MaNPL ) AS pbtkn_1_1 ON pbtkx_2.ID = pbtkn_1_1.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1_1.SoToKhaiNhap = hangNhap.SoToKhai AND 
                      pbtkn_1_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND pbtkn_1_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND 
                      pbtkn_1_1.MaNPL = hangNhap.MaHang INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangXuat ON pbtkx_2.ID_TKMD = hangXuat.ID AND pbtkx_2.MaSP = hangXuat.MaHang) AS A
		GROUP BY NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL                   

	END
	ELSE
	BEGIN
		select NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL, sum(LuongPhanBo) as LuongPhanBo
		FROM
		(SELECT     pbtkx_1.MaSP, pbtkx_1.SoLuongXuat, pbtkx_1.MaDoanhNghiep, pbtkn.SoToKhaiNhap, pbtkn.MaLoaiHinhNhap, pbtkn.MaHaiQuanNhap, 
                      pbtkn.NamDangKyNhap, pbtkn.DinhMucChung, pbtkn.LuongTonDau, pbtkn.LuongPhanBo, pbtkn.LuongCungUng, pbtkn.LuongTonCuoi, pbtkn.TKXuat_ID, 
                      pbtkx_1.ID_TKMD, pbtkn.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
                      hangNhap.TenHang AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, hangNhap.DonGiaKB AS DonGiaTT, hangNhap.ThueSuatXNK AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, hangNhap.TyGiaTinhThue AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat
                       WHERE      (MaDoanhNghiep LIKE 'XGC%' AND dbo.t_GC_PhanBoToKhaiXuat.ID_TKMD = @ID_TKX)) AS pbtkx_1 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap
                            WHERE      (MaLoaiHinhNhap LIKE 'N%' AND dbo.t_GC_PhanBoToKhaiNhap.MaNPL = @MaNPL)) AS pbtkn ON pbtkx_1.ID = pbtkn.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangNhap ON pbtkn.SoToKhaiNhap = hangNhap.SoToKhai AND pbtkn.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND 
                      pbtkn.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND pbtkn.MaNPL = hangNhap.MaPhu INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_1.ID_TKMD = hangXuat.TKMD_ID AND pbtkx_1.MaSP = hangXuat.MaPhu
UNION
SELECT     pbtkx.MaSP, pbtkx.SoLuongXuat, pbtkx.MaDoanhNghiep, pbtkn_1.SoToKhaiNhap, pbtkn_1.MaLoaiHinhNhap, pbtkn_1.MaHaiQuanNhap, 
                      pbtkn_1.NamDangKyNhap, pbtkn_1.DinhMucChung, pbtkn_1.LuongTonDau, pbtkn_1.LuongPhanBo, pbtkn_1.LuongCungUng, pbtkn_1.LuongTonCuoi, 
                      pbtkn_1.TKXuat_ID, pbtkx.ID_TKMD, pbtkn_1.MaNPL, hangNhap.NgayDangKy AS NgayDangKyNhap, hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, 
                      YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, hangXuat.SoToKhai AS SoToKhaiXuat, 
                      hangNhap.TenHang AS TenNPL, hangNhap.ID_DVT AS DVT_NPL, hangNhap.DonGia AS DonGiaTT, 0 AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, hangNhap.TyGiaVND AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      ((MaDoanhNghiep LIKE 'XGC%') AND ID_TKMD = @ID_TKX))  AS pbtkx INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      ((MaLoaiHinhNhap LIKE 'PH%' OR MaLoaiHinhNhap LIKE '%19' oR MaLoaiHinhNhap LIKE '%18' OR MaLoaiHinhNhap LIKE '%20') AND (MaNPL = @MaNPL))) 
                            AS pbtkn_1 ON pbtkx.ID = pbtkn_1.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangChuyenTiep AS hangNhap ON pbtkn_1.SoToKhaiNhap = hangNhap.SoToKhai AND 
                      pbtkn_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh AND pbtkn_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy) AND 
                      pbtkn_1.MaNPL = hangNhap.MaHang INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx.ID_TKMD = hangXuat.TKMD_ID AND pbtkx.MaSP = hangXuat.MaPhu
UNION
SELECT     pbtkx_3.MaSP, pbtkx_3.SoLuongXuat, pbtkx_3.MaDoanhNghiep, pbtkn_1_2.SoToKhaiNhap, pbtkn_1_2.MaLoaiHinhNhap, pbtkn_1_2.MaHaiQuanNhap, 
                      pbtkn_1_2.NamDangKyNhap, pbtkn_1_2.DinhMucChung, pbtkn_1_2.LuongTonDau, pbtkn_1_2.LuongPhanBo, pbtkn_1_2.LuongCungUng, 
                      pbtkn_1_2.LuongTonCuoi, pbtkn_1_2.TKXuat_ID, pbtkx_3.ID_TKMD, pbtkn_1_2.MaNPL, '01/01/1900' AS NgayDangKyNhap, 
                      hangXuat.MaLoaiHinh AS MaLoaiHinhXuat, YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat, hangXuat.MaHaiQuan AS MaHaiQuanXuat, 
                      hangXuat.SoToKhai AS SoToKhaiXuat, hangNhap.Ten AS TenNPL, hangNhap.DVT_ID AS DVT_NPL, 0 AS DonGiaTT, 0 AS ThueSuat, 
                      hangXuat.NgayDangKy AS NgayDangKyXuat, hangXuat.TenHang AS TenSP, hangXuat.DVT_ID AS DVT_SP, 0 AS TyGiaTT, 
                      hangXuat.NgayDangKy AS NgayThucXuat, hangXuat.IDHopDong
FROM         (SELECT     ID, ID_TKMD, MaSP, SoLuongXuat, MaDoanhNghiep
                       FROM          dbo.t_GC_PhanBoToKhaiXuat AS t_GC_PhanBoToKhaiXuat_1
                       WHERE      (MaDoanhNghiep LIKE 'XGC%')) AS pbtkx_3 INNER JOIN
                          (SELECT     ID, SoToKhaiNhap, MaLoaiHinhNhap, MaHaiQuanNhap, NamDangKyNhap, MaNPL, DinhMucChung, LuongTonDau, LuongPhanBo, 
                                                   LuongCungUng, LuongTonCuoi, TKXuat_ID, MaDoanhNghiep
                            FROM          dbo.t_GC_PhanBoToKhaiNhap AS t_GC_PhanBoToKhaiNhap_1
                            WHERE      (MaLoaiHinhNhap LIKE '')) AS pbtkn_1_2 ON pbtkx_3.ID = pbtkn_1_2.TKXuat_ID INNER JOIN
                      dbo.v_GC_HangToKhai AS hangXuat ON pbtkx_3.ID_TKMD = hangXuat.TKMD_ID AND pbtkx_3.MaSP = hangXuat.MaPhu INNER JOIN
                      dbo.t_GC_NguyenPhuLieu AS hangNhap ON pbtkn_1_2.MaNPL = hangNhap.Ma AND hangNhap.HopDong_ID = hangXuat.IDHopDong) AS B
        GROUP BY NgayDangKyNhap, SoToKhaiNhap, NamDangKyNhap, MaLoaiHinhNhap, MaNPL
		END
	
END

GO


UPDATE [dbo].[t_HaiQuan_Version]
   SET [Version] = '8.7'
      ,[Date] = getdate()
      ,[Notes] = ''
      GO
