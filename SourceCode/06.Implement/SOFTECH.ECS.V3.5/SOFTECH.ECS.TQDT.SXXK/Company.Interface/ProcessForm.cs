﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface
{
    public partial class ProcessForm : BaseForm
    {
        public ProcessBackgroundForm processBackgroundForm;
        public ProcessForm()
        {
            InitializeComponent();
        }
        public Exception ExceptionForm;
        public DateTime dateFrom;
        public DateTime dateTo;
        private void SetStatus(int valueProcess, string sts)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { lblError.Text = sts + " ..." + valueProcess + "%"; uiProgressBar1.Value = valueProcess; }));
            }
            else
            {
                lblError.Text = sts + " ..." + valueProcess + "%"; uiProgressBar1.Value = valueProcess;
            }
        }
        private void CloseFrom(bool isOK)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { this.DialogResult = isOK ? DialogResult.OK : DialogResult.No; }));
            }
            else
            {
                this.DialogResult = isOK ? DialogResult.OK : DialogResult.No;
            }
        }
        private void DoWork(object obj)
        {
            try
            {
                SetStatus(0, "Kiểm tra danh sách tờ khai ");
                processBackgroundForm.ProcessTotalExport(dateFrom.ToString("yyyy-MM-dd 00:00:00"), dateTo.ToString("yyyy-MM-dd 23:59:59"));
                CloseFrom(true);
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ExceptionForm = ex;
                CloseFrom(false);
            }
        }

        private void ProcessForm_Load(object sender, EventArgs e)
        {
            processBackgroundForm = new ProcessBackgroundForm();
            processBackgroundForm.ProcessBackgroundEventArg  += new EventHandler<ProcessBackgroundForm.ProcessBackgroundEventArgs>(Process_EventArg);
            System.Threading.ThreadPool.QueueUserWorkItem(DoWork);
        }

        void Process_EventArg(object sender, ProcessBackgroundForm.ProcessBackgroundEventArgs e)
        {
            if (e.Error == null)
            {
                SetStatus(e.Percent, e.Status);
            }
            else
            {
                ExceptionForm = e.Error;
                CloseFrom(false);

            }
        }

    }
}
