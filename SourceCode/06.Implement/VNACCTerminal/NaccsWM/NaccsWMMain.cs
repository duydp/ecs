﻿namespace NaccsWM
{
    using Naccs.Core.DataView;
    using Naccs.Core.Settings;
    using Naccs.Interactive;
    using Naccs.Net.Http;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Windows.Forms;

    public class NaccsWMMain : InteractiveMain
    {
        private IContainer components;
        private HttpSettings hts_bat;
        private HttpSettings hts_cls;
        private SystemEnvironment sys_env;
        private UDataView ucDataView;

        public NaccsWMMain() : this(false)
        {
        }

        public NaccsWMMain(bool proxyError) : base(proxyError)
        {
            this.InitializeComponent();
            if (!base.isDesigning)
            {
                base.idv = this.ucDataView;
                base.idv.OnUpdateDataViewItems += new OnUpdateDataViewItemsHandler(this.DataView_OnUpdateDataViewItems);
                base.idv.OnJobFormOpen += new OnJobFormOpenHandler(this.DataView_OnJobFormOpen);
                base.idv.OnPrintJob += new OnPrintJobHandler(this.DataVeiw_OnPrintJob);
                this.HttpSet();
                base.DemoFlag = false;
                base.ihc = new HttpClient(this.hts_cls);
                base.ato_ihc = new HttpClient(this.hts_cls);
                base.acm_ihc = new HttpClient(this.hts_cls);
                base.bat_ihc = new HttpClient(this.hts_bat);
                base.logon_ihc = new HttpClient(this.hts_cls);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void HttpSet()
        {
            this.hts_cls = new HttpSettings();
            this.hts_bat = new HttpSettings();
            this.SetMinStatus();
        }

        private void InitializeComponent()
        {
            ComponentResourceManager manager = new ComponentResourceManager(typeof(NaccsWMMain));
            this.ucDataView = new UDataView();
            base.tlcMain.ContentPanel.SuspendLayout();
            base.tlcMain.SuspendLayout();
            base.pnlDataView.SuspendLayout();
            base.pnlJobMenu.SuspendLayout();
            base.pnlJobInput.SuspendLayout();
            base.pnlSupportWindow.SuspendLayout();
            base.SuspendLayout();
            base.tlcMain.BottomToolStripPanelVisible = true;
            base.tlcMain.ContentPanel.Size = new Size(0x3f8, 0x24a);
            base.tlcMain.LeftToolStripPanelVisible = true;
            base.tlcMain.RightToolStripPanelVisible = true;
            base.tlcMain.TopToolStripPanelVisible = true;
            base.pnlDataView.Controls.Add(this.ucDataView);
            base.pnlDataView.Size = new Size(0x32d, 0x24a);
            base.pnlJobMenu.Size = new Size(200, 0x17e);
            base.pnlSupportWindow.Size = new Size(200, 0x24a);
            base.uJobMenu1.Size = new Size(200, 0x17e);
            this.ucDataView.Dock = DockStyle.Fill;
            this.ucDataView.Location = new Point(0, 0);
            this.ucDataView.Name = "ucDataView";
            this.ucDataView.Size = new Size(0x32d, 0x24a);
            this.ucDataView.TabIndex = 0;
            base.AutoScaleDimensions = new SizeF(6f, 12f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x3f8, 0x2de);
            base.Icon = (Icon) manager.GetObject("$this.Icon");
            base.Name = "NaccsWMMain";
            this.Text = "Private Terminal Software";
            base.Load += new EventHandler(this.NaccsWMMain_Load);
            base.tlcMain.ContentPanel.ResumeLayout(false);
            base.tlcMain.ResumeLayout(false);
            base.tlcMain.PerformLayout();
            base.pnlDataView.ResumeLayout(false);
            base.pnlJobMenu.ResumeLayout(false);
            base.pnlJobInput.ResumeLayout(false);
            base.pnlJobInput.PerformLayout();
            base.pnlSupportWindow.ResumeLayout(false);
            base.pnlSupportWindow.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void NaccsWMMain_Load(object sender, EventArgs e)
        {
        }

        public override void OptionSetInd()
        {
            this.SetMinStatus();
        }

        private void ServerSet(string SrvCnt)
        {
            this.hts_cls.ServerName = this.sys_env.InteractiveInfo.SelectServer(base.usrenv.InteractiveInfo.ServerConnect).ServerName;
            this.hts_cls.ServerObject = this.sys_env.InteractiveInfo.SelectServer(base.usrenv.InteractiveInfo.ServerConnect).ObjectName;
            this.hts_cls.ServerPort = this.sys_env.InteractiveInfo.SelectServer(base.usrenv.InteractiveInfo.ServerConnect).ServerPort;
            this.hts_bat.ServerName = this.sys_env.BatchdocInfo.SelectServer(base.usrenv.InteractiveInfo.ServerConnect).ServerName;
            this.hts_bat.ServerObject = this.sys_env.BatchdocInfo.SelectServer(base.usrenv.InteractiveInfo.ServerConnect).ObjectName;
            this.hts_bat.ServerPort = this.sys_env.BatchdocInfo.SelectServer(base.usrenv.InteractiveInfo.ServerConnect).ServerPort;
        }

        private void SetMinStatus()
        {
            this.sys_env = SystemEnvironment.CreateInstance();
            base.usrenv = UserEnvironment.CreateInstance();
            this.ServerSet(base.usrenv.InteractiveInfo.ServerConnect);
            if (!Directory.Exists(base.pi.LogPath))
            {
                Directory.CreateDirectory(base.pi.LogPath);
            }
            this.hts_cls.UseHttps = this.sys_env.TerminalInfo.UseInternet;
            this.hts_cls.UseProxy = base.usrenv.HttpOption.UseProxy;
            this.hts_cls.ProxyServer = base.usrenv.HttpOption.ProxyName;
            this.hts_cls.ProxyPort = base.usrenv.HttpOption.ProxyPort;
            this.hts_cls.UseProxyAccount = base.usrenv.HttpOption.UseProxyAccount;
            this.hts_cls.ProxyAccount = base.usrenv.HttpOption.ProxyAccount;
            this.hts_cls.ProxyPassword = base.usrenv.HttpOption.ProxyPassword;
            this.hts_cls.ClientCertificateHash = base.usrenv.HttpOption.CertificateHash;
            this.hts_cls.SendTimeout = this.sys_env.TerminalInfo.SendTimeOut;
            this.hts_cls.UseTrace = base.usrenv.InteractiveInfo.TraceOutput;
            this.hts_cls.TraceFile = base.pi.LogPath + this.sys_env.InteractiveInfo.TraceFileName;
            this.hts_cls.TraceSize = this.sys_env.InteractiveInfo.TraceFileSize;
            this.hts_cls.IsDebug = this.sys_env.TerminalInfo.Debug;
            this.hts_bat.UseHttps = this.sys_env.TerminalInfo.UseInternet;
            this.hts_bat.UseProxy = base.usrenv.HttpOption.UseProxy;
            this.hts_bat.ProxyServer = base.usrenv.HttpOption.ProxyName;
            this.hts_bat.ProxyPort = base.usrenv.HttpOption.ProxyPort;
            this.hts_bat.UseProxyAccount = base.usrenv.HttpOption.UseProxyAccount;
            this.hts_bat.ProxyAccount = base.usrenv.HttpOption.ProxyAccount;
            this.hts_bat.ProxyPassword = base.usrenv.HttpOption.ProxyPassword;
            this.hts_bat.ClientCertificateHash = base.usrenv.HttpOption.CertificateHash;
            this.hts_bat.SendTimeout = this.sys_env.TerminalInfo.SendTimeOut;
            this.hts_bat.UseTrace = base.usrenv.InteractiveInfo.TraceOutput;
            this.hts_bat.TraceFile = base.pi.LogPath + this.sys_env.BatchdocInfo.TraceFileName;
            this.hts_bat.TraceSize = this.sys_env.BatchdocInfo.TraceFileSize;
            this.hts_bat.IsDebug = this.sys_env.TerminalInfo.Debug;
        }

        public override void UpdateMenuItemsInd()
        {
        }
    }
}

