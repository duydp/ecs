﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAD8010 : BasicVNACC
    {

        public List<VAD8010_HANG> HangHoa { get; set; }

        //public StringBuilder BuilEdiMessagesIVA(StringBuilder StrBuild)
        //{
        //    StringBuilder str = StrBuild;
        //    str = BuildEdiMessages<VAS5050>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5050");
        //    foreach (VAS5050_HANG item in HangHoa)
        //    {
        //        item.BuildEdiMessages<VAS5050_HANG>(StrBuild, true, GlobalVNACC.PathConfig, "VAS5050_HANG");

        //    }
        //    return str;
        //}
        public void LoadVAD8010(string strResult)
        {
            try
            {
                this.GetObject<VAD8010>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD8010, false, VAD8010.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAD8010.TongSoByte);
                while (true)
                {
                    VAD8010_HANG ivaHang = new VAD8010_HANG();
                    ivaHang.GetObject<VAD8010_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAD8010_HANG", false, VAD8010_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAD8010_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAD8010_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAD8010_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAD8010_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
