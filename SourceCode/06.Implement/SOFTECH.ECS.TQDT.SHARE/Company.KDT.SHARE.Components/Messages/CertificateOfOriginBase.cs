﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class CertificateOfOriginBase : IssueBase
    {
        /// <summary>
        /// Mã loại C/O
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }
        /// <summary>
        /// Loại C/O
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }
        /// <summary>
        /// Người ký
        /// </summary>
        [XmlElement("representative")]
        public string Representative { get; set; }
        /// <summary>
        /// Ngày hết hạn C/O
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }
        /// <summary>
        /// Tên địa chỉ người xuất khẩu trên C/0
        /// </summary>
        [XmlElement("exporter")]
        public string Exporter { get; set; }
        /// <summary>
        /// Mã nước xuất khẩu trên C/0
        /// </summary>
        [XmlElement("exportationCountry")]
        public string ExportationCountry { get; set; }
        /// <summary>
        /// Tên, địa chỉ người nhập khẩu
        /// </summary>
        [XmlElement("importer")]
        public string Importer { get; set; }
        /// <summary>
        /// Mã nước nhập khẩu
        /// </summary>
        [XmlElement("importationCountry")]
        public string ImportationCountry { get; set; }
        /// <summary>
        /// Thông tin mô tả chung về hàng hóa
        /// </summary>
        [XmlElement("content")]
        public string Content { get; set; }
        /// <summary>
        /// ....
        /// </summary>
        [XmlElement("isDebt")]
        public string IsDebt { set; get; }
        /// <summary>
        /// ....
        /// </summary>
        [XmlElement("submit")]
        public string Submit { get; set; }
    }
}
