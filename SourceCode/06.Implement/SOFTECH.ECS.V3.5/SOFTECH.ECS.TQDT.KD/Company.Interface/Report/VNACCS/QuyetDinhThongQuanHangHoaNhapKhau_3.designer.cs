namespace Company.Interface.Report.VNACCS
{
    partial class QuyetDinhThongQuanHangHoaNhapKhau_3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel162 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine6 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoTienGiamThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel160 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel159 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel158 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel157 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel155 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaMienGiamThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaTinhThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenKhoanMucThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDieuKhoanMienGiamThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel149 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel148 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel147 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel146 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThueSuatThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuongTinhThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaApDungThueSuatThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel142 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel141 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel140 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienGiamThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel138 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel137 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel136 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel135 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel133 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaMienGiamThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaTinhThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenKhoanMucThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.lblDieuKhoanMienGiamThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel127 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel126 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel125 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel124 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThueSuatThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuongTinhThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaApDungThueSuatThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel120 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel119 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel118 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienGiamThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel116 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel115 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel114 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel113 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel111 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaMienGiamThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaTinhThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenKhoanMucThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.lblDieuKhoanMienGiamThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel105 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel104 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel103 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel102 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThueSuatThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuongTinhThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaApDungThueSuatThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel98 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel97 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel96 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienGiamThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel94 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel93 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel92 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel91 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel89 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaMienGiamThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaTinhThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenKhoanMucThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.lblDieuKhoanMienGiamThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel82 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel81 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel80 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel79 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThueSuatThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuongTinhThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaApDungThueSuatThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel73 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel70 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel69 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDieuKhoanMienGiamThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaMienGiamThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuongTinhThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaApDungThueSuatThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel55 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel53 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel52 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel51 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienGiamThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel39 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThueSuatThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaTinhThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenKhoanMucThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDieuKhoanMienGiam = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaMienGiamThueNK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel84 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDongTuongUngDMMienThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoDKDanhMucMienThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoThuTuDongHangToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNgoaiHanNgach = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaBieuThueNK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel78 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNoiXuatXu = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel76 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNuocXuatXu = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaXacDinhMucThueNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDonViSoLuongTrongDonGiaTinhThue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel72 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel71 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDonGiaTinhThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaTinhThueM = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel68 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDongTienCuaGiaTinhThue = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel66 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel65 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel64 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel63 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel62 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel61 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel60 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienGiamThueNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTienThueNK = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel57 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiNhapThueSuat = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThueSuatThueNhapKhau = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiThueSuat = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinhChuanDanhThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuongTinhThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaTinhThueS = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel49 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel48 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel47 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel46 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel45 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel44 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel43 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel42 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel41 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel40 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDongTienCuaDonGia = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDonViCuaDonGiaVaSoLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel37 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDonGiaHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTriGiaHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoMucKhaiKhoangDieuChinh5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoMucKhaiKhoangDieuChinh4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoMucKhaiKhoangDieuChinh3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoMucKhaiKhoangDieuChinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDonViTinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuong2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuong1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoMucKhaiKhoangDieuChinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMoTaHangHoa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiTaiXacNhanGia = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaQuanLyRieng = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSoHangHoa = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoDong = new DevExpress.XtraReports.UI.XRLabel();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.lblTongSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblBieuThiTHHetHan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhanLoaiCaNhanToChuc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaHieuPhuongThucVanChuyen = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiHangHoa = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThoiHanTaiNhapTaiXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaBoPhanXuLyToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaSoHangHoaDaiDienToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongSoToKhaiChiaNho = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoNhanhToKhaiChiaNho = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioThayDoiDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayThayDoiDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.lbl33 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGioDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenCoQuanHaiQuanTiepNhanToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaPhanLoaiKiemTra = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhaiDauTien = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhaiTamNhapTaiXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 0F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel162
            // 
            this.xrLabel162.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel162.LocationFloat = new DevExpress.Utils.PointFloat(681.7874F, 108.9278F);
            this.xrLabel162.Name = "xrLabel162";
            this.xrLabel162.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel162.SizeF = new System.Drawing.SizeF(14.60443F, 10F);
            this.xrLabel162.StylePriority.UseFont = false;
            this.xrLabel162.StylePriority.UseTextAlignment = false;
            this.xrLabel162.Text = "-";
            this.xrLabel162.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLine6
            // 
            this.xrLine6.LocationFloat = new DevExpress.Utils.PointFloat(0.9999831F, 433.132F);
            this.xrLine6.Name = "xrLine6";
            this.xrLine6.SizeF = new System.Drawing.SizeF(768F, 2F);
            // 
            // lblSoTienGiamThueVaThuKhac5
            // 
            this.lblSoTienGiamThueVaThuKhac5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienGiamThueVaThuKhac5.LocationFloat = new DevExpress.Utils.PointFloat(125.71F, 475.7938F);
            this.lblSoTienGiamThueVaThuKhac5.Name = "lblSoTienGiamThueVaThuKhac5";
            this.lblSoTienGiamThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienGiamThueVaThuKhac5.SizeF = new System.Drawing.SizeF(213.38F, 10.00002F);
            this.lblSoTienGiamThueVaThuKhac5.StylePriority.UseFont = false;
            this.lblSoTienGiamThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueVaThuKhac5.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueVaThuKhac5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel160
            // 
            this.xrLabel160.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel160.LocationFloat = new DevExpress.Utils.PointFloat(27.99999F, 475.7938F);
            this.xrLabel160.Name = "xrLabel160";
            this.xrLabel160.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel160.SizeF = new System.Drawing.SizeF(97.70999F, 10.00003F);
            this.xrLabel160.StylePriority.UseFont = false;
            this.xrLabel160.StylePriority.UseTextAlignment = false;
            this.xrLabel160.Text = "Số tiền miễn giảm";
            this.xrLabel160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel159
            // 
            this.xrLabel159.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel159.LocationFloat = new DevExpress.Utils.PointFloat(27.99999F, 465.7938F);
            this.xrLabel159.Name = "xrLabel159";
            this.xrLabel159.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel159.SizeF = new System.Drawing.SizeF(68.46002F, 10.00006F);
            this.xrLabel159.StylePriority.UseFont = false;
            this.xrLabel159.StylePriority.UseTextAlignment = false;
            this.xrLabel159.Text = "Số tiền thuế";
            this.xrLabel159.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel158
            // 
            this.xrLabel158.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel158.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 445.7936F);
            this.xrLabel158.Name = "xrLabel158";
            this.xrLabel158.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel158.SizeF = new System.Drawing.SizeF(118.1015F, 10F);
            this.xrLabel158.StylePriority.UseFont = false;
            this.xrLabel158.StylePriority.UseTextAlignment = false;
            this.xrLabel158.Text = "Số lượng tính thuế";
            this.xrLabel158.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel157
            // 
            this.xrLabel157.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel157.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 435.7936F);
            this.xrLabel157.Name = "xrLabel157";
            this.xrLabel157.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel157.SizeF = new System.Drawing.SizeF(118.1016F, 10.00003F);
            this.xrLabel157.StylePriority.UseFont = false;
            this.xrLabel157.StylePriority.UseTextAlignment = false;
            this.xrLabel157.Text = "Mã áp dụng thuế suất";
            this.xrLabel157.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTienThueVaThuKhac5
            // 
            this.lblSoTienThueVaThuKhac5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienThueVaThuKhac5.LocationFloat = new DevExpress.Utils.PointFloat(125.71F, 465.7938F);
            this.lblSoTienThueVaThuKhac5.Name = "lblSoTienThueVaThuKhac5";
            this.lblSoTienThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienThueVaThuKhac5.SizeF = new System.Drawing.SizeF(213.38F, 10.00002F);
            this.lblSoTienThueVaThuKhac5.StylePriority.UseFont = false;
            this.lblSoTienThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueVaThuKhac5.Text = "1.234.567.890.123.456";
            this.lblSoTienThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueVaThuKhac5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel155
            // 
            this.xrLabel155.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel155.LocationFloat = new DevExpress.Utils.PointFloat(27.99999F, 455.7937F);
            this.xrLabel155.Name = "xrLabel155";
            this.xrLabel155.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel155.SizeF = new System.Drawing.SizeF(62.29763F, 10.00002F);
            this.xrLabel155.StylePriority.UseFont = false;
            this.xrLabel155.StylePriority.UseTextAlignment = false;
            this.xrLabel155.Text = "Thuế suất";
            this.xrLabel155.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaMienGiamThueVaThuKhac5
            // 
            this.lblMaMienGiamThueVaThuKhac5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaMienGiamThueVaThuKhac5.LocationFloat = new DevExpress.Utils.PointFloat(452.2932F, 465.7938F);
            this.lblMaMienGiamThueVaThuKhac5.Name = "lblMaMienGiamThueVaThuKhac5";
            this.lblMaMienGiamThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaMienGiamThueVaThuKhac5.SizeF = new System.Drawing.SizeF(60.99982F, 10.00006F);
            this.lblMaMienGiamThueVaThuKhac5.StylePriority.UseFont = false;
            this.lblMaMienGiamThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblMaMienGiamThueVaThuKhac5.Text = "XXXXE";
            this.lblMaMienGiamThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDonViTinhChuanDanhThueVaThuKhac5
            // 
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.LocationFloat = new DevExpress.Utils.PointFloat(613.1843F, 445.7938F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.Name = "lblMaDonViTinhChuanDanhThueVaThuKhac5";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.SizeF = new System.Drawing.SizeF(36.62714F, 10.00006F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.StylePriority.UseFont = false;
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTriGiaTinhThueVaThuKhac5
            // 
            this.lblTriGiaTinhThueVaThuKhac5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaTinhThueVaThuKhac5.LocationFloat = new DevExpress.Utils.PointFloat(125.71F, 445.7938F);
            this.lblTriGiaTinhThueVaThuKhac5.Name = "lblTriGiaTinhThueVaThuKhac5";
            this.lblTriGiaTinhThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueVaThuKhac5.SizeF = new System.Drawing.SizeF(213.3799F, 10.00002F);
            this.lblTriGiaTinhThueVaThuKhac5.StylePriority.UseFont = false;
            this.lblTriGiaTinhThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueVaThuKhac5.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueVaThuKhac5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenKhoanMucThueVaThuKhac5
            // 
            this.lblTenKhoanMucThueVaThuKhac5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenKhoanMucThueVaThuKhac5.LocationFloat = new DevExpress.Utils.PointFloat(125.8851F, 435.7936F);
            this.lblTenKhoanMucThueVaThuKhac5.Name = "lblTenKhoanMucThueVaThuKhac5";
            this.lblTenKhoanMucThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenKhoanMucThueVaThuKhac5.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblTenKhoanMucThueVaThuKhac5.StylePriority.UseFont = false;
            this.lblTenKhoanMucThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblTenKhoanMucThueVaThuKhac5.Text = "WWWWWWWWE";
            this.lblTenKhoanMucThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDieuKhoanMienGiamThueVaThuKhac5
            // 
            this.lblDieuKhoanMienGiamThueVaThuKhac5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDieuKhoanMienGiamThueVaThuKhac5.LocationFloat = new DevExpress.Utils.PointFloat(513.293F, 465.7938F);
            this.lblDieuKhoanMienGiamThueVaThuKhac5.Multiline = true;
            this.lblDieuKhoanMienGiamThueVaThuKhac5.Name = "lblDieuKhoanMienGiamThueVaThuKhac5";
            this.lblDieuKhoanMienGiamThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDieuKhoanMienGiamThueVaThuKhac5.SizeF = new System.Drawing.SizeF(242.2935F, 25.99997F);
            this.lblDieuKhoanMienGiamThueVaThuKhac5.StylePriority.UseFont = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac5.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiamThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel149
            // 
            this.xrLabel149.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel149.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 465.7938F);
            this.xrLabel149.Name = "xrLabel149";
            this.xrLabel149.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel149.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel149.StylePriority.UseFont = false;
            this.xrLabel149.StylePriority.UseTextAlignment = false;
            this.xrLabel149.Text = "VND";
            this.xrLabel149.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel148
            // 
            this.xrLabel148.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel148.LocationFloat = new DevExpress.Utils.PointFloat(10.78992F, 435.7936F);
            this.xrLabel148.Name = "xrLabel148";
            this.xrLabel148.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel148.SizeF = new System.Drawing.SizeF(18F, 10F);
            this.xrLabel148.StylePriority.UseFont = false;
            this.xrLabel148.StylePriority.UseTextAlignment = false;
            this.xrLabel148.Text = "5";
            this.xrLabel148.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel147
            // 
            this.xrLabel147.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel147.LocationFloat = new DevExpress.Utils.PointFloat(29F, 435.7936F);
            this.xrLabel147.Name = "xrLabel147";
            this.xrLabel147.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel147.SizeF = new System.Drawing.SizeF(23.79897F, 10F);
            this.xrLabel147.StylePriority.UseFont = false;
            this.xrLabel147.StylePriority.UseTextAlignment = false;
            this.xrLabel147.Text = "Tên";
            this.xrLabel147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel146
            // 
            this.xrLabel146.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel146.LocationFloat = new DevExpress.Utils.PointFloat(27.99999F, 445.7938F);
            this.xrLabel146.Name = "xrLabel146";
            this.xrLabel146.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel146.SizeF = new System.Drawing.SizeF(97.71003F, 10F);
            this.xrLabel146.StylePriority.UseFont = false;
            this.xrLabel146.StylePriority.UseTextAlignment = false;
            this.xrLabel146.Text = "Trị giá tính thuế";
            this.xrLabel146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThueSuatThueVaThuKhac5
            // 
            this.lblThueSuatThueVaThuKhac5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblThueSuatThueVaThuKhac5.LocationFloat = new DevExpress.Utils.PointFloat(125.71F, 455.7937F);
            this.lblThueSuatThueVaThuKhac5.Name = "lblThueSuatThueVaThuKhac5";
            this.lblThueSuatThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatThueVaThuKhac5.SizeF = new System.Drawing.SizeF(213.38F, 10.00003F);
            this.lblThueSuatThueVaThuKhac5.StylePriority.UseFont = false;
            this.lblThueSuatThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblThueSuatThueVaThuKhac5.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThueSuatThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblSoLuongTinhThueVaThuKhac5
            // 
            this.lblSoLuongTinhThueVaThuKhac5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoLuongTinhThueVaThuKhac5.LocationFloat = new DevExpress.Utils.PointFloat(513.2932F, 445.7938F);
            this.lblSoLuongTinhThueVaThuKhac5.Name = "lblSoLuongTinhThueVaThuKhac5";
            this.lblSoLuongTinhThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuongTinhThueVaThuKhac5.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblSoLuongTinhThueVaThuKhac5.StylePriority.UseFont = false;
            this.lblSoLuongTinhThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTinhThueVaThuKhac5.Text = "123.456.789.012";
            this.lblSoLuongTinhThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoLuongTinhThueVaThuKhac5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaApDungThueSuatThueVaThuKhac5
            // 
            this.lblMaApDungThueSuatThueVaThuKhac5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaApDungThueSuatThueVaThuKhac5.LocationFloat = new DevExpress.Utils.PointFloat(513.2932F, 435.7936F);
            this.lblMaApDungThueSuatThueVaThuKhac5.Name = "lblMaApDungThueSuatThueVaThuKhac5";
            this.lblMaApDungThueSuatThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaApDungThueSuatThueVaThuKhac5.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblMaApDungThueSuatThueVaThuKhac5.StylePriority.UseFont = false;
            this.lblMaApDungThueSuatThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblMaApDungThueSuatThueVaThuKhac5.Text = "XXXXXXXXXE";
            this.lblMaApDungThueSuatThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel142
            // 
            this.xrLabel142.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel142.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 455.7937F);
            this.xrLabel142.Name = "xrLabel142";
            this.xrLabel142.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel142.SizeF = new System.Drawing.SizeF(232.4131F, 10.00003F);
            this.xrLabel142.StylePriority.UseFont = false;
            this.xrLabel142.StylePriority.UseTextAlignment = false;
            this.xrLabel142.Text = "Miễn / Giảm / Không chịu thuế và thu khác";
            this.xrLabel142.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel141
            // 
            this.xrLabel141.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel141.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 445.7938F);
            this.xrLabel141.Name = "xrLabel141";
            this.xrLabel141.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel141.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel141.StylePriority.UseFont = false;
            this.xrLabel141.StylePriority.UseTextAlignment = false;
            this.xrLabel141.Text = "VND";
            this.xrLabel141.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel140
            // 
            this.xrLabel140.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel140.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 475.7938F);
            this.xrLabel140.Name = "xrLabel140";
            this.xrLabel140.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel140.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel140.StylePriority.UseFont = false;
            this.xrLabel140.StylePriority.UseTextAlignment = false;
            this.xrLabel140.Text = "VND";
            this.xrLabel140.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTienGiamThueVaThuKhac4
            // 
            this.lblSoTienGiamThueVaThuKhac4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienGiamThueVaThuKhac4.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 417.132F);
            this.lblSoTienGiamThueVaThuKhac4.Name = "lblSoTienGiamThueVaThuKhac4";
            this.lblSoTienGiamThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienGiamThueVaThuKhac4.SizeF = new System.Drawing.SizeF(213.38F, 10.00002F);
            this.lblSoTienGiamThueVaThuKhac4.StylePriority.UseFont = false;
            this.lblSoTienGiamThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueVaThuKhac4.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueVaThuKhac4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel138
            // 
            this.xrLabel138.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel138.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 417.132F);
            this.xrLabel138.Name = "xrLabel138";
            this.xrLabel138.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel138.SizeF = new System.Drawing.SizeF(97.70999F, 10.00003F);
            this.xrLabel138.StylePriority.UseFont = false;
            this.xrLabel138.StylePriority.UseTextAlignment = false;
            this.xrLabel138.Text = "Số tiền miễn giảm";
            this.xrLabel138.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel137
            // 
            this.xrLabel137.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel137.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 407.1319F);
            this.xrLabel137.Name = "xrLabel137";
            this.xrLabel137.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel137.SizeF = new System.Drawing.SizeF(68.45995F, 10.00006F);
            this.xrLabel137.StylePriority.UseFont = false;
            this.xrLabel137.StylePriority.UseTextAlignment = false;
            this.xrLabel137.Text = "Số tiền thuế";
            this.xrLabel137.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel136
            // 
            this.xrLabel136.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel136.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 387.132F);
            this.xrLabel136.Name = "xrLabel136";
            this.xrLabel136.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel136.SizeF = new System.Drawing.SizeF(118.1014F, 10.00003F);
            this.xrLabel136.StylePriority.UseFont = false;
            this.xrLabel136.StylePriority.UseTextAlignment = false;
            this.xrLabel136.Text = "Số lượng tính thuế";
            this.xrLabel136.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel135
            // 
            this.xrLabel135.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel135.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 377.132F);
            this.xrLabel135.Name = "xrLabel135";
            this.xrLabel135.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel135.SizeF = new System.Drawing.SizeF(118.1014F, 10.00006F);
            this.xrLabel135.StylePriority.UseFont = false;
            this.xrLabel135.StylePriority.UseTextAlignment = false;
            this.xrLabel135.Text = "Mã áp dụng thuế suất";
            this.xrLabel135.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTienThueVaThuKhac4
            // 
            this.lblSoTienThueVaThuKhac4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienThueVaThuKhac4.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 407.1319F);
            this.lblSoTienThueVaThuKhac4.Name = "lblSoTienThueVaThuKhac4";
            this.lblSoTienThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienThueVaThuKhac4.SizeF = new System.Drawing.SizeF(213.38F, 10.00002F);
            this.lblSoTienThueVaThuKhac4.StylePriority.UseFont = false;
            this.lblSoTienThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueVaThuKhac4.Text = "1.234.567.890.123.456";
            this.lblSoTienThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueVaThuKhac4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel133
            // 
            this.xrLabel133.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel133.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 397.132F);
            this.xrLabel133.Name = "xrLabel133";
            this.xrLabel133.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel133.SizeF = new System.Drawing.SizeF(62.29763F, 10.00002F);
            this.xrLabel133.StylePriority.UseFont = false;
            this.xrLabel133.StylePriority.UseTextAlignment = false;
            this.xrLabel133.Text = "Thuế suất";
            this.xrLabel133.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaMienGiamThueVaThuKhac4
            // 
            this.lblMaMienGiamThueVaThuKhac4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaMienGiamThueVaThuKhac4.LocationFloat = new DevExpress.Utils.PointFloat(452.2932F, 407.1319F);
            this.lblMaMienGiamThueVaThuKhac4.Name = "lblMaMienGiamThueVaThuKhac4";
            this.lblMaMienGiamThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaMienGiamThueVaThuKhac4.SizeF = new System.Drawing.SizeF(60.99973F, 10F);
            this.lblMaMienGiamThueVaThuKhac4.StylePriority.UseFont = false;
            this.lblMaMienGiamThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblMaMienGiamThueVaThuKhac4.Text = "XXXXE";
            this.lblMaMienGiamThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDonViTinhChuanDanhThueVaThuKhac4
            // 
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.LocationFloat = new DevExpress.Utils.PointFloat(613.1843F, 387.132F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.Name = "lblMaDonViTinhChuanDanhThueVaThuKhac4";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.SizeF = new System.Drawing.SizeF(36.62726F, 10.00003F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.StylePriority.UseFont = false;
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTriGiaTinhThueVaThuKhac4
            // 
            this.lblTriGiaTinhThueVaThuKhac4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaTinhThueVaThuKhac4.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 387.132F);
            this.lblTriGiaTinhThueVaThuKhac4.Name = "lblTriGiaTinhThueVaThuKhac4";
            this.lblTriGiaTinhThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueVaThuKhac4.SizeF = new System.Drawing.SizeF(213.3799F, 10.00002F);
            this.lblTriGiaTinhThueVaThuKhac4.StylePriority.UseFont = false;
            this.lblTriGiaTinhThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueVaThuKhac4.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueVaThuKhac4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenKhoanMucThueVaThuKhac4
            // 
            this.lblTenKhoanMucThueVaThuKhac4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenKhoanMucThueVaThuKhac4.LocationFloat = new DevExpress.Utils.PointFloat(125.8852F, 377.1319F);
            this.lblTenKhoanMucThueVaThuKhac4.Name = "lblTenKhoanMucThueVaThuKhac4";
            this.lblTenKhoanMucThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenKhoanMucThueVaThuKhac4.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblTenKhoanMucThueVaThuKhac4.StylePriority.UseFont = false;
            this.lblTenKhoanMucThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblTenKhoanMucThueVaThuKhac4.Text = "WWWWWWWWE";
            this.lblTenKhoanMucThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine5
            // 
            this.xrLine5.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 374.7791F);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.SizeF = new System.Drawing.SizeF(768F, 2F);
            // 
            // lblDieuKhoanMienGiamThueVaThuKhac4
            // 
            this.lblDieuKhoanMienGiamThueVaThuKhac4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDieuKhoanMienGiamThueVaThuKhac4.LocationFloat = new DevExpress.Utils.PointFloat(513.2932F, 407.1319F);
            this.lblDieuKhoanMienGiamThueVaThuKhac4.Multiline = true;
            this.lblDieuKhoanMienGiamThueVaThuKhac4.Name = "lblDieuKhoanMienGiamThueVaThuKhac4";
            this.lblDieuKhoanMienGiamThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDieuKhoanMienGiamThueVaThuKhac4.SizeF = new System.Drawing.SizeF(242.2935F, 25.99997F);
            this.lblDieuKhoanMienGiamThueVaThuKhac4.StylePriority.UseFont = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac4.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiamThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel127
            // 
            this.xrLabel127.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel127.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 407.1319F);
            this.xrLabel127.Name = "xrLabel127";
            this.xrLabel127.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel127.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel127.StylePriority.UseFont = false;
            this.xrLabel127.StylePriority.UseTextAlignment = false;
            this.xrLabel127.Text = "VND";
            this.xrLabel127.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel126
            // 
            this.xrLabel126.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel126.LocationFloat = new DevExpress.Utils.PointFloat(10.00005F, 377.132F);
            this.xrLabel126.Name = "xrLabel126";
            this.xrLabel126.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel126.SizeF = new System.Drawing.SizeF(18F, 10F);
            this.xrLabel126.StylePriority.UseFont = false;
            this.xrLabel126.StylePriority.UseTextAlignment = false;
            this.xrLabel126.Text = "4";
            this.xrLabel126.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel125
            // 
            this.xrLabel125.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel125.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 377.132F);
            this.xrLabel125.Name = "xrLabel125";
            this.xrLabel125.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel125.SizeF = new System.Drawing.SizeF(23.79897F, 10F);
            this.xrLabel125.StylePriority.UseFont = false;
            this.xrLabel125.StylePriority.UseTextAlignment = false;
            this.xrLabel125.Text = "Tên";
            this.xrLabel125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel124
            // 
            this.xrLabel124.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel124.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 387.132F);
            this.xrLabel124.Name = "xrLabel124";
            this.xrLabel124.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel124.SizeF = new System.Drawing.SizeF(97.71003F, 10F);
            this.xrLabel124.StylePriority.UseFont = false;
            this.xrLabel124.StylePriority.UseTextAlignment = false;
            this.xrLabel124.Text = "Trị giá tính thuế";
            this.xrLabel124.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThueSuatThueVaThuKhac4
            // 
            this.lblThueSuatThueVaThuKhac4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblThueSuatThueVaThuKhac4.LocationFloat = new DevExpress.Utils.PointFloat(125.71F, 397.1318F);
            this.lblThueSuatThueVaThuKhac4.Name = "lblThueSuatThueVaThuKhac4";
            this.lblThueSuatThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatThueVaThuKhac4.SizeF = new System.Drawing.SizeF(213.38F, 10.00003F);
            this.lblThueSuatThueVaThuKhac4.StylePriority.UseFont = false;
            this.lblThueSuatThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblThueSuatThueVaThuKhac4.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThueSuatThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblSoLuongTinhThueVaThuKhac4
            // 
            this.lblSoLuongTinhThueVaThuKhac4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoLuongTinhThueVaThuKhac4.LocationFloat = new DevExpress.Utils.PointFloat(513.2932F, 387.1318F);
            this.lblSoLuongTinhThueVaThuKhac4.Name = "lblSoLuongTinhThueVaThuKhac4";
            this.lblSoLuongTinhThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuongTinhThueVaThuKhac4.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblSoLuongTinhThueVaThuKhac4.StylePriority.UseFont = false;
            this.lblSoLuongTinhThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTinhThueVaThuKhac4.Text = "123.456.789.012";
            this.lblSoLuongTinhThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoLuongTinhThueVaThuKhac4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaApDungThueSuatThueVaThuKhac4
            // 
            this.lblMaApDungThueSuatThueVaThuKhac4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaApDungThueSuatThueVaThuKhac4.LocationFloat = new DevExpress.Utils.PointFloat(513.2932F, 377.1319F);
            this.lblMaApDungThueSuatThueVaThuKhac4.Name = "lblMaApDungThueSuatThueVaThuKhac4";
            this.lblMaApDungThueSuatThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaApDungThueSuatThueVaThuKhac4.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblMaApDungThueSuatThueVaThuKhac4.StylePriority.UseFont = false;
            this.lblMaApDungThueSuatThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblMaApDungThueSuatThueVaThuKhac4.Text = "XXXXXXXXXE";
            this.lblMaApDungThueSuatThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel120
            // 
            this.xrLabel120.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel120.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 397.132F);
            this.xrLabel120.Name = "xrLabel120";
            this.xrLabel120.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel120.SizeF = new System.Drawing.SizeF(232.413F, 10F);
            this.xrLabel120.StylePriority.UseFont = false;
            this.xrLabel120.StylePriority.UseTextAlignment = false;
            this.xrLabel120.Text = "Miễn / Giảm / Không chịu thuế và thu khác";
            this.xrLabel120.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel119
            // 
            this.xrLabel119.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel119.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 387.132F);
            this.xrLabel119.Name = "xrLabel119";
            this.xrLabel119.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel119.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel119.StylePriority.UseFont = false;
            this.xrLabel119.StylePriority.UseTextAlignment = false;
            this.xrLabel119.Text = "VND";
            this.xrLabel119.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel118
            // 
            this.xrLabel118.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel118.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 417.132F);
            this.xrLabel118.Name = "xrLabel118";
            this.xrLabel118.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel118.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel118.StylePriority.UseFont = false;
            this.xrLabel118.StylePriority.UseTextAlignment = false;
            this.xrLabel118.Text = "VND";
            this.xrLabel118.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTienGiamThueVaThuKhac3
            // 
            this.lblSoTienGiamThueVaThuKhac3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienGiamThueVaThuKhac3.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 358.7791F);
            this.lblSoTienGiamThueVaThuKhac3.Name = "lblSoTienGiamThueVaThuKhac3";
            this.lblSoTienGiamThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienGiamThueVaThuKhac3.SizeF = new System.Drawing.SizeF(213.38F, 10.00002F);
            this.lblSoTienGiamThueVaThuKhac3.StylePriority.UseFont = false;
            this.lblSoTienGiamThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueVaThuKhac3.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueVaThuKhac3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel116
            // 
            this.xrLabel116.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel116.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 358.7791F);
            this.xrLabel116.Name = "xrLabel116";
            this.xrLabel116.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel116.SizeF = new System.Drawing.SizeF(97.70999F, 10.00003F);
            this.xrLabel116.StylePriority.UseFont = false;
            this.xrLabel116.StylePriority.UseTextAlignment = false;
            this.xrLabel116.Text = "Số tiền miễn giảm";
            this.xrLabel116.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel115
            // 
            this.xrLabel115.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel115.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 348.7791F);
            this.xrLabel115.Name = "xrLabel115";
            this.xrLabel115.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel115.SizeF = new System.Drawing.SizeF(68.45994F, 10.00003F);
            this.xrLabel115.StylePriority.UseFont = false;
            this.xrLabel115.StylePriority.UseTextAlignment = false;
            this.xrLabel115.Text = "Số tiền thuế";
            this.xrLabel115.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel114
            // 
            this.xrLabel114.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel114.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 328.7789F);
            this.xrLabel114.Name = "xrLabel114";
            this.xrLabel114.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel114.SizeF = new System.Drawing.SizeF(117.916F, 10.00003F);
            this.xrLabel114.StylePriority.UseFont = false;
            this.xrLabel114.StylePriority.UseTextAlignment = false;
            this.xrLabel114.Text = "Số lượng tính thuế";
            this.xrLabel114.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel113
            // 
            this.xrLabel113.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel113.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 318.7791F);
            this.xrLabel113.Name = "xrLabel113";
            this.xrLabel113.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel113.SizeF = new System.Drawing.SizeF(117.9159F, 10F);
            this.xrLabel113.StylePriority.UseFont = false;
            this.xrLabel113.StylePriority.UseTextAlignment = false;
            this.xrLabel113.Text = "Mã áp dụng thuế suất";
            this.xrLabel113.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTienThueVaThuKhac3
            // 
            this.lblSoTienThueVaThuKhac3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienThueVaThuKhac3.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 348.7791F);
            this.lblSoTienThueVaThuKhac3.Name = "lblSoTienThueVaThuKhac3";
            this.lblSoTienThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienThueVaThuKhac3.SizeF = new System.Drawing.SizeF(213.38F, 10.00002F);
            this.lblSoTienThueVaThuKhac3.StylePriority.UseFont = false;
            this.lblSoTienThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueVaThuKhac3.Text = "1.234.567.890.123.456";
            this.lblSoTienThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueVaThuKhac3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel111
            // 
            this.xrLabel111.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel111.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 338.779F);
            this.xrLabel111.Name = "xrLabel111";
            this.xrLabel111.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel111.SizeF = new System.Drawing.SizeF(62.29763F, 10.00002F);
            this.xrLabel111.StylePriority.UseFont = false;
            this.xrLabel111.StylePriority.UseTextAlignment = false;
            this.xrLabel111.Text = "Thuế suất";
            this.xrLabel111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaMienGiamThueVaThuKhac3
            // 
            this.lblMaMienGiamThueVaThuKhac3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaMienGiamThueVaThuKhac3.LocationFloat = new DevExpress.Utils.PointFloat(452.2932F, 348.7791F);
            this.lblMaMienGiamThueVaThuKhac3.Name = "lblMaMienGiamThueVaThuKhac3";
            this.lblMaMienGiamThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaMienGiamThueVaThuKhac3.SizeF = new System.Drawing.SizeF(60.99966F, 10.00003F);
            this.lblMaMienGiamThueVaThuKhac3.StylePriority.UseFont = false;
            this.lblMaMienGiamThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblMaMienGiamThueVaThuKhac3.Text = "XXXXE";
            this.lblMaMienGiamThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDonViTinhChuanDanhThueVaThuKhac3
            // 
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.LocationFloat = new DevExpress.Utils.PointFloat(612.9987F, 328.7791F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.Name = "lblMaDonViTinhChuanDanhThueVaThuKhac3";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.SizeF = new System.Drawing.SizeF(36.81287F, 10.00003F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.StylePriority.UseFont = false;
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTriGiaTinhThueVaThuKhac3
            // 
            this.lblTriGiaTinhThueVaThuKhac3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaTinhThueVaThuKhac3.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 328.7789F);
            this.lblTriGiaTinhThueVaThuKhac3.Name = "lblTriGiaTinhThueVaThuKhac3";
            this.lblTriGiaTinhThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueVaThuKhac3.SizeF = new System.Drawing.SizeF(213.3799F, 10.00002F);
            this.lblTriGiaTinhThueVaThuKhac3.StylePriority.UseFont = false;
            this.lblTriGiaTinhThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueVaThuKhac3.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueVaThuKhac3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenKhoanMucThueVaThuKhac3
            // 
            this.lblTenKhoanMucThueVaThuKhac3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenKhoanMucThueVaThuKhac3.LocationFloat = new DevExpress.Utils.PointFloat(125.8852F, 318.779F);
            this.lblTenKhoanMucThueVaThuKhac3.Name = "lblTenKhoanMucThueVaThuKhac3";
            this.lblTenKhoanMucThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenKhoanMucThueVaThuKhac3.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblTenKhoanMucThueVaThuKhac3.StylePriority.UseFont = false;
            this.lblTenKhoanMucThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblTenKhoanMucThueVaThuKhac3.Text = "WWWWWWWWE";
            this.lblTenKhoanMucThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine4
            // 
            this.xrLine4.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 315.9408F);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.SizeF = new System.Drawing.SizeF(768F, 2F);
            // 
            // lblDieuKhoanMienGiamThueVaThuKhac3
            // 
            this.lblDieuKhoanMienGiamThueVaThuKhac3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDieuKhoanMienGiamThueVaThuKhac3.LocationFloat = new DevExpress.Utils.PointFloat(513.2932F, 348.7791F);
            this.lblDieuKhoanMienGiamThueVaThuKhac3.Multiline = true;
            this.lblDieuKhoanMienGiamThueVaThuKhac3.Name = "lblDieuKhoanMienGiamThueVaThuKhac3";
            this.lblDieuKhoanMienGiamThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDieuKhoanMienGiamThueVaThuKhac3.SizeF = new System.Drawing.SizeF(242.2935F, 26F);
            this.lblDieuKhoanMienGiamThueVaThuKhac3.StylePriority.UseFont = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac3.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiamThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel105
            // 
            this.xrLabel105.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel105.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 348.7791F);
            this.xrLabel105.Name = "xrLabel105";
            this.xrLabel105.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel105.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel105.StylePriority.UseFont = false;
            this.xrLabel105.StylePriority.UseTextAlignment = false;
            this.xrLabel105.Text = "VND";
            this.xrLabel105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel104
            // 
            this.xrLabel104.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel104.LocationFloat = new DevExpress.Utils.PointFloat(10.00005F, 318.7791F);
            this.xrLabel104.Name = "xrLabel104";
            this.xrLabel104.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel104.SizeF = new System.Drawing.SizeF(18F, 10F);
            this.xrLabel104.StylePriority.UseFont = false;
            this.xrLabel104.StylePriority.UseTextAlignment = false;
            this.xrLabel104.Text = "3";
            this.xrLabel104.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel103
            // 
            this.xrLabel103.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel103.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 318.7791F);
            this.xrLabel103.Name = "xrLabel103";
            this.xrLabel103.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel103.SizeF = new System.Drawing.SizeF(23.79897F, 10F);
            this.xrLabel103.StylePriority.UseFont = false;
            this.xrLabel103.StylePriority.UseTextAlignment = false;
            this.xrLabel103.Text = "Tên";
            this.xrLabel103.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel102
            // 
            this.xrLabel102.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel102.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 328.7789F);
            this.xrLabel102.Name = "xrLabel102";
            this.xrLabel102.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel102.SizeF = new System.Drawing.SizeF(97.71003F, 10F);
            this.xrLabel102.StylePriority.UseFont = false;
            this.xrLabel102.StylePriority.UseTextAlignment = false;
            this.xrLabel102.Text = "Trị giá tính thuế";
            this.xrLabel102.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThueSuatThueVaThuKhac3
            // 
            this.lblThueSuatThueVaThuKhac3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblThueSuatThueVaThuKhac3.LocationFloat = new DevExpress.Utils.PointFloat(125.71F, 338.779F);
            this.lblThueSuatThueVaThuKhac3.Name = "lblThueSuatThueVaThuKhac3";
            this.lblThueSuatThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatThueVaThuKhac3.SizeF = new System.Drawing.SizeF(213.38F, 10F);
            this.lblThueSuatThueVaThuKhac3.StylePriority.UseFont = false;
            this.lblThueSuatThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblThueSuatThueVaThuKhac3.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThueSuatThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblSoLuongTinhThueVaThuKhac3
            // 
            this.lblSoLuongTinhThueVaThuKhac3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoLuongTinhThueVaThuKhac3.LocationFloat = new DevExpress.Utils.PointFloat(513.2932F, 328.7789F);
            this.lblSoLuongTinhThueVaThuKhac3.Name = "lblSoLuongTinhThueVaThuKhac3";
            this.lblSoLuongTinhThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuongTinhThueVaThuKhac3.SizeF = new System.Drawing.SizeF(99.70538F, 9.999969F);
            this.lblSoLuongTinhThueVaThuKhac3.StylePriority.UseFont = false;
            this.lblSoLuongTinhThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTinhThueVaThuKhac3.Text = "123.456.789.012";
            this.lblSoLuongTinhThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoLuongTinhThueVaThuKhac3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaApDungThueSuatThueVaThuKhac3
            // 
            this.lblMaApDungThueSuatThueVaThuKhac3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaApDungThueSuatThueVaThuKhac3.LocationFloat = new DevExpress.Utils.PointFloat(513.1075F, 318.7791F);
            this.lblMaApDungThueSuatThueVaThuKhac3.Name = "lblMaApDungThueSuatThueVaThuKhac3";
            this.lblMaApDungThueSuatThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaApDungThueSuatThueVaThuKhac3.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblMaApDungThueSuatThueVaThuKhac3.StylePriority.UseFont = false;
            this.lblMaApDungThueSuatThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblMaApDungThueSuatThueVaThuKhac3.Text = "XXXXXXXXXE";
            this.lblMaApDungThueSuatThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel98
            // 
            this.xrLabel98.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel98.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 338.779F);
            this.xrLabel98.Name = "xrLabel98";
            this.xrLabel98.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel98.SizeF = new System.Drawing.SizeF(232.4131F, 10.00003F);
            this.xrLabel98.StylePriority.UseFont = false;
            this.xrLabel98.StylePriority.UseTextAlignment = false;
            this.xrLabel98.Text = "Miễn / Giảm / Không chịu thuế và thu khác";
            this.xrLabel98.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel97
            // 
            this.xrLabel97.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel97.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 328.7789F);
            this.xrLabel97.Name = "xrLabel97";
            this.xrLabel97.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel97.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel97.StylePriority.UseFont = false;
            this.xrLabel97.StylePriority.UseTextAlignment = false;
            this.xrLabel97.Text = "VND";
            this.xrLabel97.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel96
            // 
            this.xrLabel96.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel96.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 358.7791F);
            this.xrLabel96.Name = "xrLabel96";
            this.xrLabel96.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel96.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel96.StylePriority.UseFont = false;
            this.xrLabel96.StylePriority.UseTextAlignment = false;
            this.xrLabel96.Text = "VND";
            this.xrLabel96.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTienGiamThueVaThuKhac2
            // 
            this.lblSoTienGiamThueVaThuKhac2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienGiamThueVaThuKhac2.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 299.9408F);
            this.lblSoTienGiamThueVaThuKhac2.Name = "lblSoTienGiamThueVaThuKhac2";
            this.lblSoTienGiamThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienGiamThueVaThuKhac2.SizeF = new System.Drawing.SizeF(213.38F, 10.00002F);
            this.lblSoTienGiamThueVaThuKhac2.StylePriority.UseFont = false;
            this.lblSoTienGiamThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueVaThuKhac2.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueVaThuKhac2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel94
            // 
            this.xrLabel94.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel94.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 299.9408F);
            this.xrLabel94.Name = "xrLabel94";
            this.xrLabel94.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel94.SizeF = new System.Drawing.SizeF(97.70999F, 10.00003F);
            this.xrLabel94.StylePriority.UseFont = false;
            this.xrLabel94.StylePriority.UseTextAlignment = false;
            this.xrLabel94.Text = "Số tiền miễn giảm";
            this.xrLabel94.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel93
            // 
            this.xrLabel93.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel93.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 289.9408F);
            this.xrLabel93.Name = "xrLabel93";
            this.xrLabel93.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel93.SizeF = new System.Drawing.SizeF(68.45993F, 10.00006F);
            this.xrLabel93.StylePriority.UseFont = false;
            this.xrLabel93.StylePriority.UseTextAlignment = false;
            this.xrLabel93.Text = "Số tiền thuế";
            this.xrLabel93.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel92
            // 
            this.xrLabel92.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel92.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 269.9408F);
            this.xrLabel92.Name = "xrLabel92";
            this.xrLabel92.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel92.SizeF = new System.Drawing.SizeF(117.9159F, 10.00003F);
            this.xrLabel92.StylePriority.UseFont = false;
            this.xrLabel92.StylePriority.UseTextAlignment = false;
            this.xrLabel92.Text = "Số lượng tính thuế";
            this.xrLabel92.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel91
            // 
            this.xrLabel91.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel91.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 259.9408F);
            this.xrLabel91.Name = "xrLabel91";
            this.xrLabel91.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel91.SizeF = new System.Drawing.SizeF(117.9159F, 10.00002F);
            this.xrLabel91.StylePriority.UseFont = false;
            this.xrLabel91.StylePriority.UseTextAlignment = false;
            this.xrLabel91.Text = "Mã áp dụng thuế suất";
            this.xrLabel91.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTienThueVaThuKhac2
            // 
            this.lblSoTienThueVaThuKhac2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienThueVaThuKhac2.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 289.9409F);
            this.lblSoTienThueVaThuKhac2.Name = "lblSoTienThueVaThuKhac2";
            this.lblSoTienThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienThueVaThuKhac2.SizeF = new System.Drawing.SizeF(213.38F, 10.00002F);
            this.lblSoTienThueVaThuKhac2.StylePriority.UseFont = false;
            this.lblSoTienThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueVaThuKhac2.Text = "1.234.567.890.123.456";
            this.lblSoTienThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueVaThuKhac2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel89
            // 
            this.xrLabel89.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel89.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 279.9408F);
            this.xrLabel89.Name = "xrLabel89";
            this.xrLabel89.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel89.SizeF = new System.Drawing.SizeF(62.29763F, 10.00002F);
            this.xrLabel89.StylePriority.UseFont = false;
            this.xrLabel89.StylePriority.UseTextAlignment = false;
            this.xrLabel89.Text = "Thuế suất";
            this.xrLabel89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaMienGiamThueVaThuKhac2
            // 
            this.lblMaMienGiamThueVaThuKhac2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaMienGiamThueVaThuKhac2.LocationFloat = new DevExpress.Utils.PointFloat(452.2932F, 289.9409F);
            this.lblMaMienGiamThueVaThuKhac2.Name = "lblMaMienGiamThueVaThuKhac2";
            this.lblMaMienGiamThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaMienGiamThueVaThuKhac2.SizeF = new System.Drawing.SizeF(60.81418F, 10F);
            this.lblMaMienGiamThueVaThuKhac2.StylePriority.UseFont = false;
            this.lblMaMienGiamThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblMaMienGiamThueVaThuKhac2.Text = "XXXXE";
            this.lblMaMienGiamThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDonViTinhChuanDanhThueVaThuKhac2
            // 
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.LocationFloat = new DevExpress.Utils.PointFloat(613.1843F, 269.9409F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.Name = "lblMaDonViTinhChuanDanhThueVaThuKhac2";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.SizeF = new System.Drawing.SizeF(36.62726F, 10.00003F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.StylePriority.UseFont = false;
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTriGiaTinhThueVaThuKhac2
            // 
            this.lblTriGiaTinhThueVaThuKhac2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaTinhThueVaThuKhac2.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 269.9408F);
            this.lblTriGiaTinhThueVaThuKhac2.Name = "lblTriGiaTinhThueVaThuKhac2";
            this.lblTriGiaTinhThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueVaThuKhac2.SizeF = new System.Drawing.SizeF(213.3799F, 10.00002F);
            this.lblTriGiaTinhThueVaThuKhac2.StylePriority.UseFont = false;
            this.lblTriGiaTinhThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueVaThuKhac2.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueVaThuKhac2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenKhoanMucThueVaThuKhac2
            // 
            this.lblTenKhoanMucThueVaThuKhac2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenKhoanMucThueVaThuKhac2.LocationFloat = new DevExpress.Utils.PointFloat(125.8852F, 259.9408F);
            this.lblTenKhoanMucThueVaThuKhac2.Name = "lblTenKhoanMucThueVaThuKhac2";
            this.lblTenKhoanMucThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenKhoanMucThueVaThuKhac2.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblTenKhoanMucThueVaThuKhac2.StylePriority.UseFont = false;
            this.lblTenKhoanMucThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblTenKhoanMucThueVaThuKhac2.Text = "WWWWWWWWE";
            this.lblTenKhoanMucThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLine3
            // 
            this.xrLine3.LocationFloat = new DevExpress.Utils.PointFloat(1.589457E-05F, 257.9408F);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.SizeF = new System.Drawing.SizeF(768F, 2F);
            // 
            // lblDieuKhoanMienGiamThueVaThuKhac2
            // 
            this.lblDieuKhoanMienGiamThueVaThuKhac2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDieuKhoanMienGiamThueVaThuKhac2.LocationFloat = new DevExpress.Utils.PointFloat(513.2932F, 289.9408F);
            this.lblDieuKhoanMienGiamThueVaThuKhac2.Multiline = true;
            this.lblDieuKhoanMienGiamThueVaThuKhac2.Name = "lblDieuKhoanMienGiamThueVaThuKhac2";
            this.lblDieuKhoanMienGiamThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDieuKhoanMienGiamThueVaThuKhac2.SizeF = new System.Drawing.SizeF(242.2935F, 26F);
            this.lblDieuKhoanMienGiamThueVaThuKhac2.StylePriority.UseFont = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiamThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel82
            // 
            this.xrLabel82.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel82.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 289.9409F);
            this.xrLabel82.Name = "xrLabel82";
            this.xrLabel82.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel82.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel82.StylePriority.UseFont = false;
            this.xrLabel82.StylePriority.UseTextAlignment = false;
            this.xrLabel82.Text = "VND";
            this.xrLabel82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel81
            // 
            this.xrLabel81.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel81.LocationFloat = new DevExpress.Utils.PointFloat(10.00005F, 259.9408F);
            this.xrLabel81.Name = "xrLabel81";
            this.xrLabel81.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel81.SizeF = new System.Drawing.SizeF(18F, 10F);
            this.xrLabel81.StylePriority.UseFont = false;
            this.xrLabel81.StylePriority.UseTextAlignment = false;
            this.xrLabel81.Text = "2";
            this.xrLabel81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel80
            // 
            this.xrLabel80.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel80.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 259.9408F);
            this.xrLabel80.Name = "xrLabel80";
            this.xrLabel80.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel80.SizeF = new System.Drawing.SizeF(23.79897F, 10F);
            this.xrLabel80.StylePriority.UseFont = false;
            this.xrLabel80.StylePriority.UseTextAlignment = false;
            this.xrLabel80.Text = "Tên";
            this.xrLabel80.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel79
            // 
            this.xrLabel79.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel79.LocationFloat = new DevExpress.Utils.PointFloat(28.00005F, 269.9408F);
            this.xrLabel79.Name = "xrLabel79";
            this.xrLabel79.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel79.SizeF = new System.Drawing.SizeF(97.71003F, 10F);
            this.xrLabel79.StylePriority.UseFont = false;
            this.xrLabel79.StylePriority.UseTextAlignment = false;
            this.xrLabel79.Text = "Trị giá tính thuế";
            this.xrLabel79.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThueSuatThueVaThuKhac2
            // 
            this.lblThueSuatThueVaThuKhac2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblThueSuatThueVaThuKhac2.LocationFloat = new DevExpress.Utils.PointFloat(125.71F, 279.9408F);
            this.lblThueSuatThueVaThuKhac2.Name = "lblThueSuatThueVaThuKhac2";
            this.lblThueSuatThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatThueVaThuKhac2.SizeF = new System.Drawing.SizeF(213.38F, 10.00003F);
            this.lblThueSuatThueVaThuKhac2.StylePriority.UseFont = false;
            this.lblThueSuatThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblThueSuatThueVaThuKhac2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThueSuatThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblSoLuongTinhThueVaThuKhac2
            // 
            this.lblSoLuongTinhThueVaThuKhac2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoLuongTinhThueVaThuKhac2.LocationFloat = new DevExpress.Utils.PointFloat(513.2932F, 269.9408F);
            this.lblSoLuongTinhThueVaThuKhac2.Name = "lblSoLuongTinhThueVaThuKhac2";
            this.lblSoLuongTinhThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuongTinhThueVaThuKhac2.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblSoLuongTinhThueVaThuKhac2.StylePriority.UseFont = false;
            this.lblSoLuongTinhThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTinhThueVaThuKhac2.Text = "123.456.789.012";
            this.lblSoLuongTinhThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoLuongTinhThueVaThuKhac2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaApDungThueSuatThueVaThuKhac2
            // 
            this.lblMaApDungThueSuatThueVaThuKhac2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaApDungThueSuatThueVaThuKhac2.LocationFloat = new DevExpress.Utils.PointFloat(513.2932F, 259.9408F);
            this.lblMaApDungThueSuatThueVaThuKhac2.Name = "lblMaApDungThueSuatThueVaThuKhac2";
            this.lblMaApDungThueSuatThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaApDungThueSuatThueVaThuKhac2.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblMaApDungThueSuatThueVaThuKhac2.StylePriority.UseFont = false;
            this.lblMaApDungThueSuatThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblMaApDungThueSuatThueVaThuKhac2.Text = "XXXXXXXXXE";
            this.lblMaApDungThueSuatThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel73
            // 
            this.xrLabel73.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel73.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 279.9408F);
            this.xrLabel73.Name = "xrLabel73";
            this.xrLabel73.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel73.SizeF = new System.Drawing.SizeF(232.413F, 10.00003F);
            this.xrLabel73.StylePriority.UseFont = false;
            this.xrLabel73.StylePriority.UseTextAlignment = false;
            this.xrLabel73.Text = "Miễn / Giảm / Không chịu thuế và thu khác";
            this.xrLabel73.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel70
            // 
            this.xrLabel70.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel70.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 269.9408F);
            this.xrLabel70.Name = "xrLabel70";
            this.xrLabel70.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel70.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel70.StylePriority.UseFont = false;
            this.xrLabel70.StylePriority.UseTextAlignment = false;
            this.xrLabel70.Text = "VND";
            this.xrLabel70.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel69
            // 
            this.xrLabel69.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel69.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 299.9408F);
            this.xrLabel69.Name = "xrLabel69";
            this.xrLabel69.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel69.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel69.StylePriority.UseFont = false;
            this.xrLabel69.StylePriority.UseTextAlignment = false;
            this.xrLabel69.Text = "VND";
            this.xrLabel69.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDieuKhoanMienGiamThueVaThuKhac1
            // 
            this.lblDieuKhoanMienGiamThueVaThuKhac1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDieuKhoanMienGiamThueVaThuKhac1.LocationFloat = new DevExpress.Utils.PointFloat(513.2932F, 230.9846F);
            this.lblDieuKhoanMienGiamThueVaThuKhac1.Multiline = true;
            this.lblDieuKhoanMienGiamThueVaThuKhac1.Name = "lblDieuKhoanMienGiamThueVaThuKhac1";
            this.lblDieuKhoanMienGiamThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDieuKhoanMienGiamThueVaThuKhac1.SizeF = new System.Drawing.SizeF(242.2935F, 26F);
            this.lblDieuKhoanMienGiamThueVaThuKhac1.StylePriority.UseFont = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac1.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiamThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaMienGiamThueVaThuKhac1
            // 
            this.lblMaMienGiamThueVaThuKhac1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaMienGiamThueVaThuKhac1.LocationFloat = new DevExpress.Utils.PointFloat(452.2932F, 230.9846F);
            this.lblMaMienGiamThueVaThuKhac1.Name = "lblMaMienGiamThueVaThuKhac1";
            this.lblMaMienGiamThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaMienGiamThueVaThuKhac1.SizeF = new System.Drawing.SizeF(60.99985F, 10.00002F);
            this.lblMaMienGiamThueVaThuKhac1.StylePriority.UseFont = false;
            this.lblMaMienGiamThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblMaMienGiamThueVaThuKhac1.Text = "XXXXE";
            this.lblMaMienGiamThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDonViTinhChuanDanhThueVaThuKhac1
            // 
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.LocationFloat = new DevExpress.Utils.PointFloat(612.9987F, 210.9846F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.Name = "lblMaDonViTinhChuanDanhThueVaThuKhac1";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.SizeF = new System.Drawing.SizeF(36.81287F, 10.00002F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.StylePriority.UseFont = false;
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoLuongTinhThueVaThuKhac1
            // 
            this.lblSoLuongTinhThueVaThuKhac1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoLuongTinhThueVaThuKhac1.LocationFloat = new DevExpress.Utils.PointFloat(513.1075F, 210.9846F);
            this.lblSoLuongTinhThueVaThuKhac1.Name = "lblSoLuongTinhThueVaThuKhac1";
            this.lblSoLuongTinhThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuongTinhThueVaThuKhac1.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblSoLuongTinhThueVaThuKhac1.StylePriority.UseFont = false;
            this.lblSoLuongTinhThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTinhThueVaThuKhac1.Text = "123.456.789.012";
            this.lblSoLuongTinhThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoLuongTinhThueVaThuKhac1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaApDungThueSuatThueVaThuKhac1
            // 
            this.lblMaApDungThueSuatThueVaThuKhac1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaApDungThueSuatThueVaThuKhac1.LocationFloat = new DevExpress.Utils.PointFloat(513.1075F, 200.9846F);
            this.lblMaApDungThueSuatThueVaThuKhac1.Name = "lblMaApDungThueSuatThueVaThuKhac1";
            this.lblMaApDungThueSuatThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaApDungThueSuatThueVaThuKhac1.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblMaApDungThueSuatThueVaThuKhac1.StylePriority.UseFont = false;
            this.lblMaApDungThueSuatThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblMaApDungThueSuatThueVaThuKhac1.Text = "XXXXXXXXXE";
            this.lblMaApDungThueSuatThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel55
            // 
            this.xrLabel55.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel55.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 220.9846F);
            this.xrLabel55.Name = "xrLabel55";
            this.xrLabel55.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel55.SizeF = new System.Drawing.SizeF(232.4131F, 10.00002F);
            this.xrLabel55.StylePriority.UseFont = false;
            this.xrLabel55.StylePriority.UseTextAlignment = false;
            this.xrLabel55.Text = "Miễn / Giảm / Không chịu thuế và thu khác";
            this.xrLabel55.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel53
            // 
            this.xrLabel53.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel53.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 210.9846F);
            this.xrLabel53.Name = "xrLabel53";
            this.xrLabel53.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel53.SizeF = new System.Drawing.SizeF(117.9159F, 10.00002F);
            this.xrLabel53.StylePriority.UseFont = false;
            this.xrLabel53.StylePriority.UseTextAlignment = false;
            this.xrLabel53.Text = "Số lượng tính thuế";
            this.xrLabel53.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel52
            // 
            this.xrLabel52.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel52.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 200.9846F);
            this.xrLabel52.Name = "xrLabel52";
            this.xrLabel52.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel52.SizeF = new System.Drawing.SizeF(117.9159F, 10.00002F);
            this.xrLabel52.StylePriority.UseFont = false;
            this.xrLabel52.StylePriority.UseTextAlignment = false;
            this.xrLabel52.Text = "Mã áp dụng thuế suất";
            this.xrLabel52.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel51
            // 
            this.xrLabel51.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel51.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 240.9847F);
            this.xrLabel51.Name = "xrLabel51";
            this.xrLabel51.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel51.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel51.StylePriority.UseFont = false;
            this.xrLabel51.StylePriority.UseTextAlignment = false;
            this.xrLabel51.Text = "VND";
            this.xrLabel51.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTienGiamThueVaThuKhac1
            // 
            this.lblSoTienGiamThueVaThuKhac1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienGiamThueVaThuKhac1.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 240.9847F);
            this.lblSoTienGiamThueVaThuKhac1.Name = "lblSoTienGiamThueVaThuKhac1";
            this.lblSoTienGiamThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienGiamThueVaThuKhac1.SizeF = new System.Drawing.SizeF(213.38F, 10.00002F);
            this.lblSoTienGiamThueVaThuKhac1.StylePriority.UseFont = false;
            this.lblSoTienGiamThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueVaThuKhac1.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueVaThuKhac1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel39
            // 
            this.xrLabel39.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel39.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 230.9846F);
            this.xrLabel39.Name = "xrLabel39";
            this.xrLabel39.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel39.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel39.StylePriority.UseFont = false;
            this.xrLabel39.StylePriority.UseTextAlignment = false;
            this.xrLabel39.Text = "VND";
            this.xrLabel39.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTienThueVaThuKhac1
            // 
            this.lblSoTienThueVaThuKhac1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienThueVaThuKhac1.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 230.9846F);
            this.lblSoTienThueVaThuKhac1.Name = "lblSoTienThueVaThuKhac1";
            this.lblSoTienThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienThueVaThuKhac1.SizeF = new System.Drawing.SizeF(213.38F, 10.00002F);
            this.lblSoTienThueVaThuKhac1.StylePriority.UseFont = false;
            this.lblSoTienThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueVaThuKhac1.Text = "1.234.567.890.123.456";
            this.lblSoTienThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueVaThuKhac1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblThueSuatThueVaThuKhac1
            // 
            this.lblThueSuatThueVaThuKhac1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblThueSuatThueVaThuKhac1.LocationFloat = new DevExpress.Utils.PointFloat(125.71F, 220.9846F);
            this.lblThueSuatThueVaThuKhac1.Name = "lblThueSuatThueVaThuKhac1";
            this.lblThueSuatThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatThueVaThuKhac1.SizeF = new System.Drawing.SizeF(213.38F, 10.00003F);
            this.lblThueSuatThueVaThuKhac1.StylePriority.UseFont = false;
            this.lblThueSuatThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatThueVaThuKhac1.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThueSuatThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel33.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 210.9846F);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel33.StylePriority.UseFont = false;
            this.xrLabel33.StylePriority.UseTextAlignment = false;
            this.xrLabel33.Text = "VND";
            this.xrLabel33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTriGiaTinhThueVaThuKhac1
            // 
            this.lblTriGiaTinhThueVaThuKhac1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaTinhThueVaThuKhac1.LocationFloat = new DevExpress.Utils.PointFloat(125.7101F, 210.9846F);
            this.lblTriGiaTinhThueVaThuKhac1.Name = "lblTriGiaTinhThueVaThuKhac1";
            this.lblTriGiaTinhThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueVaThuKhac1.SizeF = new System.Drawing.SizeF(213.3799F, 10.00002F);
            this.lblTriGiaTinhThueVaThuKhac1.StylePriority.UseFont = false;
            this.lblTriGiaTinhThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueVaThuKhac1.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueVaThuKhac1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTenKhoanMucThueVaThuKhac1
            // 
            this.lblTenKhoanMucThueVaThuKhac1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenKhoanMucThueVaThuKhac1.LocationFloat = new DevExpress.Utils.PointFloat(125.8852F, 200.9846F);
            this.lblTenKhoanMucThueVaThuKhac1.Name = "lblTenKhoanMucThueVaThuKhac1";
            this.lblTenKhoanMucThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenKhoanMucThueVaThuKhac1.SizeF = new System.Drawing.SizeF(99.89117F, 10.00002F);
            this.lblTenKhoanMucThueVaThuKhac1.StylePriority.UseFont = false;
            this.lblTenKhoanMucThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblTenKhoanMucThueVaThuKhac1.Text = "WWWWWWWWE";
            this.lblTenKhoanMucThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel27.LocationFloat = new DevExpress.Utils.PointFloat(28.00002F, 240.9846F);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.SizeF = new System.Drawing.SizeF(97.70999F, 10.00003F);
            this.xrLabel27.StylePriority.UseFont = false;
            this.xrLabel27.StylePriority.UseTextAlignment = false;
            this.xrLabel27.Text = "Số tiền miễn giảm";
            this.xrLabel27.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel26
            // 
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel26.LocationFloat = new DevExpress.Utils.PointFloat(28.00002F, 230.9846F);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.SizeF = new System.Drawing.SizeF(68.45997F, 10.00002F);
            this.xrLabel26.StylePriority.UseFont = false;
            this.xrLabel26.StylePriority.UseTextAlignment = false;
            this.xrLabel26.Text = "Số tiền thuế";
            this.xrLabel26.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel22.LocationFloat = new DevExpress.Utils.PointFloat(28.00002F, 220.9846F);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.SizeF = new System.Drawing.SizeF(62.29763F, 10.00002F);
            this.xrLabel22.StylePriority.UseFont = false;
            this.xrLabel22.StylePriority.UseTextAlignment = false;
            this.xrLabel22.Text = "Thuế suất";
            this.xrLabel22.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel18.LocationFloat = new DevExpress.Utils.PointFloat(28.00002F, 210.9846F);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.SizeF = new System.Drawing.SizeF(97.71003F, 10F);
            this.xrLabel18.StylePriority.UseFont = false;
            this.xrLabel18.StylePriority.UseTextAlignment = false;
            this.xrLabel18.Text = "Trị giá tính thuế";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel13.LocationFloat = new DevExpress.Utils.PointFloat(28.00002F, 200.9846F);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.SizeF = new System.Drawing.SizeF(23.79897F, 10F);
            this.xrLabel13.StylePriority.UseFont = false;
            this.xrLabel13.StylePriority.UseTextAlignment = false;
            this.xrLabel13.Text = "Tên";
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel11.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 200.9846F);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.SizeF = new System.Drawing.SizeF(18F, 10F);
            this.xrLabel11.StylePriority.UseFont = false;
            this.xrLabel11.StylePriority.UseTextAlignment = false;
            this.xrLabel11.Text = "1";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel8.LocationFloat = new DevExpress.Utils.PointFloat(350.5652F, 98.92778F);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel8.StylePriority.UseFont = false;
            this.xrLabel8.StylePriority.UseTextAlignment = false;
            this.xrLabel8.Text = "VND";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDieuKhoanMienGiam
            // 
            this.lblDieuKhoanMienGiam.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDieuKhoanMienGiam.LocationFloat = new DevExpress.Utils.PointFloat(114.46F, 178.928F);
            this.lblDieuKhoanMienGiam.Name = "lblDieuKhoanMienGiam";
            this.lblDieuKhoanMienGiam.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDieuKhoanMienGiam.SizeF = new System.Drawing.SizeF(476.1287F, 10.00002F);
            this.lblDieuKhoanMienGiam.StylePriority.UseFont = false;
            this.lblDieuKhoanMienGiam.StylePriority.UseTextAlignment = false;
            this.lblDieuKhoanMienGiam.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiam.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaMienGiamThueNK
            // 
            this.lblMaMienGiamThueNK.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaMienGiamThueNK.LocationFloat = new DevExpress.Utils.PointFloat(64.68519F, 178.9279F);
            this.lblMaMienGiamThueNK.Name = "lblMaMienGiamThueNK";
            this.lblMaMienGiamThueNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaMienGiamThueNK.SizeF = new System.Drawing.SizeF(49.77483F, 10.00003F);
            this.lblMaMienGiamThueNK.StylePriority.UseFont = false;
            this.lblMaMienGiamThueNK.StylePriority.UseTextAlignment = false;
            this.lblMaMienGiamThueNK.Text = "XXXXE";
            this.lblMaMienGiamThueNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel84
            // 
            this.xrLabel84.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel84.LocationFloat = new DevExpress.Utils.PointFloat(339.7751F, 158.9279F);
            this.xrLabel84.Name = "xrLabel84";
            this.xrLabel84.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel84.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel84.StylePriority.UseFont = false;
            this.xrLabel84.StylePriority.UseTextAlignment = false;
            this.xrLabel84.Text = "-";
            this.xrLabel84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoDongTuongUngDMMienThue
            // 
            this.lblSoDongTuongUngDMMienThue.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoDongTuongUngDMMienThue.LocationFloat = new DevExpress.Utils.PointFloat(354.5652F, 158.9279F);
            this.lblSoDongTuongUngDMMienThue.Name = "lblSoDongTuongUngDMMienThue";
            this.lblSoDongTuongUngDMMienThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDongTuongUngDMMienThue.SizeF = new System.Drawing.SizeF(31.77481F, 10.00002F);
            this.lblSoDongTuongUngDMMienThue.StylePriority.UseFont = false;
            this.lblSoDongTuongUngDMMienThue.StylePriority.UseTextAlignment = false;
            this.lblSoDongTuongUngDMMienThue.Text = "XXE";
            this.lblSoDongTuongUngDMMienThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoDKDanhMucMienThue
            // 
            this.lblSoDKDanhMucMienThue.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoDKDanhMucMienThue.LocationFloat = new DevExpress.Utils.PointFloat(232.3512F, 158.9279F);
            this.lblSoDKDanhMucMienThue.Name = "lblSoDKDanhMucMienThue";
            this.lblSoDKDanhMucMienThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDKDanhMucMienThue.SizeF = new System.Drawing.SizeF(106.7388F, 10F);
            this.lblSoDKDanhMucMienThue.StylePriority.UseFont = false;
            this.lblSoDKDanhMucMienThue.StylePriority.UseTextAlignment = false;
            this.lblSoDKDanhMucMienThue.Text = "NNNNNNNNN1NE";
            this.lblSoDKDanhMucMienThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDKDanhMucMienThue.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblSoThuTuDongHangToKhai
            // 
            this.lblSoThuTuDongHangToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoThuTuDongHangToKhai.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 148.9278F);
            this.lblSoThuTuDongHangToKhai.Name = "lblSoThuTuDongHangToKhai";
            this.lblSoThuTuDongHangToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoThuTuDongHangToKhai.SizeF = new System.Drawing.SizeF(27.32587F, 10F);
            this.lblSoThuTuDongHangToKhai.StylePriority.UseFont = false;
            this.lblSoThuTuDongHangToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoThuTuDongHangToKhai.Text = "XE";
            this.lblSoThuTuDongHangToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaNgoaiHanNgach
            // 
            this.lblMaNgoaiHanNgach.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaNgoaiHanNgach.LocationFloat = new DevExpress.Utils.PointFloat(547.4948F, 138.9277F);
            this.lblMaNgoaiHanNgach.Name = "lblMaNgoaiHanNgach";
            this.lblMaNgoaiHanNgach.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNgoaiHanNgach.SizeF = new System.Drawing.SizeF(35.58765F, 10F);
            this.lblMaNgoaiHanNgach.StylePriority.UseFont = false;
            this.lblMaNgoaiHanNgach.StylePriority.UseTextAlignment = false;
            this.lblMaNgoaiHanNgach.Text = "X";
            this.lblMaNgoaiHanNgach.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaBieuThueNK
            // 
            this.lblMaBieuThueNK.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaBieuThueNK.LocationFloat = new DevExpress.Utils.PointFloat(627.6047F, 128.9279F);
            this.lblMaBieuThueNK.Name = "lblMaBieuThueNK";
            this.lblMaBieuThueNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaBieuThueNK.SizeF = new System.Drawing.SizeF(35.58765F, 10F);
            this.lblMaBieuThueNK.StylePriority.UseFont = false;
            this.lblMaBieuThueNK.StylePriority.UseTextAlignment = false;
            this.lblMaBieuThueNK.Text = "XXE";
            this.lblMaBieuThueNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel78
            // 
            this.xrLabel78.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel78.LocationFloat = new DevExpress.Utils.PointFloat(613.0001F, 128.9279F);
            this.xrLabel78.Name = "xrLabel78";
            this.xrLabel78.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel78.SizeF = new System.Drawing.SizeF(14.60443F, 10F);
            this.xrLabel78.StylePriority.UseFont = false;
            this.xrLabel78.StylePriority.UseTextAlignment = false;
            this.xrLabel78.Text = "-";
            this.xrLabel78.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTenNoiXuatXu
            // 
            this.lblTenNoiXuatXu.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenNoiXuatXu.LocationFloat = new DevExpress.Utils.PointFloat(547.4948F, 128.9279F);
            this.lblTenNoiXuatXu.Name = "lblTenNoiXuatXu";
            this.lblTenNoiXuatXu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNoiXuatXu.SizeF = new System.Drawing.SizeF(65.50391F, 9.999962F);
            this.lblTenNoiXuatXu.StylePriority.UseFont = false;
            this.lblTenNoiXuatXu.StylePriority.UseTextAlignment = false;
            this.lblTenNoiXuatXu.Text = "XXXXXXE";
            this.lblTenNoiXuatXu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel76
            // 
            this.xrLabel76.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel76.LocationFloat = new DevExpress.Utils.PointFloat(532.7049F, 128.9277F);
            this.xrLabel76.Name = "xrLabel76";
            this.xrLabel76.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel76.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel76.StylePriority.UseFont = false;
            this.xrLabel76.StylePriority.UseTextAlignment = false;
            this.xrLabel76.Text = "-";
            this.xrLabel76.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaNuocXuatXu
            // 
            this.lblMaNuocXuatXu.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaNuocXuatXu.LocationFloat = new DevExpress.Utils.PointFloat(497.1155F, 128.9278F);
            this.lblMaNuocXuatXu.Name = "lblMaNuocXuatXu";
            this.lblMaNuocXuatXu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaNuocXuatXu.SizeF = new System.Drawing.SizeF(35.58765F, 10F);
            this.lblMaNuocXuatXu.StylePriority.UseFont = false;
            this.lblMaNuocXuatXu.StylePriority.UseTextAlignment = false;
            this.lblMaNuocXuatXu.Text = "XE";
            this.lblMaNuocXuatXu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaXacDinhMucThueNhapKhau
            // 
            this.lblMaXacDinhMucThueNhapKhau.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaXacDinhMucThueNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(532.7049F, 118.9278F);
            this.lblMaXacDinhMucThueNhapKhau.Name = "lblMaXacDinhMucThueNhapKhau";
            this.lblMaXacDinhMucThueNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaXacDinhMucThueNhapKhau.SizeF = new System.Drawing.SizeF(102.5023F, 9.999977F);
            this.lblMaXacDinhMucThueNhapKhau.StylePriority.UseFont = false;
            this.lblMaXacDinhMucThueNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblMaXacDinhMucThueNhapKhau.Text = "XXXXXXXXXE";
            this.lblMaXacDinhMucThueNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDonViSoLuongTrongDonGiaTinhThue
            // 
            this.lblDonViSoLuongTrongDonGiaTinhThue.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDonViSoLuongTrongDonGiaTinhThue.LocationFloat = new DevExpress.Utils.PointFloat(697.3918F, 108.9278F);
            this.lblDonViSoLuongTrongDonGiaTinhThue.Name = "lblDonViSoLuongTrongDonGiaTinhThue";
            this.lblDonViSoLuongTrongDonGiaTinhThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonViSoLuongTrongDonGiaTinhThue.SizeF = new System.Drawing.SizeF(44.19806F, 9.999969F);
            this.lblDonViSoLuongTrongDonGiaTinhThue.StylePriority.UseFont = false;
            this.lblDonViSoLuongTrongDonGiaTinhThue.StylePriority.UseTextAlignment = false;
            this.lblDonViSoLuongTrongDonGiaTinhThue.Text = "XXXE";
            this.lblDonViSoLuongTrongDonGiaTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel72
            // 
            this.xrLabel72.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel72.LocationFloat = new DevExpress.Utils.PointFloat(649.8115F, 108.9278F);
            this.xrLabel72.Name = "xrLabel72";
            this.xrLabel72.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel72.SizeF = new System.Drawing.SizeF(31.65161F, 9.999992F);
            this.xrLabel72.StylePriority.UseFont = false;
            this.xrLabel72.StylePriority.UseTextAlignment = false;
            this.xrLabel72.Text = "VND";
            this.xrLabel72.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel71
            // 
            this.xrLabel71.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel71.LocationFloat = new DevExpress.Utils.PointFloat(635.2072F, 108.9278F);
            this.xrLabel71.Name = "xrLabel71";
            this.xrLabel71.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel71.SizeF = new System.Drawing.SizeF(14.60443F, 10F);
            this.xrLabel71.StylePriority.UseFont = false;
            this.xrLabel71.StylePriority.UseTextAlignment = false;
            this.xrLabel71.Text = "-";
            this.xrLabel71.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblDonGiaTinhThue
            // 
            this.lblDonGiaTinhThue.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDonGiaTinhThue.LocationFloat = new DevExpress.Utils.PointFloat(497.1155F, 108.9278F);
            this.lblDonGiaTinhThue.Name = "lblDonGiaTinhThue";
            this.lblDonGiaTinhThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonGiaTinhThue.SizeF = new System.Drawing.SizeF(138.0917F, 10F);
            this.lblDonGiaTinhThue.StylePriority.UseFont = false;
            this.lblDonGiaTinhThue.StylePriority.UseTextAlignment = false;
            this.lblDonGiaTinhThue.Text = "12.345.678.901.234.567";
            this.lblDonGiaTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblDonGiaTinhThue.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaTinhThueM
            // 
            this.lblTriGiaTinhThueM.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaTinhThueM.LocationFloat = new DevExpress.Utils.PointFloat(547.4949F, 98.92782F);
            this.lblTriGiaTinhThueM.Name = "lblTriGiaTinhThueM";
            this.lblTriGiaTinhThueM.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueM.SizeF = new System.Drawing.SizeF(138.0917F, 10F);
            this.lblTriGiaTinhThueM.StylePriority.UseFont = false;
            this.lblTriGiaTinhThueM.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueM.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaTinhThueM.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueM.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel68
            // 
            this.xrLabel68.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel68.LocationFloat = new DevExpress.Utils.PointFloat(532.7049F, 98.92778F);
            this.xrLabel68.Name = "xrLabel68";
            this.xrLabel68.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel68.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel68.StylePriority.UseFont = false;
            this.xrLabel68.StylePriority.UseTextAlignment = false;
            this.xrLabel68.Text = "-";
            this.xrLabel68.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaDongTienCuaGiaTinhThue
            // 
            this.lblMaDongTienCuaGiaTinhThue.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDongTienCuaGiaTinhThue.LocationFloat = new DevExpress.Utils.PointFloat(502.7031F, 98.92778F);
            this.lblMaDongTienCuaGiaTinhThue.Name = "lblMaDongTienCuaGiaTinhThue";
            this.lblMaDongTienCuaGiaTinhThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDongTienCuaGiaTinhThue.SizeF = new System.Drawing.SizeF(30F, 10F);
            this.lblMaDongTienCuaGiaTinhThue.StylePriority.UseFont = false;
            this.lblMaDongTienCuaGiaTinhThue.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienCuaGiaTinhThue.Text = "XXE";
            this.lblMaDongTienCuaGiaTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrLabel66
            // 
            this.xrLabel66.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel66.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 138.9279F);
            this.xrLabel66.Name = "xrLabel66";
            this.xrLabel66.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel66.SizeF = new System.Drawing.SizeF(113.3551F, 10.00002F);
            this.xrLabel66.StylePriority.UseFont = false;
            this.xrLabel66.StylePriority.UseTextAlignment = false;
            this.xrLabel66.Text = "Mã ngoài hạng ngạch";
            this.xrLabel66.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel65
            // 
            this.xrLabel65.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel65.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 128.9278F);
            this.xrLabel65.Name = "xrLabel65";
            this.xrLabel65.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel65.SizeF = new System.Drawing.SizeF(101.9239F, 10F);
            this.xrLabel65.StylePriority.UseFont = false;
            this.xrLabel65.StylePriority.UseTextAlignment = false;
            this.xrLabel65.Text = "Nước xuất xứ";
            this.xrLabel65.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel64
            // 
            this.xrLabel64.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel64.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 118.9278F);
            this.xrLabel64.Name = "xrLabel64";
            this.xrLabel64.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel64.SizeF = new System.Drawing.SizeF(137.5115F, 10F);
            this.xrLabel64.StylePriority.UseFont = false;
            this.xrLabel64.StylePriority.UseTextAlignment = false;
            this.xrLabel64.Text = "Mã áp dụng thuế tuyệt đối";
            this.xrLabel64.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel63
            // 
            this.xrLabel63.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel63.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 108.9278F);
            this.xrLabel63.Name = "xrLabel63";
            this.xrLabel63.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel63.SizeF = new System.Drawing.SizeF(101.9239F, 10F);
            this.xrLabel63.StylePriority.UseFont = false;
            this.xrLabel63.StylePriority.UseTextAlignment = false;
            this.xrLabel63.Text = "Đơn giá tính thuế";
            this.xrLabel63.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel62
            // 
            this.xrLabel62.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel62.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 98.92782F);
            this.xrLabel62.Name = "xrLabel62";
            this.xrLabel62.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel62.SizeF = new System.Drawing.SizeF(107.5115F, 10F);
            this.xrLabel62.StylePriority.UseFont = false;
            this.xrLabel62.StylePriority.UseTextAlignment = false;
            this.xrLabel62.Text = "Trị giá tính thuế(M)";
            this.xrLabel62.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel61
            // 
            this.xrLabel61.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel61.LocationFloat = new DevExpress.Utils.PointFloat(350.5651F, 138.9279F);
            this.xrLabel61.Name = "xrLabel61";
            this.xrLabel61.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel61.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel61.StylePriority.UseFont = false;
            this.xrLabel61.StylePriority.UseTextAlignment = false;
            this.xrLabel61.Text = "VND";
            this.xrLabel61.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel60
            // 
            this.xrLabel60.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel60.LocationFloat = new DevExpress.Utils.PointFloat(350.5652F, 128.9278F);
            this.xrLabel60.Name = "xrLabel60";
            this.xrLabel60.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel60.SizeF = new System.Drawing.SizeF(35.77487F, 10F);
            this.xrLabel60.StylePriority.UseFont = false;
            this.xrLabel60.StylePriority.UseTextAlignment = false;
            this.xrLabel60.Text = "VND";
            this.xrLabel60.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoTienGiamThueNK
            // 
            this.lblSoTienGiamThueNK.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienGiamThueNK.LocationFloat = new DevExpress.Utils.PointFloat(136.0863F, 138.9279F);
            this.lblSoTienGiamThueNK.Name = "lblSoTienGiamThueNK";
            this.lblSoTienGiamThueNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienGiamThueNK.SizeF = new System.Drawing.SizeF(203.0037F, 10F);
            this.lblSoTienGiamThueNK.StylePriority.UseFont = false;
            this.lblSoTienGiamThueNK.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueNK.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueNK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblSoTienThueNK
            // 
            this.lblSoTienThueNK.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTienThueNK.LocationFloat = new DevExpress.Utils.PointFloat(136.0863F, 128.9278F);
            this.lblSoTienThueNK.Name = "lblSoTienThueNK";
            this.lblSoTienThueNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTienThueNK.SizeF = new System.Drawing.SizeF(203.0037F, 10F);
            this.lblSoTienThueNK.StylePriority.UseFont = false;
            this.lblSoTienThueNK.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueNK.Text = "1.234.567.890.123.456";
            this.lblSoTienThueNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueNK.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel57
            // 
            this.xrLabel57.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel57.LocationFloat = new DevExpress.Utils.PointFloat(353.55F, 118.9278F);
            this.xrLabel57.Name = "xrLabel57";
            this.xrLabel57.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel57.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel57.StylePriority.UseFont = false;
            this.xrLabel57.StylePriority.UseTextAlignment = false;
            this.xrLabel57.Text = "-";
            this.xrLabel57.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblPhanLoaiNhapThueSuat
            // 
            this.lblPhanLoaiNhapThueSuat.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiNhapThueSuat.LocationFloat = new DevExpress.Utils.PointFloat(368.34F, 118.9278F);
            this.lblPhanLoaiNhapThueSuat.Name = "lblPhanLoaiNhapThueSuat";
            this.lblPhanLoaiNhapThueSuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiNhapThueSuat.SizeF = new System.Drawing.SizeF(18F, 10F);
            this.lblPhanLoaiNhapThueSuat.StylePriority.UseFont = false;
            this.lblPhanLoaiNhapThueSuat.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiNhapThueSuat.Text = "X";
            this.lblPhanLoaiNhapThueSuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThueSuatThueNhapKhau
            // 
            this.lblThueSuatThueNhapKhau.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblThueSuatThueNhapKhau.LocationFloat = new DevExpress.Utils.PointFloat(114.46F, 118.9279F);
            this.lblThueSuatThueNhapKhau.Name = "lblThueSuatThueNhapKhau";
            this.lblThueSuatThueNhapKhau.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThueSuatThueNhapKhau.SizeF = new System.Drawing.SizeF(239F, 10F);
            this.lblThueSuatThueNhapKhau.StylePriority.UseFont = false;
            this.lblThueSuatThueNhapKhau.StylePriority.UseTextAlignment = false;
            this.lblThueSuatThueNhapKhau.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblThueSuatThueNhapKhau.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblThueSuatThueNhapKhau.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lblThueSuatThueNhapKhau_BeforePrint);
            // 
            // lblMaPhanLoaiThueSuat
            // 
            this.lblMaPhanLoaiThueSuat.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaPhanLoaiThueSuat.LocationFloat = new DevExpress.Utils.PointFloat(96.46001F, 118.9278F);
            this.lblMaPhanLoaiThueSuat.Name = "lblMaPhanLoaiThueSuat";
            this.lblMaPhanLoaiThueSuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiThueSuat.SizeF = new System.Drawing.SizeF(18F, 10F);
            this.lblMaPhanLoaiThueSuat.StylePriority.UseFont = false;
            this.lblMaPhanLoaiThueSuat.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiThueSuat.Text = "X";
            this.lblMaPhanLoaiThueSuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDonViTinhChuanDanhThue
            // 
            this.lblMaDonViTinhChuanDanhThue.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinhChuanDanhThue.LocationFloat = new DevExpress.Utils.PointFloat(350.5652F, 108.9278F);
            this.lblMaDonViTinhChuanDanhThue.Name = "lblMaDonViTinhChuanDanhThue";
            this.lblMaDonViTinhChuanDanhThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinhChuanDanhThue.SizeF = new System.Drawing.SizeF(35.77484F, 9.999992F);
            this.lblMaDonViTinhChuanDanhThue.StylePriority.UseFont = false;
            this.lblMaDonViTinhChuanDanhThue.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinhChuanDanhThue.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoLuongTinhThue
            // 
            this.lblSoLuongTinhThue.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoLuongTinhThue.LocationFloat = new DevExpress.Utils.PointFloat(136.0863F, 108.9278F);
            this.lblSoLuongTinhThue.Name = "lblSoLuongTinhThue";
            this.lblSoLuongTinhThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuongTinhThue.SizeF = new System.Drawing.SizeF(203.0037F, 10F);
            this.lblSoLuongTinhThue.StylePriority.UseFont = false;
            this.lblSoLuongTinhThue.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTinhThue.Text = "123.456.789.012";
            this.lblSoLuongTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuongTinhThue.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblTriGiaTinhThueS
            // 
            this.lblTriGiaTinhThueS.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaTinhThueS.LocationFloat = new DevExpress.Utils.PointFloat(136.0863F, 98.92782F);
            this.lblTriGiaTinhThueS.Name = "lblTriGiaTinhThueS";
            this.lblTriGiaTinhThueS.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueS.SizeF = new System.Drawing.SizeF(203.0037F, 10F);
            this.lblTriGiaTinhThueS.StylePriority.UseFont = false;
            this.lblTriGiaTinhThueS.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueS.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueS.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel49
            // 
            this.xrLabel49.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel49.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 188.9846F);
            this.xrLabel49.Name = "xrLabel49";
            this.xrLabel49.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel49.SizeF = new System.Drawing.SizeF(101.9239F, 10.00002F);
            this.xrLabel49.StylePriority.UseFont = false;
            this.xrLabel49.StylePriority.UseTextAlignment = false;
            this.xrLabel49.Text = "Thuế và thu khác";
            this.xrLabel49.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel48
            // 
            this.xrLabel48.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel48.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 168.9279F);
            this.xrLabel48.Name = "xrLabel48";
            this.xrLabel48.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel48.SizeF = new System.Drawing.SizeF(215.7763F, 10.00002F);
            this.xrLabel48.StylePriority.UseFont = false;
            this.xrLabel48.StylePriority.UseTextAlignment = false;
            this.xrLabel48.Text = "Miễn / Giảm / Không chịu thuế nhập khẩu";
            this.xrLabel48.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel47
            // 
            this.xrLabel47.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel47.LocationFloat = new DevExpress.Utils.PointFloat(34.1624F, 158.9279F);
            this.xrLabel47.Name = "xrLabel47";
            this.xrLabel47.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel47.SizeF = new System.Drawing.SizeF(190.4243F, 10.00003F);
            this.xrLabel47.StylePriority.UseFont = false;
            this.xrLabel47.StylePriority.UseTextAlignment = false;
            this.xrLabel47.Text = "Danh mục miễn thuế nhập khẩu";
            this.xrLabel47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel46
            // 
            this.xrLabel46.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel46.LocationFloat = new DevExpress.Utils.PointFloat(34.1624F, 148.9278F);
            this.xrLabel46.Name = "xrLabel46";
            this.xrLabel46.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel46.SizeF = new System.Drawing.SizeF(352.1775F, 10.00002F);
            this.xrLabel46.StylePriority.UseFont = false;
            this.xrLabel46.StylePriority.UseTextAlignment = false;
            this.xrLabel46.Text = "Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng";
            this.xrLabel46.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel45
            // 
            this.xrLabel45.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel45.LocationFloat = new DevExpress.Utils.PointFloat(34.1624F, 138.9279F);
            this.xrLabel45.Name = "xrLabel45";
            this.xrLabel45.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel45.SizeF = new System.Drawing.SizeF(101.9239F, 10.00002F);
            this.xrLabel45.StylePriority.UseFont = false;
            this.xrLabel45.StylePriority.UseTextAlignment = false;
            this.xrLabel45.Text = "Số tiền miễn giảm";
            this.xrLabel45.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel44
            // 
            this.xrLabel44.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel44.LocationFloat = new DevExpress.Utils.PointFloat(34.1624F, 128.9278F);
            this.xrLabel44.Name = "xrLabel44";
            this.xrLabel44.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel44.SizeF = new System.Drawing.SizeF(80.29758F, 10.00002F);
            this.xrLabel44.StylePriority.UseFont = false;
            this.xrLabel44.StylePriority.UseTextAlignment = false;
            this.xrLabel44.Text = "Số tiền thuế";
            this.xrLabel44.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel43
            // 
            this.xrLabel43.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel43.LocationFloat = new DevExpress.Utils.PointFloat(34.1624F, 118.9278F);
            this.xrLabel43.Name = "xrLabel43";
            this.xrLabel43.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel43.SizeF = new System.Drawing.SizeF(62.29763F, 10.00002F);
            this.xrLabel43.StylePriority.UseFont = false;
            this.xrLabel43.StylePriority.UseTextAlignment = false;
            this.xrLabel43.Text = "Thuế suất";
            this.xrLabel43.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel42
            // 
            this.xrLabel42.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel42.LocationFloat = new DevExpress.Utils.PointFloat(34.1624F, 108.9278F);
            this.xrLabel42.Name = "xrLabel42";
            this.xrLabel42.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel42.SizeF = new System.Drawing.SizeF(101.9239F, 10F);
            this.xrLabel42.StylePriority.UseFont = false;
            this.xrLabel42.StylePriority.UseTextAlignment = false;
            this.xrLabel42.Text = "Số lượng tính thuế";
            this.xrLabel42.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel41
            // 
            this.xrLabel41.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel41.LocationFloat = new DevExpress.Utils.PointFloat(34.1624F, 98.92778F);
            this.xrLabel41.Name = "xrLabel41";
            this.xrLabel41.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel41.SizeF = new System.Drawing.SizeF(101.9239F, 10F);
            this.xrLabel41.StylePriority.UseFont = false;
            this.xrLabel41.StylePriority.UseTextAlignment = false;
            this.xrLabel41.Text = "Trị giá tính thuế(S)";
            this.xrLabel41.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel40
            // 
            this.xrLabel40.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel40.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 88.92781F);
            this.xrLabel40.Name = "xrLabel40";
            this.xrLabel40.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel40.SizeF = new System.Drawing.SizeF(86.46F, 10F);
            this.xrLabel40.StylePriority.UseFont = false;
            this.xrLabel40.StylePriority.UseTextAlignment = false;
            this.xrLabel40.Text = "Thuể nhập khẩu";
            this.xrLabel40.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaDongTienCuaDonGia
            // 
            this.lblMaDongTienCuaDonGia.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDongTienCuaDonGia.LocationFloat = new DevExpress.Utils.PointFloat(585.3715F, 78.92783F);
            this.lblMaDongTienCuaDonGia.Name = "lblMaDongTienCuaDonGia";
            this.lblMaDongTienCuaDonGia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDongTienCuaDonGia.SizeF = new System.Drawing.SizeF(27.62714F, 10F);
            this.lblMaDongTienCuaDonGia.StylePriority.UseFont = false;
            this.lblMaDongTienCuaDonGia.StylePriority.UseTextAlignment = false;
            this.lblMaDongTienCuaDonGia.Text = "XXE";
            this.lblMaDongTienCuaDonGia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblDonViCuaDonGiaVaSoLuong
            // 
            this.lblDonViCuaDonGiaVaSoLuong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDonViCuaDonGiaVaSoLuong.LocationFloat = new DevExpress.Utils.PointFloat(630.6047F, 78.9278F);
            this.lblDonViCuaDonGiaVaSoLuong.Name = "lblDonViCuaDonGiaVaSoLuong";
            this.lblDonViCuaDonGiaVaSoLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonViCuaDonGiaVaSoLuong.SizeF = new System.Drawing.SizeF(36.9762F, 9.999992F);
            this.lblDonViCuaDonGiaVaSoLuong.StylePriority.UseFont = false;
            this.lblDonViCuaDonGiaVaSoLuong.StylePriority.UseTextAlignment = false;
            this.lblDonViCuaDonGiaVaSoLuong.Text = "XXXE";
            this.lblDonViCuaDonGiaVaSoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel37
            // 
            this.xrLabel37.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel37.LocationFloat = new DevExpress.Utils.PointFloat(613.8148F, 78.9278F);
            this.xrLabel37.Name = "xrLabel37";
            this.xrLabel37.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel37.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel37.StylePriority.UseFont = false;
            this.xrLabel37.StylePriority.UseTextAlignment = false;
            this.xrLabel37.Text = "-";
            this.xrLabel37.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblDonGiaHoaDon
            // 
            this.lblDonGiaHoaDon.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblDonGiaHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(493.7567F, 78.9278F);
            this.lblDonGiaHoaDon.Name = "lblDonGiaHoaDon";
            this.lblDonGiaHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonGiaHoaDon.SizeF = new System.Drawing.SizeF(75.73904F, 10.00001F);
            this.lblDonGiaHoaDon.StylePriority.UseFont = false;
            this.lblDonGiaHoaDon.StylePriority.UseTextAlignment = false;
            this.lblDonGiaHoaDon.Text = "123.456.789";
            this.lblDonGiaHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblDonGiaHoaDon.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel34
            // 
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel34.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 78.9278F);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.SizeF = new System.Drawing.SizeF(98.56512F, 10F);
            this.xrLabel34.StylePriority.UseFont = false;
            this.xrLabel34.StylePriority.UseTextAlignment = false;
            this.xrLabel34.Text = "Đơn giá hóa đơn";
            this.xrLabel34.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTriGiaHoaDon
            // 
            this.lblTriGiaHoaDon.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTriGiaHoaDon.LocationFloat = new DevExpress.Utils.PointFloat(96.45996F, 78.92783F);
            this.lblTriGiaHoaDon.Name = "lblTriGiaHoaDon";
            this.lblTriGiaHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaHoaDon.SizeF = new System.Drawing.SizeF(171.4788F, 9.999992F);
            this.lblTriGiaHoaDon.StylePriority.UseFont = false;
            this.lblTriGiaHoaDon.StylePriority.UseTextAlignment = false;
            this.lblTriGiaHoaDon.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaHoaDon.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel30.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 78.92783F);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.SizeF = new System.Drawing.SizeF(86.46F, 10F);
            this.xrLabel30.StylePriority.UseFont = false;
            this.xrLabel30.StylePriority.UseTextAlignment = false;
            this.xrLabel30.Text = "Trị giá hóa đơn";
            this.xrLabel30.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoMucKhaiKhoangDieuChinh5
            // 
            this.lblSoMucKhaiKhoangDieuChinh5.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoMucKhaiKhoangDieuChinh5.LocationFloat = new DevExpress.Utils.PointFloat(266.9388F, 68.92783F);
            this.lblSoMucKhaiKhoangDieuChinh5.Name = "lblSoMucKhaiKhoangDieuChinh5";
            this.lblSoMucKhaiKhoangDieuChinh5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoMucKhaiKhoangDieuChinh5.SizeF = new System.Drawing.SizeF(24.25F, 10F);
            this.lblSoMucKhaiKhoangDieuChinh5.StylePriority.UseFont = false;
            this.lblSoMucKhaiKhoangDieuChinh5.StylePriority.UseTextAlignment = false;
            this.lblSoMucKhaiKhoangDieuChinh5.Text = "N";
            this.lblSoMucKhaiKhoangDieuChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoMucKhaiKhoangDieuChinh5.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblSoMucKhaiKhoangDieuChinh4
            // 
            this.lblSoMucKhaiKhoangDieuChinh4.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoMucKhaiKhoangDieuChinh4.LocationFloat = new DevExpress.Utils.PointFloat(248.9388F, 68.92786F);
            this.lblSoMucKhaiKhoangDieuChinh4.Name = "lblSoMucKhaiKhoangDieuChinh4";
            this.lblSoMucKhaiKhoangDieuChinh4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoMucKhaiKhoangDieuChinh4.SizeF = new System.Drawing.SizeF(18F, 10F);
            this.lblSoMucKhaiKhoangDieuChinh4.StylePriority.UseFont = false;
            this.lblSoMucKhaiKhoangDieuChinh4.StylePriority.UseTextAlignment = false;
            this.lblSoMucKhaiKhoangDieuChinh4.Text = "N";
            this.lblSoMucKhaiKhoangDieuChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoMucKhaiKhoangDieuChinh4.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblSoMucKhaiKhoangDieuChinh3
            // 
            this.lblSoMucKhaiKhoangDieuChinh3.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoMucKhaiKhoangDieuChinh3.LocationFloat = new DevExpress.Utils.PointFloat(230.9388F, 68.92783F);
            this.lblSoMucKhaiKhoangDieuChinh3.Name = "lblSoMucKhaiKhoangDieuChinh3";
            this.lblSoMucKhaiKhoangDieuChinh3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoMucKhaiKhoangDieuChinh3.SizeF = new System.Drawing.SizeF(18F, 10F);
            this.lblSoMucKhaiKhoangDieuChinh3.StylePriority.UseFont = false;
            this.lblSoMucKhaiKhoangDieuChinh3.StylePriority.UseTextAlignment = false;
            this.lblSoMucKhaiKhoangDieuChinh3.Text = "N";
            this.lblSoMucKhaiKhoangDieuChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoMucKhaiKhoangDieuChinh3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblSoMucKhaiKhoangDieuChinh2
            // 
            this.lblSoMucKhaiKhoangDieuChinh2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoMucKhaiKhoangDieuChinh2.LocationFloat = new DevExpress.Utils.PointFloat(212.085F, 68.92786F);
            this.lblSoMucKhaiKhoangDieuChinh2.Name = "lblSoMucKhaiKhoangDieuChinh2";
            this.lblSoMucKhaiKhoangDieuChinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoMucKhaiKhoangDieuChinh2.SizeF = new System.Drawing.SizeF(18F, 10F);
            this.lblSoMucKhaiKhoangDieuChinh2.StylePriority.UseFont = false;
            this.lblSoMucKhaiKhoangDieuChinh2.StylePriority.UseTextAlignment = false;
            this.lblSoMucKhaiKhoangDieuChinh2.Text = "N";
            this.lblSoMucKhaiKhoangDieuChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoMucKhaiKhoangDieuChinh2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblMaDonViTinh2
            // 
            this.lblMaDonViTinh2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinh2.LocationFloat = new DevExpress.Utils.PointFloat(613.0001F, 68.92783F);
            this.lblMaDonViTinh2.Name = "lblMaDonViTinh2";
            this.lblMaDonViTinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinh2.SizeF = new System.Drawing.SizeF(52.58069F, 9.999989F);
            this.lblMaDonViTinh2.StylePriority.UseFont = false;
            this.lblMaDonViTinh2.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinh2.Text = "XXXE";
            this.lblMaDonViTinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblMaDonViTinh1
            // 
            this.lblMaDonViTinh1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaDonViTinh1.LocationFloat = new DevExpress.Utils.PointFloat(613.0001F, 58.92776F);
            this.lblMaDonViTinh1.Name = "lblMaDonViTinh1";
            this.lblMaDonViTinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaDonViTinh1.SizeF = new System.Drawing.SizeF(52.58069F, 10F);
            this.lblMaDonViTinh1.StylePriority.UseFont = false;
            this.lblMaDonViTinh1.StylePriority.UseTextAlignment = false;
            this.lblMaDonViTinh1.Text = "XXXE";
            this.lblMaDonViTinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblSoLuong2
            // 
            this.lblSoLuong2.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoLuong2.LocationFloat = new DevExpress.Utils.PointFloat(482.3284F, 68.92783F);
            this.lblSoLuong2.Name = "lblSoLuong2";
            this.lblSoLuong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuong2.SizeF = new System.Drawing.SizeF(130.6703F, 9.999989F);
            this.lblSoLuong2.StylePriority.UseFont = false;
            this.lblSoLuong2.StylePriority.UseTextAlignment = false;
            this.lblSoLuong2.Text = "123.456.789.012";
            this.lblSoLuong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuong2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblSoLuong1
            // 
            this.lblSoLuong1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoLuong1.LocationFloat = new DevExpress.Utils.PointFloat(482.3284F, 58.92776F);
            this.lblSoLuong1.Name = "lblSoLuong1";
            this.lblSoLuong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuong1.SizeF = new System.Drawing.SizeF(130.6703F, 10F);
            this.lblSoLuong1.StylePriority.UseFont = false;
            this.lblSoLuong1.StylePriority.UseTextAlignment = false;
            this.lblSoLuong1.Text = "123.456.789.012";
            this.lblSoLuong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuong1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel25.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 68.92783F);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.SizeF = new System.Drawing.SizeF(86.45996F, 9.999989F);
            this.xrLabel25.StylePriority.UseFont = false;
            this.xrLabel25.StylePriority.UseTextAlignment = false;
            this.xrLabel25.Text = "Số lượng (2)";
            this.xrLabel25.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel23.LocationFloat = new DevExpress.Utils.PointFloat(395.1915F, 58.92779F);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.SizeF = new System.Drawing.SizeF(86.46F, 10F);
            this.xrLabel23.StylePriority.UseFont = false;
            this.xrLabel23.StylePriority.UseTextAlignment = false;
            this.xrLabel23.Text = "Số lượng (1)";
            this.xrLabel23.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoMucKhaiKhoangDieuChinh1
            // 
            this.lblSoMucKhaiKhoangDieuChinh1.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoMucKhaiKhoangDieuChinh1.LocationFloat = new DevExpress.Utils.PointFloat(194.085F, 68.92786F);
            this.lblSoMucKhaiKhoangDieuChinh1.Name = "lblSoMucKhaiKhoangDieuChinh1";
            this.lblSoMucKhaiKhoangDieuChinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoMucKhaiKhoangDieuChinh1.SizeF = new System.Drawing.SizeF(18F, 10F);
            this.lblSoMucKhaiKhoangDieuChinh1.StylePriority.UseFont = false;
            this.lblSoMucKhaiKhoangDieuChinh1.StylePriority.UseTextAlignment = false;
            this.lblSoMucKhaiKhoangDieuChinh1.Text = "N";
            this.lblSoMucKhaiKhoangDieuChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoMucKhaiKhoangDieuChinh1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel19.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 68.92783F);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.SizeF = new System.Drawing.SizeF(184.085F, 10F);
            this.xrLabel19.StylePriority.UseFont = false;
            this.xrLabel19.StylePriority.UseTextAlignment = false;
            this.xrLabel19.Text = "Số của mục khai khoản điều chỉnh";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMoTaHangHoa
            // 
            this.lblMoTaHangHoa.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblMoTaHangHoa.LocationFloat = new DevExpress.Utils.PointFloat(106.8767F, 21.99999F);
            this.lblMoTaHangHoa.Multiline = true;
            this.lblMoTaHangHoa.Name = "lblMoTaHangHoa";
            this.lblMoTaHangHoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMoTaHangHoa.SizeF = new System.Drawing.SizeF(648.7134F, 36.9278F);
            this.lblMoTaHangHoa.StylePriority.UseFont = false;
            this.lblMoTaHangHoa.StylePriority.UseTextAlignment = false;
            this.lblMoTaHangHoa.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWW0WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6W" +
                "WWWWWWW7WWWWWWWWW8WWWWWWWWW9WWWWWWWWWE";
            this.lblMoTaHangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.LocationFloat = new DevExpress.Utils.PointFloat(9.99999F, 19.99998F);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.SizeF = new System.Drawing.SizeF(86.46F, 10F);
            this.xrLabel17.StylePriority.UseFont = false;
            this.xrLabel17.StylePriority.UseTextAlignment = false;
            this.xrLabel17.Text = "Mô tả hàng hóa";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel15.LocationFloat = new DevExpress.Utils.PointFloat(420.0328F, 9.999974F);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.SizeF = new System.Drawing.SizeF(159.6776F, 9.999998F);
            this.xrLabel15.StylePriority.UseFont = false;
            this.xrLabel15.StylePriority.UseTextAlignment = false;
            this.xrLabel15.Text = "Mã phân loại tái xác nhận giá";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiTaiXacNhanGia
            // 
            this.lblMaPhanLoaiTaiXacNhanGia.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaPhanLoaiTaiXacNhanGia.LocationFloat = new DevExpress.Utils.PointFloat(579.7105F, 9.999974F);
            this.lblMaPhanLoaiTaiXacNhanGia.Name = "lblMaPhanLoaiTaiXacNhanGia";
            this.lblMaPhanLoaiTaiXacNhanGia.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiTaiXacNhanGia.SizeF = new System.Drawing.SizeF(22.57507F, 9.999998F);
            this.lblMaPhanLoaiTaiXacNhanGia.StylePriority.UseFont = false;
            this.lblMaPhanLoaiTaiXacNhanGia.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiTaiXacNhanGia.Text = "[X]";
            this.lblMaPhanLoaiTaiXacNhanGia.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel12.LocationFloat = new DevExpress.Utils.PointFloat(230.9388F, 9.999974F);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.SizeF = new System.Drawing.SizeF(100.9574F, 9.999998F);
            this.xrLabel12.StylePriority.UseFont = false;
            this.xrLabel12.StylePriority.UseTextAlignment = false;
            this.xrLabel12.Text = "Mã quản lý riêng";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaQuanLyRieng
            // 
            this.lblMaQuanLyRieng.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaQuanLyRieng.LocationFloat = new DevExpress.Utils.PointFloat(331.8961F, 9.999974F);
            this.lblMaQuanLyRieng.Name = "lblMaQuanLyRieng";
            this.lblMaQuanLyRieng.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaQuanLyRieng.SizeF = new System.Drawing.SizeF(81.29541F, 9.999998F);
            this.lblMaQuanLyRieng.StylePriority.UseFont = false;
            this.lblMaQuanLyRieng.StylePriority.UseTextAlignment = false;
            this.lblMaQuanLyRieng.Text = "XXXXXXE";
            this.lblMaQuanLyRieng.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 9.999974F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(86.46F, 10F);
            this.xrLabel2.StylePriority.UseFont = false;
            this.xrLabel2.StylePriority.UseTextAlignment = false;
            this.xrLabel2.Text = "Mã số hàng hóa";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSoHangHoa
            // 
            this.lblMaSoHangHoa.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaSoHangHoa.LocationFloat = new DevExpress.Utils.PointFloat(106.8767F, 9.999974F);
            this.lblMaSoHangHoa.Name = "lblMaSoHangHoa";
            this.lblMaSoHangHoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSoHangHoa.SizeF = new System.Drawing.SizeF(112.3428F, 10.00001F);
            this.lblMaSoHangHoa.StylePriority.UseFont = false;
            this.lblMaSoHangHoa.StylePriority.UseTextAlignment = false;
            this.lblMaSoHangHoa.Text = "XXXX.XX.XX.X1XE";
            this.lblMaSoHangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel36
            // 
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel36.LocationFloat = new DevExpress.Utils.PointFloat(569.4957F, 78.92783F);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel36.StylePriority.UseFont = false;
            this.xrLabel36.StylePriority.UseTextAlignment = false;
            this.xrLabel36.Text = "-";
            this.xrLabel36.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLine2
            // 
            this.xrLine2.LocationFloat = new DevExpress.Utils.PointFloat(0F, 198.9846F);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.SizeF = new System.Drawing.SizeF(768F, 2F);
            // 
            // lblSoDong
            // 
            this.lblSoDong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoDong.LocationFloat = new DevExpress.Utils.PointFloat(14.99999F, 0F);
            this.lblSoDong.Name = "lblSoDong";
            this.lblSoDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoDong.SizeF = new System.Drawing.SizeF(26.04833F, 10F);
            this.lblSoDong.StylePriority.UseFont = false;
            this.lblSoDong.StylePriority.UseTextAlignment = false;
            this.lblSoDong.Text = "XE";
            this.lblSoDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // TopMargin
            // 
            this.TopMargin.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblTongSoTrang,
            this.xrLabel29,
            this.lblSoTrang,
            this.xrLabel3,
            this.xrLabel32,
            this.xrLabel31,
            this.xrLabel10,
            this.lblBieuThiTHHetHan,
            this.lblPhanLoaiCaNhanToChuc,
            this.lblMaHieuPhuongThucVanChuyen,
            this.lblMaPhanLoaiHangHoa,
            this.lblThoiHanTaiNhapTaiXuat,
            this.lblMaBoPhanXuLyToKhai,
            this.lblMaSoHangHoaDaiDienToKhai,
            this.lblTongSoToKhaiChiaNho,
            this.lblSoNhanhToKhaiChiaNho,
            this.lblGioThayDoiDangKy,
            this.lblNgayThayDoiDangKy,
            this.xrLabel21,
            this.xrLabel20,
            this.lbl33,
            this.lblGioDangKy,
            this.xrLabel16,
            this.lblMaLoaiHinh,
            this.lblTenCoQuanHaiQuanTiepNhanToKhai,
            this.xrLabel14,
            this.lblNgayDangKy,
            this.lblMaPhanLoaiKiemTra,
            this.lblSoToKhaiDauTien,
            this.xrLabel9,
            this.lblSoToKhaiTamNhapTaiXuat,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLabel5,
            this.xrLabel4,
            this.lblSoToKhai,
            this.xrLabel1,
            this.xrLine1});
            this.TopMargin.HeightF = 128F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongSoTrang
            // 
            this.lblTongSoTrang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(736.5899F, 18.49999F);
            this.lblTongSoTrang.Name = "lblTongSoTrang";
            this.lblTongSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoTrang.SizeF = new System.Drawing.SizeF(19.00006F, 11F);
            this.lblTongSoTrang.StylePriority.UseFont = false;
            this.lblTongSoTrang.StylePriority.UseTextAlignment = false;
            this.lblTongSoTrang.Text = "N";
            this.lblTongSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel29.LocationFloat = new DevExpress.Utils.PointFloat(726.5899F, 18.49999F);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.SizeF = new System.Drawing.SizeF(10F, 11F);
            this.xrLabel29.StylePriority.UseFont = false;
            this.xrLabel29.StylePriority.UseTextAlignment = false;
            this.xrLabel29.Text = "/";
            this.xrLabel29.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(696.5899F, 18.49999F);
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTrang.SizeF = new System.Drawing.SizeF(30F, 11F);
            this.lblSoTrang.StylePriority.UseFont = false;
            this.lblSoTrang.StylePriority.UseTextAlignment = false;
            this.lblSoTrang.Text = "N";
            this.lblSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.lblSoTrang.PrintOnPage += new DevExpress.XtraReports.UI.PrintOnPageEventHandler(this.lblSoTrang_PrintOnPage);
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(10.00002F, 45.54166F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(96.87669F, 10F);
            this.xrLabel3.StylePriority.UseFont = false;
            this.xrLabel3.StylePriority.UseTextAlignment = false;
            this.xrLabel3.Text = "Số tờ khai";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel32.LocationFloat = new DevExpress.Utils.PointFloat(726.5899F, 85.54163F);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.SizeF = new System.Drawing.SizeF(13F, 10F);
            this.xrLabel32.StylePriority.UseFont = false;
            this.xrLabel32.StylePriority.UseTextAlignment = false;
            this.xrLabel32.Text = "-";
            this.xrLabel32.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel31.LocationFloat = new DevExpress.Utils.PointFloat(532.7049F, 45.54166F);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.SizeF = new System.Drawing.SizeF(10.21F, 10F);
            this.xrLabel31.StylePriority.UseFont = false;
            this.xrLabel31.StylePriority.UseTextAlignment = false;
            this.xrLabel31.Text = "/";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel10.LocationFloat = new DevExpress.Utils.PointFloat(493.7567F, 45.54166F);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.SizeF = new System.Drawing.SizeF(14.79F, 10F);
            this.xrLabel10.StylePriority.UseFont = false;
            this.xrLabel10.StylePriority.UseTextAlignment = false;
            this.xrLabel10.Text = "-";
            this.xrLabel10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblBieuThiTHHetHan
            // 
            this.lblBieuThiTHHetHan.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblBieuThiTHHetHan.LocationFloat = new DevExpress.Utils.PointFloat(739.5899F, 85.54163F);
            this.lblBieuThiTHHetHan.Name = "lblBieuThiTHHetHan";
            this.lblBieuThiTHHetHan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBieuThiTHHetHan.SizeF = new System.Drawing.SizeF(16F, 10F);
            this.lblBieuThiTHHetHan.StylePriority.UseFont = false;
            this.lblBieuThiTHHetHan.StylePriority.UseTextAlignment = false;
            this.lblBieuThiTHHetHan.Text = "X";
            this.lblBieuThiTHHetHan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblPhanLoaiCaNhanToChuc
            // 
            this.lblPhanLoaiCaNhanToChuc.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblPhanLoaiCaNhanToChuc.LocationFloat = new DevExpress.Utils.PointFloat(411.7416F, 65.54164F);
            this.lblPhanLoaiCaNhanToChuc.Name = "lblPhanLoaiCaNhanToChuc";
            this.lblPhanLoaiCaNhanToChuc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhanLoaiCaNhanToChuc.SizeF = new System.Drawing.SizeF(33F, 10F);
            this.lblPhanLoaiCaNhanToChuc.StylePriority.UseFont = false;
            this.lblPhanLoaiCaNhanToChuc.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiCaNhanToChuc.Text = "[ X ]";
            this.lblPhanLoaiCaNhanToChuc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaHieuPhuongThucVanChuyen
            // 
            this.lblMaHieuPhuongThucVanChuyen.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaHieuPhuongThucVanChuyen.LocationFloat = new DevExpress.Utils.PointFloat(395.1916F, 65.54164F);
            this.lblMaHieuPhuongThucVanChuyen.Name = "lblMaHieuPhuongThucVanChuyen";
            this.lblMaHieuPhuongThucVanChuyen.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHieuPhuongThucVanChuyen.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaHieuPhuongThucVanChuyen.StylePriority.UseFont = false;
            this.lblMaHieuPhuongThucVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblMaHieuPhuongThucVanChuyen.Text = "X";
            this.lblMaHieuPhuongThucVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiHangHoa
            // 
            this.lblMaPhanLoaiHangHoa.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaPhanLoaiHangHoa.LocationFloat = new DevExpress.Utils.PointFloat(378.6416F, 65.54164F);
            this.lblMaPhanLoaiHangHoa.Name = "lblMaPhanLoaiHangHoa";
            this.lblMaPhanLoaiHangHoa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiHangHoa.SizeF = new System.Drawing.SizeF(16.55F, 10F);
            this.lblMaPhanLoaiHangHoa.StylePriority.UseFont = false;
            this.lblMaPhanLoaiHangHoa.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiHangHoa.Text = "X";
            this.lblMaPhanLoaiHangHoa.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblThoiHanTaiNhapTaiXuat
            // 
            this.lblThoiHanTaiNhapTaiXuat.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblThoiHanTaiNhapTaiXuat.LocationFloat = new DevExpress.Utils.PointFloat(657.5034F, 85.54164F);
            this.lblThoiHanTaiNhapTaiXuat.Name = "lblThoiHanTaiNhapTaiXuat";
            this.lblThoiHanTaiNhapTaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThoiHanTaiNhapTaiXuat.SizeF = new System.Drawing.SizeF(68.1579F, 10.00001F);
            this.lblThoiHanTaiNhapTaiXuat.StylePriority.UseFont = false;
            this.lblThoiHanTaiNhapTaiXuat.StylePriority.UseTextAlignment = false;
            this.lblThoiHanTaiNhapTaiXuat.Text = "dd/MM/yyyy";
            this.lblThoiHanTaiNhapTaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaBoPhanXuLyToKhai
            // 
            this.lblMaBoPhanXuLyToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaBoPhanXuLyToKhai.LocationFloat = new DevExpress.Utils.PointFloat(657.5032F, 75.54164F);
            this.lblMaBoPhanXuLyToKhai.Name = "lblMaBoPhanXuLyToKhai";
            this.lblMaBoPhanXuLyToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaBoPhanXuLyToKhai.SizeF = new System.Drawing.SizeF(23.96F, 10F);
            this.lblMaBoPhanXuLyToKhai.StylePriority.UseFont = false;
            this.lblMaBoPhanXuLyToKhai.StylePriority.UseTextAlignment = false;
            this.lblMaBoPhanXuLyToKhai.Text = "XE";
            this.lblMaBoPhanXuLyToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaSoHangHoaDaiDienToKhai
            // 
            this.lblMaSoHangHoaDaiDienToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaSoHangHoaDaiDienToKhai.LocationFloat = new DevExpress.Utils.PointFloat(657.5034F, 65.54164F);
            this.lblMaSoHangHoaDaiDienToKhai.Name = "lblMaSoHangHoaDaiDienToKhai";
            this.lblMaSoHangHoaDaiDienToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSoHangHoaDaiDienToKhai.SizeF = new System.Drawing.SizeF(41.67F, 10F);
            this.lblMaSoHangHoaDaiDienToKhai.StylePriority.UseFont = false;
            this.lblMaSoHangHoaDaiDienToKhai.StylePriority.UseTextAlignment = false;
            this.lblMaSoHangHoaDaiDienToKhai.Text = "XXXE";
            this.lblMaSoHangHoaDaiDienToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTongSoToKhaiChiaNho
            // 
            this.lblTongSoToKhaiChiaNho.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTongSoToKhaiChiaNho.LocationFloat = new DevExpress.Utils.PointFloat(542.9182F, 45.54166F);
            this.lblTongSoToKhaiChiaNho.Name = "lblTongSoToKhaiChiaNho";
            this.lblTongSoToKhaiChiaNho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoToKhaiChiaNho.SizeF = new System.Drawing.SizeF(22.08F, 10F);
            this.lblTongSoToKhaiChiaNho.StylePriority.UseFont = false;
            this.lblTongSoToKhaiChiaNho.StylePriority.UseTextAlignment = false;
            this.lblTongSoToKhaiChiaNho.Text = "NE";
            this.lblTongSoToKhaiChiaNho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTongSoToKhaiChiaNho.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblSoNhanhToKhaiChiaNho
            // 
            this.lblSoNhanhToKhaiChiaNho.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoNhanhToKhaiChiaNho.LocationFloat = new DevExpress.Utils.PointFloat(508.5467F, 45.54166F);
            this.lblSoNhanhToKhaiChiaNho.Name = "lblSoNhanhToKhaiChiaNho";
            this.lblSoNhanhToKhaiChiaNho.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoNhanhToKhaiChiaNho.SizeF = new System.Drawing.SizeF(24.15643F, 10F);
            this.lblSoNhanhToKhaiChiaNho.StylePriority.UseFont = false;
            this.lblSoNhanhToKhaiChiaNho.StylePriority.UseTextAlignment = false;
            this.lblSoNhanhToKhaiChiaNho.Text = "NE";
            this.lblSoNhanhToKhaiChiaNho.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoNhanhToKhaiChiaNho.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // lblGioThayDoiDangKy
            // 
            this.lblGioThayDoiDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblGioThayDoiDangKy.LocationFloat = new DevExpress.Utils.PointFloat(446.2933F, 85.54163F);
            this.lblGioThayDoiDangKy.Name = "lblGioThayDoiDangKy";
            this.lblGioThayDoiDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioThayDoiDangKy.SizeF = new System.Drawing.SizeF(60F, 10F);
            this.lblGioThayDoiDangKy.StylePriority.UseFont = false;
            this.lblGioThayDoiDangKy.StylePriority.UseTextAlignment = false;
            this.lblGioThayDoiDangKy.Text = "hh:mm:ss";
            this.lblGioThayDoiDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayThayDoiDangKy
            // 
            this.lblNgayThayDoiDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayThayDoiDangKy.LocationFloat = new DevExpress.Utils.PointFloat(366.0899F, 85.54163F);
            this.lblNgayThayDoiDangKy.Name = "lblNgayThayDoiDangKy";
            this.lblNgayThayDoiDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayThayDoiDangKy.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblNgayThayDoiDangKy.StylePriority.UseFont = false;
            this.lblNgayThayDoiDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayThayDoiDangKy.Text = "dd/MM/yyyy";
            this.lblNgayThayDoiDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel21.LocationFloat = new DevExpress.Utils.PointFloat(508.5433F, 85.54163F);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.SizeF = new System.Drawing.SizeF(148.96F, 10F);
            this.xrLabel21.StylePriority.UseFont = false;
            this.xrLabel21.StylePriority.UseTextAlignment = false;
            this.xrLabel21.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrLabel21.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel20.LocationFloat = new DevExpress.Utils.PointFloat(508.5433F, 75.54164F);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.SizeF = new System.Drawing.SizeF(148.96F, 10F);
            this.xrLabel20.StylePriority.UseFont = false;
            this.xrLabel20.StylePriority.UseTextAlignment = false;
            this.xrLabel20.Text = "Mã bộ phận xử lý tờ khai";
            this.xrLabel20.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lbl33
            // 
            this.lbl33.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl33.LocationFloat = new DevExpress.Utils.PointFloat(452.2933F, 65.54164F);
            this.lbl33.Name = "lbl33";
            this.lbl33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lbl33.SizeF = new System.Drawing.SizeF(205.21F, 10F);
            this.lbl33.StylePriority.UseFont = false;
            this.lbl33.StylePriority.UseTextAlignment = false;
            this.lbl33.Text = "Mã số hàng hóa đại diện của tờ khai";
            this.lbl33.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblGioDangKy
            // 
            this.lblGioDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblGioDangKy.LocationFloat = new DevExpress.Utils.PointFloat(177.085F, 85.54163F);
            this.lblGioDangKy.Name = "lblGioDangKy";
            this.lblGioDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGioDangKy.SizeF = new System.Drawing.SizeF(60F, 10F);
            this.lblGioDangKy.StylePriority.UseFont = false;
            this.lblGioDangKy.StylePriority.UseTextAlignment = false;
            this.lblGioDangKy.Text = "hh:mm:ss";
            this.lblGioDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.LocationFloat = new DevExpress.Utils.PointFloat(237.09F, 85.54163F);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.SizeF = new System.Drawing.SizeF(129F, 10F);
            this.xrLabel16.StylePriority.UseFont = false;
            this.xrLabel16.StylePriority.UseTextAlignment = false;
            this.xrLabel16.Text = "Ngày thay đổi đăng ký";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaLoaiHinh.LocationFloat = new DevExpress.Utils.PointFloat(343.3416F, 65.54164F);
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaLoaiHinh.SizeF = new System.Drawing.SizeF(35.3F, 10F);
            this.lblMaLoaiHinh.StylePriority.UseFont = false;
            this.lblMaLoaiHinh.StylePriority.UseTextAlignment = false;
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblTenCoQuanHaiQuanTiepNhanToKhai
            // 
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.LocationFloat = new DevExpress.Utils.PointFloat(237.09F, 75.54164F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Name = "lblTenCoQuanHaiQuanTiepNhanToKhai";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.SizeF = new System.Drawing.SizeF(102F, 10F);
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.StylePriority.UseFont = false;
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.StylePriority.UseTextAlignment = false;
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Text = "XXXXXXXXXE";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.LocationFloat = new DevExpress.Utils.PointFloat(237.09F, 65.54164F);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.SizeF = new System.Drawing.SizeF(106.25F, 10F);
            this.xrLabel14.StylePriority.UseFont = false;
            this.xrLabel14.StylePriority.UseTextAlignment = false;
            this.xrLabel14.Text = "Mã loại hình";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.lblNgayDangKy.LocationFloat = new DevExpress.Utils.PointFloat(96.45999F, 85.54163F);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.SizeF = new System.Drawing.SizeF(80.2F, 10F);
            this.lblNgayDangKy.StylePriority.UseFont = false;
            this.lblNgayDangKy.StylePriority.UseTextAlignment = false;
            this.lblNgayDangKy.Text = "dd/MM/yyyy";
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblMaPhanLoaiKiemTra
            // 
            this.lblMaPhanLoaiKiemTra.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblMaPhanLoaiKiemTra.LocationFloat = new DevExpress.Utils.PointFloat(152.71F, 65.54164F);
            this.lblMaPhanLoaiKiemTra.Name = "lblMaPhanLoaiKiemTra";
            this.lblMaPhanLoaiKiemTra.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaPhanLoaiKiemTra.SizeF = new System.Drawing.SizeF(39.59F, 10F);
            this.lblMaPhanLoaiKiemTra.StylePriority.UseFont = false;
            this.lblMaPhanLoaiKiemTra.StylePriority.UseTextAlignment = false;
            this.lblMaPhanLoaiKiemTra.Text = "XX E";
            this.lblMaPhanLoaiKiemTra.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhaiDauTien
            // 
            this.lblSoToKhaiDauTien.AutoWidth = true;
            this.lblSoToKhaiDauTien.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhaiDauTien.LocationFloat = new DevExpress.Utils.PointFloat(368.34F, 45.54166F);
            this.lblSoToKhaiDauTien.Name = "lblSoToKhaiDauTien";
            this.lblSoToKhaiDauTien.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiDauTien.SizeF = new System.Drawing.SizeF(125.4167F, 10F);
            this.lblSoToKhaiDauTien.StylePriority.UseFont = false;
            this.lblSoToKhaiDauTien.StylePriority.UseTextAlignment = false;
            this.lblSoToKhaiDauTien.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiDauTien.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoToKhaiDauTien.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel9
            // 
            this.xrLabel9.AutoWidth = true;
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.LocationFloat = new DevExpress.Utils.PointFloat(237.09F, 45.54166F);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.SizeF = new System.Drawing.SizeF(131.25F, 10F);
            this.xrLabel9.StylePriority.UseFont = false;
            this.xrLabel9.StylePriority.UseTextAlignment = false;
            this.xrLabel9.Text = "Số tờ khai đầu tiên";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhaiTamNhapTaiXuat
            // 
            this.lblSoToKhaiTamNhapTaiXuat.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhaiTamNhapTaiXuat.LocationFloat = new DevExpress.Utils.PointFloat(237.085F, 55.54165F);
            this.lblSoToKhaiTamNhapTaiXuat.Name = "lblSoToKhaiTamNhapTaiXuat";
            this.lblSoToKhaiTamNhapTaiXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhaiTamNhapTaiXuat.SizeF = new System.Drawing.SizeF(117.71F, 10F);
            this.lblSoToKhaiTamNhapTaiXuat.StylePriority.UseFont = false;
            this.lblSoToKhaiTamNhapTaiXuat.StylePriority.UseTextAlignment = false;
            this.lblSoToKhaiTamNhapTaiXuat.Text = "NNNNNNNNN1NE";
            this.lblSoToKhaiTamNhapTaiXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblSoToKhaiTamNhapTaiXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel7.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 85.54163F);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.SizeF = new System.Drawing.SizeF(86.46F, 10F);
            this.xrLabel7.StylePriority.UseFont = false;
            this.xrLabel7.StylePriority.UseTextAlignment = false;
            this.xrLabel7.Text = "Ngày đăng ký";
            this.xrLabel7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel6.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 75.54164F);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.SizeF = new System.Drawing.SizeF(227.09F, 10F);
            this.xrLabel6.StylePriority.UseFont = false;
            this.xrLabel6.StylePriority.UseTextAlignment = false;
            this.xrLabel6.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            this.xrLabel6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel5.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 65.54164F);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.SizeF = new System.Drawing.SizeF(142.71F, 10F);
            this.xrLabel5.StylePriority.UseFont = false;
            this.xrLabel5.StylePriority.UseTextAlignment = false;
            this.xrLabel5.Text = "Mã phân loại kiểm tra";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.xrLabel4.LocationFloat = new DevExpress.Utils.PointFloat(10.00001F, 55.54165F);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.SizeF = new System.Drawing.SizeF(227.085F, 10F);
            this.xrLabel4.StylePriority.UseFont = false;
            this.xrLabel4.StylePriority.UseTextAlignment = false;
            this.xrLabel4.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            this.xrLabel4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoToKhai.LocationFloat = new DevExpress.Utils.PointFloat(106.8767F, 45.54166F);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.SizeF = new System.Drawing.SizeF(117.71F, 10F);
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.StylePriority.UseTextAlignment = false;
            this.lblSoToKhai.Text = "NNNNNNNNN1NE";
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 13F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(175F, 18.5F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(409.2857F, 23F);
            this.xrLabel1.StylePriority.UseFont = false;
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "Tờ khai hàng hóa nhập khẩu (thông quan)";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.LocationFloat = new DevExpress.Utils.PointFloat(0.9999905F, 95.5416F);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.SizeF = new System.Drawing.SizeF(768F, 2F);
            // 
            // BottomMargin
            // 
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel28,
            this.xrLabel24,
            this.xrLabel2,
            this.xrLine2,
            this.xrLabel36,
            this.lblMaSoHangHoa,
            this.lblSoDong,
            this.lblMaQuanLyRieng,
            this.xrLabel12,
            this.lblMaPhanLoaiTaiXacNhanGia,
            this.xrLabel15,
            this.xrLabel17,
            this.lblMoTaHangHoa,
            this.xrLabel19,
            this.lblSoMucKhaiKhoangDieuChinh1,
            this.xrLabel23,
            this.xrLabel25,
            this.lblSoLuong1,
            this.lblSoLuong2,
            this.lblMaDonViTinh1,
            this.lblMaDonViTinh2,
            this.lblSoMucKhaiKhoangDieuChinh2,
            this.lblSoMucKhaiKhoangDieuChinh3,
            this.lblSoMucKhaiKhoangDieuChinh4,
            this.lblSoMucKhaiKhoangDieuChinh5,
            this.xrLabel30,
            this.lblTriGiaHoaDon,
            this.xrLabel34,
            this.lblDonGiaHoaDon,
            this.xrLabel37,
            this.lblDonViCuaDonGiaVaSoLuong,
            this.lblMaDongTienCuaDonGia,
            this.xrLabel40,
            this.xrLabel41,
            this.xrLabel42,
            this.xrLabel43,
            this.xrLabel44,
            this.xrLabel45,
            this.xrLabel46,
            this.xrLabel47,
            this.xrLabel48,
            this.xrLabel49,
            this.lblTriGiaTinhThueS,
            this.lblSoLuongTinhThue,
            this.lblMaDonViTinhChuanDanhThue,
            this.lblMaPhanLoaiThueSuat,
            this.lblThueSuatThueNhapKhau,
            this.lblPhanLoaiNhapThueSuat,
            this.xrLabel57,
            this.lblSoTienThueNK,
            this.lblSoTienGiamThueNK,
            this.xrLabel60,
            this.xrLabel61,
            this.xrLabel62,
            this.xrLabel63,
            this.xrLabel64,
            this.xrLabel65,
            this.xrLabel66,
            this.lblMaDongTienCuaGiaTinhThue,
            this.xrLabel68,
            this.lblTriGiaTinhThueM,
            this.lblDonGiaTinhThue,
            this.xrLabel71,
            this.xrLabel72,
            this.lblDonViSoLuongTrongDonGiaTinhThue,
            this.lblMaXacDinhMucThueNhapKhau,
            this.lblMaNuocXuatXu,
            this.xrLabel76,
            this.lblTenNoiXuatXu,
            this.xrLabel78,
            this.lblMaBieuThueNK,
            this.lblMaNgoaiHanNgach,
            this.lblSoThuTuDongHangToKhai,
            this.lblSoDKDanhMucMienThue,
            this.lblSoDongTuongUngDMMienThue,
            this.xrLabel84,
            this.lblMaMienGiamThueNK,
            this.lblDieuKhoanMienGiam,
            this.xrLabel8,
            this.xrLabel11,
            this.xrLabel13,
            this.xrLabel18,
            this.xrLabel22,
            this.xrLabel26,
            this.xrLabel27,
            this.lblTenKhoanMucThueVaThuKhac1,
            this.lblTriGiaTinhThueVaThuKhac1,
            this.xrLabel33,
            this.lblThueSuatThueVaThuKhac1,
            this.lblSoTienThueVaThuKhac1,
            this.xrLabel39,
            this.lblSoTienGiamThueVaThuKhac1,
            this.xrLabel51,
            this.xrLabel52,
            this.xrLabel53,
            this.xrLabel55,
            this.lblMaApDungThueSuatThueVaThuKhac1,
            this.lblSoLuongTinhThueVaThuKhac1,
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1,
            this.lblMaMienGiamThueVaThuKhac1,
            this.lblDieuKhoanMienGiamThueVaThuKhac1,
            this.xrLabel69,
            this.xrLabel70,
            this.xrLabel73,
            this.lblMaApDungThueSuatThueVaThuKhac2,
            this.lblSoLuongTinhThueVaThuKhac2,
            this.lblThueSuatThueVaThuKhac2,
            this.xrLabel79,
            this.xrLabel80,
            this.xrLabel81,
            this.xrLabel82,
            this.lblDieuKhoanMienGiamThueVaThuKhac2,
            this.xrLine3,
            this.lblTenKhoanMucThueVaThuKhac2,
            this.lblTriGiaTinhThueVaThuKhac2,
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2,
            this.lblMaMienGiamThueVaThuKhac2,
            this.xrLabel89,
            this.lblSoTienThueVaThuKhac2,
            this.xrLabel91,
            this.xrLabel92,
            this.xrLabel93,
            this.xrLabel94,
            this.lblSoTienGiamThueVaThuKhac2,
            this.xrLabel96,
            this.xrLabel97,
            this.xrLabel98,
            this.lblMaApDungThueSuatThueVaThuKhac3,
            this.lblSoLuongTinhThueVaThuKhac3,
            this.lblThueSuatThueVaThuKhac3,
            this.xrLabel102,
            this.xrLabel103,
            this.xrLabel104,
            this.xrLabel105,
            this.lblDieuKhoanMienGiamThueVaThuKhac3,
            this.xrLine4,
            this.lblTenKhoanMucThueVaThuKhac3,
            this.lblTriGiaTinhThueVaThuKhac3,
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3,
            this.lblMaMienGiamThueVaThuKhac3,
            this.xrLabel111,
            this.lblSoTienThueVaThuKhac3,
            this.xrLabel113,
            this.xrLabel114,
            this.xrLabel115,
            this.xrLabel116,
            this.lblSoTienGiamThueVaThuKhac3,
            this.xrLabel118,
            this.xrLabel119,
            this.xrLabel120,
            this.lblMaApDungThueSuatThueVaThuKhac4,
            this.lblSoLuongTinhThueVaThuKhac4,
            this.lblThueSuatThueVaThuKhac4,
            this.xrLabel124,
            this.xrLabel125,
            this.xrLabel126,
            this.xrLabel127,
            this.lblDieuKhoanMienGiamThueVaThuKhac4,
            this.xrLine5,
            this.lblTenKhoanMucThueVaThuKhac4,
            this.lblTriGiaTinhThueVaThuKhac4,
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4,
            this.lblMaMienGiamThueVaThuKhac4,
            this.xrLabel133,
            this.lblSoTienThueVaThuKhac4,
            this.xrLabel135,
            this.xrLabel136,
            this.xrLabel137,
            this.xrLabel138,
            this.lblSoTienGiamThueVaThuKhac4,
            this.xrLabel140,
            this.xrLabel141,
            this.xrLabel142,
            this.lblMaApDungThueSuatThueVaThuKhac5,
            this.lblSoLuongTinhThueVaThuKhac5,
            this.lblThueSuatThueVaThuKhac5,
            this.xrLabel146,
            this.xrLabel147,
            this.xrLabel148,
            this.xrLabel149,
            this.lblDieuKhoanMienGiamThueVaThuKhac5,
            this.lblTenKhoanMucThueVaThuKhac5,
            this.lblTriGiaTinhThueVaThuKhac5,
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5,
            this.lblMaMienGiamThueVaThuKhac5,
            this.xrLabel155,
            this.lblSoTienThueVaThuKhac5,
            this.xrLabel157,
            this.xrLabel158,
            this.xrLabel159,
            this.xrLabel160,
            this.lblSoTienGiamThueVaThuKhac5,
            this.xrLine6,
            this.xrLabel162});
            this.Detail1.HeightF = 653.125F;
            this.Detail1.Name = "Detail1";
            // 
            // xrLabel28
            // 
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel28.LocationFloat = new DevExpress.Utils.PointFloat(41.04832F, 0F);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.SizeF = new System.Drawing.SizeF(25.00666F, 10F);
            this.xrLabel28.StylePriority.UseFont = false;
            this.xrLabel28.StylePriority.UseTextAlignment = false;
            this.xrLabel28.Text = ">";
            this.xrLabel28.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.xrLabel24.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.SizeF = new System.Drawing.SizeF(15F, 10F);
            this.xrLabel24.StylePriority.UseFont = false;
            this.xrLabel24.StylePriority.UseTextAlignment = false;
            this.xrLabel24.Text = "< ";
            this.xrLabel24.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // QuyetDinhThongQuanHangHoaNhapKhau_3
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(51, 26, 128, 100);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiKiemTra;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhaiDauTien;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhaiTamNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRLabel lblTenCoQuanHaiQuanTiepNhanToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel lbl33;
        private DevExpress.XtraReports.UI.XRLabel lblGioDangKy;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel lblSoNhanhToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRLabel lblGioThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblNgayThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRLabel lblBieuThiTHHetHan;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiCaNhanToChuc;
        private DevExpress.XtraReports.UI.XRLabel lblMaHieuPhuongThucVanChuyen;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiHangHoa;
        private DevExpress.XtraReports.UI.XRLabel lblThoiHanTaiNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRLabel lblMaBoPhanXuLyToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblMaSoHangHoaDaiDienToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblMaSoHangHoa;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiTaiXacNhanGia;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lblMaQuanLyRieng;
        private DevExpress.XtraReports.UI.XRLabel lblMoTaHangHoa;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinh2;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinh1;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuong2;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuong1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel lblSoMucKhaiKhoangDieuChinh1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel41;
        private DevExpress.XtraReports.UI.XRLabel xrLabel40;
        private DevExpress.XtraReports.UI.XRLabel lblMaDongTienCuaDonGia;
        private DevExpress.XtraReports.UI.XRLabel lblDonViCuaDonGiaVaSoLuong;
        private DevExpress.XtraReports.UI.XRLabel xrLabel37;
        private DevExpress.XtraReports.UI.XRLabel lblDonGiaHoaDon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaHoaDon;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel lblSoMucKhaiKhoangDieuChinh5;
        private DevExpress.XtraReports.UI.XRLabel lblSoMucKhaiKhoangDieuChinh4;
        private DevExpress.XtraReports.UI.XRLabel lblSoMucKhaiKhoangDieuChinh3;
        private DevExpress.XtraReports.UI.XRLabel lblSoMucKhaiKhoangDieuChinh2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel49;
        private DevExpress.XtraReports.UI.XRLabel xrLabel48;
        private DevExpress.XtraReports.UI.XRLabel xrLabel47;
        private DevExpress.XtraReports.UI.XRLabel xrLabel46;
        private DevExpress.XtraReports.UI.XRLabel xrLabel45;
        private DevExpress.XtraReports.UI.XRLabel xrLabel44;
        private DevExpress.XtraReports.UI.XRLabel xrLabel43;
        private DevExpress.XtraReports.UI.XRLabel xrLabel42;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaTinhThueS;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinhChuanDanhThue;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuongTinhThue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel64;
        private DevExpress.XtraReports.UI.XRLabel xrLabel63;
        private DevExpress.XtraReports.UI.XRLabel xrLabel62;
        private DevExpress.XtraReports.UI.XRLabel xrLabel61;
        private DevExpress.XtraReports.UI.XRLabel xrLabel60;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienGiamThueNK;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienThueNK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel57;
        private DevExpress.XtraReports.UI.XRLabel lblPhanLoaiNhapThueSuat;
        private DevExpress.XtraReports.UI.XRLabel lblThueSuatThueNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel lblMaPhanLoaiThueSuat;
        private DevExpress.XtraReports.UI.XRLabel lblSoDKDanhMucMienThue;
        private DevExpress.XtraReports.UI.XRLabel lblSoThuTuDongHangToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblMaNgoaiHanNgach;
        private DevExpress.XtraReports.UI.XRLabel lblMaBieuThueNK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel78;
        private DevExpress.XtraReports.UI.XRLabel lblTenNoiXuatXu;
        private DevExpress.XtraReports.UI.XRLabel xrLabel76;
        private DevExpress.XtraReports.UI.XRLabel lblMaNuocXuatXu;
        private DevExpress.XtraReports.UI.XRLabel lblMaXacDinhMucThueNhapKhau;
        private DevExpress.XtraReports.UI.XRLabel lblDonViSoLuongTrongDonGiaTinhThue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel72;
        private DevExpress.XtraReports.UI.XRLabel xrLabel71;
        private DevExpress.XtraReports.UI.XRLabel lblDonGiaTinhThue;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaTinhThueM;
        private DevExpress.XtraReports.UI.XRLabel xrLabel68;
        private DevExpress.XtraReports.UI.XRLabel lblMaDongTienCuaGiaTinhThue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel66;
        private DevExpress.XtraReports.UI.XRLabel xrLabel65;
        private DevExpress.XtraReports.UI.XRLabel lblDieuKhoanMienGiam;
        private DevExpress.XtraReports.UI.XRLabel lblMaMienGiamThueNK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel84;
        private DevExpress.XtraReports.UI.XRLabel lblSoDongTuongUngDMMienThue;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel lblTenKhoanMucThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel51;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienGiamThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel39;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRLabel lblThueSuatThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaTinhThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuongTinhThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRLabel lblMaApDungThueSuatThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel55;
        private DevExpress.XtraReports.UI.XRLabel xrLabel53;
        private DevExpress.XtraReports.UI.XRLabel xrLabel52;
        private DevExpress.XtraReports.UI.XRLabel lblDieuKhoanMienGiamThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRLabel lblMaMienGiamThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinhChuanDanhThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienGiamThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel116;
        private DevExpress.XtraReports.UI.XRLabel xrLabel115;
        private DevExpress.XtraReports.UI.XRLabel xrLabel114;
        private DevExpress.XtraReports.UI.XRLabel xrLabel113;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel111;
        private DevExpress.XtraReports.UI.XRLabel lblMaMienGiamThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinhChuanDanhThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaTinhThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRLabel lblTenKhoanMucThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel lblDieuKhoanMienGiamThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel105;
        private DevExpress.XtraReports.UI.XRLabel xrLabel104;
        private DevExpress.XtraReports.UI.XRLabel xrLabel103;
        private DevExpress.XtraReports.UI.XRLabel xrLabel102;
        private DevExpress.XtraReports.UI.XRLabel lblThueSuatThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuongTinhThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRLabel lblMaApDungThueSuatThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel98;
        private DevExpress.XtraReports.UI.XRLabel xrLabel97;
        private DevExpress.XtraReports.UI.XRLabel xrLabel96;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienGiamThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel94;
        private DevExpress.XtraReports.UI.XRLabel xrLabel93;
        private DevExpress.XtraReports.UI.XRLabel xrLabel92;
        private DevExpress.XtraReports.UI.XRLabel xrLabel91;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel89;
        private DevExpress.XtraReports.UI.XRLabel lblMaMienGiamThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinhChuanDanhThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaTinhThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRLabel lblTenKhoanMucThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel lblDieuKhoanMienGiamThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel82;
        private DevExpress.XtraReports.UI.XRLabel xrLabel81;
        private DevExpress.XtraReports.UI.XRLabel xrLabel80;
        private DevExpress.XtraReports.UI.XRLabel xrLabel79;
        private DevExpress.XtraReports.UI.XRLabel lblThueSuatThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuongTinhThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRLabel lblMaApDungThueSuatThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel73;
        private DevExpress.XtraReports.UI.XRLabel xrLabel70;
        private DevExpress.XtraReports.UI.XRLabel xrLabel69;
        private DevExpress.XtraReports.UI.XRLine xrLine6;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienGiamThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel160;
        private DevExpress.XtraReports.UI.XRLabel xrLabel159;
        private DevExpress.XtraReports.UI.XRLabel xrLabel158;
        private DevExpress.XtraReports.UI.XRLabel xrLabel157;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel155;
        private DevExpress.XtraReports.UI.XRLabel lblMaMienGiamThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinhChuanDanhThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaTinhThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRLabel lblTenKhoanMucThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRLabel lblDieuKhoanMienGiamThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel149;
        private DevExpress.XtraReports.UI.XRLabel xrLabel148;
        private DevExpress.XtraReports.UI.XRLabel xrLabel147;
        private DevExpress.XtraReports.UI.XRLabel xrLabel146;
        private DevExpress.XtraReports.UI.XRLabel lblThueSuatThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuongTinhThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRLabel lblMaApDungThueSuatThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel142;
        private DevExpress.XtraReports.UI.XRLabel xrLabel141;
        private DevExpress.XtraReports.UI.XRLabel xrLabel140;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienGiamThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel138;
        private DevExpress.XtraReports.UI.XRLabel xrLabel137;
        private DevExpress.XtraReports.UI.XRLabel xrLabel136;
        private DevExpress.XtraReports.UI.XRLabel xrLabel135;
        private DevExpress.XtraReports.UI.XRLabel lblSoTienThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel133;
        private DevExpress.XtraReports.UI.XRLabel lblMaMienGiamThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRLabel lblMaDonViTinhChuanDanhThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRLabel lblTriGiaTinhThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRLabel lblTenKhoanMucThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLabel lblDieuKhoanMienGiamThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel127;
        private DevExpress.XtraReports.UI.XRLabel xrLabel126;
        private DevExpress.XtraReports.UI.XRLabel xrLabel125;
        private DevExpress.XtraReports.UI.XRLabel xrLabel124;
        private DevExpress.XtraReports.UI.XRLabel lblThueSuatThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuongTinhThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRLabel lblMaApDungThueSuatThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel120;
        private DevExpress.XtraReports.UI.XRLabel xrLabel119;
        private DevExpress.XtraReports.UI.XRLabel xrLabel118;
        private DevExpress.XtraReports.UI.XRLabel xrLabel162;
        private DevExpress.XtraReports.UI.XRLabel lblSoDong;
        private DevExpress.XtraReports.UI.XRLabel lblSoTrang;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoTrang;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
    }
}
