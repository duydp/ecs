using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class HangCoDetail
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Co_ID { set; get; }
		public string MaNguyenTe { set; get; }
		public string MaChuyenNganh { set; get; }
		public string GhiChu { set; get; }
		public long HMD_ID { set; get; }
		public int SoThuTuHang { set; get; }
		public string MaHS { set; get; }
		public string MaPhu { set; get; }
		public string TenHang { set; get; }
		public string NuocXX_ID { set; get; }
		public string DVT_ID { set; get; }
		public decimal SoLuong { set; get; }
		public double DonGiaKB { set; get; }
		public double TriGiaKB { set; get; }
		public double TrongLuong { set; get; }
		public string LoaiKien { set; get; }
		public string SoHoaDon { set; get; }
		public string SoHieuKien { set; get; }
		public DateTime NgayHoaDon { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HangCoDetail Load(long id)
		{
			const string spName = "[dbo].[p_KDT_HangCoDetail_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			HangCoDetail entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new HangCoDetail();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Co_ID"))) entity.Co_ID = reader.GetInt64(reader.GetOrdinal("Co_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenTe"))) entity.MaNguyenTe = reader.GetString(reader.GetOrdinal("MaNguyenTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaChuyenNganh"))) entity.MaChuyenNganh = reader.GetString(reader.GetOrdinal("MaChuyenNganh"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("HMD_ID"))) entity.HMD_ID = reader.GetInt64(reader.GetOrdinal("HMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDouble(reader.GetOrdinal("DonGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDouble(reader.GetOrdinal("TriGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDouble(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKien"))) entity.LoaiKien = reader.GetString(reader.GetOrdinal("LoaiKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuKien"))) entity.SoHieuKien = reader.GetString(reader.GetOrdinal("SoHieuKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDon"))) entity.NgayHoaDon = reader.GetDateTime(reader.GetOrdinal("NgayHoaDon"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<HangCoDetail> SelectCollectionAll()
		{
			List<HangCoDetail> collection = new List<HangCoDetail>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				HangCoDetail entity = new HangCoDetail();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Co_ID"))) entity.Co_ID = reader.GetInt64(reader.GetOrdinal("Co_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenTe"))) entity.MaNguyenTe = reader.GetString(reader.GetOrdinal("MaNguyenTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaChuyenNganh"))) entity.MaChuyenNganh = reader.GetString(reader.GetOrdinal("MaChuyenNganh"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("HMD_ID"))) entity.HMD_ID = reader.GetInt64(reader.GetOrdinal("HMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDouble(reader.GetOrdinal("DonGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDouble(reader.GetOrdinal("TriGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDouble(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKien"))) entity.LoaiKien = reader.GetString(reader.GetOrdinal("LoaiKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuKien"))) entity.SoHieuKien = reader.GetString(reader.GetOrdinal("SoHieuKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDon"))) entity.NgayHoaDon = reader.GetDateTime(reader.GetOrdinal("NgayHoaDon"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<HangCoDetail> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<HangCoDetail> collection = new List<HangCoDetail>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				HangCoDetail entity = new HangCoDetail();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Co_ID"))) entity.Co_ID = reader.GetInt64(reader.GetOrdinal("Co_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenTe"))) entity.MaNguyenTe = reader.GetString(reader.GetOrdinal("MaNguyenTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaChuyenNganh"))) entity.MaChuyenNganh = reader.GetString(reader.GetOrdinal("MaChuyenNganh"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("HMD_ID"))) entity.HMD_ID = reader.GetInt64(reader.GetOrdinal("HMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuTuHang"))) entity.SoThuTuHang = reader.GetInt32(reader.GetOrdinal("SoThuTuHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhu"))) entity.MaPhu = reader.GetString(reader.GetOrdinal("MaPhu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonGiaKB"))) entity.DonGiaKB = reader.GetDouble(reader.GetOrdinal("DonGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaKB"))) entity.TriGiaKB = reader.GetDouble(reader.GetOrdinal("TriGiaKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDouble(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKien"))) entity.LoaiKien = reader.GetString(reader.GetOrdinal("LoaiKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuKien"))) entity.SoHieuKien = reader.GetString(reader.GetOrdinal("SoHieuKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDon"))) entity.NgayHoaDon = reader.GetDateTime(reader.GetOrdinal("NgayHoaDon"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_HangCoDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_HangCoDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_HangCoDetail_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_HangCoDetail_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertHangCoDetail(long co_ID, string maNguyenTe, string maChuyenNganh, string ghiChu, long hMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, double donGiaKB, double triGiaKB, double trongLuong, string loaiKien, string soHoaDon, string soHieuKien, DateTime ngayHoaDon)
		{
			HangCoDetail entity = new HangCoDetail();	
			entity.Co_ID = co_ID;
			entity.MaNguyenTe = maNguyenTe;
			entity.MaChuyenNganh = maChuyenNganh;
			entity.GhiChu = ghiChu;
			entity.HMD_ID = hMD_ID;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHS = maHS;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.NuocXX_ID = nuocXX_ID;
			entity.DVT_ID = dVT_ID;
			entity.SoLuong = soLuong;
			entity.DonGiaKB = donGiaKB;
			entity.TriGiaKB = triGiaKB;
			entity.TrongLuong = trongLuong;
			entity.LoaiKien = loaiKien;
			entity.SoHoaDon = soHoaDon;
			entity.SoHieuKien = soHieuKien;
			entity.NgayHoaDon = ngayHoaDon;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_HangCoDetail_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Co_ID", SqlDbType.BigInt, Co_ID);
			db.AddInParameter(dbCommand, "@MaNguyenTe", SqlDbType.VarChar, MaNguyenTe);
			db.AddInParameter(dbCommand, "@MaChuyenNganh", SqlDbType.VarChar, MaChuyenNganh);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
			db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Float, TrongLuong);
			db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.NVarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
			db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year <= 1753 ? DBNull.Value : (object) NgayHoaDon);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HangCoDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangCoDetail item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHangCoDetail(long id, long co_ID, string maNguyenTe, string maChuyenNganh, string ghiChu, long hMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, double donGiaKB, double triGiaKB, double trongLuong, string loaiKien, string soHoaDon, string soHieuKien, DateTime ngayHoaDon)
		{
			HangCoDetail entity = new HangCoDetail();			
			entity.ID = id;
			entity.Co_ID = co_ID;
			entity.MaNguyenTe = maNguyenTe;
			entity.MaChuyenNganh = maChuyenNganh;
			entity.GhiChu = ghiChu;
			entity.HMD_ID = hMD_ID;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHS = maHS;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.NuocXX_ID = nuocXX_ID;
			entity.DVT_ID = dVT_ID;
			entity.SoLuong = soLuong;
			entity.DonGiaKB = donGiaKB;
			entity.TriGiaKB = triGiaKB;
			entity.TrongLuong = trongLuong;
			entity.LoaiKien = loaiKien;
			entity.SoHoaDon = soHoaDon;
			entity.SoHieuKien = soHieuKien;
			entity.NgayHoaDon = ngayHoaDon;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_HangCoDetail_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Co_ID", SqlDbType.BigInt, Co_ID);
			db.AddInParameter(dbCommand, "@MaNguyenTe", SqlDbType.VarChar, MaNguyenTe);
			db.AddInParameter(dbCommand, "@MaChuyenNganh", SqlDbType.VarChar, MaChuyenNganh);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
			db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Float, TrongLuong);
			db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.NVarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
			db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year == 1753 ? DBNull.Value : (object) NgayHoaDon);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HangCoDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangCoDetail item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHangCoDetail(long id, long co_ID, string maNguyenTe, string maChuyenNganh, string ghiChu, long hMD_ID, int soThuTuHang, string maHS, string maPhu, string tenHang, string nuocXX_ID, string dVT_ID, decimal soLuong, double donGiaKB, double triGiaKB, double trongLuong, string loaiKien, string soHoaDon, string soHieuKien, DateTime ngayHoaDon)
		{
			HangCoDetail entity = new HangCoDetail();			
			entity.ID = id;
			entity.Co_ID = co_ID;
			entity.MaNguyenTe = maNguyenTe;
			entity.MaChuyenNganh = maChuyenNganh;
			entity.GhiChu = ghiChu;
			entity.HMD_ID = hMD_ID;
			entity.SoThuTuHang = soThuTuHang;
			entity.MaHS = maHS;
			entity.MaPhu = maPhu;
			entity.TenHang = tenHang;
			entity.NuocXX_ID = nuocXX_ID;
			entity.DVT_ID = dVT_ID;
			entity.SoLuong = soLuong;
			entity.DonGiaKB = donGiaKB;
			entity.TriGiaKB = triGiaKB;
			entity.TrongLuong = trongLuong;
			entity.LoaiKien = loaiKien;
			entity.SoHoaDon = soHoaDon;
			entity.SoHieuKien = soHieuKien;
			entity.NgayHoaDon = ngayHoaDon;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_HangCoDetail_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Co_ID", SqlDbType.BigInt, Co_ID);
			db.AddInParameter(dbCommand, "@MaNguyenTe", SqlDbType.VarChar, MaNguyenTe);
			db.AddInParameter(dbCommand, "@MaChuyenNganh", SqlDbType.VarChar, MaChuyenNganh);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@SoThuTuHang", SqlDbType.Int, SoThuTuHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@MaPhu", SqlDbType.VarChar, MaPhu);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, NuocXX_ID);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonGiaKB", SqlDbType.Float, DonGiaKB);
			db.AddInParameter(dbCommand, "@TriGiaKB", SqlDbType.Float, TriGiaKB);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Float, TrongLuong);
			db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, LoaiKien);
			db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.NVarChar, SoHoaDon);
			db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, SoHieuKien);
			db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year == 1753 ? DBNull.Value : (object) NgayHoaDon);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HangCoDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangCoDetail item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHangCoDetail(long id)
		{
			HangCoDetail entity = new HangCoDetail();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_HangCoDetail_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_HangCoDetail_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HangCoDetail> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangCoDetail item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}