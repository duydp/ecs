using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Entity
{
    public partial class License
    {
        #region Private members.

        protected string _ID = string.Empty;
        protected string _Name = string.Empty;
        protected string _Description = string.Empty;
        protected int _NumberClient;
        protected int _Term;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public string ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public string Name
        {
            set { this._Name = value; }
            get { return this._Name; }
        }

        public string Description
        {
            set { this._Description = value; }
            get { return this._Description; }
        }

        public int NumberClient
        {
            set { this._NumberClient = value; }
            get { return this._NumberClient; }
        }

        public int Term
        {
            set { this._Term = value; }
            get { return this._Term; }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static License Load(string iD)
        {
            const string spName = "[dbo].[p_LICENSE_License_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, iD);
            License entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new License();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetString(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("Name"))) entity.Name = reader.GetString(reader.GetOrdinal("Name"));
                if (!reader.IsDBNull(reader.GetOrdinal("Description"))) entity.Description = reader.GetString(reader.GetOrdinal("Description"));
                if (!reader.IsDBNull(reader.GetOrdinal("NumberClient"))) entity.NumberClient = reader.GetInt32(reader.GetOrdinal("NumberClient"));
                if (!reader.IsDBNull(reader.GetOrdinal("Term"))) entity.Term = reader.GetInt32(reader.GetOrdinal("Term"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------


        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LICENSE_License_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_License_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LICENSE_License_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_LICENSE_License_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static string InsertLicense(string name, string description, int numberClient, int term)
        {
            License entity = new License();
            entity.Name = name;
            entity.Description = description;
            entity.NumberClient = numberClient;
            entity.Term = term;
            return entity.Insert();
        }

        public string Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public string Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_License_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
            db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
            db.AddInParameter(dbCommand, "@NumberClient", SqlDbType.Int, NumberClient);
            db.AddInParameter(dbCommand, "@Term", SqlDbType.Int, Term);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (string)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._ID = (string)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateLicense(string iD, string name, string description, int numberClient, int term)
        {
            License entity = new License();
            entity.Name = name;
            entity.Description = description;
            entity.NumberClient = numberClient;
            entity.Term = term;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_LICENSE_License_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
            db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
            db.AddInParameter(dbCommand, "@NumberClient", SqlDbType.Int, NumberClient);
            db.AddInParameter(dbCommand, "@Term", SqlDbType.Int, Term);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateLicense(string iD, string name, string description, int numberClient, int term)
        {
            License entity = new License();
            entity.ID = iD;
            entity.Name = name;
            entity.Description = description;
            entity.NumberClient = numberClient;
            entity.Term = term;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_License_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);
            db.AddInParameter(dbCommand, "@Name", SqlDbType.NVarChar, Name);
            db.AddInParameter(dbCommand, "@Description", SqlDbType.NVarChar, Description);
            db.AddInParameter(dbCommand, "@NumberClient", SqlDbType.Int, NumberClient);
            db.AddInParameter(dbCommand, "@Term", SqlDbType.Int, Term);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteLicense(string iD)
        {
            License entity = new License();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_LICENSE_License_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.VarChar, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        #endregion
    }
}