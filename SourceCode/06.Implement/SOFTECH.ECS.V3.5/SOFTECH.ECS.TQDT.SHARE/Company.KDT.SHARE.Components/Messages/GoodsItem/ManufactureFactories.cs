﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages.CSSX;

namespace Company.KDT.SHARE.Components.Messages.GoodsItem
{
  public  class ManufactureFactories
    {
        /// Thông tin Cơ sở sản xuất (CSSX)
        /// </summary>
      [XmlElement("ManufactureFactory")]
      public List<ManufactureFactory> ManufactureFactory { get; set; }

    }
}
