﻿/*
Run this script on:

        .\sqlserver.ECS_TQDT_GC_V4_VINATEX_19-1-2016    -  This database will be modified

to synchronize it with:

        113.160.225.156,2461.ECS_TQDT_GC_V4

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 01/20/2016 8:37:05 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping extended properties'
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'dbo', 'VIEW', N't_View_KDT_GC_HCT_HD', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'dbo', 'VIEW', N't_View_KDT_GC_HCT_HD', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'dbo', 'VIEW', N't_View_KDT_GC_HMD_HD', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'dbo', 'VIEW', N't_View_KDT_GC_HMD_HD', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_dropextendedproperty N'MS_DiagramPane1', 'SCHEMA', N'dbo', 'VIEW', N'v_KDT_SP_Xuat_HD', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_dropextendedproperty N'MS_DiagramPaneCount', 'SCHEMA', N'dbo', 'VIEW', N'v_KDT_SP_Xuat_HD', NULL, NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping foreign keys from [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] DROP
CONSTRAINT [FK_t_HangMauDich_t_ToKhaiMauDich]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] DROP CONSTRAINT [IX_t_KDT_HangMauDich]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Dropping constraints from [dbo].[t_KDT_VNACC_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich] DROP CONSTRAINT [DF__t_KDT_VNA__HopDo__5EB688AA]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_GC_CungUng]'
GO
ALTER TABLE [dbo].[t_KDT_GC_CungUng] ADD
[ThanhTien] [numeric] (24, 6) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_Load]'
GO
SET QUOTED_IDENTIFIER OFF
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongCungUng],
	[ThanhTien]
FROM
	[dbo].[t_KDT_GC_CungUng]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongCungUng],
	[ThanhTien]
FROM
	[dbo].[t_KDT_GC_CungUng]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_GC_CungUng_ChungTu]'
GO
ALTER TABLE [dbo].[t_KDT_GC_CungUng_ChungTu] ADD
[DonGia] [numeric] (24, 6) NULL,
[TriGia] [numeric] (24, 6) NULL,
[TyGia] [numeric] (24, 6) NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_ChungTu_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_Insert]
	@Master_ID bigint,
	@SoChungTu varchar(50),
	@NgayChungTu datetime,
	@LoaiChungTu int,
	@NoiCap nvarchar(255),
	@SoLuong numeric(24, 4),
	@DonGia numeric(24, 6),
	@TriGia numeric(24, 6),
	@TyGia numeric(24, 6),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_CungUng_ChungTu]
(
	[Master_ID],
	[SoChungTu],
	[NgayChungTu],
	[LoaiChungTu],
	[NoiCap],
	[SoLuong],
	[DonGia],
	[TriGia],
	[TyGia]
)
VALUES 
(
	@Master_ID,
	@SoChungTu,
	@NgayChungTu,
	@LoaiChungTu,
	@NoiCap,
	@SoLuong,
	@DonGia,
	@TriGia,
	@TyGia
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_ChungTu_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_Update]
	@ID bigint,
	@Master_ID bigint,
	@SoChungTu varchar(50),
	@NgayChungTu datetime,
	@LoaiChungTu int,
	@NoiCap nvarchar(255),
	@SoLuong numeric(24, 4),
	@DonGia numeric(24, 6),
	@TriGia numeric(24, 6),
	@TyGia numeric(24, 6)
AS

UPDATE
	[dbo].[t_KDT_GC_CungUng_ChungTu]
SET
	[Master_ID] = @Master_ID,
	[SoChungTu] = @SoChungTu,
	[NgayChungTu] = @NgayChungTu,
	[LoaiChungTu] = @LoaiChungTu,
	[NoiCap] = @NoiCap,
	[SoLuong] = @SoLuong,
	[DonGia] = @DonGia,
	[TriGia] = @TriGia,
	[TyGia] = @TyGia
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_ChungTu_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SoChungTu varchar(50),
	@NgayChungTu datetime,
	@LoaiChungTu int,
	@NoiCap nvarchar(255),
	@SoLuong numeric(24, 4),
	@DonGia numeric(24, 6),
	@TriGia numeric(24, 6),
	@TyGia numeric(24, 6)
	
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_CungUng_ChungTu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_CungUng_ChungTu] 
		SET
			[Master_ID] = @Master_ID,
			[SoChungTu] = @SoChungTu,
			[NgayChungTu] = @NgayChungTu,
			[LoaiChungTu] = @LoaiChungTu,
			[NoiCap] = @NoiCap,
			[SoLuong] = @SoLuong,
			[DonGia] = @DonGia,
			[TriGia] = @TriGia,
			[TyGia] = @TyGia
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_CungUng_ChungTu]
		(
			[Master_ID],
			[SoChungTu],
			[NgayChungTu],
			[LoaiChungTu],
			[NoiCap],
			[SoLuong],
			[DonGia],
			[TriGia],
			[TyGia]
		)
		VALUES 
		(
			@Master_ID,
			@SoChungTu,
			@NgayChungTu,
			@LoaiChungTu,
			@NoiCap,
			@SoLuong,
			@DonGia,
			@TriGia,
			@TyGia
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_ChungTu_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_Load]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoChungTu],
	[NgayChungTu],
	[LoaiChungTu],
	[NoiCap],
	[SoLuong],
	[DonGia],
	[TriGia],
	[TyGia]
FROM
	[dbo].[t_KDT_GC_CungUng_ChungTu]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_ChungTu_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_SelectAll]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoChungTu],
	[NgayChungTu],
	[LoaiChungTu],
	[NoiCap],
	[SoLuong],
	[DonGia],
	[TriGia],
	[TyGia]
FROM
	[dbo].[t_KDT_GC_CungUng_ChungTu]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_GC_NPLHuy]'
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[t_KDT_GC_NPLHuy]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[Master_ID] [bigint] NOT NULL,
[MaNPL] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[TenNPL] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaHS] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DVT_ID] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LuongNPLHuy] [numeric] (24, 4) NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_GC_NPLHuy] on [dbo].[t_KDT_GC_NPLHuy]'
GO
ALTER TABLE [dbo].[t_KDT_GC_NPLHuy] ADD CONSTRAINT [PK_t_KDT_GC_NPLHuy] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuy_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuy_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuy_Insert]
	@Master_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(6),
	@LuongNPLHuy numeric(24, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_NPLHuy]
(
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongNPLHuy]
)
VALUES 
(
	@Master_ID,
	@MaNPL,
	@TenNPL,
	@MaHS,
	@DVT_ID,
	@LuongNPLHuy
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuy_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuy_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuy_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(6),
	@LuongNPLHuy numeric(24, 4)
AS

UPDATE
	[dbo].[t_KDT_GC_NPLHuy]
SET
	[Master_ID] = @Master_ID,
	[MaNPL] = @MaNPL,
	[TenNPL] = @TenNPL,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[LuongNPLHuy] = @LuongNPLHuy
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuy_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuy_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuy_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(6),
	@LuongNPLHuy numeric(24, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_NPLHuy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_NPLHuy] 
		SET
			[Master_ID] = @Master_ID,
			[MaNPL] = @MaNPL,
			[TenNPL] = @TenNPL,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[LuongNPLHuy] = @LuongNPLHuy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_NPLHuy]
		(
			[Master_ID],
			[MaNPL],
			[TenNPL],
			[MaHS],
			[DVT_ID],
			[LuongNPLHuy]
		)
		VALUES 
		(
			@Master_ID,
			@MaNPL,
			@TenNPL,
			@MaHS,
			@DVT_ID,
			@LuongNPLHuy
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuy_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuy_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_NPLHuy]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuy_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuy_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongNPLHuy]
FROM
	[dbo].[t_KDT_GC_NPLHuy]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuy_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuy_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuy_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongNPLHuy]
FROM
	[dbo].[t_KDT_GC_NPLHuy]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[t_KDT_GC_NPLHuyDangKy]'
GO
CREATE TABLE [dbo].[t_KDT_GC_NPLHuyDangKy]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[SoTiepNhan] [bigint] NULL,
[NgayTiepNhan] [datetime] NULL,
[HopDong_ID] [bigint] NULL,
[SoHopDong] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NgayHopDong] [datetime] NULL,
[NgayHetHan] [datetime] NULL,
[TrangThaiXuLy] [int] NULL,
[MaHQ] [varchar] (6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MaDoanhNghiep] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[GuidStr] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[DeXuatKhac] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[LyDoHuy] [nvarchar] (250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_GC_NPLHuyDangKy] on [dbo].[t_KDT_GC_NPLHuyDangKy]'
GO
ALTER TABLE [dbo].[t_KDT_GC_NPLHuyDangKy] ADD CONSTRAINT [PK_t_KDT_GC_NPLHuyDangKy] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuyDangKy_Insert]'
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuyDangKy_Insert]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuyDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@HopDong_ID bigint,
	@SoHopDong varchar(100),
	@NgayHopDong datetime,
	@NgayHetHan datetime,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoHuy nvarchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_NPLHuyDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoHuy]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@HopDong_ID,
	@SoHopDong,
	@NgayHopDong,
	@NgayHetHan,
	@TrangThaiXuLy,
	@MaHQ,
	@MaDoanhNghiep,
	@GuidStr,
	@DeXuatKhac,
	@LyDoHuy
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuyDangKy_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuyDangKy_Update]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuyDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@HopDong_ID bigint,
	@SoHopDong varchar(100),
	@NgayHopDong datetime,
	@NgayHetHan datetime,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoHuy nvarchar(250)
AS

UPDATE
	[dbo].[t_KDT_GC_NPLHuyDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[HopDong_ID] = @HopDong_ID,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[NgayHetHan] = @NgayHetHan,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[GuidStr] = @GuidStr,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoHuy] = @LyDoHuy
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuyDangKy_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuyDangKy_InsertUpdate]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuyDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@HopDong_ID bigint,
	@SoHopDong varchar(100),
	@NgayHopDong datetime,
	@NgayHetHan datetime,
	@TrangThaiXuLy int,
	@MaHQ varchar(6),
	@MaDoanhNghiep varchar(50),
	@GuidStr varchar(100),
	@DeXuatKhac nvarchar(250),
	@LyDoHuy nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_NPLHuyDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_NPLHuyDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[HopDong_ID] = @HopDong_ID,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[NgayHetHan] = @NgayHetHan,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[GuidStr] = @GuidStr,
			[DeXuatKhac] = @DeXuatKhac,
			[LyDoHuy] = @LyDoHuy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_NPLHuyDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[HopDong_ID],
			[SoHopDong],
			[NgayHopDong],
			[NgayHetHan],
			[TrangThaiXuLy],
			[MaHQ],
			[MaDoanhNghiep],
			[GuidStr],
			[DeXuatKhac],
			[LyDoHuy]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@HopDong_ID,
			@SoHopDong,
			@NgayHopDong,
			@NgayHetHan,
			@TrangThaiXuLy,
			@MaHQ,
			@MaDoanhNghiep,
			@GuidStr,
			@DeXuatKhac,
			@LyDoHuy
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuyDangKy_Delete]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuyDangKy_Delete]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuyDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_GC_NPLHuyDangKy]
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuyDangKy_Load]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuyDangKy_Load]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuyDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoHuy]
FROM
	[dbo].[t_KDT_GC_NPLHuyDangKy]
WHERE
	[ID] = @ID
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_HCT_HD]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_HCT_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuyDangKy_SelectAll]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuyDangKy_SelectAll]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuyDangKy_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoHuy]
FROM
	[dbo].[t_KDT_GC_NPLHuyDangKy]	

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_Insert]'
GO
SET QUOTED_IDENTIFIER OFF
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Insert]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_Insert]
	@Master_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(6),
	@LuongCungUng numeric(24, 4),
	@ThanhTien numeric(24, 6),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_GC_CungUng]
(
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongCungUng],
	[ThanhTien]
)
VALUES 
(
	@Master_ID,
	@MaNPL,
	@TenNPL,
	@MaHS,
	@DVT_ID,
	@LuongCungUng,
	@ThanhTien
)

SET @ID = SCOPE_IDENTITY()

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_Update]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_Update]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(6),
	@LuongCungUng numeric(24, 4),
	@ThanhTien numeric(24, 6)
AS

UPDATE
	[dbo].[t_KDT_GC_CungUng]
SET
	[Master_ID] = @Master_ID,
	[MaNPL] = @MaNPL,
	[TenNPL] = @TenNPL,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[LuongCungUng] = @LuongCungUng,
	[ThanhTien] = @ThanhTien
WHERE
	[ID] = @ID

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_InsertUpdate]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaNPL nvarchar(100),
	@TenNPL nvarchar(255),
	@MaHS varchar(10),
	@DVT_ID varchar(6),
	@LuongCungUng numeric(24, 4),
	@ThanhTien numeric(24, 6)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_GC_CungUng] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_GC_CungUng] 
		SET
			[Master_ID] = @Master_ID,
			[MaNPL] = @MaNPL,
			[TenNPL] = @TenNPL,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[LuongCungUng] = @LuongCungUng,
			[ThanhTien] = @ThanhTien
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_GC_CungUng]
		(
			[Master_ID],
			[MaNPL],
			[TenNPL],
			[MaHS],
			[DVT_ID],
			[LuongCungUng],
			[ThanhTien]
		)
		VALUES 
		(
			@Master_ID,
			@MaNPL,
			@TenNPL,
			@MaHS,
			@DVT_ID,
			@LuongCungUng,
			@ThanhTien
		)		
	END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[t_KDT_VNACC_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich] ALTER COLUMN [HopDong_ID] [bigint] NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_ALL_BC07HSTK_GC_TT38]'
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		minhnd
-- Create date: 16/05/2015
-- Description:	All Máy móc, Thiết bị
-- =============================================

CREATE PROCEDURE [dbo].[p_GC_ALL_BC07HSTK_GC_TT38] 
	-- Add the parameters for the stored procedure here
	--@HD_ID BIGINT,
	@MaHaiQuan CHAR(6),
	@MaDoanhNghiep VARCHAR(14)	
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


SELECT  ROW_NUMBER() OVER ( ORDER BY ThietBi.SoHopDong ) AS 'STT' ,
        ThietBi.SoHopDong ,
        --ThietBi.NgayKy ,
        --ThietBi.NgayHetHan ,
        --ThietBi.SoToKhai ,
        --ThietBi.NgayDangKy ,
        --MAX(ThietBi.MaLoaiHinh) as MaLoaiHinh ,
        ThietBi.MaHang,
        MAX(ThietBi.TenHang) as TenHang ,
        MAX(t_HaiQuan_DonViTinh.Ten) AS DVT ,
        SUM(ThietBi.SoLuongTamNhap) as SoLuongTamNhap ,
        SUM(ThietBi.SoLuongTaiXuat) as SoLuongTaiXuat ,
        case when (SUM(ThietBi.SoLuongTamNhap) - SUM(ThietBi.SoLuongTaiXuat))<0 then 0 else (SUM(ThietBi.SoLuongTamNhap) - SUM(ThietBi.SoLuongTaiXuat)) end as ConLai,
        MAX(ThietBi.GhiChu) as GhiChu
        
        --SUM(ABS(ThietBi.SoLuongTamNhap - ThietBi.SoLuongTaiXuat)) AS ConLai
--select *
FROM    ( SELECT    t_KDT_GC_HopDong.SoHopDong ,
                    t_KDT_GC_HopDong.NgayKy ,
                    t_KDT_GC_HopDong.NgayHetHan ,
                    CASE WHEN MaLoaiHinh LIKE '%V%'
                         THEN ( SELECT TOP 1
                                        SoTKVNACCS
                                FROM    dbo.t_VNACCS_CapSoToKhai
                                WHERE   SoTK = t_KDT_ToKhaiMauDich.SoToKhai
                              )
                         ELSE SoToKhai
                    END AS SoToKhai ,
                    t_KDT_ToKhaiMauDich.NgayDangKy ,
                    t_KDT_HangMauDich.ID ,
                    t_KDT_HangMauDich.TKMD_ID AS ID_ToKhai ,
                    t_KDT_HangMauDich.MaPhu AS MaHang ,
                    t_KDT_HangMauDich.TenHang ,
                    t_KDT_HangMauDich.DVT_ID AS DVT ,
                    --t_KDT_ToKhaiMauDich.SoToKhai ,
                    --t_KDT_ToKhaiMauDich.NgayDangKy ,
                    --t_KDT_HangMauDich.SoLuong AS SoLuongTamNhap ,
                    --0 AS SoLuongTaiXuat ,
                    case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%N%' then t_KDT_HangMauDich.SoLuong else 0 end AS SoLuongTamNhap, 
                                              case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%X%' then t_KDT_HangMauDich.SoLuong else 0 end AS SoLuongTaiXuat, 
                    t_KDT_ToKhaiMauDich.MaLoaiHinh ,
                    t_KDT_ToKhaiMauDich.MaHaiQuan ,
                    t_KDT_ToKhaiMauDich.MaDoanhNghiep,
                    Case when t_KDT_ToKhaiMauDich.MaLoaiHinh like '%V%' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai = (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_ToKhaiMauDich.SoToKhai)) else (select top 1 SoHopDong from t_KDT_ToKhaiMauDich where SoToKhai = t_KDT_ToKhaiMauDich.SoToKhai)  end AS GhiChu

          FROM      t_KDT_HangMauDich
                    INNER JOIN t_KDT_ToKhaiMauDich ON t_KDT_HangMauDich.TKMD_ID = t_KDT_ToKhaiMauDich.ID
                    INNER JOIN t_KDT_GC_HopDong ON t_KDT_GC_HopDong.ID = t_KDT_ToKhaiMauDich.IDHopDong
          WHERE     ( t_KDT_ToKhaiMauDich.LoaiHangHoa = 'T' )
                    AND t_KDT_GC_HopDong.NamTN >= 2014
                    or t_KDT_ToKhaiMauDich.MaLoaiHinh = 'G23'

--select * from t_KDT_ToKhaiMauDich where ID = 10377
                       --AND (t_KDT_ToKhaiMauDich.IDHopDong = @HD_ID) 
                       --minhnd	fix thiết bị
                       --AND (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%NGC%')
                       --Or (t_KDT_ToKhaiMauDich.MaLoaiHinh = 'NVG13')
                       --minhnd	fix thiết bị
                       
                       --minhnd	fix hangmau
                       --UNION
                       --SELECT     0 as ID, 0 AS ID_ToKhai, t_GC_HangMau.Ma as MaHang, 
                       --                      t_GC_HangMau.Ten as TenHang, t_GC_HangMau.DVT_ID AS DVT, t_KDT_ToKhaiMauDich.SoToKhai, 
                       --                      t_KDT_ToKhaiMauDich.NgayDangKy, t_GC_HangMau.SoLuongDangKy AS SoLuongTamNhap, 0 AS SoLuongTaiXuat, 
                       --                      t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.MaHaiQuan AS MaHaiQuan, 
                       --                      t_KDT_ToKhaiMauDich.MaDoanhNghiep
                       --FROM         t_KDT_ToKhaiMauDich INNER JOIN
                       --                      t_GC_HangMau ON t_KDT_ToKhaiMauDich.IDHopDong = t_GC_HangMau.HopDong_ID
                       --WHERE     (t_GC_HangMau.HopDong_ID = @HD_ID)
                       
                       --minhnd	fix hangmau
          UNION
          SELECT    t_KDT_GC_HopDong.SoHopDong ,
                    t_KDT_GC_HopDong.NgayKy ,
                    t_KDT_GC_HopDong.NgayHetHan ,
                    CASE WHEN MaLoaiHinh LIKE '%V%'
                         THEN ( SELECT TOP 1
                                        SoTKVNACCS
                                FROM    dbo.t_VNACCS_CapSoToKhai
                                WHERE   SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai
                              )
                         ELSE SoToKhai
                    END AS SoToKhai ,
                    t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,
                    t_KDT_GC_HangChuyenTiep.ID ,
                    t_KDT_GC_HangChuyenTiep.Master_ID AS ID_ToKhai ,
                    t_KDT_GC_HangChuyenTiep.MaHang ,
                    t_KDT_GC_HangChuyenTiep.TenHang ,
                    t_KDT_GC_HangChuyenTiep.ID_DVT AS DVT ,
                    --t_KDT_GC_ToKhaiChuyenTiep.SoToKhai ,
                    --t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,
                    --0 AS SoLuongTamNhap ,
                    --t_KDT_GC_HangChuyenTiep.SoLuong AS SoLuongTaiXuat ,
                    case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%N%' then t_KDT_GC_HangChuyenTiep.SoLuong else 0 end AS SoLuongTamNhap, 
                                              case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%X%' then t_KDT_GC_HangChuyenTiep.SoLuong else 0 end AS SoLuongTaiXuat, 
                    t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,
                    t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan AS MaHaiQuan ,
                    t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep,
                    Case when t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%V%' then (select top 1 GhiChu from t_KDT_VNACC_ToKhaiMauDich where SoToKhai = (select top 1 SoTKVNACCS from t_VNACCS_CapSoToKhai where SoTK = t_KDT_GC_ToKhaiChuyenTiep.SoToKhai)) else (select top 1 SoHopDong from t_KDT_ToKhaiMauDich where SoToKhai = t_KDT_ToKhaiMauDich.SoToKhai)  end AS GhiChu
          FROM      t_KDT_GC_ToKhaiChuyenTiep
                    INNER JOIN t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                    INNER JOIN dbo.t_KDT_GC_HopDong ON t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = t_KDT_GC_HopDong.ID
          WHERE     ( t_KDT_GC_HopDong.NamTN >= 2014 )
                    AND ( t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%TBN%' OR
                    t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%G22%' 
                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh in ('NGC20','XGC20')
                          or t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh like '%V%'
                          and t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa = 'T'
                        )
        ) AS ThietBi
        INNER JOIN t_HaiQuan_DonViTinh ON ThietBi.DVT = t_HaiQuan_DonViTinh.ID
GROUP BY ThietBi.SoHopDong ,
        --ThietBi.NgayKy ,
        --ThietBi.NgayHetHan ,
        --ThietBi.SoToKhai ,
        --ThietBi.NgayDangKy ,
        --ThietBi.MaLoaiHinh,
        ThietBi.MaHang ,
        --ThietBi.TenHang ,
        --t_HaiQuan_DonViTinh.Ten ,
        ThietBi.MaHaiQuan ,
        ThietBi.MaDoanhNghiep
HAVING  ( ThietBi.MaHaiQuan = 'C34C' )
        AND ( ThietBi.MaDoanhNghiep = '0400101556' )
ORDER BY ThietBi.SoHopDong, ThietBi.MaHang

end
--select * from t_KDT_ToKhaiMauDich where NgayDangKy = '2014-11-24'
--update t_KDT_VNACC_ToKhaiMauDich set TrangThaiXuLy = 0 where SoToKhai = 100370301730
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[t_View_KDT_GC_HMD_HD]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_GC_HMD_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [dbo].[v_KDT_SP_Xuat_HD]'
GO
EXEC sp_refreshview N'[dbo].[v_KDT_SP_Xuat_HD]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_GC_GetDinhMucSanPhamForBC06_TT38]'
GO
CREATE PROCEDURE [dbo].[p_GC_GetDinhMucSanPhamForBC06_TT38]         
 -- Add the parameters for the stored procedure here        
    @IDHopDong BIGINT ,
    @MaHaiQuan CHAR(6) ,
    @MaDoanhNghiep VARCHAR(14) ,
    @TuNgay DATETIME ,
    @DenNgay DATETIME
AS 
    BEGIN    
        SELECT DISTINCT
                --ROW_NUMBER() OVER ( ORDER BY v_KDT_SP_Xuat_HD.MaPhu ) AS 'STT' ,
                CONVERT(VARCHAR(50), ( CASE WHEN ToKhai.MaLoaiHinh LIKE '%V%'
                                            THEN ( SELECT TOP 1
                                                            SoTKVNACCS
                                                   FROM     t_VNACCS_CapSoToKhai
                                                   WHERE    SoTK = ToKhai.SoToKhai
                                                 )
                                            ELSE ToKhai.SoToKhai
                                       END )) AS SoToKhai ,
                ToKhai.MaLoaiHinh ,
                ToKhai.NgayDangKy ,
                v_KDT_SP_Xuat_HD.MaPhu AS MaHang ,
                ToKhai.TenHang ,
                '' AS DVT ,
                ToKhai.SoLuong ,
                ToKhai.SoLuong AS TongCong ,
                DM.MaNguyenPhuLieu ,
                CONVERT(DECIMAL(16,6),DM.DinhMucSuDung) AS DinhMucSuDung ,
                CONVERT(DECIMAL(16,6),DM.TyLeHaoHut) AS TyLeHaoHut ,
                CONVERT(DECIMAL(24,6),ToKhai.SoLuong * DM.DinhMucSuDung) AS LuongNPLChuaHH ,
                ( ToKhai.SoLuong * DM.DinhMucSuDung ) + ( ToKhai.SoLuong
                                                          * ( DM.DinhMucSuDung
                                                              * ( DM.TyLeHaoHut
                                                              / 100 ) ) ) AS LuongNPLCoHH
        FROM    v_KDT_SP_Xuat_HD
                INNER JOIN ( SELECT t_KDT_ToKhaiMauDich.SoToKhai ,
                                    t_KDT_ToKhaiMauDich.MaLoaiHinh ,
                                    t_KDT_ToKhaiMauDich.NgayDangKy ,
                                    t_KDT_ToKhaiMauDich.IDHopDong ,
                                    t_KDT_ToKhaiMauDich.MaHaiQuan ,
                                    t_KDT_HangMauDich.MaPhu AS MaHang ,
                                    t_KDT_HangMauDich.TenHang ,
                                    t_KDT_HangMauDich.SoLuong ,
                                    t_KDT_ToKhaiMauDich.MaDoanhNghiep
                             FROM   t_KDT_ToKhaiMauDich
                                    INNER JOIN t_KDT_HangMauDich ON t_KDT_ToKhaiMauDich.ID = t_KDT_HangMauDich.TKMD_ID
                             WHERE  ( t_KDT_ToKhaiMauDich.TrangThaiXuLy = 1 )
                                    AND ( t_KDT_ToKhaiMauDich.IDHopDong = @IDHopDong )
                                    AND ( t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE 'X%' )
                                    AND ( t_KDT_ToKhaiMauDich.LoaiHangHoa = 'S' )
                             UNION
                             SELECT t_KDT_GC_ToKhaiChuyenTiep.SoToKhai ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh ,
                                    t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy ,
                                    t_KDT_GC_ToKhaiChuyenTiep.IDHopDong ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan ,
                                    t_KDT_GC_HangChuyenTiep.MaHang ,
                                    t_KDT_GC_HangChuyenTiep.TenHang ,
                                    t_KDT_GC_HangChuyenTiep.SoLuong ,
                                    t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep
                             FROM   t_KDT_GC_ToKhaiChuyenTiep
                                    INNER JOIN t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID
                             WHERE  ( t_KDT_GC_ToKhaiChuyenTiep.TrangThaiXuLy = 1 )
                                    AND ( t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @IDHopDong )
                                    AND ( t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'PHSPX'
                                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XGC19'
                                          OR t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh = 'XVE54'
                                        )
                           ) AS ToKhai ON v_KDT_SP_Xuat_HD.IDHopDong = ToKhai.IDHopDong
                                          AND v_KDT_SP_Xuat_HD.MaPhu = ToKhai.MaHang
                INNER JOIN t_HaiQuan_DonViTinh ON v_KDT_SP_Xuat_HD.DVT_ID = t_HaiQuan_DonViTinh.ID
                INNER JOIN t_HaiQuan_LoaiHinhMauDich ON ToKhai.MaLoaiHinh = t_HaiQuan_LoaiHinhMauDich.ID
                INNER JOIN dbo.t_GC_DinhMuc AS DM ON ToKhai.MaHang = DM.MaSanPham
                                                     AND ToKhai.IDHopDong = DM.HopDong_ID
        WHERE   ( v_KDT_SP_Xuat_HD.IDHopDong = @IDHopDong )
                AND ( ToKhai.MaDoanhNghiep = @MaDoanhNghiep )
                AND ( ToKhai.MaHaiQuan = @MaHaiQuan )
                AND ToKhai.NgayDangKy >= @TuNgay
                AND ToKhai.NgayDangKy <= @DenNgay
ORDER BY        MaNguyenPhuLieu, 
                ToKhai.NgayDangKy,
                MaHang     
    END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_SelectDynamic]'
GO
SET QUOTED_IDENTIFIER OFF
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 22, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongCungUng],
	[ThanhTien]
FROM [dbo].[t_KDT_GC_CungUng] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_CungUng_ChungTu_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_GC_CungUng_ChungTu_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_HT
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 20, 2014
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_GC_CungUng_ChungTu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SoChungTu],
	[NgayChungTu],
	[LoaiChungTu],
	[NoiCap],
	[SoLuong],
	[DonGia],
	[TriGia],
	[TyGia]
FROM [dbo].[t_KDT_GC_CungUng_ChungTu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuy_DeleteDynamic]'
GO
SET QUOTED_IDENTIFIER ON
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuy_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_NPLHuy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuy_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuy_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaNPL],
	[TenNPL],
	[MaHS],
	[DVT_ID],
	[LuongNPLHuy]
FROM [dbo].[t_KDT_GC_NPLHuy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuyDangKy_DeleteDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuyDangKy_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuyDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_GC_NPLHuyDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLHuyDangKy_SelectDynamic]'
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_NPLHuyDangKy_SelectDynamic]
-- Database: ECS_TQDT_GC_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, January 19, 2016
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_NPLHuyDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[HopDong_ID],
	[SoHopDong],
	[NgayHopDong],
	[NgayHetHan],
	[TrangThaiXuLy],
	[MaHQ],
	[MaDoanhNghiep],
	[GuidStr],
	[DeXuatKhac],
	[LyDoHuy]
FROM [dbo].[t_KDT_GC_NPLHuyDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding constraints to [dbo].[t_KDT_VNACC_ToKhaiMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_VNACC_ToKhaiMauDich] ADD CONSTRAINT [DF__t_KDT_VNA__HopDo__7EC430E0] DEFAULT ('0') FOR [HopDong_ID]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [dbo].[t_KDT_HangMauDich]'
GO
ALTER TABLE [dbo].[t_KDT_HangMauDich] ADD
CONSTRAINT [FK_t_HangMauDich_t_ToKhaiMauDich] FOREIGN KEY ([TKMD_ID]) REFERENCES [dbo].[t_KDT_ToKhaiMauDich] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '22.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('22.2',GETDATE(), N'NPL_CU vs NPL Hủy vào mẫu 15')
END	