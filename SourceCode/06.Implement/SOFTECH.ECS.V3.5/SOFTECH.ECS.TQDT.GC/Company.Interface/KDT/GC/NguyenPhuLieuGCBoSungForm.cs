﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Linq;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
namespace Company.Interface.KDT.GC
{
    public partial class NguyenPhuLieuGCBoSungForm : BaseFormHaveGuidPanel
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPKMuaVN = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
        public string MaLoaiPK = string.Empty;
        public bool boolFlag;
        public String Caption;
        public bool IsChange;

        public NguyenPhuLieuGCBoSungForm()
        {
            InitializeComponent();

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = LoaiPK.HPKCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (pkdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || pkdk.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || pkdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void SetData()
        {
            try
            {
                txtMa.TextChanged -= new EventHandler(txt_TextChanged);
                txtMa.Text = HangPK.MaHang.Trim();
                txtMa.TextChanged += new EventHandler(txt_TextChanged);

                txtTen.TextChanged -= new EventHandler(txt_TextChanged);
                txtTen.Text = HangPK.TenHang.Trim();
                txtTen.TextChanged += new EventHandler(txt_TextChanged);

                txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaHS.Text = HangPK.MaHS.Trim();
                txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

                cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
                cbDonViTinh.SelectedValue = HangPK.DVT_ID;
                cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

                cbbNguonGoc.TextChanged -= new EventHandler(txt_TextChanged);
                cbbNguonGoc.SelectedValue = HangPK.TinhTrang;
                cbbNguonGoc.TextChanged += new EventHandler(txt_TextChanged);

                txtMaCu.TextChanged -= new EventHandler(txt_TextChanged);
                txtMaCu.Text = HangPK.ThongTinCu;
                txtMaCu.TextChanged += new EventHandler(txt_TextChanged);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void NguyenPhuLieuGCEditForm_Load(object sender, EventArgs e)
        {
            bool isSuaSP = false;
            isSuaSP = MaLoaiPK == "503";
            dgList.RootTable.Columns["ThongTinCu"].Visible =
            lblCu.Visible = txtMaCu.Visible = lblTenCu.Visible = isSuaSP;
            txtMa.Focus();

            //collumn Soluong
            dgList.Tables[0].Columns[7].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            //collumn Dongia
            dgList.Tables[0].Columns[8].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            Caption = this.Text;

            if (_DonViTinh == null || _DonViTinh.Rows.Count == 0)
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";

            cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;


            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == MaLoaiPK)
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (LoaiPK.HPKCollection.Count == 0) LoaiPK.LoadCollection();
            if (this.OpenType == OpenFormType.View)
            {
                btnAdd.Enabled = false;
                btnXoa.Enabled = false;
                btnAddExcel.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
            else
            {

                btnAdd.Enabled = true;
                btnXoa.Enabled = true;
                btnAddExcel.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            if (boolFlag == true)
            {
                btnAdd.Enabled = true;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;

            }

            LoaiPK.MaPhuKien = MaLoaiPK;
            if (MaLoaiPK == "103")
            {
                txtMa.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            }
            BindData();
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {

        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                if (MaLoaiPK == "503")
                {
                    isValid &= ValidateControl.ValidateSpecialChar(txtMaCu, errorProvider, "Mã nguyên phụ liệu", isOnlyWarning);
                    isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã nguyên phụ liệu", isOnlyWarning);
                }
                else if (MaLoaiPK == "803")
                {
                    isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã nguyên phụ liệu", isOnlyWarning);
                }
                else
                {
                    isValid &= ValidateControl.ValidateSpecialChar(txtMaCu, errorProvider, "Mã nguyên phụ liệu", isOnlyWarning);
                }
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
            return isValid;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(MaLoaiPK))
            {
                ShowMessage("Mã loại phụ kiện chưa được xác nhận", false);
                DialogResult = DialogResult.Cancel;
            };
            if (!ValidateForm(false))
                return;
            txtMa.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            {
                checkExitsNPLAndSTTHang();
            }
        }

        private void checkExitsNPLAndSTTHang()
        {
            try
            {
                Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                npl.Ma = txtMa.Text.Trim();
                npl.HopDong_ID = this.HD.ID;

                if (npl.Load())
                {
                    if (npl.Ma.Trim().ToUpper() != HangPK.MaHang.Trim().ToUpper())
                    {
                        showMsg("MSG_2702056");
                        txtMa.Focus();
                        return;
                    }
                }
                LoaiPK.HPKCollection.Remove(HangPK);
                foreach (HangPhuKien HPK in LoaiPK.HPKCollection)
                {
                    if (HPK.MaHang == txtMa.Text.Trim())
                    {
                        showMsg("MSG_2702056");
                        txtMa.Focus();
                        if (HangPK.MaHang.Trim().Length > 0)
                            LoaiPK.HPKCollection.Add(HangPK);
                        return;
                    }
                }
                HangPK.Delete(LoaiPK.MaPhuKien, HD.ID);
                HangPK.ID = 0;
                HangPK.TenHang = txtTen.Text.Trim();
                HangPK.MaHang = txtMa.Text.Trim();
                HangPK.MaHS = txtMaHS.Text.Trim();
                HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                HangPK.ThongTinCu = txtMaCu.Text.Trim();
                HangPK.TinhTrang = cbbNguonGoc.SelectedValue == null ? "1" : cbbNguonGoc.SelectedValue.ToString();
                LoaiPK.HPKCollection.Add(HangPK);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                LoaiPK.InsertUpdateBoSungNPL(pkdk.HopDong_ID);
                reset();
                this.setErro();
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }


        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                HangPK = (HangPhuKien)e.Row.DataRow;
                SetData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();

        }
        public bool CheckExitNPLInMuaVN(HangPhuKien HPK)
        {
            foreach (HangPhuKien HangPK in LoaiPKMuaVN.HPKCollection)
            {
                if (HangPK.MaHang.Trim().ToUpper() == HPK.MaHang.Trim().ToUpper())
                {
                    return true;
                }
            }
            return false;
        }
        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private void NguyenPhuLieuGCBoSungForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (LoaiPK.HPKCollection.Count > 0)
            {
                LoaiPK.MaPhuKien = MaLoaiPK;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(MaLoaiPK);
                pkdk.PKCollection.Remove(LoaiPK);
                pkdk.PKCollection.Add(LoaiPK);
            }
            else
            {
                pkdk.PKCollection.Remove(LoaiPK);
                LoaiPK.Delete(pkdk.HopDong_ID); ;
            }
        }

        private void uiGroupBox2_Click(object sender, EventArgs e)
        {

        }
        private void setErro()
        {
            error.Clear();
            error.SetError(txtMa, null);
            error.SetError(txtMaHS, null);
            error.SetError(txtTen, null);


        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (!btnAdd.Enabled) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count < 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {

                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien hpkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (CheckExitNPLInMuaVN(hpkDelete))
                        {
                            showMsg("MSG_DEL04");
                            return;
                        }

                    }
                }
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien hpkDelete = (HangPhuKien)row.GetRow().DataRow;
                        try
                        {
                            if (hpkDelete.ID > 0)
                            {
                                hpkDelete.Delete(LoaiPK.MaPhuKien, pkdk.HopDong_ID);
                            }

                            LoaiPK.HPKCollection.Remove(hpkDelete);
                        }
                        catch { }
                    }
                }
                dgList.DataSource = LoaiPK.HPKCollection;
                this.reset();
                this.setErro();
                BindData();
                this.SetChange(true);
            }

        }

        private void reset()
        {
            txtMa.TextChanged -= new EventHandler(txt_TextChanged);
            txtMa.Text = "";
            txtMa.TextChanged += new EventHandler(txt_TextChanged);

            txtTen.TextChanged -= new EventHandler(txt_TextChanged);
            txtTen.Text = "";
            txtTen.TextChanged += new EventHandler(txt_TextChanged);

            txtMaHS.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaHS.Text = "";
            txtMaHS.TextChanged += new EventHandler(txt_TextChanged);

            cbDonViTinh.TextChanged -= new EventHandler(txt_TextChanged);
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            cbDonViTinh.TextChanged += new EventHandler(txt_TextChanged);

            BindData();
            HangPK = new HangPhuKien();

        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            FormChonNPL frm = new FormChonNPL();
            frm.HD = HD;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (frm.HPKCollection.Count > 0)
                    {
                        if (LoaiPK.HPKCollection.Count == 0)
                        {
                            foreach (HangPhuKien hangPK in frm.HPKCollection)
                            {
                                hangPK.Master_ID = LoaiPK.ID;
                                LoaiPK.HPKCollection.Add(hangPK);
                            }
                        }
                        else
                        {
                            bool IsExits = false;
                            foreach (HangPhuKien hangPK in frm.HPKCollection)
                            {
                                IsExits = false;
                                foreach (HangPhuKien item in LoaiPK.HPKCollection)
                                {
                                    if (hangPK.MaHang == item.MaHang)
                                    {
                                        IsExits = true;
                                        break;
                                    }
                                }
                                if (!IsExits)
                                {
                                    hangPK.Master_ID = LoaiPK.ID;
                                    LoaiPK.HPKCollection.Add(hangPK);
                                }
                            }
                        }
                        this.SetChange(true);
                    }
                    //foreach (HangPhuKien hangPK in frm.HPKCollection)
                    //{
                    //    hangPK.Master_ID = LoaiPK.ID;
                    //    LoaiPK.HPKCollection.Add(hangPK);
                    //}
                    LoaiPK.Master_ID = pkdk.ID;
                    LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                    if (boolFlag == true)
                    {
                        LoaiPK.InsertUpdateBoSungDaDangKy(pkdk.HopDong_ID);
                        boolFlag = false;
                    }
                    else
                    {
                        LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
                    }
                    Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
                    if (lpk == null)
                        pkdk.PKCollection.Add(LoaiPK);
                    dgList.DataSource = LoaiPK.HPKCollection;
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
                reset();
                this.setErro();
            }
        }

        private void btnAddExcel_Click(object sender, EventArgs e)
        {
            try
            {
                Company.Interface.KDT.SXXK.NguyenPhuLieuReadExcelForm f = new Company.Interface.KDT.SXXK.NguyenPhuLieuReadExcelForm();
                f.FormImport = "PK";
                f.HD = HD;
                f.OpenType = this.OpenType;
                f.ShowDialog();
                foreach (NguyenPhuLieu item in f.NPLCollection)
                {
                    LoaiPK.HPKCollection.Add(new HangPhuKien
                    {
                        MaHang = item.Ma.Trim(),
                        TenHang = item.Ten.Trim(),
                        MaHS = item.MaHS,
                        DVT_ID = item.DVT_ID,

                    }
                    );
                }
                this.SetChange(f.ImportExcelSucces);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                if (boolFlag == true)
                {
                    LoaiPK.InsertUpdateBoSungDaDangKy(pkdk.HopDong_ID);
                    boolFlag = false;
                }
                else
                {
                    LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
                }
                Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
                if (lpk == null)
                    pkdk.PKCollection.Add(LoaiPK);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            HangPK = (HangPhuKien)i.GetRow().DataRow;
                            SetData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH NGUYÊN PHỤ LIỆU KHAI BÁO CỦA PHỤ KIỆN.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaNPL.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}