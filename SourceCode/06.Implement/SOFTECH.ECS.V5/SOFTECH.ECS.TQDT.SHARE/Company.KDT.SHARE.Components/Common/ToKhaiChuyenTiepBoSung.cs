﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data;

namespace Company.KDT.SHARE.Components.Common
{
    public partial class ToKhaiChuyenTiepBoSung
    {
        public static int InsertUpdateByTKCT(long TKCT_ID, int PhienBan, bool ChuKySo, string GhiChu, SqlTransaction transaction)
        {
            const string spName = "p_KDT_ToKhaiChuyenTiepBoSung_InsertUpdate_ByTKCT";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKCT_ID", SqlDbType.BigInt, TKCT_ID);
            db.AddInParameter(dbCommand, "@PhienBan", SqlDbType.Int, PhienBan);
            db.AddInParameter(dbCommand, "@ChuKySo", SqlDbType.Bit, ChuKySo);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
    }
}
