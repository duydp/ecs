using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.SXXK.ToKhai;
using Janus.Windows.GridEX;
using GemBox.Spreadsheet;
namespace Company.Interface.SXXK
{
    public partial class ImportTKForm : BaseForm
    {
        public ToKhaiMauDichCollection TKCollection = new ToKhaiMauDichCollection();
        public ThongTinDieuChinhCollection TTDCCollection = new ThongTinDieuChinhCollection();
        public ImportTKForm()
        {
            InitializeComponent();
        }

        private int ConvertStringToInt(string str)
        {
            if (str.Length == 1)
                return str[0] - 'A';
            else
            {
                int t = (str[0] - 'A' + 1)*26 + (str[1] - 'A');
                return t;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (chkSaveSetting.Checked == true)
            {
                //GlobalSettings.Luu_TK(txtMaSPColumn.Text, txtMaNPLColumn.Text, txtTKSDColumn.Text, txtTLHHColumn.Text,txtTKCColumn.Text);
            }
            this.Cursor = Cursors.WaitCursor;
            btnSave.Enabled = false;
            try
            {
                new ToKhaiMauDich().InsertUpdate(this.TKCollection);
                //new ThongTinDieuChinh().InsertUpdate(this.TTDCCollection);
                MLMessages("Nhập thành công.", "MSG_PUB02", "", false);
                //ShowMessage("Import thành công.", false);
            }
            catch(Exception ex)
            {
                ShowMessage("" + ex.Message, false);
            }
            finally
            {
                btnSave.Enabled = true;
                this.Cursor = Cursors.Default;
                this.Close();
            }


        }

        private void ImportTKForm_Load(object sender, EventArgs e)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            //txtMaSPColumn.Text = GlobalSettings.ToKhaiMauDich.MaSP;
            //txtMaNPLColumn.Text = GlobalSettings.ToKhaiMauDich.MaNPL;
            //txtTKSDColumn.Text = GlobalSettings.ToKhaiMauDich.ToKhaiMauDichSuDung;
            //txtTLHHColumn.Text = GlobalSettings.ToKhaiMauDich.TyLeHH;
            //txtTKCColumn.Text = GlobalSettings.ToKhaiMauDich.ToKhaiMauDichChung;
            this.dgList.DataSource = this.TKCollection;
        }
        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {

        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                error.SetIconPadding(txtRow, 8);
                return;

            }
            this.Cursor = Cursors.WaitCursor;
            uiButton1.Enabled = false;
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text,true );
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
                this.uiButton1.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);

                this.uiButton1.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }

            WorksheetRowCollection wsrc = ws.Rows;

            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        if (Convert.ToString(wsr.Cells[ConvertStringToInt(txtSoToKhai.Text)].Value).Length == 0) break;
                        ToKhaiMauDich TK = new ToKhaiMauDich();
                        ThongTinDieuChinh TTDC = new ThongTinDieuChinh();
                        TK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                        TTDC.MaHaiQuan = TK.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                        TTDC.SoToKhai = TK.SoToKhai = Convert.ToInt32(wsr.Cells[ConvertStringToInt(txtSoToKhai.Text)].Value);
                        TTDC.MaLoaiHinh = TK.MaLoaiHinh = Convert.ToString(wsr.Cells[ConvertStringToInt(txtMaLH.Text)].Value);
                        try
                        {
                            TK.NgayDangKy = DateTime.FromOADate((double)wsr.Cells[ConvertStringToInt(txtNgayDangKy.Text)].Value);
                        }
                        catch { }
                        TTDC.NamDangKy = TK.NamDangKy = Convert.ToInt16(TK.NgayDangKy.Year);
                        TTDC.LanDieuChinh = 0;
                        TK.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                        TK.TenDonViDoiTac = Convert.ToString(wsr.Cells[ConvertStringToInt(txtTenDVDT.Text)].Value);
                        TK.ChiTietDonViDoiTac = "";
                        TK.SoGiayPhep = Convert.ToString(wsr.Cells[ConvertStringToInt(txtSoGP.Text)].Value);
                        try
                        {
                            TK.NgayGiayPhep = DateTime.FromOADate((double)wsr.Cells[ConvertStringToInt(txtNgayGP.Text)].Value);
                        }
                        catch
                        { }
                        try
                        {
                            TK.NgayHetHanGiayPhep = Convert.ToDateTime((double)wsr.Cells[ConvertStringToInt(txtNgayHHGP.Text)].Value);
                        }
                        catch
                        { }
                        TK.SoHopDong = Convert.ToString(wsr.Cells[ConvertStringToInt(txtSoHD.Text)].Value);
                        try
                        {
                            TK.NgayHopDong = DateTime.FromOADate((double)wsr.Cells[ConvertStringToInt(txtNgayHD.Text)].Value);
                        }
                        catch
                        { }
                        try
                        {
                            TK.NgayHetHanHopDong = DateTime.FromOADate((double)wsr.Cells[ConvertStringToInt(txtNgayHHHD.Text)].Value);
                        }
                        catch
                        { }
                        TK.SoHoaDonThuongMai= Convert.ToString(wsr.Cells[ConvertStringToInt(txtSoHDTM.Text)].Value);
                        try
                        {
                            TK.NgayHoaDonThuongMai = DateTime.FromOADate((double)wsr.Cells[ConvertStringToInt(txtNgayHDTM.Text)].Value);
                        }
                        catch
                        { }
                        TK.PTVT_ID = Convert.ToString(wsr.Cells[ConvertStringToInt(txtPTVT.Text)].Value);
                        TK.SoHieuPTVT = Convert.ToString(wsr.Cells[ConvertStringToInt(txtSoPTVT.Text)].Value);
                        try
                        {
                            TK.NgayDenPTVT = DateTime.FromOADate((double)wsr.Cells[ConvertStringToInt(txtNgayDen.Text)].Value);
                        }
                        catch
                        { }
                        TK.LoaiVanDon = Convert.ToString(wsr.Cells[ConvertStringToInt(txtLoaiVD.Text)].Value);
                        TK.SoVanDon= Convert.ToString(wsr.Cells[ConvertStringToInt(txtSoVD.Text)].Value);
                        try
                        {
                            TK.NgayVanDon = DateTime.FromOADate((double)wsr.Cells[ConvertStringToInt(txtNgayVD.Text)].Value);
                        }
                        catch { }
                        TK.NuocNK_ID = Convert.ToString(wsr.Cells[ConvertStringToInt(txtNuocNK.Text)].Value);
                        TK.NuocXK_ID = Convert.ToString(wsr.Cells[ConvertStringToInt(txtNuocXK.Text)].Value);
                        TK.DiaDiemXepHang = Convert.ToString(wsr.Cells[ConvertStringToInt(txtDDXH.Text)].Value);
                        TK.CuaKhau_ID = Convert.ToString(wsr.Cells[ConvertStringToInt(txtCuaKhau.Text)].Value);
                        TK.NguyenTe_ID = Convert.ToString(wsr.Cells[ConvertStringToInt(txtNguyenTe.Text)].Value);
                        TK.TyGiaTinhThue = Convert.ToDecimal(wsr.Cells[ConvertStringToInt(txtTyGiaTT.Text)].Value);
                        TK.TyGiaUSD = Convert.ToDecimal(wsr.Cells[ConvertStringToInt(txtTyGiaUSD.Text)].Value);
                        TK.PTTT_ID = Convert.ToString(wsr.Cells[ConvertStringToInt(txtPTTT.Text)].Value);
                        TK.SoLuongPLTK = Convert.ToInt16(wsr.Cells[ConvertStringToInt(txtSoPLTK.Text)].Value);
                        TK.TenChuHang = Convert.ToString(wsr.Cells[ConvertStringToInt(txtTenChuHang.Text)].Value);
                        TK.SoContainer20 = Convert.ToDecimal(wsr.Cells[ConvertStringToInt(txtContainer20.Text)].Value);
                        TK.SoContainer40 = Convert.ToDecimal(wsr.Cells[ConvertStringToInt(txtContainer40.Text)].Value);
                        TK.SoKien = Convert.ToDecimal(wsr.Cells[ConvertStringToInt(txtSoKien.Text)].Value);
                        TK.TrongLuong = Convert.ToDecimal(wsr.Cells[ConvertStringToInt(txtTrongLuong.Text)].Value);
                        TK.TongTriGiaKhaiBao = Convert.ToDecimal(wsr.Cells[ConvertStringToInt(txtTongTGKB.Text)].Value);
                        TK.TongTriGiaTinhThue = Convert.ToDecimal(wsr.Cells[ConvertStringToInt(txtTongTGTT.Text)].Value);
                        TK.LePhiHaiQuan = Convert.ToDecimal(wsr.Cells[ConvertStringToInt(txtLPHQ.Text)].Value);
                        TK.PhiBaoHiem = Convert.ToDecimal(wsr.Cells[ConvertStringToInt(txtPhiBaoHiem.Text)].Value);
                        TK.PhiVanChuyen = Convert.ToDecimal(wsr.Cells[ConvertStringToInt(txtPhiVC.Text)].Value);
                        TK.Xuat_NPL_SP = Convert.ToString(wsr.Cells[ConvertStringToInt(txtNL_SP.Text)].Value);
                        TK.ThanhLy = Convert.ToString(wsr.Cells[ConvertStringToInt(txtThanhLy.Text)].Value);
                        try
                        {
                            TK.NGAY_THN_THX = DateTime.FromOADate((double)wsr.Cells[ConvertStringToInt(txtNgayNhapXuat.Text)].Value);
                        }
                        catch { }
                        try
                        {
                            TK.NgayHoanThanh = DateTime.FromOADate((double)wsr.Cells[ConvertStringToInt(txtNgayHoanThanh.Text)].Value);
                        }
                        catch { }
                        if (TK.MaLoaiHinh.Contains("NSX")) TK.NgayHoanThanh = TK.NGAY_THN_THX = TK.NgayDangKy;
                        TK.MaDonViUT = Convert.ToString(wsr.Cells[ConvertStringToInt(txtMaDVUT.Text)].Value);
                        TK.TrangThaiThanhKhoan = Convert.ToString(wsr.Cells[ConvertStringToInt(txtTTTK.Text)].Value);
                       
                        TK.ChungTu = Convert.ToString(wsr.Cells[ConvertStringToInt(txtChungTu.Text)].Value);
                        TK.PhanLuong = Convert.ToString(wsr.Cells[ConvertStringToInt(txtPhanLuong.Text)].Value);
                        this.TKCollection.Add(TK);
                        this.TTDCCollection.Add(TTDC);
                    }
                    catch
                    {
                        if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                        {
                            this.uiButton1.Enabled = true;
                            this.Cursor = Cursors.Default;
                            return;
                        }
                    }
                }
            }
            dgList.DataSource = this.TKCollection;
            dgList.Refetch();
            this.uiButton1.Enabled = true;
            this.Cursor = Cursors.Default;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {

        }

        private void txtTenDLTTHQ_TextChanged(object sender, EventArgs e)
        {

        }

        private void ImportTKForm_Load_1(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }


 

    }
}