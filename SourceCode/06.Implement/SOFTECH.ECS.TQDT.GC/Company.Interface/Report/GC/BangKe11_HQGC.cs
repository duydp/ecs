﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;
 

namespace Company.Interface.Report.GC
{
    public partial class BangKe11_HQGC : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        public bool First = false;
        public bool Last = false;
        public BangKe11_HQGC()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt)
        {
            //dt.TableName = "t_GC_ThanhKhoanHDGC";
            ReportHeader.Visible = this.First;
            ReportFooter1.Visible = this.Last;
            ReportFooter.Visible = this.Last;
            this.DataSource = dt;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP =="")
                {
                    loaiSP = dr["TenSanPham"].ToString();
                   
                }
                else
                    {
                        loaiSP = loaiSP + "," + dr["TenSanPham"].ToString();
                    }
            }
            lblMathang.Text = loaiSP; 

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong; 
            //
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblSP1.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[4].ColumnName);
            lblSP2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[10].ColumnName  );
            lblDVTSP.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[6].ColumnName );
            lblDVTSP2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[12].ColumnName);
            lblSoluongSP1.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[5].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblSoLuongSP2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[11].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongSP + "}");
            lblDM1.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[7].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDM2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[13].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");          
            lblTLHH1.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[8].ColumnName, "{0:N0}");
            lblTLHH2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[14].ColumnName, "{0:N0}");
            lblLSD1.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[9].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLSD2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[15].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[2].ColumnName );
            lblDVT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".DVT" );
            lblTongLuongCU.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[16].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            xrTableCell7.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[17].ColumnName, "{0:N5}");
            xrTableCell6.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[18].ColumnName, "{0:N2}");
            lblHTCU.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[19].ColumnName);
            xrLabel19.Text = GlobalSettings.TieudeNgay;

        }
    }
}
