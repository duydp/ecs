USE [ECS.TQDT.KD]
GO

/****** Object:  Table [dbo].[t_KDT_KetQuaXuLy]    Script Date: 05/17/2010 02:10:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_KetQuaXuLy](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ReferenceID] [uniqueidentifier] NOT NULL,
	[LoaiChungTu] [varchar](10) NOT NULL,
	[LoaiThongDiep] [nvarchar](100) NOT NULL,
	[NoiDung] [nvarchar](500) NOT NULL,
	[Ngay] [datetime] NOT NULL,
 CONSTRAINT [PK_t_KDT_KetQuaXuLy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N't_KDT_KetQuaXuLy', @level2type=N'COLUMN',@level2name=N'LoaiThongDiep'
GO

ALTER TABLE [dbo].[t_KDT_KetQuaXuLy] ADD  CONSTRAINT [DF_t_KDT_KetQuaXuLy_LoaiChungTu]  DEFAULT ('TK') FOR [LoaiChungTu]
GO

ALTER TABLE [dbo].[t_KDT_KetQuaXuLy] ADD  CONSTRAINT [DF_t_KDT_KetQuaXuLy_CreatedTime]  DEFAULT (getdate()) FOR [Ngay]
GO

