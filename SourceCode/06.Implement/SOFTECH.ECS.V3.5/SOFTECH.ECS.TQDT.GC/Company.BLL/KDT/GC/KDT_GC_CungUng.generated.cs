using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_CungUng : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string MaNPL { set; get; }
		public string TenNPL { set; get; }
		public string MaHS { set; get; }
		public string DVT_ID { set; get; }
		public decimal LuongCungUng { set; get; }
        public decimal ThanhTien { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_CungUng> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_CungUng> collection = new List<KDT_GC_CungUng>();
			while (reader.Read())
			{
				KDT_GC_CungUng entity = new KDT_GC_CungUng();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDecimal(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhTien"))) entity.ThanhTien = reader.GetDecimal(reader.GetOrdinal("ThanhTien"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_CungUng> collection, long id)
        {
            foreach (KDT_GC_CungUng item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_CungUng VALUES(@Master_ID, @MaNPL, @TenNPL, @MaHS, @DVT_ID, @LuongCungUng,@ThanhTien)";
            string update = "UPDATE t_KDT_GC_CungUng SET Master_ID = @Master_ID, MaNPL = @MaNPL, TenNPL = @TenNPL, MaHS = @MaHS, DVT_ID = @DVT_ID, LuongCungUng = @LuongCungUng,ThanhTien = @ThanhTien WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_CungUng WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongCungUng", SqlDbType.Decimal, "LuongCungUng", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongCungUng", SqlDbType.Decimal, "LuongCungUng", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_CungUng VALUES(@Master_ID, @MaNPL, @TenNPL, @MaHS, @DVT_ID, @LuongCungUng.@ThanhTien)";
            string update = "UPDATE t_KDT_GC_CungUng SET Master_ID = @Master_ID, MaNPL = @MaNPL, TenNPL = @TenNPL, MaHS = @MaHS, DVT_ID = @DVT_ID, LuongCungUng = @LuongCungUng,ThanhTien = @ThanhTien WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_CungUng WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongCungUng", SqlDbType.Decimal, "LuongCungUng", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.NVarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongCungUng", SqlDbType.Decimal, "LuongCungUng", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@ThanhTien", SqlDbType.Decimal, "ThanhTien", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_CungUng Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_CungUng_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_CungUng> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_CungUng> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_CungUng> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_CungUng_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_CungUng_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_CungUng_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_CungUng_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_CungUng(long master_ID, string maNPL, string tenNPL, string maHS, string dVT_ID, decimal luongCungUng,decimal thanhTien)
		{
			KDT_GC_CungUng entity = new KDT_GC_CungUng();	
			entity.Master_ID = master_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongCungUng = luongCungUng;
            entity.ThanhTien = thanhTien;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_CungUng_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongCungUng", SqlDbType.Decimal, LuongCungUng);
            db.AddInParameter(dbCommand, "@ThanhTien", SqlDbType.Decimal, ThanhTien);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_CungUng> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_CungUng item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_CungUng(long id, long master_ID, string maNPL, string tenNPL, string maHS, string dVT_ID, decimal luongCungUng,decimal thanhTien)
		{
			KDT_GC_CungUng entity = new KDT_GC_CungUng();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongCungUng = luongCungUng;
            entity.ThanhTien = thanhTien;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_CungUng_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongCungUng", SqlDbType.Decimal, LuongCungUng);
            db.AddInParameter(dbCommand, "@ThanhTien", SqlDbType.Decimal, ThanhTien);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_CungUng> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_CungUng item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_CungUng(long id, long master_ID, string maNPL, string tenNPL, string maHS, string dVT_ID, decimal luongCungUng,decimal ThanhTien)
		{
			KDT_GC_CungUng entity = new KDT_GC_CungUng();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT_ID = dVT_ID;
			entity.LuongCungUng = luongCungUng;
            entity.ThanhTien = ThanhTien;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_CungUng_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.NVarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@LuongCungUng", SqlDbType.Decimal, LuongCungUng);
            db.AddInParameter(dbCommand, "@ThanhTien", SqlDbType.Decimal, ThanhTien);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_CungUng> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_CungUng item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_CungUng(long id)
		{
			KDT_GC_CungUng entity = new KDT_GC_CungUng();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_CungUng_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_CungUng_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_CungUng> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_CungUng item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}