-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Insert]
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuan nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoPhiPhaiNop numeric(12, 0),
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@NgayPhatHanhChungTu datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
(
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuan],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoPhiPhaiNop],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[NgayPhatHanhChungTu]
)
VALUES 
(
	@TenCoQuanHaiQuan,
	@TenChiCucHaiQuan,
	@SoChungTu,
	@TenDonViXuatNhapKhau,
	@MaDonViXuatNhapKhau,
	@MaBuuChinh,
	@DiaChiNguoiXuatNhapKhau,
	@SoDienThoaiNguoiXuatNhapKhau,
	@SoToKhai,
	@NgayDangKyToKhai,
	@MaPhanLoaiToKhai,
	@TenNganHangTraThay,
	@MaNganHangTraThay,
	@KiHieuChungTuPhatHanhHanMuc,
	@SoHieuPhatHanhHanMuc,
	@TongSoPhiPhaiNop,
	@SoTaiKhoanKhoBac,
	@TenKhoBac,
	@NgayPhatHanhChungTu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Update]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuan nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoPhiPhaiNop numeric(12, 0),
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@NgayPhatHanhChungTu datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
SET
	[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
	[TenChiCucHaiQuan] = @TenChiCucHaiQuan,
	[SoChungTu] = @SoChungTu,
	[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
	[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
	[MaBuuChinh] = @MaBuuChinh,
	[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
	[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
	[SoToKhai] = @SoToKhai,
	[NgayDangKyToKhai] = @NgayDangKyToKhai,
	[MaPhanLoaiToKhai] = @MaPhanLoaiToKhai,
	[TenNganHangTraThay] = @TenNganHangTraThay,
	[MaNganHangTraThay] = @MaNganHangTraThay,
	[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
	[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
	[TongSoPhiPhaiNop] = @TongSoPhiPhaiNop,
	[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
	[TenKhoBac] = @TenKhoBac,
	[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_InsertUpdate]
	@ID bigint,
	@TenCoQuanHaiQuan nvarchar(210),
	@TenChiCucHaiQuan nvarchar(210),
	@SoChungTu numeric(12, 0),
	@TenDonViXuatNhapKhau nvarchar(300),
	@MaDonViXuatNhapKhau varchar(13),
	@MaBuuChinh varchar(7),
	@DiaChiNguoiXuatNhapKhau nvarchar(300),
	@SoDienThoaiNguoiXuatNhapKhau varchar(20),
	@SoToKhai numeric(12, 0),
	@NgayDangKyToKhai datetime,
	@MaPhanLoaiToKhai varchar(3),
	@TenNganHangTraThay nvarchar(210),
	@MaNganHangTraThay varchar(11),
	@KiHieuChungTuPhatHanhHanMuc varchar(10),
	@SoHieuPhatHanhHanMuc varchar(10),
	@TongSoPhiPhaiNop numeric(12, 0),
	@SoTaiKhoanKhoBac varchar(15),
	@TenKhoBac nvarchar(210),
	@NgayPhatHanhChungTu datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu] 
		SET
			[TenCoQuanHaiQuan] = @TenCoQuanHaiQuan,
			[TenChiCucHaiQuan] = @TenChiCucHaiQuan,
			[SoChungTu] = @SoChungTu,
			[TenDonViXuatNhapKhau] = @TenDonViXuatNhapKhau,
			[MaDonViXuatNhapKhau] = @MaDonViXuatNhapKhau,
			[MaBuuChinh] = @MaBuuChinh,
			[DiaChiNguoiXuatNhapKhau] = @DiaChiNguoiXuatNhapKhau,
			[SoDienThoaiNguoiXuatNhapKhau] = @SoDienThoaiNguoiXuatNhapKhau,
			[SoToKhai] = @SoToKhai,
			[NgayDangKyToKhai] = @NgayDangKyToKhai,
			[MaPhanLoaiToKhai] = @MaPhanLoaiToKhai,
			[TenNganHangTraThay] = @TenNganHangTraThay,
			[MaNganHangTraThay] = @MaNganHangTraThay,
			[KiHieuChungTuPhatHanhHanMuc] = @KiHieuChungTuPhatHanhHanMuc,
			[SoHieuPhatHanhHanMuc] = @SoHieuPhatHanhHanMuc,
			[TongSoPhiPhaiNop] = @TongSoPhiPhaiNop,
			[SoTaiKhoanKhoBac] = @SoTaiKhoanKhoBac,
			[TenKhoBac] = @TenKhoBac,
			[NgayPhatHanhChungTu] = @NgayPhatHanhChungTu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
		(
			[TenCoQuanHaiQuan],
			[TenChiCucHaiQuan],
			[SoChungTu],
			[TenDonViXuatNhapKhau],
			[MaDonViXuatNhapKhau],
			[MaBuuChinh],
			[DiaChiNguoiXuatNhapKhau],
			[SoDienThoaiNguoiXuatNhapKhau],
			[SoToKhai],
			[NgayDangKyToKhai],
			[MaPhanLoaiToKhai],
			[TenNganHangTraThay],
			[MaNganHangTraThay],
			[KiHieuChungTuPhatHanhHanMuc],
			[SoHieuPhatHanhHanMuc],
			[TongSoPhiPhaiNop],
			[SoTaiKhoanKhoBac],
			[TenKhoBac],
			[NgayPhatHanhChungTu]
		)
		VALUES 
		(
			@TenCoQuanHaiQuan,
			@TenChiCucHaiQuan,
			@SoChungTu,
			@TenDonViXuatNhapKhau,
			@MaDonViXuatNhapKhau,
			@MaBuuChinh,
			@DiaChiNguoiXuatNhapKhau,
			@SoDienThoaiNguoiXuatNhapKhau,
			@SoToKhai,
			@NgayDangKyToKhai,
			@MaPhanLoaiToKhai,
			@TenNganHangTraThay,
			@MaNganHangTraThay,
			@KiHieuChungTuPhatHanhHanMuc,
			@SoHieuPhatHanhHanMuc,
			@TongSoPhiPhaiNop,
			@SoTaiKhoanKhoBac,
			@TenKhoBac,
			@NgayPhatHanhChungTu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuan],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoPhiPhaiNop],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuan],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoPhiPhaiNop],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[NgayPhatHanhChungTu]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TenCoQuanHaiQuan],
	[TenChiCucHaiQuan],
	[SoChungTu],
	[TenDonViXuatNhapKhau],
	[MaDonViXuatNhapKhau],
	[MaBuuChinh],
	[DiaChiNguoiXuatNhapKhau],
	[SoDienThoaiNguoiXuatNhapKhau],
	[SoToKhai],
	[NgayDangKyToKhai],
	[MaPhanLoaiToKhai],
	[TenNganHangTraThay],
	[MaNganHangTraThay],
	[KiHieuChungTuPhatHanhHanMuc],
	[SoHieuPhatHanhHanMuc],
	[TongSoPhiPhaiNop],
	[SoTaiKhoanKhoBac],
	[TenKhoBac],
	[NgayPhatHanhChungTu]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_LePhiPhaiThu]	

GO

