﻿using System;
using System.Collections.Generic;
using System.Text;
using CA_QuanLyGCN.Report;

namespace CA_QuanLyGCN
{
    public class SizeFont
    {
        #region Size font

        public  float TenDoanhNghiep;
        public  float DiaChiDoanhNghiep;
        public  float MaDoanhNghiep;
        public  float CoQuanCapMaDoanhNghiep;
        public  float NgayCapMST;
        public  float TenNguoiDaiDien;
        public  float QuocTich;
        public  float ChucDanh;
        public  float SoCMND;
        public  float NgayCapCMND;
        public  float CoQuanCapCMND;
        public  float HoKhau;
        public  float TenCoQuan;
        public  float TenPhongBan;
        public  float TenDayDuPhongBan;
        public  float SoGiayCN;
        public  float TenCoSoKD;
        public  float NganhNgheKD;
        public  float DiaChiCoSoKinhDoanh;
        public  float NgayChungNhan;
        #endregion

        #region Bold font
        public bool TenDoanhNghiepBold;
        public bool DiaChiDoanhNghiepBold;
        public bool MaDoanhNghiepBold;
        public bool CoQuanCapMaDoanhNghiepBold;
        public bool NgayCapMSTBold;
        public bool TenNguoiDaiDienBold;
        public bool QuocTichBold;
        public bool ChucDanhBold;
        public bool SoCMNDBold;
        public bool NgayCapCMNDBold;
        public bool CoQuanCapCMNDBold;
        public bool HoKhauBold;
        public bool TenCoQuanBold;
        public bool TenPhongBanBold;
        public bool TenDayDuPhongBanBold;
        public bool SoGiayCNBold;
        public bool TenCoSoKDBold;
        public bool NganhNgheKDBold;
        public bool DiaChiCoSoKinhDoanhBold;
        public bool NgayChungNhanBold;
#endregion
#region Italic
        public bool TenDoanhNghiepItalic;
        public bool DiaChiDoanhNghiepItalic;
        public bool MaDoanhNghiepItalic;
        public bool CoQuanCapMaDoanhNghiepItalic;
        public bool NgayCapMSTItalic;
        public bool TenNguoiDaiDienItalic;
        public bool QuocTichItalic;
        public bool ChucDanhItalic;
        public bool SoCMNDItalic;
        public bool NgayCapCMNDItalic;
        public bool CoQuanCapCMNDItalic;
        public bool HoKhauItalic;
        public bool TenCoQuanItalic;
        public bool TenPhongBanItalic;
        public bool TenDayDuPhongBanItalic;
        public bool SoGiayCNItalic;
        public bool TenCoSoKDItalic;
        public bool NganhNgheKDItalic;
        public bool DiaChiCoSoKinhDoanhItalic;
        public bool NgayChungNhanItalic;
#endregion


        public SizeFont()
        {
            //writeXml(applicationPath);
        }
        public void writeXml(string applicationPath)
        {
            try
            {

                System.Xml.Serialization.XmlSerializer writer =
                    new System.Xml.Serialization.XmlSerializer(typeof(SizeFont));
                System.IO.FileInfo f = new System.IO.FileInfo(applicationPath + "\\Config\\SizeFont.xml");
                if (f.Exists) f.Delete();
                else
                {
                    System.IO.StreamWriter file = new System.IO.StreamWriter(applicationPath + "\\Config\\SizeFont.xml");
                    writer.Serialize(file, this);
                    file.Close();
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void ReadXml(string ApplicationPath)
        {
            try
            {
                System.IO.FileInfo info = new System.IO.FileInfo(ApplicationPath + "\\Config\\SizeFont.xml");
                if (info.Exists)
                {
                    System.Xml.Serialization.XmlSerializer reader =
             new System.Xml.Serialization.XmlSerializer(typeof(SizeFont));
                    System.IO.StreamReader file = new System.IO.StreamReader(ApplicationPath + "\\Config\\SizeFont.xml");
                    SizeFont temp = (SizeFont)reader.Deserialize(file);

                    this.TenDoanhNghiep = temp.TenDoanhNghiep;
                    this.DiaChiDoanhNghiep = temp.DiaChiDoanhNghiep;
                    this.MaDoanhNghiep = temp.MaDoanhNghiep;
                    this.CoQuanCapMaDoanhNghiep = temp.CoQuanCapMaDoanhNghiep;
                    this.NgayCapMST = temp.NgayCapMST;
                    this.TenNguoiDaiDien = temp.TenNguoiDaiDien;
                    this.QuocTich = temp.QuocTich;
                    this.ChucDanh = temp.ChucDanh;
                    this.SoCMND = temp.SoCMND;
                    this.NgayCapCMND = temp.NgayCapCMND;
                    this.CoQuanCapCMND = temp.CoQuanCapCMND;
                    this.HoKhau = temp.HoKhau;
                    this.TenCoQuan = temp.TenCoQuan;
                    this.TenPhongBan = temp.TenPhongBan;
                    this.TenDayDuPhongBan = temp.TenDayDuPhongBan;
                    this.SoGiayCN = temp.SoGiayCN;
                    this.TenCoSoKD = temp.TenCoSoKD;
                    this.NganhNgheKD = temp.NganhNgheKD;
                    this.DiaChiCoSoKinhDoanh = temp.DiaChiCoSoKinhDoanh;
                    this.NgayChungNhan = temp.NgayChungNhan;

                    this.TenDoanhNghiepItalic = temp.TenDoanhNghiepItalic;
                    this.DiaChiDoanhNghiepItalic = temp.DiaChiDoanhNghiepItalic;
                    this.MaDoanhNghiepItalic = temp.MaDoanhNghiepItalic;
                    this.CoQuanCapMaDoanhNghiepItalic = temp.CoQuanCapMaDoanhNghiepItalic;
                    this.NgayCapMSTItalic = temp.NgayCapMSTItalic;
                    this.TenNguoiDaiDienItalic = temp.TenNguoiDaiDienItalic;
                    this.QuocTichItalic = temp.QuocTichItalic;
                    this.ChucDanhItalic = temp.ChucDanhItalic;
                    this.SoCMNDItalic = temp.SoCMNDItalic;
                    this.NgayCapCMNDItalic = temp.NgayCapCMNDItalic;
                    this.CoQuanCapCMNDItalic = temp.CoQuanCapCMNDItalic;
                    this.HoKhauItalic = temp.HoKhauItalic;
                    this.TenCoQuanItalic = temp.TenCoQuanItalic;
                    this.TenPhongBanItalic = temp.TenPhongBanItalic;
                    this.TenDayDuPhongBanItalic = temp.TenDayDuPhongBanItalic;
                    this.SoGiayCNItalic = temp.SoGiayCNItalic;
                    this.TenCoSoKDItalic = temp.TenCoSoKDItalic;
                    this.NganhNgheKDItalic = temp.NganhNgheKDItalic;
                    this.DiaChiCoSoKinhDoanhItalic = temp.DiaChiCoSoKinhDoanhItalic;
                    this.NgayChungNhanItalic = temp.NgayChungNhanItalic;

                    this.TenDoanhNghiepBold = temp.TenDoanhNghiepBold;
                    this.DiaChiDoanhNghiepBold = temp.DiaChiDoanhNghiepBold;
                    this.MaDoanhNghiepBold = temp.MaDoanhNghiepBold;
                    this.CoQuanCapMaDoanhNghiepBold = temp.CoQuanCapMaDoanhNghiepBold;
                    this.NgayCapMSTBold = temp.NgayCapMSTBold;
                    this.TenNguoiDaiDienBold = temp.TenNguoiDaiDienBold;
                    this.QuocTichBold = temp.QuocTichBold;
                    this.ChucDanhBold = temp.ChucDanhBold;
                    this.SoCMNDBold = temp.SoCMNDBold;
                    this.NgayCapCMNDBold = temp.NgayCapCMNDBold;
                    this.CoQuanCapCMNDBold = temp.CoQuanCapCMNDBold;
                    this.HoKhauBold = temp.HoKhauBold;
                    this.TenCoQuanBold = temp.TenCoQuanBold;
                    this.TenPhongBanBold = temp.TenPhongBanBold;
                    this.TenDayDuPhongBanBold = temp.TenDayDuPhongBanBold;
                    this.SoGiayCNBold = temp.SoGiayCNBold;
                    this.TenCoSoKDBold = temp.TenCoSoKDBold;
                    this.NganhNgheKDBold = temp.NganhNgheKDBold;
                    this.DiaChiCoSoKinhDoanhBold = temp.DiaChiCoSoKinhDoanhBold;
                    this.NgayChungNhanBold = temp.NgayChungNhanBold;

                    file.Close();
                }
                else
                {
                    GiayChungNhanReport report = new GiayChungNhanReport();
                    report.GetDefaultFont();
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
