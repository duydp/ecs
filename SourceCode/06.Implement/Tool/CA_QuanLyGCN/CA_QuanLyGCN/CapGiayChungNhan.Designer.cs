﻿namespace CA_QuanLyGCN
{
    partial class CapGiayChungNhan
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CapGiayChungNhan));
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgay = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.cmbNganhNgheKD = new Janus.Windows.EditControls.UIComboBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnIn = new Janus.Windows.EditControls.UIButton();
            this.btnLuuThongTin = new Janus.Windows.EditControls.UIButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDiaChi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTenDoanhNghiep = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblMSTDN = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSoCN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenCSKD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.uiGroupBox1.Controls.Add(this.ccNgay);
            this.uiGroupBox1.Controls.Add(this.cmbNganhNgheKD);
            this.uiGroupBox1.Controls.Add(this.lblStatus);
            this.uiGroupBox1.Controls.Add(this.uiButton1);
            this.uiGroupBox1.Controls.Add(this.btnIn);
            this.uiGroupBox1.Controls.Add(this.btnLuuThongTin);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.txtDiaChi);
            this.uiGroupBox1.Controls.Add(this.lblTenDoanhNghiep);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.lblMSTDN);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtSoCN);
            this.uiGroupBox1.Controls.Add(this.txtTenCSKD);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(589, 208);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // ccNgay
            // 
            // 
            // 
            // 
            this.ccNgay.DropDownCalendar.Name = "";
            this.ccNgay.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgay.Location = new System.Drawing.Point(496, 113);
            this.ccNgay.Name = "ccNgay";
            this.ccNgay.Size = new System.Drawing.Size(81, 20);
            this.ccNgay.TabIndex = 3;
            this.ccNgay.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgay.VisualStyleManager = this.visualStyleManager1;
            // 
            // visualStyleManager1
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "Office2007";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme1.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme1);
            // 
            // cmbNganhNgheKD
            // 
            this.cmbNganhNgheKD.Location = new System.Drawing.Point(148, 113);
            this.cmbNganhNgheKD.Name = "cmbNganhNgheKD";
            this.cmbNganhNgheKD.Size = new System.Drawing.Size(180, 20);
            this.cmbNganhNgheKD.TabIndex = 1;
            this.cmbNganhNgheKD.VisualStyleManager = this.visualStyleManager1;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.ForeColor = System.Drawing.Color.Blue;
            this.lblStatus.Location = new System.Drawing.Point(145, 192);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 13);
            this.lblStatus.TabIndex = 5;
            this.lblStatus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnIn
            // 
            this.btnIn.Icon = ((System.Drawing.Icon)(resources.GetObject("btnIn.Icon")));
            this.btnIn.Location = new System.Drawing.Point(251, 164);
            this.btnIn.Name = "btnIn";
            this.btnIn.Size = new System.Drawing.Size(119, 23);
            this.btnIn.TabIndex = 6;
            this.btnIn.Text = "In giấy chứng nhận";
            this.btnIn.VisualStyleManager = this.visualStyleManager1;
            this.btnIn.Click += new System.EventHandler(this.btnIn_Click);
            // 
            // btnLuuThongTin
            // 
            this.btnLuuThongTin.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLuuThongTin.Icon")));
            this.btnLuuThongTin.Location = new System.Drawing.Point(148, 164);
            this.btnLuuThongTin.Name = "btnLuuThongTin";
            this.btnLuuThongTin.Size = new System.Drawing.Size(97, 23);
            this.btnLuuThongTin.TabIndex = 5;
            this.btnLuuThongTin.Text = "Lưu thông tin";
            this.btnLuuThongTin.VisualStyleManager = this.visualStyleManager1;
            this.btnLuuThongTin.Click += new System.EventHandler(this.btnLuuThongTin_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(458, 117);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(32, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Ngày";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(334, 117);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 1;
            this.label6.Text = "Số CN";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 116);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(122, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Ngành nghề kinh doanh";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 142);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Địa chỉ cơ sở kinh doanh";
            // 
            // txtDiaChi
            // 
            this.txtDiaChi.Location = new System.Drawing.Point(148, 138);
            this.txtDiaChi.Name = "txtDiaChi";
            this.txtDiaChi.Size = new System.Drawing.Size(432, 20);
            this.txtDiaChi.TabIndex = 4;
            this.txtDiaChi.VisualStyleManager = this.visualStyleManager1;
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.AutoSize = true;
            this.lblTenDoanhNghiep.Location = new System.Drawing.Point(145, 56);
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.Size = new System.Drawing.Size(94, 13);
            this.lblTenDoanhNghiep.TabIndex = 1;
            this.lblTenDoanhNghiep.Text = "Tên doanh nghiệp";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(94, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Tên doanh nghiệp";
            // 
            // lblMSTDN
            // 
            this.lblMSTDN.AutoSize = true;
            this.lblMSTDN.Location = new System.Drawing.Point(145, 23);
            this.lblMSTDN.Name = "lblMSTDN";
            this.lblMSTDN.Size = new System.Drawing.Size(100, 13);
            this.lblMSTDN.TabIndex = 1;
            this.lblMSTDN.Text = "MST Doanh nghiệp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "MST Doanh nghiệp";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 90);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tên cơ sở kinh doanh";
            // 
            // txtSoCN
            // 
            this.txtSoCN.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtSoCN.Location = new System.Drawing.Point(378, 113);
            this.txtSoCN.Name = "txtSoCN";
            this.txtSoCN.Size = new System.Drawing.Size(74, 20);
            this.txtSoCN.TabIndex = 2;
            this.txtSoCN.VisualStyleManager = this.visualStyleManager1;
            // 
            // txtTenCSKD
            // 
            this.txtTenCSKD.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtTenCSKD.Location = new System.Drawing.Point(148, 86);
            this.txtTenCSKD.Name = "txtTenCSKD";
            this.txtTenCSKD.Size = new System.Drawing.Size(432, 20);
            this.txtTenCSKD.TabIndex = 0;
            this.txtTenCSKD.VisualStyleManager = this.visualStyleManager1;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // uiButton1
            // 
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(378, 164);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(119, 23);
            this.uiButton1.TabIndex = 6;
            this.uiButton1.Text = "Cấu hình font chữ";
            this.uiButton1.VisualStyleManager = this.visualStyleManager1;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // CapGiayChungNhan
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(589, 208);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "CapGiayChungNhan";
            this.Text = "Thông tin chứng nhận cơ sở";
            this.Load += new System.EventHandler(this.CapGiayChungNhan_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChi;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenCSKD;
        private Janus.Windows.EditControls.UIButton btnLuuThongTin;
        private System.Windows.Forms.Label lblTenDoanhNghiep;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblMSTDN;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnIn;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCN;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private Janus.Windows.EditControls.UIComboBox cmbNganhNgheKD;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgay;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIButton uiButton1;
    }
}
