﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages
{
    public class Result
    {
        /// <summary>
        /// Noi dung giam dinh
        /// </summary>
        [XmlElement("content")]
        public string content { get; set; }
        /// <summary>
        /// ket qua giam dinh
        /// </summary>
        [XmlElement("resultOfExam")]
        public string resultOfExam { get; set; }
    }
}
