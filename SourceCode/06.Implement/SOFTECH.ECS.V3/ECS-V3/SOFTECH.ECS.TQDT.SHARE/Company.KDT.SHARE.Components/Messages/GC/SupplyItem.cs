﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class SupplyItem
    {
        /// <summary>
        /// Loại chứng từ
        /// </summary>
        [XmlElement("Invoice")]
        public Invoice Invoice { get; set; }
        /// <summary>
        /// Dòng hàng
        /// </summary>
        [XmlElement("InvoiceLine")]
        public InvoiceLine InvoiceLine { get; set; }
        /// <summary>
        /// Lượng cung ứng
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }
    }
}
