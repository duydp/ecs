﻿namespace Company.Interface.Report.SXXK
{
    partial class PhuLucToKhaiNhap
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PhuLucToKhaiNhap));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblBanLuuHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTriGiaNT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.XuatXu9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblBanLuuHaiQuan,
            this.xrLabel2,
            this.lblTongTriGiaNT,
            this.lblNgayDangKy,
            this.lblMaHaiQuan,
            this.xrLabel3,
            this.lblSoToKhai,
            this.xrLabel1,
            this.xrTable1,
            this.xrPictureBox1});
            this.Detail.Height = 1158;
            this.Detail.Name = "Detail";
            // 
            // lblBanLuuHaiQuan
            // 
            this.lblBanLuuHaiQuan.BackColor = System.Drawing.Color.LightCyan;
            this.lblBanLuuHaiQuan.Font = new System.Drawing.Font("Times New Roman", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblBanLuuHaiQuan.Location = new System.Drawing.Point(267, 38);
            this.lblBanLuuHaiQuan.Name = "lblBanLuuHaiQuan";
            this.lblBanLuuHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanLuuHaiQuan.ParentStyleUsing.UseBackColor = false;
            this.lblBanLuuHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblBanLuuHaiQuan.Size = new System.Drawing.Size(283, 17);
            this.lblBanLuuHaiQuan.Text = "Bản lưu người khai Hải quan";
            this.lblBanLuuHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.Location = new System.Drawing.Point(217, 675);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.ParentStyleUsing.UseFont = false;
            this.xrLabel2.ParentStyleUsing.UseForeColor = false;
            this.xrLabel2.Size = new System.Drawing.Size(458, 83);
            this.xrLabel2.Text = "HÀNG GIA CÔNG MIỄN THUẾ THEO QUY ĐỊNH TẠI ĐIỂM 1,4 MỤC I PHẦN D THÔNG TƯ SỐ 59/20" +
                "07/TT-BTC NGÀY 14/06/2007 VÀ THUỘC ĐỐI TƯỢNG KHÔNG CHỊU THUẾ GTGT";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblTongTriGiaNT
            // 
            this.lblTongTriGiaNT.Location = new System.Drawing.Point(692, 542);
            this.lblTongTriGiaNT.Name = "lblTongTriGiaNT";
            this.lblTongTriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaNT.Size = new System.Drawing.Size(91, 34);
            this.lblTongTriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Location = new System.Drawing.Point(325, 97);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.Size = new System.Drawing.Size(83, 17);
            this.lblNgayDangKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaHaiQuan
            // 
            this.lblMaHaiQuan.Location = new System.Drawing.Point(508, 75);
            this.lblMaHaiQuan.Name = "lblMaHaiQuan";
            this.lblMaHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaHaiQuan.Size = new System.Drawing.Size(50, 17);
            this.lblMaHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Location = new System.Drawing.Point(442, 75);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.Size = new System.Drawing.Size(50, 17);
            this.xrLabel3.Text = "GC";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Location = new System.Drawing.Point(325, 75);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.Size = new System.Drawing.Size(83, 17);
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.Location = new System.Drawing.Point(325, 58);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.Size = new System.Drawing.Size(100, 17);
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(58, 200);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9});
            this.xrTable1.Size = new System.Drawing.Size(722, 333);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang1,
            this.MaHS1,
            this.XuatXu1,
            this.Luong1,
            this.DVT1,
            this.DonGiaNT1,
            this.TriGiaNT1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang1
            // 
            this.TenHang1.Location = new System.Drawing.Point(0, 0);
            this.TenHang1.Name = "TenHang1";
            this.TenHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang1.Size = new System.Drawing.Size(274, 37);
            this.TenHang1.Tag = "Tên hàng 1";
            this.TenHang1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS1
            // 
            this.MaHS1.Location = new System.Drawing.Point(274, 0);
            this.MaHS1.Name = "MaHS1";
            this.MaHS1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS1.Size = new System.Drawing.Size(92, 37);
            this.MaHS1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu1
            // 
            this.XuatXu1.Location = new System.Drawing.Point(366, 0);
            this.XuatXu1.Name = "XuatXu1";
            this.XuatXu1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu1.Size = new System.Drawing.Size(58, 37);
            this.XuatXu1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong1
            // 
            this.Luong1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong1.Location = new System.Drawing.Point(424, 0);
            this.Luong1.Name = "Luong1";
            this.Luong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong1.ParentStyleUsing.UseFont = false;
            this.Luong1.Size = new System.Drawing.Size(75, 37);
            this.Luong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT1
            // 
            this.DVT1.Location = new System.Drawing.Point(499, 0);
            this.DVT1.Name = "DVT1";
            this.DVT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT1.Size = new System.Drawing.Size(67, 37);
            this.DVT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT1
            // 
            this.DonGiaNT1.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT1.Name = "DonGiaNT1";
            this.DonGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT1.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT1
            // 
            this.TriGiaNT1.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT1.Name = "TriGiaNT1";
            this.TriGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT1.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang2,
            this.MaHS2,
            this.XuatXu2,
            this.Luong2,
            this.DVT2,
            this.DonGiaNT2,
            this.TriGiaNT2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang2
            // 
            this.TenHang2.Location = new System.Drawing.Point(0, 0);
            this.TenHang2.Name = "TenHang2";
            this.TenHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang2.Size = new System.Drawing.Size(274, 37);
            this.TenHang2.Tag = "Tên hàng 2";
            this.TenHang2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS2
            // 
            this.MaHS2.Location = new System.Drawing.Point(274, 0);
            this.MaHS2.Name = "MaHS2";
            this.MaHS2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS2.Size = new System.Drawing.Size(92, 37);
            this.MaHS2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu2
            // 
            this.XuatXu2.Location = new System.Drawing.Point(366, 0);
            this.XuatXu2.Name = "XuatXu2";
            this.XuatXu2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu2.Size = new System.Drawing.Size(58, 37);
            this.XuatXu2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong2
            // 
            this.Luong2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong2.Location = new System.Drawing.Point(424, 0);
            this.Luong2.Name = "Luong2";
            this.Luong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong2.ParentStyleUsing.UseFont = false;
            this.Luong2.Size = new System.Drawing.Size(75, 37);
            this.Luong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT2
            // 
            this.DVT2.Location = new System.Drawing.Point(499, 0);
            this.DVT2.Name = "DVT2";
            this.DVT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT2.Size = new System.Drawing.Size(67, 37);
            this.DVT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT2
            // 
            this.DonGiaNT2.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT2.Name = "DonGiaNT2";
            this.DonGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT2.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT2
            // 
            this.TriGiaNT2.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT2.Name = "TriGiaNT2";
            this.TriGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT2.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang3,
            this.MaHS3,
            this.XuatXu3,
            this.Luong3,
            this.DVT3,
            this.DonGiaNT3,
            this.TriGiaNT3});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang3
            // 
            this.TenHang3.Location = new System.Drawing.Point(0, 0);
            this.TenHang3.Name = "TenHang3";
            this.TenHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang3.Size = new System.Drawing.Size(274, 37);
            this.TenHang3.Tag = "Tên hàng 3";
            this.TenHang3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS3
            // 
            this.MaHS3.Location = new System.Drawing.Point(274, 0);
            this.MaHS3.Name = "MaHS3";
            this.MaHS3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS3.Size = new System.Drawing.Size(92, 37);
            this.MaHS3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu3
            // 
            this.XuatXu3.Location = new System.Drawing.Point(366, 0);
            this.XuatXu3.Name = "XuatXu3";
            this.XuatXu3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu3.Size = new System.Drawing.Size(58, 37);
            this.XuatXu3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong3
            // 
            this.Luong3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong3.Location = new System.Drawing.Point(424, 0);
            this.Luong3.Name = "Luong3";
            this.Luong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong3.ParentStyleUsing.UseFont = false;
            this.Luong3.Size = new System.Drawing.Size(75, 37);
            this.Luong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT3
            // 
            this.DVT3.Location = new System.Drawing.Point(499, 0);
            this.DVT3.Name = "DVT3";
            this.DVT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT3.Size = new System.Drawing.Size(67, 37);
            this.DVT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT3
            // 
            this.DonGiaNT3.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT3.Name = "DonGiaNT3";
            this.DonGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT3.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT3
            // 
            this.TriGiaNT3.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT3.Name = "TriGiaNT3";
            this.TriGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT3.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang4,
            this.MaHS4,
            this.XuatXu4,
            this.Luong4,
            this.DVT4,
            this.DonGiaNT4,
            this.TriGiaNT4});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang4
            // 
            this.TenHang4.Location = new System.Drawing.Point(0, 0);
            this.TenHang4.Name = "TenHang4";
            this.TenHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang4.Size = new System.Drawing.Size(274, 37);
            this.TenHang4.Tag = "Tên hàng 4";
            this.TenHang4.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS4
            // 
            this.MaHS4.Location = new System.Drawing.Point(274, 0);
            this.MaHS4.Name = "MaHS4";
            this.MaHS4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS4.Size = new System.Drawing.Size(92, 37);
            this.MaHS4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu4
            // 
            this.XuatXu4.Location = new System.Drawing.Point(366, 0);
            this.XuatXu4.Name = "XuatXu4";
            this.XuatXu4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu4.Size = new System.Drawing.Size(58, 37);
            this.XuatXu4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong4
            // 
            this.Luong4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong4.Location = new System.Drawing.Point(424, 0);
            this.Luong4.Name = "Luong4";
            this.Luong4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong4.ParentStyleUsing.UseFont = false;
            this.Luong4.Size = new System.Drawing.Size(75, 37);
            this.Luong4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT4
            // 
            this.DVT4.Location = new System.Drawing.Point(499, 0);
            this.DVT4.Name = "DVT4";
            this.DVT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT4.Size = new System.Drawing.Size(67, 37);
            this.DVT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT4
            // 
            this.DonGiaNT4.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT4.Name = "DonGiaNT4";
            this.DonGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT4.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT4
            // 
            this.TriGiaNT4.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT4.Name = "TriGiaNT4";
            this.TriGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT4.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang5,
            this.MaHS5,
            this.XuatXu5,
            this.Luong5,
            this.DVT5,
            this.DonGiaNT5,
            this.TriGiaNT5});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang5
            // 
            this.TenHang5.Location = new System.Drawing.Point(0, 0);
            this.TenHang5.Name = "TenHang5";
            this.TenHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang5.Size = new System.Drawing.Size(274, 37);
            this.TenHang5.Tag = "Tên hàng 5";
            this.TenHang5.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS5
            // 
            this.MaHS5.Location = new System.Drawing.Point(274, 0);
            this.MaHS5.Name = "MaHS5";
            this.MaHS5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS5.Size = new System.Drawing.Size(92, 37);
            this.MaHS5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu5
            // 
            this.XuatXu5.Location = new System.Drawing.Point(366, 0);
            this.XuatXu5.Name = "XuatXu5";
            this.XuatXu5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu5.Size = new System.Drawing.Size(58, 37);
            this.XuatXu5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong5
            // 
            this.Luong5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong5.Location = new System.Drawing.Point(424, 0);
            this.Luong5.Name = "Luong5";
            this.Luong5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong5.ParentStyleUsing.UseFont = false;
            this.Luong5.Size = new System.Drawing.Size(75, 37);
            this.Luong5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT5
            // 
            this.DVT5.Location = new System.Drawing.Point(499, 0);
            this.DVT5.Name = "DVT5";
            this.DVT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT5.Size = new System.Drawing.Size(67, 37);
            this.DVT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT5
            // 
            this.DonGiaNT5.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT5.Name = "DonGiaNT5";
            this.DonGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT5.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT5
            // 
            this.TriGiaNT5.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT5.Name = "TriGiaNT5";
            this.TriGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT5.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang6,
            this.MaHS6,
            this.XuatXu6,
            this.Luong6,
            this.DVT6,
            this.DonGiaNT6,
            this.TriGiaNT6});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang6
            // 
            this.TenHang6.Location = new System.Drawing.Point(0, 0);
            this.TenHang6.Name = "TenHang6";
            this.TenHang6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang6.Size = new System.Drawing.Size(274, 37);
            this.TenHang6.Tag = "Tên hàng 6";
            this.TenHang6.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS6
            // 
            this.MaHS6.Location = new System.Drawing.Point(274, 0);
            this.MaHS6.Name = "MaHS6";
            this.MaHS6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS6.Size = new System.Drawing.Size(92, 37);
            this.MaHS6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu6
            // 
            this.XuatXu6.Location = new System.Drawing.Point(366, 0);
            this.XuatXu6.Name = "XuatXu6";
            this.XuatXu6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu6.Size = new System.Drawing.Size(58, 37);
            this.XuatXu6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong6
            // 
            this.Luong6.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong6.Location = new System.Drawing.Point(424, 0);
            this.Luong6.Name = "Luong6";
            this.Luong6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong6.ParentStyleUsing.UseFont = false;
            this.Luong6.Size = new System.Drawing.Size(75, 37);
            this.Luong6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT6
            // 
            this.DVT6.Location = new System.Drawing.Point(499, 0);
            this.DVT6.Name = "DVT6";
            this.DVT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT6.Size = new System.Drawing.Size(67, 37);
            this.DVT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT6
            // 
            this.DonGiaNT6.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT6.Name = "DonGiaNT6";
            this.DonGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT6.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT6
            // 
            this.TriGiaNT6.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT6.Name = "TriGiaNT6";
            this.TriGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT6.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang7,
            this.MaHS7,
            this.XuatXu7,
            this.Luong7,
            this.DVT7,
            this.DonGiaNT7,
            this.TriGiaNT7});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang7
            // 
            this.TenHang7.Location = new System.Drawing.Point(0, 0);
            this.TenHang7.Name = "TenHang7";
            this.TenHang7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang7.Size = new System.Drawing.Size(274, 37);
            this.TenHang7.Tag = "Tên hàng 7";
            this.TenHang7.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS7
            // 
            this.MaHS7.Location = new System.Drawing.Point(274, 0);
            this.MaHS7.Name = "MaHS7";
            this.MaHS7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS7.Size = new System.Drawing.Size(92, 37);
            this.MaHS7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu7
            // 
            this.XuatXu7.Location = new System.Drawing.Point(366, 0);
            this.XuatXu7.Name = "XuatXu7";
            this.XuatXu7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu7.Size = new System.Drawing.Size(58, 37);
            this.XuatXu7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong7
            // 
            this.Luong7.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong7.Location = new System.Drawing.Point(424, 0);
            this.Luong7.Name = "Luong7";
            this.Luong7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong7.ParentStyleUsing.UseFont = false;
            this.Luong7.Size = new System.Drawing.Size(75, 37);
            this.Luong7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT7
            // 
            this.DVT7.Location = new System.Drawing.Point(499, 0);
            this.DVT7.Name = "DVT7";
            this.DVT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT7.Size = new System.Drawing.Size(67, 37);
            this.DVT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT7
            // 
            this.DonGiaNT7.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT7.Name = "DonGiaNT7";
            this.DonGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT7.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT7
            // 
            this.TriGiaNT7.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT7.Name = "TriGiaNT7";
            this.TriGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT7.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang8,
            this.MaHS8,
            this.XuatXu8,
            this.Luong8,
            this.DVT8,
            this.DonGiaNT8,
            this.TriGiaNT8});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang8
            // 
            this.TenHang8.Location = new System.Drawing.Point(0, 0);
            this.TenHang8.Name = "TenHang8";
            this.TenHang8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang8.Size = new System.Drawing.Size(274, 37);
            this.TenHang8.Tag = "Tên hàng 8";
            this.TenHang8.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS8
            // 
            this.MaHS8.Location = new System.Drawing.Point(274, 0);
            this.MaHS8.Name = "MaHS8";
            this.MaHS8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS8.Size = new System.Drawing.Size(92, 37);
            this.MaHS8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu8
            // 
            this.XuatXu8.Location = new System.Drawing.Point(366, 0);
            this.XuatXu8.Name = "XuatXu8";
            this.XuatXu8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu8.Size = new System.Drawing.Size(58, 37);
            this.XuatXu8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong8
            // 
            this.Luong8.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong8.Location = new System.Drawing.Point(424, 0);
            this.Luong8.Name = "Luong8";
            this.Luong8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong8.ParentStyleUsing.UseFont = false;
            this.Luong8.Size = new System.Drawing.Size(75, 37);
            this.Luong8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT8
            // 
            this.DVT8.Location = new System.Drawing.Point(499, 0);
            this.DVT8.Name = "DVT8";
            this.DVT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT8.Size = new System.Drawing.Size(67, 37);
            this.DVT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT8
            // 
            this.DonGiaNT8.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT8.Name = "DonGiaNT8";
            this.DonGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT8.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT8
            // 
            this.TriGiaNT8.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT8.Name = "TriGiaNT8";
            this.TriGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT8.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang9,
            this.MaHS9,
            this.XuatXu9,
            this.Luong9,
            this.DVT9,
            this.DonGiaNT9,
            this.TriGiaNT9});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Size = new System.Drawing.Size(722, 37);
            // 
            // TenHang9
            // 
            this.TenHang9.Location = new System.Drawing.Point(0, 0);
            this.TenHang9.Name = "TenHang9";
            this.TenHang9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang9.Size = new System.Drawing.Size(274, 37);
            this.TenHang9.Tag = "Tên hàng 9";
            this.TenHang9.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang_PreviewClick);
            // 
            // MaHS9
            // 
            this.MaHS9.Location = new System.Drawing.Point(274, 0);
            this.MaHS9.Name = "MaHS9";
            this.MaHS9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS9.Size = new System.Drawing.Size(92, 37);
            this.MaHS9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // XuatXu9
            // 
            this.XuatXu9.Location = new System.Drawing.Point(366, 0);
            this.XuatXu9.Name = "XuatXu9";
            this.XuatXu9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.XuatXu9.Size = new System.Drawing.Size(58, 37);
            this.XuatXu9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // Luong9
            // 
            this.Luong9.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Luong9.Location = new System.Drawing.Point(424, 0);
            this.Luong9.Name = "Luong9";
            this.Luong9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong9.ParentStyleUsing.UseFont = false;
            this.Luong9.Size = new System.Drawing.Size(75, 37);
            this.Luong9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // DVT9
            // 
            this.DVT9.Location = new System.Drawing.Point(499, 0);
            this.DVT9.Name = "DVT9";
            this.DVT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT9.Size = new System.Drawing.Size(67, 37);
            this.DVT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DonGiaNT9
            // 
            this.DonGiaNT9.Location = new System.Drawing.Point(566, 0);
            this.DonGiaNT9.Name = "DonGiaNT9";
            this.DonGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT9.Size = new System.Drawing.Size(63, 37);
            this.DonGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // TriGiaNT9
            // 
            this.TriGiaNT9.Location = new System.Drawing.Point(629, 0);
            this.TriGiaNT9.Name = "TriGiaNT9";
            this.TriGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT9.Size = new System.Drawing.Size(93, 37);
            this.TriGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.Location = new System.Drawing.Point(0, 0);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.Size = new System.Drawing.Size(814, 1155);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.AutoSize;
            // 
            // PhuLucToKhaiNhap
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Margins = new System.Drawing.Printing.Margins(7, 5, 6, 0);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell TenHang1;
        private DevExpress.XtraReports.UI.XRTableCell MaHS1;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu1;
        private DevExpress.XtraReports.UI.XRTableCell Luong1;
        private DevExpress.XtraReports.UI.XRTableCell DVT1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell TenHang2;
        private DevExpress.XtraReports.UI.XRTableCell MaHS2;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu2;
        private DevExpress.XtraReports.UI.XRTableCell Luong2;
        private DevExpress.XtraReports.UI.XRTableCell DVT2;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell TenHang3;
        private DevExpress.XtraReports.UI.XRTableCell MaHS3;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu3;
        private DevExpress.XtraReports.UI.XRTableCell Luong3;
        private DevExpress.XtraReports.UI.XRTableCell DVT3;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell TenHang4;
        private DevExpress.XtraReports.UI.XRTableCell MaHS4;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu4;
        private DevExpress.XtraReports.UI.XRTableCell Luong4;
        private DevExpress.XtraReports.UI.XRTableCell DVT4;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell TenHang5;
        private DevExpress.XtraReports.UI.XRTableCell MaHS5;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu5;
        private DevExpress.XtraReports.UI.XRTableCell Luong5;
        private DevExpress.XtraReports.UI.XRTableCell DVT5;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell TenHang6;
        private DevExpress.XtraReports.UI.XRTableCell MaHS6;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu6;
        private DevExpress.XtraReports.UI.XRTableCell Luong6;
        private DevExpress.XtraReports.UI.XRTableCell DVT6;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell TenHang7;
        private DevExpress.XtraReports.UI.XRTableCell MaHS7;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu7;
        private DevExpress.XtraReports.UI.XRTableCell Luong7;
        private DevExpress.XtraReports.UI.XRTableCell DVT7;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell TenHang8;
        private DevExpress.XtraReports.UI.XRTableCell MaHS8;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu8;
        private DevExpress.XtraReports.UI.XRTableCell Luong8;
        private DevExpress.XtraReports.UI.XRTableCell DVT8;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell TenHang9;
        private DevExpress.XtraReports.UI.XRTableCell MaHS9;
        private DevExpress.XtraReports.UI.XRTableCell XuatXu9;
        private DevExpress.XtraReports.UI.XRTableCell Luong9;
        private DevExpress.XtraReports.UI.XRTableCell DVT9;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT9;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT9;
        private DevExpress.XtraReports.UI.XRLabel lblMaHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblTongTriGiaNT;
        public DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblBanLuuHaiQuan;
    }
}
