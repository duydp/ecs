﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.KD;
using Company.KDT.SHARE.Components.Nodes.KD;

namespace SOFTECH.ECS.V3.Components.KD.SHARE
{
    // SiHV
    public partial class ProcessXML
    {
        /// <summary>
        /// SetValueAttachDocumentItem Attach Item
        /// </summary>
        /// <param name="Parent">XElement</param>
        /// <param name="ctk">ChungTuKem</param>
        /// <param name="stt">int</param>
        /// <returns>XElement</returns>
        public static XElement SetValueAttachDocumentItem(XElement Parent, ChungTuKem ctk, int stt)
        {
            try
            {
                if (Parent != null && Parent.Elements().Count() > 0 && ctk != null)
                {
                    bool AttachedFiles = false;
                    foreach (XElement element in Parent.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeName.sequence.ToUpper()))      // Số thứ tự
                        {
                            element.SetValue(stt);
                        }
                        if (element.Name.ToString().ToUpper().Equals(NodeName.issuer.ToUpper()))        // Mã Loại chứng từ
                        {
                            element.SetValue(ctk.MA_LOAI_CT);
                        }
                        if (element.Name.ToString().ToUpper().Equals(NodeName.issue.ToUpper()))         // Ngày chứng từ
                        {
                            element.SetValue(ctk.NGAY_CT);
                        }
                        if (element.Name.ToString().ToUpper().Equals(NodeName.reference.ToUpper()))      // Số chứng từ
                        {
                            element.SetValue(ctk.SO_CT);
                        }
                        if (element.Name.ToString().ToUpper().Equals(NodeName.description.ToUpper()))        // Chú thích diễn giải
                        {
                            element.SetValue(ctk.DIENGIAI);
                        }
                        if (element.Name.ToString().ToUpper().Equals(NodeName.AttachedFiles.ToUpper()))         // Danh sách file đính kèm, lặp lại nhiều lần
                        {
                            if (AttachedFiles == false)
                            {
                                IList<ChungTuKemChiTiet> lCTKCT = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(ctk.ID);
                                if (lCTKCT != null && lCTKCT.Count > 0)
                                {

                                    if (lCTKCT.Count == 1)
                                    {
                                        foreach (XElement AttachedFile in element.Elements())
                                        {
                                            if (AttachedFile.Name.ToString().ToUpper().Equals(NodeName.AttachedFile.ToUpper()))
                                                SetValueAttachedFile(AttachedFile, lCTKCT[0]);
                                        }
                                    }
                                    else if (lCTKCT.Count > 1)
                                    {
                                        XElement TempAttachedFile = new XElement(element.Element(NodeName.AttachedFile));
                                        foreach (XElement AttachedFile in element.Elements())
                                        {
                                            if (AttachedFile.Name.ToString().ToUpper().Equals(NodeName.AttachedFile.ToUpper()))
                                            {
                                                //TempAttachedFile = AttachedFile;
                                                SetValueAttachedFile(AttachedFile, lCTKCT[0]);
                                            }
                                        }
                                        for (int i = 1; i < lCTKCT.Count; i++)
                                        {
                                            XElement NewAttachedFile = new XElement(TempAttachedFile);
                                            element.Add(SetValueAttachedFile(NewAttachedFile, lCTKCT[i]));
                                        }
                                    }
                                }
                            }
                            AttachedFiles = true;
                        }
                    }
                }
                return Parent;
            }
            catch { return Parent; }
        }
        /// <summary>
        /// SetValueAttachedFile Attach file
        /// </summary>
        /// <param name="Parent">XElement</param>
        /// <param name="ctkct">ChungTuKemChiTiet</param>
        /// <returns>XElement</returns>
        public static XElement SetValueAttachedFile(XElement Parent, ChungTuKemChiTiet ctkct)
        {
            if (Parent != null && ctkct != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeName.fileName.ToUpper()))      // Tên file
                    {
                        element.SetValue(ctkct.FileName);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.content.ToUpper()))  // Nội dung
                    {
                        if (ctkct.NoiDung != null)
                        {
                            element.SetValue(ctkct.NoiDung);
                        }
                    }
                }
            }
            return Parent;

        }            
        /// <summary>
        /// SetValueAgent
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueAgent(XElement Parent, ToKhaiMauDich tkmd)
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())                                   // Đại lý
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))      // Tên đại lý
                    {
                        element.SetValue(tkmd.TenDaiLyTTHQ);// Tên người được cấp giấy phép
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeName.Identity.ToUpper()))  // Mã đại lý
                    {
                        element.SetValue(tkmd.MaDaiLyTTHQ);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeName.status.ToUpper()))    //??????????????????????/
                    {
                        element.SetValue("");
                    }
                }
            }
            return Parent;
        }              // Đại lý
        /// <summary>
        /// SetValueImporter
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueImporter(XElement Parent, ToKhaiMauDich tkmd)              // người được cấp giấy phép
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))      // Tên người được cấp giấy phép
                    {
                        element.SetValue(tkmd.TenDoanhNghiep);                                  
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeName.Identity.ToUpper()))  // Ma người được cấp giấy phép
                    {
                        element.SetValue(tkmd.MaDoanhNghiep);
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueExporter
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueExporter(XElement Parent, ToKhaiMauDich tkmd)                // người được xuat khau
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))          // Tên người xuat khau
                    {
                        element.SetValue(tkmd.TenDonViDoiTac);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeName.Identity.ToUpper()))      // Ma người xuat
                    {
                        element.SetValue("");//???????????????????????????
                    }
                }
            }
            return Parent;
        }      
        /// <summary>
        /// SetValueDeclarationDocument
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueDeclarationDocument(XElement Parent, ToKhaiMauDich tkmd)     // To Khai
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeName.reference.ToUpper()))         //Số TK
                    {
                        element.SetValue(tkmd.SoToKhai);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeName.issue.ToUpper()))             //Ngày TK	1	an19	YYYY-MM-DD HH:mm:ss ?????????????????????
                    {
                        element.SetValue(tkmd.NgayDangKy);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeName.natureOfTransaction.ToUpper()))//Mã LH	1	an..10	Danh mục chuẩn
                    {
                        element.SetValue(tkmd.MaLoaiHinh);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeName.declarationOffice.ToUpper())) //Mã hải quan 	1	an..6	Danh mục chuẩn
                    {
                        element.SetValue(tkmd.MaHaiQuan);
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueCurrencyExchange
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueCurrencyExchange(XElement Parent, ToKhaiMauDich tkmd)      // Nguyên tệ
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                NguyenTe nt = NguyenTe.Load(tkmd.NguyenTe_ID);
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeName.CurrencyType.ToUpper()))  //Mã nguyên tệ	1	a3	Danh mục chuẩn
                    {
                        if (nt != null)
                            element.SetValue(nt.ID);
                        else
                            element.SetValue("");

                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeName.rate.ToUpper()))          //Ngày TK	1	an19	YYYY-MM-DD HH:mm:ss ?????????????????????
                    {
                        if(nt != null)
                            element.SetValue(nt.Ten);
                        else
                            element.SetValue("");
                    }                   
                }                
            }
            return Parent;
        }
        /// <summary>
        /// SetValueDeclarationPackaging (So kien)
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueDeclarationPackaging(XElement Parent, ToKhaiMauDich tkmd)  // Số kiện
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
                {
                    foreach (XElement element in Parent.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeName.quantity.ToUpper()))  
                        {
                            element.SetValue(tkmd.SoKien);
                        }
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueInvoice
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueInvoice(XElement Parent, ToKhaiMauDich tkmd)               // Hóa đơn thương mại
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeName.issue.ToUpper()))      //Ngày hóa đơn	1	an10	YYYY-MM-DD
                    {
                        element.SetValue(tkmd.NgayHoaDonThuongMai);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeName.reference.ToUpper()))  //Số hóa đơn	1	an..50	
                    {
                        element.SetValue(tkmd.SoHoaDonThuongMai);
                    }
                    if (element.Name.ToString().ToUpper().Equals(NodeName.type.ToUpper()))        //Mã loại hóa đơn thương mại	1	an..3	Danh mục chuẩn
                    {
                        element.SetValue("");                                                     //???????????????????????
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueRepresentativePerson
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValueRepresentativePerson(XElement Parent, ToKhaiMauDich tkmd)  // Người đại diện doanh nghiệp	1	none	Ký, đóng dấu trên tờ khai

        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeName.RepresentativePerson.ToUpper()))  //Chức vụ	1	a..256	Bắt buộc phải có

                    {
                        element.SetValue(tkmd.MaDonViUT);
                    }

                    if (element.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))                   //Tên	1	a..35	Bắt buộc phải có
	
                    {
                        element.SetValue(tkmd.TenDonViUT);

                    }                   
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueGoodsShipment
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValue_GoodsShipment(XElement Parent, ToKhaiMauDich tkmd, bool NK)         // Thông tin về hàng hóa	
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                //IList<HangMauDich> listHMD = HangMauDich.SelectCollectionBy_TKMD_ID(tkmd.ID);
                XElement CustomsGoodsItem = Parent.Element(NodeName.CustomsGoodsItem);
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeName.exportationCountry.ToUpper()))
                    {
                        if (NK)
                            element.SetValue(tkmd.NuocXK_ID);
                        else
                            element.SetValue(tkmd.NuocNK_ID);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.Consignor.ToUpper()))
                    {
                        XElement Consignor = element;
                        if (Consignor.Elements().Count() > 0)
                        {
                            foreach (XElement childElement in Consignor.Elements())
                            {
                                if (childElement.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))         //Tên người giao hàng
                                    childElement.SetValue(tkmd.TenDonViDoiTac);
                                else if (childElement.Name.ToString().ToUpper().Equals(NodeName.identity.ToUpper()))//Mã người giao hàng
                                    childElement.SetValue("");  //?????????????????????????????????????
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.Consignee.ToUpper()))                //Người nhận hàng
                    {
                        XElement Consignee = element;
                        if (Consignee.Elements().Count() > 0)
                        {
                            foreach (XElement childElement in Consignee.Elements())
                            {
                                if (childElement.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))                   //Tên người nhận hàng
                                    childElement.SetValue(tkmd.MaDoanhNghiep);
                                else if (childElement.Name.ToString().ToUpper().Equals(NodeName.identity.ToUpper()))//Mã người nhận hàng
                                    childElement.SetValue(tkmd.TenDoanhNghiep);
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.NotifyParty.ToUpper()))
                    {
                        XElement Consignee = element;
                        if (Consignee.Elements().Count() > 0)
                        {
                            foreach (XElement childElement in Consignee.Elements())
                            {
                                if (childElement.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))                   //Tên người nhận trung gian
                                    childElement.SetValue(tkmd.TenDonViUT);
                                else if (childElement.Name.ToString().ToUpper().Equals(NodeName.identity.ToUpper()))//Mã người nhận trung gian
                                    childElement.SetValue(tkmd.MaDonViUT);
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.DeliveryDestination.ToUpper()))
                    {
                        foreach (XElement childElement in element.Elements())
                        {
                            if (childElement.Name.ToString().ToUpper().Equals(NodeName.line.ToUpper()))
                                childElement.SetValue(tkmd.DKGH_ID);                                                     //Địa điểm giao hàng	1	an..60
                        }
                    }
                    //Cửa khẩu nhập
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.EntryCustomsOffice.ToUpper()))
                    {
                        XElement EntryCustomsOffice = element;
                        if (EntryCustomsOffice.Elements().Count() > 0)
                        {
                            CuaKhau ck = CuaKhau.Load(tkmd.MaHaiQuan);
                            foreach (XElement childElement in EntryCustomsOffice.Elements())
                            {
                                if (childElement.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))                   //Tên cửa khẩu nhập	0	an..256
                                {
                                    if (ck != null)
                                        childElement.SetValue(ck.Ten);
                                    else childElement.SetValue("");
                                }
                                else if (childElement.Name.ToString().ToUpper().Equals(NodeName.code.ToUpper()))    //Mã cửa khẩu nhập	0	an..256
                                    if (ck != null)
                                    {
                                        childElement.SetValue(ck.ID);
                                    }
                                    else childElement.SetValue("");
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.ExitCustomsOffice.ToUpper()))
                    {
                        XElement ExitCustomsOffice = element;
                        if (ExitCustomsOffice.Elements().Count() > 0)
                        {
                            CuaKhau ck = CuaKhau.Load(tkmd.MaHaiQuan);
                            foreach (XElement childElement in ExitCustomsOffice.Elements())
                            {
                                if (childElement.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))                   //Tên cửa khẩu xuat	0	an..256
                                {
                                    childElement.SetValue(""); //??????????????????????????????????????????????????
                                }
                                else if (childElement.Name.ToString().ToUpper().Equals(NodeName.code.ToUpper()))    //Mã cửa khẩu xuat	0	an..256
                                    childElement.SetValue(""); //??????????????????????????????????????????????????
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.exporter.ToUpper()))                 //Người xuất khẩu	1	none
                        ProcessXML.SetValueExporter(element, tkmd);
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.importer.ToUpper()))                 //Người Nhap khẩu	1	none
                        ProcessXML.SetValueImporter(element, tkmd);
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.TradeTerm.ToUpper()))                //Điều kiện giao hàng	1	none
                    {
                        XElement TradeTerm = element;
                        foreach (XElement child in TradeTerm.Elements())
                        {
                            if (child.Name.ToString().ToUpper().Equals(NodeName.condition.ToUpper()))               //Mã điều kiện giao hàng	1	an..7	Danh mục chuẩn
                            {
                                child.SetValue(tkmd.DKGH_ID);
                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.CustomsGoodsItem.ToUpper()))         //Hàng khai báo	1	none
                    {
                        CustomsGoodsItem = element;
                    }
                }
                if (CustomsGoodsItem != null)
                {
                    IList<HangMauDich> listHMD = HangMauDich.SelectCollectionBy_TKMD_ID(tkmd.ID);
                    if (listHMD != null)
                    {
                        if (listHMD.Count > 1)
                        {
                            SetValue_CustomsGoodsItem(CustomsGoodsItem, tkmd, listHMD[0]);
                            for (int i = 1; i < listHMD.Count; i++)
                            {
                                XElement CustomsGoodsItemNew = new XElement(CustomsGoodsItem);
                                Parent.Add(SetValue_CustomsGoodsItem(CustomsGoodsItemNew, tkmd, listHMD[i]));
                            }
                        }
                        else if (listHMD.Count == 1)
                            SetValue_CustomsGoodsItem(CustomsGoodsItem, tkmd, listHMD[0]);
                    }     
                }

            }
            return Parent;
        }
        /// <summary>
        /// SetValue_CustomsGoodsItem
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValue_CustomsGoodsItem(XElement Parent, ToKhaiMauDich tkmd, HangMauDich hmd)      // Hàng khai báo
        {
            if (Parent != null && hmd != null && tkmd != null)
            {
                    foreach (XElement element in Parent.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeName.customsValue.ToUpper()))             
                            element.SetValue(hmd.TenHang);       //Hàng khai báo	1	none
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.sequence.ToUpper()))                  
                            element.SetValue(hmd.SoThuTuHang);   //Số thứ tự hàng	1	n..5
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.statisticalValue.ToUpper()))          
                            element.SetValue("");                //Trị giá tính thuế	1	n..16   ?????????????????????????
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.unitPrice.ToUpper()))                 
                            element.SetValue(hmd.DonGiaKB);      //Đơn giá nguyên tệ	1	n..16,2
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.Manufacturer.ToUpper()))              
                        {   //Hãng sản xuất	1	none
                            XElement Manufacturer = element;
                            if (Manufacturer.Elements().Count() > 0)
                            {
                                CuaKhau ck = CuaKhau.Load(tkmd.MaHaiQuan);
                                foreach (XElement childElement in Manufacturer.Elements())
                                {
                                    if (childElement.Name.ToString().Equals(NodeName.name.ToUpper()))               
                                    {
                                        childElement.SetValue("");      //Tên Hãng  ?????????????????????????????????????????????
                                    }
                                    else if (childElement.Name.ToString().ToUpper().Equals(NodeName.identity.ToUpper()))
                                        childElement.SetValue("");      //Mã Hãng   ?????????????????????????????????????????????
                                }
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.Origin.ToUpper()))        
                            element.Element(NodeName.originCountry).SetValue(hmd.NuocXX_ID); //Mã nước xuất xứ
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.Commodity.ToUpper()))     
                        {
                            SetValue_HangMauDichChiTiet(element,hmd);//Chi tiết hàng, thuế
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.AdditionalDocument.ToUpper()))     
                        {                                                   //CO  Or GiayPhep
                            if (element.Elements().Count() > 7)
                                SetValue_CustomsGoodsItem_CO(element, hmd);
                            else
                                SetValue_CustomsGoodsItem_GiayPhep(element, hmd);
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.AdditionalInformation.ToUpper()))
                        {
                            //Thông tin tờ khai trị giá	0	none	Tờ khai trị giá yêu cầu bao nhiêu chỉ tiêu thì khai đủ theo mẫu.
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeName.content.ToUpper()))
                                    child.SetValue("");     // Giá trị	1	an..512	 ????????????????????????????????????????????
                                if (child.Name.ToString().ToUpper().Equals(NodeName.statement.ToUpper()))
                                    child.SetValue("");     // Mã loại thông tin	1	an..3	Danh mục chuẩn ??????????????????
                                if (child.Name.ToString().ToUpper().Equals(NodeName.statementDescription.ToUpper()))
                                    child.SetValue("");     // Mô tả thông tin	0	an..512
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.customsValue.ToUpper()))
                        {
                            //  Trị giá Hải quan	1	none ??????????????????????????????????????????????????????????????????????
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeName.exitToEntryCharge.ToUpper()))
                                    child.SetValue("");     // Tổng chi phí khác (vận tải, bảo hiểm và các chi phí khác từ cảng xuất đến cảng nhập)	1	
                                                            //n..16,2	Tối đa 2 chữ số thập phân	1	an..3	Danh mục chuẩn ????????????????????
                                if (child.Name.ToString().ToUpper().Equals(NodeName.freightCharge.ToUpper()))
                                    child.SetValue("");     // Phí vận tải	1	n..16,2	Tối đa 2 chữ số thập phân ?????????????????????????????????
                                if (child.Name.ToString().ToUpper().Equals(NodeName.method.ToUpper()))
                                    child.SetValue("");     // Phương pháp xác định trị giá	1	n1	Danh mục chuẩn. Nếu không có thì để xâu rỗng
                                if (child.Name.ToString().ToUpper().Equals(NodeName.otherChargeDeduction.ToUpper()))
                                    child.SetValue("");     //Tổng các khoản phải cộng - tổng các khoản được trừ	1	n..16,2	Tối đa 2 chữ số thập phân. Nếu không có thì để giá trị 0
                            }
                            
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.ValuationAdjustment.ToUpper()))
                        {
                            // 1..n
                            // Các giá trị điều chỉnh	2	none	Chỉ nhập khi khai tờ khai trị giá. Có bao nhiêu khoản điều chỉnh phải nhập đủ
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeName.addition.ToUpper()))
                                    child.SetValue("");     // Mã loại giá trị	1	n..4	Danh mục chuẩn
                                if (child.Name.ToString().ToUpper().Equals(NodeName.percentage.ToUpper()))
                                    child.SetValue("");     // % giá trị	1	n..10,2	Tối đa 2 chữ số thập phân. Nếu không có thì để xâu rỗng
                                if (child.Name.ToString().ToUpper().Equals(NodeName.amount.ToUpper()))
                                    child.SetValue("");     // Số tiền	1	n..16,2	Tối đa hai chữ số thập phân. Nếu không có thì để giá trị 0
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.SpecializedManagement.ToUpper()))
                        {
                            // Thông tin về hàng quản lý chuyên ngành	0		Có thể lặp lại nếu có nhiều loại quản lý chuyên ngành (vd: dệt may, da giầy, thủy sản …)
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeName.type.ToUpper()))
                                    child.SetValue("");     // Loại hàng quảnlý chuyên ngành	1	n..2	Tham chiếu sheet SpecializedManagement
                                if (child.Name.ToString().ToUpper().Equals(NodeName.identification.ToUpper()))
                                    child.SetValue("");     // Mã hàng quản lý chuyên ngành	1	an..35	VD: Mã HTS, …
                                if (child.Name.ToString().ToUpper().Equals(NodeName.quantity.ToUpper()))
                                    child.SetValue("");     // Lượng hàng quản lý chuyên ngành	1	n..16,2	VD: Lượng HTS, …
                                if (child.Name.ToString().ToUpper().Equals(NodeName.measureUnit.ToUpper()))
                                    child.SetValue("");     //Đơn vị tính hàng quản lý chuyên ngành	1	an..3	VD: Đơn vị tính HTS, …
                                if (child.Name.ToString().ToUpper().Equals(NodeName.unitPrice.ToUpper()))
                                    child.SetValue("");     // Đơn giá hàng quản lý chuyên ngành	1	n..16,2	VD: Đơn giá HTS, …
                            }
                        }				
                    }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueHangMauDichChiTiet
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="hmd">HangMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValue_HangMauDichChiTiet(XElement Parent, HangMauDich hmd)        // Hàng Hóa Chi Tiết	
        {
            if (Parent != null && Parent.Elements().Count() > 0 && hmd != null)
            {
                foreach (XElement element in Parent.Elements())
                {
                    if (element.Name.ToString().ToUpper().Equals(NodeName.description.ToUpper()))           //Tên hàng	1	an..256
                        element.SetValue(hmd.TenHang);
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.identification.ToUpper()))        //Mã hàng do nhà sản xuất hoặc doanh nghiệp quy định	1	an..30	Nếu không có thì để xâu rỗng
                        element.SetValue(hmd.MaPhu); 
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.tariffClassification.ToUpper()))  //Mã HS	1	n..12	Tối thiểu 8 số
                        element.SetValue(hmd.MaHS);
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.tariffClassificationExtension.ToUpper()))//Mã HS mở rộng	0	n..12	Nếu không có thì để xâu rỗng
                        element.SetValue(hmd.Ma_HTS);   //???????????????????????????
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.brand.ToUpper()))                 //Nhãn hiệu	0	an..256	Nếu không có thì để xâu rỗng
                        element.SetValue("");           //???????????????????????????
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.grade.ToUpper()))                 //Quy cách, phẩm chất	0	an..1000	Nếu không có thì để xâu rỗng
                        element.SetValue("");           //???????????????????????????
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.ingredients.ToUpper()))           //Thành phần	0	an..100	Nếu không có thì để xâu rỗng
                        element.SetValue("");           //???????????????????????????
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.modelNumber.ToUpper()))           //model của hàng hóa	0	an..35	Nếu không có thì để xâu rỗng
                        element.SetValue("");           //???????????????????????????
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.DutyTaxFee.ToUpper()))            //Thuế	1	none
                    {
                        XElement DutyTaxFee = element;
                        if (DutyTaxFee != null && DutyTaxFee.Elements().Count() > 0)
                        {
                            foreach (XElement child in DutyTaxFee.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeName.adValoremTaxBase.ToUpper()))//Số thuế tự tính	1	n..16	Tối đa 2 chữ số thập phân
                                    child.SetValue(hmd.ThueSuatXNK);//??????????????????????????????
                                if (child.Name.ToString().ToUpper().Equals(NodeName.dutyRegime.ToUpper()))
                                    child.SetValue(""); //????????????????????????????????
                                if (child.Name.ToString().ToUpper().Equals(NodeName.specificTaxBase.ToUpper()))
                                    child.SetValue(""); //????????????????????????????????
                                if (child.Name.ToString().ToUpper().Equals(NodeName.tax.ToUpper()))             //Thuế suất	1	n..4,4	Tối đa 4 chữ số thập phân
                                    child.SetValue(hmd.ThueSuatXNK);
                                if (child.Name.ToString().ToUpper().Equals(NodeName.type.ToUpper()))            //Loại thuế	1	n..2	Danh mục chuẩn
                                    child.SetValue(""); //?????????????????????????????????? 

                            }
                        }
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.InvoiceLine.ToUpper()))          //Chưa sử dụng
                    {
                        foreach (XElement childElement in element.Elements())
                        {
                            if (childElement.Name.ToString().Equals(NodeName.itemCharge.ToUpper()))
                            {
                                childElement.SetValue(""); //none	Xâu rỗng
                            }
                            else if (childElement.Name.ToString().ToUpper().Equals(NodeName.line.ToUpper()))
                                childElement.SetValue(""); //none	Xâu rỗng
                        }
                    }

                }
            }
            return Parent;
        }
        /// <summary>
        /// SetCustomsGoodsItemCO (CO of HangMauDich)
        /// </summary>
        /// <param name="Parent">XElement</param>
        /// <param name="hmd">HangMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValue_CustomsGoodsItem_CO(XElement Parent, HangMauDich hmd)       // C/O
        {
            IList<HangMauDich_CO> listHMD_CO = HangMauDich_CO.SelectCollectionBy_HangMauDich_ID(hmd.ID);

            if (Parent != null && hmd != null && listHMD_CO != null && listHMD_CO.Count > 0)
            {
                HangMauDich_CO HMD_CO = listHMD_CO[0];
                CO COOfH = CO.Load(HMD_CO.CO_ID);
                if (COOfH != null)
                {
                    foreach (XElement element in Parent.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeName.issue.ToUpper()))
                            element.SetValue(COOfH.NgayCO);             // Ngày C/O	1	an10	YYYY-MM-DD
                        if (element.Name.ToString().ToUpper().Equals(NodeName.issuer.ToUpper()))
                            element.SetValue(COOfH.ToChucCap);          // Tổ chức, người cấp C/O	1	a..100
                        if (element.Name.ToString().ToUpper().Equals(NodeName.issueLocation.ToUpper()))
                            element.SetValue(COOfH.NuocCapCO);          // Nước cấp	1	a..2	Danh mục chuẩn
                        if (element.Name.ToString().ToUpper().Equals(NodeName.reference.ToUpper()))
                            element.SetValue(COOfH.SoCO);               // Số C/O	1	an..35	Nếu nợ thì số C/O để rỗng
                        if (element.Name.ToString().ToUpper().Equals(NodeName.type.ToUpper()))
                            element.SetValue("");                       // Mã loại C/O	1	an..3	Danh mục chuẩn ?????????????????????
                        if (element.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))
                            element.SetValue(COOfH.LoaiCO);             // Loại C/O	1	an..256	
                        if (element.Name.ToString().ToUpper().Equals(NodeName.expire.ToUpper()))
                            element.SetValue("");                       // Ngày hết hạn C/O	1	an10	YYYY-MM-DD. Nếu không có thì để xâu rỗng
                        if (element.Name.ToString().ToUpper().Equals(NodeName.exporter.ToUpper()))
                            element.SetValue(COOfH.TenDiaChiNguoiXK);   // Tên, địa chỉ người xuất khẩu trên C/O	1	an..1000
                        if (element.Name.ToString().ToUpper().Equals(NodeName.exportationCountry.ToUpper()))
                            element.SetValue(COOfH.MaNuocXKTrenCO);     // Mã nước xuất khẩu trên C/O	1	a..2	Danh mục chuẩn
                        if (element.Name.ToString().ToUpper().Equals(NodeName.importer.ToUpper()))
                            element.SetValue(COOfH.TenDiaChiNguoiNK);   // Tên, địa chỉ người nhập khẩu	1	an..1000	
                        if (element.Name.ToString().ToUpper().Equals(NodeName.importationCountry.ToUpper()))
                            element.SetValue(COOfH.MaNuocNKTrenCO);     // Mã nước nhập khẩu trên C/O	1	a..2	Danh mục chuẩn
                        if (element.Name.ToString().ToUpper().Equals(NodeName.content.ToUpper()))
                            element.SetValue(COOfH.ThongTinMoTaChiTiet);// Thông tin mô tả chung về hàng hóa	1	a..2000	
                    }
                }
            }
            return Parent;          
        }
        /// <summary>
        /// SetCustomsGoodsItemGiayPhep (Giay Phep of hang Mau Dich)
        /// </summary>
        /// <param name="Parent">XElement</param>
        /// <param name="hmd">HangMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValue_CustomsGoodsItem_GiayPhep(XElement Parent, HangMauDich hmd) // Giấy phép	0	none	Không có thì không khai
        {
            HangMauDich_GiayPhep HMD_GP = HangMauDich_GiayPhep.Load(hmd.ID);

            if (Parent != null && hmd != null && HMD_GP != null)
            {
                GiayPhep gp = GiayPhep.Load(HMD_GP.GiayPhep_ID);                
                if (gp != null)
                {
                    foreach (XElement element in Parent.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeName.issue.ToUpper()))
                            element.SetValue(gp.NgayGiayPhep);      // Ngày giấy phép	1	an10	YYYY-MM-DD
                        if (element.Name.ToString().ToUpper().Equals(NodeName.issuer.ToUpper()))
                            element.SetValue(gp.NguoiCap);          // Người cấp giấy phép	1	a..
                        if (element.Name.ToString().ToUpper().Equals(NodeName.issueLocation.ToUpper()))
                            element.SetValue(gp.NoiCap);            // Nơi cấp giấy phép	1	an..200	
                        if (element.Name.ToString().ToUpper().Equals(NodeName.reference.ToUpper()))
                            element.SetValue(gp.SoGiayPhep);        // Số giấy phép	1	an..35	
                        if (element.Name.ToString().ToUpper().Equals(NodeName.type.ToUpper()))
                            element.SetValue(gp.LoaiKB);            // Loại giấy phép	1	an..3	Danh mục chuẩn ????????????
                        if (element.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))
                            element.SetValue("");                   // Tên giấy phép	0	an..256	?????????????????????????????????
                        if (element.Name.ToString().ToUpper().Equals(NodeName.expire.ToUpper()))
                            element.SetValue(gp.NgayHetHan);        //Ngày hết hạn giấy phép	0	an10	YYYY-MM-DD. Nếu không có thì để xâu rỗng
                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// SetValueAdditionalDocument
        /// </summary>
        /// <param name="Root">XElement</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <returns>XElement</returns>
        public static XElement SetValue_ToKhai_HopDongOrVanDon(XElement Parent, ToKhaiMauDich tkmd)  //Hợp đồng	1	none	
        {
            if (Parent != null && Parent.Elements().Count() > 0 && tkmd != null)
            {
                if (Parent.Elements().Count() > 5)
                {
                    HopDongThuongMai hdtm = HopDongThuongMai.LoadBy_SoHopDong(tkmd.SoHopDong);
                    foreach (XElement element in Parent.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeName.issue.ToUpper()))         //Ngày hợp đồng	1	an10	YYYY-MM-DD
                            element.SetValue(tkmd.NgayHopDong);
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.reference.ToUpper()))//Số hợp đồng	1	an..50
                                element.SetValue(tkmd.SoHopDong);
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.type.ToUpper()))     //Loại chứng từ (= 315)	1	an..3	Danh mục chuẩn
                                element.SetValue(tkmd.LoaiToKhaiGiaCong); 
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))     //Hợp đồng	1	a..256	Hợp đồng
                                element.SetValue(""); //???????????????????????
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.issueLocation.ToUpper()))
                                element.SetValue(""); // ???????????????????????????????????
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.issuer.ToUpper()))
                                element.SetValue(""); // ???????????????????????????????????
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.expire.ToUpper()))
                            {
                                if (hdtm != null)
                                    element.SetValue(hdtm.NgayHopDongTM);
                                else
                                    element.SetValue("");
                            }
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.exporter.ToUpper()))
                            element.SetValue(""); // ??????????????????????????????
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.exportationCountry.ToUpper()))
                            element.SetValue(""); // ??????????????????????????????
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.importer.ToUpper()))
                            element.SetValue(""); // ??????????????????????????????
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.importationCountry.ToUpper()))
                            element.SetValue(""); // ??????????????????????????????
                    }
                }
                else
                {
                    // Van Don
                    VanDon vd = VanDon.LoadBy_SoVanDon(tkmd.SoVanDon);
                    foreach (XElement element in Parent.Elements())
                    {
                        if (element.Name.ToString().ToUpper().Equals(NodeName.issue.ToUpper()))             //Ngày vận đơn	1	an10	YYYY-MM-DD
                            element.SetValue(tkmd.NgayVanDon);
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.reference.ToUpper()))    //Số vận đơn	1	an..35
                            element.SetValue(tkmd.SoVanDon);
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.type.ToUpper()))         //Mã loại vận đơn	1	an..3	Danh mục chuẩn
                            {
                                if (vd != null)
                                    element.SetValue(vd.ID); //????????????????????????????
                            }
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.name.ToUpper()))         //Loại vận đơn	1	an..256	
                            element.SetValue(""); //?????????????????????????????
                        else if (element.Name.ToString().ToUpper().Equals(NodeName.expire.ToUpper()))       //Ngày hết hạn CT	0	an10	YYYY-MM-DD. Nếu không có thì để xâu rỗng
                            element.SetValue("");  //??????????????????????????????

                    }
                }
            }
            return Parent;
        }
        /// <summary>
        /// AddVouchers (Add chung tu)
        /// </summary>
        /// <param name="documentXML">XDocument</param>
        /// <param name="tkmd">ToKhaiMauDich</param>
        /// <param name="issuer">int</param>
        /// <param name="function">int</param>
        /// <param name="status">int</param>
        /// <param name="declarationOffice">int</param>
        /// <returns>XDocument</returns>
        public static XElement AddVouchers(XElement Parent, ToKhaiMauDich tkmd, int issuer, int function, int status, int declarationOffice)// Add Chung tu
        {
            if (Parent != null && tkmd != null)
            {
                CuaKhau ck = CuaKhau.Load(tkmd.MaHaiQuan);
                DonViHaiQuan dvhq = DonViHaiQuan.Load(tkmd.MaHaiQuan);    
                foreach(XElement element in Parent.Elements())
                {
                    if(element.Name.ToString().ToUpper().Equals(NodeName.issuer.ToUpper()))             //issuer 	Loại chứng từ
                    {
                        element.SetValue(issuer);
                    }else if (element.Name.ToString().ToUpper().Equals(NodeName.reference.ToUpper()))   // Số tham chiếu tờ khai
                    {
                        element.SetValue(tkmd.ID);
                    }else if (element.Name.ToString().ToUpper().Equals(NodeName.issue.ToUpper()))       // Ngày khai báo
                    {
                       element.SetValue(tkmd.NgayGiayPhep);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.function.ToUpper()))     //Chức năng
                    {
                        element.SetValue(function);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.issueLocation.ToUpper()))//Nơi khai báo
                    {
                        if (ck != null)
                            element.SetValue(ck.Ten);
                        else element.SetValue("");
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.status.ToUpper()))       // Trạng thái của chứng từ
                    {
                        element.SetValue(status);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.customsReference.ToUpper()))//Số tiếp nhận tờ khai; Dùng trong trường hợp khai sửa bản đăng ký
                    {
                        element.SetValue(tkmd.GUIDSTR);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.acceptance.ToUpper()))   // Ngày đăng ký chứng từ 
                    {
                        element.SetValue(tkmd.NgayDangKy);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.declarationOffice.ToUpper())) //Đơn vị HQ khai báo
                    {
                         if (dvhq != null)
                             element.SetValue(dvhq.Ten);
                        else
                             element.SetValue("");
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.natureOfTransaction.ToUpper()))  // Mã loại hình
                    {
                        element.SetValue(tkmd.MaLoaiHinh);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.Agent.ToUpper()))                //Đại lý
                    {
                        SetValueAgent(element, tkmd);
                    }
                    else if (element.Name.ToString().ToUpper().Equals(NodeName.Importer.ToUpper()))             //Người nhập khẩu
                    {
                        SetValueImporter(element,tkmd);
                    }else if (element.Name.ToString().ToUpper().Equals(NodeName.DeclarationDocument.ToUpper()))
                    {
                        SetValueDeclarationDocument(element,tkmd);
                    }
                }             
            }
            return Parent;

        }
    }
}
