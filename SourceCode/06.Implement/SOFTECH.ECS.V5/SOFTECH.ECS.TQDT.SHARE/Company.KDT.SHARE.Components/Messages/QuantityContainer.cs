﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages
{
    public class QuantityContainer
    {
        //Số Container 20
        [XmlElement("quantity20")]
        public string Quantity20 { get; set; }


        //Số Container 40
        [XmlElement("quantity40")]
        public string Quantity40 { get; set; }

        //Số Container 45
        [XmlElement("quantity45")]
        public string Quantity45 { get; set; }

        //Số Container khác
        [XmlElement("quantityOthers")]
        public string QuantityOthers { get; set; }

    }
}
