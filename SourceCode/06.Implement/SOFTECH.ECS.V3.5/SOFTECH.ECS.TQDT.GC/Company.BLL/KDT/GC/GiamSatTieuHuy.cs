﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.GC.BLL.KDT.GC
{
    public partial class GiamSatTieuHuy
    {
        public List<HangGSTieuHuy> HangGSTieuHuys { get; set; }

        public void LoadHangGSTieuHuy()
        {
            HangGSTieuHuys = HangGSTieuHuy.SelectCollectionDynamic("Master_ID=" + ID, "");
        }
    }
}
