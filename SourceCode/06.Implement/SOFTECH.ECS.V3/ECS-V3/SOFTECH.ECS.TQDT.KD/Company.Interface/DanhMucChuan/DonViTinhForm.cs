﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.DanhMucChuan
{
    public partial class DonViTinhForm : BaseForm
    {
        DataSet ds = new DataSet();
        public DonViTinhForm()
        {
            InitializeComponent();
        }

        private void DonViTinhForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            ds = DonViTinh.SelectAll();
            dgList.DataSource = ds;
            dgList.DataMember = ds.Tables[0].TableName;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region GetChanges

                //Updated by Hungtq, 10/08/2012.
                if (ds.Tables[0].GetChanges() != null)
                {
                    DataRow[] rows;

                    foreach (DataRow dr in ds.Tables[0].GetChanges().Rows)
                    {
                        if (dr.RowState == DataRowState.Added)
                        {
                            rows = ds.Tables[0].Select(" ID= '" + dr["ID"].ToString().Trim() + "'");

                            for (int i = 0; i < rows.Length; i++)
                            {
                                rows[i]["DateCreated"] = System.DateTime.Now;
                                rows[i]["DateModified"] = System.DateTime.Now;
                            }
                        }
                        else if (dr.RowState == DataRowState.Modified)
                        {
                            rows = ds.Tables[0].Select(" ID= '" + dr["ID"].ToString().Trim() + "'");

                            for (int i = 0; i < rows.Length; i++)
                            {
                                rows[i]["DateModified"] = System.DateTime.Now;
                            }
                        }
                    }
                }

                #endregion

                DonViTinh.Update(ds);
                ShowMessage(setText("Cập nhật thành công.", "Save successfully"), false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            if (ShowMessage(setText("Bạn có muốn xóa các cửa khẩu này không?", "Do you want to delete ?"), true) != "Yes") e.Cancel = true;
        }
        private bool CheckID(string Id)
        {
            if (ds.Tables[0].Select(" ID= '" + Id.Trim() + "'").Length == 0) return false;
            else return true;
        }
        private void dgList_UpdatingCell(object sender, Janus.Windows.GridEX.UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "ID")
            {
                string s = e.Value.ToString();
                if (CheckID(s))
                {
                    ShowMessage(setText("Mã đơn vị tính này đã có.","This code is exist"), false);
                    e.Cancel = true;
                }
                if (s.Trim().Length == 0)
                {
                    ShowMessage(setText("Mã đơn vị không được rỗng.","Unit code must be filled"), false);
                    e.Cancel = true;
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}