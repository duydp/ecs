﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VNACC_ImportExcelToGrid
{
    public partial class FrmMain : Form
    {
        private List<Utils.ElementEntity> elementCollections = new List<Utils.ElementEntity>();
        private string error = "";

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            numDongBatDau.Value = 5;
            openFileDialog.RestoreDirectory = true;
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            txtPathFile.Text = ChooseFile();

            if (!ValidateControlPathFile()) return;

            if (!LayTenSheet(txtPathFile.Text, cboSheetName, lblMessage)) return;

            if (!LayTenSheet(txtPathFile.Text, cboSheetName, lblMessage)) return;
        }

        private string ChooseFile()
        {
            try
            {
                openFileDialog.FileName = "";
                openFileDialog.InitialDirectory = txtPathFile.Text.Length != 0 ? txtPathFile.Text : Application.StartupPath;
                openFileDialog.DefaultExt = "xls";
                openFileDialog.Filter = "Microsoft Excel|*.xls";

                if (DialogResult.OK == openFileDialog.ShowDialog())
                {
                    return openFileDialog.FileName;
                }
                else
                    return "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return "";
            }
        }

        private bool LayTenSheet(string filePath, ComboBox cboSheetName, System.Windows.Forms.Label lblMessage)
        {
            try
            {
                string error = "";

                List<string> tenSheet = Utils.Utilities.GetWorkSheetName(filePath, ref error);

                lblMessage.Text = "";
                if (error.Length != 0)
                {
                    lblMessage.Text = error;
                    MessageBox.Show(error, this.Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                else
                    lblMessage.Text = "Lấy tên Sheet thành công.";

                cboSheetName.DataSource = tenSheet;

                if (cboSheetName.Items.Count > 0)
                    cboSheetName.SelectedIndex = 0;
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { this.Cursor = Cursors.Default; }

            return true;
        }

        private bool ValidateControl()
        {
            bool valid = true;
            lblMessage.Text = "";

            if (cboSheetName.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập tên Sheet.";
                cboSheetName.Focus();
            }
            else if (numDongBatDau.Value == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập giá trị dòng bắt đầu đọc dữ liệu.";
                numDongBatDau.Focus();
            }
            else if (valid && numDongBatDau.Value <= 0)
            {
                valid = false;
                lblMessage.Text = "Dòng bắt đầu phải lớn hơn 0.";
                numDongBatDau.Focus();
            }

            else if (txtNo.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'No.' cần kiểm tra.";
                txtNo.Focus();
            }
            else if (txtElementNameJP.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'Element name Tiếng Nhật' cần kiểm tra.";
                txtElementNameJP.Focus();
            }
            else if (txtElementNameEN.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'Element name Tiếng Anh' cần kiểm tra.";
                txtElementNameEN.Focus();
            }
            else if (txtElementNameVN.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'Element name Tiếng Việt' cần kiểm tra.";
                txtElementNameVN.Focus();
            }
            else if (txtID.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'ID' cần kiểm tra.";
                txtID.Focus();
            }
            else if (txtAttribute.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'Attribute' cần kiểm tra.";
                txtAttribute.Focus();
            }
            else if (txtDigit.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'Digit' cần kiểm tra.";
                txtDigit.Focus();
            }
            else if (txtRepetition1.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'Repetition 1' cần kiểm tra.";
                txtRepetition1.Focus();
            }
            else if (txtCondition1.Text.Trim().Length == 0 || txtCondition2.Text.Trim().Length == 0
                || txtCondition3.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'Condition 1, 2, 3' cần kiểm tra.";
                txtCondition1.Focus();
            }
            else if (txtCode.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'Code' cần kiểm tra.";
                txtCode.Focus();
            }
            else if (txtFormat.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'Format' cần kiểm tra.";
                txtFormat.Focus();
            }
            else if (txtReferenceDB.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'Reference DB' cần kiểm tra.";
                txtReferenceDB.Focus();
            }
            else if (txtInputConditionJP.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập mã cột 'Input Condition Tiếng Nhật' cần kiểm tra.";
                txtInputConditionJP.Focus();
            }
            //else if (txtInputConditionEN.Text.Trim().Length == 0)
            //{
            //    valid = false;
            //    lblMessage.Text = "Bạn chưa nhập mã cột 'Input Condition Tiếng Anh' cần kiểm tra.";
            //    txtInputConditionEN.Focus();
            //}

            return valid;
        }

        private bool ValidateControlPathFile()
        {
            bool valid = true;
            lblMessage.Text = "";

            if (txtPathFile.Text.Trim().Length == 0)
            {
                valid = false;
                lblMessage.Text = "Bạn chưa nhập đường dẫn file Excel.";
                txtPathFile.Focus();
            }

            return valid;
        }

        private void GetCodeColumnExcel()
        {
            Utils.ColumnExcel.SheetName = cboSheetName.Text;

            Utils.ColumnExcel.CodeNo = Convert.ToChar(txtNo.Text);
            Utils.ColumnExcel.CodeColumn = Convert.ToChar(txtColumn.Text);
            Utils.ColumnExcel.CodeElementNameJP = Convert.ToChar(txtElementNameJP.Text);
            Utils.ColumnExcel.CodeElementNameEN = Convert.ToChar(txtElementNameEN.Text);
            Utils.ColumnExcel.CodeElementNameVN = Convert.ToChar(txtElementNameVN.Text);
            Utils.ColumnExcel.CodeID = Convert.ToChar(txtID.Text);
            Utils.ColumnExcel.CodeAttribute = Convert.ToChar(txtAttribute.Text);
            Utils.ColumnExcel.CodeDigit = Convert.ToChar(txtDigit.Text);
            Utils.ColumnExcel.CodeRepetition1 = Convert.ToChar(txtRepetition1.Text);
            Utils.ColumnExcel.CodeCondition1 = Convert.ToChar(txtCondition1.Text);
            Utils.ColumnExcel.CodeCondition2 = Convert.ToChar(txtCondition2.Text);
            Utils.ColumnExcel.CodeCondition3 = Convert.ToChar(txtCondition3.Text);
            Utils.ColumnExcel.CodeCode = Convert.ToChar(txtCode.Text);
            Utils.ColumnExcel.CodeFormat = Convert.ToChar(txtFormat.Text);
            Utils.ColumnExcel.CodeReferenceDB = Convert.ToChar(txtReferenceDB.Text);
            Utils.ColumnExcel.CodeInputConditionJP = Convert.ToChar(txtInputConditionJP.Text);
            Utils.ColumnExcel.CodeInputConditionEN = txtInputConditionEN.Text.Length > 0 ? Convert.ToChar(txtInputConditionEN.Text) : ' ';

            Utils.ColumnExcel.IntNo = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeNo);
            Utils.ColumnExcel.IntColumn = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeColumn);
            Utils.ColumnExcel.IntElementNameJP = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeElementNameJP);
            Utils.ColumnExcel.IntElementNameEN = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeElementNameEN);
            Utils.ColumnExcel.IntElementNameVN = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeElementNameVN);
            Utils.ColumnExcel.IntID = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeID);
            Utils.ColumnExcel.IntAttribute = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeAttribute);
            Utils.ColumnExcel.IntDigit = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeDigit);
            Utils.ColumnExcel.IntRepetition1 = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeRepetition1);
            Utils.ColumnExcel.IntCondition1 = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeCondition1);
            Utils.ColumnExcel.IntCondition2 = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeCondition2);
            Utils.ColumnExcel.IntCondition3 = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeCondition3);
            Utils.ColumnExcel.IntCode = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeCode);
            Utils.ColumnExcel.IntFormat = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeFormat);
            Utils.ColumnExcel.IntReferenceDB = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeReferenceDB);
            Utils.ColumnExcel.IntInputConditionJP = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeInputConditionJP);
            Utils.ColumnExcel.IntInputConditionEN = Utils.Utilities.ConvertCharToInt(Utils.ColumnExcel.CodeInputConditionEN);
        }

        private void Search()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                //Bao cao ban hang
                elementCollections = Utils.Utilities.ReadExcel(txtPathFile.Text, cboSheetName.Text, (int)numDongBatDau.Value, ref error, 0);

                lblMessage.Text = "";
                if (error.Length != 0)
                {
                    lblMessage.Text = error;
                    return;
                }
                else
                    lblMessage.Text = "Đọc dữ liệu từ file Excel thành công.";


                //Kiem tra tao thong tin lap lai cot 'Column' (neu co)
                int cnt = 0;
                List<Utils.LoopElement> loopColumnss = new List<Utils.LoopElement>();
                Utils.LoopElement loop = null;
                bool isHaveLoop = false;

                #region Kiem tra lap 'Column'
                foreach (Utils.ElementEntity item in elementCollections)
                {
                    cnt += 1;

                    if (item.Column.Trim() != "")
                    {
                        isHaveLoop = true;

                        if (loop == null)
                            loop = new Utils.LoopElement();
                        if (item.Column.Trim() != "*")
                        {
                            try
                            {
                                if (loop.LoopCount == 0)
                                    loop.LoopCount = Convert.ToInt32(item.Column.Trim());
                                loop.IsLoopColumn = true;
                                loop.Elements.Add(item);
                            }
                            catch { }
                        }
                        else
                        {
                            loop.Elements.Add(item);
                        }

                        //Kiem tra khi chuyen tiep tu thong tin Column co gia tri lap nhung la ban ghi cuoi cung thi cung them moi thong tin lap da tao truoc khi thiet lap lai thong tin.
                        if (cnt == elementCollections.Count)
                        {
                            loopColumnss.Add(loop);
                        }
                    }
                    else
                    {
                        //Kiem tra khi chuyen tiep tu thong tin Column co gia tri lap sang rong thi them moi thong tin lap da tao truoc khi thiet lap lai thong tin.
                        if (isHaveLoop)
                            loopColumnss.Add(loop);

                        isHaveLoop = false;
                        loop = null;
                    }
                }

                LoopElement(loopColumnss);

                #endregion

                //Kiem tra tao thong tin lap lai cot 'Repetition1' (neu co)
                cnt = 0;
                List<Utils.LoopElement> loopRepetitions = new List<Utils.LoopElement>();
                loop = null;
                isHaveLoop = false;
                bool isHaveNumericPreviousStep = false;

                #region Kiem tra lap 'Repetition'
                foreach (Utils.ElementEntity item in elementCollections)
                {
                    cnt += 1;

                    if (item.Repetition1.Trim() != "")
                    {
                        isHaveLoop = true;

                        if (loop == null)
                            loop = new Utils.LoopElement();
                        if (item.Repetition1.Trim() != "*")
                        {
                            try
                            {
                                if (isHaveNumericPreviousStep)
                                {
                                    loopRepetitions.Add(loop);

                                    isHaveLoop = false;
                                    loop = null;
                                    isHaveNumericPreviousStep = false;
                                }

                                if (loop == null)
                                    loop = new Utils.LoopElement();
                                if (loop.LoopCount == 0)
                                    loop.LoopCount = Convert.ToInt32(item.Repetition1.Trim());
                                loop.IsLoopRepetition = true;
                                loop.Elements.Add(item);
                                isHaveNumericPreviousStep = true;
                            }
                            catch { }
                        }
                        else
                        {
                            loop.Elements.Add(item);
                        }

                        //Kiem tra khi chuyen tiep tu thong tin Column co gia tri lap nhung la ban ghi cuoi cung thi cung them moi thong tin lap da tao truoc khi thiet lap lai thong tin.
                        if (cnt == elementCollections.Count)
                        {
                            loopRepetitions.Add(loop);
                        }
                    }
                    else
                    {
                        //Kiem tra khi chuyen tiep tu thong tin Column co gia tri lap sang rong thi them moi thong tin lap da tao truoc khi thiet lap lai thong tin.
                        if (isHaveLoop)
                            loopRepetitions.Add(loop);

                        isHaveLoop = false;
                        loop = null;
                        isHaveNumericPreviousStep = false;
                    }
                }

                LoopElement(loopRepetitions);

                #endregion

                //Doc file text du lieu
                string path = @"d:\Working\SOFTECH-ECS\SoftechCollections\ECS - TQDT\06.Implement\Tai lieu\TaiLieuVNACCS\1 Cua\SAA\SAA.txt";
                string[] duLieuText = System.IO.File.ReadAllLines(path);
                Utils.ElementEntity results = null;

                for (int i = 0; i < duLieuText.Length; i++)
                {
                    results = elementCollections.Find(delegate(Utils.ElementEntity o) { return o.No == i.ToString(); });
                    if (results != null)
                    {
                        results.Data = duLieuText[i > 0 ? i - 1 : i];
                    }
                }

                //Binding
                gridMain.DataSource = elementCollections;
                lblMessage.Text = "Bản ghi: " + elementCollections.Count.ToString("00000");
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void LoopElement(List<Utils.LoopElement> loops)
        {
            //Tao thong tin lap theo Column
            foreach (Utils.LoopElement itemLoop in loops)
            {
                itemLoop.FirstNo = int.Parse(itemLoop.Elements[0].No);
                itemLoop.LastNo = int.Parse(itemLoop.Elements[itemLoop.Elements.Count - 1].No);
                itemLoop.IndexInsertNo = itemLoop.LastNo;

                //Tao thong tin lap: so lan lap - thong tin dau tien da co
                List<Utils.ElementEntity> listCopy = new List<Utils.ElementEntity>();
                int nextNo = itemLoop.LastNo;

                for (int i = 1; i < itemLoop.LoopCount; i++)
                {
                    foreach (Utils.ElementEntity itemCopy in itemLoop.Elements)
                    {
                        nextNo += 1;
                        Utils.ElementEntity e = (Utils.ElementEntity)itemCopy.Clone();
                        e.No = nextNo.ToString();
                        listCopy.Add(e);
                    }
                }

                //foreach (Utils.ElementEntity item in listCopy)
                //{
                //    itemLoop.Elements.Add(item);
                //}

                //Cap nhat lai thu tu
                itemLoop.FirstNo = int.Parse(listCopy[0].No);
                itemLoop.LastNo = int.Parse(listCopy[listCopy.Count - 1].No);

                //elementCollections.AddRange(itemLoop.Elements);
                //Lay vi tri index cua element
                int index = elementCollections.FindIndex(delegate(Utils.ElementEntity o) { return o.No == itemLoop.IndexInsertNo.ToString(); });
                //Them moi vao vi tri tiep theo
                elementCollections.InsertRange(++index, listCopy);

                //Thiet lap lai so thu tu cho cac elemeent tiep theo
                int lastIndex = elementCollections.FindIndex(delegate(Utils.ElementEntity o) { return o.No == itemLoop.LastNo.ToString(); });
                int no = Convert.ToInt32(itemLoop.LastNo);
                for (int i = 0; i < elementCollections.Count; i++)
                {
                    if (i > lastIndex)
                    {
                        no += 1;
                        elementCollections[i].No = no.ToString();
                    }
                }
            }
        }

        private void btnReadFile_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (!ValidateControl()) return;

                GetCodeColumnExcel();

                Search();

                MessageBox.Show("Đọc file thành công.");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally { this.Cursor = Cursors.Default; }

        }

        private DataSet GetDataOnGrid()
        {
            DataSet ds = new DataSet();

            try
            {
                this.Cursor = Cursors.WaitCursor;

                gridMain.UpdateData();

                List<Utils.ElementEntity> mainInfo = new List<Utils.ElementEntity>();
                foreach (Utils.ElementEntity item in elementCollections)
                {
                    if (item.Repetition1.Trim() == "")
                    {
                        mainInfo.Add(item);
                    }
                }

                DataTable dtMain = Utils.GenericListToDataTable.ConvertTo(mainInfo);
                dtMain.TableName = "Main";
                ds.Merge(dtMain);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally { this.Cursor = Cursors.Default; }

            return ds;
        }

        private void btnGetData_Click(object sender, EventArgs e)
        {
            DataSet ds = GetDataOnGrid();
        }

        private void gridMain_FormattingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                int i = 1;
                for (i = 0; i < gridMain.RowCount; i++)
                {
                    Utils.ElementEntity obj = (Utils.ElementEntity)gridMain.GetRow(i).DataRow;

                    if (obj.Data != null && obj.Data.Length != (obj.Attribute.ToLower().Trim() != "v" ? Convert.ToInt32(obj.Digit) : Convert.ToInt32(obj.Digit) * 3))
                    {
                        if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                        {
                            Janus.Windows.GridEX.GridEXFormatStyle rowcol = new Janus.Windows.GridEX.GridEXFormatStyle();
                            rowcol.ForeColor = Color.Red;
                            gridMain.GetRow(i).RowStyle = rowcol;                            
                        }
                    }
                }
            }
            catch (Exception ex) { MessageBox.Show(ex.Message); }
        }
    }
}
