﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.DuLieuChuan;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT.SXXK
{
    public partial class SelectSanPhamDMSUAForm : BaseForm
    {
        public SanPham SanPhamSelected = new SanPham();
        public SelectSanPhamDMSUAForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SelectSanPhamDMSUAForm_Load(object sender, EventArgs e)
        {
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            BindData();
        }

        public void BindData()
        {
            DataSet ds = DinhMucDangKy.getSanPhamDaDKDinhMuc(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            dgList.DataSource = ds.Tables[0];

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string maSP = e.Row.Cells["Ma"].Text;
                this.SanPhamSelected.Ma = maSP;
                //this.SanPhamSelected.Ten = e.Row.Cells["Ten"].Text;
                //this.SanPhamSelected.MaHS = e.Row.Cells["MaHS"].Text;
                //this.SanPhamSelected.DVT_ID = e.Row.Cells["DVT_ID"].Text;
                //this.SanPhamSelected.ID = long.Parse(e.Row.Cells["ID"].Text);
                //SanPham.Load(SanPhamSelected.ID);
                this.Hide();
            }
        }
    }
}
