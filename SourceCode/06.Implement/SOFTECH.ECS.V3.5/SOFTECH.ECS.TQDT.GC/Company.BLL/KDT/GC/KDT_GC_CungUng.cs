﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using System.Threading;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Common;
namespace Company.GC.BLL.KDT.GC
{
    public partial class KDT_GC_CungUng
    {
        private List<KDT_GC_CungUng_Detail> _ListCungUngDetail = new List<KDT_GC_CungUng_Detail>();

        public List<KDT_GC_CungUng_Detail> CungUngDetail_List
        {
            set { this._ListCungUngDetail = value; }
            get { return this._ListCungUngDetail; }
        }
        public bool InsertUpdatFull()
        {
            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert(transaction);
                    }
                    else
                        this.Update(transaction);
                    foreach (KDT_GC_CungUng_Detail item in this.CungUngDetail_List)
                    {
                        item.Master_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                        foreach (KDT_GC_CungUng_ChungTu itemdetail in item.CungUngCT_List)
                        {
                            itemdetail.Master_ID = item.ID;
                            if (itemdetail.ID == 0)
                            {
                                itemdetail.Insert(transaction);
                            }
                            else
                            {
                                itemdetail.Update(transaction);
                            }

                        }
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                    throw;
                }
            }
            return ret;
        }
        public void DeleteFull(long id)
        {
            this.ID = id;
            List<KDT_GC_CungUng_Detail> CungUngDetailList = KDT_GC_CungUng_Detail.SelectCollectionDynamic("Master_ID = " + id, "");
            foreach (KDT_GC_CungUng_Detail item in CungUngDetailList)
            {
                KDT_GC_CungUng_ChungTu.DeleteDynamic("Master_ID = " + item.ID);
                item.Delete();
            }
            this.Delete();
        }
        public static KDT_GC_CungUng LoadFull(long id)
        {
            KDT_GC_CungUng Cungung = KDT_GC_CungUng.SelectCollectionDynamic("ID = " + id, "")[0];
            List<KDT_GC_CungUng_Detail> listCU_Detail = KDT_GC_CungUng_Detail.SelectCollectionDynamic("master_id =" + id, "");
            foreach (KDT_GC_CungUng_Detail CU_Detail in listCU_Detail)
            {
                CU_Detail.CungUngCT_List = KDT_GC_CungUng_ChungTu.SelectCollectionDynamic("master_id =" + CU_Detail.ID, "");
                Cungung.CungUngDetail_List.Add(CU_Detail);
            }
            return Cungung;
        }
        public static DataSet SelectTKXTheoHD(long IDHopDong, string MaNPL)
        {
            const string spName = "[dbo].[p_KDT_ToKhaiMauDich_SelectByHopDong]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@IDHopDong", SqlDbType.Int, IDHopDong);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);

            return db.ExecuteDataSet(dbCommand);
        }
        public static DataTable GetDSNPLCungUngByHopDong(long IDHopDong)
        {
            const string spName = "[dbo].[p_KDT_GC_CungUng_DanhSachCUbyHopDong]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.Int, IDHopDong);
            //db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }

        public static DataTable GetDSNPLCungUngByHopDong_New(long IDHopDong)
        {
            const string sql = @"SELECT  cu.MaNPL ,
            SUM(cu.LuongCungUng) AS LuongCungUng
    FROM    dbo.t_KDT_GC_CungUng AS cu
            JOIN dbo.t_KDT_GC_CungUngDangKy AS dk ON dk.ID = cu.Master_ID
    WHERE   dk.HopDong_ID = @HopDong_ID
    GROUP BY cu.MaNPL
    ORDER BY cu.MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);

            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.Int, IDHopDong);
            //db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
    }
  
}
