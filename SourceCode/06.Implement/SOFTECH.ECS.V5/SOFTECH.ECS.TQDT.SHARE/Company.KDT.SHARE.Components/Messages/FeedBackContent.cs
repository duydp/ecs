﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class FeedBackContent:IssueBase
    {
        /// <summary>
        /// Nội dung gửi từ hải quan.
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public List<AdditionalInformation> AdditionalInformations { get; set; }

        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }

        [XmlElement("natureOfTransaction")]
        public string NatureOfTransaction { get; set; }
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }

        [XmlElement("AdditionalDocument")]
        public AdditionalDocument AdditionalDocument { get; set; }
    }
}
