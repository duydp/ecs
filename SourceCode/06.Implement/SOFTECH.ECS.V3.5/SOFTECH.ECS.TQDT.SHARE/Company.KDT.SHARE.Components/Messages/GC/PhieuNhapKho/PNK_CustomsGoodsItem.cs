﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.GC.PhieuNhapKho
{
  public  class PNK_CustomsGoodsItem
    {
        /// <summary>
        /// Chi tiết hàng, thuế
        /// </summary>
        [XmlElement("Commodity")]
        public PNK_Commodity Commodity { get; set; }


        /// <summary>
        /// .....
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public PNK_GoodsMeasure GoodsMeasure { get; set; }

        /// <summary>
        /// Mã người nhận hàng
        /// </summary>
        [XmlElement("DeclarationDocument")]
        public PNK_DeclarationDocument DeclarationDocument { get; set; }

        /// <summary>
        /// Thông tin hợp đồng gia công
        /// </summary>
        [XmlElement("ContractReference")]
        public PNK_ContractReference ContractReference { get; set; }
    }
}
