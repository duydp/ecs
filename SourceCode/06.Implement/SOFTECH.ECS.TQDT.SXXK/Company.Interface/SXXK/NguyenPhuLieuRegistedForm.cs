using System;
using System.Data;
using System.Windows.Forms;
using Company.BLL.DuLieuChuan;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using Company.BLL;
using Company.Interface.Report.SXXK;
using Company.Interface.Report;

namespace Company.Interface.SXXK
{
    public partial class NguyenPhuLieuRegistedForm : BaseForm
    {
        public NguyenPhuLieu NguyenPhuLieuSelected = new NguyenPhuLieu();
        public NguyenPhuLieuCollection NPLCollection = new NguyenPhuLieuCollection();
        private DataSet dsRegistedList;
        public NguyenPhuLieuRegistedForm()
        {
            InitializeComponent();
        }

        private void setDataToComboUserKB()
        {
            DataTable dt = new Company.QuanTri.User().SelectAll().Tables[0];
            DataRow dr = dt.NewRow();
            dr["USER_NAME"] = -1;
            dr["HO_TEN"] = "--Tất cả--";
            dt.Rows.InsertAt(dr, 0);
            cbUserKB.DataSource = dt;
            cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
            cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
        }

        //public void BindData()
        //{
        //    this.NPLCollection = new NguyenPhuLieu().SelectCollectionDynamic("MaDoanhNghiep = '" + this.MaDoanhNghiep + "'", "");
        //    dgList.DataSource = this.NPLCollection;
        //}

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];

            if (this.CalledForm == "DinhMucSendForm")
            {
                //ctrDonViHaiQuan.Enabled = false;
            }

            lblHint.Visible = this.CalledForm != string.Empty;
        }

        //-----------------------------------------------------------------------------------------

        private void NguyenPhuLieuRegistedForm_Load(object sender, EventArgs e)
        {
            lblTongNPL.Text = "0";
            btnExportExcel.Visible = true;

            if (GlobalSettings.MA_DON_VI != "4000395355") btnMapping.Visible = false;
            dgList.Tables[0].Columns["MaDoanhNghiep"].Visible = GlobalSettings.DAI_LY_TTHQ;
            setDataToComboUserKB();
            this.khoitao_DuLieuChuan();
            cbUserKB.SelectedIndex = 0;
            // Nguyên phụ liệu đã đăng ký.
            this.search();
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleNguyenPhuLieu.CapNhatDuLieu)))
                {
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    uiButton1.Visible = false;
                    uiButton3.Visible = false;
                    btnDelete.Visible = false;
                }
            }

            dgList.RootTable.RowHeaderWidth = 60;
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------

        //private void btnGetListFromHQ_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        WSForm wsForm = new WSForm();
        //        wsForm.ShowDialog(this);
        //        if (!wsForm.IsReady) return;
        //        this.Cursor = Cursors.WaitCursor;
        //        this.dsRegistedList = new NguyenPhuLieu().WS_GetDanhSachDaDangKy(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
        //        dgList.DataSource = this.dsRegistedList.Tables[0];
        //        // Cập nhật vào CSDL.
        //        bool ret = new NguyenPhuLieu().UpdateRegistedToDatabase(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, this.dsRegistedList);
        //        this.Cursor = Cursors.Default;
        //        if (ret)
        //        {
        //            this.ShowMessage("Cập nhật thành công", false);
        //            BindData();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        this.Cursor = Cursors.Default;
        //        this.ShowMessage("Có lỗi: " + ex.Message, false);
        //    }
        //    finally
        //    {
        //        this.Cursor = Cursors.Default;
        //    }
        //}

        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (lblHint.Visible)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string maNPL = e.Row.Cells["Ma"].Text;
                    this.NguyenPhuLieuSelected.Ma = maNPL;
                    this.NguyenPhuLieuSelected.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.NguyenPhuLieuSelected.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.NguyenPhuLieuSelected.Ten = e.Row.Cells["Ten"].Text;
                    this.NguyenPhuLieuSelected.MaHS = e.Row.Cells["MaHS"].Text;
                    this.NguyenPhuLieuSelected.DVT_ID = e.Row.Cells["DVT_ID"].Text;
                    this.NguyenPhuLieuSelected.Load();
                    this.Hide();
                }
            }
            else
            {

            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            ImportNPLForm f = new ImportNPLForm();
            f.NPLCollection = this.NPLCollection;
            f.ShowDialog();
            this.search();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            NguyenPhuLieuEditForm f = new NguyenPhuLieuEditForm();
            f.ShowDialog();
            this.search();
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //  if (ShowMessage("Bạn có muốn xóa các nguyên phụ liệu này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa các nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NguyenPhuLieu hmd = (NguyenPhuLieu)i.GetRow().DataRow;
                        hmd.Delete();
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            SelectNPLForm f = new SelectNPLForm();
            f.ShowDialog();
            ReportViewNPLForm f2 = new ReportViewNPLForm();
            if (f.NPLCollectionSelect.Count > 0)
            {
                f2.NPLCollection = f.NPLCollectionSelect;
                f2.Show();
            }
        }
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "MaDoanhNghiep = '" + MaDoanhNghiep + "' AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "'";
            if (txtMaNPL.Text.Trim().Length > 0)
            {
                where += " AND Ma Like '%" + txtMaNPL.Text.Trim() + "%'";
            }

            if (txtTenNPL.Text.Trim().Length > 0)
            {
                where += " AND Ten Like N'%" + txtTenNPL.Text.Trim() + "%'";
            }

            this.NPLCollection = new NguyenPhuLieu().SelectCollectionDynamic(where, "Ma");
            dgList.DataSource = this.NPLCollection;

            lblTongNPL.Text = NPLCollection.Count.ToString();
        }

        private DataTable dtNPL;
        private void searchNguoiKhaiBao(string userName)
        {
            if (userName.Equals("-1"))
            {
                search();
            }
            else
            {
                //if (dtNPL == null || dtNPL.Rows.Count == 0)
                dtNPL = new NguyenPhuLieu().GetNPLFromUserName(userName).Tables[0];
                dgList.DataSource = dtNPL;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            searchNguoiKhaiBao(cbUserKB.SelectedItem.Value.ToString());
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            // if (ShowMessage("Bạn có muốn xóa các nguyên phụ liệu này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa các nguyên phụ liệu này không?", "MSG_DEL01", "", true) == "Yes")
            {

                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        NguyenPhuLieu hmd = (NguyenPhuLieu)i.GetRow().DataRow;
                        hmd.Delete();
                    }
                }
                search();
            }

        }

        private void dgList_RecordUpdated(object sender, EventArgs e)
        {
            GridEXRow row = dgList.GetRow();
            NguyenPhuLieu NPL = (NguyenPhuLieu)row.DataRow;
            if (NPL.Ten == "")
            {
                // ShowMessage("Tên nguyên phụ liệu không được trống", false);
                MLMessages("Tên nguyên phụ liệu không được trống", "MSG_NPL06", "", false);
                search();
                return;
            }
            NPL.Update();
        }

        private void btnMapping_Click(object sender, EventArgs e)
        {
            NguyenPhuLieuMappingForm f = new NguyenPhuLieuMappingForm();
            f.ShowDialog();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "NguyenPhuLieuDaDangKy_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                sfNPL.ShowDialog();

                if (sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //-----------------------------------------------------------------------------------------
    }
}