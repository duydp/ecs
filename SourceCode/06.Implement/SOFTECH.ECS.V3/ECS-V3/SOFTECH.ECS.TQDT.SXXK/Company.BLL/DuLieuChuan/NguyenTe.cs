using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System;
using System.Xml;
using Company.BLL.DuLieuChuan;

namespace Company.BLL.DuLieuChuan
{
    public class NguyenTe : BaseClass
    {
        public static DataSet SelectAll()
        {
            string query = "SELECT * FROM t_HaiQuan_NguyenTe ORDER BY Ten";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteDataSet(dbCommand);
        }
        //Update by Phiph
        public static string SelectName(string id)
        {
            string query = "SELECT Ten FROM t_HaiQuan_NguyenTe WHERE Id='" + id + "'";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            return db.ExecuteScalar(dbCommand).ToString();
        }
        public static void Update(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_NguyenTe VALUES(@Id,@Ten)";
            string update = "UPDATE t_HaiQuan_NguyenTe SET Ten = @Ten WHERE Id=@Id";
            string delete = "DELETE FROM t_HaiQuan_NguyenTe WHERE ID = @Id";

            DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);

            DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@Ten", SqlDbType.NVarChar, "Ten", DataRowVersion.Current);

            DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@Id", SqlDbType.Char, "Id", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
        public static void Update(SqlTransaction transaction, string id, decimal tygia)
        {
            string update = "UPDATE t_HaiQuan_NguyenTe SET TyGiaTinhThue = @TyGiaTinhThue WHERE Id=@Id";

            SqlCommand UpdateCommand = (SqlCommand)db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@Id", SqlDbType.Char, id);
            db.AddInParameter(UpdateCommand, "@TyGiaTinhThue", SqlDbType.Money, tygia);
            if (transaction != null)
                db.ExecuteNonQuery(UpdateCommand, transaction);
            else
                db.ExecuteNonQuery(UpdateCommand);
        }

        public static decimal GetTyGia(string idNT)
        {
            string query = "SELECT TyGiaTinhThue FROM t_HaiQuan_NguyenTe where ID='" + idNT + "'";
            DbCommand dbCommand = db.GetSqlStringCommand(query);
            object o = db.ExecuteScalar(dbCommand);
            decimal tygia = 0;
            try
            {
                tygia = Convert.ToDecimal(o);
            }
            catch
            {

            }
            return tygia;
        }

        public static void UpdateTyGia(XmlElement root)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    foreach (XmlElement item in root.ChildNodes)
                    {
                        Update(transaction, item.ChildNodes[0].InnerText, Convert.ToDecimal(item.ChildNodes[2].InnerText));
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
        }
    }
}