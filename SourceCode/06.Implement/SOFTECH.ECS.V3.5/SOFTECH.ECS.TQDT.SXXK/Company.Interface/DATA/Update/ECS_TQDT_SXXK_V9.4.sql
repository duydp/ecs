
/****** Object:  View [dbo].[v_KDT_SXXK_NPLXuatTon]    Script Date: 06/28/2013 17:08:49 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_KDT_SXXK_NPLXuatTon]'))
DROP VIEW [dbo].[v_KDT_SXXK_NPLXuatTon]
GO


/****** Object:  View [dbo].[v_KDT_SXXK_NPLXuatTon]    Script Date: 06/28/2013 17:08:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*----------------------------------------------------------------------*/
CREATE VIEW [dbo].[v_KDT_SXXK_NPLXuatTon]
AS
SELECT     TOP (100) PERCENT a.SoToKhai, a.MaLoaiHinh, a.NamDangKy, a.MaHaiQuan, c.MaSanPham AS MaSP, b.TenHang AS TenSP, e.Ten AS TenDVT_SP, 
                      b.SoLuong AS LuongSP, c.MaNguyenPhuLieu AS MaNPL, c.DinhMucChung AS DinhMuc, b.SoLuong * c.DinhMucChung AS LuongNPL, 
                      b.SoLuong * c.DinhMucChung AS TonNPL, a.BangKeHoSoThanhLy_ID, CASE WHEN year(d .NGAY_THN_THX) 
                      > 1900 THEN d .Ngay_THN_THX ELSE d .NgayDangKy END AS NgayThucXuat, CASE WHEN year(d .NgayHoanThanh) 
                      > 1900 THEN d .NgayHoanThanh ELSE d .NgayDangKy END AS NgayHoanThanhXuat, d.NgayHoanThanh, a.NgayDangKy, b.MaHaiQuan AS Expr1
FROM         dbo.t_KDT_SXXK_BKToKhaiXuat AS a INNER JOIN
                      dbo.t_SXXK_ToKhaiMauDich AS d ON a.SoToKhai = d.SoToKhai AND a.MaLoaiHinh = d.MaLoaiHinh AND a.NamDangKy = d.NamDangKy AND 
                      a.MaHaiQuan = d.MaHaiQuan INNER JOIN
                      dbo.t_SXXK_HangMauDich AS b ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh AND a.NamDangKy = b.NamDangKy AND 
                      a.MaHaiQuan = b.MaHaiQuan INNER JOIN
                      dbo.t_SXXK_DinhMuc AS c ON b.MaPhu = c.MaSanPham AND b.MaHaiQuan = c.MaHaiQuan AND d.MaDoanhNghiep = c.MaDoanhNghiep INNER JOIN
                      dbo.t_HaiQuan_DonViTinh AS e ON b.DVT_ID = e.ID
WHERE     (a.MaLoaiHinh LIKE 'XSX%' OR
                      a.MaLoaiHinh LIKE 'XCX%') AND (d.Xuat_NPL_SP = 'S')
ORDER BY d.NgayHoanThanh, a.SoToKhai

GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "a"
            Begin Extent = 
               Top = 0
               Left = 8
               Bottom = 214
               Right = 215
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "d"
            Begin Extent = 
               Top = 47
               Left = 253
               Bottom = 274
               Right = 456
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "b"
            Begin Extent = 
               Top = 6
               Left = 486
               Bottom = 304
               Right = 646
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "c"
            Begin Extent = 
               Top = 43
               Left = 679
               Bottom = 300
               Right = 855
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "e"
            Begin Extent = 
               Top = 230
               Left = 0
               Bottom = 346
               Right = 159
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_KDT_SXXK_NPLXuatTon'
GO

EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'v_KDT_SXXK_NPLXuatTon'
GO




/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_SelectBy_Master_ID]    Script Date: 06/28/2013 17:03:52 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_SelectBy_Master_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectBy_Master_ID]
GO



/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_DinhMuc_SelectBy_Master_ID]    Script Date: 06/28/2013 17:03:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_SelectBy_Master_ID]
-- Database: HaiQuanLuoi
-- Author: Ngo Thanh Tung
-- Time created: Monday, August 25, 2008
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	a.*, b.Ten as TenNPL
FROM
	t_KDT_SXXK_DinhMuc a 
	INNER JOIN t_KDT_SXXK_DinhMucDangKy dmdk ON 
	dmdk.id = a.Master_ID
	INNER JOIN t_SXXK_NguyenPhuLieu b  ON a.MaNguyenPhuLieu = b.Ma
	AND b.MaHaiQuan = dmdk.MaHaiQuan AND b.MaDoanhNghiep = dmdk.MaDoanhNghiep
WHERE
	a.[Master_ID] = @Master_ID



GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '9.4') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('9.4',GETDATE(), N'Cập nhật thanh khoản nhiều chi cục')
END