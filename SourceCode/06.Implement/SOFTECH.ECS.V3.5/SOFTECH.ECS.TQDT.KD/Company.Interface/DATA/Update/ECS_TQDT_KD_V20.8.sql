GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
SET ANSI_WARNINGS ON
GO
SET ANSI_NULLS ON
GO
DELETE FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522'
GO
--------------------------------------BẢNG MÃ SẮC THUẾ DÙNG TRONG VNACCS  ---------------------------------------
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='X')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN) VALUES  ( 'A522' ,'X' ,N'Thuế xuất khẩu cho hàng miễn thuế XK',N'Thuế xuất khẩu')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='X' ,Name_VN = N'Thuế xuất khẩu cho hàng miễn thuế XK' ,Name_EN=N'Thuế xuất khẩu'  WHERE ReferenceDB='A522' AND Code='X'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='N')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN) VALUES  ( 'A522' ,'N' ,N'Thuế nhập khẩu cho hàng miễn thuế NK',N'Thuế nhập khẩu')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='N' ,Name_VN = N'Thuế nhập khẩu cho hàng miễn thuế NK' ,Name_EN=N'Thuế nhập khẩu'  WHERE ReferenceDB='A522' AND Code='N'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN) VALUES  ( 'A522' ,'B' ,N'Thuế tự vệ cho hàng miễn thuế TV',N'Thuế tự vệ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B' ,Name_VN = N'Thuế tự vệ cho hàng miễn thuế TV' ,Name_EN=N'Thuế tự vệ'  WHERE ReferenceDB='A522' AND Code='B'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN) VALUES  ( 'A522' ,'G' ,N'Thuế chống bán phá giá cho hàng miễn thuế CBPG',N'Thuế chống bán phá giá')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G' ,Name_VN = N'Thuế chống bán phá giá cho hàng miễn thuế CBPG' ,Name_EN=N'Thuế chống bán phá giá'  WHERE ReferenceDB='A522' AND Code='G'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='C')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN) VALUES  ( 'A522' ,'C' ,N'Thuế chống trợ cấp cho hàng miễn thuế CTC',N'Thuế chống trợ cấp')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='C' ,Name_VN = N'Thuế chống trợ cấp cho hàng miễn thuế CTC' ,Name_EN=N'Thuế chống trợ cấp'  WHERE ReferenceDB='A522' AND Code='C'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='P')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN) VALUES  ( 'A522' ,'P' ,N'Thuế chống phân biệt đối xử cho hàng miễn thuế CPBĐX',N'Thuế chống phân biệt đối xử')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='P' ,Name_VN = N'Thuế chống phân biệt đối xử cho hàng miễn thuế CPBĐX' ,Name_EN=N'Thuế chống phân biệt đối xử'  WHERE ReferenceDB='A522' AND Code='P'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='D')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN) VALUES  ( 'A522' ,'D' ,N'Mã dự trữ',N'Mã dự trữ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='D' ,Name_VN = N'Mã dự trữ' ,Name_EN=N'Mã dự trữ'  WHERE ReferenceDB='A522' AND Code='D'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='E')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN) VALUES  ( 'A522' ,'E' ,N'Mã dự trữ',N'Mã dự trữ')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='E' ,Name_VN = N'Mã dự trữ' ,Name_EN=N'Mã dự trữ'  WHERE ReferenceDB='A522' AND Code='E'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='T')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN) VALUES  ( 'A522' ,'T' ,N'Thuế tiêu thụ đặc biệt cho hàng miễn thuế TTĐB',N'Thuế tiêu thụ đặc biệt')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='T' ,Name_VN = N'Thuế tiêu thụ đặc biệt cho hàng miễn thuế TTĐB' ,Name_EN=N'Thuế tiêu thụ đặc biệt'  WHERE ReferenceDB='A522' AND Code='T'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='M')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN) VALUES  ( 'A522' ,'M' ,N'Thuế Bảo vệ môi trường cho hàng miễn thuế BVMT',N'Thuế Bảo vệ môi trường')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='M' ,Name_VN = N'Thuế Bảo vệ môi trường cho hàng miễn thuế BVMT' ,Name_EN=N'Thuế Bảo vệ môi trường'  WHERE ReferenceDB='A522' AND Code='M'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='V')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN) VALUES  ( 'A522' ,'V' ,N'Thuế Giá trị gia tăng cho hàng miễn thuế GTGT',N'Thuế Giá trị gia tăng')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='V' ,Name_VN = N'Thuế Giá trị gia tăng cho hàng miễn thuế GTGT' ,Name_EN=N'Thuế Giá trị gia tăng'  WHERE ReferenceDB='A522' AND Code='V'
END

END
--------------------------------------BẢNG MÃ THUẾ SUẤT THUẾ CHỐNG BÁN PHÁ GIÁ DÙNG TRONG VNACCS  ---------------------------------------
BEGIN
 IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G001')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'G001' ,N'Tên nhà SX/XK : Lianzhong stainless Steel Corporation - Trung Quốc (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019',N'Thuế chống bán phá giá',N'25.35%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G001' ,Name_VN = N'Tên nhà SX/XK : Lianzhong stainless Steel Corporation - Trung Quốc (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019' ,Name_EN=N'Thuế chống bán phá giá' ,Notes=N'25.35%' WHERE ReferenceDB='A522' AND Code='G001'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G002')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'G002' ,N'Tên nhà SX/XK : Fujian Southeast stainless Steel Co., Ltd - Trung Quốc (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019',N'Thuế chống bán phá giá',N'25.35%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G002' ,Name_VN = N'Tên nhà SX/XK : Fujian Southeast stainless Steel Co., Ltd - Trung Quốc (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019' ,Name_EN=N'Thuế chống bán phá giá' ,Notes=N'25.35%' WHERE ReferenceDB='A522' AND Code='G002'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G003')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'G003' ,N'Tên nhà SX/XK : Các nhà sản xuất khác (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019',N'Thuế chống bán phá giá',N'25.35%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G003' ,Name_VN = N'Tên nhà SX/XK : Các nhà sản xuất khác (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019' ,Name_EN=N'Thuế chống bán phá giá' ,Notes=N'25.35%' WHERE ReferenceDB='A522' AND Code='G003'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G004')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'G004' ,N'Tên nhà SX/XK : PT Jindal stainless  Indonesia - Indonesia (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019',N'Thuế chống bán phá giá',N'13.03%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G004' ,Name_VN = N'Tên nhà SX/XK : PT Jindal stainless  Indonesia - Indonesia (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019' ,Name_EN=N'Thuế chống bán phá giá' ,Notes=N'13.03%' WHERE ReferenceDB='A522' AND Code='G004'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G005')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'G005' ,N'Tên nhà SX/XK : Các nhà sản xuất khác - Indonesia (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019',N'Thuế chống bán phá giá',N'13.03%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G005' ,Name_VN = N'Tên nhà SX/XK : Các nhà sản xuất khác - Indonesia (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019' ,Name_EN=N'Thuế chống bán phá giá' ,Notes=N'13.03%' WHERE ReferenceDB='A522' AND Code='G005'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G006')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'G006' ,N'Tên nhà SX/XK : Bahru stainless sdn.Bhd. - Malaysia (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019',N'Thuế chống bán phá giá',N'9.31%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G006' ,Name_VN = N'Tên nhà SX/XK : Bahru stainless sdn.Bhd. - Malaysia (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019' ,Name_EN=N'Thuế chống bán phá giá' ,Notes=N'9.31%' WHERE ReferenceDB='A522' AND Code='G006'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G007')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'G007' ,N'Tên nhà SX/XK : Các nhà sản xuất khác - Malaysia (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019',N'Thuế chống bán phá giá',N'9.55%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G007' ,Name_VN = N'Tên nhà SX/XK : Các nhà sản xuất khác - Malaysia (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019' ,Name_EN=N'Thuế chống bán phá giá' ,Notes=N'9.55%' WHERE ReferenceDB='A522' AND Code='G007'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G008')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'G008' ,N'Tên nhà SX/XK : Yieh United Steel Corporation - Đài Loan (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019',N'Thuế chống bán phá giá',N'13.79%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G008' ,Name_VN = N'Tên nhà SX/XK : Yieh United Steel Corporation - Đài Loan (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019' ,Name_EN=N'Thuế chống bán phá giá' ,Notes=N'13.79%' WHERE ReferenceDB='A522' AND Code='G008'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G009')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'G009' ,N'Tên nhà SX/XK : Yuan Long stainless Steel Corp. - Đài Loan (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019',N'Thuế chống bán phá giá',N'37.29%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G009' ,Name_VN = N'Tên nhà SX/XK : Yuan Long stainless Steel Corp. - Đài Loan (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019' ,Name_EN=N'Thuế chống bán phá giá' ,Notes=N'37.29%' WHERE ReferenceDB='A522' AND Code='G009'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G010')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'G010' ,N'Tên nhà SX/XK : Các nhà sản xuất khác - Đài Loan (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019',N'Thuế chống bán phá giá',N'13.79%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G010' ,Name_VN = N'Tên nhà SX/XK : Các nhà sản xuất khác - Đài Loan (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019' ,Name_EN=N'Thuế chống bán phá giá' ,Notes=N'13.79%' WHERE ReferenceDB='A522' AND Code='G010'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='G011')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'G011' ,N'Tên nhà SX/XK : Shanxi Taigang Stainless Steel Co., Ltd (STSS) - Trung Quốc (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019',N'Thuế chống bán phá giá',N'17.47%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='G011' ,Name_VN = N'Tên nhà SX/XK : Shanxi Taigang Stainless Steel Co., Ltd (STSS) - Trung Quốc (Thuế suất thuế CBPG 1656/QĐ-BCT; 3465/QĐ-BCT ngày 23/8/2016 giai đoạn từ 14/5/2016 đến 6/10/2019' ,Name_EN=N'Thuế chống bán phá giá' ,Notes=N'17.47%' WHERE ReferenceDB='A522' AND Code='G011'
END

END
--------------------------------------BẢNG MÃ BIỂU THUẾ TỰ VỆ TRONG VNACCS ---------------------------------------
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B001')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B001' ,N'Dầu nành tinh luyện (Mã HS : 1507.90.90 )( Giai đoạn áp dụng : 7/5/2016-6/5/2017)',N'Thuế tự vệ',N'2%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B001' ,Name_VN = N'Dầu nành tinh luyện (Mã HS : 1507.90.90 )( Giai đoạn áp dụng : 7/5/2016-6/5/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'2%'  WHERE ReferenceDB='A522' AND Code='B001'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B002')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B002' ,N'Dầu cọ stearin tinh luyện  (Mã HS : 1511.90.91 )( Giai đoạn áp dụng : 7/5/2016-6/5/2017)',N'Thuế tự vệ',N'2%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B002' ,Name_VN = N'Dầu cọ stearin tinh luyện  (Mã HS : 1511.90.91 )( Giai đoạn áp dụng : 7/5/2016-6/5/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'2%'  WHERE ReferenceDB='A522' AND Code='B002'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B003')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B003' ,N'Dầu olein tinh luyện IV57 (Mã HS : 1511.90.92 )( Giai đoạn áp dụng : 7/5/2016-6/5/2017)',N'Thuế tự vệ',N'2%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B003' ,Name_VN = N'Dầu olein tinh luyện IV57 (Mã HS : 1511.90.92 )( Giai đoạn áp dụng : 7/5/2016-6/5/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'2%'  WHERE ReferenceDB='A522' AND Code='B003'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B004')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B004' ,N'Dầu olein tinh luyện IV57 (Mã HS : 1511.90.99 )( Giai đoạn áp dụng : 7/5/2016-6/5/2017)',N'Thuế tự vệ',N'2%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B004' ,Name_VN = N'Dầu olein tinh luyện IV57 (Mã HS : 1511.90.99 )( Giai đoạn áp dụng : 7/5/2016-6/5/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'2%'  WHERE ReferenceDB='A522' AND Code='B004'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B005')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B005' ,N'Mặt cắt ngang hình chữ nhật (kể cả hình vuông), có chiều rộng nhỏ hơn hai lần chiều dày (Mã HS : 7207.11.00 )( Giai đoạn áp dụng : 22/3/2016-21/3/2017)',N'Thuế tự vệ',N'23,3%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B005' ,Name_VN = N'Mặt cắt ngang hình chữ nhật (kể cả hình vuông), có chiều rộng nhỏ hơn hai lần chiều dày (Mã HS : 7207.11.00 )( Giai đoạn áp dụng : 22/3/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'23,3%' WHERE ReferenceDB='A522' AND Code='B005'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B006')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B006' ,N'Loại khác (Mã HS : 7207.19.00 )( Giai đoạn áp dụng : 22/3/2016-21/3/2017)',N'Thuế tự vệ',N'23,3%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B006' ,Name_VN = N'Loại khác (Mã HS : 7207.19.00 )( Giai đoạn áp dụng : 22/3/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'23,3%' WHERE ReferenceDB='A522' AND Code='B006'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B007')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B007' ,N' Loại khác (Mã HS : 7207.20.29 )( Giai đoạn áp dụng : 22/3/2016-21/3/2017)',N'Thuế tự vệ',N'23,3%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B007' ,Name_VN = N' Loại khác (Mã HS : 7207.20.29 )( Giai đoạn áp dụng : 22/3/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'23,3%' WHERE ReferenceDB='A522' AND Code='B007'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B008')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B008' ,N'Loại khác (Mã HS : 7207.20.99 )( Giai đoạn áp dụng : 22/3/2016-21/3/2017)',N'Thuế tự vệ',N'23,3%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B008' ,Name_VN = N'Loại khác (Mã HS : 7207.20.99 )( Giai đoạn áp dụng : 22/3/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'23,3%' WHERE ReferenceDB='A522' AND Code='B008'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B009')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B009' ,N'Loại khác (Mã HS : 7224.90.00 )( Giai đoạn áp dụng : 22/3/2016-21/3/2017)',N'Thuế tự vệ',N'23,3%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B009' ,Name_VN = N'Loại khác (Mã HS : 7224.90.00 )( Giai đoạn áp dụng : 22/3/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'23,3%' WHERE ReferenceDB='A522' AND Code='B009'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B010')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B010' ,N'Có răng khía, rãnh, gân hoặc các dạng khác được tạo thành trong quá trình cán (Mã HS : 7213.10.00)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)',N'Thuế tự vệ',N'15.4%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B010' ,Name_VN = N'Có răng khía, rãnh, gân hoặc các dạng khác được tạo thành trong quá trình cán (Mã HS : 7213.10.00)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'15.4%'  WHERE ReferenceDB='A522' AND Code='B010'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B011')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B011' ,N'Thép cốt bê tông (Mã HS : 7213.91.20)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)',N'Thuế tự vệ',N'15.4%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B011' ,Name_VN = N'Thép cốt bê tông (Mã HS : 7213.91.20)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'15.4%'  WHERE ReferenceDB='A522' AND Code='B011'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B012')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B012' ,N'Thép cốt bê tông (Mã HS : 7214.20.31)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)',N'Thuế tự vệ',N'15.4%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B012' ,Name_VN = N'Thép cốt bê tông (Mã HS : 7214.20.31)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'15.4%'  WHERE ReferenceDB='A522' AND Code='B012'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B013')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B013' ,N'Thép cốt bê tông (Mã HS : 7214.20.41)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)',N'Thuế tự vệ',N'15.4%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B013' ,Name_VN = N'Thép cốt bê tông (Mã HS : 7214.20.41)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'15.4%'  WHERE ReferenceDB='A522' AND Code='B013'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B014')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B014' ,N'Loại khác (Mã HS : 7227.90.00)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)',N'Thuế tự vệ',N'15.4%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B014' ,Name_VN = N'Loại khác (Mã HS : 7227.90.00)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'15.4%'  WHERE ReferenceDB='A522' AND Code='B014'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B015')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B015' ,N'Có mặt cắt ngang hình tròn (Mã HS : 7228.30.10)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)',N'Thuế tự vệ',N'15.4%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B015' ,Name_VN = N'Có mặt cắt ngang hình tròn (Mã HS : 7228.30.10)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'15.4%'  WHERE ReferenceDB='A522' AND Code='B015'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B016')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B016' ,N'Thép hợp kim có chứa nguyên tố Bo và /hoặc Crôm trừ chủng loại thép cán phẳng đặc cán nóng, thuộc mã số 9811.00.00 (Mã HS : 7224.90.00,7227.90.00,7228.30.10)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)',N'Thuế tự vệ',N'15.4%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B016' ,Name_VN = N'Thép hợp kim có chứa nguyên tố Bo và /hoặc Crôm trừ chủng loại thép cán phẳng đặc cán nóng, thuộc mã số 9811.00.00 (Mã HS : 7224.90.00,7227.90.00,7228.30.10)( Giai đoạn áp dụng : 02/8/2016-21/3/2017)' ,Name_EN=N'Thuế tự vệ',Notes=N'15.4%'  WHERE ReferenceDB='A522' AND Code='B016'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='B017')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'B017' ,N'Muối natri của axit glutamic (MSG) (Mã HS : 2922.42.20)( Giai đoạn áp dụng : 25/3/2016-24/3/2017)',N'Thuế tự vệ',N'4.390.999 đồng/tấn')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='B017' ,Name_VN = N'Muối natri của axit glutamic (MSG) (Mã HS : 2922.42.20)( Giai đoạn áp dụng : 25/3/2016-24/3/2017)' ,Name_EN=N'Thuế tự vệ' ,Notes=N'4.390.999 đồng/tấn' WHERE ReferenceDB='A522' AND Code='B017'
END

END
--------------------------------------BẢNG MÃ THUẾ SUẤT THUẾ TIÊU THỤ ĐẶC BIỆT DÙNG TRONG VNACCS ---------------------------------------
BEGIN
    IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB010')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB010' ,N'Thuốc lá điếu, xì gà và các chế phẩm khác từ cây thuốc lá.',N'Thuế tiêu thụ đặc biệt',N'70%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB010' ,Name_VN = N'Thuốc lá điếu, xì gà và các chế phẩm khác từ cây thuốc lá.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'70%'  WHERE ReferenceDB='A522' AND Code='TB010'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB020')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB020' ,N'Rượu từ 20 độ trở lên',N'Thuế tiêu thụ đặc biệt',N'55%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB020' ,Name_VN = N'Rượu từ 20 độ trở lên' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'55%'  WHERE ReferenceDB='A522' AND Code='TB020'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB030')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB030' ,N'Rượu dưới 20 độ.',N'Thuế tiêu thụ đặc biệt',N'30%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB030' ,Name_VN = N'Rượu dưới 20 độ.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'30%'  WHERE ReferenceDB='A522' AND Code='TB030'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB040')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB040' ,N'Bia',N'Thuế tiêu thụ đặc biệt',N'55%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB040' ,Name_VN = N'Bia' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'55%'  WHERE ReferenceDB='A522' AND Code='TB040'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB051')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB051' ,N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh từ 1.500 cm3 trở xuống; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'40%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB051' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh từ 1.500 cm3 trở xuống; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'40%'  WHERE ReferenceDB='A522' AND Code='TB051'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB052')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB052' ,N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 1.500 cm3 đến 2.000 cm3 ; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'45%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB052' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 1.500 cm3 đến 2.000 cm3 ; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'45%'  WHERE ReferenceDB='A522' AND Code='TB052'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB061')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB061' ,N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 2.500 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'50%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB061' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 2.500 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'50%'  WHERE ReferenceDB='A522' AND Code='TB061'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB062')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB062' ,N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.500 cm3 đến 3.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'55%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB062' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.500 cm3 đến 3.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'55%'  WHERE ReferenceDB='A522' AND Code='TB062'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB070')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB070' ,N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 1 Luật sửa đổi bổ sung một số Điều của Luật thuế tiêu thụ đặc biệt số 70/2014/QH13',N'Thuế tiêu thụ đặc biệt',N'60%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB070' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g Điều 1 Luật sửa đổi bổ sung một số Điều của Luật thuế tiêu thụ đặc biệt số 70/2014/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'60%'  WHERE ReferenceDB='A522' AND Code='TB070'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB071')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB071' ,N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3  đến 4.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'90%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB071' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3  đến 4.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'90%'  WHERE ReferenceDB='A522' AND Code='TB071'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB072')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB072' ,N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 4.000 cm3  đến 5.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'110%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB072' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 4.000 cm3  đến 5.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'110%'  WHERE ReferenceDB='A522' AND Code='TB072'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB073')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB073' ,N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 5.000 cm3  đến 6.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'130%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB073' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 5.000 cm3  đến 6.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'130%'  WHERE ReferenceDB='A522' AND Code='TB073'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB074')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB074' ,N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 6.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'150%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB074' ,Name_VN = N'Xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 6.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'150%'  WHERE ReferenceDB='A522' AND Code='TB074'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB080')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB080' ,N'Xe ô tô chở người từ 10 đến dưới 16 chỗ, trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'15%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB080' ,Name_VN = N'Xe ô tô chở người từ 10 đến dưới 16 chỗ, trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'15%'  WHERE ReferenceDB='A522' AND Code='TB080'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB090')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB090' ,N'Xe ô tô chở người từ 16 đến dưới 24 chỗ, trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'10%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB090' ,Name_VN = N'Xe ô tô chở người từ 16 đến dưới 24 chỗ, trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'10%'  WHERE ReferenceDB='A522' AND Code='TB090'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB101')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB101' ,N'Xe ô tô vừa chở người, vừa chở hàng có dung tích xi lanh từ 2.500 cm3 trở xuống ; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'15%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB101' ,Name_VN = N'Xe ô tô vừa chở người, vừa chở hàng có dung tích xi lanh từ 2.500 cm3 trở xuống ; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'15%'  WHERE ReferenceDB='A522' AND Code='TB101'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB102')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB102' ,N'Xe ô tô vừa chở người, vừa chở hàng có dung tích xi lanh trên 2.500 cm3 đến 3.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'20%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB102' ,Name_VN = N'Xe ô tô vừa chở người, vừa chở hàng có dung tích xi lanh trên 2.500 cm3 đến 3.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'20%'  WHERE ReferenceDB='A522' AND Code='TB102'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB103')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB103' ,N'Xe ô tô vừa chở người, vừa chở hàng có dung tích xi lanh trên 3.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13',N'Thuế tiêu thụ đặc biệt',N'25%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB103' ,Name_VN = N'Xe ô tô vừa chở người, vừa chở hàng có dung tích xi lanh trên 3.000 cm3; trừ loại quy định tại điểm 4đ, 4e và 4g  Khoản 2 Điều 2 Luật số 106/2016/QH13' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'25%'  WHERE ReferenceDB='A522' AND Code='TB103'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB111')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB111' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh từ 1.500 cm3 trở xuống.',N'Thuế tiêu thụ đặc biệt',N'28%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB111' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh từ 1.500 cm3 trở xuống.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'28%'  WHERE ReferenceDB='A522' AND Code='TB111'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB112')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB112' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 1.500 cm3 đến 2.000 cm3',N'Thuế tiêu thụ đặc biệt',N'31,5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB112' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 1.500 cm3 đến 2.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'31,5%'  WHERE ReferenceDB='A522' AND Code='TB112'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB121')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB121' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 2.500 cm3.',N'Thuế tiêu thụ đặc biệt',N'35%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB121' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 2.500 cm3.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'35%'  WHERE ReferenceDB='A522' AND Code='TB121'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB122')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB122' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 2.500 cm3.',N'Thuế tiêu thụ đặc biệt',N'38,5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB122' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 2.500 cm3.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'38,5%'  WHERE ReferenceDB='A522' AND Code='TB122'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB131')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB131' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3 đến 4.000 cm3',N'Thuế tiêu thụ đặc biệt',N'63%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB131' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3 đến 4.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'63%'  WHERE ReferenceDB='A522' AND Code='TB131'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB132')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB132' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 4.000 cm3 đến 5.000 cm3',N'Thuế tiêu thụ đặc biệt',N'77%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB132' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 4.000 cm3 đến 5.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'77%'  WHERE ReferenceDB='A522' AND Code='TB132'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB133')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB133' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 5.000 cm3 đến 6.000 cm3',N'Thuế tiêu thụ đặc biệt',N'91%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB133' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 5.000 cm3 đến 6.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'91%'  WHERE ReferenceDB='A522' AND Code='TB133'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB134')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB134' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên  6.000 cm3',N'Thuế tiêu thụ đặc biệt',N'105%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB134' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên  6.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'105%'  WHERE ReferenceDB='A522' AND Code='TB134'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB140')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB140' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 10 đến dưới 16 chỗ.',N'Thuế tiêu thụ đặc biệt',N'10,5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB140' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 10 đến dưới 16 chỗ.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'10,5%'  WHERE ReferenceDB='A522' AND Code='TB140'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB150')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB150' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 16 đến dưới 24 chỗ.',N'Thuế tiêu thụ đặc biệt',N'7%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB150' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô chở người từ 16 đến dưới 24 chỗ.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'7%'  WHERE ReferenceDB='A522' AND Code='TB150'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB161')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB161' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô vừa chở người, vừa chở hàng, có dung tích xi lanh từ 2.500 cm3 trở xuống',N'Thuế tiêu thụ đặc biệt',N'10,5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB161' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô vừa chở người, vừa chở hàng, có dung tích xi lanh từ 2.500 cm3 trở xuống' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'10,5%'  WHERE ReferenceDB='A522' AND Code='TB161'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB162')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB162' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô vừa chở người, vừa chở hàng, có dung tích xi lanh trên 2.500 cm3 đến 3.000 cm3',N'Thuế tiêu thụ đặc biệt',N'14%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB162' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô vừa chở người, vừa chở hàng, có dung tích xi lanh trên 2.500 cm3 đến 3.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'14%'  WHERE ReferenceDB='A522' AND Code='TB162'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB163')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB163' ,N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô vừa chở người, vừa chở hàng, có dung tích xi lanh trên 3.000 cm3',N'Thuế tiêu thụ đặc biệt',N'17,5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB163' ,Name_VN = N'Xe ô tô chạy bằng xăng kết hợp năng lượng điện, năng lượng sinh học, trong đó tỷ trọng xăng sử dụng không quá 70% số năng lượng sử dụng; thuộc loại xe ô tô vừa chở người, vừa chở hàng, có dung tích xi lanh trên 3.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'17,5%'  WHERE ReferenceDB='A522' AND Code='TB163'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB171')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB171' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh từ 1.500 cm3 trở xuống.',N'Thuế tiêu thụ đặc biệt',N'20%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB171' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh từ 1.500 cm3 trở xuống.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'20%'  WHERE ReferenceDB='A522' AND Code='TB171'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB172')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB172' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 1.500 cm3 đến 2.000 cm3',N'Thuế tiêu thụ đặc biệt',N'22,5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB172' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 1.500 cm3 đến 2.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'22,5%'  WHERE ReferenceDB='A522' AND Code='TB172'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB181')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB181' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 2.500 cm3 .',N'Thuế tiêu thụ đặc biệt',N'25%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB181' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.000 cm3 đến 2.500 cm3 .' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'25%'  WHERE ReferenceDB='A522' AND Code='TB181'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB182')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB182' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.500 cm3 đến 3.000 cm3 .',N'Thuế tiêu thụ đặc biệt',N'27,5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB182' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 2.500 cm3 đến 3.000 cm3 .' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'27,5%'  WHERE ReferenceDB='A522' AND Code='TB182'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB191')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB191' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3 đến 4.000 cm3',N'Thuế tiêu thụ đặc biệt',N'45%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB191' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 3.000 cm3 đến 4.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'45%'  WHERE ReferenceDB='A522' AND Code='TB191'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB192')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB192' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 4.000 cm3 đến 5.000 cm3',N'Thuế tiêu thụ đặc biệt',N'55%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB192' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 4.000 cm3 đến 5.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'55%'  WHERE ReferenceDB='A522' AND Code='TB192'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB193')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB193' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 5.000 cm3 đến 6.000 cm3',N'Thuế tiêu thụ đặc biệt',N'65%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB193' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 5.000 cm3 đến 6.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'65%'  WHERE ReferenceDB='A522' AND Code='TB193'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB194')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB194' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 6.000 cm3',N'Thuế tiêu thụ đặc biệt',N'75%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB194' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 9 chỗ trở xuống có dung tích xi lanh trên 6.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'75%'  WHERE ReferenceDB='A522' AND Code='TB194'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB200')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB200' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 10 đến dưới 16 chỗ.',N'Thuế tiêu thụ đặc biệt',N'7,5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB200' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 10 đến dưới 16 chỗ.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'7,5%'  WHERE ReferenceDB='A522' AND Code='TB200'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB210')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB210' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 16 đến dưới 24 chỗ.',N'Thuế tiêu thụ đặc biệt',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB210' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô chở người từ 16 đến dưới 24 chỗ.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'5%'  WHERE ReferenceDB='A522' AND Code='TB210'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB221')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB221' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô vừa chở người, vừa chở hàng, loại có dung tích xi lanh từ 2.500 cm3 trở xuống',N'Thuế tiêu thụ đặc biệt',N'7,5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB221' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô vừa chở người, vừa chở hàng, loại có dung tích xi lanh từ 2.500 cm3 trở xuống' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'7,5%'  WHERE ReferenceDB='A522' AND Code='TB221'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB222')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB222' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô vừa chở người, vừa chở hàng, loại có dung tích xi lanh từ 2.500 cm3 đến 3.000 cm3',N'Thuế tiêu thụ đặc biệt',N'10%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB222' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô vừa chở người, vừa chở hàng, loại có dung tích xi lanh từ 2.500 cm3 đến 3.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'10%'  WHERE ReferenceDB='A522' AND Code='TB222'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB223')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB223' ,N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô vừa chở người, vừa chở hàng, loại có dung tích xi lanh trên 3.000 cm3',N'Thuế tiêu thụ đặc biệt',N'12,5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB223' ,Name_VN = N'Xe ô tô chạy bằng năng lượng sinh học, thuộc loại xe ô tô vừa chở người, vừa chở hàng, loại có dung tích xi lanh trên 3.000 cm3' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'12,5%'  WHERE ReferenceDB='A522' AND Code='TB223'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB230')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB230' ,N'Xe ô tô chạy bằng điện, loại chở người từ 9 chỗ trở xuống.',N'Thuế tiêu thụ đặc biệt',N'15%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB230' ,Name_VN = N'Xe ô tô chạy bằng điện, loại chở người từ 9 chỗ trở xuống.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'15%'  WHERE ReferenceDB='A522' AND Code='TB230'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB240')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB240' ,N'Xe ô tô chạy bằng điện, loại chở người từ 10 đến dưới 16 chỗ.',N'Thuế tiêu thụ đặc biệt',N'10%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB240' ,Name_VN = N'Xe ô tô chạy bằng điện, loại chở người từ 10 đến dưới 16 chỗ.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'10%'  WHERE ReferenceDB='A522' AND Code='TB240'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB250')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB250' ,N'Xe ô tô chạy bằng điện, loại chở người từ 16 đến dưới 24 chỗ.',N'Thuế tiêu thụ đặc biệt',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB250' ,Name_VN = N'Xe ô tô chạy bằng điện, loại chở người từ 16 đến dưới 24 chỗ.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'5%'  WHERE ReferenceDB='A522' AND Code='TB250'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB260')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB260' ,N'Xe ô tô chạy bằng điện, loại thiết kế vừa chở người, vừa chở hàng.',N'Thuế tiêu thụ đặc biệt',N'10%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB260' ,Name_VN = N'Xe ô tô chạy bằng điện, loại thiết kế vừa chở người, vừa chở hàng.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'10%'  WHERE ReferenceDB='A522' AND Code='TB260'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB261')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB261' ,N'Xe mô tô hôm (motorhome) không phân biệt dung tích xi lanh',N'Thuế tiêu thụ đặc biệt',N'70%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB261' ,Name_VN = N'Xe mô tô hôm (motorhome) không phân biệt dung tích xi lanh' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'70%'  WHERE ReferenceDB='A522' AND Code='TB261'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB270')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB270' ,N'Xe mô tô hai bánh, xe mô tô ba bánh có dung tích xi lanh trên 125cm3.',N'Thuế tiêu thụ đặc biệt',N'20%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB270' ,Name_VN = N'Xe mô tô hai bánh, xe mô tô ba bánh có dung tích xi lanh trên 125cm3.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'20%'  WHERE ReferenceDB='A522' AND Code='TB270'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB280')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB280' ,N'Tàu bay.',N'Thuế tiêu thụ đặc biệt',N'30%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB280' ,Name_VN = N'Tàu bay.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'30%'  WHERE ReferenceDB='A522' AND Code='TB280'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB290')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB290' ,N'Du thuyền.',N'Thuế tiêu thụ đặc biệt',N'30%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB290' ,Name_VN = N'Du thuyền.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'30%'  WHERE ReferenceDB='A522' AND Code='TB290'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB300')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB300' ,N'Xăng các loại, nap-ta, chế phẩm tái hợp và các chế phẩm khác để pha chế xăng.',N'Thuế tiêu thụ đặc biệt',N'10%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB300' ,Name_VN = N'Xăng các loại, nap-ta, chế phẩm tái hợp và các chế phẩm khác để pha chế xăng.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'10%'  WHERE ReferenceDB='A522' AND Code='TB300'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB310')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB310' ,N'Điều hoà nhiệt độ công suất từ 90.000 BTU trở xuống.',N'Thuế tiêu thụ đặc biệt',N'10%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB310' ,Name_VN = N'Điều hoà nhiệt độ công suất từ 90.000 BTU trở xuống.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'10%'  WHERE ReferenceDB='A522' AND Code='TB310'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB320')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB320' ,N'Bài lá.',N'Thuế tiêu thụ đặc biệt',N'40%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB320' ,Name_VN = N'Bài lá.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'40%'  WHERE ReferenceDB='A522' AND Code='TB320'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB330')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB330' ,N'Vàng mã, hàng mã.',N'Thuế tiêu thụ đặc biệt',N'70%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB330' ,Name_VN = N'Vàng mã, hàng mã.' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'70%'  WHERE ReferenceDB='A522' AND Code='TB330'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB301')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB301' ,N'Xăng E5',N'Thuế tiêu thụ đặc biệt',N'8%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB301' ,Name_VN = N'Xăng E5' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'8%'  WHERE ReferenceDB='A522' AND Code='TB301'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='TB302')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'TB302' ,N'Xăng E10',N'Thuế tiêu thụ đặc biệt',N'7%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='TB302' ,Name_VN = N'Xăng E10' ,Name_EN=N'Thuế tiêu thụ đặc biệt',Notes=N'7%'  WHERE ReferenceDB='A522' AND Code='TB302'
END

END
--------------------------------------BẢNG MÃ THUẾ SUẤT THUẾ GIÁ TRỊ GIA TĂNG DÙNG TRONG VNACCS  ---------------------------------------
BEGIN
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB015')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB015' ,N'Nước sạch phục vụ sản xuất và sinh hoạt',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB015' ,Name_VN = N'Nước sạch phục vụ sản xuất và sinh hoạt' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB015'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB025')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB025' ,N'Quặng để sản xuất phân bón',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB025' ,Name_VN = N'Quặng để sản xuất phân bón' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB025'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB035')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB035' ,N'Thuốc phòng trừ sâu bệnh  và chất kích thích tăng trưởng vật nuôi, cây trồng',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB035' ,Name_VN = N'Thuốc phòng trừ sâu bệnh  và chất kích thích tăng trưởng vật nuôi, cây trồng' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB035'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB055')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB055' ,N'Mủ cao su sơ chế',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB055' ,Name_VN = N'Mủ cao su sơ chế' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB055'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB065')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB065' ,N'Nhựa thông sơ chế',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB065' ,Name_VN = N'Nhựa thông sơ chế' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB065'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB075')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB075' ,N'Lưới, dây giềng và sợi để đan lưới đánh cá',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB075' ,Name_VN = N'Lưới, dây giềng và sợi để đan lưới đánh cá' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB075'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB085')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB085' ,N'Thực phẩm tươi sống; lâm sản chưa qua chế biến, trừ gỗ, măng và sản phẩm quy định tại khoản 1 Điều 5 Luật thuế GTGT số 13/2008/QH12',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB085' ,Name_VN = N'Thực phẩm tươi sống; lâm sản chưa qua chế biến, trừ gỗ, măng và sản phẩm quy định tại khoản 1 Điều 5 Luật thuế GTGT số 13/2008/QH12' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB085'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB095')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB095' ,N'Đường; phụ phẩm trong sản xuất đường, bao gồm gỉ đường, bã mía, bã bùn',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB095' ,Name_VN = N'Đường; phụ phẩm trong sản xuất đường, bao gồm gỉ đường, bã mía, bã bùn' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB095'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB105')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB105' ,N'Sản phẩm bằng đay, cói, tre, nứa, lá, rơm, vỏ dừa, sọ dừa, bèo tây và các sản phẩm thủ công khác sản xuất bằng nguyên liệu tận dụng từ nông nghiệp ',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB105' ,Name_VN = N'Sản phẩm bằng đay, cói, tre, nứa, lá, rơm, vỏ dừa, sọ dừa, bèo tây và các sản phẩm thủ công khác sản xuất bằng nguyên liệu tận dụng từ nông nghiệp ' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB105'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB115')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB115' ,N'Bông sơ chế',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB115' ,Name_VN = N'Bông sơ chế' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB115'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB125')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB125' ,N'Giấy in báo',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB125' ,Name_VN = N'Giấy in báo' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB125'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB145')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB145' ,N'Thiết bị, dụng cụ y tế; bông, băng vệ sinh y tế',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB145' ,Name_VN = N'Thiết bị, dụng cụ y tế; bông, băng vệ sinh y tế' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB145'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB155')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB155' ,N'Thuốc phòng bệnh, chữa bệnh; sản phẩm hóa dược, dược liệu là nguyên liệu sản xuất thuốc chữa bệnh, thuốc phòng bệnh',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB155' ,Name_VN = N'Thuốc phòng bệnh, chữa bệnh; sản phẩm hóa dược, dược liệu là nguyên liệu sản xuất thuốc chữa bệnh, thuốc phòng bệnh' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB155'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB165')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB165' ,N'Giáo cụ dùng để giảng dạy và học tập, bao gồm các loại mô hình, hình vẽ, bảng, phấn, thước kẻ, com-pa và các loại thiết bị, dụng cụ chuyên dùng cho giảng dạy, nghiên cứu, thí nghiệm khoa học',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB165' ,Name_VN = N'Giáo cụ dùng để giảng dạy và học tập, bao gồm các loại mô hình, hình vẽ, bảng, phấn, thước kẻ, com-pa và các loại thiết bị, dụng cụ chuyên dùng cho giảng dạy, nghiên cứu, thí nghiệm khoa học' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB165'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB175')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB175' ,N'Phim nhập khẩu',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB175' ,Name_VN = N'Phim nhập khẩu' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB175'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB185')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB185' ,N'Đồ chơi cho trẻ em; sách các loại, trừ sách quy định tại khoản 15 Điều 5 Luật thuế GTGT số 13/2008/QH12',N'Thuế giá trị gia tăng',N'5%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB185' ,Name_VN = N'Đồ chơi cho trẻ em; sách các loại, trừ sách quy định tại khoản 15 Điều 5 Luật thuế GTGT số 13/2008/QH12' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'5%' WHERE ReferenceDB='A522' AND Code='VB185'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='VB901')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'VB901' ,N'Hàng hóa thuộc đối tượng chịu thuế GTGT với mức thuế suất 10%',N'Thuế giá trị gia tăng',N'10%')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='VB901' ,Name_VN = N'Hàng hóa thuộc đối tượng chịu thuế GTGT với mức thuế suất 10%' ,Name_EN=N'Thuế giá trị gia tăng' ,Notes =N'10%' WHERE ReferenceDB='A522' AND Code='VB901'
END

END
--------------------------------------BẢNG MÃ THUẾ SUẤT THUẾ BẢO VỆ MÔI TRƯỜNG DÙNG TRONG VNACCS ---------------------------------------
BEGIN
   IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB010')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB010' ,N'Xăng, trừ etanol',N'Thuế bảo vệ môi trường',N'3.000/Lít')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB010' ,Name_VN = N'Xăng, trừ etanol' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'3.000/Lít'  WHERE ReferenceDB='A522' AND Code='MB010'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB202')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB202' ,N'Nhiên liệu bay',N'Thuế bảo vệ môi trường',N'3.000/Lít')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB202' ,Name_VN = N'Nhiên liệu bay' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'3.000/Lít'  WHERE ReferenceDB='A522' AND Code='MB202'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB030')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB030' ,N'Dầu diezel',N'Thuế bảo vệ môi trường',N'1.500/Lít')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB030' ,Name_VN = N'Dầu diezel' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'1.500/Lít'  WHERE ReferenceDB='A522' AND Code='MB030'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB040')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB040' ,N'Dầu hỏa',N'Thuế bảo vệ môi trường',N'300/Lít')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB040' ,Name_VN = N'Dầu hỏa' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'300/Lít'  WHERE ReferenceDB='A522' AND Code='MB040'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB050')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB050' ,N'Dầu mazut',N'Thuế bảo vệ môi trường',N'900/Lít')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB050' ,Name_VN = N'Dầu mazut' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'900/Lít'  WHERE ReferenceDB='A522' AND Code='MB050'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB060')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB060' ,N'Dầu nhờn',N'Thuế bảo vệ môi trường',N'900/Lít')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB060' ,Name_VN = N'Dầu nhờn' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'900/Lít'  WHERE ReferenceDB='A522' AND Code='MB060'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB070')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB070' ,N'Mỡ nhờn',N'Thuế bảo vệ môi trường',N'900/Kg')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB070' ,Name_VN = N'Mỡ nhờn' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'900/Kg'  WHERE ReferenceDB='A522' AND Code='MB070'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB110')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB110' ,N'Than nâu',N'Thuế bảo vệ môi trường',N'10.000/Tấn')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB110' ,Name_VN = N'Than nâu' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'10.000/Tấn'  WHERE ReferenceDB='A522' AND Code='MB110'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB120')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB120' ,N'Than an - tra - xít (antraxit)',N'Thuế bảo vệ môi trường',N'20.000/Tấn')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB120' ,Name_VN = N'Than an - tra - xít (antraxit)' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'20.000/Tấn'  WHERE ReferenceDB='A522' AND Code='MB120'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB130')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB130' ,N'Than mỡ',N'Thuế bảo vệ môi trường',N'10.000/Tấn')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB130' ,Name_VN = N'Than mỡ' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'10.000/Tấn'  WHERE ReferenceDB='A522' AND Code='MB130'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB140')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB140' ,N'Than đá khác',N'Thuế bảo vệ môi trường',N'10.000/Tấn')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB140' ,Name_VN = N'Than đá khác' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'10.000/Tấn'  WHERE ReferenceDB='A522' AND Code='MB140'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB200')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB200' ,N'III. Dung dịch Hydro-chloro-fluoro-carbon (HCFC)',N'Thuế bảo vệ môi trường',N'4.000/Kg')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB200' ,Name_VN = N'III. Dung dịch Hydro-chloro-fluoro-carbon (HCFC)' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'4.000/Kg'  WHERE ReferenceDB='A522' AND Code='MB200'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB300')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB300' ,N'IV. Túi ni lông thuộc diện chịu thuế BVMT',N'Thuế bảo vệ môi trường',N'40.000/Kg')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB300' ,Name_VN = N'IV. Túi ni lông thuộc diện chịu thuế BVMT' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'40.000/Kg'  WHERE ReferenceDB='A522' AND Code='MB300'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB400')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB400' ,N'V. Thuốc diệt cỏ thuộc loại hạn chế sử dụng',N'Thuế bảo vệ môi trường',N'500/Kg')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB400' ,Name_VN = N'V. Thuốc diệt cỏ thuộc loại hạn chế sử dụng' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'500/Kg'  WHERE ReferenceDB='A522' AND Code='MB400'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB500')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB500' ,N'VI. Thuốc trừ mối thuộc loại hạn chế sử dụng',N'Thuế bảo vệ môi trường',N'1.000/Kg')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB500' ,Name_VN = N'VI. Thuốc trừ mối thuộc loại hạn chế sử dụng' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'1.000/Kg'  WHERE ReferenceDB='A522' AND Code='MB500'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB600')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB600' ,N'VII. Thuốc bảo quản lâm sản thuộc loại hạn chế sử dụng',N'Thuế bảo vệ môi trường',N'1.000/Kg')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB600' ,Name_VN = N'VII. Thuốc bảo quản lâm sản thuộc loại hạn chế sử dụng' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'1.000/Kg'  WHERE ReferenceDB='A522' AND Code='MB600'
END
IF NOT EXISTS (SELECT * FROM dbo.t_VNACC_Category_Common WHERE ReferenceDB='A522' AND Code='MB700')
BEGIN
    INSERT INTO dbo.t_VNACC_Category_Common( ReferenceDB,Code,Name_VN,Name_EN,Notes) VALUES  ( 'A522' ,'MB700' ,N'VIII. Thuốc khử trùng kho thuộc loại hạn chế sử dụng',N'Thuế bảo vệ môi trường',N'1.000/Kg')
END
ELSE
BEGIN
    UPDATE dbo.t_VNACC_Category_Common SET Code='MB700' ,Name_VN = N'VIII. Thuốc khử trùng kho thuộc loại hạn chế sử dụng' ,Name_EN=N'Thuế bảo vệ môi trường' ,Notes =N'1.000/Kg'  WHERE ReferenceDB='A522' AND Code='MB700'
END

END

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '20.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('20.8',GETDATE(), N'BẢNG MÃ ÁP DỤNG THUẾ SUẤT MỨC THUẾ VÀ THU KHÁC DÙNG TRONG VNACCS')
END