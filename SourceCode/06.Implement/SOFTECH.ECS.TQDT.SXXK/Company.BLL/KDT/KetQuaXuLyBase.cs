﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KD.BLL.KDT
{
    public partial class KetQuaXuLy
    {
        #region Properties.

        public int ID { set; get; }
        public Guid ReferenceID { set; get; }
        public long ItemID { set; get; }
        public string LoaiChungTu { set; get; }
        public string LoaiThongDiep { set; get; }
        public string NoiDung { set; get; }
        public DateTime Ngay { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static KetQuaXuLy Load(int id)
        {
            const string spName = "[dbo].[p_KDT_KetQuaXuLy_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
            KetQuaXuLy entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new KetQuaXuLy();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ReferenceID"))) entity.ReferenceID = reader.GetGuid(reader.GetOrdinal("ReferenceID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ItemID"))) entity.ItemID = reader.GetInt64(reader.GetOrdinal("ItemID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiChungTu"))) entity.LoaiChungTu = reader.GetString(reader.GetOrdinal("LoaiChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiThongDiep"))) entity.LoaiThongDiep = reader.GetString(reader.GetOrdinal("LoaiThongDiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay"))) entity.Ngay = reader.GetDateTime(reader.GetOrdinal("Ngay"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------
        public static List<KetQuaXuLy> SelectCollectionAll()
        {
            List<KetQuaXuLy> collection = new List<KetQuaXuLy>();
            SqlDataReader reader = (SqlDataReader)SelectReaderAll();
            while (reader.Read())
            {
                KetQuaXuLy entity = new KetQuaXuLy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ReferenceID"))) entity.ReferenceID = reader.GetGuid(reader.GetOrdinal("ReferenceID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ItemID"))) entity.ItemID = reader.GetInt64(reader.GetOrdinal("ItemID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiChungTu"))) entity.LoaiChungTu = reader.GetString(reader.GetOrdinal("LoaiChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiThongDiep"))) entity.LoaiThongDiep = reader.GetString(reader.GetOrdinal("LoaiThongDiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay"))) entity.Ngay = reader.GetDateTime(reader.GetOrdinal("Ngay"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public static List<KetQuaXuLy> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            List<KetQuaXuLy> collection = new List<KetQuaXuLy>();

            SqlDataReader reader = (SqlDataReader)SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                KetQuaXuLy entity = new KetQuaXuLy();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ReferenceID"))) entity.ReferenceID = reader.GetGuid(reader.GetOrdinal("ReferenceID"));
                if (!reader.IsDBNull(reader.GetOrdinal("ItemID"))) entity.ItemID = reader.GetInt64(reader.GetOrdinal("ItemID"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiChungTu"))) entity.LoaiChungTu = reader.GetString(reader.GetOrdinal("LoaiChungTu"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiThongDiep"))) entity.LoaiThongDiep = reader.GetString(reader.GetOrdinal("LoaiThongDiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("Ngay"))) entity.Ngay = reader.GetDateTime(reader.GetOrdinal("Ngay"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		


        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_KetQuaXuLy_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_KetQuaXuLy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_KetQuaXuLy_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_KetQuaXuLy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static int InsertKetQuaXuLy(Guid referenceID, long itemID, string loaiChungTu, string loaiThongDiep, string noiDung, DateTime ngay)
        {
            KetQuaXuLy entity = new KetQuaXuLy();
            entity.ReferenceID = referenceID;
            entity.ItemID = itemID;
            entity.LoaiChungTu = loaiChungTu;
            entity.LoaiThongDiep = loaiThongDiep;
            entity.NoiDung = noiDung;
            entity.Ngay = ngay;
            return entity.Insert();
        }

        public int Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_KetQuaXuLy_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
            db.AddInParameter(dbCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, ReferenceID);
            db.AddInParameter(dbCommand, "@ItemID", SqlDbType.BigInt, ItemID);
            db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
            db.AddInParameter(dbCommand, "@LoaiThongDiep", SqlDbType.NVarChar, LoaiThongDiep);
            db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
            db.AddInParameter(dbCommand, "@Ngay", SqlDbType.DateTime, Ngay.Year <= 1753 ? DBNull.Value : (object)Ngay);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (int)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<KetQuaXuLy> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KetQuaXuLy item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateKetQuaXuLy(int id, Guid referenceID, long itemID, string loaiChungTu, string loaiThongDiep, string noiDung, DateTime ngay)
        {
            KetQuaXuLy entity = new KetQuaXuLy();
            entity.ID = id;
            entity.ReferenceID = referenceID;
            entity.ItemID = itemID;
            entity.LoaiChungTu = loaiChungTu;
            entity.LoaiThongDiep = loaiThongDiep;
            entity.NoiDung = noiDung;
            entity.Ngay = ngay;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_KetQuaXuLy_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, ReferenceID);
            db.AddInParameter(dbCommand, "@ItemID", SqlDbType.BigInt, ItemID);
            db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
            db.AddInParameter(dbCommand, "@LoaiThongDiep", SqlDbType.NVarChar, LoaiThongDiep);
            db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
            db.AddInParameter(dbCommand, "@Ngay", SqlDbType.DateTime, Ngay.Year == 1753 ? DBNull.Value : (object)Ngay);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<KetQuaXuLy> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KetQuaXuLy item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateKetQuaXuLy(int id, Guid referenceID, long itemID, string loaiChungTu, string loaiThongDiep, string noiDung, DateTime ngay)
        {
            KetQuaXuLy entity = new KetQuaXuLy();
            entity.ID = id;
            entity.ReferenceID = referenceID;
            entity.ItemID = itemID;
            entity.LoaiChungTu = loaiChungTu;
            entity.LoaiThongDiep = loaiThongDiep;
            entity.NoiDung = noiDung;
            entity.Ngay = ngay;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_KetQuaXuLy_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
            db.AddInParameter(dbCommand, "@ReferenceID", SqlDbType.UniqueIdentifier, ReferenceID);
            db.AddInParameter(dbCommand, "@ItemID", SqlDbType.BigInt, ItemID);
            db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
            db.AddInParameter(dbCommand, "@LoaiThongDiep", SqlDbType.NVarChar, LoaiThongDiep);
            db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
            db.AddInParameter(dbCommand, "@Ngay", SqlDbType.DateTime, Ngay.Year == 1753 ? DBNull.Value : (object)Ngay);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<KetQuaXuLy> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KetQuaXuLy item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteKetQuaXuLy(int id)
        {
            KetQuaXuLy entity = new KetQuaXuLy();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_KetQuaXuLy_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_KDT_KetQuaXuLy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<KetQuaXuLy> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (KetQuaXuLy item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}