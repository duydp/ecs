﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Data;
using SQLDMO;
using System.Reflection;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace Company.Interface
{
    public partial class BackUpAndReStoreForm : BaseForm
    {
        public bool isBackUp = true;
        public static DateTime lastBackUp = new DateTime(1900, 01, 01);
        SQLDMO.BackupClass backupClass;
        //SQLDMO.RestoreClass restoreClass = new SQLDMO.RestoreClass();

        public BackUpAndReStoreForm()
        {
            InitializeComponent();
        }

        private void BackUpAndReStoreForm_Load(object sender, EventArgs e)
        {
            try
            {
                //Tao mac dinh thu muc sao luu du lieu
                string folderDataBackup = "";
                string folderDataRemote = "";

                try
                {
                    /******************************************************************
                    //1. Tạo thư mục lưu data (Nếu chưa có): [Ổ đĩa]/SOFTECH/ECS/DATABASE 
                    //2. Kiểm tra tên file data, tên file log vật lý tại thư mục lưu dữ liệu có trùng tên trước khi copy.
                    //   Nếu đã có thì cảnh báo và không thực hiện copy file.
                    //3. Copy file du lieu từ [Thư mục chương trình ECS]\DATA den vi tri thư mục mới: [Ổ đĩa]/SOFTECH/ECS/DATABASE
                    //4. Lấy thông tin đường dẫn mới của file data.
                    *******************************************************************/

                    string driverName = "";
                    string[] drivers = System.IO.Directory.GetLogicalDrives();

                    for (int i = 0; i < drivers.Length; i++)
                    {
                        if (drivers[i] == "C:\\" || drivers[i] == "A:\\" || drivers[i] == "B:\\")
                            continue;
                        if (drivers[i] == "D:\\")
                        {
                            driverName = drivers[i];
                            break;
                        }
                        else
                        {
                            driverName = drivers[i];
                            break;
                        }
                    }

                    folderDataRemote = Company.KDT.SHARE.Components.SQL.GetFolderDatabaseRemote(GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS);

                    if (folderDataRemote.Trim().Length == 0)
                    {
                        folderDataBackup = (driverName + "SOFTECH\\ECS\\TQDT\\DATABASE\\BACKUP");
                        if (!System.IO.Directory.Exists(folderDataBackup))
                        {
                            //System.IO.Directory.CreateDirectory(folderDataBackup);
                            //Dung dong code duoi de phan quyen tren folder
                            Company.KDT.SHARE.Components.CommonApplicationData direc = new Company.KDT.SHARE.Components.CommonApplicationData(driverName + "SOFTECH\\ECS\\TQDT\\DATABASE", "BACKUP", true);
                        }
                    }
                    else
                        folderDataBackup = folderDataRemote;
                }
                catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

                if (!GlobalSettings.PathBackup.Equals(""))
                    txtPath.Text = GlobalSettings.PathBackup.Trim();
                else
                    txtPath.Text = folderDataBackup;

                editBox1.Text = string.Format("{0}_{1}", GlobalSettings.DATABASE_NAME, DateTime.Now.ToString("dd_MM_yyyy_[HH_mm].bak"));
                if (GlobalSettings.NGAYSAOLUU == "")
                    label3.Enabled = label4.Enabled = uiComboBox1.Enabled = false;

                string nhacNhoSaoLuu = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHAC_NHO_SAO_LUU");

                if (GlobalSettings.NGON_NGU == "0")
                {
                    if (isBackUp)
                    {
                        if (nhacNhoSaoLuu != "")
                            uiComboBox1.SelectedValue = nhacNhoSaoLuu;
                        else
                            uiComboBox1.SelectedValue = "7";

                        //uiButton1.Text = "Sao lưu";
                    }
                    //else
                    //{
                    //    label2.Visible = label3.Visible = label4.Visible = uiComboBox1.Visible = false;
                    //    uiButton1.Text = "Phục hồi";
                    //}
                }
                else
                {

                    if (isBackUp)
                    {
                        if (nhacNhoSaoLuu != "")
                            uiComboBox1.SelectedValue = nhacNhoSaoLuu;
                        else
                            uiComboBox1.SelectedValue = "7";

                        //uiButton1.Text = "Backup";
                    }
                    //else
                    //{
                    //    label2.Visible = label3.Visible = label4.Visible = uiComboBox1.Visible = false;
                    //    uiButton1.Text = "Restore";
                    //}

                }

                uiCheckBox1.Checked = nhacNhoSaoLuu != null || nhacNhoSaoLuu != "" ? true : false;

                uiCheckBox1_CheckedChanged(null, EventArgs.Empty);

                backupClass = new SQLDMO.BackupClass();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (string.IsNullOrEmpty(txtPath.Text) || string.IsNullOrEmpty(editBox1.Text))
                    ShowMessage("Đường dẫn hoặc tên file không được để trống", false);
                else
                {
                    if (isBackUp)
                    {
                        using (SqlConnection cnn = new SqlConnection())
                        {
                            cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3};Connect Timeout={4}", new object[] { GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS, Company.KDT.SHARE.Components.Globals.TimeoutBackup * 60 }); //TimeoutBackup = phút * 60 giây.
//                            cnn.Open();
//                            string cmdText = @"			                                
//                                            BEGIN
//
//                                            EXEC SP_CONFIGURE 'remote query timeout', 1800
//                                            reconfigure
//                                            EXEC sp_configure
//
//
//                                            EXEC SP_CONFIGURE 'show advanced options', 1
//                                            reconfigure
//                                            EXEC sp_configure
//
//
//                                            EXEC SP_CONFIGURE 'remote query timeout', 1800
//                                            reconfigure
//                                            EXEC sp_configure
//
//                                            END
//                                         ";
//                            SqlCommand sqlCmd = new SqlCommand(cmdText);
//                            sqlCmd.CommandType = CommandType.Text;
//                            sqlCmd.Connection = cnn;
//                            sqlCmd.ExecuteNonQuery();
//                            cnn.Close();


                            cnn.Open();
                            string cmdText = @"
			                                --Default backup database LanNT                                            
                                            --Create 10-05-2012
                                            BEGIN

                                            DECLARE @filedata  AS NVARCHAR(255)
                                            DECLARE @filename  AS NVARCHAR(255)
                                            DECLARE @dbName AS NVARCHAR(255)
                                           
                                            SET @dbName = '" + GlobalSettings.DATABASE_NAME + @"'
                                            SET @filename ='" + Path.Combine(txtPath.Text, editBox1.Text) + @"'		                                
                                            BACKUP DATABASE @dbName
                                            TO DISK =@filename
                                            WITH FORMAT,INIT,MEDIANAME = 'Z_SQLServerBackups',
                                            NAME = 'Full Backup of AdventureWorks',
                                            SKIP,NOREWIND,NOUNLOAD,STATS = 10;
                                            END
                                         ";
                            SqlCommand sqlCmd = new SqlCommand(cmdText);
                            sqlCmd.CommandType = CommandType.Text;
                            sqlCmd.CommandTimeout = Company.KDT.SHARE.Components.Globals.TimeoutBackup * 60;
                            sqlCmd.Connection = cnn;
                            sqlCmd.ExecuteNonQuery();
                        }
                    }

                    if (uiCheckBox1.Checked)
                    {
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", DateTime.Today.ToString("dd/MM/yyyy"));
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", uiComboBox1.SelectedValue.ToString());
                    }
                    else
                    {
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", "");
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", "");
                    }

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("LastBackup", DateTime.Now.ToString("dd/MM/yyyy"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PathBackup", txtPath.Text.Trim());

                    ShowMessage("Sao lưu dữ liệu thành công.", false);
                    GlobalSettings.RefreshKey();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                string st = "";
                if (isBackUp)
                    st = "Lỗi khi sao lưu dữ liệu.";
                else
                    st = "Lỗi khi phục hồi dữ liệu.";
                ShowMessage(st + " " + ex.Message, false);

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void editBox1_ButtonClick(object sender, EventArgs e)
        {

        }

        private void uiCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (uiCheckBox1.Checked == false)
            {
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = false;
            }
            else
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = true;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            FrmFolderBrowse frm = new FrmFolderBrowse(GlobalSettings.USER, GlobalSettings.PASS, GlobalSettings.SERVER_NAME);
            frm.ShowDialog(this);
            txtPath.Text = FrmFolderBrowse.pathSelected;
        }

        private void uiCheckBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (uiCheckBox1.Checked == false)
            {
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = false;
            }
            else
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = true;
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (uiCheckBox1.Checked)
                {
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", DateTime.Today.ToString("dd/MM/yyyy"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", uiComboBox1.SelectedValue.ToString());
                }
                else
                {
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", "");
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", "");
                }

                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PathBackup", txtPath.Text.Trim());

                ShowMessage("Lưu cấu hình thành công.", false);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

    }
}