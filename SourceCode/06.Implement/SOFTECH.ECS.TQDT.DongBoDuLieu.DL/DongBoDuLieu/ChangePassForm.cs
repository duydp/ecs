﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;
using System.Security.Cryptography;

namespace DongBoDuLieu
{
    public partial class ChangePassForm : BaseForm
    {
        public ChangePassForm()
        {
            InitializeComponent();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            User user = ((SiteIdentity)MainForm.EcsQuanTri.Identity).user;
            if (user.PASSWORD != EncryptPassword(txtMatKhauCu.Text))
            {
              //  ShowMessage("Mật khẩu cũ không đúng.", false);
                MLMessages("Mật khẩu cũ không đúng.", "MSG_PUB35", "", false);
            }
            else
            {
                user.PASSWORD = EncryptPassword(txtMatKhauMoi.Text);
                try
                {
                    user.Update();
                   // ShowMessage("Đổi mật khẩu thành công.", false);
                    MLMessages("Đổi mật khẩu thành công.", "MSG_PUB36", "", false);
                }
                catch (Exception ex)
                {
                    //ShowMessage("Đổi mật khẩu không thành công. Lỗi: " + ex.Message, false);
                    MLMessages("Đổi mật khẩu không thành công. Lỗi: " + ex.Message, "MSG_PUB37", "", false);
                }
                finally
                {
                    this.Close();
                }
            }
            
        }
        public string EncryptPassword(string password)
        {
            UnicodeEncoding encoding = new UnicodeEncoding();
            byte[] hashBytes = encoding.GetBytes(password);

            // compute SHA-1 hash.
            SHA1 sha1 = new SHA1CryptoServiceProvider();
            byte[] bytePassword = sha1.ComputeHash(hashBytes);
            string cryptPassword = Convert.ToBase64String(bytePassword);
            return cryptPassword;
        }
    }
}

