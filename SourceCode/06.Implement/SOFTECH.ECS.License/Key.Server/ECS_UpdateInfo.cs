﻿using System;
using System.Collections.Generic;
using System.Text;
using KeySecurity;

namespace KeyServer
{
    public class ECS_UpdateInfo
    {
        List<Key> listkey = new List<Key>();
        private string Check_machineCode(string machineCode, string productID)
        {
            try
            {
                string query = string.Format("MachineCode = '{0}' and ProductId = '{1}'", machineCode, productID);
                listkey = Key.SelectCollectionDynamic(query, null);
                if (listkey == null || listkey.Count == 0)
                {
                    return "0";
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }

        }
        public string Ecs_Update_Info(string messages)
        {
            string error = string.Empty;
            ECS_InforCustomer info = new ECS_InforCustomer();
            try
            {
                info = KeyCode.Deserialize<ECS_InforCustomer>(KeyCode.ConvertFromBase64(messages));
                string check = Check_machineCode(info.MachineCode, info.Product_ID);
                if (string.IsNullOrEmpty(check))
                {
                    foreach (Key key_update in listkey)
                    {
                        LM_Keys_Activity key = new LM_Keys_Activity();
                        key.ActivedKey = key_update.ActivedKey;
                        key.RecordCount = info.RecordCount;
                        key.CustomerCode = info.Id;
                        key.CustomerName = info.Name;
                        key.CustomerAddress = info.Address;
                        key.CustomerPhone = info.Phone;
                        key.CustomerEmail = info.Email;
                        //key_update.CustomerDescription = info.Contact_Person;
                        key.CustomerField1 = info.Id_Customs;
                        key.CustomerField2 = info.IP_Customs;
                        key.CustomerField3 = info.ServerName;
                        key.CustomerField4 = info.Data_Name;
                        key.CustomerField5 = info.Data_User;
                        key.CustomerField6 = info.Data_Pass;
                        key.CustomerField7 = info.Data_Version;
                        key.CustomerField8 = info.App_Version;
                        key.LastCheckStatus = info.LastCheck;
                        key.CustomerField9 = info.temp1;
                        key.CreatedDate = DateTime.Now;
                        // key_update.CustomerField10 = info.temp2; // Đã danh cho trạng thái thông báo
                        key.InsertUpdate();
                    }
                }
                else
                {
                    error = check;
                }
            }
            catch (System.Exception ex)
            {
                error = ex.Message;
            }
            return error;
        }


    }
}
