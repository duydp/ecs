-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_ThongTinDinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Insert]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6),
	@SoDinhMuc int,
	@NgayDangKy datetime,
	@NgayApDung datetime,
	@NgayHetHan datetime,
	@ThanhLy int
AS
INSERT INTO [dbo].[t_SXXK_ThongTinDinhMuc]
(
	[MaSanPham],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[NgayHetHan],
	[ThanhLy]
)
VALUES
(
	@MaSanPham,
	@MaDoanhNghiep,
	@MaHaiQuan,
	@SoDinhMuc,
	@NgayDangKy,
	@NgayApDung,
	@NgayHetHan,
	@ThanhLy
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Update]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6),
	@SoDinhMuc int,
	@NgayDangKy datetime,
	@NgayApDung datetime,
	@NgayHetHan datetime,
	@ThanhLy int
AS

UPDATE
	[dbo].[t_SXXK_ThongTinDinhMuc]
SET
	[SoDinhMuc] = @SoDinhMuc,
	[NgayDangKy] = @NgayDangKy,
	[NgayApDung] = @NgayApDung,
	[NgayHetHan] = @NgayHetHan,
	[ThanhLy] = @ThanhLy
WHERE
	[MaSanPham] = @MaSanPham
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaHaiQuan] = @MaHaiQuan

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_InsertUpdate]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6),
	@SoDinhMuc int,
	@NgayDangKy datetime,
	@NgayApDung datetime,
	@NgayHetHan datetime,
	@ThanhLy int
AS
IF EXISTS(SELECT [MaSanPham], [MaDoanhNghiep], [MaHaiQuan] FROM [dbo].[t_SXXK_ThongTinDinhMuc] WHERE [MaSanPham] = @MaSanPham AND [MaDoanhNghiep] = @MaDoanhNghiep AND [MaHaiQuan] = @MaHaiQuan)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_ThongTinDinhMuc] 
		SET
			[SoDinhMuc] = @SoDinhMuc,
			[NgayDangKy] = @NgayDangKy,
			[NgayApDung] = @NgayApDung,
			[NgayHetHan] = @NgayHetHan,
			[ThanhLy] = @ThanhLy
		WHERE
			[MaSanPham] = @MaSanPham
			AND [MaDoanhNghiep] = @MaDoanhNghiep
			AND [MaHaiQuan] = @MaHaiQuan
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_ThongTinDinhMuc]
	(
			[MaSanPham],
			[MaDoanhNghiep],
			[MaHaiQuan],
			[SoDinhMuc],
			[NgayDangKy],
			[NgayApDung],
			[NgayHetHan],
			[ThanhLy]
	)
	VALUES
	(
			@MaSanPham,
			@MaDoanhNghiep,
			@MaHaiQuan,
			@SoDinhMuc,
			@NgayDangKy,
			@NgayApDung,
			@NgayHetHan,
			@ThanhLy
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Delete]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6)
AS

DELETE FROM 
	[dbo].[t_SXXK_ThongTinDinhMuc]
WHERE
	[MaSanPham] = @MaSanPham
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaHaiQuan] = @MaHaiQuan

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_ThongTinDinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_Load]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaSanPham],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[NgayHetHan],
	[ThanhLy]
FROM
	[dbo].[t_SXXK_ThongTinDinhMuc]
WHERE
	[MaSanPham] = @MaSanPham
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaHaiQuan] = @MaHaiQuan
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaSanPham],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[NgayHetHan],
	[ThanhLy]
FROM [dbo].[t_SXXK_ThongTinDinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_ThongTinDinhMuc_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_ThongTinDinhMuc_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaSanPham],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoDinhMuc],
	[NgayDangKy],
	[NgayApDung],
	[NgayHetHan],
	[ThanhLy]
FROM
	[dbo].[t_SXXK_ThongTinDinhMuc]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_SXXK_SanPham_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_SanPham_Insert]

IF OBJECT_ID(N'[dbo].[p_SXXK_SanPham_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_SanPham_Update]

IF OBJECT_ID(N'[dbo].[p_SXXK_SanPham_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_SanPham_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_SXXK_SanPham_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_SanPham_Delete]

IF OBJECT_ID(N'[dbo].[p_SXXK_SanPham_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_SanPham_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_SanPham_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_SanPham_Load]

IF OBJECT_ID(N'[dbo].[p_SXXK_SanPham_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_SanPham_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_SanPham_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_SanPham_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_Insert]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(10),
	@DVT_ID varchar(3)
AS
INSERT INTO [dbo].[t_SXXK_SanPham]
(
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
)
VALUES
(
	@MaHaiQuan,
	@MaDoanhNghiep,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_Update]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(10),
	@DVT_ID varchar(3)
AS

UPDATE
	[dbo].[t_SXXK_SanPham]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma
	AND [MaHaiQuan] = @MaHaiQuan

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_InsertUpdate]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(10),
	@DVT_ID varchar(3)
AS
IF EXISTS(SELECT [MaDoanhNghiep], [Ma], [MaHaiQuan] FROM [dbo].[t_SXXK_SanPham] WHERE [MaDoanhNghiep] = @MaDoanhNghiep AND [Ma] = @Ma AND [MaHaiQuan] = @MaHaiQuan)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_SanPham] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID
		WHERE
			[MaDoanhNghiep] = @MaDoanhNghiep
			AND [Ma] = @Ma
			AND [MaHaiQuan] = @MaHaiQuan
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_SanPham]
	(
			[MaHaiQuan],
			[MaDoanhNghiep],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID]
	)
	VALUES
	(
			@MaHaiQuan,
			@MaDoanhNghiep,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_Delete]
	@MaDoanhNghiep varchar(14),
	@Ma nvarchar(50),
	@MaHaiQuan char(6)
AS

DELETE FROM 
	[dbo].[t_SXXK_SanPham]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma
	AND [MaHaiQuan] = @MaHaiQuan

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_SanPham] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_Load]
	@MaDoanhNghiep varchar(14),
	@Ma nvarchar(50),
	@MaHaiQuan char(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
FROM
	[dbo].[t_SXXK_SanPham]
WHERE
	[MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma
	AND [MaHaiQuan] = @MaHaiQuan
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
FROM [dbo].[t_SXXK_SanPham] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_SanPham_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_SanPham_SelectAll]






AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
FROM
	[dbo].[t_SXXK_SanPham]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieu_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Insert]

IF OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieu_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Update]

IF OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieu_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieu_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Delete]

IF OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieu_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieu_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Load]

IF OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieu_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_NguyenPhuLieu_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Insert]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(10),
	@DVT_ID char(3)
AS
INSERT INTO [dbo].[t_SXXK_NguyenPhuLieu]
(
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
)
VALUES
(
	@MaHaiQuan,
	@MaDoanhNghiep,
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Update]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(10),
	@DVT_ID char(3)
AS

UPDATE
	[dbo].[t_SXXK_NguyenPhuLieu]
SET
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_InsertUpdate]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(10),
	@DVT_ID char(3)
AS
IF EXISTS(SELECT [MaHaiQuan], [MaDoanhNghiep], [Ma] FROM [dbo].[t_SXXK_NguyenPhuLieu] WHERE [MaHaiQuan] = @MaHaiQuan AND [MaDoanhNghiep] = @MaDoanhNghiep AND [Ma] = @Ma)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_NguyenPhuLieu] 
		SET
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID
		WHERE
			[MaHaiQuan] = @MaHaiQuan
			AND [MaDoanhNghiep] = @MaDoanhNghiep
			AND [Ma] = @Ma
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_NguyenPhuLieu]
	(
			[MaHaiQuan],
			[MaDoanhNghiep],
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID]
	)
	VALUES
	(
			@MaHaiQuan,
			@MaDoanhNghiep,
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Delete]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_SXXK_NguyenPhuLieu]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_NguyenPhuLieu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_Load]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@Ma nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
FROM
	[dbo].[t_SXXK_NguyenPhuLieu]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [Ma] = @Ma
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
FROM [dbo].[t_SXXK_NguyenPhuLieu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_NguyenPhuLieu_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_NguyenPhuLieu_SelectAll]






AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID]
FROM
	[dbo].[t_SXXK_NguyenPhuLieu]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectAll]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]

IF OBJECT_ID(N'[dbo].[p_SXXK_DinhMuc_DeleteBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_SXXK_DinhMuc_DeleteBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Insert]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@GhiChu varchar(250),
	@IsFromVietNam bit
AS
INSERT INTO [dbo].[t_SXXK_DinhMuc]
(
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam]
)
VALUES
(
	@MaHaiQuan,
	@MaDoanhNghiep,
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@DinhMucChung,
	@GhiChu,
	@IsFromVietNam
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Update]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@GhiChu varchar(250),
	@IsFromVietNam bit
AS

UPDATE
	[dbo].[t_SXXK_DinhMuc]
SET
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[DinhMucChung] = @DinhMucChung,
	[GhiChu] = @GhiChu,
	[IsFromVietNam] = @IsFromVietNam
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_InsertUpdate]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DinhMucSuDung numeric(18, 8),
	@TyLeHaoHut numeric(18, 5),
	@DinhMucChung numeric(18, 8),
	@GhiChu varchar(250),
	@IsFromVietNam bit
AS
IF EXISTS(SELECT [MaHaiQuan], [MaDoanhNghiep], [MaSanPham], [MaNguyenPhuLieu] FROM [dbo].[t_SXXK_DinhMuc] WHERE [MaHaiQuan] = @MaHaiQuan AND [MaDoanhNghiep] = @MaDoanhNghiep AND [MaSanPham] = @MaSanPham AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu)
	BEGIN
		UPDATE
			[dbo].[t_SXXK_DinhMuc] 
		SET
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[DinhMucChung] = @DinhMucChung,
			[GhiChu] = @GhiChu,
			[IsFromVietNam] = @IsFromVietNam
		WHERE
			[MaHaiQuan] = @MaHaiQuan
			AND [MaDoanhNghiep] = @MaDoanhNghiep
			AND [MaSanPham] = @MaSanPham
			AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_SXXK_DinhMuc]
	(
			[MaHaiQuan],
			[MaDoanhNghiep],
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[DinhMucChung],
			[GhiChu],
			[IsFromVietNam]
	)
	VALUES
	(
			@MaHaiQuan,
			@MaDoanhNghiep,
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@DinhMucChung,
			@GhiChu,
			@IsFromVietNam
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Delete]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50)
AS

DELETE FROM 
	[dbo].[t_SXXK_DinhMuc]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_DeleteBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_DeleteBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6)
AS

DELETE FROM [dbo].[t_SXXK_DinhMuc]
WHERE
	[MaSanPham] = @MaSanPham
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaHaiQuan] = @MaHaiQuan

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_SXXK_DinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_Load]
	@MaHaiQuan char(6),
	@MaDoanhNghiep varchar(14),
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam]
FROM
	[dbo].[t_SXXK_DinhMuc]
WHERE
	[MaHaiQuan] = @MaHaiQuan
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaSanPham] = @MaSanPham
	AND [MaNguyenPhuLieu] = @MaNguyenPhuLieu
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectBy_MaSanPham_And_MaDoanhNghiep_And_MaHaiQuan]
	@MaSanPham nvarchar(50),
	@MaDoanhNghiep varchar(14),
	@MaHaiQuan char(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam]
FROM
	[dbo].[t_SXXK_DinhMuc]
WHERE
	[MaSanPham] = @MaSanPham
	AND [MaDoanhNghiep] = @MaDoanhNghiep
	AND [MaHaiQuan] = @MaHaiQuan

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam]
FROM [dbo].[t_SXXK_DinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_DinhMuc_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_DinhMuc_SelectAll]









AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaHaiQuan],
	[MaDoanhNghiep],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[DinhMucChung],
	[GhiChu],
	[IsFromVietNam]
FROM
	[dbo].[t_SXXK_DinhMuc]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_SelectBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_SelectBy_Master_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_SanPham_DeleteBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_SanPham_DeleteBy_Master_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Insert]
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@STTHang int,
	@Master_ID bigint,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_SanPham]
(
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID]
)
VALUES 
(
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@STTHang,
	@Master_ID
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Update]
	@ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@STTHang int,
	@Master_ID bigint
AS

UPDATE
	[dbo].[t_KDT_SXXK_SanPham]
SET
	[Ma] = @Ma,
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[STTHang] = @STTHang,
	[Master_ID] = @Master_ID
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_InsertUpdate]
	@ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@STTHang int,
	@Master_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_SanPham] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_SanPham] 
		SET
			[Ma] = @Ma,
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[STTHang] = @STTHang,
			[Master_ID] = @Master_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_SanPham]
		(
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[STTHang],
			[Master_ID]
		)
		VALUES 
		(
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@STTHang,
			@Master_ID
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_SanPham]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_DeleteBy_Master_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_DeleteBy_Master_ID]
	@Master_ID bigint
AS

DELETE FROM [dbo].[t_KDT_SXXK_SanPham]
WHERE
	[Master_ID] = @Master_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_SanPham] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID]
FROM
	[dbo].[t_KDT_SXXK_SanPham]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_SelectBy_Master_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_SelectBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID]
FROM
	[dbo].[t_KDT_SXXK_SanPham]
WHERE
	[Master_ID] = @Master_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID]
FROM [dbo].[t_KDT_SXXK_SanPham] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_SanPham_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_SanPham_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID]
FROM
	[dbo].[t_KDT_SXXK_SanPham]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectBy_Master_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_NguyenPhuLieu_DeleteBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_DeleteBy_Master_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Insert]
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@STTHang int,
	@Master_ID bigint,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_NguyenPhuLieu]
(
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID]
)
VALUES 
(
	@Ma,
	@Ten,
	@MaHS,
	@DVT_ID,
	@STTHang,
	@Master_ID
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Update]
	@ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@STTHang int,
	@Master_ID bigint
AS

UPDATE
	[dbo].[t_KDT_SXXK_NguyenPhuLieu]
SET
	[Ma] = @Ma,
	[Ten] = @Ten,
	[MaHS] = @MaHS,
	[DVT_ID] = @DVT_ID,
	[STTHang] = @STTHang,
	[Master_ID] = @Master_ID
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_InsertUpdate]
	@ID bigint,
	@Ma nvarchar(50),
	@Ten nvarchar(254),
	@MaHS varchar(12),
	@DVT_ID char(3),
	@STTHang int,
	@Master_ID bigint
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_NguyenPhuLieu] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_NguyenPhuLieu] 
		SET
			[Ma] = @Ma,
			[Ten] = @Ten,
			[MaHS] = @MaHS,
			[DVT_ID] = @DVT_ID,
			[STTHang] = @STTHang,
			[Master_ID] = @Master_ID
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_NguyenPhuLieu]
		(
			[Ma],
			[Ten],
			[MaHS],
			[DVT_ID],
			[STTHang],
			[Master_ID]
		)
		VALUES 
		(
			@Ma,
			@Ten,
			@MaHS,
			@DVT_ID,
			@STTHang,
			@Master_ID
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_NguyenPhuLieu]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_DeleteBy_Master_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_DeleteBy_Master_ID]
	@Master_ID bigint
AS

DELETE FROM [dbo].[t_KDT_SXXK_NguyenPhuLieu]
WHERE
	[Master_ID] = @Master_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_NguyenPhuLieu] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID]
FROM
	[dbo].[t_KDT_SXXK_NguyenPhuLieu]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectBy_Master_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID]
FROM
	[dbo].[t_KDT_SXXK_NguyenPhuLieu]
WHERE
	[Master_ID] = @Master_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID]
FROM [dbo].[t_KDT_SXXK_NguyenPhuLieu] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_NguyenPhuLieu_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ma],
	[Ten],
	[MaHS],
	[DVT_ID],
	[STTHang],
	[Master_ID]
FROM
	[dbo].[t_KDT_SXXK_NguyenPhuLieu]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_SelectBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectBy_Master_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_DinhMuc_DeleteBy_Master_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_DeleteBy_Master_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Insert]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Insert]
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 10),
	@TyLeHaoHut numeric(18, 5),
	@GhiChu varchar(240),
	@STTHang int,
	@Master_ID bigint,
	@IsFromVietNam bit,
	@MaDinhDanhLenhSX nvarchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_DinhMuc]
(
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam],
	[MaDinhDanhLenhSX]
)
VALUES 
(
	@MaSanPham,
	@MaNguyenPhuLieu,
	@DVT_ID,
	@DinhMucSuDung,
	@TyLeHaoHut,
	@GhiChu,
	@STTHang,
	@Master_ID,
	@IsFromVietNam,
	@MaDinhDanhLenhSX
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Update]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Update]
	@ID bigint,
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 10),
	@TyLeHaoHut numeric(18, 5),
	@GhiChu varchar(240),
	@STTHang int,
	@Master_ID bigint,
	@IsFromVietNam bit,
	@MaDinhDanhLenhSX nvarchar(50)
AS

UPDATE
	[dbo].[t_KDT_SXXK_DinhMuc]
SET
	[MaSanPham] = @MaSanPham,
	[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
	[DVT_ID] = @DVT_ID,
	[DinhMucSuDung] = @DinhMucSuDung,
	[TyLeHaoHut] = @TyLeHaoHut,
	[GhiChu] = @GhiChu,
	[STTHang] = @STTHang,
	[Master_ID] = @Master_ID,
	[IsFromVietNam] = @IsFromVietNam,
	[MaDinhDanhLenhSX] = @MaDinhDanhLenhSX
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_InsertUpdate]
	@ID bigint,
	@MaSanPham nvarchar(50),
	@MaNguyenPhuLieu nvarchar(50),
	@DVT_ID char(3),
	@DinhMucSuDung numeric(18, 10),
	@TyLeHaoHut numeric(18, 5),
	@GhiChu varchar(240),
	@STTHang int,
	@Master_ID bigint,
	@IsFromVietNam bit,
	@MaDinhDanhLenhSX nvarchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_DinhMuc] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_DinhMuc] 
		SET
			[MaSanPham] = @MaSanPham,
			[MaNguyenPhuLieu] = @MaNguyenPhuLieu,
			[DVT_ID] = @DVT_ID,
			[DinhMucSuDung] = @DinhMucSuDung,
			[TyLeHaoHut] = @TyLeHaoHut,
			[GhiChu] = @GhiChu,
			[STTHang] = @STTHang,
			[Master_ID] = @Master_ID,
			[IsFromVietNam] = @IsFromVietNam,
			[MaDinhDanhLenhSX] = @MaDinhDanhLenhSX
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_DinhMuc]
		(
			[MaSanPham],
			[MaNguyenPhuLieu],
			[DVT_ID],
			[DinhMucSuDung],
			[TyLeHaoHut],
			[GhiChu],
			[STTHang],
			[Master_ID],
			[IsFromVietNam],
			[MaDinhDanhLenhSX]
		)
		VALUES 
		(
			@MaSanPham,
			@MaNguyenPhuLieu,
			@DVT_ID,
			@DinhMucSuDung,
			@TyLeHaoHut,
			@GhiChu,
			@STTHang,
			@Master_ID,
			@IsFromVietNam,
			@MaDinhDanhLenhSX
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Delete]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_DinhMuc]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_DeleteBy_Master_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_DeleteBy_Master_ID]
	@Master_ID bigint
AS

DELETE FROM [dbo].[t_KDT_SXXK_DinhMuc]
WHERE
	[Master_ID] = @Master_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_DinhMuc] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_Load]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam],
	[MaDinhDanhLenhSX]
FROM
	[dbo].[t_KDT_SXXK_DinhMuc]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_SelectBy_Master_ID]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectBy_Master_ID]
	@Master_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam],
	[MaDinhDanhLenhSX]
FROM
	[dbo].[t_KDT_SXXK_DinhMuc]
WHERE
	[Master_ID] = @Master_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam],
	[MaDinhDanhLenhSX]
FROM [dbo].[t_KDT_SXXK_DinhMuc] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_TAP
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[MaSanPham],
	[MaNguyenPhuLieu],
	[DVT_ID],
	[DinhMucSuDung],
	[TyLeHaoHut],
	[GhiChu],
	[STTHang],
	[Master_ID],
	[IsFromVietNam],
	[MaDinhDanhLenhSX]
FROM
	[dbo].[t_KDT_SXXK_DinhMuc]	

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '38.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('38.2',GETDATE(), N'CẬP NHẬT THAY ĐỔI KIỂU DỮ LIỆU MỘT SỐ TRƯỜNG CỦA NGUYÊN PHỤ LIỆU - SẢN PHẨM -ĐỊNH MỨC')
END