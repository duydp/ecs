using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_GiayPhep_SAA : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaNguoiKhai { set; get; }
		public string MaBuuChinhNguoiKhai { set; get; }
		public string DiaChiNguoiKhai { set; get; }
		public string MaNuocNguoiKhai { set; get; }
		public string SoDienThoaiNguoiKhai { set; get; }
		public string SoFaxNguoiKhai { set; get; }
		public string EmailNguoiKhai { set; get; }
		public decimal SoDonXinCapPhep { set; get; }
		public int ChucNangChungTu { set; get; }
		public string LoaiGiayPhep { set; get; }
		public string MaDonViCapPhep { set; get; }
		public string SoHopDong { set; get; }
		public string MaThuongNhanXuatKhau { set; get; }
		public string TenThuongNhanXuatKhau { set; get; }
		public string MaBuuChinhXuatKhau { set; get; }
		public string SoNhaTenDuongXuatKhau { set; get; }
		public string PhuongXaXuatKhau { set; get; }
		public string QuanHuyenXuatKhau { set; get; }
		public string TinhThanhPhoXuatKhau { set; get; }
		public string MaQuocGiaXuatKhau { set; get; }
		public string SoDienThoaiXuatKhau { set; get; }
		public string SoFaxXuatKhau { set; get; }
		public string EmailXuatKhau { set; get; }
		public string NuocXuatKhau { set; get; }
		public string MaThuongNhanNhapKhau { set; get; }
		public string TenThuongNhanNhapKhau { set; get; }
		public string SoDangKyKinhDoanh { set; get; }
		public string MaBuuChinhNhapKhau { set; get; }
		public string DiaChiThuongNhanNhapKhau { set; get; }
		public string MaQuocGiaNhapKhau { set; get; }
		public string SoDienThoaiNhapKhau { set; get; }
		public string SoFaxNhapKhau { set; get; }
		public string EmailNhapKhau { set; get; }
		public string NuocQuaCanh { set; get; }
		public string PhuongTienVanChuyen { set; get; }
		public string MaCuaKhauNhap { set; get; }
		public string TenCuaKhauNhap { set; get; }
		public string SoGiayChungNhan { set; get; }
		public string DiaDiemKiemDich { set; get; }
		public string BenhMienDich { set; get; }
		public DateTime NgayTiemPhong { set; get; }
		public string KhuTrung { set; get; }
		public string NongDo { set; get; }
		public string DiaDiemNuoiTrong { set; get; }
		public DateTime NgayKiemDich { set; get; }
		public DateTime ThoiGianKiemDich { set; get; }
		public string DiaDiemGiamSat { set; get; }
		public DateTime NgayGiamSat { set; get; }
		public DateTime ThoiGianGiamSat { set; get; }
		public decimal SoBanCanCap { set; get; }
		public string VatDungKhac { set; get; }
		public string HoSoLienQuan { set; get; }
		public string NoiChuyenDen { set; get; }
		public string NguoiKiemTra { set; get; }
		public string KetQuaKiemTra { set; get; }
		public string TenTau { set; get; }
		public string QuocTich { set; get; }
		public string TenThuyenTruong { set; get; }
		public string TenBacSi { set; get; }
		public decimal SoThuyenVien { set; get; }
		public decimal SoHanhKhach { set; get; }
		public string CangRoiCuoiCung { set; get; }
		public string CangDenTiepTheo { set; get; }
		public string CangBocHangDauTien { set; get; }
		public DateTime NgayRoiCang { set; get; }
		public string TenHangCangDauTien { set; get; }
		public decimal SoLuongHangCangDauTien { set; get; }
		public string DonViTinhSoLuongHangCangDauTien { set; get; }
		public decimal KhoiLuongHangCangDauTien { set; get; }
		public string DonViTinhKhoiLuongHangCangDauTien { set; get; }
		public string TenHangCanBoc { set; get; }
		public decimal SoLuongHangCanBoc { set; get; }
		public string DonViTinhSoLuongHangCanBoc { set; get; }
		public decimal KhoiLuongHangCanBoc { set; get; }
		public string DonViTinhKhoiLuongHangCanBoc { set; get; }
		public string MaKetQuaXuLy { set; get; }
		public string KetQuaXuLySoGiayPhep { set; get; }
		public DateTime KetQuaXuLyNgayCap { set; get; }
		public DateTime KetQuaXuLyHieuLucTuNgay { set; get; }
		public DateTime KetQuaXuLyHieuLucDenNgay { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		public string LyDoKhongDat { set; get; }
		public string DaiDienDonViCapPhep { set; get; }
		public string HoSoKemTheo_1 { set; get; }
		public string HoSoKemTheo_2 { set; get; }
		public string HoSoKemTheo_3 { set; get; }
		public string HoSoKemTheo_4 { set; get; }
		public string HoSoKemTheo_5 { set; get; }
		public string HoSoKemTheo_6 { set; get; }
		public string HoSoKemTheo_7 { set; get; }
		public string HoSoKemTheo_8 { set; get; }
		public string HoSoKemTheo_9 { set; get; }
		public string HoSoKemTheo_10 { set; get; }
		public string TenLoaiPhuongTienVanChuyen { set; get; }
		public int TrangThaiXuLy { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_GiayPhep_SAA> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_GiayPhep_SAA> collection = new List<KDT_VNACC_GiayPhep_SAA>();
			while (reader.Read())
			{
				KDT_VNACC_GiayPhep_SAA entity = new KDT_VNACC_GiayPhep_SAA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhai"))) entity.MaNguoiKhai = reader.GetString(reader.GetOrdinal("MaNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhNguoiKhai"))) entity.MaBuuChinhNguoiKhai = reader.GetString(reader.GetOrdinal("MaBuuChinhNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiNguoiKhai"))) entity.DiaChiNguoiKhai = reader.GetString(reader.GetOrdinal("DiaChiNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNguoiKhai"))) entity.MaNuocNguoiKhai = reader.GetString(reader.GetOrdinal("MaNuocNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiNguoiKhai"))) entity.SoDienThoaiNguoiKhai = reader.GetString(reader.GetOrdinal("SoDienThoaiNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoFaxNguoiKhai"))) entity.SoFaxNguoiKhai = reader.GetString(reader.GetOrdinal("SoFaxNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("EmailNguoiKhai"))) entity.EmailNguoiKhai = reader.GetString(reader.GetOrdinal("EmailNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDonXinCapPhep"))) entity.SoDonXinCapPhep = reader.GetDecimal(reader.GetOrdinal("SoDonXinCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChucNangChungTu"))) entity.ChucNangChungTu = reader.GetInt32(reader.GetOrdinal("ChucNangChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiGiayPhep"))) entity.LoaiGiayPhep = reader.GetString(reader.GetOrdinal("LoaiGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViCapPhep"))) entity.MaDonViCapPhep = reader.GetString(reader.GetOrdinal("MaDonViCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaThuongNhanXuatKhau"))) entity.MaThuongNhanXuatKhau = reader.GetString(reader.GetOrdinal("MaThuongNhanXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenThuongNhanXuatKhau"))) entity.TenThuongNhanXuatKhau = reader.GetString(reader.GetOrdinal("TenThuongNhanXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhXuatKhau"))) entity.MaBuuChinhXuatKhau = reader.GetString(reader.GetOrdinal("MaBuuChinhXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNhaTenDuongXuatKhau"))) entity.SoNhaTenDuongXuatKhau = reader.GetString(reader.GetOrdinal("SoNhaTenDuongXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongXaXuatKhau"))) entity.PhuongXaXuatKhau = reader.GetString(reader.GetOrdinal("PhuongXaXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuanHuyenXuatKhau"))) entity.QuanHuyenXuatKhau = reader.GetString(reader.GetOrdinal("QuanHuyenXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhThanhPhoXuatKhau"))) entity.TinhThanhPhoXuatKhau = reader.GetString(reader.GetOrdinal("TinhThanhPhoXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaQuocGiaXuatKhau"))) entity.MaQuocGiaXuatKhau = reader.GetString(reader.GetOrdinal("MaQuocGiaXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiXuatKhau"))) entity.SoDienThoaiXuatKhau = reader.GetString(reader.GetOrdinal("SoDienThoaiXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoFaxXuatKhau"))) entity.SoFaxXuatKhau = reader.GetString(reader.GetOrdinal("SoFaxXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("EmailXuatKhau"))) entity.EmailXuatKhau = reader.GetString(reader.GetOrdinal("EmailXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXuatKhau"))) entity.NuocXuatKhau = reader.GetString(reader.GetOrdinal("NuocXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaThuongNhanNhapKhau"))) entity.MaThuongNhanNhapKhau = reader.GetString(reader.GetOrdinal("MaThuongNhanNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenThuongNhanNhapKhau"))) entity.TenThuongNhanNhapKhau = reader.GetString(reader.GetOrdinal("TenThuongNhanNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDangKyKinhDoanh"))) entity.SoDangKyKinhDoanh = reader.GetString(reader.GetOrdinal("SoDangKyKinhDoanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhNhapKhau"))) entity.MaBuuChinhNhapKhau = reader.GetString(reader.GetOrdinal("MaBuuChinhNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiThuongNhanNhapKhau"))) entity.DiaChiThuongNhanNhapKhau = reader.GetString(reader.GetOrdinal("DiaChiThuongNhanNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaQuocGiaNhapKhau"))) entity.MaQuocGiaNhapKhau = reader.GetString(reader.GetOrdinal("MaQuocGiaNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiNhapKhau"))) entity.SoDienThoaiNhapKhau = reader.GetString(reader.GetOrdinal("SoDienThoaiNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoFaxNhapKhau"))) entity.SoFaxNhapKhau = reader.GetString(reader.GetOrdinal("SoFaxNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("EmailNhapKhau"))) entity.EmailNhapKhau = reader.GetString(reader.GetOrdinal("EmailNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocQuaCanh"))) entity.NuocQuaCanh = reader.GetString(reader.GetOrdinal("NuocQuaCanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongTienVanChuyen"))) entity.PhuongTienVanChuyen = reader.GetString(reader.GetOrdinal("PhuongTienVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCuaKhauNhap"))) entity.MaCuaKhauNhap = reader.GetString(reader.GetOrdinal("MaCuaKhauNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCuaKhauNhap"))) entity.TenCuaKhauNhap = reader.GetString(reader.GetOrdinal("TenCuaKhauNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoGiayChungNhan"))) entity.SoGiayChungNhan = reader.GetString(reader.GetOrdinal("SoGiayChungNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemKiemDich"))) entity.DiaDiemKiemDich = reader.GetString(reader.GetOrdinal("DiaDiemKiemDich"));
				if (!reader.IsDBNull(reader.GetOrdinal("BenhMienDich"))) entity.BenhMienDich = reader.GetString(reader.GetOrdinal("BenhMienDich"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiemPhong"))) entity.NgayTiemPhong = reader.GetDateTime(reader.GetOrdinal("NgayTiemPhong"));
				if (!reader.IsDBNull(reader.GetOrdinal("KhuTrung"))) entity.KhuTrung = reader.GetString(reader.GetOrdinal("KhuTrung"));
				if (!reader.IsDBNull(reader.GetOrdinal("NongDo"))) entity.NongDo = reader.GetString(reader.GetOrdinal("NongDo"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemNuoiTrong"))) entity.DiaDiemNuoiTrong = reader.GetString(reader.GetOrdinal("DiaDiemNuoiTrong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKiemDich"))) entity.NgayKiemDich = reader.GetDateTime(reader.GetOrdinal("NgayKiemDich"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianKiemDich"))) entity.ThoiGianKiemDich = reader.GetDateTime(reader.GetOrdinal("ThoiGianKiemDich"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiamSat"))) entity.DiaDiemGiamSat = reader.GetString(reader.GetOrdinal("DiaDiemGiamSat"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayGiamSat"))) entity.NgayGiamSat = reader.GetDateTime(reader.GetOrdinal("NgayGiamSat"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianGiamSat"))) entity.ThoiGianGiamSat = reader.GetDateTime(reader.GetOrdinal("ThoiGianGiamSat"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoBanCanCap"))) entity.SoBanCanCap = reader.GetDecimal(reader.GetOrdinal("SoBanCanCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("VatDungKhac"))) entity.VatDungKhac = reader.GetString(reader.GetOrdinal("VatDungKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoLienQuan"))) entity.HoSoLienQuan = reader.GetString(reader.GetOrdinal("HoSoLienQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiChuyenDen"))) entity.NoiChuyenDen = reader.GetString(reader.GetOrdinal("NoiChuyenDen"));
				if (!reader.IsDBNull(reader.GetOrdinal("NguoiKiemTra"))) entity.NguoiKiemTra = reader.GetString(reader.GetOrdinal("NguoiKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaKiemTra"))) entity.KetQuaKiemTra = reader.GetString(reader.GetOrdinal("KetQuaKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenTau"))) entity.TenTau = reader.GetString(reader.GetOrdinal("TenTau"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuocTich"))) entity.QuocTich = reader.GetString(reader.GetOrdinal("QuocTich"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenThuyenTruong"))) entity.TenThuyenTruong = reader.GetString(reader.GetOrdinal("TenThuyenTruong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenBacSi"))) entity.TenBacSi = reader.GetString(reader.GetOrdinal("TenBacSi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThuyenVien"))) entity.SoThuyenVien = reader.GetDecimal(reader.GetOrdinal("SoThuyenVien"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHanhKhach"))) entity.SoHanhKhach = reader.GetDecimal(reader.GetOrdinal("SoHanhKhach"));
				if (!reader.IsDBNull(reader.GetOrdinal("CangRoiCuoiCung"))) entity.CangRoiCuoiCung = reader.GetString(reader.GetOrdinal("CangRoiCuoiCung"));
				if (!reader.IsDBNull(reader.GetOrdinal("CangDenTiepTheo"))) entity.CangDenTiepTheo = reader.GetString(reader.GetOrdinal("CangDenTiepTheo"));
				if (!reader.IsDBNull(reader.GetOrdinal("CangBocHangDauTien"))) entity.CangBocHangDauTien = reader.GetString(reader.GetOrdinal("CangBocHangDauTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayRoiCang"))) entity.NgayRoiCang = reader.GetDateTime(reader.GetOrdinal("NgayRoiCang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangCangDauTien"))) entity.TenHangCangDauTien = reader.GetString(reader.GetOrdinal("TenHangCangDauTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongHangCangDauTien"))) entity.SoLuongHangCangDauTien = reader.GetDecimal(reader.GetOrdinal("SoLuongHangCangDauTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTinhSoLuongHangCangDauTien"))) entity.DonViTinhSoLuongHangCangDauTien = reader.GetString(reader.GetOrdinal("DonViTinhSoLuongHangCangDauTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("KhoiLuongHangCangDauTien"))) entity.KhoiLuongHangCangDauTien = reader.GetDecimal(reader.GetOrdinal("KhoiLuongHangCangDauTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTinhKhoiLuongHangCangDauTien"))) entity.DonViTinhKhoiLuongHangCangDauTien = reader.GetString(reader.GetOrdinal("DonViTinhKhoiLuongHangCangDauTien"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangCanBoc"))) entity.TenHangCanBoc = reader.GetString(reader.GetOrdinal("TenHangCanBoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongHangCanBoc"))) entity.SoLuongHangCanBoc = reader.GetDecimal(reader.GetOrdinal("SoLuongHangCanBoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTinhSoLuongHangCanBoc"))) entity.DonViTinhSoLuongHangCanBoc = reader.GetString(reader.GetOrdinal("DonViTinhSoLuongHangCanBoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("KhoiLuongHangCanBoc"))) entity.KhoiLuongHangCanBoc = reader.GetDecimal(reader.GetOrdinal("KhoiLuongHangCanBoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTinhKhoiLuongHangCanBoc"))) entity.DonViTinhKhoiLuongHangCanBoc = reader.GetString(reader.GetOrdinal("DonViTinhKhoiLuongHangCanBoc"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKetQuaXuLy"))) entity.MaKetQuaXuLy = reader.GetString(reader.GetOrdinal("MaKetQuaXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLySoGiayPhep"))) entity.KetQuaXuLySoGiayPhep = reader.GetString(reader.GetOrdinal("KetQuaXuLySoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyNgayCap"))) entity.KetQuaXuLyNgayCap = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyNgayCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyHieuLucTuNgay"))) entity.KetQuaXuLyHieuLucTuNgay = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyHieuLucTuNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyHieuLucDenNgay"))) entity.KetQuaXuLyHieuLucDenNgay = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyHieuLucDenNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoKhongDat"))) entity.LyDoKhongDat = reader.GetString(reader.GetOrdinal("LyDoKhongDat"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaiDienDonViCapPhep"))) entity.DaiDienDonViCapPhep = reader.GetString(reader.GetOrdinal("DaiDienDonViCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_1"))) entity.HoSoKemTheo_1 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_1"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_2"))) entity.HoSoKemTheo_2 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_2"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_3"))) entity.HoSoKemTheo_3 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_3"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_4"))) entity.HoSoKemTheo_4 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_4"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_5"))) entity.HoSoKemTheo_5 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_5"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_6"))) entity.HoSoKemTheo_6 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_6"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_7"))) entity.HoSoKemTheo_7 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_7"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_8"))) entity.HoSoKemTheo_8 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_8"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_9"))) entity.HoSoKemTheo_9 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_9"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_10"))) entity.HoSoKemTheo_10 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_10"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenLoaiPhuongTienVanChuyen"))) entity.TenLoaiPhuongTienVanChuyen = reader.GetString(reader.GetOrdinal("TenLoaiPhuongTienVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_GiayPhep_SAA> collection, long id)
        {
            foreach (KDT_VNACC_GiayPhep_SAA item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_GiayPhep_SAA VALUES(@MaNguoiKhai, @MaBuuChinhNguoiKhai, @DiaChiNguoiKhai, @MaNuocNguoiKhai, @SoDienThoaiNguoiKhai, @SoFaxNguoiKhai, @EmailNguoiKhai, @SoDonXinCapPhep, @ChucNangChungTu, @LoaiGiayPhep, @MaDonViCapPhep, @SoHopDong, @MaThuongNhanXuatKhau, @TenThuongNhanXuatKhau, @MaBuuChinhXuatKhau, @SoNhaTenDuongXuatKhau, @PhuongXaXuatKhau, @QuanHuyenXuatKhau, @TinhThanhPhoXuatKhau, @MaQuocGiaXuatKhau, @SoDienThoaiXuatKhau, @SoFaxXuatKhau, @EmailXuatKhau, @NuocXuatKhau, @MaThuongNhanNhapKhau, @TenThuongNhanNhapKhau, @SoDangKyKinhDoanh, @MaBuuChinhNhapKhau, @DiaChiThuongNhanNhapKhau, @MaQuocGiaNhapKhau, @SoDienThoaiNhapKhau, @SoFaxNhapKhau, @EmailNhapKhau, @NuocQuaCanh, @PhuongTienVanChuyen, @MaCuaKhauNhap, @TenCuaKhauNhap, @SoGiayChungNhan, @DiaDiemKiemDich, @BenhMienDich, @NgayTiemPhong, @KhuTrung, @NongDo, @DiaDiemNuoiTrong, @NgayKiemDich, @ThoiGianKiemDich, @DiaDiemGiamSat, @NgayGiamSat, @ThoiGianGiamSat, @SoBanCanCap, @VatDungKhac, @HoSoLienQuan, @NoiChuyenDen, @NguoiKiemTra, @KetQuaKiemTra, @TenTau, @QuocTich, @TenThuyenTruong, @TenBacSi, @SoThuyenVien, @SoHanhKhach, @CangRoiCuoiCung, @CangDenTiepTheo, @CangBocHangDauTien, @NgayRoiCang, @TenHangCangDauTien, @SoLuongHangCangDauTien, @DonViTinhSoLuongHangCangDauTien, @KhoiLuongHangCangDauTien, @DonViTinhKhoiLuongHangCangDauTien, @TenHangCanBoc, @SoLuongHangCanBoc, @DonViTinhSoLuongHangCanBoc, @KhoiLuongHangCanBoc, @DonViTinhKhoiLuongHangCanBoc, @MaKetQuaXuLy, @KetQuaXuLySoGiayPhep, @KetQuaXuLyNgayCap, @KetQuaXuLyHieuLucTuNgay, @KetQuaXuLyHieuLucDenNgay, @Notes, @InputMessageID, @MessageTag, @IndexTag, @LyDoKhongDat, @DaiDienDonViCapPhep, @HoSoKemTheo_1, @HoSoKemTheo_2, @HoSoKemTheo_3, @HoSoKemTheo_4, @HoSoKemTheo_5, @HoSoKemTheo_6, @HoSoKemTheo_7, @HoSoKemTheo_8, @HoSoKemTheo_9, @HoSoKemTheo_10, @TenLoaiPhuongTienVanChuyen, @TrangThaiXuLy, @NgayKhaiBao)";
            string update = "UPDATE t_KDT_VNACC_GiayPhep_SAA SET MaNguoiKhai = @MaNguoiKhai, MaBuuChinhNguoiKhai = @MaBuuChinhNguoiKhai, DiaChiNguoiKhai = @DiaChiNguoiKhai, MaNuocNguoiKhai = @MaNuocNguoiKhai, SoDienThoaiNguoiKhai = @SoDienThoaiNguoiKhai, SoFaxNguoiKhai = @SoFaxNguoiKhai, EmailNguoiKhai = @EmailNguoiKhai, SoDonXinCapPhep = @SoDonXinCapPhep, ChucNangChungTu = @ChucNangChungTu, LoaiGiayPhep = @LoaiGiayPhep, MaDonViCapPhep = @MaDonViCapPhep, SoHopDong = @SoHopDong, MaThuongNhanXuatKhau = @MaThuongNhanXuatKhau, TenThuongNhanXuatKhau = @TenThuongNhanXuatKhau, MaBuuChinhXuatKhau = @MaBuuChinhXuatKhau, SoNhaTenDuongXuatKhau = @SoNhaTenDuongXuatKhau, PhuongXaXuatKhau = @PhuongXaXuatKhau, QuanHuyenXuatKhau = @QuanHuyenXuatKhau, TinhThanhPhoXuatKhau = @TinhThanhPhoXuatKhau, MaQuocGiaXuatKhau = @MaQuocGiaXuatKhau, SoDienThoaiXuatKhau = @SoDienThoaiXuatKhau, SoFaxXuatKhau = @SoFaxXuatKhau, EmailXuatKhau = @EmailXuatKhau, NuocXuatKhau = @NuocXuatKhau, MaThuongNhanNhapKhau = @MaThuongNhanNhapKhau, TenThuongNhanNhapKhau = @TenThuongNhanNhapKhau, SoDangKyKinhDoanh = @SoDangKyKinhDoanh, MaBuuChinhNhapKhau = @MaBuuChinhNhapKhau, DiaChiThuongNhanNhapKhau = @DiaChiThuongNhanNhapKhau, MaQuocGiaNhapKhau = @MaQuocGiaNhapKhau, SoDienThoaiNhapKhau = @SoDienThoaiNhapKhau, SoFaxNhapKhau = @SoFaxNhapKhau, EmailNhapKhau = @EmailNhapKhau, NuocQuaCanh = @NuocQuaCanh, PhuongTienVanChuyen = @PhuongTienVanChuyen, MaCuaKhauNhap = @MaCuaKhauNhap, TenCuaKhauNhap = @TenCuaKhauNhap, SoGiayChungNhan = @SoGiayChungNhan, DiaDiemKiemDich = @DiaDiemKiemDich, BenhMienDich = @BenhMienDich, NgayTiemPhong = @NgayTiemPhong, KhuTrung = @KhuTrung, NongDo = @NongDo, DiaDiemNuoiTrong = @DiaDiemNuoiTrong, NgayKiemDich = @NgayKiemDich, ThoiGianKiemDich = @ThoiGianKiemDich, DiaDiemGiamSat = @DiaDiemGiamSat, NgayGiamSat = @NgayGiamSat, ThoiGianGiamSat = @ThoiGianGiamSat, SoBanCanCap = @SoBanCanCap, VatDungKhac = @VatDungKhac, HoSoLienQuan = @HoSoLienQuan, NoiChuyenDen = @NoiChuyenDen, NguoiKiemTra = @NguoiKiemTra, KetQuaKiemTra = @KetQuaKiemTra, TenTau = @TenTau, QuocTich = @QuocTich, TenThuyenTruong = @TenThuyenTruong, TenBacSi = @TenBacSi, SoThuyenVien = @SoThuyenVien, SoHanhKhach = @SoHanhKhach, CangRoiCuoiCung = @CangRoiCuoiCung, CangDenTiepTheo = @CangDenTiepTheo, CangBocHangDauTien = @CangBocHangDauTien, NgayRoiCang = @NgayRoiCang, TenHangCangDauTien = @TenHangCangDauTien, SoLuongHangCangDauTien = @SoLuongHangCangDauTien, DonViTinhSoLuongHangCangDauTien = @DonViTinhSoLuongHangCangDauTien, KhoiLuongHangCangDauTien = @KhoiLuongHangCangDauTien, DonViTinhKhoiLuongHangCangDauTien = @DonViTinhKhoiLuongHangCangDauTien, TenHangCanBoc = @TenHangCanBoc, SoLuongHangCanBoc = @SoLuongHangCanBoc, DonViTinhSoLuongHangCanBoc = @DonViTinhSoLuongHangCanBoc, KhoiLuongHangCanBoc = @KhoiLuongHangCanBoc, DonViTinhKhoiLuongHangCanBoc = @DonViTinhKhoiLuongHangCanBoc, MaKetQuaXuLy = @MaKetQuaXuLy, KetQuaXuLySoGiayPhep = @KetQuaXuLySoGiayPhep, KetQuaXuLyNgayCap = @KetQuaXuLyNgayCap, KetQuaXuLyHieuLucTuNgay = @KetQuaXuLyHieuLucTuNgay, KetQuaXuLyHieuLucDenNgay = @KetQuaXuLyHieuLucDenNgay, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, LyDoKhongDat = @LyDoKhongDat, DaiDienDonViCapPhep = @DaiDienDonViCapPhep, HoSoKemTheo_1 = @HoSoKemTheo_1, HoSoKemTheo_2 = @HoSoKemTheo_2, HoSoKemTheo_3 = @HoSoKemTheo_3, HoSoKemTheo_4 = @HoSoKemTheo_4, HoSoKemTheo_5 = @HoSoKemTheo_5, HoSoKemTheo_6 = @HoSoKemTheo_6, HoSoKemTheo_7 = @HoSoKemTheo_7, HoSoKemTheo_8 = @HoSoKemTheo_8, HoSoKemTheo_9 = @HoSoKemTheo_9, HoSoKemTheo_10 = @HoSoKemTheo_10, TenLoaiPhuongTienVanChuyen = @TenLoaiPhuongTienVanChuyen, TrangThaiXuLy = @TrangThaiXuLy, NgayKhaiBao = @NgayKhaiBao WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_GiayPhep_SAA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, "MaBuuChinhNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, "MaNuocNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, "SoFaxNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailNguoiKhai", SqlDbType.VarChar, "EmailNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, "SoDienThoaiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, "SoFaxXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailXuatKhau", SqlDbType.VarChar, "EmailXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXuatKhau", SqlDbType.VarChar, "NuocXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKyKinhDoanh", SqlDbType.VarChar, "SoDangKyKinhDoanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, "DiaChiThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocQuaCanh", SqlDbType.VarChar, "NuocQuaCanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCuaKhauNhap", SqlDbType.VarChar, "MaCuaKhauNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCuaKhauNhap", SqlDbType.NVarChar, "TenCuaKhauNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayChungNhan", SqlDbType.NVarChar, "SoGiayChungNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemKiemDich", SqlDbType.NVarChar, "DiaDiemKiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BenhMienDich", SqlDbType.NVarChar, "BenhMienDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiemPhong", SqlDbType.DateTime, "NgayTiemPhong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhuTrung", SqlDbType.NVarChar, "KhuTrung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NongDo", SqlDbType.NVarChar, "NongDo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemNuoiTrong", SqlDbType.NVarChar, "DiaDiemNuoiTrong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKiemDich", SqlDbType.DateTime, "NgayKiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianKiemDich", SqlDbType.DateTime, "ThoiGianKiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemGiamSat", SqlDbType.NVarChar, "DiaDiemGiamSat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayGiamSat", SqlDbType.DateTime, "NgayGiamSat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianGiamSat", SqlDbType.DateTime, "ThoiGianGiamSat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoBanCanCap", SqlDbType.Decimal, "SoBanCanCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@VatDungKhac", SqlDbType.NVarChar, "VatDungKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiChuyenDen", SqlDbType.NVarChar, "NoiChuyenDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiKiemTra", SqlDbType.NVarChar, "NguoiKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, "KetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenTau", SqlDbType.NVarChar, "TenTau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuocTich", SqlDbType.VarChar, "QuocTich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuyenTruong", SqlDbType.NVarChar, "TenThuyenTruong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenBacSi", SqlDbType.NVarChar, "TenBacSi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuyenVien", SqlDbType.Decimal, "SoThuyenVien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHanhKhach", SqlDbType.Decimal, "SoHanhKhach", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CangRoiCuoiCung", SqlDbType.NVarChar, "CangRoiCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CangDenTiepTheo", SqlDbType.NVarChar, "CangDenTiepTheo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CangBocHangDauTien", SqlDbType.NVarChar, "CangBocHangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayRoiCang", SqlDbType.DateTime, "NgayRoiCang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangCangDauTien", SqlDbType.NVarChar, "TenHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongHangCangDauTien", SqlDbType.Decimal, "SoLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhSoLuongHangCangDauTien", SqlDbType.VarChar, "DonViTinhSoLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhoiLuongHangCangDauTien", SqlDbType.Decimal, "KhoiLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhKhoiLuongHangCangDauTien", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangCanBoc", SqlDbType.NVarChar, "TenHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongHangCanBoc", SqlDbType.Decimal, "SoLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhSoLuongHangCanBoc", SqlDbType.VarChar, "DonViTinhSoLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhoiLuongHangCanBoc", SqlDbType.Decimal, "KhoiLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhKhoiLuongHangCanBoc", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoKhongDat", SqlDbType.NVarChar, "LyDoKhongDat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenLoaiPhuongTienVanChuyen", SqlDbType.NVarChar, "TenLoaiPhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, "MaBuuChinhNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, "MaNuocNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, "SoFaxNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailNguoiKhai", SqlDbType.VarChar, "EmailNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, "SoDienThoaiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, "SoFaxXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailXuatKhau", SqlDbType.VarChar, "EmailXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXuatKhau", SqlDbType.VarChar, "NuocXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKyKinhDoanh", SqlDbType.VarChar, "SoDangKyKinhDoanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, "DiaChiThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocQuaCanh", SqlDbType.VarChar, "NuocQuaCanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCuaKhauNhap", SqlDbType.VarChar, "MaCuaKhauNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCuaKhauNhap", SqlDbType.NVarChar, "TenCuaKhauNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayChungNhan", SqlDbType.NVarChar, "SoGiayChungNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemKiemDich", SqlDbType.NVarChar, "DiaDiemKiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BenhMienDich", SqlDbType.NVarChar, "BenhMienDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiemPhong", SqlDbType.DateTime, "NgayTiemPhong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhuTrung", SqlDbType.NVarChar, "KhuTrung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NongDo", SqlDbType.NVarChar, "NongDo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemNuoiTrong", SqlDbType.NVarChar, "DiaDiemNuoiTrong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKiemDich", SqlDbType.DateTime, "NgayKiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianKiemDich", SqlDbType.DateTime, "ThoiGianKiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemGiamSat", SqlDbType.NVarChar, "DiaDiemGiamSat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayGiamSat", SqlDbType.DateTime, "NgayGiamSat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianGiamSat", SqlDbType.DateTime, "ThoiGianGiamSat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoBanCanCap", SqlDbType.Decimal, "SoBanCanCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@VatDungKhac", SqlDbType.NVarChar, "VatDungKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiChuyenDen", SqlDbType.NVarChar, "NoiChuyenDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiKiemTra", SqlDbType.NVarChar, "NguoiKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, "KetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenTau", SqlDbType.NVarChar, "TenTau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuocTich", SqlDbType.VarChar, "QuocTich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuyenTruong", SqlDbType.NVarChar, "TenThuyenTruong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenBacSi", SqlDbType.NVarChar, "TenBacSi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuyenVien", SqlDbType.Decimal, "SoThuyenVien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHanhKhach", SqlDbType.Decimal, "SoHanhKhach", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CangRoiCuoiCung", SqlDbType.NVarChar, "CangRoiCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CangDenTiepTheo", SqlDbType.NVarChar, "CangDenTiepTheo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CangBocHangDauTien", SqlDbType.NVarChar, "CangBocHangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayRoiCang", SqlDbType.DateTime, "NgayRoiCang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangCangDauTien", SqlDbType.NVarChar, "TenHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongHangCangDauTien", SqlDbType.Decimal, "SoLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhSoLuongHangCangDauTien", SqlDbType.VarChar, "DonViTinhSoLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhoiLuongHangCangDauTien", SqlDbType.Decimal, "KhoiLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhKhoiLuongHangCangDauTien", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangCanBoc", SqlDbType.NVarChar, "TenHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongHangCanBoc", SqlDbType.Decimal, "SoLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhSoLuongHangCanBoc", SqlDbType.VarChar, "DonViTinhSoLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhoiLuongHangCanBoc", SqlDbType.Decimal, "KhoiLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhKhoiLuongHangCanBoc", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoKhongDat", SqlDbType.NVarChar, "LyDoKhongDat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenLoaiPhuongTienVanChuyen", SqlDbType.NVarChar, "TenLoaiPhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_GiayPhep_SAA VALUES(@MaNguoiKhai, @MaBuuChinhNguoiKhai, @DiaChiNguoiKhai, @MaNuocNguoiKhai, @SoDienThoaiNguoiKhai, @SoFaxNguoiKhai, @EmailNguoiKhai, @SoDonXinCapPhep, @ChucNangChungTu, @LoaiGiayPhep, @MaDonViCapPhep, @SoHopDong, @MaThuongNhanXuatKhau, @TenThuongNhanXuatKhau, @MaBuuChinhXuatKhau, @SoNhaTenDuongXuatKhau, @PhuongXaXuatKhau, @QuanHuyenXuatKhau, @TinhThanhPhoXuatKhau, @MaQuocGiaXuatKhau, @SoDienThoaiXuatKhau, @SoFaxXuatKhau, @EmailXuatKhau, @NuocXuatKhau, @MaThuongNhanNhapKhau, @TenThuongNhanNhapKhau, @SoDangKyKinhDoanh, @MaBuuChinhNhapKhau, @DiaChiThuongNhanNhapKhau, @MaQuocGiaNhapKhau, @SoDienThoaiNhapKhau, @SoFaxNhapKhau, @EmailNhapKhau, @NuocQuaCanh, @PhuongTienVanChuyen, @MaCuaKhauNhap, @TenCuaKhauNhap, @SoGiayChungNhan, @DiaDiemKiemDich, @BenhMienDich, @NgayTiemPhong, @KhuTrung, @NongDo, @DiaDiemNuoiTrong, @NgayKiemDich, @ThoiGianKiemDich, @DiaDiemGiamSat, @NgayGiamSat, @ThoiGianGiamSat, @SoBanCanCap, @VatDungKhac, @HoSoLienQuan, @NoiChuyenDen, @NguoiKiemTra, @KetQuaKiemTra, @TenTau, @QuocTich, @TenThuyenTruong, @TenBacSi, @SoThuyenVien, @SoHanhKhach, @CangRoiCuoiCung, @CangDenTiepTheo, @CangBocHangDauTien, @NgayRoiCang, @TenHangCangDauTien, @SoLuongHangCangDauTien, @DonViTinhSoLuongHangCangDauTien, @KhoiLuongHangCangDauTien, @DonViTinhKhoiLuongHangCangDauTien, @TenHangCanBoc, @SoLuongHangCanBoc, @DonViTinhSoLuongHangCanBoc, @KhoiLuongHangCanBoc, @DonViTinhKhoiLuongHangCanBoc, @MaKetQuaXuLy, @KetQuaXuLySoGiayPhep, @KetQuaXuLyNgayCap, @KetQuaXuLyHieuLucTuNgay, @KetQuaXuLyHieuLucDenNgay, @Notes, @InputMessageID, @MessageTag, @IndexTag, @LyDoKhongDat, @DaiDienDonViCapPhep, @HoSoKemTheo_1, @HoSoKemTheo_2, @HoSoKemTheo_3, @HoSoKemTheo_4, @HoSoKemTheo_5, @HoSoKemTheo_6, @HoSoKemTheo_7, @HoSoKemTheo_8, @HoSoKemTheo_9, @HoSoKemTheo_10, @TenLoaiPhuongTienVanChuyen, @TrangThaiXuLy, @NgayKhaiBao)";
            string update = "UPDATE t_KDT_VNACC_GiayPhep_SAA SET MaNguoiKhai = @MaNguoiKhai, MaBuuChinhNguoiKhai = @MaBuuChinhNguoiKhai, DiaChiNguoiKhai = @DiaChiNguoiKhai, MaNuocNguoiKhai = @MaNuocNguoiKhai, SoDienThoaiNguoiKhai = @SoDienThoaiNguoiKhai, SoFaxNguoiKhai = @SoFaxNguoiKhai, EmailNguoiKhai = @EmailNguoiKhai, SoDonXinCapPhep = @SoDonXinCapPhep, ChucNangChungTu = @ChucNangChungTu, LoaiGiayPhep = @LoaiGiayPhep, MaDonViCapPhep = @MaDonViCapPhep, SoHopDong = @SoHopDong, MaThuongNhanXuatKhau = @MaThuongNhanXuatKhau, TenThuongNhanXuatKhau = @TenThuongNhanXuatKhau, MaBuuChinhXuatKhau = @MaBuuChinhXuatKhau, SoNhaTenDuongXuatKhau = @SoNhaTenDuongXuatKhau, PhuongXaXuatKhau = @PhuongXaXuatKhau, QuanHuyenXuatKhau = @QuanHuyenXuatKhau, TinhThanhPhoXuatKhau = @TinhThanhPhoXuatKhau, MaQuocGiaXuatKhau = @MaQuocGiaXuatKhau, SoDienThoaiXuatKhau = @SoDienThoaiXuatKhau, SoFaxXuatKhau = @SoFaxXuatKhau, EmailXuatKhau = @EmailXuatKhau, NuocXuatKhau = @NuocXuatKhau, MaThuongNhanNhapKhau = @MaThuongNhanNhapKhau, TenThuongNhanNhapKhau = @TenThuongNhanNhapKhau, SoDangKyKinhDoanh = @SoDangKyKinhDoanh, MaBuuChinhNhapKhau = @MaBuuChinhNhapKhau, DiaChiThuongNhanNhapKhau = @DiaChiThuongNhanNhapKhau, MaQuocGiaNhapKhau = @MaQuocGiaNhapKhau, SoDienThoaiNhapKhau = @SoDienThoaiNhapKhau, SoFaxNhapKhau = @SoFaxNhapKhau, EmailNhapKhau = @EmailNhapKhau, NuocQuaCanh = @NuocQuaCanh, PhuongTienVanChuyen = @PhuongTienVanChuyen, MaCuaKhauNhap = @MaCuaKhauNhap, TenCuaKhauNhap = @TenCuaKhauNhap, SoGiayChungNhan = @SoGiayChungNhan, DiaDiemKiemDich = @DiaDiemKiemDich, BenhMienDich = @BenhMienDich, NgayTiemPhong = @NgayTiemPhong, KhuTrung = @KhuTrung, NongDo = @NongDo, DiaDiemNuoiTrong = @DiaDiemNuoiTrong, NgayKiemDich = @NgayKiemDich, ThoiGianKiemDich = @ThoiGianKiemDich, DiaDiemGiamSat = @DiaDiemGiamSat, NgayGiamSat = @NgayGiamSat, ThoiGianGiamSat = @ThoiGianGiamSat, SoBanCanCap = @SoBanCanCap, VatDungKhac = @VatDungKhac, HoSoLienQuan = @HoSoLienQuan, NoiChuyenDen = @NoiChuyenDen, NguoiKiemTra = @NguoiKiemTra, KetQuaKiemTra = @KetQuaKiemTra, TenTau = @TenTau, QuocTich = @QuocTich, TenThuyenTruong = @TenThuyenTruong, TenBacSi = @TenBacSi, SoThuyenVien = @SoThuyenVien, SoHanhKhach = @SoHanhKhach, CangRoiCuoiCung = @CangRoiCuoiCung, CangDenTiepTheo = @CangDenTiepTheo, CangBocHangDauTien = @CangBocHangDauTien, NgayRoiCang = @NgayRoiCang, TenHangCangDauTien = @TenHangCangDauTien, SoLuongHangCangDauTien = @SoLuongHangCangDauTien, DonViTinhSoLuongHangCangDauTien = @DonViTinhSoLuongHangCangDauTien, KhoiLuongHangCangDauTien = @KhoiLuongHangCangDauTien, DonViTinhKhoiLuongHangCangDauTien = @DonViTinhKhoiLuongHangCangDauTien, TenHangCanBoc = @TenHangCanBoc, SoLuongHangCanBoc = @SoLuongHangCanBoc, DonViTinhSoLuongHangCanBoc = @DonViTinhSoLuongHangCanBoc, KhoiLuongHangCanBoc = @KhoiLuongHangCanBoc, DonViTinhKhoiLuongHangCanBoc = @DonViTinhKhoiLuongHangCanBoc, MaKetQuaXuLy = @MaKetQuaXuLy, KetQuaXuLySoGiayPhep = @KetQuaXuLySoGiayPhep, KetQuaXuLyNgayCap = @KetQuaXuLyNgayCap, KetQuaXuLyHieuLucTuNgay = @KetQuaXuLyHieuLucTuNgay, KetQuaXuLyHieuLucDenNgay = @KetQuaXuLyHieuLucDenNgay, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, LyDoKhongDat = @LyDoKhongDat, DaiDienDonViCapPhep = @DaiDienDonViCapPhep, HoSoKemTheo_1 = @HoSoKemTheo_1, HoSoKemTheo_2 = @HoSoKemTheo_2, HoSoKemTheo_3 = @HoSoKemTheo_3, HoSoKemTheo_4 = @HoSoKemTheo_4, HoSoKemTheo_5 = @HoSoKemTheo_5, HoSoKemTheo_6 = @HoSoKemTheo_6, HoSoKemTheo_7 = @HoSoKemTheo_7, HoSoKemTheo_8 = @HoSoKemTheo_8, HoSoKemTheo_9 = @HoSoKemTheo_9, HoSoKemTheo_10 = @HoSoKemTheo_10, TenLoaiPhuongTienVanChuyen = @TenLoaiPhuongTienVanChuyen, TrangThaiXuLy = @TrangThaiXuLy, NgayKhaiBao = @NgayKhaiBao WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_GiayPhep_SAA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, "MaBuuChinhNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, "MaNuocNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, "SoFaxNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailNguoiKhai", SqlDbType.VarChar, "EmailNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, "SoDienThoaiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, "SoFaxXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailXuatKhau", SqlDbType.VarChar, "EmailXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXuatKhau", SqlDbType.VarChar, "NuocXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKyKinhDoanh", SqlDbType.VarChar, "SoDangKyKinhDoanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, "DiaChiThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocQuaCanh", SqlDbType.VarChar, "NuocQuaCanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCuaKhauNhap", SqlDbType.VarChar, "MaCuaKhauNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCuaKhauNhap", SqlDbType.NVarChar, "TenCuaKhauNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoGiayChungNhan", SqlDbType.NVarChar, "SoGiayChungNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemKiemDich", SqlDbType.NVarChar, "DiaDiemKiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BenhMienDich", SqlDbType.NVarChar, "BenhMienDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiemPhong", SqlDbType.DateTime, "NgayTiemPhong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhuTrung", SqlDbType.NVarChar, "KhuTrung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NongDo", SqlDbType.NVarChar, "NongDo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemNuoiTrong", SqlDbType.NVarChar, "DiaDiemNuoiTrong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKiemDich", SqlDbType.DateTime, "NgayKiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianKiemDich", SqlDbType.DateTime, "ThoiGianKiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemGiamSat", SqlDbType.NVarChar, "DiaDiemGiamSat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayGiamSat", SqlDbType.DateTime, "NgayGiamSat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianGiamSat", SqlDbType.DateTime, "ThoiGianGiamSat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoBanCanCap", SqlDbType.Decimal, "SoBanCanCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@VatDungKhac", SqlDbType.NVarChar, "VatDungKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiChuyenDen", SqlDbType.NVarChar, "NoiChuyenDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NguoiKiemTra", SqlDbType.NVarChar, "NguoiKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, "KetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenTau", SqlDbType.NVarChar, "TenTau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuocTich", SqlDbType.VarChar, "QuocTich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuyenTruong", SqlDbType.NVarChar, "TenThuyenTruong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenBacSi", SqlDbType.NVarChar, "TenBacSi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThuyenVien", SqlDbType.Decimal, "SoThuyenVien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHanhKhach", SqlDbType.Decimal, "SoHanhKhach", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CangRoiCuoiCung", SqlDbType.NVarChar, "CangRoiCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CangDenTiepTheo", SqlDbType.NVarChar, "CangDenTiepTheo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CangBocHangDauTien", SqlDbType.NVarChar, "CangBocHangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayRoiCang", SqlDbType.DateTime, "NgayRoiCang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangCangDauTien", SqlDbType.NVarChar, "TenHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongHangCangDauTien", SqlDbType.Decimal, "SoLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhSoLuongHangCangDauTien", SqlDbType.VarChar, "DonViTinhSoLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhoiLuongHangCangDauTien", SqlDbType.Decimal, "KhoiLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhKhoiLuongHangCangDauTien", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangCanBoc", SqlDbType.NVarChar, "TenHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongHangCanBoc", SqlDbType.Decimal, "SoLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhSoLuongHangCanBoc", SqlDbType.VarChar, "DonViTinhSoLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhoiLuongHangCanBoc", SqlDbType.Decimal, "KhoiLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhKhoiLuongHangCanBoc", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoKhongDat", SqlDbType.NVarChar, "LyDoKhongDat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenLoaiPhuongTienVanChuyen", SqlDbType.NVarChar, "TenLoaiPhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, "MaBuuChinhNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, "DiaChiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, "MaNuocNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, "SoDienThoaiNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, "SoFaxNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailNguoiKhai", SqlDbType.VarChar, "EmailNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, "SoDienThoaiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, "SoFaxXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailXuatKhau", SqlDbType.VarChar, "EmailXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXuatKhau", SqlDbType.VarChar, "NuocXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKyKinhDoanh", SqlDbType.VarChar, "SoDangKyKinhDoanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, "DiaChiThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocQuaCanh", SqlDbType.VarChar, "NuocQuaCanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, "PhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCuaKhauNhap", SqlDbType.VarChar, "MaCuaKhauNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCuaKhauNhap", SqlDbType.NVarChar, "TenCuaKhauNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoGiayChungNhan", SqlDbType.NVarChar, "SoGiayChungNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemKiemDich", SqlDbType.NVarChar, "DiaDiemKiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BenhMienDich", SqlDbType.NVarChar, "BenhMienDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiemPhong", SqlDbType.DateTime, "NgayTiemPhong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhuTrung", SqlDbType.NVarChar, "KhuTrung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NongDo", SqlDbType.NVarChar, "NongDo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemNuoiTrong", SqlDbType.NVarChar, "DiaDiemNuoiTrong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKiemDich", SqlDbType.DateTime, "NgayKiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianKiemDich", SqlDbType.DateTime, "ThoiGianKiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemGiamSat", SqlDbType.NVarChar, "DiaDiemGiamSat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayGiamSat", SqlDbType.DateTime, "NgayGiamSat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianGiamSat", SqlDbType.DateTime, "ThoiGianGiamSat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoBanCanCap", SqlDbType.Decimal, "SoBanCanCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@VatDungKhac", SqlDbType.NVarChar, "VatDungKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiChuyenDen", SqlDbType.NVarChar, "NoiChuyenDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NguoiKiemTra", SqlDbType.NVarChar, "NguoiKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, "KetQuaKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenTau", SqlDbType.NVarChar, "TenTau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuocTich", SqlDbType.VarChar, "QuocTich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuyenTruong", SqlDbType.NVarChar, "TenThuyenTruong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenBacSi", SqlDbType.NVarChar, "TenBacSi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThuyenVien", SqlDbType.Decimal, "SoThuyenVien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHanhKhach", SqlDbType.Decimal, "SoHanhKhach", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CangRoiCuoiCung", SqlDbType.NVarChar, "CangRoiCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CangDenTiepTheo", SqlDbType.NVarChar, "CangDenTiepTheo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CangBocHangDauTien", SqlDbType.NVarChar, "CangBocHangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayRoiCang", SqlDbType.DateTime, "NgayRoiCang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangCangDauTien", SqlDbType.NVarChar, "TenHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongHangCangDauTien", SqlDbType.Decimal, "SoLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhSoLuongHangCangDauTien", SqlDbType.VarChar, "DonViTinhSoLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhoiLuongHangCangDauTien", SqlDbType.Decimal, "KhoiLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhKhoiLuongHangCangDauTien", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCangDauTien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangCanBoc", SqlDbType.NVarChar, "TenHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongHangCanBoc", SqlDbType.Decimal, "SoLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhSoLuongHangCanBoc", SqlDbType.VarChar, "DonViTinhSoLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhoiLuongHangCanBoc", SqlDbType.Decimal, "KhoiLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhKhoiLuongHangCanBoc", SqlDbType.VarChar, "DonViTinhKhoiLuongHangCanBoc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoKhongDat", SqlDbType.NVarChar, "LyDoKhongDat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, "DaiDienDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenLoaiPhuongTienVanChuyen", SqlDbType.NVarChar, "TenLoaiPhuongTienVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_GiayPhep_SAA Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_GiayPhep_SAA> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_GiayPhep_SAA> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_GiayPhep_SAA> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_GiayPhep_SAA(string maNguoiKhai, string maBuuChinhNguoiKhai, string diaChiNguoiKhai, string maNuocNguoiKhai, string soDienThoaiNguoiKhai, string soFaxNguoiKhai, string emailNguoiKhai, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string maDonViCapPhep, string soHopDong, string maThuongNhanXuatKhau, string tenThuongNhanXuatKhau, string maBuuChinhXuatKhau, string soNhaTenDuongXuatKhau, string phuongXaXuatKhau, string quanHuyenXuatKhau, string tinhThanhPhoXuatKhau, string maQuocGiaXuatKhau, string soDienThoaiXuatKhau, string soFaxXuatKhau, string emailXuatKhau, string nuocXuatKhau, string maThuongNhanNhapKhau, string tenThuongNhanNhapKhau, string soDangKyKinhDoanh, string maBuuChinhNhapKhau, string diaChiThuongNhanNhapKhau, string maQuocGiaNhapKhau, string soDienThoaiNhapKhau, string soFaxNhapKhau, string emailNhapKhau, string nuocQuaCanh, string phuongTienVanChuyen, string maCuaKhauNhap, string tenCuaKhauNhap, string soGiayChungNhan, string diaDiemKiemDich, string benhMienDich, DateTime ngayTiemPhong, string khuTrung, string nongDo, string diaDiemNuoiTrong, DateTime ngayKiemDich, DateTime thoiGianKiemDich, string diaDiemGiamSat, DateTime ngayGiamSat, DateTime thoiGianGiamSat, decimal soBanCanCap, string vatDungKhac, string hoSoLienQuan, string noiChuyenDen, string nguoiKiemTra, string ketQuaKiemTra, string tenTau, string quocTich, string tenThuyenTruong, string tenBacSi, decimal soThuyenVien, decimal soHanhKhach, string cangRoiCuoiCung, string cangDenTiepTheo, string cangBocHangDauTien, DateTime ngayRoiCang, string tenHangCangDauTien, decimal soLuongHangCangDauTien, string donViTinhSoLuongHangCangDauTien, decimal khoiLuongHangCangDauTien, string donViTinhKhoiLuongHangCangDauTien, string tenHangCanBoc, decimal soLuongHangCanBoc, string donViTinhSoLuongHangCanBoc, decimal khoiLuongHangCanBoc, string donViTinhKhoiLuongHangCanBoc, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string notes, string inputMessageID, string messageTag, string indexTag, string lyDoKhongDat, string daiDienDonViCapPhep, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, string tenLoaiPhuongTienVanChuyen, int trangThaiXuLy, DateTime ngayKhaiBao)
		{
			KDT_VNACC_GiayPhep_SAA entity = new KDT_VNACC_GiayPhep_SAA();	
			entity.MaNguoiKhai = maNguoiKhai;
			entity.MaBuuChinhNguoiKhai = maBuuChinhNguoiKhai;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.MaNuocNguoiKhai = maNuocNguoiKhai;
			entity.SoDienThoaiNguoiKhai = soDienThoaiNguoiKhai;
			entity.SoFaxNguoiKhai = soFaxNguoiKhai;
			entity.EmailNguoiKhai = emailNguoiKhai;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.MaDonViCapPhep = maDonViCapPhep;
			entity.SoHopDong = soHopDong;
			entity.MaThuongNhanXuatKhau = maThuongNhanXuatKhau;
			entity.TenThuongNhanXuatKhau = tenThuongNhanXuatKhau;
			entity.MaBuuChinhXuatKhau = maBuuChinhXuatKhau;
			entity.SoNhaTenDuongXuatKhau = soNhaTenDuongXuatKhau;
			entity.PhuongXaXuatKhau = phuongXaXuatKhau;
			entity.QuanHuyenXuatKhau = quanHuyenXuatKhau;
			entity.TinhThanhPhoXuatKhau = tinhThanhPhoXuatKhau;
			entity.MaQuocGiaXuatKhau = maQuocGiaXuatKhau;
			entity.SoDienThoaiXuatKhau = soDienThoaiXuatKhau;
			entity.SoFaxXuatKhau = soFaxXuatKhau;
			entity.EmailXuatKhau = emailXuatKhau;
			entity.NuocXuatKhau = nuocXuatKhau;
			entity.MaThuongNhanNhapKhau = maThuongNhanNhapKhau;
			entity.TenThuongNhanNhapKhau = tenThuongNhanNhapKhau;
			entity.SoDangKyKinhDoanh = soDangKyKinhDoanh;
			entity.MaBuuChinhNhapKhau = maBuuChinhNhapKhau;
			entity.DiaChiThuongNhanNhapKhau = diaChiThuongNhanNhapKhau;
			entity.MaQuocGiaNhapKhau = maQuocGiaNhapKhau;
			entity.SoDienThoaiNhapKhau = soDienThoaiNhapKhau;
			entity.SoFaxNhapKhau = soFaxNhapKhau;
			entity.EmailNhapKhau = emailNhapKhau;
			entity.NuocQuaCanh = nuocQuaCanh;
			entity.PhuongTienVanChuyen = phuongTienVanChuyen;
			entity.MaCuaKhauNhap = maCuaKhauNhap;
			entity.TenCuaKhauNhap = tenCuaKhauNhap;
			entity.SoGiayChungNhan = soGiayChungNhan;
			entity.DiaDiemKiemDich = diaDiemKiemDich;
			entity.BenhMienDich = benhMienDich;
			entity.NgayTiemPhong = ngayTiemPhong;
			entity.KhuTrung = khuTrung;
			entity.NongDo = nongDo;
			entity.DiaDiemNuoiTrong = diaDiemNuoiTrong;
			entity.NgayKiemDich = ngayKiemDich;
			entity.ThoiGianKiemDich = thoiGianKiemDich;
			entity.DiaDiemGiamSat = diaDiemGiamSat;
			entity.NgayGiamSat = ngayGiamSat;
			entity.ThoiGianGiamSat = thoiGianGiamSat;
			entity.SoBanCanCap = soBanCanCap;
			entity.VatDungKhac = vatDungKhac;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.NoiChuyenDen = noiChuyenDen;
			entity.NguoiKiemTra = nguoiKiemTra;
			entity.KetQuaKiemTra = ketQuaKiemTra;
			entity.TenTau = tenTau;
			entity.QuocTich = quocTich;
			entity.TenThuyenTruong = tenThuyenTruong;
			entity.TenBacSi = tenBacSi;
			entity.SoThuyenVien = soThuyenVien;
			entity.SoHanhKhach = soHanhKhach;
			entity.CangRoiCuoiCung = cangRoiCuoiCung;
			entity.CangDenTiepTheo = cangDenTiepTheo;
			entity.CangBocHangDauTien = cangBocHangDauTien;
			entity.NgayRoiCang = ngayRoiCang;
			entity.TenHangCangDauTien = tenHangCangDauTien;
			entity.SoLuongHangCangDauTien = soLuongHangCangDauTien;
			entity.DonViTinhSoLuongHangCangDauTien = donViTinhSoLuongHangCangDauTien;
			entity.KhoiLuongHangCangDauTien = khoiLuongHangCangDauTien;
			entity.DonViTinhKhoiLuongHangCangDauTien = donViTinhKhoiLuongHangCangDauTien;
			entity.TenHangCanBoc = tenHangCanBoc;
			entity.SoLuongHangCanBoc = soLuongHangCanBoc;
			entity.DonViTinhSoLuongHangCanBoc = donViTinhSoLuongHangCanBoc;
			entity.KhoiLuongHangCanBoc = khoiLuongHangCanBoc;
			entity.DonViTinhKhoiLuongHangCanBoc = donViTinhKhoiLuongHangCanBoc;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.LyDoKhongDat = lyDoKhongDat;
			entity.DaiDienDonViCapPhep = daiDienDonViCapPhep;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.TenLoaiPhuongTienVanChuyen = tenLoaiPhuongTienVanChuyen;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayKhaiBao = ngayKhaiBao;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, MaBuuChinhNguoiKhai);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, MaNuocNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, SoDienThoaiNguoiKhai);
			db.AddInParameter(dbCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, SoFaxNguoiKhai);
			db.AddInParameter(dbCommand, "@EmailNguoiKhai", SqlDbType.VarChar, EmailNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@MaDonViCapPhep", SqlDbType.VarChar, MaDonViCapPhep);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, MaThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, TenThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, MaBuuChinhXuatKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, SoNhaTenDuongXuatKhau);
			db.AddInParameter(dbCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, PhuongXaXuatKhau);
			db.AddInParameter(dbCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, QuanHuyenXuatKhau);
			db.AddInParameter(dbCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, TinhThanhPhoXuatKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, MaQuocGiaXuatKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, SoDienThoaiXuatKhau);
			db.AddInParameter(dbCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, SoFaxXuatKhau);
			db.AddInParameter(dbCommand, "@EmailXuatKhau", SqlDbType.VarChar, EmailXuatKhau);
			db.AddInParameter(dbCommand, "@NuocXuatKhau", SqlDbType.VarChar, NuocXuatKhau);
			db.AddInParameter(dbCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, MaThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, TenThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@SoDangKyKinhDoanh", SqlDbType.VarChar, SoDangKyKinhDoanh);
			db.AddInParameter(dbCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, MaBuuChinhNhapKhau);
			db.AddInParameter(dbCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, DiaChiThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, MaQuocGiaNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, SoDienThoaiNhapKhau);
			db.AddInParameter(dbCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, SoFaxNhapKhau);
			db.AddInParameter(dbCommand, "@EmailNhapKhau", SqlDbType.VarChar, EmailNhapKhau);
			db.AddInParameter(dbCommand, "@NuocQuaCanh", SqlDbType.VarChar, NuocQuaCanh);
			db.AddInParameter(dbCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, PhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@MaCuaKhauNhap", SqlDbType.VarChar, MaCuaKhauNhap);
			db.AddInParameter(dbCommand, "@TenCuaKhauNhap", SqlDbType.NVarChar, TenCuaKhauNhap);
			db.AddInParameter(dbCommand, "@SoGiayChungNhan", SqlDbType.NVarChar, SoGiayChungNhan);
			db.AddInParameter(dbCommand, "@DiaDiemKiemDich", SqlDbType.NVarChar, DiaDiemKiemDich);
			db.AddInParameter(dbCommand, "@BenhMienDich", SqlDbType.NVarChar, BenhMienDich);
			db.AddInParameter(dbCommand, "@NgayTiemPhong", SqlDbType.DateTime, NgayTiemPhong.Year <= 1753 ? DBNull.Value : (object) NgayTiemPhong);
			db.AddInParameter(dbCommand, "@KhuTrung", SqlDbType.NVarChar, KhuTrung);
			db.AddInParameter(dbCommand, "@NongDo", SqlDbType.NVarChar, NongDo);
			db.AddInParameter(dbCommand, "@DiaDiemNuoiTrong", SqlDbType.NVarChar, DiaDiemNuoiTrong);
			db.AddInParameter(dbCommand, "@NgayKiemDich", SqlDbType.DateTime, NgayKiemDich.Year <= 1753 ? DBNull.Value : (object) NgayKiemDich);
			db.AddInParameter(dbCommand, "@ThoiGianKiemDich", SqlDbType.DateTime, ThoiGianKiemDich.Year <= 1753 ? DBNull.Value : (object) ThoiGianKiemDich);
			db.AddInParameter(dbCommand, "@DiaDiemGiamSat", SqlDbType.NVarChar, DiaDiemGiamSat);
			db.AddInParameter(dbCommand, "@NgayGiamSat", SqlDbType.DateTime, NgayGiamSat.Year <= 1753 ? DBNull.Value : (object) NgayGiamSat);
			db.AddInParameter(dbCommand, "@ThoiGianGiamSat", SqlDbType.DateTime, ThoiGianGiamSat.Year <= 1753 ? DBNull.Value : (object) ThoiGianGiamSat);
			db.AddInParameter(dbCommand, "@SoBanCanCap", SqlDbType.Decimal, SoBanCanCap);
			db.AddInParameter(dbCommand, "@VatDungKhac", SqlDbType.NVarChar, VatDungKhac);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@NoiChuyenDen", SqlDbType.NVarChar, NoiChuyenDen);
			db.AddInParameter(dbCommand, "@NguoiKiemTra", SqlDbType.NVarChar, NguoiKiemTra);
			db.AddInParameter(dbCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, KetQuaKiemTra);
			db.AddInParameter(dbCommand, "@TenTau", SqlDbType.NVarChar, TenTau);
			db.AddInParameter(dbCommand, "@QuocTich", SqlDbType.VarChar, QuocTich);
			db.AddInParameter(dbCommand, "@TenThuyenTruong", SqlDbType.NVarChar, TenThuyenTruong);
			db.AddInParameter(dbCommand, "@TenBacSi", SqlDbType.NVarChar, TenBacSi);
			db.AddInParameter(dbCommand, "@SoThuyenVien", SqlDbType.Decimal, SoThuyenVien);
			db.AddInParameter(dbCommand, "@SoHanhKhach", SqlDbType.Decimal, SoHanhKhach);
			db.AddInParameter(dbCommand, "@CangRoiCuoiCung", SqlDbType.NVarChar, CangRoiCuoiCung);
			db.AddInParameter(dbCommand, "@CangDenTiepTheo", SqlDbType.NVarChar, CangDenTiepTheo);
			db.AddInParameter(dbCommand, "@CangBocHangDauTien", SqlDbType.NVarChar, CangBocHangDauTien);
			db.AddInParameter(dbCommand, "@NgayRoiCang", SqlDbType.DateTime, NgayRoiCang.Year <= 1753 ? DBNull.Value : (object) NgayRoiCang);
			db.AddInParameter(dbCommand, "@TenHangCangDauTien", SqlDbType.NVarChar, TenHangCangDauTien);
			db.AddInParameter(dbCommand, "@SoLuongHangCangDauTien", SqlDbType.Decimal, SoLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongHangCangDauTien", SqlDbType.VarChar, DonViTinhSoLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@KhoiLuongHangCangDauTien", SqlDbType.Decimal, KhoiLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@DonViTinhKhoiLuongHangCangDauTien", SqlDbType.VarChar, DonViTinhKhoiLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@TenHangCanBoc", SqlDbType.NVarChar, TenHangCanBoc);
			db.AddInParameter(dbCommand, "@SoLuongHangCanBoc", SqlDbType.Decimal, SoLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongHangCanBoc", SqlDbType.VarChar, DonViTinhSoLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@KhoiLuongHangCanBoc", SqlDbType.Decimal, KhoiLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@DonViTinhKhoiLuongHangCanBoc", SqlDbType.VarChar, DonViTinhKhoiLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@LyDoKhongDat", SqlDbType.NVarChar, LyDoKhongDat);
			db.AddInParameter(dbCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, DaiDienDonViCapPhep);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@TenLoaiPhuongTienVanChuyen", SqlDbType.NVarChar, TenLoaiPhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_GiayPhep_SAA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SAA item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_GiayPhep_SAA(long id, string maNguoiKhai, string maBuuChinhNguoiKhai, string diaChiNguoiKhai, string maNuocNguoiKhai, string soDienThoaiNguoiKhai, string soFaxNguoiKhai, string emailNguoiKhai, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string maDonViCapPhep, string soHopDong, string maThuongNhanXuatKhau, string tenThuongNhanXuatKhau, string maBuuChinhXuatKhau, string soNhaTenDuongXuatKhau, string phuongXaXuatKhau, string quanHuyenXuatKhau, string tinhThanhPhoXuatKhau, string maQuocGiaXuatKhau, string soDienThoaiXuatKhau, string soFaxXuatKhau, string emailXuatKhau, string nuocXuatKhau, string maThuongNhanNhapKhau, string tenThuongNhanNhapKhau, string soDangKyKinhDoanh, string maBuuChinhNhapKhau, string diaChiThuongNhanNhapKhau, string maQuocGiaNhapKhau, string soDienThoaiNhapKhau, string soFaxNhapKhau, string emailNhapKhau, string nuocQuaCanh, string phuongTienVanChuyen, string maCuaKhauNhap, string tenCuaKhauNhap, string soGiayChungNhan, string diaDiemKiemDich, string benhMienDich, DateTime ngayTiemPhong, string khuTrung, string nongDo, string diaDiemNuoiTrong, DateTime ngayKiemDich, DateTime thoiGianKiemDich, string diaDiemGiamSat, DateTime ngayGiamSat, DateTime thoiGianGiamSat, decimal soBanCanCap, string vatDungKhac, string hoSoLienQuan, string noiChuyenDen, string nguoiKiemTra, string ketQuaKiemTra, string tenTau, string quocTich, string tenThuyenTruong, string tenBacSi, decimal soThuyenVien, decimal soHanhKhach, string cangRoiCuoiCung, string cangDenTiepTheo, string cangBocHangDauTien, DateTime ngayRoiCang, string tenHangCangDauTien, decimal soLuongHangCangDauTien, string donViTinhSoLuongHangCangDauTien, decimal khoiLuongHangCangDauTien, string donViTinhKhoiLuongHangCangDauTien, string tenHangCanBoc, decimal soLuongHangCanBoc, string donViTinhSoLuongHangCanBoc, decimal khoiLuongHangCanBoc, string donViTinhKhoiLuongHangCanBoc, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string notes, string inputMessageID, string messageTag, string indexTag, string lyDoKhongDat, string daiDienDonViCapPhep, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, string tenLoaiPhuongTienVanChuyen, int trangThaiXuLy, DateTime ngayKhaiBao)
		{
			KDT_VNACC_GiayPhep_SAA entity = new KDT_VNACC_GiayPhep_SAA();			
			entity.ID = id;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.MaBuuChinhNguoiKhai = maBuuChinhNguoiKhai;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.MaNuocNguoiKhai = maNuocNguoiKhai;
			entity.SoDienThoaiNguoiKhai = soDienThoaiNguoiKhai;
			entity.SoFaxNguoiKhai = soFaxNguoiKhai;
			entity.EmailNguoiKhai = emailNguoiKhai;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.MaDonViCapPhep = maDonViCapPhep;
			entity.SoHopDong = soHopDong;
			entity.MaThuongNhanXuatKhau = maThuongNhanXuatKhau;
			entity.TenThuongNhanXuatKhau = tenThuongNhanXuatKhau;
			entity.MaBuuChinhXuatKhau = maBuuChinhXuatKhau;
			entity.SoNhaTenDuongXuatKhau = soNhaTenDuongXuatKhau;
			entity.PhuongXaXuatKhau = phuongXaXuatKhau;
			entity.QuanHuyenXuatKhau = quanHuyenXuatKhau;
			entity.TinhThanhPhoXuatKhau = tinhThanhPhoXuatKhau;
			entity.MaQuocGiaXuatKhau = maQuocGiaXuatKhau;
			entity.SoDienThoaiXuatKhau = soDienThoaiXuatKhau;
			entity.SoFaxXuatKhau = soFaxXuatKhau;
			entity.EmailXuatKhau = emailXuatKhau;
			entity.NuocXuatKhau = nuocXuatKhau;
			entity.MaThuongNhanNhapKhau = maThuongNhanNhapKhau;
			entity.TenThuongNhanNhapKhau = tenThuongNhanNhapKhau;
			entity.SoDangKyKinhDoanh = soDangKyKinhDoanh;
			entity.MaBuuChinhNhapKhau = maBuuChinhNhapKhau;
			entity.DiaChiThuongNhanNhapKhau = diaChiThuongNhanNhapKhau;
			entity.MaQuocGiaNhapKhau = maQuocGiaNhapKhau;
			entity.SoDienThoaiNhapKhau = soDienThoaiNhapKhau;
			entity.SoFaxNhapKhau = soFaxNhapKhau;
			entity.EmailNhapKhau = emailNhapKhau;
			entity.NuocQuaCanh = nuocQuaCanh;
			entity.PhuongTienVanChuyen = phuongTienVanChuyen;
			entity.MaCuaKhauNhap = maCuaKhauNhap;
			entity.TenCuaKhauNhap = tenCuaKhauNhap;
			entity.SoGiayChungNhan = soGiayChungNhan;
			entity.DiaDiemKiemDich = diaDiemKiemDich;
			entity.BenhMienDich = benhMienDich;
			entity.NgayTiemPhong = ngayTiemPhong;
			entity.KhuTrung = khuTrung;
			entity.NongDo = nongDo;
			entity.DiaDiemNuoiTrong = diaDiemNuoiTrong;
			entity.NgayKiemDich = ngayKiemDich;
			entity.ThoiGianKiemDich = thoiGianKiemDich;
			entity.DiaDiemGiamSat = diaDiemGiamSat;
			entity.NgayGiamSat = ngayGiamSat;
			entity.ThoiGianGiamSat = thoiGianGiamSat;
			entity.SoBanCanCap = soBanCanCap;
			entity.VatDungKhac = vatDungKhac;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.NoiChuyenDen = noiChuyenDen;
			entity.NguoiKiemTra = nguoiKiemTra;
			entity.KetQuaKiemTra = ketQuaKiemTra;
			entity.TenTau = tenTau;
			entity.QuocTich = quocTich;
			entity.TenThuyenTruong = tenThuyenTruong;
			entity.TenBacSi = tenBacSi;
			entity.SoThuyenVien = soThuyenVien;
			entity.SoHanhKhach = soHanhKhach;
			entity.CangRoiCuoiCung = cangRoiCuoiCung;
			entity.CangDenTiepTheo = cangDenTiepTheo;
			entity.CangBocHangDauTien = cangBocHangDauTien;
			entity.NgayRoiCang = ngayRoiCang;
			entity.TenHangCangDauTien = tenHangCangDauTien;
			entity.SoLuongHangCangDauTien = soLuongHangCangDauTien;
			entity.DonViTinhSoLuongHangCangDauTien = donViTinhSoLuongHangCangDauTien;
			entity.KhoiLuongHangCangDauTien = khoiLuongHangCangDauTien;
			entity.DonViTinhKhoiLuongHangCangDauTien = donViTinhKhoiLuongHangCangDauTien;
			entity.TenHangCanBoc = tenHangCanBoc;
			entity.SoLuongHangCanBoc = soLuongHangCanBoc;
			entity.DonViTinhSoLuongHangCanBoc = donViTinhSoLuongHangCanBoc;
			entity.KhoiLuongHangCanBoc = khoiLuongHangCanBoc;
			entity.DonViTinhKhoiLuongHangCanBoc = donViTinhKhoiLuongHangCanBoc;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.LyDoKhongDat = lyDoKhongDat;
			entity.DaiDienDonViCapPhep = daiDienDonViCapPhep;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.TenLoaiPhuongTienVanChuyen = tenLoaiPhuongTienVanChuyen;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayKhaiBao = ngayKhaiBao;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_GiayPhep_SAA_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, MaBuuChinhNguoiKhai);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, MaNuocNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, SoDienThoaiNguoiKhai);
			db.AddInParameter(dbCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, SoFaxNguoiKhai);
			db.AddInParameter(dbCommand, "@EmailNguoiKhai", SqlDbType.VarChar, EmailNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@MaDonViCapPhep", SqlDbType.VarChar, MaDonViCapPhep);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, MaThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, TenThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, MaBuuChinhXuatKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, SoNhaTenDuongXuatKhau);
			db.AddInParameter(dbCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, PhuongXaXuatKhau);
			db.AddInParameter(dbCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, QuanHuyenXuatKhau);
			db.AddInParameter(dbCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, TinhThanhPhoXuatKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, MaQuocGiaXuatKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, SoDienThoaiXuatKhau);
			db.AddInParameter(dbCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, SoFaxXuatKhau);
			db.AddInParameter(dbCommand, "@EmailXuatKhau", SqlDbType.VarChar, EmailXuatKhau);
			db.AddInParameter(dbCommand, "@NuocXuatKhau", SqlDbType.VarChar, NuocXuatKhau);
			db.AddInParameter(dbCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, MaThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, TenThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@SoDangKyKinhDoanh", SqlDbType.VarChar, SoDangKyKinhDoanh);
			db.AddInParameter(dbCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, MaBuuChinhNhapKhau);
			db.AddInParameter(dbCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, DiaChiThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, MaQuocGiaNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, SoDienThoaiNhapKhau);
			db.AddInParameter(dbCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, SoFaxNhapKhau);
			db.AddInParameter(dbCommand, "@EmailNhapKhau", SqlDbType.VarChar, EmailNhapKhau);
			db.AddInParameter(dbCommand, "@NuocQuaCanh", SqlDbType.VarChar, NuocQuaCanh);
			db.AddInParameter(dbCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, PhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@MaCuaKhauNhap", SqlDbType.VarChar, MaCuaKhauNhap);
			db.AddInParameter(dbCommand, "@TenCuaKhauNhap", SqlDbType.NVarChar, TenCuaKhauNhap);
			db.AddInParameter(dbCommand, "@SoGiayChungNhan", SqlDbType.NVarChar, SoGiayChungNhan);
			db.AddInParameter(dbCommand, "@DiaDiemKiemDich", SqlDbType.NVarChar, DiaDiemKiemDich);
			db.AddInParameter(dbCommand, "@BenhMienDich", SqlDbType.NVarChar, BenhMienDich);
			db.AddInParameter(dbCommand, "@NgayTiemPhong", SqlDbType.DateTime, NgayTiemPhong.Year <= 1753 ? DBNull.Value : (object) NgayTiemPhong);
			db.AddInParameter(dbCommand, "@KhuTrung", SqlDbType.NVarChar, KhuTrung);
			db.AddInParameter(dbCommand, "@NongDo", SqlDbType.NVarChar, NongDo);
			db.AddInParameter(dbCommand, "@DiaDiemNuoiTrong", SqlDbType.NVarChar, DiaDiemNuoiTrong);
			db.AddInParameter(dbCommand, "@NgayKiemDich", SqlDbType.DateTime, NgayKiemDich.Year <= 1753 ? DBNull.Value : (object) NgayKiemDich);
			db.AddInParameter(dbCommand, "@ThoiGianKiemDich", SqlDbType.DateTime, ThoiGianKiemDich.Year <= 1753 ? DBNull.Value : (object) ThoiGianKiemDich);
			db.AddInParameter(dbCommand, "@DiaDiemGiamSat", SqlDbType.NVarChar, DiaDiemGiamSat);
			db.AddInParameter(dbCommand, "@NgayGiamSat", SqlDbType.DateTime, NgayGiamSat.Year <= 1753 ? DBNull.Value : (object) NgayGiamSat);
			db.AddInParameter(dbCommand, "@ThoiGianGiamSat", SqlDbType.DateTime, ThoiGianGiamSat.Year <= 1753 ? DBNull.Value : (object) ThoiGianGiamSat);
			db.AddInParameter(dbCommand, "@SoBanCanCap", SqlDbType.Decimal, SoBanCanCap);
			db.AddInParameter(dbCommand, "@VatDungKhac", SqlDbType.NVarChar, VatDungKhac);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@NoiChuyenDen", SqlDbType.NVarChar, NoiChuyenDen);
			db.AddInParameter(dbCommand, "@NguoiKiemTra", SqlDbType.NVarChar, NguoiKiemTra);
			db.AddInParameter(dbCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, KetQuaKiemTra);
			db.AddInParameter(dbCommand, "@TenTau", SqlDbType.NVarChar, TenTau);
			db.AddInParameter(dbCommand, "@QuocTich", SqlDbType.VarChar, QuocTich);
			db.AddInParameter(dbCommand, "@TenThuyenTruong", SqlDbType.NVarChar, TenThuyenTruong);
			db.AddInParameter(dbCommand, "@TenBacSi", SqlDbType.NVarChar, TenBacSi);
			db.AddInParameter(dbCommand, "@SoThuyenVien", SqlDbType.Decimal, SoThuyenVien);
			db.AddInParameter(dbCommand, "@SoHanhKhach", SqlDbType.Decimal, SoHanhKhach);
			db.AddInParameter(dbCommand, "@CangRoiCuoiCung", SqlDbType.NVarChar, CangRoiCuoiCung);
			db.AddInParameter(dbCommand, "@CangDenTiepTheo", SqlDbType.NVarChar, CangDenTiepTheo);
			db.AddInParameter(dbCommand, "@CangBocHangDauTien", SqlDbType.NVarChar, CangBocHangDauTien);
			db.AddInParameter(dbCommand, "@NgayRoiCang", SqlDbType.DateTime, NgayRoiCang.Year <= 1753 ? DBNull.Value : (object) NgayRoiCang);
			db.AddInParameter(dbCommand, "@TenHangCangDauTien", SqlDbType.NVarChar, TenHangCangDauTien);
			db.AddInParameter(dbCommand, "@SoLuongHangCangDauTien", SqlDbType.Decimal, SoLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongHangCangDauTien", SqlDbType.VarChar, DonViTinhSoLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@KhoiLuongHangCangDauTien", SqlDbType.Decimal, KhoiLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@DonViTinhKhoiLuongHangCangDauTien", SqlDbType.VarChar, DonViTinhKhoiLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@TenHangCanBoc", SqlDbType.NVarChar, TenHangCanBoc);
			db.AddInParameter(dbCommand, "@SoLuongHangCanBoc", SqlDbType.Decimal, SoLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongHangCanBoc", SqlDbType.VarChar, DonViTinhSoLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@KhoiLuongHangCanBoc", SqlDbType.Decimal, KhoiLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@DonViTinhKhoiLuongHangCanBoc", SqlDbType.VarChar, DonViTinhKhoiLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@LyDoKhongDat", SqlDbType.NVarChar, LyDoKhongDat);
			db.AddInParameter(dbCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, DaiDienDonViCapPhep);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@TenLoaiPhuongTienVanChuyen", SqlDbType.NVarChar, TenLoaiPhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_GiayPhep_SAA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SAA item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_GiayPhep_SAA(long id, string maNguoiKhai, string maBuuChinhNguoiKhai, string diaChiNguoiKhai, string maNuocNguoiKhai, string soDienThoaiNguoiKhai, string soFaxNguoiKhai, string emailNguoiKhai, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string maDonViCapPhep, string soHopDong, string maThuongNhanXuatKhau, string tenThuongNhanXuatKhau, string maBuuChinhXuatKhau, string soNhaTenDuongXuatKhau, string phuongXaXuatKhau, string quanHuyenXuatKhau, string tinhThanhPhoXuatKhau, string maQuocGiaXuatKhau, string soDienThoaiXuatKhau, string soFaxXuatKhau, string emailXuatKhau, string nuocXuatKhau, string maThuongNhanNhapKhau, string tenThuongNhanNhapKhau, string soDangKyKinhDoanh, string maBuuChinhNhapKhau, string diaChiThuongNhanNhapKhau, string maQuocGiaNhapKhau, string soDienThoaiNhapKhau, string soFaxNhapKhau, string emailNhapKhau, string nuocQuaCanh, string phuongTienVanChuyen, string maCuaKhauNhap, string tenCuaKhauNhap, string soGiayChungNhan, string diaDiemKiemDich, string benhMienDich, DateTime ngayTiemPhong, string khuTrung, string nongDo, string diaDiemNuoiTrong, DateTime ngayKiemDich, DateTime thoiGianKiemDich, string diaDiemGiamSat, DateTime ngayGiamSat, DateTime thoiGianGiamSat, decimal soBanCanCap, string vatDungKhac, string hoSoLienQuan, string noiChuyenDen, string nguoiKiemTra, string ketQuaKiemTra, string tenTau, string quocTich, string tenThuyenTruong, string tenBacSi, decimal soThuyenVien, decimal soHanhKhach, string cangRoiCuoiCung, string cangDenTiepTheo, string cangBocHangDauTien, DateTime ngayRoiCang, string tenHangCangDauTien, decimal soLuongHangCangDauTien, string donViTinhSoLuongHangCangDauTien, decimal khoiLuongHangCangDauTien, string donViTinhKhoiLuongHangCangDauTien, string tenHangCanBoc, decimal soLuongHangCanBoc, string donViTinhSoLuongHangCanBoc, decimal khoiLuongHangCanBoc, string donViTinhKhoiLuongHangCanBoc, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string notes, string inputMessageID, string messageTag, string indexTag, string lyDoKhongDat, string daiDienDonViCapPhep, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, string tenLoaiPhuongTienVanChuyen, int trangThaiXuLy, DateTime ngayKhaiBao)
		{
			KDT_VNACC_GiayPhep_SAA entity = new KDT_VNACC_GiayPhep_SAA();			
			entity.ID = id;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.MaBuuChinhNguoiKhai = maBuuChinhNguoiKhai;
			entity.DiaChiNguoiKhai = diaChiNguoiKhai;
			entity.MaNuocNguoiKhai = maNuocNguoiKhai;
			entity.SoDienThoaiNguoiKhai = soDienThoaiNguoiKhai;
			entity.SoFaxNguoiKhai = soFaxNguoiKhai;
			entity.EmailNguoiKhai = emailNguoiKhai;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.MaDonViCapPhep = maDonViCapPhep;
			entity.SoHopDong = soHopDong;
			entity.MaThuongNhanXuatKhau = maThuongNhanXuatKhau;
			entity.TenThuongNhanXuatKhau = tenThuongNhanXuatKhau;
			entity.MaBuuChinhXuatKhau = maBuuChinhXuatKhau;
			entity.SoNhaTenDuongXuatKhau = soNhaTenDuongXuatKhau;
			entity.PhuongXaXuatKhau = phuongXaXuatKhau;
			entity.QuanHuyenXuatKhau = quanHuyenXuatKhau;
			entity.TinhThanhPhoXuatKhau = tinhThanhPhoXuatKhau;
			entity.MaQuocGiaXuatKhau = maQuocGiaXuatKhau;
			entity.SoDienThoaiXuatKhau = soDienThoaiXuatKhau;
			entity.SoFaxXuatKhau = soFaxXuatKhau;
			entity.EmailXuatKhau = emailXuatKhau;
			entity.NuocXuatKhau = nuocXuatKhau;
			entity.MaThuongNhanNhapKhau = maThuongNhanNhapKhau;
			entity.TenThuongNhanNhapKhau = tenThuongNhanNhapKhau;
			entity.SoDangKyKinhDoanh = soDangKyKinhDoanh;
			entity.MaBuuChinhNhapKhau = maBuuChinhNhapKhau;
			entity.DiaChiThuongNhanNhapKhau = diaChiThuongNhanNhapKhau;
			entity.MaQuocGiaNhapKhau = maQuocGiaNhapKhau;
			entity.SoDienThoaiNhapKhau = soDienThoaiNhapKhau;
			entity.SoFaxNhapKhau = soFaxNhapKhau;
			entity.EmailNhapKhau = emailNhapKhau;
			entity.NuocQuaCanh = nuocQuaCanh;
			entity.PhuongTienVanChuyen = phuongTienVanChuyen;
			entity.MaCuaKhauNhap = maCuaKhauNhap;
			entity.TenCuaKhauNhap = tenCuaKhauNhap;
			entity.SoGiayChungNhan = soGiayChungNhan;
			entity.DiaDiemKiemDich = diaDiemKiemDich;
			entity.BenhMienDich = benhMienDich;
			entity.NgayTiemPhong = ngayTiemPhong;
			entity.KhuTrung = khuTrung;
			entity.NongDo = nongDo;
			entity.DiaDiemNuoiTrong = diaDiemNuoiTrong;
			entity.NgayKiemDich = ngayKiemDich;
			entity.ThoiGianKiemDich = thoiGianKiemDich;
			entity.DiaDiemGiamSat = diaDiemGiamSat;
			entity.NgayGiamSat = ngayGiamSat;
			entity.ThoiGianGiamSat = thoiGianGiamSat;
			entity.SoBanCanCap = soBanCanCap;
			entity.VatDungKhac = vatDungKhac;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.NoiChuyenDen = noiChuyenDen;
			entity.NguoiKiemTra = nguoiKiemTra;
			entity.KetQuaKiemTra = ketQuaKiemTra;
			entity.TenTau = tenTau;
			entity.QuocTich = quocTich;
			entity.TenThuyenTruong = tenThuyenTruong;
			entity.TenBacSi = tenBacSi;
			entity.SoThuyenVien = soThuyenVien;
			entity.SoHanhKhach = soHanhKhach;
			entity.CangRoiCuoiCung = cangRoiCuoiCung;
			entity.CangDenTiepTheo = cangDenTiepTheo;
			entity.CangBocHangDauTien = cangBocHangDauTien;
			entity.NgayRoiCang = ngayRoiCang;
			entity.TenHangCangDauTien = tenHangCangDauTien;
			entity.SoLuongHangCangDauTien = soLuongHangCangDauTien;
			entity.DonViTinhSoLuongHangCangDauTien = donViTinhSoLuongHangCangDauTien;
			entity.KhoiLuongHangCangDauTien = khoiLuongHangCangDauTien;
			entity.DonViTinhKhoiLuongHangCangDauTien = donViTinhKhoiLuongHangCangDauTien;
			entity.TenHangCanBoc = tenHangCanBoc;
			entity.SoLuongHangCanBoc = soLuongHangCanBoc;
			entity.DonViTinhSoLuongHangCanBoc = donViTinhSoLuongHangCanBoc;
			entity.KhoiLuongHangCanBoc = khoiLuongHangCanBoc;
			entity.DonViTinhKhoiLuongHangCanBoc = donViTinhKhoiLuongHangCanBoc;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.LyDoKhongDat = lyDoKhongDat;
			entity.DaiDienDonViCapPhep = daiDienDonViCapPhep;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.TenLoaiPhuongTienVanChuyen = tenLoaiPhuongTienVanChuyen;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.NgayKhaiBao = ngayKhaiBao;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@MaBuuChinhNguoiKhai", SqlDbType.VarChar, MaBuuChinhNguoiKhai);
			db.AddInParameter(dbCommand, "@DiaChiNguoiKhai", SqlDbType.NVarChar, DiaChiNguoiKhai);
			db.AddInParameter(dbCommand, "@MaNuocNguoiKhai", SqlDbType.VarChar, MaNuocNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDienThoaiNguoiKhai", SqlDbType.VarChar, SoDienThoaiNguoiKhai);
			db.AddInParameter(dbCommand, "@SoFaxNguoiKhai", SqlDbType.VarChar, SoFaxNguoiKhai);
			db.AddInParameter(dbCommand, "@EmailNguoiKhai", SqlDbType.VarChar, EmailNguoiKhai);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@MaDonViCapPhep", SqlDbType.VarChar, MaDonViCapPhep);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, MaThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, TenThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, MaBuuChinhXuatKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, SoNhaTenDuongXuatKhau);
			db.AddInParameter(dbCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, PhuongXaXuatKhau);
			db.AddInParameter(dbCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, QuanHuyenXuatKhau);
			db.AddInParameter(dbCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, TinhThanhPhoXuatKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, MaQuocGiaXuatKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, SoDienThoaiXuatKhau);
			db.AddInParameter(dbCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, SoFaxXuatKhau);
			db.AddInParameter(dbCommand, "@EmailXuatKhau", SqlDbType.VarChar, EmailXuatKhau);
			db.AddInParameter(dbCommand, "@NuocXuatKhau", SqlDbType.VarChar, NuocXuatKhau);
			db.AddInParameter(dbCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, MaThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, TenThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@SoDangKyKinhDoanh", SqlDbType.VarChar, SoDangKyKinhDoanh);
			db.AddInParameter(dbCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, MaBuuChinhNhapKhau);
			db.AddInParameter(dbCommand, "@DiaChiThuongNhanNhapKhau", SqlDbType.NVarChar, DiaChiThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, MaQuocGiaNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, SoDienThoaiNhapKhau);
			db.AddInParameter(dbCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, SoFaxNhapKhau);
			db.AddInParameter(dbCommand, "@EmailNhapKhau", SqlDbType.VarChar, EmailNhapKhau);
			db.AddInParameter(dbCommand, "@NuocQuaCanh", SqlDbType.VarChar, NuocQuaCanh);
			db.AddInParameter(dbCommand, "@PhuongTienVanChuyen", SqlDbType.VarChar, PhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@MaCuaKhauNhap", SqlDbType.VarChar, MaCuaKhauNhap);
			db.AddInParameter(dbCommand, "@TenCuaKhauNhap", SqlDbType.NVarChar, TenCuaKhauNhap);
			db.AddInParameter(dbCommand, "@SoGiayChungNhan", SqlDbType.NVarChar, SoGiayChungNhan);
			db.AddInParameter(dbCommand, "@DiaDiemKiemDich", SqlDbType.NVarChar, DiaDiemKiemDich);
			db.AddInParameter(dbCommand, "@BenhMienDich", SqlDbType.NVarChar, BenhMienDich);
			db.AddInParameter(dbCommand, "@NgayTiemPhong", SqlDbType.DateTime, NgayTiemPhong.Year <= 1753 ? DBNull.Value : (object) NgayTiemPhong);
			db.AddInParameter(dbCommand, "@KhuTrung", SqlDbType.NVarChar, KhuTrung);
			db.AddInParameter(dbCommand, "@NongDo", SqlDbType.NVarChar, NongDo);
			db.AddInParameter(dbCommand, "@DiaDiemNuoiTrong", SqlDbType.NVarChar, DiaDiemNuoiTrong);
			db.AddInParameter(dbCommand, "@NgayKiemDich", SqlDbType.DateTime, NgayKiemDich.Year <= 1753 ? DBNull.Value : (object) NgayKiemDich);
			db.AddInParameter(dbCommand, "@ThoiGianKiemDich", SqlDbType.DateTime, ThoiGianKiemDich.Year <= 1753 ? DBNull.Value : (object) ThoiGianKiemDich);
			db.AddInParameter(dbCommand, "@DiaDiemGiamSat", SqlDbType.NVarChar, DiaDiemGiamSat);
			db.AddInParameter(dbCommand, "@NgayGiamSat", SqlDbType.DateTime, NgayGiamSat.Year <= 1753 ? DBNull.Value : (object) NgayGiamSat);
			db.AddInParameter(dbCommand, "@ThoiGianGiamSat", SqlDbType.DateTime, ThoiGianGiamSat.Year <= 1753 ? DBNull.Value : (object) ThoiGianGiamSat);
			db.AddInParameter(dbCommand, "@SoBanCanCap", SqlDbType.Decimal, SoBanCanCap);
			db.AddInParameter(dbCommand, "@VatDungKhac", SqlDbType.NVarChar, VatDungKhac);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@NoiChuyenDen", SqlDbType.NVarChar, NoiChuyenDen);
			db.AddInParameter(dbCommand, "@NguoiKiemTra", SqlDbType.NVarChar, NguoiKiemTra);
			db.AddInParameter(dbCommand, "@KetQuaKiemTra", SqlDbType.NVarChar, KetQuaKiemTra);
			db.AddInParameter(dbCommand, "@TenTau", SqlDbType.NVarChar, TenTau);
			db.AddInParameter(dbCommand, "@QuocTich", SqlDbType.VarChar, QuocTich);
			db.AddInParameter(dbCommand, "@TenThuyenTruong", SqlDbType.NVarChar, TenThuyenTruong);
			db.AddInParameter(dbCommand, "@TenBacSi", SqlDbType.NVarChar, TenBacSi);
			db.AddInParameter(dbCommand, "@SoThuyenVien", SqlDbType.Decimal, SoThuyenVien);
			db.AddInParameter(dbCommand, "@SoHanhKhach", SqlDbType.Decimal, SoHanhKhach);
			db.AddInParameter(dbCommand, "@CangRoiCuoiCung", SqlDbType.NVarChar, CangRoiCuoiCung);
			db.AddInParameter(dbCommand, "@CangDenTiepTheo", SqlDbType.NVarChar, CangDenTiepTheo);
			db.AddInParameter(dbCommand, "@CangBocHangDauTien", SqlDbType.NVarChar, CangBocHangDauTien);
			db.AddInParameter(dbCommand, "@NgayRoiCang", SqlDbType.DateTime, NgayRoiCang.Year <= 1753 ? DBNull.Value : (object) NgayRoiCang);
			db.AddInParameter(dbCommand, "@TenHangCangDauTien", SqlDbType.NVarChar, TenHangCangDauTien);
			db.AddInParameter(dbCommand, "@SoLuongHangCangDauTien", SqlDbType.Decimal, SoLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongHangCangDauTien", SqlDbType.VarChar, DonViTinhSoLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@KhoiLuongHangCangDauTien", SqlDbType.Decimal, KhoiLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@DonViTinhKhoiLuongHangCangDauTien", SqlDbType.VarChar, DonViTinhKhoiLuongHangCangDauTien);
			db.AddInParameter(dbCommand, "@TenHangCanBoc", SqlDbType.NVarChar, TenHangCanBoc);
			db.AddInParameter(dbCommand, "@SoLuongHangCanBoc", SqlDbType.Decimal, SoLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongHangCanBoc", SqlDbType.VarChar, DonViTinhSoLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@KhoiLuongHangCanBoc", SqlDbType.Decimal, KhoiLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@DonViTinhKhoiLuongHangCanBoc", SqlDbType.VarChar, DonViTinhKhoiLuongHangCanBoc);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@LyDoKhongDat", SqlDbType.NVarChar, LyDoKhongDat);
			db.AddInParameter(dbCommand, "@DaiDienDonViCapPhep", SqlDbType.NVarChar, DaiDienDonViCapPhep);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@TenLoaiPhuongTienVanChuyen", SqlDbType.NVarChar, TenLoaiPhuongTienVanChuyen);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_GiayPhep_SAA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SAA item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_GiayPhep_SAA(long id)
		{
			KDT_VNACC_GiayPhep_SAA entity = new KDT_VNACC_GiayPhep_SAA();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SAA_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_GiayPhep_SAA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SAA item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}