﻿using System;
using System.Drawing;
using Company.BLL.DuLieuChuan;
using Company.BLL.SXXK.ToKhai ;
using Company.BLL.SXXK.ThanhKhoan;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.BLL;
using Company.BLL.Utils;

namespace Company.Interface
{
    public partial class LoaiHinhKhacForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        private decimal clg;
        private decimal dgnt;
      
        public HangMauDichCollection HMDCollection = new HangMauDichCollection();       

        public string LoaiHangHoa;
        public ToKhaiMauDich TKMD;
        private decimal luong;

        public string MaNguyenTe;
        public string MaNuocXX;
        public string NhomLoaiHinh = string.Empty;
        private decimal st_clg;
        private bool edit = false;
        //-----------------------------------------------------------------------------------------				
        private decimal tgnt;
        private decimal tgtt_gtgt;

        private decimal tgtt_nk;
        private decimal tgtt_ttdb;
        private decimal tl_clg;
        private decimal tongTGKB;
        private decimal ts_gtgt;

        private decimal ts_nk;
        private decimal ts_ttdb;
        private decimal tt_gtgt;

        private decimal tt_nk;
        private decimal tt_ttdb;
        public decimal TyGiaTT;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;
        //-----------------------------------------------------------------------------------------

        public LoaiHinhKhacForm()
        {
            InitializeComponent();
        }

        private void khoitao_GiaoDien()
        {
            //if (this.NhomLoaiHinh.Substring(1, 2) == "GC" || this.NhomLoaiHinh.Substring(1, 2) == "SX")
            //{
               txtMaHang.ButtonStyle = EditButtonStyle.Ellipsis;
            //   cbDonViTinh.ReadOnly = true;
            //   txtMaHang.BackColor = txtTenHang.BackColor = cbDonViTinh.BackColor = Color.FromArgb(255, 255, 192);
            //}
        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
        }

        private decimal tinhthue()
        {
            this.tgnt = this.dgnt * this.luong;
            if (edit) this.tgtt_nk = Convert.ToDecimal(txtTGTT_NK.Value);
            else this.tgtt_nk = this.tgnt * this.TyGiaTT;

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;
            
            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk,MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = this.tt_ttdb;
            txtTienThue_GTGT.Value = this.tt_gtgt;
            txtTien_CLG.Value = this.st_clg;

            txtTongSoTienThue.Value = this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg;

            return this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg;
        }


       
        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich hmd in HMDCollection)
            {
                if (hmd.MaPhu.Trim() == maHang) return true;
            }
            return false;
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (txtMaHang.Text == "")
            {
                //epError.SetError(txtMaHang,"Mã Hàng không được rỗng");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    epError.SetError(txtMaHang, "Mã Hàng không được rỗng");
                }
                else
                {
                    epError.SetError(txtMaHang, "Code good is empty");
                }
                epError.SetIconPadding(txtMaHang, -8);
                return;
            }
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);
                //epError.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    epError.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                }
                else
                {
                    epError.SetError(txtMaHS, "HS code is invalid");
                }
                return;
            }
          
            if (checkMaHangExit(txtMaHang.Text))
            {
                //ShowMessage("Mặt hàng này đã có rồi, bạn hãy chọn mặt hàng khác.", false);
                MLMessages("Mặt hàng này đã có rồi, bạn hãy chọn mặt hàng khác.", "MSG_PUB10", "", false);
                return;
            }
         
            this.ThemHang();
        }

        //them hang :
        private void ThemHang()
        {
            HangMauDich hmd = new HangMauDich();
            hmd.SoThuTuHang = 0;
            hmd.MaHS = txtMaHS.Text;
            hmd.MaPhu = txtMaHang.Text;
            hmd.TenHang = txtTenHang.Text;
            hmd.NuocXX_ID = ctrNuocXX.Ma;
            hmd.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            hmd.SoLuong = Convert.ToDecimal(txtLuong.Value);
            hmd.DonGiaKB = Convert.ToDecimal(txtDGNT.Value);
            hmd.TriGiaKB = Convert.ToDecimal(txtTGNT.Value);
            hmd.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Value);
            hmd.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Value);
            hmd.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Value);
            hmd.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Value);
            hmd.TyLeThuKhac = Convert.ToDecimal(txtTL_CLG.Value);
            hmd.ThueXNK = Convert.ToDecimal(txtTienThue_NK.Value);
            hmd.ThueTTDB = Convert.ToDecimal(txtTienThue_TTDB.Value);
            hmd.ThueGTGT = Convert.ToDecimal(txtTienThue_GTGT.Value);
            hmd.Ton = Convert.ToDecimal(txtLuong.Value); 
            if (chkMienThue.Checked)
                hmd.MienThue = 1;
            else
                hmd.MienThue = 0;

            // Tính thuế.
            hmd.TinhThue(this.TyGiaTT);
            this.HMDCollection.Add(hmd);          

            this.tongTGKB += Convert.ToDecimal(txtTGNT.Value);
            if (GlobalSettings.NGON_NGU == "0")
            {
                lblTongTGKB.Text = string.Format("Tổng trị giá khai báo: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
            }
            else
            {
                lblTongTGKB.Text = string.Format("Total of value declaration: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
            }
            this.refresh_STTHang();
            dgList.DataSource = this.HMDCollection;
            dgList.Refetch();
            txtMaHang.Text = "";
            txtTenHang.Text = "";
            txtMaHS.Text = "";
            txtLuong.Value = 0;
            txtDGNT.Value = 0;
            txtTGTT_NK.Value = 0;
            this.tinhthue();
            txtTGNT.Value = 0;
            txtTriGiaKB.Value = 0;
            this.edit = false;
            txtTGTT_NK.Value = 0;
            txtTS_NK.Value = 0;
            epError.Clear();
            txtMaHang.Focus();
            

        }
        //-----------------------------------------------------------------------------------------
        #region  txtMaNPL_Leave
        //private void txtMaNPL_Leave(object sender, EventArgs e)
        //{
            //this.fillNguyenPhuLieu(txtMaNPL.Text);
        //    if (LoaiHangHoa == "N")
        //    {
        //        Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
        //        npl.Ma = txtMaHang.Text.Trim();
        //        npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
        //        npl.MaHaiQuan = MaHaiQuan;
        //        if (npl.Load())
        //        {
        //            txtMaHang.Text = npl.Ma;
        //            txtMaHS.Text = npl.MaHS;
        //            txtTenHang.Text = npl.Ten;
        //            cbDonViTinh.SelectedValue = npl.DVT_ID;
        //        }
        //        else
        //        {
        //            epError.SetIconPadding(txtMaHang, -8);
        //            epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");

        //            txtTenHang.Text = txtMaHS.Text = string.Empty;
        //            cbDonViTinh.SelectedValue = "";
        //        }
        //    }
        //    else
        //    {
        //        Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
        //        sp.Ma = txtMaHang.Text.Trim();
        //        sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
        //        sp.MaHaiQuan = MaHaiQuan;
        //        if (sp.Load())
        //        {
        //            txtMaHang.Text = sp.Ma;
        //            txtMaHS.Text = sp.MaHS;
        //            txtTenHang.Text = sp.Ten;
        //            cbDonViTinh.SelectedValue = sp.DVT_ID;
        //        }
        //        else
        //        {
        //            epError.SetIconPadding(txtMaHang, -8);
        //            epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
        //            txtTenHang.Text = txtMaHS.Text = string.Empty;
        //            cbDonViTinh.SelectedValue = "";
        //        }
        //    }
        //}
        #endregion 
        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();

            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.MaNguyenTe + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.MaNguyenTe);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.MaNguyenTe);

            this.tongTGKB = 0;
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                // Tính lại thuế.
                hmd.TinhThue(this.TyGiaTT);
                // Tổng trị giá khai báo.
                this.tongTGKB += hmd.TriGiaKB;
            }
            dgList.DataSource = this.HMDCollection;

            if (GlobalSettings.NGON_NGU == "0")
            {
                lblTongTGKB.Text = string.Format("Tổng trị giá khai báo: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                lblTyGiaTT.Text = "Tỷ giá tính thuế: " + this.TyGiaTT.ToString("N");
            }
            else
            {
                lblTongTGKB.Text = string.Format("Total of value declaration: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                lblTyGiaTT.Text = "Tax rate: " + this.TyGiaTT.ToString("N");
            }

            
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
          try{
                this.luong = Convert.ToDecimal(txtLuong.Value);
                this.edit = false;
                this.tinhthue();
                epError.SetError(txtDGNT, null);
                epError.SetError(txtLuong, null);
            }
            catch
            {
                epError.SetError(txtDGNT, setText("Dữ liệu không hợp lệ", "Invalid input value"));
                epError.SetError(txtLuong, setText("Dữ liệu không hợp lệ", "Invalid input value"));
            }
            
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            try
            {
                this.dgnt = Convert.ToDecimal(txtDGNT.Value);
                this.edit = false;
                this.tinhthue();
                epError.SetError(txtDGNT, null);
                epError.SetError(txtLuong, null);
            }catch
            {
                epError.SetError(txtDGNT,setText("Dữ liệu không hợp lệ","Invalid input value"));
                epError.SetError(txtLuong,setText("Dữ liệu không hợp lệ", "Invalid input value"));
            }
            
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            this.tinhthue();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            this.tinhthue();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            this.tinhthue();
        }

        private void txtTS_CLG_Leave(object sender, EventArgs e)
        {
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;
            this.tinhthue();
        }

        //-----------------------------------------------------------------------------------------------
        private void refresh_STTHang()
        {
            int i = 1;
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                hmd.SoThuTuHang = (short)i++;
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
        }

        private void chkMienThue_CheckedChanged(object sender, EventArgs e)
        {
            grbThue.Enabled = !chkMienThue.Checked;
            txtTS_NK.Value = 0;
            txtTS_TTDB.Value = 0;
            txtTS_GTGT.Value = 0;
            txtTL_CLG.Value = 0;
            this.tinhthue();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #region comment
        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            switch (this.NhomLoaiHinh)
            {

                case "N":
                    NguyenPhuLieuRegistedForm f = new NguyenPhuLieuRegistedForm();
                    f.CalledForm = "HangMauDichForm";
                    f.MaHaiQuan = this.MaHaiQuan.Trim();
                    f.ShowDialog();
                    if (f.NguyenPhuLieuSelected.Ma != "")
                    {
                        txtMaHang.Text = f.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = f.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = f.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = f.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                    }
                    break;
                case "X":
                    if (this.LoaiHangHoa == "N")
                    {
                        NguyenPhuLieuRegistedForm fNPLX = new NguyenPhuLieuRegistedForm();
                        fNPLX.CalledForm = "HangMauDichForm";
                        fNPLX.MaHaiQuan = this.MaHaiQuan.Trim();
                        fNPLX.ShowDialog();
                        if (fNPLX.NguyenPhuLieuSelected.Ma != "")
                        {
                            txtMaHang.Text = fNPLX.NguyenPhuLieuSelected.Ma;
                            txtTenHang.Text = fNPLX.NguyenPhuLieuSelected.Ten;
                            txtMaHS.Text = fNPLX.NguyenPhuLieuSelected.MaHS;
                            cbDonViTinh.SelectedValue = fNPLX.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                        }
                    }
                    else
                    {
                        SanPhamRegistedForm fSPX = new SanPhamRegistedForm();
                        fSPX.CalledForm = "HangMauDichForm";
                        fSPX.MaHaiQuan = this.MaHaiQuan.Trim();
                        fSPX.ShowDialog();
                        if (fSPX.SanPhamSelected.Ma != "")
                        {
                            txtMaHang.Text = fSPX.SanPhamSelected.Ma;
                            txtTenHang.Text = fSPX.SanPhamSelected.Ten;
                            txtMaHS.Text = fSPX.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = fSPX.SanPhamSelected.DVT_ID.PadRight(3);
                        }
                    }
                    break;
                //case "N":
                //    if (this.LoaiHangHoa == "N")
                //    {
                //        NguyenPhuLieu npl = new NguyenPhuLieu();
                //        npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //        npl.MaHaiQuan = "temp";
                //        //npl.NgayKy = "temp";
                //        npl.SoHopDong = "temp";
                //        NguyenPhuLieuRegistedGCForm f1 = new NguyenPhuLieuRegistedGCForm();
                //        f1.NguyenPhuLieuSelected = npl;
                //        f1.ShowDialog();
                //        if (f1.NguyenPhuLieuSelected.Ma != "")
                //        {
                //            txtMaHang.Text = npl.Ma;
                //            txtTenHang.Text = npl.Ten;
                //            txtMaHS.Text = npl.MaHS;
                //            cbDonViTinh.SelectedValue = npl.DVT_ID;
                //        }
                //    }
                //    else
                //    {
                //        ThietBi tb = new ThietBi();
                //        tb.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //        tb.MaHaiQuan = this.MaHaiQuan.Trim() ;
                //        // tb.NgayKy = "temp";
                //        //tb.SoHopDong = ;
                //        ThietBiRegistedForm ftb = new ThietBiRegistedForm();
                //        ftb.ThietBiSelected = tb;
                //        ftb.ShowDialog();
                //        if (ftb.ThietBiSelected.Ma != "")
                //        {
                //            txtMaHang.Text = tb.Ma;
                //            txtTenHang.Text = tb.Ten;
                //            txtMaHS.Text = tb.MaHS;
                //            cbDonViTinh.SelectedValue = tb.DVT_ID;
                //        }
                //    }
                //    break;
                //case "X":
                //    if (this.LoaiHangHoa == "N")
                //    {
                //        NguyenPhuLieu npl = new NguyenPhuLieu();
                //        npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //        npl.MaHaiQuan = "temp";
                //        // npl.NgayKy = "temp";
                //        npl.SoHopDong = "temp";
                //        NguyenPhuLieuRegistedGCForm f2 = new NguyenPhuLieuRegistedGCForm();
                //        f2.NguyenPhuLieuSelected = npl;
                //        f2.ShowDialog();
                //        if (f2.NguyenPhuLieuSelected.Ma != "")
                //        {
                //            txtMaHang.Text = npl.Ma;
                //            txtTenHang.Text = npl.Ten;
                //            txtMaHS.Text = npl.MaHS;
                //            cbDonViTinh.SelectedValue = npl.DVT_ID;
                //        }
                //    }
                //    else if (LoaiHangHoa == "S")
                //    {
                //        SanPham sp = new SanPham();
                //        sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //        sp.MaHaiQuan = this.MaHaiQuan.Trim() ;
                //        //sp.NgayKy;
                //        sp.SoHopDong = "temp";
                //        SanPhamRegistedGCForm f3 = new SanPhamRegistedGCForm();
                //        f3.SanPhamSelected = sp;
                //        f3.ShowDialog();
                //        if (f3.SanPhamSelected.Ma != "")
                //        {
                //            txtMaHang.Text = sp.Ma;
                //            txtTenHang.Text = sp.Ten;
                //            txtMaHS.Text = sp.MaHS;
                //            cbDonViTinh.SelectedValue = sp.DVT_ID;
                //        }
                //    }
                //    else
                //    {
                //        ThietBi tb = new ThietBi();
                //        tb.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //        tb.MaHaiQuan = "temp";
                //        // tb.NgayKy = HD.NgayKy;
                //        tb.SoHopDong = "temp";
                //        ThietBiRegistedForm ftb = new ThietBiRegistedForm();
                //        ftb.ThietBiSelected = tb;
                //        ftb.ShowDialog();
                //        if (ftb.ThietBiSelected.Ma != "")
                //        {
                //            txtMaHang.Text = tb.Ma;
                //            txtTenHang.Text = tb.Ten;
                //            txtMaHS.Text = tb.MaHS;
                //            cbDonViTinh.SelectedValue = tb.DVT_ID;
                //        }
                //    }
                //    break;
            }
        }
        #endregion comment
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    LoaiHinhKhacModifyForm f = new LoaiHinhKhacModifyForm();
                    f.HMD = this.HMDCollection[i.Position];

                //    f.HD = this.HD;
                    f.NhomLoaiHinh = this.NhomLoaiHinh;
                    f.LoaiHangHoa = this.LoaiHangHoa;
                    f.MaHaiQuan = this.MaHaiQuan.Trim();
                    f.MaNguyenTe = this.MaNguyenTe;
                    f.TyGiaTT = this.TyGiaTT;
                    f.collection = this.HMDCollection;
                    f.ShowDialog();
                   if (f.IsEdited)
                    {
                      if (f.HMD.SoThuTuHang  > 0)
                            f.HMD.Update();
                       else
                            this.HMDCollection[i.Position] = f.HMD;
                    }
                    else if (f.IsDeleted)
                   {
                       if (f.HMD.SoThuTuHang > 0)
                          f.HMD.Delete();
                      this.HMDCollection.RemoveAt(i.Position);
                  }
                  dgList.DataSource = this.HMDCollection;
                 try
                    {
                      dgList.Refetch();
                   }
                   catch { dgList.DataSource = this.HMDCollection; }
                   this.tongTGKB = 0;
                   foreach (HangMauDich hmd in this.HMDCollection)
                   {
                       // Tính lại thuế.
                       hmd.TinhThue(this.TyGiaTT);
                       // Tổng trị giá khai báo.
                       this.tongTGKB += hmd.TriGiaKB;
                   }
                   if (GlobalSettings.NGON_NGU == "0")
                   {
                       lblTongTGKB.Text = string.Format("Tổng trị giá khai báo: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                   }
                   else
                   {
                       lblTongTGKB.Text = string.Format("Total of value declaration: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                   }
                  
                
                }
                break;
            }
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                //if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa hàng hóa này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    HangMauDich hmd = (HangMauDich)e.Row.DataRow;
                    //if (hmd.ID > 0)
                    //{
                    //    hmd.Delete();
                    //}
                    this.tongTGKB -= hmd.TriGiaKB;
                    if (GlobalSettings.NGON_NGU == "0")
                    {
                        lblTongTGKB.Text = string.Format("Tổng trị giá khai báo: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                    }
                    else
                    {
                        lblTongTGKB.Text = string.Format("Total of value declaration: {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (true)
            {
                if (this.TKMD.TrangThaiThanhKhoan =="")
                {
                    //ShowMessage("Tờ khai đã được duyệt không được sửa", false);
                    MLMessages("Tờ khai đã được duyệt không được sửa", "MSG_SEN12", "", false);
                    e.Cancel = true;
                    return;
                }
                //if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                if (MLMessages("Bạn có muốn xóa hàng hóa này không?", "MSG_DEL01", "", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;
                            //if (hmd.ID > 0)
                            //{
                            //    hmd.Delete();
                            //}
                        }
                    }
                    if (TKMD.TrangThaiThanhKhoan != "")
                    {
                        this.TKMD.Load();
                       // this.TKMD.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        this.TKMD.Update();
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void uiGroupBox2_Click(object sender, EventArgs e)
        {

        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            this.tinhthue();
        }

        private void txtTGTT_NK_TextChanged(object sender, EventArgs e)
        {
            this.edit = true;
        }

        private void uiGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void txtMaHang_Leave(object sender, EventArgs e)
        {
            if (this.TKMD.MaLoaiHinh.Contains("SX"))
            {
                if (LoaiHangHoa == "N")
                {
                    Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
                    npl.Ma = txtMaHang.Text.Trim();
                    npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    npl.MaHaiQuan = MaHaiQuan;
                    if (npl.Load())
                    {
                        txtMaHang.Text = npl.Ma;
                        txtMaHS.Text = npl.MaHS;
                        txtTenHang.Text = npl.Ten;
                        cbDonViTinh.SelectedValue = npl.DVT_ID;
                        epError.SetError(txtMaHang,null);
                    }
                    else
                    {
                        epError.SetIconPadding(txtMaHang, -8);
                        //epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            epError.SetError(txtMaHang, "Không tồn tại nguyên phụ liệu này.");
                        }
                        else
                        {
                            epError.SetError(txtMaHang, "This material does not exist.");
                        }

                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                }
                else
                {
                    Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                    sp.Ma = txtMaHang.Text.Trim();
                    sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    sp.MaHaiQuan = MaHaiQuan;
                    if (sp.Load())
                    {
                        txtMaHang.Text = sp.Ma;
                        txtMaHS.Text = sp.MaHS;
                        txtTenHang.Text = sp.Ten;
                        cbDonViTinh.SelectedValue = sp.DVT_ID;
                        epError.SetError(txtMaHang, null);
                    }
                    else
                    {
                        epError.SetIconPadding(txtMaHang, -8);
                       // epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                        if (GlobalSettings.NGON_NGU == "0")
                        {
                            epError.SetError(txtMaHang, "Không tồn tại sản phẩm này.");
                        }
                        else
                        {
                            epError.SetError(txtMaHang, "This product does not exist.");
                        }
                        txtTenHang.Text = txtMaHS.Text = string.Empty;
                        cbDonViTinh.SelectedValue = "";
                    }
                }
            }
        }

    }
}
