﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using Company.GC.BLL;
using Company.GC.BLL.KDT;
namespace Company.Interface.PhongKhai
{
    public partial class LoadDongBoDuLieuForm : BaseForm
    {
        public LoadDongBoDuLieuForm()
        {
            InitializeComponent();
        }
        public static bool chkErrConn = false;
        public int Step = 1;
        private int value = 0;

        #region KHAIDT
        public void UpdateHMDKDT(string MaHaiQuan, string MaDN)
        {
            try
            {
                //ToKhaiMauDich tkmd=new ToKhaiMauDich();
                ToKhaiMauDich.DongBoDuLieuKhaiDT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "KhaiDT");
            }
            catch (Exception ex)
            {
                ShowMessage(" Lỗi : " + ex.Message, false);
            }
        }
        #endregion


        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            if (ShowMessage("Bạn có chắc chắn là cập nhật không ?", true) == "Yes")
            {
                if (txtDatabase.Text.Trim() == "SXXK")
                {
                    lblTrangthai.Text = "Đang đồng bộ dữ liệu .....";
                    backgroundWorker1.RunWorkerAsync(lblTrangthai);


                }
            }
        }



        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void DongBoDuLieuByDNForm_Load(object sender, EventArgs e)
        {
            timer1.Start();
            lblTrangthai.Text = " ";
            txtMaHQ.Text = GlobalSettings.MA_HAI_QUAN;
            txtMaDN.Text = GlobalSettings.MA_DON_VI;
            txtTenDN.Text = GlobalSettings.TEN_DON_VI;
            txtDatabase.Text = "KhaiDT";
            //
            if (txtDatabase.Text.Trim() == "KhaiDT")
            {
                lblTrangthai.Text = "Đang đồng bộ dữ liệu .....";
                backgroundWorker1.RunWorkerAsync(lblTrangthai);
            }

        }
        private void DongBoDanhMucLoaiSPGC()
        {
            DataSet ds = Company.KDT.SHARE.Components.DuLieuChuan.NhomSanPham.SelectAll("SLXNK");
            Company.KDT.SHARE.Components.DuLieuChuan.NhomSanPham nhomSP = new Company.KDT.SHARE.Components.DuLieuChuan.NhomSanPham();
            nhomSP.InsertUpdate(ds.Tables[0]);
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                DongBoDanhMucLoaiSPGC();
                Company.GC.BLL.KDT.GC.HopDong.DongBoDuLieuDaDuyet(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "SLXNK");
                Company.GC.BLL.KDT.GC.HopDong.DongBoDuLieuKhaiDT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "KhaiDT");
                //Company.GC.BLL.KDT.GC.PhuKienDangKy.DongBoDuLieuSLNXK(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "SLXNK");
                try
                {
                    Company.GC.BLL.KDT.GC.PhuKienDangKy.DongBoDuLieuKhaiDT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "KhaiDT");
                }
                catch
                { }
                Company.GC.BLL.KDT.GC.DinhMucDangKy.DongBoDuLieuKhaiDT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "KhaiDT");
                Company.GC.BLL.KDT.ToKhaiMauDich.DongBoDuLieuKhaiDT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "KhaiDT");
                Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep.DongBoDuLieuKhaiDT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "KhaiDT");
                Company.GC.BLL.KDT.GC.BKCungUngDangKy.DongBoDuLieuKhaiDT(GlobalSettings.MA_DON_VI, GlobalSettings.MA_HAI_QUAN, "KhaiDT");

                //UpdateHMDKDT(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());            
                //updateSPSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                if (chkErrConn == true)
                    ShowMessage("Không kết nối được với hệ thống chi cục Hải quan, đồng bộ dữ liệu không thành công", false);
                //updateDMSXXK(txtMaHQ.Text.Trim(), txtMaDN.Text.Trim());
                else
                    Step = 10;
            }
            catch(Exception ex)
            {                
                ShowMessage(ex.Message,false);
                Step = 10;
            }
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //lblTrangthai.Text = "Đã hoàn thành việc đồng bộ dữ liệu từ hệ thống Hải quan.";
            //lblTrangthai.Text = "Đang đồng bộ dữ liệu từ hệ thống Hải quan...";
            //timer1.Start();
            this.value = 300;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //if (uiProgressBar1.Value < 250)
            //    uiProgressBar1.Value++;
            //else
            //    this.Close();

            //
            if (uiProgressBar1.Value == 300) this.Close();
            if (this.value < 299)
            {
                this.value += Step;
                if (value > 299)
                    value = 300;
            }
            else
            {
                if (uiProgressBar1.Value < 299) this.value = uiProgressBar1.Value + 1;
            }
            uiProgressBar1.Value = this.value;

        }

        private void uiProgressBar1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void LoadDongBoDuLieuForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (uiProgressBar1.Value < 300) e.Cancel = true;
        }
    }
}

