using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.Skins;
using Activation.Server;
using Licenses_v2.Module;
using System.Configuration;

namespace Licenses_v2
{
    public partial class FrmMain : DevExpress.XtraEditors.XtraForm
    {
        public FrmMain()
        {
            InitializeComponent();
            this.LoadSkin();
        }

        private void LoadSkin()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            skinLicense.LookAndFeel.SkinName = config.AppSettings.Settings["Skin"].Value.ToString();
            if (config.AppSettings.Settings["NavBarStyle"].Value.ToString() == "NavigationPane")
                navBarLicenses.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.NavigationPane;
            else navBarLicenses.PaintStyleKind = DevExpress.XtraNavBar.NavBarViewKind.Default;
        }

        private void navCustNew_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("Cust_New"))
            {
                FrmCustomerInfo obj = new FrmCustomerInfo(null);
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Cust_New"].Activate();
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            this.barStaticTimer.Caption = DateTime.Now.ToString();
        }

        private void navCustManager_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("FrmCustomerManager"))
            {
                FrmCustomerManager obj = new FrmCustomerManager();
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["FrmCustomerManager"].Activate();
            }
        }

        private void navProdNew_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("Prod_New"))
            {
                FrmProductInfo obj = new FrmProductInfo(null);
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Prod_New"].Activate();
            }
        }

        private void navProdManager_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("FrmProductManager"))
            {
                FrmProductManager obj = new FrmProductManager();
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["FrmProductManager"].Activate();
            }
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            barStaticAction.Caption = "Mã nhân viên đăng nhập: " + Program.LoginID;

            this.WindowState = FormWindowState.Maximized;
        }

        private void navLicNew_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("Lic_New"))
            {
                FrmLicenseInfo obj = new FrmLicenseInfo(null);
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Lic_New"].Activate();
            }
        }

        private void navLicManager_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("FrmLicenseManager"))
            {
                FrmLicenseManager obj = new FrmLicenseManager();
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["FrmLicenseManager"].Activate();
            }
        }

        private void navLicNewSerial_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("Ser_New"))
            {
                FrmSerialInfo obj = new FrmSerialInfo(null);
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Ser_New"].Activate();
            }
        }

        private void navLicSerManager_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("FrmSerialManager"))
            {
                FrmSerialManager obj = new FrmSerialManager();
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["FrmSerialManager"].Activate();
            }
        }

        private void navInvNew_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("Inv_New"))
            {
                FrmInvoiceInfo obj = new FrmInvoiceInfo(null);
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Inv_New"].Activate();
            }
        }

        private void navInvManager_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("FrmInvoiceManager"))
            {
                FrmInvoiceManager obj = new FrmInvoiceManager();
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["FrmInvoiceManager"].Activate();
            }
        }

        private void navSysEmpl_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("Empl_New"))
            {
                FrmEmployeeInfo obj = new FrmEmployeeInfo(null);
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Empl_New"].Activate();
            }
        }

        private void navSysInfoEmpl_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("FrmEmployeeManager"))
            {
                FrmEmployeeManager obj = new FrmEmployeeManager();
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["FrmEmployeeManager"].Activate();
            }
        }

        private void navSysConfig_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("FrmConfig"))
            {
                FrmConfig obj = new FrmConfig();
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["FrmConfig"].Activate();
            }
        }

        private void navSysGroupEmpl_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("Group_New"))
            {
                FrmGroupEmployeeInfo obj = new FrmGroupEmployeeInfo(null);
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["Group_New"].Activate();
            }
        }

        private void navSysGroupManager_LinkClicked(object sender, DevExpress.XtraNavBar.NavBarLinkEventArgs e)
        {
            if (!ClassLib.IsFormShowed("FrmGroupEmployeeManager"))
            {
                FrmGroupEmployeeManager obj = new FrmGroupEmployeeManager();
                obj.MdiParent = this;
                obj.Show();
            }
            else
            {
                Application.OpenForms["FrmGroupEmployeeManager"].Activate();
            }
        }
    }
}