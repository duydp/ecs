﻿namespace Company.Interface
{
    partial class ThongkeHangTonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListToKhai_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListSanPham_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListSPTON_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dglistTKTon_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThongkeHangTonForm));
            Janus.Windows.GridEX.GridEXLayout dgListNPLTK_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListPhanBo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTKChuaPhanBo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgToKhaiXuatDaPhanBo_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgTKX_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uicmdThongke = new Janus.Windows.EditControls.UIButton();
            this.uicmdSelectTK = new Janus.Windows.EditControls.UIButton();
            this.uiTPSanPham = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgListToKhai = new Janus.Windows.GridEX.GridEX();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.dgListSanPham = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton7 = new Janus.Windows.EditControls.UIButton();
            this.uiButton6 = new Janus.Windows.EditControls.UIButton();
            this.dgListSPTON = new Janus.Windows.GridEX.GridEX();
            this.uiButton5 = new Janus.Windows.EditControls.UIButton();
            this.dglistTKTon = new Janus.Windows.GridEX.GridEX();
            this.uiTabSanPham = new Janus.Windows.UI.Tab.UITab();
            this.tabQLNPLTon = new Janus.Windows.UI.Tab.UITabPage();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.filterEditor1 = new Janus.Windows.FilterEditor.FilterEditor();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiButton4 = new Janus.Windows.EditControls.UIButton();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenChuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnTimKiemTK = new Janus.Windows.EditControls.UIButton();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtNamTiepNhan = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.dgListNPLTK = new Janus.Windows.GridEX.GridEX();
            this.uiTabPage3 = new Janus.Windows.UI.Tab.UITabPage();
            this.cmdPrint = new Janus.Windows.EditControls.UIButton();
            this.uiButton9 = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.filterEditor2 = new Janus.Windows.FilterEditor.FilterEditor();
            this.dgListPhanBo = new Janus.Windows.GridEX.GridEX();
            this.loaiHinhMauDichHControl1 = new Company.Interface.Controls.LoaiHinhMauDichHControl();
            this.label10 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cbSanPham = new Janus.Windows.EditControls.UIComboBox();
            this.btnTheoDoiPB = new Janus.Windows.EditControls.UIButton();
            this.txSoTKXuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNamDKXuat = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiTabPage4 = new Janus.Windows.UI.Tab.UITabPage();
            this.uiButton10 = new Janus.Windows.EditControls.UIButton();
            this.dgListTKChuaPhanBo = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.dgToKhaiXuatDaPhanBo = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrDonViHaiQuan = new Company.Interface.Controls.DonViHaiQuanControl();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.ccToDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label7 = new System.Windows.Forms.Label();
            this.ccFromDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label6 = new System.Windows.Forms.Label();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnChonTK = new Janus.Windows.EditControls.UIButton();
            this.dgTKX = new Janus.Windows.GridEX.GridEX();
            this.gridEXPrintDocument1 = new Janus.Windows.GridEX.GridEXPrintDocument();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.uiTPSanPham.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListToKhai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSanPham)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPTON)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dglistTKTon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTabSanPham)).BeginInit();
            this.uiTabSanPham.SuspendLayout();
            this.tabQLNPLTon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.uiTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLTK)).BeginInit();
            this.uiTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListPhanBo)).BeginInit();
            this.uiTabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKChuaPhanBo)).BeginInit();
            this.uiTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhaiXuatDaPhanBo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.grbMain.Controls.Add(this.uiTabSanPham);
            this.grbMain.Size = new System.Drawing.Size(1135, 629);
            // 
            // vsmMain
            // 
            this.vsmMain.DefaultColorScheme = "Office2003";
            // 
            // uicmdThongke
            // 
            this.uicmdThongke.Location = new System.Drawing.Point(698, 27);
            this.uicmdThongke.Name = "uicmdThongke";
            this.uicmdThongke.Size = new System.Drawing.Size(84, 40);
            this.uicmdThongke.TabIndex = 196;
            this.uicmdThongke.Text = "Thống kê";
            this.uicmdThongke.VisualStyleManager = this.vsmMain;
            // 
            // uicmdSelectTK
            // 
            this.uicmdSelectTK.Location = new System.Drawing.Point(592, 27);
            this.uicmdSelectTK.Name = "uicmdSelectTK";
            this.uicmdSelectTK.Size = new System.Drawing.Size(84, 40);
            this.uicmdSelectTK.TabIndex = 195;
            this.uicmdSelectTK.Text = "Chọn tờ khai";
            this.uicmdSelectTK.VisualStyleManager = this.vsmMain;
            // 
            // uiTPSanPham
            // 
            this.uiTPSanPham.Controls.Add(this.uiGroupBox1);
            this.uiTPSanPham.Controls.Add(this.uiGroupBox2);
            this.uiTPSanPham.Key = "KiemTraLuongXuat";
            this.uiTPSanPham.Location = new System.Drawing.Point(1, 21);
            this.uiTPSanPham.Name = "uiTPSanPham";
            this.uiTPSanPham.Size = new System.Drawing.Size(1133, 607);
            this.uiTPSanPham.TabStop = true;
            this.uiTPSanPham.Text = "Kiểm tra lượng  sản phẩm xuất";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.BorderColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.dgListToKhai);
            this.uiGroupBox1.Controls.Add(this.btnDelete);
            this.uiGroupBox1.Controls.Add(this.dgListSanPham);
            this.uiGroupBox1.Location = new System.Drawing.Point(11, 1);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1111, 168);
            this.uiGroupBox1.TabIndex = 0;
            // 
            // dgListToKhai
            // 
            this.dgListToKhai.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListToKhai.AlternatingColors = true;
            this.dgListToKhai.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListToKhai.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListToKhai.ColumnAutoResize = true;
            dgListToKhai_DesignTimeLayout.LayoutString = resources.GetString("dgListToKhai_DesignTimeLayout.LayoutString");
            this.dgListToKhai.DesignTimeLayout = dgListToKhai_DesignTimeLayout;
            this.dgListToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListToKhai.GroupByBoxVisible = false;
            this.dgListToKhai.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListToKhai.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListToKhai.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListToKhai.Location = new System.Drawing.Point(3, 8);
            this.dgListToKhai.Name = "dgListToKhai";
            this.dgListToKhai.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListToKhai.Size = new System.Drawing.Size(1105, 125);
            this.dgListToKhai.TabIndex = 214;
            this.dgListToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListToKhai.VisualStyleManager = this.vsmMain;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDelete.Icon")));
            this.btnDelete.Location = new System.Drawing.Point(1037, 139);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(70, 23);
            this.btnDelete.TabIndex = 211;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // dgListSanPham
            // 
            this.dgListSanPham.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSanPham.AlternatingColors = true;
            this.dgListSanPham.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListSanPham.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListSanPham.ColumnAutoResize = true;
            dgListSanPham_DesignTimeLayout.LayoutString = resources.GetString("dgListSanPham_DesignTimeLayout.LayoutString");
            this.dgListSanPham.DesignTimeLayout = dgListSanPham_DesignTimeLayout;
            this.dgListSanPham.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListSanPham.GroupByBoxVisible = false;
            this.dgListSanPham.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSanPham.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSanPham.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListSanPham.Location = new System.Drawing.Point(3, 8);
            this.dgListSanPham.Name = "dgListSanPham";
            this.dgListSanPham.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSanPham.Size = new System.Drawing.Size(1105, 125);
            this.dgListSanPham.TabIndex = 207;
            this.dgListSanPham.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListSanPham.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.BorderColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiButton7);
            this.uiGroupBox2.Controls.Add(this.uiButton6);
            this.uiGroupBox2.Controls.Add(this.dgListSPTON);
            this.uiGroupBox2.Controls.Add(this.uiButton5);
            this.uiGroupBox2.Controls.Add(this.dglistTKTon);
            this.uiGroupBox2.Location = new System.Drawing.Point(11, 168);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1111, 437);
            this.uiGroupBox2.TabIndex = 1;
            // 
            // uiButton7
            // 
            this.uiButton7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton7.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton7.Icon")));
            this.uiButton7.Location = new System.Drawing.Point(993, 7);
            this.uiButton7.Name = "uiButton7";
            this.uiButton7.Size = new System.Drawing.Size(114, 23);
            this.uiButton7.TabIndex = 2;
            this.uiButton7.Text = "Kiểm tra";
            this.uiButton7.VisualStyleManager = this.vsmMain;
            this.uiButton7.Click += new System.EventHandler(this.KiemTra_Click);
            // 
            // uiButton6
            // 
            this.uiButton6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton6.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton6.Icon")));
            this.uiButton6.Location = new System.Drawing.Point(868, 7);
            this.uiButton6.Name = "uiButton6";
            this.uiButton6.Size = new System.Drawing.Size(119, 23);
            this.uiButton6.TabIndex = 1;
            this.uiButton6.Text = "Chọn tờ khai";
            this.uiButton6.VisualStyleManager = this.vsmMain;
            this.uiButton6.Click += new System.EventHandler(this.ChonToKhai_Click);
            // 
            // dgListSPTON
            // 
            this.dgListSPTON.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListSPTON.AlternatingColors = true;
            this.dgListSPTON.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListSPTON.AutomaticSort = false;
            this.dgListSPTON.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListSPTON.ColumnAutoResize = true;
            dgListSPTON_DesignTimeLayout.LayoutString = resources.GetString("dgListSPTON_DesignTimeLayout.LayoutString");
            this.dgListSPTON.DesignTimeLayout = dgListSPTON_DesignTimeLayout;
            this.dgListSPTON.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListSPTON.GroupByBoxVisible = false;
            this.dgListSPTON.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPTON.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPTON.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListSPTON.Location = new System.Drawing.Point(2, 37);
            this.dgListSPTON.Name = "dgListSPTON";
            this.dgListSPTON.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSPTON.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListSPTON.Size = new System.Drawing.Size(1105, 394);
            this.dgListSPTON.TabIndex = 2;
            this.dgListSPTON.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListSPTON.VisualStyleManager = this.vsmMain;
            this.dgListSPTON.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgListSPTON_RowDoubleClick);
            this.dgListSPTON.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListSPTON_LoadingRow);
            // 
            // uiButton5
            // 
            this.uiButton5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton5.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton5.Icon")));
            this.uiButton5.Location = new System.Drawing.Point(731, 7);
            this.uiButton5.Name = "uiButton5";
            this.uiButton5.Size = new System.Drawing.Size(131, 23);
            this.uiButton5.TabIndex = 0;
            this.uiButton5.Text = "Chọn sản phẩm";
            this.uiButton5.VisualStyleManager = this.vsmMain;
            this.uiButton5.Click += new System.EventHandler(this.ChonSanPhamKiemTra_Click);
            // 
            // dglistTKTon
            // 
            this.dglistTKTon.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dglistTKTon.AlternatingColors = true;
            this.dglistTKTon.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dglistTKTon.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dglistTKTon.ColumnAutoResize = true;
            dglistTKTon_DesignTimeLayout.LayoutString = resources.GetString("dglistTKTon_DesignTimeLayout.LayoutString");
            this.dglistTKTon.DesignTimeLayout = dglistTKTon_DesignTimeLayout;
            this.dglistTKTon.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dglistTKTon.GroupByBoxVisible = false;
            this.dglistTKTon.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dglistTKTon.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dglistTKTon.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dglistTKTon.Location = new System.Drawing.Point(3, 38);
            this.dglistTKTon.Name = "dglistTKTon";
            this.dglistTKTon.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dglistTKTon.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dglistTKTon.Size = new System.Drawing.Size(1105, 390);
            this.dglistTKTon.TabIndex = 3;
            this.dglistTKTon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dglistTKTon.VisualStyleManager = this.vsmMain;
            // 
            // uiTabSanPham
            // 
            this.uiTabSanPham.BackColor = System.Drawing.Color.Transparent;
            this.uiTabSanPham.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTabSanPham.FlatBorderColor = System.Drawing.Color.Transparent;
            this.uiTabSanPham.Location = new System.Drawing.Point(0, 0);
            this.uiTabSanPham.Name = "uiTabSanPham";
            this.uiTabSanPham.Size = new System.Drawing.Size(1135, 629);
            this.uiTabSanPham.TabIndex = 0;
            this.uiTabSanPham.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tabQLNPLTon,
            this.uiTabPage2,
            this.uiTPSanPham,
            this.uiTabPage3,
            this.uiTabPage4,
            this.uiTabPage1});
            this.uiTabSanPham.VisualStyleManager = this.vsmMain;
            this.uiTabSanPham.SelectedTabChanged += new Janus.Windows.UI.Tab.TabEventHandler(this.uiTabSanPham_SelectedTabChanged);
            // 
            // tabQLNPLTon
            // 
            this.tabQLNPLTon.Controls.Add(this.uiGroupBox5);
            this.tabQLNPLTon.Controls.Add(this.dgList);
            this.tabQLNPLTon.Key = "tabQLNPLTon";
            this.tabQLNPLTon.Location = new System.Drawing.Point(1, 21);
            this.tabQLNPLTon.Name = "tabQLNPLTon";
            this.tabQLNPLTon.Size = new System.Drawing.Size(1133, 607);
            this.tabQLNPLTon.TabStop = true;
            this.tabQLNPLTon.Text = "Quản lý nguyên phụ liệu tồn";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.filterEditor1);
            this.uiGroupBox5.Location = new System.Drawing.Point(11, 12);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1099, 63);
            this.uiGroupBox5.TabIndex = 189;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // filterEditor1
            // 
            this.filterEditor1.AutoApply = true;
            this.filterEditor1.BackColor = System.Drawing.Color.Transparent;
            this.filterEditor1.DefaultConditionOperator = Janus.Data.ConditionOperator.BeginsWith;
            this.filterEditor1.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.UseFormatStyle;
            this.filterEditor1.Location = new System.Drawing.Point(39, 11);
            this.filterEditor1.MinSize = new System.Drawing.Size(465, 44);
            this.filterEditor1.Name = "filterEditor1";
            this.filterEditor1.Office2007ColorScheme = Janus.Windows.Common.Office2007ColorScheme.Default;
            this.filterEditor1.ScrollMode = Janus.Windows.UI.Dock.ScrollMode.Both;
            this.filterEditor1.ScrollStep = 15;
            this.filterEditor1.Size = new System.Drawing.Size(555, 47);
            this.filterEditor1.SourceControl = this.dgList;
            this.filterEditor1.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(-1, 92);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1133, 516);
            this.dgList.TabIndex = 187;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.uiButton4);
            this.uiTabPage2.Controls.Add(this.uiButton2);
            this.uiTabPage2.Controls.Add(this.uiGroupBox3);
            this.uiTabPage2.Controls.Add(this.dgListNPLTK);
            this.uiTabPage2.Key = "ThongKeLuongTon";
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(1133, 607);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thống kê lượng tồn theo tờ khai";
            // 
            // uiButton4
            // 
            this.uiButton4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton4.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton4.Icon")));
            this.uiButton4.Location = new System.Drawing.Point(954, 580);
            this.uiButton4.Name = "uiButton4";
            this.uiButton4.Size = new System.Drawing.Size(75, 23);
            this.uiButton4.TabIndex = 1;
            this.uiButton4.Text = "Lưu";
            this.uiButton4.Visible = false;
            this.uiButton4.VisualStyleManager = this.vsmMain;
            this.uiButton4.Click += new System.EventHandler(this.LuuTonToKhai_Click);
            // 
            // uiButton2
            // 
            this.uiButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(1035, 580);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(75, 23);
            this.uiButton2.TabIndex = 2;
            this.uiButton2.Text = "In";
            this.uiButton2.VisualStyleManager = this.vsmMain;
            this.uiButton2.Click += new System.EventHandler(this.inDanhSachTon_Click_1);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.txtTenChuHang);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtMaNPL);
            this.uiGroupBox3.Controls.Add(this.label4);
            this.uiGroupBox3.Controls.Add(this.btnTimKiemTK);
            this.uiGroupBox3.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Controls.Add(this.txtNamTiepNhan);
            this.uiGroupBox3.Location = new System.Drawing.Point(11, 3);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1099, 103);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // txtTenChuHang
            // 
            this.txtTenChuHang.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtTenChuHang.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtTenChuHang.Location = new System.Drawing.Point(136, 58);
            this.txtTenChuHang.Name = "txtTenChuHang";
            this.txtTenChuHang.Size = new System.Drawing.Size(502, 21);
            this.txtTenChuHang.TabIndex = 7;
            this.txtTenChuHang.Text = "*";
            this.txtTenChuHang.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(18, 63);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Tên chủ hàng";
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.txtMaNPL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNPL.Location = new System.Drawing.Point(519, 31);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(209, 21);
            this.txtMaNPL.TabIndex = 5;
            this.txtMaNPL.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(435, 36);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mã NPL";
            // 
            // btnTimKiemTK
            // 
            this.btnTimKiemTK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTimKiemTK.Icon = ((System.Drawing.Icon)(resources.GetObject("btnTimKiemTK.Icon")));
            this.btnTimKiemTK.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnTimKiemTK.Location = new System.Drawing.Point(644, 58);
            this.btnTimKiemTK.Name = "btnTimKiemTK";
            this.btnTimKiemTK.Size = new System.Drawing.Size(84, 23);
            this.btnTimKiemTK.TabIndex = 8;
            this.btnTimKiemTK.Text = "Tìm kiếm";
            this.btnTimKiemTK.VisualStyleManager = this.vsmMain;
            this.btnTimKiemTK.WordWrap = false;
            this.btnTimKiemTK.Click += new System.EventHandler(this.TimToKhaiTon_Click);
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.DecimalDigits = 0;
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.FormatString = "#####";
            this.txtSoTiepNhan.Location = new System.Drawing.Point(136, 31);
            this.txtSoTiepNhan.MaxLength = 5;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(49, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoTiepNhan.Value = ((ulong)(0ul));
            this.txtSoTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(198, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Năm đăng ký";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(18, 39);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(63, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Số tờ khai";
            // 
            // txtNamTiepNhan
            // 
            this.txtNamTiepNhan.DecimalDigits = 0;
            this.txtNamTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamTiepNhan.FormatString = "####";
            this.txtNamTiepNhan.Location = new System.Drawing.Point(350, 31);
            this.txtNamTiepNhan.MaxLength = 4;
            this.txtNamTiepNhan.Name = "txtNamTiepNhan";
            this.txtNamTiepNhan.Size = new System.Drawing.Size(37, 21);
            this.txtNamTiepNhan.TabIndex = 3;
            this.txtNamTiepNhan.Text = "2012";
            this.txtNamTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtNamTiepNhan.Value = ((short)(2012));
            this.txtNamTiepNhan.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16;
            this.txtNamTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // dgListNPLTK
            // 
            this.dgListNPLTK.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListNPLTK.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgListNPLTK.ColumnAutoResize = true;
            dgListNPLTK_DesignTimeLayout.LayoutString = resources.GetString("dgListNPLTK_DesignTimeLayout.LayoutString");
            this.dgListNPLTK.DesignTimeLayout = dgListNPLTK_DesignTimeLayout;
            this.dgListNPLTK.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPLTK.GroupByBoxVisible = false;
            this.dgListNPLTK.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLTK.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLTK.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPLTK.Location = new System.Drawing.Point(11, 112);
            this.dgListNPLTK.Name = "dgListNPLTK";
            this.dgListNPLTK.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPLTK.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPLTK.Size = new System.Drawing.Size(1099, 462);
            this.dgListNPLTK.TabIndex = 3;
            this.dgListNPLTK.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPLTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListNPLTK.VisualStyleManager = this.vsmMain;
            this.dgListNPLTK.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListNPLTK_LoadingRow);
            this.dgListNPLTK.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListNPLTK_FormattingRow);
            // 
            // uiTabPage3
            // 
            this.uiTabPage3.Controls.Add(this.cmdPrint);
            this.uiTabPage3.Controls.Add(this.uiButton9);
            this.uiTabPage3.Controls.Add(this.uiGroupBox6);
            this.uiTabPage3.Controls.Add(this.dgListPhanBo);
            this.uiTabPage3.Key = "TheoDoiPhanBo";
            this.uiTabPage3.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage3.Name = "uiTabPage3";
            this.uiTabPage3.Size = new System.Drawing.Size(1133, 607);
            this.uiTabPage3.TabStop = true;
            this.uiTabPage3.Text = "Theo dõi phân bổ";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdPrint.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdPrint.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdPrint.Icon")));
            this.cmdPrint.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.cmdPrint.Location = new System.Drawing.Point(845, 575);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(101, 23);
            this.cmdPrint.TabIndex = 1;
            this.cmdPrint.Text = "In phân bổ";
            this.cmdPrint.VisualStyleManager = this.vsmMain;
            this.cmdPrint.WordWrap = false;
            this.cmdPrint.Click += new System.EventHandler(this.cmdPrint_Click);
            // 
            // uiButton9
            // 
            this.uiButton9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton9.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton9.Icon")));
            this.uiButton9.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.uiButton9.Location = new System.Drawing.Point(952, 575);
            this.uiButton9.Name = "uiButton9";
            this.uiButton9.Size = new System.Drawing.Size(170, 23);
            this.uiButton9.TabIndex = 2;
            this.uiButton9.Text = "Lưu thông tin phân bổ";
            this.uiButton9.VisualStyleManager = this.vsmMain;
            this.uiButton9.WordWrap = false;
            this.uiButton9.Click += new System.EventHandler(this.uiButton9_Click);
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.filterEditor2);
            this.uiGroupBox6.Controls.Add(this.loaiHinhMauDichHControl1);
            this.uiGroupBox6.Controls.Add(this.label10);
            this.uiGroupBox6.Controls.Add(this.label1);
            this.uiGroupBox6.Controls.Add(this.cbSanPham);
            this.uiGroupBox6.Controls.Add(this.btnTheoDoiPB);
            this.uiGroupBox6.Controls.Add(this.txSoTKXuat);
            this.uiGroupBox6.Controls.Add(this.label11);
            this.uiGroupBox6.Controls.Add(this.label12);
            this.uiGroupBox6.Controls.Add(this.txtNamDKXuat);
            this.uiGroupBox6.Location = new System.Drawing.Point(11, 3);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(1111, 118);
            this.uiGroupBox6.TabIndex = 0;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // filterEditor2
            // 
            this.filterEditor2.AutoApply = true;
            this.filterEditor2.BackColor = System.Drawing.Color.Transparent;
            this.filterEditor2.DefaultConditionOperator = Janus.Data.ConditionOperator.Equal;
            this.filterEditor2.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.UseFormatStyle;
            this.filterEditor2.Location = new System.Drawing.Point(24, 67);
            this.filterEditor2.MinSize = new System.Drawing.Size(403, 44);
            this.filterEditor2.Name = "filterEditor2";
            this.filterEditor2.Office2007ColorScheme = Janus.Windows.Common.Office2007ColorScheme.Default;
            this.filterEditor2.ScrollMode = Janus.Windows.UI.Dock.ScrollMode.Both;
            this.filterEditor2.ScrollStep = 15;
            this.filterEditor2.Size = new System.Drawing.Size(642, 45);
            this.filterEditor2.SourceControl = this.dgListPhanBo;
            this.filterEditor2.VisualStyleManager = this.vsmMain;
            // 
            // dgListPhanBo
            // 
            this.dgListPhanBo.AlternatingColors = true;
            this.dgListPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListPhanBo.AutomaticSort = false;
            this.dgListPhanBo.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListPhanBo_DesignTimeLayout.LayoutString = resources.GetString("dgListPhanBo_DesignTimeLayout.LayoutString");
            this.dgListPhanBo.DesignTimeLayout = dgListPhanBo_DesignTimeLayout;
            this.dgListPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListPhanBo.GroupByBoxVisible = false;
            this.dgListPhanBo.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListPhanBo.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListPhanBo.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListPhanBo.Location = new System.Drawing.Point(11, 127);
            this.dgListPhanBo.Name = "dgListPhanBo";
            this.dgListPhanBo.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListPhanBo.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListPhanBo.Size = new System.Drawing.Size(1111, 440);
            this.dgListPhanBo.TabIndex = 3;
            this.dgListPhanBo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListPhanBo.VisualStyleManager = this.vsmMain;
            this.dgListPhanBo.EditingCell += new Janus.Windows.GridEX.EditingCellEventHandler(this.dgListPhanBo_EditingCell);
            this.dgListPhanBo.RecordUpdated += new System.EventHandler(this.dgListPhanBo_RecordUpdated);
            this.dgListPhanBo.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListPhanBo_LoadingRow);
            // 
            // loaiHinhMauDichHControl1
            // 
            this.loaiHinhMauDichHControl1.BackColor = System.Drawing.Color.Transparent;
            this.loaiHinhMauDichHControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loaiHinhMauDichHControl1.Location = new System.Drawing.Point(105, 37);
            this.loaiHinhMauDichHControl1.Ma = "";
            this.loaiHinhMauDichHControl1.Name = "loaiHinhMauDichHControl1";
            this.loaiHinhMauDichHControl1.Nhom = "";
            this.loaiHinhMauDichHControl1.ReadOnly = false;
            this.loaiHinhMauDichHControl1.Size = new System.Drawing.Size(471, 22);
            this.loaiHinhMauDichHControl1.TabIndex = 7;
            this.loaiHinhMauDichHControl1.VisualStyleManager = this.vsmMain;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(25, 42);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(74, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "Mã loại hình";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(316, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Sản phẩm";
            // 
            // cbSanPham
            // 
            this.cbSanPham.Location = new System.Drawing.Point(385, 12);
            this.cbSanPham.Name = "cbSanPham";
            this.cbSanPham.Size = new System.Drawing.Size(178, 21);
            this.cbSanPham.TabIndex = 5;
            this.cbSanPham.VisualStyleManager = this.vsmMain;
            // 
            // btnTheoDoiPB
            // 
            this.btnTheoDoiPB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTheoDoiPB.Icon = ((System.Drawing.Icon)(resources.GetObject("btnTheoDoiPB.Icon")));
            this.btnTheoDoiPB.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnTheoDoiPB.Location = new System.Drawing.Point(577, 34);
            this.btnTheoDoiPB.Name = "btnTheoDoiPB";
            this.btnTheoDoiPB.Size = new System.Drawing.Size(84, 23);
            this.btnTheoDoiPB.TabIndex = 8;
            this.btnTheoDoiPB.Text = "Tìm kiếm";
            this.btnTheoDoiPB.VisualStyleManager = this.vsmMain;
            this.btnTheoDoiPB.WordWrap = false;
            this.btnTheoDoiPB.Click += new System.EventHandler(this.XemPhanBo_Click);
            // 
            // txSoTKXuat
            // 
            this.txSoTKXuat.DecimalDigits = 0;
            this.txSoTKXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txSoTKXuat.FormatString = "#####";
            this.txSoTKXuat.Location = new System.Drawing.Point(105, 10);
            this.txSoTKXuat.MaxLength = 5;
            this.txSoTKXuat.Name = "txSoTKXuat";
            this.txSoTKXuat.Size = new System.Drawing.Size(49, 21);
            this.txSoTKXuat.TabIndex = 1;
            this.txSoTKXuat.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txSoTKXuat.Value = ((ulong)(0ul));
            this.txSoTKXuat.ValueType = Janus.Windows.GridEX.NumericEditValueType.UInt64;
            this.txSoTKXuat.VisualStyleManager = this.vsmMain;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(168, 15);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Năm đăng ký";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(36, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Số tờ khai";
            // 
            // txtNamDKXuat
            // 
            this.txtNamDKXuat.DecimalDigits = 0;
            this.txtNamDKXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNamDKXuat.FormatString = "####";
            this.txtNamDKXuat.Location = new System.Drawing.Point(254, 11);
            this.txtNamDKXuat.MaxLength = 4;
            this.txtNamDKXuat.Name = "txtNamDKXuat";
            this.txtNamDKXuat.Size = new System.Drawing.Size(37, 21);
            this.txtNamDKXuat.TabIndex = 3;
            this.txtNamDKXuat.Text = "2008";
            this.txtNamDKXuat.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtNamDKXuat.Value = ((short)(2008));
            this.txtNamDKXuat.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int16;
            this.txtNamDKXuat.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage4
            // 
            this.uiTabPage4.Controls.Add(this.uiButton10);
            this.uiTabPage4.Controls.Add(this.dgListTKChuaPhanBo);
            this.uiTabPage4.Key = "PhanBoToKhaiXuat";
            this.uiTabPage4.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage4.Name = "uiTabPage4";
            this.uiTabPage4.Size = new System.Drawing.Size(1133, 607);
            this.uiTabPage4.TabStop = true;
            this.uiTabPage4.Text = "Thực hiện phân bổ cho tờ khai xuất";
            // 
            // uiButton10
            // 
            this.uiButton10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiButton10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton10.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton10.Icon")));
            this.uiButton10.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.uiButton10.Location = new System.Drawing.Point(1038, 577);
            this.uiButton10.Name = "uiButton10";
            this.uiButton10.Size = new System.Drawing.Size(81, 23);
            this.uiButton10.TabIndex = 278;
            this.uiButton10.Text = "Phân bổ";
            this.uiButton10.VisualStyleManager = this.vsmMain;
            this.uiButton10.WordWrap = false;
            this.uiButton10.Click += new System.EventHandler(this.PhanBo_Click);
            // 
            // dgListTKChuaPhanBo
            // 
            this.dgListTKChuaPhanBo.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTKChuaPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgListTKChuaPhanBo.AutomaticSort = false;
            this.dgListTKChuaPhanBo.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListTKChuaPhanBo.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListTKChuaPhanBo_DesignTimeLayout.LayoutString = resources.GetString("dgListTKChuaPhanBo_DesignTimeLayout.LayoutString");
            this.dgListTKChuaPhanBo.DesignTimeLayout = dgListTKChuaPhanBo_DesignTimeLayout;
            this.dgListTKChuaPhanBo.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKChuaPhanBo.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTKChuaPhanBo.GroupByBoxVisible = false;
            this.dgListTKChuaPhanBo.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListTKChuaPhanBo.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTKChuaPhanBo.Hierarchical = true;
            this.dgListTKChuaPhanBo.ImageList = this.ImageList1;
            this.dgListTKChuaPhanBo.Location = new System.Drawing.Point(20, 3);
            this.dgListTKChuaPhanBo.Name = "dgListTKChuaPhanBo";
            this.dgListTKChuaPhanBo.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTKChuaPhanBo.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKChuaPhanBo.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKChuaPhanBo.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKChuaPhanBo.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListTKChuaPhanBo.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKChuaPhanBo.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKChuaPhanBo.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTKChuaPhanBo.Size = new System.Drawing.Size(1099, 568);
            this.dgListTKChuaPhanBo.TabIndex = 205;
            this.dgListTKChuaPhanBo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgListTKChuaPhanBo.VisualStyleManager = this.vsmMain;
            this.dgListTKChuaPhanBo.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListTKChuaPhanBo_LoadingRow);
            this.dgListTKChuaPhanBo.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgListTKChuaPhanBo_FormattingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.dgToKhaiXuatDaPhanBo);
            this.uiTabPage1.Key = "tkxDaPhanBo";
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(1133, 607);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Tờ khai xuất đã phân bổ";
            // 
            // dgToKhaiXuatDaPhanBo
            // 
            this.dgToKhaiXuatDaPhanBo.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgToKhaiXuatDaPhanBo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgToKhaiXuatDaPhanBo.AutomaticSort = false;
            this.dgToKhaiXuatDaPhanBo.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgToKhaiXuatDaPhanBo.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgToKhaiXuatDaPhanBo.ColumnAutoResize = true;
            dgToKhaiXuatDaPhanBo_DesignTimeLayout.LayoutString = resources.GetString("dgToKhaiXuatDaPhanBo_DesignTimeLayout.LayoutString");
            this.dgToKhaiXuatDaPhanBo.DesignTimeLayout = dgToKhaiXuatDaPhanBo_DesignTimeLayout;
            this.dgToKhaiXuatDaPhanBo.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgToKhaiXuatDaPhanBo.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiXuatDaPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgToKhaiXuatDaPhanBo.GroupByBoxVisible = false;
            this.dgToKhaiXuatDaPhanBo.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiXuatDaPhanBo.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgToKhaiXuatDaPhanBo.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiXuatDaPhanBo.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgToKhaiXuatDaPhanBo.Hierarchical = true;
            this.dgToKhaiXuatDaPhanBo.ImageList = this.ImageList1;
            this.dgToKhaiXuatDaPhanBo.Location = new System.Drawing.Point(17, 20);
            this.dgToKhaiXuatDaPhanBo.Name = "dgToKhaiXuatDaPhanBo";
            this.dgToKhaiXuatDaPhanBo.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgToKhaiXuatDaPhanBo.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgToKhaiXuatDaPhanBo.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgToKhaiXuatDaPhanBo.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiXuatDaPhanBo.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgToKhaiXuatDaPhanBo.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgToKhaiXuatDaPhanBo.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiXuatDaPhanBo.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgToKhaiXuatDaPhanBo.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgToKhaiXuatDaPhanBo.Size = new System.Drawing.Size(1099, 568);
            this.dgToKhaiXuatDaPhanBo.TabIndex = 206;
            this.dgToKhaiXuatDaPhanBo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgToKhaiXuatDaPhanBo.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.ctrDonViHaiQuan);
            this.uiGroupBox4.Controls.Add(this.label3);
            this.uiGroupBox4.Controls.Add(this.btnSearch);
            this.uiGroupBox4.Controls.Add(this.ccToDate);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.ccFromDate);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(53, 5);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(686, 96);
            this.uiGroupBox4.TabIndex = 304;
            this.uiGroupBox4.Text = "Tìm kiếm thông tin tờ khai";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // ctrDonViHaiQuan
            // 
            this.ctrDonViHaiQuan.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.ctrDonViHaiQuan.BackColor = System.Drawing.Color.Transparent;
            this.ctrDonViHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDonViHaiQuan.Location = new System.Drawing.Point(161, 19);
            this.ctrDonViHaiQuan.Ma = "C34C";
            this.ctrDonViHaiQuan.MaCuc = "";
            this.ctrDonViHaiQuan.Name = "ctrDonViHaiQuan";
            this.ctrDonViHaiQuan.ReadOnly = true;
            this.ctrDonViHaiQuan.Size = new System.Drawing.Size(366, 39);
            this.ctrDonViHaiQuan.TabIndex = 274;
            this.ctrDonViHaiQuan.VisualStyleManager = null;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(60, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 13);
            this.label3.TabIndex = 273;
            this.label3.Text = "Mã hải quan";
            // 
            // btnSearch
            // 
            this.btnSearch.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSearch.Icon")));
            this.btnSearch.Location = new System.Drawing.Point(546, 50);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(93, 40);
            this.btnSearch.TabIndex = 272;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyleManager = this.vsmMain;
            // 
            // ccToDate
            // 
            this.ccToDate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccToDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccToDate.DropDownCalendar.Name = "";
            this.ccToDate.DropDownCalendar.Visible = false;
            this.ccToDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccToDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccToDate.Location = new System.Drawing.Point(392, 52);
            this.ccToDate.Name = "ccToDate";
            this.ccToDate.Nullable = true;
            this.ccToDate.NullButtonText = "Xóa";
            this.ccToDate.ShowNullButton = true;
            this.ccToDate.Size = new System.Drawing.Size(120, 21);
            this.ccToDate.TabIndex = 271;
            this.ccToDate.TodayButtonText = "Hôm nay";
            this.ccToDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccToDate.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(325, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 270;
            this.label7.Text = "Đến ngày";
            // 
            // ccFromDate
            // 
            this.ccFromDate.BackColor = System.Drawing.Color.White;
            // 
            // 
            // 
            this.ccFromDate.DropDownCalendar.FirstMonth = new System.DateTime(2006, 10, 1, 0, 0, 0, 0);
            this.ccFromDate.DropDownCalendar.Name = "";
            this.ccFromDate.DropDownCalendar.Visible = false;
            this.ccFromDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccFromDate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccFromDate.Location = new System.Drawing.Point(190, 52);
            this.ccFromDate.Name = "ccFromDate";
            this.ccFromDate.Nullable = true;
            this.ccFromDate.NullButtonText = "Xóa";
            this.ccFromDate.ShowNullButton = true;
            this.ccFromDate.Size = new System.Drawing.Size(120, 21);
            this.ccFromDate.TabIndex = 269;
            this.ccFromDate.TodayButtonText = "Hôm nay";
            this.ccFromDate.Value = new System.DateTime(2008, 1, 1, 0, 0, 0, 0);
            this.ccFromDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccFromDate.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(60, 57);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(124, 13);
            this.label6.TabIndex = 269;
            this.label6.Text = "Chọn tờ khai từ ngày";
            // 
            // uiButton1
            // 
            this.uiButton1.Location = new System.Drawing.Point(625, 120);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(84, 40);
            this.uiButton1.TabIndex = 303;
            this.uiButton1.Text = "Chọn tờ khai";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            // 
            // btnChonTK
            // 
            this.btnChonTK.Location = new System.Drawing.Point(718, 120);
            this.btnChonTK.Name = "btnChonTK";
            this.btnChonTK.Size = new System.Drawing.Size(74, 40);
            this.btnChonTK.TabIndex = 302;
            this.btnChonTK.Text = "Thống kê";
            this.btnChonTK.VisualStyleManager = this.vsmMain;
            // 
            // dgTKX
            // 
            this.dgTKX.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgTKX.AlternatingColors = true;
            this.dgTKX.AutomaticSort = false;
            this.dgTKX.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKX.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKX.ColumnAutoResize = true;
            dgTKX_DesignTimeLayout.LayoutString = resources.GetString("dgTKX_DesignTimeLayout.LayoutString");
            this.dgTKX.DesignTimeLayout = dgTKX_DesignTimeLayout;
            this.dgTKX.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.dgTKX.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKX.GroupByBoxVisible = false;
            this.dgTKX.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKX.Location = new System.Drawing.Point(0, 172);
            this.dgTKX.Name = "dgTKX";
            this.dgTKX.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKX.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKX.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKX.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKX.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKX.Size = new System.Drawing.Size(795, 165);
            this.dgTKX.TabIndex = 195;
            this.dgTKX.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgTKX.VisualStyleManager = this.vsmMain;
            // 
            // gridEXPrintDocument1
            // 
            this.gridEXPrintDocument1.CardColumnsPerPage = 1;
            this.gridEXPrintDocument1.FitColumns = Janus.Windows.GridEX.FitColumnsMode.SizingColumns;
            this.gridEXPrintDocument1.GridEX = this.dgListNPLTK;
            this.gridEXPrintDocument1.PageHeaderCenter = "DANH SÁCH NGUYÊN PHỤ LIỆU TỒN THỰC TẾ";
            this.gridEXPrintDocument1.PageHeaderFormatStyle.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // ThongkeHangTonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1135, 629);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ThongkeHangTonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thống kê NPL tồn";
            this.Load += new System.EventHandler(this.ThongkeHangTonForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.uiTPSanPham.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListToKhai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSanPham)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPTON)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dglistTKTon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiTabSanPham)).EndInit();
            this.uiTabSanPham.ResumeLayout(false);
            this.tabQLNPLTon.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.uiTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLTK)).EndInit();
            this.uiTabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListPhanBo)).EndInit();
            this.uiTabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKChuaPhanBo)).EndInit();
            this.uiTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhaiXuatDaPhanBo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKX)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton uicmdSelectTK;
        private Janus.Windows.EditControls.UIButton uicmdThongke;
        private Janus.Windows.UI.Tab.UITab uiTabSanPham;
        private Janus.Windows.UI.Tab.UITabPage uiTPSanPham;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private Janus.Windows.CalendarCombo.CalendarCombo ccToDate;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.CalendarCombo.CalendarCombo ccFromDate;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton btnChonTK;
        private Janus.Windows.GridEX.GridEX dgTKX;
        private Company.Interface.Controls.DonViHaiQuanControl ctrDonViHaiQuan;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.UI.Tab.UITabPage tabQLNPLTon;
        private Janus.Windows.GridEX.GridEX dgListSanPham;
        private Janus.Windows.GridEX.GridEX dglistTKTon;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.GridEX dgListSPTON;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenChuHang;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIButton btnTimKiemTK;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamTiepNhan;
        private Janus.Windows.GridEX.GridEX dgListNPLTK;
        private Janus.Windows.EditControls.UIButton uiButton4;
        private Janus.Windows.GridEX.GridEXPrintDocument gridEXPrintDocument1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private Janus.Windows.FilterEditor.FilterEditor filterEditor1;
        private Janus.Windows.EditControls.UIButton uiButton7;
        private Janus.Windows.EditControls.UIButton uiButton6;
        private Janus.Windows.EditControls.UIButton uiButton5;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage3;
        private Janus.Windows.GridEX.GridEX dgListPhanBo;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnTheoDoiPB;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txSoTKXuat;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtNamDKXuat;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox cbSanPham;
        private Company.Interface.Controls.LoaiHinhMauDichHControl loaiHinhMauDichHControl1;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage4;
        private Janus.Windows.EditControls.UIButton uiButton10;
        private Janus.Windows.GridEX.GridEX dgListTKChuaPhanBo;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.FilterEditor.FilterEditor filterEditor2;
        private Janus.Windows.EditControls.UIButton uiButton9;
        private Janus.Windows.EditControls.UIButton cmdPrint;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.GridEX.GridEX dgToKhaiXuatDaPhanBo;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private Janus.Windows.GridEX.GridEX dgListToKhai;
    }
}