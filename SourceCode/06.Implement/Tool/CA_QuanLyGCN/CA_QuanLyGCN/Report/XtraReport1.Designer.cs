namespace CA_QuanLyGCN.Report
{
    partial class XtraReport1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();


            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XtraReport1));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHoSo1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHoSo2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoGiayChungNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblThangChungNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNamChungNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoQuanCapCN = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayChungNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTruSo1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblTruSo2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblChucDanh = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNguoDaiDien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell16 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblNamCMND = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblQuocTich = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoCMND_HC = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayCMND = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThangCMND = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoQuanCapCMND = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHoKhau1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblHoKhau2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChungNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuKienChungNhan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGhiChu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDieuKienChungNhan2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNam = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiem = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgay = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThang = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrLabel3,
            this.xrLabel2,
            this.xrLabel1,
            this.xrPictureBox1});
            this.Detail.HeightF = 1054.042F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrLabel3
            // 
            this.xrLabel3.LocationFloat = new DevExpress.Utils.PointFloat(110.4167F, 88.66501F);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.SizeF = new System.Drawing.SizeF(57.29168F, 16.75F);
            this.xrLabel3.Text = "xrLabel1";
            // 
            // xrLabel2
            // 
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(98.95834F, 66.79F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(111.4584F, 16.75F);
            this.xrLabel2.Text = "xrLabel1";
            // 
            // xrLabel1
            // 
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(98.95834F, 41.62499F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(111.4584F, 16.75F);
            this.xrLabel1.Text = "xrLabel1";
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.LocationFloat = new DevExpress.Utils.PointFloat(28.92F, 0F);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.SizeF = new System.Drawing.SizeF(703.08F, 1036.25F);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 62F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 43F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(73.95834F, 291F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow8,
            this.xrTableRow7,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow13,
            this.xrTableRow12,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow14});
            this.xrTable1.SizeF = new System.Drawing.SizeF(616.67F, 584.3674F);
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.lblHoSo1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Weight = 1.0852842620116705;
            // 
            // lblHoSo1
            // 
            this.lblHoSo1.Name = "lblHoSo1";
            this.lblHoSo1.Text = "lblHoSo1";
            this.lblHoSo1.Weight = 1.9147157379883295;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHoSo2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1.125;
            // 
            // lblHoSo2
            // 
            this.lblHoSo2.Name = "lblHoSo2";
            this.lblHoSo2.Text = "lblHoSo2";
            this.lblHoSo2.Weight = 3;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell7,
            this.lblSoGiayChungNhan});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 2.2083337402343748;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 1.9518397457711068;
            // 
            // lblSoGiayChungNhan
            // 
            this.lblSoGiayChungNhan.Name = "lblSoGiayChungNhan";
            this.lblSoGiayChungNhan.Text = "lblSoGiayChungNhan";
            this.lblSoGiayChungNhan.Weight = 1.0481602542288933;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell4,
            this.lblNgayChungNhan,
            this.lblThangChungNhan,
            this.lblNamChungNhan,
            this.lblCoQuanCapCN});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 1.0833325195312502;
            // 
            // lblThangChungNhan
            // 
            this.lblThangChungNhan.Name = "lblThangChungNhan";
            this.lblThangChungNhan.Text = "NN";
            this.lblThangChungNhan.Weight = 0.3294323584319338;
            // 
            // lblNamChungNhan
            // 
            this.lblNamChungNhan.Name = "lblNamChungNhan";
            this.lblNamChungNhan.Text = "NNNN";
            this.lblNamChungNhan.Weight = 0.80434872070830254;
            // 
            // lblCoQuanCapCN
            // 
            this.lblCoQuanCapCN.Name = "lblCoQuanCapCN";
            this.lblCoQuanCapCN.Text = "lblCoQuanCapCN";
            this.lblCoQuanCapCN.Weight = 1.2959851688054416;
            // 
            // lblNgayChungNhan
            // 
            this.lblNgayChungNhan.Name = "lblNgayChungNhan";
            this.lblNgayChungNhan.Text = "NN";
            this.lblNgayChungNhan.Weight = 0.35953250201525178;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 0.21070125003907048;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell5,
            this.lblTruSo1});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1.1666674804687502;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Weight = 0.361202415000077;
            // 
            // lblTruSo1
            // 
            this.lblTruSo1.Name = "lblTruSo1";
            this.lblTruSo1.Text = "lblTruSo1";
            this.lblTruSo1.Weight = 2.6387975849999235;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblTruSo2});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1.0416638183593752;
            // 
            // lblTruSo2
            // 
            this.lblTruSo2.Name = "lblTruSo2";
            this.lblTruSo2.Text = "lblTruSo2";
            this.lblTruSo2.Weight = 3.0000000000000004;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell19,
            this.lblQuocTich,
            this.lblChucDanh});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1.1249975585937502;
            // 
            // lblChucDanh
            // 
            this.lblChucDanh.Name = "lblChucDanh";
            this.lblChucDanh.Text = "lblChucDanh";
            this.lblChucDanh.Weight = 1.3645490691139;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell16,
            this.lblNguoDaiDien});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1.1249975585937502;
            // 
            // lblNguoDaiDien
            // 
            this.lblNguoDaiDien.Name = "lblNguoDaiDien";
            this.lblNguoDaiDien.Text = "lblNguoDaiDien";
            this.lblNguoDaiDien.Weight = 1.6555175177232906;
            // 
            // xrTableCell16
            // 
            this.xrTableCell16.Name = "xrTableCell16";
            this.xrTableCell16.Weight = 1.3444824822767099;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell23,
            this.lblSoCMND_HC,
            this.lblNgayCMND,
            this.lblThangCMND,
            this.lblNamCMND});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1.1249975585937502;
            // 
            // lblNamCMND
            // 
            this.lblNamCMND.Name = "lblNamCMND";
            this.lblNamCMND.Text = "NNNN";
            this.lblNamCMND.Weight = 0.7433097659682063;
            // 
            // lblQuocTich
            // 
            this.lblQuocTich.Name = "lblQuocTich";
            this.lblQuocTich.Text = "lblQuocTich";
            this.lblQuocTich.Weight = 1.2742485465055042;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.36120238438059632;
            // 
            // lblSoCMND_HC
            // 
            this.lblSoCMND_HC.Name = "lblSoCMND_HC";
            this.lblSoCMND_HC.Text = "lblSoCMND_HC";
            this.lblSoCMND_HC.Weight = 0.958192932808318;
            // 
            // lblNgayCMND
            // 
            this.lblNgayCMND.Name = "lblNgayCMND";
            this.lblNgayCMND.Text = "NN";
            this.lblNgayCMND.Weight = 0.34448238429437189;
            // 
            // lblThangCMND
            // 
            this.lblThangCMND.Name = "lblThangCMND";
            this.lblThangCMND.Text = "NN";
            this.lblThangCMND.Weight = 0.3369574877171792;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.61705742921192519;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell27,
            this.lblCoQuanCapCMND});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1.1249975585937502;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 0.45569642857317838;
            // 
            // lblCoQuanCapCMND
            // 
            this.lblCoQuanCapCMND.Name = "lblCoQuanCapCMND";
            this.lblCoQuanCapCMND.Text = "lblCoQuanCapCMND";
            this.lblCoQuanCapCMND.Weight = 2.5443035714268221;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.lblHoKhau1});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1.1249975585937502;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 1.4147166017647994;
            // 
            // lblHoKhau1
            // 
            this.lblHoKhau1.Name = "lblHoKhau1";
            this.lblHoKhau1.Text = "lblHoKhau1";
            this.lblHoKhau1.Weight = 1.5852833982352013;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell24,
            this.lblGhiChu1});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 1.7499951192925303;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 0.28595183864346974;
            // 
            // lblGhiChu1
            // 
            this.lblGhiChu1.Name = "lblGhiChu1";
            this.lblGhiChu1.Weight = 2.7140481613565308;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblHoKhau2});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 1.0416625976562501;
            // 
            // lblHoKhau2
            // 
            this.lblHoKhau2.Name = "lblHoKhau2";
            this.lblHoKhau2.Text = "lblHoKhau2";
            this.lblHoKhau2.Weight = 3.0000000000000009;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.lblChungNhan});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 3.2083300748644614;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Weight = 0.45569639182980148;
            // 
            // lblChungNhan
            // 
            this.lblChungNhan.Name = "lblChungNhan";
            this.lblChungNhan.Text = "lblChungNhan";
            this.lblChungNhan.Weight = 2.5443036081701993;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.lblDieuKienChungNhan1});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 1.0832638542483017;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 2.0919705213277671;
            // 
            // lblDieuKienChungNhan1
            // 
            this.lblDieuKienChungNhan1.Name = "lblDieuKienChungNhan1";
            this.lblDieuKienChungNhan1.Text = "lblDieuKienChungNhan1";
            this.lblDieuKienChungNhan1.Weight = 0.90802947867223371;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell26,
            this.lblGhiChu2});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 0.99993179679131394;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Weight = 1.4147166017647994;
            // 
            // lblGhiChu2
            // 
            this.lblGhiChu2.Name = "lblGhiChu2";
            this.lblGhiChu2.Text = "lblGhiChu2";
            this.lblGhiChu2.Weight = 1.5852833982352013;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDieuKienChungNhan2});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.99993156953859486;
            // 
            // lblDieuKienChungNhan2
            // 
            this.lblDieuKienChungNhan2.Name = "lblDieuKienChungNhan2";
            this.lblDieuKienChungNhan2.Text = "lblDieuKienChungNhan2";
            this.lblDieuKienChungNhan2.Weight = 3.0000000000000009;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.lblDiaDiem,
            this.xrTableCell41,
            this.lblNgay,
            this.lblThang,
            this.lblNam});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 1.0415966064458508;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Weight = 1.0852840841115663;
            // 
            // lblNam
            // 
            this.lblNam.Name = "lblNam";
            this.lblNam.Text = "NN";
            this.lblNam.Weight = 0.29264143655555436;
            // 
            // lblDiaDiem
            // 
            this.lblDiaDiem.Name = "lblDiaDiem";
            this.lblDiaDiem.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 3, 0, 0, 100F);
            this.lblDiaDiem.StylePriority.UsePadding = false;
            this.lblDiaDiem.StylePriority.UseTextAlignment = false;
            this.lblDiaDiem.Text = "lblDiaDiem";
            this.lblDiaDiem.TextAlignment = DevExpress.XtraPrinting.TextAlignment.BottomRight;
            this.lblDiaDiem.Weight = 0.61873096141935657;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.21571796717317338;
            // 
            // lblNgay
            // 
            this.lblNgay.Name = "lblNgay";
            this.lblNgay.Text = "NN";
            this.lblNgay.Weight = 0.44481676525077951;
            // 
            // lblThang
            // 
            this.lblThang.Name = "lblThang";
            this.lblThang.Text = "NN";
            this.lblThang.Weight = 0.34280878548957022;
            // 
            // XtraReport1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin});
            this.Margins = new System.Drawing.Printing.Margins(21, 73, 62, 43);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell lblHoSo1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblHoSo2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell lblSoGiayChungNhan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell lblThangChungNhan;
        private DevExpress.XtraReports.UI.XRTableCell lblNamChungNhan;
        private DevExpress.XtraReports.UI.XRTableCell lblCoQuanCapCN;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayChungNhan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell lblTruSo1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell lblTruSo2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell16;
        private DevExpress.XtraReports.UI.XRTableCell lblNguoDaiDien;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell lblQuocTich;
        private DevExpress.XtraReports.UI.XRTableCell lblChucDanh;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell lblSoCMND_HC;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayCMND;
        private DevExpress.XtraReports.UI.XRTableCell lblThangCMND;
        private DevExpress.XtraReports.UI.XRTableCell lblNamCMND;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell lblCoQuanCapCMND;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell lblHoKhau1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChu1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell lblHoKhau2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell lblChungNhan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKienChungNhan1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKienChungNhan2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiem;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell lblNgay;
        private DevExpress.XtraReports.UI.XRTableCell lblThang;
        private DevExpress.XtraReports.UI.XRTableCell lblNam;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell lblGhiChu2;
    }
}
