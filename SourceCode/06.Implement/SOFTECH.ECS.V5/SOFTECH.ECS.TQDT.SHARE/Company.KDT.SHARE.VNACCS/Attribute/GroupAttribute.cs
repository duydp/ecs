﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.KDT.SHARE.VNACCS
{
    public class GroupAttribute
    {
        public string LoopID { get; set; }
        //So lan lap
        public int Repetition { get; set; }
        public List<PropertiesAttribute> listAttribute { get; set; }

        public GroupAttribute(string idgroup, int repetition, List<PropertiesAttribute> AttributeCollec)
        {
            this.LoopID = idgroup;
            this.Repetition = repetition;
            listAttribute = AttributeCollec;
        }
        public void SetValueCollection(object Value, string GroupID, int IndexLoop)
        {
            try
            {
                PropertiesAttribute obj = this.listAttribute.Find(x => x.GroupID == GroupID);
                obj.ListValue[IndexLoop] = Value;
                
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public StringBuilder ToEDI(StringBuilder Edi)
        {
            StringBuilder EdiGroup = Edi;
            for (int i = 0; i < this.Repetition; i++)
            {
                for (int j = 0; j < this.listAttribute.Count; j++)
                {
                    this.listAttribute[j].ToEDI(EdiGroup, this.listAttribute[j].ListValue[i], true);
                }
            }
            return EdiGroup;
        }
        
    }
}
