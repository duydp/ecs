﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class MaterialsNorm
    {
        /// <summary>
        /// ....
        /// </summary>
        [XmlElement("Material")]
        public Product Material { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement("rateExchange")]
        public string RateExchange { get; set; }
        /// <summary>
        /// ....
        /// </summary> 
        [XmlElement("norm")]
        public string Norm { get; set; }
        /// <summary>
        /// ....
        /// </summary>
        [XmlElement("loss")]
        public string Loss { get; set; }
    }
}
