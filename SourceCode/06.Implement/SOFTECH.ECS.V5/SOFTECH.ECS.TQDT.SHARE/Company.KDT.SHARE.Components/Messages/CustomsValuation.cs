﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class CustomsValuation
    {
  
        
        /// <summary>
        /// Tổng chi phí khác(vận tải, bảo hiểm và các chi phí khác từ cảng xuất đến cảng nhập) ,n..16,2
        /// </summary>
        [XmlElement("exitToEntryCharge")]
        public string ExitToEntryCharge { get; set; }
        /// <summary>
        /// Chi phí vận tải n..16,2
        /// </summary>
        [XmlElement("freightCharge")]
        public string FreightCharge { get; set; }
        /// <summary>
        /// Phương pháp xác định giá n1
        /// </summary>
        [XmlElement("method")]
        public string Method { get; set; }
        /// <summary>
        /// Tổng các khoản phải cộng - tổng các khoản được trừ n..16,2
        /// </summary>
        [XmlElement("otherChargeDeduction")]
        public string OtherChargeDeduction { get; set; }
    }
}
