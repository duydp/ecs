﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu.WareHouse;
using Janus.Windows.GridEX;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseSPMaperManagementForm : Company.Interface.BaseForm
    {
        public T_KHOKETOAN_SANPHAM SP = new T_KHOKETOAN_SANPHAM();
        public T_KHOKETOAN_SANPHAM_MAP SPMap = new T_KHOKETOAN_SANPHAM_MAP();

        public WareHouseSPMaperManagementForm()
        {
            InitializeComponent();

            cbHopDong.DataSource = HopDong.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "ID");
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
            cbHopDong.SelectedIndex = -1;
        }

        private void WareHouseSPMaperManagementForm_Load(object sender, EventArgs e)
        {

        }
        private void BinDataSP()
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic("HOPDONG_ID = " + cbHopDong.Value, "");
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataSPMap()
        {
            try
            {
                grListSPMap.Refetch();
                grListSPMap.DataSource = SP.SPCollection;
                grListSPMap.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            BinDataSP();
        }

        private void grListSP_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SP = (T_KHOKETOAN_SANPHAM)i.GetRow().DataRow;
                        SP.SPCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionBy_KHOSP_ID(SP.ID);
                    }
                }
                BindDataSPMap();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grListSPMap_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                SPMap = new T_KHOKETOAN_SANPHAM_MAP();
                SPMap = (T_KHOKETOAN_SANPHAM_MAP)e.Row.DataRow;
                HopDong HD = HopDong.Load(SPMap.HOPDONG_ID);
                SP = T_KHOKETOAN_SANPHAM.Load(SPMap.KHOSP_ID);
                SP.SPCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionBy_KHOSP_ID(SP.ID);
                WareHouseSPMaperForm f = new WareHouseSPMaperForm();
                f.HD = HD;
                f.SP = SP;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            try
            {
                HopDong HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value));
                WareHouseNPLAutoMaperForm f = new WareHouseNPLAutoMaperForm();
                f.HD = HD;
                f.Type = "SP";
                f.ShowDialog(this);
                BinDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                HopDong HD = HopDong.Load(Convert.ToInt64(cbHopDong.Value));
                WareHouseSPMaperForm f = new WareHouseSPMaperForm();
                f.HD = HD;
                f.ShowDialog(this);
                BinDataSP();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSearchSP_Click(object sender, EventArgs e)
        {
            try
            {
                grListSP.Refetch();
                grListSP.DataSource = T_KHOKETOAN_SANPHAM.SelectCollectionDynamic("MASP LIKE '%" + txtMaSP.Text.ToString() + "%' AND HOPDONG_ID = " + cbHopDong.Value, "");
                grListSP.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearchSPMap_Click(object sender, EventArgs e)
        {
            try
            {
                grListSPMap.Refetch();
                grListSPMap.DataSource = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionDynamic("MASPMAP LIKE '%" + txtSPMap.Text.ToString() + "%' AND HOPDONG_ID = " + cbHopDong.Value, "");
                grListSPMap.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                List<T_KHOKETOAN_SANPHAM> ItemColl = new List<T_KHOKETOAN_SANPHAM>();
                if (grListSP.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_KHOKETOAN_SANPHAM)i.GetRow().DataRow);
                        }

                    }
                    string error = "";
                    string errorToTal = "";
                    foreach (T_KHOKETOAN_SANPHAM item in ItemColl)
                    {
                        if (item.ID > 0)
                        {
                            List<T_KHOKETOAN_PHIEUXNKHO_HANGHOA> HHCollection = T_KHOKETOAN_PHIEUXNKHO_HANGHOA.SelectCollectionDynamic("MAHANG ='" +item.MASP+ "'", "");
                            if (HHCollection.Count > 0)
                            {
                                foreach (T_KHOKETOAN_PHIEUXNKHO_HANGHOA HH in HHCollection)
                                {
                                    T_KHOKETOAN_PHIEUXNKHO PhieuXNK = T_KHOKETOAN_PHIEUXNKHO.Load(HH.PHIEUXNKHO_ID);
                                    error += "[" + HH.STT + "]-[" + HH.MAHANG + "]-[" + PhieuXNK.SOCT + "]-[" + PhieuXNK.NGAYCT.ToString("dd/MM/yyyy") + "]\n";                                    
                                }
                            }
                            else
                            {
                                List<T_KHOKETOAN_SANPHAM_MAP> SPMAPCollection = T_KHOKETOAN_SANPHAM_MAP.SelectCollectionBy_KHOSP_ID(item.ID);
                                foreach (T_KHOKETOAN_SANPHAM_MAP SP in SPMAPCollection)
                                {
                                    SP.Delete();
                                }
                                item.Delete();
                            }
                        }
                    }
                    if (!String.IsNullOrEmpty(error))
                    {
                        errorToTal += "MÃ SẢN PHẨM ĐÃ SỬ DỤNG TRONG PHIẾU XUẤT NHẬP KHO - KHÔNG ĐƯỢC XÓA .\n[STT]-[MÃ SẢN PHẨM]-[SỐ CHỨNG TỪ]-[NGÀY CHỨNG TỪ]\n" + error;
                        ShowMessageTQDT(errorToTal,false);
                    }
                    else
                    {
                        ShowMessageTQDT("Xóa thành công !",false);
                    }
                    btnSearchSP_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
