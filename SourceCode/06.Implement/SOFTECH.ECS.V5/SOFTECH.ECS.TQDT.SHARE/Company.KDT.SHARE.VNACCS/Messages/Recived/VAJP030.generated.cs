using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAJP030 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute SBC { get; set; }
public PropertiesAttribute SBN { get; set; }
public PropertiesAttribute SPC { get; set; }
public PropertiesAttribute ADS { get; set; }
public PropertiesAttribute SCC { get; set; }
public PropertiesAttribute SUN { get; set; }
public PropertiesAttribute SFN { get; set; }
public PropertiesAttribute SBE { get; set; }
public PropertiesAttribute DOS { get; set; }
public PropertiesAttribute ONO { get; set; }
public PropertiesAttribute FNC { get; set; }
public PropertiesAttribute PRT { get; set; }
public PropertiesAttribute OAC { get; set; }
public PropertiesAttribute OAN { get; set; }
public PropertiesAttribute PCT { get; set; }
public PropertiesAttribute PLN { get; set; }
public PropertiesAttribute PLD { get; set; }
public PropertiesAttribute EFD { get; set; }
public PropertiesAttribute EPD { get; set; }
public PropertiesAttribute CNO { get; set; }
public PropertiesAttribute EXP { get; set; }
public PropertiesAttribute EPC { get; set; }
public PropertiesAttribute EBN { get; set; }
public PropertiesAttribute ESN { get; set; }
public PropertiesAttribute EDN { get; set; }
public PropertiesAttribute ECN { get; set; }
public PropertiesAttribute ERC { get; set; }
public PropertiesAttribute EPN { get; set; }
public PropertiesAttribute EFX { get; set; }
public PropertiesAttribute EEM { get; set; }
public PropertiesAttribute ECC { get; set; }
public PropertiesAttribute IMC { get; set; }
public PropertiesAttribute IMN { get; set; }
public PropertiesAttribute BRN { get; set; }
public PropertiesAttribute BPC { get; set; }
public PropertiesAttribute AOI { get; set; }
public PropertiesAttribute BCC { get; set; }
public PropertiesAttribute BPN { get; set; }
public PropertiesAttribute BFX { get; set; }
public PropertiesAttribute IEM { get; set; }
public PropertiesAttribute TCC { get; set; }
public PropertiesAttribute MTT { get; set; }
public PropertiesAttribute MTN { get; set; }
public PropertiesAttribute IBC { get; set; }
public PropertiesAttribute IBN { get; set; }
public PropertiesAttribute PRN { get; set; }
public PropertiesAttribute QUL { get; set; }
public PropertiesAttribute QUT { get; set; }
public PropertiesAttribute QUH { get; set; }
public PropertiesAttribute CTL { get; set; }
public PropertiesAttribute CTT { get; set; }
public PropertiesAttribute CTH { get; set; }
public PropertiesAttribute NOC { get; set; }
public PropertiesAttribute AVL { get; set; }
public PropertiesAttribute SPN { get; set; }
public PropertiesAttribute TRS { get; set; }
public PropertiesAttribute AUL { get; set; }

public VAJP030()
        {
SBC = new PropertiesAttribute(13, typeof(string));
SBN = new PropertiesAttribute(300, typeof(string));
SPC = new PropertiesAttribute(7, typeof(string));
ADS = new PropertiesAttribute(300, typeof(string));
SCC = new PropertiesAttribute(2, typeof(string));
SUN = new PropertiesAttribute(20, typeof(string));
SFN = new PropertiesAttribute(20, typeof(string));
SBE = new PropertiesAttribute(210, typeof(string));
DOS = new PropertiesAttribute(8, typeof(DateTime));
ONO = new PropertiesAttribute(12, typeof(int));
FNC = new PropertiesAttribute(1, typeof(int));
PRT = new PropertiesAttribute(4, typeof(string));
OAC = new PropertiesAttribute(6, typeof(string));
OAN = new PropertiesAttribute(300, typeof(string));
PCT = new PropertiesAttribute(1, typeof(int));
PLN = new PropertiesAttribute(20, typeof(string));
PLD = new PropertiesAttribute(8, typeof(DateTime));
EFD = new PropertiesAttribute(8, typeof(DateTime));
EPD = new PropertiesAttribute(8, typeof(DateTime));
CNO = new PropertiesAttribute(35, typeof(string));
EXP = new PropertiesAttribute(70, typeof(string));
EPC = new PropertiesAttribute(9, typeof(string));
EBN = new PropertiesAttribute(35, typeof(string));
ESN = new PropertiesAttribute(35, typeof(string));
EDN = new PropertiesAttribute(35, typeof(string));
ECN = new PropertiesAttribute(35, typeof(string));
ERC = new PropertiesAttribute(2, typeof(string));
EPN = new PropertiesAttribute(20, typeof(string));
EFX = new PropertiesAttribute(20, typeof(string));
EEM = new PropertiesAttribute(210, typeof(string));
ECC = new PropertiesAttribute(2, typeof(string));
IMC = new PropertiesAttribute(13, typeof(string));
IMN = new PropertiesAttribute(300, typeof(string));
BRN = new PropertiesAttribute(35, typeof(string));
BPC = new PropertiesAttribute(7, typeof(string));
AOI = new PropertiesAttribute(300, typeof(string));
BCC = new PropertiesAttribute(2, typeof(string));
BPN = new PropertiesAttribute(20, typeof(string));
BFX = new PropertiesAttribute(20, typeof(string));
IEM = new PropertiesAttribute(210, typeof(string));
TCC = new PropertiesAttribute(2, typeof(string));
MTT = new PropertiesAttribute(2, typeof(string));
MTN = new PropertiesAttribute(75, typeof(string));
IBC = new PropertiesAttribute(6, typeof(string));
IBN = new PropertiesAttribute(35, typeof(string));
PRN = new PropertiesAttribute(35, typeof(string));
QUL = new PropertiesAttribute(150, typeof(string));
QUT = new PropertiesAttribute(8, typeof(DateTime));
QUH = new PropertiesAttribute(4, typeof(string));
CTL = new PropertiesAttribute(150, typeof(string));
CTT = new PropertiesAttribute(8, typeof(DateTime));
CTH = new PropertiesAttribute(4, typeof(string));
NOC = new PropertiesAttribute(2, typeof(int));
AVL = new PropertiesAttribute(250, typeof(string));
SPN = new PropertiesAttribute(35, typeof(string));
TRS = new PropertiesAttribute(996, typeof(string));
AUL = new PropertiesAttribute(300, typeof(string));
TongSoByte = 4839;
}

}public partial class EnumGroupID
    {
}

}
