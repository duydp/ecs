﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.KD
{
    public class NodeHuongDanThuTucHQ
    {		

        /// <summary>
        /// Tờ khai
        /// </summary>
        public const string Declaration = "Declaration";
        /// <summary>
        /// Loại chứng từ (Nhập khẩu=929, Xuất khẩu=930)
        /// </summary>
        public const string issuer = "issuer";
        /// <summary>
        /// Số tham chiếu tờ khai || Số hóa đơn || Số thông báo thuế
        /// </summary>
        public const string reference = "reference";
        /// <summary>
        /// Ngày khai báo || Ngày hóa đơn || Ngày ra thông báo thuế
        /// </summary>
        public const string issue = "issue";
        /// <summary>
        /// Chức năng (=31)
        /// </summary>
        public const string function = "function";
        /// <summary>
        /// Nơi khai báo
        /// </summary>
        public const string issueLocation = "issueLocation";
        /// <summary>
        /// Trạng thái đại lý
        /// </summary>
        public const string status = "status";
        /// <summary>
        /// Số tờ khai
        /// </summary>
        public const string customsReference = "customsReference";
        /// <summary>
        /// Ngày đăng ký
        /// </summary>
        public const string acceptance = "acceptance";
        /// <summary>
        /// Đơn vị HQ khai báo
        /// </summary>
        public const string declarationOffice = "declarationOffice";
        /// <summary>
        /// Đại lý
        /// </summary>
        public const string Agent = "Agent";
        /// <summary>
        /// Tên đại lý || Tên người nhập khẩu
        /// </summary>
        public const string name = "name";
        /// <summary>
        /// Mã đại lý || Mã người nhập khẩu
        /// </summary>
        public const string identity = "identity";
        /// <summary>
        /// Người nhập khẩu
        /// </summary>
        public const string Importer = "Importer";       
        /// <summary>
        /// Mã loại hình
        /// </summary>
        public const string natureOfTransaction = "natureOfTransaction";        
        /// <summary>
        /// Mã loại hóa đơn thương mại || Mã thông báo thuế (= 006) || Sắc thuế
        /// </summary>
        public const string type = "type";
        /// <summary>
        /// Ngày hết hạn thông báo thuế
        /// </summary>
        public const string expire = "expire";
        /// <summary>
        /// Kết quả phân luồng || Tài khoản kho bạc || Tên kho bạc || Chương, loại, khoản, mục
        /// </summary>
        public const string AdditionalInformation = "AdditionalInformation";
        /// <summary>
        /// Hướng dẫn thủ tục và kết quả phân luồng || Tài khoản kho bạc || Tên kho bạc || Nội dung thông tin
        /// </summary>
        public const string content = "content";      
        /// <summary>
        /// Ghi chu mo ta seqquen
        /// </summary>
        public const string sequence = "sequence";        
        /// <summary>
        /// Hóa đơn thương mại
        /// </summary>
        public const string Invoice = "Invoice";        
        /// <summary>
        /// Mã phân luồng || Mã loại thông tin || Mã loại thông tin || Loại thông tin
        /// </summary>
        public const string statement = "statement";        
        /// <summary>
        /// Thuế
        /// </summary>
        public const string DutyTaxFee = "DutyTaxFee";
        /// <summary>
        /// Tiền thuế
        /// </summary>
        public const string adValoremTaxBase = "adValoremTaxBase";
        /// <summary>
        /// Thông báo thuế
        /// </summary>
        public const string AdditionalDocument = "AdditionalDocument";
        
    }
}
