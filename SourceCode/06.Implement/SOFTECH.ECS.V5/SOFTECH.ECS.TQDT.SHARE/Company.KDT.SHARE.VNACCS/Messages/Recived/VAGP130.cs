﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAGP130 : BasicVNACC
    {

        public List<VAGP130_HANG> HangHoa { get; set; }
        public void LoadVAGP130(string strResult)
        {
            try
            {
                this.GetObject<VAGP130>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAGP130, false, VAGP130.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAGP130.TongSoByte);
                while (true)
                {
                    VAGP130_HANG ivaHang = new VAGP130_HANG();
                    ivaHang.GetObject<VAGP130_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAGP130_HANG", false, VAGP130_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAGP130_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAGP130_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAGP130_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAGP130_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
