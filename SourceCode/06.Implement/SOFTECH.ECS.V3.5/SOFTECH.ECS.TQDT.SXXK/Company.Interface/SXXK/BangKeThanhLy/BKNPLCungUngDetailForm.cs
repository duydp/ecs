using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BKNPLCungUngDetailForm : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public KDT_SXXK_BKNPLCungUng BKNPL = new KDT_SXXK_BKNPLCungUng();
        private KDT_SXXK_BKNPLCungUng_Detail NPL_Detail = new KDT_SXXK_BKNPLCungUng_Detail();
        public decimal luongSanPham=0;
        public BKNPLCungUngDetailForm()
        {
            InitializeComponent();
        }
        private void btnLuu_Click(object sender, EventArgs e)
        {
            try
            {
                if (BKNPL != null)
                {
                    GetNPL();
                    BKNPL.Npl_Detail_List.Add(NPL_Detail);
                    NPL_Detail = new KDT_SXXK_BKNPLCungUng_Detail();
                    dgList2.DataSource = BKNPL.Npl_Detail_List;
                    dgList2.Refetch();
                    SetNPL();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void LoadData()
        {
            if (BKNPL != null)
            {
                SetTKCungUng();
                dgList2.DataSource = BKNPL.Npl_Detail_List;
                dgList2.Refetch();
            }
            
        }
        private void GetNPL()
        {
            NPL_Detail.MaNPL = txtMaNPL.Text;
            NPL_Detail.TenNPL = txtTenNPL.Text;
            NPL_Detail.SoChungTu = txtSoChungTu.Text;
            NPL_Detail.NgayChungTu = clcNgayChungTu.Value;
            NPL_Detail.LuongNPL = Convert.ToDecimal(txtLuongNPL.Value);
            NPL_Detail.DonGiaTT = Convert.ToDecimal(txtDonGiaTT.Value);
            NPL_Detail.ThueXNK = Convert.ToDecimal(txtThueXNK.Value);
            NPL_Detail.NhomLyDo = cbbNhomLyDo.SelectedValue != null ? Convert.ToInt32(cbbNhomLyDo.SelectedValue.ToString()) : 1;
            NPL_Detail.GiaiTrinh = txtGiaiTrinh.Text;
        }
        private void SetNPL()
        {
            txtMaNPL.Text = NPL_Detail.MaNPL;
            txtTenNPL.Text = NPL_Detail.TenNPL;
            txtSoChungTu.Text = NPL_Detail.SoChungTu;
            clcNgayChungTu.Value = NPL_Detail.NgayChungTu;
            txtLuongNPL.Value = NPL_Detail.LuongNPL;
            txtDonGiaTT.Value = NPL_Detail.DonGiaTT;
            txtThueXNK.Value = NPL_Detail.ThueXNK;
            txtGiaiTrinh.Text = NPL_Detail.GiaiTrinh;
            cbbNhomLyDo.SelectedValue = NPL_Detail.NhomLyDo;
        }
        private void SetTKCungUng()
        {
            txtSoToKhai.Text = BKNPL.SoToKhaiXuat;
            txtMaSanPham.Text = BKNPL.MaSanPham;
            txtLuongSanPham.Value = luongSanPham;
            txtTenSanPham.Text = BKNPL.TenSanPham;
           
        }
        private void GetTKCungUng()
        {
            txtSoToKhai.Text = BKNPL.SoToKhaiXuat;
            txtMaSanPham.Text = BKNPL.MaSanPham;
            txtTenSanPham.Text = BKNPL.TenSanPham;
          
        }
        private void btnSave_BK_Click(object sender, EventArgs e)
        {
            try
            {
                if (BKNPL.Npl_Detail_List.Count > 0)
                {
                    if (BKNPL.InsertUpdatFull())
                        ShowMessage("Lưu thành công.", false);
                    else
                        ShowMessage("Lỗi khi lưu ", false);
                }
                else
                    ShowMessage("Chưa có nguyên phụ liệu cung ứng cho sản phẩm " + BKNPL.MaSanPham, false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi khi lưu: "+ex , false);
            }
           
        }

        private void BKNPLCungUngDetailForm_Load(object sender, EventArgs e)
        {
            LoadData();
            cbbNhomLyDo.SelectedIndexChanged += new EventHandler(cbbNhomLyDo_SelectedIndexChanged);
            cbbNhomLyDo.SelectedIndex = 0;
            
        }

        void cbbNhomLyDo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtGiaiTrinh.Text.Trim()))
            {
                if(cbbNhomLyDo.SelectedIndex == 0)
                    txtGiaiTrinh.Text = "Mua tại VN";
                else
                    txtGiaiTrinh.Text = "";
            }
        }


        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                NPL_Detail = (KDT_SXXK_BKNPLCungUng_Detail)dgList2.CurrentRow.DataRow;
                SetNPL();
            }
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {
            SelectNPLBySanPhamForm f = new SelectNPLBySanPhamForm();
            f.MaSanPham = txtMaSanPham.Text;
            f.ShowDialog();
            Company.BLL.SXXK.NguyenPhuLieu npl = f.NPL;
            txtMaNPL.Text = npl.Ma;
            txtTenNPL.Text = npl.Ten;
            NPL_Detail.DVT_ID = npl.DVT_ID;
            cbbNhomLyDo.SelectedValue = 1;
            Company.BLL.SXXK.DinhMuc dm = new Company.BLL.SXXK.DinhMuc();
            dm.MaSanPHam = txtMaSanPham.Text;
            dm.MaNguyenPhuLieu = npl.Ma;
            dm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            dm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            dm.Load();
            if (dm.DinhMucChung > 0)
                txtLuongNPL.Value = Convert.ToDecimal(txtLuongSanPham.Value) * dm.DinhMucChung;
            else
                txtLuongNPL.Value = Convert.ToDecimal(txtLuongSanPham.Value) * (dm.DinhMucSuDung + ( dm.DinhMucSuDung * dm.TyLeHaoHut/100));

           
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {

                GridEXSelectedItemCollection items = dgList2.SelectedItems;
                List<KDT_SXXK_BKNPLCungUng_Detail> ItemColl = new List<KDT_SXXK_BKNPLCungUng_Detail>();
                if (dgList2.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa NPL này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_SXXK_BKNPLCungUng_Detail)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_SXXK_BKNPLCungUng_Detail item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        BKNPL.Npl_Detail_List.Remove(item);
                    }
                    dgList2.Refetch();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
            npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            npl.Ma = txtMaNPL.Text;
            if (npl.Load())
            {
                txtMaNPL.Text = npl.Ma;
                txtTenNPL.Text = npl.Ten;
                NPL_Detail.DVT_ID = npl.DVT_ID;
            }

        }

      

    }
}