﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ToKhai;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Company.Interface.Report
{
    
    public partial class ReportViewTKXSXXKForm : BaseForm
    {
        public ToKhaiXuatSXXK ToKhaiChinhReport = new ToKhaiXuatSXXK();
        public PhuLucToKhaiXuatSXXK PhuLucReport = new PhuLucToKhaiXuatSXXK();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        System.Drawing.Printing.Margins Margin = new System.Drawing.Printing.Margins();
        public ReportViewTKXSXXKForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            this.Margin = GlobalSettings.MarginTKX;
            if (this.TKMD.HMDCollection.Count <= 9) btnConfig.Visible = false;
            else
            {
                int count = (this.TKMD.HMDCollection.Count - 1) / 15 + 1;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
            }
            cboToKhai.SelectedIndex = 0;
            this.ToKhaiChinhReport.Margins = this.Margin;
            this.ToKhaiChinhReport.TKMD = this.TKMD;
            this.ToKhaiChinhReport.BindReport();
            printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
            this.ToKhaiChinhReport.CreateDocument();

        }
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.TKMD = this.TKMD;
                this.ToKhaiChinhReport.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
                int begin = (cboToKhai.SelectedIndex - 1) * 15;
                int end = cboToKhai.SelectedIndex * 15;
                if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                for (int i = begin; i < end; i++)
                    HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                this.PhuLucReport = new PhuLucToKhaiXuatSXXK();
                this.PhuLucReport.SoToKhai = this.TKMD.SoToKhai;
                if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                    this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                this.PhuLucReport.HMDCollection = HMDReportCollection;
                this.PhuLucReport.BindReport();
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.Margin = printControl1.PrintingSystem.PageMargins;
                this.ToKhaiChinhReport.setVisibleImage(chkHinhNen.Checked);
                this.ToKhaiChinhReport.Margins = this.Margin;
                GlobalSettings.MarginTKX = this.Margin;
                GlobalSettings.RefreshKey();
                this.ToKhaiChinhReport.CreateDocument();
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //switch (cboExport.SelectedValue.ToString())
            //{
            //    case "pdf":
            //        if (cboToKhai.SelectedIndex == 0)
            //        {
            //            this.ToKhaiChinhReport.ptbImage.Visible = false;
            //            this.ToKhaiChinhReport.CreateDocument();
            //        }
            //        else
            //        {
            //            this.PhuLucReport.xrPictureBox1.Visible = false;
            //            this.PhuLucReport.CreateDocument();
            //        }
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { false });
            //        if (cboToKhai.SelectedIndex == 0)
            //        {
            //            this.ToKhaiChinhReport.ptbImage.Visible = true;
            //            this.ToKhaiChinhReport.CreateDocument();
            //        }
            //        else
            //        {
            //            this.PhuLucReport.xrPictureBox1.Visible = true;
            //            this.PhuLucReport.CreateDocument();
            //        }
            //        break;
            //    case "excel":
            //        if (cboToKhai.SelectedIndex == 0)
            //        {
            //            this.ToKhaiChinhReport.ptbImage.Visible = false;
            //            this.ToKhaiChinhReport.CreateDocument();
            //        }
            //        else
            //        {
            //            this.PhuLucReport.xrPictureBox1.Visible = false;
            //            this.PhuLucReport.CreateDocument();
            //        }
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { false });
            //        if (cboToKhai.SelectedIndex == 0)
            //        {
            //            this.ToKhaiChinhReport.ptbImage.Visible = true;
            //            this.ToKhaiChinhReport.CreateDocument();
            //        }
            //        else
            //        {
            //            this.PhuLucReport.xrPictureBox1.Visible = true;
            //            this.PhuLucReport.CreateDocument();
            //        }
            //        break;
            //}
        }

        private void btnQuickPrint_Click(object sender, EventArgs e)
        {
            //if (cboToKhai.SelectedIndex == 0)
            //{
            //    this.ToKhaiChinhReport.setVisibleImage(false);
            //    this.ToKhaiChinhReport.CreateDocument();
            //    printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
            //    this.ToKhaiChinhReport.setVisibleImage(true);
            //    this.ToKhaiChinhReport.CreateDocument();
            //}
            //else
            //{
            //    this.PhuLucReport.setVisibleImage(false);
            //    this.PhuLucReport.CreateDocument();
            //    printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
            //    this.PhuLucReport.setVisibleImage(true);
            //    this.PhuLucReport.CreateDocument();
            //}
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {
            CauHinhNhomHangSXXKForm f = new CauHinhNhomHangSXXKForm();
            f.HMDCollection = this.TKMD.HMDCollection;
            f.ShowDialog(this);
            this.TKMD.HMDCollection = f.HMDCollection;
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.TKMD = this.TKMD;
                this.ToKhaiChinhReport.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                List<HangMauDich> HMDReportCollection = new List<HangMauDich>();
                int begin = (cboToKhai.SelectedIndex - 1) * 15;
                int end = cboToKhai.SelectedIndex * 15;
                if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                for (int i = begin; i < end; i++)
                    HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                this.PhuLucReport = new PhuLucToKhaiXuatSXXK();
                this.PhuLucReport.SoToKhai = this.TKMD.SoToKhai;
                if (this.TKMD.NgayDangKy != new DateTime(1900, 1, 1))
                    this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                this.PhuLucReport.HMDCollection = HMDReportCollection;
                this.PhuLucReport.BindReport();
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }
        public void setVisibleButtonConfig(bool k)
        {
            btnConfig.Visible = k;
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.setVisibleImage(chkHinhNen.Checked);
                this.ToKhaiChinhReport.CreateDocument();
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
        }
    }
}