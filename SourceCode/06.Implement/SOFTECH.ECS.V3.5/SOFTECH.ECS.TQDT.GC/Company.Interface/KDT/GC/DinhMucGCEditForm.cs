﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Interface.KDT.SXXK;
using System.Data;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Windows.Forms;
using System.Collections.Generic;
using Company.QuanTri;

namespace Company.Interface.KDT.GC
{
    public partial class DinhMucGCEditForm : BaseForm
    {
        public Company.GC.BLL.KDT.GC.DinhMuc DMDetail = new Company.GC.BLL.KDT.GC.DinhMuc();                
        public DinhMucDangKy dmdk=new DinhMucDangKy();
        public string LoaiDinhMuc;
        List<Company.GC.BLL.GC.KDT_LenhSanXuat> LenhSanXuatCollection = new List<Company.GC.BLL.GC.KDT_LenhSanXuat>();
        List<Company.GC.BLL.GC.KDT_LenhSanXuat> LenhSanXuatSPCollection = new List<Company.GC.BLL.GC.KDT_LenhSanXuat>();
        List<Company.GC.BLL.GC.GC_SanPham> SPCollection = new List<Company.GC.BLL.GC.GC_SanPham>();
        List<Company.GC.BLL.GC.GC_NguyenPhuLieu> NPLCollection = new List<Company.GC.BLL.GC.GC_NguyenPhuLieu>();
        public bool isEdit = false;
        public bool isCancel = false;
        public LogHistory Log = new LogHistory();
        public String Caption;
        public bool IsChange;
        public DinhMucGCEditForm()
        {
            InitializeComponent();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            txtDonViTinhSP.DataSource = this._DonViTinh;
            txtDonViTinhSP.DisplayMember = "Ten";
            txtDonViTinhSP.ValueMember = "ID";

            txtDonViTinhSP.TextChanged -= new EventHandler(txt_TextChanged);
            txtDonViTinhSP.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            txtDonViTinhSP.TextChanged += new EventHandler(txt_TextChanged);

            txtDonViTinhNPL.DataSource = this._DonViTinh;
            txtDonViTinhNPL.DisplayMember = "Ten";
            txtDonViTinhNPL.ValueMember = "ID";

            txtDonViTinhNPL.TextChanged -= new EventHandler(txt_TextChanged);
            txtDonViTinhNPL.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            txtDonViTinhNPL.TextChanged += new EventHandler(txt_TextChanged);

            txtDonViTinhNPL.TextChanged -= new EventHandler(txt_TextChanged);
            txtDonViTinhSP.TextChanged -= new EventHandler(txt_TextChanged);

            this.FormClosing += DinhMucGCEditForm_FormClosing;
        }

        private void txtMaSP_Leave(object sender, EventArgs e)
        {

        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = dmdk.DMCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void txtMaSP_ButtonClick(object sender, EventArgs e)
        {            
            //DinhMucSelectSanPhamForm f = new DinhMucSelectSanPhamForm();
            //f.SPDetail.HopDong_ID = dmdk.ID_HopDong;      
            //HopDong hd = new HopDong();
            //hd.ID = dmdk.ID_HopDong;
            //f.HD = hd;
            //f.ShowDialog();
            //if (f.SPDetail!=null && f.SPDetail.Ma != "")
            //{
            //    txtMaSP.Text = f.SPDetail.Ma.Trim();
            //    txtTenSP.Text = f.SPDetail.Ten.Trim();
            //    txtMaHSSP.Text = f.SPDetail.MaHS.Trim();
            //    txtDonViTinhSP.SelectedValue = f.SPDetail.DVT_ID;
            //    foreach (Company.GC.BLL.GC.KDT_LenhSanXuat item in LenhSanXuatCollection)
            //    {
            //        item.SPCollection = Company.GC.BLL.GC.KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
            //        foreach (Company.GC.BLL.GC.KDT_LenhSanXuat_SP ite in item.SPCollection)
            //        {
            //            if (ite.MaSanPham == txtMaSP.Text.ToString())
            //            {
            //                txtMaDDSX.Text = item.SoLenhSanXuat.ToString();
            //                break;
            //            }
            //        }
            //    }
            //    txtMaNPL.Focus();
            //}
        }

        private void txtMaNPL_ButtonClick(object sender, EventArgs e)
        {            
            //DinhMucSelectNguyenPhuLieuForm f = new DinhMucSelectNguyenPhuLieuForm();            
            //f.isBrower = true;
            //f.npl.HopDong_ID = dmdk.ID_HopDong;
            //HopDong hd = new HopDong();
            //hd.ID = dmdk.ID_HopDong;
            //f.HD = hd;
            //f.ShowDialog();
            //if (f.npl!=null && f.npl.Ma != "")
            //{
            //    txtMaNPL.Text = f.npl.Ma.Trim();
            //    txtTenNPL.Text = f.npl.Ten.Trim();
            //    txtMaHSNPL.Text = f.npl.MaHS.Trim();
            //    txtDonViTinhNPL.SelectedValue = f.npl.DVT_ID;
            //    txtDinhMuc.Focus();
            //}
        }
        private void LoadDataDefault()
        {
            try
            {
                NPLCollection = Company.GC.BLL.GC.GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(dmdk.ID_HopDong);
                cbbMaNPL.DataSource = NPLCollection;
                cbbMaNPL.DisplayMember = "Ma";
                cbbMaNPL.ValueMember = "Ma";

                SPCollection = Company.GC.BLL.GC.GC_SanPham.SelectCollectionBy_HopDong_ID(dmdk.ID_HopDong);
                cbbMaSP.DataSource = SPCollection;
                cbbMaSP.DisplayMember = "Ma";
                cbbMaSP.ValueMember = "Ma";

                LenhSanXuatCollection = Company.GC.BLL.GC.KDT_LenhSanXuat.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "' AND HopDong_ID = " + dmdk.ID_HopDong + "", "ID");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void DinhMucGCEditForm_Load(object sender, EventArgs e)
        {
            txtDinhMuc.TextChanged -= new EventHandler(txt_TextChanged);
            txtDinhMuc.DecimalDigits = Convert.ToInt32(GlobalSettings.SoThapPhan.DinhMuc);
            txtDinhMuc.TextChanged += new EventHandler(txt_TextChanged);

            dgList.Tables[0].Columns["DinhMucSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.DinhMuc;

            txtTyLeHH.TextChanged -= new EventHandler(txt_TextChanged);
            txtTyLeHH.DecimalDigits = Convert.ToInt32(GlobalSettings.SoThapPhan.TLHH);
            txtTyLeHH.TextChanged += new EventHandler(txt_TextChanged);
            
            dgList.Tables[0].Columns["TyLeHaoHut"].FormatString = "N" + GlobalSettings.SoThapPhan.TLHH;
            dgList.Tables[0].Columns["NPL_TuCungUng"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            Caption = this.Text;

            LoadDataDefault();
            {            
                SetData();
            }
            if (this.OpenType==OpenFormType.View)
            {
                btnAdd.Enabled = false;
                btnDelete.Enabled = false;
                copyDinhMuc.Enabled = false;
                uiButton1.Enabled = false;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
            if (isEdit || isCancel)
            {
                btnSelect.Enabled = true;
            }
            else
            {
                btnSelect.Enabled = false;
            }
            BindData();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.DMDetail = null;
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DinhMucCollection dmcoll = new DinhMucCollection();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count < 0) return;
            //if (showMsg("MSG_DEL01", true) == "Yes")
            if (ShowMessage("Doanh nghiệp có muốn xóa định mức của sản phẩm này không?", true) == "Yes")
            {                
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.DinhMuc dm = (Company.GC.BLL.KDT.GC.DinhMuc)row.GetRow().DataRow;
                        if (dm.ID > 0)
                        {
                            try
                            {
                                dm.Delete(dmdk.ID_HopDong);
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                        dmcoll.Add(dm);                       
                       
                    }
                }
                foreach (Company.GC.BLL.KDT.GC.DinhMuc dmm in dmcoll)
                {
                    dmdk.DMCollection.Remove(dmm);
                }
                BindData();
                ShowMessage("XÓA THÀNH CÔNG",false);
                Log.LogDinhMuc(dmdk, dmcoll, MessageTitle.XoaDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                this.SetChange(true);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                //Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateChoose(cbbMaNPL, errorProvider, "MÃ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenNPL, errorProvider, "TÊN/MÔ TẢ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHSNPL, errorProvider, "MÃ HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(txtDonViTinhNPL, errorProvider, "ĐƠN VỊ TÍNH", isOnlyWarning);

                isValid &= ValidateControl.ValidateChoose(cbbMaSP, errorProvider, "MÃ SẢN PHẨM", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenSP, errorProvider, "TÊN/MÔ TẢ SẢN PHẨM", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHSSP, errorProvider, "MÃ HS", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(txtDonViTinhSP, errorProvider, "ĐƠN VỊ TÍNH", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtMaDDSX, errorProvider, "MÃ ĐỊNH DANH CỦA LỆNH SẢN XUẤT", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbLenhSanXuat, errorProvider, "MÃ ĐỊNH DANH CỦA LỆNH SẢN XUẤT", isOnlyWarning);
                if (LoaiDinhMuc=="1")
                {
                    if (Convert.ToDecimal(txtTyLeHH.Value)>0)
                    {
                        errorProvider.SetError(txtTyLeHH,"CHỈ ĐƯỢC NHẬP KHI LOẠI ĐỊNH MỨC LÀ ĐỊNH MỨC KỸ THUẬT .");
                    }
                }
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (dmdk.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dmdk.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || dmdk.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void SetData()
        {
            try
            {
                cbbMaNPL.TextChanged -= new EventHandler(txt_TextChanged);
                cbbMaNPL.Value = DMDetail.MaNguyenPhuLieu;
                cbbMaNPL.TextChanged += new EventHandler(txt_TextChanged);

                cbbMaSP.TextChanged -= new EventHandler(txt_TextChanged);
                cbbMaSP.Value = DMDetail.MaSanPham;
                cbbMaSP.TextChanged += new EventHandler(txt_TextChanged);

                cbbLenhSanXuat.TextChanged -= new EventHandler(txt_TextChanged);
                cbbLenhSanXuat.Value = DMDetail.MaDDSX;
                cbbLenhSanXuat.TextChanged += new EventHandler(txt_TextChanged);

                txtDinhMuc.TextChanged -= new EventHandler(txt_TextChanged);
                txtDinhMuc.Text = DMDetail.DinhMucSuDung.ToString();
                txtDinhMuc.TextChanged += new EventHandler(txt_TextChanged);

                txtTyLeHH.TextChanged -= new EventHandler(txt_TextChanged);
                txtTyLeHH.Text = DMDetail.TyLeHaoHut.ToString();
                txtTyLeHH.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void GetData()
        {
            try
            {
                DMDetail.MaSanPham = cbbMaSP.Value.ToString().Trim();
                DMDetail.MaDDSX = cbbLenhSanXuat.Value.ToString();
                DMDetail.MaNguyenPhuLieu = cbbMaNPL.Value.ToString().Trim();
                DMDetail.TyLeHaoHut = Convert.ToDecimal(txtTyLeHH.Text);
                DMDetail.DinhMucSuDung = Convert.ToDecimal(txtDinhMuc.Text);
                DMDetail.DVT_ID = txtDonViTinhNPL.SelectedValue.ToString(); ;
                DMDetail.TenNPL = txtTenNPL.Text.Trim();
                DMDetail.TenSanPham = txtTenSP.Text.Trim();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            //txtMaSP_Leave(null, null);
            //txtMaNPL_Leave(null, null);
            //txtMaNPL.Focus();
            if (!ValidateForm(false))
                return;
            checkExitsDinhMucAndSTTHang();
            
        }
        private void checkExitsDinhMucAndSTTHang()
        {
            if (isEdit || isCancel)
            {
                Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
                dmDuyet.HopDong_ID = dmdk.ID_HopDong;
                dmDuyet.MaSanPham = cbbMaSP.Value.ToString();
                dmDuyet.MaNguyenPhuLieu = cbbMaNPL.Value.ToString().Trim();
                dmDuyet.LenhSanXuat_ID = Company.GC.BLL.GC.KDT_LenhSanXuat.GetID(cbbLenhSanXuat.Value.ToString(),dmdk.ID_HopDong);
                if (!dmDuyet.CheckExitDMbySPandNPL())
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", String.Format("Sản phẩm: {0} chưa được khai báo định mức .Chỉ những sản phẩm đã khai báo định mức mới được phép khai báo sửa hoặc hủy", cbbMaSP.Value.ToString().Trim()), false);
                    //showMsg("MSG_240230", txtMaSP.Text.Trim());
                    return;
                }
            }
            else
            {
                long id = dmdk.GetIDDinhMucExit(cbbMaSP.Value.ToString().Trim(), dmdk.ID, this.dmdk.ID_HopDong,cbbLenhSanXuat.Value.ToString());
                if (id > 0)
                {
                    string msg = "";
                    DinhMucDangKy dm = DinhMucDangKy.Load(id);
                    msg += "\nSỐ TIẾP NHẬN : " + dm.SoTiepNhan.ToString();
                    msg += "\nNGÀY TIẾP NHẬN : " + dm.NgayTiepNhan.ToString("dd-MM-yyyy HH:mm:ss");
                    int trangThaiXuly = dm.TrangThaiXuLy;
                    switch (trangThaiXuly)
                    {
                        case 1:
                            msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strDADUYET;
                            break;
                        case 2:
                            msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strKHONGPHEDUYET;
                            break;
                        case 5:
                            msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strSUATKDADUYET;
                            break;
                        case -1:
                            msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strCHUAKHAIBAO;
                            break;
                        case 0:
                            msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strCHODUYET;
                            break;
                        case -2:
                            msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strCHODUYET;
                            break;
                        case 10:
                            msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strDAHUY;
                            break;
                        case 11:
                            msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strCHOHUY;
                            break;
                        default:
                            break;
                    }
                    ShowMessageTQDT("Thông báo từ hệ thống", String.Format("Sản phẩm: {0} đã được khai báo trong danh sách định mức với\nID = {1}\n{2}", cbbMaSP.Value.ToString().Trim(), id.ToString(), msg), false);
                    //showMsg("MSG_2702034", new string[] { txtMaSP.Text.Trim(),id.ToString() + msg });
                    return;
                }
                else if (id < 0)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", String.Format("Sản phẩm: {0} đã được khai báo định mức", cbbMaSP.Value.ToString().Trim()), false);
                    //showMsg("MSG_240230", txtMaSP.Text.Trim());
                    return;
                }
                Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
                dmDuyet.HopDong_ID = dmdk.ID_HopDong;
                dmDuyet.MaSanPham = cbbMaSP.Value.ToString();
                dmDuyet.MaNguyenPhuLieu = cbbMaNPL.Value.ToString().Trim();
                dmDuyet.LenhSanXuat_ID = Company.GC.BLL.GC.KDT_LenhSanXuat.GetID(cbbLenhSanXuat.Value.ToString(),dmdk.ID_HopDong);
                if (dmDuyet.CheckExitDMbySPandNPL())
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", String.Format("Sản phẩm: {0} đã được khai báo định mức", cbbMaSP.Value.ToString().Trim()), false);
                    //showMsg("MSG_240230", txtMaSP.Text.Trim());
                    return;
                }
            }
            dmdk.DMCollection.Remove(DMDetail);                   
            foreach (Company.GC.BLL.KDT.GC.DinhMuc dm in dmdk.DMCollection)
            {
                if (dm.MaNguyenPhuLieu == cbbMaNPL.Value.ToString().Trim() && dm.MaSanPham == cbbMaNPL.Value.ToString().Trim())
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", String.Format("Sản phẩm : {0} và nguyên phụ liệu : {1} đã được nhập liệu định mức trên lưới", cbbMaSP.Value.ToString().Trim(), cbbMaNPL.Value.ToString().Trim()), false);
                    //showMsg("MSG_2702035");
                    if(DMDetail.MaSanPham.Trim().Length>0)
                        dmdk.DMCollection.Add(DMDetail);
                    return;
                }
            }
            //foreach (Company.GC.BLL.GC.KDT_LenhSanXuat item in LenhSanXuatCollection)
            //{
            //    bool isExits = false;
            //    item.SPCollection = Company.GC.BLL.GC.KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
            //    foreach (Company.GC.BLL.GC.KDT_LenhSanXuat_SP ite in item.SPCollection)
            //    {
            //        if (ite.MaSanPham == cbbMaSP.Value.ToString())
            //        {
            //            txtMaDDSX.Text = item.SoLenhSanXuat.ToString();
            //            isExits = true;
            //            break;
            //        }
            //    }
            //    if (!isExits)
            //    {
            //        errorProvider.SetError(txtMaDDSX, "Mã định danh của Lệnh sản xuất  này chưa được đăng ký hoặc cập nhật");
            //        return;    
            //    }
            //}
            GetData();
            dmdk.DMCollection.Add(DMDetail);            
            reset();
            this.SetChange(true);
        }
        private void reset()
        {
            cbbMaNPL.TextChanged -= new EventHandler(txt_TextChanged);
            cbbMaNPL.Text = "";
            cbbMaNPL.TextChanged += new EventHandler(txt_TextChanged);

            txtTenNPL.TextChanged -= new EventHandler(txt_TextChanged);
            txtTenNPL.Text = "";
            txtTenNPL.TextChanged += new EventHandler(txt_TextChanged);

            txtMaHSNPL.TextChanged -= new EventHandler(txt_TextChanged);
            txtMaHSNPL.Text = "";
            txtMaHSNPL.TextChanged += new EventHandler(txt_TextChanged);

            txtDinhMuc.TextChanged -= new EventHandler(txt_TextChanged);
            txtDinhMuc.Text = "0";
            txtDinhMuc.TextChanged += new EventHandler(txt_TextChanged);

            txtTyLeHH.TextChanged -= new EventHandler(txt_TextChanged);
            txtTyLeHH.Text = "0";
            txtTyLeHH.TextChanged += new EventHandler(txt_TextChanged);

            txtDinhMuc.TextChanged -= new EventHandler(txt_TextChanged);
            txtDinhMuc.Text = DMDetail.DinhMucSuDung.ToString();
            txtDinhMuc.TextChanged += new EventHandler(txt_TextChanged);

            txtTyLeHH.TextChanged -= new EventHandler(txt_TextChanged);
            txtTyLeHH.Text = DMDetail.TyLeHaoHut.ToString();
            txtTyLeHH.TextChanged += new EventHandler(txt_TextChanged);

            BindData();
            DMDetail = new Company.GC.BLL.KDT.GC.DinhMuc();
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                DMDetail = (DinhMuc)e.Row.DataRow;
                SetData();
            }
        }

        private void dgList_DeletingRecord(object sender, Janus.Windows.GridEX.RowActionCancelEventArgs e)
        {
            
        }

        private void txtMaNPL_Leave(object sender, EventArgs e)
        {
            //if (txtMaNPL.Text.Trim() == "")
            //    return;
            //Company.GC.BLL.GC.NguyenPhuLieu npl = (new Company.GC.BLL.GC.NguyenPhuLieu());
            //npl.Ma = txtMaNPL.Text.Trim();
            //npl.HopDong_ID = dmdk.ID_HopDong;
            //npl.Load();
            //if (npl != null && npl.Ten.Length > 0)
            //{
            //    txtMaNPL.Text = npl.Ma;
            //    txtTenNPL.Text = npl.Ten.Trim();
            //    txtMaHSNPL.Text = npl.MaHS.Trim();
            //    txtDonViTinhNPL.SelectedValue = npl.DVT_ID;
            //    txtDinhMuc.Focus();

            //    error.SetError(txtMaHSNPL , null);
            //    error.SetError(txtMaNPL, null);
            //    error.SetError(txtTenNPL , null);
               
            //}
            //else
            //{
            //    error.SetError(txtMaNPL,setText("Không tồn tại nguyên phụ liệu này.","THIS VALUE IS NOT EXIST"));
            //    txtMaNPL.Clear();
            //    txtMaNPL.Focus();
            //    return;
            //}
            //if (txtMaNPL.Text.Trim() == "")
            //    return;
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (ShowMessage("Doanh nghiệp có muốn xóa định mức này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    DinhMucCollection ItemDelete = new DinhMucCollection();
                    foreach (GridEXSelectedItem row in items)
                    {
                        if (row.RowType == RowType.Record)
                        {
                            Company.GC.BLL.KDT.GC.DinhMuc dm = (Company.GC.BLL.KDT.GC.DinhMuc)row.GetRow().DataRow;
                            if (dm.ID > 0)
                            {
                                dm.Delete(dmdk.ID_HopDong);
                                ItemDelete.Add(dm);
                            }
                        }
                    }
                    foreach (Company.GC.BLL.KDT.GC.DinhMuc dmm in ItemDelete)
                    {
                        dmdk.DMCollection.Remove(dmm);
                    }
                    BindData();
                    ShowMessage("XÓA THÀNH CÔNG", false);
                    Log.LogDinhMuc(dmdk, ItemDelete, MessageTitle.XoaDinhMuc, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    this.SetChange(true);
                }
                else
                {
                    e.Cancel = true;
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void copyDinhMuc_Click(object sender, EventArgs e)
        {
            #region
            //if (cbbMaSP.Value.ToString().Trim() == "")
            //{
            //    ShowMessage("Doanh nghiệp chưa chọn sản phẩm để sao chép định mức", false);
            //    //showMsg("MSG_2702036");           
            //    return;
            //}
            //if (dgList.GetRow() != null)
            //{
            //    if (dgList.GetRow().RowType == RowType.Record)
            //    {
            //        DinhMuc dm = (DinhMuc)dgList.GetRow().DataRow;
            //        if (dm.MaSanPham.ToUpper().Trim() == cbbMaSP.Value.ToString().Trim().ToUpper())
            //        {
            //            ShowMessageTQDT("Thông báo từ hệ thống", String.Format("Sản phẩm : {0} đã được nhập liệu định mức trên lưới", cbbMaSP.Value.ToString().Trim()), false);
            //            //showMsg("MSG_2702037", txtMaSP.Text.Trim());
            //            return;
            //        }
            //        long id = dmdk.GetIDDinhMucExit(cbbMaSP.Value.ToString().Trim(), dmdk.ID, this.dmdk.ID_HopDong);
            //        if (id > 0)
            //        {
            //            string msg = "";
            //            DinhMucDangKy dmdkTemp = DinhMucDangKy.Load(id);
            //            msg += "\nSỐ TIẾP NHẬN : " + dmdkTemp.SoTiepNhan.ToString();
            //            msg += "\nNGÀY TIẾP NHẬN : " + dmdkTemp.NgayTiepNhan.ToString("dd-MM-yyyy HH:mm:ss");
            //            int trangThaiXuly = dmdkTemp.TrangThaiXuLy;
            //            switch (trangThaiXuly)
            //            {
            //                case 1:
            //                    msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strDADUYET;
            //                    break;
            //                case 2:
            //                    msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strKHONGPHEDUYET;
            //                    break;
            //                case 5:
            //                    msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strSUATKDADUYET;
            //                    break;
            //                case -1:
            //                    msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strCHUAKHAIBAO;
            //                    break;
            //                case 0:
            //                    msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strCHODUYET;
            //                    break;
            //                case -2:
            //                    msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strCHODUYET;
            //                    break;
            //                case 10:
            //                    msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strDAHUY;
            //                    break;
            //                case 11:
            //                    msg += "\nTRẠNG THÁI XỬ LÝ : " + TrangThaiXuLy.strCHOHUY;
            //                    break;
            //                default:
            //                    break;
            //            }
            //            ShowMessageTQDT("Thông báo từ hệ thống", String.Format("Sản phẩm: {0} đã được khai báo trong danh sách định mức với\nID = {1}\n{2}", cbbMaSP.Value.ToString().Trim(), id.ToString(), msg), false);
            //            //showMsg("MSG_2702034", new string[] { txtMaSP.Text.Trim(), id.ToString()});
            //            return;
            //        }
            //        else if (id < 0)
            //        {
            //            ShowMessageTQDT("Thông báo từ hệ thống", String.Format("Sản phẩm: {0} đã được khai báo định mức", cbbMaSP.Value.ToString().Trim()), false);
            //            //showMsg("MSG_240230", txtMaSP.Text.Trim());
            //            return;
            //        }
            //        Company.GC.BLL.GC.DinhMuc dmDuyet = new Company.GC.BLL.GC.DinhMuc();
            //        dmDuyet.HopDong_ID = dmdk.ID_HopDong;
            //        dmDuyet.MaSanPham = cbbMaSP.Value.ToString().Trim();
            //        if (dmDuyet.CheckExitsDinhMucSanPham())
            //        {
            //            ShowMessageTQDT("Thông báo từ hệ thống", String.Format("Sản phẩm: {0} đã được khai báo định mức", cbbMaSP.Value.ToString().Trim()), false);
            //            //showMsg("MSG_240230", txtMaSP.Text.Trim());
            //            return;
            //        }
            //        DinhMucCollection DMCollectionCopy = new DinhMucCollection();
            //        foreach (DinhMuc dmCopy in dmdk.DMCollection)
            //        {
            //            if (dmCopy.MaSanPham.ToUpper().Trim() == dm.MaSanPham.ToUpper().Trim())
            //            {
            //                DinhMuc dmSaoChep = new DinhMuc();
            //                dmSaoChep.MaSanPham = cbbMaSP.Value.ToString().Trim();
            //                dmSaoChep.MaNguyenPhuLieu = dmCopy.MaNguyenPhuLieu;
            //                dmSaoChep.NPL_TuCungUng = dmCopy.NPL_TuCungUng;
            //                dmSaoChep.TenNPL = dmCopy.TenNPL;
            //                dmSaoChep.TenSanPham = dmCopy.TenSanPham;
            //                dmSaoChep.TyLeHaoHut = dmCopy.TyLeHaoHut;
            //                dmSaoChep.GhiChu = dmCopy.GhiChu;
            //                dmSaoChep.DVT_ID = dmCopy.DVT_ID;
            //                dmSaoChep.DinhMucSuDung = dmCopy.DinhMucSuDung;
            //                DMCollectionCopy.Add(dmSaoChep);
            //            }
            //        }
            //        CopyDinhMucGCForm copyDmForm = new CopyDinhMucGCForm();
            //        copyDmForm.DMCollectionCopy=DMCollectionCopy;
            //        copyDmForm.DMDangKy=dmdk;
            //        if (dmdk.TrangThaiXuLy ==TrangThaiXuLy.CHUA_KHAI_BAO)
            //        {
            //            copyDmForm.OpenType = OpenFormType.Edit;
            //        }
            //        else
            //            copyDmForm.OpenType = OpenFormType.View;
            //        copyDmForm.ShowDialog();
            //        BindData();
            //    }
            //}
            #endregion
            if (cbbMaSP.Value.ToString().Trim() == "")
            {
                ShowMessage("Doanh nghiệp chưa chọn Sản phẩm để sao chép định mức", false);       
                return;
            }
            if (cbbLenhSanXuat.Value.ToString().Trim() == "")
            {
                ShowMessage("Doanh nghiệp chưa chọn Lệnh sản xuất của sản phẩm .", false);
                return;
            }
            SaoChepDinhMucGCForm f = new SaoChepDinhMucGCForm();            
            DinhMucCollection DMCollectionTransfer = new DinhMucCollection();
            foreach (DinhMuc item in dmdk.DMCollection)
            {
                if (item.MaSanPham !=cbbMaSP.Value.ToString())
                {
                    DMCollectionTransfer.Add(item);
                }
            }
            f.DMCollectionTransfer = DMCollectionTransfer;
            f.DMDangKy = dmdk;
            f.MaSanPham = cbbMaSP.Value.ToString();
            f.ShowDialog(this);
            if (f.DMCollectionCopy.Count >0)
            {
                foreach (DinhMuc item in f.DMCollectionCopy)
                {
                    DMDetail = new DinhMuc();
                    DMDetail.MaSanPham = cbbMaSP.Value.ToString();
                    DMDetail.MaDDSX = cbbLenhSanXuat.Value.ToString();
                    DMDetail.MaNguyenPhuLieu = item.MaNguyenPhuLieu;
                    DMDetail.TyLeHaoHut = item.TyLeHaoHut;
                    DMDetail.DinhMucSuDung = item.DinhMucSuDung;
                    DMDetail.DVT_ID = item.DVT_ID;
                    DMDetail.TenNPL = item.TenNPL;
                    DMDetail.TenSanPham = txtTenSP.Text.ToString();
                    dmdk.DMCollection.Add(DMDetail);
                }
                ShowMessageTQDT("Sao chép thành công",false);
                this.SetChange(true);
            }
            BindData();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {

            if (cbbMaSP.Value.ToString().Trim() == "")
            {
                ShowMessage("Doanh nghiệp chưa chọn sản phẩm để nhập định mức", false);
                //showMsg("MSG_2702036");             
                return;
            }

            CopyDinhMucGCDaDuyetForm f = new CopyDinhMucGCDaDuyetForm();
            HopDong HD=new HopDong();
            HD.ID=dmdk.ID_HopDong;
            HD = HopDong.Load(HD.ID);
            f.HD = HD;
            f.ShowDialog();
            if (f.SP != null)
            {
                DataTable dsDinhMuc = new Company.GC.BLL.GC.DinhMuc().getDinhMuc(HD.ID, f.SP.Ma);
                foreach (DataRow row in dsDinhMuc.Rows)
                {
                    DinhMuc dm = new DinhMuc();
                    dm.DinhMucSuDung = Convert.ToDecimal(row["DinhMucSuDung"]);
                    dm.MaNguyenPhuLieu = row["MaNguyenPhuLieu"].ToString();
                    dm.MaSanPham = cbbMaSP.Value.ToString().Trim();
                    dm.TyLeHaoHut = Convert.ToDecimal(row["TyLeHaoHut"]);
                    dm.TenNPL = row["TenNPL"].ToString();
                    dm.TenSanPham = txtTenSP.Text.Trim();
                    dm.DVT_ID = row["DVT_ID"].ToString();                    
                    dmdk.DMCollection.Add(dm);
                }
                BindData();
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //try
            //{
            //    if (txtMaDDSX.Text.Length ==0)
            //    {
            //        ShowMessageTQDT("Thông báo từ hệ thống","Doanh nghiệp chưa nhập mã định danh ", false);
            //        return;
            //    }
            //    foreach (Company.GC.BLL.GC.KDT_LenhSanXuat item in LenhSanXuatCollection)
            //    {
            //        bool isExits = false;
            //        item.SPCollection = Company.GC.BLL.GC.KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
            //        foreach (Company.GC.BLL.GC.KDT_LenhSanXuat_SP ite in item.SPCollection)
            //        {
            //            if (ite.MaSanPham == cbbMaSP.Value.ToString())
            //            {
            //                txtMaDDSX.Text = item.SoLenhSanXuat.ToString();
            //                isExits = true;
            //                break;
            //            }
            //        }
            //        if (!isExits)
            //        {
            //            errorProvider.SetError(txtMaDDSX, "Mã định danh của Lệnh sản xuất  này chưa được đăng ký hoặc cập nhật .");
            //            return;
            //        }
            //    }
            //    foreach (DinhMuc dm in dmdk.DMCollection)
            //    {
            //        if (cbbMaSP.Value.ToString() == dm.MaSanPham)
            //        {
            //            dm.MaDDSX = txtMaDDSX.Text;   
            //        }
            //    }
            //    ShowMessageTQDT("Thông báo từ hệ thống","Sao chép thành công ",false);
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            DMDetail = (DinhMuc)i.GetRow().DataRow;
                            SetData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }

        private void cbbMaSP_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaSP.Value != null)
                {
                    LenhSanXuatSPCollection = new List<Company.GC.BLL.GC.KDT_LenhSanXuat>();
                    foreach (Company.GC.BLL.GC.GC_SanPham item in SPCollection)
                    {
                        if (item.Ma == cbbMaSP.Value.ToString())
                        {
                            txtTenSP.TextChanged -= new EventHandler(txt_TextChanged);
                            txtTenSP.Text = item.Ten.ToString();
                            txtTenSP.TextChanged += new EventHandler(txt_TextChanged);

                            txtMaHSSP.TextChanged -= new EventHandler(txt_TextChanged);
                            txtMaHSSP.Text = item.MaHS;
                            txtMaHSSP.TextChanged += new EventHandler(txt_TextChanged);

                            txtDonViTinhSP.TextChanged -= new EventHandler(txt_TextChanged);
                            txtDonViTinhSP.SelectedValue = item.DVT_ID;
                            txtDonViTinhSP.TextChanged += new EventHandler(txt_TextChanged);

                            foreach (Company.GC.BLL.GC.KDT_LenhSanXuat it in LenhSanXuatCollection)
                            {
                                it.SPCollection = Company.GC.BLL.GC.KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(it.ID);
                                foreach (Company.GC.BLL.GC.KDT_LenhSanXuat_SP ite in it.SPCollection)
                                {
                                    if (ite.MaSanPham == cbbMaSP.Value.ToString())
                                    {
                                        //txtMaDDSX.Text = it.SoLenhSanXuat.ToString();
                                        LenhSanXuatSPCollection.Add(it);
                                    }
                                }
                            }
                        }
                    }
                    cbbLenhSanXuat.DataSource = LenhSanXuatSPCollection;
                    if (LenhSanXuatSPCollection.Count >=1)
                    {
                        cbbLenhSanXuat.SelectedIndex = 0;
                    }
                    cbbLenhSanXuat.DisplayMember = "SoLenhSanXuat";
                    cbbLenhSanXuat.ValueMember = "SoLenhSanXuat";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaNPL_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaNPL.Value != null)
                {
                    foreach (Company.GC.BLL.GC.GC_NguyenPhuLieu item in NPLCollection)
                    {
                        if (item.Ma == cbbMaNPL.Value.ToString())
                        {
                            txtTenNPL.TextChanged -= new EventHandler(txt_TextChanged);
                            txtTenNPL.Text = item.Ten.ToString();
                            txtTenNPL.TextChanged += new EventHandler(txt_TextChanged);

                            txtMaHSNPL.TextChanged -= new EventHandler(txt_TextChanged);
                            txtMaHSNPL.Text = item.MaHS;
                            txtMaHSNPL.TextChanged += new EventHandler(txt_TextChanged);

                            txtDonViTinhNPL.TextChanged -= new EventHandler(txt_TextChanged);
                            txtDonViTinhNPL.SelectedValue = item.DVT_ID;
                            txtDonViTinhNPL.TextChanged += new EventHandler(txt_TextChanged);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaDDSX_ButtonClick(object sender, EventArgs e)
        {
            LenhSanXuatSPForm lenhSanXuatSPForm = new LenhSanXuatSPForm();
            HopDong HD = new HopDong();
            HD = HopDong.Load(dmdk.ID_HopDong);
            lenhSanXuatSPForm.HD = HD;
            lenhSanXuatSPForm.ShowDialog(this);
            LoadDataDefault();
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            try
            {
                DinhMucSelectSanPhamForm f = new DinhMucSelectSanPhamForm();
                f.SPDetail.HopDong_ID = dmdk.ID_HopDong;
                HopDong hd = new HopDong();
                hd.ID = dmdk.ID_HopDong;
                f.HD = hd;
                f.ShowDialog();
                List<Company.GC.BLL.GC.GC_DinhMuc> DMCollection = new List<Company.GC.BLL.GC.GC_DinhMuc>();
                if (f.SPSelectCollection.Count >=1)
                {
                    foreach (Company.GC.BLL.GC.SanPham item in f.SPSelectCollection)
                    {
                        DinhMucCollection Items = new DinhMucCollection();
                        foreach (DinhMuc it in dmdk.DMCollection)
                        {
                            if (it.MaSanPham == item.Ma)
                            {
                                Items.Add(it);
                            }
                        }
                        foreach (DinhMuc items in Items)
                        {
                            if (items.ID > 0)
                                items.Delete();
                            dmdk.DMCollection.Remove(items);
                        }
                        DMCollection = new List<Company.GC.BLL.GC.GC_DinhMuc>();
                        DMCollection = Company.GC.BLL.GC.GC_DinhMuc.SelectCollectionDynamic("MaSanPham = '" + item.Ma + "' AND HopDong_ID = " + dmdk.ID_HopDong + "", "");
                        foreach (Company.GC.BLL.GC.GC_DinhMuc ite in DMCollection)
                        {
                            DMDetail = new DinhMuc();
                            DMDetail.MaSanPham = ite.MaSanPham;
                            Company.GC.BLL.GC.KDT_LenhSanXuat lenhSX = new Company.GC.BLL.GC.KDT_LenhSanXuat();
                            if (ite.LenhSanXuat_ID > 0)
                            {
                                lenhSX = Company.GC.BLL.GC.KDT_LenhSanXuat.Load(ite.LenhSanXuat_ID);
                                DMDetail.MaDDSX = lenhSX.SoLenhSanXuat.ToString();
                            }
                            else
                            {
                                DMDetail.MaDDSX = String.Empty;
                            }
                            DMDetail.MaNguyenPhuLieu = ite.MaNguyenPhuLieu;
                            DMDetail.TyLeHaoHut = ite.TyLeHaoHut;
                            DMDetail.DinhMucSuDung = ite.DinhMucSuDung;
                            NguyenPhuLieu NPL = new NguyenPhuLieu();
                            NPL = NguyenPhuLieu.Load(ite.HopDong_ID ,ite.MaNguyenPhuLieu);
                            DMDetail.DVT_ID = NPL.DVT_ID;
                            DMDetail.TenNPL = ite.TenNPL;
                            DMDetail.TenSanPham = item.Ten;
                            dmdk.DMCollection.Add(DMDetail);
                            this.SetChange(true);
                        }
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLenhSanXuat_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbLenhSanXuat.Value != null)
                {
                    List<Company.GC.BLL.GC.GC_DinhMuc> DMCollection = new List<Company.GC.BLL.GC.GC_DinhMuc>();
                    DMCollection = new List<Company.GC.BLL.GC.GC_DinhMuc>();
                    long LenhSanXuat_ID = Company.GC.BLL.GC.KDT_LenhSanXuat.GetID(cbbLenhSanXuat.Value.ToString(),dmdk.ID_HopDong);
                    DMCollection = Company.GC.BLL.GC.GC_DinhMuc.SelectCollectionDynamic("MaSanPham = '" + cbbMaSP.Value.ToString() + "' AND HopDong_ID = " + dmdk.ID_HopDong + "AND LenhSanXuat_ID =" + LenhSanXuat_ID + "", "");
                    bool isExits = false;
                    if (DMCollection.Count > 0)
                    {
                        foreach (DinhMuc item in dmdk.DMCollection)
                        {
                            if (item.MaSanPham == cbbMaSP.Value.ToString())
                            {
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            if (ShowMessageTQDT("Mã sản phẩm : [" + cbbMaSP.Value.ToString() + "] này đã đăng ký thông tin định mức với lệnh sản xuất : [" + cbbLenhSanXuat.Value.ToString() + "] trước đó .\n Bạn có muốn Lấy danh sách định mức của mã sản phẩm này không ?", true) == "Yes")
                            {
                                foreach (Company.GC.BLL.GC.GC_DinhMuc ite in DMCollection)
                                {
                                    DMDetail = new DinhMuc();
                                    DMDetail.MaSanPham = ite.MaSanPham;
                                    DMDetail.MaDDSX = cbbLenhSanXuat.Value.ToString();
                                    DMDetail.MaNguyenPhuLieu = ite.MaNguyenPhuLieu;
                                    DMDetail.TyLeHaoHut = ite.TyLeHaoHut;
                                    DMDetail.DinhMucSuDung = ite.DinhMucSuDung;
                                    NguyenPhuLieu NPL = new NguyenPhuLieu();
                                    NPL = NguyenPhuLieu.Load(ite.HopDong_ID, ite.MaNguyenPhuLieu);
                                    DMDetail.DVT_ID = NPL.DVT_ID;
                                    DMDetail.TenNPL = ite.TenNPL;
                                    DMDetail.TenSanPham = txtTenSP.Text;
                                    dmdk.DMCollection.Add(DMDetail);
                                    this.SetChange(true);
                                }
                            }   
                        }
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void lnkCoppy_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                if (cbbLenhSanXuat.Value == null)
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "DOANH NGHIỆP CHƯA CHỌN LỆNH SẢN XUẤT ĐỂ SAO CHÉP ", false);
                    return;
                }
                foreach (Company.GC.BLL.GC.KDT_LenhSanXuat item in LenhSanXuatCollection)
                {
                    bool isExits = false;
                    item.SPCollection = Company.GC.BLL.GC.KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
                    foreach (Company.GC.BLL.GC.KDT_LenhSanXuat_SP ite in item.SPCollection)
                    {
                        if (ite.MaSanPham == cbbMaSP.Value.ToString())
                        {
                            cbbLenhSanXuat.Text = item.SoLenhSanXuat.ToString();
                            isExits = true;
                            break;
                        }
                    }
                    if (!isExits)
                    {
                        errorProvider.SetError(cbbLenhSanXuat, "MÃ ĐỊNH DANH CỦA LỆNH SẢN XUẤT  NÀY CHƯA ĐƯỢC ĐĂNG KÝ HOẶC CẬP NHẬT .");
                        return;
                    }
                }
                foreach (DinhMuc dm in dmdk.DMCollection)
                {
                    if (cbbMaSP.Value.ToString() == dm.MaSanPham)
                    {
                        dm.MaDDSX = cbbLenhSanXuat.Value.ToString();
                    }
                }
                ShowMessageTQDT("Thông báo từ hệ thống", "SAO CHÉP THÀNH CÔNG ", false);
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearchNPL_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["MaNguyenPhuLieu"], ConditionOperator.Contains, txtMaNPL.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void DinhMucGCEditForm_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (e.CloseReason == CloseReason.WindowsShutDown) return;
            {
                if (true)
                {

                }
                else
                {
                    e.Cancel = true;
                }
            }
            Dictionary<String, String> Dictionary = new Dictionary<String, String>();
            foreach (Control c in this.Controls)
            {                
                if (c is Janus.Windows.GridEX.EditControls.EditBox)
                {
                    Dictionary.Add(c.Name, c.Text);
                }
                else if (c is Janus.Windows.GridEX.EditControls.NumericEditBox)
                {
                    Dictionary.Add(c.Name, c.Text);
                }
                else if (c is Janus.Windows.GridEX.EditControls.MultiColumnCombo)
                {
                    Dictionary.Add(c.Name, c.Text);
                }
                else if (c is Janus.Windows.EditControls.UIComboBox)
                {
                    Dictionary.Add(c.Name, c.Text);
                }
                c.TextChanged += new EventHandler(c_ControlChanged);
            }
        }
        void c_ControlChanged(object sender, EventArgs e)
        {

        }
    }
}