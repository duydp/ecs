﻿
using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
//Kiểm tra interface của project LanNT
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using System.Data;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.Interface.GC;
using Company.KDT.SHARE.Components.Messages.Send;
namespace Company.Interface
{
    public partial class HangMauDichForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        private decimal clg;
        private decimal dgnt;
        //public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public string LoaiHangHoa;
        public ToKhaiMauDich TKMD;
        private decimal luong;
        public string MaNguyenTe;
        public string MaNuocXX;
        public string NhomLoaiHinh = string.Empty;
        private decimal st_clg;
        private HangMauDich hangMauDich = null;
        //-----------------------------------------------------------------------------------------				
        private decimal tgnt;
        private decimal tgtt_gtgt;

        private decimal tgtt_nk; 
        private decimal tgtt_ttdb;
        private decimal tl_clg;
        private decimal tongTGKB;
        private decimal ts_gtgt;
        private decimal tgtt_bvmt;
        private decimal tgtt_chongbanphagia;

        private decimal ts_nk;
        private decimal ts_ttdb;
        private decimal tt_gtgt;
        private decimal ts_bvmt;
        private decimal ts_chongbanphagia;

        private decimal tt_nk;
        private decimal tt_ttdb;
        private decimal tt_chongbanphagia;
        private decimal tt_bvmt;
        private decimal tt_thukhac;
        public decimal TyGiaTT;
        NguyenPhuLieuRegistedForm NPLRegistedForm;
        SanPhamRegistedForm SPRegistedForm;


        //-----------------------------------------------------------------------------------------

        public HangMauDichForm()
        {
            InitializeComponent();
        }
        public HangMauDichForm(HangMauDich hangmaudich)
            : this()
        {
            hangMauDich = hangmaudich;
        }
        private void khoitao_GiaoDien()
        {
            cbBieuThueBVMT.Enabled = cbBieuThueCBPG.Enabled = cbBieuThueGTGT.Enabled = cbBieuThueTTDB.Enabled = cbBieuThueXNK.Enabled = GlobalSettings.SendV4;
            uiTabPage3.Enabled = GlobalSettings.SendV4;
            txtTriGiaTinhThueBVMT.Enabled = txtTSBVMT.Enabled = txtTTBVMT.Enabled = GlobalSettings.SendV4;
            txtTriGiaTTCPG.Enabled = txtTSCPG.Enabled = txtTienThueCPG.Enabled = GlobalSettings.SendV4;
            chkIsOld.Enabled = chkHangDongBo.Enabled = GlobalSettings.SendV4;
            lblCopyBieuThue.Enabled = GlobalSettings.SendV4;
        }

        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll().Tables[0];
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
           txtTGNT.DecimalDigits = GlobalSettings.TriGiaNT;
           if (GlobalSettings.SendV4)
           {
               #region Biểu thuế
               DataTable dtBieuThue = BieuThue.SelectAll().Tables[0];
               DataTable dtBieuThueNhap = BieuThue.SelectDynamic("MoTaKhac = 'N' OR MoTaKhac = 'KHAC' OR MaBieuThue = ''", "MaBieuThue").Tables[0];
               DataTable dtBieuThueXuat = BieuThue.SelectDynamic("MoTaKhac = 'X' OR MoTaKhac = 'KHAC' OR MaBieuThue = ''", "MaBieuThue").Tables[0];
               DataTable dtBieuThueVAT = BieuThue.SelectDynamic("MoTaKhac = 'VAT' OR MoTaKhac = 'KHAC' OR MaBieuThue = ''", "MaBieuThue").Tables[0];
               DataTable dtBieuThueTTDB = BieuThue.SelectDynamic("MoTaKhac = 'TTDB' OR MoTaKhac = 'KHAC' OR MaBieuThue = ''", "MaBieuThue").Tables[0];


               cbBieuThueXNK.DataSource = this.NhomLoaiHinh.Contains("N") ? dtBieuThueNhap : dtBieuThueXuat;
               cbBieuThueTTDB.DataSource = dtBieuThueTTDB;
               cbBieuThueBVMT.DataSource = dtBieuThue;
               cbBieuThueGTGT.DataSource = dtBieuThueVAT;
               cbBieuThueCBPG.DataSource = dtBieuThue;
               cbBieuThueXNK.ValueMember = cbBieuThueTTDB.ValueMember = cbBieuThueBVMT.ValueMember = cbBieuThueGTGT.ValueMember = cbBieuThueCBPG.ValueMember = "MaBieuThue";
               cbBieuThueXNK.DisplayMember = cbBieuThueTTDB.DisplayMember = cbBieuThueBVMT.DisplayMember = cbBieuThueGTGT.DisplayMember = cbBieuThueCBPG.DisplayMember = "TenBieuThue";
               cbBieuThueXNK.SelectedIndex = cbBieuThueTTDB.SelectedIndex = cbBieuThueBVMT.SelectedIndex = cbBieuThueGTGT.SelectedIndex = cbBieuThueCBPG.SelectedIndex = 0;
               #endregion
           }
        }

        private decimal tinhthue()
        {

            this.tgtt_nk = this.tgnt * this.TyGiaTT;
            if (!chkThueTuyetDoi.Checked)
                this.tt_nk = this.tgtt_nk * this.ts_nk;
            else
            {
                Decimal dgntTuyetDoi = Convert.ToDecimal(txtDonGiaTuyetDoi.Value);
                this.tt_nk = Math.Round(dgntTuyetDoi * TyGiaTT * Convert.ToDecimal(luong));
            }
            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;

            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;
            //Thuế môi trường
            this.tgtt_bvmt = this.tgtt_nk;
            this.tt_bvmt = this.ts_bvmt == 0 ? Convert.ToDecimal(txtTTBVMT.Value) : this.ts_bvmt * this.tgtt_bvmt;

            //Thu khác
            tt_thukhac = Convert.ToDecimal(txtTriGiaThuKhac.Value);

            //Chênh lệch giá
            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            //Thuế chống bán phá giá
            this.tgtt_chongbanphagia = this.tgtt_nk;
            this.tt_chongbanphagia = this.tgtt_chongbanphagia * this.ts_chongbanphagia;


            //Thuế giá trị gia tăng
            this.tgtt_gtgt = this.tgtt_bvmt + this.tt_bvmt + this.st_clg + this.tt_chongbanphagia + tt_thukhac;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            //txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;



            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTriGiaTinhThueBVMT.Value = this.tgtt_bvmt;
            txtTTBVMT.Value = Math.Round(this.tt_bvmt, MidpointRounding.AwayFromZero);

            txtTienThueCPG.Value = Math.Round(this.tt_chongbanphagia, MidpointRounding.AwayFromZero);
            txtTriGiaTTCPG.Value = this.tgtt_chongbanphagia;



            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg + this.tt_bvmt + this.tt_chongbanphagia + this.tt_thukhac, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg + this.tt_bvmt + this.tt_chongbanphagia, MidpointRounding.AwayFromZero);
        }
        private decimal tinhthue2()
        {

            this.luong = Convert.ToDecimal(txtLuong.Value);
            if (luong == 0) return 0;
            this.tgnt = Convert.ToDecimal(txtTGNT.Value);
            txtDGNT.Text = Helpers.Format(tgnt / this.luong, GlobalSettings.DonGiaNT);
            dgnt = Convert.ToDecimal(txtDGNT.Value);

            if (!chkThueTuyetDoi.Checked)
            {
                this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            }

            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;

            this.tgnt = Convert.ToDecimal(this.dgnt) * (this.luong);

            this.tgtt_nk = Convert.ToDecimal(txtTGTT_NK.Value);
            this.tt_thukhac = Convert.ToDecimal(txtTriGiaThuKhac.Value);
            if (tgtt_nk == 0)
            {
                tgtt_nk = tgnt * TyGiaTT;
            }
            if (!chkThueTuyetDoi.Checked)
                this.tt_nk = this.tgtt_nk * this.ts_nk;
            else
            {
                decimal dgntTuyetDoi = Convert.ToDecimal(txtDonGiaTuyetDoi.Value);
                this.tt_nk = Math.Round(dgntTuyetDoi * TyGiaTT * Convert.ToDecimal(luong));
            }
            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            //Thuế môi trường
            this.tgtt_bvmt = this.tgtt_nk ;
            this.tt_bvmt = this.ts_bvmt == 0 ? Convert.ToDecimal(txtTTBVMT.Value) : this.ts_bvmt * this.tgtt_bvmt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            //Thuế chống bán phá giá
            this.tgtt_chongbanphagia = this.tgtt_nk;
            this.tt_chongbanphagia = this.tgtt_chongbanphagia * this.ts_chongbanphagia;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb + this.st_clg + this.tt_bvmt + this.tt_chongbanphagia + this.tt_thukhac;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            //txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;
            txtTriGiaTinhThueBVMT.Value = this.tgtt_bvmt;
            txtTTBVMT.Value = Math.Round(this.tt_bvmt, MidpointRounding.AwayFromZero);

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);

            txtTienThueCPG.Value = Math.Round(this.tt_chongbanphagia, MidpointRounding.AwayFromZero);
            txtTriGiaTTCPG.Value = this.tgtt_chongbanphagia;

            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);
            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg + this.tt_bvmt + this.tt_chongbanphagia + this.tt_thukhac, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg + this.tt_bvmt + this.tt_chongbanphagia + this.tt_thukhac, MidpointRounding.AwayFromZero);
        }
        private decimal tinhthue3()
        {
            this.tgnt = Convert.ToDecimal(txtTGNT.Value);
            decimal tgntkophi = tgnt;
            if (TKMD.DKGH_ID.Trim() == "CNF" || TKMD.DKGH_ID.Trim() == "CFR") tgntkophi = this.tgnt - TKMD.PhiVanChuyen;

            this.tgtt_nk = tgntkophi * this.TyGiaTT;
            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;
            this.tt_thukhac = Convert.ToDecimal(txtTriGiaThuKhac.Value);
            //Thuế môi trường
            this.tgtt_bvmt = this.tgtt_nk;
            this.tt_bvmt = this.ts_bvmt == 0 ? Convert.ToDecimal(txtTTBVMT.Value) : this.ts_bvmt * this.tgtt_bvmt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            //Thuế chống bán phá giá
            this.tgtt_chongbanphagia = this.tgtt_nk;
            this.tt_chongbanphagia = this.tgtt_chongbanphagia * this.ts_chongbanphagia;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb + this.st_clg + this.tt_bvmt + this.tt_chongbanphagia + this.tt_thukhac;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            txtTriGiaKB.Value = tgnt * this.TyGiaTT;
            //txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;
            txtTriGiaTinhThueBVMT.Value = this.tgtt_bvmt;

            txtTTBVMT.Value = Math.Round(this.tt_bvmt, MidpointRounding.AwayFromZero);
            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTienThueCPG.Value = Math.Round(this.tt_chongbanphagia, MidpointRounding.AwayFromZero);
            txtTriGiaTTCPG.Value = this.tgtt_chongbanphagia;

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg + this.tt_bvmt + this.tt_chongbanphagia + this.tt_thukhac, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg + this.tt_bvmt + this.tt_chongbanphagia + this.tt_thukhac, MidpointRounding.AwayFromZero);
        }
        //-----------------------------------------------------------------------------------------
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich hmd in TKMD.HMDCollection)
            {
                if (hmd.MaPhu.Trim() == maHang) return true;
            }
            return false;
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.IsTinhThue)
                {
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
                }
            cvError.Validate();

            if (!cvError.IsValid) return;
            epError.Clear();
            if (string.IsNullOrEmpty(ctrNuocXX.Ma))
            {
                epError.SetError(ctrNuocXX, "Không được để trống nước xuất xứ");
                return;
            }
            if (string.IsNullOrEmpty(cbDonViTinh.Text))
            {
                epError.SetError(cbDonViTinh, "Không được để trống đơn vị tính");
                return;
            }
            txtMaHang.Focus();

            try
            {
                bool isAddNew = false;
                if (hangMauDich == null /*|| hangMauDich.ID == 0*/)
                {
                    hangMauDich = new HangMauDich();
                    hangMauDich.SoThuTuHang = 0;
                    isAddNew = true;
                }
                GetHang(hangMauDich);
                // Tính thuế.
                //hmd.TinhThue(this.TyGiaTT);
                if (isAddNew || AddNewChungTu)
                    TKMD.HMDCollection.Add(hangMauDich);
                TKMD.SoLuongPLTK = (short)Helpers.GetLoading(TKMD.HMDCollection.Count);
                this.refresh_STTHang();
                dgList.DataSource = TKMD.HMDCollection;
                dgList.Refetch();
                #region Comment by LanNT thay đoạn này bằng đoạn dưới
                //txtMaHang.Text = "";
                //txtTenHang.Text = "";
                //txtMaHS.Text = "";
                //txtLuong.Value = 0;
                //txtDGNT.Value = 0;
                //txtTGTT_NK.Value = 0;

                //txtTGNT.Value = 0;
                //txtTriGiaKB.Value = 0;
                //txtTGTT_TTDB.Value = 0;
                //txtTGTT_NK.Value = 0;
                //txtTGTT_GTGT.Value = 0;
                //txtCLG.Value = 0;
                //txtTienThue_NK.Value = 0;
                //txtTienThue_TTDB.Value = 0;
                //txtTienThue_GTGT.Value = 0;
                //txtTongSoTienThue.Value = 0;
                //grbThue.Enabled = true;
                //chkMienThue.Checked = false;
                //chkThueTuyetDoi.Checked = false;
                //chkHangFOC.Checked = false;
                //this.edit = false;
                //epError.Clear();
                //LuongCon = 0;
                //txtTSVatGiam.Text = "";
                //txtTSXNKGiam.Text = "";
                //txtTSTTDBGiam.Text = "";
                #endregion Comment by LanNT thay đoạn này bằng đoạn dưới
                btnAddNew_Click(null, null);

            }
            catch (Exception ex)
            {
                SingleMessage.SendMail(setText("Dữ liệu của bạn không hợp lệ ", "Invalid input value"),
                    TKMD.MaHaiQuan, new SendEventArgs(ex));
            }
        }

        private void GetHang(HangMauDich hmd)
        {

            hmd.MaHS = txtMaHS.Text;
            hmd.MaPhu = txtMaHang.Text.Trim();
            hmd.TenHang = txtTenHang.Text.Trim();
            hmd.NuocXX_ID = ctrNuocXX.Ma;
            hmd.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            hmd.SoLuong = Convert.ToDecimal(txtLuong.Value);
            hmd.DonGiaKB = Convert.ToDecimal(txtDGNT.Value);
            hmd.DonGiaTuyetDoi = Convert.ToDouble(txtDonGiaTuyetDoi.Value);
            hmd.TriGiaKB = Convert.ToDecimal(txtTGNT.Value);
            hmd.TriGiaKB_VND = Convert.ToDecimal(txtTriGiaKB.Value);
            hmd.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Value);
            hmd.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Value);
            hmd.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Value);
            hmd.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Value);
            //hmd.TyLeThuKhac = Convert.ToDecimal(txtTL_CLG.Value);
            hmd.ThueXNK = Math.Round(Convert.ToDecimal(txtTienThue_NK.Value), 0);
            hmd.ThueTTDB = Math.Round(Convert.ToDecimal(txtTienThue_TTDB.Value), 0);
            hmd.ThueGTGT = Math.Round(Convert.ToDecimal(txtTienThue_GTGT.Value), 0);
            hmd.DonGiaTT = hmd.TriGiaTT / Convert.ToDecimal(hmd.SoLuong);
            hmd.PhuThu = Convert.ToDecimal(txtTien_CLG.Value);
            hmd.FOC = chkHangFOC.Checked;
            hmd.ThueTuyetDoi = chkThueTuyetDoi.Checked;
            hmd.ThueSuatXNKGiam = Convert.ToDecimal(txtTSXNKGiam.Text.Trim());
            hmd.ThueSuatTTDBGiam = Convert.ToDecimal(txtTSTTDBGiam.Text.Trim());
            hmd.ThueSuatVATGiam = Convert.ToDecimal(txtTSVatGiam.Text.Trim());
            hmd.TyLeThuKhac = Convert.ToDecimal(txtTyLeThuKhac.Value);
            hmd.TriGiaThuKhac = (txtTriGiaThuKhac.Text != "" ? Convert.ToDecimal(txtTriGiaThuKhac.Text) : 0);
            //Thue BVMT
            hmd.ThueBVMT = Math.Round(Convert.ToDecimal(txtTTBVMT.Value), 0);
            hmd.ThueSuatBVMT = Convert.ToDecimal(txtTSBVMT.Value);
            //Thue Chong Pha Gia
            hmd.ThueSuatChongPhaGia = Math.Round(Convert.ToDecimal(txtTienThueCPG.Value), 0);
            hmd.ThueSuatChongPhaGia = Convert.ToDecimal(txtTSCPG.Value);
            hmd.isHangCu = chkIsOld.Checked;
            if (chkMienThue.Checked)
                hmd.MienThue = 1;
            else
                hmd.MienThue = 0;
            #region Thêm Mới V3
            hmd.ThongTinKhac = chkHangDongBo.Checked ? "isHangDongBo" : string.Empty;
            hmd.MaHSMoRong = txtMaHSMoRong.Text;
            hmd.NhanHieu = txtNhanHieu.Text;
            hmd.QuyCachPhamChat = txtQuyCach.Text;
            hmd.ThanhPhan = txtThanhPhan.Text;
            hmd.Model = txtModel.Text;
            hmd.TenHangSX = txtTenHangSX.Text;
            hmd.MaHangSX = txtMaHangSX.Text;


            hmd.BieuThueXNK = (cbBieuThueXNK.SelectedValue != null) ? cbBieuThueXNK.SelectedValue.ToString() : string.Empty;
            hmd.BieuThueTTDB = (cbBieuThueTTDB.SelectedValue != null) ? cbBieuThueTTDB.SelectedValue.ToString() : string.Empty;
            hmd.BieuThueBVMT = (cbBieuThueBVMT.SelectedValue != null) ? cbBieuThueBVMT.SelectedValue.ToString() : string.Empty;
            hmd.BieuThueGTGT = (cbBieuThueGTGT.SelectedValue != null) ? cbBieuThueGTGT.SelectedValue.ToString() : string.Empty;
            hmd.BieuThueCBPG = (cbBieuThueCBPG.SelectedValue != null) ? cbBieuThueCBPG.SelectedValue.ToString() : string.Empty;

            if (hangMauDich.MienGiamThueCollection != null && hangMauDich.MienGiamThueCollection.Count > 0)
            {
                hangMauDich.MienGiamThueCollection[0].SoVanBanMienGiam = txtGiamThue_SoVanBan.Text.Trim();
                hangMauDich.MienGiamThueCollection[1].ThueSuatTruocGiam = (double)txtGiamThue_ThueSuatGoc.Value;
                hangMauDich.MienGiamThueCollection[2].TyLeMienGiam = (double)txtGiamThue_TyLeGiam.Value;
            }
            else if (!string.IsNullOrEmpty(txtGiamThue_SoVanBan.Text))
            {
                Company.KDT.SHARE.QuanLyChungTu.MienGiamThue miengiam = new Company.KDT.SHARE.QuanLyChungTu.MienGiamThue()
                {
                    SoVanBanMienGiam = txtGiamThue_SoVanBan.Text.Trim(),
                    ThueSuatTruocGiam = Convert.ToDouble(txtGiamThue_ThueSuatGoc.Value),
                    TyLeMienGiam = Convert.ToDouble(txtGiamThue_TyLeGiam.Value)
                };
                hangMauDich.MienGiamThueCollection = new List<Company.KDT.SHARE.QuanLyChungTu.MienGiamThue>();
                hangMauDich.MienGiamThueCollection.Add(miengiam);
            }
            #endregion

            #region Đơn vị tính quy đổi
            if (DVT_QuyDoiCtr.IsEnable)
                hangMauDich.DVT_QuyDoi = DVT_QuyDoiCtr.DVT_QuyDoi;
            else if (DVT_QuyDoiCtr.DVT_QuyDoi != null)
            {
                hangMauDich.DVT_QuyDoi = DVT_QuyDoiCtr.DVT_QuyDoi;
                hangMauDich.DVT_QuyDoi.IsUse = false;
            }
            else
                hangMauDich.DVT_QuyDoi = null;
            #endregion
        }
        private void SetHang(HangMauDich hmd)
        {

            txtMaHang.Text = hmd.MaPhu;
            txtTenHang.Text = hmd.TenHang;
            txtMaHS.Text = hmd.MaHS;
            cbDonViTinh.SelectedValue = hmd.DVT_ID;

            #region Thêm Mới V3
            txtMaHSMoRong.Text = hmd.MaHSMoRong;
            txtMaHangSX.Text = hmd.MaHangSX;
            txtTenHangSX.Text = hmd.TenHangSX;
            txtNhanHieu.Text = hmd.NhanHieu;
            txtQuyCach.Text = hmd.QuyCachPhamChat;
            txtThanhPhan.Text = hmd.ThanhPhan;
            txtModel.Text = hmd.Model;
            chkHangDongBo.Checked = hmd.ThongTinKhac != null && hmd.ThongTinKhac.Contains("isHangDongBo");
            #endregion

            chkHangFOC.Checked = hmd.FOC;
            chkThueTuyetDoi.Checked = hmd.ThueTuyetDoi;
            txtLuong.Value = hmd.SoLuong;
            txtDGNT.Value = hmd.DonGiaKB;
            txtDonGiaTuyetDoi.Value = hmd.DonGiaTuyetDoi;
            txtTGNT.Value = hmd.TriGiaKB;
            txtTriGiaKB.Value = hmd.TriGiaKB_VND;
            ctrNuocXX.Ma = hmd.NuocXX_ID;

            txtTGTT_NK.Value = hmd.TriGiaTT;
            txtTS_NK.Value = hmd.ThueSuatXNK;
            txtTS_TTDB.Value = hmd.ThueSuatTTDB;
            txtTS_GTGT.Value = hmd.ThueSuatGTGT;
            txtTL_CLG.Value = hmd.PhuThu;
            txtTienThue_NK.Value = hmd.ThueXNK;
            txtTienThue_TTDB.Value = hmd.ThueTTDB;
            txtTienThue_GTGT.Value = hmd.ThueGTGT;
            txtTien_CLG.Value = hmd.TriGiaThuKhac;
            txtTSXNKGiam.Text = hmd.ThueSuatXNKGiam.ToString();
            txtTSTTDBGiam.Text = hmd.ThueSuatTTDBGiam.ToString();
            txtTSVatGiam.Text = hmd.ThueSuatVATGiam.ToString();
            txtTyLeThuKhac.Value = hmd.TyLeThuKhac;
            txtTriGiaThuKhac.Value = hmd.TriGiaThuKhac;
            chkMienThue.Checked = hmd.MienThue == 1 ? true : false;
            //Thuế môi trường
            txtTSBVMT.Value = hmd.ThueSuatBVMT;
            txtTTBVMT.Value = hmd.ThueBVMT;

            //Thuế chống bán phá giá
            txtTSCPG.Value = hmd.ThueSuatChongPhaGia;
            txtTienThueCPG.Value = hmd.ThueChongPhaGia;
            chkIsOld.Checked = hmd.isHangCu;
            //Biểu thuế
            cbBieuThueXNK.SelectedValue = hmd.BieuThueXNK;
            cbBieuThueTTDB.SelectedValue = hmd.BieuThueTTDB;
            cbBieuThueBVMT.SelectedValue = hmd.BieuThueBVMT;
            cbBieuThueGTGT.SelectedValue = hmd.BieuThueGTGT;
            cbBieuThueCBPG.SelectedValue = hmd.BieuThueCBPG;

            if (hmd.MienGiamThueCollection != null && hmd.MienGiamThueCollection.Count > 0)
            {
                txtGiamThue_SoVanBan.Text = hmd.MienGiamThueCollection[0].SoVanBanMienGiam;
                txtGiamThue_ThueSuatGoc.Value = hmd.MienGiamThueCollection[0].ThueSuatTruocGiam;
                txtGiamThue_TyLeGiam.Value = hmd.MienGiamThueCollection[0].TyLeMienGiam;
            }
            else
            {
                txtGiamThue_SoVanBan.Text = string.Empty;
                txtGiamThue_ThueSuatGoc.Value = string.Empty;
                txtGiamThue_TyLeGiam.Value = string.Empty;
            }
            if (Company.KDT.SHARE.Components.Globals.IsTinhThue)
            {
                if (this.NhomLoaiHinh.Contains("N"))
                    tinhthue2();
                else
                    tinhthue3();
            }

            #region Đơn vị tính quy đổi
            if (hangMauDich.DVT_QuyDoi != null)
            {
                DVT_QuyDoiCtr.DVT_QuyDoi = hmd.DVT_QuyDoi;
                DVT_QuyDoiCtr.Master_Id = 0;
            }
            else if (hmd.ID > 0)
            {
                DVT_QuyDoiCtr.Master_Id = hmd.ID;
                DVT_QuyDoiCtr.Type = "HMD";
            }
            else
                DVT_QuyDoiCtr.DVT_QuyDoi = null;
            DVT_QuyDoiCtr.DonViTinhQuyDoiControl_Load(null, null);
            #endregion

        }
        private void TinhTriGiaTinhThue()
        {
            decimal Phi = Convert.ToDecimal(TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen);
            decimal TongTriGiaHang = 0;
            foreach (HangMauDich hmd in TKMD.HMDCollection)
            {
                if (hmd.FOC) continue;
                TongTriGiaHang += hmd.TriGiaKB;
            }
            if (TongTriGiaHang == 0) return;
            decimal TriGiaTTMotDong = Phi / TongTriGiaHang;
            foreach (HangMauDich hmd in TKMD.HMDCollection)
            {
                if (hmd.FOC) continue;
                hmd.TriGiaTT = (TriGiaTTMotDong * hmd.TriGiaKB + hmd.TriGiaKB) * TyGiaTT;
                hmd.DonGiaTT = hmd.TriGiaTT / Convert.ToDecimal(hmd.SoLuong);
                if (!hmd.ThueTuyetDoi)
                    hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
                hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, MidpointRounding.AwayFromZero);
                hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB) * hmd.ThueSuatGTGT / 100, MidpointRounding.AwayFromZero);
                hmd.TriGiaThuKhac = hmd.TyLeThuKhac > 0 ? Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, MidpointRounding.AwayFromZero) : hmd.TriGiaThuKhac;
                //hmd.PhuThu = (hmd.TyLeThuKhac * hmd.ThueXNK) / 10;
            }

        }
        private void reSetError()
        {
            epError.SetError(txtMaHang, null);
            epError.SetError(txtTenHang, null);
            epError.SetError(txtMaHS, null);
        }
        //-----------------------------------------------------------------------------------------
        private void txtMaNPL_Leave(object sender, EventArgs e)
        {

            reSetError();
            bool isNotFound = false;
            if (this.TKMD.LoaiHangHoa == "N")
            {

                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.HopDong_ID = TKMD.IDHopDong;
                npl.Ma = txtMaHang.Text.Trim();
                if (npl.Load())
                {
                    txtMaHang.Text = npl.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = npl.MaHS;
                    txtTenHang.Text = npl.Ten;
                    cbDonViTinh.SelectedValue = npl.DVT_ID;
                    epError.SetError(txtMaHang, null);
                    epError.SetError(txtTenHang, null);
                    epError.SetError(txtMaHS, null);

                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.TKMD.LoaiHangHoa == "S")
            {
                SanPham sp = new SanPham();
                sp.HopDong_ID = TKMD.IDHopDong;
                sp.Ma = txtMaHang.Text.Trim();
                if (sp.Load())
                {
                    txtMaHang.Text = sp.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    cbDonViTinh.SelectedValue = sp.DVT_ID;

                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.TKMD.LoaiHangHoa == "T")
            {
                ThietBi tb = new ThietBi();
                tb.HopDong_ID = TKMD.IDHopDong;
                tb.Ma = txtMaHang.Text.Trim();
                if (tb.Load())
                {
                    txtMaHang.Text = tb.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = tb.MaHS;
                    txtTenHang.Text = tb.Ten;
                    cbDonViTinh.SelectedValue = tb.DVT_ID;

                }
                else
                {
                    isNotFound = true;
                }
            }
            else if (this.TKMD.LoaiHangHoa == "H")
            {
                HangMau hangmau = HangMau.Load(TKMD.IDHopDong, txtMaHang.Text.Trim());
                if (hangmau != null)
                {
                    txtMaHang.Text = hangmau.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = hangmau.MaHS;
                    txtTenHang.Text = hangmau.Ten;
                    cbDonViTinh.SelectedValue = hangmau.DVT_ID;
                }
                else isNotFound = true;

            }
            if (isNotFound)
            {
                epError.SetError(txtMaHang, setText("Không tồn tại mã này.", "This value is not exist"));
                txtTenHang.Text = txtMaHS.Text = string.Empty;
                cbDonViTinh.SelectedValue = "";
                return;
            }
        }

        private void HangMauDichForm_Load(object sender, EventArgs e)
        {
            if (TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "N")
                txtLuong.FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            else
                txtLuong.FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
            txtDGNT.FormatString = "N" + GlobalSettings.DonGiaNT;
            txtTriGiaKB.FormatString = "N" + GlobalSettings.TriGiaNT;


            this.khoitao_DuLieuChuan();
            bool isActive = TKMD.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || TKMD.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET ||
                            TKMD.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || TKMD.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET;


            btnXoa.Enabled = btnGhi.Enabled = isActive;

            if (hangMauDich != null)
            {
                hangMauDich.LoadMienGiamThue();
                SetHang(hangMauDich);
            }

            if (btnXoa.Enabled)
                this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            else this.dgList.AllowDelete = InheritableBoolean.False;


            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.MaNguyenTe + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.MaNguyenTe);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.MaNguyenTe);

            if (this.NhomLoaiHinh[0].ToString().Equals("X"))
                label16.Text = setText("Trị giá tính thuế XK", "Export Tax assessment");


            dgList.DataSource = TKMD.HMDCollection;

            lblTyGiaTT.Text = "Tỷ giá tính thuế: " + this.TyGiaTT.ToString("N");
            refresh_STTHang();
            if (this.OpenType == Company.KDT.SHARE.Components.OpenFormType.View)
                btnGhi.Visible = false;
            txtMaHang.Focus();
            likTinhThue.Visible = !Company.KDT.SHARE.Components.Globals.IsTinhThue;
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            this.luong = Convert.ToDecimal(txtLuong.Value);
            if (luong == 0) return;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            this.dgnt = Convert.ToDecimal(txtDGNT.Value);
            //txtDGNT.FormatString = "N" + GlobalSettings.DonGiaNT;
            //txtDGNT.Value = dgnt;
            if (GlobalSettings.TinhThueTGNT == "0")
            {
                this.luong = Convert.ToDecimal(txtLuong.Value);
                if (luong != 0)
                {
                    // Convert.ToDouble(Convert.ToDecimal(this.dgnt) * (this.luong));
                    txtTGNT.Text = Helpers.Format(dgnt * Convert.ToDecimal(luong), GlobalSettings.TriGiaNT);
                    tgnt = Convert.ToDecimal(txtTGNT.Value);
                    if (this.NhomLoaiHinh.Contains("N"))
                        tinhthue();
                    else
                        tinhthue3();
                }

            }
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        private void txtTS_CLG_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
        }

        //-----------------------------------------------------------------------------------------------
        private void refresh_STTHang()
        {
            int i = 1;
            TKMD.TongTriGiaKhaiBao = 0;
            decimal tongThue = 0;
            foreach (HangMauDich hmd in TKMD.HMDCollection)
            {
                hmd.SoThuTuHang = i++;
                TKMD.TongTriGiaKhaiBao += (decimal)hmd.TriGiaKB;
                TKMD.TongTriGiaTinhThue += (decimal)hmd.TriGiaTT;
                tongThue += (decimal)hmd.ThueTTDB + (decimal)hmd.ThueXNK + (decimal)hmd.ThueGTGT + (decimal)hmd.TriGiaThuKhac;
            }
            lblTongTGKB.Text = string.Format("Tổng trị giá nguyên tệ :  {0} ({1})", TKMD.TongTriGiaKhaiBao, this.MaNguyenTe);
            lblTongTienThue.Text = string.Format("Tổng tiền thuế :  {0} (VND)", tongThue.ToString("N"));
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["NuocXX_ID"].Value != null)
                e.Row.Cells["NuocXX_ID"].Text = this.Nuoc_GetName(e.Row.Cells["NuocXX_ID"].Value.ToString());
            if (e.Row.Cells["DVT_ID"].Value != null)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void chkMienThue_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(this.NhomLoaiHinh)) return;
            switch (TKMD.LoaiHangHoa)
            {
                case "N":
                    //if (this.NPLRegistedForm == null)
                    this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                    this.NPLRegistedForm.isBrower = true;
                    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = TKMD.IDHopDong;
                    this.NPLRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.NPLRegistedForm.NguyenPhuLieuSelected.Ma))
                    {
                        txtMaHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                        txtTenHang.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                        txtMaHS.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3);
                    }
                    break;
                case "S":
                    //if (this.SPRegistedForm == null)
                    this.SPRegistedForm = new SanPhamRegistedForm();
                    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SPRegistedForm.SanPhamSelected.HopDong_ID = TKMD.IDHopDong;
                    this.SPRegistedForm.ShowDialog();
                    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                    {
                        txtMaHang.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtTenHang.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                    }
                    break;
                case "T":
                    ThietBiRegistedForm ftb = new ThietBiRegistedForm();
                    ftb.ThietBiSelected.HopDong_ID = TKMD.IDHopDong;
                    ftb.isBrower = true;
                    ftb.ShowDialog();
                    if (!string.IsNullOrEmpty(ftb.ThietBiSelected.Ma))
                    {
                        txtMaHang.Text = ftb.ThietBiSelected.Ma;
                        txtTenHang.Text = ftb.ThietBiSelected.Ten;
                        txtMaHS.Text = ftb.ThietBiSelected.MaHS;
                        cbDonViTinh.SelectedValue = ftb.ThietBiSelected.DVT_ID;
                    }
                    break;
                case "H":
                    HangMauRegistedForm hangmauForm = new HangMauRegistedForm();
                    hangmauForm.isBrower = true;
                    hangmauForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    hangmauForm.HangMauSelected.HopDong_ID = TKMD.IDHopDong;
                    hangmauForm.ShowDialog();
                    if (!string.IsNullOrEmpty(hangmauForm.HangMauSelected.Ma))
                    {
                        txtMaHang.Text = hangmauForm.HangMauSelected.Ma;
                        txtTenHang.Text = hangmauForm.HangMauSelected.Ten;
                        txtMaHS.Text = hangmauForm.HangMauSelected.MaHS;
                        cbDonViTinh.SelectedValue = hangmauForm.HangMauSelected.DVT_ID.PadRight(3);
                    }
                    break;
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            epError.Clear();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0) return;
            #region LanNT Comment thay doan nay bang doan duoi

            //foreach (GridEXSelectedItem i in items)
            //{
            //    if (i.RowType == RowType.Record)
            //    {
            //        HangMauDichEditForm f = new HangMauDichEditForm();
            //        f.HMD = this.HMDCollection[i.Position];

            //        f.NhomLoaiHinh = this.NhomLoaiHinh;
            //        f.LoaiHangHoa = this.LoaiHangHoa;
            //        f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        f.MaNguyenTe = this.MaNguyenTe;
            //        f.TyGiaTT = this.TyGiaTT;
            //        f.collection = this.HMDCollection;
            //        f.TKMD = this.TKMD;
            //        if (this.TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            //            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            //        else
            //            f.OpenType = Company.KDT.SHARE.Components.OpenFormType.View;
            //        if (f.HMD.MienThue == 1)
            //        {
            //            f.chkMienThue.Checked = true;
            //            f.chkMienThue_CheckedChanged(null, null);
            //        }

            //        f.ShowDialog();
            //        if (f.IsEdited)
            //        {
            //            if (f.HMD.TKMD_ID > 0)
            //                f.HMD.Update();
            //            else
            //                this.HMDCollection[i.Position] = f.HMD;
            //        }
            //        else if (f.IsDeleted)
            //        {
            //            if (f.HMD.TKMD_ID > 0)
            //                f.HMD.Delete();
            //            this.HMDCollection.RemoveAt(i.Position);
            //        }
            //        dgList.DataSource = this.HMDCollection;
            //        try
            //        {
            //            dgList.Refetch();
            //        }
            //        catch { dgList.DataSource = this.HMDCollection; }
            //        refresh_STTHang();
            //    }
            //    break;
            //}
            #endregion LanNT Comment thay doan nay bang doan duoi
            hangMauDich = (HangMauDich)items[0].GetRow().DataRow;
            SetHang(hangMauDich);
        }


        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
                {
                    HangMauDich hmd = (HangMauDich)e.Row.DataRow;
                    if (hmd.ID > 0)
                    {
                        hmd.Delete();
                    }
                    this.tongTGKB -= hmd.TriGiaKB;
                    lblTongTGKB.Text = string.Format(lblTongTGKB.Text + ": {0} ({1})", this.tongTGKB.ToString("N"), this.MaNguyenTe);
                }
                else
                {
                    e.Cancel = true;
                }
            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangMauDich hmd = (HangMauDich)i.GetRow().DataRow;
                        if (hmd.ID > 0)
                        {
                            if (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hóa đơn thương mại, không thể xóa.", false);
                                e.Cancel = true;
                                return;
                            }
                            else if (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hợp đồng thương mại, không thể xóa.", false);
                                e.Cancel = true;
                                return;
                            }

                            hmd.Delete();
                        }
                    }
                }
                refresh_STTHang();
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void SaoChepCha_Click(object sender, EventArgs e)
        {
            if (!(dgList.GetRow() != null && dgList.GetRow().RowType == RowType.Record))
                return;
            HangMauDich hmd = (HangMauDich)dgList.GetRow().DataRow;
            txtMaHang.Text = hmd.MaPhu;
            txtTenHang.Text = hmd.TenHang;
            txtMaHS.Text = hmd.MaHS;
            cbDonViTinh.SelectedValue = hmd.DVT_ID;
            ctrNuocXX.Ma = hmd.NuocXX_ID;
            txtDGNT.Value = this.dgnt = hmd.DonGiaKB;
            txtLuong.Value = this.luong = hmd.SoLuong;
            txtTGNT.Value = hmd.TriGiaKB;
            txtTriGiaKB.Value = hmd.TriGiaKB_VND;
            txtTGTT_NK.Value = hmd.TriGiaTT;
            txtTS_NK.Value = this.ts_nk = hmd.ThueSuatXNK;
            txtTS_TTDB.Value = this.ts_ttdb = hmd.ThueSuatTTDB;
            txtTS_GTGT.Value = this.ts_gtgt = hmd.ThueSuatGTGT;
            txtTL_CLG.Value = this.tl_clg = hmd.PhuThu;
            txtTriGiaThuKhac.Value = hmd.TriGiaThuKhac;
            txtTyLeThuKhac.Value = hmd.TyLeThuKhac;
            chkHangFOC.Checked = hmd.FOC;
            chkMienThue.Checked = hmd.ThueTuyetDoi;
            txtLuong.Focus();
        }

        private void txtTGTT_NK_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue2();
            else
                tinhthue3();
            txtTriGiaThuKhac.Value = (decimal.Parse(txtTGTT_NK.Text) / 100) * decimal.Parse(txtTyLeThuKhac.Text);

        }
        private void txtTyLeThuKhac_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            txtTriGiaThuKhac.Value = (Convert.ToDecimal(txtTGTT_NK.Value) / 100) * Convert.ToDecimal(txtTyLeThuKhac.Value);
        }
        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            //if (!MaHS.Validate(txtMaHS.Text, 10))
            //{
            //     txtMaHS.Focus();
            //    epError.SetIconPadding(txtMaHS, -8);
            //    epError.SetError(txtMaHS,setText("Mã HS không hợp lệ.","Invalid input value"));
            //}
            //else
            //{
            //    epError.SetError(txtMaHS, string.Empty);
            //}
            string MoTa = MaHS.CheckExist(txtMaHS.Text);
            if (MoTa == "")
            {
                txtMaHS.Focus();
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã HS không có trong danh mục mã HS.", "This HS is not exist"));
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            List<HangMauDich> hmdColl = new List<HangMauDich>();
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng hóa này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        hmdColl.Add((HangMauDich)i.GetRow().DataRow);
                    }

                }
                foreach (HangMauDich hmd in hmdColl)
                {
                    try
                    {
                        if (hmd.ID > 0)
                        {
                            if (Company.KDT.SHARE.QuanLyChungTu.HoaDonThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hóa đơn thương mại, không thể xóa.", false);
                                return;
                            }
                            else if (Company.KDT.SHARE.QuanLyChungTu.HopDongThuongMaiDetail.SelectCollectionBy_HMD_ID(hmd.ID).Count > 0)
                            {
                                new Company.Controls.KDTMessageBoxControl().ShowMessage("Thông tin hàng này đang sử dụng trong Chứng từ kèm Hợp đồng thương mại, không thể xóa.", false);
                                return;
                            }

                            hmd.Delete();
                        }
                        try
                        {
                            TKMD.HMDCollection.Remove(hmd);
                        }
                        catch { }
                    }
                    catch { }
                }
            }
            dgList.DataSource = TKMD.HMDCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            refresh_STTHang();
        }

        private void txtMaHS_TextChanged(object sender, EventArgs e)
        {

        }

        private void uiCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (chkThueTuyetDoi.Checked)
            {
                txtTS_NK.Enabled = false;
                txtTienThue_NK.ReadOnly = false;
                txtDonGiaTuyetDoi.Enabled = true;

                this.txtTienThue_NK.BackColor = Color.White;
            }
            else
            {
                txtTS_NK.Enabled = true;
                txtTienThue_NK.ReadOnly = true;
                txtDonGiaTuyetDoi.Enabled = false;
                this.txtTienThue_NK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            }
        }
        private void txtTienThue_NK_Leave(object sender, EventArgs e)
        {

        }

        private void txtDonGiaTuyetDoi_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }

        private void txtTGNT_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            if (GlobalSettings.TinhThueTGNT == "1")
            {
                this.luong = Convert.ToDecimal(txtLuong.Value);
                if (luong != 0)
                {
                    this.tgnt = Convert.ToDecimal(txtTGNT.Value);
                    txtDGNT.Text = Helpers.Format(tgnt / Convert.ToDecimal(luong), GlobalSettings.DonGiaNT);
                    dgnt = Convert.ToDecimal(txtDGNT.Value);
                    if (this.NhomLoaiHinh.Contains("N"))
                        tinhthue();
                    else
                        tinhthue3();
                }
            }
        }
        private void lblTyGiaTT_Click(object sender, EventArgs e)
        {

        }

        private void txtLuong_Click(object sender, EventArgs e)
        {

        }

        private void txtTriGiaThuKhac_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            if (Convert.ToDecimal(txtTGTT_NK.Value) == 0) return;
            txtTyLeThuKhac.Value = Convert.ToDecimal(txtTriGiaThuKhac.Value) / (Convert.ToDecimal(txtTGTT_NK.Value) / 100);
        }

        private void btnAddNew_Click(object sender, EventArgs e)
        {
            hangMauDich = new HangMauDich();
            hangMauDich.SoThuTuHang = 0;
            SetHang(hangMauDich);
            hangMauDich = null;
        }

        private void linkLabel1_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            Company.Interface.Report.CauHinhInForm f = new Company.Interface.Report.CauHinhInForm();
            f.Listlable.Add(f.lblDonGiaNT);
            f.Listlable.Add(f.lblTriGiaNT);
            if (TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "X")
                f.Listlable.Add(f.lblLuongSP);
            else
                f.Listlable.Add(f.lblLuongNPL);
            f.ShowDialog();
            this.txtDGNT.Focus();
            if (TKMD.MaLoaiHinh.Substring(0, 1).ToUpper() == "N")
                txtLuong.FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            else
                txtLuong.FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
            txtDGNT.FormatString = "N" + GlobalSettings.DonGiaNT;
            txtTriGiaKB.FormatString = "N" + GlobalSettings.TriGiaNT;
        }

        private void linkLabel2_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            Company.Interface.CauHinhToKhaiForm f = new Company.Interface.CauHinhToKhaiForm();
            f.Listlable.Add(f.lblPhuongThucTT);
            f.ShowDialog();
            this.txtDGNT.Focus();
        }

        private void txtTSBVMT_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            this.ts_bvmt = Convert.ToDecimal(txtTSBVMT.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }

        private void txtTSCPG_Leave(object sender, EventArgs e)
        {
            if (!Company.KDT.SHARE.Components.Globals.IsTinhThue)
                return;
            this.ts_chongbanphagia = Convert.ToDecimal(txtTSCPG.Value) / 100;
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }

        private void lblCopyBieuThue_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            if (ShowMessage("Bạn có muốn thiết lập biểu thuế và thuế suất tương tự cho tất cả các hàng trong tờ khai", true) == "Yes")
            {
                foreach (HangMauDich hmd in TKMD.HMDCollection)
                {
                    hmd.BieuThueXNK = cbBieuThueXNK.SelectedValue.ToString();
                    hmd.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Value);
                    hmd.BieuThueTTDB = cbBieuThueTTDB.SelectedValue.ToString();
                    hmd.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Value);
                    hmd.BieuThueBVMT = cbBieuThueBVMT.SelectedValue.ToString();
                    hmd.ThueSuatBVMT = Convert.ToDecimal(txtTSBVMT.Value);
                    hmd.BieuThueGTGT = cbBieuThueGTGT.SelectedValue.ToString();
                    hmd.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Value);
                }
                tinhLaiThue();
            }
        }
        private void tinhLaiThue()
        {
            this.TKMD.TyGiaTinhThue = this.TyGiaTT;
            this.TKMD.TongTriGiaKhaiBao = 0;
            decimal Phi = TKMD.PhiBaoHiem + TKMD.PhiKhac + TKMD.PhiVanChuyen;
            decimal TongTriGiaHang = 0;
            foreach (Company.GC.BLL.KDT.HangMauDich hmd in this.TKMD.HMDCollection)
            {
                TongTriGiaHang += hmd.TriGiaKB;
                this.TKMD.TongTriGiaKhaiBao += Convert.ToDecimal(hmd.TriGiaKB);
            }
            decimal TriGiaTTMotDong = 0;
            if (TongTriGiaHang == 0) return;
            TriGiaTTMotDong = (Phi == 0 || GlobalSettings.TuDongTinhThue == "0") ? this.TyGiaTT : (1 + Phi / TongTriGiaHang) * TKMD.TyGiaTinhThue;
            this.TKMD.TongTriGiaTinhThue = 0;
            foreach (Company.GC.BLL.KDT.HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (hmd.FOC)
                {
                    hmd.TriGiaTT = hmd.ThueXNK = hmd.ThueTTDB = hmd.ThueGTGT = hmd.TriGiaThuKhac = hmd.ThueSuatXNK = hmd.ThueSuatTTDB = hmd.ThueSuatGTGT = hmd.TyLeThuKhac = 0;
                    continue;
                }

                hmd.DonGiaTT = TriGiaTTMotDong * hmd.DonGiaKB;
                hmd.TriGiaTT = Math.Round(hmd.DonGiaTT * hmd.SoLuong, MidpointRounding.AwayFromZero);
                this.TKMD.TongTriGiaTinhThue += Convert.ToDecimal(hmd.TriGiaTT);
                if (!hmd.ThueTuyetDoi)
                    hmd.ThueXNK = Math.Round((hmd.TriGiaTT * hmd.ThueSuatXNK) / 100, MidpointRounding.AwayFromZero);
                else
                {
                    hmd.ThueXNK = Math.Round(Convert.ToDecimal(hmd.DonGiaTuyetDoi) * TKMD.TyGiaTinhThue * hmd.SoLuong);
                }
                hmd.ThueTTDB = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * hmd.ThueSuatTTDB / 100, MidpointRounding.AwayFromZero);
                hmd.ThueBVMT = hmd.ThueSuatBVMT > 0 ? Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB) * Convert.ToDecimal(hmd.ThueSuatBVMT) / 100) : hmd.ThueBVMT;
                hmd.ThueChongPhaGia = Math.Round((hmd.ThueXNK + hmd.TriGiaTT) * Convert.ToDecimal(hmd.ThueSuatChongPhaGia));
                hmd.TriGiaThuKhac = hmd.TyLeThuKhac > 0 ? Math.Round(hmd.TriGiaTT * hmd.TyLeThuKhac / 100, MidpointRounding.AwayFromZero) : hmd.TriGiaThuKhac;
                hmd.ThueGTGT = Math.Round((hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueChongPhaGia + hmd.ThueTTDB + hmd.TriGiaThuKhac + hmd.ThueBVMT) * hmd.ThueSuatGTGT / 100, MidpointRounding.AwayFromZero);
            }
            dgList.DataSource = this.TKMD.HMDCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        bool AddNewChungTu = false;
        private void btnChungTuTruoc_Click(object sender, EventArgs e)
        {
            ListChungTuTruocDoForm f = new ListChungTuTruocDoForm();
            if (hangMauDich == null)
                f.Master_ID = 0;
            else if (hangMauDich.ThuTucHQTruocDo != null)
                f.ThuTucTruoc = hangMauDich.ThuTucHQTruocDo;
            else
                f.Master_ID = hangMauDich.ID;
            f.Type = "HMD";
            f.ShowDialog(this);
            if (f.DialogResult == System.Windows.Forms.DialogResult.OK)
            {
                if (hangMauDich == null)
                {
                    hangMauDich = new HangMauDich();
                    hangMauDich.SoThuTuHang = 0;
                    AddNewChungTu = true;
                }
                else
                    AddNewChungTu = false;
                hangMauDich.ThuTucHQTruocDo = f.ThuTucTruoc;
            }
        }

        private void linkLabel3_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Value);
            this.luong = Convert.ToDecimal(txtLuong.Value);
            this.tgnt = Math.Round(this.dgnt * this.luong,Company.KDT.SHARE.Components.Globals.TriGiaNT);

            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value);
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value);
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value);
            
            txtTGNT.Value = Helpers.FormatNumeric(this.dgnt * this.luong, Company.KDT.SHARE.Components.Globals.TriGiaNT);
            if (this.NhomLoaiHinh.Contains("N"))
                tinhthue();
            else
                tinhthue3();
        }
    }
}
