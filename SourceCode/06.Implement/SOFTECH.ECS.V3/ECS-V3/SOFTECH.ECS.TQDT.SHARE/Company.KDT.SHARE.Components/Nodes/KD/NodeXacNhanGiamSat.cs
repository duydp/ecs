﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.KD
{
    public class NodeXacNhanGiamSat
    {
        public const string AdditionalInformation = "AdditionalInformation";

        /// <summary>
        ///Thông tin xác nhận hàng ra khỏi khu vực giám sát	1	an..512
        /// </summary>
        public const string content = "content";

        /// <summary>
        /// Mã nội dung phản hồi	0	an..3	HQ cấp
        /// </summary>
        public const string statement = "statement";	
    }
}
