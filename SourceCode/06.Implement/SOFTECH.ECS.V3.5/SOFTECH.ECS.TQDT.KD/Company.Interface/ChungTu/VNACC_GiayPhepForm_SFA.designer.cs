﻿namespace Company.Interface
{
    partial class VNACC_GiayPhepForm_SFA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_GiayPhepForm_SFA));
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdChiThiHaiQuan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHaiQuan");
            this.cmdKetQuaHQ1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaHQ");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdlayPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdlayPhanHoi");
            this.cmdChiThiHaiQuan = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHaiQuan");
            this.cmdKetQuaHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaHQ");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdToolBar = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucLoaiGiayPhep = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucMaDonViCapPhep = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucPhanLoaiTraCuu = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucChucNangChungTu = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label19 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtTenDonViCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSoGiayPhepKQXL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtSoDonXinCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dtHieuLucDenNgayKQXL = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtHieuLucTuNgayKQXL = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayCapKQXL = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label23 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMaKQXL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.dtNgayKhaiBao = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucBenDen = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.ucBenDi = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.txtGiaTriHangHoa = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucMaQuocGiaNhapKhau = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtMaThuongNhanNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTenThuongNhanNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtMaBuuChinhNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.txtSoNhaTenDuongNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.txtEmailNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoFaxNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoDienThoaiNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.pnlHoSoKemTheo = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.txt10 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txt9 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txt8 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txt7 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txt6 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txt5 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txt4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txt3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txt2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txt1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblHoSoKemTheo = new System.Windows.Forms.Label();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucMaQuocGiaXuatKhau = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTenThuongNhanXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaBuuChinhXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtQuanHuyenXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoNhaTenDuongXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEmailXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoFaxXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoDienThoaiXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTinhThanhPhoXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhuongXaXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtThoiGianKiemTra = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label38 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.dtThoiGianNhapKhauDuKien = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label24 = new System.Windows.Forms.Label();
            this.txtDaiDienCoQuanKiemTra = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaDiemKiemTra = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaDiemTapKetHangHoa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDaiDienThuongNhanNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtHoSoLienQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChuCoQuanCapGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label58 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToolBar)).BeginInit();
            this.cmdToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            this.pnlHoSoKemTheo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 843), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 35);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 843);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 819);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 819);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Location = new System.Drawing.Point(203, 35);
            this.grbMain.Size = new System.Drawing.Size(753, 843);
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang,
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdlayPhanHoi,
            this.cmdChiThiHaiQuan,
            this.cmdKetQuaHQ});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.TopRebar = this.cmdToolBar;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdLuu1,
            this.Separator1,
            this.cmdKhaiBao1,
            this.Separator2,
            this.cmdChiThiHaiQuan1,
            this.cmdKetQuaHQ1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(959, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdChiThiHaiQuan1
            // 
            this.cmdChiThiHaiQuan1.Key = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan1.Name = "cmdChiThiHaiQuan1";
            // 
            // cmdKetQuaHQ1
            // 
            this.cmdKetQuaHQ1.Key = "cmdKetQuaHQ";
            this.cmdKetQuaHQ1.Name = "cmdKetQuaHQ1";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Image = ((System.Drawing.Image)(resources.GetObject("cmdThemHang.Image")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Image = ((System.Drawing.Image)(resources.GetObject("cmdLuu.Image")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("cmdKhaiBao.Image")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdlayPhanHoi
            // 
            this.cmdlayPhanHoi.Image = ((System.Drawing.Image)(resources.GetObject("cmdlayPhanHoi.Image")));
            this.cmdlayPhanHoi.Key = "cmdlayPhanHoi";
            this.cmdlayPhanHoi.Name = "cmdlayPhanHoi";
            this.cmdlayPhanHoi.Text = "Lấy phản hồi";
            // 
            // cmdChiThiHaiQuan
            // 
            this.cmdChiThiHaiQuan.Image = ((System.Drawing.Image)(resources.GetObject("cmdChiThiHaiQuan.Image")));
            this.cmdChiThiHaiQuan.Key = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan.Name = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan.Text = "Chỉ thị Hải quan";
            // 
            // cmdKetQuaHQ
            // 
            this.cmdKetQuaHQ.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaHQ.Image")));
            this.cmdKetQuaHQ.Key = "cmdKetQuaHQ";
            this.cmdKetQuaHQ.Name = "cmdKetQuaHQ";
            this.cmdKetQuaHQ.Text = "Thông tin từ HQ";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmdToolBar
            // 
            this.cmdToolBar.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdToolBar.CommandManager = this.cmbMain;
            this.cmdToolBar.Controls.Add(this.uiCommandBar1);
            this.cmdToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmdToolBar.Name = "cmdToolBar";
            this.cmdToolBar.Size = new System.Drawing.Size(959, 32);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.ucLoaiGiayPhep);
            this.uiGroupBox1.Controls.Add(this.ucMaDonViCapPhep);
            this.uiGroupBox1.Controls.Add(this.ucPhanLoaiTraCuu);
            this.uiGroupBox1.Controls.Add(this.ucChucNangChungTu);
            this.uiGroupBox1.Controls.Add(this.label19);
            this.uiGroupBox1.Controls.Add(this.label45);
            this.uiGroupBox1.Controls.Add(this.label44);
            this.uiGroupBox1.Controls.Add(this.label43);
            this.uiGroupBox1.Controls.Add(this.txtTenDonViCapPhep);
            this.uiGroupBox1.Controls.Add(this.txtTenNguoiKhai);
            this.uiGroupBox1.Controls.Add(this.txtMaNguoiKhai);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.txtSoGiayPhepKQXL);
            this.uiGroupBox1.Controls.Add(this.label42);
            this.uiGroupBox1.Controls.Add(this.txtSoDonXinCapPhep);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.dtHieuLucDenNgayKQXL);
            this.uiGroupBox1.Controls.Add(this.dtHieuLucTuNgayKQXL);
            this.uiGroupBox1.Controls.Add(this.dtNgayCapKQXL);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.label17);
            this.uiGroupBox1.Controls.Add(this.txtMaKQXL);
            this.uiGroupBox1.Controls.Add(this.label41);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(753, 184);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // ucLoaiGiayPhep
            // 
            this.ucLoaiGiayPhep.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucLoaiGiayPhep.Appearance.Options.UseBackColor = true;
            this.ucLoaiGiayPhep.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A528;
            this.ucLoaiGiayPhep.Code = "";
            this.ucLoaiGiayPhep.ColorControl = System.Drawing.Color.Empty;
            this.ucLoaiGiayPhep.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucLoaiGiayPhep.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucLoaiGiayPhep.IsOnlyWarning = true;
            this.ucLoaiGiayPhep.IsValidate = false;
            this.ucLoaiGiayPhep.Location = new System.Drawing.Point(399, 68);
            this.ucLoaiGiayPhep.Name = "ucLoaiGiayPhep";
            this.ucLoaiGiayPhep.Name_VN = "";
            this.ucLoaiGiayPhep.SetOnlyWarning = true;
            this.ucLoaiGiayPhep.SetValidate = true;
            this.ucLoaiGiayPhep.ShowColumnCode = true;
            this.ucLoaiGiayPhep.ShowColumnName = false;
            this.ucLoaiGiayPhep.Size = new System.Drawing.Size(114, 21);
            this.ucLoaiGiayPhep.TabIndex = 5;
            this.ucLoaiGiayPhep.TagName = "";
            this.ucLoaiGiayPhep.Where = null;
            this.ucLoaiGiayPhep.WhereCondition = "";
            // 
            // ucMaDonViCapPhep
            // 
            this.ucMaDonViCapPhep.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaDonViCapPhep.Appearance.Options.UseBackColor = true;
            this.ucMaDonViCapPhep.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A700;
            this.ucMaDonViCapPhep.Code = "";
            this.ucMaDonViCapPhep.ColorControl = System.Drawing.Color.Empty;
            this.ucMaDonViCapPhep.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaDonViCapPhep.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaDonViCapPhep.IsOnlyWarning = true;
            this.ucMaDonViCapPhep.IsValidate = false;
            this.ucMaDonViCapPhep.Location = new System.Drawing.Point(146, 100);
            this.ucMaDonViCapPhep.Name = "ucMaDonViCapPhep";
            this.ucMaDonViCapPhep.Name_VN = "";
            this.ucMaDonViCapPhep.SetOnlyWarning = true;
            this.ucMaDonViCapPhep.SetValidate = true;
            this.ucMaDonViCapPhep.ShowColumnCode = true;
            this.ucMaDonViCapPhep.ShowColumnName = false;
            this.ucMaDonViCapPhep.Size = new System.Drawing.Size(114, 21);
            this.ucMaDonViCapPhep.TabIndex = 6;
            this.ucMaDonViCapPhep.TagName = "";
            this.ucMaDonViCapPhep.Where = null;
            this.ucMaDonViCapPhep.WhereCondition = "";
            // 
            // ucPhanLoaiTraCuu
            // 
            this.ucPhanLoaiTraCuu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucPhanLoaiTraCuu.Appearance.Options.UseBackColor = true;
            this.ucPhanLoaiTraCuu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E026;
            this.ucPhanLoaiTraCuu.Code = "";
            this.ucPhanLoaiTraCuu.ColorControl = System.Drawing.Color.Empty;
            this.ucPhanLoaiTraCuu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucPhanLoaiTraCuu.Enabled = false;
            this.ucPhanLoaiTraCuu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucPhanLoaiTraCuu.IsOnlyWarning = false;
            this.ucPhanLoaiTraCuu.IsValidate = true;
            this.ucPhanLoaiTraCuu.Location = new System.Drawing.Point(399, 38);
            this.ucPhanLoaiTraCuu.Name = "ucPhanLoaiTraCuu";
            this.ucPhanLoaiTraCuu.Name_VN = "";
            this.ucPhanLoaiTraCuu.SetOnlyWarning = false;
            this.ucPhanLoaiTraCuu.SetValidate = false;
            this.ucPhanLoaiTraCuu.ShowColumnCode = true;
            this.ucPhanLoaiTraCuu.ShowColumnName = false;
            this.ucPhanLoaiTraCuu.Size = new System.Drawing.Size(114, 26);
            this.ucPhanLoaiTraCuu.TabIndex = 3;
            this.ucPhanLoaiTraCuu.TagName = "";
            this.ucPhanLoaiTraCuu.Where = null;
            this.ucPhanLoaiTraCuu.WhereCondition = "";
            this.ucPhanLoaiTraCuu.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucChucNangChungTu
            // 
            this.ucChucNangChungTu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucChucNangChungTu.Appearance.Options.UseBackColor = true;
            this.ucChucNangChungTu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E025;
            this.ucChucNangChungTu.Code = "";
            this.ucChucNangChungTu.ColorControl = System.Drawing.Color.Empty;
            this.ucChucNangChungTu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucChucNangChungTu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucChucNangChungTu.IsOnlyWarning = false;
            this.ucChucNangChungTu.IsValidate = true;
            this.ucChucNangChungTu.Location = new System.Drawing.Point(146, 68);
            this.ucChucNangChungTu.Name = "ucChucNangChungTu";
            this.ucChucNangChungTu.Name_VN = "";
            this.ucChucNangChungTu.SetOnlyWarning = false;
            this.ucChucNangChungTu.SetValidate = false;
            this.ucChucNangChungTu.ShowColumnCode = true;
            this.ucChucNangChungTu.ShowColumnName = true;
            this.ucChucNangChungTu.Size = new System.Drawing.Size(114, 26);
            this.ucChucNangChungTu.TabIndex = 4;
            this.ucChucNangChungTu.TagName = "";
            this.ucChucNangChungTu.Where = null;
            this.ucChucNangChungTu.WhereCondition = "";
            this.ucChucNangChungTu.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(15, 76);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(126, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Chức năng của chứng từ";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(443, 168);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(52, 13);
            this.label45.TabIndex = 21;
            this.label45.Text = "đến ngày";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(196, 168);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(86, 13);
            this.label44.TabIndex = 19;
            this.label44.Text = "Hiệu lực từ ngày";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(416, 137);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(79, 13);
            this.label43.TabIndex = 17;
            this.label43.Text = "Ngày cấp phép";
            // 
            // txtTenDonViCapPhep
            // 
            this.txtTenDonViCapPhep.Location = new System.Drawing.Point(266, 100);
            this.txtTenDonViCapPhep.Name = "txtTenDonViCapPhep";
            this.txtTenDonViCapPhep.Size = new System.Drawing.Size(465, 21);
            this.txtTenDonViCapPhep.TabIndex = 7;
            // 
            // txtTenNguoiKhai
            // 
            this.txtTenNguoiKhai.Location = new System.Drawing.Point(266, 14);
            this.txtTenNguoiKhai.Name = "txtTenNguoiKhai";
            this.txtTenNguoiKhai.Size = new System.Drawing.Size(465, 21);
            this.txtTenNguoiKhai.TabIndex = 1;
            // 
            // txtMaNguoiKhai
            // 
            this.txtMaNguoiKhai.Location = new System.Drawing.Point(146, 14);
            this.txtMaNguoiKhai.Name = "txtMaNguoiKhai";
            this.txtMaNguoiKhai.Size = new System.Drawing.Size(114, 21);
            this.txtMaNguoiKhai.TabIndex = 0;
            this.txtMaNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(67, 19);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Mã người khai";
            // 
            // txtSoGiayPhepKQXL
            // 
            this.txtSoGiayPhepKQXL.Enabled = false;
            this.txtSoGiayPhepKQXL.Location = new System.Drawing.Point(288, 133);
            this.txtSoGiayPhepKQXL.Name = "txtSoGiayPhepKQXL";
            this.txtSoGiayPhepKQXL.Size = new System.Drawing.Size(109, 21);
            this.txtSoGiayPhepKQXL.TabIndex = 9;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(213, 137);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(69, 13);
            this.label42.TabIndex = 15;
            this.label42.Text = "Số giấy phép";
            // 
            // txtSoDonXinCapPhep
            // 
            this.txtSoDonXinCapPhep.Location = new System.Drawing.Point(146, 41);
            this.txtSoDonXinCapPhep.Name = "txtSoDonXinCapPhep";
            this.txtSoDonXinCapPhep.ReadOnly = true;
            this.txtSoDonXinCapPhep.Size = new System.Drawing.Size(114, 21);
            this.txtSoDonXinCapPhep.TabIndex = 2;
            this.txtSoDonXinCapPhep.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(36, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Số đơn xin cấp phép";
            // 
            // dtHieuLucDenNgayKQXL
            // 
            // 
            // 
            // 
            this.dtHieuLucDenNgayKQXL.DropDownCalendar.Name = "";
            this.dtHieuLucDenNgayKQXL.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtHieuLucDenNgayKQXL.Enabled = false;
            this.dtHieuLucDenNgayKQXL.IsNullDate = true;
            this.dtHieuLucDenNgayKQXL.Location = new System.Drawing.Point(501, 160);
            this.dtHieuLucDenNgayKQXL.Name = "dtHieuLucDenNgayKQXL";
            this.dtHieuLucDenNgayKQXL.Size = new System.Drawing.Size(100, 21);
            this.dtHieuLucDenNgayKQXL.TabIndex = 12;
            this.dtHieuLucDenNgayKQXL.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtHieuLucTuNgayKQXL
            // 
            // 
            // 
            // 
            this.dtHieuLucTuNgayKQXL.DropDownCalendar.Name = "";
            this.dtHieuLucTuNgayKQXL.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtHieuLucTuNgayKQXL.Enabled = false;
            this.dtHieuLucTuNgayKQXL.IsNullDate = true;
            this.dtHieuLucTuNgayKQXL.Location = new System.Drawing.Point(288, 160);
            this.dtHieuLucTuNgayKQXL.Name = "dtHieuLucTuNgayKQXL";
            this.dtHieuLucTuNgayKQXL.Size = new System.Drawing.Size(109, 21);
            this.dtHieuLucTuNgayKQXL.TabIndex = 11;
            this.dtHieuLucTuNgayKQXL.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtNgayCapKQXL
            // 
            // 
            // 
            // 
            this.dtNgayCapKQXL.DropDownCalendar.Name = "";
            this.dtNgayCapKQXL.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayCapKQXL.Enabled = false;
            this.dtNgayCapKQXL.IsNullDate = true;
            this.dtNgayCapKQXL.Location = new System.Drawing.Point(501, 133);
            this.dtNgayCapKQXL.Name = "dtNgayCapKQXL";
            this.dtNgayCapKQXL.Size = new System.Drawing.Size(100, 21);
            this.dtNgayCapKQXL.TabIndex = 10;
            this.dtNgayCapKQXL.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(305, 46);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(88, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Phân loại tra cứu";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(317, 76);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Loại giấy phép";
            // 
            // txtMaKQXL
            // 
            this.txtMaKQXL.Enabled = false;
            this.txtMaKQXL.Location = new System.Drawing.Point(146, 133);
            this.txtMaKQXL.Name = "txtMaKQXL";
            this.txtMaKQXL.Size = new System.Drawing.Size(52, 21);
            this.txtMaKQXL.TabIndex = 8;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(53, 137);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(87, 13);
            this.label41.TabIndex = 13;
            this.label41.Text = "Mã kết quả xử lý";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(40, 105);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Mã đơn vị cấp phép";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(3, 661);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(75, 13);
            this.label26.TabIndex = 26;
            this.label26.Text = "Ngày khai báo";
            // 
            // dtNgayKhaiBao
            // 
            // 
            // 
            // 
            this.dtNgayKhaiBao.DropDownCalendar.Name = "";
            this.dtNgayKhaiBao.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayKhaiBao.Enabled = false;
            this.dtNgayKhaiBao.IsNullDate = true;
            this.dtNgayKhaiBao.Location = new System.Drawing.Point(146, 653);
            this.dtNgayKhaiBao.Name = "dtNgayKhaiBao";
            this.dtNgayKhaiBao.Size = new System.Drawing.Size(100, 21);
            this.dtNgayKhaiBao.TabIndex = 16;
            this.dtNgayKhaiBao.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.ucBenDen);
            this.uiGroupBox2.Controls.Add(this.ucBenDi);
            this.uiGroupBox2.Controls.Add(this.txtGiaTriHangHoa);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox2.Controls.Add(this.pnlHoSoKemTheo);
            this.uiGroupBox2.Controls.Add(this.lblHoSoKemTheo);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox3);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.dtThoiGianKiemTra);
            this.uiGroupBox2.Controls.Add(this.label38);
            this.uiGroupBox2.Controls.Add(this.label25);
            this.uiGroupBox2.Controls.Add(this.label26);
            this.uiGroupBox2.Controls.Add(this.dtThoiGianNhapKhauDuKien);
            this.uiGroupBox2.Controls.Add(this.label24);
            this.uiGroupBox2.Controls.Add(this.txtDaiDienCoQuanKiemTra);
            this.uiGroupBox2.Controls.Add(this.txtDiaDiemKiemTra);
            this.uiGroupBox2.Controls.Add(this.txtDiaDiemTapKetHangHoa);
            this.uiGroupBox2.Controls.Add(this.txtDaiDienThuongNhanNhapKhau);
            this.uiGroupBox2.Controls.Add(this.txtHoSoLienQuan);
            this.uiGroupBox2.Controls.Add(this.txtGhiChuCoQuanCapGiayPhep);
            this.uiGroupBox2.Controls.Add(this.txtGhiChu);
            this.uiGroupBox2.Controls.Add(this.label12);
            this.uiGroupBox2.Controls.Add(this.label16);
            this.uiGroupBox2.Controls.Add(this.dtNgayKhaiBao);
            this.uiGroupBox2.Controls.Add(this.label47);
            this.uiGroupBox2.Controls.Add(this.label48);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.label39);
            this.uiGroupBox2.Controls.Add(this.label37);
            this.uiGroupBox2.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox2.Controls.Add(this.label46);
            this.uiGroupBox2.Controls.Add(this.label36);
            this.uiGroupBox2.Controls.Add(this.txtSoVanDon);
            this.uiGroupBox2.Controls.Add(this.label58);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.Top;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 184);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(753, 659);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // ucBenDen
            // 
            this.ucBenDen.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucBenDen.Appearance.Options.UseBackColor = true;
            this.ucBenDen.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ucBenDen.Code = "";
            this.ucBenDen.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucBenDen.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucBenDen.IsOnlyWarning = false;
            this.ucBenDen.IsValidate = true;
            this.ucBenDen.Location = new System.Drawing.Point(146, 395);
            this.ucBenDen.Name = "ucBenDen";
            this.ucBenDen.Name_VN = "";
            this.ucBenDen.SetOnlyWarning = false;
            this.ucBenDen.SetValidate = false;
            this.ucBenDen.ShowColumnCode = true;
            this.ucBenDen.ShowColumnName = true;
            this.ucBenDen.Size = new System.Drawing.Size(296, 21);
            this.ucBenDen.TabIndex = 8;
            this.ucBenDen.TagCode = "";
            this.ucBenDen.TagName = "";
            this.ucBenDen.Where = null;
            this.ucBenDen.WhereCondition = "";
            // 
            // ucBenDi
            // 
            this.ucBenDi.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucBenDi.Appearance.Options.UseBackColor = true;
            this.ucBenDi.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A016;
            this.ucBenDi.Code = "";
            this.ucBenDi.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucBenDi.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucBenDi.IsOnlyWarning = false;
            this.ucBenDi.IsValidate = true;
            this.ucBenDi.Location = new System.Drawing.Point(117, 199);
            this.ucBenDi.Name = "ucBenDi";
            this.ucBenDi.Name_VN = "";
            this.ucBenDi.SetOnlyWarning = false;
            this.ucBenDi.SetValidate = false;
            this.ucBenDi.ShowColumnCode = true;
            this.ucBenDi.ShowColumnName = true;
            this.ucBenDi.Size = new System.Drawing.Size(296, 21);
            this.ucBenDi.TabIndex = 2;
            this.ucBenDi.TagCode = "";
            this.ucBenDi.TagName = "";
            this.ucBenDi.Where = null;
            this.ucBenDi.WhereCondition = "";
            // 
            // txtGiaTriHangHoa
            // 
            this.txtGiaTriHangHoa.DecimalDigits = 2;
            this.txtGiaTriHangHoa.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtGiaTriHangHoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGiaTriHangHoa.Location = new System.Drawing.Point(146, 423);
            this.txtGiaTriHangHoa.MaxLength = 19;
            this.txtGiaTriHangHoa.Name = "txtGiaTriHangHoa";
            this.txtGiaTriHangHoa.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtGiaTriHangHoa.Size = new System.Drawing.Size(144, 21);
            this.txtGiaTriHangHoa.TabIndex = 10;
            this.txtGiaTriHangHoa.Text = "0.00";
            this.txtGiaTriHangHoa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGiaTriHangHoa.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.txtGiaTriHangHoa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.ucMaQuocGiaNhapKhau);
            this.uiGroupBox4.Controls.Add(this.txtMaThuongNhanNhapKhau);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.txtTenThuongNhanNhapKhau);
            this.uiGroupBox4.Controls.Add(this.label20);
            this.uiGroupBox4.Controls.Add(this.txtMaBuuChinhNhapKhau);
            this.uiGroupBox4.Controls.Add(this.label52);
            this.uiGroupBox4.Controls.Add(this.label53);
            this.uiGroupBox4.Controls.Add(this.label54);
            this.uiGroupBox4.Controls.Add(this.label55);
            this.uiGroupBox4.Controls.Add(this.txtSoNhaTenDuongNhapKhau);
            this.uiGroupBox4.Controls.Add(this.label56);
            this.uiGroupBox4.Controls.Add(this.label57);
            this.uiGroupBox4.Controls.Add(this.txtEmailNhapKhau);
            this.uiGroupBox4.Controls.Add(this.txtSoFaxNhapKhau);
            this.uiGroupBox4.Controls.Add(this.txtSoDienThoaiNhapKhau);
            this.uiGroupBox4.Location = new System.Drawing.Point(2, 232);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(738, 153);
            this.uiGroupBox4.TabIndex = 7;
            this.uiGroupBox4.Text = "Doanh nghiệp Nhập khẩu";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // ucMaQuocGiaNhapKhau
            // 
            this.ucMaQuocGiaNhapKhau.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaQuocGiaNhapKhau.Appearance.Options.UseBackColor = true;
            this.ucMaQuocGiaNhapKhau.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaQuocGiaNhapKhau.Code = "";
            this.ucMaQuocGiaNhapKhau.ColorControl = System.Drawing.Color.Empty;
            this.ucMaQuocGiaNhapKhau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaQuocGiaNhapKhau.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaQuocGiaNhapKhau.IsOnlyWarning = false;
            this.ucMaQuocGiaNhapKhau.IsValidate = true;
            this.ucMaQuocGiaNhapKhau.Location = new System.Drawing.Point(115, 126);
            this.ucMaQuocGiaNhapKhau.Name = "ucMaQuocGiaNhapKhau";
            this.ucMaQuocGiaNhapKhau.Name_VN = "";
            this.ucMaQuocGiaNhapKhau.SetOnlyWarning = false;
            this.ucMaQuocGiaNhapKhau.SetValidate = false;
            this.ucMaQuocGiaNhapKhau.ShowColumnCode = true;
            this.ucMaQuocGiaNhapKhau.ShowColumnName = false;
            this.ucMaQuocGiaNhapKhau.Size = new System.Drawing.Size(58, 26);
            this.ucMaQuocGiaNhapKhau.TabIndex = 4;
            this.ucMaQuocGiaNhapKhau.TagName = "";
            this.ucMaQuocGiaNhapKhau.Where = null;
            this.ucMaQuocGiaNhapKhau.WhereCondition = "";
            this.ucMaQuocGiaNhapKhau.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtMaThuongNhanNhapKhau
            // 
            this.txtMaThuongNhanNhapKhau.Location = new System.Drawing.Point(115, 19);
            this.txtMaThuongNhanNhapKhau.Name = "txtMaThuongNhanNhapKhau";
            this.txtMaThuongNhanNhapKhau.Size = new System.Drawing.Size(162, 21);
            this.txtMaThuongNhanNhapKhau.TabIndex = 0;
            this.txtMaThuongNhanNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(21, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Mã";
            // 
            // txtTenThuongNhanNhapKhau
            // 
            this.txtTenThuongNhanNhapKhau.Location = new System.Drawing.Point(115, 46);
            this.txtTenThuongNhanNhapKhau.Name = "txtTenThuongNhanNhapKhau";
            this.txtTenThuongNhanNhapKhau.Size = new System.Drawing.Size(614, 21);
            this.txtTenThuongNhanNhapKhau.TabIndex = 1;
            this.txtTenThuongNhanNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 50);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(25, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Tên";
            // 
            // txtMaBuuChinhNhapKhau
            // 
            this.txtMaBuuChinhNhapKhau.Location = new System.Drawing.Point(115, 73);
            this.txtMaBuuChinhNhapKhau.Name = "txtMaBuuChinhNhapKhau";
            this.txtMaBuuChinhNhapKhau.Size = new System.Drawing.Size(162, 21);
            this.txtMaBuuChinhNhapKhau.TabIndex = 2;
            this.txtMaBuuChinhNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(355, 131);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(40, 13);
            this.label52.TabIndex = 12;
            this.label52.Text = "Số Fax";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 104);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(39, 13);
            this.label53.TabIndex = 6;
            this.label53.Text = "Địa chỉ";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(546, 132);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(31, 13);
            this.label54.TabIndex = 14;
            this.label54.Text = "Email";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(6, 78);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(71, 13);
            this.label55.TabIndex = 4;
            this.label55.Text = "Mã bưu chính";
            // 
            // txtSoNhaTenDuongNhapKhau
            // 
            this.txtSoNhaTenDuongNhapKhau.Location = new System.Drawing.Point(115, 100);
            this.txtSoNhaTenDuongNhapKhau.Name = "txtSoNhaTenDuongNhapKhau";
            this.txtSoNhaTenDuongNhapKhau.Size = new System.Drawing.Size(614, 21);
            this.txtSoNhaTenDuongNhapKhau.TabIndex = 3;
            this.txtSoNhaTenDuongNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(179, 131);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(56, 13);
            this.label56.TabIndex = 10;
            this.label56.Text = "Điện thoại";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(6, 131);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(64, 13);
            this.label57.TabIndex = 8;
            this.label57.Text = "Mã quốc gia";
            // 
            // txtEmailNhapKhau
            // 
            this.txtEmailNhapKhau.Location = new System.Drawing.Point(583, 127);
            this.txtEmailNhapKhau.Name = "txtEmailNhapKhau";
            this.txtEmailNhapKhau.Size = new System.Drawing.Size(146, 21);
            this.txtEmailNhapKhau.TabIndex = 7;
            this.txtEmailNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtSoFaxNhapKhau
            // 
            this.txtSoFaxNhapKhau.Location = new System.Drawing.Point(404, 127);
            this.txtSoFaxNhapKhau.Name = "txtSoFaxNhapKhau";
            this.txtSoFaxNhapKhau.Size = new System.Drawing.Size(137, 21);
            this.txtSoFaxNhapKhau.TabIndex = 6;
            this.txtSoFaxNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtSoDienThoaiNhapKhau
            // 
            this.txtSoDienThoaiNhapKhau.Location = new System.Drawing.Point(236, 127);
            this.txtSoDienThoaiNhapKhau.Name = "txtSoDienThoaiNhapKhau";
            this.txtSoDienThoaiNhapKhau.Size = new System.Drawing.Size(113, 21);
            this.txtSoDienThoaiNhapKhau.TabIndex = 5;
            this.txtSoDienThoaiNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // pnlHoSoKemTheo
            // 
            this.pnlHoSoKemTheo.Controls.Add(this.label40);
            this.pnlHoSoKemTheo.Controls.Add(this.txt10);
            this.pnlHoSoKemTheo.Controls.Add(this.label35);
            this.pnlHoSoKemTheo.Controls.Add(this.txt9);
            this.pnlHoSoKemTheo.Controls.Add(this.label34);
            this.pnlHoSoKemTheo.Controls.Add(this.txt8);
            this.pnlHoSoKemTheo.Controls.Add(this.label33);
            this.pnlHoSoKemTheo.Controls.Add(this.txt7);
            this.pnlHoSoKemTheo.Controls.Add(this.label32);
            this.pnlHoSoKemTheo.Controls.Add(this.txt6);
            this.pnlHoSoKemTheo.Controls.Add(this.label31);
            this.pnlHoSoKemTheo.Controls.Add(this.txt5);
            this.pnlHoSoKemTheo.Controls.Add(this.label30);
            this.pnlHoSoKemTheo.Controls.Add(this.txt4);
            this.pnlHoSoKemTheo.Controls.Add(this.label29);
            this.pnlHoSoKemTheo.Controls.Add(this.txt3);
            this.pnlHoSoKemTheo.Controls.Add(this.label28);
            this.pnlHoSoKemTheo.Controls.Add(this.txt2);
            this.pnlHoSoKemTheo.Controls.Add(this.label27);
            this.pnlHoSoKemTheo.Controls.Add(this.txt1);
            this.pnlHoSoKemTheo.Enabled = false;
            this.pnlHoSoKemTheo.Location = new System.Drawing.Point(146, 554);
            this.pnlHoSoKemTheo.Name = "pnlHoSoKemTheo";
            this.pnlHoSoKemTheo.Size = new System.Drawing.Size(443, 27);
            this.pnlHoSoKemTheo.TabIndex = 10;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(376, 7);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(23, 13);
            this.label40.TabIndex = 1;
            this.label40.Text = "10.";
            // 
            // txt10
            // 
            this.txt10.Location = new System.Drawing.Point(399, 3);
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(19, 21);
            this.txt10.TabIndex = 11;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(334, 7);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(17, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "9.";
            // 
            // txt9
            // 
            this.txt9.Location = new System.Drawing.Point(351, 3);
            this.txt9.Name = "txt9";
            this.txt9.Size = new System.Drawing.Size(19, 21);
            this.txt9.TabIndex = 11;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(292, 7);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(17, 13);
            this.label34.TabIndex = 1;
            this.label34.Text = "8.";
            // 
            // txt8
            // 
            this.txt8.Location = new System.Drawing.Point(309, 3);
            this.txt8.Name = "txt8";
            this.txt8.Size = new System.Drawing.Size(19, 21);
            this.txt8.TabIndex = 11;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(250, 7);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(17, 13);
            this.label33.TabIndex = 1;
            this.label33.Text = "7.";
            // 
            // txt7
            // 
            this.txt7.Location = new System.Drawing.Point(267, 3);
            this.txt7.Name = "txt7";
            this.txt7.Size = new System.Drawing.Size(19, 21);
            this.txt7.TabIndex = 11;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(208, 7);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(17, 13);
            this.label32.TabIndex = 1;
            this.label32.Text = "6.";
            // 
            // txt6
            // 
            this.txt6.Location = new System.Drawing.Point(225, 3);
            this.txt6.Name = "txt6";
            this.txt6.Size = new System.Drawing.Size(19, 21);
            this.txt6.TabIndex = 11;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(166, 7);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(17, 13);
            this.label31.TabIndex = 1;
            this.label31.Text = "5.";
            // 
            // txt5
            // 
            this.txt5.Location = new System.Drawing.Point(183, 3);
            this.txt5.Name = "txt5";
            this.txt5.Size = new System.Drawing.Size(19, 21);
            this.txt5.TabIndex = 11;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(126, 7);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(17, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "4.";
            // 
            // txt4
            // 
            this.txt4.Location = new System.Drawing.Point(143, 3);
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(19, 21);
            this.txt4.TabIndex = 11;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(84, 7);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(17, 13);
            this.label29.TabIndex = 1;
            this.label29.Text = "3.";
            // 
            // txt3
            // 
            this.txt3.Location = new System.Drawing.Point(101, 3);
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(19, 21);
            this.txt3.TabIndex = 11;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(45, 7);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(17, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "2.";
            // 
            // txt2
            // 
            this.txt2.Location = new System.Drawing.Point(62, 3);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(19, 21);
            this.txt2.TabIndex = 11;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(3, 7);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(17, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "1.";
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(20, 3);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(19, 21);
            this.txt1.TabIndex = 11;
            // 
            // lblHoSoKemTheo
            // 
            this.lblHoSoKemTheo.AutoSize = true;
            this.lblHoSoKemTheo.Location = new System.Drawing.Point(3, 561);
            this.lblHoSoKemTheo.Name = "lblHoSoKemTheo";
            this.lblHoSoKemTheo.Size = new System.Drawing.Size(137, 13);
            this.lblHoSoKemTheo.TabIndex = 22;
            this.lblHoSoKemTheo.Text = "Danh sách chứng từ đi kèm";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Controls.Add(this.ucMaQuocGiaXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtTenThuongNhanXuatKhau);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.txtMaBuuChinhXuatKhau);
            this.uiGroupBox3.Controls.Add(this.label21);
            this.uiGroupBox3.Controls.Add(this.label22);
            this.uiGroupBox3.Controls.Add(this.label11);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtQuanHuyenXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtSoNhaTenDuongXuatKhau);
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.txtEmailXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtSoFaxXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtSoDienThoaiXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtTinhThanhPhoXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtPhuongXaXuatKhau);
            this.uiGroupBox3.Location = new System.Drawing.Point(2, 13);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(738, 154);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.Text = "Doanh nghiệp Xuất khẩu";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // ucMaQuocGiaXuatKhau
            // 
            this.ucMaQuocGiaXuatKhau.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaQuocGiaXuatKhau.Appearance.Options.UseBackColor = true;
            this.ucMaQuocGiaXuatKhau.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaQuocGiaXuatKhau.Code = "";
            this.ucMaQuocGiaXuatKhau.ColorControl = System.Drawing.Color.Empty;
            this.ucMaQuocGiaXuatKhau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaQuocGiaXuatKhau.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaQuocGiaXuatKhau.IsOnlyWarning = false;
            this.ucMaQuocGiaXuatKhau.IsValidate = true;
            this.ucMaQuocGiaXuatKhau.Location = new System.Drawing.Point(115, 126);
            this.ucMaQuocGiaXuatKhau.Name = "ucMaQuocGiaXuatKhau";
            this.ucMaQuocGiaXuatKhau.Name_VN = "";
            this.ucMaQuocGiaXuatKhau.SetOnlyWarning = false;
            this.ucMaQuocGiaXuatKhau.SetValidate = false;
            this.ucMaQuocGiaXuatKhau.ShowColumnCode = true;
            this.ucMaQuocGiaXuatKhau.ShowColumnName = false;
            this.ucMaQuocGiaXuatKhau.Size = new System.Drawing.Size(58, 26);
            this.ucMaQuocGiaXuatKhau.TabIndex = 6;
            this.ucMaQuocGiaXuatKhau.TagName = "";
            this.ucMaQuocGiaXuatKhau.Where = null;
            this.ucMaQuocGiaXuatKhau.WhereCondition = "";
            this.ucMaQuocGiaXuatKhau.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtTenThuongNhanXuatKhau
            // 
            this.txtTenThuongNhanXuatKhau.Location = new System.Drawing.Point(115, 20);
            this.txtTenThuongNhanXuatKhau.Name = "txtTenThuongNhanXuatKhau";
            this.txtTenThuongNhanXuatKhau.Size = new System.Drawing.Size(614, 21);
            this.txtTenThuongNhanXuatKhau.TabIndex = 0;
            this.txtTenThuongNhanXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên";
            // 
            // txtMaBuuChinhXuatKhau
            // 
            this.txtMaBuuChinhXuatKhau.Location = new System.Drawing.Point(115, 47);
            this.txtMaBuuChinhXuatKhau.Name = "txtMaBuuChinhXuatKhau";
            this.txtMaBuuChinhXuatKhau.Size = new System.Drawing.Size(162, 21);
            this.txtMaBuuChinhXuatKhau.TabIndex = 1;
            this.txtMaBuuChinhXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 105);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(70, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "Quận, huyện";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(414, 105);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(83, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Tỉnh, thành phố";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(355, 131);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Số Fax";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(417, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Phường, xã";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Số nhà, tên đường";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(546, 132);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Mã bưu chính";
            // 
            // txtQuanHuyenXuatKhau
            // 
            this.txtQuanHuyenXuatKhau.Location = new System.Drawing.Point(115, 101);
            this.txtQuanHuyenXuatKhau.Name = "txtQuanHuyenXuatKhau";
            this.txtQuanHuyenXuatKhau.Size = new System.Drawing.Size(296, 21);
            this.txtQuanHuyenXuatKhau.TabIndex = 4;
            // 
            // txtSoNhaTenDuongXuatKhau
            // 
            this.txtSoNhaTenDuongXuatKhau.Location = new System.Drawing.Point(115, 74);
            this.txtSoNhaTenDuongXuatKhau.Name = "txtSoNhaTenDuongXuatKhau";
            this.txtSoNhaTenDuongXuatKhau.Size = new System.Drawing.Size(296, 21);
            this.txtSoNhaTenDuongXuatKhau.TabIndex = 2;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(179, 131);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Điện thoại";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 131);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Mã quốc gia";
            // 
            // txtEmailXuatKhau
            // 
            this.txtEmailXuatKhau.Location = new System.Drawing.Point(583, 127);
            this.txtEmailXuatKhau.Name = "txtEmailXuatKhau";
            this.txtEmailXuatKhau.Size = new System.Drawing.Size(146, 21);
            this.txtEmailXuatKhau.TabIndex = 9;
            this.txtEmailXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtSoFaxXuatKhau
            // 
            this.txtSoFaxXuatKhau.Location = new System.Drawing.Point(404, 127);
            this.txtSoFaxXuatKhau.Name = "txtSoFaxXuatKhau";
            this.txtSoFaxXuatKhau.Size = new System.Drawing.Size(137, 21);
            this.txtSoFaxXuatKhau.TabIndex = 8;
            this.txtSoFaxXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtSoDienThoaiXuatKhau
            // 
            this.txtSoDienThoaiXuatKhau.Location = new System.Drawing.Point(236, 127);
            this.txtSoDienThoaiXuatKhau.Name = "txtSoDienThoaiXuatKhau";
            this.txtSoDienThoaiXuatKhau.Size = new System.Drawing.Size(113, 21);
            this.txtSoDienThoaiXuatKhau.TabIndex = 7;
            this.txtSoDienThoaiXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtTinhThanhPhoXuatKhau
            // 
            this.txtTinhThanhPhoXuatKhau.Location = new System.Drawing.Point(499, 101);
            this.txtTinhThanhPhoXuatKhau.Name = "txtTinhThanhPhoXuatKhau";
            this.txtTinhThanhPhoXuatKhau.Size = new System.Drawing.Size(230, 21);
            this.txtTinhThanhPhoXuatKhau.TabIndex = 5;
            // 
            // txtPhuongXaXuatKhau
            // 
            this.txtPhuongXaXuatKhau.Location = new System.Drawing.Point(499, 74);
            this.txtPhuongXaXuatKhau.Name = "txtPhuongXaXuatKhau";
            this.txtPhuongXaXuatKhau.Size = new System.Drawing.Size(230, 21);
            this.txtPhuongXaXuatKhau.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 428);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Trị giá hàng hóa";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 399);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Bến đến";
            // 
            // dtThoiGianKiemTra
            // 
            // 
            // 
            // 
            this.dtThoiGianKiemTra.DropDownCalendar.Name = "";
            this.dtThoiGianKiemTra.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtThoiGianKiemTra.IsNullDate = true;
            this.dtThoiGianKiemTra.Location = new System.Drawing.Point(146, 477);
            this.dtThoiGianKiemTra.Name = "dtThoiGianKiemTra";
            this.dtThoiGianKiemTra.Size = new System.Drawing.Size(90, 21);
            this.dtThoiGianKiemTra.TabIndex = 12;
            this.dtThoiGianKiemTra.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(8, 207);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(36, 13);
            this.label38.TabIndex = 5;
            this.label38.Text = "Bến đi";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(3, 485);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(91, 13);
            this.label25.TabIndex = 16;
            this.label25.Text = "Thời gian kiểm tra";
            // 
            // dtThoiGianNhapKhauDuKien
            // 
            // 
            // 
            // 
            this.dtThoiGianNhapKhauDuKien.DropDownCalendar.Name = "";
            this.dtThoiGianNhapKhauDuKien.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtThoiGianNhapKhauDuKien.IsNullDate = true;
            this.dtThoiGianNhapKhauDuKien.Location = new System.Drawing.Point(585, 391);
            this.dtThoiGianNhapKhauDuKien.Name = "dtThoiGianNhapKhauDuKien";
            this.dtThoiGianNhapKhauDuKien.Size = new System.Drawing.Size(90, 21);
            this.dtThoiGianNhapKhauDuKien.TabIndex = 9;
            this.dtThoiGianNhapKhauDuKien.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(443, 398);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(141, 13);
            this.label24.TabIndex = 10;
            this.label24.Text = "Thời gian nhập khẩu dự kiến";
            // 
            // txtDaiDienCoQuanKiemTra
            // 
            this.txtDaiDienCoQuanKiemTra.Enabled = false;
            this.txtDaiDienCoQuanKiemTra.Location = new System.Drawing.Point(146, 804);
            this.txtDaiDienCoQuanKiemTra.Name = "txtDaiDienCoQuanKiemTra";
            this.txtDaiDienCoQuanKiemTra.Size = new System.Drawing.Size(585, 21);
            this.txtDaiDienCoQuanKiemTra.TabIndex = 19;
            // 
            // txtDiaDiemKiemTra
            // 
            this.txtDiaDiemKiemTra.Location = new System.Drawing.Point(339, 477);
            this.txtDiaDiemKiemTra.Name = "txtDiaDiemKiemTra";
            this.txtDiaDiemKiemTra.Size = new System.Drawing.Size(392, 21);
            this.txtDiaDiemKiemTra.TabIndex = 13;
            // 
            // txtDiaDiemTapKetHangHoa
            // 
            this.txtDiaDiemTapKetHangHoa.Location = new System.Drawing.Point(146, 450);
            this.txtDiaDiemTapKetHangHoa.Name = "txtDiaDiemTapKetHangHoa";
            this.txtDiaDiemTapKetHangHoa.Size = new System.Drawing.Size(585, 21);
            this.txtDiaDiemTapKetHangHoa.TabIndex = 11;
            // 
            // txtDaiDienThuongNhanNhapKhau
            // 
            this.txtDaiDienThuongNhanNhapKhau.Enabled = false;
            this.txtDaiDienThuongNhanNhapKhau.Location = new System.Drawing.Point(146, 680);
            this.txtDaiDienThuongNhanNhapKhau.Name = "txtDaiDienThuongNhanNhapKhau";
            this.txtDaiDienThuongNhanNhapKhau.Size = new System.Drawing.Size(585, 21);
            this.txtDaiDienThuongNhanNhapKhau.TabIndex = 17;
            this.txtDaiDienThuongNhanNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtHoSoLienQuan
            // 
            this.txtHoSoLienQuan.Location = new System.Drawing.Point(146, 504);
            this.txtHoSoLienQuan.Multiline = true;
            this.txtHoSoLienQuan.Name = "txtHoSoLienQuan";
            this.txtHoSoLienQuan.Size = new System.Drawing.Size(585, 44);
            this.txtHoSoLienQuan.TabIndex = 14;
            this.txtHoSoLienQuan.VisualStyleManager = this.vsmMain;
            // 
            // txtGhiChuCoQuanCapGiayPhep
            // 
            this.txtGhiChuCoQuanCapGiayPhep.Enabled = false;
            this.txtGhiChuCoQuanCapGiayPhep.Location = new System.Drawing.Point(146, 738);
            this.txtGhiChuCoQuanCapGiayPhep.Multiline = true;
            this.txtGhiChuCoQuanCapGiayPhep.Name = "txtGhiChuCoQuanCapGiayPhep";
            this.txtGhiChuCoQuanCapGiayPhep.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGhiChuCoQuanCapGiayPhep.Size = new System.Drawing.Size(585, 60);
            this.txtGhiChuCoQuanCapGiayPhep.TabIndex = 18;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.Location = new System.Drawing.Point(146, 587);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtGhiChu.Size = new System.Drawing.Size(585, 60);
            this.txtGhiChu.TabIndex = 15;
            this.txtGhiChu.VisualStyleManager = this.vsmMain;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 178);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Số hợp đồng";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(434, 178);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(61, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "Số vận đơn";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(0, 808);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(128, 13);
            this.label47.TabIndex = 32;
            this.label47.Text = "Đại diện cơ quan kiểm tra";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(244, 481);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(89, 13);
            this.label48.TabIndex = 18;
            this.label48.Text = "Địa điểm kiểm tra";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 454);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(133, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Địa điểm tập kết hàng hóa";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(3, 508);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(80, 13);
            this.label39.TabIndex = 20;
            this.label39.Text = "Hồ sơ liên quan";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(3, 685);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(114, 26);
            this.label37.TabIndex = 28;
            this.label37.Text = "Đại diện thương nhân \r\nnhập khẩu";
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(117, 173);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(296, 21);
            this.txtSoHopDong.TabIndex = 0;
            this.txtSoHopDong.VisualStyleManager = this.vsmMain;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(3, 722);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(188, 13);
            this.label46.TabIndex = 30;
            this.label46.Text = "Ghi chú (Dành cho Cơ quan cấp phép)";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(3, 587);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(42, 13);
            this.label36.TabIndex = 24;
            this.label36.Text = "Ghi chú";
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.Location = new System.Drawing.Point(501, 173);
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(230, 21);
            this.txtSoVanDon.TabIndex = 1;
            this.txtSoVanDon.VisualStyleManager = this.vsmMain;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(728, 826);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(16, 13);
            this.label58.TabIndex = 34;
            this.label58.Text = "   ";
            // 
            // VNACC_GiayPhepForm_SFA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(959, 881);
            this.Controls.Add(this.cmdToolBar);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_GiayPhepForm_SFA";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin giấy phép (SFA: Application for Certificate on satisfaction of food qua" +
                "lity for importation)";
            this.Load += new System.EventHandler(this.VNACC_GiayPhepForm_Load);
            this.Controls.SetChildIndex(this.cmdToolBar, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToolBar)).EndInit();
            this.cmdToolBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            this.pnlHoSoKemTheo.ResumeLayout(false);
            this.pnlHoSoKemTheo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar cmdToolBar;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdlayPhanHoi;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoFaxXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtEmailXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenThuongNhanXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiKhai;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDonXinCapPhep;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.CalendarCombo.CalendarCombo dtThoiGianKiemTra;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.CalendarCombo.CalendarCombo dtThoiGianNhapKhauDuKien;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtDaiDienThuongNhanNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private Janus.Windows.GridEX.EditControls.EditBox txtHoSoLienQuan;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucChucNangChungTu;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaQuocGiaXuatKhau;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiKhai;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayKhaiBao;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonViCapPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayPhepKQXL;
        private System.Windows.Forms.Label label42;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaKQXL;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private Janus.Windows.CalendarCombo.CalendarCombo dtHieuLucDenNgayKQXL;
        private Janus.Windows.CalendarCombo.CalendarCombo dtHieuLucTuNgayKQXL;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayCapKQXL;
        private Janus.Windows.GridEX.EditControls.EditBox txtDaiDienCoQuanKiemTra;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChuCoQuanCapGiayPhep;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label46;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHaiQuan;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHaiQuan1;
        private System.Windows.Forms.Panel pnlHoSoKemTheo;
        private System.Windows.Forms.Label label40;
        private Janus.Windows.GridEX.EditControls.EditBox txt10;
        private System.Windows.Forms.Label label35;
        private Janus.Windows.GridEX.EditControls.EditBox txt9;
        private System.Windows.Forms.Label label34;
        private Janus.Windows.GridEX.EditControls.EditBox txt8;
        private System.Windows.Forms.Label label33;
        private Janus.Windows.GridEX.EditControls.EditBox txt7;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.GridEX.EditControls.EditBox txt6;
        private System.Windows.Forms.Label label31;
        private Janus.Windows.GridEX.EditControls.EditBox txt5;
        private System.Windows.Forms.Label label30;
        private Janus.Windows.GridEX.EditControls.EditBox txt4;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.GridEX.EditControls.EditBox txt3;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.GridEX.EditControls.EditBox txt2;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txt1;
        private System.Windows.Forms.Label lblHoSoKemTheo;
        private System.Windows.Forms.Label label58;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaQuocGiaNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaThuongNhanNhapKhau;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenThuongNhanNhapKhau;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhNhapKhau;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoNhaTenDuongNhapKhau;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private Janus.Windows.GridEX.EditControls.EditBox txtEmailNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoFaxNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiNhapKhau;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtGiaTriHangHoa;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemTapKetHangHoa;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoNhaTenDuongXuatKhau;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemKiemTra;
        private System.Windows.Forms.Label label48;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuanHuyenXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtTinhThanhPhoXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhuongXaXuatKhau;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucPhanLoaiTraCuu;
        private System.Windows.Forms.Label label23;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaDonViCapPhep;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucBenDi;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ucBenDen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucLoaiGiayPhep;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaHQ1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaHQ;
    }
}