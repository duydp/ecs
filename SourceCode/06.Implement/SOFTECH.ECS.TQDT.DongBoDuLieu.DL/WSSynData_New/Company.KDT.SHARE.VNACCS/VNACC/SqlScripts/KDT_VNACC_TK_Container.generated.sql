-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_Container_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Insert]
	@TKMD_ID bigint,
	@MaDiaDiem1 varchar(7),
	@MaDiaDiem2 varchar(7),
	@MaDiaDiem3 varchar(7),
	@MaDiaDiem4 varchar(7),
	@MaDiaDiem5 varchar(7),
	@TenDiaDiem nvarchar(70),
	@DiaChiDiaDiem nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_Container]
(
	[TKMD_ID],
	[MaDiaDiem1],
	[MaDiaDiem2],
	[MaDiaDiem3],
	[MaDiaDiem4],
	[MaDiaDiem5],
	[TenDiaDiem],
	[DiaChiDiaDiem],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@MaDiaDiem1,
	@MaDiaDiem2,
	@MaDiaDiem3,
	@MaDiaDiem4,
	@MaDiaDiem5,
	@TenDiaDiem,
	@DiaChiDiaDiem,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaDiaDiem1 varchar(7),
	@MaDiaDiem2 varchar(7),
	@MaDiaDiem3 varchar(7),
	@MaDiaDiem4 varchar(7),
	@MaDiaDiem5 varchar(7),
	@TenDiaDiem nvarchar(70),
	@DiaChiDiaDiem nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_Container]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaDiaDiem1] = @MaDiaDiem1,
	[MaDiaDiem2] = @MaDiaDiem2,
	[MaDiaDiem3] = @MaDiaDiem3,
	[MaDiaDiem4] = @MaDiaDiem4,
	[MaDiaDiem5] = @MaDiaDiem5,
	[TenDiaDiem] = @TenDiaDiem,
	[DiaChiDiaDiem] = @DiaChiDiaDiem,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaDiaDiem1 varchar(7),
	@MaDiaDiem2 varchar(7),
	@MaDiaDiem3 varchar(7),
	@MaDiaDiem4 varchar(7),
	@MaDiaDiem5 varchar(7),
	@TenDiaDiem nvarchar(70),
	@DiaChiDiaDiem nvarchar(100),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_Container] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_Container] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaDiaDiem1] = @MaDiaDiem1,
			[MaDiaDiem2] = @MaDiaDiem2,
			[MaDiaDiem3] = @MaDiaDiem3,
			[MaDiaDiem4] = @MaDiaDiem4,
			[MaDiaDiem5] = @MaDiaDiem5,
			[TenDiaDiem] = @TenDiaDiem,
			[DiaChiDiaDiem] = @DiaChiDiaDiem,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_Container]
		(
			[TKMD_ID],
			[MaDiaDiem1],
			[MaDiaDiem2],
			[MaDiaDiem3],
			[MaDiaDiem4],
			[MaDiaDiem5],
			[TenDiaDiem],
			[DiaChiDiaDiem],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaDiaDiem1,
			@MaDiaDiem2,
			@MaDiaDiem3,
			@MaDiaDiem4,
			@MaDiaDiem5,
			@TenDiaDiem,
			@DiaChiDiaDiem,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_Container]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_Container] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaDiaDiem1],
	[MaDiaDiem2],
	[MaDiaDiem3],
	[MaDiaDiem4],
	[MaDiaDiem5],
	[TenDiaDiem],
	[DiaChiDiaDiem],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_Container]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[MaDiaDiem1],
	[MaDiaDiem2],
	[MaDiaDiem3],
	[MaDiaDiem4],
	[MaDiaDiem5],
	[TenDiaDiem],
	[DiaChiDiaDiem],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_TK_Container] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_Container_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_Container_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaDiaDiem1],
	[MaDiaDiem2],
	[MaDiaDiem3],
	[MaDiaDiem4],
	[MaDiaDiem5],
	[TenDiaDiem],
	[DiaChiDiaDiem],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_TK_Container]	

GO

