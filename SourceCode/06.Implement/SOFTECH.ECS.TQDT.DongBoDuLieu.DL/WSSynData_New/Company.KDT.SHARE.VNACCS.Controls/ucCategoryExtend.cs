﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;

namespace Company.KDT.SHARE.VNACCS.Controls
{
    public partial class ucCategoryExtend : ucBase
    {
        public string Where { get; set; }
        private bool _showColumnCode = true;
        public bool ShowColumnCode
        {
            get { return _showColumnCode; }
            set { _showColumnCode = value; }
        }

        private bool _showColumnName = true;
        public bool ShowColumnName
        {
            get { return _showColumnName; }
            set { _showColumnName = value; }
        }

        private ECategory _categoryType = ECategory.E001;
        /// <summary>
        /// Xác định loại danh mục.
        /// </summary>
        public ECategory CategoryType
        {
            get { return _categoryType; }
            set { _categoryType = value; }
        }

        private string _referenceDB;
        /// <summary>
        /// Xác định mã loại danh mục
        /// </summary>
        public string ReferenceDB
        {
            get
            {
                _referenceDB = CategoryType.ToString();

                return _referenceDB;
            }
            //set { _referenceDB = value; }
        }

        /// <summary>
        /// Mã
        /// </summary>
        public string Code
        {
            get
            {
                if (SetValidate)
                {
                    if (!ValidateControl(false))
                        return txtCode.Text.Trim() != "" ? txtCode.Text.Trim() : "";
                    else
                    {
                        return cboCategory.EditValue.ToString().Trim();
                    }
                }
                else
                    return cboCategory.EditValue != null ? cboCategory.EditValue.ToString().Trim() : txtCode.Text.Trim();
            }
            set { cboCategory.EditValue = value; }
        }

        /// <summary>
        /// Tên Tiếng việt
        /// </summary>
        public string Name_VN
        {
            get
            {
                if (SetValidate)
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Text.Trim();
                    }
                }
                else
                    return cboCategory.Text.Trim();
            }
            set { cboCategory.Text = value; }
        }

        public string TagCode
        {
            get { return txtCode.Tag != null ? txtCode.Tag.ToString() : ""; }
            set { txtCode.Tag = value; }
        }

        public string TagName
        {
            get { return cboCategory.Tag != null ? cboCategory.Tag.ToString() : ""; }
            set { cboCategory.Tag = value; }
        }

        private EImportExport _importType = EImportExport.I;
        /// <summary>
        /// Xác định loại Nhập/ Xuất
        /// </summary>
        public EImportExport ImportType
        {
            get { return _importType; }
            set { _importType = value; }
        }

        /// <summary>
        /// NumberOfKeyItems: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm cặp khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class ContainerSize
        /// </summary>
        public string NumberOfKeyItems
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Properties.GetDataSourceRowByKeyValue("NumberOfKeyItems").ToString().Trim();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }

        /// <summary>
        /// CountryCode: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm cặp khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class CityUNLOCODE
        /// </summary>
        public string CountryCode
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Properties.GetDataSourceRowByKeyValue("CountryCode").ToString().Trim();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }

        private bool _setValidate = false;
        public bool SetValidate
        {
            get { return _setValidate; }
            set { _setValidate = value; }
        }

        private bool _isValidate = false;
        public bool IsValidate
        {
            get
            {
                return SetValidate ? (_isValidate = ValidateControl(false)) : true;
            }
            set { _isValidate = value; }
        }

        private bool _isOnlyWarning = false;
        public bool IsOnlyWarning
        {
            get
            {
                return _isOnlyWarning = ValidateControl(true);
            }
            set
            {
                if (value == true)
                    _isOnlyWarning = ValidateControl(true);
            }
        }

        public bool IsUpperCase
        {
            set
            {
                if (value == true)
                {
                    txtCode.Properties.CharacterCasing = CharacterCasing.Upper;
                    cboCategory.Properties.CharacterCasing = CharacterCasing.Upper;
                }
                else
                {
                    txtCode.Properties.CharacterCasing = CharacterCasing.Normal;
                    cboCategory.Properties.CharacterCasing = CharacterCasing.Normal;
                }
            }
        }

        /// <summary>
        /// CustomsSubSectionCode: Trường hợp bảng có Code trùng nhau, lấy thêm thông tin này để làm khóa tránh trùng thông tin.
        /// Dùng bổ sung cho class Nhóm xử lý hồ sơ (Customs-Sub Section)
        /// </summary>
        public string CustomsSubSectionCode
        {
            get
            {
                try
                {
                    if (!ValidateControl(false))
                        return "";
                    else
                    {
                        return cboCategory.Properties.GetDataSourceRowByKeyValue("CustomsSubSectionCode").ToString().Trim();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return "";
                }
            }
        }

        public ucCategoryExtend()
        {
            InitializeComponent();

            cboCategory.Properties.NullText = "";

            txtCode.Enter += new EventHandler(txtCode_Enter);
            cboCategory.Enter += new EventHandler(cboCategory_Enter);
        }

        private void ucCategoryExtend_Load(object sender, EventArgs e)
        {
            LoadData(false);
        }

        private void LoadData(bool isReload)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string whereCondition = "";
                string orderBy = "";
                string valueMember = "Code";
                string displayMember = "Name";
                string valueMemberCaption = "Mã";
                string displayMemberCaption = "Tên";
                whereCondition += base.WhereCondition != "" ? base.WhereCondition + " And " : "";

                switch (CategoryType)
                {
                    //case ECategory.A001:
                    //    break;

                    case ECategory.A014: //Nhóm xử lý hồ sơ
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%' and ImportExportClassification = '{1}'", ReferenceDB.ToString(), ImportType.ToString());
                        orderBy = "CustomsSubSectionCode asc";
                        valueMember = "ID"; //"CustomsSubSectionCode";
                        displayMember = "Name";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCustomsSubSections = VNACC_Category_CustomsSubSection.SelectCollectionBy(ucBase.dataCustomsSubSectionGlobals, ReferenceDB.ToString(), ImportType.ToString());
                        VNACC_Category_CustomsSubSection ItemA014 = new VNACC_Category_CustomsSubSection();
                        dataCustomsSubSections.Add(ItemA014);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataCustomsSubSections;
                        #endregion
                        break;

                    case ECategory.A015: // Mã nước(Country, coded), Mã nước xuất xứ
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "NationCode asc";
                        valueMember = "NationCode";
                        displayMember = "CountryShortName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataNations = VNACC_Category_Nation.SelectCollectionBy(ucBase.dataNationGlobals, ReferenceDB.ToString());
                        VNACC_Category_Nation ItemA015 = new VNACC_Category_Nation();
                        dataNations.Add(ItemA015);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataNations;
                        #endregion
                        break;
                    case ECategory.A016: // Mã địa điểm xếp hàng, Mã địa điểm dỡ hàng
                        #region Load data
                        whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        //orderBy = "CityCode asc";
                        valueMember = "LOCODE";
                        displayMember = "CityNameOrStateName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";
                        //Create obj and load data
                        dataCitys = VNACC_Category_CityUNLOCODE.SelectCollectionAllMinimize2(Where);
                        dataCitys.Add(new VNACC_Category_CityUNLOCODE { LOCODE = "", CityNameOrStateName = "" });
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataCitys;
                        #endregion
                        break;

                    //case ECategory.A017:
                    //    break;
                    //case ECategory.A021:
                    //    break;

                    case ECategory.A038: //Cơ quan Hải quan
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "CustomsCode asc";
                        valueMember = "CustomsCode";
                        displayMember = "CustomsOfficeNameInVietnamese";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCustomsOffices = VNACC_Category_CustomsOffice.SelectCollectionBy(ucBase.dataCustomsOfficeGlobals, ReferenceDB.ToString());
                        VNACC_Category_CustomsOffice ItemA038 = new VNACC_Category_CustomsOffice();
                        dataCustomsOffices.Add(ItemA038);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataCustomsOffices;
                        #endregion
                        break;

                    case ECategory.A202: // Mã địa điểm lưu kho hàng chờ thông quan dự kiến, Địa điểm trung chuyển cho vận chuyển bảo thuế (khai báo gộp), Địa điểm đích cho vận chuyển bảo thuế (khai báo gộp)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "BondedAreaCode asc";
                        valueMember = "BondedAreaCode";
                        displayMember = "BondedAreaName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCargos = VNACC_Category_Cargo.SelectCollectionBy(ucBase.dataCargoGlobals, ReferenceDB.ToString());
                        VNACC_Category_Cargo ItemA202 = new VNACC_Category_Cargo();
                        dataCargos.Add(ItemA202);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataCargos;
                        #endregion
                        break;

                    case ECategory.A301: //Container size
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "ContainerSizeCode asc";
                        valueMember = "ContainerSizeCode";
                        displayMember = "ContainerSizeName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataContainerSizes = VNACC_Category_ContainerSize.SelectCollectionBy(ucBase.dataContainerSizeGlobals, ReferenceDB.ToString());
                        VNACC_Category_ContainerSize ItemA301 = new VNACC_Category_ContainerSize();
                        dataContainerSizes.Add(ItemA301);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataContainerSizes;
                        #endregion
                        break;

                    //case ECategory.B314:
                    //    break;

                    case ECategory.A316: //Mã đơn vị tính (Packages unit code)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "NumberOfPackagesUnitCode asc";
                        valueMember = "NumberOfPackagesUnitCode";
                        displayMember = "NumberOfPackagesUnitName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataPackagesUnits = VNACC_Category_PackagesUnit.SelectCollectionBy(ucBase.dataPackagesUnitGlobals, ReferenceDB.ToString());
                        VNACC_Category_PackagesUnit ItemA316 = new VNACC_Category_PackagesUnit();
                        dataPackagesUnits.Add(ItemA316);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataPackagesUnits;
                        #endregion
                        break;

                    case ECategory.A404: //Mã biểu thuế nhập khẩu
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "TaxCode asc";
                        valueMember = "TaxCode";
                        displayMember = "TaxName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataTaxClassificationCodes = VNACC_Category_TaxClassificationCode.SelectCollectionBy(ucBase.dataTaxClassificationCodeGlobals, ReferenceDB.ToString());
                        VNACC_Category_TaxClassificationCode ItemA404 = new VNACC_Category_TaxClassificationCode();
                        dataTaxClassificationCodes.Add(ItemA404);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataTaxClassificationCodes;
                        #endregion
                        break;

                    case ECategory.A501: // Mã đơn vị tính thuế tuyệt đối, Mã đơn vị tính, Đơn vị của đơn giá hóa đơn và số lượng
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "Code asc";
                        valueMember = "Code";
                        displayMember = "Name";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataQuantityUnits = VNACC_Category_QuantityUnit.SelectCollectionBy(ucBase.dataQuantityUnitGlobals, ReferenceDB.ToString());
                        VNACC_Category_QuantityUnit ItemA501 = new VNACC_Category_QuantityUnit();
                        dataQuantityUnits.Add(ItemA501);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataQuantityUnits;
                        #endregion
                        break;

                    case ECategory.A506: //Mã số hàng hóa (HS)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "HSCode asc";
                        valueMember = "HSCode";
                        displayMember = "";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataHSCodes = VNACC_Category_HSCode.SelectCollectionBy(ucBase.dataHSCodeGlobals, ReferenceDB.ToString());
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataHSCodes;
                        #endregion
                        break;

                    //case ECategory.A515:
                    //    break;
                    //case ECategory.A517:
                    //    break;

                    case ECategory.A527: //Mã đồng tiền
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "CurrencyCode asc";
                        valueMember = "CurrencyCode";
                        displayMember = "CurrencyName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataCurrencyExchanges = VNACC_Category_CurrencyExchange.SelectCollectionBy(ucBase.dataCurrencyExchangeGlobals, ReferenceDB.ToString());
                        VNACC_Category_CurrencyExchange ItemA527 = new VNACC_Category_CurrencyExchange();
                        dataCurrencyExchanges.Add(ItemA527);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataCurrencyExchanges;
                        #endregion
                        break;

                    //case ECategory.A528:
                    //    break;

                    case ECategory.A546: //Loại chứng từ đính kèm
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "ApplicationProcedureType asc";
                        valueMember = "ApplicationProcedureType";
                        displayMember = "ApplicationProcedureName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataApplicationProcedureTypes = VNACC_Category_ApplicationProcedureType.SelectCollectionBy(ucBase.dataApplicationProcedureTypeGlobals, ReferenceDB.ToString());
                        VNACC_Category_ApplicationProcedureType ItemA546 = new VNACC_Category_ApplicationProcedureType();
                        dataApplicationProcedureTypes.Add(ItemA546);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataApplicationProcedureTypes;
                        #endregion
                        break;

                    //case ECategory.B501:
                    //    break;
                    //case ECategory.B530:
                    //    break;
                    //case ECategory.B700:
                    //    break;
                    //case ECategory.B701:
                    //    break;

                    case ECategory.A601: //Mã địa điểm dỡ hàng (Stations)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "StationCode asc";
                        valueMember = "StationCode";
                        displayMember = "StationName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataStations = VNACC_Category_Station.SelectCollectionBy(ucBase.dataStationGlobals, ReferenceDB.ToString());
                        VNACC_Category_Station ItemA601 = new VNACC_Category_Station();
                        dataStations.Add(ItemA601);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataStations;
                        #endregion
                        break;

                    case ECategory.A620: //Mã địa điểm dỡ hàng (Border gate)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "BorderGateCode asc";
                        valueMember = "BorderGateCode";
                        displayMember = "BorderGateName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataBorderGates = VNACC_Category_BorderGate.SelectCollectionBy(ucBase.dataBorderGateGlobals, ReferenceDB.ToString());
                        VNACC_Category_BorderGate ItemA620 = new VNACC_Category_BorderGate();
                        dataBorderGates.Add(ItemA620);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataBorderGates;
                        #endregion
                        break;

                    case ECategory.A621: //Transport means (Vehicle)
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "TransportMeansCode asc";
                        valueMember = "TransportMeansCode";
                        displayMember = "TransportMeansName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataTransportMeans = VNACC_Category_TransportMean.SelectCollectionBy(ucBase.dataTransportMeanGlobals, ReferenceDB.ToString());
                        VNACC_Category_TransportMean ItemA621 = new VNACC_Category_TransportMean();
                        dataTransportMeans.Add(ItemA621);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataTransportMeans;
                        #endregion
                        break;

                    case ECategory.A700: //OGA User
                        #region Load data
                        //whereCondition += string.Format("TableID like '{0}%'", ReferenceDB.ToString());
                        orderBy = "OfficeOfApplicationCode asc";
                        valueMember = "OfficeOfApplicationCode";
                        displayMember = "OfficeOfApplicationName";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        dataOGAUsers = VNACC_Category_OGAUser.SelectCollectionBy(ucBase.dataOGAUserGlobals, ReferenceDB.ToString());
                        VNACC_Category_OGAUser ItemA700 = new VNACC_Category_OGAUser();
                        dataOGAUsers.Add(ItemA700);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = dataOGAUsers;
                        #endregion
                        break;

                    case ECategory.A519: //Mã văn bản pháp quy khác
                    case ECategory.A520: //Mã miễn / Giảm / Không chịu thuế nhập khẩu
                    case ECategory.A521: //Mã miễn / Giảm / Không chịu thuế và thu khác
                    case ECategory.A522: //Mã áp dụng thuế suất / Mức thuế và thu khác
                    case ECategory.A402: //Mã xác định mức thuế nhập khẩu theo lượng
                    case ECategory.A528: //Phân loại giấy phép nhập khẩu

                    case ECategory.A401: //Mã tên khoản điều chỉnh
                    case ECategory.A557: //Mã phân loại phí vận chuyển                    
                    case ECategory.B524: //Trạng thái khai báo VNACC
                    case ECategory.A553://Mã điều kiện giá hóa đơn
                    case ECategory.E001:
                    case ECategory.E002:
                    case ECategory.E003:
                    case ECategory.E004:
                    case ECategory.E005:
                    case ECategory.E006:
                    case ECategory.E007:
                    case ECategory.E008:
                    case ECategory.E009:
                    case ECategory.E010:
                    case ECategory.E011:
                    case ECategory.E012:
                    case ECategory.E013:
                    case ECategory.E014:
                    case ECategory.E015:
                    case ECategory.E016:
                    case ECategory.E017:
                    case ECategory.E018:
                    case ECategory.E019:
                    case ECategory.E020:
                    case ECategory.E021:
                    case ECategory.E022:
                    case ECategory.E023:
                    case ECategory.E024:
                    case ECategory.E025:
                    case ECategory.E026:
                    case ECategory.E027:
                    case ECategory.E028:
                    case ECategory.E029:
                    case ECategory.E030:
                    case ECategory.E031:
                    case ECategory.E032:
                    case ECategory.E033:
                        #region Load data
                        //whereCondition += string.Format("ReferenceDB = '{0}'", ReferenceDB.ToString());
                        orderBy = "Code asc";
                        valueMember = "Code";
                        displayMember = "Name_VN";
                        valueMemberCaption = "Mã";
                        displayMemberCaption = "Tên";

                        //Create obj and load data
                        datas = VNACC_Category_Common.SelectCollectionBy(ucBase.dataGlobals, ReferenceDB.ToString());
                        VNACC_Category_Common ItemEmpty = new VNACC_Category_Common();
                        datas.Add(ItemEmpty);
                        // Specify the data source to display in the dropdown.
                        cboCategory.Properties.DataSource = datas;
                        #endregion
                        break;
                }

                //SET COLUMN
                cboCategory.Properties.Columns.Clear();
                // The field providing the editor's display text.
                if (ShowColumnCode && ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = displayMember;
                    cboCategory.Properties.ValueMember = valueMember;
                }
                else if (ShowColumnCode && !ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = valueMember;
                    cboCategory.Properties.ValueMember = valueMember;
                }
                else if (!ShowColumnCode && ShowColumnName)
                {
                    cboCategory.Properties.DisplayMember = displayMember;
                    cboCategory.Properties.ValueMember = displayMember;
                }

                // Add two columns to the dropdown.
                LookUpColumnInfoCollection coll = cboCategory.Properties.Columns;
                // A column to display the Format field's values.
                coll.Add(new LookUpColumnInfo(valueMember, valueMemberCaption, 0));
                if (displayMember != "")
                    coll.Add(new LookUpColumnInfo(displayMember, displayMemberCaption, 0));

                if (CategoryType == ECategory.A014)
                    coll.Add(new LookUpColumnInfo("CustomsSubSectionCode", "Mã", 0));

                if (CategoryType == ECategory.A016)
                    coll.Add(new LookUpColumnInfo("CountryCode", "Mã nước", 0));

                //  Set column widths according to their contents and resize the popup, if required.
                cboCategory.Properties.BestFitMode = BestFitMode.BestFitResizePopup;
                // Enable auto completion search mode.
                //cboCategory.Properties.SearchMode = SearchMode.AutoComplete; //Luu y: khi thiet lap thuoc tinh nay, khi chon Name co Code null se tu dong lay dong dau tien trong danh sach.
                // Specify the column against which to perform the search.
                cboCategory.Properties.AutoSearchColumnIndex = 1;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { Cursor = Cursors.Default; }
        }

        /// <summary>
        /// Tải mới lại dữ liệu danh mục
        /// </summary>
        public void ReLoadData()
        {
            LoadData(true);
        }

        private bool ValidateControl(bool isOnlyWarning)
        {
            bool isValid = true;

            isValid &= ValidationControl.ValidateNullLookUpEditDevExpress(cboCategory, dxErrorProvider, "", isOnlyWarning);

            if (isValid)
            {
                isValid &= ValidationControl.ValidateChooseLookUpEditDevExpress(cboCategory, dxErrorProvider, "", isOnlyWarning);
            }

            return isValid;
        }

        public delegate void EditValueChangedHandle(object sender, EventArgs e);
        public event EditValueChangedHandle EditValueChanged;
        private void cboCategory_EditValueChanged(object sender, EventArgs e)
        {
            txtCode.Text = Code;

            if (EditValueChanged != null)
            {
                EditValueChanged(sender, e);
            }
        }

        private void txtCode_Leave(object sender, EventArgs e)
        {
            Code = txtCode.Text.Trim();
        }

        public delegate void EnterHandle(object sender, EventArgs e);
        public event EnterHandle OnEnter;
        void cboCategory_Enter(object sender, EventArgs e)
        {
            if (OnEnter != null)
            {
                OnEnter(sender, e);
            }
        }
        void txtCode_Enter(object sender, EventArgs e)
        {
            if (OnEnter != null)
            {
                OnEnter(sender, e);
            }
        }
    }
}
