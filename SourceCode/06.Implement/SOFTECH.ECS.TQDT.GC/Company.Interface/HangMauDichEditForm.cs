﻿using System;
using System.Drawing;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL.Utils;
using System.Data;
using Company.Interface.GC;

namespace Company.Interface
{
    public partial class HangMauDichEditForm : BaseForm
    {
        public bool IsEdited = false;
        public bool IsDeleted = false;
        //-----------------------------------------------------------------------------------------
        public HangMauDich HMD = new HangMauDich();
        public Company.GC.BLL.KDT.GC.HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        public string LoaiHangHoa = "";
        public string NhomLoaiHinh = string.Empty;
        public ToKhaiMauDich TKMD;
        public decimal TyGiaTT;

        //-----------------------------------------------------------------------------------------				
        // Tính thuế
        private decimal luong;
        private decimal dgnt;
        private decimal tgnt;

        private decimal tgtt_nk;
        private decimal tgtt_ttdb;
        private decimal tgtt_gtgt;
        private decimal clg;

        private decimal ts_nk;
        private decimal ts_ttdb;
        private decimal ts_gtgt;
        private decimal tl_clg;

        private decimal tt_nk;
        private decimal tt_ttdb;
        private decimal tt_gtgt;
        private decimal st_clg;
        //-----------------------------------------------------------------------------------------
        private string MaHangOld = "";
        //private NguyenPhuLieuRegistedForm NPLRegistedForm;
        //private SanPhamRegistedForm SPRegistedForm;
        private decimal LuongCon = 0;
        //-----------------------------------------------------------------------------------------
        //DATLMQ bổ sung biến phục vụ cho việc tự động lưu nội dung tờ khai sửa đổi bổ sung 19/05/2011
        public static string maHS_Old = string.Empty;
        public static string tenHang_Old = string.Empty;
        public static string maHang_Old = string.Empty;
        public static string xuatXu_Old = string.Empty;
        public static decimal soLuong_Old;
        public static string dvt_Old = string.Empty;
        public static decimal donGiaNT_Old;
        public static decimal triGiaNT_Old;

        public HangMauDichEditForm()
        {
            InitializeComponent();
        }
        private void KiemTraTonTaiHang()
        {
            if (this.TKMD.LoaiHangHoa == "N")
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = txtMaHang.Text.Trim();
                if (npl.Load())
                {
                    epError.SetError(txtMaHang, null);
                    epError.SetError(txtTenHang, null);
                    epError.SetError(txtMaHS, null);

                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại nguyên phụ liệu này.", "This value is not exist"));
                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    return;
                }
            }
            else if (this.TKMD.LoaiHangHoa == "S")
            {
                SanPham sp = new SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = txtMaHang.Text.Trim();
                if (sp.Load())
                {
                    epError.SetError(txtMaHang, null);
                    epError.SetError(txtTenHang, null);
                    epError.SetError(txtMaHS, null);
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại sản phẩm này.", "This value is not exist"));
                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    return;
                }
            }
            else if (this.TKMD.LoaiHangHoa == "T")
            {
                ThietBi tb = new ThietBi();
                tb.HopDong_ID = HD.ID;
                tb.Ma = txtMaHang.Text.Trim();
                if (tb.Load())
                {
                    epError.SetError(txtMaHang, null);
                    epError.SetError(txtTenHang, null);
                    epError.SetError(txtMaHS, null);
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));
                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    return;
                }
            }

        }
        private void khoitao_GiaoDien()
        {
            if (this.NhomLoaiHinh.StartsWith("N")) SetVisibleHTS(false);
            else
            {
                //if (this.LoaiHangHoa != "S" || this.TKMD.MaMid == "") SetVisibleHTS(false);
                if (this.LoaiHangHoa != "S" || GlobalSettings.MaMID.Trim() == "") SetVisibleHTS(false);
                
            }
            if (this.TKMD.LoaiHangHoa == "N")
            {
                txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            }
            else
            {
                txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
                txtSoLuong_HTS.DecimalDigits = GlobalSettings.SoThapPhan.LuongSP;
            }
            if (NhomLoaiHinh == "NGC")
            {
                lblSoLuongDaCo.Text = setText("Lượng còn được nhập","The import quanlity remain ");
            }
            else
            {
                lblSoLuongDaCo.Text = setText("Lượng còn được xuất", "The export quanlity remain");
            }
        }
        private void SetVisibleHTS(bool b)
        {
            txtMa_HTS.Visible = lbl_MaHTS.Visible = txtSoLuong_HTS.Visible = lblSoLuong_HTS.Visible = cbbDVT_HTS.Visible = lblDVT_HTS.Visible = b;
            if (!b)
            {
                txtMaHS.Width = txtTenHang.Width = cbDonViTinh.Width = txtLuong.Width = txtMaHang.Width;
            }
        }
        private void khoitao_DuLieuChuan()
        {
            // Nước XX.
            this._Nuoc = Nuoc.SelectAll();

            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll();
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            cbbDVT_HTS.DataSource = this._DonViTinh;
            cbbDVT_HTS.SelectedValue = "15 ";
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
        }

   


        private void HangMauDichEditForm_Load(object sender, EventArgs e)
        {
            ctrNuocXX.Ma = this.HMD.NuocXX_ID;
            NhomLoaiHinh = TKMD.MaLoaiHinh.Substring(0, 3);
            LoaiHangHoa = TKMD.LoaiHangHoa;
            this.khoitao_DuLieuChuan();
            this.khoitao_GiaoDien();
            lblNguyenTe_DGNT.Text = lblNguyenTe_TGNT.Text = "(" + this.TKMD.NguyenTe_ID + ")";
            toolTip1.SetToolTip(lblNguyenTe_DGNT, this.TKMD.NguyenTe_ID);
            toolTip1.SetToolTip(lblNguyenTe_TGNT, this.TKMD.NguyenTe_ID);

            // Tính lại thuế.
            //this.HMD.TinhThue(this.TyGiaTT);
            lblTyGiaTT.Text += " : " + this.TyGiaTT.ToString("N");

            // Bind data.
            txtMaHang.Text = this.HMD.MaPhu;
            this.txtMaHang_Leave(null, null);
            txtTenHang.Text = this.HMD.TenHang;
            txtMaHS.Text = this.HMD.MaHS;
            txtMa_HTS.Text = this.HMD.Ma_HTS;
            txtSoLuong_HTS.Value = this.HMD.SoLuong_HTS;
            cbDonViTinh.SelectedValue = this.HMD.DVT_ID;
            cbbDVT_HTS.SelectedValue = this.HMD.DVT_HTS;
            //luu lai ma hang
            MaHangOld = this.HMD.MaPhu;

            maHS_Old = this.HMD.MaHS;
            tenHang_Old = this.HMD.TenHang;
            maHang_Old = this.HMD.MaPhu;
            xuatXu_Old = this.HMD.NuocXX_ID;
            soLuong_Old = this.HMD.SoLuong;
            dvt_Old = this.HMD.DVT_ID;
            triGiaNT_Old = this.HMD.TriGiaKB;
            donGiaNT_Old = this.HMD.DonGiaKB;

            txtDGNT.Value = this.dgnt = this.HMD.DonGiaKB;
            txtLuong.Value = this.luong = this.HMD.SoLuong;
            txtTGNT.Value = this.HMD.TriGiaKB;
            txtTriGiaKB.Value = this.HMD.TriGiaKB_VND;
            ctrNuocXX.Ma = this.HMD.NuocXX_ID;
            txtTGTT_NK.Value = this.HMD.TriGiaTT;
            txtTS_NK.Value = this.ts_nk = this.HMD.ThueSuatXNK;
            txtTS_TTDB.Value = this.ts_ttdb = this.HMD.ThueSuatTTDB;
            txtTS_GTGT.Value = this.ts_gtgt = this.HMD.ThueSuatGTGT;
            txtTL_CLG.Value = this.tl_clg = this.HMD.TyLeThuKhac;

            txtTSXNKGiam.Text = HMD.ThueSuatXNKGiam;
            txtTSTTDBGiam.Text = HMD.ThueSuatTTDBGiam;
            txtTSVatGiam.Text = HMD.ThueSuatVATGiam;
            this.tinhthue2();
            if (this.OpenType == Company.GC.BLL.OpenFormType.View)
                btnGhi.Enabled = false;
            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                btnGhi.Enabled = true;
        }

        private void txtLuong_Leave(object sender, EventArgs e)
        {
            this.luong = Convert.ToDecimal(txtLuong.Value);
            txtSoLuong_HTS.Value = Math.Round(this.luong / 12);
            LuongCon = Convert.ToDecimal(txtSoLuongConDcNhap.Text);
            if (this.luong > LuongCon)
            {
                if (NhomLoaiHinh.StartsWith("N"))
                {
                    //if (!(MLMessages("Bạn đã nhập quá số lượng có thể nhập.Bạn có muốn tiếp tục không?","MSG_WRN16","", true) == "Yes"))
                    if (!(showMsg("MSG_WRN07", true) == "Yes"))
                    {
                        luong = 0;
                        txtLuong.Value = "0";
                    }
                }
                else
                {
                    //if (!(MLMessages("Bạn đã xuất quá số lượng có thể xuất.Bạn có muốn tiếp tục không?", "MSG_WRN16", "", true) == "Yes"))
                    if (!(showMsg("MSG_WRN08", true) == "Yes"))
                    {
                        luong = 0;
                        txtLuong.Value = "0";
                    }
                }
            }
            LuongCon = 0;
            this.tinhthue();
        }

        private void txtDGNT_Leave(object sender, EventArgs e)
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Value);
            this.tinhthue();
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtMaHang_ButtonClick(object sender, EventArgs e)
        {
            if (this.LoaiHangHoa == "N")
            {
                NguyenPhuLieuRegistedGCForm f2 = new NguyenPhuLieuRegistedGCForm();
                f2.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                f2.isBrower = true;
                f2.ShowDialog();
                if (f2.NguyenPhuLieuSelected.Ma != "")
                {
                    txtMaHang.Text = f2.NguyenPhuLieuSelected.Ma;
                    txtTenHang.Text = f2.NguyenPhuLieuSelected.Ten;
                    txtMaHS.Text = f2.NguyenPhuLieuSelected.MaHS;
                    cbDonViTinh.SelectedValue = f2.NguyenPhuLieuSelected.DVT_ID;
                    if (NhomLoaiHinh == "NGC")
                    {
                        LuongCon = f2.NguyenPhuLieuSelected.SoLuongDangKy - f2.NguyenPhuLieuSelected.SoLuongDaDung - f2.NguyenPhuLieuSelected.SoLuongCungUng;
                        if (HMD.ID > 0 && HMD.MaPhu.Trim().ToUpper() == f2.NguyenPhuLieuSelected.Ma.ToUpper().Trim())
                            LuongCon += HMD.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = f2.NguyenPhuLieuSelected.SoLuongDaNhap - f2.NguyenPhuLieuSelected.SoLuongDaDung;// +f2.NguyenPhuLieuSelected.SoLuongCungUng;
                        if (HMD.ID > 0 && HMD.MaPhu.Trim().ToUpper() == f2.NguyenPhuLieuSelected.Ma.ToUpper().Trim())
                            LuongCon += HMD.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                }
            }
            else if (LoaiHangHoa == "S")
            {
                SanPhamRegistedGCForm f3 = new SanPhamRegistedGCForm();
                f3.isBrower = true;
                f3.SanPhamSelected.HopDong_ID = HD.ID;
                f3.ShowDialog();
                if (f3.SanPhamSelected.Ma != "")
                {
                    txtMaHang.Text = f3.SanPhamSelected.Ma;
                    txtTenHang.Text = f3.SanPhamSelected.Ten;
                    txtMaHS.Text = f3.SanPhamSelected.MaHS;
                    cbDonViTinh.SelectedValue = f3.SanPhamSelected.DVT_ID;
                    LuongCon = f3.SanPhamSelected.SoLuongDangKy - f3.SanPhamSelected.SoLuongDaXuat;
                    if (HMD.ID > 0 && HMD.MaPhu.Trim().ToUpper() == f3.SanPhamSelected.Ma.ToUpper().Trim())
                        LuongCon += HMD.SoLuong;
                    txtSoLuongConDcNhap.Text = LuongCon.ToString();
                }
            }
            else
            {
                ThietBiRegistedForm ftb = new ThietBiRegistedForm();
                ftb.ThietBiSelected.HopDong_ID = this.HD.ID;
                ftb.isBrower = true;
                ftb.ShowDialog();
                if (ftb.ThietBiSelected.Ma != "")
                {
                    txtMaHang.Text = ftb.ThietBiSelected.Ma;
                    txtTenHang.Text = ftb.ThietBiSelected.Ten;
                    txtMaHS.Text = ftb.ThietBiSelected.MaHS;
                    cbDonViTinh.SelectedValue = ftb.ThietBiSelected.DVT_ID;
                    if (NhomLoaiHinh == "NGC")
                    {
                        LuongCon = ftb.ThietBiSelected.SoLuongDangKy - ftb.ThietBiSelected.SoLuongDaNhap;
                        if (HMD.ID > 0 && HMD.MaPhu.Trim().ToUpper() == ftb.ThietBiSelected.Ma.ToUpper().Trim())
                            LuongCon += HMD.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = ftb.ThietBiSelected.SoLuongDaNhap;
                        if (HMD.ID > 0 && HMD.MaPhu.Trim().ToUpper() == ftb.ThietBiSelected.Ma.ToUpper().Trim())
                            LuongCon += HMD.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                }
            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            if (TKMD.ID > 0)
            {
                if (TKMD.MaLoaiHinh.Contains("XGC"))
                {
                    if (PhanBoToKhaiXuat.CheckPhanBoToKhaiXuat(TKMD.ID))
                    {
                        showMsg("MSG_ALL01");
                        //ShowMessage("Tờ khai này đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                        return;
                    }
                }
            }
            KiemTraTonTaiHang();
            this.tinhthue();
            txtLuong.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS,setText( "Mã số HS không hợp lệ.","This value is invalid"));
                return;
            }
            if (checkMaHangExit(txtMaHang.Text.Trim()))
            {
                showMsg("MSG_0203054");
                //ShowMessage("Đã có hàng này trong danh sách.", false);
                return;
            }

            if (TKMD.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
            {
                if (maHS_Old != txtMaHS.Text)
                    ToKhaiMauDichForm.maHS_Edit = txtMaHS.Text;
                else
                    ToKhaiMauDichForm.maHS_Edit = maHS_Old;
                if (tenHang_Old != txtTenHang.Text)
                    ToKhaiMauDichForm.tenHang_Edit = txtTenHang.Text;
                else
                    ToKhaiMauDichForm.tenHang_Edit = tenHang_Old;
                if (maHang_Old != txtMaHang.Text)
                    ToKhaiMauDichForm.maHang_Edit = txtMaHang.Text;
                else
                    ToKhaiMauDichForm.maHang_Edit = maHang_Old;
                if (xuatXu_Old != ctrNuocXX.Ma)
                    ToKhaiMauDichForm.xuatXu_Edit = ctrNuocXX.Ma;
                else
                    ToKhaiMauDichForm.xuatXu_Edit = xuatXu_Old;
                if (dvt_Old != cbDonViTinh.SelectedValue.ToString())
                    ToKhaiMauDichForm.dvt_Edit = cbDonViTinh.SelectedValue.ToString();
                else
                    ToKhaiMauDichForm.dvt_Edit = dvt_Old;
                if (soLuong_Old != Convert.ToDecimal(txtLuong.Text))
                    ToKhaiMauDichForm.soLuong_Edit = Convert.ToDecimal(txtLuong.Text);
                else
                    ToKhaiMauDichForm.soLuong_Edit = soLuong_Old;
                if (donGiaNT_Old != Convert.ToDecimal(txtDGNT.Text))
                    ToKhaiMauDichForm.dongiaNT_Edit = Convert.ToDecimal(txtDGNT.Text);
                else
                    ToKhaiMauDichForm.dongiaNT_Edit = donGiaNT_Old;
                if (triGiaNT_Old != Convert.ToDecimal(txtTGNT.Text))
                    ToKhaiMauDichForm.trigiaNT_Edit = Convert.ToDecimal(txtTGNT.Text);
                else
                    ToKhaiMauDichForm.trigiaNT_Edit = triGiaNT_Old;

                try
                {
                    this.HMD.MaHS = txtMaHS.Text;
                    this.HMD.Ma_HTS = txtMa_HTS.Text;
                    this.HMD.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
                    this.HMD.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
                    this.HMD.MaPhu = txtMaHang.Text;
                    this.HMD.TenHang = txtTenHang.Text;
                    this.HMD.NuocXX_ID = ctrNuocXX.Ma;
                    this.HMD.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                    this.HMD.SoLuong = Convert.ToDecimal(txtLuong.Text);
                    this.HMD.DonGiaKB = Convert.ToDecimal(txtDGNT.Text);
                    this.HMD.TriGiaKB = Convert.ToDecimal(txtTGNT.Text);
                    this.HMD.Ma_HTS = txtMa_HTS.Text;
                    this.HMD.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
                    this.HMD.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
                    HMD.TriGiaKB_VND = Convert.ToDecimal(txtTriGiaKB.Value);
                    HMD.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Value);
                    HMD.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Value);
                    HMD.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Value);
                    HMD.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Value);
                    HMD.TyLeThuKhac = Convert.ToDecimal(txtTL_CLG.Value);
                    HMD.ThueXNK = Math.Round(Convert.ToDecimal(txtTienThue_NK.Value), 0);
                    HMD.ThueTTDB = Math.Round(Convert.ToDecimal(txtTienThue_TTDB.Value), 0);
                    HMD.ThueGTGT = Math.Round(Convert.ToDecimal(txtTienThue_GTGT.Value), 0);
                    HMD.DonGiaTT = HMD.TriGiaTT / Convert.ToDecimal(HMD.SoLuong);
                    HMD.TriGiaThuKhac = Convert.ToDecimal(txtTien_CLG.Value);
                    HMD.ThueSuatXNKGiam = txtTSXNKGiam.Text.Trim();
                    HMD.ThueSuatTTDBGiam = txtTSTTDBGiam.Text.Trim();
                    HMD.ThueSuatVATGiam = txtTSVatGiam.Text.Trim();
                    if (chkMienThue.Checked)
                        HMD.MienThue = 1;
                    else
                        HMD.MienThue = 0;
                    // Tính thuế.                      
                    this.IsEdited = true;
                    txtMaHang.Text = "";
                    txtTenHang.Text = "";
                    txtMaHS.Text = "";
                    txtLuong.Value = 0;
                    txtDGNT.Value = 0;
                    this.Close();
                }
                catch (Exception ex)
                {
                    ShowMessage(setText("Dữ liệu của bạn không hợp lệ ", "Invalid input value") + ex.Message, false);
                }
            }
            else
            {
                try
                {
                    this.HMD.MaHS = txtMaHS.Text;
                    this.HMD.Ma_HTS = txtMa_HTS.Text;
                    this.HMD.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
                    this.HMD.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
                    this.HMD.MaPhu = txtMaHang.Text;
                    this.HMD.TenHang = txtTenHang.Text;
                    this.HMD.NuocXX_ID = ctrNuocXX.Ma;
                    this.HMD.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                    this.HMD.SoLuong = Convert.ToDecimal(txtLuong.Text);
                    this.HMD.DonGiaKB = Convert.ToDecimal(txtDGNT.Text);
                    this.HMD.TriGiaKB = Convert.ToDecimal(txtTGNT.Text);
                    this.HMD.Ma_HTS = txtMa_HTS.Text;
                    this.HMD.DVT_HTS = cbbDVT_HTS.SelectedValue.ToString();
                    this.HMD.SoLuong_HTS = Convert.ToDecimal(txtSoLuong_HTS.Value);
                    HMD.TriGiaKB_VND = Convert.ToDecimal(txtTriGiaKB.Value);
                    HMD.TriGiaTT = Convert.ToDecimal(txtTGTT_NK.Value);
                    HMD.ThueSuatXNK = Convert.ToDecimal(txtTS_NK.Value);
                    HMD.ThueSuatTTDB = Convert.ToDecimal(txtTS_TTDB.Value);
                    HMD.ThueSuatGTGT = Convert.ToDecimal(txtTS_GTGT.Value);
                    HMD.TyLeThuKhac = Convert.ToDecimal(txtTL_CLG.Value);
                    HMD.ThueXNK = Math.Round(Convert.ToDecimal(txtTienThue_NK.Value), 0);
                    HMD.ThueTTDB = Math.Round(Convert.ToDecimal(txtTienThue_TTDB.Value), 0);
                    HMD.ThueGTGT = Math.Round(Convert.ToDecimal(txtTienThue_GTGT.Value), 0);
                    HMD.DonGiaTT = HMD.TriGiaTT / Convert.ToDecimal(HMD.SoLuong);
                    HMD.TriGiaThuKhac = Convert.ToDecimal(txtTien_CLG.Value);
                    HMD.ThueSuatXNKGiam = txtTSXNKGiam.Text.Trim();
                    HMD.ThueSuatTTDBGiam = txtTSTTDBGiam.Text.Trim();
                    HMD.ThueSuatVATGiam = txtTSVatGiam.Text.Trim();
                    if (chkMienThue.Checked)
                        HMD.MienThue = 1;
                    else
                        HMD.MienThue = 0;
                    // Tính thuế.                      
                    this.IsEdited = true;
                    txtMaHang.Text = "";
                    txtTenHang.Text = "";
                    txtMaHS.Text = "";
                    txtLuong.Value = 0;
                    txtDGNT.Value = 0;
                    this.Close();
                }
                catch (Exception ex)
                {
                    ShowMessage(setText("Dữ liệu của bạn không hợp lệ ", "Invalid input value") + ex.Message, false);
                }
            }
        }
        private bool checkMaHangExit(string maHang)
        {
            foreach (HangMauDich HMD in this.TKMD.HMDCollection)
            {
                if (HMD.MaPhu.Trim() == maHang && maHang != MaHangOld) return true;
            }
            return false;
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            this.IsDeleted = true;
            this.Close();
        }

        private void txtMaHang_Leave(object sender, EventArgs e)
        {
            if (LoaiHangHoa == "N")
            {
                NguyenPhuLieu npl = new NguyenPhuLieu();
                npl.HopDong_ID = HD.ID;
                npl.Ma = txtMaHang.Text.Trim();
                if (npl.Load())
                {
                    txtMaHang.Text = npl.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = npl.MaHS;
                    txtTenHang.Text = npl.Ten;
                    cbDonViTinh.SelectedValue = npl.DVT_ID;
                    if (NhomLoaiHinh == "NGC")
                    {
                        LuongCon = npl.SoLuongDangKy - npl.SoLuongDaNhap - npl.SoLuongCungUng; ;
                        if (HMD.ID > 0 && HMD.MaPhu.Trim().ToUpper() == npl.Ma.ToUpper().Trim())
                            LuongCon += HMD.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = npl.SoLuongDaNhap - npl.SoLuongDaDung;//+ npl.SoLuongCungUng; ;
                        if (HMD.ID > 0 && HMD.MaPhu.Trim().ToUpper() == npl.Ma.ToUpper().Trim())
                            LuongCon += HMD.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang,setText("Không tồn tại nguyên phụ liệu này.","This value is not exist"));

                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    txtSoLuongConDcNhap.Text = "0";
                    return;
                }
            }
            else if (LoaiHangHoa == "S")
            {
                SanPham sp = new SanPham();
                sp.HopDong_ID = HD.ID;
                sp.Ma = txtMaHang.Text.Trim();
                if (sp.Load())
                {
                    txtMaHang.Text = sp.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = sp.MaHS;
                    txtTenHang.Text = sp.Ten;
                    cbDonViTinh.SelectedValue = sp.DVT_ID;
                    LuongCon = sp.SoLuongDangKy - sp.SoLuongDaXuat;
                    if (HMD.ID > 0 && HMD.MaPhu.Trim().ToUpper() == sp.Ma.ToUpper().Trim())
                        LuongCon += HMD.SoLuong;
                    txtSoLuongConDcNhap.Text = LuongCon.ToString();
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang,setText( "Không tồn tại sản phẩm này.","This value is not exist"));

                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    txtSoLuongConDcNhap.Text = "0";
                    return;
                }
            }
            else if (LoaiHangHoa == "T")
            {
                ThietBi tb = new ThietBi();
                tb.HopDong_ID = HD.ID;
                tb.Ma = txtMaHang.Text.Trim();
                if (tb.Load())
                {
                    txtMaHang.Text = tb.Ma;
                    if (txtMaHS.Text.Trim().Length == 0)
                        txtMaHS.Text = tb.MaHS;
                    txtTenHang.Text = tb.Ten;
                    cbDonViTinh.SelectedValue = tb.DVT_ID;
                    if (NhomLoaiHinh.StartsWith("NGC"))
                    {
                        LuongCon = tb.SoLuongDangKy - tb.SoLuongDaNhap;
                        if (HMD.ID > 0 && HMD.MaPhu.Trim().ToUpper() == tb.Ma.ToUpper().Trim())
                            LuongCon += HMD.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                    else
                    {
                        LuongCon = tb.SoLuongDaNhap;
                        if (HMD.ID > 0 && HMD.MaPhu.Trim().ToUpper() == tb.Ma.ToUpper().Trim())
                            LuongCon += HMD.SoLuong;
                        txtSoLuongConDcNhap.Text = LuongCon.ToString();
                    }
                }
                else
                {
                    epError.SetIconPadding(txtMaHang, -8);
                    epError.SetError(txtMaHang, setText("Không tồn tại thiết bị này.", "This value is not exist"));

                    txtTenHang.Text = txtMaHS.Text = string.Empty;
                    cbDonViTinh.SelectedValue = "";
                    txtSoLuongConDcNhap.Text = "0";
                    return;
                }
            }
        }


        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã HS không hợp lệ.", "This value is invalid"));
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }
            string MoTa = MaHS.CheckExist(txtMaHS.Text);
            if (MoTa == "")
            {
                epError.SetIconPadding(txtMaHS, -8);
                epError.SetError(txtMaHS, setText("Mã HS không có trong danh mục mã HS.", "This value is not exist"));
            }
            else
            {
                epError.SetError(txtMaHS, string.Empty);
            }
        }
        private decimal tinhthue2()
        {
            this.dgnt = Convert.ToDecimal(txtDGNT.Text);
            this.luong = Convert.ToDecimal(txtLuong.Text);
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;

            this.tgnt = this.dgnt * Convert.ToDecimal(this.luong);
            this.tgtt_nk = Convert.ToDecimal(txtTGTT_NK.Value);
            if (tgtt_nk == 0)
            {
                tgtt_nk = tgnt * TyGiaTT;
            }

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private decimal tinhthue()
        {
            this.tgnt = this.dgnt * Convert.ToDecimal(this.luong);
            this.tgtt_nk = this.tgnt * this.TyGiaTT;

            this.tt_nk = this.tgtt_nk * this.ts_nk;

            this.tgtt_ttdb = this.tgtt_nk + this.tt_nk;
            this.tt_ttdb = this.tgtt_ttdb * ts_ttdb;

            this.tgtt_gtgt = this.tgtt_ttdb + this.tt_ttdb;
            this.tt_gtgt = this.tgtt_gtgt * this.ts_gtgt;

            this.clg = this.tgtt_nk;
            this.st_clg = this.clg * this.tl_clg;

            txtTriGiaKB.Value = this.tgnt * this.TyGiaTT;
            txtTGNT.Value = this.tgnt;
            txtTGTT_NK.Value = this.tgtt_nk;
            txtTGTT_TTDB.Value = this.tgtt_ttdb;
            txtTGTT_GTGT.Value = this.tgtt_gtgt;
            txtCLG.Value = this.clg;

            txtTienThue_NK.Value = Math.Round(this.tt_nk, MidpointRounding.AwayFromZero);
            txtTienThue_TTDB.Value = Math.Round(this.tt_ttdb, MidpointRounding.AwayFromZero);
            txtTienThue_GTGT.Value = Math.Round(this.tt_gtgt, MidpointRounding.AwayFromZero);
            txtTien_CLG.Value = Math.Round(this.st_clg, MidpointRounding.AwayFromZero);

            txtTongSoTienThue.Value = Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);

            return Math.Round(this.tt_nk + this.tt_ttdb + this.tt_gtgt + this.st_clg, MidpointRounding.AwayFromZero);
        }
        private void txtTGTT_NK_Leave(object sender, EventArgs e)
        {
            this.tinhthue2();
        }

        private void txtTS_NK_Leave(object sender, EventArgs e)
        {
            this.ts_nk = Convert.ToDecimal(txtTS_NK.Value) / 100;
            tinhthue2();
        }

        private void txtTS_TTDB_Leave(object sender, EventArgs e)
        {
            this.ts_ttdb = Convert.ToDecimal(txtTS_TTDB.Value) / 100;
            tinhthue2();
        }

        private void txtTS_GTGT_Leave(object sender, EventArgs e)
        {
            this.ts_gtgt = Convert.ToDecimal(txtTS_GTGT.Value) / 100;
            tinhthue2();
        }

        private void txtTL_CLG_Leave(object sender, EventArgs e)
        {
            this.tl_clg = Convert.ToDecimal(txtTL_CLG.Value) / 100;
            tinhthue2();
        }

        private void txtTGNT_Leave(object sender, EventArgs e)
        {
            decimal trigia = Convert.ToDecimal(txtTGNT.Text);
            decimal luong = Convert.ToDecimal(txtLuong.Text);
            decimal dongia = trigia / luong;
            txtDGNT.Text = dongia.ToString();
            this.dgnt = Convert.ToDecimal(txtDGNT.Value);            
            this.tinhthue();
        }

    }
}
