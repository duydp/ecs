﻿namespace Company.Interface.SXXK.BangKeThanhLy
{
    partial class TongNPLTonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgTKNTL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TongNPLTonForm));
            this.dgTKNTL = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgTKNTL)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.dgTKNTL);
            this.grbMain.Size = new System.Drawing.Size(632, 366);
            // 
            // dgTKNTL
            // 
            this.dgTKNTL.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKNTL.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgTKNTL.AlternatingColors = true;
            this.dgTKNTL.AutomaticSort = false;
            this.dgTKNTL.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgTKNTL.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgTKNTL.ColumnAutoResize = true;
            dgTKNTL_DesignTimeLayout.LayoutString = resources.GetString("dgTKNTL_DesignTimeLayout.LayoutString");
            this.dgTKNTL.DesignTimeLayout = dgTKNTL_DesignTimeLayout;
            this.dgTKNTL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgTKNTL.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgTKNTL.GroupByBoxVisible = false;
            this.dgTKNTL.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgTKNTL.ImageList = this.ImageList1;
            this.dgTKNTL.Location = new System.Drawing.Point(0, 0);
            this.dgTKNTL.Name = "dgTKNTL";
            this.dgTKNTL.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgTKNTL.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgTKNTL.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgTKNTL.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgTKNTL.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgTKNTL.Size = new System.Drawing.Size(632, 366);
            this.dgTKNTL.TabIndex = 20;
            this.dgTKNTL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgTKNTL.VisualStyleManager = this.vsmMain;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // TongNPLTonForm
            // 
            this.ClientSize = new System.Drawing.Size(632, 366);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "TongNPLTonForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Tổng lượng NPL tồn của tờ khai nhập đưa vào thanh khoản";
            this.Load += new System.EventHandler(this.TongNPLTonForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgTKNTL)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX dgTKNTL;
        private System.Windows.Forms.ImageList ImageList1;
    }
}
