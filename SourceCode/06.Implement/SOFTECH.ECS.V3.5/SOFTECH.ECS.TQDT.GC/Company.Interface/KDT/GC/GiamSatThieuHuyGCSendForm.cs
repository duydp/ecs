﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using System.Data;
using Company.GC.BLL.KDT;
using System.IO;
using Company.Interface.KDT.SXXK;
using Company.Interface.GC;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components.Common;
using Company.GC.BLL.KDT.SXXK;
using Company.KDT.SHARE.QuanLyChungTu;
namespace Company.Interface.KDT.GC
{
    public partial class GiamSatThieuHuyGCSendForm : BaseFormHaveGuidPanel
    {

        public HopDong HD = new Company.GC.BLL.KDT.GC.HopDong();
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        private List<HangGSTieuHuy> HangGsTieuHuys = new List<HangGSTieuHuy>();
        public GiamSatTieuHuy dnGSTieuHuy = new GiamSatTieuHuy() { TrangThaiXuLy = -1 };
        private GiamSatTieuHuyBoSung gsTieuHuyBS = new GiamSatTieuHuyBoSung();
        private List<GiamSatTieuHuyBoSung> gsTieuHuyBSCollections = new List<GiamSatTieuHuyBoSung>();
        public String Caption;
        public bool IsChange;
        public GiamSatThieuHuyGCSendForm()
        {
            InitializeComponent();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void Set(GiamSatTieuHuy gsTieuHuy)
        {
            txtSoGiayPhep.TextChanged -= new EventHandler(txt_TextChanged);
            txtSoGiayPhep.Text = gsTieuHuy.SoGiayPhep;
            txtSoGiayPhep.TextChanged += new EventHandler(txt_TextChanged);

            ccNgayGiayPhep.TextChanged -= new EventHandler(txt_TextChanged);
            ccNgayGiayPhep.Value = gsTieuHuy.NgayGiayPhep.Year > 1900 ? gsTieuHuy.NgayGiayPhep : DateTime.Now;
            ccNgayGiayPhep.TextChanged += new EventHandler(txt_TextChanged);

            ccNgayHetHan.TextChanged -= new EventHandler(txt_TextChanged);
            ccNgayHetHan.Value = gsTieuHuy.NgayHetHan.Year > 1900 ? gsTieuHuy.NgayHetHan : DateTime.Now;
            ccNgayHetHan.TextChanged += new EventHandler(txt_TextChanged);

            txtToChucCap.TextChanged -= new EventHandler(txt_TextChanged);
            txtToChucCap.Text = gsTieuHuy.ToChucCap;
            txtToChucCap.TextChanged += new EventHandler(txt_TextChanged);

            txtCacbenthamgia.TextChanged -= new EventHandler(txt_TextChanged);
            txtCacbenthamgia.Text = gsTieuHuy.CacBenThamGia;
            txtCacbenthamgia.TextChanged += new EventHandler(txt_TextChanged);

            ccThoiGianTieuHuy.TextChanged -= new EventHandler(txt_TextChanged);
            ccThoiGianTieuHuy.Value = gsTieuHuy.ThoiGianTieuHuy.Year > 1900 ? gsTieuHuy.ThoiGianTieuHuy : DateTime.Now;
            ccThoiGianTieuHuy.TextChanged += new EventHandler(txt_TextChanged);

            txtDiaDiemTieuHuy.TextChanged -= new EventHandler(txt_TextChanged);
            txtDiaDiemTieuHuy.Text = gsTieuHuy.DiaDiemTieuHuy;
            txtDiaDiemTieuHuy.TextChanged += new EventHandler(txt_TextChanged);

            txtGhiChuKhac.TextChanged -= new EventHandler(txt_TextChanged);
            txtGhiChuKhac.Text = gsTieuHuy.GhiChuKhac;
            txtGhiChuKhac.TextChanged += new EventHandler(txt_TextChanged);

            gsTieuHuyBSCollections = (List<GiamSatTieuHuyBoSung>)GiamSatTieuHuyBoSung.SelectCollectionBy_GSTH_ID(gsTieuHuy.ID);
            if (gsTieuHuyBSCollections.Count != 0)
            {
                txtSoThamChieuPLHD.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoThamChieuPLHD.Text = gsTieuHuyBSCollections[0].SoThamChieuPLHD;
                txtSoThamChieuPLHD.TextChanged += new EventHandler(txt_TextChanged);

                txtSoDangKyPLHD.TextChanged -= new EventHandler(txt_TextChanged);
                txtSoDangKyPLHD.Text = gsTieuHuyBSCollections[0].SoDangKyPLHD;
                txtSoDangKyPLHD.TextChanged += new EventHandler(txt_TextChanged);

                dtNgayDangKyPLHD.TextChanged -= new EventHandler(txt_TextChanged);
                dtNgayDangKyPLHD.Value = gsTieuHuyBSCollections[0].NgayDangKyPLHD;
                dtNgayDangKyPLHD.TextChanged += new EventHandler(txt_TextChanged);
            }
        }
        private void Get(GiamSatTieuHuy gsTieuHuy)
        {
            gsTieuHuy.SoGiayPhep = txtSoGiayPhep.Text;
            gsTieuHuy.NgayGiayPhep = Convert.ToDateTime(ccNgayGiayPhep.Value);
            gsTieuHuy.NgayHetHan = Convert.ToDateTime(ccNgayHetHan.Value);
            gsTieuHuy.ToChucCap = txtToChucCap.Text;
            gsTieuHuy.CacBenThamGia = txtCacbenthamgia.Text;
            gsTieuHuy.ThoiGianTieuHuy = Convert.ToDateTime(ccThoiGianTieuHuy.Value);
            gsTieuHuy.DiaDiemTieuHuy = txtDiaDiemTieuHuy.Text;
            gsTieuHuy.GhiChuKhac = txtGhiChuKhac.Text;
            gsTieuHuy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            gsTieuHuy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            gsTieuHuy.HopDong_ID = HD.ID;

            //gsTieuHuy.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
            if (gsTieuHuy.NgayTiepNhan.Year < 1900) gsTieuHuy.NgayTiepNhan = new DateTime(1900, 1, 1);

            if (gsTieuHuyBSCollections.Count != 0)
            {
                gsTieuHuyBS = gsTieuHuyBSCollections[0];
                gsTieuHuyBS.SoThamChieuPLHD = txtSoThamChieuPLHD.Text;
                gsTieuHuyBS.SoDangKyPLHD = txtSoDangKyPLHD.Text;
                gsTieuHuyBS.NgayDangKyPLHD = dtNgayDangKyPLHD.Value;
            }
            else
            {
                gsTieuHuyBS = new GiamSatTieuHuyBoSung();
                gsTieuHuyBS.GSTH_ID = gsTieuHuy.ID;
                gsTieuHuyBS.SoThamChieuPLHD = txtSoThamChieuPLHD.Text;
                gsTieuHuyBS.SoDangKyPLHD = txtSoDangKyPLHD.Text;
                gsTieuHuyBS.NgayDangKyPLHD = dtNgayDangKyPLHD.Value;
            }
        }
        private void GiamSatThieuHuyGCSendForm_Load(object sender, EventArgs e)
        {
            txtSoGiayPhep.Focus();
            if (dnGSTieuHuy == null)
                dnGSTieuHuy = new GiamSatTieuHuy();
            Set(dnGSTieuHuy);
            HangGsTieuHuys = HangGSTieuHuy.SelectCollectionDynamic("Master_ID=" + dnGSTieuHuy.ID, "");
            dgList.DataSource = HangGsTieuHuys;
            Caption = this.Text;
            if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.GiamSatTieuHuy;
                sendXML.master_id = dnGSTieuHuy.ID;
                if (sendXML.Load())
                {
                    cmdSend.Enabled = cmdSend1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdDelete.Enabled = cmdDelete1.Enabled =
                        cmdChonHang.Enabled = cmdChonHang1.Enabled = HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    lblTrangThai.Text = "Đã khai báo nhưng chưa nhận được phản hồi";
                }
                else
                    setCommandStatus();
            }
            else
            {
                if (OpenType == OpenFormType.Insert)
                {
                    dnGSTieuHuy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                }
                setCommandStatus();
            }
        }
        public void setCommandStatus()
        {
            txtSoHopDong.Text = HD.SoHopDong;
            txtSoTiepNhan.Text = dnGSTieuHuy.SoTiepNhan != 0 ? dnGSTieuHuy.SoTiepNhan.ToString() : string.Empty;
            if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {

                cmdChonHang.Enabled = cmdChonHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdDelete.Enabled = cmdDelete1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
            }
            else if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                lblTrangThai.Text = setText("Chờ duyệt", "Wait for approval");
                HuyKhaiBao.Enabled = HuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdDelete.Enabled = cmdDelete1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChonHang.Enabled = cmdChonHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

            }
            else if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET
                || dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO
                || dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {

                HuyKhaiBao.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChonHang.Enabled = cmdChonHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdDelete.Enabled = cmdDelete1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            }
            if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                lblTrangThai.Text = setText("Chưa khai báo", "Not declared yet");
            }
            else if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                lblTrangThai.Text = "Không phê duyệt";
            }
            else if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                lblTrangThai.Text = "Đã hủy";
        }

        private void ChonHang()
        {
            CheckValid();
            HangTieuHuyForm f = new HangTieuHuyForm();
            f.dnGSTieuHuy = dnGSTieuHuy;
            f.HangTieuHuys = HangGsTieuHuys;
            f.IdHopDong = HD.ID;
            f.ShowDialog();
            HangGsTieuHuys = f.HangTieuHuys;
            dgList.DataSource = HangGsTieuHuys;
            this.SetChange(f.IsChange);
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void Xoa()
        {
            try
            {
                if (ShowMessage("Bạn có thật sự muốn xóa không?", true) != "Yes") return;
                dnGSTieuHuy.Delete();
                HangGSTieuHuy.DeleteCollection(HangGsTieuHuys);
                dnGSTieuHuy = new GiamSatTieuHuy();
                HangGsTieuHuys = new List<HangGSTieuHuy>();
                ShowMessage("Xóa thành công", false);
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdDelete":
                    Xoa();
                    break;
                case "cmdSave":
                    this.save();
                    break;
                case "cmdChonHang":
                    this.ChonHang();
                    break;
                case "cmdSend":
                    this.SendV3();
                    break;
                    //Msg dung để huy khai báo khong được sửa dụng trên hệ thống HQ.
                //case "HuyKhaiBao":
                //    SendHuyV5();
                //    break;
                case "NhanDuLieu":
                    this.FeedBackV3();
                    break;
                case "XacNhan":
                    // this.FeedBackV3();
                    //this.LaySoTiepNhanDT();
                    break;
                case "TaoMoiDM":
                    //  this.TaoDanhSachDMMoi();
                    break;
                case "cmdResult":
                    this.Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGuidString();
                    break;
                case "cmdAddExcel":
                    this.AddExcel();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;

            }
        }
        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = dnGSTieuHuy.ID;
                f.loaiKhaiBao = LoaiKhaiBao.GiamSatTieuHuy;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", dnGSTieuHuy.ID, LoaiKhaiBao.GiamSatTieuHuy), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageGSTieuHuy(dnGSTieuHuy);
                    dnGSTieuHuy.InsertUpdate();
                    ShowMessage("Cập nhật thông tin thành công .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void AddExcel()
        {
            try
            {
                GiamSatThieuHuyReadExcelForm f = new GiamSatThieuHuyReadExcelForm();
                f.giamSatTieuHuy = dnGSTieuHuy;
                f.ShowDialog(this);
                dgList.DataSource = HangGsTieuHuys;
                this.SetChange(f.ImportSucess);
                try
                {
                    dgList.Refetch();
                }
                catch
                {
                    dgList.Refresh();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void UpdateGuidString()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_GC_GiamSatTieuHuy", "", Convert.ToInt32(gsTieuHuyBS.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void Result()
        {
            btnResultHistory_Click(null,null);
        }
        private bool CheckValid()
        {
            bool isValid = false;
            isValid = ValidateControl.ValidateNull(txtSoGiayPhep, error, "Số giấy phép");
            isValid &= ValidateControl.ValidateDate(ccNgayGiayPhep, error, "Ngày giấy phép");
            isValid &= ValidateControl.ValidateDate(ccNgayHetHan, error, "Ngày hết hạn");

            isValid &= ValidateControl.ValidateNull(txtSoThamChieuPLHD, error, "Số tham chiếu phụ lục hợp đồng");
            isValid &= ValidateControl.ValidateNull(txtSoDangKyPLHD, error, "Số đăng ký phụ lục hợp đồng");

            isValid &= ValidateControl.ValidateNull(txtToChucCap, error, "Tổ chức cấp");
            isValid &= ValidateControl.ValidateNull(txtCacbenthamgia, error, "Các bên tham gia");
            isValid &= ValidateControl.ValidateNull(ccThoiGianTieuHuy, error, "Thời gian tiêu hủy");
            isValid &= ValidateControl.ValidateNull(txtDiaDiemTieuHuy, error, "Địa điểm tiêu hủy");
            return isValid;
        }
        private void save()
        {
            if (!CheckValid()) return;

            //Kiem tra hang giam sat
            if (HangGsTieuHuys == null || HangGsTieuHuys.Count == 0)
            {
                ShowMessage("Chưa chọn hàng giám sát, tiêu hủy.", false);
                return;
            }

            Get(dnGSTieuHuy);
            try
            {
                if (string.IsNullOrEmpty(dnGSTieuHuy.GUIDSTR))
                    dnGSTieuHuy.GUIDSTR = Guid.NewGuid().ToString();

                if (dnGSTieuHuy.ID == 0)
                    dnGSTieuHuy.Insert();
                else 
                    dnGSTieuHuy.Update();

                //Luu thong tin bo sung cua Giam sat tieu huy
                gsTieuHuyBS.InsertUpdate();

               // HangGSTieuHuy.DeleteDynamic("Master_ID=" + dnGSTieuHuy.ID);
                int index = 1;
                foreach (HangGSTieuHuy item in HangGsTieuHuys)
                {
                    item.STTHang = index++;
                    item.Master_ID = dnGSTieuHuy.ID;
                    if (item.ID > 0)
                        item.InsertUpdate();
                    else
                        item.Insert();
                    if (item.DVT_QuyDoi != null)
                    {
                        if (item.DVT_QuyDoi.IsUse)
                        {
                            item.DVT_QuyDoi.Master_ID = item.ID;
                            item.DVT_QuyDoi.Type = "YCGSTH";
                            item.DVT_QuyDoi.InsertUpdate();
                        }
                        else
                        {
                            if (item.DVT_QuyDoi.ID > 0)
                                item.DVT_QuyDoi.Delete();
                        }
                    }
                }
                this.SetChange(false);
                ShowMessage("Lưu thành công", false);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, false);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {

            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = dnGSTieuHuy.ID;
            form.DeclarationIssuer = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY;
            form.ShowDialog(this);
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            HangTieuHuyForm f = new HangTieuHuyForm();
            f.dnGSTieuHuy = dnGSTieuHuy;
            f.HangTieuHuys = HangGsTieuHuys;
            f.hangTieuHuy = (HangGSTieuHuy)e.Row.DataRow;
            f.IsEnable = dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ||
                        dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET ||
                        dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY;
            f.ShowDialog();
            this.SetChange(f.IsChange);
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            int i = Convert.ToInt32(e.Row.Cells["LoaiHang"].Value);
            string loaiHang = string.Empty;
            if (i == 1) loaiHang = "Nguyên phụ liệu";
            else if (i == 2) loaiHang = "Sản phẩm";
            else if (i == 3) loaiHang = "Thiết bị";
            else if (i == 4) loaiHang = "Hàng mẫu";
            e.Row.Cells["LoaiHang"].Text = loaiHang;
        }

        #region V3
        private void SendV3()
        {
            if (dnGSTieuHuy.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            if (HangGsTieuHuys.Count == 0)
            {
                ShowMessage("Bạn chưa nhập hàng", false);
                return;
            }
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.GiamSatTieuHuy;
            sendXML.master_id = dnGSTieuHuy.ID;

            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }
            dnGSTieuHuy.HangGSTieuHuys = HangGsTieuHuys;
            HopDong HD = new HopDong();
            HD.ID = dnGSTieuHuy.HopDong_ID;
            HD = HopDong.Load(dnGSTieuHuy.HopDong_ID);
            try
            {
                dnGSTieuHuy.GUIDSTR = Guid.NewGuid().ToString();
                PhuKienDangKyCollection pkHD = new PhuKienDangKyCollection();
                pkHD = HD.GetPK();
                //if (pkHD.Count <= 0)
                //{
                //    ShowMessage("Hợp đồng không có phụ kiện", false);
                //    return;
                //}

                GC_DNGiamSatTieuHuy dnGiamSatTieuHuy = Mapper_V4.ToDataTransferGSTieuHuy(dnGSTieuHuy, HD);
                ObjectSend msgSend = new ObjectSend(
                               new NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = dnGSTieuHuy.MaDoanhNghiep
                               }
                                 , new NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dnGSTieuHuy.MaHaiQuan),
                                     Identity =  VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dnGSTieuHuy.MaHaiQuan.Trim())// : dnGSTieuHuy.MaHaiQuan
                                 }
                              ,
                                new SubjectBase()
                                {
                                    Type = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                                    Function = dnGiamSatTieuHuy.Function,
                                    Reference = dnGSTieuHuy.GUIDSTR,
                                }
                                ,
                                dnGiamSatTieuHuy);

                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);

                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(dnGSTieuHuy.ID, MessageTitle.KhaiBaoDNTieuHuy);
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = cmdSend1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdDelete.Enabled = cmdDelete1.Enabled = cmdChonHang.Enabled = cmdChonHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    sendXML.LoaiHS = LoaiKhaiBao.GiamSatTieuHuy;
                    sendXML.master_id = dnGSTieuHuy.ID;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.func = Convert.ToInt32(dnGiamSatTieuHuy.Function);
                    sendXML.InsertUpdate();
                    dnGSTieuHuy.Update();
                    FeedBackV3();
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                    ShowMessageTQDT(msgInfor, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(ex.Message, dnGSTieuHuy.MaHaiQuan, new SendEventArgs(string.Empty, new TimeSpan(), ex));
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.DNGiamSatTieuHuySendHandler(dnGSTieuHuy, ref msgInfor, e);

        }
        private void FeedBackV3()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;

            SubjectBase subjectBase = new SubjectBase()
            {
                Issuer = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                Reference = dnGSTieuHuy.GUIDSTR,
                Function = DeclarationFunction.HOI_TRANG_THAI,
                Type = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,

            };
            ObjectSend msgSend = new ObjectSend(
                                        new NameBase()
                                        {
                                            Name = GlobalSettings.TEN_DON_VI,
                                            Identity = dnGSTieuHuy.MaDoanhNghiep
                                        },
                                          new NameBase()
                                          {
                                              Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dnGSTieuHuy.MaHaiQuan.Trim()),
                                              Identity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(dnGSTieuHuy.MaHaiQuan)
                                              //Identity = dnGSTieuHuy.MaHaiQuan
                                          }, subjectBase, null);

            while (isFeedBack)
            {
                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET && count > 0)
                    {
                        if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;
                    }
                    else if (dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || dnGSTieuHuy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        ShowMessageTQDT(msgInfor, false);
                        isFeedBack = false;
                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    else isFeedBack = false;
                }
                if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                    this.setCommandStatus();
            }
            if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                this.setCommandStatus();
        }
        private void CanceldV3()
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.GiamSatTieuHuy;
            sendXML.master_id = dnGSTieuHuy.ID;
            if (sendXML.Load())
            {
                showMsg("MSG_WRN05");
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                return;
            }
            DeclarationBase CancelDnGSTieuHuy =
                Mapper.HuyKhaiBao(DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                                  dnGSTieuHuy.GUIDSTR,
                                  dnGSTieuHuy.SoTiepNhan,
                                  dnGSTieuHuy.MaHaiQuan,
                                  dnGSTieuHuy.NgayTiepNhan);
            ObjectSend msgSend = new ObjectSend(
                           new NameBase()
                           {
                               Name = GlobalSettings.TEN_DON_VI,
                               Identity = dnGSTieuHuy.MaDoanhNghiep
                           }
                             , new NameBase()
                             {
                                 Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(dnGSTieuHuy.MaHaiQuan),
                                 Identity = dnGSTieuHuy.MaHaiQuan
                             }
                          ,
                            new SubjectBase()
                            {
                                Type = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY,
                                Function = DeclarationFunction.HUY,
                                Reference = dnGSTieuHuy.GUIDSTR,
                            }
                            ,
                            CancelDnGSTieuHuy);
            SendMessageForm sendForm = new SendMessageForm();
            sendForm.Send += SendMessage;
            bool isSend = sendForm.DoSend(msgSend);
            if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
            {
                dnGSTieuHuy.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                sendForm.Message.XmlSaveMessage(dnGSTieuHuy.ID, MessageTitle.HQHuyKhaiBaoToKhai);
                NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dnGSTieuHuy.Update();
                FeedBackV3();
            }
            else if (!string.IsNullOrEmpty(msgInfor))
                ShowMessageTQDT(msgInfor, false);
        }
        private void SendHuyV5()
        {
            if (ShowMessage("Bạn có muốn khai báo hủy bảng kê tự cung ứng này ?", true) == "Yes")
            {
                //   bool isKhaiSua = false;
                if (dnGSTieuHuy.ID == 0)
                {
                    this.ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    dnGSTieuHuy = GiamSatTieuHuy.Load(dnGSTieuHuy.ID);
                    dnGSTieuHuy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    //if (BKCungUng.TrangThaiXuLy == 5)
                    //    isKhaiSua = true;
                    //else
                    //    isKhaiSua = false;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.GiamSatTieuHuy;
                sendXML.master_id = dnGSTieuHuy.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdChonHang.Enabled = cmdChonHang1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    
                    return;
                }
                try
                {
                        string returnMessage = string.Empty;
                        dnGSTieuHuy.GUIDSTR = Guid.NewGuid().ToString();
                        //SXXK_GiamSatTieuHuy GiamSatTieuHuy = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferGiamSatTieuHuy(BKCungUng, GlobalSettings.TEN_DON_VI, isKhaiSua);
                        //PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(BKCungUng, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = CancelMessageV5(dnGSTieuHuy);
                        //BKCungUng.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(dnGSTieuHuy.ID, "Hủy khai báo bảng kê");
                            NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdSave.Enabled = cmdSave1.Enabled = cmdChonHang.Enabled = cmdChonHang1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //btnDelete.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.GiamSatTieuHuy;
                            sendXML.master_id = dnGSTieuHuy.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = Convert.ToInt32(msgSend.Subject.Function);
                            sendXML.InsertUpdate();

                            dnGSTieuHuy.Update();
                            FeedBackV3();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        public static ObjectSend CancelMessageV5(GiamSatTieuHuy BK)
        {
            string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
            string sfmtDate = "yyyy-MM-dd";
            //string sfmtVnDateTime = "dd-MM-yyyy HH:mm:ss";
            //bool IsToKhaiNhap = BK.MaLoaiHinh.StartsWith("N");
            BK.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
            //bool IsHuyToKhai = (BK.ActionStatus == (short)ActionStatus.ToKhaiXinHuy);
            bool IsHuyToKhai = true;
            string issuer = string.Empty;
            issuer = DeclarationIssuer.GC_DENGHI_GIAMSAT_TIEU_HUY;

            DeclarationBase declaredCancel = new DeclarationBase()
            {
                Issuer = issuer,
                Reference = BK.GUIDSTR,
                IssueLocation = string.Empty,
                Agents = new List<Agent>(),

                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(BK.SoTiepNhan, 0),
                Acceptance = BK.NgayTiepNhan.ToString(sfmtDate),
                DeclarationOffice = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BK.MaHaiQuan.Trim()),
                Importer = new NameBase()
                {
                    Name = GlobalSettings.TEN_DON_VI,
                    Identity = BK.MaDoanhNghiep
                },
                AdditionalInformations = new List<AdditionalInformation>()
            };
            declaredCancel.Agents.Add(new Agent()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = BK.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            declaredCancel.AdditionalInformations.Add(
                new AdditionalInformation()
                {
                    Content = new Content() { Text = "Xin hủy bảng kê NPL cung ứng do sai số liệu" }
                }
                );
            if (IsHuyToKhai)
            {
                //List<HuyToKhai> cancelContent = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_BK_ID(BK.ID);
                //if (cancelContent.Count > 0)
                //{
                //    declaredCancel.AdditionalInformations[0].Content.Text = cancelContent[0].LyDoHuy;
                //    declaredCancel.Reference = guidHuyTK;
                //}
            }
            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = BK.MaDoanhNghiep
            },
            new NameBase()
            {
                Name = DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN),
                Identity = VNACCS_Mapper.GetCodeVNACCMaHaiQuan(BK.MaHaiQuan.Trim())
            },
            new SubjectBase()
            {
                Type = issuer,
                Function = declaredCancel.Function,
                Reference = declaredCancel.Reference //BK.GUIDSTR
            }, declaredCancel);
            return objSend;
        }
        #endregion

        private void txtSoThamChieuPLHD_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                SelectPhuKienForm pkForm = new SelectPhuKienForm();
                pkForm.HopDongID = HD.ID;

                if (pkForm.ShowDialog() == DialogResult.OK)
                {
                    if (pkForm.PhuKienDangKySelected != null)
                    {
                        txtSoThamChieuPLHD.Text = pkForm.PhuKienDangKySelected.SoPhuKien;
                        txtSoDangKyPLHD.Text = pkForm.PhuKienDangKySelected.SoTiepNhan.ToString();
                        dtNgayDangKyPLHD.Value = pkForm.PhuKienDangKySelected.NgayTiepNhan;
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH HÀNG HÓA KHAI BÁO TIÊU HỦY.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["MaHang"], ConditionOperator.Contains, txtMaHang.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void GiamSatThieuHuyGCSendForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Khai báo Đề nghị giám sát tiêu huỷ có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.save();
                }
            }
        }

    }
}