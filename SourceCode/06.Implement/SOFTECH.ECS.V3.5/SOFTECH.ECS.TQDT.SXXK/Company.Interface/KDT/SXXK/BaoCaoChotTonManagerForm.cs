﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.SXXK
{
    public partial class BaoCaoChotTonManagerForm : BaseForm
    {
        public BaoCaoChotTonManagerForm()
        {
            InitializeComponent();
        }

        private void BaoCaoChotTonManagerForm_Load(object sender, EventArgs e)
        {
            try
            {
                ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                ctrCoQuanHaiQuan.BackColor = Color.Transparent;
                cbStatus.SelectedValue = "-1";
                btnTimKiem_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private string GetSearchWhere()
        {
            try
            {
                string where = " 1=1";
                if (!String.IsNullOrEmpty(txtSoTiepNhan.Text))
                    where += " AND SoTiepNhan = " + txtSoTiepNhan.Text;
                if (!String.IsNullOrEmpty(txtNamTiepNhan.Text))
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Text;
                if (!String.IsNullOrEmpty(cbStatus.SelectedValue.ToString()))
                    where += " AND TrangThaiXuLy =" + cbStatus.SelectedValue;
                if (!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Code))
                    where += " AND MaHaiQuan = '" + ctrCoQuanHaiQuan.Code + "'";
                return where;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = KDT_VNACCS_TotalInventoryReport.SelectCollectionDynamic(GetSearchWhere(), "");
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH _" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel(dgList);
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                string TrangThai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (TrangThai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;
                    case "-3":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                        break;
                    case "10":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                        break;
                    case "4":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo Sửa";
                        break;
                    case "-2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt khai báo sửa";
                        break;
                    case "-4":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo Hủy";
                        break;
                    case "11":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt Khai báo hủy";
                        break;
                    case "5":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đang sửa";
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                KDT_VNACCS_TotalInventoryReport totalInventoryReport = new KDT_VNACCS_TotalInventoryReport();
                totalInventoryReport = (KDT_VNACCS_TotalInventoryReport)dgList.GetRow().DataRow;
                BaoCaoChotTonForm f = new BaoCaoChotTonForm();
                f.totalInventoryReport = totalInventoryReport;
                f.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void uiContextMenu1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdUpdateStatus":
                    UpdateStatus();
                    break;
            } 
        }
        private void UpdateStatus()
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        List<KDT_VNACCS_TotalInventoryReport> BCCTCollection = new List<KDT_VNACCS_TotalInventoryReport>();
                        foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                        {
                            BCCTCollection.Add((KDT_VNACCS_TotalInventoryReport)grItem.GetRow().DataRow);
                        }
                        for (int i = 0; i < BCCTCollection.Count; i++)
                        {
                            Company.Interface.KDT.GC.UpdateStatusForm f = new Company.Interface.KDT.GC.UpdateStatusForm();
                            f.BCCT = BCCTCollection[i];
                            f.formType = "BCCT";
                            f.ShowDialog(this);
                        }
                        this.btnTimKiem_Click(null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (ShowMessageTQDT("DOANH NGHIỆP CÓ MUỐN XOÁ BÁO CÁO CHỐT TỒN NÀY KHÔNG ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            KDT_VNACCS_TotalInventoryReport goodItem = new KDT_VNACCS_TotalInventoryReport();
                            goodItem = (KDT_VNACCS_TotalInventoryReport)i.GetRow().DataRow;
                            goodItem.DetailCollection = KDT_VNACCS_TotalInventoryReport_Detail.SelectCollectionBy_TotalInventory_ID(goodItem.ID);
                            if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA || goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                ShowMessage("Báo cáo chốt tồn này đã gửi lên HQ nên không được xóa !", false);
                            }
                            else
                            {
                                goodItem.DeleteFull();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    //BindData();
                    btnTimKiem_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
