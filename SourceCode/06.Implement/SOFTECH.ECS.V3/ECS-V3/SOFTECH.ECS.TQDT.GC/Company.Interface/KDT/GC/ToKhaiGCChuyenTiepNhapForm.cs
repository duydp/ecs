﻿using System;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components;
using Infragistics.Excel;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.GC.BLL.KDT.SXXK;
using System.Data;




namespace Company.Interface.KDT.GC
{
    public partial class ToKhaiGCChuyenTiepNhapForm : BaseForm
    {
        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep TKCT = new Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep() { TrangThaiXuLy = -1 };
        //ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        public Company.GC.BLL.KDT.GC.HopDong HDGC = new Company.GC.BLL.KDT.GC.HopDong();
        public Company.GC.BLL.KDT.GC.HopDong HDGCDoiXung = new Company.GC.BLL.KDT.GC.HopDong();
        public bool isByHand = false;//Nhập trực tiếp bằng tay. LanNT
        string msgInfor = string.Empty;
        FeedBackContent feedbackContent = null;
        public string NhomLoaiHinh = "N";
        //public bool boolFlag = false ;
        //public bool boolFlagEdit = false;
        public bool ismoreGoods = false;
        private string xmlCurrent = "";
        private string LoaiHinhHangHoa = "";
        public ToKhaiGCChuyenTiepNhapForm()
        {
            InitializeComponent();
            CreateCommandBosung();
            Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
        }

        private void KhoitaoDuLieuChuan()
        {
            txtDaiDienDN.Text = GlobalSettings.NguoiLienHe;
            txtChucVu.Text = GlobalSettings.ChucVu;
            #region Phương thức thanh toán
            cbPTTT.DataSource = PhuongThucThanhToan.SelectAll().Tables[0];
            cbPTTT.DisplayMember = cbPTTT.ValueMember = "ID";
            cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;
            #endregion
            ctrCuaKhau.Ma = NhomLoaiHinh.StartsWith("N") ? GlobalSettings.DIA_DIEM_DO_HANG : GlobalSettings.CUA_KHAU;
            #region Điều kiện giao hàng
            cbDKGH.DataSource = DieuKienGiaoHang.SelectAll().Tables[0];
            cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
            #endregion

            radTB.CheckedChanged -= radSP_CheckedChanged;
            radNPL.CheckedChanged -= radSP_CheckedChanged;
            radSP.CheckedChanged -= radSP_CheckedChanged;
            radHangMau.CheckedChanged -= radSP_CheckedChanged;

            ccNgayHopDongNhan.ReadOnly = ccNgayHHHopDongNhan.ReadOnly = true;
            ccNgayHopDongGiao.ReadOnly = ccNgayHHHopDongGiao.ReadOnly = false;
            txtSoHopDongNhan.ReadOnly = true;
            txtSoHongDonggiao.ReadOnly = false;
            if (isByHand)
            {
                txtsotiepnhan.ReadOnly = false;
            }
        }
        private void radSP_CheckedChanged(object sender, EventArgs e)
        {
            //  if (ShowMessage("Bây giờ sẽ xóa các hàng trước đây và thêm mới\r\nBạn có đồng ý không?", true) != "Yes") return;
            bool isDelete = TKCT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO
                || TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_HUY ||
                TKCT.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET;
            if (!isDelete) return;
            try
            {
                foreach (HangChuyenTiep hct in TKCT.HCTCollection)
                {
                    hct.Delete();
                }
            }
            catch { }
            TKCT.HCTCollection.Clear();
            dgList.DataSource = TKCT.HCTCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }
        private void KhoiTaoGiaoDien()
        {


            if (!string.IsNullOrEmpty(this.TKCT.MaLoaiHinh))
            {
                if (TKCT.MaLoaiHinh.Contains("PH"))
                    this.NhomLoaiHinh = this.TKCT.MaLoaiHinh.Trim().Substring(this.TKCT.MaLoaiHinh.Trim().Length - 1, 1);
                else
                    this.NhomLoaiHinh = this.TKCT.MaLoaiHinh.Trim().Substring(0, 1);
            }
            if (this.NhomLoaiHinh == "N")
            {
                this.Text += " " + setText(" nhập", "");
            }
            else
            {
                pnlToKhaiMauDich.BackColor = System.Drawing.Color.FromArgb(255, 228, 225);
                this.Text += " " + setText(" xuất", "");

                grbNguoiXK.Text = "Người nhập khẩu";
                grbNguoiNK.Text = "Người xuất khẩu";

                grbNuocXK.Text = "Nước nhập khẩu";

                radHangMau.Visible = false;

                grbHopDongGiao.Text = "Hợp đồng nhận";
                grbHopDongNhan.Text = "Hợp đồng giao";

            }
            cbLoaiHinh.Nhom = NhomLoaiHinh;
            if (this.OpenType == OpenFormType.Insert)
            {
                cbLoaiHinh.cbTen.SelectedIndex = 0;
            }
            switch (TKCT.LoaiHangHoa)
            {
                case "N":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    break;
                case "S":
                    if (TKCT.MaLoaiHinh.EndsWith("X"))
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                    }
                    else
                    {
                        dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    }
                    break;
                case "T":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
                    break;
                case "H":
                    dgList.RootTable.Columns["SoLuong"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongSP;
                    break;
            }

        }
        private void txtSoHopDongNhan_ButtonClick(object sender, EventArgs e)
        {
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = false;
            f.ShowDialog();
            if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
            {
                if (this.TKCT.HCTCollection != null && this.TKCT.HCTCollection.Count > 0)
                {
                    if (showMsg("MSG_SAV05", true) == "Yes")
                    //if (MLMessages("Chương trình sẽ xóa tất cả các hàng đã nhập của hợp đồng trước đó.Bạn có muốn thay đổi hợp đồng không ?", "MSG_SAV05", "", true) == "Yes")
                    {
                        string LoaiHangHoa = TKCT.LoaiHangHoa;

                        foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                        {
                            if (HCT.ID > 0)
                                HCT.Delete(TKCT.IDHopDong, LoaiHangHoa, TKCT.MaLoaiHinh);
                        }
                        TKCT.HCTCollection.Clear();
                        try
                        {
                            dgList.DataSource = this.TKCT.HCTCollection;
                            dgList.Refetch();
                        }
                        catch
                        {
                            dgList.Refresh();
                        }
                    }
                    else
                        return;
                }
                this.HDGC = f.HopDongSelected;
                txtSoHopDongNhan.Text = f.HopDongSelected.SoHopDong;
                ccNgayHopDongNhan.Text = f.HopDongSelected.NgayKy.ToLongDateString();
                ccNgayHHHopDongNhan.Text = f.HopDongSelected.NgayHetHan.ToLongDateString();
                this.TKCT.IDHopDong = this.HDGC.ID;

            }

        }
        private void Set()
        {
            #region Thông tin khai báo
            switch (this.TKCT.TrangThaiXuLy)
            {
                case -1:
                    lbltrangthai.Text = "Chưa gửi đến Hải quan";
                    if (GlobalSettings.NGON_NGU == "1")
                    {
                        lbltrangthai.Text = "Not declared yet";
                    }
                    break;
                case 0:
                    lbltrangthai.Text = "Chờ duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "1")
                    {
                        lbltrangthai.Text = "Wait for approval";
                    }
                    break;
                case 1:
                    lbltrangthai.Text = "Đã duyệt chính thức";
                    if (GlobalSettings.NGON_NGU == "1")
                    {
                        lbltrangthai.Text = "Approved";
                    }
                    break;
            }
            if (this.TKCT.SoTiepNhan > 0)
                txtsotiepnhan.Value = this.TKCT.SoTiepNhan;
            else txtsotiepnhan.Value = string.Empty;
            if (this.TKCT.SoToKhai > 0)
                txtToKhaiSo.Value = this.TKCT.SoToKhai;
            else txtToKhaiSo.Value = string.Empty;

            if (TKCT.NgayDangKy.Year > 1900)
                ccNgayDangKy.Text = this.TKCT.NgayDangKy.ToShortDateString();
            #endregion

            cbLoaiHinh.Ma = !string.IsNullOrEmpty(this.TKCT.MaLoaiHinh) ? this.TKCT.MaLoaiHinh.Trim() : NhomLoaiHinh + "GC18";
            #region Người nhập khẩu
            txtMaDonVi.Text = this.TKCT.MaDoanhNghiep;
            txtTenDonVi.Text = GlobalSettings.TEN_DON_VI;
            #endregion

            txtTenDonViDoiTac.Text = TKCT.TenDonViDoiTac;
            txtMaDonViDoiTac.Text = TKCT.MaKhachHang;

            #region Thông tin đại lý và đơn vị ủy thác
            if (TKCT.ID == 0)
            {
                //txtMaDaiLy.Text = GlobalSettings.MA_DON_VI;
                //txtTenDaiLy.Text = GlobalSettings.TEN_DON_VI;

                //txtMaDonViUyThac.Text = GlobalSettings.MA_DON_VI;
                //txtTenDonViUyThac.Text = GlobalSettings.TEN_DON_VI;

                txtMaDaiLy.Text = this.TKCT.MaDaiLy;
                txtTenDaiLy.Text = this.TKCT.TenDaiLyTTHQ;

                txtMaDonViUyThac.Text = this.TKCT.MaDonViUT;
                txtTenDonViUyThac.Text = this.TKCT.TenDonViUT;
            }
            else
            {

                txtMaDaiLy.Text = this.TKCT.MaDaiLy;
                txtTenDaiLy.Text = this.TKCT.TenDaiLyTTHQ;

                txtMaDonViUyThac.Text = this.TKCT.MaDonViUT;
                txtTenDonViUyThac.Text = this.TKCT.TenDonViUT;
            }
            #endregion

            #region Đại diện DN
            txtDaiDienDN.Text = TKCT.DaiDienDoanhNghiep;
            txtChucVu.Text = TKCT.CanBoDangKy;
            #endregion

            #region Đồng tiền thanh toán
            ctrNguyenTe.Ma = TKCT.NguyenTe_ID;
            txtTyGiaTinhThue.Value = TKCT.TyGiaVND;
            txtTyGiaUSD.Value = TKCT.TyGiaUSD;
            #endregion
            if (NhomLoaiHinh == "N")
                ctrNuocXuatKhau.Ma = TKCT.NuocXK_ID;
            else ctrNuocXuatKhau.Ma = TKCT.NuocNK_ID;

            cbDKGH.SelectedValue = TKCT.DKGH_ID;
            cbPTTT.SelectedValue = TKCT.PTTT_ID;

            #region Hợp đồng nhận
            txtSoHopDongNhan.Text = TKCT.SoHopDongDV;
            if (TKCT.NgayHDDV.Year > 1900)
                ccNgayHopDongNhan.Value = TKCT.NgayHDDV;

            if (TKCT.NgayHetHanHDDV.Year > 1900)
                ccNgayHHHopDongNhan.Value = TKCT.NgayHetHanHDDV;
            #endregion

            #region Hợp đồng giao
            txtSoHongDonggiao.Text = TKCT.SoHDKH;
            if (TKCT.NgayHDKH.Year > 1900)
                ccNgayHopDongGiao.Value = TKCT.NgayHDKH;
            if (TKCT.NgayHetHanHDKH.Year > 1900)
                ccNgayHHHopDongGiao.Value = TKCT.NgayHetHanHDKH;
            #endregion

            ctrCuaKhau.Ma = TKCT.CuaKhau_ID;

            #region Trọng lượng
            txtTrongLuong.Value = TKCT.TrongLuong;
            txtTrongLuongTinh.Value = TKCT.TrongLuongNet;
            #endregion

            txtMaNguoiGiao.Text = TKCT.NguoiChiDinhDV;
            txtTenNguoiGiao.Text = TKCT.NguoiChiDinhKH;
            txtDiaDiemGiaohang.Text = TKCT.DiaDiemXepHang;
            ccThoiGianGiaoHang.Value = TKCT.ThoiGianGiaoHang;
            txtSoKien.Value = TKCT.SoKien;
            if (TKCT.ID == 0)
            {
                //if (cbLoaiHinh.Ma.Substring(0, 1) == "N")
                if (cbLoaiHinh.Ma.IndexOf("PL") > 0 || cbLoaiHinh.Ma.IndexOf("18") > 0)
                    TKCT.LoaiHangHoa = "N";
                else if (cbLoaiHinh.Ma.IndexOf("SP") > 0 || cbLoaiHinh.Ma.IndexOf("19") > 0)
                    TKCT.LoaiHangHoa = "S";
                else if (cbLoaiHinh.Ma.IndexOf("TB") > 0 || cbLoaiHinh.Ma.IndexOf("20") > 0)
                    TKCT.LoaiHangHoa = "T";
            }

            if (TKCT.LoaiHangHoa == "S")
            {
                radSP.Checked = true;
                radNPL.Checked = false;
                radTB.Checked = false;
                radHangMau.Checked = false;
            }
            else if (TKCT.LoaiHangHoa == "N")
            {
                radSP.Checked = false;
                radNPL.Checked = true;
                radTB.Checked = false;
                radHangMau.Checked = false;
            }
            else if (TKCT.LoaiHangHoa == "T")
            {
                radSP.Checked = false;
                radNPL.Checked = false;
                radTB.Checked = true;
                radHangMau.Checked = false;
            }
            else if (TKCT.LoaiHangHoa == "H")
            {
                radSP.Checked = false;
                radNPL.Checked = false;
                radTB.Checked = false;
                radHangMau.Checked = true;
            }

            #region Load Default
            if (TKCT.ID == 0)
            {
                txtMaDonVi.Text = GlobalSettings.MA_DON_VI;
                txtTenDonVi.Text = GlobalSettings.TEN_DON_VI;
                txtTenDonViDoiTac.Text = GlobalSettings.TEN_DOI_TAC;
                txtTyGiaUSD.Value = GlobalSettings.TY_GIA_USD;
                ctrCuaKhau.Ma = GlobalSettings.CUA_KHAU;
                ctrNuocXuatKhau.Ma = GlobalSettings.NUOC;
                cbPTTT.SelectedValue = GlobalSettings.PTTT_MAC_DINH;
                cbDKGH.SelectedValue = GlobalSettings.DKGH_MAC_DINH;
                ctrNguyenTe.Ma = GlobalSettings.NGUYEN_TE_MAC_DINH;
                txtDaiDienDN.Text = GlobalSettings.NguoiLienHe;
                txtChucVu.Text = GlobalSettings.ChucVu;
            }
            else if (TKCT.HCTCollection != null && TKCT.HCTCollection.Count == 0)
                this.TKCT.LoadHCTCollection();
            dgList.DataSource = this.TKCT.HCTCollection;
            #endregion

            #region Event controls

            radTB.CheckedChanged += radSP_CheckedChanged;
            radNPL.CheckedChanged += radSP_CheckedChanged;
            radSP.CheckedChanged += radSP_CheckedChanged;
            radHangMau.CheckedChanged += radSP_CheckedChanged;

            if (TKCT.ThoiGianGiaoHang.Year > 1900) ccThoiGianGiaoHang.Value = TKCT.ThoiGianGiaoHang;
            else ccThoiGianGiaoHang.Value = DateTime.Today;
            
            #endregion 

            #region Đề xuất khác
            if (!string.IsNullOrEmpty(TKCT.DeXuatKhac))
                txtDeXuatKhac.Text = TKCT.DeXuatKhac; 
            #endregion

            if (TKCT != null && !string.IsNullOrEmpty(TKCT.LyDoSua))
                txtLyDoSua.Text = TKCT.LyDoSua;
        }
        private void Get()
        {

            TKCT.MaLoaiHinh = cbLoaiHinh.Ma;
            #region Người nhập khẩu
            this.TKCT.MaDoanhNghiep = txtMaDonVi.Text;
            #endregion

            TKCT.TenDonViDoiTac = txtTenDonViDoiTac.Text;
            TKCT.MaKhachHang = txtMaDonViDoiTac.Text;


            #region Đơn vị ủy thác
            TKCT.MaDonViUT = txtMaDonViUyThac.Text;
            this.TKCT.TenDonViUT = txtTenDonViUyThac.Text;
            #endregion

            #region Đại lý làm tt hải quan
            TKCT.TenDaiLyTTHQ = txtTenDaiLy.Text;
            TKCT.MaDaiLy = txtMaDaiLy.Text;
            #endregion

            #region Đại diện doanh nghiệp

            TKCT.DaiDienDoanhNghiep = txtDaiDienDN.Text;
            TKCT.CanBoDangKy = txtChucVu.Text;

            #endregion

            #region Đồng tiền thanh toán
            TKCT.NguyenTe_ID = ctrNguyenTe.Ma;
            TKCT.TyGiaVND = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            TKCT.TyGiaUSD = Convert.ToDecimal(txtTyGiaUSD.Value);
            #endregion

            if (NhomLoaiHinh == "N")
                TKCT.NuocXK_ID = ctrNuocXuatKhau.Ma;
            else TKCT.NuocNK_ID = ctrNuocXuatKhau.Ma;

            TKCT.DKGH_ID = cbDKGH.SelectedValue.ToString();
            TKCT.PTTT_ID = cbPTTT.SelectedValue.ToString();

            #region Hợp đồng nhận
            TKCT.SoHopDongDV = txtSoHopDongNhan.Text;
            TKCT.NgayHDDV = ccNgayHopDongNhan.Value;
            TKCT.NgayHetHanHDDV = ccNgayHHHopDongNhan.Value;
            #endregion

            #region Hợp đồng giao
            TKCT.SoHDKH = txtSoHongDonggiao.Text;
            TKCT.NgayHDKH = ccNgayHopDongGiao.Value;
            TKCT.NgayHetHanHDKH = ccNgayHHHopDongGiao.Value;
            #endregion

            TKCT.CuaKhau_ID = ctrCuaKhau.Ma;

            #region Trọng lượng
            TKCT.TrongLuong = Convert.ToDecimal(txtTrongLuong.Value);
            TKCT.TrongLuongNet = Convert.ToDouble(txtTrongLuongTinh.Value);
            #endregion

            TKCT.NguoiChiDinhKH = txtTenNguoiGiao.Text;
            TKCT.NguoiChiDinhDV = txtMaNguoiGiao.Text;
            TKCT.DiaDiemXepHang = txtDiaDiemGiaohang.Text;
            TKCT.ThoiGianGiaoHang = ccThoiGianGiaoHang.Value;
            TKCT.SoKien = Convert.ToDecimal(txtSoKien.Value);

            if (radSP.Checked) TKCT.LoaiHangHoa = "S";
            else if (radHangMau.Checked) TKCT.LoaiHangHoa = "H";
            else if (radNPL.Checked) TKCT.LoaiHangHoa = "N";
            else if (radTB.Checked) TKCT.LoaiHangHoa = "T";

            if (isByHand)//Nếu nhập từ hợp đồng đã duyệt
            {
                TKCT.SoTiepNhan = Convert.ToInt32(txtsotiepnhan.Value);
                TKCT.NgayDangKy = ccNgayDangKy.Value;
                TKCT.SoToKhai = Convert.ToInt32(txtToKhaiSo.Value);
                TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;

            }
            #region Đề xuất khác
            TKCT.DeXuatKhac = txtDeXuatKhac.Text;
            #endregion

            TKCT.LyDoSua = txtLyDoSua.Text;
        }
        private void ToKhaiGCChuyenTiepNhapForm_Load(object sender, EventArgs e)
        {
            //An nut Xac nhan
            XacNhan.Visible = Janus.Windows.UI.InheritableBoolean.False;

            MenuEnable();
            try
            {
                KhoitaoDuLieuChuan();
                KhoiTaoGiaoDien();
                //if (NhomLoaiHinh.StartsWith("N"))
                //{
                HDGC.ID = TKCT.IDHopDong;
                if (HDGC.ID > 0)
                {
                    HDGC = HopDong.Load(HDGC.ID);
                    TKCT.SoHopDongDV = HDGC.SoHopDong;
                    TKCT.NgayHDDV = HDGC.NgayKy;
                    TKCT.NgayHetHanHDDV = HDGC.NgayHetHan;
                }
                //}
                //else
                //{
                //    HDGCDoiXung.ID = TKCT.ID_Relation;
                //    if (HDGCDoiXung.ID > 0)
                //    {
                //        HDGCDoiXung = HopDong.Load(HDGC.ID);
                //        TKCT.SoHDKH = HDGCDoiXung.SoHopDong;
                //        TKCT.NgayHDKH = HDGCDoiXung.NgayKy;
                //        TKCT.NgayHetHanHDKH = HDGCDoiXung.NgayHetHan;
                //    }
                //}
                this.Set();
                switch (this.OpenType)
                {
                    case OpenFormType.Edit:
                        break;
                    case OpenFormType.View:
                        // this.ViewData(true);
                        break;
                    case OpenFormType.Insert:
                        txtTyGiaUSD.Text = Company.KDT.SHARE.Components.DuLieuChuan.NguyenTe.GetTyGia("USD").ToString();
                        break;
                    default:
                        break;
                }
                string MaLoaiHinh = cbLoaiHinh.Ma.Trim();

                setCommandStatus();
                LoaiHinhHangHoa = cbLoaiHinh.Ma;


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }
        }

        private void ViewData(bool isView)
        {
            cmdThemHang.Enabled = isView == true ? Janus.Windows.UI.InheritableBoolean.False : Janus.Windows.UI.InheritableBoolean.True;
            cmdSave.Enabled = isView == true ? Janus.Windows.UI.InheritableBoolean.False : Janus.Windows.UI.InheritableBoolean.True;
            cmdSend.Enabled = isView == true ? Janus.Windows.UI.InheritableBoolean.False : Janus.Windows.UI.InheritableBoolean.True;
            txtToKhaiSo.ReadOnly = isView;
            txtDaiDienDN.ReadOnly = isView;
            txtChucVu.ReadOnly = isView;
            ccNgayDangKy.ReadOnly = isView;
            //txtMaDonVinhan.ReadOnly = isView;
            //txtTenDonVinhan.ReadOnly = isView;
            //txtSoHopDongNhan.ReadOnly = isView;
            txtSoHopDongNhan.ButtonStyle = EditButtonStyle.NoButton;
            //ccNgayHopDongNhan.ReadOnly = isView;
            //ccNgayHHHopDongNhan.ReadOnly = isView;
            //txtMaDVGiao.ReadOnly = isView;
            //txtTenDVGiao.ReadOnly = isView;
            //Update by Khanh 11-07-2012
            if (TKCT.MaLoaiHinh.Substring(0, 1).ToUpper() == "N" || TKCT.MaLoaiHinh.EndsWith("N"))
            {
                txtSoHopDongNhan.ReadOnly = isView;
                ccNgayHopDongNhan.ReadOnly = isView;
                ccNgayHHHopDongNhan.ReadOnly = isView;
            }
            else
            {
                txtSoHongDonggiao.ReadOnly = isView;
                ccNgayHopDongGiao.ReadOnly = isView;
                ccNgayHHHopDongGiao.ReadOnly = isView;
            }
            
            //txtNguoiChiDinhKH.ReadOnly = isView;
            txtMaDonViUyThac.ReadOnly = isView;
            cbLoaiHinh.ReadOnly = isView;
            //txtTyGiaTinhThue.ReadOnly = isView;
            txtTyGiaUSD.ReadOnly = isView;
            //txtLePhiHQ.ReadOnly = isView;
            //txtSoBienLai.ReadOnly = isView;
            //ccNgayBienLai.ReadOnly = isView;
            //txtChungTu.ReadOnly = isView;
            //txtDiaDiemGiaohang.ReadOnly = isView;
            //nguyenTeControl1.ReadOnly = isView;
            //txtNguoiChiDinhDV.ReadOnly = isView;
            //txtNguoiChiDinhKH.ReadOnly = isView;
        }

        private void MenuEnable()
        {
            //if (TKCT.SoTiepNhan == 0)
            //{
            //    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //}
            //else if (TKCT.SoTiepNhan != 0)
            //{
            //    XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

            //    if (TKCT.SoToKhai != 0)
            //    {

            //        Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //        cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //        cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    }

            //}
        }
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                switch (e.Command.Key)
                {
                    case "cmdSave":
                        this.Save();
                        break;
                    case "cmdThemHang":
                        this.Add();
                        break;
                    case "cmdThemMotHang":
                        this.Add();
                        break;
                    case "cmdThemNhieuHang":
                        this.AddManyGoods();
                        break;
                    case "ToKhaiViet":
                        this.Print();
                        break;
                    case "InPhieuTN":
                        this.inPhieuTN();
                        break;
                    case "cmdSend":
                        SendV3();
                        checkTrangThaiXuLyTK();
                        break;
                    case "XacNhan":
                        FeedBackV3();
                        break;
                    case "NhanDuLieu":
                        FeedBackV3();
                        checkTrangThaiXuLyTK();
                        break;
                    case "Huy":
                        CancelV3();
                        break;
                    case "cmdHuyToKhaiDaDuyet":
                        ShowHuyToKhaiDaDuyet();
                        break;
                    case "cmdReadExcel":
                        this.ReadExcel();
                        break;
                    case "cmdChungTuKem":
                    case "cmdGiayPhep":
                    case "cmdHopDongThuongMai":
                    case "cmdHoaDonThuongMai":
                    case "cmdDeNghiChuyenCuaKhau":
                    case "cmdChungTuDangAnh":
                        ChungTuDinhKem(e.Command.Key);
                        break;
                    case "cmdKetQuaXuLy":
                        XemKetQuaXuLy();
                        break;
                    case "cmdSuaToKhaiDaDuyet":
                        SuaToKhaiDaDuyet();
                        break;
                    case "cmdInTKDTSuaDoiBoSung":
                        InToKhaiDienTuSuaDoiBoSung();
                        break;
                    case "cmdChungTuNo":
                        AddChungTuNo();
                        break;
                    case "cmdNPLCungUng":
                        AddNPLCungUng();
                        break;
                    case "cmdIntokhaiTQ":
                        this.PrintGCCTTQ();
                        break;
                    case "cmdInToKhai196":
                        this.InToKhaiGCCT_196();
                        break;
                    default:
                        break;

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }
            //MenuEnable();
        }
        private void ShowHuyToKhaiDaDuyet()
        {

            string msg = "Bạn có muốn hủy tờ khai đã duyệt này không?";
            msg += "\n\nSố tờ khai: " + TKCT.SoToKhai.ToString();
            List<HuyToKhaiCT> items = HuyToKhaiCT.SelectCollectionDynamic(string.Format("TKCT_ID={0}", TKCT.ID), "");
            if (items.Count == 0 && MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
            {

                HuyToKhaiCTForm f = new HuyToKhaiCTForm();
                f.TKCT = TKCT;
                f.ShowDialog();

            }
            else
            {
                HuyToKhaiCTForm f = new HuyToKhaiCTForm();
                f.TKCT = TKCT;
                f.ShowDialog();
            }
        }
        private void checkTrangThaiXuLyTK()
        {
            NoiDungChinhSuaTKCTForm noiDungDieuChinhTKForm = new NoiDungChinhSuaTKCTForm();
            string where = "TKMD_ID = " + TKCT.ID + " AND MALOAIHINH = '" + TKCT.MaLoaiHinh + "'";
            noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
            //Nếu là tờ khai sửa đã được duyệt
            if (TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                        ndDieuChinhTK.Update();
                    }
                }
            }
            //Nếu là tờ khai sửa đang chờ duyệt
            else if (TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET)
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHO_DUYET;
                        ndDieuChinhTK.Update();
                    }
                }
            }
            //Nếu là tờ khai sửa mới
            else
            {
                foreach (NoiDungDieuChinhTK ndDieuChinhTK in noiDungDieuChinhTKForm.ListNoiDungDieuChinhTK)
                {
                    if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                    {
                        ndDieuChinhTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                        ndDieuChinhTK.Update();
                    }
                }
            }
        }
        //datlmq update InToKhaiDienTuSuaDoiBoSung 30/08/2010
        private void InToKhaiDienTuSuaDoiBoSung()
        {
            checkTrangThaiXuLyTK();
            bool ok = true;
            NoiDungChinhSuaTKCTForm ndDieuChinhTKForm = new NoiDungChinhSuaTKCTForm();
            string where = "TKMD_ID = " + TKCT.ID + " AND MALOAIHINH = '" + TKCT.MaLoaiHinh + "'";
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                if (ndDieuChinhTK.TrangThai != Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET)
                {
                    ShowMessage("Thông tin sửa đổi, bổ sung chưa được duyệt. Không thể in tờ khai sửa đổi bổ sung", false);
                    ok = false;
                }
            }
            if (ok)
            {
                //Lấy đường dẫn file gốc
                string destFile = "";
                string fileName = "Mau_so_8._Mau_To_khai_sua_doi_bo_sung.xls";
                string sourcePath = Company.KDT.SHARE.QuanLyChungTu.BaseClass.GetPathProgram() + "\\ExcelTemplate\\" + fileName;

                //Đọc nội dung file gốc, rồi lấy thông tin tờ khai điền vào các ô còn trống
                try
                {
                    Workbook workBook = Workbook.Load(sourcePath);
                    Worksheet workSheet = workBook.Worksheets[0];

                    workSheet.Rows[5].Cells[6].Value = GlobalSettings.TEN_HAI_QUAN;
                    workSheet.Rows[5].Cells[6].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[5].Cells[6].CellFormat.Font.Height = 10 * 20;

                    if (TKCT.MaLoaiHinh.Contains("N"))
                    {
                        workSheet.Rows[7].Cells[6].Value = GlobalSettings.TEN_DON_VI;
                        workSheet.Rows[7].Cells[6].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[7].Cells[6].CellFormat.Font.Height = 10 * 20;
                    }
                    else
                    {
                        workSheet.Rows[7].Cells[6].Value = TKCT.TenKH;
                        workSheet.Rows[7].Cells[6].CellFormat.Font.Name = "Times New Roman";
                        workSheet.Rows[7].Cells[6].CellFormat.Font.Height = 10 * 20;
                    }

                    workSheet.Rows[9].Cells[3].Value = TKCT.SoToKhai;
                    workSheet.Rows[9].Cells[3].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[9].Cells[3].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[9].Cells[9].Value = TKCT.MaLoaiHinh;
                    workSheet.Rows[9].Cells[9].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[9].Cells[9].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[10].Cells[4].Value = TKCT.NgayTiepNhan;
                    workSheet.Rows[10].Cells[4].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[10].Cells[4].CellFormat.Font.Height = 10 * 20;

                    workSheet.Rows[9].Cells[15].Value = NoiDungDieuChinhTK.SelectMaxSoDieuChinh(TKCT.ID, TKCT.MaLoaiHinh);
                    workSheet.Rows[9].Cells[15].CellFormat.Font.Name = "Times New Roman";
                    workSheet.Rows[9].Cells[15].CellFormat.Font.Height = 10 * 20;

                    //Gán giá trị tương ứng vào các ô trong bảng nội dung sửa đổi
                    fillNoiDungTKChinh(workSheet, 13, 1);
                    fillNoiDungTKSua(workSheet, 13, 1);

                    //Chọn đường dẫn file cần lưu
                    SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                    saveFileDialog1.Filter = "Excel Files (*.xls)|*.xls";
                    string dateNow = DateTime.Now.ToShortDateString().Replace("/", "");
                    saveFileDialog1.FileName = "ToKhaiSuaBoSung" + "_TK" + TKCT.SoToKhai + "_NamDK" + TKCT.NamDK + "_" + dateNow;
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        destFile = saveFileDialog1.FileName;
                        //Ghi nội dung file gốc vào file cần lưu
                        try
                        {
                            byte[] sourceFile = Globals.ReadFile(sourcePath);
                            Globals.WriteFile(destFile, sourceFile);
                            workBook.Save(destFile);
                            ShowMessage("Save file thành công", false);
                        }
                        catch (Exception ex)
                        {
                            ShowMessage(ex.Message, false);
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message, false);
                    return;
                }
            }
        }

        private void fillNoiDungTKChinh(Worksheet worksheet, int row, int column)
        {
            int j = 1;
            NoiDungChinhSuaTKCTForm ndDieuChinhTKForm = new NoiDungChinhSuaTKCTForm();
            string where = "TKMD_ID = " + TKCT.ID + " AND MALOAIHINH = '" + TKCT.MaLoaiHinh + "'";
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                NoiDungChinhSuaTKCTDetailForm ndDieuChinhTKDetailForm = new NoiDungChinhSuaTKCTDetailForm();
                ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(ndDieuChinhTK.ID);
                foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail)
                {
                    if (row >= 13 && row <= 31)
                    {
                        worksheet.Rows[row].Cells[column].Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                        int thuong1 = worksheet.Rows[row].Cells[column].Value.ToString().Length / 54;
                        int sodu1 = worksheet.Rows[row].Cells[column].Value.ToString().Length % 54;
                        if (thuong1 == 0)
                        {
                            Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row, column + 10);
                            mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                            worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                            mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                            mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                            //j = thuong1;
                        }
                        else
                        {
                            if (sodu1 == 0)
                            {
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                                worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                            else
                            {
                                thuong1 += 1;
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column, row + thuong1 - 1, column + 10);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKChinh;
                                worksheet.Rows[row].Cells[column].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                        }

                        if (thuong1 == 0)
                        {
                            row += 1;
                            j++;
                        }
                        else
                        {
                            row += thuong1;
                            j++;
                        }
                    }
                }
                //}
            }
        }

        private void fillNoiDungTKSua(Worksheet worksheet, int row, int column)
        {
            int j = 1;
            NoiDungChinhSuaTKCTForm ndDieuChinhTKForm = new NoiDungChinhSuaTKCTForm();
            string where = "TKMD_ID = " + TKCT.ID + " AND MALOAIHINH = '" + TKCT.MaLoaiHinh + "'";
            ndDieuChinhTKForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
            foreach (NoiDungDieuChinhTK ndDieuChinhTK in ndDieuChinhTKForm.ListNoiDungDieuChinhTK)
            {
                //if (ndDieuChinhTK.TrangThai == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
                //{
                NoiDungChinhSuaTKCTDetailForm ndDieuChinhTKDetailForm = new NoiDungChinhSuaTKCTDetailForm();
                ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail = NoiDungDieuChinhTKDetail.SelectCollectionBy_Id_DieuChinh(ndDieuChinhTK.ID);
                foreach (NoiDungDieuChinhTKDetail ndDieuChinhTKChiTiet in ndDieuChinhTKDetailForm.ListNoiDungDieuChinhTKDetail)
                {
                    if (row >= 13 && row <= 31)
                    {
                        worksheet.Rows[row].Cells[column + 11].Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                        int thuong1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length / 54;
                        int sodu1 = worksheet.Rows[row].Cells[column + 11].Value.ToString().Length % 54;
                        if (thuong1 == 0)
                        {
                            Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row, column + 20);
                            mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                            worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                            mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                            mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                            //j = thuong1;
                        }
                        else
                        {
                            if (sodu1 == 0)
                            {
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                                worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                            else
                            {
                                thuong1 += 1;
                                Infragistics.Excel.WorksheetMergedCellsRegion mergedCellRegion1 = worksheet.MergedCellsRegions.Add(row, column + 11, row + thuong1 - 1, column + 20);
                                mergedCellRegion1.Value = j.ToString() + ". " + ndDieuChinhTKChiTiet.NoiDungTKSua;
                                worksheet.Rows[row].Cells[column + 11].CellFormat.WrapText = ExcelDefaultableBoolean.True;
                                mergedCellRegion1.CellFormat.Font.Name = "Times New Roman";
                                mergedCellRegion1.CellFormat.Font.Height = 10 * 20;
                                //j = thuong1;
                            }
                        }

                        if (thuong1 == 0)
                        {
                            row += 1;
                            j++;
                        }
                        else
                        {
                            row += thuong1;
                            j++;
                        }
                    }
                }
                //}
            }
        }
        private void SuaToKhaiDaDuyet()
        {
            string msg = "Bạn có muốn chuyển trạng thái của tờ khai đã duyệt sang tờ khai sửa không?";
            msg += "\n\nSố tờ khai: " + TKCT.SoToKhai.ToString();
            msg += "\n----------------------";
            //msg += "\nCó " + TKCT.HMDCollection.Count.ToString() + " sản phẩm đăng ký";
            if (MLMessages(msg, "MSG_CHA01", "", true) == "Yes")
            {
                long id = TKCT.ID;
                TKCT.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET;
                //Cap nhat trang thai cho to khai
                TKCT.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua;

                TKCT.Update();

                //Cap nhat trang thai sua to khai
                Company.KDT.SHARE.Components.KetQuaXuLy kqxl = new Company.KDT.SHARE.Components.KetQuaXuLy();
                kqxl.ItemID = TKCT.ID;
                kqxl.ReferenceID = new Guid(TKCT.GUIDSTR);
                kqxl.LoaiChungTu = Company.KDT.SHARE.Components.KetQuaXuLy.LoaiChungTu_ToKhai;
                kqxl.LoaiThongDiep = "Chuyển trạng thái sửa tờ khai"; // Company.KD.BLL.KDT.KetQuaXuLy.LoaiThongDiep_ToKhaiSuaDuocDuyet;
                kqxl.NoiDung = "Chuyển trạng thái sửa tờ khai chuyển tiếp";
                kqxl.Ngay = DateTime.Now;
                kqxl.Insert();
                setCommandStatus();
                this.ViewData(false);
            }
        }

        private void XemKetQuaXuLy()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = TKCT.ID;
            form.DeclarationIssuer = TKCT.MaLoaiHinh.Substring(0, 1).ToUpper() == "N" ? DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_NHAP : DeclarationIssuer.GC_TOKHAI_CHUYENTIEP_XUAT;
            form.ShowDialog(this);
        }

        private void ReadExcel()
        {
            if (txtSoHopDongNhan.Text.Trim() == "")
            {
                epError.SetError(txtSoHopDongNhan, setText("Số hợp đồng nhận không được rỗng.", "This VietNam must be filled"));
                epError.SetIconPadding(txtSoHopDongNhan, -8);
                return;
            }
            if (txtSoHongDonggiao.Text.Trim() == "")
            {
                epError.SetError(txtSoHongDonggiao, setText("Số hợp đồng giao không được rỗng.", "This VietNam must be filled"));
                epError.SetIconPadding(txtSoHongDonggiao, -8);
                return;
            }

            if (this.HDGC.SoHopDong == "")
            {
                this.HDGC.SoHopDong = txtSoHopDongNhan.Text;
                this.HDGC.NgayKy = ccNgayHopDongNhan.Value;
                //this.HDGC.MaDoanhNghiep = txtMaDonVinhan.Text;
                //this.HDGC.MaHaiQuan = GlobalSett1ings.MA_HAI_QUAN;
            }
            this.TKCT.MaLoaiHinh = cbLoaiHinh.Ma;
            ReadExcelHCTForm f = new ReadExcelHCTForm();
            f.TKCT = this.TKCT;
            f.ShowDialog();
            try
            {
                dgList.DataSource = TKCT.HCTCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }

        }

        private void NhanDuLieuTK()
        {
            this.Cursor = Cursors.Default;
            WSForm wsForm = new WSForm();
            string password = "";
            {
                try
                {
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                    LayPhanHoi(password);

                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    //if (loaiWS == "1")
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                //{
                                //    HangDoi hd = new HangDoi();
                                //    hd.ID = TKCT.ID;
                                //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                //    hd.TrangThai = TKCT.TrangThaiXuLy;
                                //    hd.ChucNang = ChucNang.KHAI_BAO;
                                //    hd.PassWord = password;
                                //    MainForm.AddToQueueForm(hd);
                                //    MainForm.ShowQueueForm();
                                //}
                                showMsg("MSG_WRN12");
                                //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                                return;
                            }
                            else
                            {
                                showMsg("MSG_2702016", msg[0]);
                                //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            }
                        }
                        else
                        {
                            if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                                showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                                showMsg("MSG_2702004", ex.Message);
                                //ShowMessage(ex.Message, false);
                            }
                            setCommandStatus();
                        }
                        #endregion FPTService
                    }

                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo danh sách Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();

                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }

            }
        }

        private void HuyKhaiBao()
        {
            this.Cursor = Cursors.Default;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "TKCT";
            sendXML.master_id = TKCT.ID;
            if (sendXML.Load())
            {
                //showMsg("MSG_STN01");
                //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_STN11", "", false);
                // return;
            }
            WSForm wsForm = new WSForm();
            string password = "";
            {
                try
                {
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                    this.Cursor = Cursors.WaitCursor;
                    //xmlCurrent = this.TKCT.WSCancel(password);
                    //xmlCurrent = SOFTECH.ECS.V3.Components.GC.SHARE.ProcessXML.HuyKhaiBao(password, sendXML.msg, this.TKCT);
                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = TKCT.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 3;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password);

                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    //if (loaiWS == "1")
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                //{
                                //    HangDoi hd = new HangDoi();
                                //    hd.ID = TKCT.ID;
                                //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                //    hd.TrangThai = TKCT.TrangThaiXuLy;
                                //    hd.ChucNang = ChucNang.KHAI_BAO;
                                //    hd.PassWord = password;
                                //    MainForm.AddToQueueForm(hd);
                                //    MainForm.ShowQueueForm();
                                //}
                                showMsg("MSG_WRN12");
                                //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                                return;
                            }
                            else
                            {
                                showMsg("MSG_2702016", msg[0]);
                                //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            }
                        }
                        else
                        {
                            if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                                showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                                showMsg("MSG_2702004", ex.Message);
                                //ShowMessage(ex.Message, false);
                            }
                            setCommandStatus();
                        }
                        #endregion FPTService
                    }

                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo danh sách Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();

                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }

            }
        }

        private void HuyToKhai()
        {
            this.Cursor = Cursors.Default;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "TKCT";
            sendXML.master_id = TKCT.ID;
            if (sendXML.Load())
            {
                //showMsg("MSG_STN01");
                //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thông hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_STN11", "", false);
                // return;
            }
            WSForm wsForm = new WSForm();
            string password = "";
            {
                try
                {
                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                    this.Cursor = Cursors.WaitCursor;
                    //xmlCurrent = this.TKCT.WSCancel(password);
                    //xmlCurrent = SOFTECH.ECS.V3.Components.GC.SHARE.ProcessXML.HuyToKhai(password, sendXML.msg, this.TKCT);

                    Company.GC.BLL.KDT.KetQuaXuLy kqxl = new Company.GC.BLL.KDT.KetQuaXuLy();
                    kqxl.ItemID = this.TKCT.ID;
                    kqxl.ReferenceID = new Guid(this.TKCT.GUIDSTR);
                    kqxl.LoaiChungTu = Company.GC.BLL.KDT.KetQuaXuLy.LoaiChungTu_ToKhai;
                    kqxl.LoaiThongDiep = Company.GC.BLL.KDT.KetQuaXuLy.LoaiThongDiep_KhaiBaoHuyTK_DuocDuyet;
                    kqxl.NoiDung = string.Format("Khai báo hủy tờ khai");
                    kqxl.Ngay = DateTime.Now;
                    kqxl.Insert();

                    TKCT.ActionStatus = (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiXinHuy;
                    TKCT.Update();

                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = TKCT.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 3;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();
                    LayPhanHoi(password);

                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    //if (loaiWS == "1")
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                //{
                                //    HangDoi hd = new HangDoi();
                                //    hd.ID = TKCT.ID;
                                //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                //    hd.TrangThai = TKCT.TrangThaiXuLy;
                                //    hd.ChucNang = ChucNang.KHAI_BAO;
                                //    hd.PassWord = password;
                                //    MainForm.AddToQueueForm(hd);
                                //    MainForm.ShowQueueForm();
                                //}
                                showMsg("MSG_WRN12");
                                //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                                return;
                            }
                            else
                            {
                                showMsg("MSG_2702016", msg[0]);
                                //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            }
                        }
                        else
                        {
                            if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                                showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                                showMsg("MSG_2702004", ex.Message);
                                //ShowMessage(ex.Message, false);
                            }
                            setCommandStatus();
                        }
                        #endregion FPTService
                    }

                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo danh sách Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();

                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }

            }
        }

        private void Send()
        {

            this.Cursor = Cursors.Default;
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            sendXML.LoaiHS = "TKCT";
            sendXML.master_id = TKCT.ID;
            if (sendXML.Load())
            {
                if (TKCT.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET)
                {
                    showMsg("MSG_STN01");
                    //MLMessages("Danh sách đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.\nHãy chọn chức năng xác nhận thông tin cho danh sách này.", "MSG_STN11", "", false);
                    return;
                }
            }
            WSForm wsForm = new WSForm();
            string password = "";
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (this.TKCT.HCTCollection.Count == 0)
                {
                    showMsg("MSG_SEN08");
                    //MLMessages("Chưa nhập thông tin hàng của tờ khai.","MSG_SAV06", "", false);
                    return;
                }

                if (this.TKCT.ID == 0)
                {
                    this.TKCT.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    this.TKCT.SoTiepNhan = 0;
                    this.TKCT.NgayTiepNhan = new DateTime(1900, 01, 01);
                }
                try
                {

                    if (GlobalSettings.PassWordDT == "")
                    {
                        wsForm.ShowDialog(this);
                        if (!wsForm.IsReady) return;
                    }
                    password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();

                    this.Cursor = Cursors.WaitCursor;
                    //xmlCurrent = SOFTECH.ECS.V3.Components.GC.SHARE.ProcessXML.SendTKNhapGCCT(password, this.TKCT.ID);

                    this.Cursor = Cursors.Default;
                    sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                    sendXML.LoaiHS = "TKCT";
                    sendXML.master_id = TKCT.ID;
                    sendXML.msg = xmlCurrent;
                    sendXML.func = 1;
                    xmlCurrent = "";
                    sendXML.InsertUpdate();

                    LayPhanHoi(password);

                }
                catch (Exception ex)
                {
                    this.Cursor = Cursors.Default;
                    //if (loaiWS == "1")
                    {
                        #region FPTService
                        string[] msg = ex.Message.Split('|');
                        if (msg.Length == 2)
                        {
                            if (msg[1] == "DOTNET_LEVEL")
                            {
                                //if (ShowMessage("Khai báo không thành công.Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                                //{
                                //    HangDoi hd = new HangDoi();
                                //    hd.ID = TKCT.ID;
                                //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                                //    hd.TrangThai = TKCT.TrangThaiXuLy;
                                //    hd.ChucNang = ChucNang.KHAI_BAO;
                                //    hd.PassWord = password;
                                //    MainForm.AddToQueueForm(hd);
                                //    MainForm.ShowQueueForm();
                                //}
                                showMsg("MSG_WRN12");
                                //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                                return;
                            }
                            else
                            {
                                showMsg("MSG_2702016", msg[0]);
                                //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            }
                        }
                        else
                        {
                            if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                                showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                                showMsg("MSG_2702004", ex.Message);
                                //ShowMessage(ex.Message, false);
                            }
                            setCommandStatus();
                        }
                        #endregion FPTService
                    }

                    StreamWriter write = File.AppendText("Error.txt");
                    write.WriteLine("--------------------------------");
                    write.WriteLine("Lỗi khi khai báo danh sách Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                    write.WriteLine(ex.StackTrace);
                    write.WriteLine("Lỗi là : ");
                    write.WriteLine(ex.Message);
                    write.WriteLine("--------------------------------");
                    write.Flush();
                    write.Close();

                }
                finally
                {
                    this.Cursor = Cursors.Default;
                }

            }
        }

        private void LayPhanHoi(string pass)
        {
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            XmlDocument doc = new XmlDocument();
            try
            {
                sendXML.LoaiHS = "TKCT";
                sendXML.master_id = TKCT.ID;
                sendXML.Load();
                this.Cursor = Cursors.WaitCursor;
                //xmlCurrent = SOFTECH.ECS.V3.Components.GC.ToKhaiChuyenTiep.LayPhanHoi(pass, sendXML.msg);
                string currMsg = xmlCurrent;

                //Company.KDT.SHARE.Components.Messages.ApproverMessage approver = new Company.KDT.SHARE.Components.Messages.ToKhaiGapLoi();
                //approver.Successor = new Company.KDT.SHARE.Components.Messages.ToKhaiCTCapSoTiepNhan();
                //string msgResult =  approver.ProcessRequest(new Company.KDT.SHARE.Components.Messages.MessageXml(currMsg));


                xmlCurrent = TKCT.PhanTichMSG(xmlCurrent);
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {

                    if (showMsg("MSG_STN02", true) == "Yes")
                    //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                    {
                        this.Refresh();
                        LayPhanHoi(pass);
                    }
                    return;

                }

                if (TKCT.ActionStatus == (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua && sendXML.func != 3)
                {
                    if (TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                    {
                        currMsg.XmlSaveMessage(TKCT.ID, MessageTitle.KhaiBaoToKhaiSuaCTDuyet);
                        showMsg("Tờ khai sửa được duyệt: " + TKCT.SoToKhai.ToString(), new string[] { TKCT.SoToKhai.ToString(), "" });
                        lbltrangthai.Text = GlobalSettings.NGON_NGU == "1" ? "Approved " : "Đã duyệt";
                    }
                    else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        string ret = string.Format("Khai báo tờ khai sửa thành công. Số tiếp nhận: " + TKCT.SoTiepNhan.ToString(), new string[] { TKCT.SoTiepNhan.ToString(), "" });
                        currMsg.XmlSaveMessage(TKCT.ID, MessageTitle.KhaiBaoToKhaiSuaCTCapSo, ret);
                        showMsg(ret);
                        lbltrangthai.Text = "Chờ duyệt";
                    }
                    else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string thongBao = "";
                        string where = "LoaiChungTu = '" + Company.GC.BLL.KDT.KetQuaXuLy.LoaiChungTu_ToKhaiCT + "' AND ItemID=" + TKCT.ID;
                        List<Company.GC.BLL.KDT.KetQuaXuLy> kqxl = Company.GC.BLL.KDT.KetQuaXuLy.SelectCollectionDynamic(where, "ID DESC");
                        if (kqxl.Count > 0)
                        {
                            Company.GC.BLL.KDT.KetQuaXuLy kq = kqxl[0];
                            thongBao = kq.NoiDung;
                            showMsg("Thông báo: " + thongBao, "");
                        }
                        lbltrangthai.Text = "Không phê duyệt";
                        currMsg.XmlSaveMessage(TKCT.ID, MessageTitle.KhaiBaoToKhaiCTKoDuyet, thongBao);

                    }

                }
                else
                {
                    if (TKCT.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        string msg = "";
                        if (sendXML.func != 3)
                        {
                            currMsg.XmlSaveMessage(TKCT.ID, MessageTitle.KhaiBaoToKhaiCTKoDuyet, TKCT.HUONGDAN);
                            msg = "Từ chối tiếp nhận. Lý do: " + TKCT.HUONGDAN;
                            if (TKCT.ActionStatus != (int)Company.KDT.SHARE.Components.ActionStatus.ToKhaiSua)
                            {
                                TKCT.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                                TKCT.HUONGDAN = "";
                                sendXML.Delete();
                            }
                            else
                                TKCT.TrangThaiXuLy = TrangThaiXuLy.KHONG_PHE_DUYET;
                            TKCT.Update();

                        }
                        else
                        {
                            TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            TKCT.Update();
                            msg = Company.GC.BLL.KDT.KetQuaXuLy.LayKetQuaXuLy(TKCT.ID, Company.GC.BLL.KDT.KetQuaXuLy.LoaiThongDiep_TuChoiTiepNhan);
                            currMsg.XmlSaveMessage(TKCT.ID, MessageTitle.KhaiBaoToKhaiCTDuocDuyet, msg);

                        }
                        showMsg(msg, "");
                    }
                    else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                    {
                        currMsg.XmlSaveMessage(TKCT.ID, MessageTitle.KhaiBaoToKhaiCTChoDuyet, string.Format("Số tiếp nhận ={0}", TKCT.SoTiepNhan));
                        showMsg("MSG_SEN02", TKCT.SoTiepNhan);
                        //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + TKCT.SoTiepNhan, "MSG_SEN02", TKCT.SoTiepNhan.ToString(), false);
                        txtsotiepnhan.Text = this.TKCT.SoTiepNhan.ToString();
                        lbltrangthai.Text = setText("Chờ duyệt chính thức", "Wait for approval");

                    }
                    else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET && TKCT.PhanLuong == "")
                    {
                        string ret = string.Format("Tờ khai được cấp số: " + TKCT.SoToKhai.ToString(), new string[] { TKCT.SoToKhai.ToString(), "" });
                        showMsg(ret);
                        currMsg.XmlSaveMessage(TKCT.ID, MessageTitle.KhaiBaoToKhaiCTDuocDuyet, ret);
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKCT.SoToKhai.ToString(), "MSG_SEN03", "Declaration No.: " + TKCT.SoToKhai.ToString(), false);
                        lbltrangthai.Text = GlobalSettings.NGON_NGU == "1" ? "Approved " : "Đã duyệt";
                        txtToKhaiSo.Text = TKCT.SoToKhai.ToString();
                    }
                    else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET && TKCT.PhanLuong != "")
                    {
                        string tenluong = "Xanh";
                        if (TKCT.PhanLuong == TrangThaiPhanLuong.LUONG_VANG)
                            tenluong = "Vàng";
                        else if (TKCT.PhanLuong == TrangThaiPhanLuong.LUONG_DO)
                            tenluong = "Đỏ";
                        string ret = string.Format("Tờ khai được phân luồng: " + tenluong + "\nHướng dẫn: " + TKCT.HUONGDAN, new string[] { TKCT.SoToKhai.ToString(), "" });
                        showMsg(ret);
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKCT.SoToKhai.ToString(), "MSG_SEN03", "Declaration No.: " + TKCT.SoToKhai.ToString(), false);
                        lbltrangthai.Text = GlobalSettings.NGON_NGU == "1" ? "Approved " : "Đã duyệt";

                        txtToKhaiSo.Text = TKCT.SoToKhai.ToString();
                        currMsg.XmlSaveMessage(TKCT.ID, MessageTitle.KhaiBaoToKhaiCTDuocPhanLuong, ret);

                    }
                    else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                    {
                        currMsg.XmlSaveMessage(TKCT.ID, MessageTitle.KhaiBaoToKhaiCTKoDuyet);
                        showMsg("MSG_SEN05");
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Hải quan không phê duyệt.","MSG_SEN05","", false);
                        if (GlobalSettings.NGON_NGU == "1")
                        {
                            lbltrangthai.Text = "Not approved ";
                        }
                        else
                        {
                            lbltrangthai.Text = "Hải quan không phê duyệt";
                        }

                    }
                    else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                    {
                        string where = "ReferenceID = '" + TKCT.GUIDSTR + "'";
                        List<Company.KDT.SHARE.Components.Message> msg = Company.KDT.SHARE.Components.Message.SelectCollectionDynamic(where, "CreatedTime DESC");
                        if (msg.Count > 0)
                        {
                            showMsg("Thông báo: " + msg[0].TieuDeThongBao + "\r\n" + msg[0].NoiDungThongBao, "");
                            sendXML.Delete();
                        }
                    }
                }

                //xoa thông tin msg nay trong database
                //sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            //sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận  danh sách Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void inPhieuTN()
        {
            if (this.TKCT.SoTiepNhan == 0) return;
            Company.Interface.Report.GC.PhieuTN phieuTN = new Company.Interface.Report.GC.PhieuTN();
            phieuTN.phieu = "TỜ KHAI CHUYỂN TIẾP";
            phieuTN.soTN = this.TKCT.SoTiepNhan.ToString();
            phieuTN.ngayTN = this.TKCT.NgayTiepNhan.ToString("dd/MM/yyyy");
            phieuTN.maHaiQuan = TKCT.MaHaiQuanTiepNhan;
            phieuTN.BindReport();
            phieuTN.ShowPreview();
        }
        private void PrintGCCTTQ()
        {
            //ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                Report.ReportViewGCCTTQForm f = new Report.ReportViewGCCTTQForm();
                f.TKCT = this.TKCT;
                f.TKCT.LoadHCTCollection();
                f.Show();
            }
        }
        private void InToKhaiGCCT_196()
        {
            //ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
            GridEXRow row = dgList.GetRow();
            if (row.RowType == RowType.Record)
            {
                if (TKCT.ID == 0)
                {
                    ShowMessage("Vui lòng lưu lại thông tin tờ khai trước khi in", false);
                    return;
                }
                if (TKCT.MaLoaiHinh.Substring(0, 1).ToUpper() == "N" || TKCT.MaLoaiHinh.PadRight(1) == "N")
                {

                    Report.ReportViewGCCTTQ_TT196Form f = new Report.ReportViewGCCTTQ_TT196Form();
                    f.TKCT = this.TKCT;
                    f.TKCT.LoadHCTCollection();
                    f.TKCT.LoadChungTuKem();
                    f.Show();
                }
                else
                {
                    Report.ReportViewGCCTTQ_G_TT196Form f = new Report.ReportViewGCCTTQ_G_TT196Form();
                    f.TKCT = this.TKCT;
                    f.TKCT.LoadHCTCollection();
                    f.TKCT.LoadChungTuKem();
                    f.Show();
                }
            }
        }
        private void Print()
        {
            if (this.TKCT.HCTCollection.Count == 0) return;
            if (this.TKCT.ID == 0)
            {
                showMsg("MSG_PRI01");
                //MLMessages("Bạn hãy lưu trước khi in.","MSG_PRI01","", false);
                return;
            }
            Report.ReportViewTKCTForm f = new Company.Interface.Report.ReportViewTKCTForm();
            f.TKCT = this.TKCT;
            f.ShowDialog();
        }

        private void Add()
        {
            if (txtSoHopDongNhan.Text.Trim() == "")
            {
                epError.SetError(txtSoHopDongNhan, setText("Số hợp đồng nhận không được rỗng.", "This VietNam must be filled"));
                epError.SetIconPadding(txtSoHopDongNhan, -8);
                return;
            }
            if (txtSoHongDonggiao.Text.Trim() == "")
            {
                epError.SetError(txtSoHongDonggiao, setText("Số hợp đồng giao không được rỗng.", "This VietNam must be filled"));
                epError.SetIconPadding(txtSoHongDonggiao, -8);
                return;
            }
            if (radNPL.Checked) TKCT.LoaiHangHoa = "N";
            if (radSP.Checked) TKCT.LoaiHangHoa = "S";
            if (radTB.Checked) TKCT.LoaiHangHoa = "T";
            if (radHangMau.Checked) TKCT.LoaiHangHoa = "H";


            HangGCCTForm frmHangCT = new HangGCCTForm();
            frmHangCT.LoaiHangHoa = TKCT.LoaiHangHoa;
            frmHangCT.NhomLoaiHinh = cbLoaiHinh.Ma;
            frmHangCT.MaNguyenTe = ctrNguyenTe.Ma;
            frmHangCT.TKCT = this.TKCT;
            frmHangCT.NhomLoaiHinh = this.NhomLoaiHinh;
            frmHangCT.MaHaiQuan = this.TKCT.MaHaiQuanTiepNhan;
            frmHangCT.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);


            if (this.HDGC.SoHopDong == "")
            {
                this.HDGC.SoHopDong = txtSoHopDongNhan.Text;
                this.HDGC.NgayKy = ccNgayHopDongNhan.Value;

            }
            frmHangCT.isByHand = isByHand;
            frmHangCT.OpenType = OpenFormType.Edit;
            frmHangCT.ShowDialog();
            dgList.DataSource = this.TKCT.HCTCollection;
            try
            {
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        //Thêm nhiều hàng.
        private void AddManyGoods()
        {
            if (txtSoHopDongNhan.Text.Trim() == "")
            {
                epError.SetError(txtSoHopDongNhan, setText("Số hợp đồng nhận không được rỗng.", "This VietNam must be filled"));
                epError.SetIconPadding(txtSoHopDongNhan, -8);
                return;
            }
            if (txtSoHongDonggiao.Text.Trim() == "")
            {
                epError.SetError(txtSoHongDonggiao, setText("Số hợp đồng giao không được rỗng.", "This VietNam must be filled"));
                epError.SetIconPadding(txtSoHongDonggiao, -8);
                return;
            }

            if (this.HDGC.SoHopDong == "")
            {
                this.HDGC.SoHopDong = txtSoHopDongNhan.Text;
                this.HDGC.NgayKy = ccNgayHopDongNhan.Value;
                //this.HDGC.MaDoanhNghiep = txtMaDonVinhan.Text;
                //this.HDGC.MaHaiQuan = GlobalSet1tings.MA_HAI_QUAN;
            }

            Company.Interface.KDT.ToKhai.SelectNhieuHangMauDichChuyenTiepForm f = new Company.Interface.KDT.ToKhai.SelectNhieuHangMauDichChuyenTiepForm();
            f.HD = this.HDGC;
            ismoreGoods = true;
            f.ismoreGoods = this.ismoreGoods;
            f.LoaiHangHoa = this.LoaiHinhHangHoa;
            f.NhomLH = this.NhomLoaiHinh;
            f.HCTCollection = new List<HangChuyenTiep>();
            foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
            {
                HangChuyenTiep HCTNew = HCT.SaoChepHCT();
                f.HCTCollection.Add(HCTNew);
            }
            f.TKCT = TKCT;
            //f.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Text);
            f.ShowDialog();
            try
            {
                dgList.DataSource = TKCT.HCTCollection;
                dgList.Refetch();
            }
            catch
            {
                dgList.Refresh();
            }
        }

        private void TaoToKhaiDoiXung()
        {
            //ToKhaiChuyenTiep tkCopy = new ToKhaiChuyenTiep();
            //tkCopy.SoTiepNhan = 0;
            //tkCopy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            //tkCopy.NgayTiepNhan = new DateTime(1900, 01, 01);
            //tkCopy.MaHaiQuanTiepNhan = TKCT.MaHaiQuanTiepNhan;
            //tkCopy.SoToKhai = Convert.ToInt32(txtToKhaiSo.Value);
            ////tkCopy.CanBoDangKy = txtCanbodangky.Text;
            //tkCopy.NgayDangKy = ccNgayDangKy.Value;
            //tkCopy.MaLoaiHinh = cbLoaiHinh.Ma.Substring(0, cbLoaiHinh.Ma.Length - 1);
            //if (TKCT.MaLoaiHinh.EndsWith("N") || TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
            //    tkCopy.MaLoaiHinh += "X";
            //else
            //    tkCopy.MaLoaiHinh += "N";
            //tkCopy.MaDoanhNghiep = this.TKCT.MaKhachHang;
            //tkCopy.SoHopDongDV = this.TKCT.SoHDKH;
            //tkCopy.NgayHDDV = TKCT.NgayHDKH;
            //tkCopy.NgayHetHanHDDV = TKCT.NgayHetHanHDKH;
            //tkCopy.NguoiChiDinhDV = TKCT.NguoiChiDinhKH;
            //tkCopy.DiaDiemXepHang = TKCT.DiaDiemXepHang;

            //tkCopy.MaKhachHang = TKCT.MaDoanhNghiep;
            //tkCopy.TenKH = GlobalSettings.TEN_DON_VI;
            //tkCopy.SoHDKH = TKCT.SoHopDongDV;
            //tkCopy.NgayHDKH = TKCT.NgayHDDV;
            //tkCopy.NgayHetHanHDKH = TKCT.NgayHetHanHDDV;
            //tkCopy.NguoiChiDinhKH = TKCT.NguoiChiDinhDV;
            ////tkCopy.MaHaiQuanKH = donViHaiQuanControl2.Ma;

            ////tkCopy.MaDaiLy = txtMaDonViUyThac.Text;
            ////tkCopy.TyGiaVND = (decimal)txtTyGiaTinhThue.Value;
            ////tkCopy.TyGiaUSD = (decimal)txtTyGiaUSD.Value;
            ////tkCopy.LePhiHQ = (decimal)txtLePhiHQ.Value;
            ////tkCopy.SoBienLai = txtSoBienLai.Text;
            ////tkCopy.NgayBienLai = ccNgayBienLai.Value;
            ////tkCopy.ChungTu = txtChungTu.Text;
            ////tkCopy.NguyenTe_ID = nguyenTeControl1.Ma;
            //tkCopy.IDHopDong = HDGCDoiXung.ID;
            //foreach (HangChuyenTiep hang in TKCT.HCTCollection)
            //{
            //    HangChuyenTiep hangCopy = new HangChuyenTiep();
            //    hangCopy.DonGia = hang.DonGia;
            //    hangCopy.ID_DVT = hang.ID_DVT;
            //    hangCopy.ID_NuocXX = hang.ID_NuocXX;
            //    hangCopy.MaHang = hang.MaHang;
            //    hangCopy.MaHS = hang.MaHS;
            //    hangCopy.SoLuong = hang.SoLuong;
            //    hangCopy.SoThuTuHang = hang.SoThuTuHang;
            //    hangCopy.TenHang = hang.TenHang;
            //    hangCopy.TriGia = hang.TriGia;
            //    tkCopy.HCTCollection.Add(hangCopy);
            //}
            //try
            //{
            //    tkCopy.InsertUpdateFullCopy(TKCT);
            //    showMsg("MSG_0203033", tkCopy.ID);
            //    //MLMessages("Tạo tờ khai đối xứng thành công.Tờ khai đối xứng có ID=" + tkCopy.ID.ToString(), "MSG_COPY01", tkCopy.ID.ToString(), false);
            //}
            //catch (Exception ex)
            //{
            //    showMsg("MSG_COPY02", ex.Message);
            //    //MLMessages("Lỗi khi tạo tờ khai đối xứng : " + ex.Message, "MSG_COPY02", ex.Message, false);
            //}
        }

        private void CapNhatToKhaiDoiXung()
        {
            ToKhaiChuyenTiep tkCopy = new ToKhaiChuyenTiep();
            tkCopy.ID = TKCT.ID_Relation;
            tkCopy = ToKhaiChuyenTiep.Load(tkCopy.ID);
            if (tkCopy == null)
            {
                if (showMsg("MSG_COPY03", true) == "Yes")
                //if (MLMessages("Tờ khai đối xứng đã bị xóa. Bạn có muốn tạo tờ khai đối xứng không ? ","MSG_COPY03","", true) == "Yes")
                {
                    TaoToKhaiDoiXung();
                }
                return;
            }
            tkCopy.LoadHCTCollection();
            tkCopy.SoTiepNhan = 0;
            tkCopy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
            tkCopy.NgayTiepNhan = new DateTime(1900, 01, 01);
            tkCopy.MaHaiQuanTiepNhan = TKCT.MaHaiQuanTiepNhan;
            tkCopy.SoToKhai = Convert.ToInt32(txtToKhaiSo.Value);
            //tkCopy.CanBoDangKy = txtCanbodangky.Text;
            tkCopy.NgayDangKy = ccNgayDangKy.Value;
            tkCopy.MaLoaiHinh = cbLoaiHinh.Ma.Substring(0, cbLoaiHinh.Ma.Length - 1);
            if (TKCT.MaLoaiHinh.EndsWith("N") || TKCT.MaLoaiHinh.Substring(0, 1).Equals("N"))
                tkCopy.MaLoaiHinh += "X";
            else
                tkCopy.MaLoaiHinh += "N";
            tkCopy.MaDoanhNghiep = this.TKCT.MaKhachHang;
            tkCopy.SoHopDongDV = this.TKCT.SoHDKH;
            tkCopy.NgayHDDV = TKCT.NgayHDKH;
            tkCopy.NgayHetHanHDDV = TKCT.NgayHetHanHDKH;
            tkCopy.NguoiChiDinhDV = TKCT.NguoiChiDinhKH;
            tkCopy.DiaDiemXepHang = TKCT.DiaDiemXepHang;

            tkCopy.MaKhachHang = TKCT.MaDoanhNghiep;
            tkCopy.TenKH = GlobalSettings.TEN_DON_VI;
            tkCopy.SoHDKH = TKCT.SoHopDongDV;
            tkCopy.NgayHDKH = TKCT.NgayHDDV;
            tkCopy.NgayHetHanHDKH = TKCT.NgayHetHanHDDV;
            tkCopy.NguoiChiDinhKH = TKCT.NguoiChiDinhDV;
            //tkCopy.MaHaiQuanKH = donViHaiQuanControl2.Ma;

            //tkCopy.MaDaiLy = txtMaDonViUyThac.Text;
            //tkCopy.TyGiaVND = (decimal)txtTyGiaTinhThue.Value;
            //tkCopy.TyGiaUSD = (decimal)txtTyGiaUSD.Value;
            //tkCopy.LePhiHQ = (decimal)txtLePhiHQ.Value;
            //tkCopy.SoBienLai = txtSoBienLai.Text;
            //tkCopy.NgayBienLai = ccNgayBienLai.Value;
            //tkCopy.ChungTu = txtChungTu.Text;
            //tkCopy.NguyenTe_ID = nguyenTeControl1.Ma;
            tkCopy.IDHopDong = HDGCDoiXung.ID;
            tkCopy.HCTCollection.Clear();
            foreach (HangChuyenTiep hang in TKCT.HCTCollection)
            {
                HangChuyenTiep hangCopy = new HangChuyenTiep();
                hangCopy.DonGia = hang.DonGia;
                hangCopy.ID_DVT = hang.ID_DVT;
                hangCopy.ID_NuocXX = hang.ID_NuocXX;
                hangCopy.MaHang = hang.MaHang;
                hangCopy.MaHS = hang.MaHS;
                hangCopy.SoLuong = hang.SoLuong;
                hangCopy.SoThuTuHang = hang.SoThuTuHang;
                hangCopy.TenHang = hang.TenHang;
                hangCopy.TriGia = hang.TriGia;
                tkCopy.HCTCollection.Add(hangCopy);
            }
            try
            {
                tkCopy.CapNhatFullCopy();
                showMsg("MSG_2702025");
                //MLMessages("Cập nhật tờ khai đối xứng thành công.", "MSG_SAV02", "", false);

            }
            catch (Exception ex)
            {
                showMsg("MSG_COPY04", ex.Message);
                //ShowMessage("Lỗi khi cập nhật tờ khai đối xứng : " + ex.Message, false);
            }

        }

        private void XoaToKhaiDoiXung()
        {
            try
            {
                ToKhaiChuyenTiep tkCopy = new ToKhaiChuyenTiep();
                tkCopy.ID = TKCT.ID_Relation;
                tkCopy.XoaFullCopy(TKCT);
                showMsg("MSG_0203034");
                //MLMessages("Xóa tờ khai đối xứng thành công.","MSG_DEL02","", false);
            }
            catch (Exception ex)
            {
                showMsg("MSG_0203035", ex.Message);
                //ShowMessage("Lỗi khi xóa tờ khai đối xứng : " + ex.Message, false);
            }
        }

        private void Save()
        {
            try
            {
                bool isKhaibaoSua = TKCT.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET ? true : false;
                cvError.Validate();
                if (cvError.IsValid)
                {
                    if (TKCT.ID > 0)
                    {
                        if (TKCT.MaLoaiHinh.EndsWith("X") || TKCT.MaLoaiHinh.Substring(0, 1).Equals("X")) //Kiem tra them MLH: XGC18, XGC19, XGC20
                        {
                            if (Company.GC.BLL.GC.PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(TKCT.ID))
                            {
                                showMsg("MSG_ALL01");
                                //ShowMessage("Tờ khai chuyển tiếp có id = " + TKCT.ID + " này đã được phân bổ nên không thể xóa được.", false);
                                return;
                            }
                        }
                        else
                        {
                            if (Company.GC.BLL.GC.PhanBoToKhaiNhap.CheckPhanBoToKhaiNhap((int)TKCT.SoToKhai, TKCT.MaLoaiHinh, TKCT.MaHaiQuanTiepNhan, (short)TKCT.NgayDangKy.Year, TKCT.IDHopDong))
                            {
                                showMsg("MSG_ALL01");
                                //ShowMessage("Tờ khai chuyển tiếp có id= " + TKCT.ID + " đã được phân bổ nên không chỉnh sửa dữ liệu được.", false);
                                return;
                            }
                        }
                    }
                    //if (this.TKCT.HCTCollection.Count == 0)
                    //{
                    //    showMsg("MSG_SAV06");
                    //    //MLMessages("Chưa nhập thông tin hàng của tờ khai","MSG_SAV06","", false);
                    //    return;
                    //}
                    bool isValid = Globals.ValidateNull(txtSoHopDongNhan, epError, "Số hợp đồng nhận") &&
                        Globals.ValidateNull(txtSoHongDonggiao, epError, "Số hợp đồng giao");
                        isValid &= Globals.ValidateNull(txtDaiDienDN, epError, "Đại diện doanh nghiệp") &&
                        Globals.ValidateNull(txtChucVu, epError, "Chức vụ người đại diện");
                    if (!isValid) return;
                    if (txtSoHopDongNhan.Text.Trim() == txtSoHongDonggiao.Text.Trim())
                    {
                        showMsg("MSG_0203036");
                        //MLMessages("Hợp đồng nhận và hợp đồng giao trùng nhau, vui lòng chọn lại hợp đồng", "MSG_SAV09", "", false);
                        return;
                    }

                    bool isValidate = Globals.ValidateNull(txtTenDonViDoiTac, epError, grbNguoiXK.Text);
                    //isValidate &= Globals.ValidateNull(txtMaDonViDoiTac, epError, "Mã đơn vị đối tác");
                    isValidate &= Globals.ValidateNull(txtDaiDienDN, epError, "Đại diện doanh nghiệp");

                    isValidate &= Globals.ValidateNull(cbDKGH, epError, "Điều kiện giao hàng");
                    isValidate &= Globals.ValidateNull(cbPTTT, epError, "Phương thức thanh toán");
                    isValidate &= Globals.ValidateNull(txtSoHopDongNhan, epError, "Số hợp đồng nhận");
                    isValidate &= Globals.ValidateNull(txtSoHongDonggiao, epError, "Số hợp đồng giao");
                    if (string.IsNullOrEmpty(ctrNuocXuatKhau.Ma))
                    {
                        Globals.ShowMessageTQDT("Thông báo", "Mã nước không được để trống", false);
                        ctrNuocXuatKhau.Focus();
                        return;

                    }

                    if (TKCT.HCTCollection.Count == 0)
                    {
                        Globals.ShowMessageTQDT("Thông báo", "Chưa nhập thông tin hàng của tờ khai", false);
                        return;
                    }

                    if (!isValidate)
                        return;

                    if (isKhaibaoSua)
                    {
                        //if (string.IsNullOrEmpty(txtLyDoSua.Text))
                        //{
                        //    ShowMessage("Bạn chưa nhập lý do sửa !", false);
                        //    return;
                        //}
                        #region OldCode
                        string whereCondition = "TKMD_ID = " + TKCT.ID + " AND MaLoaiHinh = '" + TKCT.MaLoaiHinh.Trim() + "' ";
                        if (NoiDungDieuChinhTK.SelectCollectionDynamic(whereCondition, "").Count == 0)
                        {
                            if (MLMessages("Chưa có dữ liệu sửa đổi, bổ sung tờ khai. Bạn có muốn sửa đổi, bổ sung không?", "MSG_SAV06", "", true) == "Yes")
                            {
                                NoiDungChinhSuaTKCTForm noiDungChinhSuaTKForm = new NoiDungChinhSuaTKCTForm();
                                noiDungChinhSuaTKForm.TKCT = TKCT;
                                noiDungChinhSuaTKForm.noiDungDCTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                                noiDungChinhSuaTKForm.ShowDialog();
                            }
                            else
                                return;
                        }
                        else
                        {
                            int count = 0;
                            NoiDungChinhSuaTKDetailForm ndDieuChinhTKChiTietForm = new NoiDungChinhSuaTKDetailForm();
                            string where = "TKMD_ID = " + TKCT.ID + " AND MALOAIHINH = '" + TKCT.MaLoaiHinh.Trim() + "' ";
                            ndDieuChinhTKChiTietForm.ListNoiDungDieuChinhTK = NoiDungDieuChinhTK.SelectCollectionDynamic(where, "");
                            foreach (NoiDungDieuChinhTK nddctk in ndDieuChinhTKChiTietForm.ListNoiDungDieuChinhTK)
                            {
                                if (nddctk.TrangThai != 1)
                                    ++count;
                            }
                            if (count == 0)
                            {
                                if (MLMessages("Chưa có dữ liệu sửa đổi, bổ sung tờ khai. Bạn có muốn sửa đổi, bổ sung không?", "MSG_SAV06", "", true) == "Yes")
                                {
                                    NoiDungChinhSuaTKCTForm noiDungChinhSuaTKForm = new NoiDungChinhSuaTKCTForm();
                                    noiDungChinhSuaTKForm.TKCT = this.TKCT;
                                    noiDungChinhSuaTKForm.noiDungDCTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
                                    noiDungChinhSuaTKForm.ShowDialog();
                                }
                                else
                                    return;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        if (TKCT.SoTiepNhan > 0)
                        {
                            TKCT.TrangThaiXuLy = TrangThaiXuLy.CHO_DUYET;
                            TKCT.ActionStatus = 0;
                        }
                        else
                        {
                            TKCT.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                            TKCT.ActionStatus = -1;
                            TKCT.NgayTiepNhan = DateTime.Parse("01/01/1900");
                        }

                        if (TKCT.SoToKhai > 0)
                            TKCT.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        else
                        {
                            TKCT.PhanLuong = "";
                            TKCT.HUONGDAN = "";
                            TKCT.NgayDangKy = DateTime.Parse("01/01/1900");
                        }
                    }
                    this.TKCT.MaHaiQuanTiepNhan = HDGC.MaHaiQuan;
                    Get();
                    long idTK = TKCT.ID;
                    this.TKCT.IDHopDong = HDGC.ID;

                    //TODO: Update by Hungtq 22/02/2011
                    TKCT.PTTT_ID = cbPTTT.SelectedValue.ToString();
                    TKCT.DKGH_ID = cbDKGH.SelectedValue.ToString();
                    //Nhập bằng tay ở HD đã duyệt và không sửa. LanNT
                    if (!isByHand && !isKhaibaoSua)
                        TKCT.TrangThaiXuLy = -1;
                    try
                    {
                        if (string.IsNullOrEmpty(TKCT.GUIDSTR))
                            this.TKCT.GUIDSTR = Guid.NewGuid().ToString();
                        this.TKCT.InsertUpdateFull();

                        if (isByHand)
                        {
                            if (TKCT.MaLoaiHinh.Contains("PHPLN") || TKCT.MaLoaiHinh.Contains("PHSPN") || TKCT.MaLoaiHinh.Contains("NGC18") || TKCT.MaLoaiHinh.Contains("NGC19"))
                                Company.GC.BLL.GC.NPLNhapTonThucTe.UpdateNguyenPhuLieuTonThucTeByTKCT(this.TKCT);
                        }
                        showMsg("MSG_SAV02");
                        //MLMessages("Lưu thành công","MSG_SAV02","", false);
                    }
                    catch (Exception ex)
                    {
                        //showMsg("MSG_0203036", ex.Message);
                        ShowMessage("Có lỗi khi cập nhật : " + ex.Message, false);
                    }
                    setCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }
        }

        private void LaySoTiepNhanDT()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode node = null;
            WSForm wsForm = new WSForm();
            string password = "";
            Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
            try
            {
                sendXML.LoaiHS = "TKCT";
                sendXML.master_id = TKCT.ID;
                if (!sendXML.Load())
                {
                    showMsg("MSG_STN01");
                    //MLMessages("Danh sách không có phản hồi do chưa gửi thông tin tới hải quan.","MSG_STN01","", false);
                    return;
                }
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                if (GlobalSettings.PassWordDT != "")
                    password = GlobalSettings.PassWordDT;
                else
                    password = wsForm.txtMatKhau.Text.Trim();
                this.Cursor = Cursors.WaitCursor;
                {
                    xmlCurrent = TKCT.LayPhanHoi(password, sendXML.msg);
                }
                this.Cursor = Cursors.Default;
                // Thực hiện kiểm tra.  
                if (xmlCurrent != "")
                {
                    doc.LoadXml(xmlCurrent);
                    node = doc.SelectSingleNode("Megs");
                    if (node == null)
                    {
                        if (showMsg("MSG_STN02", true) == "Yes")
                        //if (MLMessages("Chưa có phản hồi từ hải quan.Bạn có muốn tiếp tục xác nhận thông tin không","MSG_STN02","", true) == "Yes")
                        {
                            this.Refresh();
                            LayPhanHoi(password);
                        }
                        else
                        {
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                            NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            cbLoaiHinh.ReadOnly = true;
                            XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        }
                        return;
                    }
                }
                if (sendXML.func == 1)
                {
                    showMsg("MSG_SEN02", TKCT.SoTiepNhan);
                    //MLMessages("Đăng ký thành công!\nSố tiếp nhận đăng ký điện tử: " + TKCT.SoTiepNhan,"MSG_SEN02",TKCT.SoTiepNhan.ToString(), false);
                    txtsotiepnhan.Text = this.TKCT.SoTiepNhan.ToString();
                    lbltrangthai.Text = setText("Chờ duyệt chính thức", "Wait for  approval");

                }
                else if (sendXML.func == 3)
                {
                    showMsg("MSG_2702024");
                    //MLMessages("Hủy khai báo thành công","MSG_CAN01","", false);
                    txtsotiepnhan.Text = "";

                    lbltrangthai.Text = setText("Chưa khai báo", "Not declared yet");

                }
                else if (sendXML.func == 2)
                {
                    if (TKCT.SoToKhai == TrangThaiXuLy.DA_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN11", new string[] { TKCT.SoToKhai.ToString(), mess });
                        //MLMessages("Trạng thái chứng từ đã thay đổi: Đã duyệt chính thức.\nSố tờ khai : " + TKCT.SoToKhai.ToString(),"MSG_SEN03","Declaration No : "+TKCT.SoToKhai.ToString(), false);

                        lbltrangthai.Text = setText("Đã duyệt", "Approved");

                        txtToKhaiSo.Text = TKCT.SoToKhai.ToString();
                    }
                    else if (TKCT.SoToKhai == TrangThaiXuLy.CHO_DUYET)
                    {
                        string mess = "";
                        if (node != null)
                        {
                            if (node.HasChildNodes)
                                mess += "\nCó phản hồi từ hải quan : \r\n";
                            foreach (XmlNode nodeCon in node.ChildNodes)
                            {
                                mess += Company.KDT.SHARE.Components.Utils.FontConverter.TCVN2Unicode(nodeCon.InnerText) + "\n";
                            }
                        }
                        showMsg("MSG_SEN04", mess);
                        //MLMessages("Hải quan chưa duyệt danh sách này","MSG_STN06","", false);
                    }
                }
                //xoa thông tin msg nay trong database
                //sendXML.Delete();
                setCommandStatus();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                //if (loaiWS == "1")
                {
                    #region FPTService
                    string[] msg = ex.Message.Split('|');
                    if (msg.Length == 2)
                    {
                        if (msg[1] == "DOTNET_LEVEL")
                        {
                            //if (ShowMessage("Có lỗi do hệ thống phía hải quan.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                            //{
                            //    HangDoi hd = new HangDoi();
                            //    hd.ID = TKCT.ID;
                            //    hd.LoaiToKhai = LoaiToKhai.TO_KHAI_MAU_DICH;
                            //    hd.TrangThai = TKCT.TrangThaiXuLy;
                            //    hd.ChucNang = ChucNang.XAC_NHAN_THONG_TIN;
                            //    hd.PassWord = pass;
                            //    MainForm.AddToQueueForm(hd);
                            //    MainForm.ShowQueueForm();
                            //}
                            showMsg("MSG_WRN12");
                            //ShowMessage("Không kết nối được với hệ thống hải quan.", false);
                            return;
                        }
                        else
                        {
                            showMsg("MSG_2702016", msg[0]);
                            //ShowMessage("Có lỗi trong khai báo : " + msg[0], false);
                            if (msg[0] != "Sai mật khẩu hoặc tên truy nhập!")
                            {
                                sendXML.Delete();
                                setCommandStatus();
                            }
                            else
                            {
                                GlobalSettings.PassWordDT = "";
                            }
                        }
                    }
                    else
                    {
                        if (ex.Message.Trim() != "Sai mật khẩu hoặc tên truy nhập!")
                        {
                            showMsg("MSG_WRN13", ex.Message);
                            //ShowMessage("Xảy ra lỗi không xác định." + ex.Message, false);
                            sendXML.Delete();
                            setCommandStatus();
                        }
                        else
                        {
                            GlobalSettings.PassWordDT = "";
                            showMsg("MSG_2702004", ex.Message);
                            //ShowMessage(ex.Message, false);
                        }
                    }
                    #endregion FPTService
                }

                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Lỗi khi xác nhận Tờ khai chuyển tiếp. Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void setCommandStatus()
        {
            if (isByHand)
            {
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                txtsotiepnhan.Text = "0";
                cmdChungTuKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdChungTuBoSung1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                return;
            }
            if (TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET && !string.IsNullOrEmpty(TKCT.PhanLuong))
            {
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = cmdHuyToKhaiDaDuyet1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cbLoaiHinh.ReadOnly = true;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lbltrangthai.Text = setText("Đã duyệt", "Approved");
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdChungTuKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET && string.IsNullOrEmpty(TKCT.PhanLuong))
            {
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lbltrangthai.Text = setText("Đã duyệt", "Approved");
                cmdChungTuKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }
            else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
            {
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdSuaToKhaiDaDuyet.Enabled = cmdSuaToKhaiDaDuyet1.Enabled =
                    cmdHuyToKhaiDaDuyet.Enabled = cmdHuyToKhaiDaDuyet1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                sendXML.master_id = TKCT.ID;
                sendXML.LoaiHS = LoaiKhaiBao.ToKhaiCT;
                if (sendXML.Load())
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend.Enabled = cmdSend2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }


                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lbltrangthai.Text = setText("Chưa khai báo", "Not declared yet");
                txtsotiepnhan.Text = string.Empty;
                cmdChungTuKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                if (TKCT.SoToKhai > 0)
                {
                    cmdSuaToKhaiDaDuyet.Enabled = cmdSuaToKhaiDaDuyet1.Enabled =
                   cmdHuyToKhaiDaDuyet.Enabled = cmdHuyToKhaiDaDuyet1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdChungTuBoSung1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                }
                else
                {

                }
            }
            else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend2.Enabled = cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu2.Enabled = NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                Huy.Enabled = cmdSuaToKhaiDaDuyet.Enabled = cmdSuaToKhaiDaDuyet1.Enabled =
                   cmdHuyToKhaiDaDuyet.Enabled = cmdHuyToKhaiDaDuyet1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave1.Enabled = cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                cmdChungTuKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lbltrangthai.Text = "Không phê duyệt";

                if (TKCT.SoToKhai > 0)
                {
                    cmdSuaToKhaiDaDuyet1.Enabled = cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdHuyToKhaiDaDuyet1.Enabled = cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    cmdSend2.Enabled = cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                }
                else
                {
                    //cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    //cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                }
            }
            else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaToKhaiDaDuyet.Enabled = cmdSuaToKhaiDaDuyet1.Enabled =
                  cmdHuyToKhaiDaDuyet.Enabled = cmdHuyToKhaiDaDuyet1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lbltrangthai.Text = setText("Chờ duyệt", "Wait for approval");

            }
            else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                cmdSuaToKhaiDaDuyet.Visible = cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdChungTuKem.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lbltrangthai.Text = "Đã hủy";
            }
            else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                cmdSuaToKhaiDaDuyet.Visible = cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Visible = cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lbltrangthai.Text = "Chờ hủy";
            }
            else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaToKhaiDaDuyet.Visible = cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdChungTuKem.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet1.Enabled = cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Huy2.Enabled = Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                lbltrangthai.Text = "Sửa tờ khai đã duyệt";
                txtToKhaiSo.Text = TKCT.SoToKhai.ToString();

            }
            else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                lbltrangthai.Text = "Chưa khai báo";


            }
            else if (TKCT.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA)
            {
                NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                Huy.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemHang.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdThemHang1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;

                cmdSuaToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdHuyToKhaiDaDuyet.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lbltrangthai.Text = "Chờ duyệt";

                txtsotiepnhan.Text = TKCT.SoTiepNhan.ToString();
            }
            cmdChungTuBoSung1.Enabled = cmdChungTuKem.Enabled == Janus.Windows.UI.InheritableBoolean.True ? Janus.Windows.UI.InheritableBoolean.False : Janus.Windows.UI.InheritableBoolean.True;


            #region Tiêu đề phân luồng

            if (TKCT.PhanLuong == "1")
            {
                lblPhanLuong.Text = "Luồng Xanh";
            }
            else if (TKCT.PhanLuong == "2")
            {
                lblPhanLuong.Text = "Luồng Vàng";
            }
            else if (TKCT.PhanLuong == "3")
            {
                lblPhanLuong.Text = "Luồng Đỏ";
            }
            else
            {
                lblPhanLuong.Text = "Chưa phân luồng";
            }

            #endregion
        }



        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["ID_NuocXX"].Text = this.Nuoc_GetName(e.Row.Cells["ID_NuocXX"].Text.ToString());
            e.Row.Cells["ID_DVT"].Text = this.DonViTinh_GetName(e.Row.Cells["ID_DVT"].Text.ToString());
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0) return;
            int pos = items[0].Position;
            Company.GC.BLL.KDT.GC.HangChuyenTiep hangCT = this.TKCT.HCTCollection[pos];

            HangGCCTForm frmHangCT = new HangGCCTForm(hangCT);
            frmHangCT.isByHand = isByHand;
            frmHangCT.TKCT = this.TKCT;
            frmHangCT.OpenType = Company.KDT.SHARE.Components.OpenFormType.Edit;
            frmHangCT.NhomLoaiHinh = this.NhomLoaiHinh;
            frmHangCT.MaHaiQuan = this.TKCT.MaHaiQuanTiepNhan;
            frmHangCT.MaNguyenTe = ctrNguyenTe.Ma;
            frmHangCT.TyGiaTT = Convert.ToDecimal(txtTyGiaTinhThue.Value);
            this.TKCT.DKGH_ID = cbDKGH.SelectedValue != null ? cbDKGH.SelectedValue.ToString() : "-1";
            frmHangCT.ShowDialog();

        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (TKCT.ID > 0)
            {
                if (TKCT.MaLoaiHinh.EndsWith("X") || TKCT.MaLoaiHinh.Substring(0, 1).Equals("X"))
                {
                    if (Company.GC.BLL.GC.PhanBoToKhaiXuat.CheckPhanBoToKhaiChuyenTiepXuat(TKCT.ID))
                    {
                        showMsg("MSG_ALL01");
                        //ShowMessage("Tờ khai chuyển tiếp có id = " + TKCT.ID + " này đã được phân bổ nên không thể xóa được.", false);
                        return;
                    }
                }
            }
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        string LoaiHangHoa = "N";
                        if (TKCT.MaLoaiHinh.IndexOf("SP") > 0 || TKCT.MaLoaiHinh.IndexOf("19") > 0)
                            LoaiHangHoa = "S";
                        else if (TKCT.MaLoaiHinh.IndexOf("TB") > 0 || TKCT.MaLoaiHinh.IndexOf("20") > 0)
                            LoaiHangHoa = "T";
                        HangChuyenTiep hct = (HangChuyenTiep)i.GetRow().DataRow;
                        if (hct.ID > 0)
                        {
                            hct.Delete(TKCT.IDHopDong, LoaiHangHoa, TKCT.MaLoaiHinh);
                        }
                    }
                }
                //long idTK = TKCT.ID;                
                //string st = "Nhập";
                //if (TKCT.MaLoaiHinh.EndsWith("X") || TKCT.MaLoaiHinh.Substring(0,1).Equals("X"))
                //    st = "Xuất";
                //if (TKCT.MaDoanhNghiep.Trim() == TKCT.MaKhachHang.Trim())
                //{
                //    string msg = "";
                //    if (TKCT.ID_Relation > 0)
                //    {
                //        msg = "Bạn có muốn cập nhật tờ khai " + st + "  đối xứng không ?";
                //        if (ShowMessage(msg, true) == "Yes")
                //        {
                //            CapNhatToKhaiDoiXung();
                //        }
                //    }
                //    else
                //    {
                //        msg = "Bạn có muốn tạo tờ khai " + st + " đối xứng không ?";
                //        if (ShowMessage(msg, true) == "Yes")
                //        {
                //            XoaToKhaiDoiXung();
                //        }
                //    }
                //}               
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void txtSoHongDonggiao_ButtonClick(object sender, EventArgs e)
        {
            HopDongManageForm f = new HopDongManageForm();
            f.IsBrowseForm = true;
            f.IsDaDuyet = false;
            f.ShowDialog();
            if (!string.IsNullOrEmpty(f.HopDongSelected.SoHopDong))
            {
                txtSoHongDonggiao.Text = f.HopDongSelected.SoHopDong;
                ccNgayHopDongGiao.Value = f.HopDongSelected.NgayKy;
                ccNgayHHHopDongGiao.Value = f.HopDongSelected.NgayHetHan;
                HDGCDoiXung.ID = f.HopDongSelected.ID;
                TKCT.ID_Relation = f.HopDongSelected.ID;
            }
        }


        #region Chung tu kem
        private void ChungTuDinhKem(string key)
        {
            try
            {
                switch (key)
                {
                    case "cmdGiayPhep":
                        txtSoGiayPhep_ButtonClick(null, null);
                        break;
                    case "cmdHopDongThuongMai":
                        ViewHopDongTM(false);
                        break;
                    case "cmdHoaDonThuongMai":
                        txtSoHoaDonThuongMai_ButtonClick(null, null);
                        break;
                    case "cmdDeNghiChuyenCuaKhau":
                        DeNghiChuyenCuaKhau("");
                        break;
                    case "cmdChungTuDangAnh":
                        AddChungTuAnh(false);
                        break;
                    case "cmdChungTuNo":
                        AddChungTuNo();
                        break;
                    default:
                        txtSoGiayPhep_ButtonClick(null, null);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }
        }
        #endregion

        #region TẠO COMMAND CHUNG TU KEM BO SUNG TOOLBAR

        //HUNGTQ, Update 11/06/2010.

        private Janus.Windows.UI.CommandBars.UICommand cmdChungTuBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdVanDonBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdGiayPhepBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHopDongBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHoaDonThuongMaiBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCOBoSung1;
        private Janus.Windows.UI.CommandBars.UICommand cmdChuyenCuaKhauBoSung1;
        //private Janus.Windows.UI.CommandBars.UICommand cmdChungTuNoBoSung1;
        /// <summary>
        /// Tạo command chứng từ đính kèm bổ sung.
        /// </summary>
        private void CreateCommandBosung()
        {
            try
            {
                #region Initialize
                //Tao nut command tren toolbar.
                this.cmdChungTuBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuBoSung");
                this.cmdVanDonBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVanDonBoSung");
                this.cmdGiayPhepBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayPhepBoSung");
                this.cmdHopDongBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHopDongBoSung");
                this.cmdHoaDonThuongMaiBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHoaDonThuongMaiBoSung");
                this.cmdCOBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCOBoSung");
                this.cmdChuyenCuaKhauBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenCuaKhauBoSung");
                this.cmdChungTuDangAnhBS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuDangAnhBS");
                //cmdChungTuNoBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuNoBoSung");
                this.cmbToolBar.CommandManager = this.cmMain;
                this.cmbToolBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                this.cmdChungTuBoSung1
            });

                if (!this.cmMain.Commands.Contains("cmdChungTuBoSung"))
                    this.cmMain.Commands.Add(this.cmdChungTuBoSung1);
                if (!this.cmMain.Commands.Contains("cmdGiayPhepBoSung"))
                    this.cmMain.Commands.Add(this.cmdGiayPhepBoSung1);
                if (!this.cmMain.Commands.Contains("cmdHopDongBoSung"))
                    this.cmMain.Commands.Add(this.cmdHopDongBoSung1);
                if (!this.cmMain.Commands.Contains("cmdHoaDonThuongMaiBoSung"))
                    this.cmMain.Commands.Add(this.cmdHoaDonThuongMaiBoSung1);
                if (!this.cmMain.Commands.Contains("cmdCOBoSung"))
                    this.cmMain.Commands.Add(this.cmdCOBoSung1);
                if (!this.cmMain.Commands.Contains("cmdChuyenCuaKhauBoSung"))
                    this.cmMain.Commands.Add(this.cmdChuyenCuaKhauBoSung1);
                if (!this.cmMain.Commands.Contains("cmdChungTuDangAnhBS"))
                    this.cmMain.Commands.Add(this.cmdChungTuDangAnhBS1);
                //if (!this.cmMain.Commands.Contains("cmdChungTuNoBoSung"))
                //    this.cmMain.Commands.Add(this.cmdChungTuNoBoSung1);
                //this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
                //    this.cmdChungTuBoSung1,                
                //    this.cmdGiayPhepBoSung1,
                //    this.cmdHopDongBoSung1,
                //    this.cmdHoaDonThuongMaiBoSung1,
                //    this.cmdCOBoSung1,
                //    this.cmdChuyenCuaKhauBoSung1,
                //    this.cmdChungTuDangAnhBS1
                //});

                // 
                // cmdChungTuBoSung
                // 
                this.cmdChungTuBoSung1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdGiayPhepBoSung1,
            this.cmdHopDongBoSung1,
            this.cmdHoaDonThuongMaiBoSung1,
            //this.cmdCOBoSung1,
            this.cmdChuyenCuaKhauBoSung1,
            this.cmdChungTuDangAnhBS1,
            //this.cmdChungTuNoBoSung1
                });

                this.cmdChungTuBoSung1.Key = "cmdChungTuBoSung";
                this.cmdChungTuBoSung1.Name = "cmdChungTuBoSung1";
                this.cmdChungTuBoSung1.Text = "Chứng từ bổ sung";
                this.cmdChungTuBoSung1.ImageIndex = 4;
                // 
                // cmdHopDongBoSung1
                // 
                this.cmdHopDongBoSung1.Key = "cmdHopDongBoSung";
                this.cmdHopDongBoSung1.Name = "cmdHopDongBoSung1";
                this.cmdHopDongBoSung1.Text = "Hợp đồng thương mại";
                this.cmdHopDongBoSung1.ImageIndex = 0;
                // 
                // cmdVanDonBoSung1
                // 
                this.cmdVanDonBoSung1.Key = "cmdVanDonBoSung";
                this.cmdVanDonBoSung1.Name = "cmdVanDonBoSung1";
                this.cmdVanDonBoSung1.Text = "Vận đơn";
                this.cmdVanDonBoSung1.ImageIndex = 0;
                // 
                // CO1
                // 
                this.cmdCOBoSung1.Key = "cmdCOBoSung";
                this.cmdCOBoSung1.Name = "cmdCOBoSung1";
                this.cmdCOBoSung1.Text = "CO";
                this.cmdCOBoSung1.ImageIndex = 0;
                // 
                // cmdChuyenCuaKhauBoSung1
                // 
                this.cmdChuyenCuaKhauBoSung1.Key = "cmdChuyenCuaKhauBoSung";
                this.cmdChuyenCuaKhauBoSung1.Name = "cmdChuyenCuaKhauBoSung1";
                this.cmdChuyenCuaKhauBoSung1.Text = "Đề nghị chuyển cửa khẩu";
                this.cmdChuyenCuaKhauBoSung1.ImageIndex = 0;
                // 
                // cmdGiayPhepBoSung1
                // 
                this.cmdGiayPhepBoSung1.Key = "cmdGiayPhepBoSung";
                this.cmdGiayPhepBoSung1.Name = "cmdGiayPhepBoSung1";
                this.cmdGiayPhepBoSung1.Text = "Giấy phép";
                this.cmdGiayPhepBoSung1.ImageIndex = 0;
                // 
                // cmdHoaDonThuongMaiBoSung1
                // 
                this.cmdHoaDonThuongMaiBoSung1.Key = "cmdHoaDonThuongMaiBoSung";
                this.cmdHoaDonThuongMaiBoSung1.Name = "cmdHoaDonThuongMaiBoSung1";
                this.cmdHoaDonThuongMaiBoSung1.Text = "Hóa đơn thương mại";
                this.cmdHoaDonThuongMaiBoSung1.ImageIndex = 0;
                // 
                // cmdChungTuDangAnhBS1
                // 
                this.cmdChungTuDangAnhBS1.Key = "cmdChungTuDangAnhBS";
                this.cmdChungTuDangAnhBS1.Name = "cmdChungTuDangAnhBS1";
                this.cmdChungTuDangAnhBS1.Text = "Chứng từ dạng ảnh";
                this.cmdChungTuDangAnhBS1.ImageIndex = 0;

                ////
                //// cmdcmdChungTuNoBoSung1
                ////
                //cmdChungTuNoBoSung1.Key = "cmdChungTuNoBoSung";
                //cmdChungTuNoBoSung1.Text = "Chứng từ nợ";
                //cmdChungTuNoBoSung1.ImageIndex = 0;

                #endregion

                cmdChungTuBoSung1.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdChungTuBoSung1_Click);
                cmdGiayPhepBoSung1.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdChungTuBoSung1_Click);
                cmdHopDongBoSung1.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdChungTuBoSung1_Click);
                cmdHoaDonThuongMaiBoSung1.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdChungTuBoSung1_Click);
                cmdCOBoSung1.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdChungTuBoSung1_Click);
                cmdChuyenCuaKhauBoSung1.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdChungTuBoSung1_Click);
                cmdChungTuDangAnhBS1.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdChungTuBoSung1_Click);
                //cmdChungTuNoBoSung1.Click += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmdChungTuBoSung1_Click);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void cmdChungTuBoSung1_Click(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            try
            {
                switch (e.Command.Key)
                {
                    //case "cmdVanDonBoSung":
                    //    if (!ValidateKhaiBoSung(TKCT.SoToKhai)) return;
                    //    this.txtSoVanTaiDon_ButtonClick("1", null);
                    //    break;
                    case "cmdHoaDonThuongMaiBoSung":
                        if (!ValidateKhaiBoSung(TKCT.SoToKhai)) return;
                        this.txtSoHoaDonThuongMai_ButtonClick("1", null);
                        break;
                    //case "CO":
                    //    if (!ValidateKhaiBoSung(TKCT.SoToKhai)) return;
                    //    this.QuanLyCo("1");
                    //    break;
                    case "cmdHopDongBoSung":
                        if (!ValidateKhaiBoSung(TKCT.SoToKhai)) return;
                        ViewHopDongTM(true);
                        break;
                    case "cmdGiayPhepBoSung":
                        if (!ValidateKhaiBoSung(TKCT.SoToKhai)) return;
                        this.txtSoGiayPhep_ButtonClick("1", null);
                        break;
                    case "cmdChuyenCuaKhauBoSung":
                        if (!ValidateKhaiBoSung(TKCT.SoToKhai)) return;
                        this.DeNghiChuyenCuaKhau("1");
                        break;

                    case "cmdChungTuDangAnhBS":
                        if (!ValidateKhaiBoSung(TKCT.SoToKhai)) return;
                        this.AddChungTuAnh(true);
                        break;
                    case "cmdChungTuNoBoSung":
                        AddChungTuNo();
                        break;
                    default:
                        if (!ValidateKhaiBoSung(TKCT.SoToKhai)) return;
                        this.txtSoGiayPhep_ButtonClick("1", null);
                        break;
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void txtSoGiayPhep_ButtonClick(object sender, EventArgs e)
        {
            if (TKCT.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListGiayPhepTKMDFormGCCT f = new ListGiayPhepTKMDFormGCCT();
            if (sender != null)
            {
                f.isKhaiBoSung = true;
            }
            else
                f.isKhaiBoSung = false;
            TKCT.LoadChungTuKem();
            f.TKCT = TKCT;
            f.ShowDialog();
        }

        private void ViewHopDongTM(bool loaikhai)
        {
            if (TKCT.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHopDongTKMDFormGCCT f = new ListHopDongTKMDFormGCCT();
            if (loaikhai == true)
            {
                f.isKhaiBoSung = true;
            }
            f.TKCT = TKCT;
            f.ShowDialog();
        }

        private void txtSoHoaDonThuongMai_ButtonClick(object sender, EventArgs e)
        {
            if (TKCT.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            ListHoaDonTKMDFormGCCT f = new ListHoaDonTKMDFormGCCT();
            if (sender != null)
                f.isKhaiBoSung = true;
            f.TKCT = TKCT;
            f.ShowDialog();
        }

        private void DeNghiChuyenCuaKhau(string isKhaiBoSung)
        {
            if (TKCT.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            var f = new ListChuyenCuaKhauTKMDFormGCCT();
            if (isKhaiBoSung != "")
                f.isKhaiBoSung = true;
            f.TKCT = TKCT;
            f.ShowDialog();
        }

        private void AddChungTuAnh(bool isKhaiBoSung)
        {
            if (TKCT.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập thông tin chứng từ", false);
                return;
            }
            var ctForm = new ListChungTuAnhFormGCCT();
            ctForm.isKhaiBoSung = isKhaiBoSung;
            ctForm.TKCT = TKCT;
            ctForm.ShowDialog();
        }
        private void AddChungTuNo()
        {
            ListChungTuNoFormGCCT ctForm = new ListChungTuNoFormGCCT();
            ctForm.TKCT = this.TKCT;
            ctForm.ShowDialog();
        }
        private void AddNPLCungUng()
        {
            if (TKCT.ID == 0)
            {
                ShowMessage("Lưu thông tin tờ khai trước khi nhập chứng từ đính kèm", false);
                return;
            }
            Company.Interface.NPLCungUngFormGCCT ctForm = new Company.Interface.NPLCungUngFormGCCT();
            ctForm.TKCT = this.TKCT;
            ctForm.ShowDialog();
        }
        #region Validate Khai bo sung

        /// <summary>
        /// Kiem tra thong tin truoc khi khai bo sung chung tu.
        /// </summary>
        /// <param name="soToKhai"></param>
        /// <returns></returns>
        /// HUNGTQ, Update 07/06/2010.
        private bool ValidateKhaiBoSung(long soToKhai)
        {
            if (soToKhai == 0)
            {
                string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                Globals.ShowMessageTQDT(msg, false);

                return false;
            }

            return true;
        }

        #endregion

        private void CtrNguyenTeValueChanged(object sender, EventArgs e)
        {
            txtTyGiaTinhThue.Text = ctrNguyenTe.TyGia.ToString();
        }

        #endregion

        #region Send V3 Create by LANNT
        private void FeedBackV3()
        {

            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                ObjectSend msgSend = SingleMessage.FeedBackCT(TKCT, TKCT.GUIDSTR);

                SendMessageForm dlgSendForm = new SendMessageForm();
                dlgSendForm.Send += SendMessage;
                isFeedBack = dlgSendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    if (
                         feedbackContent.Function == DeclarationFunction.CHUA_XU_LY ||
                         feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN
                         )
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TO_KHAI && count > 0)
                    {
                        if (feedbackContent.Function == DeclarationFunction.THONG_QUAN ||
                            feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            isFeedBack = false;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = true;
                            ShowMessageTQDT(msgInfor, false);
                        }
                        count--;

                    }
                    else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN ||
                        feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        isFeedBack = false;
                        ShowMessageTQDT(msgInfor, false);
                    }
                    else
                    {
                        isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                    }
                    if (feedbackContent.Function != DeclarationFunction.CHUA_XU_LY)
                        setCommandStatus();
                }
            }



        }

        private void CancelV3()
        {

            ObjectSend msgSend = SingleMessage.CancelMessageCT(TKCT);
            SendMessageForm dlgSendForm = new SendMessageForm();

            dlgSendForm.Send += SendMessage;
            bool isSend = dlgSendForm.DoSend(msgSend);

            if (isSend)
            {
                FeedBackV3();
                dlgSendForm.Message.XmlSaveMessage(TKCT.ID, MessageTitle.HuyKhaiBaoToKhai);
            }

        }
        private void SendV3()
        {
            try
            {
                if (TKCT.ID == 0)
                {
                    this.ShowMessage("Bạn hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (TKCT.HCTCollection.Count == 0)
                {
                    this.ShowMessage("Bạn chưa nhập hàng khai báo\r\nVui lòng nhập thông tin hàng trước khi khai báo", false);
                    return;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.ToKhaiCT;
                sendXML.master_id = TKCT.ID;
                if (sendXML.Load())
                {
                    MLMessages("Tờ khai đã gửi đến Hải quan. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", "MSG_SEN03", "", false);
                    cmdSend2.Enabled = cmdSend.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    NhanDuLieu2.Enabled = NhanDuLieu.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }

                if (TKCT.SoToKhai != 0 && TKCT.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET)
                {
                    string msg = "Tờ khai này đã có số tờ khai, không thể khai báo mới tờ khai.\r\nBạn hãy chuyển tờ khai sang trạng thái khác.";
                    ShowMessage(msg, false);
                    return;
                }
                else
                {

                    ObjectSend msgSend = SingleMessage.SendMessageCT(TKCT);

                    TKCT.TrangThaiXuLy = TKCT.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET ? TrangThaiXuLy.CHUA_KHAI_BAO : TrangThaiXuLy.SUATKDADUYET;



                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessage;

                    bool isSend = dlgSendForm.DoSend(msgSend);

                    

                    if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                    {
                        dlgSendForm.Message.XmlSaveMessage(TKCT.ID, TKCT.TrangThaiXuLy != TrangThaiXuLy.SUATKDADUYET ? MessageTitle.KhaiBaoToKhai : MessageTitle.KhaiBaoSuaTK);
                        sendXML = new MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.ToKhaiCT;
                        sendXML.master_id = TKCT.ID;
                        sendXML.func = 1;
                        sendXML.InsertUpdate();
                        TKCT.Update();
                        FeedBackV3();

                    }
                    else if (!string.IsNullOrEmpty(msgInfor))
                        ShowMessageTQDT(msgInfor, false);


                }
                setCommandStatus();


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                SingleMessage.SendMail(TKCT.MaHaiQuanTiepNhan, new SendEventArgs(ex));
            }
        }

        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);

            this.Invoke(new MethodInvoker(
                delegate
                {
                    if (this.TKCT.SoTiepNhan > 0)

                        txtsotiepnhan.Text = this.TKCT.SoTiepNhan.ToString();
                    if (this.TKCT.SoToKhai > 0)
                        txtToKhaiSo.Text = this.TKCT.SoToKhai.ToString();
                    setCommandStatus();
                }));
        }
        /// <summary>
        /// Xử lý message tờ khai trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.ToKhaiSendHandlerCT(TKCT, ref msgInfor, sender, e);

        }

        #endregion

        private void btnNoiDungChinhSua_Click(object sender, EventArgs e)
        {
            NoiDungChinhSuaTKCTForm noiDungChinhSuaTKForm = new NoiDungChinhSuaTKCTForm();
            noiDungChinhSuaTKForm.TKCT = TKCT;
            noiDungChinhSuaTKForm.noiDungDCTK.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO;
            noiDungChinhSuaTKForm.ShowDialog();
        }
    }
}