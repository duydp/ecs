﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;

namespace Company.Interface.Report
{
    
    public partial class ReportViewForm : BaseForm
    {
        public ToKhaiNhap ToKhaiChinhReport = new ToKhaiNhap();
        public PhuLucToKhaiNhap PhuLucReport = new PhuLucToKhaiNhap();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        private System.Drawing.Printing.Margins MarginTKN = new System.Drawing.Printing.Margins();
        private System.Drawing.Printing.Margins MarginPhuLucTKN = new System.Drawing.Printing.Margins();
        public int _Sotrang;
        public ReportViewForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            this.MarginTKN = GlobalSettings.MarginTKN;
            this.MarginPhuLucTKN = GlobalSettings.MarginPhuLucTKN;
            if (this.TKMD.HMDCollection.Count > 3)
            {
                int count = (this.TKMD.HMDCollection.Count - 1) / 9 + 1;
                for (int i = 0; i < count; i++)
                    this.AddItemComboBox();
            }
            else
                uiGroupBox2.Visible = false;
            cboToKhai.SelectedIndex = 0;

        }
        // edit 
        private void chkInBanLuuHaiQuan_CheckedChanged(object sender, EventArgs e)
        {
            
        }
        public void AddItemComboBox()
        {
            cboToKhai.Items.Add("Phụ lục " + cboToKhai.Items.Count, cboToKhai.Items.Count);
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.Margins = this.MarginTKN;
                this.ToKhaiChinhReport.TKMD = this.TKMD;
                this.ToKhaiChinhReport.BindReport();
                printControl1.PrintingSystem = ToKhaiChinhReport.PrintingSystem;
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                try
                {
                    this._Sotrang = cboToKhai.SelectedIndex ;
                }catch{}
                HangMauDichCollection HMDReportCollection = new HangMauDichCollection();
                int begin = (cboToKhai.SelectedIndex - 1) * 9;
                int end = cboToKhai.SelectedIndex * 9;
                if (end > this.TKMD.HMDCollection.Count) end = this.TKMD.HMDCollection.Count;
                for (int i = begin; i < end; i++)
                    HMDReportCollection.Add(this.TKMD.HMDCollection[i]);
                this.PhuLucReport = new PhuLucToKhaiNhap();
                this.PhuLucReport.SoToKhai = this.TKMD.SoToKhai;
                this.PhuLucReport.TKMD = this.TKMD;
                if(this.TKMD.NgayDangKy!= new DateTime(1900,1,1))
                    this.PhuLucReport.NgayDangKy = this.TKMD.NgayDangKy;
                this.PhuLucReport.HMDCollection = HMDReportCollection;
                this.PhuLucReport.Margins = this.MarginPhuLucTKN;
                this.PhuLucReport.trang = true;
                this.PhuLucReport._SoTrang = this._Sotrang;
                this.PhuLucReport.BindReport();
                printControl1.PrintingSystem = PhuLucReport.PrintingSystem;
                this.PhuLucReport.CreateDocument();

            }
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.MarginTKN = printControl1.PrintingSystem.PageMargins;
                Properties.Settings.Default.MarginTKN = this.MarginTKN;
                Properties.Settings.Default.Save();
                GlobalSettings.RefreshKey();
                this.ToKhaiChinhReport.Margins = this.MarginTKN;
                this.ToKhaiChinhReport.ptbImage.Visible = false;
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.MarginPhuLucTKN = printControl1.PrintingSystem.PageMargins;
                this.PhuLucReport.Margins = this.MarginPhuLucTKN;

                Properties.Settings.Default.MarginPhuLucTKN = this.MarginPhuLucTKN;
                Properties.Settings.Default.Save();
                GlobalSettings.RefreshKey();
                this.PhuLucReport.xrPictureBox1.Visible = false;
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                } 
                this.PhuLucReport.xrPictureBox1.Visible = true;
                this.PhuLucReport.CreateDocument();
            }
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            //switch (cboExport.SelectedValue.ToString())
            //{
            //    case "pdf":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportPdf, new object[] { true });
            //        break;
            //    case "excel":
            //        printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            //        break;

            //}
        }

        private void btnQuickPrint_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.MarginTKN = printControl1.PrintingSystem.PageMargins;
                Properties.Settings.Default.MarginTKN = this.MarginTKN;
                Properties.Settings.Default.Save();
                GlobalSettings.RefreshKey();
                this.ToKhaiChinhReport.Margins = this.MarginTKN;
                this.ToKhaiChinhReport.ptbImage.Visible = false;
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                }
                this.ToKhaiChinhReport.ptbImage.Visible = true;
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.MarginPhuLucTKN = printControl1.PrintingSystem.PageMargins;
                this.PhuLucReport.Margins = this.MarginPhuLucTKN;
                Properties.Settings.Default.MarginPhuLucTKN = this.MarginPhuLucTKN;
                Properties.Settings.Default.Save();
                GlobalSettings.RefreshKey();
                this.PhuLucReport.xrPictureBox1.Visible = false;
                try
                {
                    if (cboToKhai.Items.Count > 2)
                    {
                        this.PhuLucReport.trang = true;
                    }
                }
                catch { }
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.PrintDirect, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message,false);
                } 
                this.PhuLucReport.xrPictureBox1.Visible = true;
                this.PhuLucReport.CreateDocument();
            }
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            this.ToKhaiChinhReport.setNhomHang(txtTenNhomHang.Text);
            this.ToKhaiChinhReport.CreateDocument();
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.setVisibleImage(false);
                this.ToKhaiChinhReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                this.ToKhaiChinhReport.setVisibleImage(true);
                this.ToKhaiChinhReport.CreateDocument();
            }
            else
            {
                this.PhuLucReport.xrPictureBox1.Visible = false;
                this.PhuLucReport.CreateDocument();
                try
                {
                    printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi: " + ex.Message, false);
                }
                this.PhuLucReport.xrPictureBox1.Visible = true;
                this.PhuLucReport.CreateDocument();
            }
        }

        private void printControl1_Load(object sender, EventArgs e)
        {

        }

        private void chkMienThue1_CheckedChanged(object sender, EventArgs e)
        {
            if (cboToKhai.SelectedIndex == 0)
            {
                this.ToKhaiChinhReport.MienThue1 = chkMienThue1.Checked;
                this.ToKhaiChinhReport.MienThue2 = chkMienThue2.Checked;
                this.ToKhaiChinhReport.BindReport();
                this.ToKhaiChinhReport.CreateDocument();

            }
            else
            {
                this.PhuLucReport.MienThue1 = chkMienThue1.Checked;
                this.PhuLucReport.MienThue2 = chkMienThue2.Checked;
                this.PhuLucReport.BindReport();
                this.PhuLucReport.CreateDocument();

            }
        }

        private void chkInBanLuuHaiQuan_CheckedChanged_1(object sender, EventArgs e)
        {
            
        }


    }
}