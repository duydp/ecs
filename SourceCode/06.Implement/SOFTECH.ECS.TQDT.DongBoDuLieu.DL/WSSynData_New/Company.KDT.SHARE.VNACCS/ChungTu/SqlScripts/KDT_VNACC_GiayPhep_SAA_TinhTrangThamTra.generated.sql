-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteBy_GiayPhep_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteBy_GiayPhep_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Insert]
	@GiayPhep_ID bigint,
	@TongSoToKhai numeric(6, 0),
	@TinhTrangThamTra varchar(2),
	@PhuongThucXuLyCuoiCung numeric(1, 0),
	@SoDonXinCapPhep numeric(12, 0),
	@MaNhanVienKiemTra varchar(8),
	@NgayKhaiBao datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@NgayGiaoViec datetime,
	@GioGiaoViec datetime,
	@NguoiGiaoViec varchar(8),
	@NoiDungGiaoViec nvarchar(768),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
(
	[GiayPhep_ID],
	[TongSoToKhai],
	[TinhTrangThamTra],
	[PhuongThucXuLyCuoiCung],
	[SoDonXinCapPhep],
	[MaNhanVienKiemTra],
	[NgayKhaiBao],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[NgayGiaoViec],
	[GioGiaoViec],
	[NguoiGiaoViec],
	[NoiDungGiaoViec],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@GiayPhep_ID,
	@TongSoToKhai,
	@TinhTrangThamTra,
	@PhuongThucXuLyCuoiCung,
	@SoDonXinCapPhep,
	@MaNhanVienKiemTra,
	@NgayKhaiBao,
	@MaNguoiKhai,
	@TenNguoiKhai,
	@NgayGiaoViec,
	@GioGiaoViec,
	@NguoiGiaoViec,
	@NoiDungGiaoViec,
	@Notes,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Update]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TongSoToKhai numeric(6, 0),
	@TinhTrangThamTra varchar(2),
	@PhuongThucXuLyCuoiCung numeric(1, 0),
	@SoDonXinCapPhep numeric(12, 0),
	@MaNhanVienKiemTra varchar(8),
	@NgayKhaiBao datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@NgayGiaoViec datetime,
	@GioGiaoViec datetime,
	@NguoiGiaoViec varchar(8),
	@NoiDungGiaoViec nvarchar(768),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
SET
	[GiayPhep_ID] = @GiayPhep_ID,
	[TongSoToKhai] = @TongSoToKhai,
	[TinhTrangThamTra] = @TinhTrangThamTra,
	[PhuongThucXuLyCuoiCung] = @PhuongThucXuLyCuoiCung,
	[SoDonXinCapPhep] = @SoDonXinCapPhep,
	[MaNhanVienKiemTra] = @MaNhanVienKiemTra,
	[NgayKhaiBao] = @NgayKhaiBao,
	[MaNguoiKhai] = @MaNguoiKhai,
	[TenNguoiKhai] = @TenNguoiKhai,
	[NgayGiaoViec] = @NgayGiaoViec,
	[GioGiaoViec] = @GioGiaoViec,
	[NguoiGiaoViec] = @NguoiGiaoViec,
	[NoiDungGiaoViec] = @NoiDungGiaoViec,
	[Notes] = @Notes,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_InsertUpdate]
	@ID bigint,
	@GiayPhep_ID bigint,
	@TongSoToKhai numeric(6, 0),
	@TinhTrangThamTra varchar(2),
	@PhuongThucXuLyCuoiCung numeric(1, 0),
	@SoDonXinCapPhep numeric(12, 0),
	@MaNhanVienKiemTra varchar(8),
	@NgayKhaiBao datetime,
	@MaNguoiKhai varchar(13),
	@TenNguoiKhai nvarchar(300),
	@NgayGiaoViec datetime,
	@GioGiaoViec datetime,
	@NguoiGiaoViec varchar(8),
	@NoiDungGiaoViec nvarchar(768),
	@Notes nvarchar(250),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra] 
		SET
			[GiayPhep_ID] = @GiayPhep_ID,
			[TongSoToKhai] = @TongSoToKhai,
			[TinhTrangThamTra] = @TinhTrangThamTra,
			[PhuongThucXuLyCuoiCung] = @PhuongThucXuLyCuoiCung,
			[SoDonXinCapPhep] = @SoDonXinCapPhep,
			[MaNhanVienKiemTra] = @MaNhanVienKiemTra,
			[NgayKhaiBao] = @NgayKhaiBao,
			[MaNguoiKhai] = @MaNguoiKhai,
			[TenNguoiKhai] = @TenNguoiKhai,
			[NgayGiaoViec] = @NgayGiaoViec,
			[GioGiaoViec] = @GioGiaoViec,
			[NguoiGiaoViec] = @NguoiGiaoViec,
			[NoiDungGiaoViec] = @NoiDungGiaoViec,
			[Notes] = @Notes,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
		(
			[GiayPhep_ID],
			[TongSoToKhai],
			[TinhTrangThamTra],
			[PhuongThucXuLyCuoiCung],
			[SoDonXinCapPhep],
			[MaNhanVienKiemTra],
			[NgayKhaiBao],
			[MaNguoiKhai],
			[TenNguoiKhai],
			[NgayGiaoViec],
			[GioGiaoViec],
			[NguoiGiaoViec],
			[NoiDungGiaoViec],
			[Notes],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@GiayPhep_ID,
			@TongSoToKhai,
			@TinhTrangThamTra,
			@PhuongThucXuLyCuoiCung,
			@SoDonXinCapPhep,
			@MaNhanVienKiemTra,
			@NgayKhaiBao,
			@MaNguoiKhai,
			@TenNguoiKhai,
			@NgayGiaoViec,
			@GioGiaoViec,
			@NguoiGiaoViec,
			@NoiDungGiaoViec,
			@Notes,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TongSoToKhai],
	[TinhTrangThamTra],
	[PhuongThucXuLyCuoiCung],
	[SoDonXinCapPhep],
	[MaNhanVienKiemTra],
	[NgayKhaiBao],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[NgayGiaoViec],
	[GioGiaoViec],
	[NguoiGiaoViec],
	[NoiDungGiaoViec],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectBy_GiayPhep_ID]
	@GiayPhep_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TongSoToKhai],
	[TinhTrangThamTra],
	[PhuongThucXuLyCuoiCung],
	[SoDonXinCapPhep],
	[MaNhanVienKiemTra],
	[NgayKhaiBao],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[NgayGiaoViec],
	[GioGiaoViec],
	[NguoiGiaoViec],
	[NoiDungGiaoViec],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]
WHERE
	[GiayPhep_ID] = @GiayPhep_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[GiayPhep_ID],
	[TongSoToKhai],
	[TinhTrangThamTra],
	[PhuongThucXuLyCuoiCung],
	[SoDonXinCapPhep],
	[MaNhanVienKiemTra],
	[NgayKhaiBao],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[NgayGiaoViec],
	[GioGiaoViec],
	[NguoiGiaoViec],
	[NoiDungGiaoViec],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 03, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[GiayPhep_ID],
	[TongSoToKhai],
	[TinhTrangThamTra],
	[PhuongThucXuLyCuoiCung],
	[SoDonXinCapPhep],
	[MaNhanVienKiemTra],
	[NgayKhaiBao],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[NgayGiaoViec],
	[GioGiaoViec],
	[NguoiGiaoViec],
	[NoiDungGiaoViec],
	[Notes],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_GiayPhep_SAA_TinhTrangThamTra]	

GO

