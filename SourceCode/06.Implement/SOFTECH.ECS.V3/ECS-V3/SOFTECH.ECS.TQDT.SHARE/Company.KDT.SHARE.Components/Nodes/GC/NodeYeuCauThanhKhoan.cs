﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.GC
{
    public class NodeYeuCauThanhKhoan
    {
        /// <summary>
        /// Loại chứng từ (=605)
        /// </summary>
	    public const string issuer	= "issuer";			
    		
	    /// <summary>
        /// Số tham chiếu chứng từ ||Số đăng ký hợp đồng
	    /// </summary>
	    public const string reference	= "reference";
    		
	    /// <summary>
        /// Ngày khai chứng từ 	1	an19	YYYY-MM-DD HH:mm:ss ||Ngày hợp đồng
	    /// </summary>
	    public const string issue	= "issue";
    	
	    /// <summary>
        /// Chức năng của chứng từ (=8)	1	n..2	Danh mục chuẩn
	    /// </summary>
	    public const string function	= "function";
    	
	    /// <summary>
        /// Số đăng ký chứng từ 
	    /// </summary>
	    public const string customsReference	= "customsReference";
    		
	    /// <summary>
	    /// Trạng thái chứng từ 	1	an..3	Danh mục chuẩn
	    /// </summary>
	    public const string status	= "status";		
    	
    	/// <summary>
        /// Ngày đăng ký chứng từ 		an19	YYYY-MM-DD HH:mm:ss
    	/// </summary>
	    public const string acceptance	= "acceptance"; 	
		
		/// <summary>
        /// Mã hải quan 	1	an..6	Danh mục chuẩn ||Mã hải quan tiếp nhận
		/// </summary>
	    public const string declarationOffice	= "declarationOffice";	
		
		/// <summary>
        /// Thông tin người khai hải quan
		/// </summary>
	    public const string Agent	= "Agent";	
		
		/// <summary>
        /// Tên người khai hải quan ||Tên doanh nghiệp
		/// </summary>
	    public const string name	= "name";		
		
		/// <summary>
        /// Mã người khai hải quan 	1	an..17	Danh mục chuẩn ||Mã doanh nghiệp
		/// </summary>
	    public const string identity	= "identity";
		
		/// <summary>
        /// Trạng thái đại lý	1	n1	Danh mục chuẩn
		/// </summary>
	   // public const string status	= "status";	
		
		/// <summary>
        /// Thông tin thương nhân gia công hàng hóa
		/// </summary>
	    public const string Importer	= "Importer";	
		
		/// <summary>
        /// Thông tin thanh khoản
		/// </summary>
	    public const string ContractReference	= "ContractReference";						
    			
	    public const string AdditionalInfomation	= "AdditionalInfomation";	
		
		/// <summary>
        /// Ghi chú khác
		/// </summary>
	    public const string content	= "content";							
    								
	    public const string AdditionalDocument	= "AdditionalDocument";	
		
		/// <summary>
        /// Mã xác định loại chứng từ thanh khoản (tiêu hủy, biếu tặng…)		
		/// </summary>
	    public const string documentType	= "documentType";
		
		/// <summary>
        /// Tham chiếu chứng từ (ví dụ: bộ key TK)
		/// </summary>
	    public const string documentReference	= "documentReference";		
		
		/// <summary>
        /// Ghi chú, mô tả chứng từ đi kèm || Mô tả hàng hóa
		/// </summary>
	    public const string description	= "description";	
					
	    public const string GoodsItems	= "GoodsItems";	
		
		/// <summary>
        /// Chi tiết hàng
		/// </summary>
	    public const string Commodity	= "Commodity";	
		
        /// <summary>
        ///Mã hàng do doanh nghiệp khai
        /// </summary>
	    public const string identification	= "identification";						
    	
		/// <summary>
        /// Loại hàng
		/// </summary>
	    public const string comodityType	= "comodityType";							
    								
	    public const string GoodsMeasure	= "GoodsMeasure";
		
		/// <summary>
        /// Số lượng
		/// </summary>
	    public const string quantity	= "quantity";

        /// <summary>
        /// Mã đơn vị tính
        /// </summary>
        public const string measureUnit = "measureUnit";							
    }
}
