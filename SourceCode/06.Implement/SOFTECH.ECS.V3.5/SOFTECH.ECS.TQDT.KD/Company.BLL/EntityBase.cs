using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Windows.Forms;
using System.IO;

namespace Company.KD.BLL
{
    public class EntityBase
    {
        protected SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
        public static string GetPathProgram()
        {
            FileInfo fileInfo = new FileInfo(Application.ExecutablePath);
            return fileInfo.DirectoryName;
        }
    }
}