using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.SXXK;
using Janus.Windows.GridEX;
using GemBox.Spreadsheet;
//using DongBoDuLieu.BLL.SXXK;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using Company.BLL.SXXK.ToKhai;

namespace Company.Interface.SXXK
{
    public partial class ImportAll4Xml : BaseForm
    {
        //DataSXXKCollection _dataSource;

        public ImportAll4Xml()
        {
            InitializeComponent();
        }

        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
            txtFilePath.Text = openFileDialog1.FileName;
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {


            //try
            //{

            //    if (!System.IO.File.Exists(openFileDialog1.FileName)) throw new NotSupportedException("Tệp tin Config không tồn tại");
            //    XmlSerializer xs = new XmlSerializer(typeof(DataSXXKCollection));
            //    StreamReader sr = new StreamReader(openFileDialog1.FileName);
            //    _dataSource = xs.Deserialize(sr) as DataSXXKCollection;
            //    sr.Close();
            //    ReadSourceNPL();
            //    dgListNPLDN.DataSource = _dataSource[0].NguyenPhuLieu;

            //    ReadSourceSP();
            //    dgListSPDN.DataSource = _dataSource[0].SanPham;
            //    ReadSourceDM();
            //    dgListDMDN.DataSource = _dataSource[0].DinhMuc;
            //    ReadSourceTK();
            //    dgListTKDN.DataSource = _dataSource[0].ToKhai;
            //}
            //catch (Exception ex)
            //{
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //    Globals.ShowMessage("Định dạng data không đúng", false);
            //}
        }
        #region Command LanNT
        //private decimal getDecimal(object objValue)
        //{
        //    if (objValue.ToString() == "") return 0;
        //    else
        //        return decimal.Parse(objValue.ToString());
        //}
        //private DateTime getDate(object objValue)
        //{
        //    if (objValue.ToString().Length == 0)
        //        return new DateTime(1900, 1, 1);
        //    else return DateTime.Parse(objValue.ToString());
        //}
        //private void ReadSourceNPL()
        //{

        //    #region NguyenPhuLieu
        //    NguyenPhuLieuCollection npls = NguyenPhuLieu.GetNPLXML(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
        //    DataColumn dc = new DataColumn("nplExist", typeof(bool));
        //    _dataSource[0].NguyenPhuLieu.Columns.Add(dc);

        //    foreach (DataRow dr in _dataSource[0].NguyenPhuLieu.Rows)
        //    {
        //        bool isExist = false;
        //        foreach (NguyenPhuLieu item in npls)
        //        {
        //            if (item.Ma.Trim() == dr["Ma"].ToString().Trim() && item.DVT_ID.Trim() == dr["DVT_ID"].ToString().Trim())
        //            {
        //                isExist = true;
        //                break;
        //            }
        //        }
        //        dr["nplExist"] = isExist;
        //    }
        //    #endregion NguyenPhuLieu
        //}
        //private void ReadSourceDM()
        //{
        //    #region Dinhmuc

        //    DinhMucCollection dinhmucs = new DinhMuc().SelectCollectionDynamic("MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "' and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'", "");
        //    DataColumn dc = new DataColumn("dmExist", typeof(bool));
        //    _dataSource[0].DinhMuc.Columns.Add(dc);
        //    foreach (DataRow item in _dataSource[0].DinhMuc.Rows)
        //    {
        //        bool isExist = false;
        //        foreach (DinhMuc dmItem in dinhmucs)
        //        {
        //            if (item["MA_SP"].ToString() == dmItem.MaSanPHam && dmItem.MaNguyenPhuLieu == item["MA_NPL"].ToString())
        //            {
        //                isExist = true;
        //                break;
        //            }
        //        }
        //        item["dmExist"] = isExist;
        //    }
        //    #endregion Dinh muc
        //}
        //private void ReadSourceSP()
        //{
        //    #region San Pham
        //    SanPham sp = new SanPham();
        //    SanPhamCollection sps = sp.SelectCollectionDynamic("MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "' and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'", "");
        //    DataColumn dc = new DataColumn("spExist", typeof(bool));
        //    _dataSource[0].SanPham.Columns.Add(dc);
        //    foreach (DataRow item in _dataSource[0].SanPham.Rows)
        //    {
        //        bool isExist = false;
        //        foreach (SanPham sanpham in sps)
        //        {
        //            if (sanpham.Ma == item["Ma"].ToString() && sanpham.DVT_ID == item["DVT_ID"].ToString())
        //            {
        //                isExist = true;
        //                break;
        //            }
        //        }
        //        item["spExist"] = isExist;
        //    }
        //    #endregion
        //}
        //private void ReadSourceTK()
        //{
        //    ToKhaiMauDich tkmd = new ToKhaiMauDich();
        //    ToKhaiMauDichCollection tkmds = tkmd.SelectCollectionDynamic("MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "' and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'", "");
        //    DataColumn dc = new DataColumn("tkExist", typeof(bool));
        //    _dataSource[0].ToKhai.Columns.Add(dc);
        //    foreach (DataRow item in _dataSource[0].ToKhai.Rows)
        //    {
        //        bool isExist = false;
        //        foreach (ToKhaiMauDich tokhai in tkmds)
        //        {
        //            if (tokhai.SoToKhai == int.Parse(item["SOTK"].ToString()) && tokhai.MaLoaiHinh == item["MA_LH"].ToString())
        //            {
        //                isExist = true;
        //                break;
        //            }
        //        }
        //        item["tkExist"] = isExist;
        //    }
        //}
        #endregion Command LanNT

        private void btnSave_Click(object sender, EventArgs e)
        {
            //bool overide = chkOverwrite.Checked;

            //#region NguyenPhuLieu
            //NguyenPhuLieu npl = null;
            //NguyenPhuLieuCollection NguyenPhuLieuSyncs = new NguyenPhuLieuCollection();
            //foreach (DataRow dr in _dataSource[0].NguyenPhuLieu.Rows)
            //{
            //    bool isExist = (bool)dr["nplExist"];
            //    if (!(overide && isExist))
            //    {
            //        npl = new NguyenPhuLieu();
            //    }
            //    npl.Ma = dr["Ma"].ToString();
            //    npl.Ten = dr["Ten"].ToString();
            //    npl.MaHS = dr["MaHS"].ToString();
            //    npl.DVT_ID = dr["DVT_ID"].ToString();

            //    npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //    npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;// dr["MaHaiQuan"].ToString();

            //    NguyenPhuLieuSyncs.Add(npl);
            //}
            //#endregion NguyenPhuLieu
            //#region Dinhmuc
            //DinhMuc dm = new DinhMuc();
            //DinhMucCollection dinhmucsSyncs = new DinhMucCollection();
            //ThongTinDinhMuc thongtinDm = null;
            //ThongTinDinhMucCollection thongtindinhmucs = new ThongTinDinhMucCollection();
            //foreach (DataRow item in _dataSource[0].DinhMuc.Rows)
            //{
            //    bool isExist = (bool)item["dmExist"];

            //    if (!(overide && isExist))
            //    {
            //        dm = new DinhMuc();
            //    }
            //    dm.MaNguyenPhuLieu = item["MA_NPL"].ToString();
            //    dm.MaSanPHam = item["MA_SP"].ToString();
            //    dm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //    dm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    dm.DinhMucChung = decimal.Parse(item["DM_CHUNG"].ToString());
            //    dm.DinhMucSuDung = decimal.Parse(item["DM_SD"].ToString());
            //    dm.TyLeHaoHut = decimal.Parse(item["TL_HH"].ToString());
            //    dm.GhiChu = item["GHI_CHU"].ToString();


            //    thongtinDm = new ThongTinDinhMuc();
            //    thongtinDm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //    thongtinDm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    thongtinDm.MaSanPham = dm.MaSanPHam;
            //    thongtinDm.NgayDangKy = DateTime.Now;
            //    thongtindinhmucs.Add(thongtinDm);
            //    dinhmucsSyncs.Add(dm);
            //}
            //#endregion Dinh muc
            //#region San Pham
            //SanPham sp = new SanPham();
            //SanPhamCollection sanphamsSyncs = new SanPhamCollection();
            //foreach (DataRow item in _dataSource[0].SanPham.Rows)
            //{
            //    bool isExist = (bool)item["spExist"];
            //    if (!(overide && isExist))
            //    {
            //        sp = new SanPham();
            //    }
            //    sp.Ma = item["Ma"].ToString();
            //    sp.Ten = item["Ten"].ToString();
            //    sp.MaHS = item["MaHS"].ToString();
            //    sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //    sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    sp.DVT_ID = item["DVT_ID"].ToString();
            //    sanphamsSyncs.Add(sp);
            //}
            //#endregion
            //#region ToKhai
            //ToKhaiMauDich tkmd = new ToKhaiMauDich();
            //ToKhaiMauDichCollection tokhaiSyncs = new ToKhaiMauDichCollection();
            //foreach (DataRow item in _dataSource[0].ToKhai.Rows)
            //{
            //    bool isExist = (bool)item["tkExist"];
            //    if (!(overide && isExist))
            //    {
            //        tkmd = new ToKhaiMauDich();
            //    }

            //    //Chua set het 
            //    #region Set
            //    tkmd.SoToKhai = int.Parse(item["SOTK"].ToString());
            //    tkmd.TrangThai = 1;
            //    tkmd.MaLoaiHinh = item["MA_LH"].ToString();
            //    tkmd.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    tkmd.NgayDangKy = getDate(item["NGAY_DK"]);
            //    tkmd.NamDangKy = short.Parse(tkmd.NgayDangKy.Year.ToString());
            //    tkmd.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //    tkmd.MaDonViUT = item["MA_DVUT"].ToString();
            //    tkmd.ChiTietDonViDoiTac = item["DV_DT"].ToString();
            //    tkmd.PTVT_ID = item["MA_PTVT"].ToString();
            //    tkmd.NgayDenPTVT = getDate(item["NGAYDEN"]);
            //    tkmd.SoHieuPTVT = item["TEN_PTVT"].ToString();
            //    tkmd.SoVanDon = item["VAN_DON"].ToString();
            //    tkmd.CuaKhau_ID = item["MA_CK"].ToString();
            //    tkmd.SoGiayPhep = item["SO_GP"].ToString();
            //    tkmd.NgayGiayPhep = getDate(item["NGAY_GP"]);
            //    tkmd.NgayHetHanGiayPhep = getDate(item["NGAY_HHGP"]);
            //    tkmd.NgayGiayPhep = getDate(item["NGAY_GP"]);
            //    tkmd.SoHopDong = item["SO_HD"].ToString();
            //    tkmd.TyGiaUSD = getDecimal(item["TYGIA_USD"]);
            //    tkmd.NgayHopDong = getDate(item["NGAY_HD"]);
            //    tkmd.NgayHetHanHopDong = getDate(item["NGAY_HHHD"]);
            //    tkmd.NuocNK_ID = item["NUOC_NK"].ToString();
            //    tkmd.NuocXK_ID = item["NUOC_XK"].ToString();

            //    tkmd.DKGH_ID = item["MA_GH"].ToString();
            //    tkmd.SoHang = item["SOHANG"].ToString().Length > 0 ? short.Parse(item["SOHANG"].ToString()) : (short)0;

            //    tkmd.PTTT_ID = item["MA_PTTT"].ToString();
            //    tkmd.NguyenTe_ID = item["MA_NT"].ToString();

            //    tkmd.TyGiaTinhThue = getDecimal(item["TYGIA_VND"]);
            //    tkmd.LePhiHaiQuan = getDecimal(item["LEPHI_HQ"]);
            //    //BL_LPHQ
            //    //GIAYTO có nhưng không thấy field
            //    tkmd.TenChuHang = item["TENCH"].ToString();
            //    tkmd.SoContainer40 = getDecimal(item["SO_CONTAINER40"]);
            //    tkmd.NgayVanDon = getDate(item["NGAY_VANDON"]);
            //    //NGAY_KB
            //    //DDIEM_KH
            //    tkmd.TrongLuong = getDecimal(item["TR_LUONG"]);
            //    tkmd.TongTriGiaKhaiBao = getDecimal(item["TONGTGKB"]);
            //    tkmd.TongTriGiaTinhThue = getDecimal(item["TONGTGTT"]);
            //    tkmd.NgayHopDong = getDate(item["NGAY_HDTM"]);
            //    tkmd.DiaDiemXepHang = item["CANGNN"].ToString();
            //    tkmd.SoKien = getDecimal(item["SO_KIEN"]);
            //    tkmd.SoContainer20 = getDecimal(item["SO_CONTAINER"]);
            //    tkmd.PhanLuong = item["PLUONG"].ToString();
            //    tkmd.SoHoaDonThuongMai = item["SO_HDTM"].ToString();
            //    tkmd.NgayHoanThanh = getDate(item["NGAY_HOANTHANH"]);
            //    tkmd.SoHang = item["SOHANG"].ToString() != "" ? short.Parse(item["SOHANG"].ToString()) : (short)0;

            //    #endregion
            //    tokhaiSyncs.Add(tkmd);
            //}
            //#endregion ToKhai
            //try
            //{

            //    npl.Saved(NguyenPhuLieuSyncs);
            //    sp.Saved(sanphamsSyncs);
            //    thongtinDm.Saved(thongtindinhmucs);
            //    dm.Saved(dinhmucsSyncs);
            //    tkmd.Saved(tokhaiSyncs);
            //    Globals.ShowMessage("Lưu thành công!", false);
            //}
            //catch (Exception ex)
            //{
            //    Globals.ShowMessage("Lưu không thành công!", false);
            //    Logger.LocalLogger.Instance().WriteMessage(ex);
            //}

        }

    }
}