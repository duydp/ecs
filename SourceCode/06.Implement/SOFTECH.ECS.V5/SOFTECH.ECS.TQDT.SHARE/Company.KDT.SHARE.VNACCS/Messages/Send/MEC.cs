﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class MEC : BasicVNACC
    {
        public StringBuilder BuidMessagesEdiMEE(StringBuilder strBuilder)
        {
            return this.BuildEdiMessages<MEC>(strBuilder, true, GlobalVNACC.PathConfig, "MEE");
        }
    }
}
