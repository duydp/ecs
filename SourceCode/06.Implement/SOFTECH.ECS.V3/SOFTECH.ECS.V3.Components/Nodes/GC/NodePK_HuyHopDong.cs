﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SOFTECH.ECS.V3.Components.Nodes.GC
{
    public class NodePK_HuyHopDong
    {
        /// <summary>
        /// Lý do hủy hợp đồng
        /// </summary>
        public const string AdditionalInformation = "AdditionalInformation";            
    }
}
