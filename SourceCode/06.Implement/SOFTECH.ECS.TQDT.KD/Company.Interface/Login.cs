﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;
//
using System.Collections;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Tab;
using System.Globalization;
using Janus.Windows.GridEX;
using Janus.Windows.ExplorerBar;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Resources;
using Janus.Windows.UI.Dock;
using System.Collections;


namespace Company.Interface
{
    public partial class Login : Form
    {
    
        protected Company.Controls.MessageBoxControl _MsgBox;
        public Login()
        {
            InitializeComponent();
        }
        public string setText(string vnText, string enText)
        {
            if (Properties.Settings.Default.NgonNgu == "1")
            {
                return enText.Trim();
            }
            else
            {
                return vnText.Trim();
            }
        }
        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new Company.Controls.MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog();
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.EcsQuanTri = ECSPrincipal.ValidateLogin(txtUser.Text.Trim(), txtMatKhau.Text.Trim());
                if (MainForm.EcsQuanTri == null)
                {
                    ShowMessage(setText("Đăng nhập không thành công.", "Login fail!"), false);
                    MainForm.isLoginSuccess = false;
                }
                else
                {
                    MainForm.isLoginSuccess = true;
                    this.Close();
                }
            }
            catch(Exception ex) {
                MessageBox.Show("Không kết nối được cơ sở dữ liệu : "+ex.Message);
                linkLabel1_LinkClicked(null, null);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ThietLapThongSoKBForm f = new ThietLapThongSoKBForm();
            f.ShowDialog();
        }
        private void Login_Load(object sender, EventArgs e)
        {
            txtUser.Text = txtMatKhau.Text = string.Empty;
            txtUser.Focus();
            try
            {
                uiButton3.Text= setText("Đăng nhập","Login");
                uiButton2.Text = setText("Thoát", "Exit");               
                linkLabel1.Text = setText("Cấu hình thông số kết nối", "Configuration of connected parameters");                
                if (Properties.Settings.Default.NgonNgu == "1")
                {
                    this.BackgroundImage = System.Drawing.Image.FromFile("LOGIN-KD-eng.png");
                }
                else
                {
                    this.BackgroundImage = System.Drawing.Image.FromFile("LOGIN-KD.png");
                }

                // this.BackgroundImageLayout = ImageLayout.Stretch;
            }
            catch { }

        }                
      
    }
}