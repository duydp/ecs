using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_TKVC_Container : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long Master_ID { set; get; }
		public string SotieuDe { set; get; }
		public string SoHieuContainer { set; get; }
		public string SoDongHangTrenTK { set; get; }
		public string SoSeal1 { set; get; }
		public string SoSeal2 { set; get; }
		public string SoSeal3 { set; get; }
		public string SoSeal4 { set; get; }
		public string SoSeal5 { set; get; }
		public string SoSeal6 { set; get; }
		public string Note { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_TKVC_Container> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_TKVC_Container> collection = new List<KDT_VNACC_TKVC_Container>();
			while (reader.Read())
			{
				KDT_VNACC_TKVC_Container entity = new KDT_VNACC_TKVC_Container();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SotieuDe"))) entity.SotieuDe = reader.GetString(reader.GetOrdinal("SotieuDe"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHieuContainer"))) entity.SoHieuContainer = reader.GetString(reader.GetOrdinal("SoHieuContainer"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDongHangTrenTK"))) entity.SoDongHangTrenTK = reader.GetString(reader.GetOrdinal("SoDongHangTrenTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeal1"))) entity.SoSeal1 = reader.GetString(reader.GetOrdinal("SoSeal1"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeal2"))) entity.SoSeal2 = reader.GetString(reader.GetOrdinal("SoSeal2"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeal3"))) entity.SoSeal3 = reader.GetString(reader.GetOrdinal("SoSeal3"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeal4"))) entity.SoSeal4 = reader.GetString(reader.GetOrdinal("SoSeal4"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeal5"))) entity.SoSeal5 = reader.GetString(reader.GetOrdinal("SoSeal5"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeal6"))) entity.SoSeal6 = reader.GetString(reader.GetOrdinal("SoSeal6"));
				if (!reader.IsDBNull(reader.GetOrdinal("Note"))) entity.Note = reader.GetString(reader.GetOrdinal("Note"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_TKVC_Container> collection, long id)
        {
            foreach (KDT_VNACC_TKVC_Container item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_TKVC_Container VALUES(@Master_ID, @SotieuDe, @SoHieuContainer, @SoDongHangTrenTK, @SoSeal1, @SoSeal2, @SoSeal3, @SoSeal4, @SoSeal5, @SoSeal6, @Note)";
            string update = "UPDATE t_KDT_VNACC_TKVC_Container SET Master_ID = @Master_ID, SotieuDe = @SotieuDe, SoHieuContainer = @SoHieuContainer, SoDongHangTrenTK = @SoDongHangTrenTK, SoSeal1 = @SoSeal1, SoSeal2 = @SoSeal2, SoSeal3 = @SoSeal3, SoSeal4 = @SoSeal4, SoSeal5 = @SoSeal5, SoSeal6 = @SoSeal6, Note = @Note WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TKVC_Container WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SotieuDe", SqlDbType.VarChar, "SotieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuContainer", SqlDbType.VarChar, "SoHieuContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDongHangTrenTK", SqlDbType.VarChar, "SoDongHangTrenTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal1", SqlDbType.VarChar, "SoSeal1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal2", SqlDbType.VarChar, "SoSeal2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal3", SqlDbType.VarChar, "SoSeal3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal4", SqlDbType.VarChar, "SoSeal4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal5", SqlDbType.VarChar, "SoSeal5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal6", SqlDbType.VarChar, "SoSeal6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Note", SqlDbType.VarChar, "Note", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SotieuDe", SqlDbType.VarChar, "SotieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuContainer", SqlDbType.VarChar, "SoHieuContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDongHangTrenTK", SqlDbType.VarChar, "SoDongHangTrenTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal1", SqlDbType.VarChar, "SoSeal1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal2", SqlDbType.VarChar, "SoSeal2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal3", SqlDbType.VarChar, "SoSeal3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal4", SqlDbType.VarChar, "SoSeal4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal5", SqlDbType.VarChar, "SoSeal5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal6", SqlDbType.VarChar, "SoSeal6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Note", SqlDbType.VarChar, "Note", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_TKVC_Container VALUES(@Master_ID, @SotieuDe, @SoHieuContainer, @SoDongHangTrenTK, @SoSeal1, @SoSeal2, @SoSeal3, @SoSeal4, @SoSeal5, @SoSeal6, @Note)";
            string update = "UPDATE t_KDT_VNACC_TKVC_Container SET Master_ID = @Master_ID, SotieuDe = @SotieuDe, SoHieuContainer = @SoHieuContainer, SoDongHangTrenTK = @SoDongHangTrenTK, SoSeal1 = @SoSeal1, SoSeal2 = @SoSeal2, SoSeal3 = @SoSeal3, SoSeal4 = @SoSeal4, SoSeal5 = @SoSeal5, SoSeal6 = @SoSeal6, Note = @Note WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_TKVC_Container WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SotieuDe", SqlDbType.VarChar, "SotieuDe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHieuContainer", SqlDbType.VarChar, "SoHieuContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDongHangTrenTK", SqlDbType.VarChar, "SoDongHangTrenTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal1", SqlDbType.VarChar, "SoSeal1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal2", SqlDbType.VarChar, "SoSeal2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal3", SqlDbType.VarChar, "SoSeal3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal4", SqlDbType.VarChar, "SoSeal4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal5", SqlDbType.VarChar, "SoSeal5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal6", SqlDbType.VarChar, "SoSeal6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Note", SqlDbType.VarChar, "Note", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Master_ID", SqlDbType.BigInt, "Master_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SotieuDe", SqlDbType.VarChar, "SotieuDe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHieuContainer", SqlDbType.VarChar, "SoHieuContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDongHangTrenTK", SqlDbType.VarChar, "SoDongHangTrenTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal1", SqlDbType.VarChar, "SoSeal1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal2", SqlDbType.VarChar, "SoSeal2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal3", SqlDbType.VarChar, "SoSeal3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal4", SqlDbType.VarChar, "SoSeal4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal5", SqlDbType.VarChar, "SoSeal5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal6", SqlDbType.VarChar, "SoSeal6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Note", SqlDbType.VarChar, "Note", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_TKVC_Container Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_Container_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_TKVC_Container> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_TKVC_Container> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_TKVC_Container> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_Container_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_Container_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_Container_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_TKVC_Container_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_TKVC_Container(long master_ID, string sotieuDe, string soHieuContainer, string soDongHangTrenTK, string soSeal1, string soSeal2, string soSeal3, string soSeal4, string soSeal5, string soSeal6, string note)
		{
			KDT_VNACC_TKVC_Container entity = new KDT_VNACC_TKVC_Container();	
			entity.Master_ID = master_ID;
			entity.SotieuDe = sotieuDe;
			entity.SoHieuContainer = soHieuContainer;
			entity.SoDongHangTrenTK = soDongHangTrenTK;
			entity.SoSeal1 = soSeal1;
			entity.SoSeal2 = soSeal2;
			entity.SoSeal3 = soSeal3;
			entity.SoSeal4 = soSeal4;
			entity.SoSeal5 = soSeal5;
			entity.SoSeal6 = soSeal6;
			entity.Note = note;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_Container_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SotieuDe", SqlDbType.VarChar, SotieuDe);
			db.AddInParameter(dbCommand, "@SoHieuContainer", SqlDbType.VarChar, SoHieuContainer);
			db.AddInParameter(dbCommand, "@SoDongHangTrenTK", SqlDbType.VarChar, SoDongHangTrenTK);
			db.AddInParameter(dbCommand, "@SoSeal1", SqlDbType.VarChar, SoSeal1);
			db.AddInParameter(dbCommand, "@SoSeal2", SqlDbType.VarChar, SoSeal2);
			db.AddInParameter(dbCommand, "@SoSeal3", SqlDbType.VarChar, SoSeal3);
			db.AddInParameter(dbCommand, "@SoSeal4", SqlDbType.VarChar, SoSeal4);
			db.AddInParameter(dbCommand, "@SoSeal5", SqlDbType.VarChar, SoSeal5);
			db.AddInParameter(dbCommand, "@SoSeal6", SqlDbType.VarChar, SoSeal6);
			db.AddInParameter(dbCommand, "@Note", SqlDbType.VarChar, Note);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_TKVC_Container> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_Container item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_TKVC_Container(long id, long master_ID, string sotieuDe, string soHieuContainer, string soDongHangTrenTK, string soSeal1, string soSeal2, string soSeal3, string soSeal4, string soSeal5, string soSeal6, string note)
		{
			KDT_VNACC_TKVC_Container entity = new KDT_VNACC_TKVC_Container();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SotieuDe = sotieuDe;
			entity.SoHieuContainer = soHieuContainer;
			entity.SoDongHangTrenTK = soDongHangTrenTK;
			entity.SoSeal1 = soSeal1;
			entity.SoSeal2 = soSeal2;
			entity.SoSeal3 = soSeal3;
			entity.SoSeal4 = soSeal4;
			entity.SoSeal5 = soSeal5;
			entity.SoSeal6 = soSeal6;
			entity.Note = note;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_TKVC_Container_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SotieuDe", SqlDbType.VarChar, SotieuDe);
			db.AddInParameter(dbCommand, "@SoHieuContainer", SqlDbType.VarChar, SoHieuContainer);
			db.AddInParameter(dbCommand, "@SoDongHangTrenTK", SqlDbType.VarChar, SoDongHangTrenTK);
			db.AddInParameter(dbCommand, "@SoSeal1", SqlDbType.VarChar, SoSeal1);
			db.AddInParameter(dbCommand, "@SoSeal2", SqlDbType.VarChar, SoSeal2);
			db.AddInParameter(dbCommand, "@SoSeal3", SqlDbType.VarChar, SoSeal3);
			db.AddInParameter(dbCommand, "@SoSeal4", SqlDbType.VarChar, SoSeal4);
			db.AddInParameter(dbCommand, "@SoSeal5", SqlDbType.VarChar, SoSeal5);
			db.AddInParameter(dbCommand, "@SoSeal6", SqlDbType.VarChar, SoSeal6);
			db.AddInParameter(dbCommand, "@Note", SqlDbType.VarChar, Note);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_TKVC_Container> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_Container item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_TKVC_Container(long id, long master_ID, string sotieuDe, string soHieuContainer, string soDongHangTrenTK, string soSeal1, string soSeal2, string soSeal3, string soSeal4, string soSeal5, string soSeal6, string note)
		{
			KDT_VNACC_TKVC_Container entity = new KDT_VNACC_TKVC_Container();			
			entity.ID = id;
			entity.Master_ID = master_ID;
			entity.SotieuDe = sotieuDe;
			entity.SoHieuContainer = soHieuContainer;
			entity.SoDongHangTrenTK = soDongHangTrenTK;
			entity.SoSeal1 = soSeal1;
			entity.SoSeal2 = soSeal2;
			entity.SoSeal3 = soSeal3;
			entity.SoSeal4 = soSeal4;
			entity.SoSeal5 = soSeal5;
			entity.SoSeal6 = soSeal6;
			entity.Note = note;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_Container_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, Master_ID);
			db.AddInParameter(dbCommand, "@SotieuDe", SqlDbType.VarChar, SotieuDe);
			db.AddInParameter(dbCommand, "@SoHieuContainer", SqlDbType.VarChar, SoHieuContainer);
			db.AddInParameter(dbCommand, "@SoDongHangTrenTK", SqlDbType.VarChar, SoDongHangTrenTK);
			db.AddInParameter(dbCommand, "@SoSeal1", SqlDbType.VarChar, SoSeal1);
			db.AddInParameter(dbCommand, "@SoSeal2", SqlDbType.VarChar, SoSeal2);
			db.AddInParameter(dbCommand, "@SoSeal3", SqlDbType.VarChar, SoSeal3);
			db.AddInParameter(dbCommand, "@SoSeal4", SqlDbType.VarChar, SoSeal4);
			db.AddInParameter(dbCommand, "@SoSeal5", SqlDbType.VarChar, SoSeal5);
			db.AddInParameter(dbCommand, "@SoSeal6", SqlDbType.VarChar, SoSeal6);
			db.AddInParameter(dbCommand, "@Note", SqlDbType.VarChar, Note);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_TKVC_Container> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_Container item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_TKVC_Container(long id)
		{
			KDT_VNACC_TKVC_Container entity = new KDT_VNACC_TKVC_Container();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_Container_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_TKVC_Container_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_TKVC_Container> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_TKVC_Container item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}