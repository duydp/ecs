﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.BLL.KDT;

namespace DongBoDuLieu.BLL.TKMD_V3
{
    public class TQDT_ToKhaiMauDich
    {
        public static long InsertUpdateTKMD_SXXK(DataRow drTKMD, SqlTransaction transaction, SqlDatabase db)
        {
            ToKhaiMauDich tkmd = ConvertToTKMD(drTKMD);
            string where = @"MaLoaiHinh = '"+tkmd.MaLoaiHinh.Trim()+"' AND SoToKhai = "+tkmd.SoToKhai+" AND Year(NgayDangKy) = "+tkmd.NgayDangKy.Year
                + " AND MaDoanhNghiep = '"+tkmd.MaDoanhNghiep.Trim()+"' AND MaHaiQuan = '"+tkmd.MaHaiQuan.Trim()+"'";
            ToKhaiMauDichCollection tkmdCollection = (new ToKhaiMauDich()).SelectCollectionDynamicDBDL(where,null,transaction,db);
            if (tkmdCollection != null && tkmdCollection.Count > 0)
            {
                ToKhaiMauDich tkmdOld = tkmdCollection[0];
              
                    tkmdOld.SoTiepNhan = tkmd.SoTiepNhan;
                    tkmdOld.TrangThaiXuLy = 1;
                    tkmdOld.PhanLuong = tkmd.PhanLuong;
                    switch (tkmd.PhanLuong)
                    {
                        case "1":
                            tkmdOld.HUONGDAN = "Chấp nhận thông quan " + tkmd.HUONGDAN;
                            break; 
                        case "2":
                            tkmdOld.HUONGDAN = "Đề nghị xuất trình hồ sơ điện tử" + tkmd.HUONGDAN;
                            break;
                        case "3":
                            tkmdOld.HUONGDAN = tkmd.HUONGDAN;
                            break;
                    }
                    
                    tkmdOld.DeXuatKhac = tkmd.DeXuatKhac;
                    tkmdOld.SoVanDon = tkmd.SoVanDon;
                    tkmdOld.NgayVanDon = tkmd.NgayVanDon;
                    tkmdOld.UpdateTransactionV3(transaction, db);
                return tkmdOld.ID;
            }
            else
                return 0;
        }


        private static ToKhaiMauDich ConvertToTKMD(DataRow drTKMD)
        {
            ToKhaiMauDich tkmd = new ToKhaiMauDich();
            tkmd.MaHaiQuan = drTKMD["MA_HQ"].ToString();
            tkmd.SoTiepNhan = System.Convert.ToInt32(drTKMD["SOTN"]);
            tkmd.SoToKhai = System.Convert.ToInt32(drTKMD["SoTKChinhThuc"]);
            tkmd.MaLoaiHinh = drTKMD["MA_LH"].ToString();
            tkmd.MaDoanhNghiep = drTKMD["MA_DV"].ToString();
            tkmd.MaDaiLyTTHQ = drTKMD["ecsMa_DL"].ToString();
            tkmd.TenDaiLyTTHQ = drTKMD["ecsTen_DL"].ToString();
            tkmd.TenDonViDoiTac = drTKMD["DV_DT"].ToString();
            tkmd.PTVT_ID = drTKMD["MA_PTVT"].ToString();
            tkmd.NgayDangKy = string.IsNullOrEmpty(drTKMD["NGAY_DK"].ToString()) ? default(DateTime) : Convert.ToDateTime(drTKMD["NGAY_DK"].ToString());
            tkmd.NgayTiepNhan = string.IsNullOrEmpty(drTKMD["NGAY_DK"].ToString()) ? default(DateTime) : Convert.ToDateTime(drTKMD["NGAY_DK"].ToString());
            if (!string.IsNullOrEmpty(drTKMD["NGAYDEN"].ToString()))
                tkmd.NgayDenPTVT = Convert.ToDateTime(drTKMD["NGAYDEN"].ToString());

            if (!string.IsNullOrEmpty(drTKMD["SO_GP"].ToString()))
            {
                tkmd.SoGiayPhep = drTKMD["SO_GP"].ToString();
                tkmd.NgayGiayPhep = Convert.ToDateTime(drTKMD["NGAY_GP"].ToString());
                tkmd.NgayHetHanGiayPhep = Convert.ToDateTime(drTKMD["NGAY_HHGP"].ToString());
            }
            if (!string.IsNullOrEmpty(drTKMD["SO_HD"].ToString()))
            {
                tkmd.SoHopDong = drTKMD["SO_HD"].ToString();
                tkmd.NgayHopDong = Convert.ToDateTime(drTKMD["NGAY_HD"].ToString());
                tkmd.NgayHetHanHopDong = Convert.ToDateTime(drTKMD["NGAY_HHHD"].ToString());
            }

            if (!string.IsNullOrEmpty(drTKMD["SO_HDTM"].ToString()))
            {
                tkmd.SoHoaDonThuongMai = drTKMD["SO_HDTM"].ToString();
                tkmd.NgayHoaDonThuongMai = Convert.ToDateTime(drTKMD["NGAY_HDTM"].ToString());

            }
            if (!string.IsNullOrEmpty(drTKMD["VAN_DON"].ToString()))
            {
                tkmd.SoVanDon = drTKMD["VAN_DON"].ToString();
                tkmd.NgayVanDon = Convert.ToDateTime(drTKMD["NGAY_VANDON"].ToString());
            }
            tkmd.NuocNK_ID = drTKMD["NUOC_NK"].ToString();
            tkmd.NuocXK_ID = drTKMD["NUOC_XK"].ToString();
            tkmd.CuaKhau_ID = drTKMD["MA_CK"].ToString();
            tkmd.DKGH_ID = drTKMD["MA_GH"].ToString();
            tkmd.NguyenTe_ID = drTKMD["MA_NT"].ToString();
            tkmd.TyGiaTinhThue = System.Convert.ToDecimal(drTKMD["TYGIA_VND"]);
            tkmd.TyGiaUSD = System.Convert.ToDecimal(drTKMD["TyGia_USD"]);
            tkmd.PTTT_ID = drTKMD["MA_PTTT"].ToString();

            tkmd.SoHang = System.Convert.ToInt16(drTKMD["SOHANG"]);
            tkmd.SoLuongPLTK = System.Convert.ToInt16(drTKMD["SO_PLTK"]);
            tkmd.TenChuHang = drTKMD["DAIDIEN_DN"].ToString();
            tkmd.SoContainer20 = System.Convert.ToDecimal(drTKMD["SO_CONTAINER"]);
            tkmd.SoContainer40 = System.Convert.ToDecimal(drTKMD["SO_CONTAINER40"]);
            tkmd.SoKien = System.Convert.ToDecimal(drTKMD["SO_KIEN"]);
            tkmd.TrongLuong = System.Convert.ToDecimal(drTKMD["TR_LUONG"]);
            tkmd.TongTriGiaKhaiBao = System.Convert.ToDecimal(drTKMD["TONGTGKB"]);
            tkmd.TongTriGiaTinhThue = System.Convert.ToDecimal(drTKMD["TONGTGTT"]);
            tkmd.LePhiHaiQuan = System.Convert.ToDecimal(drTKMD["LEPHI_HQ"]);
            tkmd.PhiBaoHiem = System.Convert.ToDecimal(drTKMD["PHI_BH"]);
            tkmd.PhiVanChuyen = System.Convert.ToDecimal(drTKMD["PHI_VC"]);
            tkmd.MaDonViUT = drTKMD["Ma_DVUT"].ToString();
            tkmd.TenDonViUT = drTKMD["ecsTen_DVUT"].ToString();
           // tkmd.TrongLuongNet = System.Convert.ToDouble(drTKMD["ecsTrong_Luong_Tinh"]);
            tkmd.PhanLuong = drTKMD["PhanLuong"].ToString();
            tkmd.DeXuatKhac = drTKMD["DeXuatKhac"].ToString();
            tkmd.NamDK = System.Convert.ToInt32(drTKMD["NamDK"]);
            tkmd.HUONGDAN = drTKMD["LyDoTDLuong"].ToString();

            return tkmd;
        }

        /// <summary>
        /// Do WS trả về chuẩn UTF-8 nên cần phải convert sang Unicode
        /// </summary>
        /// <param name="StrUtf8"> Nhập vào chuỗi  Encode UTF 8</param>
        /// <returns> Trả về chuỗi Encode Unicode</returns>
        private static string ConvertEncoding(string StrUtf8)
        {
            byte[] ByteUtf8;
            byte[] ByteUnicode;
            ByteUtf8 = GetRawBytes(StrUtf8);
            ByteUnicode = Encoding.Convert(Encoding.UTF8, Encoding.Unicode, ByteUtf8);

            return Encoding.Unicode.GetString(ByteUnicode);

        }
        /// <summary>
        /// String to Byte
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        private static byte[] GetRawBytes(string str)
        {

            int charcount = str.Length;

            byte[] byttemp = new byte[charcount];



            for (int i = 0; i < charcount; i++)
            {

                byttemp[i] = (byte)str[i];

            }



            return byttemp;

        }
    }
}
