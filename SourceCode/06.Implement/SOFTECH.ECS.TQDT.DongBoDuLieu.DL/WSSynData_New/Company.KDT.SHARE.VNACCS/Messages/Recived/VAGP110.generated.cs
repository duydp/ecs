using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class VAGP110 : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute SBC { get; set; }
public PropertiesAttribute SBN { get; set; }
public PropertiesAttribute ONO { get; set; }
public PropertiesAttribute FNC { get; set; }
public PropertiesAttribute PRT { get; set; }
public PropertiesAttribute DOS { get; set; }
public PropertiesAttribute OAC { get; set; }
public PropertiesAttribute OAN { get; set; }
public PropertiesAttribute IMC { get; set; }
public PropertiesAttribute IMN { get; set; }
public PropertiesAttribute IPC { get; set; }
public PropertiesAttribute IAD { get; set; }
public PropertiesAttribute ICC { get; set; }
public PropertiesAttribute IPN { get; set; }
public PropertiesAttribute IFX { get; set; }
public PropertiesAttribute IEM { get; set; }
public PropertiesAttribute FSC { get; set; }
public PropertiesAttribute GCN { get; set; }
public PropertiesAttribute EPC { get; set; }
public PropertiesAttribute EPN { get; set; }
public PropertiesAttribute AD { get; set; }
public PropertiesAttribute RMA { get; set; }
public PropertiesAttribute SUD { get; set; }
public PropertiesAttribute PIC { get; set; }

public VAGP110()
        {
SBC = new PropertiesAttribute(13, typeof(string));
SBN = new PropertiesAttribute(300, typeof(string));
ONO = new PropertiesAttribute(12, typeof(int));
FNC = new PropertiesAttribute(1, typeof(int));
PRT = new PropertiesAttribute(4, typeof(string));
DOS = new PropertiesAttribute(8, typeof(DateTime));
OAC = new PropertiesAttribute(6, typeof(string));
OAN = new PropertiesAttribute(300, typeof(string));
IMC = new PropertiesAttribute(13, typeof(string));
IMN = new PropertiesAttribute(300, typeof(string));
IPC = new PropertiesAttribute(7, typeof(string));
IAD = new PropertiesAttribute(300, typeof(string));
ICC = new PropertiesAttribute(2, typeof(string));
IPN = new PropertiesAttribute(20, typeof(string));
IFX = new PropertiesAttribute(20, typeof(string));
IEM = new PropertiesAttribute(210, typeof(string));
FSC = new PropertiesAttribute(17, typeof(string));
GCN = new PropertiesAttribute(17, typeof(string));
EPC = new PropertiesAttribute(6, typeof(string));
EPN = new PropertiesAttribute(35, typeof(string));
AD = new PropertiesAttribute(750, typeof(string));
RMA = new PropertiesAttribute(996, typeof(string));
SUD = new PropertiesAttribute(8, typeof(DateTime));
PIC = new PropertiesAttribute(300, typeof(string));
TongSoByte = 3693;
}

}public partial class EnumGroupID
    {
}

}
