﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC02NewForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public DataSet dsSanPham;
        public DataSet dsSanPhamTemp;
        public DataTable dtBangSanPham;
        private BangKe02New BC02;
        public HopDong HD = new HopDong();
        Company.GC.BLL.GC.SanPham SP;
        public XRLabel label;
        int sotable;
        int soLuongSP;
        public ReportViewBC02NewForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            TKMD = new ToKhaiMauDich();
            dsSanPham = new DataSet();
            dtBangSanPham = new DataTable();
            BC02 = new BangKe02New();
            SP = new Company.GC.BLL.GC.SanPham();
            label = new XRLabel();
            dsSanPhamTemp = TKMD.GetSPXuatAll(this.HD.ID);
            soLuongSP = dsSanPhamTemp.Tables[0].Rows.Count;
            sotable = (soLuongSP - 1) / 6 + 1;
            this.BC02.report = this;
            for (int i = 0; i < this.sotable; i++)
            {
                cbPage.Items.Add("Trang " + (i + 1), i);
            }
            if(cbPage.Items.Count > 0)
                cbPage.SelectedIndex = 0;

            btnNext.Enabled = !this.BC02.Last;
            btnPrivious.Enabled = !this.BC02.First;
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BC02.First = (cbPage.SelectedIndex == 0);
            this.BC02.Last = (cbPage.SelectedIndex == (sotable - 1));
            this.BC02.HD = this.HD;

            btnNext.Enabled = !this.BC02.Last;
            btnPrivious.Enabled = !this.BC02.First;

            if (dsSanPham.Tables["dtBangSanPham" + cbPage.SelectedIndex] != null)
                this.BC02.BindReport(this.dsSanPham.Tables["dtBangSanPham" + cbPage.SelectedIndex]);
            else
            {
                dtBangSanPham = new DataTable("dtBangSanPham" + cbPage.SelectedIndex);
                dtBangSanPham = CreatSchemaDataSetNewRP02(cbPage.SelectedIndex);
                this.BC02.BindReport(this.dtBangSanPham);
                dsSanPham.Tables.Add(dtBangSanPham);
                
            }
            printControl1.PrintingSystem = this.BC02.PrintingSystem;
            this.BC02.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }
        }


        public DataTable CreatSchemaDataSetNewRP02(int BangSo)
        {
            DataTable dttemp = new DataTable("dtBangSanPham" + BangSo.ToString());
            DataColumn[] dcCol = new DataColumn[8];

            dcCol[0] = new DataColumn("TKXK", typeof(string));
            dcCol[0].Caption = "TKXK";

            int t = 1;
            for (int k = BangSo * 6; k < (BangSo + 1) * 6; k++)
            {

                if (k < soLuongSP)
                {
                    string Ma = dsSanPhamTemp.Tables[0].Rows[k].ItemArray[0].ToString();
                    string DVT_ID = SP.GetDVTByMa(Ma,this.HD.ID);

                    /* DataSet tạm để lấy tên sản phẩm */
                    DataSet dsSP = SP.GetSP(this.HD.ID, Ma);
                    string Ten = "";
                    if(dsSP.Tables[0].Rows.Count > 0)
                         Ten = dsSP.Tables[0].Rows[0].ItemArray[0].ToString();

                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "Mã " + Ma;
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = " Tên : " + Ten + "\r\n Mã : " + Ma + "\r\n ĐV tính : " + DonViTinh_GetName(DVT_ID);
                    t++;
                }
                else
                {
                    dcCol[t] = new DataColumn();
                    dcCol[t].ColumnName = "";
                    dcCol[t].DataType = typeof(decimal);
                    dcCol[t].Caption = "";
                    t++;
                }
            }

            dcCol[t] = new DataColumn();
            dcCol[t].ColumnName = "GhiChu";
            dcCol[t].DataType = typeof(string);
            dcCol[t].Caption = "GhiChu";
            dttemp.Columns.AddRange(dcCol);


            ToKhaiMauDichCollection TKMDCol = this.HD.GetTKXKBCSanPhamNew();

            foreach (ToKhaiMauDich TKMD in TKMDCol)
            {
                TKMD.LoadHMDCollection();
                DataRow drData = dttemp.NewRow();
                foreach (HangMauDich HMD in TKMD.HMDCollection)
                {
                    drData[0] = TKMD.SoToKhai.ToString().Trim() + "/" + LoaiHinhMauDich_GetTenVT(TKMD.MaLoaiHinh).Trim() + "/" + TKMD.MaHaiQuan + "\r\n" + TKMD.NgayDangKy.ToShortDateString();
                    for (int n = 1; n <= 6; n++)
                    {
                        string ColName = "Mã " + HMD.MaPhu;
                        if (dttemp.Columns[n].ColumnName == ColName)
                            drData[n] = HMD.SoLuong;
                    }
                }
                for (int n = 1; n <= 6; n++)
                {
                    if (drData[n].ToString() != "")
                    {
                        dttemp.Rows.Add(drData);
                        break;
                    }
                }

            }

            List<ToKhaiChuyenTiep> TKCTCol = this.HD.GetTKCTXuatSP();

            foreach (ToKhaiChuyenTiep TKCT in TKCTCol)
            {
                TKCT.LoadHCTCollection();
                DataRow drData = dttemp.NewRow();
                foreach (HangChuyenTiep HCT in TKCT.HCTCollection)
                {
                    drData[0] = TKCT.SoToKhai.ToString().Trim() +"-" + TKCT.MaLoaiHinh.Trim() + "-" + DateTime.Parse(TKCT.NgayDangKy.ToString().Trim()).Year;
                    for (int n = 1; n <= 6; n++)
                    {
                        string ColName = "Mã " + HCT.MaHang;
                        if (dttemp.Columns[n].ColumnName == ColName)
                            drData[n] = HCT.SoLuong;
                    }
                }
                for (int n = 1; n <= 6; n++)
                {
                    if (drData[n].ToString() != "")
                    {
                        dttemp.Rows.Add(drData);
                        break;
                    }
                }

            }

            return dttemp;
        }

        private void btnPrivious_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex--;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            cbPage.SelectedIndex++;
            cboToKhai_SelectedIndexChanged(null, null);
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.BC02.setText(this.label, txtName.Text);
            this.BC02.CreateDocument();
        }
    }
}