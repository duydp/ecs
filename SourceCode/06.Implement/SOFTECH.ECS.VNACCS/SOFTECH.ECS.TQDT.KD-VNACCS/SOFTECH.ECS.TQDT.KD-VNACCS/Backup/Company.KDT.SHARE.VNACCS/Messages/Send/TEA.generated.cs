using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

public partial class TEA : BasicVNACC
    {
public static int TongSoByte { get; set; }
public PropertiesAttribute TEN { get; set; }
public PropertiesAttribute IEC { get; set; }
public PropertiesAttribute CH { get; set; }
public PropertiesAttribute IMA { get; set; }
public PropertiesAttribute IMT { get; set; }
public PropertiesAttribute TED { get; set; }
public PropertiesAttribute IPN { get; set; }
public PropertiesAttribute PPC { get; set; }
public PropertiesAttribute PO { get; set; }
public PropertiesAttribute RE { get; set; }
public PropertiesAttribute SEL { get; set; }
public PropertiesAttribute PTI { get; set; }
public PropertiesAttribute ICN { get; set; }
public PropertiesAttribute DOC { get; set; }
public PropertiesAttribute IB { get; set; }
public GroupAttribute IC_ { get; set; }
public GroupAttribute C__ { get; set; }
public PropertiesAttribute IR { get; set; }
public PropertiesAttribute CRP { get; set; }

public TEA()
        {
TEN = new PropertiesAttribute(12, typeof(int));
IEC = new PropertiesAttribute(1, typeof(string));
CH = new PropertiesAttribute(6, typeof(string));
IMA = new PropertiesAttribute(300, typeof(string));
IMT = new PropertiesAttribute(20, typeof(string));
TED = new PropertiesAttribute(8, typeof(DateTime));
IPN = new PropertiesAttribute(210, typeof(string));
PPC = new PropertiesAttribute(300, typeof(string));
PO = new PropertiesAttribute(300, typeof(string));
RE = new PropertiesAttribute(5, typeof(string));
SEL = new PropertiesAttribute(300, typeof(string));
PTI = new PropertiesAttribute(8, typeof(DateTime));
ICN = new PropertiesAttribute(20, typeof(string));
DOC = new PropertiesAttribute(8, typeof(DateTime));
IB = new PropertiesAttribute(300, typeof(string));
IR = new PropertiesAttribute(765, typeof(string));
CRP = new PropertiesAttribute(420, typeof(string));
#region IC_
List<PropertiesAttribute> listIC_ = new List<PropertiesAttribute>();
listIC_.Add(new PropertiesAttribute(1, 5, EnumGroupID.TEA_IC_, typeof(int)));
listIC_.Add(new PropertiesAttribute(20, 5, EnumGroupID.TEA_CA_, typeof(string)));
listIC_.Add(new PropertiesAttribute(8, 5, EnumGroupID.TEA_DA_, typeof(DateTime)));
listIC_.Add(new PropertiesAttribute(300, 5, EnumGroupID.TEA_AI_, typeof(string)));
IC_ = new GroupAttribute("IC_", 5, listIC_);
#endregion IC_
#region C__
List<PropertiesAttribute> listC__ = new List<PropertiesAttribute>();
listC__.Add(new PropertiesAttribute(13, 15, EnumGroupID.TEA_C__, typeof(string)));
listC__.Add(new PropertiesAttribute(300, 15, EnumGroupID.TEA_N__, typeof(string)));
C__ = new GroupAttribute("C__", 15, listC__);
#endregion C__
TongSoByte = 9457;
}

}public partial class EnumGroupID
    {
public static readonly string TEA_IC_ = "TEA_IC_";
public static readonly string TEA_CA_ = "TEA_CA_";
public static readonly string TEA_DA_ = "TEA_DA_";
public static readonly string TEA_AI_ = "TEA_AI_";
public static readonly string TEA_C__ = "TEA_C__";
public static readonly string TEA_N__ = "TEA_N__";
}

}
