using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhThongQuanHangHoaXuatKhau_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public QuyetDinhThongQuanHangHoaXuatKhau_2()
        {
            InitializeComponent();
        }
        public void LoadBanXacNhanNoiDungToKhaiHangHoaXuatKhau_2()
        {
            try
            {
DataTable dt = new DataTable();
            dt.Columns.Add("Date", typeof(string));
            dt.Columns.Add("Ten", typeof(string));
            dt.Columns.Add("NoiDung", typeof(string));
            dt.Columns.Add("STT", typeof(string));
            int STT = 0;
            while (dt.Rows.Count < 10)
            {
                STT++;
                DataRow dr = dt.NewRow();
                dr["Date"] = "dd/MM/yyyy";
                dr["Ten"] = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWWE";
                dr["NoiDung"] = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8WWWWWWWWW9WWWWWWWWW0WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWWE";
                dr["STT"] = STT.ToString();
                dt.Rows.Add(dr);
            }
            DetailReport1.DataSource = dt;
            lblNgaychithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "Date");
            lblTenchithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "Ten");
            lblNoidungchithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "NoiDung");
            lblSTTChiThiHQ.DataBindings.Add("Text", DetailReport1.DataSource, "STT");
            }
            catch (Exception e)
            {
                
               
            }
            
        }
    }
}
