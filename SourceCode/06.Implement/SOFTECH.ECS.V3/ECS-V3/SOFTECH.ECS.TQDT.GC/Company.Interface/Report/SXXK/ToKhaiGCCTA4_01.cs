﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report.SXXK
{
    public partial class ToKhaiGCCTA4_01 : DevExpress.XtraReports.UI.XtraReport
    {
        public Report.ReportViewGCCTA4Form report;
       // public ToKhaiMauDich TKCT = new ToKhaiMauDich();
        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        public int temp = 1;
        public bool BanLuuHaiQuan = false;
        public ToKhaiGCCTA4_01()
        {
            InitializeComponent();
        }
        //public decimal TinhTongThueHMD()
        //{
        //    decimal tong = 0;
        //    foreach (HangMauDich hmd in this.TKCT.HMDCollection)
        //        tong += hmd.ThueXNK;
        //    return tong;
        //}
        private string ToStringForReport(string s)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + "    ";
            temp += s[s.Length - 1];
            return temp;
        }
        public void setVisibleImage(bool t)
        {
            //ptbImage.Visible = t;
            //lblBanLuuHaiQuan.Visible = t;
        }
        public void setNhomHang(string tenNhomHang)
        {
            switch (temp)
            {
                case 1:
                    TenHang1.Text = tenNhomHang;
                    break;
                case 2:
                    TenHang2.Text = tenNhomHang;
                    break;
                case 3:
                    TenHang3.Text = tenNhomHang;
                    break;
                case 4:
                    TenHang4.Text = tenNhomHang;
                    break;
                case 5:
                    TenHang5.Text = tenNhomHang;
                    break;
                case 6:
                    TenHang6.Text = tenNhomHang;
                    break;
                case 7:
                    TenHang7.Text = tenNhomHang;
                    break;
                case 8:
                    TenHang8.Text = tenNhomHang;
                    break;
                case 9:
                    TenHang9.Text = tenNhomHang;
                    break;
                case 10:
                    TenHang10.Text = tenNhomHang;
                    break;
                case 11:
                    TenHang11.Text = tenNhomHang;
                    break;
                case 12:
                    TenHang12.Text = tenNhomHang;
                    break;
                case 13:
                    TenHang13.Text = tenNhomHang;
                    break;
                case 14:
                    TenHang14.Text = tenNhomHang;
                    break;
                case 15:
                    TenHang15.Text = tenNhomHang;
                    break;
                case 16:
                    TenHang16.Text = tenNhomHang;
                    break;
                case 17:
                    TenHang17.Text = tenNhomHang;
                    break;
                case 18:
                    TenHang18.Text = tenNhomHang;
                    break;
                case 19:
                    TenHang19.Text = tenNhomHang;
                    break;
            }


        }

        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
    }
}
