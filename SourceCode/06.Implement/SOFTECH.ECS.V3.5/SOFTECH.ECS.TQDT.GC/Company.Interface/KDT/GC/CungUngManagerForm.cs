﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
using SOFTECH.ECS.TQDT.GC.KDT.GC;
namespace Company.Interface.KDT.GC
{
    public partial class CungUngManagerForm : BaseForm
    {
       public KDT_GC_CungUngDangKy CungUngDangky = new KDT_GC_CungUngDangKy();
        List<KDT_GC_CungUngDangKy> CungUngDKList = new List<KDT_GC_CungUngDangKy>();
        public HopDong HD = new HopDong();
        public CungUngManagerForm()
        {
            InitializeComponent();
        }

      
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                int id= Convert.ToInt32(dgList.CurrentRow.Cells["ID"].Value);
                CungUngForm frm = new CungUngForm();
                frm.HD = this.HD;
                frm.CungUngDK = KDT_GC_CungUngDangKy.LoadFull(id);
                frm.ShowDialog();
            }
        }

        private void CungUngManagerForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            if (HD.ID > 0)
                CungUngDKList = KDT_GC_CungUngDangKy.SelectCollectionDynamic("HopDong_ID = " + HD.ID, "");
            else
                CungUngDKList = KDT_GC_CungUngDangKy.SelectCollectionAll();

            dgList.DataSource = CungUngDKList;
            dgList.Refetch();
        }
        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case"cmdNPLCungUng":
                    ThemBangKeCungUng();
                    break;

            }
        }
        private void ThemBangKeCungUng()
        {
            CungUngDangky=  new KDT_GC_CungUngDangKy();
            CungUngDangky.HopDong_ID = HD.ID;
            CungUngDangky.SoHopDong = HD.SoHopDong;
            CungUngDangky.NgayHopDong = HD.NgayKy;
            CungUngDangky.NgayHetHan = HD.NgayHetHan;
            CungUngForm frm = new CungUngForm();
            frm.CungUngDK = CungUngDangky;
            frm.ShowDialog();
            LoadData();

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
             if (e.Row.RowType == RowType.Record)
            {
                string trangthai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                 switch(trangthai)
                 {
                     case"-1":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                         break;
                     case "0":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                         break;
                     case "1":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                         break;
                     case "2":
                         e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                         break;
                 }
             }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<KDT_GC_CungUngDangKy> ItemColl = new List<KDT_GC_CungUngDangKy>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa bang kê NPL này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_GC_CungUngDangKy)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_GC_CungUngDangKy item in ItemColl)
                    {
                        if (item.ID > 0)
                        {
                            if (item.TrangThaiXuLy != -1 || item.TrangThaiXuLy != 2)
                            {
                                item.DeleteFull(item.ID);
                                CungUngDKList.Remove(item);
                            }
                            else
                            {
                                ShowMessage("Bảng kê ID: " + item.ID + " đã khai báo không thể xóa.", false);
                            }
                        }
                    }
                    dgList.Refetch();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

      
        
    }
}
