﻿

using System;
using System.Windows.Forms;
using Company.BLL.SXXK ;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
using System.IO;


using System.Xml.Serialization;
using Company.BLL.SXXK.ToKhai;

namespace Company.Interface.SXXK
{
    public partial class ToKhaiMauDichDaDangKyForm : BaseForm
    {
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();
        private ToKhaiMauDichCollection tmpCollection = new ToKhaiMauDichCollection();
        private string xmlCurrent = "";
        public string nhomLoaiHinh = "";
        public ToKhaiMauDichDaDangKyForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

      

        

        private void ToKhaiMauDichManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor; 
                this.search();               
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

      
        //private ToKhaiMauDich getTKMDByID(long id)
        //{
        //    foreach (ToKhaiMauDich tk in this.tkmdCollection)
        //    {
        //        if (tk.ID == id) return tk;
        //    }
        //    return null;
        //}

        

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            if (chkAll.Checked)
            {
                ccTuNgay.Text = "";
                ccNgayDen.Text = "";

            }
            string where = " MaHaiQuan ='" + GlobalSettings.MA_HAI_QUAN + "' and  MaDoanhNghiep='" + GlobalSettings.MA_DON_VI.Trim() + "'";
            if (ccTuNgay.Text != "" && ccNgayDen.Text != "")
            {
                if (ccTuNgay.Value < ccNgayDen.Value)
                    where += " and NgayDangKy between '" + ccTuNgay.Value.ToString("MM/dd/yyyy") + "'  and  '" + ccNgayDen.Value.ToString("MM/dd/yyyy") + "' ";
            }
                        
            // Thực hiện tìm kiếm.    
            try 
            {
                this.tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamic(where, "NgayDangKy");
                dgList.DataSource = this.tkmdCollection;
                dgList.Refetch();
            }
            catch { dgList.Refresh(); }
          
           

           
        }


        private void btnSearch_Click(object sender, EventArgs e)
        {
            this.search();
        }

        //private string checkDataImport(ToKhaiMauDichCollection collection)
        //{
        //    string st = "";
        //    foreach (ToKhaiMauDich tkmd in collection)
        //    {
        //        ToKhaiMauDich tkmdInDatabase = new ToKhaiMauDich();
        //        tkmdInDatabase.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
        //        //tkmdInDatabase.NamDangKy = GlobalSettings.na
        //        tkmdInDatabase.Load();
        //        if (tkmdInDatabase != null)
        //        { 
        //                tmpCollection.Add(tkmd);                    
        //        }
        //        else
        //        {
        //            if (tkmd.ID > 0)
        //                tkmd.ID = 0;
        //            tmpCollection.Add(tkmd);
        //        }
        //    }
        //    return st;
        //}
        //private void ImportData()
        //{
        //    if (openFileDialog1.ShowDialog(this) == DialogResult.OK)
        //    {
        //        tmpCollection.Clear();
        //        try
        //        {
        //            XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
        //            FileStream fs = new FileStream(openFileDialog1.FileName, FileMode.Open);
        //            ToKhaiMauDichCollection tkmdCollection = (ToKhaiMauDichCollection)serializer.Deserialize(fs);
        //            fs.Close();
        //            string st = checkDataImport(tkmdCollection);
        //            if (st != "")
        //            {
        //                if (ShowMessage("Có thông tin không đúng bạn có muốn tiếp tục import không ?\nNếu có sẽ bỏ qua các thông tin đã được duyệt.", true) == "Yes")
        //                {
        //                    ToKhaiMauDich.NhapDuLieuXML(tmpCollection);
        //                    ShowMessage("Import thành công", false);
        //                }
        //            }
        //            else
        //            {
        //                ToKhaiMauDich.NhapDuLieuXML(tmpCollection);
        //                ShowMessage("Import thành công", false);
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            ShowMessage("Lỗi : " + ex.Message, false);
        //        }
        //    }
        //}
        private void ExportData()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0)
            {
                ShowMessage("Chưa chọn danh sách tờ khai", false);
                return;
            }
            try
            {
                ToKhaiMauDichCollection col = new ToKhaiMauDichCollection();
                if (saveFileDialog1.ShowDialog(this) == DialogResult.OK)
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ToKhaiMauDichCollection));
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);

                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ToKhaiMauDich tkmdSelected = (ToKhaiMauDich)i.GetRow().DataRow;
                            tkmdSelected.LoadHMDCollection();
                            col.Add(tkmdSelected);
                        }
                    }
                    serializer.Serialize(fs, col);
                    fs.Close();
                }
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
       
       
      

        /// <summary>
        /// Gửi thông tin đăng ký đến Hải quan.
        /// </summary>
     

        /// <summary>
        /// Hủy thông tin đã đăng ký.
        /// </summary>
   

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXuat_Click(object sender, EventArgs e)
        {
          this.ExportData();
        }


       

       
    }
}