GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_License_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_License_Details]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_License]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_License]
GO
CREATE TABLE t_KDT_VNACCS_License
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
TKMD_ID BIGINT NOT NULL,
GhiChuKhac NVARCHAR(2000),
[FileName] NVARCHAR(255) NOT NULL,
Content NVARCHAR(MAX) NOT NULL,
GuidStr VARCHAR(MAX)
)

GO
CREATE TABLE t_KDT_VNACCS_License_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
License_ID BIGINT NOT NULL FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_License (ID) ,
NguoiCapGP NVARCHAR(255) NOT NULL,
SoGP NVARCHAR(35) NOT NULL,
NgayCapGP DATETIME NOT NULL,
NoiCapGP NVARCHAR(255) NOT NULL,
LoaiGP NVARCHAR(4) NOT NULL,
NgayHetHanGP DATETIME NOT NULL,
)

GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_ContractDocument_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_ContractDocument_Details]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_ContractDocument]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_ContractDocument]
GO
CREATE TABLE t_KDT_VNACCS_ContractDocument
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
TKMD_ID BIGINT NOT NULL,
GhiChuKhac NVARCHAR(2000),
[FileName] NVARCHAR(255) NOT NULL,
Content NVARCHAR(MAX) NOT NULL,
GuidStr VARCHAR(MAX)
)

GO
CREATE TABLE t_KDT_VNACCS_ContractDocument_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
ContractDocument_ID BIGINT NOT NULL FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_ContractDocument (ID) ,
SoHopDong NVARCHAR(50) NOT NULL,
NgayHopDong DATETIME NOT NULL,
ThoiHanThanhToan DATETIME NOT NULL,
TongTriGia NUMERIC(21,6) NOT NULL,
)

GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_CommercialInvoice_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_CommercialInvoice_Details]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_CommercialInvoice]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_CommercialInvoice]
GO
CREATE TABLE t_KDT_VNACCS_CommercialInvoice
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
TKMD_ID BIGINT NOT NULL,
GhiChuKhac NVARCHAR(2000),
[FileName] NVARCHAR(255) NOT NULL,
Content NVARCHAR(MAX) NOT NULL,
GuidStr VARCHAR(MAX)
)
GO
CREATE TABLE t_KDT_VNACCS_CommercialInvoice_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
CommercialInvoice_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_CommercialInvoice(ID),
SoHoaDonTM NVARCHAR(50) NOT NULL,
NgayPhatHanhHDTM DATETIME NOT NULL
)
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_CertificateOfOrigin_Details]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_CertificateOfOrigin]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_CertificateOfOrigin]
GO
CREATE TABLE t_KDT_VNACCS_CertificateOfOrigin
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
TKMD_ID BIGINT NOT NULL,
GhiChuKhac NVARCHAR(200),
[FileName] NVARCHAR(255) NOT NULL,
Content NVARCHAR(MAX) NOT NULL,
GuidStr VARCHAR(MAX)
)
GO
CREATE TABLE t_KDT_VNACCS_CertificateOfOrigin_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
CertificateOfOrigin_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_CertificateOfOrigin(ID),
SoCO NVARCHAR(35) NOT NULL,
LoaiCO NVARCHAR(35) NOT NULL,
ToChucCapCO NVARCHAR(255) NOT NULL,
NgayCapCO DATETIME NOT NULL,
NuocCapCO VARCHAR(2) NOT NULL,
NguoiCapCO NVARCHAR(255)
)

GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BillOfLading_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BillOfLading_Details]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BillOfLading]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BillOfLading]
GO
CREATE TABLE t_KDT_VNACCS_BillOfLading
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
TKMD_ID BIGINT NOT NULL,
GhiChuKhac NVARCHAR(200),
[FileName] NVARCHAR(255) NOT NULL,
Content NVARCHAR(MAX) NOT NULL,
GuidStr VARCHAR(MAX)
)

GO

CREATE TABLE t_KDT_VNACCS_BillOfLading_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
BillOfLading_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_BillOfLading(ID),
SoVanDon NVARCHAR(35) NOT NULL,
NgayVanDon DATETIME NOT NULL,
NuocPhatHanh VARCHAR(2) NOT NULL,
DiaDiemCTQC NVARCHAR(255),
LoaiVanDon NUMERIC(1,0) NOT NULL
)
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_Container_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_Container_Details]
GO
CREATE TABLE t_KDT_VNACCS_Container_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
TKMD_ID BIGINT NOT NULL,
GhiChuKhac NVARCHAR(200),
[FileName] NVARCHAR(255) NOT NULL,
Content NVARCHAR(MAX) NOT NULL,
GuidStr VARCHAR(MAX)
)

GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_AdditionalDocument_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_AdditionalDocument_Details]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_AdditionalDocument]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_AdditionalDocument]
GO
CREATE TABLE t_KDT_VNACCS_AdditionalDocument
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
TKMD_ID BIGINT NOT NULL,
GhiChuKhac NVARCHAR(200),
[FileName] NVARCHAR(255) NOT NULL,
Content NVARCHAR(MAX) NOT NULL,
GuidStr VARCHAR(MAX)
)

GO

CREATE TABLE t_KDT_VNACCS_AdditionalDocument_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
AdditionalDocument_ID BIGINT FOREIGN KEY REFERENCES dbo.t_KDT_VNACCS_AdditionalDocument(ID),
SoChungTu NVARCHAR(35) NOT NULL,
TenChungTu NVARCHAR(255) NOT NULL,
NgayPhatHanh DATETIME NOT NULL,
NoiPhatHanh NVARCHAR(255),
)
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_OverTime]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_OverTime]
GO
CREATE TABLE t_KDT_VNACCS_OverTime
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
NgayDangKy DATETIME NOT NULL,
GioLamThuTuc NVARCHAR(6) NOT NULL,
NoiDung NVARCHAR(200) NOT NULL,
GuidStr VARCHAR(MAX)
)

GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP]
GO
CREATE TABLE t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT NOT NULL,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
NamQuyetToan NUMERIC(4) NOT NULL,
LoaiHinhBaoCao NUMERIC(1) NOT NULL,
DVTBaoCao NUMERIC(1) NOT NULL,
GhiChuKhac NVARCHAR(2000),
[FileName] NVARCHAR(255),
Content NVARCHAR(MAX) NOT NULL,
GuidStr VARCHAR(MAX)
)

GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details]
GO
CREATE TABLE t_KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
STT NUMERIC(5) NOT NULL,
LoaiHangHoa NUMERIC(5) NOT NULL,
TaiKhoan NUMERIC(5) NOT NULL,
TenHangHoa NVARCHAR(255) NOT NULL,
MaHangHoa NVARCHAR(50) ,
DVT NVARCHAR(4),
TonDauKy NUMERIC(18,4) NOT NULL ,
NhapTrongKy NUMERIC(18,4) NOT NULL ,
XuatTrongKy NUMERIC(18,4) NOT NULL ,
TonCuoiKy NUMERIC(18,4) NOT NULL ,
GhiChu NVARCHAR(2000)
)

GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detais]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detais]
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB]
GO
CREATE TABLE t_KDT_VNACCS_BaoCaoQuyetToan_MMTB
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
TrangThaiXuLy INT NOT NULL,
SoTN BIGINT,
NgayTN DATETIME,
MaHQ NVARCHAR(6) NOT NULL,
MaDoanhNghiep NVARCHAR(17) NOT NULL,
GuidStr VARCHAR(MAX)
)

GO
CREATE TABLE t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detais
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
STT NUMERIC(5) NOT NULL ,
SoHopDong NVARCHAR(80) NOT NULL,
NgayHopDong DATETIME NOT NULL,
MaHQ NVARCHAR(6) NOT NULL ,
NgayHetHan DATETIME NOT NULL,
GhiChuKhac NVARCHAR(2000)
)

GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detais]'))
	DROP TABLE [dbo].[t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detais]
GO
CREATE TABLE t_KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detais
(
ID BIGINT IDENTITY(1,1) PRIMARY KEY ,
STT NUMERIC(5) NOT NULL ,
TenHangHoa NVARCHAR(255) NOT NULL,
MaHangHoa NVARCHAR(50),
MaHS NUMERIC(12) NOT NULL ,
GhiChu NVARCHAR(2000),
LuongTamNhap NUMERIC(18,4) NOT NULL,
LuongTaiXuat NUMERIC(18,4) NOT NULL,
LuongChuyenTiep NUMERIC(18,4) NOT NULL ,
SoHopDong NVARCHAR(80) NOT NULL ,
NgayHopDong DATETIME NOT NULL,
MaHQ NVARCHAR(6) NOT NULL,
NgayHetHan DATETIME NOT NULL,
LuongConLai NUMERIC(18,4),
DVT NVARCHAR(4)
)
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_License_SelectDynamicBy_TKMD]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_License_SelectDynamicBy_TKMD]
GO    
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_VNACCS_License_SelectDynamicBy_TKMD]    
-- Database: ECS_TQDT_KD_V4_23_07_2015_SOI_PHU_MAI    
-- Author: Ngo Thanh Tung    
-- Time created: Wednesday, November 16, 2016    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_VNACCS_License_SelectDynamicBy_TKMD]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT    *  
  FROM      dbo.t_KDT_VNACCS_License  
            INNER JOIN dbo.t_KDT_VNACCS_License_Details ON t_KDT_VNACCS_License_Details.License_ID = t_KDT_VNACCS_License.ID    
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL 

GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectDynamicBy_TKMD]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectDynamicBy_TKMD]
GO        
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectDynamicBy_TKMD]    
-- Database: ECS_TQDT_KD_V4_23_07_2015_SOI_PHU_MAI    
-- Author: Ngo Thanh Tung    
-- Time created: Wednesday, November 16, 2016    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CertificateOfOrigin_SelectDynamicBy_TKMD]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT    *  
  FROM      t_KDT_VNACCS_CertificateOfOrigin  
            INNER JOIN dbo.t_KDT_VNACCS_CertificateOfOrigin_Details ON t_KDT_VNACCS_CertificateOfOrigin_Details.CertificateOfOrigin_ID = t_KDT_VNACCS_CertificateOfOrigin.ID     
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL 
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamicBy_TKMD]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamicBy_TKMD]
GO  
  ------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamicBy_TKMD]    
-- Database: ECS_TQDT_KD_V4_23_07_2015_SOI_PHU_MAI    
-- Author: Ngo Thanh Tung    
-- Time created: Wednesday, November 16, 2016    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_VNACCS_AdditionalDocument_SelectDynamicBy_TKMD]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'  SELECT    *  
  FROM      t_KDT_VNACCS_AdditionalDocument  
            INNER JOIN dbo.t_KDT_VNACCS_AdditionalDocument_Details ON t_KDT_VNACCS_AdditionalDocument_Details.AdditionalDocument_ID = t_KDT_VNACCS_AdditionalDocument.ID  
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL 
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLading_SelectDynamicBy_TKMD]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_SelectDynamicBy_TKMD]
GO  
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLading_SelectDynamicBy_TKMD]    
-- Database: ECS_TQDT_KD_V4_23_07_2015_SOI_PHU_MAI    
-- Author: Ngo Thanh Tung    
-- Time created: Wednesday, November 16, 2016    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLading_SelectDynamicBy_TKMD]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'  SELECT    *  
  FROM      t_KDT_VNACCS_BillOfLading  
            INNER JOIN dbo.t_KDT_VNACCS_BillOfLading_Details ON t_KDT_VNACCS_BillOfLading_Details.BillOfLading_ID = t_KDT_VNACCS_BillOfLading.ID  
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL    
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CommercialInvoice_SelectDynamicBy_TKMD]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_SelectDynamicBy_TKMD]
GO     
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CommercialInvoice_SelectDynamicBy_TKMD]    
-- Database: ECS_TQDT_KD_V4_23_07_2015_SOI_PHU_MAI    
-- Author: Ngo Thanh Tung    
-- Time created: Wednesday, November 16, 2016    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CommercialInvoice_SelectDynamicBy_TKMD]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT    *  
  FROM      t_KDT_VNACCS_CommercialInvoice  
            INNER JOIN dbo.t_KDT_VNACCS_CommercialInvoice_Details ON t_KDT_VNACCS_CommercialInvoice_Details.CommercialInvoice_ID = t_KDT_VNACCS_CommercialInvoice.ID     
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL 
GO
IF  EXISTS (SELECT * FROM SYS.OBJECTS WHERE OBJECT_ID = OBJECT_ID(N'[dbo].[p_KDT_VNACCS_ContractDocument_SelectDynamicBy_TKMD]') AND TYPE IN (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_SelectDynamicBy_TKMD]
GO     
------------------------------------------------------------------------------------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_VNACCS_ContractDocument_SelectDynamicBy_TKMD]    
-- Database: ECS_TQDT_KD_V4_23_07_2015_SOI_PHU_MAI    
-- Author: Ngo Thanh Tung    
-- Time created: Wednesday, November 16, 2016    
------------------------------------------------------------------------------------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_VNACCS_ContractDocument_SelectDynamicBy_TKMD]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT    *  
  FROM      dbo.t_KDT_VNACCS_ContractDocument  
            INNER JOIN dbo.t_KDT_VNACCS_ContractDocument_Details ON t_KDT_VNACCS_ContractDocument_Details.ContractDocument_ID = t_KDT_VNACCS_ContractDocument.ID  
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END    
    
EXEC sp_executesql @SQL 

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '34.0') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('34.0',GETDATE(), N'CẬP NHẬT KHAI BÁO CHỨNG TỪ VNACCS')
END

