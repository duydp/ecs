﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
//using Company.Interface.GC;
//using Company.Interface.SXXK;

using Janus.Windows.EditControls;
using Janus.Windows.ExplorerBar;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;
using Company.Interface.SXXK;

namespace Company.Interface
{
    partial class MainForm
    {
        private UICommandManager cmMain;
        private UIRebar TopRebar1;
        private UICommand cmdHeThong;
        private UICommand cmdHeThong1;
        private UICommand cmdThoat;
        private UICommand cmdThoat1;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private UIPanelManager pmMain;
        private UICommandBar cmbMenu;
        private UICommand cmdLoaiHinh;
        private UIPanel pnlSXXK;
        private UIPanelInnerContainer pnlSXXKContainer;
        private UIPanel pnlGiaCong;
        private UIPanelInnerContainer pnlGiaCongContainer;
        private UIPanel pnlKinhDoanh;
        private UIPanelInnerContainer pnlKinhDoanhContainer;
        private UIPanel pnlDauTu;
        internal ImageList ilSmall;
        internal ImageList ilMedium;
        internal ImageList ilLarge;
        private UICommand cmdReceiveAll;
        private Janus.Windows.ExplorerBar.ExplorerBar expGiaCong;
        private Janus.Windows.ExplorerBar.ExplorerBar expKD;
        private Janus.Windows.UI.CommandBars.UICommand cmdThoat2;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup1 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem1 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem2 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem3 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup2 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem4 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem5 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem6 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup3 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem7 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem8 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem9 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup4 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem10 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem11 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem12 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem13 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem14 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem15 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem16 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem17 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup5 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem18 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem19 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem20 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem21 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem22 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup6 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem23 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem24 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup7 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem25 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem26 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem27 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup8 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem28 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem29 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup9 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem30 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem31 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem32 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup10 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem33 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem34 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup11 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem35 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem36 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem37 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup12 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem38 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem39 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem40 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup13 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem41 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem42 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem43 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup14 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem44 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem45 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup15 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem46 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem47 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem48 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup16 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem49 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem50 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup17 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem51 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem52 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel1 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel2 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel3 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            Janus.Windows.UI.StatusBar.UIStatusBarPanel uiStatusBarPanel4 = new Janus.Windows.UI.StatusBar.UIStatusBarPanel();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbMenu = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdHeThong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.cmdDanhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDanhMuc");
            this.cmdBieuThue1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBieuThue");
            this.QuanTri1 = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.Command11 = new Janus.Windows.UI.CommandBars.UICommand("Command1");
            this.Command01 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.cmdCuaSo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaSo");
            this.cmbDongBoDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("cmbDongBoDuLieu");
            this.cmdHeThong = new Janus.Windows.UI.CommandBars.UICommand("cmdHeThong");
            this.cmdDataVersion1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDataVersion");
            this.cmdBackUp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBackUp");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdCapNhatHS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS");
            this.Separator9 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdNhapXuat2 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXuat");
            this.Separator10 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.NhacNho1 = new Janus.Windows.UI.CommandBars.UICommand("NhacNho");
            this.cmdConfig1 = new Janus.Windows.UI.CommandBars.UICommand("cmdConfig");
            this.cmdCauHinh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.mnuQuerySQL1 = new Janus.Windows.UI.CommandBars.UICommand("mnuQuerySQL");
            this.cmdLog1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdDaily1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDaily");
            this.Separator12 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.LoginUser1 = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.cmdDoiMatKhauHQ1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDoiMatKhauHQ");
            this.Separator4 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdThoat2 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdThoat = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdLoaiHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiHinh");
            this.cmdReceiveAll = new Janus.Windows.UI.CommandBars.UICommand("cmdReceiveAll");
            this.NhacNho = new Janus.Windows.UI.CommandBars.UICommand("NhacNho");
            this.DongBoDuLieu = new Janus.Windows.UI.CommandBars.UICommand("DongBoDuLieu");
            this.cmdImport = new Janus.Windows.UI.CommandBars.UICommand("cmdImport");
            this.cmdImportNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.cmdNPLNhapTon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLNhapTon");
            this.cmdImportNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdImportNPL");
            this.cmdImportSP = new Janus.Windows.UI.CommandBars.UICommand("cmdImportSP");
            this.cmdImportDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportDM");
            this.cmdImportTTDM = new Janus.Windows.UI.CommandBars.UICommand("cmdImportTTDM");
            this.cmdImportToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdImportToKhai");
            this.cmdImportHangHoa = new Janus.Windows.UI.CommandBars.UICommand("cmdImportHangHoa");
            this.Command1 = new Janus.Windows.UI.CommandBars.UICommand("Command1");
            this.cmd20071 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd20031 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.cmdVN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdEL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdEL");
            this.cmd2007 = new Janus.Windows.UI.CommandBars.UICommand("cmd2007");
            this.cmd2003 = new Janus.Windows.UI.CommandBars.UICommand("cmd2003");
            this.Command0 = new Janus.Windows.UI.CommandBars.UICommand("Command0");
            this.cmdHelp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHelp");
            this.cmdAutoUpdate1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.Separator5 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdTeamview1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTeamview");
            this.Separator11 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdTool1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTool");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdActivate1 = new Janus.Windows.UI.CommandBars.UICommand("cmdActivate");
            this.cmdAbout1 = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdHelp = new Janus.Windows.UI.CommandBars.UICommand("cmdHelp");
            this.cmdAbout = new Janus.Windows.UI.CommandBars.UICommand("cmdAbout");
            this.cmdNPLNhapTon = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLNhapTon");
            this.cmdDanhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdDanhMuc");
            this.cmdGetCategoryOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGetCategoryOnline");
            this.Separator6 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdHaiQuan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNuoc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdMaHS1 = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNguyenTe1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.cmdNhomCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhomCuaKhau");
            this.cmdLoaiPhiChungTuThanhToan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiPhiChungTuThanhToan");
            this.cmdMaHS = new Janus.Windows.UI.CommandBars.UICommand("cmdMaHS");
            this.cmdNuoc = new Janus.Windows.UI.CommandBars.UICommand("cmdNuoc");
            this.cmdHaiQuan = new Janus.Windows.UI.CommandBars.UICommand("cmdHaiQuan");
            this.cmdNguyenTe = new Janus.Windows.UI.CommandBars.UICommand("cmdNguyenTe");
            this.cmdDVT = new Janus.Windows.UI.CommandBars.UICommand("cmdDVT");
            this.cmdPTTT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTTT");
            this.cmdPTVT = new Janus.Windows.UI.CommandBars.UICommand("cmdPTVT");
            this.cmdDKGH = new Janus.Windows.UI.CommandBars.UICommand("cmdDKGH");
            this.cmdCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaKhau");
            this.cmdBackUp = new Janus.Windows.UI.CommandBars.UICommand("cmdBackUp");
            this.cmdRestore = new Janus.Windows.UI.CommandBars.UICommand("cmdRestore");
            this.ThongSoKetNoi = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdThietLapIn = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapIn");
            this.cmdExportExccel = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdImportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdDongBoPhongKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdDongBoPhongKhai");
            this.cmdImportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdExportExccel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdCauHinh = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinh");
            this.ThongSoKetNoi1 = new Janus.Windows.UI.CommandBars.UICommand("ThongSoKetNoi");
            this.TLThongTinDNHQ1 = new Janus.Windows.UI.CommandBars.UICommand("TLThongTinDNHQ");
            this.cmdCauHinhToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinhToKhai");
            this.cmdThietLapIn1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThietLapIn");
            this.cmdChuKySo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuKySo");
            this.cmdTimer1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTimer");
            this.cmdCauHinhToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdCauHinhToKhai");
            this.QuanTri = new Janus.Windows.UI.CommandBars.UICommand("QuanTri");
            this.QuanLyNguoiDung1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNhom1 = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNhom");
            this.QuanLyNguoiDung = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNguoiDung");
            this.QuanLyNhom = new Janus.Windows.UI.CommandBars.UICommand("QuanLyNhom");
            this.LoginUser = new Janus.Windows.UI.CommandBars.UICommand("LoginUser");
            this.cmdChangePass = new Janus.Windows.UI.CommandBars.UICommand("cmdChangePass");
            this.cmdDoiMatKhauHQ = new Janus.Windows.UI.CommandBars.UICommand("cmdDoiMatKhauHQ");
            this.cmdCuaSo = new Janus.Windows.UI.CommandBars.UICommand("cmdCuaSo");
            this.cmdCloseAll1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.cmdCloseAllButThis1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButThis");
            this.cmdCloseAll = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAll");
            this.cmdCloseAllButThis = new Janus.Windows.UI.CommandBars.UICommand("cmdCloseAllButThis");
            this.cmdAutoUpdate = new Janus.Windows.UI.CommandBars.UICommand("cmdAutoUpdate");
            this.cmdNhapXML = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXML");
            this.cmdNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPL");
            this.cmdSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSP");
            this.cmdDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdDM");
            this.cmdTK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTK");
            this.cmdNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdNPL");
            this.cmdSP = new Janus.Windows.UI.CommandBars.UICommand("cmdSP");
            this.cmdDM = new Janus.Windows.UI.CommandBars.UICommand("cmdDM");
            this.cmdTK = new Janus.Windows.UI.CommandBars.UICommand("cmdTK");
            this.cmdXuatDuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieu");
            this.cmdHUNGNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGNPL");
            this.cmdHUNGSP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGSP");
            this.cmdHUNGDM1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGDM");
            this.cmdHUNGTK1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGTK");
            this.cmdXuatNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatNPL");
            this.cmdXuatDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDinhMuc");
            this.cmdXuatSanPham = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatSanPham");
            this.cmdXuatToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatToKhai");
            this.cmdVN = new Janus.Windows.UI.CommandBars.UICommand("cmdVN");
            this.cmdEL = new Janus.Windows.UI.CommandBars.UICommand("cmdEL");
            this.cmdHUNGNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGNPL");
            this.cmdHUNGSP = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGSP");
            this.cmdHUNGDM = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGDM");
            this.cmdHUNGTK = new Janus.Windows.UI.CommandBars.UICommand("cmdHUNGTK");
            this.cmdActivate = new Janus.Windows.UI.CommandBars.UICommand("cmdActivate");
            this.cmdXuatDuLieuDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuDN");
            this.cmdXuatNPLDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatNPLDN");
            this.cmdXuatSPDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatSPDN");
            this.cmdXuatDMDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDMDN");
            this.cmdXuatTKDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatTKDN");
            this.cmdXuatNPLDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatNPLDN");
            this.cmdXuatSPDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatSPDN");
            this.cmXuat = new Janus.Windows.UI.CommandBars.UICommand("cmXuat");
            this.cmdXuatDMDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDMDN");
            this.cmdXuatTKDN = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatTKDN");
            this.cmdNhapDuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDuLieu");
            this.cmdNhapNPL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapNPL");
            this.cmdNhapSanPham1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapSanPham");
            this.cmdNhapDinhMuc1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDinhMuc");
            this.cmdNhapToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhai");
            this.cmdNhapNPL = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapNPL");
            this.cmdNhapSanPham = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapSanPham");
            this.cmdNhapDinhMuc = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDinhMuc");
            this.cmdNhapToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapToKhai");
            this.QuanlyMess = new Janus.Windows.UI.CommandBars.UICommand("QuanlyMess");
            this.cmdConfig = new Janus.Windows.UI.CommandBars.UICommand("cmdConfig");
            this.mnuQuerySQL = new Janus.Windows.UI.CommandBars.UICommand("mnuQuerySQL");
            this.mnuFormSQL1 = new Janus.Windows.UI.CommandBars.UICommand("mnuFormSQL");
            this.mnuMiniSQL1 = new Janus.Windows.UI.CommandBars.UICommand("mnuMiniSQL");
            this.TraCuuMaHS = new Janus.Windows.UI.CommandBars.UICommand("TraCuuMaHS");
            this.cmdNhomCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdNhomCuaKhau");
            this.cmdLog = new Janus.Windows.UI.CommandBars.UICommand("cmdLog");
            this.cmdLoaiPhiChungTuThanhToan = new Janus.Windows.UI.CommandBars.UICommand("cmdLoaiPhiChungTuThanhToan");
            this.cmdDataVersion = new Janus.Windows.UI.CommandBars.UICommand("cmdDataVersion");
            this.cmdImportXml = new Janus.Windows.UI.CommandBars.UICommand("cmdImportXml");
            this.cmdChuKySo = new Janus.Windows.UI.CommandBars.UICommand("cmdChuKySo");
            this.cmdTimer = new Janus.Windows.UI.CommandBars.UICommand("cmdTimer");
            this.mnuFormSQL = new Janus.Windows.UI.CommandBars.UICommand("mnuFormSQL");
            this.mnuMiniSQL = new Janus.Windows.UI.CommandBars.UICommand("mnuMiniSQL");
            this.cmdNhapXuat = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXuat");
            this.cmdImportXml1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportXml");
            this.cmdNhapXML1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXML");
            this.cmdImportExcel2 = new Janus.Windows.UI.CommandBars.UICommand("cmdImportExcel");
            this.cmdNhapDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapDuLieu");
            this.Separator8 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdXuatDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieu");
            this.cmdXuatDuLieuDN1 = new Janus.Windows.UI.CommandBars.UICommand("cmdXuatDuLieuDN");
            this.cmdBieuThue = new Janus.Windows.UI.CommandBars.UICommand("cmdBieuThue");
            this.TraCuuMaHS1 = new Janus.Windows.UI.CommandBars.UICommand("TraCuuMaHS");
            this.Separator7 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdTraCuuXNKOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuXNKOnline");
            this.cmdTraCuuNoThueOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuNoThueOnline");
            this.cmdTraCuuVanBanOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuVanBanOnline");
            this.cmdTuVanHQOnline1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTuVanHQOnline");
            this.cmdTraCuuXNKOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuXNKOnline");
            this.cmdTraCuuNoThueOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuNoThueOnline");
            this.cmdTraCuuVanBanOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTraCuuVanBanOnline");
            this.cmdTuVanHQOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdTuVanHQOnline");
            this.cmdGetCategoryOnline = new Janus.Windows.UI.CommandBars.UICommand("cmdGetCategoryOnline");
            this.cmdTeamview = new Janus.Windows.UI.CommandBars.UICommand("cmdTeamview");
            this.cmdCapNhatHS = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS");
            this.cmdCapNhatHS8SoAuto1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8SoAuto");
            this.cmdCapNhatHS8SoManual1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8SoManual");
            this.cmdCapNhatHS8SoAuto = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8SoAuto");
            this.cmdCapNhatHS8SoManual = new Janus.Windows.UI.CommandBars.UICommand("cmdCapNhatHS8SoManual");
            this.cmdTool = new Janus.Windows.UI.CommandBars.UICommand("cmdTool");
            this.cmdImageResizeHelp1 = new Janus.Windows.UI.CommandBars.UICommand("cmdImageResizeHelp");
            this.cmdImageResizeHelp = new Janus.Windows.UI.CommandBars.UICommand("cmdImageResizeHelp");
            this.cmbDongBoDuLieu = new Janus.Windows.UI.CommandBars.UICommand("cmbDongBoDuLieu");
            this.DongBoDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("DongBoDuLieu");
            this.cmdQuanLyDL1 = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyDL");
            this.cmdQuanLyDL = new Janus.Windows.UI.CommandBars.UICommand("cmdQuanLyDL");
            this.cmdDaily = new Janus.Windows.UI.CommandBars.UICommand("cmdDaily");
            this.ilSmall = new System.Windows.Forms.ImageList(this.components);
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExccel");
            this.cmdThoat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.pmMain = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expSXXK = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.uiPanel2 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel2Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expKhaiBao_TheoDoi = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSXXK = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSXXKContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.pnlGiaCong = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlGiaCongContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expGiaCong = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlKinhDoanh = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlKinhDoanhContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expKD = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlDauTu = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlDauTuContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.expDT = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.pnlSend = new Janus.Windows.UI.Dock.UIPanel();
            this.pnlSendContainer = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.ilMedium = new System.Windows.Forms.ImageList(this.components);
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.statusBar = new Janus.Windows.UI.StatusBar.UIStatusBar();
            this.cmdThoat3 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.vlECS = new SoftechVersion.ValidVersion(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.cmdThoat4 = new Janus.Windows.UI.CommandBars.UICommand("cmdThoat");
            this.cmdNhapXuat1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNhapXuat");
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lblWebsite = new DevExpress.XtraEditors.LabelControl();
            this.lblEmail = new DevExpress.XtraEditors.LabelControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.timerNgayThangNam = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expSXXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).BeginInit();
            this.uiPanel2.SuspendLayout();
            this.uiPanel2Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expKhaiBao_TheoDoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).BeginInit();
            this.pnlSXXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).BeginInit();
            this.pnlGiaCong.SuspendLayout();
            this.pnlGiaCongContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).BeginInit();
            this.pnlKinhDoanh.SuspendLayout();
            this.pnlKinhDoanhContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).BeginInit();
            this.pnlDauTu.SuspendLayout();
            this.pnlDauTuContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).BeginInit();
            this.pnlSend.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.BackColor = System.Drawing.SystemColors.Control;
            this.grbMain.Controls.Add(this.panel1);
            this.grbMain.Location = new System.Drawing.Point(203, 29);
            this.grbMain.Size = new System.Drawing.Size(614, 427);
            this.grbMain.Visible = false;
            this.grbMain.Click += new System.EventHandler(this.grbMain_Click);
            // 
            // cmMain
            // 
            this.cmMain.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmMain.AllowMerge = false;
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong,
            this.cmdThoat,
            this.cmdLoaiHinh,
            this.cmdReceiveAll,
            this.NhacNho,
            this.DongBoDuLieu,
            this.cmdImport,
            this.cmdImportNPL,
            this.cmdImportSP,
            this.cmdImportDM,
            this.cmdImportTTDM,
            this.cmdImportToKhai,
            this.cmdImportHangHoa,
            this.Command1,
            this.cmd2007,
            this.cmd2003,
            this.Command0,
            this.cmdHelp,
            this.cmdAbout,
            this.cmdNPLNhapTon,
            this.cmdDanhMuc,
            this.cmdMaHS,
            this.cmdNuoc,
            this.cmdHaiQuan,
            this.cmdNguyenTe,
            this.cmdDVT,
            this.cmdPTTT,
            this.cmdPTVT,
            this.cmdDKGH,
            this.cmdCuaKhau,
            this.cmdBackUp,
            this.cmdRestore,
            this.ThongSoKetNoi,
            this.TLThongTinDNHQ,
            this.cmdThietLapIn,
            this.cmdExportExccel,
            this.cmdImportExcel,
            this.cmdDongBoPhongKhai,
            this.cmdCauHinh,
            this.cmdCauHinhToKhai,
            this.QuanTri,
            this.QuanLyNguoiDung,
            this.QuanLyNhom,
            this.LoginUser,
            this.cmdChangePass,
            this.cmdDoiMatKhauHQ,
            this.cmdCuaSo,
            this.cmdCloseAll,
            this.cmdCloseAllButThis,
            this.cmdAutoUpdate,
            this.cmdNhapXML,
            this.cmdNPL,
            this.cmdSP,
            this.cmdDM,
            this.cmdTK,
            this.cmdXuatDuLieu,
            this.cmdXuatNPL,
            this.cmdXuatDinhMuc,
            this.cmdXuatSanPham,
            this.cmdXuatToKhai,
            this.cmdVN,
            this.cmdEL,
            this.cmdHUNGNPL,
            this.cmdHUNGSP,
            this.cmdHUNGDM,
            this.cmdHUNGTK,
            this.cmdActivate,
            this.cmdXuatDuLieuDN,
            this.cmdXuatNPLDN,
            this.cmdXuatSPDN,
            this.cmXuat,
            this.cmdXuatDMDN,
            this.cmdXuatTKDN,
            this.cmdNhapDuLieu,
            this.cmdNhapNPL,
            this.cmdNhapSanPham,
            this.cmdNhapDinhMuc,
            this.cmdNhapToKhai,
            this.QuanlyMess,
            this.cmdConfig,
            this.mnuQuerySQL,
            this.TraCuuMaHS,
            this.cmdNhomCuaKhau,
            this.cmdLog,
            this.cmdLoaiPhiChungTuThanhToan,
            this.cmdDataVersion,
            this.cmdImportXml,
            this.cmdChuKySo,
            this.cmdTimer,
            this.mnuFormSQL,
            this.mnuMiniSQL,
            this.cmdNhapXuat,
            this.cmdBieuThue,
            this.cmdTraCuuXNKOnline,
            this.cmdTraCuuNoThueOnline,
            this.cmdTraCuuVanBanOnline,
            this.cmdTuVanHQOnline,
            this.cmdGetCategoryOnline,
            this.cmdTeamview,
            this.cmdCapNhatHS,
            this.cmdCapNhatHS8SoAuto,
            this.cmdCapNhatHS8SoManual,
            this.cmdTool,
            this.cmdImageResizeHelp,
            this.cmbDongBoDuLieu,
            this.cmdQuanLyDL,
            this.cmdDaily});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("eae49f54-3bfa-4a6a-8b46-89b443ba80cd");
            this.cmMain.ImageList = this.ilSmall;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.LockCommandBars = true;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbMenu
            // 
            this.cmbMenu.AllowClose = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowCustomize = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.AllowMerge = Janus.Windows.UI.InheritableBoolean.False;
            this.cmbMenu.CommandBarType = Janus.Windows.UI.CommandBars.CommandBarType.Menu;
            this.cmbMenu.CommandManager = this.cmMain;
            this.cmbMenu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHeThong1,
            this.cmdDanhMuc1,
            this.cmdBieuThue1,
            this.QuanTri1,
            this.Command11,
            this.Command01,
            this.cmdCuaSo1,
            this.cmbDongBoDuLieu1});
            this.cmbMenu.Key = "cmbMenu";
            this.cmbMenu.Location = new System.Drawing.Point(0, 0);
            this.cmbMenu.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbMenu.MergeRowOrder = 0;
            this.cmbMenu.Name = "cmbMenu";
            this.cmbMenu.RowIndex = 0;
            this.cmbMenu.Size = new System.Drawing.Size(820, 26);
            this.cmbMenu.Text = "cmbMenu";
            this.cmbMenu.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMenu_CommandClick);
            // 
            // cmdHeThong1
            // 
            this.cmdHeThong1.Key = "cmdHeThong";
            this.cmdHeThong1.Name = "cmdHeThong1";
            this.cmdHeThong1.Text = "&Hệ thống";
            // 
            // cmdDanhMuc1
            // 
            this.cmdDanhMuc1.Key = "cmdDanhMuc";
            this.cmdDanhMuc1.Name = "cmdDanhMuc1";
            this.cmdDanhMuc1.Text = "&Danh mục";
            // 
            // cmdBieuThue1
            // 
            this.cmdBieuThue1.Key = "cmdBieuThue";
            this.cmdBieuThue1.Name = "cmdBieuThue1";
            // 
            // QuanTri1
            // 
            this.QuanTri1.Key = "QuanTri";
            this.QuanTri1.Name = "QuanTri1";
            this.QuanTri1.Text = "&Quản trị";
            // 
            // Command11
            // 
            this.Command11.Key = "Command1";
            this.Command11.Name = "Command11";
            this.Command11.Text = "&Giao diện";
            // 
            // Command01
            // 
            this.Command01.Key = "Command0";
            this.Command01.Name = "Command01";
            this.Command01.Text = "&Trợ giúp";
            // 
            // cmdCuaSo1
            // 
            this.cmdCuaSo1.Key = "cmdCuaSo";
            this.cmdCuaSo1.Name = "cmdCuaSo1";
            this.cmdCuaSo1.Text = "&Cửa sổ";
            // 
            // cmbDongBoDuLieu1
            // 
            this.cmbDongBoDuLieu1.Key = "cmbDongBoDuLieu";
            this.cmbDongBoDuLieu1.Name = "cmbDongBoDuLieu1";
            // 
            // cmdHeThong
            // 
            this.cmdHeThong.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdDataVersion1,
            this.cmdBackUp1,
            this.Separator1,
            this.cmdCapNhatHS1,
            this.Separator9,
            this.cmdNhapXuat2,
            this.Separator10,
            this.NhacNho1,
            this.cmdConfig1,
            this.cmdCauHinh1,
            this.mnuQuerySQL1,
            this.cmdLog1,
            this.Separator3,
            this.cmdDaily1,
            this.Separator12,
            this.LoginUser1,
            this.cmdChangePass1,
            this.cmdDoiMatKhauHQ1,
            this.Separator4,
            this.cmdThoat2});
            this.cmdHeThong.Key = "cmdHeThong";
            this.cmdHeThong.Name = "cmdHeThong";
            this.cmdHeThong.Text = "&Hệ thống";
            // 
            // cmdDataVersion1
            // 
            this.cmdDataVersion1.ImageIndex = 42;
            this.cmdDataVersion1.Key = "cmdDataVersion";
            this.cmdDataVersion1.Name = "cmdDataVersion1";
            this.cmdDataVersion1.Text = "Dữ liệu phiên bản: [?]";
            // 
            // cmdBackUp1
            // 
            this.cmdBackUp1.Key = "cmdBackUp";
            this.cmdBackUp1.Name = "cmdBackUp1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdCapNhatHS1
            // 
            this.cmdCapNhatHS1.Key = "cmdCapNhatHS";
            this.cmdCapNhatHS1.Name = "cmdCapNhatHS1";
            // 
            // Separator9
            // 
            this.Separator9.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator9.Key = "Separator";
            this.Separator9.Name = "Separator9";
            // 
            // cmdNhapXuat2
            // 
            this.cmdNhapXuat2.Key = "cmdNhapXuat";
            this.cmdNhapXuat2.Name = "cmdNhapXuat2";
            // 
            // Separator10
            // 
            this.Separator10.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator10.Key = "Separator";
            this.Separator10.Name = "Separator10";
            // 
            // NhacNho1
            // 
            this.NhacNho1.Icon = ((System.Drawing.Icon)(resources.GetObject("NhacNho1.Icon")));
            this.NhacNho1.Key = "NhacNho";
            this.NhacNho1.Name = "NhacNho1";
            // 
            // cmdConfig1
            // 
            this.cmdConfig1.ImageIndex = 37;
            this.cmdConfig1.Key = "cmdConfig";
            this.cmdConfig1.Name = "cmdConfig1";
            // 
            // cmdCauHinh1
            // 
            this.cmdCauHinh1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCauHinh1.Icon")));
            this.cmdCauHinh1.Key = "cmdCauHinh";
            this.cmdCauHinh1.Name = "cmdCauHinh1";
            // 
            // mnuQuerySQL1
            // 
            this.mnuQuerySQL1.ImageIndex = 42;
            this.mnuQuerySQL1.Key = "mnuQuerySQL";
            this.mnuQuerySQL1.Name = "mnuQuerySQL1";
            // 
            // cmdLog1
            // 
            this.cmdLog1.ImageIndex = 41;
            this.cmdLog1.Key = "cmdLog";
            this.cmdLog1.Name = "cmdLog1";
            // 
            // Separator3
            // 
            this.Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator3.Key = "Separator";
            this.Separator3.Name = "Separator3";
            // 
            // cmdDaily1
            // 
            this.cmdDaily1.Image = ((System.Drawing.Image)(resources.GetObject("cmdDaily1.Image")));
            this.cmdDaily1.Key = "cmdDaily";
            this.cmdDaily1.Name = "cmdDaily1";
            // 
            // Separator12
            // 
            this.Separator12.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator12.Key = "Separator";
            this.Separator12.Name = "Separator12";
            // 
            // LoginUser1
            // 
            this.LoginUser1.Icon = ((System.Drawing.Icon)(resources.GetObject("LoginUser1.Icon")));
            this.LoginUser1.Key = "LoginUser";
            this.LoginUser1.Name = "LoginUser1";
            // 
            // cmdChangePass1
            // 
            this.cmdChangePass1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdChangePass1.Icon")));
            this.cmdChangePass1.Key = "cmdChangePass";
            this.cmdChangePass1.Name = "cmdChangePass1";
            this.cmdChangePass1.Text = "Đổi mật khẩu đăng nhập";
            // 
            // cmdDoiMatKhauHQ1
            // 
            this.cmdDoiMatKhauHQ1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdDoiMatKhauHQ1.Icon")));
            this.cmdDoiMatKhauHQ1.Key = "cmdDoiMatKhauHQ";
            this.cmdDoiMatKhauHQ1.Name = "cmdDoiMatKhauHQ1";
            this.cmdDoiMatKhauHQ1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // Separator4
            // 
            this.Separator4.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator4.Key = "Separator";
            this.Separator4.Name = "Separator4";
            // 
            // cmdThoat2
            // 
            this.cmdThoat2.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThoat2.Icon")));
            this.cmdThoat2.Key = "cmdThoat";
            this.cmdThoat2.Name = "cmdThoat2";
            // 
            // cmdThoat
            // 
            this.cmdThoat.Key = "cmdThoat";
            this.cmdThoat.Name = "cmdThoat";
            this.cmdThoat.Text = "Thoát";
            // 
            // cmdLoaiHinh
            // 
            this.cmdLoaiHinh.Key = "cmdLoaiHinh";
            this.cmdLoaiHinh.Name = "cmdLoaiHinh";
            this.cmdLoaiHinh.Text = "Loại hình";
            // 
            // cmdReceiveAll
            // 
            this.cmdReceiveAll.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdReceiveAll.Icon")));
            this.cmdReceiveAll.Key = "cmdReceiveAll";
            this.cmdReceiveAll.Name = "cmdReceiveAll";
            this.cmdReceiveAll.Text = "Cập nhật thông tin";
            // 
            // NhacNho
            // 
            this.NhacNho.Key = "NhacNho";
            this.NhacNho.Name = "NhacNho";
            this.NhacNho.Text = "Nhắc nhở";
            // 
            // DongBoDuLieu
            // 
            this.DongBoDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("DongBoDuLieu.Icon")));
            this.DongBoDuLieu.Key = "DongBoDuLieu";
            this.DongBoDuLieu.Name = "DongBoDuLieu";
            this.DongBoDuLieu.Text = "Truyền nhận dữ liệu";
            // 
            // cmdImport
            // 
            this.cmdImport.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportNPL1,
            this.cmdImportSP1,
            this.cmdImportDM1,
            this.cmdImportToKhai1,
            this.cmdImportHangHoa1,
            this.cmdNPLNhapTon1});
            this.cmdImport.Key = "cmdImport";
            this.cmdImport.Name = "cmdImport";
            this.cmdImport.Text = "Import from Excel";
            // 
            // cmdImportNPL1
            // 
            this.cmdImportNPL1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportNPL1.Icon")));
            this.cmdImportNPL1.Key = "cmdImportNPL";
            this.cmdImportNPL1.Name = "cmdImportNPL1";
            this.cmdImportNPL1.Text = "Nguyên phụ liệu";
            // 
            // cmdImportSP1
            // 
            this.cmdImportSP1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportSP1.Icon")));
            this.cmdImportSP1.Key = "cmdImportSP";
            this.cmdImportSP1.Name = "cmdImportSP1";
            this.cmdImportSP1.Text = "Sản phẩm";
            // 
            // cmdImportDM1
            // 
            this.cmdImportDM1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportDM1.Icon")));
            this.cmdImportDM1.Key = "cmdImportDM";
            this.cmdImportDM1.Name = "cmdImportDM1";
            this.cmdImportDM1.Text = "Định mức";
            // 
            // cmdImportToKhai1
            // 
            this.cmdImportToKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportToKhai1.Icon")));
            this.cmdImportToKhai1.Key = "cmdImportToKhai";
            this.cmdImportToKhai1.Name = "cmdImportToKhai1";
            this.cmdImportToKhai1.Text = "Tờ khai";
            // 
            // cmdImportHangHoa1
            // 
            this.cmdImportHangHoa1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportHangHoa1.Icon")));
            this.cmdImportHangHoa1.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa1.Name = "cmdImportHangHoa1";
            this.cmdImportHangHoa1.Text = "Hàng của tờ khai";
            // 
            // cmdNPLNhapTon1
            // 
            this.cmdNPLNhapTon1.Key = "cmdNPLNhapTon";
            this.cmdNPLNhapTon1.Name = "cmdNPLNhapTon1";
            // 
            // cmdImportNPL
            // 
            this.cmdImportNPL.Key = "cmdImportNPL";
            this.cmdImportNPL.Name = "cmdImportNPL";
            this.cmdImportNPL.Text = "Import NPL";
            // 
            // cmdImportSP
            // 
            this.cmdImportSP.Key = "cmdImportSP";
            this.cmdImportSP.Name = "cmdImportSP";
            this.cmdImportSP.Text = "Import Sản phẩm";
            // 
            // cmdImportDM
            // 
            this.cmdImportDM.Key = "cmdImportDM";
            this.cmdImportDM.Name = "cmdImportDM";
            this.cmdImportDM.Text = "Import Định mức";
            // 
            // cmdImportTTDM
            // 
            this.cmdImportTTDM.Key = "cmdImportTTDM";
            this.cmdImportTTDM.Name = "cmdImportTTDM";
            this.cmdImportTTDM.Text = "Import thông tin định mức";
            // 
            // cmdImportToKhai
            // 
            this.cmdImportToKhai.Key = "cmdImportToKhai";
            this.cmdImportToKhai.Name = "cmdImportToKhai";
            this.cmdImportToKhai.Text = "Import tờ khai";
            // 
            // cmdImportHangHoa
            // 
            this.cmdImportHangHoa.Key = "cmdImportHangHoa";
            this.cmdImportHangHoa.Name = "cmdImportHangHoa";
            this.cmdImportHangHoa.Text = "Import hàng tờ khai";
            // 
            // Command1
            // 
            this.Command1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmd20071,
            this.cmd20031,
            this.cmdVN1,
            this.cmdEL1});
            this.Command1.Key = "Command1";
            this.Command1.Name = "Command1";
            this.Command1.Text = "Giao diện";
            // 
            // cmd20071
            // 
            this.cmd20071.Key = "cmd2007";
            this.cmd20071.Name = "cmd20071";
            // 
            // cmd20031
            // 
            this.cmd20031.Key = "cmd2003";
            this.cmd20031.Name = "cmd20031";
            // 
            // cmdVN1
            // 
            this.cmdVN1.Checked = Janus.Windows.UI.InheritableBoolean.False;
            this.cmdVN1.ImageIndex = 35;
            this.cmdVN1.Key = "cmdVN";
            this.cmdVN1.Name = "cmdVN1";
            this.cmdVN1.Text = "Tiếng Việt";
            // 
            // cmdEL1
            // 
            this.cmdEL1.ImageIndex = 36;
            this.cmdEL1.Key = "cmdEL";
            this.cmdEL1.Name = "cmdEL1";
            this.cmdEL1.Text = "Tiếng Anh";
            // 
            // cmd2007
            // 
            this.cmd2007.Key = "cmd2007";
            this.cmd2007.Name = "cmd2007";
            this.cmd2007.Text = "Office 2007";
            // 
            // cmd2003
            // 
            this.cmd2003.Key = "cmd2003";
            this.cmd2003.Name = "cmd2003";
            this.cmd2003.Text = "Office 2003";
            // 
            // Command0
            // 
            this.Command0.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHelp1,
            this.cmdAutoUpdate1,
            this.Separator5,
            this.cmdTeamview1,
            this.Separator11,
            this.cmdTool1,
            this.Separator2,
            this.cmdActivate1,
            this.cmdAbout1});
            this.Command0.Key = "Command0";
            this.Command0.Name = "Command0";
            this.Command0.Text = "Trợ giúp";
            // 
            // cmdHelp1
            // 
            this.cmdHelp1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdHelp1.Icon")));
            this.cmdHelp1.Key = "cmdHelp";
            this.cmdHelp1.Name = "cmdHelp1";
            this.cmdHelp1.Text = "&Hướng dẫn sử dụng";
            // 
            // cmdAutoUpdate1
            // 
            this.cmdAutoUpdate1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAutoUpdate1.Icon")));
            this.cmdAutoUpdate1.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate1.Name = "cmdAutoUpdate1";
            // 
            // Separator5
            // 
            this.Separator5.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator5.Key = "Separator";
            this.Separator5.Name = "Separator5";
            // 
            // cmdTeamview1
            // 
            this.cmdTeamview1.Key = "cmdTeamview";
            this.cmdTeamview1.Name = "cmdTeamview1";
            // 
            // Separator11
            // 
            this.Separator11.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator11.Key = "Separator";
            this.Separator11.Name = "Separator11";
            // 
            // cmdTool1
            // 
            this.cmdTool1.Key = "cmdTool";
            this.cmdTool1.Name = "cmdTool1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdActivate1
            // 
            this.cmdActivate1.ImageIndex = 43;
            this.cmdActivate1.Key = "cmdActivate";
            this.cmdActivate1.Name = "cmdActivate1";
            // 
            // cmdAbout1
            // 
            this.cmdAbout1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdAbout1.Icon")));
            this.cmdAbout1.Key = "cmdAbout";
            this.cmdAbout1.Name = "cmdAbout1";
            this.cmdAbout1.Text = "&Thông tin sản phẩm";
            // 
            // cmdHelp
            // 
            this.cmdHelp.Key = "cmdHelp";
            this.cmdHelp.Name = "cmdHelp";
            this.cmdHelp.Shortcut = System.Windows.Forms.Shortcut.CtrlH;
            this.cmdHelp.Text = "Hướng dẫn sử dụng";
            // 
            // cmdAbout
            // 
            this.cmdAbout.Key = "cmdAbout";
            this.cmdAbout.Name = "cmdAbout";
            this.cmdAbout.Text = "Thông tin sản phẩm";
            // 
            // cmdNPLNhapTon
            // 
            this.cmdNPLNhapTon.Key = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Name = "cmdNPLNhapTon";
            this.cmdNPLNhapTon.Text = "NPL nhập tồn";
            // 
            // cmdDanhMuc
            // 
            this.cmdDanhMuc.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdGetCategoryOnline1,
            this.Separator6,
            this.cmdHaiQuan1,
            this.cmdNuoc1,
            this.cmdMaHS1,
            this.cmdNguyenTe1,
            this.cmdDVT1,
            this.cmdPTTT1,
            this.cmdPTVT1,
            this.cmdDKGH1,
            this.cmdCuaKhau1,
            this.cmdNhomCuaKhau1,
            this.cmdLoaiPhiChungTuThanhToan1});
            this.cmdDanhMuc.Key = "cmdDanhMuc";
            this.cmdDanhMuc.Name = "cmdDanhMuc";
            this.cmdDanhMuc.Text = "DanhMuc";
            // 
            // cmdGetCategoryOnline1
            // 
            this.cmdGetCategoryOnline1.Key = "cmdGetCategoryOnline";
            this.cmdGetCategoryOnline1.Name = "cmdGetCategoryOnline1";
            // 
            // Separator6
            // 
            this.Separator6.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator6.Key = "Separator";
            this.Separator6.Name = "Separator6";
            // 
            // cmdHaiQuan1
            // 
            this.cmdHaiQuan1.ImageIndex = 39;
            this.cmdHaiQuan1.Key = "cmdHaiQuan";
            this.cmdHaiQuan1.Name = "cmdHaiQuan1";
            // 
            // cmdNuoc1
            // 
            this.cmdNuoc1.ImageIndex = 39;
            this.cmdNuoc1.Key = "cmdNuoc";
            this.cmdNuoc1.Name = "cmdNuoc1";
            // 
            // cmdMaHS1
            // 
            this.cmdMaHS1.ImageIndex = 39;
            this.cmdMaHS1.Key = "cmdMaHS";
            this.cmdMaHS1.Name = "cmdMaHS1";
            // 
            // cmdNguyenTe1
            // 
            this.cmdNguyenTe1.ImageIndex = 39;
            this.cmdNguyenTe1.Key = "cmdNguyenTe";
            this.cmdNguyenTe1.Name = "cmdNguyenTe1";
            // 
            // cmdDVT1
            // 
            this.cmdDVT1.ImageIndex = 39;
            this.cmdDVT1.Key = "cmdDVT";
            this.cmdDVT1.Name = "cmdDVT1";
            // 
            // cmdPTTT1
            // 
            this.cmdPTTT1.ImageIndex = 39;
            this.cmdPTTT1.Key = "cmdPTTT";
            this.cmdPTTT1.Name = "cmdPTTT1";
            // 
            // cmdPTVT1
            // 
            this.cmdPTVT1.ImageIndex = 39;
            this.cmdPTVT1.Key = "cmdPTVT";
            this.cmdPTVT1.Name = "cmdPTVT1";
            // 
            // cmdDKGH1
            // 
            this.cmdDKGH1.ImageIndex = 39;
            this.cmdDKGH1.Key = "cmdDKGH";
            this.cmdDKGH1.Name = "cmdDKGH1";
            // 
            // cmdCuaKhau1
            // 
            this.cmdCuaKhau1.ImageIndex = 39;
            this.cmdCuaKhau1.Key = "cmdCuaKhau";
            this.cmdCuaKhau1.Name = "cmdCuaKhau1";
            // 
            // cmdNhomCuaKhau1
            // 
            this.cmdNhomCuaKhau1.ImageIndex = 39;
            this.cmdNhomCuaKhau1.Key = "cmdNhomCuaKhau";
            this.cmdNhomCuaKhau1.Name = "cmdNhomCuaKhau1";
            // 
            // cmdLoaiPhiChungTuThanhToan1
            // 
            this.cmdLoaiPhiChungTuThanhToan1.ImageIndex = 39;
            this.cmdLoaiPhiChungTuThanhToan1.Key = "cmdLoaiPhiChungTuThanhToan";
            this.cmdLoaiPhiChungTuThanhToan1.Name = "cmdLoaiPhiChungTuThanhToan1";
            // 
            // cmdMaHS
            // 
            this.cmdMaHS.Key = "cmdMaHS";
            this.cmdMaHS.Name = "cmdMaHS";
            this.cmdMaHS.Text = "Mã HS";
            // 
            // cmdNuoc
            // 
            this.cmdNuoc.Key = "cmdNuoc";
            this.cmdNuoc.Name = "cmdNuoc";
            this.cmdNuoc.Text = "Nước";
            // 
            // cmdHaiQuan
            // 
            this.cmdHaiQuan.Key = "cmdHaiQuan";
            this.cmdHaiQuan.Name = "cmdHaiQuan";
            this.cmdHaiQuan.Text = "Đơn vị Hải quan";
            // 
            // cmdNguyenTe
            // 
            this.cmdNguyenTe.Key = "cmdNguyenTe";
            this.cmdNguyenTe.Name = "cmdNguyenTe";
            this.cmdNguyenTe.Text = "Nguyên tệ";
            // 
            // cmdDVT
            // 
            this.cmdDVT.Key = "cmdDVT";
            this.cmdDVT.Name = "cmdDVT";
            this.cmdDVT.Text = "Đơn vị tính";
            // 
            // cmdPTTT
            // 
            this.cmdPTTT.Key = "cmdPTTT";
            this.cmdPTTT.Name = "cmdPTTT";
            this.cmdPTTT.Text = "Phương thức thanh toán";
            // 
            // cmdPTVT
            // 
            this.cmdPTVT.Key = "cmdPTVT";
            this.cmdPTVT.Name = "cmdPTVT";
            this.cmdPTVT.Text = "Phương thức vận tải";
            // 
            // cmdDKGH
            // 
            this.cmdDKGH.Key = "cmdDKGH";
            this.cmdDKGH.Name = "cmdDKGH";
            this.cmdDKGH.Text = "Điều kiện giao hàng";
            // 
            // cmdCuaKhau
            // 
            this.cmdCuaKhau.Key = "cmdCuaKhau";
            this.cmdCuaKhau.Name = "cmdCuaKhau";
            this.cmdCuaKhau.Text = "Cửa khẩu";
            // 
            // cmdBackUp
            // 
            this.cmdBackUp.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBackUp.Icon")));
            this.cmdBackUp.Key = "cmdBackUp";
            this.cmdBackUp.Name = "cmdBackUp";
            this.cmdBackUp.Text = "Sao lưu dữ liệu";
            // 
            // cmdRestore
            // 
            this.cmdRestore.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdRestore.Icon")));
            this.cmdRestore.Key = "cmdRestore";
            this.cmdRestore.Name = "cmdRestore";
            this.cmdRestore.Text = "Phục hồi dữ liệu";
            // 
            // ThongSoKetNoi
            // 
            this.ThongSoKetNoi.Icon = ((System.Drawing.Icon)(resources.GetObject("ThongSoKetNoi.Icon")));
            this.ThongSoKetNoi.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi.Name = "ThongSoKetNoi";
            this.ThongSoKetNoi.Text = "Thiết lập thông số kết nối";
            // 
            // TLThongTinDNHQ
            // 
            this.TLThongTinDNHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("TLThongTinDNHQ.Icon")));
            this.TLThongTinDNHQ.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Name = "TLThongTinDNHQ";
            this.TLThongTinDNHQ.Text = "Thiết lập thông tin doanh nghiệp và hải quan";
            // 
            // cmdThietLapIn
            // 
            this.cmdThietLapIn.Key = "cmdThietLapIn";
            this.cmdThietLapIn.Name = "cmdThietLapIn";
            this.cmdThietLapIn.Text = "Thiết lập thông số in báo cáo";
            // 
            // cmdExportExccel
            // 
            this.cmdExportExccel.Key = "cmdExportExccel";
            this.cmdExportExccel.Name = "cmdExportExccel";
            this.cmdExportExccel.Text = "Xuất dữ liệu ra file Excel";
            // 
            // cmdImportExcel
            // 
            this.cmdImportExcel.Key = "cmdImportExcel";
            this.cmdImportExcel.Name = "cmdImportExcel";
            this.cmdImportExcel.Text = "Nhập dữ liêu từ file Excel";
            // 
            // cmdDongBoPhongKhai
            // 
            this.cmdDongBoPhongKhai.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportExcel1,
            this.cmdExportExccel1});
            this.cmdDongBoPhongKhai.Key = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Name = "cmdDongBoPhongKhai";
            this.cmdDongBoPhongKhai.Text = "Đồng bộ dữ liệu với phòng khai";
            // 
            // cmdImportExcel1
            // 
            this.cmdImportExcel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdImportExcel1.Icon")));
            this.cmdImportExcel1.Key = "cmdImportExcel";
            this.cmdImportExcel1.Name = "cmdImportExcel1";
            // 
            // cmdExportExccel1
            // 
            this.cmdExportExccel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdExportExccel1.Icon")));
            this.cmdExportExccel1.Key = "cmdExportExccel";
            this.cmdExportExccel1.Name = "cmdExportExccel1";
            // 
            // cmdCauHinh
            // 
            this.cmdCauHinh.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThongSoKetNoi1,
            this.TLThongTinDNHQ1,
            this.cmdCauHinhToKhai1,
            this.cmdThietLapIn1,
            this.cmdChuKySo1,
            this.cmdTimer1});
            this.cmdCauHinh.Key = "cmdCauHinh";
            this.cmdCauHinh.Name = "cmdCauHinh";
            this.cmdCauHinh.Text = "Cấu hình hệ thống";
            // 
            // ThongSoKetNoi1
            // 
            this.ThongSoKetNoi1.Icon = ((System.Drawing.Icon)(resources.GetObject("ThongSoKetNoi1.Icon")));
            this.ThongSoKetNoi1.Key = "ThongSoKetNoi";
            this.ThongSoKetNoi1.Name = "ThongSoKetNoi1";
            this.ThongSoKetNoi1.Text = "Cấu hình thông số kết nối";
            // 
            // TLThongTinDNHQ1
            // 
            this.TLThongTinDNHQ1.Icon = ((System.Drawing.Icon)(resources.GetObject("TLThongTinDNHQ1.Icon")));
            this.TLThongTinDNHQ1.Key = "TLThongTinDNHQ";
            this.TLThongTinDNHQ1.Name = "TLThongTinDNHQ1";
            this.TLThongTinDNHQ1.Text = "Cấu hình thông tin doanh nghiệp và hải quan";
            // 
            // cmdCauHinhToKhai1
            // 
            this.cmdCauHinhToKhai1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCauHinhToKhai1.Icon")));
            this.cmdCauHinhToKhai1.Key = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai1.Name = "cmdCauHinhToKhai1";
            // 
            // cmdThietLapIn1
            // 
            this.cmdThietLapIn1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThietLapIn1.Icon")));
            this.cmdThietLapIn1.Key = "cmdThietLapIn";
            this.cmdThietLapIn1.Name = "cmdThietLapIn1";
            this.cmdThietLapIn1.Text = "Cấu hình thông số in báo cáo";
            // 
            // cmdChuKySo1
            // 
            this.cmdChuKySo1.Key = "cmdChuKySo";
            this.cmdChuKySo1.Name = "cmdChuKySo1";
            // 
            // cmdTimer1
            // 
            this.cmdTimer1.Key = "cmdTimer";
            this.cmdTimer1.Name = "cmdTimer1";
            // 
            // cmdCauHinhToKhai
            // 
            this.cmdCauHinhToKhai.Key = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Name = "cmdCauHinhToKhai";
            this.cmdCauHinhToKhai.Text = "Cấu hình tham số mặc định của tờ khai";
            // 
            // QuanTri
            // 
            this.QuanTri.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.QuanLyNguoiDung1,
            this.QuanLyNhom1});
            this.QuanTri.Key = "QuanTri";
            this.QuanTri.Name = "QuanTri";
            this.QuanTri.Text = "Quản trị";
            // 
            // QuanLyNguoiDung1
            // 
            this.QuanLyNguoiDung1.Icon = ((System.Drawing.Icon)(resources.GetObject("QuanLyNguoiDung1.Icon")));
            this.QuanLyNguoiDung1.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung1.Name = "QuanLyNguoiDung1";
            // 
            // QuanLyNhom1
            // 
            this.QuanLyNhom1.Icon = ((System.Drawing.Icon)(resources.GetObject("QuanLyNhom1.Icon")));
            this.QuanLyNhom1.Key = "QuanLyNhom";
            this.QuanLyNhom1.Name = "QuanLyNhom1";
            // 
            // QuanLyNguoiDung
            // 
            this.QuanLyNguoiDung.Key = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Name = "QuanLyNguoiDung";
            this.QuanLyNguoiDung.Text = "Quản lý người dùng";
            // 
            // QuanLyNhom
            // 
            this.QuanLyNhom.Key = "QuanLyNhom";
            this.QuanLyNhom.Name = "QuanLyNhom";
            this.QuanLyNhom.Text = "Quản lý nhóm người dùng";
            // 
            // LoginUser
            // 
            this.LoginUser.Icon = ((System.Drawing.Icon)(resources.GetObject("LoginUser.Icon")));
            this.LoginUser.Key = "LoginUser";
            this.LoginUser.Name = "LoginUser";
            this.LoginUser.Text = "Đăng nhập người dùng khác";
            // 
            // cmdChangePass
            // 
            this.cmdChangePass.Key = "cmdChangePass";
            this.cmdChangePass.Name = "cmdChangePass";
            this.cmdChangePass.Text = "Đổi mật khẩu";
            // 
            // cmdDoiMatKhauHQ
            // 
            this.cmdDoiMatKhauHQ.Key = "cmdDoiMatKhauHQ";
            this.cmdDoiMatKhauHQ.Name = "cmdDoiMatKhauHQ";
            this.cmdDoiMatKhauHQ.Text = "Đổi mật khẩu kết nối với hải quan";
            // 
            // cmdCuaSo
            // 
            this.cmdCuaSo.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCloseAll1,
            this.cmdCloseAllButThis1});
            this.cmdCuaSo.Key = "cmdCuaSo";
            this.cmdCuaSo.Name = "cmdCuaSo";
            this.cmdCuaSo.Text = "Cửa sổ";
            // 
            // cmdCloseAll1
            // 
            this.cmdCloseAll1.Key = "cmdCloseAll";
            this.cmdCloseAll1.Name = "cmdCloseAll1";
            // 
            // cmdCloseAllButThis1
            // 
            this.cmdCloseAllButThis1.Key = "cmdCloseAllButThis";
            this.cmdCloseAllButThis1.Name = "cmdCloseAllButThis1";
            // 
            // cmdCloseAll
            // 
            this.cmdCloseAll.Key = "cmdCloseAll";
            this.cmdCloseAll.Name = "cmdCloseAll";
            this.cmdCloseAll.Text = "Đóng tất cả";
            // 
            // cmdCloseAllButThis
            // 
            this.cmdCloseAllButThis.Key = "cmdCloseAllButThis";
            this.cmdCloseAllButThis.Name = "cmdCloseAllButThis";
            this.cmdCloseAllButThis.Text = "Đóng tất cả trừ cửa sổ đang kích hoạt";
            // 
            // cmdAutoUpdate
            // 
            this.cmdAutoUpdate.Key = "cmdAutoUpdate";
            this.cmdAutoUpdate.Name = "cmdAutoUpdate";
            this.cmdAutoUpdate.Text = "&Cập nhập chương trình";
            // 
            // cmdNhapXML
            // 
            this.cmdNhapXML.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNPL1,
            this.cmdSP1,
            this.cmdDM1,
            this.cmdTK1});
            this.cmdNhapXML.Key = "cmdNhapXML";
            this.cmdNhapXML.Name = "cmdNhapXML";
            this.cmdNhapXML.Text = "Nhập dữ liệu từ đại lý";
            // 
            // cmdNPL1
            // 
            this.cmdNPL1.ImageIndex = 51;
            this.cmdNPL1.Key = "cmdNPL";
            this.cmdNPL1.Name = "cmdNPL1";
            // 
            // cmdSP1
            // 
            this.cmdSP1.ImageIndex = 52;
            this.cmdSP1.Key = "cmdSP";
            this.cmdSP1.Name = "cmdSP1";
            // 
            // cmdDM1
            // 
            this.cmdDM1.ImageIndex = 49;
            this.cmdDM1.Key = "cmdDM";
            this.cmdDM1.Name = "cmdDM1";
            // 
            // cmdTK1
            // 
            this.cmdTK1.ImageIndex = 53;
            this.cmdTK1.Key = "cmdTK";
            this.cmdTK1.Name = "cmdTK1";
            // 
            // cmdNPL
            // 
            this.cmdNPL.Key = "cmdNPL";
            this.cmdNPL.Name = "cmdNPL";
            this.cmdNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdSP
            // 
            this.cmdSP.Key = "cmdSP";
            this.cmdSP.Name = "cmdSP";
            this.cmdSP.Text = "Sản phẩm";
            // 
            // cmdDM
            // 
            this.cmdDM.Key = "cmdDM";
            this.cmdDM.Name = "cmdDM";
            this.cmdDM.Text = "Định mức";
            // 
            // cmdTK
            // 
            this.cmdTK.Key = "cmdTK";
            this.cmdTK.Name = "cmdTK";
            this.cmdTK.Text = "Tờ khai";
            // 
            // cmdXuatDuLieu
            // 
            this.cmdXuatDuLieu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdHUNGNPL1,
            this.cmdHUNGSP1,
            this.cmdHUNGDM1,
            this.cmdHUNGTK1});
            this.cmdXuatDuLieu.Key = "cmdXuatDuLieu";
            this.cmdXuatDuLieu.Name = "cmdXuatDuLieu";
            this.cmdXuatDuLieu.Text = "Xuất dữ liệu cho phòng khai";
            // 
            // cmdHUNGNPL1
            // 
            this.cmdHUNGNPL1.ImageIndex = 51;
            this.cmdHUNGNPL1.Key = "cmdHUNGNPL";
            this.cmdHUNGNPL1.Name = "cmdHUNGNPL1";
            // 
            // cmdHUNGSP1
            // 
            this.cmdHUNGSP1.ImageIndex = 52;
            this.cmdHUNGSP1.Key = "cmdHUNGSP";
            this.cmdHUNGSP1.Name = "cmdHUNGSP1";
            // 
            // cmdHUNGDM1
            // 
            this.cmdHUNGDM1.ImageIndex = 49;
            this.cmdHUNGDM1.Key = "cmdHUNGDM";
            this.cmdHUNGDM1.Name = "cmdHUNGDM1";
            // 
            // cmdHUNGTK1
            // 
            this.cmdHUNGTK1.ImageIndex = 53;
            this.cmdHUNGTK1.Key = "cmdHUNGTK";
            this.cmdHUNGTK1.Name = "cmdHUNGTK1";
            // 
            // cmdXuatNPL
            // 
            this.cmdXuatNPL.Key = "cmdXuatNPL";
            this.cmdXuatNPL.Name = "cmdXuatNPL";
            this.cmdXuatNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdXuatDinhMuc
            // 
            this.cmdXuatDinhMuc.Key = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc.Name = "cmdXuatDinhMuc";
            this.cmdXuatDinhMuc.Text = "Định mức";
            // 
            // cmdXuatSanPham
            // 
            this.cmdXuatSanPham.Key = "cmdXuatSanPham";
            this.cmdXuatSanPham.Name = "cmdXuatSanPham";
            this.cmdXuatSanPham.Text = "Sản phẩm";
            // 
            // cmdXuatToKhai
            // 
            this.cmdXuatToKhai.Key = "cmdXuatToKhai";
            this.cmdXuatToKhai.Name = "cmdXuatToKhai";
            this.cmdXuatToKhai.Text = "Tờ khai";
            // 
            // cmdVN
            // 
            this.cmdVN.Key = "cmdVN";
            this.cmdVN.Name = "cmdVN";
            this.cmdVN.Text = "English";
            // 
            // cmdEL
            // 
            this.cmdEL.Key = "cmdEL";
            this.cmdEL.Name = "cmdEL";
            this.cmdEL.Text = "English";
            // 
            // cmdHUNGNPL
            // 
            this.cmdHUNGNPL.Key = "cmdHUNGNPL";
            this.cmdHUNGNPL.Name = "cmdHUNGNPL";
            this.cmdHUNGNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdHUNGSP
            // 
            this.cmdHUNGSP.Key = "cmdHUNGSP";
            this.cmdHUNGSP.Name = "cmdHUNGSP";
            this.cmdHUNGSP.Text = "Sản phẩm";
            // 
            // cmdHUNGDM
            // 
            this.cmdHUNGDM.Key = "cmdHUNGDM";
            this.cmdHUNGDM.Name = "cmdHUNGDM";
            this.cmdHUNGDM.Text = "Định mức";
            // 
            // cmdHUNGTK
            // 
            this.cmdHUNGTK.Key = "cmdHUNGTK";
            this.cmdHUNGTK.Name = "cmdHUNGTK";
            this.cmdHUNGTK.Text = "Tờ khai";
            // 
            // cmdActivate
            // 
            this.cmdActivate.Key = "cmdActivate";
            this.cmdActivate.Name = "cmdActivate";
            this.cmdActivate.Shortcut = System.Windows.Forms.Shortcut.CtrlF10;
            this.cmdActivate.Text = "Kích hoạt phần mềm";
            // 
            // cmdXuatDuLieuDN
            // 
            this.cmdXuatDuLieuDN.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdXuatNPLDN1,
            this.cmdXuatSPDN1,
            this.cmdXuatDMDN1,
            this.cmdXuatTKDN1});
            this.cmdXuatDuLieuDN.Key = "cmdXuatDuLieuDN";
            this.cmdXuatDuLieuDN.Name = "cmdXuatDuLieuDN";
            this.cmdXuatDuLieuDN.Text = "Xuất dữ liệu cho doanh nghiệp";
            // 
            // cmdXuatNPLDN1
            // 
            this.cmdXuatNPLDN1.ImageIndex = 51;
            this.cmdXuatNPLDN1.Key = "cmdXuatNPLDN";
            this.cmdXuatNPLDN1.Name = "cmdXuatNPLDN1";
            this.cmdXuatNPLDN1.Text = "Nguyên phụ liệu";
            // 
            // cmdXuatSPDN1
            // 
            this.cmdXuatSPDN1.ImageIndex = 52;
            this.cmdXuatSPDN1.Key = "cmdXuatSPDN";
            this.cmdXuatSPDN1.Name = "cmdXuatSPDN1";
            this.cmdXuatSPDN1.Text = "Sản phẩm";
            // 
            // cmdXuatDMDN1
            // 
            this.cmdXuatDMDN1.ImageIndex = 49;
            this.cmdXuatDMDN1.Key = "cmdXuatDMDN";
            this.cmdXuatDMDN1.Name = "cmdXuatDMDN1";
            this.cmdXuatDMDN1.Text = "Định mức";
            // 
            // cmdXuatTKDN1
            // 
            this.cmdXuatTKDN1.ImageIndex = 53;
            this.cmdXuatTKDN1.Key = "cmdXuatTKDN";
            this.cmdXuatTKDN1.Name = "cmdXuatTKDN1";
            this.cmdXuatTKDN1.Text = "Tờ khai";
            // 
            // cmdXuatNPLDN
            // 
            this.cmdXuatNPLDN.Key = "cmdXuatNPLDN";
            this.cmdXuatNPLDN.Name = "cmdXuatNPLDN";
            this.cmdXuatNPLDN.Text = "NPL";
            // 
            // cmdXuatSPDN
            // 
            this.cmdXuatSPDN.Key = "cmdXuatSPDN";
            this.cmdXuatSPDN.Name = "cmdXuatSPDN";
            this.cmdXuatSPDN.Text = "SP";
            // 
            // cmXuat
            // 
            this.cmXuat.Key = "cmXuat";
            this.cmXuat.Name = "cmXuat";
            this.cmXuat.Text = "Xuất dữ liệu cho doanh nghiệp";
            // 
            // cmdXuatDMDN
            // 
            this.cmdXuatDMDN.Key = "cmdXuatDMDN";
            this.cmdXuatDMDN.Name = "cmdXuatDMDN";
            this.cmdXuatDMDN.Text = "ĐM";
            // 
            // cmdXuatTKDN
            // 
            this.cmdXuatTKDN.Key = "cmdXuatTKDN";
            this.cmdXuatTKDN.Name = "cmdXuatTKDN";
            this.cmdXuatTKDN.Text = "TK";
            // 
            // cmdNhapDuLieu
            // 
            this.cmdNhapDuLieu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNhapNPL1,
            this.cmdNhapSanPham1,
            this.cmdNhapDinhMuc1,
            this.cmdNhapToKhai1});
            this.cmdNhapDuLieu.Key = "cmdNhapDuLieu";
            this.cmdNhapDuLieu.Name = "cmdNhapDuLieu";
            this.cmdNhapDuLieu.Text = "Nhập dữ liệu từ doanh nghiệp";
            // 
            // cmdNhapNPL1
            // 
            this.cmdNhapNPL1.ImageIndex = 51;
            this.cmdNhapNPL1.Key = "cmdNhapNPL";
            this.cmdNhapNPL1.Name = "cmdNhapNPL1";
            // 
            // cmdNhapSanPham1
            // 
            this.cmdNhapSanPham1.ImageIndex = 52;
            this.cmdNhapSanPham1.Key = "cmdNhapSanPham";
            this.cmdNhapSanPham1.Name = "cmdNhapSanPham1";
            // 
            // cmdNhapDinhMuc1
            // 
            this.cmdNhapDinhMuc1.ImageIndex = 49;
            this.cmdNhapDinhMuc1.Key = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc1.Name = "cmdNhapDinhMuc1";
            // 
            // cmdNhapToKhai1
            // 
            this.cmdNhapToKhai1.ImageIndex = 53;
            this.cmdNhapToKhai1.Key = "cmdNhapToKhai";
            this.cmdNhapToKhai1.Name = "cmdNhapToKhai1";
            // 
            // cmdNhapNPL
            // 
            this.cmdNhapNPL.Key = "cmdNhapNPL";
            this.cmdNhapNPL.Name = "cmdNhapNPL";
            this.cmdNhapNPL.Text = "Nguyên phụ liệu";
            // 
            // cmdNhapSanPham
            // 
            this.cmdNhapSanPham.Key = "cmdNhapSanPham";
            this.cmdNhapSanPham.Name = "cmdNhapSanPham";
            this.cmdNhapSanPham.Text = "Sản phẩm";
            // 
            // cmdNhapDinhMuc
            // 
            this.cmdNhapDinhMuc.Key = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc.Name = "cmdNhapDinhMuc";
            this.cmdNhapDinhMuc.Text = "Định mức";
            // 
            // cmdNhapToKhai
            // 
            this.cmdNhapToKhai.Key = "cmdNhapToKhai";
            this.cmdNhapToKhai.Name = "cmdNhapToKhai";
            this.cmdNhapToKhai.Text = "Tờ khai";
            // 
            // QuanlyMess
            // 
            this.QuanlyMess.Key = "QuanlyMess";
            this.QuanlyMess.Name = "QuanlyMess";
            this.QuanlyMess.Text = "Quản lý Message khai báo";
            // 
            // cmdConfig
            // 
            this.cmdConfig.Key = "cmdConfig";
            this.cmdConfig.Name = "cmdConfig";
            this.cmdConfig.Text = "Thiết lập cấu hình Doanh nghiệp";
            // 
            // mnuQuerySQL
            // 
            this.mnuQuerySQL.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.mnuFormSQL1,
            this.mnuMiniSQL1});
            this.mnuQuerySQL.Key = "mnuQuerySQL";
            this.mnuQuerySQL.Name = "mnuQuerySQL";
            this.mnuQuerySQL.Text = "Thực hiện truy vấn SQL";
            // 
            // mnuFormSQL1
            // 
            this.mnuFormSQL1.ImageIndex = 42;
            this.mnuFormSQL1.Key = "mnuFormSQL";
            this.mnuFormSQL1.Name = "mnuFormSQL1";
            // 
            // mnuMiniSQL1
            // 
            this.mnuMiniSQL1.ImageIndex = 42;
            this.mnuMiniSQL1.Key = "mnuMiniSQL";
            this.mnuMiniSQL1.Name = "mnuMiniSQL1";
            // 
            // TraCuuMaHS
            // 
            this.TraCuuMaHS.ImageIndex = 1;
            this.TraCuuMaHS.Key = "TraCuuMaHS";
            this.TraCuuMaHS.Name = "TraCuuMaHS";
            this.TraCuuMaHS.Text = "Tra cứu biểu thuế (Mã HS)";
            // 
            // cmdNhomCuaKhau
            // 
            this.cmdNhomCuaKhau.Key = "cmdNhomCuaKhau";
            this.cmdNhomCuaKhau.Name = "cmdNhomCuaKhau";
            this.cmdNhomCuaKhau.Text = "Nhóm cửa khẩu";
            // 
            // cmdLog
            // 
            this.cmdLog.Key = "cmdLog";
            this.cmdLog.Name = "cmdLog";
            this.cmdLog.Text = "Nhật ký chương trình";
            // 
            // cmdLoaiPhiChungTuThanhToan
            // 
            this.cmdLoaiPhiChungTuThanhToan.Key = "cmdLoaiPhiChungTuThanhToan";
            this.cmdLoaiPhiChungTuThanhToan.Name = "cmdLoaiPhiChungTuThanhToan";
            this.cmdLoaiPhiChungTuThanhToan.SelectedImageIndex = 39;
            this.cmdLoaiPhiChungTuThanhToan.Text = "Loại phí chứng từ thanh toán";
            // 
            // cmdDataVersion
            // 
            this.cmdDataVersion.Key = "cmdDataVersion";
            this.cmdDataVersion.Name = "cmdDataVersion";
            this.cmdDataVersion.Text = "[?]";
            // 
            // cmdImportXml
            // 
            this.cmdImportXml.ImageIndex = 8;
            this.cmdImportXml.Key = "cmdImportXml";
            this.cmdImportXml.Name = "cmdImportXml";
            this.cmdImportXml.Text = "Nạp dữ liệu từ xml";
            // 
            // cmdChuKySo
            // 
            this.cmdChuKySo.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdChuKySo.Icon")));
            this.cmdChuKySo.Key = "cmdChuKySo";
            this.cmdChuKySo.Name = "cmdChuKySo";
            this.cmdChuKySo.Text = "Cấu hình chữ ký số";
            // 
            // cmdTimer
            // 
            this.cmdTimer.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdTimer.Icon")));
            this.cmdTimer.Key = "cmdTimer";
            this.cmdTimer.Name = "cmdTimer";
            this.cmdTimer.Text = "Cấu hình thời gian";
            // 
            // mnuFormSQL
            // 
            this.mnuFormSQL.ImageIndex = 38;
            this.mnuFormSQL.Key = "mnuFormSQL";
            this.mnuFormSQL.Name = "mnuFormSQL";
            this.mnuFormSQL.Text = "Form SQL";
            // 
            // mnuMiniSQL
            // 
            this.mnuMiniSQL.ImageIndex = 38;
            this.mnuMiniSQL.Key = "mnuMiniSQL";
            this.mnuMiniSQL.Name = "mnuMiniSQL";
            this.mnuMiniSQL.Text = "MiniSQL";
            // 
            // cmdNhapXuat
            // 
            this.cmdNhapXuat.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImportXml1,
            this.cmdNhapXML1,
            this.cmdImportExcel2,
            this.cmdNhapDuLieu1,
            this.Separator8,
            this.cmdXuatDuLieu1,
            this.cmdXuatDuLieuDN1});
            this.cmdNhapXuat.ImageIndex = 45;
            this.cmdNhapXuat.Key = "cmdNhapXuat";
            this.cmdNhapXuat.Name = "cmdNhapXuat";
            this.cmdNhapXuat.Text = "Nhập xuất dữ liệu";
            // 
            // cmdImportXml1
            // 
            this.cmdImportXml1.ImageIndex = 48;
            this.cmdImportXml1.Key = "cmdImportXml";
            this.cmdImportXml1.Name = "cmdImportXml1";
            // 
            // cmdNhapXML1
            // 
            this.cmdNhapXML1.ImageIndex = 48;
            this.cmdNhapXML1.Key = "cmdNhapXML";
            this.cmdNhapXML1.Name = "cmdNhapXML1";
            // 
            // cmdImportExcel2
            // 
            this.cmdImportExcel2.ImageIndex = 48;
            this.cmdImportExcel2.Key = "cmdImportExcel";
            this.cmdImportExcel2.Name = "cmdImportExcel2";
            // 
            // cmdNhapDuLieu1
            // 
            this.cmdNhapDuLieu1.ImageIndex = 48;
            this.cmdNhapDuLieu1.Key = "cmdNhapDuLieu";
            this.cmdNhapDuLieu1.Name = "cmdNhapDuLieu1";
            // 
            // Separator8
            // 
            this.Separator8.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator8.Key = "Separator";
            this.Separator8.Name = "Separator8";
            // 
            // cmdXuatDuLieu1
            // 
            this.cmdXuatDuLieu1.ImageIndex = 45;
            this.cmdXuatDuLieu1.Key = "cmdXuatDuLieu";
            this.cmdXuatDuLieu1.Name = "cmdXuatDuLieu1";
            // 
            // cmdXuatDuLieuDN1
            // 
            this.cmdXuatDuLieuDN1.ImageIndex = 45;
            this.cmdXuatDuLieuDN1.Key = "cmdXuatDuLieuDN";
            this.cmdXuatDuLieuDN1.Name = "cmdXuatDuLieuDN1";
            // 
            // cmdBieuThue
            // 
            this.cmdBieuThue.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.TraCuuMaHS1,
            this.Separator7,
            this.cmdTraCuuXNKOnline1,
            this.cmdTraCuuNoThueOnline1,
            this.cmdTraCuuVanBanOnline1,
            this.cmdTuVanHQOnline1});
            this.cmdBieuThue.Key = "cmdBieuThue";
            this.cmdBieuThue.Name = "cmdBieuThue";
            this.cmdBieuThue.Text = "&Biểu thuế (Mã HS)";
            // 
            // TraCuuMaHS1
            // 
            this.TraCuuMaHS1.Key = "TraCuuMaHS";
            this.TraCuuMaHS1.Name = "TraCuuMaHS1";
            // 
            // Separator7
            // 
            this.Separator7.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator7.Key = "Separator";
            this.Separator7.Name = "Separator7";
            // 
            // cmdTraCuuXNKOnline1
            // 
            this.cmdTraCuuXNKOnline1.Key = "cmdTraCuuXNKOnline";
            this.cmdTraCuuXNKOnline1.Name = "cmdTraCuuXNKOnline1";
            // 
            // cmdTraCuuNoThueOnline1
            // 
            this.cmdTraCuuNoThueOnline1.Key = "cmdTraCuuNoThueOnline";
            this.cmdTraCuuNoThueOnline1.Name = "cmdTraCuuNoThueOnline1";
            // 
            // cmdTraCuuVanBanOnline1
            // 
            this.cmdTraCuuVanBanOnline1.Key = "cmdTraCuuVanBanOnline";
            this.cmdTraCuuVanBanOnline1.Name = "cmdTraCuuVanBanOnline1";
            // 
            // cmdTuVanHQOnline1
            // 
            this.cmdTuVanHQOnline1.Key = "cmdTuVanHQOnline";
            this.cmdTuVanHQOnline1.Name = "cmdTuVanHQOnline1";
            // 
            // cmdTraCuuXNKOnline
            // 
            this.cmdTraCuuXNKOnline.ImageIndex = 47;
            this.cmdTraCuuXNKOnline.Key = "cmdTraCuuXNKOnline";
            this.cmdTraCuuXNKOnline.Name = "cmdTraCuuXNKOnline";
            this.cmdTraCuuXNKOnline.Text = "Tra cứu biểu thuế Xuất nhập khẩu trực tuyến";
            // 
            // cmdTraCuuNoThueOnline
            // 
            this.cmdTraCuuNoThueOnline.ImageIndex = 47;
            this.cmdTraCuuNoThueOnline.Key = "cmdTraCuuNoThueOnline";
            this.cmdTraCuuNoThueOnline.Name = "cmdTraCuuNoThueOnline";
            this.cmdTraCuuNoThueOnline.Text = "Tra cứu nợ thuế trực tuyến";
            // 
            // cmdTraCuuVanBanOnline
            // 
            this.cmdTraCuuVanBanOnline.ImageIndex = 47;
            this.cmdTraCuuVanBanOnline.Key = "cmdTraCuuVanBanOnline";
            this.cmdTraCuuVanBanOnline.Name = "cmdTraCuuVanBanOnline";
            this.cmdTraCuuVanBanOnline.Text = "Tra cứu văn bản trực tuyến";
            // 
            // cmdTuVanHQOnline
            // 
            this.cmdTuVanHQOnline.ImageIndex = 47;
            this.cmdTuVanHQOnline.Key = "cmdTuVanHQOnline";
            this.cmdTuVanHQOnline.Name = "cmdTuVanHQOnline";
            this.cmdTuVanHQOnline.Text = "Tư vấn Hải quan trực tuyến";
            // 
            // cmdGetCategoryOnline
            // 
            this.cmdGetCategoryOnline.ImageIndex = 44;
            this.cmdGetCategoryOnline.Key = "cmdGetCategoryOnline";
            this.cmdGetCategoryOnline.Name = "cmdGetCategoryOnline";
            this.cmdGetCategoryOnline.Text = "Cập nhật danh mục trực tuyến";
            // 
            // cmdTeamview
            // 
            this.cmdTeamview.ImageIndex = 54;
            this.cmdTeamview.Key = "cmdTeamview";
            this.cmdTeamview.Name = "cmdTeamview";
            this.cmdTeamview.Text = "Hỗ trợ trực tuyến qua Teamview";
            // 
            // cmdCapNhatHS
            // 
            this.cmdCapNhatHS.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCapNhatHS8SoAuto1,
            this.cmdCapNhatHS8SoManual1});
            this.cmdCapNhatHS.ImageIndex = 55;
            this.cmdCapNhatHS.Key = "cmdCapNhatHS";
            this.cmdCapNhatHS.Name = "cmdCapNhatHS";
            this.cmdCapNhatHS.Text = "Cập nhật biểu thuế (Mã HS 8 số)";
            // 
            // cmdCapNhatHS8SoAuto1
            // 
            this.cmdCapNhatHS8SoAuto1.Key = "cmdCapNhatHS8SoAuto";
            this.cmdCapNhatHS8SoAuto1.Name = "cmdCapNhatHS8SoAuto1";
            // 
            // cmdCapNhatHS8SoManual1
            // 
            this.cmdCapNhatHS8SoManual1.Key = "cmdCapNhatHS8SoManual";
            this.cmdCapNhatHS8SoManual1.Name = "cmdCapNhatHS8SoManual1";
            // 
            // cmdCapNhatHS8SoAuto
            // 
            this.cmdCapNhatHS8SoAuto.ImageIndex = 55;
            this.cmdCapNhatHS8SoAuto.Key = "cmdCapNhatHS8SoAuto";
            this.cmdCapNhatHS8SoAuto.Name = "cmdCapNhatHS8SoAuto";
            this.cmdCapNhatHS8SoAuto.Text = "Cập nhật mã HS 8 số tự động";
            // 
            // cmdCapNhatHS8SoManual
            // 
            this.cmdCapNhatHS8SoManual.ImageIndex = 55;
            this.cmdCapNhatHS8SoManual.Key = "cmdCapNhatHS8SoManual";
            this.cmdCapNhatHS8SoManual.Name = "cmdCapNhatHS8SoManual";
            this.cmdCapNhatHS8SoManual.Text = "Cập nhật mã HS 8 số theo lựa chọn";
            // 
            // cmdTool
            // 
            this.cmdTool.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdImageResizeHelp1});
            this.cmdTool.ImageIndex = 57;
            this.cmdTool.Key = "cmdTool";
            this.cmdTool.Name = "cmdTool";
            this.cmdTool.Text = "Công cụ hỗ trợ";
            // 
            // cmdImageResizeHelp1
            // 
            this.cmdImageResizeHelp1.Key = "cmdImageResizeHelp";
            this.cmdImageResizeHelp1.Name = "cmdImageResizeHelp1";
            // 
            // cmdImageResizeHelp
            // 
            this.cmdImageResizeHelp.ImageIndex = 58;
            this.cmdImageResizeHelp.Key = "cmdImageResizeHelp";
            this.cmdImageResizeHelp.Name = "cmdImageResizeHelp";
            this.cmdImageResizeHelp.Text = "Hướng dẫn sử dụng điều chỉnh dung lượng ảnh";
            // 
            // cmbDongBoDuLieu
            // 
            this.cmbDongBoDuLieu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.DongBoDuLieu1,
            this.cmdQuanLyDL1});
            this.cmbDongBoDuLieu.Key = "cmbDongBoDuLieu";
            this.cmbDongBoDuLieu.Name = "cmbDongBoDuLieu";
            this.cmbDongBoDuLieu.Text = "Đồng bộ dữ liệu";
            // 
            // DongBoDuLieu1
            // 
            this.DongBoDuLieu1.Key = "DongBoDuLieu";
            this.DongBoDuLieu1.Name = "DongBoDuLieu1";
            // 
            // cmdQuanLyDL1
            // 
            this.cmdQuanLyDL1.Key = "cmdQuanLyDL";
            this.cmdQuanLyDL1.Name = "cmdQuanLyDL1";
            // 
            // cmdQuanLyDL
            // 
            this.cmdQuanLyDL.ImageIndex = 37;
            this.cmdQuanLyDL.Key = "cmdQuanLyDL";
            this.cmdQuanLyDL.Name = "cmdQuanLyDL";
            this.cmdQuanLyDL.Text = "Quản lý đại lý";
            // 
            // cmdDaily
            // 
            this.cmdDaily.Key = "cmdDaily";
            this.cmdDaily.Name = "cmdDaily";
            this.cmdDaily.Text = "Doanh nghiệp khai Đai Lý";
            // 
            // ilSmall
            // 
            this.ilSmall.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmall.ImageStream")));
            this.ilSmall.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmall.Images.SetKeyName(0, "");
            this.ilSmall.Images.SetKeyName(1, "");
            this.ilSmall.Images.SetKeyName(2, "");
            this.ilSmall.Images.SetKeyName(3, "");
            this.ilSmall.Images.SetKeyName(4, "");
            this.ilSmall.Images.SetKeyName(5, "");
            this.ilSmall.Images.SetKeyName(6, "");
            this.ilSmall.Images.SetKeyName(7, "");
            this.ilSmall.Images.SetKeyName(8, "");
            this.ilSmall.Images.SetKeyName(9, "");
            this.ilSmall.Images.SetKeyName(10, "");
            this.ilSmall.Images.SetKeyName(11, "");
            this.ilSmall.Images.SetKeyName(12, "");
            this.ilSmall.Images.SetKeyName(13, "");
            this.ilSmall.Images.SetKeyName(14, "");
            this.ilSmall.Images.SetKeyName(15, "");
            this.ilSmall.Images.SetKeyName(16, "");
            this.ilSmall.Images.SetKeyName(17, "");
            this.ilSmall.Images.SetKeyName(18, "");
            this.ilSmall.Images.SetKeyName(19, "");
            this.ilSmall.Images.SetKeyName(20, "");
            this.ilSmall.Images.SetKeyName(21, "");
            this.ilSmall.Images.SetKeyName(22, "");
            this.ilSmall.Images.SetKeyName(23, "");
            this.ilSmall.Images.SetKeyName(24, "");
            this.ilSmall.Images.SetKeyName(25, "");
            this.ilSmall.Images.SetKeyName(26, "");
            this.ilSmall.Images.SetKeyName(27, "");
            this.ilSmall.Images.SetKeyName(28, "");
            this.ilSmall.Images.SetKeyName(29, "");
            this.ilSmall.Images.SetKeyName(30, "");
            this.ilSmall.Images.SetKeyName(31, "");
            this.ilSmall.Images.SetKeyName(32, "");
            this.ilSmall.Images.SetKeyName(33, "");
            this.ilSmall.Images.SetKeyName(34, "");
            this.ilSmall.Images.SetKeyName(35, "vi-VN.gif");
            this.ilSmall.Images.SetKeyName(36, "en-US.gif");
            this.ilSmall.Images.SetKeyName(37, "shell32_279.ico");
            this.ilSmall.Images.SetKeyName(38, "RightDatabase32.gif");
            this.ilSmall.Images.SetKeyName(39, "folder_page.png");
            this.ilSmall.Images.SetKeyName(40, "key.png");
            this.ilSmall.Images.SetKeyName(41, "shell32_21.ico");
            this.ilSmall.Images.SetKeyName(42, "DatabaseLinkerDatabasesOrphan.png");
            this.ilSmall.Images.SetKeyName(43, "86.ico");
            this.ilSmall.Images.SetKeyName(44, "cmdAutoUpdate1.Icon.ico");
            this.ilSmall.Images.SetKeyName(45, "export.ico");
            this.ilSmall.Images.SetKeyName(46, "application_view_tile.png");
            this.ilSmall.Images.SetKeyName(47, "web_find.png");
            this.ilSmall.Images.SetKeyName(48, "import.ico");
            this.ilSmall.Images.SetKeyName(49, "cmdImportDM1.Icon.ico");
            this.ilSmall.Images.SetKeyName(50, "cmdImportHangHoa1.Icon.ico");
            this.ilSmall.Images.SetKeyName(51, "cmdImportNPL1.Icon.ico");
            this.ilSmall.Images.SetKeyName(52, "cmdImportSP1.Icon.ico");
            this.ilSmall.Images.SetKeyName(53, "cmdImportToKhai1.Icon.ico");
            this.ilSmall.Images.SetKeyName(54, "TeamViewer.ico");
            this.ilSmall.Images.SetKeyName(55, "page_edit.png");
            this.ilSmall.Images.SetKeyName(56, "cmdRestore.Icon.ico");
            this.ilSmall.Images.SetKeyName(57, "TienIch1.Icon.ico");
            this.ilSmall.Images.SetKeyName(58, "help_16.png");
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 24);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 494);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbMenu});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbMenu);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(820, 26);
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Key = "cmdExportExccel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // cmdThoat1
            // 
            this.cmdThoat1.Key = "cmdThoat";
            this.cmdThoat1.Name = "cmdThoat1";
            // 
            // pmMain
            // 
            this.pmMain.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(247)))));
            this.pmMain.ContainerControl = this;
            this.pmMain.DefaultPanelSettings.ActiveCaptionMode = Janus.Windows.UI.Dock.ActiveCaptionMode.Never;
            this.pmMain.DefaultPanelSettings.CaptionDisplayMode = Janus.Windows.UI.Dock.PanelCaptionDisplayMode.Text;
            this.pmMain.DefaultPanelSettings.CaptionHeight = 30;
            this.pmMain.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.pmMain.DefaultPanelSettings.DarkCaptionFormatStyle.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.pmMain.DefaultPanelSettings.InnerAreaStyle = Janus.Windows.UI.Dock.PanelInnerAreaStyle.Window;
            this.pmMain.DefaultPanelSettings.TabStateStyles.FormatStyle.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.pmMain.TabbedMdi = true;
            this.pmMain.TabbedMdiSettings.TabStateStyles.SelectedFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.pmMain.Tag = null;
            this.pmMain.VisualStyleManager = this.vsmMain;
            this.uiPanel0.Id = new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("ff6e7c0b-1418-48c4-93db-38c36983d83e");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.uiPanel2.Id = new System.Guid("aa65e349-23e9-4c90-b1e3-2ee334b109d8");
            this.uiPanel0.Panels.Add(this.uiPanel2);
            this.pmMain.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.pmMain.BeginPanelInfo();
            this.pmMain.AddDockPanelInfo(new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, Janus.Windows.UI.Dock.PanelDockStyle.Left, true, new System.Drawing.Size(200, 427), true);
            this.pmMain.AddDockPanelInfo(new System.Guid("ff6e7c0b-1418-48c4-93db-38c36983d83e"), new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), -1, true);
            this.pmMain.AddDockPanelInfo(new System.Guid("aa65e349-23e9-4c90-b1e3-2ee334b109d8"), new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), -1, true);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("96d876c4-e448-4823-b699-fcff62ff56b1"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(88, 116), new System.Drawing.Size(0, 6), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("2b3e5f09-9a24-4b99-bf7e-8ee886f8383d"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("d5e59413-5184-45bc-bbc5-9b40a268e6ec"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("52dc898d-e5c5-4c3e-964e-6134d41411e2"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("038f8df0-b141-4aac-bb44-6015ce71b26f"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("ab8bee13-8397-4584-b8e9-5cfc2b506e10"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("adc6599d-0d45-4f54-a9f5-4903d12e3180"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("909dcf6f-3f87-4ae2-9530-c163039509ea"), Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator, true, new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("ff6e7c0b-1418-48c4-93db-38c36983d83e"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.AddFloatingPanelInfo(new System.Guid("aa65e349-23e9-4c90-b1e3-2ee334b109d8"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.pmMain.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.GroupStyle = Janus.Windows.UI.Dock.PanelGroupStyle.OutlookNavigator;
            this.uiPanel0.Location = new System.Drawing.Point(3, 29);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.SelectedPanel = this.uiPanel1;
            this.uiPanel0.Size = new System.Drawing.Size(200, 427);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Panel 0";
            // 
            // uiPanel1
            // 
            this.uiPanel1.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiPanel1.Icon")));
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 0);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(196, 325);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Loại hình SXXK";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.expSXXK);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(194, 294);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // expSXXK
            // 
            this.expSXXK.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expSXXK.BackgroundFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expSXXK.ColumnSeparation = 20;
            this.expSXXK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expSXXK.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarGroup1.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarGroup1.Icon")));
            explorerBarGroup1.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarGroup1.Image")));
            explorerBarItem1.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem1.Icon")));
            explorerBarItem1.Key = "nplKhaiBao";
            explorerBarItem1.Text = "Khai báo ";
            explorerBarItem2.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem2.Icon")));
            explorerBarItem2.Key = "nplTheoDoi";
            explorerBarItem2.Text = "Theo dõi";
            explorerBarItem3.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem3.Icon")));
            explorerBarItem3.Key = "nplDaDangKy";
            explorerBarItem3.Text = "Đã đăng ký";
            explorerBarGroup1.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem1,
            explorerBarItem2,
            explorerBarItem3});
            explorerBarGroup1.Key = "grpNguyenPhuLieu";
            explorerBarGroup1.Text = "Nguyên phụ liệu";
            explorerBarGroup2.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarGroup2.Icon")));
            explorerBarItem4.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem4.Icon")));
            explorerBarItem4.Key = "spKhaiBao";
            explorerBarItem4.Text = "Khai báo";
            explorerBarItem5.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem5.Icon")));
            explorerBarItem5.Key = "spTheoDoi";
            explorerBarItem5.Text = "Theo dõi";
            explorerBarItem6.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem6.Icon")));
            explorerBarItem6.Key = "spDaDangKy";
            explorerBarItem6.Text = "Đã đăng ký";
            explorerBarGroup2.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem4,
            explorerBarItem5,
            explorerBarItem6});
            explorerBarGroup2.Key = "grpSanPham";
            explorerBarGroup2.Text = "Sản phẩm";
            explorerBarGroup3.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarGroup3.Icon")));
            explorerBarItem7.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem7.Icon")));
            explorerBarItem7.Key = "dmKhaiBao";
            explorerBarItem7.Text = "Khai báo";
            explorerBarItem8.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem8.Icon")));
            explorerBarItem8.Key = "dmTheoDoi";
            explorerBarItem8.Text = "Theo dõi";
            explorerBarItem9.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem9.Icon")));
            explorerBarItem9.Key = "dmDaDangKy";
            explorerBarItem9.Text = "Đã đăng ký";
            explorerBarGroup3.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem7,
            explorerBarItem8,
            explorerBarItem9});
            explorerBarGroup3.Key = "grpDinhMuc";
            explorerBarGroup3.Text = "Định mức";
            explorerBarGroup4.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarGroup4.Icon")));
            explorerBarItem10.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem10.Icon")));
            explorerBarItem10.Key = "tkNhapKhau";
            explorerBarItem10.Text = "Nhập khẩu";
            explorerBarItem11.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem11.Icon")));
            explorerBarItem11.Key = "tkXuatKhau";
            explorerBarItem11.Text = "Xuất khẩu";
            explorerBarItem12.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem12.Icon")));
            explorerBarItem12.Key = "TheoDoiTKSXXK";
            explorerBarItem12.Text = "Theo dõi ";
            explorerBarItem13.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem13.Icon")));
            explorerBarItem13.Key = "ToKhaiSXXKDangKy";
            explorerBarItem13.Text = "Đã đăng ký";
            explorerBarItem14.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem14.Icon")));
            explorerBarItem14.Key = "tkHetHan";
            explorerBarItem14.Text = "Sắp hết hạn TK";
            explorerBarItem15.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem15.Icon")));
            explorerBarItem15.Key = "ChiPhiXNK";
            explorerBarItem15.Text = "Chi phí XNK";
            explorerBarItem16.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem16.Icon")));
            explorerBarItem16.Key = "tkNopThue";
            explorerBarItem16.Text = "Tờ khai phải nộp thuế";
            explorerBarItem17.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem17.Icon")));
            explorerBarItem17.Key = "TKHangTKX";
            explorerBarItem17.Text = "Xem hàng tờ  khai xuất";
            explorerBarGroup4.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem10,
            explorerBarItem11,
            explorerBarItem12,
            explorerBarItem13,
            explorerBarItem14,
            explorerBarItem15,
            explorerBarItem16,
            explorerBarItem17});
            explorerBarGroup4.Key = "grpToKhai";
            explorerBarGroup4.Text = "Tờ khai";
            explorerBarGroup5.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarGroup5.Icon")));
            explorerBarItem18.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem18.Icon")));
            explorerBarItem18.Key = "AddHSTL";
            explorerBarItem18.Text = "Tạo mới";
            explorerBarItem19.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem19.Icon")));
            explorerBarItem19.Key = "UpdateHSTL";
            explorerBarItem19.Text = "Cập nhật";
            explorerBarItem20.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem20.Icon")));
            explorerBarItem20.Key = "HSTLManage";
            explorerBarItem20.Text = "Theo dõi";
            explorerBarItem21.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem21.Icon")));
            explorerBarItem21.Key = "HSTLClosed";
            explorerBarItem21.Text = "Đã đóng";
            explorerBarItem22.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem22.Icon")));
            explorerBarItem22.Key = "QDThanhKhoanTKN";
            explorerBarItem22.Text = "QĐ thanh khoản TKN";
            explorerBarGroup5.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem18,
            explorerBarItem19,
            explorerBarItem20,
            explorerBarItem21,
            explorerBarItem22});
            explorerBarGroup5.Key = "grpThanhLy";
            explorerBarGroup5.Text = "Hồ sơ thanh lý";
            explorerBarItem23.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem23.Image")));
            explorerBarItem23.Key = "dmhhKhaiBao";
            explorerBarItem23.Text = "Khai báo";
            explorerBarItem24.Image = ((System.Drawing.Image)(resources.GetObject("explorerBarItem24.Image")));
            explorerBarItem24.Key = "dmhhTheoDoi";
            explorerBarItem24.Text = "Theo dõi";
            explorerBarGroup6.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem23,
            explorerBarItem24});
            explorerBarGroup6.Key = "grpDMHH";
            explorerBarGroup6.Text = "Danh mục hàng hóa";
            explorerBarGroup6.Visible = false;
            this.expSXXK.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup1,
            explorerBarGroup2,
            explorerBarGroup3,
            explorerBarGroup4,
            explorerBarGroup5,
            explorerBarGroup6});
            this.expSXXK.GroupSeparation = 10;
            this.expSXXK.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expSXXK.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expSXXK.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expSXXK.ImageSize = new System.Drawing.Size(16, 16);
            this.expSXXK.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expSXXK.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expSXXK.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expSXXK.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expSXXK.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expSXXK.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expSXXK.Location = new System.Drawing.Point(0, 0);
            this.expSXXK.Name = "expSXXK";
            this.expSXXK.Size = new System.Drawing.Size(194, 294);
            this.expSXXK.TabIndex = 1;
            this.expSXXK.Text = "explorerBar1";
            this.expSXXK.TopMargin = 10;
            this.expSXXK.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2003;
            this.expSXXK.VisualStyleManager = this.vsmMain;
            this.expSXXK.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expSXXK_ItemClick);
            // 
            // uiPanel2
            // 
            this.uiPanel2.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiPanel2.Icon")));
            this.uiPanel2.InnerContainer = this.uiPanel2Container;
            this.uiPanel2.Location = new System.Drawing.Point(0, 0);
            this.uiPanel2.Name = "uiPanel2";
            this.uiPanel2.Size = new System.Drawing.Size(196, 325);
            this.uiPanel2.TabIndex = 4;
            this.uiPanel2.Text = "Quản lý NPL tồn";
            // 
            // uiPanel2Container
            // 
            this.uiPanel2Container.Controls.Add(this.expKhaiBao_TheoDoi);
            this.uiPanel2Container.Location = new System.Drawing.Point(1, 31);
            this.uiPanel2Container.Name = "uiPanel2Container";
            this.uiPanel2Container.Size = new System.Drawing.Size(194, 294);
            this.uiPanel2Container.TabIndex = 0;
            // 
            // expKhaiBao_TheoDoi
            // 
            this.expKhaiBao_TheoDoi.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKhaiBao_TheoDoi.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expKhaiBao_TheoDoi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expKhaiBao_TheoDoi.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem25.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem25.Icon")));
            explorerBarItem25.Key = "tdNPLton";
            explorerBarItem25.Text = "Theo dõi NPL Tồn";
            explorerBarItem26.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem26.Icon")));
            explorerBarItem26.Key = "tkNPLton";
            explorerBarItem26.Text = "Thống kê NPL tồn";
            explorerBarItem27.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem27.Icon")));
            explorerBarItem27.Key = "tdPBTKN";
            explorerBarItem27.Text = "Theo dõi TKN tự phân bổ";
            explorerBarGroup7.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem25,
            explorerBarItem26,
            explorerBarItem27});
            explorerBarGroup7.Key = "grpNPLTon";
            explorerBarGroup7.Text = "Nguyên Phụ Liệu Tồn";
            explorerBarItem28.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem28.Icon")));
            explorerBarItem28.Key = "tkLHKNhap";
            explorerBarItem28.Text = "Tờ Khai Nhập";
            explorerBarItem29.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem29.Icon")));
            explorerBarItem29.Key = "tkLHKXuat";
            explorerBarItem29.Text = "Tờ Khai Xuất";
            explorerBarGroup8.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem28,
            explorerBarItem29});
            explorerBarGroup8.Key = "grpTKLoaiHinhKhac";
            explorerBarGroup8.Text = "Tờ khai nhập từ hệ thống khác";
            explorerBarGroup9.Expanded = false;
            explorerBarItem30.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem30.Icon")));
            explorerBarItem30.Key = "thueTonTKNhap";
            explorerBarItem30.Text = "Thuế tồn tờ khai nhập";
            explorerBarItem31.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem31.Icon")));
            explorerBarItem31.Key = "triGiaTKNhap";
            explorerBarItem31.Text = "Trị giá hàng tờ khai nhập";
            explorerBarItem32.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem32.Icon")));
            explorerBarItem32.Key = "triGiaTKXuat";
            explorerBarItem32.Text = "Trị giá hàng tờ khai xuất";
            explorerBarGroup9.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem30,
            explorerBarItem31,
            explorerBarItem32});
            explorerBarGroup9.Key = "grpQuanLyToKhai";
            explorerBarGroup9.Text = "Quản lý tờ khai";
            explorerBarItem33.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem33.Icon")));
            explorerBarItem33.Key = "itemKTDinhMuc";
            explorerBarItem33.Text = "Định mức";
            explorerBarItem34.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem34.Icon")));
            explorerBarItem34.Key = "itemKTToKhai";
            explorerBarItem34.Text = "Tờ khai";
            explorerBarGroup10.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem33,
            explorerBarItem34});
            explorerBarGroup10.Key = "grpKiemTraDuLieu";
            explorerBarGroup10.Text = "Dữ liệu khai sai";
            this.expKhaiBao_TheoDoi.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup7,
            explorerBarGroup8,
            explorerBarGroup9,
            explorerBarGroup10});
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expKhaiBao_TheoDoi.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expKhaiBao_TheoDoi.ImageSize = new System.Drawing.Size(16, 16);
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKhaiBao_TheoDoi.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expKhaiBao_TheoDoi.Location = new System.Drawing.Point(0, 0);
            this.expKhaiBao_TheoDoi.Name = "expKhaiBao_TheoDoi";
            this.expKhaiBao_TheoDoi.Size = new System.Drawing.Size(194, 294);
            this.expKhaiBao_TheoDoi.TabIndex = 2;
            this.expKhaiBao_TheoDoi.Text = "explorerBar1";
            this.expKhaiBao_TheoDoi.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2003;
            this.expKhaiBao_TheoDoi.VisualStyleManager = this.vsmMain;
            this.expKhaiBao_TheoDoi.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expKhaiBao_TheoDoi_ItemClick);
            // 
            // pnlSXXK
            // 
            this.pnlSXXK.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSXXK.Icon")));
            this.pnlSXXK.InnerContainer = this.pnlSXXKContainer;
            this.pnlSXXK.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXK.Name = "pnlSXXK";
            this.pnlSXXK.Size = new System.Drawing.Size(209, 335);
            this.pnlSXXK.TabIndex = 4;
            this.pnlSXXK.Text = "Loại hình SXXK";
            // 
            // pnlSXXKContainer
            // 
            this.pnlSXXKContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSXXKContainer.Name = "pnlSXXKContainer";
            this.pnlSXXKContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlSXXKContainer.TabIndex = 0;
            // 
            // pnlGiaCong
            // 
            this.pnlGiaCong.Closed = true;
            this.pnlGiaCong.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlGiaCong.Icon")));
            this.pnlGiaCong.InnerContainer = this.pnlGiaCongContainer;
            this.pnlGiaCong.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCong.Name = "pnlGiaCong";
            this.pnlGiaCong.Size = new System.Drawing.Size(209, 335);
            this.pnlGiaCong.TabIndex = 4;
            this.pnlGiaCong.Text = "Loại hình gia công";
            // 
            // pnlGiaCongContainer
            // 
            this.pnlGiaCongContainer.Controls.Add(this.expGiaCong);
            this.pnlGiaCongContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlGiaCongContainer.Name = "pnlGiaCongContainer";
            this.pnlGiaCongContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlGiaCongContainer.TabIndex = 0;
            // 
            // expGiaCong
            // 
            this.expGiaCong.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expGiaCong.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expGiaCong.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem35.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem35.Icon")));
            explorerBarItem35.Key = "hdgcNhap";
            explorerBarItem35.Text = "Khai báo";
            explorerBarItem36.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem36.Icon")));
            explorerBarItem36.Key = "hdgcManage";
            explorerBarItem36.Text = "Theo dõi";
            explorerBarItem37.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem37.Icon")));
            explorerBarItem37.Key = "hdgcRegisted";
            explorerBarItem37.Text = "Đã đăng ký";
            explorerBarGroup11.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem35,
            explorerBarItem36,
            explorerBarItem37});
            explorerBarGroup11.Key = "grpHopDong";
            explorerBarGroup11.Text = "Hợp đồng";
            explorerBarItem38.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem38.Icon")));
            explorerBarItem38.Key = "dmSend";
            explorerBarItem38.Text = "Khai báo";
            explorerBarItem39.Icon = ((System.Drawing.Icon)(resources.GetObject("explorerBarItem39.Icon")));
            explorerBarItem39.Key = "dmManage";
            explorerBarItem39.Text = "Theo dõi";
            explorerBarItem40.Key = "dmRegisted";
            explorerBarItem40.Text = "Đã đăng ký";
            explorerBarGroup12.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem38,
            explorerBarItem39,
            explorerBarItem40});
            explorerBarGroup12.Key = "grpDinhMuc";
            explorerBarGroup12.Text = "Định mức";
            explorerBarItem41.Key = "pkgcNhap";
            explorerBarItem41.Text = "Khai báo";
            explorerBarItem42.Key = "pkgcManage";
            explorerBarItem42.Text = "Theo dõi";
            explorerBarItem43.Key = "pkgcRegisted";
            explorerBarItem43.Text = "Đã đăng ký";
            explorerBarGroup13.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem41,
            explorerBarItem42,
            explorerBarItem43});
            explorerBarGroup13.Key = "grpPhuKien";
            explorerBarGroup13.Text = "Phụ kiện";
            explorerBarItem44.Key = "tkNhapKhau_GC";
            explorerBarItem44.Text = "Nhập khẩu";
            explorerBarItem45.Key = "tkXuatKhau_GC";
            explorerBarItem45.Text = "Xuất khẩu";
            explorerBarGroup14.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem44,
            explorerBarItem45});
            explorerBarGroup14.Key = "grpToKhai";
            explorerBarGroup14.Text = "Tờ khai";
            explorerBarItem46.Key = "tkGCCTNhap";
            explorerBarItem46.Text = "Tờ khai GCCT nhập";
            explorerBarItem47.Key = "tkGCCTXuat";
            explorerBarItem47.Text = "Tờ khai GCCT xuất";
            explorerBarItem48.Key = "theodoiTKCT";
            explorerBarItem48.Text = "Theo dõi";
            explorerBarGroup15.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem46,
            explorerBarItem47,
            explorerBarItem48});
            explorerBarGroup15.Key = "grpGCCT";
            explorerBarGroup15.Text = "Tờ khai GCCT";
            this.expGiaCong.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup11,
            explorerBarGroup12,
            explorerBarGroup13,
            explorerBarGroup14,
            explorerBarGroup15});
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expGiaCong.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expGiaCong.ImageSize = new System.Drawing.Size(16, 16);
            this.expGiaCong.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expGiaCong.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expGiaCong.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expGiaCong.Location = new System.Drawing.Point(0, 0);
            this.expGiaCong.Name = "expGiaCong";
            this.expGiaCong.Size = new System.Drawing.Size(209, 335);
            this.expGiaCong.TabIndex = 1;
            this.expGiaCong.Text = "explorerBar2";
            this.expGiaCong.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2003;
            this.expGiaCong.VisualStyleManager = this.vsmMain;
            this.expGiaCong.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expGiaCong_ItemClick);
            // 
            // pnlKinhDoanh
            // 
            this.pnlKinhDoanh.Closed = true;
            this.pnlKinhDoanh.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlKinhDoanh.Icon")));
            this.pnlKinhDoanh.InnerContainer = this.pnlKinhDoanhContainer;
            this.pnlKinhDoanh.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanh.Name = "pnlKinhDoanh";
            this.pnlKinhDoanh.Size = new System.Drawing.Size(209, 335);
            this.pnlKinhDoanh.TabIndex = 4;
            this.pnlKinhDoanh.Text = "Loại hình kinh doanh";
            // 
            // pnlKinhDoanhContainer
            // 
            this.pnlKinhDoanhContainer.Controls.Add(this.expKD);
            this.pnlKinhDoanhContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlKinhDoanhContainer.Name = "pnlKinhDoanhContainer";
            this.pnlKinhDoanhContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlKinhDoanhContainer.TabIndex = 0;
            // 
            // expKD
            // 
            this.expKD.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expKD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expKD.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem49.Key = "tkNhapKhau_KD";
            explorerBarItem49.Text = "Nhập khẩu";
            explorerBarItem50.Key = "tkXuatKhau_KD";
            explorerBarItem50.Text = "Xuất khẩu";
            explorerBarGroup16.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem49,
            explorerBarItem50});
            explorerBarGroup16.Key = "grpToKhai";
            explorerBarGroup16.Text = "Tờ khai";
            this.expKD.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup16});
            this.expKD.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expKD.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expKD.ImageSize = new System.Drawing.Size(16, 16);
            this.expKD.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expKD.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expKD.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expKD.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expKD.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expKD.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expKD.Location = new System.Drawing.Point(0, 0);
            this.expKD.Name = "expKD";
            this.expKD.Size = new System.Drawing.Size(209, 335);
            this.expKD.TabIndex = 1;
            this.expKD.Text = "explorerBar1";
            this.expKD.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2003;
            this.expKD.VisualStyleManager = this.vsmMain;
            this.expKD.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expKD_ItemClick);
            // 
            // pnlDauTu
            // 
            this.pnlDauTu.Closed = true;
            this.pnlDauTu.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlDauTu.Icon")));
            this.pnlDauTu.InnerContainer = this.pnlDauTuContainer;
            this.pnlDauTu.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTu.Name = "pnlDauTu";
            this.pnlDauTu.Size = new System.Drawing.Size(209, 335);
            this.pnlDauTu.TabIndex = 4;
            this.pnlDauTu.Text = "Loại hình đầu tư";
            // 
            // pnlDauTuContainer
            // 
            this.pnlDauTuContainer.Controls.Add(this.expDT);
            this.pnlDauTuContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlDauTuContainer.Name = "pnlDauTuContainer";
            this.pnlDauTuContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlDauTuContainer.TabIndex = 0;
            // 
            // expDT
            // 
            this.expDT.BackgroundFormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.BorderStyle = Janus.Windows.ExplorerBar.BorderStyle.None;
            this.expDT.Cursor = System.Windows.Forms.Cursors.Hand;
            this.expDT.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem51.Key = "tkNhapKhau_DT";
            explorerBarItem51.Text = "Nhập khẩu";
            explorerBarItem52.Key = "tkXuatKhau_DT";
            explorerBarItem52.Text = "Xuất khẩu";
            explorerBarGroup17.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem51,
            explorerBarItem52});
            explorerBarGroup17.Key = "grpToKhai";
            explorerBarGroup17.Text = "Tờ khai";
            this.expDT.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup17});
            this.expDT.GroupsStateStyles.FormatStyle.BackColor = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackColorGradient = System.Drawing.Color.White;
            this.expDT.GroupsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.GroupHeaderInverted;
            this.expDT.ImageSize = new System.Drawing.Size(16, 16);
            this.expDT.ItemsStateStyles.FormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.None;
            this.expDT.ItemsStateStyles.FormatStyle.Blend = 0.55F;
            this.expDT.ItemsStateStyles.FormatStyle.ForeColor = System.Drawing.Color.Black;
            this.expDT.ItemsStateStyles.FormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.None;
            this.expDT.ItemsStateStyles.HotFormatStyle.BackgroundThemeStyle = Janus.Windows.ExplorerBar.BackgroundThemeStyle.Items;
            this.expDT.ItemsStateStyles.HotFormatStyle.ForegroundThemeStyle = Janus.Windows.ExplorerBar.ForegroundThemeStyle.Item;
            this.expDT.Location = new System.Drawing.Point(0, 0);
            this.expDT.Name = "expDT";
            this.expDT.Size = new System.Drawing.Size(209, 335);
            this.expDT.TabIndex = 2;
            this.expDT.Text = "explorerBar1";
            this.expDT.VisualStyle = Janus.Windows.ExplorerBar.VisualStyle.Office2003;
            this.expDT.VisualStyleManager = this.vsmMain;
            this.expDT.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.expDT_ItemClick);
            // 
            // pnlSend
            // 
            this.pnlSend.Icon = ((System.Drawing.Icon)(resources.GetObject("pnlSend.Icon")));
            this.pnlSend.InnerContainer = this.pnlSendContainer;
            this.pnlSend.Location = new System.Drawing.Point(0, 0);
            this.pnlSend.Name = "pnlSend";
            this.pnlSend.Size = new System.Drawing.Size(209, 335);
            this.pnlSend.TabIndex = 4;
            this.pnlSend.Text = "Khai báo / Theo dõi tờ khai";
            // 
            // pnlSendContainer
            // 
            this.pnlSendContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlSendContainer.Name = "pnlSendContainer";
            this.pnlSendContainer.Size = new System.Drawing.Size(209, 335);
            this.pnlSendContainer.TabIndex = 0;
            // 
            // ilMedium
            // 
            this.ilMedium.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilMedium.ImageStream")));
            this.ilMedium.TransparentColor = System.Drawing.Color.Transparent;
            this.ilMedium.Images.SetKeyName(0, "");
            this.ilMedium.Images.SetKeyName(1, "");
            this.ilMedium.Images.SetKeyName(2, "");
            this.ilMedium.Images.SetKeyName(3, "");
            // 
            // ilLarge
            // 
            this.ilLarge.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLarge.ImageStream")));
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLarge.Images.SetKeyName(0, "");
            this.ilLarge.Images.SetKeyName(1, "");
            this.ilLarge.Images.SetKeyName(2, "");
            this.ilLarge.Images.SetKeyName(3, "");
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 459);
            this.statusBar.Name = "statusBar";
            uiStatusBarPanel1.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel1.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel1.Key = "DoanhNghiep";
            uiStatusBarPanel1.ProgressBarValue = 0;
            uiStatusBarPanel1.Width = 285;
            uiStatusBarPanel2.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Spring;
            uiStatusBarPanel2.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel2.Key = "HaiQuan";
            uiStatusBarPanel2.ProgressBarValue = 0;
            uiStatusBarPanel2.ToggleKeyValue = Janus.Windows.UI.StatusBar.ToggleKeyValue.NumLock;
            uiStatusBarPanel2.Width = 285;
            uiStatusBarPanel3.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel3.Key = "System";
            uiStatusBarPanel3.PanelType = Janus.Windows.UI.StatusBar.StatusBarPanelType.ProgressBar;
            uiStatusBarPanel3.ProgressBarValue = 0;
            uiStatusBarPanel4.Alignment = System.Windows.Forms.HorizontalAlignment.Center;
            uiStatusBarPanel4.AutoSize = System.Windows.Forms.StatusBarPanelAutoSize.Contents;
            uiStatusBarPanel4.BorderColor = System.Drawing.Color.Empty;
            uiStatusBarPanel4.Key = "Time";
            uiStatusBarPanel4.ProgressBarValue = 0;
            uiStatusBarPanel4.Text = "dd/MM/yy hh:mm:ss tt";
            uiStatusBarPanel4.Width = 123;
            this.statusBar.Panels.AddRange(new Janus.Windows.UI.StatusBar.UIStatusBarPanel[] {
            uiStatusBarPanel1,
            uiStatusBarPanel2,
            uiStatusBarPanel3,
            uiStatusBarPanel4});
            this.statusBar.PanelsBorderColor = System.Drawing.SystemColors.ControlDark;
            this.statusBar.Size = new System.Drawing.Size(820, 23);
            this.statusBar.TabIndex = 7;
            this.statusBar.VisualStyleManager = this.vsmMain;
            // 
            // cmdThoat3
            // 
            this.cmdThoat3.Key = "cmdThoat";
            this.cmdThoat3.Name = "cmdThoat3";
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // vlECS
            // 
            this.vlECS.ExpiresInDays = 30;
            this.vlECS.ExpiresOnDate = "5/5/2009 12:00:00 AM";
            this.vlECS.NumberOfTries = 30;
            this.vlECS.RegSubKey = "SOFTWARE\\ECS_SXXK";
            this.vlECS.TrialType = SoftechVersion.TRIAL_TYPE.ttEXPIRES_ON_DATE;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.InitialDirectory = "C:\\Program Files\\Microsoft Visual Studio 8\\Common7\\IDE";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // cmdThoat4
            // 
            this.cmdThoat4.Key = "cmdThoat";
            this.cmdThoat4.Name = "cmdThoat4";
            // 
            // cmdNhapXuat1
            // 
            this.cmdNhapXuat1.ImageIndex = 45;
            this.cmdNhapXuat1.Key = "cmdNhapXuat";
            this.cmdNhapXuat1.Name = "cmdNhapXuat1";
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.lblWebsite);
            this.panel1.Controls.Add(this.lblEmail);
            this.panel1.Controls.Add(this.labelControl7);
            this.panel1.Controls.Add(this.labelControl6);
            this.panel1.Controls.Add(this.labelControl5);
            this.panel1.Controls.Add(this.labelControl4);
            this.panel1.Controls.Add(this.labelControl3);
            this.panel1.Controls.Add(this.labelControl2);
            this.panel1.Location = new System.Drawing.Point(156, 319);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(455, 105);
            this.panel1.TabIndex = 1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(21, 46);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 56);
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // lblWebsite
            // 
            this.lblWebsite.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblWebsite.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblWebsite.Appearance.Options.UseFont = true;
            this.lblWebsite.Appearance.Options.UseForeColor = true;
            this.lblWebsite.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblWebsite.Location = new System.Drawing.Point(230, 88);
            this.lblWebsite.Margin = new System.Windows.Forms.Padding(1);
            this.lblWebsite.Name = "lblWebsite";
            this.lblWebsite.Size = new System.Drawing.Size(89, 13);
            this.lblWebsite.TabIndex = 7;
            this.lblWebsite.Text = "www.softech.vn";
            // 
            // lblEmail
            // 
            this.lblEmail.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))));
            this.lblEmail.Appearance.ForeColor = System.Drawing.Color.RoyalBlue;
            this.lblEmail.Appearance.Options.UseFont = true;
            this.lblEmail.Appearance.Options.UseForeColor = true;
            this.lblEmail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblEmail.Location = new System.Drawing.Point(230, 73);
            this.lblEmail.Margin = new System.Windows.Forms.Padding(1);
            this.lblEmail.Name = "lblEmail";
            this.lblEmail.Size = new System.Drawing.Size(98, 13);
            this.lblEmail.TabIndex = 6;
            this.lblEmail.Text = "sales@softech.vn";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(165, 73);
            this.labelControl7.Margin = new System.Windows.Forms.Padding(1);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(28, 13);
            this.labelControl7.TabIndex = 5;
            this.labelControl7.Text = "Email:";
            // 
            // labelControl6
            // 
            this.labelControl6.Location = new System.Drawing.Point(165, 88);
            this.labelControl6.Margin = new System.Windows.Forms.Padding(1);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(43, 13);
            this.labelControl6.TabIndex = 4;
            this.labelControl6.Text = "Website:";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(165, 58);
            this.labelControl5.Margin = new System.Windows.Forms.Padding(1);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(134, 13);
            this.labelControl5.TabIndex = 3;
            this.labelControl5.Text = "Fax:              0511.3810278";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(165, 43);
            this.labelControl4.Margin = new System.Windows.Forms.Padding(1);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(187, 13);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "Điện thoại:    0511.3810535 - 3840888";
            // 
            // labelControl3
            // 
            this.labelControl3.Location = new System.Drawing.Point(165, 28);
            this.labelControl3.Margin = new System.Windows.Forms.Padding(1);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(284, 13);
            this.labelControl3.TabIndex = 1;
            this.labelControl3.Text = "Địa chỉ:         15 Quang Trung, Quận Hải Châu, TP Đà Nẵng";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Location = new System.Drawing.Point(165, 13);
            this.labelControl2.Margin = new System.Windows.Forms.Padding(1);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(233, 13);
            this.labelControl2.TabIndex = 0;
            this.labelControl2.Text = "Bản quyền thuộc công ty Cổ phần Softech";
            // 
            // timerNgayThangNam
            // 
            this.timerNgayThangNam.Tick += new System.EventHandler(this.timerNgayThangNam_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(820, 482);
            this.Controls.Add(this.uiPanel0);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.IsMdiContainer = true;
            this.Name = "MainForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "THÔNG QUAN ĐIỆN TỬ - SẢN XUẤT XUẤT KHẨU 3.0";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.statusBar, 0);
            this.Controls.SetChildIndex(this.uiPanel0, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expSXXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).EndInit();
            this.uiPanel2.ResumeLayout(false);
            this.uiPanel2Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expKhaiBao_TheoDoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSXXK)).EndInit();
            this.pnlSXXK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pnlGiaCong)).EndInit();
            this.pnlGiaCong.ResumeLayout(false);
            this.pnlGiaCongContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expGiaCong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlKinhDoanh)).EndInit();
            this.pnlKinhDoanh.ResumeLayout(false);
            this.pnlKinhDoanhContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expKD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlDauTu)).EndInit();
            this.pnlDauTu.ResumeLayout(false);
            this.pnlDauTuContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.expDT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pnlSend)).EndInit();
            this.pnlSend.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UICommand cmdThoat3;
        private UIPanelInnerContainer pnlDauTuContainer;
        private ExplorerBar expDT;
        private UIPanel pnlSend;
        private UIPanelInnerContainer pnlSendContainer;
        private NotifyIcon notifyIcon1;
        private BackgroundWorker backgroundWorker1;
        private UICommand NhacNho1;
        private UICommand NhacNho;
        private UICommand DongBoDuLieu;
        private UICommand cmdImport;
        private UICommand cmdImportNPL1;
        private UICommand cmdImportSP1;
        private UICommand cmdImportNPL;
        private UICommand cmdImportSP;
        private UICommand cmdImportDM1;
        private UICommand cmdImportDM;
        private UICommand cmdImportToKhai1;
        private UICommand cmdImportTTDM;
        private UICommand cmdImportToKhai;
        private UICommand cmdImportHangHoa1;
        private UICommand cmdImportHangHoa;
        private UICommand Command01;
        private UICommand Command0;
        private UICommand cmdHelp1;
        private UICommand cmdAbout1;
        private UICommand cmdHelp;
        private UICommand cmdAbout;
        private UICommand Command11;
        private UICommand Command1;
        private UICommand cmd20071;
        private UICommand cmd2007;
        private UICommand cmd2003;
        private UICommand cmd20031;
        private UICommand cmdNPLNhapTon;
        private UICommand cmdDanhMuc1;
        private UICommand cmdDanhMuc;
        private UICommand cmdMaHS;
        private UICommand cmdHaiQuan1;
        private UICommand cmdNuoc1;
        private UICommand cmdMaHS1;
        private UICommand cmdNguyenTe1;
        private UICommand cmdDVT1;
        private UICommand cmdPTTT1;
        private UICommand cmdPTVT1;
        private UICommand cmdDKGH1;
        private UICommand cmdCuaKhau1;
        private UICommand cmdNuoc;
        private UICommand cmdHaiQuan;
        private UICommand cmdNguyenTe;
        private UICommand cmdDVT;
        private UICommand cmdPTTT;
        private UICommand cmdPTVT;
        private UICommand cmdDKGH;
        private UICommand cmdCuaKhau;
        private UICommand cmdBackUp;
        private UICommand cmdRestore;
        private UICommand ThongSoKetNoi;
        private UICommand TLThongTinDNHQ;
        private UICommand cmdThietLapIn;
        private UICommand cmdExportExccel;
        private UICommand cmdExportExcel1;
        private UICommand cmdImportExcel;
        private UICommand cmdDongBoPhongKhai;
        private UICommand cmdImportExcel1;
        private UICommand cmdExportExccel1;
        private UICommand cmdCauHinh1;
        private UICommand cmdCauHinh;
        private UICommand ThongSoKetNoi1;
        private UICommand TLThongTinDNHQ1;
        private UICommand cmdCauHinhToKhai1;
        private UICommand cmdThietLapIn1;
        private UICommand cmdCauHinhToKhai;
        public Janus.Windows.UI.StatusBar.UIStatusBar statusBar;
        private UICommand QuanTri1;
        private UICommand QuanTri;
        private UICommand QuanLyNguoiDung1;
        private UICommand QuanLyNhom1;
        private UICommand QuanLyNguoiDung;
        private UICommand QuanLyNhom;
        private UICommand LoginUser1;
        private UICommand LoginUser;
        private UICommand cmdChangePass1;
        private UICommand cmdChangePass;
        private UICommand cmdDoiMatKhauHQ1;
        private UICommand cmdDoiMatKhauHQ;
        private UICommand cmdCuaSo1;
        private UICommand cmdCuaSo;
        private UICommand cmdCloseAll1;
        private UICommand cmdCloseAllButThis1;
        private UICommand cmdCloseAll;
        private UICommand cmdCloseAllButThis;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel2;
        private UIPanelInnerContainer uiPanel2Container;
        private ExplorerBar expKhaiBao_TheoDoi;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private ExplorerBar expSXXK;
        private UICommand Separator1;
        private UICommand Separator3;
        private UICommand Separator4;
        private UICommand cmdBackUp1;
        private SoftechVersion.ValidVersion vlECS;
        private UICommand cmdAutoUpdate1;
        private UICommand cmdAutoUpdate;
        private UICommand cmdNhapXML;
        private UICommand cmdNPL1;
        private UICommand cmdSP1;
        private UICommand cmdDM1;
        private UICommand cmdTK1;
        private UICommand cmdNPL;
        private UICommand cmdSP;
        private UICommand cmdDM;
        private UICommand cmdTK;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private UICommand cmdXuatDuLieu;
        private UICommand cmdXuatNPL;
        private UICommand cmdXuatDinhMuc;
        private UICommand cmdXuatSanPham;
        private UICommand cmdXuatToKhai;
        private UICommand cmdVN;
        private UICommand cmdEL;
        private UICommand cmdVN1;
        private UICommand cmdEL1;
        private UICommand cmdHUNGNPL;
        private UICommand cmdHUNGNPL1;
        private UICommand cmdHUNGSP1;
        private UICommand cmdHUNGDM1;
        private UICommand cmdHUNGSP;
        private UICommand cmdHUNGDM;
        private UICommand cmdHUNGTK;
        private UICommand cmdHUNGTK1;
        private UICommand Separator5;
        private UICommand cmdActivate1;
        private UICommand cmdActivate;
        private System.Windows.Forms.Timer timer1;
        private UICommand cmdXuatDuLieuDN;
        private UICommand cmdXuatNPLDN;
        private UICommand cmdXuatSPDN;
        private UICommand cmXuat;
        private UICommand cmdXuatNPLDN1;
        private UICommand cmdXuatSPDN1;
        private UICommand cmdXuatDMDN1;
        private UICommand cmdXuatTKDN1;
        private UICommand cmdXuatDMDN;
        private UICommand cmdXuatTKDN;
        private UICommand cmdNPLNhapTon1;
        private UICommand cmdNhapDuLieu;
        private UICommand cmdNhapNPL1;
        private UICommand cmdNhapSanPham1;
        private UICommand cmdNhapDinhMuc1;
        private UICommand cmdNhapToKhai1;
        private UICommand cmdNhapNPL;
        private UICommand cmdNhapSanPham;
        private UICommand cmdNhapDinhMuc;
        private UICommand cmdNhapToKhai;
        private UICommand QuanlyMess;
        private UICommand cmdConfig;
        private UICommand cmdConfig1;
        private UICommand cmdThoat4;
        private UICommand mnuQuerySQL1;
        private UICommand mnuQuerySQL;
        private UICommand TraCuuMaHS;
        private UICommand cmdNhomCuaKhau1;
        private UICommand cmdNhomCuaKhau;
        private UICommand cmdLog1;
        private UICommand cmdLog;
        private UICommand cmdLoaiPhiChungTuThanhToan1;
        private UICommand cmdLoaiPhiChungTuThanhToan;
        private UICommand cmdDataVersion;
        private UICommand cmdDataVersion1;
        private UICommand cmdImportXml;
        private UICommand cmdChuKySo;
        private UICommand cmdChuKySo1;
        private UICommand cmdTimer1;
        private UICommand cmdTimer;
        private UICommand mnuFormSQL;
        private UICommand mnuMiniSQL;
        private UICommand mnuFormSQL1;
        private UICommand mnuMiniSQL1;
        private UICommand cmdNhapXuat;
        private UICommand cmdNhapXuat1;
        private UICommand cmdBieuThue;
        private UICommand cmdTraCuuXNKOnline;
        private UICommand cmdTraCuuNoThueOnline;
        private UICommand cmdTraCuuVanBanOnline;
        private UICommand cmdTuVanHQOnline;
        private UICommand cmdBieuThue1;
        private UICommand cmdGetCategoryOnline1;
        private UICommand Separator6;
        private UICommand TraCuuMaHS1;
        private UICommand Separator7;
        private UICommand cmdTraCuuXNKOnline1;
        private UICommand cmdTraCuuNoThueOnline1;
        private UICommand cmdTraCuuVanBanOnline1;
        private UICommand cmdTuVanHQOnline1;
        private UICommand cmdGetCategoryOnline;
        private UICommand cmdNhapXuat2;
        private UICommand cmdImportExcel2;
        private UICommand cmdImportXml1;
        private UICommand cmdNhapXML1;
        private UICommand cmdNhapDuLieu1;
        private UICommand Separator8;
        private UICommand cmdXuatDuLieu1;
        private UICommand cmdXuatDuLieuDN1;
        private UICommand cmdTeamview1;
        private UICommand Separator2;
        private UICommand cmdTeamview;
        private UICommand cmdCapNhatHS;
        private UICommand cmdCapNhatHS1;
        private UICommand Separator9;
        private UICommand Separator10;
        private UICommand cmdCapNhatHS8SoAuto1;
        private UICommand cmdCapNhatHS8SoManual1;
        private UICommand cmdCapNhatHS8SoAuto;
        private UICommand cmdCapNhatHS8SoManual;
        private UICommand Separator11;
        private UICommand cmdTool1;
        private UICommand cmdTool;
        private UICommand cmdImageResizeHelp1;
        private UICommand cmdImageResizeHelp;
        private UICommand cmbDongBoDuLieu;
        private UICommand cmbDongBoDuLieu1;
        private UICommand DongBoDuLieu1;
        private UICommand cmdQuanLyDL1;
        private UICommand cmdQuanLyDL;
        private UICommand cmdDaily1;
        private UICommand Separator12;
        private UICommand cmdDaily;
        private Panel panel1;
        private PictureBox pictureBox1;
        private DevExpress.XtraEditors.LabelControl lblWebsite;
        private DevExpress.XtraEditors.LabelControl lblEmail;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.Timer timerNgayThangNam;
    }
}
