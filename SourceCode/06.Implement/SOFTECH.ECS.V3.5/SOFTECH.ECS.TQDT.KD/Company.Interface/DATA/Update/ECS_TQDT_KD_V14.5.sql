 IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_KDT_VNACC_TKVC_TrungChuyen' AND COLUMN_NAME = 'NgayDenThucTe')
  Begin
  ALTER TABLE t_KDT_VNACC_TKVC_TrungChuyen
   ADD NgayDenThucTe DateTime  NULL
  END

GO
 IF NOT EXISTS 
(SELECT * FROM INFORMATION_SCHEMA.COLUMNS  WHERE TABLE_NAME = 't_KDT_VNACC_TKVC_TrungChuyen' AND COLUMN_NAME = 'NgayDiThucTe')
  Begin
  ALTER TABLE t_KDT_VNACC_TKVC_TrungChuyen
   ADD NgayDiThucTe DateTime  NULL
  END

GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]
	@Master_ID bigint,
	@MaDiaDiemTrungChuyen varchar(7),
	@TenDiaDiemTrungChuyen varchar(20),
	@NgayDenDiaDiem_TC datetime,
	@NgayDenThucTe datetime,
	@NgayDiDiaDiem_TC datetime,
	@NgayDiThucTe datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
(
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDenThucTe],
	[NgayDiDiaDiem_TC],
	[NgayDiThucTe]
)
VALUES 
(
	@Master_ID,
	@MaDiaDiemTrungChuyen,
	@TenDiaDiemTrungChuyen,
	@NgayDenDiaDiem_TC,
	@NgayDenThucTe,
	@NgayDiDiaDiem_TC,
	@NgayDiThucTe
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaDiaDiemTrungChuyen varchar(7),
	@TenDiaDiemTrungChuyen varchar(20),
	@NgayDenDiaDiem_TC datetime,
	@NgayDenThucTe datetime,
	@NgayDiDiaDiem_TC datetime,
	@NgayDiThucTe datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
SET
	[Master_ID] = @Master_ID,
	[MaDiaDiemTrungChuyen] = @MaDiaDiemTrungChuyen,
	[TenDiaDiemTrungChuyen] = @TenDiaDiemTrungChuyen,
	[NgayDenDiaDiem_TC] = @NgayDenDiaDiem_TC,
	[NgayDenThucTe] = @NgayDenThucTe,
	[NgayDiDiaDiem_TC] = @NgayDiDiaDiem_TC,
	[NgayDiThucTe] = @NgayDiThucTe
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaDiaDiemTrungChuyen varchar(7),
	@TenDiaDiemTrungChuyen varchar(20),
	@NgayDenDiaDiem_TC datetime,
	@NgayDenThucTe datetime,
	@NgayDiDiaDiem_TC datetime,
	@NgayDiThucTe datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TKVC_TrungChuyen] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TKVC_TrungChuyen] 
		SET
			[Master_ID] = @Master_ID,
			[MaDiaDiemTrungChuyen] = @MaDiaDiemTrungChuyen,
			[TenDiaDiemTrungChuyen] = @TenDiaDiemTrungChuyen,
			[NgayDenDiaDiem_TC] = @NgayDenDiaDiem_TC,
			[NgayDenThucTe] = @NgayDenThucTe,
			[NgayDiDiaDiem_TC] = @NgayDiDiaDiem_TC,
			[NgayDiThucTe] = @NgayDiThucTe
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
		(
			[Master_ID],
			[MaDiaDiemTrungChuyen],
			[TenDiaDiemTrungChuyen],
			[NgayDenDiaDiem_TC],
			[NgayDenThucTe],
			[NgayDiDiaDiem_TC],
			[NgayDiThucTe]
		)
		VALUES 
		(
			@Master_ID,
			@MaDiaDiemTrungChuyen,
			@TenDiaDiemTrungChuyen,
			@NgayDenDiaDiem_TC,
			@NgayDenThucTe,
			@NgayDiDiaDiem_TC,
			@NgayDiThucTe
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TKVC_TrungChuyen] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDenThucTe],
	[NgayDiDiaDiem_TC],
	[NgayDiThucTe]
FROM
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDenThucTe],
	[NgayDiDiaDiem_TC],
	[NgayDiThucTe]
FROM [dbo].[t_KDT_VNACC_TKVC_TrungChuyen] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDenThucTe],
	[NgayDiDiaDiem_TC],
	[NgayDiThucTe]
FROM
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]	

GO

           --Cập nhật version
     IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '14.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('14.5', GETDATE(), N'Cap nhat bang trung chuyen cua to khai van chuyen')
END	