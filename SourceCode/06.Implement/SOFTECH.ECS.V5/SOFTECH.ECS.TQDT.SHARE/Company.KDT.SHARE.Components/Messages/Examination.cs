﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages
{
    public class Examination
    {
        /// <summary>
        /// Địa điểm 
        /// </summary>
        [XmlElement("place")]
        public string Place { get; set; }

        /// <summary>
        /// Co quan giam dinh
        /// </summary>
        [XmlElement("Examiner")]
        public NameBase Examiner { get; set; }

        /// <summary>
        /// Can bo giam dinh
        /// </summary>
        [XmlElement("ExaminePerson")]
        public NameBase ExaminePerson { get; set; }

        /// <summary>
        /// Ket qua giam dinh
        /// </summary>
        [XmlElement("Result")]
        public Result Result { get; set; }
    }
}
