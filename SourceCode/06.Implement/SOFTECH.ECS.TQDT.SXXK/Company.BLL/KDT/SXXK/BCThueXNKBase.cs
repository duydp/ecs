using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
    public partial class BCThueXNK
    {
        #region Private members.

        protected long _ID;
        protected long _STT;
        protected int _LanThanhLy;
        protected int _NamThanhLy;
        protected string _MaDoanhNghiep = String.Empty;
        protected int _SoToKhaiNhap;
        protected DateTime _NgayDangKyNhap = new DateTime(1900, 01, 01);
        protected DateTime _NgayThucNhap = new DateTime(1900, 01, 01);
        protected string _MaLoaiHinhNhap = String.Empty;
        protected string _MaNPL = String.Empty;
        protected decimal _LuongNhap;
        protected string _TenDVT_NPL = String.Empty;
        protected decimal _DonGiaTT;
        protected decimal _TyGiaTT;
        protected decimal _ThueSuat;
        protected decimal _ThueNKNop;
        protected int _SoToKhaiXuat;
        protected DateTime _NgayDangKyXuat = new DateTime(1900, 01, 01);
        protected DateTime _NgayThucXuat = new DateTime(1900, 01, 01);
        protected string _MaLoaiHinhXuat = String.Empty;
        protected decimal _LuongNPLSuDung;
        protected decimal _LuongNPLTon;
        protected decimal _TienThueHoan;
        protected decimal _TienThueTKTiep;
        protected string _GhiChu = String.Empty;
        protected string _TenNPL = String.Empty;
        protected decimal _Luong;
        protected decimal _ThueXNK;
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public long STT
        {
            set { this._STT = value; }
            get { return this._STT; }
        }
        public int LanThanhLy
        {
            set { this._LanThanhLy = value; }
            get { return this._LanThanhLy; }
        }
        public int NamThanhLy
        {
            set { this._NamThanhLy = value; }
            get { return this._NamThanhLy; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }
        public int SoToKhaiNhap
        {
            set { this._SoToKhaiNhap = value; }
            get { return this._SoToKhaiNhap; }
        }
        public DateTime NgayDangKyNhap
        {
            set { this._NgayDangKyNhap = value; }
            get { return this._NgayDangKyNhap; }
        }
        public DateTime NgayThucNhap
        {
            set { this._NgayThucNhap = value; }
            get { return this._NgayThucNhap; }
        }
        public string MaLoaiHinhNhap
        {
            set { this._MaLoaiHinhNhap = value; }
            get { return this._MaLoaiHinhNhap; }
        }
        public string MaNPL
        {
            set { this._MaNPL = value; }
            get { return this._MaNPL; }
        }
        public decimal LuongNhap
        {
            set { this._LuongNhap = value; }
            get { return this._LuongNhap; }
        }
        public string TenDVT_NPL
        {
            set { this._TenDVT_NPL = value; }
            get { return this._TenDVT_NPL; }
        }
        public decimal DonGiaTT
        {
            set { this._DonGiaTT = value; }
            get { return this._DonGiaTT; }
        }
        public decimal TyGiaTT
        {
            set { this._TyGiaTT = value; }
            get { return this._TyGiaTT; }
        }
        public decimal ThueSuat
        {
            set { this._ThueSuat = value; }
            get { return this._ThueSuat; }
        }
        public decimal ThueNKNop
        {
            set { this._ThueNKNop = value; }
            get { return this._ThueNKNop; }
        }
        public int SoToKhaiXuat
        {
            set { this._SoToKhaiXuat = value; }
            get { return this._SoToKhaiXuat; }
        }
        public DateTime NgayDangKyXuat
        {
            set { this._NgayDangKyXuat = value; }
            get { return this._NgayDangKyXuat; }
        }
        public DateTime NgayThucXuat
        {
            set { this._NgayThucXuat = value; }
            get { return this._NgayThucXuat; }
        }
        public string MaLoaiHinhXuat
        {
            set { this._MaLoaiHinhXuat = value; }
            get { return this._MaLoaiHinhXuat; }
        }
        public decimal LuongNPLSuDung
        {
            set { this._LuongNPLSuDung = value; }
            get { return this._LuongNPLSuDung; }
        }
        public decimal LuongNPLTon
        {
            set { this._LuongNPLTon = value; }
            get { return this._LuongNPLTon; }
        }
        public decimal TienThueHoan
        {
            set { this._TienThueHoan = value; }
            get { return this._TienThueHoan; }
        }
        public decimal TienThueTKTiep
        {
            set { this._TienThueTKTiep = value; }
            get { return this._TienThueTKTiep; }
        }
        public string GhiChu
        {
            set { this._GhiChu = value; }
            get { return this._GhiChu; }
        }
        public string TenNPL
        {
            set { this._TenNPL = value; }
            get { return this._TenNPL; }
        }
        public decimal Luong
        {
            set { this._Luong = value; }
            get { return this._Luong; }
        }
        public decimal ThueXNK
        {
            set { this._ThueXNK = value; }
            get { return this._ThueXNK; }
        }
        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_KDT_SXXK_BCThueXNK_Load";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) this._STT = reader.GetInt64(reader.GetOrdinal("STT"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamThanhLy"))) this._NamThanhLy = reader.GetInt32(reader.GetOrdinal("NamThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) this._SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) this._NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucNhap"))) this._NgayThucNhap = reader.GetDateTime(reader.GetOrdinal("NgayThucNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) this._MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) this._LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) this._TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) this._DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTT"))) this._TyGiaTT = reader.GetDecimal(reader.GetOrdinal("TyGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuat"))) this._ThueSuat = reader.GetDecimal(reader.GetOrdinal("ThueSuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueNKNop"))) this._ThueNKNop = reader.GetDecimal(reader.GetOrdinal("ThueNKNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) this._SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) this._NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucXuat"))) this._NgayThucXuat = reader.GetDateTime(reader.GetOrdinal("NgayThucXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) this._MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) this._LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTon"))) this._LuongNPLTon = reader.GetDecimal(reader.GetOrdinal("LuongNPLTon"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThueHoan"))) this._TienThueHoan = reader.GetDecimal(reader.GetOrdinal("TienThueHoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThueTKTiep"))) this._TienThueTKTiep = reader.GetDecimal(reader.GetOrdinal("TienThueTKTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) this._TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) this._Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) this._ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));

                return true;
            }
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_KDT_SXXK_BCThueXNK_SelectAll";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_SXXK_BCThueXNK_SelectAll";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_BCThueXNK_SelectDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_SXXK_BCThueXNK_SelectDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }
        public IDataReader SelectReaderDynamicTrans(string whereCondition, string orderByExpression, SqlTransaction trans)
        {
            string spName = "p_KDT_SXXK_BCThueXNK_SelectDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand,trans);
        }
        //---------------------------------------------------------------------------------------------

        public BCThueXNKCollection SelectCollectionAll()
        {
            BCThueXNKCollection collection = new BCThueXNKCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                BCThueXNK entity = new BCThueXNK();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt64(reader.GetOrdinal("STT"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamThanhLy"))) entity.NamThanhLy = reader.GetInt32(reader.GetOrdinal("NamThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucNhap"))) entity.NgayThucNhap = reader.GetDateTime(reader.GetOrdinal("NgayThucNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) entity.LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) entity.TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTT"))) entity.TyGiaTT = reader.GetDecimal(reader.GetOrdinal("TyGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuat"))) entity.ThueSuat = reader.GetDecimal(reader.GetOrdinal("ThueSuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueNKNop"))) entity.ThueNKNop = reader.GetDecimal(reader.GetOrdinal("ThueNKNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucXuat"))) entity.NgayThucXuat = reader.GetDateTime(reader.GetOrdinal("NgayThucXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) entity.LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTon"))) entity.LuongNPLTon = reader.GetDecimal(reader.GetOrdinal("LuongNPLTon"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThueHoan"))) entity.TienThueHoan = reader.GetDecimal(reader.GetOrdinal("TienThueHoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThueTKTiep"))) entity.TienThueTKTiep = reader.GetDecimal(reader.GetOrdinal("TienThueTKTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public BCThueXNKCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            BCThueXNKCollection collection = new BCThueXNKCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                BCThueXNK entity = new BCThueXNK();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt64(reader.GetOrdinal("STT"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamThanhLy"))) entity.NamThanhLy = reader.GetInt32(reader.GetOrdinal("NamThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucNhap"))) entity.NgayThucNhap = reader.GetDateTime(reader.GetOrdinal("NgayThucNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) entity.LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) entity.TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTT"))) entity.TyGiaTT = reader.GetDecimal(reader.GetOrdinal("TyGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuat"))) entity.ThueSuat = reader.GetDecimal(reader.GetOrdinal("ThueSuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueNKNop"))) entity.ThueNKNop = reader.GetDecimal(reader.GetOrdinal("ThueNKNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucXuat"))) entity.NgayThucXuat = reader.GetDateTime(reader.GetOrdinal("NgayThucXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) entity.LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTon"))) entity.LuongNPLTon = reader.GetDecimal(reader.GetOrdinal("LuongNPLTon"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThueHoan"))) entity.TienThueHoan = reader.GetDecimal(reader.GetOrdinal("TienThueHoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThueTKTiep"))) entity.TienThueTKTiep = reader.GetDecimal(reader.GetOrdinal("TienThueTKTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public BCThueXNKCollection SelectCollectionDynamicTransaction(string whereCondition, string orderByExpression, SqlTransaction trans)
        {
            BCThueXNKCollection collection = new BCThueXNKCollection();

            IDataReader reader = this.SelectReaderDynamicTrans(whereCondition, orderByExpression , trans);
            while (reader.Read())
            {
                BCThueXNK entity = new BCThueXNK();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt64(reader.GetOrdinal("STT"));
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamThanhLy"))) entity.NamThanhLy = reader.GetInt32(reader.GetOrdinal("NamThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucNhap"))) entity.NgayThucNhap = reader.GetDateTime(reader.GetOrdinal("NgayThucNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) entity.LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) entity.TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGiaTT"))) entity.DonGiaTT = reader.GetDecimal(reader.GetOrdinal("DonGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyGiaTT"))) entity.TyGiaTT = reader.GetDecimal(reader.GetOrdinal("TyGiaTT"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueSuat"))) entity.ThueSuat = reader.GetDecimal(reader.GetOrdinal("ThueSuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueNKNop"))) entity.ThueNKNop = reader.GetDecimal(reader.GetOrdinal("ThueNKNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayThucXuat"))) entity.NgayThucXuat = reader.GetDateTime(reader.GetOrdinal("NgayThucXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) entity.LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTon"))) entity.LuongNPLTon = reader.GetDecimal(reader.GetOrdinal("LuongNPLTon"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThueHoan"))) entity.TienThueHoan = reader.GetDecimal(reader.GetOrdinal("TienThueHoan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TienThueTKTiep"))) entity.TienThueTKTiep = reader.GetDecimal(reader.GetOrdinal("TienThueTKTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("Luong"))) entity.Luong = reader.GetDecimal(reader.GetOrdinal("Luong"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThueXNK"))) entity.ThueXNK = reader.GetDecimal(reader.GetOrdinal("ThueXNK"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_BCThueXNK_Insert";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, this._STT);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, this._NamThanhLy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, this._SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, this._NgayDangKyNhap);
            db.AddInParameter(dbCommand, "@NgayThucNhap", SqlDbType.DateTime, this._NgayThucNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, this._MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, this._LuongNhap);
            db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, this._TenDVT_NPL);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, this._DonGiaTT);
            db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Decimal, this._TyGiaTT);
            db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, this._ThueSuat);
            db.AddInParameter(dbCommand, "@ThueNKNop", SqlDbType.Decimal, this._ThueNKNop);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
            db.AddInParameter(dbCommand, "@NgayThucXuat", SqlDbType.DateTime, this._NgayThucXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, this._LuongNPLSuDung);
            db.AddInParameter(dbCommand, "@LuongNPLTon", SqlDbType.Decimal, this._LuongNPLTon);
            db.AddInParameter(dbCommand, "@TienThueHoan", SqlDbType.Decimal, this._TienThueHoan);
            db.AddInParameter(dbCommand, "@TienThueTKTiep", SqlDbType.Decimal, this._TienThueTKTiep);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Decimal, this._ThueXNK);
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                this._ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(BCThueXNKCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BCThueXNK item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, BCThueXNKCollection collection)
        {
            foreach (BCThueXNK item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_BCThueXNK_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, this._STT);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, this._NamThanhLy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, this._SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, this._NgayDangKyNhap);
            db.AddInParameter(dbCommand, "@NgayThucNhap", SqlDbType.DateTime, this._NgayThucNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, this._MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, this._LuongNhap);
            db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, this._TenDVT_NPL);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, this._DonGiaTT);
            db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Decimal, this._TyGiaTT);
            db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, this._ThueSuat);
            db.AddInParameter(dbCommand, "@ThueNKNop", SqlDbType.Decimal, this._ThueNKNop);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
            db.AddInParameter(dbCommand, "@NgayThucXuat", SqlDbType.DateTime, this._NgayThucXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, this._LuongNPLSuDung);
            db.AddInParameter(dbCommand, "@LuongNPLTon", SqlDbType.Decimal, this._LuongNPLTon);
            db.AddInParameter(dbCommand, "@TienThueHoan", SqlDbType.Decimal, this._TienThueHoan);
            db.AddInParameter(dbCommand, "@TienThueTKTiep", SqlDbType.Decimal, this._TienThueTKTiep);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Decimal, this._ThueXNK);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(BCThueXNKCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BCThueXNK item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_BCThueXNK_Update";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            db.AddInParameter(dbCommand, "@STT", SqlDbType.BigInt, this._STT);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, this._NamThanhLy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, this._SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, this._NgayDangKyNhap);
            db.AddInParameter(dbCommand, "@NgayThucNhap", SqlDbType.DateTime, this._NgayThucNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, this._MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, this._LuongNhap);
            db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, this._TenDVT_NPL);
            db.AddInParameter(dbCommand, "@DonGiaTT", SqlDbType.Decimal, this._DonGiaTT);
            db.AddInParameter(dbCommand, "@TyGiaTT", SqlDbType.Decimal, this._TyGiaTT);
            db.AddInParameter(dbCommand, "@ThueSuat", SqlDbType.Decimal, this._ThueSuat);
            db.AddInParameter(dbCommand, "@ThueNKNop", SqlDbType.Decimal, this._ThueNKNop);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
            db.AddInParameter(dbCommand, "@NgayThucXuat", SqlDbType.DateTime, this._NgayThucXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, this._LuongNPLSuDung);
            db.AddInParameter(dbCommand, "@LuongNPLTon", SqlDbType.Decimal, this._LuongNPLTon);
            db.AddInParameter(dbCommand, "@TienThueHoan", SqlDbType.Decimal, this._TienThueHoan);
            db.AddInParameter(dbCommand, "@TienThueTKTiep", SqlDbType.Decimal, this._TienThueTKTiep);
            db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            db.AddInParameter(dbCommand, "@Luong", SqlDbType.Decimal, this._Luong);
            db.AddInParameter(dbCommand, "@ThueXNK", SqlDbType.Decimal, this._ThueXNK);
            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(BCThueXNKCollection collection, SqlTransaction transaction)
        {
            foreach (BCThueXNK item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_BCThueXNK_Delete";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteAllTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_SXXK_BCThueXNK_DeleteAll";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public int DeleteDynamicTransaction(SqlTransaction transaction, int lanThanLy, string MaDoanhNghiep)
        {
            string spName = "DELETE FROM t_KDT_SXXK_BCThueXNK WHERE LanThanhLy = " + lanThanLy + " AND MaDoanhNghiep ='" + MaDoanhNghiep + "'"; 
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(BCThueXNKCollection collection, SqlTransaction transaction)
        {
            foreach (BCThueXNK item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(BCThueXNKCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BCThueXNK item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion
    }
}