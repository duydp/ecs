﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Company.Interface.GC;
using Company.Interface.SXXK;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.QuanLyChungTu.CX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT
{
    public partial class BaoCaoQuyetToan_HangHoaForm : Company.Interface.BaseForm
    {
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_New();
        public KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
      
        public bool isAdd = true;
        public bool isExits = false;
        public string LoaiHangHoa = "";
        public NguyenPhuLieuRegistedForm NPLRegistedForm;
        public SanPhamRegistedForm SPRegistedForm;
        public NguyenPhuLieuRegistedForm_CX NPLRegistedForm_CX;

        List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu> NPLCollection = new List<Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu>();
        List<Company.BLL.KDT.SXXK.SXXK_SanPham> SPCollection = new List<Company.BLL.KDT.SXXK.SXXK_SanPham>();
        IList<HangDuaVao> HangDuaVaoCollection;

        public BaoCaoQuyetToan_HangHoaForm()
        {
            InitializeComponent();
        }
        private void SetCommandStatus()
        {
            if (this.OpenType==OpenFormType.View)
            {
                btnAdd.Enabled = false;
                btnImportExcel.Enabled = false;
                btnDelete.Enabled = false;
            }
            else
            {
                btnAdd.Enabled = true;
                btnImportExcel.Enabled = true;
                btnDelete.Enabled = true;
            }
        }
        private void BindDataHangHoa()
        {
            try
            {
                dgListTotal.Refresh();
                dgListTotal.DataSource = goodItem.GoodItemsCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BaoCaoQuyetToan_HangHoaForm_Load(object sender, EventArgs e)
        {
            SetCommandStatus();
            BindDataHangHoa();
            try
            {
                switch (LoaiHangHoa)
                {
                    case "NPL":
                        if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                        {
                            HangDuaVaoCollection = HangDuaVao.SelectCollectionAll();
                            cbbMaHangHoa.DataSource = HangDuaVaoCollection;
                            cbbMaHangHoa.DisplayMember = "Ma";
                            cbbMaHangHoa.ValueMember = "Ma";
                        }
                        else
                        {
                            NPLCollection = Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                            cbbMaHangHoa.DataSource = NPLCollection;
                            cbbMaHangHoa.DisplayMember = "Ma";
                            cbbMaHangHoa.ValueMember = "Ma";
                        }
                        break;
                    case "SP":
                        SPCollection = Company.BLL.KDT.SXXK.SXXK_SanPham.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
                        cbbMaHangHoa.DataSource = SPCollection;
                        cbbMaHangHoa.DisplayMember = "Ma";
                        cbbMaHangHoa.ValueMember = "Ma";
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbbMaHangHoa, errorProvider, " MÃ HÀNG HÓA ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenHangHoa, errorProvider, " TÊN HÀNG HÓA ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrDVTLuong1, errorProvider, " ĐƠN VỊ TÍNH ");

                //isValid &= ValidateControl.ValidateNull(txtTonDauKy, errorProvider, " Tồn đầu kỳ ");
                //isValid &= ValidateControl.ValidateNull(txtNhapTrongKy, errorProvider, " Nhập trong kỳ ", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtTaiXuat, errorProvider, " Tái xuất ");
                //isValid &= ValidateControl.ValidateNull(txtChuyenMDSD, errorProvider, " Chuyển mục đích sử dụng ");
                //isValid &= ValidateControl.ValidateNull(txtXuatKhac, errorProvider, " Xuất khác ");
                //isValid &= ValidateControl.ValidateNull(txtXuatTrongKy, errorProvider, " Xuất trong kỳ ");
                //isValid &= ValidateControl.ValidateNull(txtTonCuoiKy, errorProvider, " Tồn cuối kỳ ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void GetHangHoa()
        {
            try
            {
                if (goodItemDetail == null)
                    goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
                goodItemDetail.MaHangHoa = cbbMaHangHoa.Value.ToString();
                goodItemDetail.TenHangHoa = txtTenHangHoa.Text.ToString();
                goodItemDetail.DVT = ctrDVTLuong1.Code.ToString();
                goodItemDetail.TonDauKy = Convert.ToDecimal(txtTonDauKy.Value.ToString());
                goodItemDetail.NhapTrongKy = Convert.ToDecimal(txtNhapTrongKy.Value.ToString());
                goodItemDetail.TaiXuat = Convert.ToDecimal(txtTaiXuat.Value.ToString());
                goodItemDetail.ChuyenMDSD = Convert.ToDecimal(txtChuyenMDSD.Value.ToString());
                goodItemDetail.XuatKhac = Convert.ToDecimal(txtXuatKhac.Value.ToString());
                goodItemDetail.XuatTrongKy = Convert.ToDecimal(txtXuatTrongKy.Value.ToString());
                goodItemDetail.TonCuoiKy = Convert.ToDecimal(txtTonCuoiKy.Value.ToString());
                goodItemDetail.GhiChu = txtGhiChu.Text.ToString();
                if (isAdd)
                {
                    goodItem.GoodItemsCollection.Add(goodItemDetail);
                    int k = 1;
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in goodItem.GoodItemsCollection)
                    {
                        item.STT = k;
                        k++;
                    }
                }
                goodItemDetail = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New();
                isAdd = true;
                cbbMaHangHoa.Text = String.Empty;
                txtTenHangHoa.Text = String.Empty;
                ctrDVTLuong1.Code = String.Empty;
                txtTonDauKy.Value = String.Empty;
                txtNhapTrongKy.Value = String.Empty;
                txtTaiXuat.Value = String.Empty;
                txtChuyenMDSD.Value = String.Empty;
                txtXuatKhac.Value = String.Empty;
                txtXuatTrongKy.Value = String.Empty;
                txtTonCuoiKy.Value = String.Empty;
                txtGhiChu.Text = String.Empty;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetHangHoa()
        {
            try
            {
                cbbMaHangHoa.Value = goodItemDetail.MaHangHoa;
                txtTenHangHoa.Text = goodItemDetail.TenHangHoa;
                ctrDVTLuong1.Code = goodItemDetail.DVT;
                txtTonDauKy.Value = goodItemDetail.TonDauKy;
                txtNhapTrongKy.Value = goodItemDetail.NhapTrongKy;
                txtTaiXuat.Value = goodItemDetail.TaiXuat;
                txtChuyenMDSD.Value = goodItemDetail.ChuyenMDSD;
                txtXuatKhac.Value = goodItemDetail.XuatKhac;
                txtXuatTrongKy.Value = goodItemDetail.XuatTrongKy;
                txtTonCuoiKy.Value = goodItemDetail.TonCuoiKy;
                txtGhiChu.Text = goodItemDetail.GhiChu;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetHangHoa();
                BindDataHangHoa();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListTotal.SelectedItems;
                List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New> ItemColl = new List<KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New>();
                if (dgListTotal.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("DOANH NGHIỆP MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        goodItem.GoodItemsCollection.Remove(item);
                    }
                    int k = 1;
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New item in goodItem.GoodItemsCollection)
                    {
                        item.STT = k;
                        k++;
                    }
                    BindDataHangHoa();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgListTotal_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgListTotal.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            goodItemDetail = (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New)i.GetRow().DataRow;
                            SetHangHoa();
                            isAdd = false;
                        }
                    }   
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtTonDauKy_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtNhapTrongKy_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtTaiXuat_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtChuyenMDSD_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtXuatTrongKy_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtXuatKhac_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaHangHoa_ButtonClick(object sender, EventArgs e)
        {
            //if (this.LoaiHangHoa == "T" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            //{

            //    if (this.NPLRegistedForm_CX == null)
            //        this.NPLRegistedForm_CX = new NguyenPhuLieuRegistedForm_CX();
            //    this.NPLRegistedForm_CX.CalledForm = "HangMauDichForm";
            //    this.NPLRegistedForm_CX.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    this.NPLRegistedForm_CX.LoaiNPL = "3";
            //    this.NPLRegistedForm_CX.ShowDialog(this);
            //    if (!string.IsNullOrEmpty(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma) && this.NPLRegistedForm_CX.NguyenPhuLieuSelected != null)
            //    {
            //        txtMaHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ma;
            //        txtTenHangHoa.Text = this.NPLRegistedForm_CX.NguyenPhuLieuSelected.Ten;
            //        ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm_CX.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

            //    }

            //}
            //else if (this.LoaiHangHoa == "NPL")
            //{
            //    if (this.NPLRegistedForm == null)
            //        this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
            //    this.NPLRegistedForm.CalledForm = "HangMauDichForm";
            //    this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    this.NPLRegistedForm.isBrowser = true;
            //    this.NPLRegistedForm.ShowDialog(this);
            //    if (this.NPLRegistedForm.NguyenPhuLieuSelected.Ma != "" && this.NPLRegistedForm.NguyenPhuLieuSelected != null)
            //    {
            //        txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
            //        txtTenHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
            //        ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

            //    }
            //}
            //else
            //{
            //    this.SPRegistedForm = new SanPhamRegistedForm();
            //    this.SPRegistedForm.CalledForm = "HangMauDichForm";
            //    this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    this.SPRegistedForm.ShowDialog();
            //    if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
            //    {
            //        txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
            //        txtTenHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ten;
            //        ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
            //    }
            //}
        }

        private void txtMaHangHoa_Leave(object sender, EventArgs e)
        {
            //if (LoaiHangHoa == "T" && Company.KDT.SHARE.Components.Globals.LaDNCX)
            //{
            //    IList<HangDuaVao> listTB = HangDuaVao.SelectCollectionDynamic(string.Format("Ma = '{0}' and LoaiNPL = '3'", txtMaHangHoa.Text.Trim()), null);
            //    if (listTB != null && listTB.Count > 0)
            //    {
            //        HangDuaVao tb = listTB[0];
            //        txtTenHangHoa.Text = tb.Ten;
            //        ctrDVTLuong1.Code = this.DVT_VNACC(tb.DVT_ID.PadRight(3));
            //    }
            //    else
            //    {
            //    }
            //}
            //else if (LoaiHangHoa == "NPL")
            //{
            //    Company.BLL.SXXK.NguyenPhuLieu npl = new Company.BLL.SXXK.NguyenPhuLieu();
            //    npl.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    npl.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //    npl.Ma = txtMaHangHoa.Text;
            //    if (npl.Load())
            //    {
            //        txtMaHangHoa.Text = npl.Ma;
            //        txtTenHangHoa.Text = npl.Ten;
            //        ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));

            //    }


            //}
            //else if (LoaiHangHoa == "SP")
            //{
            //    Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
            //    sp.Ma = txtMaHangHoa.Text;
            //    sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //    sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            //    if (sp.Load())
            //    {
            //        txtMaHangHoa.Text = sp.Ma;
            //        txtTenHangHoa.Text = sp.Ten;
            //        ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
            //    }
            //}            
        }

        private void dgListTotal_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType==RowType.Record)
            {
                goodItemDetail = (KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Details_New)e.Row.DataRow;
                SetHangHoa();
                isAdd = false; 
            }
        }

        private void dgListTotal_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            ReadExcelFormNPL f = new ReadExcelFormNPL();
            f.goodItemNew = goodItem;
            f.LoaiHangHoa = LoaiHangHoa;
            f.ShowDialog(this);
            BindDataHangHoa();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgListTotal.RootTable.Columns["MaHangHoa"], ConditionOperator.Contains, txtMaHang.Text);
                dgListTotal.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaHangHoa_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaHangHoa.Value != null)
                {
                    switch (LoaiHangHoa)
                    {
                        case "NPL":
                            if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                            {
                                foreach (HangDuaVao item in HangDuaVaoCollection)
                                {
                                    if (item.Ma == cbbMaHangHoa.Value.ToString())
                                    {
                                        txtTenHangHoa.Text = item.Ten;
                                        ctrDVTLuong1.Code = this.DVT_VNACC(item.DVT_ID.PadRight(3));
                                        break;
                                    }
                                }
                            }
                            else
                            {
                                foreach (Company.BLL.KDT.SXXK.SXXK_NguyenPhuLieu item in NPLCollection)
                                {
                                    if (item.Ma == cbbMaHangHoa.Value.ToString())
                                    {
                                        txtTenHangHoa.Text = item.Ten;
                                        ctrDVTLuong1.Code = this.DVT_VNACC(item.DVT_ID.PadRight(3));
                                        break;
                                    }
                                }
                            }
                            break;
                        case "SP":
                            foreach (Company.BLL.KDT.SXXK.SXXK_SanPham item in SPCollection)
                            {
                                if (item.Ma == cbbMaHangHoa.Value.ToString())
                                {
                                    txtTenHangHoa.Text = item.Ten;
                                    ctrDVTLuong1.Code = this.DVT_VNACC(item.DVT_ID.PadRight(3));
                                    break;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
