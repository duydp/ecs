﻿namespace Company.Interface.Feedback
{
    partial class QuanLyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuanLyForm));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tabMain = new Janus.Windows.UI.Tab.UITab();
            this.tabPageQuestion = new Janus.Windows.UI.Tab.UITabPage();
            this.linhVucCauHoi = new Company.Interface.Feedback.LinhVucCauHoi();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.cauHoiUserControl = new Company.Interface.Feedback.CauHoiUserControl();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.btnTraLoi = new Janus.Windows.EditControls.UIButton();
            this.cauhoiDoanhNghiep1 = new Company.Interface.Feedback.CauhoiDoanhNghiep();
            this.btnDanhSachDN = new Janus.Windows.EditControls.UIButton();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tabPageSearch = new Janus.Windows.UI.Tab.UITabPage();
            this.cauHoiUserControlTim = new Company.Interface.Feedback.CauHoiUserControl();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnTim = new Janus.Windows.EditControls.UIButton();
            this.txtTim = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTraLoi = new System.Windows.Forms.RichTextBox();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNoiDung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cauHoiUserControl1 = new Company.Interface.Feedback.CauHoiUserControl();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.btnReset = new Janus.Windows.EditControls.UIButton();
            this.btnLinhVuc = new Janus.Windows.EditControls.UIButton();
            this.btnCauHoi = new Janus.Windows.EditControls.UIButton();
            this.bntSuaCauHoi = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tabMain)).BeginInit();
            this.tabMain.SuspendLayout();
            this.tabPageQuestion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            this.tabPageSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.bntSuaCauHoi);
            this.grbMain.Controls.Add(this.btnCauHoi);
            this.grbMain.Controls.Add(this.btnLinhVuc);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.btnDelete);
            this.grbMain.Controls.Add(this.btnReset);
            this.grbMain.Controls.Add(this.splitContainer1);
            this.grbMain.Size = new System.Drawing.Size(981, 672);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(3, 3);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tabMain);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.uiGroupBox2);
            this.splitContainer1.Panel2.Controls.Add(this.splitter2);
            this.splitContainer1.Panel2.Controls.Add(this.uiGroupBox1);
            this.splitContainer1.Size = new System.Drawing.Size(975, 635);
            this.splitContainer1.SplitterDistance = 434;
            this.splitContainer1.SplitterWidth = 5;
            this.splitContainer1.TabIndex = 0;
            // 
            // tabMain
            // 
            this.tabMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabMain.Location = new System.Drawing.Point(0, 0);
            this.tabMain.Name = "tabMain";
            this.tabMain.Size = new System.Drawing.Size(434, 635);
            this.tabMain.TabIndex = 0;
            this.tabMain.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.tabPageQuestion,
            this.tabPageSearch});
            this.tabMain.VisualStyleManager = this.vsmMain;
            // 
            // tabPageQuestion
            // 
            this.tabPageQuestion.Controls.Add(this.linhVucCauHoi);
            this.tabPageQuestion.Controls.Add(this.uiTab1);
            this.tabPageQuestion.Controls.Add(this.splitter1);
            this.tabPageQuestion.Location = new System.Drawing.Point(1, 21);
            this.tabPageQuestion.Name = "tabPageQuestion";
            this.tabPageQuestion.Size = new System.Drawing.Size(432, 613);
            this.tabPageQuestion.TabStop = true;
            this.tabPageQuestion.Text = "Câu hỏi";
            // 
            // linhVucCauHoi
            // 
            this.linhVucCauHoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.linhVucCauHoi.Location = new System.Drawing.Point(0, 5);
            this.linhVucCauHoi.Name = "linhVucCauHoi";
            this.linhVucCauHoi.Size = new System.Drawing.Size(432, 366);
            this.linhVucCauHoi.TabIndex = 3;
            // 
            // uiTab1
            // 
            this.uiTab1.BackColor = System.Drawing.Color.Transparent;
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiTab1.Location = new System.Drawing.Point(0, 371);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(432, 242);
            this.uiTab1.TabIndex = 2;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2003;
            this.uiTab1.VisualStyleManager = this.vsmMain;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.Controls.Add(this.cauHoiUserControl);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(430, 220);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thư viện câu hỏi";
            // 
            // cauHoiUserControl
            // 
            this.cauHoiUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cauHoiUserControl.Location = new System.Drawing.Point(0, 0);
            this.cauHoiUserControl.Name = "cauHoiUserControl";
            this.cauHoiUserControl.Size = new System.Drawing.Size(430, 220);
            this.cauHoiUserControl.TabIndex = 0;
            this.cauHoiUserControl.CauHoiSelectionChanged += new Company.Interface.Feedback.CauHoiUserControl.CauHoiSelectionChangedEventHandler(this.cauHoiUserControl_CauHoiSelectionChanged);
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.Controls.Add(this.btnTraLoi);
            this.uiTabPage2.Controls.Add(this.cauhoiDoanhNghiep1);
            this.uiTabPage2.Controls.Add(this.btnDanhSachDN);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(430, 220);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Danh sách câu hỏi doanh nghiệp";
            // 
            // btnTraLoi
            // 
            this.btnTraLoi.Location = new System.Drawing.Point(105, 194);
            this.btnTraLoi.Name = "btnTraLoi";
            this.btnTraLoi.Size = new System.Drawing.Size(90, 23);
            this.btnTraLoi.TabIndex = 1;
            this.btnTraLoi.Text = "Trả lời";
            this.btnTraLoi.VisualStyleManager = this.vsmMain;
            this.btnTraLoi.Click += new System.EventHandler(this.btnTraLoi_Click);
            // 
            // cauhoiDoanhNghiep1
            // 
            this.cauhoiDoanhNghiep1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cauhoiDoanhNghiep1.Location = new System.Drawing.Point(0, 0);
            this.cauhoiDoanhNghiep1.Name = "cauhoiDoanhNghiep1";
            this.cauhoiDoanhNghiep1.Size = new System.Drawing.Size(430, 189);
            this.cauhoiDoanhNghiep1.TabIndex = 0;
            // 
            // btnDanhSachDN
            // 
            this.btnDanhSachDN.Location = new System.Drawing.Point(9, 194);
            this.btnDanhSachDN.Name = "btnDanhSachDN";
            this.btnDanhSachDN.Size = new System.Drawing.Size(90, 23);
            this.btnDanhSachDN.TabIndex = 1;
            this.btnDanhSachDN.Text = "Danh sách ";
            this.btnDanhSachDN.VisualStyleManager = this.vsmMain;
            this.btnDanhSachDN.Click += new System.EventHandler(this.btnDanhSachDN_Click);
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(432, 5);
            this.splitter1.TabIndex = 1;
            this.splitter1.TabStop = false;
            // 
            // tabPageSearch
            // 
            this.tabPageSearch.Controls.Add(this.cauHoiUserControlTim);
            this.tabPageSearch.Controls.Add(this.uiGroupBox3);
            this.tabPageSearch.Location = new System.Drawing.Point(1, 21);
            this.tabPageSearch.Name = "tabPageSearch";
            this.tabPageSearch.Size = new System.Drawing.Size(432, 576);
            this.tabPageSearch.TabStop = true;
            this.tabPageSearch.Text = "Tìm kiếm";
            // 
            // cauHoiUserControlTim
            // 
            this.cauHoiUserControlTim.AllowDrop = true;
            this.cauHoiUserControlTim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cauHoiUserControlTim.Location = new System.Drawing.Point(0, 64);
            this.cauHoiUserControlTim.Name = "cauHoiUserControlTim";
            this.cauHoiUserControlTim.Size = new System.Drawing.Size(432, 512);
            this.cauHoiUserControlTim.TabIndex = 1;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnTim);
            this.uiGroupBox3.Controls.Add(this.txtTim);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(432, 64);
            this.uiGroupBox3.TabIndex = 0;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // btnTim
            // 
            this.btnTim.Icon = ((System.Drawing.Icon)(resources.GetObject("btnTim.Icon")));
            this.btnTim.Location = new System.Drawing.Point(351, 32);
            this.btnTim.Name = "btnTim";
            this.btnTim.Size = new System.Drawing.Size(75, 23);
            this.btnTim.TabIndex = 2;
            this.btnTim.Text = "Tìm ";
            this.btnTim.VisualStyleManager = this.vsmMain;
            this.btnTim.Click += new System.EventHandler(this.btnTim_Click);
            // 
            // txtTim
            // 
            this.txtTim.Location = new System.Drawing.Point(11, 33);
            this.txtTim.Name = "txtTim";
            this.txtTim.Size = new System.Drawing.Size(334, 21);
            this.txtTim.TabIndex = 1;
            this.txtTim.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tìm câu hỏi";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Controls.Add(this.txtTraLoi);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 109);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(536, 526);
            this.uiGroupBox2.TabIndex = 2;
            this.uiGroupBox2.Text = "Trả lời";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtTraLoi
            // 
            this.txtTraLoi.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.txtTraLoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTraLoi.Location = new System.Drawing.Point(3, 17);
            this.txtTraLoi.Name = "txtTraLoi";
            this.txtTraLoi.ReadOnly = true;
            this.txtTraLoi.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.txtTraLoi.Size = new System.Drawing.Size(530, 506);
            this.txtTraLoi.TabIndex = 0;
            this.txtTraLoi.Text = "";
            this.txtTraLoi.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter2.Location = new System.Drawing.Point(0, 104);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(536, 5);
            this.splitter2.TabIndex = 2;
            this.splitter2.TabStop = false;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.txtNoiDung);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(536, 104);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Câu hỏi";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtNoiDung
            // 
            this.txtNoiDung.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoiDung.Location = new System.Drawing.Point(3, 17);
            this.txtNoiDung.Multiline = true;
            this.txtNoiDung.Name = "txtNoiDung";
            this.txtNoiDung.ReadOnly = true;
            this.txtNoiDung.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtNoiDung.Size = new System.Drawing.Size(530, 84);
            this.txtNoiDung.TabIndex = 0;
            this.txtNoiDung.VisualStyleManager = this.vsmMain;
            this.txtNoiDung.TextChanged += new System.EventHandler(this.textBox_TextChanged);
            // 
            // cauHoiUserControl1
            // 
            this.cauHoiUserControl1.BackColor = System.Drawing.Color.Transparent;
            this.cauHoiUserControl1.Location = new System.Drawing.Point(3, 3);
            this.cauHoiUserControl1.Name = "cauHoiUserControl1";
            this.cauHoiUserControl1.Size = new System.Drawing.Size(421, 123);
            this.cauHoiUserControl1.TabIndex = 2;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(900, 644);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Icon = ((System.Drawing.Icon)(resources.GetObject("btnDelete.Icon")));
            this.btnDelete.Location = new System.Drawing.Point(819, 644);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 4;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyleManager = this.vsmMain;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.Icon = ((System.Drawing.Icon)(resources.GetObject("btnReset.Icon")));
            this.btnReset.Location = new System.Drawing.Point(738, 644);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(75, 23);
            this.btnReset.TabIndex = 3;
            this.btnReset.Text = "Làm mới";
            this.btnReset.VisualStyleManager = this.vsmMain;
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // btnLinhVuc
            // 
            this.btnLinhVuc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLinhVuc.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLinhVuc.Icon")));
            this.btnLinhVuc.Location = new System.Drawing.Point(14, 644);
            this.btnLinhVuc.Name = "btnLinhVuc";
            this.btnLinhVuc.Size = new System.Drawing.Size(90, 23);
            this.btnLinhVuc.TabIndex = 1;
            this.btnLinhVuc.Text = "Lĩnh vực...";
            this.btnLinhVuc.VisualStyleManager = this.vsmMain;
            this.btnLinhVuc.Click += new System.EventHandler(this.btnLinhVuc_Click);
            // 
            // btnCauHoi
            // 
            this.btnCauHoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnCauHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnCauHoi.Icon")));
            this.btnCauHoi.Location = new System.Drawing.Point(110, 643);
            this.btnCauHoi.Name = "btnCauHoi";
            this.btnCauHoi.Size = new System.Drawing.Size(90, 23);
            this.btnCauHoi.TabIndex = 2;
            this.btnCauHoi.Text = "Thêm câu hỏi";
            this.btnCauHoi.VisualStyleManager = this.vsmMain;
            this.btnCauHoi.Click += new System.EventHandler(this.btnCauHoi_Click);
            // 
            // bntSuaCauHoi
            // 
            this.bntSuaCauHoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bntSuaCauHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("bntSuaCauHoi.Icon")));
            this.bntSuaCauHoi.Location = new System.Drawing.Point(206, 644);
            this.bntSuaCauHoi.Name = "bntSuaCauHoi";
            this.bntSuaCauHoi.Size = new System.Drawing.Size(90, 23);
            this.bntSuaCauHoi.TabIndex = 7;
            this.bntSuaCauHoi.Text = "Sửa câu hỏi";
            this.bntSuaCauHoi.VisualStyleManager = this.vsmMain;
            this.bntSuaCauHoi.Click += new System.EventHandler(this.bntSuaCauHoi_Click);
            // 
            // QuanLyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(981, 672);
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "QuanLyForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Quản lý thông tin góp ý & giải đáp";
            this.Load += new System.EventHandler(this.QuanLyForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.tabMain)).EndInit();
            this.tabMain.ResumeLayout(false);
            this.tabPageQuestion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage2.ResumeLayout(false);
            this.tabPageSearch.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private Janus.Windows.UI.Tab.UITab tabMain;
        private Janus.Windows.UI.Tab.UITabPage tabPageQuestion;
        private CauHoiUserControl cauHoiUserControl1;
        private System.Windows.Forms.Splitter splitter1;
        private Janus.Windows.UI.Tab.UITabPage tabPageSearch;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.RichTextBox txtTraLoi;
        private System.Windows.Forms.Splitter splitter2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiDung;
        public Janus.Windows.EditControls.UIButton btnClose;
        public Janus.Windows.EditControls.UIButton btnDelete;
        public Janus.Windows.EditControls.UIButton btnReset;
        public Janus.Windows.EditControls.UIButton btnLinhVuc;
        public Janus.Windows.EditControls.UIButton btnCauHoi;
        private CauHoiUserControl cauHoiUserControlTim;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnTim;
        private Janus.Windows.GridEX.EditControls.EditBox txtTim;
        private System.Windows.Forms.Label label1;
        public Janus.Windows.EditControls.UIButton bntSuaCauHoi;
        private LinhVucCauHoi linhVucCauHoi;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private CauHoiUserControl cauHoiUserControl;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private CauhoiDoanhNghiep cauhoiDoanhNghiep1;
        private Janus.Windows.EditControls.UIButton btnTraLoi;
        private Janus.Windows.EditControls.UIButton btnDanhSachDN;
    }
}