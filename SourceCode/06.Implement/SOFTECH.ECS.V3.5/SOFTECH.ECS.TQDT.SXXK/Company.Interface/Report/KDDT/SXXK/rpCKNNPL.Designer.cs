﻿namespace Company.Interface.Report.SXXK
{
    partial class rpCKNNPL
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpCKNNPL));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox6 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox5 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox4 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox3 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lblCongTy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBill = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoInvoice = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblDoanhNghiep = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellSoCV = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHoTro = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCuc1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChuKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblNgaythang = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 0;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgaythang,
            this.xrLabel2,
            this.xrPictureBox6,
            this.xrPictureBox5,
            this.xrPictureBox4,
            this.xrPictureBox3,
            this.xrPictureBox1,
            this.xrLine2,
            this.lblCongTy,
            this.lblSoLuong,
            this.lblSoBill,
            this.lblSoInvoice,
            this.lblTenHang,
            this.xrLabel19,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel16,
            this.xrLabel15,
            this.xrTable3,
            this.xrLabel31,
            this.xrLabel1,
            this.lblHoTro,
            this.lblChiCuc1,
            this.xrLabel3});
            this.ReportHeader.Height = 505;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.ParentStyleUsing.UseBorders = false;
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.Location = new System.Drawing.Point(67, 267);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.ParentStyleUsing.UseFont = false;
            this.xrLabel2.Size = new System.Drawing.Size(633, 42);
            this.xrLabel2.Text = "        Vừa qua, Công ty chúng tôi có ký hợp đồng với khách hàng để nhập khẩu hàn" +
                "g hóa chi tiết như sau:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrPictureBox6
            // 
            this.xrPictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox6.Image")));
            this.xrPictureBox6.Location = new System.Drawing.Point(125, 375);
            this.xrPictureBox6.Name = "xrPictureBox6";
            this.xrPictureBox6.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox6.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrPictureBox5
            // 
            this.xrPictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox5.Image")));
            this.xrPictureBox5.Location = new System.Drawing.Point(125, 325);
            this.xrPictureBox5.Name = "xrPictureBox5";
            this.xrPictureBox5.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox5.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrPictureBox4
            // 
            this.xrPictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox4.Image")));
            this.xrPictureBox4.Location = new System.Drawing.Point(125, 425);
            this.xrPictureBox4.Name = "xrPictureBox4";
            this.xrPictureBox4.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrPictureBox3
            // 
            this.xrPictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox3.Image")));
            this.xrPictureBox3.Location = new System.Drawing.Point(125, 350);
            this.xrPictureBox3.Name = "xrPictureBox3";
            this.xrPictureBox3.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.Location = new System.Drawing.Point(125, 400);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrLine2
            // 
            this.xrLine2.Location = new System.Drawing.Point(442, 50);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Size = new System.Drawing.Size(141, 9);
            // 
            // lblCongTy
            // 
            this.lblCongTy.BorderColor = System.Drawing.Color.Black;
            this.lblCongTy.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCongTy.Location = new System.Drawing.Point(233, 325);
            this.lblCongTy.Name = "lblCongTy";
            this.lblCongTy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 3, 0, 100F);
            this.lblCongTy.ParentStyleUsing.UseBackColor = false;
            this.lblCongTy.ParentStyleUsing.UseBorderColor = false;
            this.lblCongTy.ParentStyleUsing.UseBorders = false;
            this.lblCongTy.ParentStyleUsing.UseFont = false;
            this.lblCongTy.Size = new System.Drawing.Size(467, 25);
            this.lblCongTy.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.BorderColor = System.Drawing.Color.Black;
            this.lblSoLuong.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong.Location = new System.Drawing.Point(217, 425);
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuong.ParentStyleUsing.UseBackColor = false;
            this.lblSoLuong.ParentStyleUsing.UseBorderColor = false;
            this.lblSoLuong.ParentStyleUsing.UseBorders = false;
            this.lblSoLuong.ParentStyleUsing.UseFont = false;
            this.lblSoLuong.Size = new System.Drawing.Size(483, 25);
            this.lblSoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoLuong.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblSoBill
            // 
            this.lblSoBill.BorderColor = System.Drawing.Color.Black;
            this.lblSoBill.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoBill.Location = new System.Drawing.Point(200, 400);
            this.lblSoBill.Name = "lblSoBill";
            this.lblSoBill.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBill.ParentStyleUsing.UseBackColor = false;
            this.lblSoBill.ParentStyleUsing.UseBorderColor = false;
            this.lblSoBill.ParentStyleUsing.UseBorders = false;
            this.lblSoBill.ParentStyleUsing.UseFont = false;
            this.lblSoBill.Size = new System.Drawing.Size(500, 25);
            this.lblSoBill.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoBill.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblSoInvoice
            // 
            this.lblSoInvoice.BorderColor = System.Drawing.Color.Black;
            this.lblSoInvoice.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoInvoice.Location = new System.Drawing.Point(225, 375);
            this.lblSoInvoice.Name = "lblSoInvoice";
            this.lblSoInvoice.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoInvoice.ParentStyleUsing.UseBackColor = false;
            this.lblSoInvoice.ParentStyleUsing.UseBorderColor = false;
            this.lblSoInvoice.ParentStyleUsing.UseBorders = false;
            this.lblSoInvoice.ParentStyleUsing.UseFont = false;
            this.lblSoInvoice.Size = new System.Drawing.Size(475, 25);
            this.lblSoInvoice.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoInvoice.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblTenHang
            // 
            this.lblTenHang.BorderColor = System.Drawing.Color.Black;
            this.lblTenHang.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenHang.Location = new System.Drawing.Point(217, 350);
            this.lblTenHang.Name = "lblTenHang";
            this.lblTenHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenHang.ParentStyleUsing.UseBackColor = false;
            this.lblTenHang.ParentStyleUsing.UseBorderColor = false;
            this.lblTenHang.ParentStyleUsing.UseBorders = false;
            this.lblTenHang.ParentStyleUsing.UseFont = false;
            this.lblTenHang.Size = new System.Drawing.Size(483, 25);
            this.lblTenHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblTenHang.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel19.Location = new System.Drawing.Point(142, 325);
            this.xrLabel19.Multiline = true;
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.ParentStyleUsing.UseFont = false;
            this.xrLabel19.Size = new System.Drawing.Size(91, 25);
            this.xrLabel19.Text = "Tên công ty:";
            this.xrLabel19.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.Location = new System.Drawing.Point(142, 400);
            this.xrLabel18.Multiline = true;
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.ParentStyleUsing.UseFont = false;
            this.xrLabel18.Size = new System.Drawing.Size(58, 25);
            this.xrLabel18.Text = "Số bill:";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.Location = new System.Drawing.Point(142, 425);
            this.xrLabel17.Multiline = true;
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.ParentStyleUsing.UseFont = false;
            this.xrLabel17.Size = new System.Drawing.Size(75, 25);
            this.xrLabel17.Text = "Số lượng:";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.Location = new System.Drawing.Point(142, 375);
            this.xrLabel16.Multiline = true;
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.ParentStyleUsing.UseFont = false;
            this.xrLabel16.Size = new System.Drawing.Size(83, 25);
            this.xrLabel16.Text = "Số Invoice:";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.Location = new System.Drawing.Point(142, 350);
            this.xrLabel15.Multiline = true;
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.ParentStyleUsing.UseFont = false;
            this.xrLabel15.Size = new System.Drawing.Size(75, 25);
            this.xrLabel15.Text = "Tên hàng:";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Location = new System.Drawing.Point(58, 8);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.ParentStyleUsing.UseBorders = false;
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow5,
            this.xrTableRow4});
            this.xrTable3.Size = new System.Drawing.Size(217, 50);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblDoanhNghiep});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(217, 25);
            // 
            // lblDoanhNghiep
            // 
            this.lblDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDoanhNghiep.Location = new System.Drawing.Point(0, 0);
            this.lblDoanhNghiep.Multiline = true;
            this.lblDoanhNghiep.Name = "lblDoanhNghiep";
            this.lblDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblDoanhNghiep.Size = new System.Drawing.Size(217, 25);
            this.lblDoanhNghiep.Text = "CÔNG TY SX & THƯƠNG MẠI VINATEX";
            this.lblDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell10});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Size = new System.Drawing.Size(217, 8);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLine1});
            this.xrTableCell10.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell10.Multiline = true;
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell10.Size = new System.Drawing.Size(217, 8);
            this.xrTableCell10.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLine1
            // 
            this.xrLine1.Location = new System.Drawing.Point(42, 0);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.Size = new System.Drawing.Size(133, 9);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellSoCV});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(217, 17);
            // 
            // cellSoCV
            // 
            this.cellSoCV.BorderColor = System.Drawing.Color.Black;
            this.cellSoCV.CanShrink = true;
            this.cellSoCV.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cellSoCV.Location = new System.Drawing.Point(0, 0);
            this.cellSoCV.Multiline = true;
            this.cellSoCV.Name = "cellSoCV";
            this.cellSoCV.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellSoCV.ParentStyleUsing.UseBackColor = false;
            this.cellSoCV.ParentStyleUsing.UseBorderColor = false;
            this.cellSoCV.ParentStyleUsing.UseBorders = false;
            this.cellSoCV.ParentStyleUsing.UseFont = false;
            this.cellSoCV.Size = new System.Drawing.Size(217, 17);
            this.cellSoCV.Text = "Số:       /CV.XNK";
            this.cellSoCV.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.cellSoCV.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.cellSoCV_PreviewDoubleClick);
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel31.Location = new System.Drawing.Point(92, 175);
            this.xrLabel31.Multiline = true;
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.ParentStyleUsing.UseFont = false;
            this.xrLabel31.Size = new System.Drawing.Size(608, 25);
            this.xrLabel31.Text = "\"V/v: Cam kết nhập nguyên phụ liệu may mặc dùng để sản xuất hàng xuất khẩu\"";
            this.xrLabel31.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel31.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.Location = new System.Drawing.Point(333, 8);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.ParentStyleUsing.UseFont = false;
            this.xrLabel1.Size = new System.Drawing.Size(366, 42);
            this.xrLabel1.Text = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM\r\nĐộc Lập - Tự Do - Hạnh Phúc";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblHoTro
            // 
            this.lblHoTro.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHoTro.Location = new System.Drawing.Point(67, 217);
            this.lblHoTro.Multiline = true;
            this.lblHoTro.Name = "lblHoTro";
            this.lblHoTro.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHoTro.ParentStyleUsing.UseFont = false;
            this.lblHoTro.Size = new System.Drawing.Size(633, 42);
            this.lblHoTro.Text = "        # chúng tôi chân thành cảm ơn sự hổ trợ của quý Hải Quan trong suốt thời " +
                "gian qua.";
            this.lblHoTro.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblHoTro.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblChiCuc1
            // 
            this.lblChiCuc1.BorderColor = System.Drawing.Color.Black;
            this.lblChiCuc1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChiCuc1.Location = new System.Drawing.Point(167, 133);
            this.lblChiCuc1.Name = "lblChiCuc1";
            this.lblChiCuc1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.lblChiCuc1.ParentStyleUsing.UseBackColor = false;
            this.lblChiCuc1.ParentStyleUsing.UseBorderColor = false;
            this.lblChiCuc1.ParentStyleUsing.UseBorders = false;
            this.lblChiCuc1.ParentStyleUsing.UseFont = false;
            this.lblChiCuc1.Size = new System.Drawing.Size(533, 25);
            this.lblChiCuc1.Text = "- CHI CỤC HẢI QUAN QL HÀNG XNK NGOÀI KCN";
            this.lblChiCuc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblChiCuc1.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.Location = new System.Drawing.Point(92, 133);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.ParentStyleUsing.UseFont = false;
            this.xrLabel3.Size = new System.Drawing.Size(75, 25);
            this.xrLabel3.Text = "Kính gửi:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel14,
            this.xrLabel13,
            this.lblChuKy});
            this.ReportFooter.Height = 180;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.Location = new System.Drawing.Point(58, 75);
            this.xrLabel14.Multiline = true;
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.ParentStyleUsing.UseFont = false;
            this.xrLabel14.Size = new System.Drawing.Size(641, 25);
            this.xrLabel14.Text = "        Trân trọng.";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.Location = new System.Drawing.Point(58, 8);
            this.xrLabel13.Multiline = true;
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.ParentStyleUsing.UseFont = false;
            this.xrLabel13.Size = new System.Drawing.Size(641, 59);
            this.xrLabel13.Text = resources.GetString("xrLabel13.Text");
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            // 
            // lblChuKy
            // 
            this.lblChuKy.BorderColor = System.Drawing.Color.Black;
            this.lblChuKy.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChuKy.Location = new System.Drawing.Point(475, 108);
            this.lblChuKy.Multiline = true;
            this.lblChuKy.Name = "lblChuKy";
            this.lblChuKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChuKy.ParentStyleUsing.UseBackColor = false;
            this.lblChuKy.ParentStyleUsing.UseBorderColor = false;
            this.lblChuKy.ParentStyleUsing.UseBorders = false;
            this.lblChuKy.ParentStyleUsing.UseFont = false;
            this.lblChuKy.Size = new System.Drawing.Size(225, 50);
            this.lblChuKy.Text = "GIÁM ĐỐC CÔNG TY";
            this.lblChuKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblChuKy.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.Location = new System.Drawing.Point(125, 325);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.Size = new System.Drawing.Size(16, 25);
            // 
            // lblNgaythang
            // 
            this.lblNgaythang.BorderColor = System.Drawing.Color.Black;
            this.lblNgaythang.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaythang.Location = new System.Drawing.Point(333, 58);
            this.lblNgaythang.Name = "lblNgaythang";
            this.lblNgaythang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblNgaythang.ParentStyleUsing.UseBackColor = false;
            this.lblNgaythang.ParentStyleUsing.UseBorderColor = false;
            this.lblNgaythang.ParentStyleUsing.UseBorders = false;
            this.lblNgaythang.ParentStyleUsing.UseFont = false;
            this.lblNgaythang.Size = new System.Drawing.Size(367, 25);
            this.lblNgaythang.Text = ".................,ngày...........tháng.........năm.............";
            this.lblNgaythang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // rpCKNNPL
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportHeader,
            this.ReportFooter});
            this.Margins = new System.Drawing.Printing.Margins(34, 30, 65, 100);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblHoTro;
        private DevExpress.XtraReports.UI.XRLabel lblChiCuc1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lblChuKy;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblDoanhNghiep;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell cellSoCV;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel lblCongTy;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuong;
        private DevExpress.XtraReports.UI.XRLabel lblSoBill;
        private DevExpress.XtraReports.UI.XRLabel lblSoInvoice;
        private DevExpress.XtraReports.UI.XRLabel lblTenHang;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox5;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox4;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox3;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox6;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblNgaythang;
    }
}
