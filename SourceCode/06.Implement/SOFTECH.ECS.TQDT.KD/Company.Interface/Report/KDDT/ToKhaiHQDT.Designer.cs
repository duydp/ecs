﻿namespace Company.Interface.Report.KDDT
{
    partial class ToKhaiHQDT
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            DevExpress.XtraPrinting.Shape.ShapeRectangle shapeRectangle4 = new DevExpress.XtraPrinting.Shape.ShapeRectangle();
            DevExpress.XtraPrinting.BarCode.Code128Generator code128Generator4 = new DevExpress.XtraPrinting.BarCode.Code128Generator();
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSoHH = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblXuatXu = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonViTinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrPanel1 = new DevExpress.XtraReports.UI.XRPanel();
            this.lblChungTu = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHuongDan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGiaTinhThue = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDongTienThanhToan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhuongThucThanhToan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDieuKienGiaoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNuocXK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPhuongTienVanTai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCangDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCangXepHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayVanTaiDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblVanTaiDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHetHanHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHetHanGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHoaDon = new DevExpress.XtraReports.UI.XRLabel();
            this.lblHoaDonThuongMai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblLoaiHinh = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiNK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiXK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGui = new DevExpress.XtraReports.UI.XRLabel();
            this.lblThamChieu = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHQCK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.xrShape1 = new DevExpress.XtraReports.UI.XRShape();
            this.xrLine17 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel36 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel35 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine19 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel34 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel32 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine18 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLine16 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel33 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel31 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine10 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine5 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel30 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel29 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine15 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel26 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel28 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel27 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel25 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine14 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel19 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine13 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel22 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel23 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel24 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel21 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel20 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine11 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine4 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine12 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel10 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine3 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel6 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLine1 = new DevExpress.XtraReports.UI.XRLine();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel4 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrBarCode1 = new DevExpress.XtraReports.UI.XRBarCode();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.PageFooter = new DevExpress.XtraReports.UI.PageFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail.Height = 33;
            this.Detail.Name = "Detail";
            // 
            // xrTable2
            // 
            this.xrTable2.Location = new System.Drawing.Point(1, 0);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable2.Size = new System.Drawing.Size(733, 33);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTT,
            this.lblTenHang,
            this.lblMaSoHH,
            this.lblXuatXu,
            this.lblSoLuong,
            this.lblDonViTinh,
            this.lblDonGiaNT,
            this.lblTriGiaNT});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(733, 33);
            // 
            // lblSTT
            // 
            this.lblSTT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSTT.Location = new System.Drawing.Point(0, 0);
            this.lblSTT.Name = "lblSTT";
            this.lblSTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSTT.ParentStyleUsing.UseBorders = false;
            this.lblSTT.Size = new System.Drawing.Size(25, 33);
            this.lblSTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTenHang
            // 
            this.lblTenHang.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblTenHang.Location = new System.Drawing.Point(25, 0);
            this.lblTenHang.Name = "lblTenHang";
            this.lblTenHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenHang.ParentStyleUsing.UseBorders = false;
            this.lblTenHang.Size = new System.Drawing.Size(222, 33);
            // 
            // lblMaSoHH
            // 
            this.lblMaSoHH.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblMaSoHH.Location = new System.Drawing.Point(247, 0);
            this.lblMaSoHH.Name = "lblMaSoHH";
            this.lblMaSoHH.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMaSoHH.ParentStyleUsing.UseBorders = false;
            this.lblMaSoHH.Size = new System.Drawing.Size(94, 33);
            // 
            // lblXuatXu
            // 
            this.lblXuatXu.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblXuatXu.Location = new System.Drawing.Point(341, 0);
            this.lblXuatXu.Name = "lblXuatXu";
            this.lblXuatXu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblXuatXu.ParentStyleUsing.UseBorders = false;
            this.lblXuatXu.Size = new System.Drawing.Size(75, 33);
            this.lblXuatXu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblSoLuong.Location = new System.Drawing.Point(416, 0);
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuong.ParentStyleUsing.UseBorders = false;
            this.lblSoLuong.Size = new System.Drawing.Size(83, 33);
            this.lblSoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblDonViTinh
            // 
            this.lblDonViTinh.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.lblDonViTinh.Location = new System.Drawing.Point(499, 0);
            this.lblDonViTinh.Name = "lblDonViTinh";
            this.lblDonViTinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonViTinh.ParentStyleUsing.UseBorders = false;
            this.lblDonViTinh.Size = new System.Drawing.Size(75, 33);
            this.lblDonViTinh.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblDonGiaNT
            // 
            this.lblDonGiaNT.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblDonGiaNT.Location = new System.Drawing.Point(574, 0);
            this.lblDonGiaNT.Name = "lblDonGiaNT";
            this.lblDonGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDonGiaNT.ParentStyleUsing.UseBorders = false;
            this.lblDonGiaNT.Size = new System.Drawing.Size(81, 33);
            this.lblDonGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // lblTriGiaNT
            // 
            this.lblTriGiaNT.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Right | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.lblTriGiaNT.Location = new System.Drawing.Point(655, 0);
            this.lblTriGiaNT.Name = "lblTriGiaNT";
            this.lblTriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTriGiaNT.ParentStyleUsing.UseBorders = false;
            this.lblTriGiaNT.Size = new System.Drawing.Size(78, 33);
            this.lblTriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrPanel1,
            this.xrLabel2,
            this.xrTable1,
            this.xrBarCode1,
            this.xrLabel3,
            this.xrLabel1});
            this.PageHeader.Height = 600;
            this.PageHeader.Name = "PageHeader";
            this.PageHeader.ParentStyleUsing.UseBorders = false;
            // 
            // xrPanel1
            // 
            this.xrPanel1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)));
            this.xrPanel1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblChungTu,
            this.lblHuongDan,
            this.lblTyGiaTinhThue,
            this.lblDongTienThanhToan,
            this.lblPhuongThucThanhToan,
            this.lblDieuKienGiaoHang,
            this.lblNuocXK,
            this.lblPhuongTienVanTai,
            this.lblCangDoHang,
            this.lblCangXepHang,
            this.lblNgayVanTaiDon,
            this.lblVanTaiDon,
            this.lblNgayHetHanHopDong,
            this.lblNgayHopDong,
            this.lblHopDong,
            this.lblNgayHetHanGiayPhep,
            this.lblNgayGiayPhep,
            this.lblGiayPhep,
            this.lblNgayHoaDon,
            this.lblHoaDonThuongMai,
            this.lblLoaiHinh,
            this.lblNguoiUyThac,
            this.lblNguoiNK,
            this.lblNguoiXK,
            this.lblToKhai,
            this.lblNgayDangKy,
            this.lblNgayGui,
            this.lblThamChieu,
            this.lblChiCucHQCK,
            this.lblChiCucHQ,
            this.xrShape1,
            this.xrLine17,
            this.xrLabel36,
            this.xrLabel35,
            this.xrLine19,
            this.xrLabel34,
            this.xrLabel32,
            this.xrLine18,
            this.xrLine16,
            this.xrLabel33,
            this.xrLabel31,
            this.xrLabel15,
            this.xrLabel14,
            this.xrLabel13,
            this.xrLine10,
            this.xrLabel12,
            this.xrLine5,
            this.xrLabel30,
            this.xrLabel29,
            this.xrLine15,
            this.xrLabel26,
            this.xrLabel28,
            this.xrLabel27,
            this.xrLabel25,
            this.xrLine14,
            this.xrLabel18,
            this.xrLabel19,
            this.xrLine13,
            this.xrLabel22,
            this.xrLabel23,
            this.xrLabel24,
            this.xrLabel21,
            this.xrLabel20,
            this.xrLine11,
            this.xrLabel11,
            this.xrLine4,
            this.xrLabel17,
            this.xrLine12,
            this.xrLabel16,
            this.xrLabel10,
            this.xrLine3,
            this.xrLabel8,
            this.xrLabel9,
            this.xrLine2,
            this.xrLabel7,
            this.xrLabel6,
            this.xrLine1,
            this.xrLabel5,
            this.xrLabel4});
            this.xrPanel1.Location = new System.Drawing.Point(1, 83);
            this.xrPanel1.Name = "xrPanel1";
            this.xrPanel1.ParentStyleUsing.UseBorders = false;
            this.xrPanel1.Size = new System.Drawing.Size(733, 467);
            // 
            // lblChungTu
            // 
            this.lblChungTu.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblChungTu.Location = new System.Drawing.Point(475, 392);
            this.lblChungTu.Name = "lblChungTu";
            this.lblChungTu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChungTu.ParentStyleUsing.UseBorders = false;
            this.lblChungTu.ParentStyleUsing.UseFont = false;
            this.lblChungTu.Size = new System.Drawing.Size(258, 75);
            // 
            // lblHuongDan
            // 
            this.lblHuongDan.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblHuongDan.Location = new System.Drawing.Point(0, 392);
            this.lblHuongDan.Name = "lblHuongDan";
            this.lblHuongDan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHuongDan.ParentStyleUsing.UseBorders = false;
            this.lblHuongDan.ParentStyleUsing.UseFont = false;
            this.lblHuongDan.Size = new System.Drawing.Size(458, 75);
            // 
            // lblTyGiaTinhThue
            // 
            this.lblTyGiaTinhThue.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTyGiaTinhThue.Location = new System.Drawing.Point(592, 342);
            this.lblTyGiaTinhThue.Name = "lblTyGiaTinhThue";
            this.lblTyGiaTinhThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGiaTinhThue.ParentStyleUsing.UseBorders = false;
            this.lblTyGiaTinhThue.ParentStyleUsing.UseFont = false;
            this.lblTyGiaTinhThue.Size = new System.Drawing.Size(142, 23);
            // 
            // lblDongTienThanhToan
            // 
            this.lblDongTienThanhToan.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblDongTienThanhToan.Location = new System.Drawing.Point(392, 342);
            this.lblDongTienThanhToan.Name = "lblDongTienThanhToan";
            this.lblDongTienThanhToan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDongTienThanhToan.ParentStyleUsing.UseBorders = false;
            this.lblDongTienThanhToan.ParentStyleUsing.UseFont = false;
            this.lblDongTienThanhToan.Size = new System.Drawing.Size(67, 23);
            this.lblDongTienThanhToan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblPhuongThucThanhToan
            // 
            this.lblPhuongThucThanhToan.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblPhuongThucThanhToan.Location = new System.Drawing.Point(483, 308);
            this.lblPhuongThucThanhToan.Name = "lblPhuongThucThanhToan";
            this.lblPhuongThucThanhToan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhuongThucThanhToan.ParentStyleUsing.UseBorders = false;
            this.lblPhuongThucThanhToan.ParentStyleUsing.UseFont = false;
            this.lblPhuongThucThanhToan.Size = new System.Drawing.Size(250, 23);
            // 
            // lblDieuKienGiaoHang
            // 
            this.lblDieuKienGiaoHang.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblDieuKienGiaoHang.Location = new System.Drawing.Point(258, 308);
            this.lblDieuKienGiaoHang.Name = "lblDieuKienGiaoHang";
            this.lblDieuKienGiaoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDieuKienGiaoHang.ParentStyleUsing.UseBorders = false;
            this.lblDieuKienGiaoHang.ParentStyleUsing.UseFont = false;
            this.lblDieuKienGiaoHang.Size = new System.Drawing.Size(200, 23);
            // 
            // lblNuocXK
            // 
            this.lblNuocXK.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNuocXK.Location = new System.Drawing.Point(683, 258);
            this.lblNuocXK.Name = "lblNuocXK";
            this.lblNuocXK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNuocXK.ParentStyleUsing.UseBorders = false;
            this.lblNuocXK.ParentStyleUsing.UseFont = false;
            this.lblNuocXK.Size = new System.Drawing.Size(51, 23);
            this.lblNuocXK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblPhuongTienVanTai
            // 
            this.lblPhuongTienVanTai.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblPhuongTienVanTai.Location = new System.Drawing.Point(392, 258);
            this.lblPhuongTienVanTai.Name = "lblPhuongTienVanTai";
            this.lblPhuongTienVanTai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPhuongTienVanTai.ParentStyleUsing.UseBorders = false;
            this.lblPhuongTienVanTai.ParentStyleUsing.UseFont = false;
            this.lblPhuongTienVanTai.Size = new System.Drawing.Size(175, 23);
            // 
            // lblCangDoHang
            // 
            this.lblCangDoHang.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblCangDoHang.Location = new System.Drawing.Point(575, 217);
            this.lblCangDoHang.Name = "lblCangDoHang";
            this.lblCangDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCangDoHang.ParentStyleUsing.UseBorders = false;
            this.lblCangDoHang.ParentStyleUsing.UseFont = false;
            this.lblCangDoHang.Size = new System.Drawing.Size(158, 23);
            // 
            // lblCangXepHang
            // 
            this.lblCangXepHang.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblCangXepHang.Location = new System.Drawing.Point(400, 217);
            this.lblCangXepHang.Name = "lblCangXepHang";
            this.lblCangXepHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCangXepHang.ParentStyleUsing.UseBorders = false;
            this.lblCangXepHang.ParentStyleUsing.UseFont = false;
            this.lblCangXepHang.Size = new System.Drawing.Size(166, 23);
            // 
            // lblNgayVanTaiDon
            // 
            this.lblNgayVanTaiDon.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayVanTaiDon.Location = new System.Drawing.Point(300, 225);
            this.lblNgayVanTaiDon.Name = "lblNgayVanTaiDon";
            this.lblNgayVanTaiDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayVanTaiDon.ParentStyleUsing.UseBorders = false;
            this.lblNgayVanTaiDon.ParentStyleUsing.UseFont = false;
            this.lblNgayVanTaiDon.Size = new System.Drawing.Size(91, 23);
            // 
            // lblVanTaiDon
            // 
            this.lblVanTaiDon.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblVanTaiDon.Location = new System.Drawing.Point(342, 183);
            this.lblVanTaiDon.Name = "lblVanTaiDon";
            this.lblVanTaiDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblVanTaiDon.ParentStyleUsing.UseBorders = false;
            this.lblVanTaiDon.ParentStyleUsing.UseFont = false;
            this.lblVanTaiDon.Size = new System.Drawing.Size(59, 23);
            // 
            // lblNgayHetHanHopDong
            // 
            this.lblNgayHetHanHopDong.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayHetHanHopDong.Location = new System.Drawing.Point(658, 150);
            this.lblNgayHetHanHopDong.Name = "lblNgayHetHanHopDong";
            this.lblNgayHetHanHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHetHanHopDong.ParentStyleUsing.UseBorders = false;
            this.lblNgayHetHanHopDong.ParentStyleUsing.UseFont = false;
            this.lblNgayHetHanHopDong.Size = new System.Drawing.Size(76, 23);
            // 
            // lblNgayHopDong
            // 
            this.lblNgayHopDong.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayHopDong.Location = new System.Drawing.Point(617, 125);
            this.lblNgayHopDong.Name = "lblNgayHopDong";
            this.lblNgayHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHopDong.ParentStyleUsing.UseBorders = false;
            this.lblNgayHopDong.ParentStyleUsing.UseFont = false;
            this.lblNgayHopDong.Size = new System.Drawing.Size(116, 23);
            // 
            // lblHopDong
            // 
            this.lblHopDong.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblHopDong.Location = new System.Drawing.Point(650, 100);
            this.lblHopDong.Name = "lblHopDong";
            this.lblHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHopDong.ParentStyleUsing.UseBorders = false;
            this.lblHopDong.ParentStyleUsing.UseFont = false;
            this.lblHopDong.Size = new System.Drawing.Size(84, 23);
            // 
            // lblNgayHetHanGiayPhep
            // 
            this.lblNgayHetHanGiayPhep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayHetHanGiayPhep.Location = new System.Drawing.Point(483, 150);
            this.lblNgayHetHanGiayPhep.Name = "lblNgayHetHanGiayPhep";
            this.lblNgayHetHanGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHetHanGiayPhep.ParentStyleUsing.UseBorders = false;
            this.lblNgayHetHanGiayPhep.ParentStyleUsing.UseFont = false;
            this.lblNgayHetHanGiayPhep.Size = new System.Drawing.Size(83, 23);
            // 
            // lblNgayGiayPhep
            // 
            this.lblNgayGiayPhep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayGiayPhep.Location = new System.Drawing.Point(442, 125);
            this.lblNgayGiayPhep.Name = "lblNgayGiayPhep";
            this.lblNgayGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayGiayPhep.ParentStyleUsing.UseBorders = false;
            this.lblNgayGiayPhep.ParentStyleUsing.UseFont = false;
            this.lblNgayGiayPhep.Size = new System.Drawing.Size(125, 23);
            // 
            // lblGiayPhep
            // 
            this.lblGiayPhep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblGiayPhep.Location = new System.Drawing.Point(483, 100);
            this.lblGiayPhep.Name = "lblGiayPhep";
            this.lblGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblGiayPhep.ParentStyleUsing.UseBorders = false;
            this.lblGiayPhep.ParentStyleUsing.UseFont = false;
            this.lblGiayPhep.Size = new System.Drawing.Size(83, 23);
            // 
            // lblNgayHoaDon
            // 
            this.lblNgayHoaDon.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNgayHoaDon.Location = new System.Drawing.Point(292, 150);
            this.lblNgayHoaDon.Name = "lblNgayHoaDon";
            this.lblNgayHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHoaDon.ParentStyleUsing.UseBorders = false;
            this.lblNgayHoaDon.ParentStyleUsing.UseFont = false;
            this.lblNgayHoaDon.Size = new System.Drawing.Size(100, 23);
            // 
            // lblHoaDonThuongMai
            // 
            this.lblHoaDonThuongMai.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblHoaDonThuongMai.Location = new System.Drawing.Point(250, 125);
            this.lblHoaDonThuongMai.Name = "lblHoaDonThuongMai";
            this.lblHoaDonThuongMai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblHoaDonThuongMai.ParentStyleUsing.UseBorders = false;
            this.lblHoaDonThuongMai.ParentStyleUsing.UseFont = false;
            this.lblHoaDonThuongMai.Size = new System.Drawing.Size(141, 23);
            // 
            // lblLoaiHinh
            // 
            this.lblLoaiHinh.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblLoaiHinh.Location = new System.Drawing.Point(350, 75);
            this.lblLoaiHinh.Name = "lblLoaiHinh";
            this.lblLoaiHinh.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblLoaiHinh.ParentStyleUsing.UseBorders = false;
            this.lblLoaiHinh.ParentStyleUsing.UseFont = false;
            this.lblLoaiHinh.Size = new System.Drawing.Size(383, 23);
            // 
            // lblNguoiUyThac
            // 
            this.lblNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNguoiUyThac.Location = new System.Drawing.Point(107, 233);
            this.lblNguoiUyThac.Name = "lblNguoiUyThac";
            this.lblNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiUyThac.ParentStyleUsing.UseBorders = false;
            this.lblNguoiUyThac.ParentStyleUsing.UseFont = false;
            this.lblNguoiUyThac.Size = new System.Drawing.Size(134, 23);
            // 
            // lblNguoiNK
            // 
            this.lblNguoiNK.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNguoiNK.Location = new System.Drawing.Point(0, 192);
            this.lblNguoiNK.Name = "lblNguoiNK";
            this.lblNguoiNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiNK.ParentStyleUsing.UseBorders = false;
            this.lblNguoiNK.ParentStyleUsing.UseFont = false;
            this.lblNguoiNK.Size = new System.Drawing.Size(242, 23);
            // 
            // lblNguoiXK
            // 
            this.lblNguoiXK.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblNguoiXK.Location = new System.Drawing.Point(0, 117);
            this.lblNguoiXK.Name = "lblNguoiXK";
            this.lblNguoiXK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNguoiXK.ParentStyleUsing.UseBorders = false;
            this.lblNguoiXK.ParentStyleUsing.UseFont = false;
            this.lblNguoiXK.Size = new System.Drawing.Size(242, 23);
            // 
            // lblToKhai
            // 
            this.lblToKhai.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblToKhai.Location = new System.Drawing.Point(492, 25);
            this.lblToKhai.Name = "lblToKhai";
            this.lblToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblToKhai.ParentStyleUsing.UseBorders = false;
            this.lblToKhai.ParentStyleUsing.UseFont = false;
            this.lblToKhai.Size = new System.Drawing.Size(242, 23);
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.lblNgayDangKy.Location = new System.Drawing.Point(608, 50);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.ParentStyleUsing.UseBorders = false;
            this.lblNgayDangKy.ParentStyleUsing.UseFont = false;
            this.lblNgayDangKy.Size = new System.Drawing.Size(125, 23);
            // 
            // lblNgayGui
            // 
            this.lblNgayGui.Font = new System.Drawing.Font("Times New Roman", 8F, System.Drawing.FontStyle.Bold);
            this.lblNgayGui.Location = new System.Drawing.Point(342, 50);
            this.lblNgayGui.Name = "lblNgayGui";
            this.lblNgayGui.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayGui.ParentStyleUsing.UseBorders = false;
            this.lblNgayGui.ParentStyleUsing.UseFont = false;
            this.lblNgayGui.Size = new System.Drawing.Size(134, 23);
            // 
            // lblThamChieu
            // 
            this.lblThamChieu.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblThamChieu.Location = new System.Drawing.Point(250, 25);
            this.lblThamChieu.Name = "lblThamChieu";
            this.lblThamChieu.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblThamChieu.ParentStyleUsing.UseBorders = false;
            this.lblThamChieu.ParentStyleUsing.UseFont = false;
            this.lblThamChieu.Size = new System.Drawing.Size(225, 23);
            // 
            // lblChiCucHQCK
            // 
            this.lblChiCucHQCK.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblChiCucHQCK.Location = new System.Drawing.Point(0, 42);
            this.lblChiCucHQCK.Name = "lblChiCucHQCK";
            this.lblChiCucHQCK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHQCK.ParentStyleUsing.UseBorders = false;
            this.lblChiCucHQCK.ParentStyleUsing.UseFont = false;
            this.lblChiCucHQCK.Size = new System.Drawing.Size(242, 23);
            // 
            // lblChiCucHQ
            // 
            this.lblChiCucHQ.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblChiCucHQ.Location = new System.Drawing.Point(125, 0);
            this.lblChiCucHQ.Name = "lblChiCucHQ";
            this.lblChiCucHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHQ.ParentStyleUsing.UseBorders = false;
            this.lblChiCucHQ.ParentStyleUsing.UseFont = false;
            this.lblChiCucHQ.Size = new System.Drawing.Size(117, 23);
            // 
            // xrShape1
            // 
            this.xrShape1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrShape1.Location = new System.Drawing.Point(23, 325);
            this.xrShape1.Name = "xrShape1";
            this.xrShape1.ParentStyleUsing.UseBorders = false;
            this.xrShape1.Shape = shapeRectangle4;
            this.xrShape1.Size = new System.Drawing.Size(33, 20);
            // 
            // xrLine17
            // 
            this.xrLine17.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine17.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine17.Location = new System.Drawing.Point(458, 292);
            this.xrLine17.Name = "xrLine17";
            this.xrLine17.ParentStyleUsing.UseBorders = false;
            this.xrLine17.Size = new System.Drawing.Size(17, 175);
            // 
            // xrLabel36
            // 
            this.xrLabel36.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel36.Location = new System.Drawing.Point(467, 375);
            this.xrLabel36.Name = "xrLabel36";
            this.xrLabel36.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel36.ParentStyleUsing.UseBorders = false;
            this.xrLabel36.ParentStyleUsing.UseFont = false;
            this.xrLabel36.Size = new System.Drawing.Size(208, 23);
            this.xrLabel36.Text = "19. Chứng từ Hải quan trước đó";
            // 
            // xrLabel35
            // 
            this.xrLabel35.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel35.Location = new System.Drawing.Point(0, 375);
            this.xrLabel35.Name = "xrLabel35";
            this.xrLabel35.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel35.ParentStyleUsing.UseBorders = false;
            this.xrLabel35.ParentStyleUsing.UseFont = false;
            this.xrLabel35.Size = new System.Drawing.Size(367, 23);
            this.xrLabel35.Text = "18. Kết quả phân luồng và hướng dẫn làm thủ tục Hải quan:";
            // 
            // xrLine19
            // 
            this.xrLine19.Location = new System.Drawing.Point(0, 358);
            this.xrLine19.Name = "xrLine19";
            this.xrLine19.ParentStyleUsing.UseBorders = false;
            this.xrLine19.Size = new System.Drawing.Size(734, 17);
            // 
            // xrLabel34
            // 
            this.xrLabel34.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel34.Location = new System.Drawing.Point(467, 342);
            this.xrLabel34.Name = "xrLabel34";
            this.xrLabel34.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel34.ParentStyleUsing.UseBorders = false;
            this.xrLabel34.ParentStyleUsing.UseFont = false;
            this.xrLabel34.Size = new System.Drawing.Size(125, 23);
            this.xrLabel34.Text = "17. Tỷ giá tính thuế:";
            // 
            // xrLabel32
            // 
            this.xrLabel32.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel32.Location = new System.Drawing.Point(467, 292);
            this.xrLabel32.Name = "xrLabel32";
            this.xrLabel32.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel32.ParentStyleUsing.UseBorders = false;
            this.xrLabel32.ParentStyleUsing.UseFont = false;
            this.xrLabel32.Size = new System.Drawing.Size(167, 23);
            this.xrLabel32.Text = "15. Phương thức thanh toán:";
            // 
            // xrLine18
            // 
            this.xrLine18.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine18.Location = new System.Drawing.Point(242, 325);
            this.xrLine18.Name = "xrLine18";
            this.xrLine18.ParentStyleUsing.UseBorders = false;
            this.xrLine18.Size = new System.Drawing.Size(483, 17);
            // 
            // xrLine16
            // 
            this.xrLine16.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine16.Location = new System.Drawing.Point(250, 283);
            this.xrLine16.Name = "xrLine16";
            this.xrLine16.ParentStyleUsing.UseBorders = false;
            this.xrLine16.Size = new System.Drawing.Size(483, 17);
            // 
            // xrLabel33
            // 
            this.xrLabel33.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel33.Location = new System.Drawing.Point(250, 342);
            this.xrLabel33.Name = "xrLabel33";
            this.xrLabel33.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel33.ParentStyleUsing.UseBorders = false;
            this.xrLabel33.ParentStyleUsing.UseFont = false;
            this.xrLabel33.Size = new System.Drawing.Size(150, 23);
            this.xrLabel33.Text = "16. Đồng tiền thanh toán:";
            // 
            // xrLabel31
            // 
            this.xrLabel31.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel31.Location = new System.Drawing.Point(250, 292);
            this.xrLabel31.Name = "xrLabel31";
            this.xrLabel31.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel31.ParentStyleUsing.UseBorders = false;
            this.xrLabel31.ParentStyleUsing.UseFont = false;
            this.xrLabel31.Size = new System.Drawing.Size(142, 23);
            this.xrLabel31.Text = "14. Điều kiện giao hàng:";
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel15.Location = new System.Drawing.Point(58, 325);
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.ParentStyleUsing.UseBorders = false;
            this.xrLabel15.ParentStyleUsing.UseFont = false;
            this.xrLabel15.Size = new System.Drawing.Size(75, 23);
            this.xrLabel15.Text = "Nộp thuế";
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel14.Location = new System.Drawing.Point(8, 308);
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.ParentStyleUsing.UseBorders = false;
            this.xrLabel14.ParentStyleUsing.UseFont = false;
            this.xrLabel14.Size = new System.Drawing.Size(125, 23);
            this.xrLabel14.Text = "Nội dung ủy quyền";
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel13.Location = new System.Drawing.Point(0, 283);
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.ParentStyleUsing.UseBorders = false;
            this.xrLabel13.ParentStyleUsing.UseFont = false;
            this.xrLabel13.Size = new System.Drawing.Size(191, 23);
            this.xrLabel13.Text = "4. Đại lý làm thủ tục Hải quan:";
            // 
            // xrLine10
            // 
            this.xrLine10.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine10.Location = new System.Drawing.Point(0, 267);
            this.xrLine10.Name = "xrLine10";
            this.xrLine10.ParentStyleUsing.UseBorders = false;
            this.xrLine10.Size = new System.Drawing.Size(242, 17);
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel12.Location = new System.Drawing.Point(0, 233);
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.ParentStyleUsing.UseBorders = false;
            this.xrLabel12.ParentStyleUsing.UseFont = false;
            this.xrLabel12.Size = new System.Drawing.Size(108, 23);
            this.xrLabel12.Text = "3. Người ủy thác:";
            // 
            // xrLine5
            // 
            this.xrLine5.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine5.Location = new System.Drawing.Point(0, 217);
            this.xrLine5.Name = "xrLine5";
            this.xrLine5.ParentStyleUsing.UseBorders = false;
            this.xrLine5.Size = new System.Drawing.Size(242, 17);
            // 
            // xrLabel30
            // 
            this.xrLabel30.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel30.Location = new System.Drawing.Point(575, 258);
            this.xrLabel30.Name = "xrLabel30";
            this.xrLabel30.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel30.ParentStyleUsing.UseBorders = false;
            this.xrLabel30.ParentStyleUsing.UseFont = false;
            this.xrLabel30.Size = new System.Drawing.Size(125, 23);
            this.xrLabel30.Text = "13. Nước xuất khẩu:";
            // 
            // xrLabel29
            // 
            this.xrLabel29.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel29.Location = new System.Drawing.Point(250, 258);
            this.xrLabel29.Name = "xrLabel29";
            this.xrLabel29.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel29.ParentStyleUsing.UseBorders = false;
            this.xrLabel29.ParentStyleUsing.UseFont = false;
            this.xrLabel29.Size = new System.Drawing.Size(150, 23);
            this.xrLabel29.Text = "12. Phương tiện vận tải:";
            // 
            // xrLine15
            // 
            this.xrLine15.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine15.Location = new System.Drawing.Point(250, 242);
            this.xrLine15.Name = "xrLine15";
            this.xrLine15.ParentStyleUsing.UseBorders = false;
            this.xrLine15.Size = new System.Drawing.Size(483, 17);
            // 
            // xrLabel26
            // 
            this.xrLabel26.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel26.Location = new System.Drawing.Point(250, 225);
            this.xrLabel26.Name = "xrLabel26";
            this.xrLabel26.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel26.ParentStyleUsing.UseBorders = false;
            this.xrLabel26.ParentStyleUsing.UseFont = false;
            this.xrLabel26.Size = new System.Drawing.Size(50, 23);
            this.xrLabel26.Text = "Ngày";
            // 
            // xrLabel28
            // 
            this.xrLabel28.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel28.Location = new System.Drawing.Point(575, 183);
            this.xrLabel28.Name = "xrLabel28";
            this.xrLabel28.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel28.ParentStyleUsing.UseBorders = false;
            this.xrLabel28.ParentStyleUsing.UseFont = false;
            this.xrLabel28.Size = new System.Drawing.Size(108, 23);
            this.xrLabel28.Text = "11. Cảng dỡ hàng:";
            // 
            // xrLabel27
            // 
            this.xrLabel27.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel27.Location = new System.Drawing.Point(400, 183);
            this.xrLabel27.Name = "xrLabel27";
            this.xrLabel27.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel27.ParentStyleUsing.UseBorders = false;
            this.xrLabel27.ParentStyleUsing.UseFont = false;
            this.xrLabel27.Size = new System.Drawing.Size(116, 23);
            this.xrLabel27.Text = "10. Cảng xếp hàng:";
            // 
            // xrLabel25
            // 
            this.xrLabel25.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel25.Location = new System.Drawing.Point(250, 183);
            this.xrLabel25.Name = "xrLabel25";
            this.xrLabel25.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel25.ParentStyleUsing.UseBorders = false;
            this.xrLabel25.ParentStyleUsing.UseFont = false;
            this.xrLabel25.Size = new System.Drawing.Size(91, 23);
            this.xrLabel25.Text = "9. Vận tải đơn:";
            // 
            // xrLine14
            // 
            this.xrLine14.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine14.Location = new System.Drawing.Point(242, 167);
            this.xrLine14.Name = "xrLine14";
            this.xrLine14.ParentStyleUsing.UseBorders = false;
            this.xrLine14.Size = new System.Drawing.Size(483, 17);
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel18.Location = new System.Drawing.Point(250, 150);
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.ParentStyleUsing.UseBorders = false;
            this.xrLabel18.ParentStyleUsing.UseFont = false;
            this.xrLabel18.Size = new System.Drawing.Size(42, 23);
            this.xrLabel18.Text = "Ngày";
            // 
            // xrLabel19
            // 
            this.xrLabel19.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel19.Location = new System.Drawing.Point(400, 100);
            this.xrLabel19.Name = "xrLabel19";
            this.xrLabel19.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel19.ParentStyleUsing.UseBorders = false;
            this.xrLabel19.ParentStyleUsing.UseFont = false;
            this.xrLabel19.Size = new System.Drawing.Size(84, 23);
            this.xrLabel19.Text = "7. Giấy phép:";
            // 
            // xrLine13
            // 
            this.xrLine13.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine13.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine13.Location = new System.Drawing.Point(567, 100);
            this.xrLine13.Name = "xrLine13";
            this.xrLine13.ParentStyleUsing.UseBorders = false;
            this.xrLine13.Size = new System.Drawing.Size(17, 192);
            // 
            // xrLabel22
            // 
            this.xrLabel22.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel22.Location = new System.Drawing.Point(575, 150);
            this.xrLabel22.Name = "xrLabel22";
            this.xrLabel22.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel22.ParentStyleUsing.UseBorders = false;
            this.xrLabel22.ParentStyleUsing.UseFont = false;
            this.xrLabel22.Size = new System.Drawing.Size(84, 23);
            this.xrLabel22.Text = "Ngày hết hạn";
            // 
            // xrLabel23
            // 
            this.xrLabel23.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel23.Location = new System.Drawing.Point(575, 125);
            this.xrLabel23.Name = "xrLabel23";
            this.xrLabel23.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel23.ParentStyleUsing.UseBorders = false;
            this.xrLabel23.ParentStyleUsing.UseFont = false;
            this.xrLabel23.Size = new System.Drawing.Size(42, 23);
            this.xrLabel23.Text = "Ngày";
            // 
            // xrLabel24
            // 
            this.xrLabel24.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel24.Location = new System.Drawing.Point(575, 100);
            this.xrLabel24.Name = "xrLabel24";
            this.xrLabel24.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel24.ParentStyleUsing.UseBorders = false;
            this.xrLabel24.ParentStyleUsing.UseFont = false;
            this.xrLabel24.Size = new System.Drawing.Size(84, 23);
            this.xrLabel24.Text = "8. Hợp đồng:";
            // 
            // xrLabel21
            // 
            this.xrLabel21.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel21.Location = new System.Drawing.Point(400, 150);
            this.xrLabel21.Name = "xrLabel21";
            this.xrLabel21.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel21.ParentStyleUsing.UseBorders = false;
            this.xrLabel21.ParentStyleUsing.UseFont = false;
            this.xrLabel21.Size = new System.Drawing.Size(84, 23);
            this.xrLabel21.Text = "Ngày hết hạn";
            // 
            // xrLabel20
            // 
            this.xrLabel20.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel20.Location = new System.Drawing.Point(400, 125);
            this.xrLabel20.Name = "xrLabel20";
            this.xrLabel20.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel20.ParentStyleUsing.UseBorders = false;
            this.xrLabel20.ParentStyleUsing.UseFont = false;
            this.xrLabel20.Size = new System.Drawing.Size(42, 23);
            this.xrLabel20.Text = "Ngày";
            // 
            // xrLine11
            // 
            this.xrLine11.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine11.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine11.Location = new System.Drawing.Point(392, 100);
            this.xrLine11.Name = "xrLine11";
            this.xrLine11.ParentStyleUsing.UseBorders = false;
            this.xrLine11.Size = new System.Drawing.Size(8, 150);
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel11.Location = new System.Drawing.Point(0, 167);
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.ParentStyleUsing.UseBorders = false;
            this.xrLabel11.ParentStyleUsing.UseFont = false;
            this.xrLabel11.Size = new System.Drawing.Size(125, 23);
            this.xrLabel11.Text = "2. Người nhập khẩu:";
            // 
            // xrLine4
            // 
            this.xrLine4.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine4.Location = new System.Drawing.Point(0, 142);
            this.xrLine4.Name = "xrLine4";
            this.xrLine4.ParentStyleUsing.UseBorders = false;
            this.xrLine4.Size = new System.Drawing.Size(242, 17);
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel17.Location = new System.Drawing.Point(250, 100);
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.ParentStyleUsing.UseBorders = false;
            this.xrLabel17.ParentStyleUsing.UseFont = false;
            this.xrLabel17.Size = new System.Drawing.Size(142, 23);
            this.xrLabel17.Text = "6. Hóa đơn thương mại:";
            // 
            // xrLine12
            // 
            this.xrLine12.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine12.Location = new System.Drawing.Point(250, 92);
            this.xrLine12.Name = "xrLine12";
            this.xrLine12.ParentStyleUsing.UseBorders = false;
            this.xrLine12.Size = new System.Drawing.Size(483, 17);
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel16.Location = new System.Drawing.Point(250, 75);
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.ParentStyleUsing.UseBorders = false;
            this.xrLabel16.ParentStyleUsing.UseFont = false;
            this.xrLabel16.Size = new System.Drawing.Size(100, 23);
            this.xrLabel16.Text = "5. Loại hình:";
            // 
            // xrLabel10
            // 
            this.xrLabel10.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel10.Location = new System.Drawing.Point(0, 92);
            this.xrLabel10.Name = "xrLabel10";
            this.xrLabel10.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel10.ParentStyleUsing.UseBorders = false;
            this.xrLabel10.ParentStyleUsing.UseFont = false;
            this.xrLabel10.Size = new System.Drawing.Size(116, 23);
            this.xrLabel10.Text = "1. Người xuất khẩu:";
            // 
            // xrLine3
            // 
            this.xrLine3.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine3.Location = new System.Drawing.Point(0, 67);
            this.xrLine3.Name = "xrLine3";
            this.xrLine3.ParentStyleUsing.UseBorders = false;
            this.xrLine3.Size = new System.Drawing.Size(733, 17);
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel8.Location = new System.Drawing.Point(492, 50);
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.ParentStyleUsing.UseBorders = false;
            this.xrLabel8.ParentStyleUsing.UseFont = false;
            this.xrLabel8.Size = new System.Drawing.Size(117, 23);
            this.xrLabel8.Text = "Ngày, giờ đăng ký:";
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel9.Location = new System.Drawing.Point(492, 8);
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.ParentStyleUsing.UseBorders = false;
            this.xrLabel9.ParentStyleUsing.UseFont = false;
            this.xrLabel9.Size = new System.Drawing.Size(75, 23);
            this.xrLabel9.Text = "Số tờ khai:";
            // 
            // xrLine2
            // 
            this.xrLine2.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine2.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine2.Location = new System.Drawing.Point(475, 0);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.ParentStyleUsing.UseBorders = false;
            this.xrLine2.Size = new System.Drawing.Size(18, 75);
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel7.Location = new System.Drawing.Point(250, 50);
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.ParentStyleUsing.UseBorders = false;
            this.xrLabel7.ParentStyleUsing.UseFont = false;
            this.xrLabel7.Size = new System.Drawing.Size(91, 23);
            this.xrLabel7.Text = "Ngày, giờ gửi:";
            // 
            // xrLabel6
            // 
            this.xrLabel6.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel6.Location = new System.Drawing.Point(250, 8);
            this.xrLabel6.Name = "xrLabel6";
            this.xrLabel6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel6.ParentStyleUsing.UseBorders = false;
            this.xrLabel6.ParentStyleUsing.UseFont = false;
            this.xrLabel6.Size = new System.Drawing.Size(100, 23);
            this.xrLabel6.Text = "Số tham chiếu:";
            // 
            // xrLine1
            // 
            this.xrLine1.LineDirection = DevExpress.XtraReports.UI.LineDirection.Vertical;
            this.xrLine1.LineStyle = System.Drawing.Drawing2D.DashStyle.Dot;
            this.xrLine1.Location = new System.Drawing.Point(232, 0);
            this.xrLine1.Name = "xrLine1";
            this.xrLine1.ParentStyleUsing.UseBorders = false;
            this.xrLine1.Size = new System.Drawing.Size(19, 367);
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.Location = new System.Drawing.Point(0, 25);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.ParentStyleUsing.UseBorders = false;
            this.xrLabel5.ParentStyleUsing.UseFont = false;
            this.xrLabel5.Size = new System.Drawing.Size(175, 23);
            this.xrLabel5.Text = "Chi cục Hải quan cửa khẩu:";
            // 
            // xrLabel4
            // 
            this.xrLabel4.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel4.Location = new System.Drawing.Point(0, 8);
            this.xrLabel4.Name = "xrLabel4";
            this.xrLabel4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel4.ParentStyleUsing.UseBorders = false;
            this.xrLabel4.ParentStyleUsing.UseFont = false;
            this.xrLabel4.Size = new System.Drawing.Size(117, 23);
            this.xrLabel4.Text = "Chi cục Hải quan:";
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.xrLabel2.Location = new System.Drawing.Point(275, 58);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.ParentStyleUsing.UseFont = false;
            this.xrLabel2.Size = new System.Drawing.Size(75, 23);
            this.xrLabel2.Text = "Nhập khẩu";
            // 
            // xrTable1
            // 
            this.xrTable1.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable1.Location = new System.Drawing.Point(1, 550);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.ParentStyleUsing.UseBorders = false;
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable1.Size = new System.Drawing.Size(733, 50);
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell2,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell3});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.ParentStyleUsing.UseBorders = false;
            this.xrTableRow1.Size = new System.Drawing.Size(733, 50);
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell1.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.ParentStyleUsing.UseBorders = false;
            this.xrTableCell1.Size = new System.Drawing.Size(25, 50);
            this.xrTableCell1.Text = "Số TT";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell7.Location = new System.Drawing.Point(25, 0);
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell7.ParentStyleUsing.UseBorders = false;
            this.xrTableCell7.Size = new System.Drawing.Size(222, 50);
            this.xrTableCell7.Text = "20. Tên hàng, quy cách phẩm chất";
            this.xrTableCell7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell8.Location = new System.Drawing.Point(247, 0);
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell8.ParentStyleUsing.UseBorders = false;
            this.xrTableCell8.Size = new System.Drawing.Size(94, 50);
            this.xrTableCell8.Text = "21. Mã số hàng hóa";
            this.xrTableCell8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell2.Location = new System.Drawing.Point(341, 0);
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell2.ParentStyleUsing.UseBorders = false;
            this.xrTableCell2.Size = new System.Drawing.Size(75, 50);
            this.xrTableCell2.Text = "22. Xuất xứ";
            this.xrTableCell2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell4.Location = new System.Drawing.Point(416, 0);
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell4.ParentStyleUsing.UseBorders = false;
            this.xrTableCell4.Size = new System.Drawing.Size(83, 50);
            this.xrTableCell4.Text = "23. Số lượng";
            this.xrTableCell4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell5.Location = new System.Drawing.Point(499, 0);
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell5.ParentStyleUsing.UseBorders = false;
            this.xrTableCell5.Size = new System.Drawing.Size(75, 50);
            this.xrTableCell5.Text = "24. Đơn vị tính";
            this.xrTableCell5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell6.Location = new System.Drawing.Point(574, 0);
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell6.ParentStyleUsing.UseBorders = false;
            this.xrTableCell6.Size = new System.Drawing.Size(81, 50);
            this.xrTableCell6.Text = "25. Đơn giá nguyên tệ";
            this.xrTableCell6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableCell3.Location = new System.Drawing.Point(655, 0);
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell3.ParentStyleUsing.UseBorders = false;
            this.xrTableCell3.Size = new System.Drawing.Size(78, 50);
            this.xrTableCell3.Text = "26. Trị giá nguyên tệ";
            this.xrTableCell3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrBarCode1
            // 
            this.xrBarCode1.Font = new System.Drawing.Font("Times New Roman", 18F);
            this.xrBarCode1.Location = new System.Drawing.Point(550, 17);
            this.xrBarCode1.Name = "xrBarCode1";
            this.xrBarCode1.PaddingInfo = new DevExpress.XtraPrinting.PaddingInfo(10, 10, 0, 0, 100F);
            this.xrBarCode1.ParentStyleUsing.UseFont = false;
            this.xrBarCode1.Size = new System.Drawing.Size(117, 33);
            this.xrBarCode1.Symbology = code128Generator4;
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.xrLabel3.Location = new System.Drawing.Point(508, 58);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.ParentStyleUsing.UseFont = false;
            this.xrLabel3.Size = new System.Drawing.Size(125, 23);
            this.xrLabel3.Text = "HQ/2009-TKDTNK";
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 16F, System.Drawing.FontStyle.Bold);
            this.xrLabel1.Location = new System.Drawing.Point(167, 17);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.ParentStyleUsing.UseFont = false;
            this.xrLabel1.Size = new System.Drawing.Size(325, 33);
            this.xrLabel1.Text = "TỜ KHAI HẢI QUAN ĐIỆN TỬ";
            // 
            // PageFooter
            // 
            this.PageFooter.Height = 95;
            this.PageFooter.Name = "PageFooter";
            // 
            // ToKhaiHQDT
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.PageHeader,
            this.PageFooter});
            this.Margins = new System.Drawing.Printing.Margins(80, 35, 100, 100);
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.PageFooterBand PageFooter;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel xrLabel4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel6;
        private DevExpress.XtraReports.UI.XRLine xrLine1;
        private DevExpress.XtraReports.UI.XRBarCode xrBarCode1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel10;
        private DevExpress.XtraReports.UI.XRLine xrLine3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLine xrLine5;
        private DevExpress.XtraReports.UI.XRLine xrLine4;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLine xrLine10;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLine xrLine12;
        private DevExpress.XtraReports.UI.XRLine xrLine11;
        private DevExpress.XtraReports.UI.XRLine xrLine13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel21;
        private DevExpress.XtraReports.UI.XRLabel xrLabel20;
        private DevExpress.XtraReports.UI.XRLabel xrLabel19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel28;
        private DevExpress.XtraReports.UI.XRLabel xrLabel27;
        private DevExpress.XtraReports.UI.XRLabel xrLabel26;
        private DevExpress.XtraReports.UI.XRLabel xrLabel25;
        private DevExpress.XtraReports.UI.XRLine xrLine14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel24;
        private DevExpress.XtraReports.UI.XRLabel xrLabel23;
        private DevExpress.XtraReports.UI.XRLabel xrLabel22;
        private DevExpress.XtraReports.UI.XRLine xrLine16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel30;
        private DevExpress.XtraReports.UI.XRLabel xrLabel29;
        private DevExpress.XtraReports.UI.XRLine xrLine15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel34;
        private DevExpress.XtraReports.UI.XRLabel xrLabel33;
        private DevExpress.XtraReports.UI.XRLine xrLine18;
        private DevExpress.XtraReports.UI.XRLine xrLine17;
        private DevExpress.XtraReports.UI.XRLabel xrLabel32;
        private DevExpress.XtraReports.UI.XRLabel xrLabel31;
        private DevExpress.XtraReports.UI.XRLine xrLine19;
        private DevExpress.XtraReports.UI.XRLabel xrLabel36;
        private DevExpress.XtraReports.UI.XRLabel xrLabel35;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRPanel xrPanel1;
        private DevExpress.XtraReports.UI.XRShape xrShape1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell lblSTT;
        private DevExpress.XtraReports.UI.XRTableCell lblTenHang;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSoHH;
        private DevExpress.XtraReports.UI.XRTableCell lblXuatXu;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong;
        private DevExpress.XtraReports.UI.XRTableCell lblDonViTinh;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaNT;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaNT;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHQCK;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHQ;
        private DevExpress.XtraReports.UI.XRLabel lblThamChieu;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGui;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiUyThac;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiNK;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiXK;
        private DevExpress.XtraReports.UI.XRLabel lblToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHetHanHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHetHanGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHoaDon;
        private DevExpress.XtraReports.UI.XRLabel lblHoaDonThuongMai;
        private DevExpress.XtraReports.UI.XRLabel lblLoaiHinh;
        private DevExpress.XtraReports.UI.XRLabel lblCangDoHang;
        private DevExpress.XtraReports.UI.XRLabel lblCangXepHang;
        private DevExpress.XtraReports.UI.XRLabel lblNgayVanTaiDon;
        private DevExpress.XtraReports.UI.XRLabel lblVanTaiDon;
        private DevExpress.XtraReports.UI.XRLabel lblPhuongThucThanhToan;
        private DevExpress.XtraReports.UI.XRLabel lblDieuKienGiaoHang;
        private DevExpress.XtraReports.UI.XRLabel lblNuocXK;
        private DevExpress.XtraReports.UI.XRLabel lblPhuongTienVanTai;
        private DevExpress.XtraReports.UI.XRLabel lblTyGiaTinhThue;
        private DevExpress.XtraReports.UI.XRLabel lblDongTienThanhToan;
        private DevExpress.XtraReports.UI.XRLabel lblHuongDan;
        private DevExpress.XtraReports.UI.XRLabel lblChungTu;
    }
}
