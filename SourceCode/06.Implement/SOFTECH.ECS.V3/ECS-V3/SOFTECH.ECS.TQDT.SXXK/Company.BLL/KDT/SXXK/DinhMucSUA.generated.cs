using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.KDT.SXXK
{
	public partial class DinhMucSUA
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaSanPham { set; get; }
		public string MaNguyenPhuLieu { set; get; }
		public string DVT_ID { set; get; }
		public decimal DinhMucSuDung { set; get; }
		public decimal TyLeHaoHut { set; get; }
		public string GhiChu { set; get; }
		public int STTHang { set; get; }
		public long Master_IDSUA { set; get; }
		public long IDDM { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static DinhMucSUA Load(long id)
		{
			const string spName = "[dbo].[p_KDT_SXXK_DinhMucSUA_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
			DinhMucSUA entity = null;
            IDataReader reader = db.ExecuteReader(dbCommand);
			if (reader.Read())
			{
				entity = new DinhMucSUA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_IDSUA"))) entity.Master_IDSUA = reader.GetInt64(reader.GetOrdinal("Master_IDSUA"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDDM"))) entity.IDDM = reader.GetInt64(reader.GetOrdinal("IDDM"));
			}
			reader.Close();
			return entity;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<DinhMucSUA> SelectCollectionAll()
		{
			List<DinhMucSUA> collection = new List<DinhMucSUA>();
			SqlDataReader reader = (SqlDataReader) SelectReaderAll();
			while (reader.Read())
			{
				DinhMucSUA entity = new DinhMucSUA();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_IDSUA"))) entity.Master_IDSUA = reader.GetInt64(reader.GetOrdinal("Master_IDSUA"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDDM"))) entity.IDDM = reader.GetInt64(reader.GetOrdinal("IDDM"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<DinhMucSUA> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			List<DinhMucSUA> collection = new List<DinhMucSUA>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
				DinhMucSUA entity = new DinhMucSUA();
				
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_IDSUA"))) entity.Master_IDSUA = reader.GetInt64(reader.GetOrdinal("Master_IDSUA"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDDM"))) entity.IDDM = reader.GetInt64(reader.GetOrdinal("IDDM"));
				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<DinhMucSUA> SelectCollectionBy_Master_IDSUA(long master_IDSUA)
		{
			List<DinhMucSUA> collection = new List<DinhMucSUA>();
            SqlDataReader reader = (SqlDataReader) SelectReaderBy_Master_IDSUA(master_IDSUA);
			while (reader.Read())
			{
				DinhMucSUA entity = new DinhMucSUA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSanPham"))) entity.MaSanPham = reader.GetString(reader.GetOrdinal("MaSanPham"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Master_IDSUA"))) entity.Master_IDSUA = reader.GetInt64(reader.GetOrdinal("Master_IDSUA"));
				if (!reader.IsDBNull(reader.GetOrdinal("IDDM"))) entity.IDDM = reader.GetInt64(reader.GetOrdinal("IDDM"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_Master_IDSUA(long master_IDSUA)
		{
			const string spName = "[dbo].[p_KDT_SXXK_DinhMucSUA_SelectBy_Master_IDSUA]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_IDSUA", SqlDbType.BigInt, master_IDSUA);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_DinhMucSUA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_DinhMucSUA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_SXXK_DinhMucSUA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_SXXK_DinhMucSUA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_Master_IDSUA(long master_IDSUA)
		{
			const string spName = "p_KDT_SXXK_DinhMucSUA_SelectBy_Master_IDSUA";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_IDSUA", SqlDbType.BigInt, master_IDSUA);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertDinhMucSUA(string maSanPham, string maNguyenPhuLieu, string dVT_ID, decimal dinhMucSuDung, decimal tyLeHaoHut, string ghiChu, int sTTHang, long master_IDSUA, long iDDM)
		{
			DinhMucSUA entity = new DinhMucSUA();	
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DVT_ID = dVT_ID;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.GhiChu = ghiChu;
			entity.STTHang = sTTHang;
			entity.Master_IDSUA = master_IDSUA;
			entity.IDDM = iDDM;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_SXXK_DinhMucSUA_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@Master_IDSUA", SqlDbType.BigInt, Master_IDSUA);
			db.AddInParameter(dbCommand, "@IDDM", SqlDbType.BigInt, IDDM);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<DinhMucSUA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DinhMucSUA item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateDinhMucSUA(long id, string maSanPham, string maNguyenPhuLieu, string dVT_ID, decimal dinhMucSuDung, decimal tyLeHaoHut, string ghiChu, int sTTHang, long master_IDSUA, long iDDM)
		{
			DinhMucSUA entity = new DinhMucSUA();			
			entity.ID = id;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DVT_ID = dVT_ID;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.GhiChu = ghiChu;
			entity.STTHang = sTTHang;
			entity.Master_IDSUA = master_IDSUA;
			entity.IDDM = iDDM;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_SXXK_DinhMucSUA_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@Master_IDSUA", SqlDbType.BigInt, Master_IDSUA);
			db.AddInParameter(dbCommand, "@IDDM", SqlDbType.BigInt, IDDM);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<DinhMucSUA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DinhMucSUA item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateDinhMucSUA(long id, string maSanPham, string maNguyenPhuLieu, string dVT_ID, decimal dinhMucSuDung, decimal tyLeHaoHut, string ghiChu, int sTTHang, long master_IDSUA, long iDDM)
		{
			DinhMucSUA entity = new DinhMucSUA();			
			entity.ID = id;
			entity.MaSanPham = maSanPham;
			entity.MaNguyenPhuLieu = maNguyenPhuLieu;
			entity.DVT_ID = dVT_ID;
			entity.DinhMucSuDung = dinhMucSuDung;
			entity.TyLeHaoHut = tyLeHaoHut;
			entity.GhiChu = ghiChu;
			entity.STTHang = sTTHang;
			entity.Master_IDSUA = master_IDSUA;
			entity.IDDM = iDDM;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_DinhMucSUA_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaSanPham", SqlDbType.VarChar, MaSanPham);
			db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNguyenPhuLieu);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, DVT_ID);
			db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
			db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.VarChar, GhiChu);
			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@Master_IDSUA", SqlDbType.BigInt, Master_IDSUA);
			db.AddInParameter(dbCommand, "@IDDM", SqlDbType.BigInt, IDDM);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<DinhMucSUA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DinhMucSUA item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteDinhMucSUA(long id)
		{
			DinhMucSUA entity = new DinhMucSUA();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_SXXK_DinhMucSUA_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_Master_IDSUA(long master_IDSUA)
		{
			const string spName = "[dbo].[p_KDT_SXXK_DinhMucSUA_DeleteBy_Master_IDSUA]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@Master_IDSUA", SqlDbType.BigInt, master_IDSUA);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_SXXK_DinhMucSUA_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<DinhMucSUA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (DinhMucSUA item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
	}	
}