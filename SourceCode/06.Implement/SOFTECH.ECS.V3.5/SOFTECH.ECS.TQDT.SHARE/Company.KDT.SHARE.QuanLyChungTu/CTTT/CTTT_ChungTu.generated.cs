using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu.CTTT
{
	public partial class CTTT_ChungTu : ICloneable
	{
		#region Properties.
		

		public int ID { set; get; }
		public string LoaiChungTu { set; get; }
		public int LanThanhLy { set; get; }
		public string SoChungTu { set; get; }
		public DateTime NgayChungTu { set; get; }
		public string HinhThucThanhToan { set; get; }
		public decimal TongTriGia { set; get; }
		public decimal ConLai { set; get; }
		public string GhiChu { set; get; }
		public string DongTienThanhToan { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<CTTT_ChungTu> ConvertToCollection(IDataReader reader)
		{
			List<CTTT_ChungTu> collection = new List<CTTT_ChungTu>();
			while (reader.Read())
			{
				CTTT_ChungTu entity = new CTTT_ChungTu();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiChungTu"))) entity.LoaiChungTu = reader.GetString(reader.GetOrdinal("LoaiChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetString(reader.GetOrdinal("SoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayChungTu"))) entity.NgayChungTu = reader.GetDateTime(reader.GetOrdinal("NgayChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("HinhThucThanhToan"))) entity.HinhThucThanhToan = reader.GetString(reader.GetOrdinal("HinhThucThanhToan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGia"))) entity.TongTriGia = reader.GetDecimal(reader.GetOrdinal("TongTriGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("ConLai"))) entity.ConLai = reader.GetDecimal(reader.GetOrdinal("ConLai"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DongTienThanhToan"))) entity.DongTienThanhToan = reader.GetString(reader.GetOrdinal("DongTienThanhToan"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<CTTT_ChungTu> collection, int id)
        {
            foreach (CTTT_ChungTu item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_CTTT_ChungTu VALUES(@LoaiChungTu, @LanThanhLy, @SoChungTu, @NgayChungTu, @HinhThucThanhToan, @TongTriGia, @ConLai, @GhiChu, @DongTienThanhToan)";
            string update = "UPDATE t_CTTT_ChungTu SET LoaiChungTu = @LoaiChungTu, LanThanhLy = @LanThanhLy, SoChungTu = @SoChungTu, NgayChungTu = @NgayChungTu, HinhThucThanhToan = @HinhThucThanhToan, TongTriGia = @TongTriGia, ConLai = @ConLai, GhiChu = @GhiChu, DongTienThanhToan = @DongTienThanhToan WHERE ID = @ID";
            string delete = "DELETE FROM t_CTTT_ChungTu WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HinhThucThanhToan", SqlDbType.VarChar, "HinhThucThanhToan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGia", SqlDbType.Decimal, "TongTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ConLai", SqlDbType.Decimal, "ConLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DongTienThanhToan", SqlDbType.VarChar, "DongTienThanhToan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HinhThucThanhToan", SqlDbType.VarChar, "HinhThucThanhToan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGia", SqlDbType.Decimal, "TongTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ConLai", SqlDbType.Decimal, "ConLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DongTienThanhToan", SqlDbType.VarChar, "DongTienThanhToan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_CTTT_ChungTu VALUES(@LoaiChungTu, @LanThanhLy, @SoChungTu, @NgayChungTu, @HinhThucThanhToan, @TongTriGia, @ConLai, @GhiChu, @DongTienThanhToan)";
            string update = "UPDATE t_CTTT_ChungTu SET LoaiChungTu = @LoaiChungTu, LanThanhLy = @LanThanhLy, SoChungTu = @SoChungTu, NgayChungTu = @NgayChungTu, HinhThucThanhToan = @HinhThucThanhToan, TongTriGia = @TongTriGia, ConLai = @ConLai, GhiChu = @GhiChu, DongTienThanhToan = @DongTienThanhToan WHERE ID = @ID";
            string delete = "DELETE FROM t_CTTT_ChungTu WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HinhThucThanhToan", SqlDbType.VarChar, "HinhThucThanhToan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGia", SqlDbType.Decimal, "TongTriGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ConLai", SqlDbType.Decimal, "ConLai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DongTienThanhToan", SqlDbType.VarChar, "DongTienThanhToan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.VarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LanThanhLy", SqlDbType.Int, "LanThanhLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayChungTu", SqlDbType.DateTime, "NgayChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HinhThucThanhToan", SqlDbType.VarChar, "HinhThucThanhToan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGia", SqlDbType.Decimal, "TongTriGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ConLai", SqlDbType.Decimal, "ConLai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DongTienThanhToan", SqlDbType.VarChar, "DongTienThanhToan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static CTTT_ChungTu Load(int id)
		{
			const string spName = "[dbo].[p_CTTT_ChungTu_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<CTTT_ChungTu> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<CTTT_ChungTu> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<CTTT_ChungTu> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_CTTT_ChungTu_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_CTTT_ChungTu_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_CTTT_ChungTu_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_CTTT_ChungTu_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertCTTT_ChungTu(string loaiChungTu, int lanThanhLy, string soChungTu, DateTime ngayChungTu, string hinhThucThanhToan, decimal tongTriGia, decimal conLai, string ghiChu, string dongTienThanhToan)
		{
			CTTT_ChungTu entity = new CTTT_ChungTu();	
			entity.LoaiChungTu = loaiChungTu;
			entity.LanThanhLy = lanThanhLy;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.HinhThucThanhToan = hinhThucThanhToan;
			entity.TongTriGia = tongTriGia;
			entity.ConLai = conLai;
			entity.GhiChu = ghiChu;
			entity.DongTienThanhToan = dongTienThanhToan;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_CTTT_ChungTu_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@HinhThucThanhToan", SqlDbType.VarChar, HinhThucThanhToan);
			db.AddInParameter(dbCommand, "@TongTriGia", SqlDbType.Decimal, TongTriGia);
			db.AddInParameter(dbCommand, "@ConLai", SqlDbType.Decimal, ConLai);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@DongTienThanhToan", SqlDbType.VarChar, DongTienThanhToan);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<CTTT_ChungTu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CTTT_ChungTu item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateCTTT_ChungTu(int id, string loaiChungTu, int lanThanhLy, string soChungTu, DateTime ngayChungTu, string hinhThucThanhToan, decimal tongTriGia, decimal conLai, string ghiChu, string dongTienThanhToan)
		{
			CTTT_ChungTu entity = new CTTT_ChungTu();			
			entity.ID = id;
			entity.LoaiChungTu = loaiChungTu;
			entity.LanThanhLy = lanThanhLy;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.HinhThucThanhToan = hinhThucThanhToan;
			entity.TongTriGia = tongTriGia;
			entity.ConLai = conLai;
			entity.GhiChu = ghiChu;
			entity.DongTienThanhToan = dongTienThanhToan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_CTTT_ChungTu_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@HinhThucThanhToan", SqlDbType.VarChar, HinhThucThanhToan);
			db.AddInParameter(dbCommand, "@TongTriGia", SqlDbType.Decimal, TongTriGia);
			db.AddInParameter(dbCommand, "@ConLai", SqlDbType.Decimal, ConLai);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@DongTienThanhToan", SqlDbType.VarChar, DongTienThanhToan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<CTTT_ChungTu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CTTT_ChungTu item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateCTTT_ChungTu(int id, string loaiChungTu, int lanThanhLy, string soChungTu, DateTime ngayChungTu, string hinhThucThanhToan, decimal tongTriGia, decimal conLai, string ghiChu, string dongTienThanhToan)
		{
			CTTT_ChungTu entity = new CTTT_ChungTu();			
			entity.ID = id;
			entity.LoaiChungTu = loaiChungTu;
			entity.LanThanhLy = lanThanhLy;
			entity.SoChungTu = soChungTu;
			entity.NgayChungTu = ngayChungTu;
			entity.HinhThucThanhToan = hinhThucThanhToan;
			entity.TongTriGia = tongTriGia;
			entity.ConLai = conLai;
			entity.GhiChu = ghiChu;
			entity.DongTienThanhToan = dongTienThanhToan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_CTTT_ChungTu_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.VarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, LanThanhLy);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@NgayChungTu", SqlDbType.DateTime, NgayChungTu.Year <= 1753 ? DBNull.Value : (object) NgayChungTu);
			db.AddInParameter(dbCommand, "@HinhThucThanhToan", SqlDbType.VarChar, HinhThucThanhToan);
			db.AddInParameter(dbCommand, "@TongTriGia", SqlDbType.Decimal, TongTriGia);
			db.AddInParameter(dbCommand, "@ConLai", SqlDbType.Decimal, ConLai);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@DongTienThanhToan", SqlDbType.VarChar, DongTienThanhToan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<CTTT_ChungTu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CTTT_ChungTu item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteCTTT_ChungTu(int id)
		{
			CTTT_ChungTu entity = new CTTT_ChungTu();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_CTTT_ChungTu_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_CTTT_ChungTu_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<CTTT_ChungTu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (CTTT_ChungTu item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}