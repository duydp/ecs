﻿namespace Company.Interface.GC.WareHouse
{
    partial class WareHouseUpdateQuantityForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem3 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem4 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.GridEX.GridEXLayout cbbMaKho_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WareHouseUpdateQuantityForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbLoaiHang = new Janus.Windows.EditControls.UIComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbbMaNT = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label25 = new System.Windows.Forms.Label();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txtTenKho = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtThueXNK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label23 = new System.Windows.Forms.Label();
            this.cbbMaKho = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.txtTriGiaVND = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtMaHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDonGiaTT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtDonViTinh = new Janus.Windows.EditControls.UIComboBox();
            this.txtTyGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDinhKhoan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaKho)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 697), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 697);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 673);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 673);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1026, 697);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 657);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1026, 40);
            this.uiGroupBox2.TabIndex = 4;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(937, 11);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(80, 23);
            this.btnClose.TabIndex = 31;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.cbbMaNT);
            this.uiGroupBox1.Controls.Add(this.label25);
            this.uiGroupBox1.Controls.Add(this.btnSave);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.txtTenKho);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.txtThueXNK);
            this.uiGroupBox1.Controls.Add(this.label23);
            this.uiGroupBox1.Controls.Add(this.cbbMaKho);
            this.uiGroupBox1.Controls.Add(this.txtTriGiaVND);
            this.uiGroupBox1.Controls.Add(this.txtMaHang);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.txtDonGiaTT);
            this.uiGroupBox1.Controls.Add(this.txtDonViTinh);
            this.uiGroupBox1.Controls.Add(this.txtTyGia);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.txtDinhKhoan);
            this.uiGroupBox1.Controls.Add(this.txtGhiChu);
            this.uiGroupBox1.Controls.Add(this.txtTenHang);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.txtSoLuong);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1026, 347);
            this.uiGroupBox1.TabIndex = 3;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.cbbLoaiHang);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1020, 60);
            this.uiGroupBox4.TabIndex = 40;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbLoaiHang
            // 
            this.cbbLoaiHang.BackColor = System.Drawing.Color.White;
            this.cbbLoaiHang.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbbLoaiHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Nguyên phụ liệu";
            uiComboBoxItem1.Value = "1";
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Sản phẩm";
            uiComboBoxItem2.Value = "2";
            uiComboBoxItem3.FormatStyle.Alpha = 0;
            uiComboBoxItem3.IsSeparator = false;
            uiComboBoxItem3.Text = "Thiết bị";
            uiComboBoxItem3.Value = "3";
            uiComboBoxItem4.FormatStyle.Alpha = 0;
            uiComboBoxItem4.IsSeparator = false;
            uiComboBoxItem4.Text = "Hàng mẫu";
            uiComboBoxItem4.Value = "4";
            this.cbbLoaiHang.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2,
            uiComboBoxItem3,
            uiComboBoxItem4});
            this.cbbLoaiHang.Location = new System.Drawing.Point(121, 22);
            this.cbbLoaiHang.Name = "cbbLoaiHang";
            this.cbbLoaiHang.Size = new System.Drawing.Size(199, 21);
            this.cbbLoaiHang.TabIndex = 47;
            this.cbbLoaiHang.ValueMember = "ID";
            this.cbbLoaiHang.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbbLoaiHang.SelectedValueChanged += new System.EventHandler(this.cbbLoaiHang_SelectedValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(15, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(60, 13);
            this.label8.TabIndex = 43;
            this.label8.Text = "Loại hàng :";
            // 
            // cbbMaNT
            // 
            this.cbbMaNT.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.cbbMaNT.Appearance.Options.UseBackColor = true;
            this.cbbMaNT.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A527;
            this.cbbMaNT.Code = "";
            this.cbbMaNT.ColorControl = System.Drawing.Color.Empty;
            this.cbbMaNT.Cursor = System.Windows.Forms.Cursors.Default;
            this.cbbMaNT.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.cbbMaNT.IsOnlyWarning = false;
            this.cbbMaNT.IsValidate = true;
            this.cbbMaNT.Location = new System.Drawing.Point(869, 178);
            this.cbbMaNT.Name = "cbbMaNT";
            this.cbbMaNT.Name_VN = "";
            this.cbbMaNT.SetOnlyWarning = false;
            this.cbbMaNT.SetValidate = false;
            this.cbbMaNT.ShowColumnCode = true;
            this.cbbMaNT.ShowColumnName = false;
            this.cbbMaNT.Size = new System.Drawing.Size(143, 21);
            this.cbbMaNT.TabIndex = 31;
            this.cbbMaNT.TagName = "";
            this.cbbMaNT.Where = null;
            this.cbbMaNT.WhereCondition = "";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(774, 182);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 13);
            this.label25.TabIndex = 30;
            this.label25.Text = "Mã nguyên tệ :";
            // 
            // btnSave
            // 
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(124, 309);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(80, 23);
            this.btnSave.TabIndex = 29;
            this.btnSave.Text = "Ghi";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(299, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 19;
            this.label3.Text = "Đơn giá :";
            // 
            // txtTenKho
            // 
            this.txtTenKho.BackColor = System.Drawing.Color.White;
            this.txtTenKho.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenKho.Location = new System.Drawing.Point(634, 124);
            this.txtTenKho.Name = "txtTenKho";
            this.txtTenKho.ReadOnly = true;
            this.txtTenKho.Size = new System.Drawing.Size(378, 21);
            this.txtTenKho.TabIndex = 25;
            this.txtTenKho.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenKho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(525, 182);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(46, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "Tỷ giá : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(525, 267);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Định khoản :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(18, 267);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 23;
            this.label10.Text = "Ghi chú :";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(525, 129);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 13);
            this.label5.TabIndex = 23;
            this.label5.Text = "Tên kho :";
            // 
            // txtThueXNK
            // 
            this.txtThueXNK.DecimalDigits = 4;
            this.txtThueXNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueXNK.Location = new System.Drawing.Point(634, 214);
            this.txtThueXNK.Name = "txtThueXNK";
            this.txtThueXNK.Size = new System.Drawing.Size(128, 21);
            this.txtThueXNK.TabIndex = 27;
            this.txtThueXNK.Text = "0.0000";
            this.txtThueXNK.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtThueXNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtThueXNK.TextChanged += new System.EventHandler(this.txtThueXNK_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(525, 92);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(48, 13);
            this.label23.TabIndex = 22;
            this.label23.Text = "Mã kho :";
            // 
            // cbbMaKho
            // 
            cbbMaKho_DesignTimeLayout.LayoutString = resources.GetString("cbbMaKho_DesignTimeLayout.LayoutString");
            this.cbbMaKho.DesignTimeLayout = cbbMaKho_DesignTimeLayout;
            this.cbbMaKho.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbMaKho.Location = new System.Drawing.Point(634, 88);
            this.cbbMaKho.Name = "cbbMaKho";
            this.cbbMaKho.SelectedIndex = -1;
            this.cbbMaKho.SelectedItem = null;
            this.cbbMaKho.Size = new System.Drawing.Size(378, 21);
            this.cbbMaKho.TabIndex = 24;
            this.cbbMaKho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaVND
            // 
            this.txtTriGiaVND.DecimalDigits = 4;
            this.txtTriGiaVND.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaVND.Location = new System.Drawing.Point(869, 214);
            this.txtTriGiaVND.Name = "txtTriGiaVND";
            this.txtTriGiaVND.ReadOnly = true;
            this.txtTriGiaVND.Size = new System.Drawing.Size(143, 21);
            this.txtTriGiaVND.TabIndex = 25;
            this.txtTriGiaVND.Text = "0.0000";
            this.txtTriGiaVND.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTriGiaVND.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHang
            // 
            this.txtMaHang.BackColor = System.Drawing.Color.White;
            this.txtMaHang.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtMaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHang.Location = new System.Drawing.Point(124, 88);
            this.txtMaHang.Name = "txtMaHang";
            this.txtMaHang.Size = new System.Drawing.Size(373, 21);
            this.txtMaHang.TabIndex = 7;
            this.txtMaHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaHang.ButtonClick += new System.EventHandler(this.txtMaHang_ButtonClick);
            this.txtMaHang.Leave += new System.EventHandler(this.txtMaHang_Leave);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(18, 218);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Số lượng :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(18, 182);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Đơn vị tính :";
            // 
            // txtDonGiaTT
            // 
            this.txtDonGiaTT.DecimalDigits = 4;
            this.txtDonGiaTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaTT.Location = new System.Drawing.Point(368, 214);
            this.txtDonGiaTT.Name = "txtDonGiaTT";
            this.txtDonGiaTT.Size = new System.Drawing.Size(129, 21);
            this.txtDonGiaTT.TabIndex = 22;
            this.txtDonGiaTT.Text = "0.0000";
            this.txtDonGiaTT.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtDonGiaTT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDonGiaTT.TextChanged += new System.EventHandler(this.txtDonGiaTT_TextChanged);
            // 
            // txtDonViTinh
            // 
            this.txtDonViTinh.BackColor = System.Drawing.Color.White;
            this.txtDonViTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonViTinh.Location = new System.Drawing.Point(124, 178);
            this.txtDonViTinh.Name = "txtDonViTinh";
            this.txtDonViTinh.Size = new System.Drawing.Size(160, 21);
            this.txtDonViTinh.TabIndex = 9;
            this.txtDonViTinh.ValueMember = "ID";
            this.txtDonViTinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtTyGia
            // 
            this.txtTyGia.DecimalDigits = 4;
            this.txtTyGia.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGia.Location = new System.Drawing.Point(634, 178);
            this.txtTyGia.Name = "txtTyGia";
            this.txtTyGia.Size = new System.Drawing.Size(127, 21);
            this.txtTyGia.TabIndex = 23;
            this.txtTyGia.Text = "1.0000";
            this.txtTyGia.Value = new decimal(new int[] {
            10000,
            0,
            0,
            262144});
            this.txtTyGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Mã hàng hóa :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(18, 129);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Tên hàng hóa :";
            // 
            // txtDinhKhoan
            // 
            this.txtDinhKhoan.BackColor = System.Drawing.Color.White;
            this.txtDinhKhoan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDinhKhoan.Location = new System.Drawing.Point(634, 251);
            this.txtDinhKhoan.Multiline = true;
            this.txtDinhKhoan.Name = "txtDinhKhoan";
            this.txtDinhKhoan.Size = new System.Drawing.Size(378, 45);
            this.txtDinhKhoan.TabIndex = 8;
            this.txtDinhKhoan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDinhKhoan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.BackColor = System.Drawing.Color.White;
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(124, 251);
            this.txtGhiChu.Multiline = true;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(356, 45);
            this.txtGhiChu.TabIndex = 8;
            this.txtGhiChu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenHang
            // 
            this.txtTenHang.BackColor = System.Drawing.Color.White;
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(124, 120);
            this.txtTenHang.Multiline = true;
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(373, 45);
            this.txtTenHang.TabIndex = 8;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(779, 218);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(67, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "Trị giá VNĐ :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(525, 218);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(63, 13);
            this.label20.TabIndex = 17;
            this.label20.Text = "Thuế XNK : ";
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.DecimalDigits = 4;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.Location = new System.Drawing.Point(124, 214);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Size = new System.Drawing.Size(160, 21);
            this.txtSoLuong.TabIndex = 21;
            this.txtSoLuong.Text = "1.0000";
            this.txtSoLuong.Value = new decimal(new int[] {
            10000,
            0,
            0,
            262144});
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuong.TextChanged += new System.EventHandler(this.txtSoLuong_TextChanged);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.dgList);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 347);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1026, 310);
            this.uiGroupBox3.TabIndex = 5;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 4;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Location = new System.Drawing.Point(3, 8);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelection;
            this.dgList.Size = new System.Drawing.Size(1020, 299);
            this.dgList.TabIndex = 91;
            this.dgList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // WareHouseUpdateQuantityForm
            // 
            this.ClientSize = new System.Drawing.Size(1232, 703);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "WareHouseUpdateQuantityForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cập nhật tồn đầu kỳ";
            this.Load += new System.EventHandler(this.WareHouseUpdateQuantityForm_Load);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaKho)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenKho;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbMaKho;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHang;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIComboBox txtDonViTinh;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtThueXNK;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTriGiaVND;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDonGiaTT;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuong;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTyGia;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnSave;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtDinhKhoan;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory cbbMaNT;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIComboBox cbbLoaiHang;
        private System.Windows.Forms.Label label8;
    }
}
