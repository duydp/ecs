﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAE2LF0_HANG : BasicVNACC
    {
        public static int TongSoByte { get; set; }
        public PropertiesAttribute R01 { get; set; }
        public PropertiesAttribute CMD { get; set; }
        public PropertiesAttribute COC { get; set; }
        public PropertiesAttribute R03 { get; set; }
        public PropertiesAttribute CMN { get; set; }
        public PropertiesAttribute QN1 { get; set; }
        public PropertiesAttribute QT1 { get; set; }
        public PropertiesAttribute QN2 { get; set; }
        public PropertiesAttribute QT2 { get; set; }
        public PropertiesAttribute BPR { get; set; }
        public PropertiesAttribute UPR { get; set; }
        public PropertiesAttribute UPC { get; set; }
        public PropertiesAttribute TSC { get; set; }
        public PropertiesAttribute R07 { get; set; }
        public PropertiesAttribute AD9 { get; set; }
        public PropertiesAttribute R14 { get; set; }
        public PropertiesAttribute R15 { get; set; }
        public PropertiesAttribute TSQ { get; set; }
        public PropertiesAttribute TSU { get; set; }
        public PropertiesAttribute CVU { get; set; }
        public PropertiesAttribute ADA { get; set; }
        public PropertiesAttribute QCV { get; set; }
        public PropertiesAttribute TRA { get; set; }
        public PropertiesAttribute TRM { get; set; }
        public PropertiesAttribute TAX { get; set; }
        public PropertiesAttribute ADB { get; set; }
        public PropertiesAttribute REG { get; set; }
        public PropertiesAttribute ADC { get; set; }
        public PropertiesAttribute TDL { get; set; }
        public PropertiesAttribute TXN { get; set; }
        public PropertiesAttribute TXR { get; set; }
        public PropertiesAttribute CUP { get; set; }
        public PropertiesAttribute IUP { get; set; }
        public PropertiesAttribute CQU { get; set; }
        public PropertiesAttribute CQC { get; set; }
        public PropertiesAttribute IQU { get; set; }
        public PropertiesAttribute IQC { get; set; }
        public PropertiesAttribute CPR { get; set; }
        public PropertiesAttribute IPR { get; set; }
        public GroupAttribute OL_ { get; set; }
        public PropertiesAttribute RE { get; set; }
        public PropertiesAttribute TRL { get; set; }

        public VAE2LF0_HANG()
        {
            R01 = new PropertiesAttribute(2, typeof(string));
            CMD = new PropertiesAttribute(12, typeof(string));
            COC = new PropertiesAttribute(7, typeof(string));
            R03 = new PropertiesAttribute(1, typeof(string));
            CMN = new PropertiesAttribute(600, typeof(string));
            QN1 = new PropertiesAttribute(12, typeof(int));
            QT1 = new PropertiesAttribute(4, typeof(string));
            QN2 = new PropertiesAttribute(12, typeof(int));
            QT2 = new PropertiesAttribute(4, typeof(string));
            BPR = new PropertiesAttribute(20, typeof(int));
            UPR = new PropertiesAttribute(9, typeof(int));
            UPC = new PropertiesAttribute(3, typeof(string));
            TSC = new PropertiesAttribute(4, typeof(string));
            R07 = new PropertiesAttribute(17, typeof(int));
            AD9 = new PropertiesAttribute(3, typeof(string));
            R14 = new PropertiesAttribute(3, typeof(string));
            R15 = new PropertiesAttribute(20, typeof(int));
            TSQ = new PropertiesAttribute(12, typeof(int));
            TSU = new PropertiesAttribute(4, typeof(string));
            CVU = new PropertiesAttribute(18, typeof(int));
            ADA = new PropertiesAttribute(3, typeof(string));
            QCV = new PropertiesAttribute(4, typeof(string));
            TRA = new PropertiesAttribute(30, typeof(string));
            TRM = new PropertiesAttribute(1, typeof(string));
            TAX = new PropertiesAttribute(16, typeof(int));
            ADB = new PropertiesAttribute(3, typeof(string));
            REG = new PropertiesAttribute(16, typeof(int));
            ADC = new PropertiesAttribute(3, typeof(string));
            TDL = new PropertiesAttribute(2, typeof(string));
            TXN = new PropertiesAttribute(12, typeof(int));
            TXR = new PropertiesAttribute(3, typeof(string));
            CUP = new PropertiesAttribute(21, typeof(string));
            IUP = new PropertiesAttribute(21, typeof(string));
            CQU = new PropertiesAttribute(12, typeof(int));
            CQC = new PropertiesAttribute(4, typeof(string));
            IQU = new PropertiesAttribute(12, typeof(int));
            IQC = new PropertiesAttribute(4, typeof(string));
            CPR = new PropertiesAttribute(16, typeof(int));
            IPR = new PropertiesAttribute(16, typeof(int));
            RE = new PropertiesAttribute(5, typeof(string));
            TRL = new PropertiesAttribute(60, typeof(string));
            #region OL_
            List<PropertiesAttribute> listOL_ = new List<PropertiesAttribute>();
            listOL_.Add(new PropertiesAttribute(2, 5, EnumGroupID.VAE2LF0_HANG_PL_, typeof(string)));
            OL_ = new GroupAttribute("OL_", 5, listOL_);
            #endregion OL_
            TongSoByte = 1133;
        }

    }
    public partial class EnumGroupID
    {
        public static readonly string VAE2LF0_HANG_PL_ = "VAE2LF0_HANG_PL_";
    }

}
