﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using Company.KDT.SHARE.Components.Nodes.GC;
using System.Linq;

namespace SOFTECH.ECS.V3.Components.GC
{
    public partial class PK_GiaHanHopDong
    {
        /// <summary>
        /// ConfigPK_GiaHanHopDong
        /// </summary>
        /// <param name="PKDK">PhuKienDangKy</param>
        /// <param name="issuer">int</param>
        /// <param name="function">int</param>
        /// <param name="status">int</param>
        /// <returns>string</returns>
        public static string ConfigPK_GiaHanHopDong(PhuKienDangKy PKDK, int issuer, int function, int status)
        {
            try
            {
                XDocument docXML = XDocument.Load("../../../SOFTECH.ECS.V3.Components/XML/GC/PK_GiaHanHopDong.xml");
                HopDong HD = HopDong.Load(PKDK.HopDong_ID);
                if (HD != null && docXML != null)
                {                    
                    XElement Root = docXML.Root;
                    PhuKienChung.ConfigPhuKien(Root, PKDK, issuer, function, status);
                    XElement AdditionalInformation = Root.Element(NodePhuKien.AdditionalInformation);
                    XElement Content = AdditionalInformation.Element(NodePhuKien.Content);
                    if (Root != null && Root.Elements().Count() > 0)
                    {
                        foreach (XElement element in Root.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.AdditionalInformation.ToUpper()))
                            {
                                AdditionalInformation = element;
                                foreach (XElement elementChild in element.Elements())
                                {
                                    if (elementChild.Name.ToString().ToUpper().Equals(NodePhuKien.Content.ToUpper()))
                                    {
                                        Content = elementChild;                                       
                                    }
                                }
                            }
                        }
                        foreach (XElement element in Content.Elements())
                        {
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.oldExpire.ToUpper()))
                                element.SetValue(HD.NgayHetHan);
                            if (element.Name.ToString().ToUpper().Equals(NodePhuKien.newExpire.ToUpper()))
                                element.SetValue(HD.NgayGiaHan);                            
                        }
                        docXML.Save(@"D:\PK_GiaHanHopDong.xml");
                        return docXML.ToString();
                    }
                }
                return "";
            }
            catch
            {
                return "";
            }
        }
    }
}
