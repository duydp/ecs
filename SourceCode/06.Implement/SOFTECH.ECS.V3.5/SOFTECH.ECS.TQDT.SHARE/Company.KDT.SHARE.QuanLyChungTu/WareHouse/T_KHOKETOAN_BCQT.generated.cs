﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.QuanLyChungTu.WareHouse
{
	public partial class T_KHOKETOAN_BCQT : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public int STT { set; get; }
		public decimal LOAIHANGHOA { set; get; }
		public decimal TAIKHOAN { set; get; }
		public string TENHANGHOA { set; get; }
		public string MAHANGHOA { set; get; }
		public string DVT { set; get; }
		public decimal LUONGTONDAU { set; get; }
		public decimal TRIGIATONDAU { set; get; }
		public decimal LUONGNHAP { set; get; }
		public decimal TRIGIANHAP { set; get; }
		public decimal LUONGXUAT { set; get; }
		public decimal TRIGIAXUAT { set; get; }
		public decimal LUONGTONCUOI { set; get; }
		public decimal TRIGIATONCUOI { set; get; }
		public string GHICHU { set; get; }
		public long HOPDONG_ID { set; get; }
		public DateTime TUNGAY { set; get; }
		public DateTime DENNGAY { set; get; }
		public string MAKHO { set; get; }
		public string TENKHO { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KHOKETOAN_BCQT> ConvertToCollection(IDataReader reader)
		{
			List<T_KHOKETOAN_BCQT> collection = new List<T_KHOKETOAN_BCQT>();
			while (reader.Read())
			{
				T_KHOKETOAN_BCQT entity = new T_KHOKETOAN_BCQT();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LOAIHANGHOA"))) entity.LOAIHANGHOA = reader.GetDecimal(reader.GetOrdinal("LOAIHANGHOA"));
				if (!reader.IsDBNull(reader.GetOrdinal("TAIKHOAN"))) entity.TAIKHOAN = reader.GetDecimal(reader.GetOrdinal("TAIKHOAN"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENHANGHOA"))) entity.TENHANGHOA = reader.GetString(reader.GetOrdinal("TENHANGHOA"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAHANGHOA"))) entity.MAHANGHOA = reader.GetString(reader.GetOrdinal("MAHANGHOA"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGTONDAU"))) entity.LUONGTONDAU = reader.GetDecimal(reader.GetOrdinal("LUONGTONDAU"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIATONDAU"))) entity.TRIGIATONDAU = reader.GetDecimal(reader.GetOrdinal("TRIGIATONDAU"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGNHAP"))) entity.LUONGNHAP = reader.GetDecimal(reader.GetOrdinal("LUONGNHAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIANHAP"))) entity.TRIGIANHAP = reader.GetDecimal(reader.GetOrdinal("TRIGIANHAP"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGXUAT"))) entity.LUONGXUAT = reader.GetDecimal(reader.GetOrdinal("LUONGXUAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIAXUAT"))) entity.TRIGIAXUAT = reader.GetDecimal(reader.GetOrdinal("TRIGIAXUAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LUONGTONCUOI"))) entity.LUONGTONCUOI = reader.GetDecimal(reader.GetOrdinal("LUONGTONCUOI"));
				if (!reader.IsDBNull(reader.GetOrdinal("TRIGIATONCUOI"))) entity.TRIGIATONCUOI = reader.GetDecimal(reader.GetOrdinal("TRIGIATONCUOI"));
				if (!reader.IsDBNull(reader.GetOrdinal("GHICHU"))) entity.GHICHU = reader.GetString(reader.GetOrdinal("GHICHU"));
				if (!reader.IsDBNull(reader.GetOrdinal("HOPDONG_ID"))) entity.HOPDONG_ID = reader.GetInt64(reader.GetOrdinal("HOPDONG_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TUNGAY"))) entity.TUNGAY = reader.GetDateTime(reader.GetOrdinal("TUNGAY"));
				if (!reader.IsDBNull(reader.GetOrdinal("DENNGAY"))) entity.DENNGAY = reader.GetDateTime(reader.GetOrdinal("DENNGAY"));
				if (!reader.IsDBNull(reader.GetOrdinal("MAKHO"))) entity.MAKHO = reader.GetString(reader.GetOrdinal("MAKHO"));
				if (!reader.IsDBNull(reader.GetOrdinal("TENKHO"))) entity.TENKHO = reader.GetString(reader.GetOrdinal("TENKHO"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KHOKETOAN_BCQT> collection, long id)
        {
            foreach (T_KHOKETOAN_BCQT item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KHOKETOAN_BCQT VALUES(@STT, @LOAIHANGHOA, @TAIKHOAN, @TENHANGHOA, @MAHANGHOA, @DVT, @LUONGTONDAU, @TRIGIATONDAU, @LUONGNHAP, @TRIGIANHAP, @LUONGXUAT, @TRIGIAXUAT, @LUONGTONCUOI, @TRIGIATONCUOI, @GHICHU, @HOPDONG_ID, @TUNGAY, @DENNGAY, @MAKHO, @TENKHO)";
            string update = "UPDATE T_KHOKETOAN_BCQT SET STT = @STT, LOAIHANGHOA = @LOAIHANGHOA, TAIKHOAN = @TAIKHOAN, TENHANGHOA = @TENHANGHOA, MAHANGHOA = @MAHANGHOA, DVT = @DVT, LUONGTONDAU = @LUONGTONDAU, TRIGIATONDAU = @TRIGIATONDAU, LUONGNHAP = @LUONGNHAP, TRIGIANHAP = @TRIGIANHAP, LUONGXUAT = @LUONGXUAT, TRIGIAXUAT = @TRIGIAXUAT, LUONGTONCUOI = @LUONGTONCUOI, TRIGIATONCUOI = @TRIGIATONCUOI, GHICHU = @GHICHU, HOPDONG_ID = @HOPDONG_ID, TUNGAY = @TUNGAY, DENNGAY = @DENNGAY, MAKHO = @MAKHO, TENKHO = @TENKHO WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_BCQT WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAIHANGHOA", SqlDbType.Decimal, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TAIKHOAN", SqlDbType.Decimal, "TAIKHOAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENHANGHOA", SqlDbType.NVarChar, "TENHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHANGHOA", SqlDbType.NVarChar, "MAHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONDAU", SqlDbType.Decimal, "LUONGTONDAU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONDAU", SqlDbType.Decimal, "TRIGIATONDAU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGNHAP", SqlDbType.Decimal, "LUONGNHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANHAP", SqlDbType.Decimal, "TRIGIANHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONCUOI", SqlDbType.Decimal, "LUONGTONCUOI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONCUOI", SqlDbType.Decimal, "TRIGIATONCUOI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TUNGAY", SqlDbType.DateTime, "TUNGAY", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DENNGAY", SqlDbType.DateTime, "DENNGAY", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAIHANGHOA", SqlDbType.Decimal, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TAIKHOAN", SqlDbType.Decimal, "TAIKHOAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENHANGHOA", SqlDbType.NVarChar, "TENHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHANGHOA", SqlDbType.NVarChar, "MAHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONDAU", SqlDbType.Decimal, "LUONGTONDAU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONDAU", SqlDbType.Decimal, "TRIGIATONDAU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGNHAP", SqlDbType.Decimal, "LUONGNHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANHAP", SqlDbType.Decimal, "TRIGIANHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONCUOI", SqlDbType.Decimal, "LUONGTONCUOI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONCUOI", SqlDbType.Decimal, "TRIGIATONCUOI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TUNGAY", SqlDbType.DateTime, "TUNGAY", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DENNGAY", SqlDbType.DateTime, "DENNGAY", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KHOKETOAN_BCQT VALUES(@STT, @LOAIHANGHOA, @TAIKHOAN, @TENHANGHOA, @MAHANGHOA, @DVT, @LUONGTONDAU, @TRIGIATONDAU, @LUONGNHAP, @TRIGIANHAP, @LUONGXUAT, @TRIGIAXUAT, @LUONGTONCUOI, @TRIGIATONCUOI, @GHICHU, @HOPDONG_ID, @TUNGAY, @DENNGAY, @MAKHO, @TENKHO)";
            string update = "UPDATE T_KHOKETOAN_BCQT SET STT = @STT, LOAIHANGHOA = @LOAIHANGHOA, TAIKHOAN = @TAIKHOAN, TENHANGHOA = @TENHANGHOA, MAHANGHOA = @MAHANGHOA, DVT = @DVT, LUONGTONDAU = @LUONGTONDAU, TRIGIATONDAU = @TRIGIATONDAU, LUONGNHAP = @LUONGNHAP, TRIGIANHAP = @TRIGIANHAP, LUONGXUAT = @LUONGXUAT, TRIGIAXUAT = @TRIGIAXUAT, LUONGTONCUOI = @LUONGTONCUOI, TRIGIATONCUOI = @TRIGIATONCUOI, GHICHU = @GHICHU, HOPDONG_ID = @HOPDONG_ID, TUNGAY = @TUNGAY, DENNGAY = @DENNGAY, MAKHO = @MAKHO, TENKHO = @TENKHO WHERE ID = @ID";
            string delete = "DELETE FROM T_KHOKETOAN_BCQT WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LOAIHANGHOA", SqlDbType.Decimal, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TAIKHOAN", SqlDbType.Decimal, "TAIKHOAN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENHANGHOA", SqlDbType.NVarChar, "TENHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAHANGHOA", SqlDbType.NVarChar, "MAHANGHOA", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONDAU", SqlDbType.Decimal, "LUONGTONDAU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONDAU", SqlDbType.Decimal, "TRIGIATONDAU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGNHAP", SqlDbType.Decimal, "LUONGNHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIANHAP", SqlDbType.Decimal, "TRIGIANHAP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LUONGTONCUOI", SqlDbType.Decimal, "LUONGTONCUOI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TRIGIATONCUOI", SqlDbType.Decimal, "TRIGIATONCUOI", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TUNGAY", SqlDbType.DateTime, "TUNGAY", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DENNGAY", SqlDbType.DateTime, "DENNGAY", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@STT", SqlDbType.Int, "STT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LOAIHANGHOA", SqlDbType.Decimal, "LOAIHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TAIKHOAN", SqlDbType.Decimal, "TAIKHOAN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENHANGHOA", SqlDbType.NVarChar, "TENHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAHANGHOA", SqlDbType.NVarChar, "MAHANGHOA", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.VarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONDAU", SqlDbType.Decimal, "LUONGTONDAU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONDAU", SqlDbType.Decimal, "TRIGIATONDAU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGNHAP", SqlDbType.Decimal, "LUONGNHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIANHAP", SqlDbType.Decimal, "TRIGIANHAP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGXUAT", SqlDbType.Decimal, "LUONGXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIAXUAT", SqlDbType.Decimal, "TRIGIAXUAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LUONGTONCUOI", SqlDbType.Decimal, "LUONGTONCUOI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TRIGIATONCUOI", SqlDbType.Decimal, "TRIGIATONCUOI", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GHICHU", SqlDbType.NVarChar, "GHICHU", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HOPDONG_ID", SqlDbType.BigInt, "HOPDONG_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TUNGAY", SqlDbType.DateTime, "TUNGAY", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DENNGAY", SqlDbType.DateTime, "DENNGAY", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MAKHO", SqlDbType.NVarChar, "MAKHO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TENKHO", SqlDbType.NVarChar, "TENKHO", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KHOKETOAN_BCQT Load(long id)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_BCQT_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KHOKETOAN_BCQT> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KHOKETOAN_BCQT> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KHOKETOAN_BCQT> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_BCQT_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_BCQT_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KHOKETOAN_BCQT_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KHOKETOAN_BCQT_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KHOKETOAN_BCQT(int sTT, decimal lOAIHANGHOA, decimal tAIKHOAN, string tENHANGHOA, string mAHANGHOA, string dVT, decimal lUONGTONDAU, decimal tRIGIATONDAU, decimal lUONGNHAP, decimal tRIGIANHAP, decimal lUONGXUAT, decimal tRIGIAXUAT, decimal lUONGTONCUOI, decimal tRIGIATONCUOI, string gHICHU, long hOPDONG_ID, DateTime tUNGAY, DateTime dENNGAY, string mAKHO, string tENKHO)
		{
			T_KHOKETOAN_BCQT entity = new T_KHOKETOAN_BCQT();	
			entity.STT = sTT;
			entity.LOAIHANGHOA = lOAIHANGHOA;
			entity.TAIKHOAN = tAIKHOAN;
			entity.TENHANGHOA = tENHANGHOA;
			entity.MAHANGHOA = mAHANGHOA;
			entity.DVT = dVT;
			entity.LUONGTONDAU = lUONGTONDAU;
			entity.TRIGIATONDAU = tRIGIATONDAU;
			entity.LUONGNHAP = lUONGNHAP;
			entity.TRIGIANHAP = tRIGIANHAP;
			entity.LUONGXUAT = lUONGXUAT;
			entity.TRIGIAXUAT = tRIGIAXUAT;
			entity.LUONGTONCUOI = lUONGTONCUOI;
			entity.TRIGIATONCUOI = tRIGIATONCUOI;
			entity.GHICHU = gHICHU;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.TUNGAY = tUNGAY;
			entity.DENNGAY = dENNGAY;
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KHOKETOAN_BCQT_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);
			db.AddInParameter(dbCommand, "@LOAIHANGHOA", SqlDbType.Decimal, LOAIHANGHOA);
			db.AddInParameter(dbCommand, "@TAIKHOAN", SqlDbType.Decimal, TAIKHOAN);
			db.AddInParameter(dbCommand, "@TENHANGHOA", SqlDbType.NVarChar, TENHANGHOA);
			db.AddInParameter(dbCommand, "@MAHANGHOA", SqlDbType.NVarChar, MAHANGHOA);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.VarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGTONDAU", SqlDbType.Decimal, LUONGTONDAU);
			db.AddInParameter(dbCommand, "@TRIGIATONDAU", SqlDbType.Decimal, TRIGIATONDAU);
			db.AddInParameter(dbCommand, "@LUONGNHAP", SqlDbType.Decimal, LUONGNHAP);
			db.AddInParameter(dbCommand, "@TRIGIANHAP", SqlDbType.Decimal, TRIGIANHAP);
			db.AddInParameter(dbCommand, "@LUONGXUAT", SqlDbType.Decimal, LUONGXUAT);
			db.AddInParameter(dbCommand, "@TRIGIAXUAT", SqlDbType.Decimal, TRIGIAXUAT);
			db.AddInParameter(dbCommand, "@LUONGTONCUOI", SqlDbType.Decimal, LUONGTONCUOI);
			db.AddInParameter(dbCommand, "@TRIGIATONCUOI", SqlDbType.Decimal, TRIGIATONCUOI);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@TUNGAY", SqlDbType.DateTime, TUNGAY.Year <= 1753 ? DBNull.Value : (object) TUNGAY);
			db.AddInParameter(dbCommand, "@DENNGAY", SqlDbType.DateTime, DENNGAY.Year <= 1753 ? DBNull.Value : (object) DENNGAY);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KHOKETOAN_BCQT> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_BCQT item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KHOKETOAN_BCQT(long id, int sTT, decimal lOAIHANGHOA, decimal tAIKHOAN, string tENHANGHOA, string mAHANGHOA, string dVT, decimal lUONGTONDAU, decimal tRIGIATONDAU, decimal lUONGNHAP, decimal tRIGIANHAP, decimal lUONGXUAT, decimal tRIGIAXUAT, decimal lUONGTONCUOI, decimal tRIGIATONCUOI, string gHICHU, long hOPDONG_ID, DateTime tUNGAY, DateTime dENNGAY, string mAKHO, string tENKHO)
		{
			T_KHOKETOAN_BCQT entity = new T_KHOKETOAN_BCQT();			
			entity.ID = id;
			entity.STT = sTT;
			entity.LOAIHANGHOA = lOAIHANGHOA;
			entity.TAIKHOAN = tAIKHOAN;
			entity.TENHANGHOA = tENHANGHOA;
			entity.MAHANGHOA = mAHANGHOA;
			entity.DVT = dVT;
			entity.LUONGTONDAU = lUONGTONDAU;
			entity.TRIGIATONDAU = tRIGIATONDAU;
			entity.LUONGNHAP = lUONGNHAP;
			entity.TRIGIANHAP = tRIGIANHAP;
			entity.LUONGXUAT = lUONGXUAT;
			entity.TRIGIAXUAT = tRIGIAXUAT;
			entity.LUONGTONCUOI = lUONGTONCUOI;
			entity.TRIGIATONCUOI = tRIGIATONCUOI;
			entity.GHICHU = gHICHU;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.TUNGAY = tUNGAY;
			entity.DENNGAY = dENNGAY;
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KHOKETOAN_BCQT_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);
			db.AddInParameter(dbCommand, "@LOAIHANGHOA", SqlDbType.Decimal, LOAIHANGHOA);
			db.AddInParameter(dbCommand, "@TAIKHOAN", SqlDbType.Decimal, TAIKHOAN);
			db.AddInParameter(dbCommand, "@TENHANGHOA", SqlDbType.NVarChar, TENHANGHOA);
			db.AddInParameter(dbCommand, "@MAHANGHOA", SqlDbType.NVarChar, MAHANGHOA);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.VarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGTONDAU", SqlDbType.Decimal, LUONGTONDAU);
			db.AddInParameter(dbCommand, "@TRIGIATONDAU", SqlDbType.Decimal, TRIGIATONDAU);
			db.AddInParameter(dbCommand, "@LUONGNHAP", SqlDbType.Decimal, LUONGNHAP);
			db.AddInParameter(dbCommand, "@TRIGIANHAP", SqlDbType.Decimal, TRIGIANHAP);
			db.AddInParameter(dbCommand, "@LUONGXUAT", SqlDbType.Decimal, LUONGXUAT);
			db.AddInParameter(dbCommand, "@TRIGIAXUAT", SqlDbType.Decimal, TRIGIAXUAT);
			db.AddInParameter(dbCommand, "@LUONGTONCUOI", SqlDbType.Decimal, LUONGTONCUOI);
			db.AddInParameter(dbCommand, "@TRIGIATONCUOI", SqlDbType.Decimal, TRIGIATONCUOI);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@TUNGAY", SqlDbType.DateTime, TUNGAY.Year <= 1753 ? DBNull.Value : (object) TUNGAY);
			db.AddInParameter(dbCommand, "@DENNGAY", SqlDbType.DateTime, DENNGAY.Year <= 1753 ? DBNull.Value : (object) DENNGAY);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KHOKETOAN_BCQT> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_BCQT item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KHOKETOAN_BCQT(long id, int sTT, decimal lOAIHANGHOA, decimal tAIKHOAN, string tENHANGHOA, string mAHANGHOA, string dVT, decimal lUONGTONDAU, decimal tRIGIATONDAU, decimal lUONGNHAP, decimal tRIGIANHAP, decimal lUONGXUAT, decimal tRIGIAXUAT, decimal lUONGTONCUOI, decimal tRIGIATONCUOI, string gHICHU, long hOPDONG_ID, DateTime tUNGAY, DateTime dENNGAY, string mAKHO, string tENKHO)
		{
			T_KHOKETOAN_BCQT entity = new T_KHOKETOAN_BCQT();			
			entity.ID = id;
			entity.STT = sTT;
			entity.LOAIHANGHOA = lOAIHANGHOA;
			entity.TAIKHOAN = tAIKHOAN;
			entity.TENHANGHOA = tENHANGHOA;
			entity.MAHANGHOA = mAHANGHOA;
			entity.DVT = dVT;
			entity.LUONGTONDAU = lUONGTONDAU;
			entity.TRIGIATONDAU = tRIGIATONDAU;
			entity.LUONGNHAP = lUONGNHAP;
			entity.TRIGIANHAP = tRIGIANHAP;
			entity.LUONGXUAT = lUONGXUAT;
			entity.TRIGIAXUAT = tRIGIAXUAT;
			entity.LUONGTONCUOI = lUONGTONCUOI;
			entity.TRIGIATONCUOI = tRIGIATONCUOI;
			entity.GHICHU = gHICHU;
			entity.HOPDONG_ID = hOPDONG_ID;
			entity.TUNGAY = tUNGAY;
			entity.DENNGAY = dENNGAY;
			entity.MAKHO = mAKHO;
			entity.TENKHO = tENKHO;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_BCQT_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, STT);
			db.AddInParameter(dbCommand, "@LOAIHANGHOA", SqlDbType.Decimal, LOAIHANGHOA);
			db.AddInParameter(dbCommand, "@TAIKHOAN", SqlDbType.Decimal, TAIKHOAN);
			db.AddInParameter(dbCommand, "@TENHANGHOA", SqlDbType.NVarChar, TENHANGHOA);
			db.AddInParameter(dbCommand, "@MAHANGHOA", SqlDbType.NVarChar, MAHANGHOA);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.VarChar, DVT);
			db.AddInParameter(dbCommand, "@LUONGTONDAU", SqlDbType.Decimal, LUONGTONDAU);
			db.AddInParameter(dbCommand, "@TRIGIATONDAU", SqlDbType.Decimal, TRIGIATONDAU);
			db.AddInParameter(dbCommand, "@LUONGNHAP", SqlDbType.Decimal, LUONGNHAP);
			db.AddInParameter(dbCommand, "@TRIGIANHAP", SqlDbType.Decimal, TRIGIANHAP);
			db.AddInParameter(dbCommand, "@LUONGXUAT", SqlDbType.Decimal, LUONGXUAT);
			db.AddInParameter(dbCommand, "@TRIGIAXUAT", SqlDbType.Decimal, TRIGIAXUAT);
			db.AddInParameter(dbCommand, "@LUONGTONCUOI", SqlDbType.Decimal, LUONGTONCUOI);
			db.AddInParameter(dbCommand, "@TRIGIATONCUOI", SqlDbType.Decimal, TRIGIATONCUOI);
			db.AddInParameter(dbCommand, "@GHICHU", SqlDbType.NVarChar, GHICHU);
			db.AddInParameter(dbCommand, "@HOPDONG_ID", SqlDbType.BigInt, HOPDONG_ID);
			db.AddInParameter(dbCommand, "@TUNGAY", SqlDbType.DateTime, TUNGAY.Year <= 1753 ? DBNull.Value : (object) TUNGAY);
			db.AddInParameter(dbCommand, "@DENNGAY", SqlDbType.DateTime, DENNGAY.Year <= 1753 ? DBNull.Value : (object) DENNGAY);
			db.AddInParameter(dbCommand, "@MAKHO", SqlDbType.NVarChar, MAKHO);
			db.AddInParameter(dbCommand, "@TENKHO", SqlDbType.NVarChar, TENKHO);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KHOKETOAN_BCQT> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_BCQT item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KHOKETOAN_BCQT(long id)
		{
			T_KHOKETOAN_BCQT entity = new T_KHOKETOAN_BCQT();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_BCQT_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KHOKETOAN_BCQT_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KHOKETOAN_BCQT> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KHOKETOAN_BCQT item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}