﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class Container_VNACCS : DeclarationBase
    {
        /// <summary>
        /// Loại chứng từ = 304
        /// </summary>
        [XmlElement("issuer")]
        public string Issuer { get; set; }

        /// <summary>
        /// Số tham chiếu chứng từ
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Chức năng của chứng từ  
        /// Phản hồi trả về cho DN = 32
        /// </summary>
        [XmlElement("function")]
        public string Function { get; set; }

        /// <summary>
        /// Nơi khai báo
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Tình trạng chứng từ
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }

        ///// <summary>
        ///// Số Tờ Khai
        ///// </summary>
        //[XmlElement("customsReference")]
        //public string customsReference { get; set; }

        /// <summary>
        /// Ngày đăng ký tờ khai
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }

        /// <summary>
        /// Hải quan đang ký tờ khai
        /// </summary>
        [XmlElement("declarationOffice")]
        public string DeclarationOffice { get; set; }


        /// <summary>
        /// Mã loại hình
        /// </summary>
        [XmlElement("natureOfTransaction")]
        public string NatureOfTransaction { get; set; }

        /// <summary>
        /// Hải quan giám sát tờ khai
        /// </summary>
        [XmlElement("declarationOfficeControl")]
        public string DeclarationOfficeControl { get; set; }

        /// <summary>
        /// Mã địa điểm giám sát tờ khai
        /// </summary>
        [XmlElement("locationControl")]
        public string LocationControl { get; set; }
        
        /// <summary>
        /// Thời gian kết xuất dữ liệu
        /// </summary>
        [XmlElement("timeExport")]
        public string TimeExport { get; set; }

        /// <summary>
        /// Luồng tờ khai
        /// 1:X
        /// 2:V
        /// 3:Đ
        /// </summary>
        [XmlElement("channel")]
        public string Channel { get; set; }

        /// <summary>
        /// "TQ: Thông quan
        ///MHBQ: Mang hàng bảo quan
        ///GPH: Giải phòng hàng
        ///CCK: Chuyển địa điểm kiểm tra"
        /// </summary>
        [XmlElement("customsStatus")]
        public string CustomsStatus { get; set; }

        /// <summary>
        /// Tình trạng niêm phong của tờ khai
        /// </summary>
        [XmlElement("isContainer")]
        public string IsContainer { get; set; }

        /// <summary>
        /// Tình trạng niêm phong của tờ khai
        /// </summary>
        [XmlElement("note")]
        public string Note { get; set; }

        /// <summary>
        /// Số định danh hàng hóa
        /// </summary>
        [XmlElement("cargoCtrlNo")]
        public string CargoCtrlNo { get; set; }

        /// <summary>
        /// Mã phương thức vận chuyển
        /// </summary>
        [XmlElement("transportationCode")]
        public string TransportationCode { get; set; }


        /// <summary>
        /// Ngày tàu đến/Ngày tàu đi dự kiến trên tờ khai
        /// </summary>
        [XmlElement("arrivalDeparture")]
        public string ArrivalDeparture { get; set; }


        [XmlElement("DeclarationDocument")]
        public Company.KDT.SHARE.Components.Messages.SXXK.Container_DeclarationDocument DeclarationDocument { get; set; }

        [XmlElement("TransportEquipments")]
        public TransportEquipments TransportEquipments { get; set; }

        [XmlElement("TransportEquipment")]
        public TransportEquipment TransportEquipment_HR { get; set; }

    }
}
