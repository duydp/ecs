﻿using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class VAGP010 : BasicVNACC
    {

        public List<VAGP010_HANG> HangHoa { get; set; }
        public void LoadVAGP010(string strResult)
        {
            try
            {
                this.GetObject<VAGP010>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAGP010, false, VAGP010.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAGP010.TongSoByte);
                while (true)
                {
                    VAGP010_HANG ivaHang = new VAGP010_HANG();
                    ivaHang.GetObject<VAGP010_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAGP010_HANG", false, VAGP010_HANG.TongSoByte);
                    if (this.HangHoa == null) this.HangHoa = new List<VAGP010_HANG>();
                    this.HangHoa.Add(ivaHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAGP010_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAGP010_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAGP010_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }



        }


    }
}
