namespace Company.Interface.Report.VNACCS
{
    partial class ToKhaiHangHoaNhapKhau2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiDauTien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoNhanhToKhaiChiaNho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoToKhaiChiaNho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiTamNhapTaiXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiKiemTra = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHieuPhuongThucVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiCaNhanToChuc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSoHangHoaDaiDienToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCoQuanHaiQuanTiepNhanToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBoPhanXuLyToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayThayDoiDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioThayDoiDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoiHanTaiNhapTaiXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBieuThiTHHetHan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDinhKemKhaiBaoDienTu1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDinhKemKhaiBaoDienTu2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDinhKemKhaiBaoDienTu3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell64 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanGhiChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoQuanLyNoiBoDN = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoQuanLyNSD = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell123 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiChiThiHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell81 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.winControlContainer1 = new DevExpress.XtraReports.UI.WinControlContainer();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.formattingRule1 = new DevExpress.XtraReports.UI.FormattingRule();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSTTChiThiHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaychithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenchithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNoidungchithihaiquan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell90 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell93 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell95 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayKhaiBaoNopThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioKhaiBaoNT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTieuDe = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThueAnHan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThueAnHan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanNopThue1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThueAnHan2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThueAnHan2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanNopThue2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThueAnHan3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThueAnHan3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanNopThue3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThueAnHan4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThueAnHan4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanNopThue4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThueAnHan5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThueAnHan5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanNopThue5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSacThueAnHan6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenSacThueAnHan6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblHanNopThue6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayKhoiHanhVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell148 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell152 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiemTrungChuyen1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDuKienDenDDTC1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayKhoiHanh1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell157 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell173 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell174 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiemTrungChuyen2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDuKienDenDDTC2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell177 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayKhoiHanh2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell179 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell163 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell165 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiemTrungChuyen3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDuKienDenDDTC3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell168 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayKhoiHanh3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell170 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell180 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiemDichVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDuKienDen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell188 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.Font = new System.Drawing.Font("Arial", 8F);
            this.Detail.HeightF = 127F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.SnapLinePadding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.StylePriority.UseFont = false;
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow8,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow7});
            this.xrTable1.SizeF = new System.Drawing.SizeF(796.0001F, 127F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell74,
            this.lblSoToKhai,
            this.xrTableCell73,
            this.xrTableCell75,
            this.xrTableCell72,
            this.lblSoToKhaiDauTien,
            this.xrTableCell76,
            this.lblSoNhanhToKhaiChiaNho,
            this.xrTableCell78,
            this.lblTongSoToKhaiChiaNho});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.44000000000000006;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Weight = 0.22150619138694019;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Text = "Số tờ khai";
            this.xrTableCell69.Weight = 0.69516055934634968;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Weight = 0.050000002176888936;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.Text = "NNNNNNNNN1NE";
            this.lblSoToKhai.Weight = 1.2358328433093813;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Weight = 0.13791601462069569;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Text = "Số tờ khai đầu tiên";
            this.xrTableCell75.Weight = 1.1215999457676427;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Weight = 0.11282835597169623;
            // 
            // lblSoToKhaiDauTien
            // 
            this.lblSoToKhaiDauTien.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSoToKhaiDauTien.Name = "lblSoToKhaiDauTien";
            this.lblSoToKhaiDauTien.StylePriority.UseFont = false;
            this.lblSoToKhaiDauTien.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiDauTien.Weight = 1.2274477500810275;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Text = "-";
            this.xrTableCell76.Weight = 0.13369657867317344;
            // 
            // lblSoNhanhToKhaiChiaNho
            // 
            this.lblSoNhanhToKhaiChiaNho.Name = "lblSoNhanhToKhaiChiaNho";
            this.lblSoNhanhToKhaiChiaNho.Text = "NE";
            this.lblSoNhanhToKhaiChiaNho.Weight = 0.2586999785805435;
            this.lblSoNhanhToKhaiChiaNho.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Text = "/";
            this.xrTableCell78.Weight = 0.12329998497247977;
            // 
            // lblTongSoToKhaiChiaNho
            // 
            this.lblTongSoToKhaiChiaNho.Name = "lblTongSoToKhaiChiaNho";
            this.lblTongSoToKhaiChiaNho.Text = "NE";
            this.lblTongSoToKhaiChiaNho.Weight = 2.6420121018148177;
            this.lblTongSoToKhaiChiaNho.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell17,
            this.lblSoToKhaiTamNhapTaiXuat});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.Weight = 0.44000000000000006;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Weight = 0.22150619138694019;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            this.xrTableCell14.Weight = 1.9809938467600321;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.13791687011718773;
            // 
            // lblSoToKhaiTamNhapTaiXuat
            // 
            this.lblSoToKhaiTamNhapTaiXuat.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiTamNhapTaiXuat.Name = "lblSoToKhaiTamNhapTaiXuat";
            this.lblSoToKhaiTamNhapTaiXuat.StylePriority.UseFont = false;
            this.lblSoToKhaiTamNhapTaiXuat.Text = "NNNNNNNNN1NE";
            this.lblSoToKhaiTamNhapTaiXuat.Weight = 5.6195833984374763;
            this.lblSoToKhaiTamNhapTaiXuat.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.lblMaPhanLoaiKiemTra,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.lblMaLoaiHinh,
            this.lblMaPhanLoaiHangHoa,
            this.lblMaHieuPhuongThucVanChuyen,
            this.xrTableCell35,
            this.lblPhanLoaiCaNhanToChuc,
            this.xrTableCell19,
            this.xrTableCell21,
            this.xrTableCell18,
            this.xrTableCell22,
            this.lblMaSoHangHoaDaiDienToKhai});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseTextAlignment = false;
            this.xrTableRow3.Weight = 0.43999999999999995;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 0.221505962505113;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Mã phân loại kiểm tra";
            this.xrTableCell26.Weight = 1.174121104567722;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 0.14999997564003598;
            // 
            // lblMaPhanLoaiKiemTra
            // 
            this.lblMaPhanLoaiKiemTra.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiKiemTra.Name = "lblMaPhanLoaiKiemTra";
            this.lblMaPhanLoaiKiemTra.StylePriority.UseFont = false;
            this.lblMaPhanLoaiKiemTra.Text = "XX E";
            this.lblMaPhanLoaiKiemTra.Weight = 0.65687295728712924;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 0.13791687011718762;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Text = "Mã loại hình";
            this.xrTableCell30.Weight = 0.78822906494140621;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 0.10244812011718735;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.StylePriority.UseFont = false;
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.Weight = 0.34375;
            // 
            // lblMaPhanLoaiHangHoa
            // 
            this.lblMaPhanLoaiHangHoa.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiHangHoa.Name = "lblMaPhanLoaiHangHoa";
            this.lblMaPhanLoaiHangHoa.StylePriority.UseFont = false;
            this.lblMaPhanLoaiHangHoa.Text = "X";
            this.lblMaPhanLoaiHangHoa.Weight = 0.20489528656005862;
            // 
            // lblMaHieuPhuongThucVanChuyen
            // 
            this.lblMaHieuPhuongThucVanChuyen.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaHieuPhuongThucVanChuyen.Name = "lblMaHieuPhuongThucVanChuyen";
            this.lblMaHieuPhuongThucVanChuyen.StylePriority.UseFont = false;
            this.lblMaHieuPhuongThucVanChuyen.Text = "X";
            this.lblMaHieuPhuongThucVanChuyen.Weight = 0.23786464691162124;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.Text = "[";
            this.xrTableCell35.Weight = 0.11286464691162124;
            // 
            // lblPhanLoaiCaNhanToChuc
            // 
            this.lblPhanLoaiCaNhanToChuc.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiCaNhanToChuc.Name = "lblPhanLoaiCaNhanToChuc";
            this.lblPhanLoaiCaNhanToChuc.StylePriority.UseFont = false;
            this.lblPhanLoaiCaNhanToChuc.Text = "X";
            this.lblPhanLoaiCaNhanToChuc.Weight = 0.15994142055511468;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.Text = "]";
            this.xrTableCell19.Weight = 0.11827485561370854;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Weight = 0.13910798549652093;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Mã số hàng hóa đại diện của tờ khai";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell18.Weight = 1.8370245504379272;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Weight = 0.13238280296325689;
            // 
            // lblMaSoHangHoaDaiDienToKhai
            // 
            this.lblMaSoHangHoaDaiDienToKhai.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSoHangHoaDaiDienToKhai.Name = "lblMaSoHangHoaDaiDienToKhai";
            this.lblMaSoHangHoaDaiDienToKhai.StylePriority.UseFont = false;
            this.lblMaSoHangHoaDaiDienToKhai.Text = "XXXE";
            this.lblMaSoHangHoaDaiDienToKhai.Weight = 1.4428000560760264;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.lblTenCoQuanHaiQuanTiepNhanToKhai,
            this.xrTableCell41,
            this.xrTableCell47,
            this.xrTableCell23,
            this.lblMaBoPhanXuLyToKhai});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.44000000000000017;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 0.22150588621117057;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            this.xrTableCell38.Weight = 2.1189109871432947;
            // 
            // lblTenCoQuanHaiQuanTiepNhanToKhai
            // 
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Name = "lblTenCoQuanHaiQuanTiepNhanToKhai";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.StylePriority.UseFont = false;
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Text = "XXXXXXXXXE";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Weight = 2.0682682004980744;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.13910766601562485;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell47.StylePriority.UsePadding = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "Mã bộ phận xử lý tờ khai";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell47.Weight = 1.8370246505737304;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.13238281249999989;
            // 
            // lblMaBoPhanXuLyToKhai
            // 
            this.lblMaBoPhanXuLyToKhai.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBoPhanXuLyToKhai.Name = "lblMaBoPhanXuLyToKhai";
            this.lblMaBoPhanXuLyToKhai.StylePriority.UseFont = false;
            this.lblMaBoPhanXuLyToKhai.Text = "XE";
            this.lblMaBoPhanXuLyToKhai.Weight = 1.442800103759742;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.lblNgayDangKy,
            this.xrTableCell15,
            this.lblGioDangKy,
            this.xrTableCell54,
            this.xrTableCell55,
            this.lblNgayThayDoiDangKy,
            this.lblGioThayDoiDangKy,
            this.xrTableCell58,
            this.xrTableCell59,
            this.lblThoiHanTaiNhapTaiXuat,
            this.xrTableCell42,
            this.lblBieuThiTHHetHan});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.Weight = 0.43999999999999984;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.221505962505113;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "Ngày đăng ký";
            this.xrTableCell50.Weight = 0.74516082557689722;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.0999999847220521;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Text = "dd/MM/yyyy";
            this.lblNgayDangKy.Weight = 0.66083322719593807;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 0.050000000000000044;
            // 
            // lblGioDangKy
            // 
            this.lblGioDangKy.Name = "lblGioDangKy";
            this.lblGioDangKy.Text = "hh:mm:ss";
            this.lblGioDangKy.Weight = 0.56291641235351564;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "Ngày thay đổi đăng ký";
            this.xrTableCell54.Weight = 1.1215989552751935;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Weight = 0.11282865385359292;
            // 
            // lblNgayThayDoiDangKy
            // 
            this.lblNgayThayDoiDangKy.Name = "lblNgayThayDoiDangKy";
            this.lblNgayThayDoiDangKy.Text = "dd/MM/yyyy";
            this.lblNgayThayDoiDangKy.Weight = 0.71556665130778874;
            // 
            // lblGioThayDoiDangKy
            // 
            this.lblGioThayDoiDangKy.Name = "lblGioThayDoiDangKy";
            this.lblGioThayDoiDangKy.Text = "hh:mm:ss";
            this.lblGioThayDoiDangKy.Weight = 0.64557851635785812;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell58.StylePriority.UsePadding = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell58.Weight = 1.4488280868530274;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 0.13238277435302748;
            // 
            // lblThoiHanTaiNhapTaiXuat
            // 
            this.lblThoiHanTaiNhapTaiXuat.Name = "lblThoiHanTaiNhapTaiXuat";
            this.lblThoiHanTaiNhapTaiXuat.Text = "dd/MM/yyyy";
            this.lblThoiHanTaiNhapTaiXuat.Weight = 0.81098310470581036;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "-";
            this.xrTableCell42.Weight = 0.17111817359924308;
            // 
            // lblBieuThiTHHetHan
            // 
            this.lblBieuThiTHHetHan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBieuThiTHHetHan.Name = "lblBieuThiTHHetHan";
            this.lblBieuThiTHHetHan.StylePriority.UseFont = false;
            this.lblBieuThiTHHetHan.Text = "X";
            this.lblBieuThiTHHetHan.Weight = 0.46069897804257914;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Borders = ((DevExpress.XtraPrinting.BorderSide)((DevExpress.XtraPrinting.BorderSide.Top | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell43,
            this.xrTableCell1,
            this.xrTableCell7,
            this.xrTableCell8,
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1,
            this.xrTableCell9,
            this.lblSoDinhKemKhaiBaoDienTu1,
            this.xrTableCell10,
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2,
            this.xrTableCell11,
            this.lblSoDinhKemKhaiBaoDienTu2,
            this.xrTableCell12,
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3,
            this.xrTableCell67,
            this.lblSoDinhKemKhaiBaoDienTu3});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.StylePriority.UseBorders = false;
            this.xrTableRow6.Weight = 0.44000000000000006;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Weight = 0.221506343974825;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Số đính kèm khai báo điện tử";
            this.xrTableCell1.Weight = 1.5059934685426564;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 0.049999840991135025;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "1";
            this.xrTableCell8.Weight = 0.10000012169207095;
            // 
            // lblPhanLoaiDinhKemKhaiBaoDienTu1
            // 
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.Name = "lblPhanLoaiDinhKemKhaiBaoDienTu1";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.Text = "XXE";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu1.Weight = 0.32499965716647078;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "-";
            this.xrTableCell9.Weight = 0.13791622417622673;
            // 
            // lblSoDinhKemKhaiBaoDienTu1
            // 
            this.lblSoDinhKemKhaiBaoDienTu1.Name = "lblSoDinhKemKhaiBaoDienTu1";
            this.lblSoDinhKemKhaiBaoDienTu1.Text = "NNNNNNNNN1NE";
            this.lblSoDinhKemKhaiBaoDienTu1.Weight = 1.1215996401256105;
            this.lblSoDinhKemKhaiBaoDienTu1.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "2";
            this.xrTableCell10.Weight = 0.11282869882812452;
            // 
            // lblPhanLoaiDinhKemKhaiBaoDienTu2
            // 
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.Name = "lblPhanLoaiDinhKemKhaiBaoDienTu2";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.Text = "XXE";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu2.Weight = 0.36140105798128896;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "-";
            this.xrTableCell11.Weight = 0.13789998310448332;
            // 
            // lblSoDinhKemKhaiBaoDienTu2
            // 
            this.lblSoDinhKemKhaiBaoDienTu2.Name = "lblSoDinhKemKhaiBaoDienTu2";
            this.lblSoDinhKemKhaiBaoDienTu2.Text = "NNNNNNNNN1NE";
            this.lblSoDinhKemKhaiBaoDienTu2.Weight = 1.1205426353183912;
            this.lblSoDinhKemKhaiBaoDienTu2.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "3";
            this.xrTableCell12.Weight = 0.12330145237794898;
            // 
            // lblPhanLoaiDinhKemKhaiBaoDienTu3
            // 
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.Name = "lblPhanLoaiDinhKemKhaiBaoDienTu3";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.StylePriority.UseTextAlignment = false;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.Text = "XXE";
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblPhanLoaiDinhKemKhaiBaoDienTu3.Weight = 0.35314212422884173;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Text = "-";
            this.xrTableCell67.Weight = 0.13789998242470553;
            // 
            // lblSoDinhKemKhaiBaoDienTu3
            // 
            this.lblSoDinhKemKhaiBaoDienTu3.Name = "lblSoDinhKemKhaiBaoDienTu3";
            this.lblSoDinhKemKhaiBaoDienTu3.Text = "NNNNNNNNN1NE";
            this.lblSoDinhKemKhaiBaoDienTu3.Weight = 2.1509690757688578;
            this.lblSoDinhKemKhaiBaoDienTu3.BeforePrint += new System.Drawing.Printing.PrintEventHandler(this.lable_BeforePrint);
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell63,
            this.xrTableCell64,
            this.xrTableCell65,
            this.lblPhanGhiChu});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 1.12;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.22150651118361819;
            // 
            // xrTableCell64
            // 
            this.xrTableCell64.Name = "xrTableCell64";
            this.xrTableCell64.Text = "Phần ghi chú";
            this.xrTableCell64.Weight = 0.89307615529297646;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Weight = 0.099999620358145275;
            // 
            // lblPhanGhiChu
            // 
            this.lblPhanGhiChu.Font = new System.Drawing.Font("Arial", 8F);
            this.lblPhanGhiChu.Name = "lblPhanGhiChu";
            this.lblPhanGhiChu.StylePriority.UseFont = false;
            this.lblPhanGhiChu.StylePriority.UsePadding = false;
            this.lblPhanGhiChu.StylePriority.UseTextAlignment = false;
            this.lblPhanGhiChu.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblPhanGhiChu.Weight = 6.7454180198668974;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell117,
            this.xrTableCell118,
            this.xrTableCell119,
            this.lblSoQuanLyNoiBoDN,
            this.xrTableCell62,
            this.xrTableCell45,
            this.xrTableCell61,
            this.lblSoQuanLyNSD});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.43999999999999995;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Weight = 0.22150573362328577;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Text = "Số quản lý nội bộ doanh nghiệp";
            this.xrTableCell118.Weight = 1.6559941347520597;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Weight = 0.22342650220239391;
            // 
            // lblSoQuanLyNoiBoDN
            // 
            this.lblSoQuanLyNoiBoDN.Name = "lblSoQuanLyNoiBoDN";
            this.lblSoQuanLyNoiBoDN.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoQuanLyNoiBoDN.Weight = 1.6788127633475325;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Weight = 1.0225523414791202;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Text = "Số quản lý người sử dụng";
            this.xrTableCell45.Weight = 1.396926688140284;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 0.15057365049644522;
            // 
            // lblSoQuanLyNSD
            // 
            this.lblSoQuanLyNSD.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoQuanLyNSD.Name = "lblSoQuanLyNSD";
            this.lblSoQuanLyNSD.StylePriority.UseFont = false;
            this.lblSoQuanLyNSD.Text = "XXXXE";
            this.lblSoQuanLyNSD.Weight = 1.6102084926605169;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell123,
            this.lblPhanLoaiChiThiHQ});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.StylePriority.UseBorders = false;
            this.xrTableRow23.Weight = 0.44000000000000061;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Weight = 0.22150565732934335;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Text = "Phân loại chỉ thị Hải quan";
            this.xrTableCell122.Weight = 1.3722436716565674;
            // 
            // xrTableCell123
            // 
            this.xrTableCell123.Name = "xrTableCell123";
            this.xrTableCell123.Weight = 0.18374999221126165;
            // 
            // lblPhanLoaiChiThiHQ
            // 
            this.lblPhanLoaiChiThiHQ.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiChiThiHQ.Name = "lblPhanLoaiChiThiHQ";
            this.lblPhanLoaiChiThiHQ.StylePriority.UseFont = false;
            this.lblPhanLoaiChiThiHQ.Text = "X";
            this.lblPhanLoaiChiThiHQ.Weight = 6.1825009855044648;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell80,
            this.xrTableCell81,
            this.xrTableCell82,
            this.xrTableCell88,
            this.xrTableCell83});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.44000000000000061;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Weight = 0.59889033384504375;
            // 
            // xrTableCell81
            // 
            this.xrTableCell81.Name = "xrTableCell81";
            this.xrTableCell81.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell81.StylePriority.UsePadding = false;
            this.xrTableCell81.StylePriority.UseTextAlignment = false;
            this.xrTableCell81.Text = "Ngày";
            this.xrTableCell81.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell81.Weight = 0.7967364592963988;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell82.StylePriority.UsePadding = false;
            this.xrTableCell82.StylePriority.UseTextAlignment = false;
            this.xrTableCell82.Text = "Tên";
            this.xrTableCell82.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell82.Weight = 2.0663890887826808;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.xrTableCell88.StylePriority.UsePadding = false;
            this.xrTableCell88.StylePriority.UseTextAlignment = false;
            this.xrTableCell88.Text = "Nội dung";
            this.xrTableCell88.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell88.Weight = 4.3979841989086177;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.StylePriority.UseTextAlignment = false;
            this.xrTableCell83.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell83.Weight = 0.10000022586889718;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 22F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 29.79164F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.winControlContainer1,
            this.xrTable5,
            this.lblTenThongTinXuat});
            this.ReportHeader.HeightF = 56.95833F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // winControlContainer1
            // 
            this.winControlContainer1.LocationFloat = new DevExpress.Utils.PointFloat(497F, 26.45833F);
            this.winControlContainer1.Name = "winControlContainer1";
            this.winControlContainer1.SizeF = new System.Drawing.SizeF(300F, 30F);
            this.winControlContainer1.WinControl = this.pictureBox1;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(288, 29);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Arial", 9F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(749.9302F, 5.208333F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable5.SizeF = new System.Drawing.SizeF(47.06982F, 21.25001F);
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoTrang});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.6;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoTrang.StylePriority.UsePadding = false;
            this.lblSoTrang.Text = "2/3";
            this.lblSoTrang.Weight = 3;
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(139.5627F, 5.208326F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(491.875F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "Tờ khai hàng hóa nhập khẩu (thông báo kết quả phân luồng)";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // formattingRule1
            // 
            this.formattingRule1.Name = "formattingRule1";
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1,
            this.ReportFooter});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable2});
            this.Detail1.HeightF = 40F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(41.94224F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9});
            this.xrTable2.SizeF = new System.Drawing.SizeF(745.0579F, 40F);
            this.xrTable2.StylePriority.UseFont = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSTTChiThiHQ,
            this.lblNgaychithihaiquan1,
            this.lblTenchithihaiquan1,
            this.lblNoidungchithihaiquan1});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 1.1294115823658166;
            // 
            // lblSTTChiThiHQ
            // 
            this.lblSTTChiThiHQ.Name = "lblSTTChiThiHQ";
            this.lblSTTChiThiHQ.StylePriority.UseTextAlignment = false;
            this.lblSTTChiThiHQ.Text = "1";
            this.lblSTTChiThiHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSTTChiThiHQ.Weight = 1.1120111035650264;
            // 
            // lblNgaychithihaiquan1
            // 
            this.lblNgaychithihaiquan1.Name = "lblNgaychithihaiquan1";
            this.lblNgaychithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblNgaychithihaiquan1.Text = "dd/MM/yyyy";
            this.lblNgaychithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgaychithihaiquan1.Weight = 4.9367030470447908;
            // 
            // lblTenchithihaiquan1
            // 
            this.lblTenchithihaiquan1.Multiline = true;
            this.lblTenchithihaiquan1.Name = "lblTenchithihaiquan1";
            this.lblTenchithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblTenchithihaiquan1.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWWE";
            this.lblTenchithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblTenchithihaiquan1.Weight = 12.80365808836547;
            // 
            // lblNoidungchithihaiquan1
            // 
            this.lblNoidungchithihaiquan1.Name = "lblNoidungchithihaiquan1";
            this.lblNoidungchithihaiquan1.StylePriority.UseTextAlignment = false;
            this.lblNoidungchithihaiquan1.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWW0WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3";
            this.lblNoidungchithihaiquan1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNoidungchithihaiquan1.Weight = 27.312548200455787;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable3});
            this.ReportFooter.HeightF = 203.125F;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrTable3
            // 
            this.xrTable3.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTable3.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow21,
            this.xrTableRow20,
            this.xrTableRow24});
            this.xrTable3.SizeF = new System.Drawing.SizeF(796.0001F, 143F);
            this.xrTable3.StylePriority.UseBorders = false;
            this.xrTable3.StylePriority.UseFont = false;
            this.xrTable3.StylePriority.UsePadding = false;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell90,
            this.xrTableCell89});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.31058818515059949;
            // 
            // xrTableCell90
            // 
            this.xrTableCell90.Name = "xrTableCell90";
            this.xrTableCell90.Weight = 1.3724814114308472;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.StylePriority.UseTextAlignment = false;
            this.xrTableCell89.Text = "Mục thông báo của Hải quan";
            this.xrTableCell89.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell89.Weight = 47.948899131616422;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell93,
            this.xrTableCell94,
            this.xrTableCell95,
            this.lblNgayKhaiBaoNopThue,
            this.xrTableCell2,
            this.lblGioKhaiBaoNT});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.StylePriority.UseBorders = false;
            this.xrTableRow11.Weight = 0.31058818515059949;
            // 
            // xrTableCell93
            // 
            this.xrTableCell93.Name = "xrTableCell93";
            this.xrTableCell93.Weight = 3.7108165471323726;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Text = "Ngày khai báo nộp thuế";
            this.xrTableCell94.Weight = 7.9224593226546993;
            // 
            // xrTableCell95
            // 
            this.xrTableCell95.Name = "xrTableCell95";
            this.xrTableCell95.Weight = 0.76477065367642894;
            // 
            // lblNgayKhaiBaoNopThue
            // 
            this.lblNgayKhaiBaoNopThue.Name = "lblNgayKhaiBaoNopThue";
            this.lblNgayKhaiBaoNopThue.Text = "dd/MM/yyyy";
            this.lblNgayKhaiBaoNopThue.Weight = 4.0830756632742693;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 1.2117665843270711;
            // 
            // lblGioKhaiBaoNT
            // 
            this.lblGioKhaiBaoNT.Name = "lblGioKhaiBaoNT";
            this.lblGioKhaiBaoNT.Text = "hh:mm:ss";
            this.lblGioKhaiBaoNT.Weight = 31.628491771982439;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell91,
            this.xrTableCell92,
            this.xrTableCell97,
            this.xrTableCell98});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.StylePriority.UseBorders = false;
            this.xrTableRow12.Weight = 0.31058818515059949;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Weight = 3.7108165471323726;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Text = "Tổng số tiền thuế chậm nộp";
            this.xrTableCell92.Weight = 9.3068452298908149;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 1.2489762409762137;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Weight = 35.054742525047878;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell99,
            this.lblTieuDe});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.StylePriority.UseBorders = false;
            this.xrTableRow13.Weight = 0.31058818515059949;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Weight = 3.7108165471323726;
            // 
            // lblTieuDe
            // 
            this.lblTieuDe.Name = "lblTieuDe";
            this.lblTieuDe.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWE";
            this.lblTieuDe.Weight = 45.6105639959149;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101,
            this.lblMaSacThueAnHan1,
            this.xrTableCell106,
            this.lblTenSacThueAnHan1,
            this.xrTableCell107,
            this.lblHanNopThue1,
            this.lblMaSacThueAnHan2,
            this.xrTableCell109,
            this.lblTenSacThueAnHan2,
            this.xrTableCell110,
            this.lblHanNopThue2});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.StylePriority.UseBorders = false;
            this.xrTableRow14.Weight = 0.31058818515059949;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Weight = 5.6798066805491576;
            // 
            // lblMaSacThueAnHan1
            // 
            this.lblMaSacThueAnHan1.Name = "lblMaSacThueAnHan1";
            this.lblMaSacThueAnHan1.Text = "X";
            this.lblMaSacThueAnHan1.Weight = 1.2263187713923003;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Weight = 0.61961520807139436;
            // 
            // lblTenSacThueAnHan1
            // 
            this.lblTenSacThueAnHan1.Name = "lblTenSacThueAnHan1";
            this.lblTenSacThueAnHan1.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan1.Weight = 6.1212858035925439;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 0.6196151777083081;
            // 
            // lblHanNopThue1
            // 
            this.lblHanNopThue1.Name = "lblHanNopThue1";
            this.lblHanNopThue1.Text = "dd/MM/yyyy";
            this.lblHanNopThue1.Weight = 7.1845372705355892;
            // 
            // lblMaSacThueAnHan2
            // 
            this.lblMaSacThueAnHan2.Name = "lblMaSacThueAnHan2";
            this.lblMaSacThueAnHan2.Text = "X";
            this.lblMaSacThueAnHan2.Weight = 1.344529866008747;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Weight = 0.61961511698213556;
            // 
            // lblTenSacThueAnHan2
            // 
            this.lblTenSacThueAnHan2.Name = "lblTenSacThueAnHan2";
            this.lblTenSacThueAnHan2.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan2.Weight = 6.4765141620517745;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Weight = 0.61961520807139436;
            // 
            // lblHanNopThue2
            // 
            this.lblHanNopThue2.Name = "lblHanNopThue2";
            this.lblHanNopThue2.Text = "dd/MM/yyyy";
            this.lblHanNopThue2.Weight = 18.809927278083929;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell112,
            this.lblMaSacThueAnHan3,
            this.xrTableCell114,
            this.lblTenSacThueAnHan3,
            this.xrTableCell116,
            this.lblHanNopThue3,
            this.lblMaSacThueAnHan4,
            this.xrTableCell126,
            this.lblTenSacThueAnHan4,
            this.xrTableCell128,
            this.lblHanNopThue4});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.StylePriority.UseBorders = false;
            this.xrTableRow15.Weight = 0.31058818515059949;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Weight = 5.6798066805491576;
            // 
            // lblMaSacThueAnHan3
            // 
            this.lblMaSacThueAnHan3.Name = "lblMaSacThueAnHan3";
            this.lblMaSacThueAnHan3.Text = "X";
            this.lblMaSacThueAnHan3.Weight = 1.2263187713923003;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Weight = 0.61961520807139436;
            // 
            // lblTenSacThueAnHan3
            // 
            this.lblTenSacThueAnHan3.Name = "lblTenSacThueAnHan3";
            this.lblTenSacThueAnHan3.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan3.Weight = 6.1212858035925439;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Weight = 0.6196151777083081;
            // 
            // lblHanNopThue3
            // 
            this.lblHanNopThue3.Name = "lblHanNopThue3";
            this.lblHanNopThue3.Text = "dd/MM/yyyy";
            this.lblHanNopThue3.Weight = 7.1845372705355892;
            // 
            // lblMaSacThueAnHan4
            // 
            this.lblMaSacThueAnHan4.Name = "lblMaSacThueAnHan4";
            this.lblMaSacThueAnHan4.Text = "X";
            this.lblMaSacThueAnHan4.Weight = 1.344529866008747;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Weight = 0.61961511698213556;
            // 
            // lblTenSacThueAnHan4
            // 
            this.lblTenSacThueAnHan4.Name = "lblTenSacThueAnHan4";
            this.lblTenSacThueAnHan4.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan4.Weight = 6.4765141620517745;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Weight = 0.61961520807139436;
            // 
            // lblHanNopThue4
            // 
            this.lblHanNopThue4.Name = "lblHanNopThue4";
            this.lblHanNopThue4.Text = "dd/MM/yyyy";
            this.lblHanNopThue4.Weight = 18.809927278083929;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell131,
            this.lblMaSacThueAnHan5,
            this.xrTableCell133,
            this.lblTenSacThueAnHan5,
            this.xrTableCell135,
            this.lblHanNopThue5,
            this.lblMaSacThueAnHan6,
            this.xrTableCell138,
            this.lblTenSacThueAnHan6,
            this.xrTableCell140,
            this.lblHanNopThue6});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.StylePriority.UseBorders = false;
            this.xrTableRow16.Weight = 0.31058818515059949;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Weight = 5.6798066805491576;
            // 
            // lblMaSacThueAnHan5
            // 
            this.lblMaSacThueAnHan5.Name = "lblMaSacThueAnHan5";
            this.lblMaSacThueAnHan5.Text = "X";
            this.lblMaSacThueAnHan5.Weight = 1.2263187713923003;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Weight = 0.61961520807139436;
            // 
            // lblTenSacThueAnHan5
            // 
            this.lblTenSacThueAnHan5.Name = "lblTenSacThueAnHan5";
            this.lblTenSacThueAnHan5.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan5.Weight = 6.1212858035925439;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Weight = 0.6196151777083081;
            // 
            // lblHanNopThue5
            // 
            this.lblHanNopThue5.Name = "lblHanNopThue5";
            this.lblHanNopThue5.Text = "dd/MM/yyyy";
            this.lblHanNopThue5.Weight = 7.1845372705355892;
            // 
            // lblMaSacThueAnHan6
            // 
            this.lblMaSacThueAnHan6.Name = "lblMaSacThueAnHan6";
            this.lblMaSacThueAnHan6.Text = "X";
            this.lblMaSacThueAnHan6.Weight = 1.344529866008747;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Weight = 0.61961511698213556;
            // 
            // lblTenSacThueAnHan6
            // 
            this.lblTenSacThueAnHan6.Name = "lblTenSacThueAnHan6";
            this.lblTenSacThueAnHan6.Text = "WWWWWWWWE";
            this.lblTenSacThueAnHan6.Weight = 6.4765141620517745;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Weight = 0.61961520807139436;
            // 
            // lblHanNopThue6
            // 
            this.lblHanNopThue6.Name = "lblHanNopThue6";
            this.lblHanNopThue6.Text = "dd/MM/yyyy";
            this.lblHanNopThue6.Weight = 18.809927278083929;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell142,
            this.xrTableCell143,
            this.xrTableCell144,
            this.lblNgayKhoiHanhVanChuyen});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.StylePriority.UseBorders = false;
            this.xrTableRow17.Weight = 0.31058818515059949;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Weight = 3.7108165471323731;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Text = "Thời hạn cho phép vận chuyển bảo thuế (khởi hành)";
            this.xrTableCell143.Weight = 17.019513327230968;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Weight = 0.72084726802265253;
            // 
            // lblNgayKhoiHanhVanChuyen
            // 
            this.lblNgayKhoiHanhVanChuyen.Name = "lblNgayKhoiHanhVanChuyen";
            this.lblNgayKhoiHanhVanChuyen.Text = "dd/MM/yyyy";
            this.lblNgayKhoiHanhVanChuyen.Weight = 27.870203400661286;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell145,
            this.xrTableCell149,
            this.xrTableCell146,
            this.xrTableCell147,
            this.xrTableCell150,
            this.xrTableCell160,
            this.xrTableCell148});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.StylePriority.UseBorders = false;
            this.xrTableRow18.Weight = 0.31058818515059949;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Weight = 3.7108165471323731;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Weight = 12.770306339351945;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.StylePriority.UseTextAlignment = false;
            this.xrTableCell146.Text = "Địa điểm";
            this.xrTableCell146.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell146.Weight = 4.9700542862647614;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.StylePriority.UseTextAlignment = false;
            this.xrTableCell147.Text = "Ngày đến";
            this.xrTableCell147.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell147.Weight = 5.1328621232679623;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Weight = 1.2392283885932587;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.StylePriority.UseTextAlignment = false;
            this.xrTableCell160.Text = "Ngày khởi hành";
            this.xrTableCell160.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell160.Weight = 5.1278057465172617;
            // 
            // xrTableCell148
            // 
            this.xrTableCell148.Name = "xrTableCell148";
            this.xrTableCell148.Weight = 16.370307111919715;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell152,
            this.xrTableCell153,
            this.xrTableCell158,
            this.xrTableCell154,
            this.lblDiaDiemTrungChuyen1,
            this.lblNgayDuKienDenDDTC1,
            this.xrTableCell156,
            this.lblNgayKhoiHanh1,
            this.xrTableCell157});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.StylePriority.UseBorders = false;
            this.xrTableRow19.Weight = 0.31058818515059949;
            // 
            // xrTableCell152
            // 
            this.xrTableCell152.Name = "xrTableCell152";
            this.xrTableCell152.Weight = 3.7108165471323731;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Text = "Thông tin trung chuyển";
            this.xrTableCell153.Weight = 9.9362066984599036;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Weight = 2.124126037739241;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Text = "1";
            this.xrTableCell154.Weight = 0.70997357278971407;
            // 
            // lblDiaDiemTrungChuyen1
            // 
            this.lblDiaDiemTrungChuyen1.Name = "lblDiaDiemTrungChuyen1";
            this.lblDiaDiemTrungChuyen1.StylePriority.UseTextAlignment = false;
            this.lblDiaDiemTrungChuyen1.Text = "XXXXXXE";
            this.lblDiaDiemTrungChuyen1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblDiaDiemTrungChuyen1.Weight = 4.9700541875847328;
            // 
            // lblNgayDuKienDenDDTC1
            // 
            this.lblNgayDuKienDenDDTC1.Name = "lblNgayDuKienDenDDTC1";
            this.lblNgayDuKienDenDDTC1.StylePriority.UseTextAlignment = false;
            this.lblNgayDuKienDenDDTC1.Text = "dd/MM/yyyy";
            this.lblNgayDuKienDenDDTC1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgayDuKienDenDDTC1.Weight = 5.1328584653119815;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.StylePriority.UseTextAlignment = false;
            this.xrTableCell156.Text = "~";
            this.xrTableCell156.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell156.Weight = 1.2392302846767125;
            // 
            // lblNgayKhoiHanh1
            // 
            this.lblNgayKhoiHanh1.Name = "lblNgayKhoiHanh1";
            this.lblNgayKhoiHanh1.StylePriority.UseTextAlignment = false;
            this.lblNgayKhoiHanh1.Text = "dd/MM/yyyy";
            this.lblNgayKhoiHanh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgayKhoiHanh1.Weight = 5.1278057465172617;
            // 
            // xrTableCell157
            // 
            this.xrTableCell157.Name = "xrTableCell157";
            this.xrTableCell157.Weight = 16.370309002835359;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell171,
            this.xrTableCell172,
            this.xrTableCell173,
            this.xrTableCell174,
            this.lblDiaDiemTrungChuyen2,
            this.lblNgayDuKienDenDDTC2,
            this.xrTableCell177,
            this.lblNgayKhoiHanh2,
            this.xrTableCell179});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.StylePriority.UseBorders = false;
            this.xrTableRow21.Weight = 0.31058818515059949;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Weight = 3.7108165471323731;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Weight = 9.9362066984599036;
            // 
            // xrTableCell173
            // 
            this.xrTableCell173.Name = "xrTableCell173";
            this.xrTableCell173.Weight = 2.124126037739241;
            // 
            // xrTableCell174
            // 
            this.xrTableCell174.Name = "xrTableCell174";
            this.xrTableCell174.Text = "2";
            this.xrTableCell174.Weight = 0.70997357278971407;
            // 
            // lblDiaDiemTrungChuyen2
            // 
            this.lblDiaDiemTrungChuyen2.Name = "lblDiaDiemTrungChuyen2";
            this.lblDiaDiemTrungChuyen2.StylePriority.UseTextAlignment = false;
            this.lblDiaDiemTrungChuyen2.Text = "XXXXXXE";
            this.lblDiaDiemTrungChuyen2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblDiaDiemTrungChuyen2.Weight = 4.9700541875847328;
            // 
            // lblNgayDuKienDenDDTC2
            // 
            this.lblNgayDuKienDenDDTC2.Name = "lblNgayDuKienDenDDTC2";
            this.lblNgayDuKienDenDDTC2.StylePriority.UseTextAlignment = false;
            this.lblNgayDuKienDenDDTC2.Text = "dd/MM/yyyy";
            this.lblNgayDuKienDenDDTC2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgayDuKienDenDDTC2.Weight = 5.1328584653119815;
            // 
            // xrTableCell177
            // 
            this.xrTableCell177.Name = "xrTableCell177";
            this.xrTableCell177.StylePriority.UseTextAlignment = false;
            this.xrTableCell177.Text = "~";
            this.xrTableCell177.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell177.Weight = 1.2392302846767125;
            // 
            // lblNgayKhoiHanh2
            // 
            this.lblNgayKhoiHanh2.Name = "lblNgayKhoiHanh2";
            this.lblNgayKhoiHanh2.StylePriority.UseTextAlignment = false;
            this.lblNgayKhoiHanh2.Text = "dd/MM/yyyy";
            this.lblNgayKhoiHanh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgayKhoiHanh2.Weight = 5.1278057465172617;
            // 
            // xrTableCell179
            // 
            this.xrTableCell179.Name = "xrTableCell179";
            this.xrTableCell179.Weight = 16.370309002835359;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell155,
            this.xrTableCell163,
            this.xrTableCell164,
            this.xrTableCell165,
            this.lblDiaDiemTrungChuyen3,
            this.lblNgayDuKienDenDDTC3,
            this.xrTableCell168,
            this.lblNgayKhoiHanh3,
            this.xrTableCell170});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.StylePriority.UseBorders = false;
            this.xrTableRow20.Weight = 0.31058818515059949;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Weight = 3.7108165471323731;
            // 
            // xrTableCell163
            // 
            this.xrTableCell163.Name = "xrTableCell163";
            this.xrTableCell163.Weight = 9.9362066984599036;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Weight = 2.124126037739241;
            // 
            // xrTableCell165
            // 
            this.xrTableCell165.Name = "xrTableCell165";
            this.xrTableCell165.Text = "3";
            this.xrTableCell165.Weight = 0.70997357278971407;
            // 
            // lblDiaDiemTrungChuyen3
            // 
            this.lblDiaDiemTrungChuyen3.Name = "lblDiaDiemTrungChuyen3";
            this.lblDiaDiemTrungChuyen3.StylePriority.UseTextAlignment = false;
            this.lblDiaDiemTrungChuyen3.Text = "XXXXXXE";
            this.lblDiaDiemTrungChuyen3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblDiaDiemTrungChuyen3.Weight = 4.9700541875847328;
            // 
            // lblNgayDuKienDenDDTC3
            // 
            this.lblNgayDuKienDenDDTC3.Name = "lblNgayDuKienDenDDTC3";
            this.lblNgayDuKienDenDDTC3.StylePriority.UseTextAlignment = false;
            this.lblNgayDuKienDenDDTC3.Text = "dd/MM/yyyy";
            this.lblNgayDuKienDenDDTC3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgayDuKienDenDDTC3.Weight = 5.1328584653119815;
            // 
            // xrTableCell168
            // 
            this.xrTableCell168.Name = "xrTableCell168";
            this.xrTableCell168.StylePriority.UseTextAlignment = false;
            this.xrTableCell168.Text = "~";
            this.xrTableCell168.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell168.Weight = 1.2392302846767125;
            // 
            // lblNgayKhoiHanh3
            // 
            this.lblNgayKhoiHanh3.Name = "lblNgayKhoiHanh3";
            this.lblNgayKhoiHanh3.StylePriority.UseTextAlignment = false;
            this.lblNgayKhoiHanh3.Text = "dd/MM/yyyy";
            this.lblNgayKhoiHanh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgayKhoiHanh3.Weight = 5.1278057465172617;
            // 
            // xrTableCell170
            // 
            this.xrTableCell170.Name = "xrTableCell170";
            this.xrTableCell170.Weight = 16.370309002835359;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell180,
            this.xrTableCell181,
            this.lblDiaDiemDichVanChuyen,
            this.lblNgayDuKienDen,
            this.xrTableCell186,
            this.xrTableCell188});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.StylePriority.UseBorders = false;
            this.xrTableRow24.Weight = 0.31058818515059949;
            // 
            // xrTableCell180
            // 
            this.xrTableCell180.Name = "xrTableCell180";
            this.xrTableCell180.Weight = 3.7108165471323731;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Text = "Địa điểm đích cho vận chuyển bảo thuế";
            this.xrTableCell181.Weight = 12.770306339351945;
            // 
            // lblDiaDiemDichVanChuyen
            // 
            this.lblDiaDiemDichVanChuyen.Name = "lblDiaDiemDichVanChuyen";
            this.lblDiaDiemDichVanChuyen.StylePriority.UseTextAlignment = false;
            this.lblDiaDiemDichVanChuyen.Text = "XXXXXXE";
            this.lblDiaDiemDichVanChuyen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblDiaDiemDichVanChuyen.Weight = 4.9700541572216457;
            // 
            // lblNgayDuKienDen
            // 
            this.lblNgayDuKienDen.Name = "lblNgayDuKienDen";
            this.lblNgayDuKienDen.StylePriority.UseTextAlignment = false;
            this.lblNgayDuKienDen.Text = "dd/MM/yyyy";
            this.lblNgayDuKienDen.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblNgayDuKienDen.Weight = 5.1328584653119815;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Weight = 1.2392302846767125;
            // 
            // xrTableCell188
            // 
            this.xrTableCell188.Name = "xrTableCell188";
            this.xrTableCell188.Weight = 21.49811474935262;
            // 
            // ToKhaiHangHoaNhapKhau2
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.DetailReport});
            this.FormattingRuleSheet.AddRange(new DevExpress.XtraReports.UI.FormattingRule[] {
            this.formattingRule1});
            this.Margins = new System.Drawing.Printing.Margins(14, 16, 22, 30);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiTamNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiKiemTra;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiHangHoa;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHieuPhuongThucVanChuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSoHangHoaDaiDienToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCoQuanHaiQuanTiepNhanToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBoPhanXuLyToKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRTableCell lblGioDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRTableCell lblGioThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell lblBieuThiTHHetHan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiCaNhanToChuc;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell lblThoiHanTaiNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDinhKemKhaiBaoDienTu3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell64;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanGhiChu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell lblSoQuanLyNSD;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell123;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiChiThiHQ;
        internal DevExpress.XtraReports.UI.XRTable xrTable1;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.FormattingRule formattingRule1;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTrang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiDinhKemKhaiBaoDienTu1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDinhKemKhaiBaoDienTu1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiDinhKemKhaiBaoDienTu2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDinhKemKhaiBaoDienTu2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiDinhKemKhaiBaoDienTu3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiDauTien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell lblSoNhanhToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRTableCell lblSoQuanLyNoiBoDN;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell81;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell lblSTTChiThiHQ;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaychithihaiquan1;
        private DevExpress.XtraReports.UI.XRTableCell lblNoidungchithihaiquan1;
        private DevExpress.XtraReports.UI.XRTableCell lblTenchithihaiquan1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell90;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell93;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell95;
        private DevExpress.XtraReports.UI.XRTableCell lblGioKhaiBaoNT;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell lblTieuDe;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThueAnHan1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThueAnHan1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell lblHanNopThue1;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThueAnHan2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThueAnHan2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell lblHanNopThue2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThueAnHan3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThueAnHan3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell lblHanNopThue3;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThueAnHan4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThueAnHan4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell lblHanNopThue4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThueAnHan5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThueAnHan5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell lblHanNopThue5;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSacThueAnHan6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell lblTenSacThueAnHan6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell lblHanNopThue6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayKhoiHanhVanChuyen;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell148;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell152;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiemTrungChuyen1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell157;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDuKienDenDDTC1;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayKhoiHanh1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell173;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell174;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiemTrungChuyen2;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDuKienDenDDTC2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell177;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayKhoiHanh2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell179;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell163;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell165;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiemTrungChuyen3;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDuKienDenDDTC3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell168;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayKhoiHanh3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell170;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell180;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiemDichVanChuyen;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDuKienDen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell188;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayKhaiBaoNopThue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.WinControlContainer winControlContainer1;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}
