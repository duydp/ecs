
/****** Object:  View [dbo].[v_KDT_SXXK_NPLNhapTon]    Script Date: 05/22/2013 15:58:28 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[v_KDT_SXXK_NPLNhapTon]'))
DROP VIEW [dbo].[v_KDT_SXXK_NPLNhapTon]
GO

CREATE VIEW dbo.v_KDT_SXXK_NPLNhapTon
AS
SELECT CASE when d.Ten IS NULL THEN (SELECT Ten FROM t_HaiQuan_DonViTinh WHERE id = CAST(v.DVT_ID AS CHAR(2))) ELSE d.Ten END AS TenDVT_NPL, v.* FROM 
(
	SELECT    a.SoToKhai, a.MaLoaiHinh, a.NamDangKy, a.NgayDangKy, a.MaHaiQuan, b.MaPhu AS MaNPL, b.TenHang AS TenNPL, 
						  --d.Ten AS TenDVT_NPL,
						  b.DVT_ID,  
						  ISNULL(c.Luong, b.SoLuong) AS Luong, ISNULL(c.Ton, b.SoLuong) AS TonDau, ISNULL(c.Ton, b.SoLuong) AS TonCuoi, 
						  CASE WHEN year(e.NGAY_THN_THX) > 1900 THEN e.Ngay_THN_THX ELSE a.NgayDangKy END AS NgayThucNhap, a.BangKeHoSoThanhLy_ID, 
						  c.ThueXNK, c.ThueXNKTon AS TonDauThueXNK, c.ThueXNKTon AS TonCuoiThueXNK, b.DonGiaTT, b.ThueSuatXNK AS ThueSuat, 
						  e.TyGiaTinhThue AS TyGiaTT, e.NgayHoanThanh
	FROM         dbo.t_KDT_SXXK_BKToKhaiNhap AS a INNER JOIN
						  dbo.t_SXXK_ToKhaiMauDich AS e ON a.SoToKhai = e.SoToKhai AND a.MaLoaiHinh = e.MaLoaiHinh AND a.NamDangKy = e.NamDangKy AND 
						  a.MaHaiQuan = e.MaHaiQuan INNER JOIN
						  dbo.t_SXXK_HangMauDich AS b ON a.SoToKhai = b.SoToKhai AND a.MaLoaiHinh = b.MaLoaiHinh AND a.NamDangKy = b.NamDangKy AND 
						  a.MaHaiQuan = b.MaHaiQuan LEFT OUTER JOIN
						  dbo.t_SXXK_ThanhLy_NPLNhapTon AS c ON a.SoToKhai = c.SoToKhai AND a.MaLoaiHinh = c.MaLoaiHinh AND a.NamDangKy = c.NamDangKy AND 
						  a.MaHaiQuan = c.MaHaiQuan AND b.MaPhu = c.MaNPL 
						  --INNER JOIN dbo.t_HaiQuan_DonViTinh AS d ON d.ID = b.DVT_ID
	--ORDER BY a.NgayDangKy
) AS v	LEFT JOIN dbo.t_HaiQuan_DonViTinh AS d ON d.ID = v.DVT_ID

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '8.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('8.9',GETDATE(), N'Cap nhat v_KDT_SXXK_NPLNhapTon - Thanh khoan')
END