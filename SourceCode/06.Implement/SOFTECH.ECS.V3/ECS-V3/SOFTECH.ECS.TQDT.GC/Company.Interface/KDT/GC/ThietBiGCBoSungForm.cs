﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Linq;
namespace Company.Interface.KDT.GC
{
    public partial class ThietBiGCBoSungForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public Company.GC.BLL.KDT.GC.PhuKienDangKy pkdk = new PhuKienDangKy();
        private Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK = new Company.GC.BLL.KDT.GC.LoaiPhuKien();
        private Company.GC.BLL.KDT.GC.HangPhuKien HangPK = new Company.GC.BLL.KDT.GC.HangPhuKien();
        public bool boolFlag;
        public string MaLoaiPK = string.Empty;

        public ThietBiGCBoSungForm()
        {
            InitializeComponent();

        }



        private void ThietBiGCEditForm_Load(object sender, EventArgs e)
        {
            bool isSuaSP = false;
            isSuaSP = MaLoaiPK == "504";
            dgThietBi.RootTable.Columns["ThongTinCu"].Visible = txtMaCu.ReadOnly =
            lblCu.Visible = txtMaCu.Visible = lblTenCu.Visible = isSuaSP;
            btnChon.Visible = MaLoaiPK != "804";

            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);

            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;


            foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien pk in pkdk.PKCollection)
            {
                if (pk.MaPhuKien.Trim() == MaLoaiPK)
                {
                    LoaiPK = pk;
                    break;
                }
            }
            if (pkdk.TrangThaiXuLy != TrangThaiXuLy.CHUA_KHAI_BAO && pkdk.TrangThaiXuLy != TrangThaiXuLy.KHONG_PHE_DUYET && pkdk.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                LoaiPK.LoadCollection();
                btnAdd.Enabled = false;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            }
            //Registered
            if (boolFlag == true)
            {
                btnAdd.Enabled = true;
                dgThietBi.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            }
            LoaiPK.MaPhuKien = MaLoaiPK;
            if (LoaiPK.HPKCollection.Count == 0)
                LoaiPK.LoadCollection();
            dgThietBi.DataSource = LoaiPK.HPKCollection;

        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
        }

        private void setErro()
        {
            error.Clear();
            error.SetError(txtMa, null);
            error.SetError(txtTen, null);
            error.SetError(txtMaHS, null);

        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            txtTriGia_Leave(null, null);
            txtMa.Focus();
            cvError.Validate();
            if (!cvError.IsValid) return;
            {
                if (Convert.ToDecimal(txtSoLuong.Text) <= 0)
                {
                    error.SetError(txtSoLuong, setText("Số lượng phải lớn hơn 0.", "This value must be greater than 0"));
                    return;
                }
                if (Convert.ToDecimal(txtDonGia.Text) <= 0)
                {
                    error.SetError(txtDonGia, setText("Đơn giá phải lớn hơn 0.", "This value must be greater than 0"));
                    return;
                }

                if (Convert.ToDecimal(txtTriGia.Text) <= 0)
                {
                    error.SetError(txtTriGia, setText("Giá gia công phải lớn hơn 0.", "This value must be greater than 0"));
                    return;
                }
                checkExitsThietBiAndSTTHang();
            }
        }
        private void checkExitsThietBiAndSTTHang()
        {
            try
            {
                Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                tb.HopDong_ID = this.HD.ID;
                tb.Ma = txtMa.Text.Trim();
                if (tb.Load())
                {
                    if (tb.Ma.Trim().ToUpper() != HangPK.MaHang.Trim().ToUpper())
                    {
                        showMsg("MSG_0203002");
                        //MLMessages("Thiết bị này đã được khai báo.","MSG_STN08","", false);
                        txtMa.Focus();
                        return;
                    }
                }
                LoaiPK.HPKCollection.Remove(HangPK);
                foreach (HangPhuKien p in LoaiPK.HPKCollection)
                {
                    if (p.MaHang == txtMa.Text.Trim())
                    {
                        showMsg("MSG_0203003");
                        //MLMessages("Đã có thiết bị này trong danh sách.", "MSG_WRN06", "", false);
                        if (HangPK.MaHang.Trim() != "")
                            LoaiPK.HPKCollection.Add(HangPK);
                        txtMa.Focus();
                        return;
                    }
                }
                HangPK.Delete(LoaiPK.MaPhuKien, HD.ID);
                HangPK.ID = 0;
                HangPK.MaHang = txtMa.Text.Trim();
                HangPK.TenHang = txtTen.Text.Trim();
                HangPK.MaHS = txtMaHS.Text.Trim();
                HangPK.ThongTinCu = txtMaCu.Text;
                HangPK.DVT_ID = cbDonViTinh.SelectedValue.ToString();
                HangPK.NuocXX_ID = ctrNuoc.Ma;
                HangPK.SoLuong = Convert.ToDecimal(txtSoLuong.Text);
                HangPK.DonGia = Convert.ToDouble(txtDonGia.Text);
                HangPK.TriGia = Convert.ToDouble(txtTriGia.Text);
                HangPK.NguyenTe_ID = ctrNguyenTe.Ma;
                HangPK.TinhTrang = chTinhTrang.Checked ? "1" : "0";
                LoaiPK.HPKCollection.Add(HangPK);
                LoaiPK.Master_ID = pkdk.ID;
                LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                if (boolFlag == true)
                {
                    LoaiPK.InsertUpdateBoSungTBDadangKy(pkdk.HopDong_ID);
                    boolFlag = false;
                }
                else
                {
                    LoaiPK.InsertUpdateBoSungTB(pkdk.HopDong_ID);
                }
                Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
                if (lpk == null)
                    pkdk.PKCollection.Add(LoaiPK);
                //PhuKienGCSendForm.isEdit = true;
                reset();
                this.setErro();
            }
            catch (Exception ex)
            {
                showMsg("MSG_2702004", ex.Message);
                //ShowMessage("Lỗi : " + ex.Message, false);
            }
        }
        private void dgThietBi_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            HangPK = (HangPhuKien)e.Row.DataRow;
            if (HangPK != null)
            {
                txtMa.Text = HangPK.MaHang;
                txtTen.Text = HangPK.TenHang;
                txtMaHS.Text = HangPK.MaHS;
                cbDonViTinh.SelectedValue = HangPK.DVT_ID;
                ctrNuoc.Ma = HangPK.NuocXX_ID;
                txtSoLuong.Text = HangPK.SoLuong.ToString();
                txtDonGia.Text = HangPK.DonGia.ToString();
                txtTriGia.Text = HangPK.TriGia.ToString();
                ctrNguyenTe.Ma = HangPK.NguyenTe_ID;
                chTinhTrang.Checked = HangPK.TinhTrang == "1";
                txtMaCu.Text = HangPK.ThongTinCu;
            }
        }

        private void dgThietBi_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value).Trim();
            e.Row.Cells["TenXuatXu"].Text = this.Nuoc_GetName(e.Row.Cells["XuatXu"].Value).Trim();
            string tinhTrang = e.Row.Cells["TinhTrang"].Value.ToString() == "0" ? "Còn mới" : "Cũ";
            e.Row.Cells["TinhTrang"].Text = tinhTrang;
            try
            {
                e.Row.Cells["TenNguyenTe"].Text = this.NguyenTe_GetName(e.Row.Cells["NguyenTe"].Value).Trim();
            }
            catch { }
        }

        private void txtTriGia_Leave(object sender, EventArgs e)
        {
            try
            {
                decimal soluong = Convert.ToDecimal(txtSoLuong.Text);
                decimal dongia = Convert.ToDecimal(txtDonGia.Text);
                decimal trigia = soluong * dongia;
                txtTriGia.Text = trigia.ToString();
            }
            catch
            {
                showMsg("MSG_0203004");
                //MLMessages("Trị giá quá lớn, vui lòng điều chỉnh lại số lượng hoặc đơn giá ","MSG_WRN31","", false);
                txtDonGia.Text = "0";
                txtTriGia.Text = "0";
            }
        }

        private void dgThietBi_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            /*
            if (showMsg("MSG_DEL01", true) == "Yes")
            {

                GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {
                            //pkDelete.Delete(LoaiPK.MaPhuKien,pkdk.HopDong_ID);
                            pkDelete.DeleteTransaction(null);
                        }
                    }
                }

            }
            else
            {
                e.Cancel = true;
            }*/
            btnXoa_Click(null, null);
        }

        private void ThietBiGCBoSungForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            //if (LoaiPK.HPKCollection.Count > 0)
            //{
            //    LoaiPK.MaPhuKien = "T07";
            //    LoaiPK.NoiDung = LoaiPhuKien_GetName("T07");
            //    pkdk.PKCollection.Remove(LoaiPK);
            //    pkdk.PKCollection.Add(LoaiPK);
            //}
            //else
            //{
            //    pkdk.PKCollection.Remove(LoaiPK);
            //    //LoaiPK.Delete(pkdk.HopDong_ID);;
            //    LoaiPK.DeleteTransaction(null);
            //}
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (!btnAdd.Enabled) return;
            if (LoaiPK.HPKCollection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgThietBi.SelectedItems;
            Company.GC.BLL.KDT.GC.HangPhuKienCollection pkColl = new HangPhuKienCollection();
            pkColl = (HangPhuKienCollection)LoaiPK.HPKCollection.Clone();
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType == RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.HangPhuKien pkDelete = (HangPhuKien)row.GetRow().DataRow;
                        if (pkDelete.ID > 0)
                        {  //pkDelete.Delete(LoaiPK.MaPhuKien,pkdk.HopDong_ID);
                            try
                            {
                                pkDelete.DeleteTransaction(null);
                            }
                            catch { }
                        }
                        try
                        {
                            pkColl.Remove(pkDelete);
                            LoaiPK.HPKCollection = (HangPhuKienCollection)pkColl.Clone();
                        }
                        catch { }
                        // pkColl.Add(pkDelete);
                    }
                }
                //foreach (HangPhuKien pk in pkColl)
                //{
                //    try
                //    {
                //        LoaiPK.HPKCollection.Remove(pk);
                //    }
                //    catch { }
                //}                                  
                dgThietBi.DataSource = LoaiPK.HPKCollection;
                this.reset();
                this.setErro();
                //try
                //{
                //    dgThietBi.Refetch();
                //}
                //catch { dgThietBi.Refresh(); }

            }
        }

        private void reset()
        {
            txtMa.Text = "";
            txtTen.Text = "";
            txtMaHS.Text = "";
            txtSoLuong.Text = "0.000";
            txtDonGia.Text = "0.000";
            txtTriGia.Text = "0.000";
            chTinhTrang.Checked = false;
            ctrNuoc.Ma = GlobalSettings.NUOC;
            ctrNguyenTe.Ma = GlobalSettings.NGUYEN_TE_MAC_DINH;
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            try
            {
                dgThietBi.Refetch();
            }
            catch
            {
                dgThietBi.Refresh();
            }
            HangPK = new HangPhuKien();


        }

        private void btnChon_Click(object sender, EventArgs e)
        {
            FormChonThietBi frm = new FormChonThietBi();
            frm.HD = HD;
            if (frm.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    foreach (HangPhuKien hangPK in frm.HPKCollection)
                    {
                        hangPK.Master_ID = LoaiPK.ID;
                        LoaiPK.HPKCollection.Add(hangPK);
                    }
                    LoaiPK.Master_ID = pkdk.ID;
                    LoaiPK.NoiDung = LoaiPhuKien_GetName(LoaiPK.MaPhuKien);
                    if (boolFlag == true)
                    {
                        LoaiPK.InsertUpdateBoSungDaDangKy(pkdk.HopDong_ID);
                        boolFlag = false;
                    }
                    else
                    {
                        LoaiPK.InsertUpdateBoSungChiTietLoaiPhuKien(pkdk.HopDong_ID);
                    }
                    Company.GC.BLL.KDT.GC.LoaiPhuKien lpk = pkdk.PKCollection.ToArray().SingleOrDefault(l => l.MaPhuKien == MaLoaiPK);
                    if (lpk == null)
                        pkdk.PKCollection.Add(LoaiPK);
                    dgThietBi.DataSource = LoaiPK.HPKCollection;
                }
                catch (Exception ex)
                {
                    ShowMessage(ex.Message, false);
                }
                reset();
                this.setErro();
            }

        }


    }
}