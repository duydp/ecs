﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraReports.UI;
using Company.Interface.Report.SXXK;
using Company.BLL.KDT.SXXK;
using DevExpress.XtraBars;

namespace Company.Interface.Report
{
    public partial class PreviewForm_KCX : BaseForm
    {
        public BangKeToKhaiNhap BKTKN = new BangKeToKhaiNhap();
        public BangKeToKhaiXuat BKTKX = new BangKeToKhaiXuat();
        public BCNPLXuatNhapTon_KCX BCNPLXNT_79 = new BCNPLXuatNhapTon_KCX();
        public BCNPLXuatNhapTon_KCX_194 BCNPLXNT_194 = new BCNPLXuatNhapTon_KCX_194();
        public Company.Interface.Report.SXXK.BCThueXNK BCThue = new Company.Interface.Report.SXXK.BCThueXNK();
        public int LanThanhLy;
        public int SoHoSo;
        public string SoHoSoString;
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public long BangKeNhapID;
        public PreviewForm_KCX()
        {
            InitializeComponent();
        }

        private void explorerBar1_ItemClick(object sender, Janus.Windows.ExplorerBar.ItemEventArgs e)
        {
            switch (e.Item.Key)
            {
                case "BKTKN":
                    printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
                    this.BKTKN.CreateDocument();
                    break;
                case "BKTKX":
                    printControl1.PrintingSystem = this.BKTKX.PrintingSystem;
                    this.BKTKX.CreateDocument();
                    break;
                case "BCNPLXNT":
                    printControl1.PrintingSystem = this.BCNPLXNT_194.PrintingSystem;
                    this.BCNPLXNT_194.CreateDocument();
                    break;
                //case "BCThueNK":
                //    printControl1.PrintingSystem = this.BCThue.PrintingSystem;
                //    this.BCThue.CreateDocument();
                //    break;
                case "BCNPLXNT_79":
                    printControl1.PrintingSystem = this.BCNPLXNT_79.PrintingSystem;
                    this.BCNPLXNT_79.CreateDocument();
                    break;
            }
        }
        private void BindComBo()
        {
            //cboToKhai.SelectedIndex = 0;
            try
            {

                cboToKhai.DataSource = new Company.BLL.KDT.SXXK.BCXuatNhapTon().GetMaNPL(GlobalSettings.MA_DON_VI, this.LanThanhLy).Tables[0];
                cboToKhai.DisplayMember = "MaNPL";
                cboToKhai.ValueMember = "MaNPL";
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
        private void PreviewForm_Load(object sender, EventArgs e)
        {
            this.BindComBo();
            printPreviewBarItem4.Enabled = printPreviewBarItem5.Enabled = Program.isActivated;
            if (GlobalSettings.GiaoDien.Id == "Office2003")
                this.defaultLookAndFeel1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            string where = "";
            try
            {
                this.BKTKN.LanThanhLy = this.LanThanhLy;
                this.BKTKN.SoHoSo = this.SoHoSo;
                this.BKTKN.BindReport(where);

                this.BKTKX.SoHoSo = this.SoHoSo;
                this.BKTKX.BindReport(this.LanThanhLy);

                this.BCNPLXNT_194.LanThanhLy = this.LanThanhLy;
                this.BCNPLXNT_194.SoHoSo = this.SoHoSo;
                this.BCNPLXNT_194.SoHoSoString = this.SoHoSoString;
                this.BCNPLXNT_194.BangKeNhapID = this.BangKeNhapID;
                this.BCNPLXNT_194.BindReport(where);

                this.BCThue.LanThanhLy = this.LanThanhLy;
                this.BCThue.SoHoSo = this.SoHoSo;
                this.BCThue.BindReport(where);

                this.BCNPLXNT_79.LanThanhLy = this.LanThanhLy;
                this.BCNPLXNT_79.SoHoSo = this.SoHoSo;
                this.BCNPLXNT_79.BindReport(where);

                printControl1.PrintingSystem = this.BKTKN.PrintingSystem;
                this.BKTKN.CreateDocument();

            }
            catch
            {
                ShowMessage("Có lỗi khi nạp dữ liệu báo cáo.", false);
            }
        }

        private void cboToKhai_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cboToKhai.SelectedIndex == 0)
            //{
            this.BCNPLXNT_194.LanThanhLy = this.LanThanhLy;
            this.BCNPLXNT_194.SoHoSo = this.SoHoSo;
            this.BCNPLXNT_194.SoHoSoString = this.SoHoSoString;
            this.BCNPLXNT_194.BindReportByMaNPL("", cboToKhai.Text.ToString());
            printControl1.PrintingSystem = this.BCNPLXNT_194.PrintingSystem;
            this.BCNPLXNT_194.CreateDocument();
            //}
        }

        private void printBarManager1_ItemClick(object sender, ItemClickEventArgs e)
        {
            switch (e.Item.Name)
            {
                case "mniExcelFile":

                    break;
            }
        }
    }
}