﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu.GCCT;
using System.Collections.Generic;
using Company.KDT.SHARE.Components;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KD.BLL.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
//using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT.GC;
#endif

namespace Company.Interface
{
    public partial class ListChungTuNoFormGCCT : BaseForm
    {
        public ToKhaiChuyenTiep TKCT;

        public ListChungTuNoFormGCCT()
        {
            InitializeComponent();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ListChungTuNoForm_Load(object sender, EventArgs e)
        {
            if (TKCT.ID > 0)
            {
                dgList.DataSource = ChungTuNo.SelectCollectionDynamic("TKCTID=" + TKCT.ID, "");
            }
            else
            {
                if (TKCT.ChungTuNoCollection.Count > 0)
                {
                    dgList.DataSource = TKCT.ChungTuNoCollection;
                }
            }

            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }


            this.dgList.RootTable.Columns["MA_LOAI_CT"].HasValueList = true;
            Janus.Windows.GridEX.GridEXValueListItemCollection valueList = this.dgList.RootTable.Columns["MA_LOAI_CT"].ValueList;

            System.Data.DataView view = LoaiChungTu.SelectAll().Tables[0].DefaultView;
            view.Sort = "ID ASC";

            for (int i = 0; i < view.Count; i++)
            {
                System.Data.DataRowView row = view[i];
                valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"].ToString(), row["Ten"].ToString()));
            }
            SetButtonState(TKCT);

        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<ChungTuNo> listChungTu = new List<ChungTuNo>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        ChungTuNo gp = (ChungTuNo)i.GetRow().DataRow;
                        listChungTu.Add(gp);
                    }
                }
            }



            try
            {
                ChungTuNo.DeleteCollection(listChungTu);
                foreach (ChungTuNo item in listChungTu)
                {
                  TKCT.ChungTuNoCollection.Remove(item);
                }
                dgList.Refetch();
                dgList.DataSource = TKCT.ChungTuNoCollection;
            }
            catch { dgList.Refresh(); }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            ChungTuNoFormGCCT ctForm = new ChungTuNoFormGCCT();
            ctForm.TKCT = TKCT;
            ctForm.ShowDialog();

            dgList.DataSource = TKCT.ChungTuNoCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChungTuNoFormGCCT f = new ChungTuNoFormGCCT();
            f.isAddNew = false;
            f.TKCT = TKCT;
            f.CTN = (ChungTuNo)e.Row.DataRow;
            f.ShowDialog();
            dgList.DataSource = TKCT.ChungTuNoCollection;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        #region Begin Set Button State


        private bool SetButtonState(ToKhaiChuyenTiep tkct)
        {
            bool status = false;

            status = (tkct.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET || tkct.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO
                || tkct.TrangThaiXuLy == TrangThaiXuLy.DA_HUY);

            btnXoa.Enabled = status;
            btnTaoMoi.Enabled = status;


            return true;
        }

        #endregion
    }
}
