﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using PopupControl;
namespace Company.Interface.Controls
{

    public partial class TimToKhai : UserControl
    {
        public Popup popup;
        public ToKhaiProviders toKhaiProviders;
        public event EventHandler<StringEventArgs> Search;
        public TimToKhai()
        {
            InitializeComponent();
            popup = new Popup(toKhaiProviders = new ToKhaiProviders());
            if (SystemInformation.IsComboBoxAnimationEnabled)
            {
                popup.ShowingAnimation = PopupAnimations.Slide | PopupAnimations.TopToBottom;
                popup.HidingAnimation = PopupAnimations.Slide | PopupAnimations.BottomToTop;
            }
            else
            {
                popup.ShowingAnimation = popup.HidingAnimation = PopupAnimations.None;
            }
            setDataToCboPhanLuong();
            cbStatus.SelectedValue = -1;
            toKhaiProviders.ProviderChanged += ToKhaiTimKiem;
            borderLabel.Image = imageList1.Images[0];
            borderLabel.Text = "Số tờ khai";
        }

        void ToKhaiTimKiem(object sender, EventArgs e)
        {
            DoSearch(e);
        }
        private void DoSearch(EventArgs e)
        {
            if (Search != null)
            {
                string where = string.Empty;

                int status = -1;
                if (cbStatus.SelectedValue != null)
                {
                    if (Convert.ToInt32(cbStatus.SelectedValue) != 5000)
                    {
                        where += " AND TrangThaiXuLy = " + cbStatus.SelectedValue;
                        status = Convert.ToInt32(cbStatus.SelectedValue);
                    }
                    else
                    {
                        where += " AND ActionStatus = 5000 AND TrangThaiXuLy = 1";
                        status = Convert.ToInt32(cbStatus.SelectedValue);
                    }
                }
                if (cboPhanLuong.SelectedValue != null && cboPhanLuong.SelectedIndex != 0)
                    where += " AND PhanLuong=" + cboPhanLuong.SelectedValue;
                if (e != null)
                {
                    if (status > -1)
                    {
                        if (toKhaiProviders.NamTiepNhan != "")
                            where += " AND YEAR(NgayTiepNhan) = " + toKhaiProviders.NamTiepNhan;
                    }
                    if (!string.IsNullOrEmpty(txtSoTiepNhan.Text))
                    {
                        where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Value + "%'";
                    }
                    if (txtSoToKhai.TextLength > 0)
                    {
                        if (Convert.ToInt32(cbStatus.SelectedValue) == 5000)
                            where += " AND LoaiVanDon like '%" + txtSoToKhai.Text + "%'";
                        else if (txtSoToKhai.TextLength == 12)
                            where += " AND LoaiVanDon like '%" + txtSoToKhai.Text + "%'";
                        else
                            where += " AND SoToKhai like '%" + txtSoToKhai.Text + "%'";
                    }

                }
                else
                {
                    where += toKhaiProviders.SearchString;
                }
                Search(this, new StringEventArgs(where, status));
            }
        }
        private void setDataToCboPhanLuong()
        {
            DataTable dtPL = new DataTable();
            dtPL.Columns.Add("ID", typeof(System.String));
            dtPL.Columns.Add("GhiChu", typeof(System.String));
            DataRow drNull = dtPL.NewRow();
            drNull["ID"] = -1;
            drNull["GhiChu"] = "--Tất cả--";
            dtPL.Rows.Add(drNull);
            for (int i = 1; i <= 3; i++)
            {
                DataRow dr = dtPL.NewRow();
                dr["ID"] = i.ToString();
                if (i == 1)
                    dr["GhiChu"] = "Luồng xanh";
                if (i == 2)
                    dr["GhiChu"] = "Luồng vàng";
                if (i == 3)
                    dr["GhiChu"] = "Luồng đỏ";
                dtPL.Rows.Add(dr);
            }
            cboPhanLuong.DataSource = dtPL;
            cboPhanLuong.DisplayMember = dtPL.Columns["GhiChu"].ToString();
            cboPhanLuong.ValueMember = dtPL.Columns["ID"].ToString();
            cboPhanLuong.SelectedValue = -1;
        }
        private void buttonDropDown_Click(object sender, EventArgs e)
        {
            //popup.Width = Widt

            popup.Show(sender as Control);
        }
        private void textBox_Enter(object sender, EventArgs e)
        {
            borderLabel.Image = null;
        }
        private void borderLabel_MouseDown(object sender, MouseEventArgs e)
        {
            txtSoToKhai.Visible = true;
            txtSoToKhai.Focus();
        }
        private void textBox_Leave(object sender, EventArgs e)
        {
            if (txtSoToKhai.Text.Length == 0)
            {
                borderLabel.Image = imageList1.Images[0];
                borderLabel.Text = "Số tờ khai";
                txtSoToKhai.Visible = false;
            }
        }
        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                DoSearch(EventArgs.Empty);
            }
        }
    }
}
