﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.QuanTri;
using Janus.Windows.GridEX;
#if KD_V3 || KD_V4
    using Company.KD.BLL;
#elif SXXK_V3 || SXXK_V4
    using Company.BLL;
#elif GC_V3 || GC_V4
    using Company.GC.BLL;
#endif
using Company.KDT.SHARE.Components;
using Helper.Controls;
using Company.Interface;
using Company.Interface.DongBoDuLieu_New;

namespace DongBoDuLieu_New.Admin
{
    public partial class QuanLyNguoiDung : Company.Interface.BaseForm
    {
        string maDoanhNghiep = "", passwordLogin = "", userLogin = "";
#if SXXK_V4
       Company.Interface.wssyndata.DaiLy[] daiLyCollection;
#else
                DaiLy[] daiLyCollection;
#endif
        

        public QuanLyNguoiDung()
        {
            InitializeComponent();

            ServiceDongBoDuLieu.GetUrlServiceDongBoDuLieu();
            maDoanhNghiep = GlobalSettings.MA_DON_VI;
            dgList.RootTable.Columns["MaDoanhNghiep"].SortIndicator = SortIndicator.Descending;
        }

        private void QuanLyNguoiDung_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (!ServiceDongBoDuLieu.CheckServiceDongBoDuLieu())
                    return;
                //Load ds Đại lý từ WS
                search();
            }
            catch (Exception ex)
            {
                MessageBoxControlV.ShowMessage("Xảy ra lỗi: " + ex.ToString(), false);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            if (e.Command.Key == "TaoMoi")
            {
                if (Login())
                {
                    NguoiDungEditForm f = new NguoiDungEditForm();
                    f.userLogin = userLogin;
                    f.passwordLogin = passwordLogin;
                    if (userLogin.Equals("administrator"))
                        f.isCheck = true;
                    f.ShowDialog();
                    search();
                    try { dgList.Refetch(); }
                    catch { dgList.Refresh(); }
                }
            }
        }

        private bool Login()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                Company.Interface.WSDBDLForm wsForm = new Company.Interface.WSDBDLForm();
                if (GlobalSettings.PASSWOR_DONGBO == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return false;
                }
                passwordLogin = GlobalSettings.PASSWOR_DONGBO != "" ? GlobalSettings.PASSWOR_DONGBO : wsForm.txtMatKhau.Text.Trim();
                userLogin = GlobalSettings.USERNAME_DONGBO != "" ? GlobalSettings.USERNAME_DONGBO : wsForm.txtMaDoanhNghiep.Text.Trim();
#if SXXK_V4
                Company.Interface.wssyndata.DaiLy userDaiLy = ServiceDongBoDuLieu.myService.LoginDaiLy(userLogin, passwordLogin);
#else
                 DaiLy userDaiLy = ServiceDongBoDuLieu.myService.LoginDaiLy(userLogin, passwordLogin);
#endif

                if (userDaiLy == null || (userDaiLy.MaDoanhNghiep != GlobalSettings.MA_DON_VI && !userDaiLy.USER_NAME.Equals("administrator")))
                {
                    MessageBoxControlV.ShowMessage("Người dùng không hợp lệ!", false);
                    userLogin = "";
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            DaiLy user = (DaiLy)e.Row.DataRow;
            NguoiDungEditForm f = new NguoiDungEditForm();
            f.user = user;
            f.ShowDialog();
            search();
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void search()
        {
            daiLyCollection = ServiceDongBoDuLieu.myService.SelectUserDaiLyAll();
            dgList.DataSource = daiLyCollection;

        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            if (MessageBoxControlV.ShowMessage("Bạn có muốn xóa người dùng này không ?", true) == DialogResult.Yes)
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DaiLy u = (DaiLy)i.GetRow().DataRow;
                        bool ok;
                        if (u.isAdmin != true)
                        {
                            if (userLogin == "" && !Login())
                            {
                                return;
                            }
                            else
                                ok = ServiceDongBoDuLieu.myService.DeleteUserDaiLy(userLogin, passwordLogin, u.USER_NAME, u.PASSWORD);

                        }
                        else
                        {
                            MessageBoxControlV.ShowMessage("Đây là người dùng mặc định của hệ thống. Không xóa thể xóa người dùng này.", false);
                            e.Cancel = true;
                        }
                    }
                }
                //Load ds Đại lý từ WS
                search();
            }
            else
                e.Cancel = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            switch (Convert.ToInt32(e.Row.Cells["isAdmin"].Value))
            {
                case 0:
                    e.Row.Cells["isAdmin"].Text = "Đại lý";
                    break;
                case 1:
                    e.Row.Cells["isAdmin"].Text = "Admin";
                    break;
            }
        }
    }
}

