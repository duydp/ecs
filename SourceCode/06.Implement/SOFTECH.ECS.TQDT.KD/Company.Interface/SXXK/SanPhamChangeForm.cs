﻿using System;
using Company.KD.BLL;
using Company.KD.BLL.SXXK;
using Company.KD.BLL.Utils;
using Company.KD.BLL.DuLieuChuan;
using System.Data;

namespace Company.Interface.SXXK
{
    public partial class SanPhamChangeForm : BaseForm
    {
        public HangHoaXuat HangXuat;
        private string tempMa = "";
        private string tempTen = "";

        public SanPhamChangeForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
       
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            // Kiểm tra tính hợp lệ của mã HS.
           

            if (!cvError.IsValid) return;

            Company.KD.BLL.SXXK.HangHoaXuat spSXXK = new Company.KD.BLL.SXXK.HangHoaXuat();
            spSXXK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            spSXXK.MaHaiQuan = "";           
            spSXXK.Ma = txtMa.Text.Trim();
            if (spSXXK.Load() && spSXXK.Ma.Trim().ToUpper() != HangXuat.Ma.Trim().ToUpper())
            {
                MLMessages("Hàng hóa xuất này đã được đăng ký.","MSG_STN09","", false);
                return;
            }
            spSXXK.Ten = txtTen.Text.Trim();
            spSXXK.MaHS = txtMaHS.Text;
            spSXXK.DVT_ID = cbDonViTinh.SelectedValue.ToString();

            try
            {
                if (HangXuat != null)
                    spSXXK.InsertUpdateFull(HangXuat.Ma);
                else
                    spSXXK.InsertUpdateFull("");
                MLMessages("Cập nhật thành công","MSG_SAV02","", false);
            }
            catch (Exception ex) { ShowMessage("Lỗi : " + ex.Message, false); return; }

            this.Close();
        }

        private void SanPhamEditForm_Load(object sender, EventArgs e)
        {

            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            if (HangXuat != null)
            {
                //
                tempMa = HangXuat.Ma;
                tempTen = HangXuat.Ten;
                //
                txtMa.Text = HangXuat.Ma;
                txtMaHS.Text = HangXuat.MaHS;
                txtTen.Text = HangXuat.Ten;
                cbDonViTinh.SelectedValue = HangXuat.DVT_ID;
            }
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            
          
        }

    }
}