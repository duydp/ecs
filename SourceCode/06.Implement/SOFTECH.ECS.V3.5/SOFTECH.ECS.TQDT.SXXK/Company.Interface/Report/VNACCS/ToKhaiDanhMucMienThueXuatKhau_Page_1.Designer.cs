﻿namespace Company.Interface.Report.VNACCS
{
    partial class ToKhaiDanhMucMienThueXuatKhau_Page_1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.PageHeader = new DevExpress.XtraReports.UI.PageHeaderBand();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongSoTrang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDanhMuc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoQuanLyDanhSachMT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell12 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCoQuanHQ = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayKhaiBao = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoiDiemKB = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgaySuaDoi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoiDiemSuaDoi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayHoanThanhKT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell28 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoiDiemHoanThanhKT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell32 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell33 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell40 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell34 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell36 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaChiNguoiKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDienThoaiNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoiHanMienThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell53 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenDuAnDauTu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell56 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDiaDiemXayDungDuAn = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMucTieuDuAn = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaMienGiamKhongChiuThueXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuKhoanMienGiam = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhamViDK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell70 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDuKienXNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiayPhepDauTu_GCNSo = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell77 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayChungNhan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell79 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell80 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblCapBoi = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiayPhepDauTu_GCNDieuChinhLan1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell110 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChungNhanDCSo1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell112 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayChungNhanDC1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell20 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell24 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuChinhBoi1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell39 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiayPhepDauTu_GCNDieuChinhLan2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell121 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChungNhanDCSo2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell119 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayChungNhanDC2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell48 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell52 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuChinhBoi2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell60 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiayPhepDauTu_GCNDieuChinhLan3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell124 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChungNhanDCSo3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell141 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayChungNhanDC3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell71 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuChinhBoi3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiayPhepDauTu_GCNDieuChinhLan4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell133 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell134 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChungNhanDCSo4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayChungNhanDC4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell97 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell98 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell99 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuChinhBoi4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGiayPhepDauTu_GCNDieuChinhLan5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell136 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell138 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblChungNhanDCSo5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayChungNhanDC5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell106 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuChinhBoi5 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1});
            this.Detail.HeightF = 758F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 9F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 110F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // PageHeader
            // 
            this.PageHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel2,
            this.lblTongSoTrang,
            this.lblTenThongTinXuat,
            this.xrLabel1});
            this.PageHeader.HeightF = 34F;
            this.PageHeader.Name = "PageHeader";
            // 
            // xrLabel2
            // 
            this.xrLabel2.LocationFloat = new DevExpress.Utils.PointFloat(753.0697F, 2.749999F);
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.SizeF = new System.Drawing.SizeF(10F, 20.04F);
            this.xrLabel2.Text = "/";
            // 
            // lblTongSoTrang
            // 
            this.lblTongSoTrang.LocationFloat = new DevExpress.Utils.PointFloat(763.0697F, 2.749999F);
            this.lblTongSoTrang.Name = "lblTongSoTrang";
            this.lblTongSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongSoTrang.SizeF = new System.Drawing.SizeF(25.9303F, 20.04176F);
            this.lblTongSoTrang.Text = "X";
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(217.5F, 2.75F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(420F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "Tờ khai danh mục miễn thuế xuất khẩu";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // xrLabel1
            // 
            this.xrLabel1.LocationFloat = new DevExpress.Utils.PointFloat(737.4446F, 2.749999F);
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.SizeF = new System.Drawing.SizeF(15.62506F, 20.04176F);
            this.xrLabel1.StylePriority.UseTextAlignment = false;
            this.xrLabel1.Text = "1";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Times New Roman", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow15,
            this.xrTableRow16,
            this.xrTableRow17,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25,
            this.xrTableRow26,
            this.xrTableRow27,
            this.xrTableRow28,
            this.xrTableRow29});
            this.xrTable1.SizeF = new System.Drawing.SizeF(763.1439F, 717.7917F);
            this.xrTable1.StylePriority.UseFont = false;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell8,
            this.lblSoDanhMuc,
            this.xrTableCell5,
            this.xrTableCell4,
            this.xrTableCell7,
            this.lblSoQuanLyDanhSachMT,
            this.xrTableCell10,
            this.xrTableCell9,
            this.xrTableCell11,
            this.lblPhanLoaiXNK});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.72;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "Số danh mục miễn thuế";
            this.xrTableCell1.Weight = 1.3854167175292969;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Weight = 0.050000000000000031;
            // 
            // lblSoDanhMuc
            // 
            this.lblSoDanhMuc.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDanhMuc.Name = "lblSoDanhMuc";
            this.lblSoDanhMuc.StylePriority.UseFont = false;
            this.lblSoDanhMuc.Text = "NNNNNNNNN1NE";
            this.lblSoDanhMuc.Weight = 1.2312495422363281;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Weight = 0.049999999999999933;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "Mã số quản lý chung";
            this.xrTableCell4.Weight = 1.3197918701171876;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Weight = 0.050000000000000017;
            // 
            // lblSoQuanLyDanhSachMT
            // 
            this.lblSoQuanLyDanhSachMT.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoQuanLyDanhSachMT.Name = "lblSoQuanLyDanhSachMT";
            this.lblSoQuanLyDanhSachMT.StylePriority.UseFont = false;
            this.lblSoQuanLyDanhSachMT.Text = "XXXXXXXXX1XXXE";
            this.lblSoQuanLyDanhSachMT.Weight = 1.4265634155273437;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Weight = 0.049999999999999961;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "Phân loại xuất nhập khẩu";
            this.xrTableCell9.Weight = 1.5528643798828123;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Weight = 0.050000000000000044;
            // 
            // lblPhanLoaiXNK
            // 
            this.lblPhanLoaiXNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiXNK.Name = "lblPhanLoaiXNK";
            this.lblPhanLoaiXNK.StylePriority.UseFont = false;
            this.lblPhanLoaiXNK.Text = "X";
            this.lblPhanLoaiXNK.Weight = 0.46555328369140625;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell12,
            this.xrTableCell13,
            this.lblCoQuanHQ,
            this.xrTableCell17,
            this.xrTableCell18,
            this.xrTableCell19,
            this.lblNgayKhaiBao,
            this.xrTableCell21,
            this.lblThoiDiemKB});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 0.72;
            // 
            // xrTableCell12
            // 
            this.xrTableCell12.Name = "xrTableCell12";
            this.xrTableCell12.Text = "Mã cơ quan Hải quan tiếp nhận tờ khai";
            this.xrTableCell12.Weight = 2.18625;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Weight = 0.05;
            // 
            // lblCoQuanHQ
            // 
            this.lblCoQuanHQ.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCoQuanHQ.Name = "lblCoQuanHQ";
            this.lblCoQuanHQ.StylePriority.UseFont = false;
            this.lblCoQuanHQ.Text = "XXXXXE";
            this.lblCoQuanHQ.Weight = 0.87312500000000048;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.050000000000000037;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Text = "Ngày khai báo";
            this.xrTableCell18.Weight = 0.92708251953125009;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.Weight = 0.049999999999999864;
            // 
            // lblNgayKhaiBao
            // 
            this.lblNgayKhaiBao.Name = "lblNgayKhaiBao";
            this.lblNgayKhaiBao.Text = "dd/MM/yyyy";
            this.lblNgayKhaiBao.Weight = 0.91692810058593777;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.Weight = 0.049999999999999933;
            // 
            // lblThoiDiemKB
            // 
            this.lblThoiDiemKB.Name = "lblThoiDiemKB";
            this.lblThoiDiemKB.Text = "hh:mm:ss";
            this.lblThoiDiemKB.Weight = 2.5280535888671873;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell14,
            this.xrTableCell30,
            this.lblNgaySuaDoi,
            this.xrTableCell23,
            this.lblThoiDiemSuaDoi,
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell31,
            this.lblNgayHoanThanhKT,
            this.xrTableCell28,
            this.lblThoiDiemHoanThanhKT});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 0.72;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.Text = "Ngày sửa đổi";
            this.xrTableCell14.Weight = 0.98958312988281238;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Weight = 0.05;
            // 
            // lblNgaySuaDoi
            // 
            this.lblNgaySuaDoi.Name = "lblNgaySuaDoi";
            this.lblNgaySuaDoi.Text = "dd/MM/yyyy";
            this.lblNgaySuaDoi.Weight = 0.832083511352539;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.050000000000000405;
            // 
            // lblThoiDiemSuaDoi
            // 
            this.lblThoiDiemSuaDoi.Name = "lblThoiDiemSuaDoi";
            this.lblThoiDiemSuaDoi.Text = "hh:mm:ss";
            this.lblThoiDiemSuaDoi.Weight = 1.1877088928222657;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 0.049999694824218871;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.Text = "Ngày hoàn thành kiểm tra";
            this.xrTableCell26.Weight = 1.4770829010009765;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 0.050000000000000114;
            // 
            // lblNgayHoanThanhKT
            // 
            this.lblNgayHoanThanhKT.Name = "lblNgayHoanThanhKT";
            this.lblNgayHoanThanhKT.Text = "dd/MM/yyyy";
            this.lblNgayHoanThanhKT.Weight = 0.82656463623046883;
            // 
            // xrTableCell28
            // 
            this.xrTableCell28.Name = "xrTableCell28";
            this.xrTableCell28.Weight = 0.049998168945312393;
            // 
            // lblThoiDiemHoanThanhKT
            // 
            this.lblThoiDiemHoanThanhKT.Name = "lblThoiDiemHoanThanhKT";
            this.lblThoiDiemHoanThanhKT.Text = "hh:mm:ss";
            this.lblThoiDiemHoanThanhKT.Weight = 2.068418273925781;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell32,
            this.xrTableCell33,
            this.xrTableCell35,
            this.xrTableCell40,
            this.lblMaNguoiKhai});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.72;
            // 
            // xrTableCell32
            // 
            this.xrTableCell32.Name = "xrTableCell32";
            this.xrTableCell32.Text = "Người khai";
            this.xrTableCell32.Weight = 0.64999999999999991;
            // 
            // xrTableCell33
            // 
            this.xrTableCell33.Name = "xrTableCell33";
            this.xrTableCell33.Weight = 0.05;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.Text = "Mã";
            this.xrTableCell35.Weight = 0.28958305358886771;
            // 
            // xrTableCell40
            // 
            this.xrTableCell40.Name = "xrTableCell40";
            this.xrTableCell40.Weight = 0.050000000000000155;
            // 
            // lblMaNguoiKhai
            // 
            this.lblMaNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaNguoiKhai.Name = "lblMaNguoiKhai";
            this.lblMaNguoiKhai.StylePriority.UseFont = false;
            this.lblMaNguoiKhai.Text = "XXXXXXXXX1-XXE";
            this.lblMaNguoiKhai.Weight = 6.5918561553955062;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell34,
            this.xrTableCell37,
            this.xrTableCell38,
            this.lblTenNguoiKhai});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Weight = 1.4;
            // 
            // xrTableCell34
            // 
            this.xrTableCell34.Name = "xrTableCell34";
            this.xrTableCell34.Weight = 0.65000000000000013;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Text = "Tên";
            this.xrTableCell37.Weight = 0.33958305358886731;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Weight = 0.050000000000000155;
            // 
            // lblTenNguoiKhai
            // 
            this.lblTenNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenNguoiKhai.Name = "lblTenNguoiKhai";
            this.lblTenNguoiKhai.StylePriority.UseFont = false;
            this.lblTenNguoiKhai.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblTenNguoiKhai.Weight = 6.5918561553955062;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell36,
            this.xrTableCell42,
            this.xrTableCell43,
            this.lblDiaChiNguoiKhai});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 1.4;
            // 
            // xrTableCell36
            // 
            this.xrTableCell36.Name = "xrTableCell36";
            this.xrTableCell36.Weight = 0.5;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "Địa chỉ";
            this.xrTableCell42.Weight = 0.48958320617675788;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Weight = 0.050000076293945483;
            // 
            // lblDiaChiNguoiKhai
            // 
            this.lblDiaChiNguoiKhai.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaChiNguoiKhai.Name = "lblDiaChiNguoiKhai";
            this.lblDiaChiNguoiKhai.StylePriority.UseFont = false;
            this.lblDiaChiNguoiKhai.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaChiNguoiKhai.Weight = 6.59185592651367;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell47,
            this.lblSoDienThoaiNK});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 0.71999999999999986;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Weight = 0.22916671752929713;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Text = "Số điện thoại";
            this.xrTableCell46.Weight = 0.76041648864746092;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Weight = 0.050000228881836084;
            // 
            // lblSoDienThoaiNK
            // 
            this.lblSoDienThoaiNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoDienThoaiNK.Name = "lblSoDienThoaiNK";
            this.lblSoDienThoaiNK.StylePriority.UseFont = false;
            this.lblSoDienThoaiNK.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblSoDienThoaiNK.Weight = 6.59185577392578;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell51,
            this.lblThoiHanMienThue});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.71999999999999986;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Text = "Thời hạn miễn thuế";
            this.xrTableCell49.Weight = 1.039583435058594;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.020000000000000018;
            // 
            // lblThoiHanMienThue
            // 
            this.lblThoiHanMienThue.Name = "lblThoiHanMienThue";
            this.lblThoiHanMienThue.Text = "dd/MM/yyyy";
            this.lblThoiHanMienThue.Weight = 6.57185577392578;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell50,
            this.xrTableCell53,
            this.lblTenDuAnDauTu});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.71999999999999986;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "Tên dự án đầu tư";
            this.xrTableCell50.Weight = 0.98958297729492206;
            // 
            // xrTableCell53
            // 
            this.xrTableCell53.Name = "xrTableCell53";
            this.xrTableCell53.Weight = 0.050000228881836195;
            // 
            // lblTenDuAnDauTu
            // 
            this.lblTenDuAnDauTu.Font = new System.Drawing.Font("Times New Roman", 7.5F);
            this.lblTenDuAnDauTu.Name = "lblTenDuAnDauTu";
            this.lblTenDuAnDauTu.StylePriority.UseFont = false;
            this.lblTenDuAnDauTu.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWWE";
            this.lblTenDuAnDauTu.Weight = 6.5918560028076154;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell55,
            this.xrTableCell56,
            this.lblDiaDiemXayDungDuAn});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 1.4;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Text = "Địa điểm xây dựng dự án";
            this.xrTableCell55.Weight = 1.4354167175292969;
            // 
            // xrTableCell56
            // 
            this.xrTableCell56.Name = "xrTableCell56";
            this.xrTableCell56.Weight = 0.05000000000000001;
            // 
            // lblDiaDiemXayDungDuAn
            // 
            this.lblDiaDiemXayDungDuAn.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiaDiemXayDungDuAn.Name = "lblDiaDiemXayDungDuAn";
            this.lblDiaDiemXayDungDuAn.StylePriority.UseFont = false;
            this.lblDiaDiemXayDungDuAn.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDiaDiemXayDungDuAn.Weight = 6.1460224914550761;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell58,
            this.xrTableCell59,
            this.lblMucTieuDuAn});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 1.4;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Text = "Mục tiêu dự án";
            this.xrTableCell58.Weight = 1.4354167175292969;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 0.05000000000000001;
            // 
            // lblMucTieuDuAn
            // 
            this.lblMucTieuDuAn.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMucTieuDuAn.Name = "lblMucTieuDuAn";
            this.lblMucTieuDuAn.StylePriority.UseFont = false;
            this.lblMucTieuDuAn.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblMucTieuDuAn.Weight = 6.1460224914550761;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell61});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.71999999999999986;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Text = "Mã miễn thuế xuất nhập khẩu";
            this.xrTableCell61.Weight = 7.6314392089843732;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell65,
            this.lblMaMienGiamKhongChiuThueXNK,
            this.xrTableCell63,
            this.lblDieuKhoanMienGiam});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.71999999999999986;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Weight = 0.95392990112304665;
            // 
            // lblMaMienGiamKhongChiuThueXNK
            // 
            this.lblMaMienGiamKhongChiuThueXNK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaMienGiamKhongChiuThueXNK.Name = "lblMaMienGiamKhongChiuThueXNK";
            this.lblMaMienGiamKhongChiuThueXNK.StylePriority.UseFont = false;
            this.lblMaMienGiamKhongChiuThueXNK.Text = "XXXXE";
            this.lblMaMienGiamKhongChiuThueXNK.Weight = 0.66226325988769519;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.050000000000000044;
            // 
            // lblDieuKhoanMienGiam
            // 
            this.lblDieuKhoanMienGiam.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDieuKhoanMienGiam.Name = "lblDieuKhoanMienGiam";
            this.lblDieuKhoanMienGiam.StylePriority.UseFont = false;
            this.lblDieuKhoanMienGiam.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiam.Weight = 5.9652460479736309;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell66,
            this.xrTableCell68,
            this.lblPhamViDK});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 1.4;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Text = "Phạm vi đăng ký DMMT";
            this.xrTableCell66.Weight = 1.4354167938232418;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Weight = 0.049999847412109472;
            // 
            // lblPhamViDK
            // 
            this.lblPhamViDK.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhamViDK.Name = "lblPhamViDK";
            this.lblPhamViDK.StylePriority.UseFont = false;
            this.lblPhamViDK.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblPhamViDK.Weight = 6.1460225677490214;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell67,
            this.xrTableCell70,
            this.lblNgayDuKienXNK});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.71999999999999986;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Text = "Ngày dự kiến xuất nhập khẩu";
            this.xrTableCell67.Weight = 1.6161933135986324;
            // 
            // xrTableCell70
            // 
            this.xrTableCell70.Name = "xrTableCell70";
            this.xrTableCell70.Weight = 0.049999694824218843;
            // 
            // lblNgayDuKienXNK
            // 
            this.lblNgayDuKienXNK.Name = "lblNgayDuKienXNK";
            this.lblNgayDuKienXNK.Text = "dd/MM/yyyy";
            this.lblNgayDuKienXNK.Weight = 5.9652462005615217;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell72,
            this.xrTableCell73,
            this.lblGiayPhepDauTu_GCNSo,
            this.xrTableCell77,
            this.xrTableCell75,
            this.xrTableCell78,
            this.lblNgayChungNhan});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.71999999999999986;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Text = "Giấy phép đầu tư số";
            this.xrTableCell72.Weight = 1.4354167938232418;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Weight = 0.049999694824218816;
            // 
            // lblGiayPhepDauTu_GCNSo
            // 
            this.lblGiayPhepDauTu_GCNSo.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiayPhepDauTu_GCNSo.Name = "lblGiayPhepDauTu_GCNSo";
            this.lblGiayPhepDauTu_GCNSo.StylePriority.UseFont = false;
            this.lblGiayPhepDauTu_GCNSo.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblGiayPhepDauTu_GCNSo.Weight = 1.9429213905334466;
            // 
            // xrTableCell77
            // 
            this.xrTableCell77.Name = "xrTableCell77";
            this.xrTableCell77.Weight = 0.14148894309997556;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Text = "Ngày chứng nhận";
            this.xrTableCell75.Weight = 1.0789892864227291;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Weight = 0.05000005722045886;
            // 
            // lblNgayChungNhan
            // 
            this.lblNgayChungNhan.Name = "lblNgayChungNhan";
            this.lblNgayChungNhan.Text = "dd/MM/yyyy";
            this.lblNgayChungNhan.Weight = 2.932623043060302;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell86,
            this.xrTableCell79,
            this.xrTableCell80,
            this.lblCapBoi});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 1.5999999999999999;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.Weight = 0.71770839691162092;
            // 
            // xrTableCell79
            // 
            this.xrTableCell79.Name = "xrTableCell79";
            this.xrTableCell79.Text = "Cấp bởi";
            this.xrTableCell79.Weight = 0.71770839691162092;
            // 
            // xrTableCell80
            // 
            this.xrTableCell80.Name = "xrTableCell80";
            this.xrTableCell80.Weight = 0.049999694824218816;
            // 
            // lblCapBoi
            // 
            this.lblCapBoi.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCapBoi.Name = "lblCapBoi";
            this.lblCapBoi.StylePriority.UseFont = false;
            this.lblCapBoi.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblCapBoi.Weight = 6.1460227203369122;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell3,
            this.xrTableCell6,
            this.lblGiayPhepDauTu_GCNDieuChinhLan1,
            this.xrTableCell110,
            this.xrTableCell111,
            this.xrTableCell113,
            this.lblChungNhanDCSo1,
            this.xrTableCell114,
            this.xrTableCell112,
            this.xrTableCell115,
            this.lblNgayChungNhanDC1});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.7200000000000002;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "Giấy phép điều chỉnh lần";
            this.xrTableCell3.Weight = 1.4354164505004878;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Weight = 0.05000034332275393;
            // 
            // lblGiayPhepDauTu_GCNDieuChinhLan1
            // 
            this.lblGiayPhepDauTu_GCNDieuChinhLan1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiayPhepDauTu_GCNDieuChinhLan1.Name = "lblGiayPhepDauTu_GCNDieuChinhLan1";
            this.lblGiayPhepDauTu_GCNDieuChinhLan1.StylePriority.UseFont = false;
            this.lblGiayPhepDauTu_GCNDieuChinhLan1.Text = "X";
            this.lblGiayPhepDauTu_GCNDieuChinhLan1.Weight = 0.38624954223632824;
            // 
            // xrTableCell110
            // 
            this.xrTableCell110.Name = "xrTableCell110";
            this.xrTableCell110.Weight = 0.050000629425048571;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.Text = "Số";
            this.xrTableCell111.Weight = 0.26458333015441848;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 0.049999833106994407;
            // 
            // lblChungNhanDCSo1
            // 
            this.lblChungNhanDCSo1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChungNhanDCSo1.Name = "lblChungNhanDCSo1";
            this.lblChungNhanDCSo1.StylePriority.UseFont = false;
            this.lblChungNhanDCSo1.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblChungNhanDCSo1.Weight = 1.8502075052261353;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Weight = 0.15000000000000022;
            // 
            // xrTableCell112
            // 
            this.xrTableCell112.Name = "xrTableCell112";
            this.xrTableCell112.Text = "Ngày";
            this.xrTableCell112.Weight = 0.41235834121704074;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Weight = 0.049999971389770526;
            // 
            // lblNgayChungNhanDC1
            // 
            this.lblNgayChungNhanDC1.Name = "lblNgayChungNhanDC1";
            this.lblNgayChungNhanDC1.Text = "dd/MM/yyyy";
            this.lblNgayChungNhanDC1.Weight = 2.9326232624053947;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell20,
            this.xrTableCell22,
            this.xrTableCell24,
            this.lblDieuChinhBoi1});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 1.5999999999999999;
            // 
            // xrTableCell20
            // 
            this.xrTableCell20.Name = "xrTableCell20";
            this.xrTableCell20.Weight = 0.49999988555908192;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Text = "Điều chỉnh bởi";
            this.xrTableCell22.Weight = 0.93541690826415991;
            // 
            // xrTableCell24
            // 
            this.xrTableCell24.Name = "xrTableCell24";
            this.xrTableCell24.Weight = 0.049999694824218816;
            // 
            // lblDieuChinhBoi1
            // 
            this.lblDieuChinhBoi1.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDieuChinhBoi1.Name = "lblDieuChinhBoi1";
            this.lblDieuChinhBoi1.StylePriority.UseFont = false;
            this.lblDieuChinhBoi1.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDieuChinhBoi1.Weight = 6.1460227203369122;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell29,
            this.xrTableCell39,
            this.lblGiayPhepDauTu_GCNDieuChinhLan2,
            this.xrTableCell121,
            this.xrTableCell122,
            this.xrTableCell117,
            this.lblChungNhanDCSo2,
            this.xrTableCell116,
            this.xrTableCell119,
            this.xrTableCell118,
            this.lblNgayChungNhanDC2});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.7200000000000002;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Text = "Giấy phép điều chỉnh lần";
            this.xrTableCell29.Weight = 1.4354164886474605;
            // 
            // xrTableCell39
            // 
            this.xrTableCell39.Name = "xrTableCell39";
            this.xrTableCell39.Weight = 0.050000305175781162;
            // 
            // lblGiayPhepDauTu_GCNDieuChinhLan2
            // 
            this.lblGiayPhepDauTu_GCNDieuChinhLan2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiayPhepDauTu_GCNDieuChinhLan2.Name = "lblGiayPhepDauTu_GCNDieuChinhLan2";
            this.lblGiayPhepDauTu_GCNDieuChinhLan2.StylePriority.UseFont = false;
            this.lblGiayPhepDauTu_GCNDieuChinhLan2.Text = "X";
            this.lblGiayPhepDauTu_GCNDieuChinhLan2.Weight = 0.38624954223632812;
            // 
            // xrTableCell121
            // 
            this.xrTableCell121.Name = "xrTableCell121";
            this.xrTableCell121.Weight = 0.050000925064086765;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Text = "Số";
            this.xrTableCell122.Weight = 0.26458300113677968;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Weight = 0.049999756813049245;
            // 
            // lblChungNhanDCSo2
            // 
            this.lblChungNhanDCSo2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChungNhanDCSo2.Name = "lblChungNhanDCSo2";
            this.lblChungNhanDCSo2.StylePriority.UseFont = false;
            this.lblChungNhanDCSo2.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblChungNhanDCSo2.Weight = 1.8502079486846921;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Weight = 0.14999999999999986;
            // 
            // xrTableCell119
            // 
            this.xrTableCell119.Name = "xrTableCell119";
            this.xrTableCell119.Text = "Ngày";
            this.xrTableCell119.Weight = 0.41235780715942372;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Weight = 0.050000658035278311;
            // 
            // lblNgayChungNhanDC2
            // 
            this.lblNgayChungNhanDC2.Name = "lblNgayChungNhanDC2";
            this.lblNgayChungNhanDC2.Text = "dd/MM/yyyy";
            this.lblNgayChungNhanDC2.Weight = 2.9326227760314936;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell48,
            this.xrTableCell52,
            this.xrTableCell54,
            this.lblDieuChinhBoi2});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.Weight = 1.5999999999999999;
            // 
            // xrTableCell48
            // 
            this.xrTableCell48.Name = "xrTableCell48";
            this.xrTableCell48.Weight = 0.49999988555908192;
            // 
            // xrTableCell52
            // 
            this.xrTableCell52.Name = "xrTableCell52";
            this.xrTableCell52.Text = "Điều chỉnh bởi";
            this.xrTableCell52.Weight = 0.93541690826415991;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Weight = 0.049999694824218816;
            // 
            // lblDieuChinhBoi2
            // 
            this.lblDieuChinhBoi2.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDieuChinhBoi2.Name = "lblDieuChinhBoi2";
            this.lblDieuChinhBoi2.StylePriority.UseFont = false;
            this.lblDieuChinhBoi2.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDieuChinhBoi2.Weight = 6.1460227203369122;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell60,
            this.xrTableCell62,
            this.lblGiayPhepDauTu_GCNDieuChinhLan3,
            this.xrTableCell127,
            this.xrTableCell124,
            this.xrTableCell125,
            this.lblChungNhanDCSo3,
            this.xrTableCell128,
            this.xrTableCell126,
            this.xrTableCell141,
            this.lblNgayChungNhanDC3});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.72000000000000031;
            // 
            // xrTableCell60
            // 
            this.xrTableCell60.Name = "xrTableCell60";
            this.xrTableCell60.Text = "Giấy phép điều chỉnh lần";
            this.xrTableCell60.Weight = 1.4354164505004881;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Weight = 0.050000343322753826;
            // 
            // lblGiayPhepDauTu_GCNDieuChinhLan3
            // 
            this.lblGiayPhepDauTu_GCNDieuChinhLan3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiayPhepDauTu_GCNDieuChinhLan3.Name = "lblGiayPhepDauTu_GCNDieuChinhLan3";
            this.lblGiayPhepDauTu_GCNDieuChinhLan3.StylePriority.UseFont = false;
            this.lblGiayPhepDauTu_GCNDieuChinhLan3.Text = "X";
            this.lblGiayPhepDauTu_GCNDieuChinhLan3.Weight = 0.38624938964843752;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Weight = 0.050001230239867966;
            // 
            // xrTableCell124
            // 
            this.xrTableCell124.Name = "xrTableCell124";
            this.xrTableCell124.Text = "Số";
            this.xrTableCell124.Weight = 0.2645826816558835;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.Weight = 0.049999895095825232;
            // 
            // lblChungNhanDCSo3
            // 
            this.lblChungNhanDCSo3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChungNhanDCSo3.Name = "lblChungNhanDCSo3";
            this.lblChungNhanDCSo3.StylePriority.UseFont = false;
            this.lblChungNhanDCSo3.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblChungNhanDCSo3.Weight = 1.8502082538604734;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Weight = 0.14999999999999963;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Text = "Ngày";
            this.xrTableCell126.Weight = 0.41235773086547872;
            // 
            // xrTableCell141
            // 
            this.xrTableCell141.Name = "xrTableCell141";
            this.xrTableCell141.Weight = 0.05000058174133315;
            // 
            // lblNgayChungNhanDC3
            // 
            this.lblNgayChungNhanDC3.Name = "lblNgayChungNhanDC3";
            this.lblNgayChungNhanDC3.Text = "dd/MM/yyyy";
            this.lblNgayChungNhanDC3.Weight = 2.9326226520538321;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell71,
            this.xrTableCell74,
            this.xrTableCell76,
            this.lblDieuChinhBoi3});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 1.6;
            // 
            // xrTableCell71
            // 
            this.xrTableCell71.Name = "xrTableCell71";
            this.xrTableCell71.Weight = 0.49999988555908192;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Text = "Điều chỉnh bởi";
            this.xrTableCell74.Weight = 0.93541690826415991;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Weight = 0.049999694824218816;
            // 
            // lblDieuChinhBoi3
            // 
            this.lblDieuChinhBoi3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDieuChinhBoi3.Name = "lblDieuChinhBoi3";
            this.lblDieuChinhBoi3.StylePriority.UseFont = false;
            this.lblDieuChinhBoi3.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDieuChinhBoi3.Weight = 6.1460227203369122;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell84,
            this.xrTableCell85,
            this.lblGiayPhepDauTu_GCNDieuChinhLan4,
            this.xrTableCell133,
            this.xrTableCell130,
            this.xrTableCell134,
            this.lblChungNhanDCSo4,
            this.xrTableCell132,
            this.xrTableCell131,
            this.xrTableCell142,
            this.lblNgayChungNhanDC4});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.71999999999999986;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.Text = "Giấy phép điều chỉnh lần";
            this.xrTableCell84.Weight = 1.4354163742065427;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.Weight = 0.050000457763671846;
            // 
            // lblGiayPhepDauTu_GCNDieuChinhLan4
            // 
            this.lblGiayPhepDauTu_GCNDieuChinhLan4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiayPhepDauTu_GCNDieuChinhLan4.Name = "lblGiayPhepDauTu_GCNDieuChinhLan4";
            this.lblGiayPhepDauTu_GCNDieuChinhLan4.StylePriority.UseFont = false;
            this.lblGiayPhepDauTu_GCNDieuChinhLan4.Text = "X";
            this.lblGiayPhepDauTu_GCNDieuChinhLan4.Weight = 0.38624916076660165;
            // 
            // xrTableCell133
            // 
            this.xrTableCell133.Name = "xrTableCell133";
            this.xrTableCell133.Weight = 0.050001535415649223;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Text = "Số";
            this.xrTableCell130.Weight = 0.26458294868469223;
            // 
            // xrTableCell134
            // 
            this.xrTableCell134.Name = "xrTableCell134";
            this.xrTableCell134.Weight = 0.049999513626098446;
            // 
            // lblChungNhanDCSo4
            // 
            this.lblChungNhanDCSo4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChungNhanDCSo4.Name = "lblChungNhanDCSo4";
            this.lblChungNhanDCSo4.StylePriority.UseFont = false;
            this.lblChungNhanDCSo4.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblChungNhanDCSo4.Weight = 1.8502079486846921;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Weight = 0.15000000000000024;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Text = "Ngày";
            this.xrTableCell131.Weight = 0.41235864639282205;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Weight = 0.0499993610382079;
            // 
            // lblNgayChungNhanDC4
            // 
            this.lblNgayChungNhanDC4.Name = "lblNgayChungNhanDC4";
            this.lblNgayChungNhanDC4.Text = "dd/MM/yyyy";
            this.lblNgayChungNhanDC4.Weight = 2.9326232624053947;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell97,
            this.xrTableCell98,
            this.xrTableCell99,
            this.lblDieuChinhBoi4});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 1.6;
            // 
            // xrTableCell97
            // 
            this.xrTableCell97.Name = "xrTableCell97";
            this.xrTableCell97.Weight = 0.49999988555908192;
            // 
            // xrTableCell98
            // 
            this.xrTableCell98.Name = "xrTableCell98";
            this.xrTableCell98.Text = "Điều chỉnh bởi";
            this.xrTableCell98.Weight = 0.93541690826415991;
            // 
            // xrTableCell99
            // 
            this.xrTableCell99.Name = "xrTableCell99";
            this.xrTableCell99.Weight = 0.049999694824218816;
            // 
            // lblDieuChinhBoi4
            // 
            this.lblDieuChinhBoi4.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDieuChinhBoi4.Name = "lblDieuChinhBoi4";
            this.lblDieuChinhBoi4.StylePriority.UseFont = false;
            this.lblDieuChinhBoi4.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDieuChinhBoi4.Weight = 6.1460227203369122;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell101,
            this.xrTableCell102,
            this.lblGiayPhepDauTu_GCNDieuChinhLan5,
            this.xrTableCell137,
            this.xrTableCell136,
            this.xrTableCell138,
            this.lblChungNhanDCSo5,
            this.xrTableCell140,
            this.xrTableCell139,
            this.xrTableCell143,
            this.lblNgayChungNhanDC5});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.72000000000000075;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Text = "Giấy phép điều chỉnh lần";
            this.xrTableCell101.Weight = 1.4354162979125975;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Weight = 0.050000648498534916;
            // 
            // lblGiayPhepDauTu_GCNDieuChinhLan5
            // 
            this.lblGiayPhepDauTu_GCNDieuChinhLan5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGiayPhepDauTu_GCNDieuChinhLan5.Name = "lblGiayPhepDauTu_GCNDieuChinhLan5";
            this.lblGiayPhepDauTu_GCNDieuChinhLan5.StylePriority.UseFont = false;
            this.lblGiayPhepDauTu_GCNDieuChinhLan5.Text = "X";
            this.lblGiayPhepDauTu_GCNDieuChinhLan5.Weight = 0.38624954223632818;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Weight = 0.050000314712524196;
            // 
            // xrTableCell136
            // 
            this.xrTableCell136.Name = "xrTableCell136";
            this.xrTableCell136.Text = "Số";
            this.xrTableCell136.Weight = 0.2645832920074459;
            // 
            // xrTableCell138
            // 
            this.xrTableCell138.Name = "xrTableCell138";
            this.xrTableCell138.Weight = 0.050000200271606454;
            // 
            // lblChungNhanDCSo5
            // 
            this.lblChungNhanDCSo5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChungNhanDCSo5.Name = "lblChungNhanDCSo5";
            this.lblChungNhanDCSo5.StylePriority.UseFont = false;
            this.lblChungNhanDCSo5.Text = "XXXXXXXXX1XXXXXXXXXE";
            this.lblChungNhanDCSo5.Weight = 1.8502082538604732;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Weight = 0.15000000000000002;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Text = "Ngày";
            this.xrTableCell139.Weight = 0.412357425689697;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Weight = 0.04999997138977097;
            // 
            // lblNgayChungNhanDC5
            // 
            this.lblNgayChungNhanDC5.Name = "lblNgayChungNhanDC5";
            this.lblNgayChungNhanDC5.Text = "dd/MM/yyyy";
            this.lblNgayChungNhanDC5.Weight = 2.9326232624053943;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell105,
            this.xrTableCell106,
            this.xrTableCell107,
            this.lblDieuChinhBoi5});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 2.19166748046875;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.Weight = 0.49999988555908192;
            // 
            // xrTableCell106
            // 
            this.xrTableCell106.Name = "xrTableCell106";
            this.xrTableCell106.Text = "Điều chỉnh bởi";
            this.xrTableCell106.Weight = 0.93541690826415991;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 0.049999694824218816;
            // 
            // lblDieuChinhBoi5
            // 
            this.lblDieuChinhBoi5.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDieuChinhBoi5.Name = "lblDieuChinhBoi5";
            this.lblDieuChinhBoi5.StylePriority.UseFont = false;
            this.lblDieuChinhBoi5.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWWE";
            this.lblDieuChinhBoi5.Weight = 6.1460227203369122;
            // 
            // ToKhaiDanhMucMienThueXuatKhau_Page_1
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.PageHeader});
            this.Margins = new System.Drawing.Printing.Margins(26, 25, 9, 110);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.PageHeaderBand PageHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRLabel lblTongSoTrang;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDanhMuc;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell lblSoQuanLyDanhSachMT;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiXNK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell lblCoQuanHQ;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayKhaiBao;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell lblThoiDiemKB;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell lblNgaySuaDoi;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell lblThoiDiemSuaDoi;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayHoanThanhKT;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell28;
        private DevExpress.XtraReports.UI.XRTableCell lblThoiDiemHoanThanhKT;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell40;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell36;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaChiNguoiKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDienThoaiNK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell lblThoiHanMienThue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell53;
        private DevExpress.XtraReports.UI.XRTableCell lblTenDuAnDauTu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell56;
        private DevExpress.XtraReports.UI.XRTableCell lblDiaDiemXayDungDuAn;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell lblMucTieuDuAn;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell lblMaMienGiamKhongChiuThueXNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKhoanMienGiam;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell lblPhamViDK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell70;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDuKienXNK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell lblGiayPhepDauTu_GCNSo;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell77;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayChungNhan;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell79;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell80;
        private DevExpress.XtraReports.UI.XRTableCell lblCapBoi;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell lblGiayPhepDauTu_GCNDieuChinhLan1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell110;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell lblChungNhanDCSo1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell112;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayChungNhanDC1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell24;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuChinhBoi1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell39;
        private DevExpress.XtraReports.UI.XRTableCell lblGiayPhepDauTu_GCNDieuChinhLan2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell121;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell lblChungNhanDCSo2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell119;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayChungNhanDC2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuChinhBoi2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell60;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell lblGiayPhepDauTu_GCNDieuChinhLan3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell124;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell lblChungNhanDCSo3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell141;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayChungNhanDC3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell71;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuChinhBoi3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell lblGiayPhepDauTu_GCNDieuChinhLan4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell133;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell134;
        private DevExpress.XtraReports.UI.XRTableCell lblChungNhanDCSo4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayChungNhanDC4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell97;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell98;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell99;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuChinhBoi4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell lblGiayPhepDauTu_GCNDieuChinhLan5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell136;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell138;
        private DevExpress.XtraReports.UI.XRTableCell lblChungNhanDCSo5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayChungNhanDC5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell106;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuChinhBoi5;
    }
}
