﻿using System;
using System.Drawing;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.GC;
using Company.GC.BLL.KDT;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.GC.BLL;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Imaging;
namespace Company.Interface
{
    public partial class ChungTuForm : BaseForm
    {
        public ChungTuCollection collection;
        public ChungTu ctDetail;
        private int stt=0;
        private Image img;
        public string MaLoaiHinh= "";
        public ChungTuForm()
        {
            InitializeComponent();
            
        }
        private byte[] GetArrayFromImagen(Image imagen)
        {
  
            MemoryStream ms = new MemoryStream();
            imagen.Save(ms, ImageFormat.Jpeg);
            byte[] matriz = ms.ToArray();

            return matriz;
        }
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if(!cvError.IsValid )return;
            if (ctDetail == null)
            {
                ctDetail = new ChungTu();               
            }
            ctDetail.TenChungTu = uiComboBox1.Text;
            ctDetail.SoBanChinh = Convert.ToInt16(txtSoBanChinh.Text);
            ctDetail.SoBanSao = Convert.ToInt16(txtSoBanPhu.Text);
            if (ctDetail.SoBanChinh == 0 && ctDetail.SoBanSao == 0)
            {
                string msg = setText("Số bản chính hoặc số bản sao phải lớn hơn không.", "Number of original copy or duplicate must be greater than 0");
                ShowMessage(msg, false);
                txtSoBanChinh.Focus();
                return;
            }
            
            if (txtFileUpload.Text.Trim().Length > 0)
            {
                ctDetail.FileUpLoad = txtFileUpload.Text.Split('\\')[txtFileUpload.Text.Split('\\').Length - 1];
                ctDetail.NoiDung = GetArrayFromImagen(img);
                img = null;
            }
            try
            {
                ctDetail.LoaiCT = Convert.ToInt32(uiComboBox1.SelectedValue);
            }
            catch { ctDetail.LoaiCT = 5; }
            if (ctDetail.SoBanChinh == 0 && ctDetail.SoBanSao == 0)
            {
                showMsg("MSG_240240");
                //ShowMessage("Số bản chính hoặc số bản sao phải lớn hơn không.", false);
                txtSoBanChinh.Focus();
                return;
            }
           
            if (collection == null)
                collection = new ChungTuCollection();
            foreach (ChungTu ct in collection)
            {
                if (ct.LoaiCT == ctDetail.LoaiCT)
                {
                    if (ctDetail.LoaiCT <= 4)
                    {
                        showMsg("MSG_0203047");
                        //ShowMessage("Đã có chứng từ này rồi", false);
                        ctDetail = null;
                        return;
                    }
                    else
                    {
                        if (ct.TenChungTu == ctDetail.TenChungTu)
                        {
                            showMsg("MSG_0203047");
                            //ShowMessage("Đã có chứng từ này rồi", false);
                            ctDetail = null;
                            return;
                        }
                    }
                }
            }
            collection.Add(ctDetail);          
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (this.OpenType == OpenFormType.Edit)
            {
                collection.Add(ctDetail);
            }
            this.Close();
        }

        private void ChungTuForm_Load(object sender, EventArgs e)
        {
            if (this.MaLoaiHinh.Substring(0, 1) == "X")
            {
                uiComboBox1.Items.RemoveAt(1);
                uiComboBox1.Items.RemoveAt(2);
            }
            if (this.OpenType==OpenFormType.Edit)
            {
                uiComboBox1.Text = ctDetail.TenChungTu;
                txtFileUpload.Text = ctDetail.FileUpLoad;
                txtSoBanChinh.Text = ctDetail.SoBanChinh.ToString();
                txtSoBanPhu.Text = ctDetail.SoBanSao.ToString();
                btnAddNew.Text = "Sửa";
            }
            else
                if (this.OpenType == OpenFormType.View)
                {
                    btnAddNew.Visible = false;
                }            
        }

        private void txtFileUpload_ButtonClick(object sender, EventArgs e)
        {
            DialogResult kq = openFileDialog1.ShowDialog();
            if (kq == DialogResult.OK)
            {
                txtFileUpload.Text = openFileDialog1.FileName;
                img = Image.FromStream(openFileDialog1.OpenFile());
            }
        }

        private void txtSoBanPhu_Click(object sender, EventArgs e)
        {

        }

     
    }
}
