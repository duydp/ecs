using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.Interface.SXXK.BangKeThanhLy;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK08Form : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public BKNPLXuatGiaCongCollection bkCollection = new BKNPLXuatGiaCongCollection();
        private string MaLoaiHinhNhap;
        private string MaLoaiHinhXuat;
        private string MaHaiQuanNhap;
        private string MaHaiQuanXuat;
        private string DVT_ID;
        private DateTime NgayDangKy;
        private DateTime NgayDangKyXuat;
        public BK08Form()
        {
            InitializeComponent();
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (this.bkCollection.Count == 0)
            {
                ShowMessage("Bạn chưa nhập NPL xuất qua loại hình XGC.", false);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (this.HSTL.getBKNPLXuatGiaCong() < 0)
                {
                    BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                    bk.MaterID = this.HSTL.ID;
                    bk.STTHang = this.HSTL.BKCollection.Count + 1;
                    bk.MaBangKe = "DTLNPLXGC";
                    bk.TenBangKe = "DTLNPLXGC";
                    bk.bkNPLXGCCollection = this.bkCollection;
                    this.HSTL.BKCollection.Add(bk);
                }
                else
                {
                    this.HSTL.BKCollection[this.HSTL.getBKNPLXuatGiaCong()].bkNPLXGCCollection = this.bkCollection;
                }
                this.HSTL.BKCollection[this.HSTL.getBKNPLXuatGiaCong()].InsertUpdate_BKNPLXGC(this.bkCollection);
                Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Lỗi lưu bảng kê: " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void BK08Form_Load(object sender, EventArgs e)
        {
            txtSoLuongXuat.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            gridEX2.Tables[0].Columns["LuongXuat"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            if (this.HSTL.TrangThaiThanhKhoan == 401)
            {
                cmdAdd.Enabled = cmdSave.Enabled = false;
                gridEX2.AllowDelete = InheritableBoolean.False;
                gridEX2.AllowEdit = InheritableBoolean.False;
            }
            gridEX2.DataSource = this.bkCollection;
        }

        private void txtToKhaiNhap_ButtonClick(object sender, EventArgs e)
        {
            int id = this.HSTL.getBKToKhaiNhap();
            DSBangKeToKhaiNhapForm f = new DSBangKeToKhaiNhapForm();
            f.BangKeHSTL_ID = this.HSTL.BKCollection[id].ID;
            f.ShowDialog(this);
            if (f.SoToKhai == 0) return;
            txtToKhaiNhap.Text = Convert.ToString(f.SoToKhai);
            txtLoaiHinhNhap.Text = LoaiHinhMauDich_GetTenVT((object)f.MaLoaiHinh);
            txtNamDKNhap.Text = f.NgayDangKy.ToString("dd/MM/yyyy");
            txtMaNPL.Text = f.MaNPL;
            txtTenNPL.Text = f.TenNPL;
            this.MaHaiQuanNhap = f.MaHaiQuan;
            this.MaLoaiHinhNhap = f.MaLoaiHinh;
            this.DVT_ID = f.DVT_ID;
            this.NgayDangKy = f.NgayDangKy;
        }

        private void txtToKhaiXGC_ButtonClick(object sender, EventArgs e)
        {
            int id = this.HSTL.getBKToKhaiXuat();
            DataSet ds = new BKToKhaiXuat().getBKToKhaiXuatGC(this.HSTL.BKCollection[id].ID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DSToKhaiXuatGCForm f = new DSToKhaiXuatGCForm();
                f.BangKeHSTL_ID = this.HSTL.BKCollection[id].ID;
                f.ds = ds;
                f.ShowDialog(this);
                if (f.SoToKhai == 0) return;
                txtToKhaiXGC.Text = Convert.ToString(f.SoToKhai);
                txtLoaiHinhXGC.Text = LoaiHinhMauDich_GetTenVT((object)f.MaLoaiHinh);
                txtNamDKXGC.Text = f.NgayDangKy.ToString("dd/MM/yyyy");
                this.MaLoaiHinhXuat = f.MaLoaiHinh;
                this.MaHaiQuanXuat = f.MaHaiQuan;
                this.NgayDangKyXuat = f.NgayDangKy;
            }
            else
            {
                // ShowMessage("Không có tờ khai xuất gia công trong danh sách bảng kê tờ khai xuất thanh lý!", false);
                MLMessages("Không có tờ khai xuất gia công trong danh sách bảng kê tờ khai xuất thanh lý!", "MSG_THK81", "", false);
            }
        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            BKNPLXuatGiaCong bk = new BKNPLXuatGiaCong();
            bk.SoToKhai = Convert.ToInt32(txtToKhaiNhap.Text);
            bk.MaLoaiHinh = this.MaLoaiHinhNhap;
            bk.NamDangKy = (Int16)Convert.ToDateTime(txtNamDKNhap.Text).Year;
            bk.MaHaiQuan = this.MaHaiQuanNhap;
            bk.NgayDangKy = this.NgayDangKy;
            bk.SoToKhaiXuat = Convert.ToInt32(txtToKhaiXGC.Text);
            bk.MaLoaiHinhXuat = this.MaLoaiHinhXuat;
            bk.NamDangKyXuat = (Int16)Convert.ToDateTime(txtNamDKXGC.Text).Year;
            bk.MaHaiQuanXuat = this.MaHaiQuanXuat;
            bk.NgayDangKyXuat = this.NgayDangKyXuat;
            bk.MaNPL = txtMaNPL.Text;
            bk.TenNPL = txtTenNPL.Text;
            bk.DVT_ID = this.DVT_ID;
            bk.TenDVT = DonViTinh_GetName(this.DVT_ID);
            bk.LuongXuat = Convert.ToDecimal(txtSoLuongXuat.Value);
            this.bkCollection.Add(bk);
            gridEX2.Refetch();
            this.ClearData();
        }
        private void ClearData()
        {
            txtToKhaiNhap.Text = "";
            txtLoaiHinhNhap.Text = "";
            txtNamDKNhap.Text = "";
            txtMaNPL.Text = "";
            txtTenNPL.Text = "";
            txtToKhaiXGC.Text = "";
            txtLoaiHinhXGC.Text = "";
            txtNamDKXGC.Text = "";
            txtSoLuongXuat.Value = 0;
        }

        private void gridEX2_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                e.Row.Cells["DVT_ID"].Text = DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
                e.Row.Cells["SoToKhaiXuat"].Text = e.Row.Cells["SoToKhaiXuat"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinhXuat"].Value) + "/" + e.Row.Cells["NamDangKyXuat"].Text;

            }
        }

        private void txtNamDKNhap_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}