-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TIA_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TIA_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Insert]
	@SoToKhai numeric(18, 0),
	@CoBaoNhapKhauXuatKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@MaNguoiKhaiDauTien varchar(5),
	@TenNguoiKhaiDauTien nvarchar(50),
	@MaNguoiXuatNhapKhau varchar(13),
	@TenNguoiXuatNhapKhau nvarchar(100),
	@ThoiHanTaiXuatNhap datetime,
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TIA]
(
	[SoToKhai],
	[CoBaoNhapKhauXuatKhau],
	[CoQuanHaiQuan],
	[MaNguoiKhaiDauTien],
	[TenNguoiKhaiDauTien],
	[MaNguoiXuatNhapKhau],
	[TenNguoiXuatNhapKhau],
	[ThoiHanTaiXuatNhap],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@SoToKhai,
	@CoBaoNhapKhauXuatKhau,
	@CoQuanHaiQuan,
	@MaNguoiKhaiDauTien,
	@TenNguoiKhaiDauTien,
	@MaNguoiXuatNhapKhau,
	@TenNguoiXuatNhapKhau,
	@ThoiHanTaiXuatNhap,
	@TrangThaiXuLy,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Update]
	@ID bigint,
	@SoToKhai numeric(18, 0),
	@CoBaoNhapKhauXuatKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@MaNguoiKhaiDauTien varchar(5),
	@TenNguoiKhaiDauTien nvarchar(50),
	@MaNguoiXuatNhapKhau varchar(13),
	@TenNguoiXuatNhapKhau nvarchar(100),
	@ThoiHanTaiXuatNhap datetime,
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TIA]
SET
	[SoToKhai] = @SoToKhai,
	[CoBaoNhapKhauXuatKhau] = @CoBaoNhapKhauXuatKhau,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[MaNguoiKhaiDauTien] = @MaNguoiKhaiDauTien,
	[TenNguoiKhaiDauTien] = @TenNguoiKhaiDauTien,
	[MaNguoiXuatNhapKhau] = @MaNguoiXuatNhapKhau,
	[TenNguoiXuatNhapKhau] = @TenNguoiXuatNhapKhau,
	[ThoiHanTaiXuatNhap] = @ThoiHanTaiXuatNhap,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_InsertUpdate]
	@ID bigint,
	@SoToKhai numeric(18, 0),
	@CoBaoNhapKhauXuatKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@MaNguoiKhaiDauTien varchar(5),
	@TenNguoiKhaiDauTien nvarchar(50),
	@MaNguoiXuatNhapKhau varchar(13),
	@TenNguoiXuatNhapKhau nvarchar(100),
	@ThoiHanTaiXuatNhap datetime,
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TIA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TIA] 
		SET
			[SoToKhai] = @SoToKhai,
			[CoBaoNhapKhauXuatKhau] = @CoBaoNhapKhauXuatKhau,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[MaNguoiKhaiDauTien] = @MaNguoiKhaiDauTien,
			[TenNguoiKhaiDauTien] = @TenNguoiKhaiDauTien,
			[MaNguoiXuatNhapKhau] = @MaNguoiXuatNhapKhau,
			[TenNguoiXuatNhapKhau] = @TenNguoiXuatNhapKhau,
			[ThoiHanTaiXuatNhap] = @ThoiHanTaiXuatNhap,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TIA]
		(
			[SoToKhai],
			[CoBaoNhapKhauXuatKhau],
			[CoQuanHaiQuan],
			[MaNguoiKhaiDauTien],
			[TenNguoiKhaiDauTien],
			[MaNguoiXuatNhapKhau],
			[TenNguoiXuatNhapKhau],
			[ThoiHanTaiXuatNhap],
			[TrangThaiXuLy],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@SoToKhai,
			@CoBaoNhapKhauXuatKhau,
			@CoQuanHaiQuan,
			@MaNguoiKhaiDauTien,
			@TenNguoiKhaiDauTien,
			@MaNguoiXuatNhapKhau,
			@TenNguoiXuatNhapKhau,
			@ThoiHanTaiXuatNhap,
			@TrangThaiXuLy,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TIA]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TIA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[CoBaoNhapKhauXuatKhau],
	[CoQuanHaiQuan],
	[MaNguoiKhaiDauTien],
	[TenNguoiKhaiDauTien],
	[MaNguoiXuatNhapKhau],
	[TenNguoiXuatNhapKhau],
	[ThoiHanTaiXuatNhap],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TIA]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoToKhai],
	[CoBaoNhapKhauXuatKhau],
	[CoQuanHaiQuan],
	[MaNguoiKhaiDauTien],
	[TenNguoiKhaiDauTien],
	[MaNguoiXuatNhapKhau],
	[TenNguoiXuatNhapKhau],
	[ThoiHanTaiXuatNhap],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TIA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TIA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, October 02, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TIA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoToKhai],
	[CoBaoNhapKhauXuatKhau],
	[CoQuanHaiQuan],
	[MaNguoiKhaiDauTien],
	[TenNguoiKhaiDauTien],
	[MaNguoiXuatNhapKhau],
	[TenNguoiXuatNhapKhau],
	[ThoiHanTaiXuatNhap],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TIA]	

GO

