﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Chức năng khai báo
    /// </summary>
    [Serializable]
    [XmlRoot("Declaration")]
    public class SubjectBase : IssueBase
    {
        /// <summary>
        /// Loại chứng từ
        /// 929	Tờ khai nhập khẩu thương mại ( KD )
        /// 930	Tờ khai xuất khẩu thương mại ( KD ),         
        /// 100 Nguyên phụ liệu SXXK
        /// 101 Sản phẩm SXXK
        /// 103 Định mức SXXK
        /// 931	Tờ khai nhập khẩu SXXK
        /// 932	Tờ khai xuất khẩu SXXK, 
        /// 601	Hợp đồng Gia công
        /// 602	Phụ kiện hợp đồng Gia công
        /// 603	Định mức sản phẩm Gia công
        /// 935	Tờ khai nhập khẩu Gia công
        /// 936	Tờ khai xuất khẩu Gia công
        /// 939	Tờ khai điện tử đơn giản
        /// 940	Tờ khai điện tử tháng
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        
    }
}
