using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_GiayPhep_SFA : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaNguoiKhai { set; get; }
		public string TenNguoiKhai { set; get; }
		public string MaDonViCapPhep { set; get; }
		public string TenDonViCapPhep { set; get; }
		public decimal SoDonXinCapPhep { set; get; }
		public int ChucNangChungTu { set; get; }
		public string LoaiGiayPhep { set; get; }
		public string MaThuongNhanXuatKhau { set; get; }
		public string TenThuongNhanXuatKhau { set; get; }
		public string MaBuuChinhXuatKhau { set; get; }
		public string SoNhaTenDuongXuatKhau { set; get; }
		public string PhuongXaXuatKhau { set; get; }
		public string QuanHuyenXuatKhau { set; get; }
		public string TinhThanhPhoXuatKhau { set; get; }
		public string MaQuocGiaXuatKhau { set; get; }
		public string SoDienThoaiXuatKhau { set; get; }
		public string SoFaxXuatKhau { set; get; }
		public string EmailXuatKhau { set; get; }
		public string SoHopDong { set; get; }
		public string SoVanDon { set; get; }
		public string MaBenDi { set; get; }
		public string TenBenDi { set; get; }
		public string MaThuongNhanNhapKhau { set; get; }
		public string TenThuongNhanNhapKhau { set; get; }
		public string MaBuuChinhNhapKhau { set; get; }
		public string SoNhaTenDuongNhapKhau { set; get; }
		public string MaQuocGiaNhapKhau { set; get; }
		public string SoDienThoaiNhapKhau { set; get; }
		public string SoFaxNhapKhau { set; get; }
		public string EmailNhapKhau { set; get; }
		public string MaBenDen { set; get; }
		public string TenBenDen { set; get; }
		public DateTime ThoiGianNhapKhauDuKien { set; get; }
		public decimal GiaTriHangHoa { set; get; }
		public string DonViTienTe { set; get; }
		public string DiaDiemTapKetHangHoa { set; get; }
		public DateTime ThoiGianKiemTra { set; get; }
		public string DiaDiemKiemTra { set; get; }
		public string HoSoLienQuan { set; get; }
		public string GhiChu { set; get; }
		public string DaiDienThuongNhanNhapKhau { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		public string MaKetQuaXuLy { set; get; }
		public string KetQuaXuLySoGiayPhep { set; get; }
		public DateTime KetQuaXuLyNgayCap { set; get; }
		public DateTime KetQuaXuLyHieuLucTuNgay { set; get; }
		public DateTime KetQuaXuLyHieuLucDenNgay { set; get; }
		public string GhiChuDanhChoCoQuanCapPhep { set; get; }
		public string DaiDienCoQuanKiemTra { set; get; }
		public DateTime NgayKhaiBao { set; get; }
		public string HoSoKemTheo_1 { set; get; }
		public string HoSoKemTheo_2 { set; get; }
		public string HoSoKemTheo_3 { set; get; }
		public string HoSoKemTheo_4 { set; get; }
		public string HoSoKemTheo_5 { set; get; }
		public string HoSoKemTheo_6 { set; get; }
		public string HoSoKemTheo_7 { set; get; }
		public string HoSoKemTheo_8 { set; get; }
		public string HoSoKemTheo_9 { set; get; }
		public string HoSoKemTheo_10 { set; get; }
		public int TrangThaiXuLy { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_GiayPhep_SFA> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_GiayPhep_SFA> collection = new List<KDT_VNACC_GiayPhep_SFA>();
			while (reader.Read())
			{
				KDT_VNACC_GiayPhep_SFA entity = new KDT_VNACC_GiayPhep_SFA();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiKhai"))) entity.MaNguoiKhai = reader.GetString(reader.GetOrdinal("MaNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiKhai"))) entity.TenNguoiKhai = reader.GetString(reader.GetOrdinal("TenNguoiKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonViCapPhep"))) entity.MaDonViCapPhep = reader.GetString(reader.GetOrdinal("MaDonViCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonViCapPhep"))) entity.TenDonViCapPhep = reader.GetString(reader.GetOrdinal("TenDonViCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDonXinCapPhep"))) entity.SoDonXinCapPhep = reader.GetDecimal(reader.GetOrdinal("SoDonXinCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChucNangChungTu"))) entity.ChucNangChungTu = reader.GetInt32(reader.GetOrdinal("ChucNangChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiGiayPhep"))) entity.LoaiGiayPhep = reader.GetString(reader.GetOrdinal("LoaiGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaThuongNhanXuatKhau"))) entity.MaThuongNhanXuatKhau = reader.GetString(reader.GetOrdinal("MaThuongNhanXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenThuongNhanXuatKhau"))) entity.TenThuongNhanXuatKhau = reader.GetString(reader.GetOrdinal("TenThuongNhanXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhXuatKhau"))) entity.MaBuuChinhXuatKhau = reader.GetString(reader.GetOrdinal("MaBuuChinhXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNhaTenDuongXuatKhau"))) entity.SoNhaTenDuongXuatKhau = reader.GetString(reader.GetOrdinal("SoNhaTenDuongXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhuongXaXuatKhau"))) entity.PhuongXaXuatKhau = reader.GetString(reader.GetOrdinal("PhuongXaXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuanHuyenXuatKhau"))) entity.QuanHuyenXuatKhau = reader.GetString(reader.GetOrdinal("QuanHuyenXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhThanhPhoXuatKhau"))) entity.TinhThanhPhoXuatKhau = reader.GetString(reader.GetOrdinal("TinhThanhPhoXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaQuocGiaXuatKhau"))) entity.MaQuocGiaXuatKhau = reader.GetString(reader.GetOrdinal("MaQuocGiaXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiXuatKhau"))) entity.SoDienThoaiXuatKhau = reader.GetString(reader.GetOrdinal("SoDienThoaiXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoFaxXuatKhau"))) entity.SoFaxXuatKhau = reader.GetString(reader.GetOrdinal("SoFaxXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("EmailXuatKhau"))) entity.EmailXuatKhau = reader.GetString(reader.GetOrdinal("EmailXuatKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBenDi"))) entity.MaBenDi = reader.GetString(reader.GetOrdinal("MaBenDi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenBenDi"))) entity.TenBenDi = reader.GetString(reader.GetOrdinal("TenBenDi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaThuongNhanNhapKhau"))) entity.MaThuongNhanNhapKhau = reader.GetString(reader.GetOrdinal("MaThuongNhanNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenThuongNhanNhapKhau"))) entity.TenThuongNhanNhapKhau = reader.GetString(reader.GetOrdinal("TenThuongNhanNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhNhapKhau"))) entity.MaBuuChinhNhapKhau = reader.GetString(reader.GetOrdinal("MaBuuChinhNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoNhaTenDuongNhapKhau"))) entity.SoNhaTenDuongNhapKhau = reader.GetString(reader.GetOrdinal("SoNhaTenDuongNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaQuocGiaNhapKhau"))) entity.MaQuocGiaNhapKhau = reader.GetString(reader.GetOrdinal("MaQuocGiaNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiNhapKhau"))) entity.SoDienThoaiNhapKhau = reader.GetString(reader.GetOrdinal("SoDienThoaiNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoFaxNhapKhau"))) entity.SoFaxNhapKhau = reader.GetString(reader.GetOrdinal("SoFaxNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("EmailNhapKhau"))) entity.EmailNhapKhau = reader.GetString(reader.GetOrdinal("EmailNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBenDen"))) entity.MaBenDen = reader.GetString(reader.GetOrdinal("MaBenDen"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenBenDen"))) entity.TenBenDen = reader.GetString(reader.GetOrdinal("TenBenDen"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianNhapKhauDuKien"))) entity.ThoiGianNhapKhauDuKien = reader.GetDateTime(reader.GetOrdinal("ThoiGianNhapKhauDuKien"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiaTriHangHoa"))) entity.GiaTriHangHoa = reader.GetDecimal(reader.GetOrdinal("GiaTriHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTienTe"))) entity.DonViTienTe = reader.GetString(reader.GetOrdinal("DonViTienTe"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemTapKetHangHoa"))) entity.DiaDiemTapKetHangHoa = reader.GetString(reader.GetOrdinal("DiaDiemTapKetHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianKiemTra"))) entity.ThoiGianKiemTra = reader.GetDateTime(reader.GetOrdinal("ThoiGianKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemKiemTra"))) entity.DiaDiemKiemTra = reader.GetString(reader.GetOrdinal("DiaDiemKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoLienQuan"))) entity.HoSoLienQuan = reader.GetString(reader.GetOrdinal("HoSoLienQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaiDienThuongNhanNhapKhau"))) entity.DaiDienThuongNhanNhapKhau = reader.GetString(reader.GetOrdinal("DaiDienThuongNhanNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKetQuaXuLy"))) entity.MaKetQuaXuLy = reader.GetString(reader.GetOrdinal("MaKetQuaXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLySoGiayPhep"))) entity.KetQuaXuLySoGiayPhep = reader.GetString(reader.GetOrdinal("KetQuaXuLySoGiayPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyNgayCap"))) entity.KetQuaXuLyNgayCap = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyNgayCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyHieuLucTuNgay"))) entity.KetQuaXuLyHieuLucTuNgay = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyHieuLucTuNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaXuLyHieuLucDenNgay"))) entity.KetQuaXuLyHieuLucDenNgay = reader.GetDateTime(reader.GetOrdinal("KetQuaXuLyHieuLucDenNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuDanhChoCoQuanCapPhep"))) entity.GhiChuDanhChoCoQuanCapPhep = reader.GetString(reader.GetOrdinal("GhiChuDanhChoCoQuanCapPhep"));
				if (!reader.IsDBNull(reader.GetOrdinal("DaiDienCoQuanKiemTra"))) entity.DaiDienCoQuanKiemTra = reader.GetString(reader.GetOrdinal("DaiDienCoQuanKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKhaiBao"))) entity.NgayKhaiBao = reader.GetDateTime(reader.GetOrdinal("NgayKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_1"))) entity.HoSoKemTheo_1 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_1"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_2"))) entity.HoSoKemTheo_2 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_2"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_3"))) entity.HoSoKemTheo_3 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_3"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_4"))) entity.HoSoKemTheo_4 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_4"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_5"))) entity.HoSoKemTheo_5 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_5"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_6"))) entity.HoSoKemTheo_6 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_6"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_7"))) entity.HoSoKemTheo_7 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_7"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_8"))) entity.HoSoKemTheo_8 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_8"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_9"))) entity.HoSoKemTheo_9 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_9"));
				if (!reader.IsDBNull(reader.GetOrdinal("HoSoKemTheo_10"))) entity.HoSoKemTheo_10 = reader.GetString(reader.GetOrdinal("HoSoKemTheo_10"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_GiayPhep_SFA> collection, long id)
        {
            foreach (KDT_VNACC_GiayPhep_SFA item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_GiayPhep_SFA VALUES(@MaNguoiKhai, @TenNguoiKhai, @MaDonViCapPhep, @TenDonViCapPhep, @SoDonXinCapPhep, @ChucNangChungTu, @LoaiGiayPhep, @MaThuongNhanXuatKhau, @TenThuongNhanXuatKhau, @MaBuuChinhXuatKhau, @SoNhaTenDuongXuatKhau, @PhuongXaXuatKhau, @QuanHuyenXuatKhau, @TinhThanhPhoXuatKhau, @MaQuocGiaXuatKhau, @SoDienThoaiXuatKhau, @SoFaxXuatKhau, @EmailXuatKhau, @SoHopDong, @SoVanDon, @MaBenDi, @TenBenDi, @MaThuongNhanNhapKhau, @TenThuongNhanNhapKhau, @MaBuuChinhNhapKhau, @SoNhaTenDuongNhapKhau, @MaQuocGiaNhapKhau, @SoDienThoaiNhapKhau, @SoFaxNhapKhau, @EmailNhapKhau, @MaBenDen, @TenBenDen, @ThoiGianNhapKhauDuKien, @GiaTriHangHoa, @DonViTienTe, @DiaDiemTapKetHangHoa, @ThoiGianKiemTra, @DiaDiemKiemTra, @HoSoLienQuan, @GhiChu, @DaiDienThuongNhanNhapKhau, @Notes, @InputMessageID, @MessageTag, @IndexTag, @MaKetQuaXuLy, @KetQuaXuLySoGiayPhep, @KetQuaXuLyNgayCap, @KetQuaXuLyHieuLucTuNgay, @KetQuaXuLyHieuLucDenNgay, @GhiChuDanhChoCoQuanCapPhep, @DaiDienCoQuanKiemTra, @NgayKhaiBao, @HoSoKemTheo_1, @HoSoKemTheo_2, @HoSoKemTheo_3, @HoSoKemTheo_4, @HoSoKemTheo_5, @HoSoKemTheo_6, @HoSoKemTheo_7, @HoSoKemTheo_8, @HoSoKemTheo_9, @HoSoKemTheo_10, @TrangThaiXuLy)";
            string update = "UPDATE t_KDT_VNACC_GiayPhep_SFA SET MaNguoiKhai = @MaNguoiKhai, TenNguoiKhai = @TenNguoiKhai, MaDonViCapPhep = @MaDonViCapPhep, TenDonViCapPhep = @TenDonViCapPhep, SoDonXinCapPhep = @SoDonXinCapPhep, ChucNangChungTu = @ChucNangChungTu, LoaiGiayPhep = @LoaiGiayPhep, MaThuongNhanXuatKhau = @MaThuongNhanXuatKhau, TenThuongNhanXuatKhau = @TenThuongNhanXuatKhau, MaBuuChinhXuatKhau = @MaBuuChinhXuatKhau, SoNhaTenDuongXuatKhau = @SoNhaTenDuongXuatKhau, PhuongXaXuatKhau = @PhuongXaXuatKhau, QuanHuyenXuatKhau = @QuanHuyenXuatKhau, TinhThanhPhoXuatKhau = @TinhThanhPhoXuatKhau, MaQuocGiaXuatKhau = @MaQuocGiaXuatKhau, SoDienThoaiXuatKhau = @SoDienThoaiXuatKhau, SoFaxXuatKhau = @SoFaxXuatKhau, EmailXuatKhau = @EmailXuatKhau, SoHopDong = @SoHopDong, SoVanDon = @SoVanDon, MaBenDi = @MaBenDi, TenBenDi = @TenBenDi, MaThuongNhanNhapKhau = @MaThuongNhanNhapKhau, TenThuongNhanNhapKhau = @TenThuongNhanNhapKhau, MaBuuChinhNhapKhau = @MaBuuChinhNhapKhau, SoNhaTenDuongNhapKhau = @SoNhaTenDuongNhapKhau, MaQuocGiaNhapKhau = @MaQuocGiaNhapKhau, SoDienThoaiNhapKhau = @SoDienThoaiNhapKhau, SoFaxNhapKhau = @SoFaxNhapKhau, EmailNhapKhau = @EmailNhapKhau, MaBenDen = @MaBenDen, TenBenDen = @TenBenDen, ThoiGianNhapKhauDuKien = @ThoiGianNhapKhauDuKien, GiaTriHangHoa = @GiaTriHangHoa, DonViTienTe = @DonViTienTe, DiaDiemTapKetHangHoa = @DiaDiemTapKetHangHoa, ThoiGianKiemTra = @ThoiGianKiemTra, DiaDiemKiemTra = @DiaDiemKiemTra, HoSoLienQuan = @HoSoLienQuan, GhiChu = @GhiChu, DaiDienThuongNhanNhapKhau = @DaiDienThuongNhanNhapKhau, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, MaKetQuaXuLy = @MaKetQuaXuLy, KetQuaXuLySoGiayPhep = @KetQuaXuLySoGiayPhep, KetQuaXuLyNgayCap = @KetQuaXuLyNgayCap, KetQuaXuLyHieuLucTuNgay = @KetQuaXuLyHieuLucTuNgay, KetQuaXuLyHieuLucDenNgay = @KetQuaXuLyHieuLucDenNgay, GhiChuDanhChoCoQuanCapPhep = @GhiChuDanhChoCoQuanCapPhep, DaiDienCoQuanKiemTra = @DaiDienCoQuanKiemTra, NgayKhaiBao = @NgayKhaiBao, HoSoKemTheo_1 = @HoSoKemTheo_1, HoSoKemTheo_2 = @HoSoKemTheo_2, HoSoKemTheo_3 = @HoSoKemTheo_3, HoSoKemTheo_4 = @HoSoKemTheo_4, HoSoKemTheo_5 = @HoSoKemTheo_5, HoSoKemTheo_6 = @HoSoKemTheo_6, HoSoKemTheo_7 = @HoSoKemTheo_7, HoSoKemTheo_8 = @HoSoKemTheo_8, HoSoKemTheo_9 = @HoSoKemTheo_9, HoSoKemTheo_10 = @HoSoKemTheo_10, TrangThaiXuLy = @TrangThaiXuLy WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_GiayPhep_SFA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, "SoDienThoaiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, "SoFaxXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailXuatKhau", SqlDbType.VarChar, "EmailXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBenDi", SqlDbType.VarChar, "MaBenDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenBenDi", SqlDbType.VarChar, "TenBenDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongNhapKhau", SqlDbType.NVarChar, "SoNhaTenDuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBenDen", SqlDbType.VarChar, "MaBenDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenBenDen", SqlDbType.VarChar, "TenBenDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianNhapKhauDuKien", SqlDbType.DateTime, "ThoiGianNhapKhauDuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaTriHangHoa", SqlDbType.Decimal, "GiaTriHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTienTe", SqlDbType.VarChar, "DonViTienTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemTapKetHangHoa", SqlDbType.NVarChar, "DiaDiemTapKetHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianKiemTra", SqlDbType.DateTime, "ThoiGianKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaiDienThuongNhanNhapKhau", SqlDbType.NVarChar, "DaiDienThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaiDienCoQuanKiemTra", SqlDbType.NVarChar, "DaiDienCoQuanKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, "SoDienThoaiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, "SoFaxXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailXuatKhau", SqlDbType.VarChar, "EmailXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBenDi", SqlDbType.VarChar, "MaBenDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenBenDi", SqlDbType.VarChar, "TenBenDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongNhapKhau", SqlDbType.NVarChar, "SoNhaTenDuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBenDen", SqlDbType.VarChar, "MaBenDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenBenDen", SqlDbType.VarChar, "TenBenDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianNhapKhauDuKien", SqlDbType.DateTime, "ThoiGianNhapKhauDuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaTriHangHoa", SqlDbType.Decimal, "GiaTriHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTienTe", SqlDbType.VarChar, "DonViTienTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemTapKetHangHoa", SqlDbType.NVarChar, "DiaDiemTapKetHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianKiemTra", SqlDbType.DateTime, "ThoiGianKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaiDienThuongNhanNhapKhau", SqlDbType.NVarChar, "DaiDienThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaiDienCoQuanKiemTra", SqlDbType.NVarChar, "DaiDienCoQuanKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_GiayPhep_SFA VALUES(@MaNguoiKhai, @TenNguoiKhai, @MaDonViCapPhep, @TenDonViCapPhep, @SoDonXinCapPhep, @ChucNangChungTu, @LoaiGiayPhep, @MaThuongNhanXuatKhau, @TenThuongNhanXuatKhau, @MaBuuChinhXuatKhau, @SoNhaTenDuongXuatKhau, @PhuongXaXuatKhau, @QuanHuyenXuatKhau, @TinhThanhPhoXuatKhau, @MaQuocGiaXuatKhau, @SoDienThoaiXuatKhau, @SoFaxXuatKhau, @EmailXuatKhau, @SoHopDong, @SoVanDon, @MaBenDi, @TenBenDi, @MaThuongNhanNhapKhau, @TenThuongNhanNhapKhau, @MaBuuChinhNhapKhau, @SoNhaTenDuongNhapKhau, @MaQuocGiaNhapKhau, @SoDienThoaiNhapKhau, @SoFaxNhapKhau, @EmailNhapKhau, @MaBenDen, @TenBenDen, @ThoiGianNhapKhauDuKien, @GiaTriHangHoa, @DonViTienTe, @DiaDiemTapKetHangHoa, @ThoiGianKiemTra, @DiaDiemKiemTra, @HoSoLienQuan, @GhiChu, @DaiDienThuongNhanNhapKhau, @Notes, @InputMessageID, @MessageTag, @IndexTag, @MaKetQuaXuLy, @KetQuaXuLySoGiayPhep, @KetQuaXuLyNgayCap, @KetQuaXuLyHieuLucTuNgay, @KetQuaXuLyHieuLucDenNgay, @GhiChuDanhChoCoQuanCapPhep, @DaiDienCoQuanKiemTra, @NgayKhaiBao, @HoSoKemTheo_1, @HoSoKemTheo_2, @HoSoKemTheo_3, @HoSoKemTheo_4, @HoSoKemTheo_5, @HoSoKemTheo_6, @HoSoKemTheo_7, @HoSoKemTheo_8, @HoSoKemTheo_9, @HoSoKemTheo_10, @TrangThaiXuLy)";
            string update = "UPDATE t_KDT_VNACC_GiayPhep_SFA SET MaNguoiKhai = @MaNguoiKhai, TenNguoiKhai = @TenNguoiKhai, MaDonViCapPhep = @MaDonViCapPhep, TenDonViCapPhep = @TenDonViCapPhep, SoDonXinCapPhep = @SoDonXinCapPhep, ChucNangChungTu = @ChucNangChungTu, LoaiGiayPhep = @LoaiGiayPhep, MaThuongNhanXuatKhau = @MaThuongNhanXuatKhau, TenThuongNhanXuatKhau = @TenThuongNhanXuatKhau, MaBuuChinhXuatKhau = @MaBuuChinhXuatKhau, SoNhaTenDuongXuatKhau = @SoNhaTenDuongXuatKhau, PhuongXaXuatKhau = @PhuongXaXuatKhau, QuanHuyenXuatKhau = @QuanHuyenXuatKhau, TinhThanhPhoXuatKhau = @TinhThanhPhoXuatKhau, MaQuocGiaXuatKhau = @MaQuocGiaXuatKhau, SoDienThoaiXuatKhau = @SoDienThoaiXuatKhau, SoFaxXuatKhau = @SoFaxXuatKhau, EmailXuatKhau = @EmailXuatKhau, SoHopDong = @SoHopDong, SoVanDon = @SoVanDon, MaBenDi = @MaBenDi, TenBenDi = @TenBenDi, MaThuongNhanNhapKhau = @MaThuongNhanNhapKhau, TenThuongNhanNhapKhau = @TenThuongNhanNhapKhau, MaBuuChinhNhapKhau = @MaBuuChinhNhapKhau, SoNhaTenDuongNhapKhau = @SoNhaTenDuongNhapKhau, MaQuocGiaNhapKhau = @MaQuocGiaNhapKhau, SoDienThoaiNhapKhau = @SoDienThoaiNhapKhau, SoFaxNhapKhau = @SoFaxNhapKhau, EmailNhapKhau = @EmailNhapKhau, MaBenDen = @MaBenDen, TenBenDen = @TenBenDen, ThoiGianNhapKhauDuKien = @ThoiGianNhapKhauDuKien, GiaTriHangHoa = @GiaTriHangHoa, DonViTienTe = @DonViTienTe, DiaDiemTapKetHangHoa = @DiaDiemTapKetHangHoa, ThoiGianKiemTra = @ThoiGianKiemTra, DiaDiemKiemTra = @DiaDiemKiemTra, HoSoLienQuan = @HoSoLienQuan, GhiChu = @GhiChu, DaiDienThuongNhanNhapKhau = @DaiDienThuongNhanNhapKhau, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag, MaKetQuaXuLy = @MaKetQuaXuLy, KetQuaXuLySoGiayPhep = @KetQuaXuLySoGiayPhep, KetQuaXuLyNgayCap = @KetQuaXuLyNgayCap, KetQuaXuLyHieuLucTuNgay = @KetQuaXuLyHieuLucTuNgay, KetQuaXuLyHieuLucDenNgay = @KetQuaXuLyHieuLucDenNgay, GhiChuDanhChoCoQuanCapPhep = @GhiChuDanhChoCoQuanCapPhep, DaiDienCoQuanKiemTra = @DaiDienCoQuanKiemTra, NgayKhaiBao = @NgayKhaiBao, HoSoKemTheo_1 = @HoSoKemTheo_1, HoSoKemTheo_2 = @HoSoKemTheo_2, HoSoKemTheo_3 = @HoSoKemTheo_3, HoSoKemTheo_4 = @HoSoKemTheo_4, HoSoKemTheo_5 = @HoSoKemTheo_5, HoSoKemTheo_6 = @HoSoKemTheo_6, HoSoKemTheo_7 = @HoSoKemTheo_7, HoSoKemTheo_8 = @HoSoKemTheo_8, HoSoKemTheo_9 = @HoSoKemTheo_9, HoSoKemTheo_10 = @HoSoKemTheo_10, TrangThaiXuLy = @TrangThaiXuLy WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_GiayPhep_SFA WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, "SoDienThoaiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, "SoFaxXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailXuatKhau", SqlDbType.VarChar, "EmailXuatKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBenDi", SqlDbType.VarChar, "MaBenDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenBenDi", SqlDbType.VarChar, "TenBenDi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoNhaTenDuongNhapKhau", SqlDbType.NVarChar, "SoNhaTenDuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBenDen", SqlDbType.VarChar, "MaBenDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenBenDen", SqlDbType.VarChar, "TenBenDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianNhapKhauDuKien", SqlDbType.DateTime, "ThoiGianNhapKhauDuKien", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaTriHangHoa", SqlDbType.Decimal, "GiaTriHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTienTe", SqlDbType.VarChar, "DonViTienTe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemTapKetHangHoa", SqlDbType.NVarChar, "DiaDiemTapKetHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianKiemTra", SqlDbType.DateTime, "ThoiGianKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaiDienThuongNhanNhapKhau", SqlDbType.NVarChar, "DaiDienThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DaiDienCoQuanKiemTra", SqlDbType.NVarChar, "DaiDienCoQuanKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNguoiKhai", SqlDbType.VarChar, "MaNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNguoiKhai", SqlDbType.NVarChar, "TenNguoiKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonViCapPhep", SqlDbType.VarChar, "MaDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, "TenDonViCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, "SoDonXinCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChucNangChungTu", SqlDbType.Int, "ChucNangChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiGiayPhep", SqlDbType.VarChar, "LoaiGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, "MaThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, "TenThuongNhanXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, "MaBuuChinhXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, "SoNhaTenDuongXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, "PhuongXaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, "QuanHuyenXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, "TinhThanhPhoXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, "MaQuocGiaXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, "SoDienThoaiXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, "SoFaxXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailXuatKhau", SqlDbType.VarChar, "EmailXuatKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoVanDon", SqlDbType.VarChar, "SoVanDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBenDi", SqlDbType.VarChar, "MaBenDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenBenDi", SqlDbType.VarChar, "TenBenDi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, "MaThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, "TenThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, "MaBuuChinhNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoNhaTenDuongNhapKhau", SqlDbType.NVarChar, "SoNhaTenDuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, "MaQuocGiaNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, "SoDienThoaiNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, "SoFaxNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@EmailNhapKhau", SqlDbType.VarChar, "EmailNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBenDen", SqlDbType.VarChar, "MaBenDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenBenDen", SqlDbType.VarChar, "TenBenDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianNhapKhauDuKien", SqlDbType.DateTime, "ThoiGianNhapKhauDuKien", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaTriHangHoa", SqlDbType.Decimal, "GiaTriHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTienTe", SqlDbType.VarChar, "DonViTienTe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemTapKetHangHoa", SqlDbType.NVarChar, "DiaDiemTapKetHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianKiemTra", SqlDbType.DateTime, "ThoiGianKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoLienQuan", SqlDbType.NVarChar, "HoSoLienQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaiDienThuongNhanNhapKhau", SqlDbType.NVarChar, "DaiDienThuongNhanNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, "MaKetQuaXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, "KetQuaXuLySoGiayPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, "KetQuaXuLyNgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucTuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, "KetQuaXuLyHieuLucDenNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, "GhiChuDanhChoCoQuanCapPhep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DaiDienCoQuanKiemTra", SqlDbType.NVarChar, "DaiDienCoQuanKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKhaiBao", SqlDbType.DateTime, "NgayKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, "HoSoKemTheo_1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, "HoSoKemTheo_2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, "HoSoKemTheo_3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, "HoSoKemTheo_4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, "HoSoKemTheo_5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, "HoSoKemTheo_6", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, "HoSoKemTheo_7", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, "HoSoKemTheo_8", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, "HoSoKemTheo_9", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, "HoSoKemTheo_10", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_GiayPhep_SFA Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SFA_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_GiayPhep_SFA> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_GiayPhep_SFA> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_GiayPhep_SFA> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SFA_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_GiayPhep_SFA(string maNguoiKhai, string tenNguoiKhai, string maDonViCapPhep, string tenDonViCapPhep, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string maThuongNhanXuatKhau, string tenThuongNhanXuatKhau, string maBuuChinhXuatKhau, string soNhaTenDuongXuatKhau, string phuongXaXuatKhau, string quanHuyenXuatKhau, string tinhThanhPhoXuatKhau, string maQuocGiaXuatKhau, string soDienThoaiXuatKhau, string soFaxXuatKhau, string emailXuatKhau, string soHopDong, string soVanDon, string maBenDi, string tenBenDi, string maThuongNhanNhapKhau, string tenThuongNhanNhapKhau, string maBuuChinhNhapKhau, string soNhaTenDuongNhapKhau, string maQuocGiaNhapKhau, string soDienThoaiNhapKhau, string soFaxNhapKhau, string emailNhapKhau, string maBenDen, string tenBenDen, DateTime thoiGianNhapKhauDuKien, decimal giaTriHangHoa, string donViTienTe, string diaDiemTapKetHangHoa, DateTime thoiGianKiemTra, string diaDiemKiemTra, string hoSoLienQuan, string ghiChu, string daiDienThuongNhanNhapKhau, string notes, string inputMessageID, string messageTag, string indexTag, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string ghiChuDanhChoCoQuanCapPhep, string daiDienCoQuanKiemTra, DateTime ngayKhaiBao, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, int trangThaiXuLy)
		{
			KDT_VNACC_GiayPhep_SFA entity = new KDT_VNACC_GiayPhep_SFA();	
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.MaDonViCapPhep = maDonViCapPhep;
			entity.TenDonViCapPhep = tenDonViCapPhep;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.MaThuongNhanXuatKhau = maThuongNhanXuatKhau;
			entity.TenThuongNhanXuatKhau = tenThuongNhanXuatKhau;
			entity.MaBuuChinhXuatKhau = maBuuChinhXuatKhau;
			entity.SoNhaTenDuongXuatKhau = soNhaTenDuongXuatKhau;
			entity.PhuongXaXuatKhau = phuongXaXuatKhau;
			entity.QuanHuyenXuatKhau = quanHuyenXuatKhau;
			entity.TinhThanhPhoXuatKhau = tinhThanhPhoXuatKhau;
			entity.MaQuocGiaXuatKhau = maQuocGiaXuatKhau;
			entity.SoDienThoaiXuatKhau = soDienThoaiXuatKhau;
			entity.SoFaxXuatKhau = soFaxXuatKhau;
			entity.EmailXuatKhau = emailXuatKhau;
			entity.SoHopDong = soHopDong;
			entity.SoVanDon = soVanDon;
			entity.MaBenDi = maBenDi;
			entity.TenBenDi = tenBenDi;
			entity.MaThuongNhanNhapKhau = maThuongNhanNhapKhau;
			entity.TenThuongNhanNhapKhau = tenThuongNhanNhapKhau;
			entity.MaBuuChinhNhapKhau = maBuuChinhNhapKhau;
			entity.SoNhaTenDuongNhapKhau = soNhaTenDuongNhapKhau;
			entity.MaQuocGiaNhapKhau = maQuocGiaNhapKhau;
			entity.SoDienThoaiNhapKhau = soDienThoaiNhapKhau;
			entity.SoFaxNhapKhau = soFaxNhapKhau;
			entity.EmailNhapKhau = emailNhapKhau;
			entity.MaBenDen = maBenDen;
			entity.TenBenDen = tenBenDen;
			entity.ThoiGianNhapKhauDuKien = thoiGianNhapKhauDuKien;
			entity.GiaTriHangHoa = giaTriHangHoa;
			entity.DonViTienTe = donViTienTe;
			entity.DiaDiemTapKetHangHoa = diaDiemTapKetHangHoa;
			entity.ThoiGianKiemTra = thoiGianKiemTra;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.GhiChu = ghiChu;
			entity.DaiDienThuongNhanNhapKhau = daiDienThuongNhanNhapKhau;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.GhiChuDanhChoCoQuanCapPhep = ghiChuDanhChoCoQuanCapPhep;
			entity.DaiDienCoQuanKiemTra = daiDienCoQuanKiemTra;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SFA_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@MaDonViCapPhep", SqlDbType.VarChar, MaDonViCapPhep);
			db.AddInParameter(dbCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, TenDonViCapPhep);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, MaThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, TenThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, MaBuuChinhXuatKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, SoNhaTenDuongXuatKhau);
			db.AddInParameter(dbCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, PhuongXaXuatKhau);
			db.AddInParameter(dbCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, QuanHuyenXuatKhau);
			db.AddInParameter(dbCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, TinhThanhPhoXuatKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, MaQuocGiaXuatKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, SoDienThoaiXuatKhau);
			db.AddInParameter(dbCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, SoFaxXuatKhau);
			db.AddInParameter(dbCommand, "@EmailXuatKhau", SqlDbType.VarChar, EmailXuatKhau);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@MaBenDi", SqlDbType.VarChar, MaBenDi);
			db.AddInParameter(dbCommand, "@TenBenDi", SqlDbType.VarChar, TenBenDi);
			db.AddInParameter(dbCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, MaThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, TenThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, MaBuuChinhNhapKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongNhapKhau", SqlDbType.NVarChar, SoNhaTenDuongNhapKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, MaQuocGiaNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, SoDienThoaiNhapKhau);
			db.AddInParameter(dbCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, SoFaxNhapKhau);
			db.AddInParameter(dbCommand, "@EmailNhapKhau", SqlDbType.VarChar, EmailNhapKhau);
			db.AddInParameter(dbCommand, "@MaBenDen", SqlDbType.VarChar, MaBenDen);
			db.AddInParameter(dbCommand, "@TenBenDen", SqlDbType.VarChar, TenBenDen);
			db.AddInParameter(dbCommand, "@ThoiGianNhapKhauDuKien", SqlDbType.DateTime, ThoiGianNhapKhauDuKien.Year <= 1753 ? DBNull.Value : (object) ThoiGianNhapKhauDuKien);
			db.AddInParameter(dbCommand, "@GiaTriHangHoa", SqlDbType.Decimal, GiaTriHangHoa);
			db.AddInParameter(dbCommand, "@DonViTienTe", SqlDbType.VarChar, DonViTienTe);
			db.AddInParameter(dbCommand, "@DiaDiemTapKetHangHoa", SqlDbType.NVarChar, DiaDiemTapKetHangHoa);
			db.AddInParameter(dbCommand, "@ThoiGianKiemTra", SqlDbType.DateTime, ThoiGianKiemTra.Year <= 1753 ? DBNull.Value : (object) ThoiGianKiemTra);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@DaiDienThuongNhanNhapKhau", SqlDbType.NVarChar, DaiDienThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, GhiChuDanhChoCoQuanCapPhep);
			db.AddInParameter(dbCommand, "@DaiDienCoQuanKiemTra", SqlDbType.NVarChar, DaiDienCoQuanKiemTra);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_GiayPhep_SFA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SFA item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_GiayPhep_SFA(long id, string maNguoiKhai, string tenNguoiKhai, string maDonViCapPhep, string tenDonViCapPhep, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string maThuongNhanXuatKhau, string tenThuongNhanXuatKhau, string maBuuChinhXuatKhau, string soNhaTenDuongXuatKhau, string phuongXaXuatKhau, string quanHuyenXuatKhau, string tinhThanhPhoXuatKhau, string maQuocGiaXuatKhau, string soDienThoaiXuatKhau, string soFaxXuatKhau, string emailXuatKhau, string soHopDong, string soVanDon, string maBenDi, string tenBenDi, string maThuongNhanNhapKhau, string tenThuongNhanNhapKhau, string maBuuChinhNhapKhau, string soNhaTenDuongNhapKhau, string maQuocGiaNhapKhau, string soDienThoaiNhapKhau, string soFaxNhapKhau, string emailNhapKhau, string maBenDen, string tenBenDen, DateTime thoiGianNhapKhauDuKien, decimal giaTriHangHoa, string donViTienTe, string diaDiemTapKetHangHoa, DateTime thoiGianKiemTra, string diaDiemKiemTra, string hoSoLienQuan, string ghiChu, string daiDienThuongNhanNhapKhau, string notes, string inputMessageID, string messageTag, string indexTag, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string ghiChuDanhChoCoQuanCapPhep, string daiDienCoQuanKiemTra, DateTime ngayKhaiBao, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, int trangThaiXuLy)
		{
			KDT_VNACC_GiayPhep_SFA entity = new KDT_VNACC_GiayPhep_SFA();			
			entity.ID = id;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.MaDonViCapPhep = maDonViCapPhep;
			entity.TenDonViCapPhep = tenDonViCapPhep;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.MaThuongNhanXuatKhau = maThuongNhanXuatKhau;
			entity.TenThuongNhanXuatKhau = tenThuongNhanXuatKhau;
			entity.MaBuuChinhXuatKhau = maBuuChinhXuatKhau;
			entity.SoNhaTenDuongXuatKhau = soNhaTenDuongXuatKhau;
			entity.PhuongXaXuatKhau = phuongXaXuatKhau;
			entity.QuanHuyenXuatKhau = quanHuyenXuatKhau;
			entity.TinhThanhPhoXuatKhau = tinhThanhPhoXuatKhau;
			entity.MaQuocGiaXuatKhau = maQuocGiaXuatKhau;
			entity.SoDienThoaiXuatKhau = soDienThoaiXuatKhau;
			entity.SoFaxXuatKhau = soFaxXuatKhau;
			entity.EmailXuatKhau = emailXuatKhau;
			entity.SoHopDong = soHopDong;
			entity.SoVanDon = soVanDon;
			entity.MaBenDi = maBenDi;
			entity.TenBenDi = tenBenDi;
			entity.MaThuongNhanNhapKhau = maThuongNhanNhapKhau;
			entity.TenThuongNhanNhapKhau = tenThuongNhanNhapKhau;
			entity.MaBuuChinhNhapKhau = maBuuChinhNhapKhau;
			entity.SoNhaTenDuongNhapKhau = soNhaTenDuongNhapKhau;
			entity.MaQuocGiaNhapKhau = maQuocGiaNhapKhau;
			entity.SoDienThoaiNhapKhau = soDienThoaiNhapKhau;
			entity.SoFaxNhapKhau = soFaxNhapKhau;
			entity.EmailNhapKhau = emailNhapKhau;
			entity.MaBenDen = maBenDen;
			entity.TenBenDen = tenBenDen;
			entity.ThoiGianNhapKhauDuKien = thoiGianNhapKhauDuKien;
			entity.GiaTriHangHoa = giaTriHangHoa;
			entity.DonViTienTe = donViTienTe;
			entity.DiaDiemTapKetHangHoa = diaDiemTapKetHangHoa;
			entity.ThoiGianKiemTra = thoiGianKiemTra;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.GhiChu = ghiChu;
			entity.DaiDienThuongNhanNhapKhau = daiDienThuongNhanNhapKhau;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.GhiChuDanhChoCoQuanCapPhep = ghiChuDanhChoCoQuanCapPhep;
			entity.DaiDienCoQuanKiemTra = daiDienCoQuanKiemTra;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_GiayPhep_SFA_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@MaDonViCapPhep", SqlDbType.VarChar, MaDonViCapPhep);
			db.AddInParameter(dbCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, TenDonViCapPhep);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, MaThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, TenThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, MaBuuChinhXuatKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, SoNhaTenDuongXuatKhau);
			db.AddInParameter(dbCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, PhuongXaXuatKhau);
			db.AddInParameter(dbCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, QuanHuyenXuatKhau);
			db.AddInParameter(dbCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, TinhThanhPhoXuatKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, MaQuocGiaXuatKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, SoDienThoaiXuatKhau);
			db.AddInParameter(dbCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, SoFaxXuatKhau);
			db.AddInParameter(dbCommand, "@EmailXuatKhau", SqlDbType.VarChar, EmailXuatKhau);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@MaBenDi", SqlDbType.VarChar, MaBenDi);
			db.AddInParameter(dbCommand, "@TenBenDi", SqlDbType.VarChar, TenBenDi);
			db.AddInParameter(dbCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, MaThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, TenThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, MaBuuChinhNhapKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongNhapKhau", SqlDbType.NVarChar, SoNhaTenDuongNhapKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, MaQuocGiaNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, SoDienThoaiNhapKhau);
			db.AddInParameter(dbCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, SoFaxNhapKhau);
			db.AddInParameter(dbCommand, "@EmailNhapKhau", SqlDbType.VarChar, EmailNhapKhau);
			db.AddInParameter(dbCommand, "@MaBenDen", SqlDbType.VarChar, MaBenDen);
			db.AddInParameter(dbCommand, "@TenBenDen", SqlDbType.VarChar, TenBenDen);
			db.AddInParameter(dbCommand, "@ThoiGianNhapKhauDuKien", SqlDbType.DateTime, ThoiGianNhapKhauDuKien.Year <= 1753 ? DBNull.Value : (object) ThoiGianNhapKhauDuKien);
			db.AddInParameter(dbCommand, "@GiaTriHangHoa", SqlDbType.Decimal, GiaTriHangHoa);
			db.AddInParameter(dbCommand, "@DonViTienTe", SqlDbType.VarChar, DonViTienTe);
			db.AddInParameter(dbCommand, "@DiaDiemTapKetHangHoa", SqlDbType.NVarChar, DiaDiemTapKetHangHoa);
			db.AddInParameter(dbCommand, "@ThoiGianKiemTra", SqlDbType.DateTime, ThoiGianKiemTra.Year <= 1753 ? DBNull.Value : (object) ThoiGianKiemTra);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@DaiDienThuongNhanNhapKhau", SqlDbType.NVarChar, DaiDienThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, GhiChuDanhChoCoQuanCapPhep);
			db.AddInParameter(dbCommand, "@DaiDienCoQuanKiemTra", SqlDbType.NVarChar, DaiDienCoQuanKiemTra);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_GiayPhep_SFA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SFA item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_GiayPhep_SFA(long id, string maNguoiKhai, string tenNguoiKhai, string maDonViCapPhep, string tenDonViCapPhep, decimal soDonXinCapPhep, int chucNangChungTu, string loaiGiayPhep, string maThuongNhanXuatKhau, string tenThuongNhanXuatKhau, string maBuuChinhXuatKhau, string soNhaTenDuongXuatKhau, string phuongXaXuatKhau, string quanHuyenXuatKhau, string tinhThanhPhoXuatKhau, string maQuocGiaXuatKhau, string soDienThoaiXuatKhau, string soFaxXuatKhau, string emailXuatKhau, string soHopDong, string soVanDon, string maBenDi, string tenBenDi, string maThuongNhanNhapKhau, string tenThuongNhanNhapKhau, string maBuuChinhNhapKhau, string soNhaTenDuongNhapKhau, string maQuocGiaNhapKhau, string soDienThoaiNhapKhau, string soFaxNhapKhau, string emailNhapKhau, string maBenDen, string tenBenDen, DateTime thoiGianNhapKhauDuKien, decimal giaTriHangHoa, string donViTienTe, string diaDiemTapKetHangHoa, DateTime thoiGianKiemTra, string diaDiemKiemTra, string hoSoLienQuan, string ghiChu, string daiDienThuongNhanNhapKhau, string notes, string inputMessageID, string messageTag, string indexTag, string maKetQuaXuLy, string ketQuaXuLySoGiayPhep, DateTime ketQuaXuLyNgayCap, DateTime ketQuaXuLyHieuLucTuNgay, DateTime ketQuaXuLyHieuLucDenNgay, string ghiChuDanhChoCoQuanCapPhep, string daiDienCoQuanKiemTra, DateTime ngayKhaiBao, string hoSoKemTheo_1, string hoSoKemTheo_2, string hoSoKemTheo_3, string hoSoKemTheo_4, string hoSoKemTheo_5, string hoSoKemTheo_6, string hoSoKemTheo_7, string hoSoKemTheo_8, string hoSoKemTheo_9, string hoSoKemTheo_10, int trangThaiXuLy)
		{
			KDT_VNACC_GiayPhep_SFA entity = new KDT_VNACC_GiayPhep_SFA();			
			entity.ID = id;
			entity.MaNguoiKhai = maNguoiKhai;
			entity.TenNguoiKhai = tenNguoiKhai;
			entity.MaDonViCapPhep = maDonViCapPhep;
			entity.TenDonViCapPhep = tenDonViCapPhep;
			entity.SoDonXinCapPhep = soDonXinCapPhep;
			entity.ChucNangChungTu = chucNangChungTu;
			entity.LoaiGiayPhep = loaiGiayPhep;
			entity.MaThuongNhanXuatKhau = maThuongNhanXuatKhau;
			entity.TenThuongNhanXuatKhau = tenThuongNhanXuatKhau;
			entity.MaBuuChinhXuatKhau = maBuuChinhXuatKhau;
			entity.SoNhaTenDuongXuatKhau = soNhaTenDuongXuatKhau;
			entity.PhuongXaXuatKhau = phuongXaXuatKhau;
			entity.QuanHuyenXuatKhau = quanHuyenXuatKhau;
			entity.TinhThanhPhoXuatKhau = tinhThanhPhoXuatKhau;
			entity.MaQuocGiaXuatKhau = maQuocGiaXuatKhau;
			entity.SoDienThoaiXuatKhau = soDienThoaiXuatKhau;
			entity.SoFaxXuatKhau = soFaxXuatKhau;
			entity.EmailXuatKhau = emailXuatKhau;
			entity.SoHopDong = soHopDong;
			entity.SoVanDon = soVanDon;
			entity.MaBenDi = maBenDi;
			entity.TenBenDi = tenBenDi;
			entity.MaThuongNhanNhapKhau = maThuongNhanNhapKhau;
			entity.TenThuongNhanNhapKhau = tenThuongNhanNhapKhau;
			entity.MaBuuChinhNhapKhau = maBuuChinhNhapKhau;
			entity.SoNhaTenDuongNhapKhau = soNhaTenDuongNhapKhau;
			entity.MaQuocGiaNhapKhau = maQuocGiaNhapKhau;
			entity.SoDienThoaiNhapKhau = soDienThoaiNhapKhau;
			entity.SoFaxNhapKhau = soFaxNhapKhau;
			entity.EmailNhapKhau = emailNhapKhau;
			entity.MaBenDen = maBenDen;
			entity.TenBenDen = tenBenDen;
			entity.ThoiGianNhapKhauDuKien = thoiGianNhapKhauDuKien;
			entity.GiaTriHangHoa = giaTriHangHoa;
			entity.DonViTienTe = donViTienTe;
			entity.DiaDiemTapKetHangHoa = diaDiemTapKetHangHoa;
			entity.ThoiGianKiemTra = thoiGianKiemTra;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.HoSoLienQuan = hoSoLienQuan;
			entity.GhiChu = ghiChu;
			entity.DaiDienThuongNhanNhapKhau = daiDienThuongNhanNhapKhau;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			entity.MaKetQuaXuLy = maKetQuaXuLy;
			entity.KetQuaXuLySoGiayPhep = ketQuaXuLySoGiayPhep;
			entity.KetQuaXuLyNgayCap = ketQuaXuLyNgayCap;
			entity.KetQuaXuLyHieuLucTuNgay = ketQuaXuLyHieuLucTuNgay;
			entity.KetQuaXuLyHieuLucDenNgay = ketQuaXuLyHieuLucDenNgay;
			entity.GhiChuDanhChoCoQuanCapPhep = ghiChuDanhChoCoQuanCapPhep;
			entity.DaiDienCoQuanKiemTra = daiDienCoQuanKiemTra;
			entity.NgayKhaiBao = ngayKhaiBao;
			entity.HoSoKemTheo_1 = hoSoKemTheo_1;
			entity.HoSoKemTheo_2 = hoSoKemTheo_2;
			entity.HoSoKemTheo_3 = hoSoKemTheo_3;
			entity.HoSoKemTheo_4 = hoSoKemTheo_4;
			entity.HoSoKemTheo_5 = hoSoKemTheo_5;
			entity.HoSoKemTheo_6 = hoSoKemTheo_6;
			entity.HoSoKemTheo_7 = hoSoKemTheo_7;
			entity.HoSoKemTheo_8 = hoSoKemTheo_8;
			entity.HoSoKemTheo_9 = hoSoKemTheo_9;
			entity.HoSoKemTheo_10 = hoSoKemTheo_10;
			entity.TrangThaiXuLy = trangThaiXuLy;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SFA_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaNguoiKhai", SqlDbType.VarChar, MaNguoiKhai);
			db.AddInParameter(dbCommand, "@TenNguoiKhai", SqlDbType.NVarChar, TenNguoiKhai);
			db.AddInParameter(dbCommand, "@MaDonViCapPhep", SqlDbType.VarChar, MaDonViCapPhep);
			db.AddInParameter(dbCommand, "@TenDonViCapPhep", SqlDbType.NVarChar, TenDonViCapPhep);
			db.AddInParameter(dbCommand, "@SoDonXinCapPhep", SqlDbType.Decimal, SoDonXinCapPhep);
			db.AddInParameter(dbCommand, "@ChucNangChungTu", SqlDbType.Int, ChucNangChungTu);
			db.AddInParameter(dbCommand, "@LoaiGiayPhep", SqlDbType.VarChar, LoaiGiayPhep);
			db.AddInParameter(dbCommand, "@MaThuongNhanXuatKhau", SqlDbType.VarChar, MaThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanXuatKhau", SqlDbType.NVarChar, TenThuongNhanXuatKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhXuatKhau", SqlDbType.VarChar, MaBuuChinhXuatKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongXuatKhau", SqlDbType.VarChar, SoNhaTenDuongXuatKhau);
			db.AddInParameter(dbCommand, "@PhuongXaXuatKhau", SqlDbType.NVarChar, PhuongXaXuatKhau);
			db.AddInParameter(dbCommand, "@QuanHuyenXuatKhau", SqlDbType.NVarChar, QuanHuyenXuatKhau);
			db.AddInParameter(dbCommand, "@TinhThanhPhoXuatKhau", SqlDbType.NVarChar, TinhThanhPhoXuatKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaXuatKhau", SqlDbType.VarChar, MaQuocGiaXuatKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiXuatKhau", SqlDbType.VarChar, SoDienThoaiXuatKhau);
			db.AddInParameter(dbCommand, "@SoFaxXuatKhau", SqlDbType.VarChar, SoFaxXuatKhau);
			db.AddInParameter(dbCommand, "@EmailXuatKhau", SqlDbType.VarChar, EmailXuatKhau);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
			db.AddInParameter(dbCommand, "@MaBenDi", SqlDbType.VarChar, MaBenDi);
			db.AddInParameter(dbCommand, "@TenBenDi", SqlDbType.VarChar, TenBenDi);
			db.AddInParameter(dbCommand, "@MaThuongNhanNhapKhau", SqlDbType.VarChar, MaThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@TenThuongNhanNhapKhau", SqlDbType.NVarChar, TenThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@MaBuuChinhNhapKhau", SqlDbType.VarChar, MaBuuChinhNhapKhau);
			db.AddInParameter(dbCommand, "@SoNhaTenDuongNhapKhau", SqlDbType.NVarChar, SoNhaTenDuongNhapKhau);
			db.AddInParameter(dbCommand, "@MaQuocGiaNhapKhau", SqlDbType.VarChar, MaQuocGiaNhapKhau);
			db.AddInParameter(dbCommand, "@SoDienThoaiNhapKhau", SqlDbType.VarChar, SoDienThoaiNhapKhau);
			db.AddInParameter(dbCommand, "@SoFaxNhapKhau", SqlDbType.VarChar, SoFaxNhapKhau);
			db.AddInParameter(dbCommand, "@EmailNhapKhau", SqlDbType.VarChar, EmailNhapKhau);
			db.AddInParameter(dbCommand, "@MaBenDen", SqlDbType.VarChar, MaBenDen);
			db.AddInParameter(dbCommand, "@TenBenDen", SqlDbType.VarChar, TenBenDen);
			db.AddInParameter(dbCommand, "@ThoiGianNhapKhauDuKien", SqlDbType.DateTime, ThoiGianNhapKhauDuKien.Year <= 1753 ? DBNull.Value : (object) ThoiGianNhapKhauDuKien);
			db.AddInParameter(dbCommand, "@GiaTriHangHoa", SqlDbType.Decimal, GiaTriHangHoa);
			db.AddInParameter(dbCommand, "@DonViTienTe", SqlDbType.VarChar, DonViTienTe);
			db.AddInParameter(dbCommand, "@DiaDiemTapKetHangHoa", SqlDbType.NVarChar, DiaDiemTapKetHangHoa);
			db.AddInParameter(dbCommand, "@ThoiGianKiemTra", SqlDbType.DateTime, ThoiGianKiemTra.Year <= 1753 ? DBNull.Value : (object) ThoiGianKiemTra);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@HoSoLienQuan", SqlDbType.NVarChar, HoSoLienQuan);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@DaiDienThuongNhanNhapKhau", SqlDbType.NVarChar, DaiDienThuongNhanNhapKhau);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			db.AddInParameter(dbCommand, "@MaKetQuaXuLy", SqlDbType.VarChar, MaKetQuaXuLy);
			db.AddInParameter(dbCommand, "@KetQuaXuLySoGiayPhep", SqlDbType.VarChar, KetQuaXuLySoGiayPhep);
			db.AddInParameter(dbCommand, "@KetQuaXuLyNgayCap", SqlDbType.DateTime, KetQuaXuLyNgayCap.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyNgayCap);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucTuNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucTuNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucTuNgay);
			db.AddInParameter(dbCommand, "@KetQuaXuLyHieuLucDenNgay", SqlDbType.DateTime, KetQuaXuLyHieuLucDenNgay.Year <= 1753 ? DBNull.Value : (object) KetQuaXuLyHieuLucDenNgay);
			db.AddInParameter(dbCommand, "@GhiChuDanhChoCoQuanCapPhep", SqlDbType.NVarChar, GhiChuDanhChoCoQuanCapPhep);
			db.AddInParameter(dbCommand, "@DaiDienCoQuanKiemTra", SqlDbType.NVarChar, DaiDienCoQuanKiemTra);
			db.AddInParameter(dbCommand, "@NgayKhaiBao", SqlDbType.DateTime, NgayKhaiBao.Year <= 1753 ? DBNull.Value : (object) NgayKhaiBao);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_1", SqlDbType.VarChar, HoSoKemTheo_1);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_2", SqlDbType.VarChar, HoSoKemTheo_2);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_3", SqlDbType.VarChar, HoSoKemTheo_3);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_4", SqlDbType.VarChar, HoSoKemTheo_4);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_5", SqlDbType.VarChar, HoSoKemTheo_5);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_6", SqlDbType.VarChar, HoSoKemTheo_6);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_7", SqlDbType.VarChar, HoSoKemTheo_7);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_8", SqlDbType.VarChar, HoSoKemTheo_8);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_9", SqlDbType.VarChar, HoSoKemTheo_9);
			db.AddInParameter(dbCommand, "@HoSoKemTheo_10", SqlDbType.VarChar, HoSoKemTheo_10);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_GiayPhep_SFA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SFA item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_GiayPhep_SFA(long id)
		{
			KDT_VNACC_GiayPhep_SFA entity = new KDT_VNACC_GiayPhep_SFA();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SFA_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_GiayPhep_SFA_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_GiayPhep_SFA> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_GiayPhep_SFA item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}