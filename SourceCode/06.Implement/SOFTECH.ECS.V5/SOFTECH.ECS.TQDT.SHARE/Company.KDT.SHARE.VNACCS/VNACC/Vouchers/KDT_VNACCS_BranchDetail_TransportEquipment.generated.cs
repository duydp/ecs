﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Company.KDT.SHARE.Components.Messages.Vouchers;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_BranchDetail_TransportEquipment : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long BranchDetail_ID { set; get; }
		public string SoContainer { set; get; }
		public string SoSeal { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_BranchDetail_TransportEquipment> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_BranchDetail_TransportEquipment> collection = new List<KDT_VNACCS_BranchDetail_TransportEquipment>();
			while (reader.Read())
			{
				KDT_VNACCS_BranchDetail_TransportEquipment entity = new KDT_VNACCS_BranchDetail_TransportEquipment();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("BranchDetail_ID"))) entity.BranchDetail_ID = reader.GetInt64(reader.GetOrdinal("BranchDetail_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoContainer"))) entity.SoContainer = reader.GetString(reader.GetOrdinal("SoContainer"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoSeal"))) entity.SoSeal = reader.GetString(reader.GetOrdinal("SoSeal"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
        protected static List<BranchDetail_Container> ConvertToCollectionNew(IDataReader reader)
        {
            List<BranchDetail_Container> collection = new List<BranchDetail_Container>();
            while (reader.Read())
            {
                BranchDetail_Container entity = new BranchDetail_Container();
                if (!reader.IsDBNull(reader.GetOrdinal("SoContainer"))) entity.Container = reader.GetString(reader.GetOrdinal("SoContainer"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoSeal"))) entity.Seal = reader.GetString(reader.GetOrdinal("SoSeal"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
		public static bool Find(List<KDT_VNACCS_BranchDetail_TransportEquipment> collection, long id)
        {
            foreach (KDT_VNACCS_BranchDetail_TransportEquipment item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BranchDetail_TransportEquipments VALUES(@BranchDetail_ID, @SoContainer, @SoSeal)";
            string update = "UPDATE t_KDT_VNACCS_BranchDetail_TransportEquipments SET BranchDetail_ID = @BranchDetail_ID, SoContainer = @SoContainer, SoSeal = @SoSeal WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BranchDetail_TransportEquipments WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BranchDetail_ID", SqlDbType.BigInt, "BranchDetail_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BranchDetail_ID", SqlDbType.BigInt, "BranchDetail_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_BranchDetail_TransportEquipments VALUES(@BranchDetail_ID, @SoContainer, @SoSeal)";
            string update = "UPDATE t_KDT_VNACCS_BranchDetail_TransportEquipments SET BranchDetail_ID = @BranchDetail_ID, SoContainer = @SoContainer, SoSeal = @SoSeal WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_BranchDetail_TransportEquipments WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@BranchDetail_ID", SqlDbType.BigInt, "BranchDetail_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@BranchDetail_ID", SqlDbType.BigInt, "BranchDetail_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoContainer", SqlDbType.NVarChar, "SoContainer", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoSeal", SqlDbType.NVarChar, "SoSeal", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_BranchDetail_TransportEquipment Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_BranchDetail_TransportEquipment> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_BranchDetail_TransportEquipment> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_BranchDetail_TransportEquipment> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_BranchDetail_TransportEquipment> SelectCollectionBy_BranchDetail_ID(long branchDetail_ID)
		{
            IDataReader reader = SelectReaderBy_BranchDetail_ID(branchDetail_ID);
			return ConvertToCollection(reader);	
		}
        public static List<BranchDetail_Container> SelectCollectionBy_BranchDetail_ID_New(long branchDetail_ID)
        {
            IDataReader reader = SelectReaderBy_BranchDetail_ID(branchDetail_ID);
            return ConvertToCollectionNew(reader);
        }
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_BranchDetail_ID(long branchDetail_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectBy_BranchDetail_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BranchDetail_ID", SqlDbType.BigInt, branchDetail_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_BranchDetail_ID(long branchDetail_ID)
		{
			const string spName = "p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectBy_BranchDetail_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BranchDetail_ID", SqlDbType.BigInt, branchDetail_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_BranchDetail_TransportEquipment(long branchDetail_ID, string soContainer, string soSeal)
		{
			KDT_VNACCS_BranchDetail_TransportEquipment entity = new KDT_VNACCS_BranchDetail_TransportEquipment();	
			entity.BranchDetail_ID = branchDetail_ID;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@BranchDetail_ID", SqlDbType.BigInt, BranchDetail_ID);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.NVarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.NVarChar, SoSeal);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_BranchDetail_TransportEquipment> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BranchDetail_TransportEquipment item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_BranchDetail_TransportEquipment(long id, long branchDetail_ID, string soContainer, string soSeal)
		{
			KDT_VNACCS_BranchDetail_TransportEquipment entity = new KDT_VNACCS_BranchDetail_TransportEquipment();			
			entity.ID = id;
			entity.BranchDetail_ID = branchDetail_ID;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_BranchDetail_TransportEquipment_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BranchDetail_ID", SqlDbType.BigInt, BranchDetail_ID);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.NVarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.NVarChar, SoSeal);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_BranchDetail_TransportEquipment> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BranchDetail_TransportEquipment item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_BranchDetail_TransportEquipment(long id, long branchDetail_ID, string soContainer, string soSeal)
		{
			KDT_VNACCS_BranchDetail_TransportEquipment entity = new KDT_VNACCS_BranchDetail_TransportEquipment();			
			entity.ID = id;
			entity.BranchDetail_ID = branchDetail_ID;
			entity.SoContainer = soContainer;
			entity.SoSeal = soSeal;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@BranchDetail_ID", SqlDbType.BigInt, BranchDetail_ID);
			db.AddInParameter(dbCommand, "@SoContainer", SqlDbType.NVarChar, SoContainer);
			db.AddInParameter(dbCommand, "@SoSeal", SqlDbType.NVarChar, SoSeal);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_BranchDetail_TransportEquipment> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BranchDetail_TransportEquipment item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public  int DeleteKDT_VNACCS_BranchDetail_TransportEquipment(long id)
		{
			KDT_VNACCS_BranchDetail_TransportEquipment entity = new KDT_VNACCS_BranchDetail_TransportEquipment();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int DeleteBy_BranchDetail_ID(long branchDetail_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_DeleteBy_BranchDetail_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@BranchDetail_ID", SqlDbType.BigInt, branchDetail_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_BranchDetail_TransportEquipment> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_BranchDetail_TransportEquipment item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}