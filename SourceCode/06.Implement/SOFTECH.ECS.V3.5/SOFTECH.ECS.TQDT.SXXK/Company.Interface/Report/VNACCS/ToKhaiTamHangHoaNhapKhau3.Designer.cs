﻿namespace Company.Interface.Report.VNACCS
{
    partial class ToKhaiTamHangHoaNhapKhau3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell68 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell69 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell74 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell73 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell75 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell72 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiDauTien = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell76 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoNhanhToKhaiChiaNho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell78 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoToKhaiChiaNho = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell13 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell14 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell17 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoToKhaiTamNhapTaiXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell25 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell26 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell27 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiKiemTra = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell29 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell30 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell31 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaLoaiHinh = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaHieuPhuongThucVanChuyen = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell35 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiCaNhanToChuc = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell19 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell21 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell18 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell22 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSoHangHoaDaiDienToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell37 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell38 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenCoQuanHaiQuanTiepNhanToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell41 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell47 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell23 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBoPhanXuLyToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell49 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell50 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell51 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell15 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell54 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell55 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblNgayThayDoiDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblGioThayDoiDangKy = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell58 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell59 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThoiHanTaiNhapTaiXuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell42 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblBieuThiTHHetHan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable5 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow36 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblSoTrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell208 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTongSoTrang = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenThongTinXuat = new DevExpress.XtraReports.UI.XRLabel();
            this.DetailReport = new DevExpress.XtraReports.UI.DetailReportBand();
            this.Detail1 = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable12 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow40 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell302 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaMienGiamThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell304 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuKhoanMienGiamThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable13 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow41 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell306 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaMienGiamThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell308 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuKhoanMienGiamThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable6 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow27 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell233 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell234 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell236 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell238 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell239 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow30 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell240 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell241 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell242 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienGiamThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell244 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell245 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable9 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow35 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell282 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell283 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell284 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell286 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell287 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow37 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell288 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell289 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell290 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienGiamThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell292 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell293 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable14 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow42 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell322 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell323 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell310 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell311 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell312 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenKhoanMucThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell314 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell324 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell326 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell327 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell325 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaApDungThueSuatThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow43 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell316 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell317 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell318 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTinhThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell320 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell328 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell330 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell331 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell329 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTinhThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell333 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow44 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell334 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell335 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell336 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell338 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell339 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell340 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell341 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable10 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow38 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell294 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaMienGiamThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell296 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuKhoanMienGiamThueVaThuKhac2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable15 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow45 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell342 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell343 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell344 = new DevExpress.XtraReports.UI.XRTableCell();
            this.aa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell346 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenKhoanMucThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell348 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell349 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell350 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell351 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell352 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaApDungThueSuatThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow46 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell354 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell355 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell356 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTinhThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell358 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell359 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell360 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell361 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell362 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTinhThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell364 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow47 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell366 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell367 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell368 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell370 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell371 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell372 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell373 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable17 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow51 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell406 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell407 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell408 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell409 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell410 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenKhoanMucThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell412 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell413 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell414 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell415 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell416 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaApDungThueSuatThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow52 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell418 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell419 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell420 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTinhThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell422 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell423 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell424 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell425 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell426 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTinhThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell428 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow53 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell430 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell431 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell432 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatThueVaThuKhac5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell434 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell435 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell436 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell437 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable16 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow48 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell374 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell375 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell376 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell377 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell378 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenKhoanMucThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell380 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell381 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell382 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell383 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell384 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaApDungThueSuatThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow49 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell386 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell387 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell388 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTinhThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell390 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell391 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell392 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell393 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell394 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTinhThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell396 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow50 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell398 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell399 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell400 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell402 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell403 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell404 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell405 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable11 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow39 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell298 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaMienGiamThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell300 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuKhoanMienGiamThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable7 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow31 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell246 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell247 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell248 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell250 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell252 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow32 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell264 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell265 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell266 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienGiamThueVaThuKhac3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell268 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell269 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable4 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow26 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell228 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaMienGiamThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell232 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuKhoanMienGiamThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable8 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow33 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell270 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell271 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell272 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell274 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell275 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow34 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell276 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell277 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell278 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienGiamThueVaThuKhac4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell280 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell281 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow28 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell255 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell256 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell253 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell257 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell254 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow29 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell258 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell259 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell260 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienGiamThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell262 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell263 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTable2 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell84 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell85 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell87 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell86 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaSoHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaQuanLyRieng = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell43 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiTaiXacNhanGia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell45 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell46 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell61 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMoTaHangHoa = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell44 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell62 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell63 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell66 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell65 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell67 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell82 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDonViTinh1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow10 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell83 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell88 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell89 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoMucKhaiKhoangDieuChinh1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoMucKhaiKhoangDieuChinh2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoMucKhaiKhoangDieuChinh3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoMucKhaiKhoangDieuChinh4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoMucKhaiKhoangDieuChinh5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell91 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell92 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell94 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDonViTinh2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow11 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell100 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell101 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell102 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell104 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell107 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell108 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell109 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaHoaDon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell111 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDongTienCuaDonGia = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell105 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonViCuaDonGiaVaSoLuong = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow12 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell113 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell114 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell115 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow13 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell126 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell127 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell128 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTinhThueS = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell130 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell131 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell132 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDongTienCuaGiaTinhThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell135 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTinhThueM = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow14 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell116 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell117 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell118 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTinhThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell120 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDonViTinhChuanDanhThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell122 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonGiaTinhThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell125 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell172 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell171 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDonViSoLuongTrongDonGiaTinhThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow17 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell160 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell161 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell162 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaPhanLoaiThueSuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatThueNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell164 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblPhanLoaiNhapThueSuat = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell166 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaXacDinhMucThueNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow16 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell149 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell150 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell151 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienThueNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell153 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell154 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell155 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell156 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNuocXuatXu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell158 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenNoiXuatXu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell169 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaBieuThueNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow15 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell137 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell139 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell140 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoTienGiamThueNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell142 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell143 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell144 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell145 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaNgoaiHanNgach = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow18 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell146 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell147 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell175 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoThuTuDongHangToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow19 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell176 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell178 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell181 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDKDanhMucMienThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell182 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoDongTuongUngDMMienThue = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow20 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell183 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell184 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell185 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow21 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell186 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaMienGiamThueNK = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblDieuKhoanMienGiam = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow22 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell189 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell190 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell191 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow23 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell192 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell196 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell193 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell197 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell198 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTenKhoanMucThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell200 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell201 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell199 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaApDungThueSuatThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow24 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell202 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell203 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell204 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell205 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblTriGiaTinhThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell215 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell210 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell211 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell212 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell213 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblSoLuongTinhThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell217 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow25 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell219 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell220 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell221 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell222 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell223 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblThueSuatThueVaThuKhac1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell225 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell226 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell227 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell229 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell231 = new DevExpress.XtraReports.UI.XRTableCell();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.HeightF = 1.041667F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 19F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 58F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable1,
            this.xrTable5,
            this.lblTenThongTinXuat});
            this.ReportHeader.HeightF = 102F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable1
            // 
            this.xrTable1.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable1.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 37.5F);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5});
            this.xrTable1.SizeF = new System.Drawing.SizeF(796.0001F, 55F);
            this.xrTable1.StylePriority.UseFont = false;
            this.xrTable1.StylePriority.UsePadding = false;
            this.xrTable1.StylePriority.UseTextAlignment = false;
            this.xrTable1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell68,
            this.xrTableCell69,
            this.xrTableCell74,
            this.lblSoToKhai,
            this.xrTableCell73,
            this.xrTableCell75,
            this.xrTableCell72,
            this.lblSoToKhaiDauTien,
            this.xrTableCell76,
            this.lblSoNhanhToKhaiChiaNho,
            this.xrTableCell78,
            this.lblTongSoToKhaiChiaNho});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 0.44000000000000006;
            // 
            // xrTableCell68
            // 
            this.xrTableCell68.Name = "xrTableCell68";
            this.xrTableCell68.Weight = 0.22150619138694019;
            // 
            // xrTableCell69
            // 
            this.xrTableCell69.Name = "xrTableCell69";
            this.xrTableCell69.Text = "Số tờ khai";
            this.xrTableCell69.Weight = 0.69516055934634968;
            // 
            // xrTableCell74
            // 
            this.xrTableCell74.Name = "xrTableCell74";
            this.xrTableCell74.Weight = 0.050000002176888936;
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.StylePriority.UseFont = false;
            this.lblSoToKhai.Text = "NNNNNNNNN1NE";
            this.lblSoToKhai.Weight = 1.2358328433093813;
            // 
            // xrTableCell73
            // 
            this.xrTableCell73.Name = "xrTableCell73";
            this.xrTableCell73.Weight = 0.13791601462069569;
            // 
            // xrTableCell75
            // 
            this.xrTableCell75.Name = "xrTableCell75";
            this.xrTableCell75.Text = "Số tờ khai đầu tiên";
            this.xrTableCell75.Weight = 1.1215999457676427;
            // 
            // xrTableCell72
            // 
            this.xrTableCell72.Name = "xrTableCell72";
            this.xrTableCell72.Weight = 0.11282835597169623;
            // 
            // lblSoToKhaiDauTien
            // 
            this.lblSoToKhaiDauTien.Font = new System.Drawing.Font("Arial", 8.5F, System.Drawing.FontStyle.Bold);
            this.lblSoToKhaiDauTien.Name = "lblSoToKhaiDauTien";
            this.lblSoToKhaiDauTien.StylePriority.UseFont = false;
            this.lblSoToKhaiDauTien.Text = "XXXXXXXXX1XE";
            this.lblSoToKhaiDauTien.Weight = 1.2274477500810275;
            // 
            // xrTableCell76
            // 
            this.xrTableCell76.Name = "xrTableCell76";
            this.xrTableCell76.Text = "-";
            this.xrTableCell76.Weight = 0.13369657867317344;
            // 
            // lblSoNhanhToKhaiChiaNho
            // 
            this.lblSoNhanhToKhaiChiaNho.Name = "lblSoNhanhToKhaiChiaNho";
            this.lblSoNhanhToKhaiChiaNho.Text = "NE";
            this.lblSoNhanhToKhaiChiaNho.Weight = 0.2586999785805435;
            // 
            // xrTableCell78
            // 
            this.xrTableCell78.Name = "xrTableCell78";
            this.xrTableCell78.Text = "/";
            this.xrTableCell78.Weight = 0.12329998497247977;
            // 
            // lblTongSoToKhaiChiaNho
            // 
            this.lblTongSoToKhaiChiaNho.Name = "lblTongSoToKhaiChiaNho";
            this.lblTongSoToKhaiChiaNho.Text = "NE";
            this.lblTongSoToKhaiChiaNho.Weight = 2.6420121018148177;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell13,
            this.xrTableCell14,
            this.xrTableCell17,
            this.lblSoToKhaiTamNhapTaiXuat});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.StylePriority.UseTextAlignment = false;
            this.xrTableRow2.Weight = 0.44000000000000006;
            // 
            // xrTableCell13
            // 
            this.xrTableCell13.Name = "xrTableCell13";
            this.xrTableCell13.Weight = 0.22150619138694019;
            // 
            // xrTableCell14
            // 
            this.xrTableCell14.Name = "xrTableCell14";
            this.xrTableCell14.StylePriority.UseTextAlignment = false;
            this.xrTableCell14.Text = "Số tờ khai tạm nhập tái xuất tương ứng";
            this.xrTableCell14.Weight = 1.9809938467600321;
            // 
            // xrTableCell17
            // 
            this.xrTableCell17.Name = "xrTableCell17";
            this.xrTableCell17.Weight = 0.13791687011718773;
            // 
            // lblSoToKhaiTamNhapTaiXuat
            // 
            this.lblSoToKhaiTamNhapTaiXuat.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoToKhaiTamNhapTaiXuat.Name = "lblSoToKhaiTamNhapTaiXuat";
            this.lblSoToKhaiTamNhapTaiXuat.StylePriority.UseFont = false;
            this.lblSoToKhaiTamNhapTaiXuat.Text = "NNNNNNNNN1NE";
            this.lblSoToKhaiTamNhapTaiXuat.Weight = 5.6195833984374763;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell25,
            this.xrTableCell26,
            this.xrTableCell27,
            this.lblMaPhanLoaiKiemTra,
            this.xrTableCell29,
            this.xrTableCell30,
            this.xrTableCell31,
            this.lblMaLoaiHinh,
            this.lblMaPhanLoaiHangHoa,
            this.lblMaHieuPhuongThucVanChuyen,
            this.xrTableCell35,
            this.lblPhanLoaiCaNhanToChuc,
            this.xrTableCell19,
            this.xrTableCell21,
            this.xrTableCell18,
            this.xrTableCell22,
            this.lblMaSoHangHoaDaiDienToKhai});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.StylePriority.UseTextAlignment = false;
            this.xrTableRow3.Weight = 0.43999999999999995;
            // 
            // xrTableCell25
            // 
            this.xrTableCell25.Name = "xrTableCell25";
            this.xrTableCell25.Weight = 0.221505962505113;
            // 
            // xrTableCell26
            // 
            this.xrTableCell26.Name = "xrTableCell26";
            this.xrTableCell26.StylePriority.UseTextAlignment = false;
            this.xrTableCell26.Text = "Mã phân loại kiểm tra";
            this.xrTableCell26.Weight = 1.1641218686553305;
            // 
            // xrTableCell27
            // 
            this.xrTableCell27.Name = "xrTableCell27";
            this.xrTableCell27.Weight = 0.13354001659253928;
            // 
            // lblMaPhanLoaiKiemTra
            // 
            this.lblMaPhanLoaiKiemTra.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiKiemTra.Name = "lblMaPhanLoaiKiemTra";
            this.lblMaPhanLoaiKiemTra.StylePriority.UseFont = false;
            this.lblMaPhanLoaiKiemTra.Text = "XX E";
            this.lblMaPhanLoaiKiemTra.Weight = 0.68333215224701749;
            // 
            // xrTableCell29
            // 
            this.xrTableCell29.Name = "xrTableCell29";
            this.xrTableCell29.Weight = 0.13791687011718762;
            // 
            // xrTableCell30
            // 
            this.xrTableCell30.Name = "xrTableCell30";
            this.xrTableCell30.Text = "Mã loại hình";
            this.xrTableCell30.Weight = 0.78822906494140621;
            // 
            // xrTableCell31
            // 
            this.xrTableCell31.Name = "xrTableCell31";
            this.xrTableCell31.Weight = 0.10244812011718735;
            // 
            // lblMaLoaiHinh
            // 
            this.lblMaLoaiHinh.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaLoaiHinh.Name = "lblMaLoaiHinh";
            this.lblMaLoaiHinh.StylePriority.UseFont = false;
            this.lblMaLoaiHinh.Text = "XXE";
            this.lblMaLoaiHinh.Weight = 0.34375;
            // 
            // lblMaPhanLoaiHangHoa
            // 
            this.lblMaPhanLoaiHangHoa.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaPhanLoaiHangHoa.Name = "lblMaPhanLoaiHangHoa";
            this.lblMaPhanLoaiHangHoa.StylePriority.UseFont = false;
            this.lblMaPhanLoaiHangHoa.Text = "X";
            this.lblMaPhanLoaiHangHoa.Weight = 0.20489528656005862;
            // 
            // lblMaHieuPhuongThucVanChuyen
            // 
            this.lblMaHieuPhuongThucVanChuyen.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaHieuPhuongThucVanChuyen.Name = "lblMaHieuPhuongThucVanChuyen";
            this.lblMaHieuPhuongThucVanChuyen.StylePriority.UseFont = false;
            this.lblMaHieuPhuongThucVanChuyen.Text = "X";
            this.lblMaHieuPhuongThucVanChuyen.Weight = 0.23786464691162124;
            // 
            // xrTableCell35
            // 
            this.xrTableCell35.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell35.Name = "xrTableCell35";
            this.xrTableCell35.StylePriority.UseFont = false;
            this.xrTableCell35.Text = "[";
            this.xrTableCell35.Weight = 0.11286464691162124;
            // 
            // lblPhanLoaiCaNhanToChuc
            // 
            this.lblPhanLoaiCaNhanToChuc.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLoaiCaNhanToChuc.Name = "lblPhanLoaiCaNhanToChuc";
            this.lblPhanLoaiCaNhanToChuc.StylePriority.UseFont = false;
            this.lblPhanLoaiCaNhanToChuc.Text = "X";
            this.lblPhanLoaiCaNhanToChuc.Weight = 0.15994142055511468;
            // 
            // xrTableCell19
            // 
            this.xrTableCell19.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell19.Name = "xrTableCell19";
            this.xrTableCell19.StylePriority.UseFont = false;
            this.xrTableCell19.Text = "]";
            this.xrTableCell19.Weight = 0.11827485561370854;
            // 
            // xrTableCell21
            // 
            this.xrTableCell21.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell21.Name = "xrTableCell21";
            this.xrTableCell21.StylePriority.UseFont = false;
            this.xrTableCell21.Weight = 0.13910798549652093;
            // 
            // xrTableCell18
            // 
            this.xrTableCell18.Name = "xrTableCell18";
            this.xrTableCell18.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell18.StylePriority.UsePadding = false;
            this.xrTableCell18.StylePriority.UseTextAlignment = false;
            this.xrTableCell18.Text = "Mã số hàng hóa đại diện của tờ khai";
            this.xrTableCell18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell18.Weight = 1.8370245504379272;
            // 
            // xrTableCell22
            // 
            this.xrTableCell22.Name = "xrTableCell22";
            this.xrTableCell22.Weight = 0.13238280296325689;
            // 
            // lblMaSoHangHoaDaiDienToKhai
            // 
            this.lblMaSoHangHoaDaiDienToKhai.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaSoHangHoaDaiDienToKhai.Name = "lblMaSoHangHoaDaiDienToKhai";
            this.lblMaSoHangHoaDaiDienToKhai.StylePriority.UseFont = false;
            this.lblMaSoHangHoaDaiDienToKhai.Text = "XXXE";
            this.lblMaSoHangHoaDaiDienToKhai.Weight = 1.4428000560760264;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell37,
            this.xrTableCell38,
            this.lblTenCoQuanHaiQuanTiepNhanToKhai,
            this.xrTableCell41,
            this.xrTableCell47,
            this.xrTableCell23,
            this.lblMaBoPhanXuLyToKhai});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Weight = 0.44000000000000017;
            // 
            // xrTableCell37
            // 
            this.xrTableCell37.Name = "xrTableCell37";
            this.xrTableCell37.Weight = 0.22150588621117057;
            // 
            // xrTableCell38
            // 
            this.xrTableCell38.Name = "xrTableCell38";
            this.xrTableCell38.Text = "Tên cơ quan Hải quan tiếp nhận tờ khai";
            this.xrTableCell38.Weight = 2.1189108345554213;
            // 
            // lblTenCoQuanHaiQuanTiepNhanToKhai
            // 
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Name = "lblTenCoQuanHaiQuanTiepNhanToKhai";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.StylePriority.UseFont = false;
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Text = "XXXXXXXXXE";
            this.lblTenCoQuanHaiQuanTiepNhanToKhai.Weight = 2.0682683530859478;
            // 
            // xrTableCell41
            // 
            this.xrTableCell41.Name = "xrTableCell41";
            this.xrTableCell41.Weight = 0.13910766601562485;
            // 
            // xrTableCell47
            // 
            this.xrTableCell47.Name = "xrTableCell47";
            this.xrTableCell47.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell47.StylePriority.UsePadding = false;
            this.xrTableCell47.StylePriority.UseTextAlignment = false;
            this.xrTableCell47.Text = "Mã bộ phận xử lý tờ khai";
            this.xrTableCell47.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell47.Weight = 1.8370246505737304;
            // 
            // xrTableCell23
            // 
            this.xrTableCell23.Name = "xrTableCell23";
            this.xrTableCell23.Weight = 0.13238281249999989;
            // 
            // lblMaBoPhanXuLyToKhai
            // 
            this.lblMaBoPhanXuLyToKhai.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMaBoPhanXuLyToKhai.Name = "lblMaBoPhanXuLyToKhai";
            this.lblMaBoPhanXuLyToKhai.StylePriority.UseFont = false;
            this.lblMaBoPhanXuLyToKhai.Text = "XE";
            this.lblMaBoPhanXuLyToKhai.Weight = 1.442800103759742;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Borders = DevExpress.XtraPrinting.BorderSide.Bottom;
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell49,
            this.xrTableCell50,
            this.xrTableCell51,
            this.lblNgayDangKy,
            this.xrTableCell15,
            this.lblGioDangKy,
            this.xrTableCell54,
            this.xrTableCell55,
            this.lblNgayThayDoiDangKy,
            this.lblGioThayDoiDangKy,
            this.xrTableCell58,
            this.xrTableCell59,
            this.lblThoiHanTaiNhapTaiXuat,
            this.xrTableCell42,
            this.lblBieuThiTHHetHan});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.StylePriority.UseBorders = false;
            this.xrTableRow5.StylePriority.UseTextAlignment = false;
            this.xrTableRow5.Weight = 0.43999999999999984;
            // 
            // xrTableCell49
            // 
            this.xrTableCell49.Name = "xrTableCell49";
            this.xrTableCell49.Weight = 0.221505962505113;
            // 
            // xrTableCell50
            // 
            this.xrTableCell50.Name = "xrTableCell50";
            this.xrTableCell50.Text = "Ngày đăng ký";
            this.xrTableCell50.Weight = 0.735162056964867;
            // 
            // xrTableCell51
            // 
            this.xrTableCell51.Name = "xrTableCell51";
            this.xrTableCell51.Weight = 0.099999984722052085;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Text = "dd/MM/yyyy";
            this.lblNgayDangKy.Weight = 0.67083199580796826;
            // 
            // xrTableCell15
            // 
            this.xrTableCell15.Name = "xrTableCell15";
            this.xrTableCell15.Weight = 0.050000000000000044;
            // 
            // lblGioDangKy
            // 
            this.lblGioDangKy.Name = "lblGioDangKy";
            this.lblGioDangKy.Text = "hh:mm:ss";
            this.lblGioDangKy.Weight = 0.56291641235351564;
            // 
            // xrTableCell54
            // 
            this.xrTableCell54.Name = "xrTableCell54";
            this.xrTableCell54.Text = "Ngày thay đổi đăng ký";
            this.xrTableCell54.Weight = 1.1215989552751935;
            // 
            // xrTableCell55
            // 
            this.xrTableCell55.Name = "xrTableCell55";
            this.xrTableCell55.Weight = 0.11282926420508535;
            // 
            // lblNgayThayDoiDangKy
            // 
            this.lblNgayThayDoiDangKy.Name = "lblNgayThayDoiDangKy";
            this.lblNgayThayDoiDangKy.Text = "dd/MM/yyyy";
            this.lblNgayThayDoiDangKy.Weight = 0.71556573578055016;
            // 
            // lblGioThayDoiDangKy
            // 
            this.lblGioThayDoiDangKy.Name = "lblGioThayDoiDangKy";
            this.lblGioThayDoiDangKy.Text = "hh:mm:ss";
            this.lblGioThayDoiDangKy.Weight = 0.64557882153360424;
            // 
            // xrTableCell58
            // 
            this.xrTableCell58.Name = "xrTableCell58";
            this.xrTableCell58.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.xrTableCell58.StylePriority.UsePadding = false;
            this.xrTableCell58.StylePriority.UseTextAlignment = false;
            this.xrTableCell58.Text = "Thời hạn tái nhập/ tái xuất";
            this.xrTableCell58.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.xrTableCell58.Weight = 1.4488280868530274;
            // 
            // xrTableCell59
            // 
            this.xrTableCell59.Name = "xrTableCell59";
            this.xrTableCell59.Weight = 0.13238277435302748;
            // 
            // lblThoiHanTaiNhapTaiXuat
            // 
            this.lblThoiHanTaiNhapTaiXuat.Name = "lblThoiHanTaiNhapTaiXuat";
            this.lblThoiHanTaiNhapTaiXuat.Text = "dd/MM/yyyy";
            this.lblThoiHanTaiNhapTaiXuat.Weight = 0.81098310470581036;
            // 
            // xrTableCell42
            // 
            this.xrTableCell42.Name = "xrTableCell42";
            this.xrTableCell42.Text = "-";
            this.xrTableCell42.Weight = 0.17111817359924308;
            // 
            // lblBieuThiTHHetHan
            // 
            this.lblBieuThiTHHetHan.Font = new System.Drawing.Font("Arial", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBieuThiTHHetHan.Name = "lblBieuThiTHHetHan";
            this.lblBieuThiTHHetHan.StylePriority.UseFont = false;
            this.lblBieuThiTHHetHan.Text = "X";
            this.lblBieuThiTHHetHan.Weight = 0.46069897804257914;
            // 
            // xrTable5
            // 
            this.xrTable5.Font = new System.Drawing.Font("Arial", 9F);
            this.xrTable5.LocationFloat = new DevExpress.Utils.PointFloat(750F, 0F);
            this.xrTable5.Name = "xrTable5";
            this.xrTable5.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow36});
            this.xrTable5.SizeF = new System.Drawing.SizeF(63.1817F, 21.25001F);
            this.xrTable5.StylePriority.UseFont = false;
            // 
            // xrTableRow36
            // 
            this.xrTableRow36.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblSoTrang,
            this.xrTableCell208,
            this.lblTongSoTrang});
            this.xrTableRow36.Name = "xrTableRow36";
            this.xrTableRow36.Weight = 0.6;
            // 
            // lblSoTrang
            // 
            this.lblSoTrang.Name = "lblSoTrang";
            this.lblSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTrang.StylePriority.UsePadding = false;
            this.lblSoTrang.StylePriority.UseTextAlignment = false;
            this.lblSoTrang.Text = "3";
            this.lblSoTrang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTrang.Weight = 2.0268922190333454;
            // 
            // xrTableCell208
            // 
            this.xrTableCell208.Name = "xrTableCell208";
            this.xrTableCell208.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell208.StylePriority.UsePadding = false;
            this.xrTableCell208.Text = "/";
            this.xrTableCell208.Weight = 0.63735100986524751;
            // 
            // lblTongSoTrang
            // 
            this.lblTongSoTrang.Name = "lblTongSoTrang";
            this.lblTongSoTrang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblTongSoTrang.StylePriority.UsePadding = false;
            this.lblTongSoTrang.Text = "3";
            this.lblTongSoTrang.Weight = 1.3626489901347525;
            // 
            // lblTenThongTinXuat
            // 
            this.lblTenThongTinXuat.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTenThongTinXuat.LocationFloat = new DevExpress.Utils.PointFloat(150F, 0F);
            this.lblTenThongTinXuat.Name = "lblTenThongTinXuat";
            this.lblTenThongTinXuat.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenThongTinXuat.SizeF = new System.Drawing.SizeF(491.875F, 21.25F);
            this.lblTenThongTinXuat.StylePriority.UseFont = false;
            this.lblTenThongTinXuat.StylePriority.UseTextAlignment = false;
            this.lblTenThongTinXuat.Text = "Tờ khai tạm hàng hóa nhập khẩu ";
            this.lblTenThongTinXuat.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // DetailReport
            // 
            this.DetailReport.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail1});
            this.DetailReport.Level = 0;
            this.DetailReport.Name = "DetailReport";
            // 
            // Detail1
            // 
            this.Detail1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable12,
            this.xrTable13,
            this.xrTable6,
            this.xrTable9,
            this.xrTable14,
            this.xrTable10,
            this.xrTable15,
            this.xrTable17,
            this.xrTable16,
            this.xrTable11,
            this.xrTable7,
            this.xrTable4,
            this.xrTable8,
            this.xrTable3,
            this.xrTable2});
            this.Detail1.HeightF = 837F;
            this.Detail1.Name = "Detail1";
            // 
            // xrTable12
            // 
            this.xrTable12.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable12.LocationFloat = new DevExpress.Utils.PointFloat(400.474F, 432.0001F);
            this.xrTable12.Name = "xrTable12";
            this.xrTable12.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow40});
            this.xrTable12.SizeF = new System.Drawing.SizeF(408.0262F, 28F);
            this.xrTable12.StylePriority.UseFont = false;
            // 
            // xrTableRow40
            // 
            this.xrTableRow40.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell302,
            this.lblMaMienGiamThueVaThuKhac4,
            this.xrTableCell304,
            this.lblDieuKhoanMienGiamThueVaThuKhac4});
            this.xrTableRow40.Name = "xrTableRow40";
            this.xrTableRow40.Weight = 0.39529476275110736;
            // 
            // xrTableCell302
            // 
            this.xrTableCell302.Name = "xrTableCell302";
            this.xrTableCell302.Weight = 2.7209952551554863;
            // 
            // lblMaMienGiamThueVaThuKhac4
            // 
            this.lblMaMienGiamThueVaThuKhac4.Name = "lblMaMienGiamThueVaThuKhac4";
            this.lblMaMienGiamThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaMienGiamThueVaThuKhac4.StylePriority.UsePadding = false;
            this.lblMaMienGiamThueVaThuKhac4.Text = "XXXXE";
            this.lblMaMienGiamThueVaThuKhac4.Weight = 4.5535709609102684;
            // 
            // xrTableCell304
            // 
            this.xrTableCell304.Name = "xrTableCell304";
            this.xrTableCell304.Weight = 1.4416484607087217;
            // 
            // lblDieuKhoanMienGiamThueVaThuKhac4
            // 
            this.lblDieuKhoanMienGiamThueVaThuKhac4.Multiline = true;
            this.lblDieuKhoanMienGiamThueVaThuKhac4.Name = "lblDieuKhoanMienGiamThueVaThuKhac4";
            this.lblDieuKhoanMienGiamThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblDieuKhoanMienGiamThueVaThuKhac4.StylePriority.UsePadding = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac4.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiamThueVaThuKhac4.Weight = 16.011344714770793;
            // 
            // xrTable13
            // 
            this.xrTable13.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable13.LocationFloat = new DevExpress.Utils.PointFloat(400.474F, 493.0001F);
            this.xrTable13.Name = "xrTable13";
            this.xrTable13.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow41});
            this.xrTable13.SizeF = new System.Drawing.SizeF(408.0262F, 28F);
            this.xrTable13.StylePriority.UseFont = false;
            // 
            // xrTableRow41
            // 
            this.xrTableRow41.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell306,
            this.lblMaMienGiamThueVaThuKhac5,
            this.xrTableCell308,
            this.lblDieuKhoanMienGiamThueVaThuKhac5});
            this.xrTableRow41.Name = "xrTableRow41";
            this.xrTableRow41.Weight = 0.39529476275110736;
            // 
            // xrTableCell306
            // 
            this.xrTableCell306.Name = "xrTableCell306";
            this.xrTableCell306.Weight = 2.7209952551554863;
            // 
            // lblMaMienGiamThueVaThuKhac5
            // 
            this.lblMaMienGiamThueVaThuKhac5.Name = "lblMaMienGiamThueVaThuKhac5";
            this.lblMaMienGiamThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaMienGiamThueVaThuKhac5.StylePriority.UsePadding = false;
            this.lblMaMienGiamThueVaThuKhac5.Text = "XXXXE";
            this.lblMaMienGiamThueVaThuKhac5.Weight = 4.5535783587214738;
            // 
            // xrTableCell308
            // 
            this.xrTableCell308.Name = "xrTableCell308";
            this.xrTableCell308.Weight = 1.4416484607087217;
            // 
            // lblDieuKhoanMienGiamThueVaThuKhac5
            // 
            this.lblDieuKhoanMienGiamThueVaThuKhac5.Multiline = true;
            this.lblDieuKhoanMienGiamThueVaThuKhac5.Name = "lblDieuKhoanMienGiamThueVaThuKhac5";
            this.lblDieuKhoanMienGiamThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblDieuKhoanMienGiamThueVaThuKhac5.StylePriority.UsePadding = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac5.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiamThueVaThuKhac5.Weight = 16.011337316959587;
            // 
            // xrTable6
            // 
            this.xrTable6.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable6.LocationFloat = new DevExpress.Utils.PointFloat(12.50003F, 310F);
            this.xrTable6.Name = "xrTable6";
            this.xrTable6.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow27,
            this.xrTableRow30});
            this.xrTable6.SizeF = new System.Drawing.SizeF(387.9739F, 28.00003F);
            this.xrTable6.StylePriority.UseFont = false;
            // 
            // xrTableRow27
            // 
            this.xrTableRow27.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell233,
            this.xrTableCell234,
            this.xrTableCell236,
            this.lblSoTienThueVaThuKhac2,
            this.xrTableCell238,
            this.xrTableCell239});
            this.xrTableRow27.Name = "xrTableRow27";
            this.xrTableRow27.Weight = 0.31058809590593411;
            // 
            // xrTableCell233
            // 
            this.xrTableCell233.Name = "xrTableCell233";
            this.xrTableCell233.Weight = 2.7209952551554863;
            // 
            // xrTableCell234
            // 
            this.xrTableCell234.Name = "xrTableCell234";
            this.xrTableCell234.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell234.StylePriority.UsePadding = false;
            this.xrTableCell234.Text = "Số tiền thuế";
            this.xrTableCell234.Weight = 4.70149094729393;
            // 
            // xrTableCell236
            // 
            this.xrTableCell236.Name = "xrTableCell236";
            this.xrTableCell236.Weight = 0.80996710510377756;
            // 
            // lblSoTienThueVaThuKhac2
            // 
            this.lblSoTienThueVaThuKhac2.Name = "lblSoTienThueVaThuKhac2";
            this.lblSoTienThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienThueVaThuKhac2.StylePriority.UsePadding = false;
            this.lblSoTienThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueVaThuKhac2.Text = "1.234.567.890.123.456";
            this.lblSoTienThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueVaThuKhac2.Weight = 13.195392665697977;
            // 
            // xrTableCell238
            // 
            this.xrTableCell238.Name = "xrTableCell238";
            this.xrTableCell238.Weight = 1.356459922239643;
            // 
            // xrTableCell239
            // 
            this.xrTableCell239.Name = "xrTableCell239";
            this.xrTableCell239.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell239.StylePriority.UsePadding = false;
            this.xrTableCell239.Text = "VND";
            this.xrTableCell239.Weight = 1.943253496054453;
            // 
            // xrTableRow30
            // 
            this.xrTableRow30.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell240,
            this.xrTableCell241,
            this.xrTableCell242,
            this.lblSoTienGiamThueVaThuKhac2,
            this.xrTableCell244,
            this.xrTableCell245});
            this.xrTableRow30.Name = "xrTableRow30";
            this.xrTableRow30.Weight = 0.48000064625430561;
            // 
            // xrTableCell240
            // 
            this.xrTableCell240.Name = "xrTableCell240";
            this.xrTableCell240.Weight = 2.7209952551554863;
            // 
            // xrTableCell241
            // 
            this.xrTableCell241.Name = "xrTableCell241";
            this.xrTableCell241.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell241.StylePriority.UsePadding = false;
            this.xrTableCell241.Text = "Số tiền miễn giảm";
            this.xrTableCell241.Weight = 6.1488070856949344;
            // 
            // xrTableCell242
            // 
            this.xrTableCell242.Name = "xrTableCell242";
            this.xrTableCell242.Weight = 0.87635872058372155;
            // 
            // lblSoTienGiamThueVaThuKhac2
            // 
            this.lblSoTienGiamThueVaThuKhac2.Name = "lblSoTienGiamThueVaThuKhac2";
            this.lblSoTienGiamThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienGiamThueVaThuKhac2.StylePriority.UsePadding = false;
            this.lblSoTienGiamThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueVaThuKhac2.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueVaThuKhac2.Weight = 11.68168491181703;
            // 
            // xrTableCell244
            // 
            this.xrTableCell244.Name = "xrTableCell244";
            this.xrTableCell244.Weight = 1.356459922239643;
            // 
            // xrTableCell245
            // 
            this.xrTableCell245.Name = "xrTableCell245";
            this.xrTableCell245.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell245.StylePriority.UsePadding = false;
            this.xrTableCell245.Text = "VND";
            this.xrTableCell245.Weight = 1.943253496054453;
            // 
            // xrTable9
            // 
            this.xrTable9.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable9.LocationFloat = new DevExpress.Utils.PointFloat(12.50007F, 432.0001F);
            this.xrTable9.Name = "xrTable9";
            this.xrTable9.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow35,
            this.xrTableRow37});
            this.xrTable9.SizeF = new System.Drawing.SizeF(387.9739F, 28.00003F);
            this.xrTable9.StylePriority.UseFont = false;
            // 
            // xrTableRow35
            // 
            this.xrTableRow35.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell282,
            this.xrTableCell283,
            this.xrTableCell284,
            this.lblSoTienThueVaThuKhac5,
            this.xrTableCell286,
            this.xrTableCell287});
            this.xrTableRow35.Name = "xrTableRow35";
            this.xrTableRow35.Weight = 0.31058809590593411;
            // 
            // xrTableCell282
            // 
            this.xrTableCell282.Name = "xrTableCell282";
            this.xrTableCell282.Weight = 2.7209952551554863;
            // 
            // xrTableCell283
            // 
            this.xrTableCell283.Name = "xrTableCell283";
            this.xrTableCell283.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell283.StylePriority.UsePadding = false;
            this.xrTableCell283.Text = "Số tiền thuế";
            this.xrTableCell283.Weight = 4.70149094729393;
            // 
            // xrTableCell284
            // 
            this.xrTableCell284.Name = "xrTableCell284";
            this.xrTableCell284.Weight = 0.80996710510377756;
            // 
            // lblSoTienThueVaThuKhac5
            // 
            this.lblSoTienThueVaThuKhac5.Name = "lblSoTienThueVaThuKhac5";
            this.lblSoTienThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienThueVaThuKhac5.StylePriority.UsePadding = false;
            this.lblSoTienThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueVaThuKhac5.Text = "1.234.567.890.123.456";
            this.lblSoTienThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueVaThuKhac5.Weight = 13.195392665697977;
            // 
            // xrTableCell286
            // 
            this.xrTableCell286.Name = "xrTableCell286";
            this.xrTableCell286.Weight = 1.356459922239643;
            // 
            // xrTableCell287
            // 
            this.xrTableCell287.Name = "xrTableCell287";
            this.xrTableCell287.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell287.StylePriority.UsePadding = false;
            this.xrTableCell287.Text = "VND";
            this.xrTableCell287.Weight = 1.943253496054453;
            // 
            // xrTableRow37
            // 
            this.xrTableRow37.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell288,
            this.xrTableCell289,
            this.xrTableCell290,
            this.lblSoTienGiamThueVaThuKhac5,
            this.xrTableCell292,
            this.xrTableCell293});
            this.xrTableRow37.Name = "xrTableRow37";
            this.xrTableRow37.Weight = 0.48000064625430561;
            // 
            // xrTableCell288
            // 
            this.xrTableCell288.Name = "xrTableCell288";
            this.xrTableCell288.Weight = 2.7209952551554863;
            // 
            // xrTableCell289
            // 
            this.xrTableCell289.Name = "xrTableCell289";
            this.xrTableCell289.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell289.StylePriority.UsePadding = false;
            this.xrTableCell289.Text = "Số tiền miễn giảm";
            this.xrTableCell289.Weight = 6.1488070856949344;
            // 
            // xrTableCell290
            // 
            this.xrTableCell290.Name = "xrTableCell290";
            this.xrTableCell290.Weight = 0.87635872058372155;
            // 
            // lblSoTienGiamThueVaThuKhac5
            // 
            this.lblSoTienGiamThueVaThuKhac5.Name = "lblSoTienGiamThueVaThuKhac5";
            this.lblSoTienGiamThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienGiamThueVaThuKhac5.StylePriority.UsePadding = false;
            this.lblSoTienGiamThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueVaThuKhac5.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueVaThuKhac5.Weight = 11.68168491181703;
            // 
            // xrTableCell292
            // 
            this.xrTableCell292.Name = "xrTableCell292";
            this.xrTableCell292.Weight = 1.356459922239643;
            // 
            // xrTableCell293
            // 
            this.xrTableCell293.Name = "xrTableCell293";
            this.xrTableCell293.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell293.StylePriority.UsePadding = false;
            this.xrTableCell293.Text = "VND";
            this.xrTableCell293.Weight = 1.943253496054453;
            // 
            // xrTable14
            // 
            this.xrTable14.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable14.LocationFloat = new DevExpress.Utils.PointFloat(12.50003F, 277F);
            this.xrTable14.Name = "xrTable14";
            this.xrTable14.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow42,
            this.xrTableRow43,
            this.xrTableRow44});
            this.xrTable14.SizeF = new System.Drawing.SizeF(796F, 33F);
            this.xrTable14.StylePriority.UseFont = false;
            // 
            // xrTableRow42
            // 
            this.xrTableRow42.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow42.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell322,
            this.xrTableCell323,
            this.xrTableCell310,
            this.xrTableCell311,
            this.xrTableCell312,
            this.lblTenKhoanMucThueVaThuKhac2,
            this.xrTableCell314,
            this.xrTableCell324,
            this.xrTableCell326,
            this.xrTableCell327,
            this.xrTableCell325,
            this.lblMaApDungThueSuatThueVaThuKhac2});
            this.xrTableRow42.Name = "xrTableRow42";
            this.xrTableRow42.StylePriority.UseBorders = false;
            this.xrTableRow42.Weight = 0.31058809590593411;
            // 
            // xrTableCell322
            // 
            this.xrTableCell322.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell322.Name = "xrTableCell322";
            this.xrTableCell322.StylePriority.UseBorders = false;
            this.xrTableCell322.Weight = 1.1472320241718166;
            // 
            // xrTableCell323
            // 
            this.xrTableCell323.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell323.Name = "xrTableCell323";
            this.xrTableCell323.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell323.StylePriority.UseBorders = false;
            this.xrTableCell323.StylePriority.UsePadding = false;
            this.xrTableCell323.Text = "2";
            this.xrTableCell323.Weight = 0.93641266167101411;
            // 
            // xrTableCell310
            // 
            this.xrTableCell310.Name = "xrTableCell310";
            this.xrTableCell310.Weight = 0.63735056931265555;
            // 
            // xrTableCell311
            // 
            this.xrTableCell311.Name = "xrTableCell311";
            this.xrTableCell311.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell311.StylePriority.UsePadding = false;
            this.xrTableCell311.Text = "Tên";
            this.xrTableCell311.Weight = 4.70149094729393;
            // 
            // xrTableCell312
            // 
            this.xrTableCell312.Name = "xrTableCell312";
            this.xrTableCell312.Weight = 0.80996710510377756;
            // 
            // lblTenKhoanMucThueVaThuKhac2
            // 
            this.lblTenKhoanMucThueVaThuKhac2.Name = "lblTenKhoanMucThueVaThuKhac2";
            this.lblTenKhoanMucThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblTenKhoanMucThueVaThuKhac2.StylePriority.UsePadding = false;
            this.lblTenKhoanMucThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblTenKhoanMucThueVaThuKhac2.Text = "WWWWWWWWE";
            this.lblTenKhoanMucThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTenKhoanMucThueVaThuKhac2.Weight = 13.195392665697977;
            // 
            // xrTableCell314
            // 
            this.xrTableCell314.Name = "xrTableCell314";
            this.xrTableCell314.Weight = 1.356459922239643;
            // 
            // xrTableCell324
            // 
            this.xrTableCell324.Name = "xrTableCell324";
            this.xrTableCell324.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell324.StylePriority.UsePadding = false;
            this.xrTableCell324.Weight = 1.9432515685610223;
            // 
            // xrTableCell326
            // 
            this.xrTableCell326.Name = "xrTableCell326";
            this.xrTableCell326.Weight = 0.87868394838373565;
            // 
            // xrTableCell327
            // 
            this.xrTableCell327.Name = "xrTableCell327";
            this.xrTableCell327.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell327.StylePriority.UsePadding = false;
            this.xrTableCell327.Text = "Mã áp dụng thuế suất";
            this.xrTableCell327.Weight = 7.5021584071493166;
            // 
            // xrTableCell325
            // 
            this.xrTableCell325.Name = "xrTableCell325";
            this.xrTableCell325.Weight = 0.78586659291903538;
            // 
            // lblMaApDungThueSuatThueVaThuKhac2
            // 
            this.lblMaApDungThueSuatThueVaThuKhac2.Name = "lblMaApDungThueSuatThueVaThuKhac2";
            this.lblMaApDungThueSuatThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaApDungThueSuatThueVaThuKhac2.StylePriority.UsePadding = false;
            this.lblMaApDungThueSuatThueVaThuKhac2.Text = "XXXXXXXXXE";
            this.lblMaApDungThueSuatThueVaThuKhac2.Weight = 16.838876586046052;
            // 
            // xrTableRow43
            // 
            this.xrTableRow43.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell316,
            this.xrTableCell317,
            this.xrTableCell318,
            this.lblTriGiaTinhThueVaThuKhac2,
            this.xrTableCell320,
            this.xrTableCell328,
            this.xrTableCell330,
            this.xrTableCell331,
            this.xrTableCell329,
            this.lblSoLuongTinhThueVaThuKhac2,
            this.xrTableCell333,
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2});
            this.xrTableRow43.Name = "xrTableRow43";
            this.xrTableRow43.Weight = 0.31058809590593411;
            // 
            // xrTableCell316
            // 
            this.xrTableCell316.Name = "xrTableCell316";
            this.xrTableCell316.Weight = 2.7209952551554863;
            // 
            // xrTableCell317
            // 
            this.xrTableCell317.Name = "xrTableCell317";
            this.xrTableCell317.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell317.StylePriority.UsePadding = false;
            this.xrTableCell317.Text = "Trị giá tính thuế";
            this.xrTableCell317.Weight = 5.5114600045137152;
            // 
            // xrTableCell318
            // 
            this.xrTableCell318.Name = "xrTableCell318";
            this.xrTableCell318.Weight = 1.5137058017649405;
            // 
            // lblTriGiaTinhThueVaThuKhac2
            // 
            this.lblTriGiaTinhThueVaThuKhac2.Name = "lblTriGiaTinhThueVaThuKhac2";
            this.lblTriGiaTinhThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueVaThuKhac2.StylePriority.UsePadding = false;
            this.lblTriGiaTinhThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueVaThuKhac2.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueVaThuKhac2.Weight = 11.68168491181703;
            // 
            // xrTableCell320
            // 
            this.xrTableCell320.Name = "xrTableCell320";
            this.xrTableCell320.Weight = 1.356459922239643;
            // 
            // xrTableCell328
            // 
            this.xrTableCell328.Name = "xrTableCell328";
            this.xrTableCell328.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell328.StylePriority.UsePadding = false;
            this.xrTableCell328.Text = "VND";
            this.xrTableCell328.Weight = 1.9432525410815345;
            // 
            // xrTableCell330
            // 
            this.xrTableCell330.Name = "xrTableCell330";
            this.xrTableCell330.Weight = 0.87868419151386323;
            // 
            // xrTableCell331
            // 
            this.xrTableCell331.Name = "xrTableCell331";
            this.xrTableCell331.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell331.StylePriority.UsePadding = false;
            this.xrTableCell331.Text = "Số lượng tính thuế";
            this.xrTableCell331.Weight = 6.7718583518315167;
            // 
            // xrTableCell329
            // 
            this.xrTableCell329.Name = "xrTableCell329";
            this.xrTableCell329.Weight = 1.5161620287644064;
            // 
            // lblSoLuongTinhThueVaThuKhac2
            // 
            this.lblSoLuongTinhThueVaThuKhac2.Name = "lblSoLuongTinhThueVaThuKhac2";
            this.lblSoLuongTinhThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoLuongTinhThueVaThuKhac2.StylePriority.UsePadding = false;
            this.lblSoLuongTinhThueVaThuKhac2.Text = "123.456.789.012";
            this.lblSoLuongTinhThueVaThuKhac2.Weight = 5.9363701110552345;
            // 
            // xrTableCell333
            // 
            this.xrTableCell333.Name = "xrTableCell333";
            this.xrTableCell333.Weight = 0.92679648534315107;
            // 
            // lblMaDonViTinhChuanDanhThueVaThuKhac2
            // 
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.Name = "lblMaDonViTinhChuanDanhThueVaThuKhac2";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac2.Weight = 9.9757133934694551;
            // 
            // xrTableRow44
            // 
            this.xrTableRow44.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell334,
            this.xrTableCell335,
            this.xrTableCell336,
            this.lblThueSuatThueVaThuKhac2,
            this.xrTableCell338,
            this.xrTableCell339,
            this.xrTableCell340,
            this.xrTableCell341});
            this.xrTableRow44.Name = "xrTableRow44";
            this.xrTableRow44.Weight = 0.31058809590593411;
            // 
            // xrTableCell334
            // 
            this.xrTableCell334.Name = "xrTableCell334";
            this.xrTableCell334.Weight = 2.7209952551554863;
            // 
            // xrTableCell335
            // 
            this.xrTableCell335.Name = "xrTableCell335";
            this.xrTableCell335.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell335.StylePriority.UsePadding = false;
            this.xrTableCell335.Text = "Thuế suất";
            this.xrTableCell335.Weight = 4.70149094729393;
            // 
            // xrTableCell336
            // 
            this.xrTableCell336.Name = "xrTableCell336";
            this.xrTableCell336.Weight = 0.80997002266531348;
            // 
            // lblThueSuatThueVaThuKhac2
            // 
            this.lblThueSuatThueVaThuKhac2.Name = "lblThueSuatThueVaThuKhac2";
            this.lblThueSuatThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatThueVaThuKhac2.StylePriority.UsePadding = false;
            this.lblThueSuatThueVaThuKhac2.StylePriority.UseTextAlignment = false;
            this.lblThueSuatThueVaThuKhac2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThueSuatThueVaThuKhac2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblThueSuatThueVaThuKhac2.Weight = 13.195389748136442;
            // 
            // xrTableCell338
            // 
            this.xrTableCell338.Name = "xrTableCell338";
            this.xrTableCell338.Weight = 1.356459922239643;
            // 
            // xrTableCell339
            // 
            this.xrTableCell339.Name = "xrTableCell339";
            this.xrTableCell339.Weight = 1.9432525410815345;
            // 
            // xrTableCell340
            // 
            this.xrTableCell340.Name = "xrTableCell340";
            this.xrTableCell340.Weight = 0.87868419151386323;
            // 
            // xrTableCell341
            // 
            this.xrTableCell341.Name = "xrTableCell341";
            this.xrTableCell341.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell341.StylePriority.UsePadding = false;
            this.xrTableCell341.Text = "Miễn / Giảm / Không chịu thuế và thu khác";
            this.xrTableCell341.Weight = 25.126900370463765;
            // 
            // xrTable10
            // 
            this.xrTable10.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable10.LocationFloat = new DevExpress.Utils.PointFloat(400.4738F, 310F);
            this.xrTable10.Name = "xrTable10";
            this.xrTable10.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow38});
            this.xrTable10.SizeF = new System.Drawing.SizeF(408.0262F, 28F);
            this.xrTable10.StylePriority.UseFont = false;
            // 
            // xrTableRow38
            // 
            this.xrTableRow38.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell294,
            this.lblMaMienGiamThueVaThuKhac2,
            this.xrTableCell296,
            this.lblDieuKhoanMienGiamThueVaThuKhac2});
            this.xrTableRow38.Name = "xrTableRow38";
            this.xrTableRow38.Weight = 0.39529476275110736;
            // 
            // xrTableCell294
            // 
            this.xrTableCell294.Name = "xrTableCell294";
            this.xrTableCell294.Weight = 2.7209952551554863;
            // 
            // lblMaMienGiamThueVaThuKhac2
            // 
            this.lblMaMienGiamThueVaThuKhac2.Name = "lblMaMienGiamThueVaThuKhac2";
            this.lblMaMienGiamThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaMienGiamThueVaThuKhac2.StylePriority.UsePadding = false;
            this.lblMaMienGiamThueVaThuKhac2.Text = "XXXXE";
            this.lblMaMienGiamThueVaThuKhac2.Weight = 4.5535786476984734;
            // 
            // xrTableCell296
            // 
            this.xrTableCell296.Name = "xrTableCell296";
            this.xrTableCell296.Weight = 1.4416484607087217;
            // 
            // lblDieuKhoanMienGiamThueVaThuKhac2
            // 
            this.lblDieuKhoanMienGiamThueVaThuKhac2.Multiline = true;
            this.lblDieuKhoanMienGiamThueVaThuKhac2.Name = "lblDieuKhoanMienGiamThueVaThuKhac2";
            this.lblDieuKhoanMienGiamThueVaThuKhac2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblDieuKhoanMienGiamThueVaThuKhac2.StylePriority.UsePadding = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac2.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiamThueVaThuKhac2.Weight = 16.011337027982588;
            // 
            // xrTable15
            // 
            this.xrTable15.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable15.LocationFloat = new DevExpress.Utils.PointFloat(12.50003F, 338.0001F);
            this.xrTable15.Name = "xrTable15";
            this.xrTable15.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow45,
            this.xrTableRow46,
            this.xrTableRow47});
            this.xrTable15.SizeF = new System.Drawing.SizeF(796F, 33F);
            this.xrTable15.StylePriority.UseFont = false;
            // 
            // xrTableRow45
            // 
            this.xrTableRow45.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow45.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell342,
            this.xrTableCell343,
            this.xrTableCell344,
            this.aa,
            this.xrTableCell346,
            this.lblTenKhoanMucThueVaThuKhac3,
            this.xrTableCell348,
            this.xrTableCell349,
            this.xrTableCell350,
            this.xrTableCell351,
            this.xrTableCell352,
            this.lblMaApDungThueSuatThueVaThuKhac3});
            this.xrTableRow45.Name = "xrTableRow45";
            this.xrTableRow45.StylePriority.UseBorders = false;
            this.xrTableRow45.Weight = 0.31058809590593411;
            // 
            // xrTableCell342
            // 
            this.xrTableCell342.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell342.Name = "xrTableCell342";
            this.xrTableCell342.StylePriority.UseBorders = false;
            this.xrTableCell342.Weight = 1.1472320241718166;
            // 
            // xrTableCell343
            // 
            this.xrTableCell343.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell343.Name = "xrTableCell343";
            this.xrTableCell343.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell343.StylePriority.UseBorders = false;
            this.xrTableCell343.StylePriority.UsePadding = false;
            this.xrTableCell343.Text = "3";
            this.xrTableCell343.Weight = 0.93641266167101411;
            // 
            // xrTableCell344
            // 
            this.xrTableCell344.Name = "xrTableCell344";
            this.xrTableCell344.Weight = 0.63735056931265555;
            // 
            // aa
            // 
            this.aa.Name = "aa";
            this.aa.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.aa.StylePriority.UsePadding = false;
            this.aa.Text = "Tên";
            this.aa.Weight = 4.70149094729393;
            // 
            // xrTableCell346
            // 
            this.xrTableCell346.Name = "xrTableCell346";
            this.xrTableCell346.Weight = 0.80996710510377756;
            // 
            // lblTenKhoanMucThueVaThuKhac3
            // 
            this.lblTenKhoanMucThueVaThuKhac3.Name = "lblTenKhoanMucThueVaThuKhac3";
            this.lblTenKhoanMucThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblTenKhoanMucThueVaThuKhac3.StylePriority.UsePadding = false;
            this.lblTenKhoanMucThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblTenKhoanMucThueVaThuKhac3.Text = "WWWWWWWWE";
            this.lblTenKhoanMucThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTenKhoanMucThueVaThuKhac3.Weight = 13.195392665697977;
            // 
            // xrTableCell348
            // 
            this.xrTableCell348.Name = "xrTableCell348";
            this.xrTableCell348.Weight = 1.356459922239643;
            // 
            // xrTableCell349
            // 
            this.xrTableCell349.Name = "xrTableCell349";
            this.xrTableCell349.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell349.StylePriority.UsePadding = false;
            this.xrTableCell349.Weight = 1.9432515685610223;
            // 
            // xrTableCell350
            // 
            this.xrTableCell350.Name = "xrTableCell350";
            this.xrTableCell350.Weight = 0.87868394838373565;
            // 
            // xrTableCell351
            // 
            this.xrTableCell351.Name = "xrTableCell351";
            this.xrTableCell351.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell351.StylePriority.UsePadding = false;
            this.xrTableCell351.Text = "Mã áp dụng thuế suất";
            this.xrTableCell351.Weight = 7.5658857312547569;
            // 
            // xrTableCell352
            // 
            this.xrTableCell352.Name = "xrTableCell352";
            this.xrTableCell352.Weight = 0.72213926881359558;
            // 
            // lblMaApDungThueSuatThueVaThuKhac3
            // 
            this.lblMaApDungThueSuatThueVaThuKhac3.Name = "lblMaApDungThueSuatThueVaThuKhac3";
            this.lblMaApDungThueSuatThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaApDungThueSuatThueVaThuKhac3.StylePriority.UsePadding = false;
            this.lblMaApDungThueSuatThueVaThuKhac3.Text = "XXXXXXXXXE";
            this.lblMaApDungThueSuatThueVaThuKhac3.Weight = 16.838876586046052;
            // 
            // xrTableRow46
            // 
            this.xrTableRow46.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell354,
            this.xrTableCell355,
            this.xrTableCell356,
            this.lblTriGiaTinhThueVaThuKhac3,
            this.xrTableCell358,
            this.xrTableCell359,
            this.xrTableCell360,
            this.xrTableCell361,
            this.xrTableCell362,
            this.lblSoLuongTinhThueVaThuKhac3,
            this.xrTableCell364,
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3});
            this.xrTableRow46.Name = "xrTableRow46";
            this.xrTableRow46.Weight = 0.31058809590593411;
            // 
            // xrTableCell354
            // 
            this.xrTableCell354.Name = "xrTableCell354";
            this.xrTableCell354.Weight = 2.7209952551554863;
            // 
            // xrTableCell355
            // 
            this.xrTableCell355.Name = "xrTableCell355";
            this.xrTableCell355.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell355.StylePriority.UsePadding = false;
            this.xrTableCell355.Text = "Trị giá tính thuế";
            this.xrTableCell355.Weight = 5.5114600045137152;
            // 
            // xrTableCell356
            // 
            this.xrTableCell356.Name = "xrTableCell356";
            this.xrTableCell356.Weight = 1.5137058017649405;
            // 
            // lblTriGiaTinhThueVaThuKhac3
            // 
            this.lblTriGiaTinhThueVaThuKhac3.Name = "lblTriGiaTinhThueVaThuKhac3";
            this.lblTriGiaTinhThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueVaThuKhac3.StylePriority.UsePadding = false;
            this.lblTriGiaTinhThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueVaThuKhac3.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueVaThuKhac3.Weight = 11.68168491181703;
            // 
            // xrTableCell358
            // 
            this.xrTableCell358.Name = "xrTableCell358";
            this.xrTableCell358.Weight = 1.356459922239643;
            // 
            // xrTableCell359
            // 
            this.xrTableCell359.Name = "xrTableCell359";
            this.xrTableCell359.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell359.StylePriority.UsePadding = false;
            this.xrTableCell359.Text = "VND";
            this.xrTableCell359.Weight = 1.9432525410815345;
            // 
            // xrTableCell360
            // 
            this.xrTableCell360.Name = "xrTableCell360";
            this.xrTableCell360.Weight = 0.87868419151386323;
            // 
            // xrTableCell361
            // 
            this.xrTableCell361.Name = "xrTableCell361";
            this.xrTableCell361.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell361.StylePriority.UsePadding = false;
            this.xrTableCell361.Text = "Số lượng tính thuế";
            this.xrTableCell361.Weight = 6.7718583518315167;
            // 
            // xrTableCell362
            // 
            this.xrTableCell362.Name = "xrTableCell362";
            this.xrTableCell362.Weight = 1.5161620287644066;
            // 
            // lblSoLuongTinhThueVaThuKhac3
            // 
            this.lblSoLuongTinhThueVaThuKhac3.Name = "lblSoLuongTinhThueVaThuKhac3";
            this.lblSoLuongTinhThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoLuongTinhThueVaThuKhac3.StylePriority.UsePadding = false;
            this.lblSoLuongTinhThueVaThuKhac3.Text = "123.456.789.012";
            this.lblSoLuongTinhThueVaThuKhac3.Weight = 5.9363701110552345;
            // 
            // xrTableCell364
            // 
            this.xrTableCell364.Name = "xrTableCell364";
            this.xrTableCell364.Weight = 0.926796485343151;
            // 
            // lblMaDonViTinhChuanDanhThueVaThuKhac3
            // 
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.Name = "lblMaDonViTinhChuanDanhThueVaThuKhac3";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac3.Weight = 9.9757133934694551;
            // 
            // xrTableRow47
            // 
            this.xrTableRow47.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell366,
            this.xrTableCell367,
            this.xrTableCell368,
            this.lblThueSuatThueVaThuKhac3,
            this.xrTableCell370,
            this.xrTableCell371,
            this.xrTableCell372,
            this.xrTableCell373});
            this.xrTableRow47.Name = "xrTableRow47";
            this.xrTableRow47.Weight = 0.31058809590593411;
            // 
            // xrTableCell366
            // 
            this.xrTableCell366.Name = "xrTableCell366";
            this.xrTableCell366.Weight = 2.7209952551554863;
            // 
            // xrTableCell367
            // 
            this.xrTableCell367.Name = "xrTableCell367";
            this.xrTableCell367.Text = "Thuế suất";
            this.xrTableCell367.Weight = 4.70149094729393;
            // 
            // xrTableCell368
            // 
            this.xrTableCell368.Name = "xrTableCell368";
            this.xrTableCell368.Weight = 0.80997002266531348;
            // 
            // lblThueSuatThueVaThuKhac3
            // 
            this.lblThueSuatThueVaThuKhac3.Name = "lblThueSuatThueVaThuKhac3";
            this.lblThueSuatThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatThueVaThuKhac3.StylePriority.UsePadding = false;
            this.lblThueSuatThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblThueSuatThueVaThuKhac3.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThueSuatThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblThueSuatThueVaThuKhac3.Weight = 13.195389748136442;
            // 
            // xrTableCell370
            // 
            this.xrTableCell370.Name = "xrTableCell370";
            this.xrTableCell370.Weight = 1.356459922239643;
            // 
            // xrTableCell371
            // 
            this.xrTableCell371.Name = "xrTableCell371";
            this.xrTableCell371.Weight = 1.9432525410815345;
            // 
            // xrTableCell372
            // 
            this.xrTableCell372.Name = "xrTableCell372";
            this.xrTableCell372.Weight = 0.87868419151386323;
            // 
            // xrTableCell373
            // 
            this.xrTableCell373.Name = "xrTableCell373";
            this.xrTableCell373.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell373.StylePriority.UsePadding = false;
            this.xrTableCell373.Text = "Miễn / Giảm / Không chịu thuế và thu khác";
            this.xrTableCell373.Weight = 25.126900370463765;
            // 
            // xrTable17
            // 
            this.xrTable17.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable17.LocationFloat = new DevExpress.Utils.PointFloat(12.50003F, 460.0001F);
            this.xrTable17.Name = "xrTable17";
            this.xrTable17.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow51,
            this.xrTableRow52,
            this.xrTableRow53});
            this.xrTable17.SizeF = new System.Drawing.SizeF(796F, 33F);
            this.xrTable17.StylePriority.UseFont = false;
            // 
            // xrTableRow51
            // 
            this.xrTableRow51.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow51.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell406,
            this.xrTableCell407,
            this.xrTableCell408,
            this.xrTableCell409,
            this.xrTableCell410,
            this.lblTenKhoanMucThueVaThuKhac5,
            this.xrTableCell412,
            this.xrTableCell413,
            this.xrTableCell414,
            this.xrTableCell415,
            this.xrTableCell416,
            this.lblMaApDungThueSuatThueVaThuKhac5});
            this.xrTableRow51.Name = "xrTableRow51";
            this.xrTableRow51.StylePriority.UseBorders = false;
            this.xrTableRow51.Weight = 0.31058809590593411;
            // 
            // xrTableCell406
            // 
            this.xrTableCell406.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell406.Name = "xrTableCell406";
            this.xrTableCell406.StylePriority.UseBorders = false;
            this.xrTableCell406.Weight = 1.1472320241718166;
            // 
            // xrTableCell407
            // 
            this.xrTableCell407.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell407.Name = "xrTableCell407";
            this.xrTableCell407.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell407.StylePriority.UseBorders = false;
            this.xrTableCell407.StylePriority.UsePadding = false;
            this.xrTableCell407.Text = "5";
            this.xrTableCell407.Weight = 0.93641266167101411;
            // 
            // xrTableCell408
            // 
            this.xrTableCell408.Name = "xrTableCell408";
            this.xrTableCell408.Weight = 0.63735056931265555;
            // 
            // xrTableCell409
            // 
            this.xrTableCell409.Name = "xrTableCell409";
            this.xrTableCell409.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell409.StylePriority.UsePadding = false;
            this.xrTableCell409.Text = "Tên";
            this.xrTableCell409.Weight = 4.70149094729393;
            // 
            // xrTableCell410
            // 
            this.xrTableCell410.Name = "xrTableCell410";
            this.xrTableCell410.Weight = 0.80996710510377756;
            // 
            // lblTenKhoanMucThueVaThuKhac5
            // 
            this.lblTenKhoanMucThueVaThuKhac5.Name = "lblTenKhoanMucThueVaThuKhac5";
            this.lblTenKhoanMucThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblTenKhoanMucThueVaThuKhac5.StylePriority.UsePadding = false;
            this.lblTenKhoanMucThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblTenKhoanMucThueVaThuKhac5.Text = "WWWWWWWWE";
            this.lblTenKhoanMucThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTenKhoanMucThueVaThuKhac5.Weight = 13.195392665697977;
            // 
            // xrTableCell412
            // 
            this.xrTableCell412.Name = "xrTableCell412";
            this.xrTableCell412.Weight = 1.356459922239643;
            // 
            // xrTableCell413
            // 
            this.xrTableCell413.Name = "xrTableCell413";
            this.xrTableCell413.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell413.StylePriority.UsePadding = false;
            this.xrTableCell413.Weight = 1.9432515685610223;
            // 
            // xrTableCell414
            // 
            this.xrTableCell414.Name = "xrTableCell414";
            this.xrTableCell414.Weight = 0.87868394838373565;
            // 
            // xrTableCell415
            // 
            this.xrTableCell415.Name = "xrTableCell415";
            this.xrTableCell415.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell415.StylePriority.UsePadding = false;
            this.xrTableCell415.Text = "Mã áp dụng thuế suất";
            this.xrTableCell415.Weight = 7.5658886488162924;
            // 
            // xrTableCell416
            // 
            this.xrTableCell416.Name = "xrTableCell416";
            this.xrTableCell416.Weight = 0.72213635125205988;
            // 
            // lblMaApDungThueSuatThueVaThuKhac5
            // 
            this.lblMaApDungThueSuatThueVaThuKhac5.Name = "lblMaApDungThueSuatThueVaThuKhac5";
            this.lblMaApDungThueSuatThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaApDungThueSuatThueVaThuKhac5.StylePriority.UsePadding = false;
            this.lblMaApDungThueSuatThueVaThuKhac5.Text = "XXXXXXXXXE";
            this.lblMaApDungThueSuatThueVaThuKhac5.Weight = 16.838876586046052;
            // 
            // xrTableRow52
            // 
            this.xrTableRow52.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell418,
            this.xrTableCell419,
            this.xrTableCell420,
            this.lblTriGiaTinhThueVaThuKhac5,
            this.xrTableCell422,
            this.xrTableCell423,
            this.xrTableCell424,
            this.xrTableCell425,
            this.xrTableCell426,
            this.lblSoLuongTinhThueVaThuKhac5,
            this.xrTableCell428,
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5});
            this.xrTableRow52.Name = "xrTableRow52";
            this.xrTableRow52.Weight = 0.31058809590593411;
            // 
            // xrTableCell418
            // 
            this.xrTableCell418.Name = "xrTableCell418";
            this.xrTableCell418.Weight = 2.7209952551554863;
            // 
            // xrTableCell419
            // 
            this.xrTableCell419.Name = "xrTableCell419";
            this.xrTableCell419.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell419.StylePriority.UsePadding = false;
            this.xrTableCell419.Text = "Trị giá tính thuế";
            this.xrTableCell419.Weight = 5.5114600045137152;
            // 
            // xrTableCell420
            // 
            this.xrTableCell420.Name = "xrTableCell420";
            this.xrTableCell420.Weight = 1.5137058017649405;
            // 
            // lblTriGiaTinhThueVaThuKhac5
            // 
            this.lblTriGiaTinhThueVaThuKhac5.Name = "lblTriGiaTinhThueVaThuKhac5";
            this.lblTriGiaTinhThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueVaThuKhac5.StylePriority.UsePadding = false;
            this.lblTriGiaTinhThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueVaThuKhac5.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueVaThuKhac5.Weight = 11.68168491181703;
            // 
            // xrTableCell422
            // 
            this.xrTableCell422.Name = "xrTableCell422";
            this.xrTableCell422.Weight = 1.356459922239643;
            // 
            // xrTableCell423
            // 
            this.xrTableCell423.Name = "xrTableCell423";
            this.xrTableCell423.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell423.StylePriority.UsePadding = false;
            this.xrTableCell423.Text = "VND";
            this.xrTableCell423.Weight = 1.9432525410815345;
            // 
            // xrTableCell424
            // 
            this.xrTableCell424.Name = "xrTableCell424";
            this.xrTableCell424.Weight = 0.87868419151386323;
            // 
            // xrTableCell425
            // 
            this.xrTableCell425.Name = "xrTableCell425";
            this.xrTableCell425.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell425.StylePriority.UsePadding = false;
            this.xrTableCell425.Text = "Số lượng tính thuế";
            this.xrTableCell425.Weight = 6.7718583518315167;
            // 
            // xrTableCell426
            // 
            this.xrTableCell426.Name = "xrTableCell426";
            this.xrTableCell426.Weight = 1.5161620287644064;
            // 
            // lblSoLuongTinhThueVaThuKhac5
            // 
            this.lblSoLuongTinhThueVaThuKhac5.Name = "lblSoLuongTinhThueVaThuKhac5";
            this.lblSoLuongTinhThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoLuongTinhThueVaThuKhac5.StylePriority.UsePadding = false;
            this.lblSoLuongTinhThueVaThuKhac5.Text = "123.456.789.012";
            this.lblSoLuongTinhThueVaThuKhac5.Weight = 5.9363662209731869;
            // 
            // xrTableCell428
            // 
            this.xrTableCell428.Name = "xrTableCell428";
            this.xrTableCell428.Weight = 0.92680037542519877;
            // 
            // lblMaDonViTinhChuanDanhThueVaThuKhac5
            // 
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.Name = "lblMaDonViTinhChuanDanhThueVaThuKhac5";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac5.Weight = 9.9757133934694551;
            // 
            // xrTableRow53
            // 
            this.xrTableRow53.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell430,
            this.xrTableCell431,
            this.xrTableCell432,
            this.lblThueSuatThueVaThuKhac5,
            this.xrTableCell434,
            this.xrTableCell435,
            this.xrTableCell436,
            this.xrTableCell437});
            this.xrTableRow53.Name = "xrTableRow53";
            this.xrTableRow53.Weight = 0.31058809590593411;
            // 
            // xrTableCell430
            // 
            this.xrTableCell430.Name = "xrTableCell430";
            this.xrTableCell430.Weight = 2.7209952551554863;
            // 
            // xrTableCell431
            // 
            this.xrTableCell431.Name = "xrTableCell431";
            this.xrTableCell431.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell431.StylePriority.UsePadding = false;
            this.xrTableCell431.Text = "Thuế suất";
            this.xrTableCell431.Weight = 4.70149094729393;
            // 
            // xrTableCell432
            // 
            this.xrTableCell432.Name = "xrTableCell432";
            this.xrTableCell432.Weight = 0.80997002266531348;
            // 
            // lblThueSuatThueVaThuKhac5
            // 
            this.lblThueSuatThueVaThuKhac5.Name = "lblThueSuatThueVaThuKhac5";
            this.lblThueSuatThueVaThuKhac5.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatThueVaThuKhac5.StylePriority.UsePadding = false;
            this.lblThueSuatThueVaThuKhac5.StylePriority.UseTextAlignment = false;
            this.lblThueSuatThueVaThuKhac5.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThueSuatThueVaThuKhac5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblThueSuatThueVaThuKhac5.Weight = 13.195389748136442;
            // 
            // xrTableCell434
            // 
            this.xrTableCell434.Name = "xrTableCell434";
            this.xrTableCell434.Weight = 1.356459922239643;
            // 
            // xrTableCell435
            // 
            this.xrTableCell435.Name = "xrTableCell435";
            this.xrTableCell435.Weight = 1.9432525410815345;
            // 
            // xrTableCell436
            // 
            this.xrTableCell436.Name = "xrTableCell436";
            this.xrTableCell436.Weight = 0.87868419151386323;
            // 
            // xrTableCell437
            // 
            this.xrTableCell437.Name = "xrTableCell437";
            this.xrTableCell437.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell437.StylePriority.UsePadding = false;
            this.xrTableCell437.Text = "Miễn / Giảm / Không chịu thuế và thu khác";
            this.xrTableCell437.Weight = 25.126900370463765;
            // 
            // xrTable16
            // 
            this.xrTable16.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable16.LocationFloat = new DevExpress.Utils.PointFloat(12.50003F, 399.0001F);
            this.xrTable16.Name = "xrTable16";
            this.xrTable16.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow48,
            this.xrTableRow49,
            this.xrTableRow50});
            this.xrTable16.SizeF = new System.Drawing.SizeF(796F, 33F);
            this.xrTable16.StylePriority.UseBorders = false;
            this.xrTable16.StylePriority.UseFont = false;
            // 
            // xrTableRow48
            // 
            this.xrTableRow48.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow48.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell374,
            this.xrTableCell375,
            this.xrTableCell376,
            this.xrTableCell377,
            this.xrTableCell378,
            this.lblTenKhoanMucThueVaThuKhac4,
            this.xrTableCell380,
            this.xrTableCell381,
            this.xrTableCell382,
            this.xrTableCell383,
            this.xrTableCell384,
            this.lblMaApDungThueSuatThueVaThuKhac4});
            this.xrTableRow48.Name = "xrTableRow48";
            this.xrTableRow48.StylePriority.UseBorders = false;
            this.xrTableRow48.Weight = 0.31058809590593411;
            // 
            // xrTableCell374
            // 
            this.xrTableCell374.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell374.Name = "xrTableCell374";
            this.xrTableCell374.StylePriority.UseBorders = false;
            this.xrTableCell374.Weight = 1.1472320241718166;
            // 
            // xrTableCell375
            // 
            this.xrTableCell375.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell375.Name = "xrTableCell375";
            this.xrTableCell375.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell375.StylePriority.UseBorders = false;
            this.xrTableCell375.StylePriority.UsePadding = false;
            this.xrTableCell375.Text = "4";
            this.xrTableCell375.Weight = 0.93641266167101411;
            // 
            // xrTableCell376
            // 
            this.xrTableCell376.Name = "xrTableCell376";
            this.xrTableCell376.Weight = 0.63735056931265555;
            // 
            // xrTableCell377
            // 
            this.xrTableCell377.Name = "xrTableCell377";
            this.xrTableCell377.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell377.StylePriority.UsePadding = false;
            this.xrTableCell377.Text = "Tên";
            this.xrTableCell377.Weight = 4.70149094729393;
            // 
            // xrTableCell378
            // 
            this.xrTableCell378.Name = "xrTableCell378";
            this.xrTableCell378.Weight = 0.80996710510377756;
            // 
            // lblTenKhoanMucThueVaThuKhac4
            // 
            this.lblTenKhoanMucThueVaThuKhac4.Name = "lblTenKhoanMucThueVaThuKhac4";
            this.lblTenKhoanMucThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblTenKhoanMucThueVaThuKhac4.StylePriority.UsePadding = false;
            this.lblTenKhoanMucThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblTenKhoanMucThueVaThuKhac4.Text = "WWWWWWWWE";
            this.lblTenKhoanMucThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.lblTenKhoanMucThueVaThuKhac4.Weight = 13.195392665697977;
            // 
            // xrTableCell380
            // 
            this.xrTableCell380.Name = "xrTableCell380";
            this.xrTableCell380.Weight = 1.356459922239643;
            // 
            // xrTableCell381
            // 
            this.xrTableCell381.Name = "xrTableCell381";
            this.xrTableCell381.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell381.StylePriority.UsePadding = false;
            this.xrTableCell381.Weight = 1.9432515685610223;
            // 
            // xrTableCell382
            // 
            this.xrTableCell382.Name = "xrTableCell382";
            this.xrTableCell382.Weight = 0.87868394838373565;
            // 
            // xrTableCell383
            // 
            this.xrTableCell383.Name = "xrTableCell383";
            this.xrTableCell383.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell383.StylePriority.UsePadding = false;
            this.xrTableCell383.Text = "Mã áp dụng thuế suất";
            this.xrTableCell383.Weight = 7.5658857312547569;
            // 
            // xrTableCell384
            // 
            this.xrTableCell384.Name = "xrTableCell384";
            this.xrTableCell384.Weight = 0.72213926881359558;
            // 
            // lblMaApDungThueSuatThueVaThuKhac4
            // 
            this.lblMaApDungThueSuatThueVaThuKhac4.Name = "lblMaApDungThueSuatThueVaThuKhac4";
            this.lblMaApDungThueSuatThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaApDungThueSuatThueVaThuKhac4.StylePriority.UsePadding = false;
            this.lblMaApDungThueSuatThueVaThuKhac4.Text = "XXXXXXXXXE";
            this.lblMaApDungThueSuatThueVaThuKhac4.Weight = 16.838876586046052;
            // 
            // xrTableRow49
            // 
            this.xrTableRow49.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell386,
            this.xrTableCell387,
            this.xrTableCell388,
            this.lblTriGiaTinhThueVaThuKhac4,
            this.xrTableCell390,
            this.xrTableCell391,
            this.xrTableCell392,
            this.xrTableCell393,
            this.xrTableCell394,
            this.lblSoLuongTinhThueVaThuKhac4,
            this.xrTableCell396,
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4});
            this.xrTableRow49.Name = "xrTableRow49";
            this.xrTableRow49.Weight = 0.31058809590593411;
            // 
            // xrTableCell386
            // 
            this.xrTableCell386.Name = "xrTableCell386";
            this.xrTableCell386.Weight = 2.7209952551554863;
            // 
            // xrTableCell387
            // 
            this.xrTableCell387.Name = "xrTableCell387";
            this.xrTableCell387.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell387.StylePriority.UsePadding = false;
            this.xrTableCell387.Text = "Trị giá tính thuế";
            this.xrTableCell387.Weight = 5.5114600045137152;
            // 
            // xrTableCell388
            // 
            this.xrTableCell388.Name = "xrTableCell388";
            this.xrTableCell388.Weight = 1.5137058017649405;
            // 
            // lblTriGiaTinhThueVaThuKhac4
            // 
            this.lblTriGiaTinhThueVaThuKhac4.Name = "lblTriGiaTinhThueVaThuKhac4";
            this.lblTriGiaTinhThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueVaThuKhac4.StylePriority.UsePadding = false;
            this.lblTriGiaTinhThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueVaThuKhac4.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueVaThuKhac4.Weight = 11.68168491181703;
            // 
            // xrTableCell390
            // 
            this.xrTableCell390.Name = "xrTableCell390";
            this.xrTableCell390.Weight = 1.356459922239643;
            // 
            // xrTableCell391
            // 
            this.xrTableCell391.Name = "xrTableCell391";
            this.xrTableCell391.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell391.StylePriority.UsePadding = false;
            this.xrTableCell391.Text = "VND";
            this.xrTableCell391.Weight = 1.9432525410815345;
            // 
            // xrTableCell392
            // 
            this.xrTableCell392.Name = "xrTableCell392";
            this.xrTableCell392.Weight = 0.87868419151386323;
            // 
            // xrTableCell393
            // 
            this.xrTableCell393.Name = "xrTableCell393";
            this.xrTableCell393.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell393.StylePriority.UsePadding = false;
            this.xrTableCell393.Text = "Số lượng tính thuế";
            this.xrTableCell393.Weight = 6.7718583518315167;
            // 
            // xrTableCell394
            // 
            this.xrTableCell394.Name = "xrTableCell394";
            this.xrTableCell394.Weight = 1.5161620287644064;
            // 
            // lblSoLuongTinhThueVaThuKhac4
            // 
            this.lblSoLuongTinhThueVaThuKhac4.Name = "lblSoLuongTinhThueVaThuKhac4";
            this.lblSoLuongTinhThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblSoLuongTinhThueVaThuKhac4.StylePriority.UsePadding = false;
            this.lblSoLuongTinhThueVaThuKhac4.Text = "123.456.789.012";
            this.lblSoLuongTinhThueVaThuKhac4.Weight = 5.9363701110552345;
            // 
            // xrTableCell396
            // 
            this.xrTableCell396.Name = "xrTableCell396";
            this.xrTableCell396.Weight = 0.92679648534315107;
            // 
            // lblMaDonViTinhChuanDanhThueVaThuKhac4
            // 
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.Name = "lblMaDonViTinhChuanDanhThueVaThuKhac4";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac4.Weight = 9.9757133934694551;
            // 
            // xrTableRow50
            // 
            this.xrTableRow50.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell398,
            this.xrTableCell399,
            this.xrTableCell400,
            this.lblThueSuatThueVaThuKhac4,
            this.xrTableCell402,
            this.xrTableCell403,
            this.xrTableCell404,
            this.xrTableCell405});
            this.xrTableRow50.Name = "xrTableRow50";
            this.xrTableRow50.Weight = 0.31058809590593411;
            // 
            // xrTableCell398
            // 
            this.xrTableCell398.Name = "xrTableCell398";
            this.xrTableCell398.Weight = 2.7209952551554863;
            // 
            // xrTableCell399
            // 
            this.xrTableCell399.Name = "xrTableCell399";
            this.xrTableCell399.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell399.StylePriority.UsePadding = false;
            this.xrTableCell399.Text = "Thuế suất";
            this.xrTableCell399.Weight = 4.70149094729393;
            // 
            // xrTableCell400
            // 
            this.xrTableCell400.Name = "xrTableCell400";
            this.xrTableCell400.Weight = 0.80997002266531348;
            // 
            // lblThueSuatThueVaThuKhac4
            // 
            this.lblThueSuatThueVaThuKhac4.Name = "lblThueSuatThueVaThuKhac4";
            this.lblThueSuatThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatThueVaThuKhac4.StylePriority.UsePadding = false;
            this.lblThueSuatThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblThueSuatThueVaThuKhac4.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThueSuatThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblThueSuatThueVaThuKhac4.Weight = 13.195389748136442;
            // 
            // xrTableCell402
            // 
            this.xrTableCell402.Name = "xrTableCell402";
            this.xrTableCell402.Weight = 1.356459922239643;
            // 
            // xrTableCell403
            // 
            this.xrTableCell403.Name = "xrTableCell403";
            this.xrTableCell403.Weight = 1.9432525410815345;
            // 
            // xrTableCell404
            // 
            this.xrTableCell404.Name = "xrTableCell404";
            this.xrTableCell404.Weight = 0.87868419151386323;
            // 
            // xrTableCell405
            // 
            this.xrTableCell405.Name = "xrTableCell405";
            this.xrTableCell405.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell405.StylePriority.UsePadding = false;
            this.xrTableCell405.Text = "Miễn / Giảm / Không chịu thuế và thu khác";
            this.xrTableCell405.Weight = 25.126900370463765;
            // 
            // xrTable11
            // 
            this.xrTable11.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable11.LocationFloat = new DevExpress.Utils.PointFloat(400.4738F, 371.0001F);
            this.xrTable11.Name = "xrTable11";
            this.xrTable11.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow39});
            this.xrTable11.SizeF = new System.Drawing.SizeF(408.0262F, 28F);
            this.xrTable11.StylePriority.UseFont = false;
            // 
            // xrTableRow39
            // 
            this.xrTableRow39.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell298,
            this.lblMaMienGiamThueVaThuKhac3,
            this.xrTableCell300,
            this.lblDieuKhoanMienGiamThueVaThuKhac3});
            this.xrTableRow39.Name = "xrTableRow39";
            this.xrTableRow39.Weight = 0.39529476275110736;
            // 
            // xrTableCell298
            // 
            this.xrTableCell298.Name = "xrTableCell298";
            this.xrTableCell298.Weight = 2.7209952551554863;
            // 
            // lblMaMienGiamThueVaThuKhac3
            // 
            this.lblMaMienGiamThueVaThuKhac3.Name = "lblMaMienGiamThueVaThuKhac3";
            this.lblMaMienGiamThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaMienGiamThueVaThuKhac3.StylePriority.UsePadding = false;
            this.lblMaMienGiamThueVaThuKhac3.Text = "XXXXE";
            this.lblMaMienGiamThueVaThuKhac3.Weight = 4.5535672620046661;
            // 
            // xrTableCell300
            // 
            this.xrTableCell300.Name = "xrTableCell300";
            this.xrTableCell300.Weight = 1.4416521596143241;
            // 
            // lblDieuKhoanMienGiamThueVaThuKhac3
            // 
            this.lblDieuKhoanMienGiamThueVaThuKhac3.Multiline = true;
            this.lblDieuKhoanMienGiamThueVaThuKhac3.Name = "lblDieuKhoanMienGiamThueVaThuKhac3";
            this.lblDieuKhoanMienGiamThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblDieuKhoanMienGiamThueVaThuKhac3.StylePriority.UsePadding = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac3.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiamThueVaThuKhac3.Weight = 16.011344714770793;
            // 
            // xrTable7
            // 
            this.xrTable7.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable7.LocationFloat = new DevExpress.Utils.PointFloat(12.50003F, 371.0001F);
            this.xrTable7.Name = "xrTable7";
            this.xrTable7.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow31,
            this.xrTableRow32});
            this.xrTable7.SizeF = new System.Drawing.SizeF(387.9739F, 28.00003F);
            this.xrTable7.StylePriority.UseFont = false;
            // 
            // xrTableRow31
            // 
            this.xrTableRow31.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell246,
            this.xrTableCell247,
            this.xrTableCell248,
            this.lblSoTienThueVaThuKhac3,
            this.xrTableCell250,
            this.xrTableCell252});
            this.xrTableRow31.Name = "xrTableRow31";
            this.xrTableRow31.Weight = 0.31058809590593411;
            // 
            // xrTableCell246
            // 
            this.xrTableCell246.Name = "xrTableCell246";
            this.xrTableCell246.Weight = 2.7209952551554863;
            // 
            // xrTableCell247
            // 
            this.xrTableCell247.Name = "xrTableCell247";
            this.xrTableCell247.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell247.StylePriority.UsePadding = false;
            this.xrTableCell247.Text = "Số tiền thuế";
            this.xrTableCell247.Weight = 4.70149094729393;
            // 
            // xrTableCell248
            // 
            this.xrTableCell248.Name = "xrTableCell248";
            this.xrTableCell248.Weight = 0.80996710510377756;
            // 
            // lblSoTienThueVaThuKhac3
            // 
            this.lblSoTienThueVaThuKhac3.Name = "lblSoTienThueVaThuKhac3";
            this.lblSoTienThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienThueVaThuKhac3.StylePriority.UsePadding = false;
            this.lblSoTienThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueVaThuKhac3.Text = "1.234.567.890.123.456";
            this.lblSoTienThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueVaThuKhac3.Weight = 13.195392665697977;
            // 
            // xrTableCell250
            // 
            this.xrTableCell250.Name = "xrTableCell250";
            this.xrTableCell250.Weight = 1.356459922239643;
            // 
            // xrTableCell252
            // 
            this.xrTableCell252.Name = "xrTableCell252";
            this.xrTableCell252.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell252.StylePriority.UsePadding = false;
            this.xrTableCell252.Text = "VND";
            this.xrTableCell252.Weight = 1.943253496054453;
            // 
            // xrTableRow32
            // 
            this.xrTableRow32.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell264,
            this.xrTableCell265,
            this.xrTableCell266,
            this.lblSoTienGiamThueVaThuKhac3,
            this.xrTableCell268,
            this.xrTableCell269});
            this.xrTableRow32.Name = "xrTableRow32";
            this.xrTableRow32.Weight = 0.48000064625430561;
            // 
            // xrTableCell264
            // 
            this.xrTableCell264.Name = "xrTableCell264";
            this.xrTableCell264.Weight = 2.7209952551554863;
            // 
            // xrTableCell265
            // 
            this.xrTableCell265.Name = "xrTableCell265";
            this.xrTableCell265.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell265.StylePriority.UsePadding = false;
            this.xrTableCell265.Text = "Số tiền miễn giảm";
            this.xrTableCell265.Weight = 6.1488070856949344;
            // 
            // xrTableCell266
            // 
            this.xrTableCell266.Name = "xrTableCell266";
            this.xrTableCell266.Weight = 0.87635872058372155;
            // 
            // lblSoTienGiamThueVaThuKhac3
            // 
            this.lblSoTienGiamThueVaThuKhac3.Name = "lblSoTienGiamThueVaThuKhac3";
            this.lblSoTienGiamThueVaThuKhac3.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienGiamThueVaThuKhac3.StylePriority.UsePadding = false;
            this.lblSoTienGiamThueVaThuKhac3.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueVaThuKhac3.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueVaThuKhac3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueVaThuKhac3.Weight = 11.68168491181703;
            // 
            // xrTableCell268
            // 
            this.xrTableCell268.Name = "xrTableCell268";
            this.xrTableCell268.Weight = 1.356459922239643;
            // 
            // xrTableCell269
            // 
            this.xrTableCell269.Name = "xrTableCell269";
            this.xrTableCell269.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell269.StylePriority.UsePadding = false;
            this.xrTableCell269.Text = "VND";
            this.xrTableCell269.Weight = 1.943253496054453;
            // 
            // xrTable4
            // 
            this.xrTable4.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable4.LocationFloat = new DevExpress.Utils.PointFloat(400.4738F, 249F);
            this.xrTable4.Name = "xrTable4";
            this.xrTable4.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow26});
            this.xrTable4.SizeF = new System.Drawing.SizeF(407.5525F, 28.00002F);
            this.xrTable4.StylePriority.UseFont = false;
            // 
            // xrTableRow26
            // 
            this.xrTableRow26.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell228,
            this.lblMaMienGiamThueVaThuKhac1,
            this.xrTableCell232,
            this.lblDieuKhoanMienGiamThueVaThuKhac1});
            this.xrTableRow26.Name = "xrTableRow26";
            this.xrTableRow26.Weight = 0.39529476275110736;
            // 
            // xrTableCell228
            // 
            this.xrTableCell228.Name = "xrTableCell228";
            this.xrTableCell228.Weight = 2.7209952551554863;
            // 
            // lblMaMienGiamThueVaThuKhac1
            // 
            this.lblMaMienGiamThueVaThuKhac1.Name = "lblMaMienGiamThueVaThuKhac1";
            this.lblMaMienGiamThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaMienGiamThueVaThuKhac1.StylePriority.UsePadding = false;
            this.lblMaMienGiamThueVaThuKhac1.Text = "XXXXE";
            this.lblMaMienGiamThueVaThuKhac1.Weight = 4.5535673308637437;
            // 
            // xrTableCell232
            // 
            this.xrTableCell232.Name = "xrTableCell232";
            this.xrTableCell232.Weight = 1.4416484115564592;
            // 
            // lblDieuKhoanMienGiamThueVaThuKhac1
            // 
            this.lblDieuKhoanMienGiamThueVaThuKhac1.Multiline = true;
            this.lblDieuKhoanMienGiamThueVaThuKhac1.Name = "lblDieuKhoanMienGiamThueVaThuKhac1";
            this.lblDieuKhoanMienGiamThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblDieuKhoanMienGiamThueVaThuKhac1.StylePriority.UsePadding = false;
            this.lblDieuKhoanMienGiamThueVaThuKhac1.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiamThueVaThuKhac1.Weight = 16.011348393969577;
            // 
            // xrTable8
            // 
            this.xrTable8.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable8.LocationFloat = new DevExpress.Utils.PointFloat(12.50003F, 493.0001F);
            this.xrTable8.Name = "xrTable8";
            this.xrTable8.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow33,
            this.xrTableRow34});
            this.xrTable8.SizeF = new System.Drawing.SizeF(387.9739F, 28.00003F);
            this.xrTable8.StylePriority.UseFont = false;
            // 
            // xrTableRow33
            // 
            this.xrTableRow33.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell270,
            this.xrTableCell271,
            this.xrTableCell272,
            this.lblSoTienThueVaThuKhac4,
            this.xrTableCell274,
            this.xrTableCell275});
            this.xrTableRow33.Name = "xrTableRow33";
            this.xrTableRow33.Weight = 0.31058809590593411;
            // 
            // xrTableCell270
            // 
            this.xrTableCell270.Name = "xrTableCell270";
            this.xrTableCell270.Weight = 2.7209952551554863;
            // 
            // xrTableCell271
            // 
            this.xrTableCell271.Name = "xrTableCell271";
            this.xrTableCell271.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell271.StylePriority.UsePadding = false;
            this.xrTableCell271.Text = "Số tiền thuế";
            this.xrTableCell271.Weight = 4.70149094729393;
            // 
            // xrTableCell272
            // 
            this.xrTableCell272.Name = "xrTableCell272";
            this.xrTableCell272.Weight = 0.80996710510377756;
            // 
            // lblSoTienThueVaThuKhac4
            // 
            this.lblSoTienThueVaThuKhac4.Name = "lblSoTienThueVaThuKhac4";
            this.lblSoTienThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienThueVaThuKhac4.StylePriority.UsePadding = false;
            this.lblSoTienThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueVaThuKhac4.Text = "1.234.567.890.123.456";
            this.lblSoTienThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueVaThuKhac4.Weight = 13.195392665697977;
            // 
            // xrTableCell274
            // 
            this.xrTableCell274.Name = "xrTableCell274";
            this.xrTableCell274.Weight = 1.356459922239643;
            // 
            // xrTableCell275
            // 
            this.xrTableCell275.Name = "xrTableCell275";
            this.xrTableCell275.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell275.StylePriority.UsePadding = false;
            this.xrTableCell275.Text = "VND";
            this.xrTableCell275.Weight = 1.943253496054453;
            // 
            // xrTableRow34
            // 
            this.xrTableRow34.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell276,
            this.xrTableCell277,
            this.xrTableCell278,
            this.lblSoTienGiamThueVaThuKhac4,
            this.xrTableCell280,
            this.xrTableCell281});
            this.xrTableRow34.Name = "xrTableRow34";
            this.xrTableRow34.Weight = 0.48000064625430561;
            // 
            // xrTableCell276
            // 
            this.xrTableCell276.Name = "xrTableCell276";
            this.xrTableCell276.Weight = 2.7209952551554863;
            // 
            // xrTableCell277
            // 
            this.xrTableCell277.Name = "xrTableCell277";
            this.xrTableCell277.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell277.StylePriority.UsePadding = false;
            this.xrTableCell277.Text = "Số tiền miễn giảm";
            this.xrTableCell277.Weight = 6.1488070856949344;
            // 
            // xrTableCell278
            // 
            this.xrTableCell278.Name = "xrTableCell278";
            this.xrTableCell278.Weight = 0.87635872058372155;
            // 
            // lblSoTienGiamThueVaThuKhac4
            // 
            this.lblSoTienGiamThueVaThuKhac4.Name = "lblSoTienGiamThueVaThuKhac4";
            this.lblSoTienGiamThueVaThuKhac4.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienGiamThueVaThuKhac4.StylePriority.UsePadding = false;
            this.lblSoTienGiamThueVaThuKhac4.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueVaThuKhac4.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueVaThuKhac4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueVaThuKhac4.Weight = 11.68168491181703;
            // 
            // xrTableCell280
            // 
            this.xrTableCell280.Name = "xrTableCell280";
            this.xrTableCell280.Weight = 1.356459922239643;
            // 
            // xrTableCell281
            // 
            this.xrTableCell281.Name = "xrTableCell281";
            this.xrTableCell281.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell281.StylePriority.UsePadding = false;
            this.xrTableCell281.Text = "VND";
            this.xrTableCell281.Weight = 1.943253496054453;
            // 
            // xrTable3
            // 
            this.xrTable3.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable3.LocationFloat = new DevExpress.Utils.PointFloat(12.50003F, 249F);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow28,
            this.xrTableRow29});
            this.xrTable3.SizeF = new System.Drawing.SizeF(387.9739F, 28.00003F);
            this.xrTable3.StylePriority.UseFont = false;
            // 
            // xrTableRow28
            // 
            this.xrTableRow28.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell255,
            this.xrTableCell256,
            this.xrTableCell253,
            this.lblSoTienThueVaThuKhac1,
            this.xrTableCell257,
            this.xrTableCell254});
            this.xrTableRow28.Name = "xrTableRow28";
            this.xrTableRow28.Weight = 0.31058809590593411;
            // 
            // xrTableCell255
            // 
            this.xrTableCell255.Name = "xrTableCell255";
            this.xrTableCell255.Weight = 2.7209952551554863;
            // 
            // xrTableCell256
            // 
            this.xrTableCell256.Name = "xrTableCell256";
            this.xrTableCell256.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell256.StylePriority.UsePadding = false;
            this.xrTableCell256.Text = "Số tiền thuế";
            this.xrTableCell256.Weight = 4.70149094729393;
            // 
            // xrTableCell253
            // 
            this.xrTableCell253.Name = "xrTableCell253";
            this.xrTableCell253.Weight = 0.80996710510377756;
            // 
            // lblSoTienThueVaThuKhac1
            // 
            this.lblSoTienThueVaThuKhac1.Name = "lblSoTienThueVaThuKhac1";
            this.lblSoTienThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienThueVaThuKhac1.StylePriority.UsePadding = false;
            this.lblSoTienThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueVaThuKhac1.Text = "1.234.567.890.123.456";
            this.lblSoTienThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueVaThuKhac1.Weight = 13.195392665697977;
            // 
            // xrTableCell257
            // 
            this.xrTableCell257.Name = "xrTableCell257";
            this.xrTableCell257.Weight = 1.356459922239643;
            // 
            // xrTableCell254
            // 
            this.xrTableCell254.Name = "xrTableCell254";
            this.xrTableCell254.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell254.StylePriority.UsePadding = false;
            this.xrTableCell254.Text = "VND";
            this.xrTableCell254.Weight = 1.943253496054453;
            // 
            // xrTableRow29
            // 
            this.xrTableRow29.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell258,
            this.xrTableCell259,
            this.xrTableCell260,
            this.lblSoTienGiamThueVaThuKhac1,
            this.xrTableCell262,
            this.xrTableCell263});
            this.xrTableRow29.Name = "xrTableRow29";
            this.xrTableRow29.Weight = 0.48000064625430561;
            // 
            // xrTableCell258
            // 
            this.xrTableCell258.Name = "xrTableCell258";
            this.xrTableCell258.Weight = 2.7209952551554863;
            // 
            // xrTableCell259
            // 
            this.xrTableCell259.Name = "xrTableCell259";
            this.xrTableCell259.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell259.StylePriority.UsePadding = false;
            this.xrTableCell259.Text = "Số tiền miễn giảm";
            this.xrTableCell259.Weight = 6.1488080582155229;
            // 
            // xrTableCell260
            // 
            this.xrTableCell260.Name = "xrTableCell260";
            this.xrTableCell260.Weight = 0.87635774806313316;
            // 
            // lblSoTienGiamThueVaThuKhac1
            // 
            this.lblSoTienGiamThueVaThuKhac1.Name = "lblSoTienGiamThueVaThuKhac1";
            this.lblSoTienGiamThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienGiamThueVaThuKhac1.StylePriority.UsePadding = false;
            this.lblSoTienGiamThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueVaThuKhac1.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueVaThuKhac1.Weight = 11.68168491181703;
            // 
            // xrTableCell262
            // 
            this.xrTableCell262.Name = "xrTableCell262";
            this.xrTableCell262.Weight = 1.356459922239643;
            // 
            // xrTableCell263
            // 
            this.xrTableCell263.Name = "xrTableCell263";
            this.xrTableCell263.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTableCell263.StylePriority.UsePadding = false;
            this.xrTableCell263.Text = "VND";
            this.xrTableCell263.Weight = 1.943253496054453;
            // 
            // xrTable2
            // 
            this.xrTable2.Font = new System.Drawing.Font("Arial", 8F);
            this.xrTable2.LocationFloat = new DevExpress.Utils.PointFloat(12.5F, 0F);
            this.xrTable2.Name = "xrTable2";
            this.xrTable2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.xrTable2.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow9,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow10,
            this.xrTableRow11,
            this.xrTableRow12,
            this.xrTableRow13,
            this.xrTableRow14,
            this.xrTableRow17,
            this.xrTableRow16,
            this.xrTableRow15,
            this.xrTableRow18,
            this.xrTableRow19,
            this.xrTableRow20,
            this.xrTableRow21,
            this.xrTableRow22,
            this.xrTableRow23,
            this.xrTableRow24,
            this.xrTableRow25});
            this.xrTable2.SizeF = new System.Drawing.SizeF(796.0002F, 249F);
            this.xrTable2.StylePriority.UseFont = false;
            this.xrTable2.StylePriority.UsePadding = false;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell84,
            this.xrTableCell85,
            this.lblSoDong,
            this.xrTableCell87,
            this.xrTableCell86});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Weight = 0.31058818515059949;
            // 
            // xrTableCell84
            // 
            this.xrTableCell84.Name = "xrTableCell84";
            this.xrTableCell84.StylePriority.UseTextAlignment = false;
            this.xrTableCell84.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell84.Weight = 0.75287091062988476;
            // 
            // xrTableCell85
            // 
            this.xrTableCell85.Name = "xrTableCell85";
            this.xrTableCell85.StylePriority.UseTextAlignment = false;
            this.xrTableCell85.Text = "<";
            this.xrTableCell85.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell85.Weight = 0.61961293182508048;
            // 
            // lblSoDong
            // 
            this.lblSoDong.Name = "lblSoDong";
            this.lblSoDong.StylePriority.UseTextAlignment = false;
            this.lblSoDong.Text = "XE";
            this.lblSoDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblSoDong.Weight = 1.3631534262983249;
            // 
            // xrTableCell87
            // 
            this.xrTableCell87.Multiline = true;
            this.xrTableCell87.Name = "xrTableCell87";
            this.xrTableCell87.StylePriority.UseTextAlignment = false;
            this.xrTableCell87.Text = ">";
            this.xrTableCell87.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell87.Weight = 18.715539728166291;
            // 
            // xrTableCell86
            // 
            this.xrTableCell86.Name = "xrTableCell86";
            this.xrTableCell86.StylePriority.UseTextAlignment = false;
            this.xrTableCell86.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            this.xrTableCell86.Weight = 27.87020185314568;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4,
            this.lblMaSoHangHoa,
            this.xrTableCell9,
            this.xrTableCell8,
            this.lblMaQuanLyRieng,
            this.xrTableCell11,
            this.xrTableCell10,
            this.xrTableCell43,
            this.lblMaPhanLoaiTaiXacNhanGia,
            this.xrTableCell6});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Weight = 0.31058818515059949;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Weight = 1.3724859621031054;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "Mã số hàng hóa";
            this.xrTableCell3.Weight = 5.2238349512524342;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Weight = 0.61961569544066175;
            // 
            // lblMaSoHangHoa
            // 
            this.lblMaSoHangHoa.Name = "lblMaSoHangHoa";
            this.lblMaSoHangHoa.Text = "XXXX.XX.XX.X1XE";
            this.lblMaSoHangHoa.Weight = 7.2856359456293047;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "Mã quản lý riêng";
            this.xrTableCell9.Weight = 5.5187788952387056;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Weight = 0.81121019273002659;
            // 
            // lblMaQuanLyRieng
            // 
            this.lblMaQuanLyRieng.Name = "lblMaQuanLyRieng";
            this.lblMaQuanLyRieng.Text = "XXXXXXE";
            this.lblMaQuanLyRieng.Weight = 4.8234074153574875;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "Mã phân loại tái xác nhận giá";
            this.xrTableCell11.Weight = 9.9620987839465638;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Weight = 0.30980761057497941;
            // 
            // xrTableCell43
            // 
            this.xrTableCell43.Name = "xrTableCell43";
            this.xrTableCell43.Text = "[";
            this.xrTableCell43.Weight = 0.61961524162017367;
            // 
            // lblMaPhanLoaiTaiXacNhanGia
            // 
            this.lblMaPhanLoaiTaiXacNhanGia.Name = "lblMaPhanLoaiTaiXacNhanGia";
            this.lblMaPhanLoaiTaiXacNhanGia.Text = "X";
            this.lblMaPhanLoaiTaiXacNhanGia.Weight = 0.74353821646015139;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "]";
            this.xrTableCell6.Weight = 12.031349939711665;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell45,
            this.xrTableCell46,
            this.xrTableCell61,
            this.lblMoTaHangHoa});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Weight = 1.1294115823658162;
            // 
            // xrTableCell45
            // 
            this.xrTableCell45.Name = "xrTableCell45";
            this.xrTableCell45.Weight = 1.3724864424636865;
            // 
            // xrTableCell46
            // 
            this.xrTableCell46.Name = "xrTableCell46";
            this.xrTableCell46.Text = "Mô tả hàng hóa";
            this.xrTableCell46.Weight = 5.22383499667708;
            // 
            // xrTableCell61
            // 
            this.xrTableCell61.Name = "xrTableCell61";
            this.xrTableCell61.Weight = 0.61961587271398066;
            // 
            // lblMoTaHangHoa
            // 
            this.lblMoTaHangHoa.Name = "lblMoTaHangHoa";
            this.lblMoTaHangHoa.Text = "WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6WWWWWWWWW7WWWWWWWWW8W" +
                "WWWWWWWW9WWWWWWWWW0WWWWWWWWW1WWWWWWWWW2WWWWWWWWW3WWWWWWWWW4WWWWWWWWW5WWWWWWWWW6W" +
                "WWWWWWWW7WWWWWWWWW8WWWWWWWWW9WWWWWWWWWE";
            this.lblMoTaHangHoa.Weight = 42.105441538210513;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell44,
            this.xrTableCell62,
            this.xrTableCell63,
            this.xrTableCell66,
            this.xrTableCell65,
            this.xrTableCell67,
            this.lblSoLuong1,
            this.xrTableCell82,
            this.lblMaDonViTinh1});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Weight = 0.3105881851505996;
            // 
            // xrTableCell44
            // 
            this.xrTableCell44.Name = "xrTableCell44";
            this.xrTableCell44.Weight = 1.3724864424636865;
            // 
            // xrTableCell62
            // 
            this.xrTableCell62.Name = "xrTableCell62";
            this.xrTableCell62.Weight = 5.22383499667708;
            // 
            // xrTableCell63
            // 
            this.xrTableCell63.Name = "xrTableCell63";
            this.xrTableCell63.Weight = 0.61961587271398066;
            // 
            // xrTableCell66
            // 
            this.xrTableCell66.Name = "xrTableCell66";
            this.xrTableCell66.Weight = 18.377077268369796;
            // 
            // xrTableCell65
            // 
            this.xrTableCell65.Name = "xrTableCell65";
            this.xrTableCell65.Text = "Số lượng (1)";
            this.xrTableCell65.Weight = 4.9911242871215755;
            // 
            // xrTableCell67
            // 
            this.xrTableCell67.Name = "xrTableCell67";
            this.xrTableCell67.Weight = 0.89296784731133361;
            // 
            // lblSoLuong1
            // 
            this.lblSoLuong1.Name = "lblSoLuong1";
            this.lblSoLuong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuong1.StylePriority.UsePadding = false;
            this.lblSoLuong1.StylePriority.UseTextAlignment = false;
            this.lblSoLuong1.Text = "123.456.789.012";
            this.lblSoLuong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuong1.Weight = 8.90446073567768;
            // 
            // xrTableCell82
            // 
            this.xrTableCell82.Name = "xrTableCell82";
            this.xrTableCell82.Weight = 1.3742038959162062;
            // 
            // lblMaDonViTinh1
            // 
            this.lblMaDonViTinh1.Name = "lblMaDonViTinh1";
            this.lblMaDonViTinh1.Text = "XXXE";
            this.lblMaDonViTinh1.Weight = 7.5656075038139292;
            // 
            // xrTableRow10
            // 
            this.xrTableRow10.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell83,
            this.xrTableCell88,
            this.xrTableCell89,
            this.lblSoMucKhaiKhoangDieuChinh1,
            this.lblSoMucKhaiKhoangDieuChinh2,
            this.lblSoMucKhaiKhoangDieuChinh3,
            this.lblSoMucKhaiKhoangDieuChinh4,
            this.lblSoMucKhaiKhoangDieuChinh5,
            this.xrTableCell91,
            this.xrTableCell92,
            this.lblSoLuong2,
            this.xrTableCell94,
            this.lblMaDonViTinh2});
            this.xrTableRow10.Name = "xrTableRow10";
            this.xrTableRow10.Weight = 0.3105881851505996;
            // 
            // xrTableCell83
            // 
            this.xrTableCell83.Name = "xrTableCell83";
            this.xrTableCell83.Weight = 1.3724864424636865;
            // 
            // xrTableCell88
            // 
            this.xrTableCell88.Name = "xrTableCell88";
            this.xrTableCell88.Text = "Số của mục khai khoản điều chỉnh";
            this.xrTableCell88.Weight = 10.843265758390915;
            // 
            // xrTableCell89
            // 
            this.xrTableCell89.Name = "xrTableCell89";
            this.xrTableCell89.Weight = 0.6196151636206928;
            // 
            // lblSoMucKhaiKhoangDieuChinh1
            // 
            this.lblSoMucKhaiKhoangDieuChinh1.Name = "lblSoMucKhaiKhoangDieuChinh1";
            this.lblSoMucKhaiKhoangDieuChinh1.Text = "N";
            this.lblSoMucKhaiKhoangDieuChinh1.Weight = 1.9063131719982023;
            // 
            // lblSoMucKhaiKhoangDieuChinh2
            // 
            this.lblSoMucKhaiKhoangDieuChinh2.Name = "lblSoMucKhaiKhoangDieuChinh2";
            this.lblSoMucKhaiKhoangDieuChinh2.Text = "N";
            this.lblSoMucKhaiKhoangDieuChinh2.Weight = 2.1686531002308387;
            // 
            // lblSoMucKhaiKhoangDieuChinh3
            // 
            this.lblSoMucKhaiKhoangDieuChinh3.Name = "lblSoMucKhaiKhoangDieuChinh3";
            this.lblSoMucKhaiKhoangDieuChinh3.Text = "N";
            this.lblSoMucKhaiKhoangDieuChinh3.Weight = 2.1686531002308391;
            // 
            // lblSoMucKhaiKhoangDieuChinh4
            // 
            this.lblSoMucKhaiKhoangDieuChinh4.Name = "lblSoMucKhaiKhoangDieuChinh4";
            this.lblSoMucKhaiKhoangDieuChinh4.Text = "N";
            this.lblSoMucKhaiKhoangDieuChinh4.Weight = 2.1686531066676547;
            // 
            // lblSoMucKhaiKhoangDieuChinh5
            // 
            this.lblSoMucKhaiKhoangDieuChinh5.Name = "lblSoMucKhaiKhoangDieuChinh5";
            this.lblSoMucKhaiKhoangDieuChinh5.Text = "N";
            this.lblSoMucKhaiKhoangDieuChinh5.Weight = 4.3453747366217126;
            // 
            // xrTableCell91
            // 
            this.xrTableCell91.Name = "xrTableCell91";
            this.xrTableCell91.Text = "Số lượng (2)";
            this.xrTableCell91.Weight = 4.9911242871215729;
            // 
            // xrTableCell92
            // 
            this.xrTableCell92.Name = "xrTableCell92";
            this.xrTableCell92.Weight = 0.89296784731133361;
            // 
            // lblSoLuong2
            // 
            this.lblSoLuong2.Name = "lblSoLuong2";
            this.lblSoLuong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuong2.StylePriority.UsePadding = false;
            this.lblSoLuong2.StylePriority.UseTextAlignment = false;
            this.lblSoLuong2.Text = "123.456.789.012";
            this.lblSoLuong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuong2.Weight = 8.90446073567768;
            // 
            // xrTableCell94
            // 
            this.xrTableCell94.Name = "xrTableCell94";
            this.xrTableCell94.Weight = 1.3742038959162066;
            // 
            // lblMaDonViTinh2
            // 
            this.lblMaDonViTinh2.Name = "lblMaDonViTinh2";
            this.lblMaDonViTinh2.Text = "XXXE";
            this.lblMaDonViTinh2.Weight = 7.5656075038139292;
            // 
            // xrTableRow11
            // 
            this.xrTableRow11.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell100,
            this.xrTableCell101,
            this.xrTableCell102,
            this.lblTriGiaHoaDon,
            this.xrTableCell104,
            this.xrTableCell107,
            this.xrTableCell108,
            this.xrTableCell109,
            this.lblDonGiaHoaDon,
            this.xrTableCell111,
            this.lblMaDongTienCuaDonGia,
            this.xrTableCell105,
            this.lblDonViCuaDonGiaVaSoLuong});
            this.xrTableRow11.Name = "xrTableRow11";
            this.xrTableRow11.Weight = 0.3105881851505996;
            // 
            // xrTableCell100
            // 
            this.xrTableCell100.Name = "xrTableCell100";
            this.xrTableCell100.Weight = 1.3724864424636865;
            // 
            // xrTableCell101
            // 
            this.xrTableCell101.Name = "xrTableCell101";
            this.xrTableCell101.Text = "Trị giá hóa đơn";
            this.xrTableCell101.Weight = 7.275031278233536;
            // 
            // xrTableCell102
            // 
            this.xrTableCell102.Name = "xrTableCell102";
            this.xrTableCell102.Weight = 0.82743433356030138;
            // 
            // lblTriGiaHoaDon
            // 
            this.lblTriGiaHoaDon.Name = "lblTriGiaHoaDon";
            this.lblTriGiaHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTriGiaHoaDon.StylePriority.UsePadding = false;
            this.lblTriGiaHoaDon.StylePriority.UseTextAlignment = false;
            this.lblTriGiaHoaDon.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaHoaDon.Weight = 11.356608840422533;
            // 
            // xrTableCell104
            // 
            this.xrTableCell104.Name = "xrTableCell104";
            this.xrTableCell104.Weight = 1.3187168798360269;
            // 
            // xrTableCell107
            // 
            this.xrTableCell107.Name = "xrTableCell107";
            this.xrTableCell107.Weight = 3.4427368057084591;
            // 
            // xrTableCell108
            // 
            this.xrTableCell108.Name = "xrTableCell108";
            this.xrTableCell108.Text = "Đơn giá hóa đơn";
            this.xrTableCell108.Weight = 5.8840959238603627;
            // 
            // xrTableCell109
            // 
            this.xrTableCell109.Name = "xrTableCell109";
            this.xrTableCell109.Weight = 0.70996688336157354;
            // 
            // lblDonGiaHoaDon
            // 
            this.lblDonGiaHoaDon.Name = "lblDonGiaHoaDon";
            this.lblDonGiaHoaDon.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblDonGiaHoaDon.StylePriority.UsePadding = false;
            this.lblDonGiaHoaDon.StylePriority.UseTextAlignment = false;
            this.lblDonGiaHoaDon.Text = "123.456.789";
            this.lblDonGiaHoaDon.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblDonGiaHoaDon.Weight = 5.5974803320013535;
            // 
            // xrTableCell111
            // 
            this.xrTableCell111.Name = "xrTableCell111";
            this.xrTableCell111.StylePriority.UseTextAlignment = false;
            this.xrTableCell111.Text = "-";
            this.xrTableCell111.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell111.Weight = 0.93768984963746593;
            // 
            // lblMaDongTienCuaDonGia
            // 
            this.lblMaDongTienCuaDonGia.Name = "lblMaDongTienCuaDonGia";
            this.lblMaDongTienCuaDonGia.Text = "XXE";
            this.lblMaDongTienCuaDonGia.Weight = 1.6593161155110019;
            // 
            // xrTableCell105
            // 
            this.xrTableCell105.Name = "xrTableCell105";
            this.xrTableCell105.StylePriority.UseTextAlignment = false;
            this.xrTableCell105.Text = "-";
            this.xrTableCell105.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell105.Weight = 1.3742114499227218;
            // 
            // lblDonViCuaDonGiaVaSoLuong
            // 
            this.lblDonViCuaDonGiaVaSoLuong.Name = "lblDonViCuaDonGiaVaSoLuong";
            this.lblDonViCuaDonGiaVaSoLuong.Text = "XXXE";
            this.lblDonViCuaDonGiaVaSoLuong.Weight = 7.5656037155462448;
            // 
            // xrTableRow12
            // 
            this.xrTableRow12.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell113,
            this.xrTableCell114,
            this.xrTableCell115});
            this.xrTableRow12.Name = "xrTableRow12";
            this.xrTableRow12.Weight = 0.3105881851505996;
            // 
            // xrTableCell113
            // 
            this.xrTableCell113.Name = "xrTableCell113";
            this.xrTableCell113.Weight = 1.3724864424636865;
            // 
            // xrTableCell114
            // 
            this.xrTableCell114.Name = "xrTableCell114";
            this.xrTableCell114.Text = "Thuế nhập khẩu";
            this.xrTableCell114.Weight = 7.275031278233536;
            // 
            // xrTableCell115
            // 
            this.xrTableCell115.Name = "xrTableCell115";
            this.xrTableCell115.Weight = 40.673861129368035;
            // 
            // xrTableRow13
            // 
            this.xrTableRow13.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell126,
            this.xrTableCell127,
            this.xrTableCell128,
            this.lblTriGiaTinhThueS,
            this.xrTableCell130,
            this.xrTableCell131,
            this.xrTableCell132,
            this.lblMaDongTienCuaGiaTinhThue,
            this.xrTableCell135,
            this.lblTriGiaTinhThueM});
            this.xrTableRow13.Name = "xrTableRow13";
            this.xrTableRow13.Weight = 0.3105881851505996;
            // 
            // xrTableCell126
            // 
            this.xrTableCell126.Name = "xrTableCell126";
            this.xrTableCell126.Weight = 2.0256615792548782;
            // 
            // xrTableCell127
            // 
            this.xrTableCell127.Name = "xrTableCell127";
            this.xrTableCell127.Text = "Trị giá tính thuế(S)";
            this.xrTableCell127.Weight = 6.6218561414423434;
            // 
            // xrTableCell128
            // 
            this.xrTableCell128.Name = "xrTableCell128";
            this.xrTableCell128.Weight = 0.82743433356030138;
            // 
            // lblTriGiaTinhThueS
            // 
            this.lblTriGiaTinhThueS.Name = "lblTriGiaTinhThueS";
            this.lblTriGiaTinhThueS.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueS.StylePriority.UsePadding = false;
            this.lblTriGiaTinhThueS.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueS.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueS.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueS.Weight = 11.356606949507096;
            // 
            // xrTableCell130
            // 
            this.xrTableCell130.Name = "xrTableCell130";
            this.xrTableCell130.Weight = 1.3187187707514627;
            // 
            // xrTableCell131
            // 
            this.xrTableCell131.Name = "xrTableCell131";
            this.xrTableCell131.Text = "VND";
            this.xrTableCell131.Weight = 3.4427368057084591;
            // 
            // xrTableCell132
            // 
            this.xrTableCell132.Name = "xrTableCell132";
            this.xrTableCell132.Text = "Trị giá tính thuế(M)";
            this.xrTableCell132.Weight = 6.8157669177043125;
            // 
            // lblMaDongTienCuaGiaTinhThue
            // 
            this.lblMaDongTienCuaGiaTinhThue.Name = "lblMaDongTienCuaGiaTinhThue";
            this.lblMaDongTienCuaGiaTinhThue.Text = "XXE";
            this.lblMaDongTienCuaGiaTinhThue.Weight = 2.3692263530294957;
            // 
            // xrTableCell135
            // 
            this.xrTableCell135.Name = "xrTableCell135";
            this.xrTableCell135.Text = "-";
            this.xrTableCell135.Weight = 0.83906348239232365;
            // 
            // lblTriGiaTinhThueM
            // 
            this.lblTriGiaTinhThueM.Name = "lblTriGiaTinhThueM";
            this.lblTriGiaTinhThueM.Text = "12.345.678.901.234.567.890";
            this.lblTriGiaTinhThueM.Weight = 13.704307516714589;
            // 
            // xrTableRow14
            // 
            this.xrTableRow14.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell116,
            this.xrTableCell117,
            this.xrTableCell118,
            this.lblSoLuongTinhThue,
            this.xrTableCell120,
            this.lblMaDonViTinhChuanDanhThue,
            this.xrTableCell122,
            this.lblDonGiaTinhThue,
            this.xrTableCell125,
            this.xrTableCell172,
            this.xrTableCell171,
            this.lblDonViSoLuongTrongDonGiaTinhThue});
            this.xrTableRow14.Name = "xrTableRow14";
            this.xrTableRow14.Weight = 0.3105881851505996;
            // 
            // xrTableCell116
            // 
            this.xrTableCell116.Name = "xrTableCell116";
            this.xrTableCell116.Weight = 2.0256615792548782;
            // 
            // xrTableCell117
            // 
            this.xrTableCell117.Name = "xrTableCell117";
            this.xrTableCell117.Text = "Số lượng tính thuế";
            this.xrTableCell117.Weight = 6.6218561414423434;
            // 
            // xrTableCell118
            // 
            this.xrTableCell118.Name = "xrTableCell118";
            this.xrTableCell118.Weight = 0.82743433356030138;
            // 
            // lblSoLuongTinhThue
            // 
            this.lblSoLuongTinhThue.Name = "lblSoLuongTinhThue";
            this.lblSoLuongTinhThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoLuongTinhThue.StylePriority.UsePadding = false;
            this.lblSoLuongTinhThue.StylePriority.UseTextAlignment = false;
            this.lblSoLuongTinhThue.Text = "123.456.789.012";
            this.lblSoLuongTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoLuongTinhThue.Weight = 11.356606949507098;
            // 
            // xrTableCell120
            // 
            this.xrTableCell120.Name = "xrTableCell120";
            this.xrTableCell120.Weight = 1.3187187707514614;
            // 
            // lblMaDonViTinhChuanDanhThue
            // 
            this.lblMaDonViTinhChuanDanhThue.Name = "lblMaDonViTinhChuanDanhThue";
            this.lblMaDonViTinhChuanDanhThue.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThue.Weight = 3.4427377511661761;
            // 
            // xrTableCell122
            // 
            this.xrTableCell122.Name = "xrTableCell122";
            this.xrTableCell122.Text = "Đơn giá tính thuế";
            this.xrTableCell122.Weight = 5.8840864101920811;
            // 
            // lblDonGiaTinhThue
            // 
            this.lblDonGiaTinhThue.Name = "lblDonGiaTinhThue";
            this.lblDonGiaTinhThue.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblDonGiaTinhThue.StylePriority.UsePadding = false;
            this.lblDonGiaTinhThue.StylePriority.UseTextAlignment = false;
            this.lblDonGiaTinhThue.Text = "123.456.789.012.345.678";
            this.lblDonGiaTinhThue.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblDonGiaTinhThue.Weight = 8.9044607742985722;
            // 
            // xrTableCell125
            // 
            this.xrTableCell125.Name = "xrTableCell125";
            this.xrTableCell125.StylePriority.UseTextAlignment = false;
            this.xrTableCell125.Text = "-";
            this.xrTableCell125.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrTableCell125.Weight = 1.3742152542824455;
            // 
            // xrTableCell172
            // 
            this.xrTableCell172.Name = "xrTableCell172";
            this.xrTableCell172.Text = "VND";
            this.xrTableCell172.Weight = 2.5629278743099624;
            // 
            // xrTableCell171
            // 
            this.xrTableCell171.Name = "xrTableCell171";
            this.xrTableCell171.Text = "-";
            this.xrTableCell171.Weight = 1.1497900424660998;
            // 
            // lblDonViSoLuongTrongDonGiaTinhThue
            // 
            this.lblDonViSoLuongTrongDonGiaTinhThue.Name = "lblDonViSoLuongTrongDonGiaTinhThue";
            this.lblDonViSoLuongTrongDonGiaTinhThue.Text = "XXXE";
            this.lblDonViSoLuongTrongDonGiaTinhThue.Weight = 3.8528829688338435;
            // 
            // xrTableRow17
            // 
            this.xrTableRow17.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell160,
            this.xrTableCell161,
            this.xrTableCell162,
            this.lblMaPhanLoaiThueSuat,
            this.lblThueSuatThueNhapKhau,
            this.xrTableCell164,
            this.lblPhanLoaiNhapThueSuat,
            this.xrTableCell166,
            this.lblMaXacDinhMucThueNhapKhau});
            this.xrTableRow17.Name = "xrTableRow17";
            this.xrTableRow17.Weight = 0.3105881851505996;
            // 
            // xrTableCell160
            // 
            this.xrTableCell160.Name = "xrTableCell160";
            this.xrTableCell160.Weight = 2.0256613428904489;
            // 
            // xrTableCell161
            // 
            this.xrTableCell161.Name = "xrTableCell161";
            this.xrTableCell161.Text = "Thuế suất";
            this.xrTableCell161.Weight = 3.6541447305246932;
            // 
            // xrTableCell162
            // 
            this.xrTableCell162.Name = "xrTableCell162";
            this.xrTableCell162.Weight = 0.309808996979551;
            // 
            // lblMaPhanLoaiThueSuat
            // 
            this.lblMaPhanLoaiThueSuat.Name = "lblMaPhanLoaiThueSuat";
            this.lblMaPhanLoaiThueSuat.Text = "X";
            this.lblMaPhanLoaiThueSuat.Weight = 1.1153073118165979;
            // 
            // lblThueSuatThueNhapKhau
            // 
            this.lblThueSuatThueNhapKhau.Name = "lblThueSuatThueNhapKhau";
            this.lblThueSuatThueNhapKhau.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXXE";
            this.lblThueSuatThueNhapKhau.Weight = 15.045355372994344;
            // 
            // xrTableCell164
            // 
            this.xrTableCell164.Name = "xrTableCell164";
            this.xrTableCell164.Text = "-";
            this.xrTableCell164.Weight = 0.64542807556551041;
            // 
            // lblPhanLoaiNhapThueSuat
            // 
            this.lblPhanLoaiNhapThueSuat.Name = "lblPhanLoaiNhapThueSuat";
            this.lblPhanLoaiNhapThueSuat.Text = "X";
            this.lblPhanLoaiNhapThueSuat.Weight = 2.7973068585379597;
            // 
            // xrTableCell166
            // 
            this.xrTableCell166.Name = "xrTableCell166";
            this.xrTableCell166.Text = "Mã áp dụng thuế tuyệt đối";
            this.xrTableCell166.Weight = 9.1849913616676968;
            // 
            // lblMaXacDinhMucThueNhapKhau
            // 
            this.lblMaXacDinhMucThueNhapKhau.Name = "lblMaXacDinhMucThueNhapKhau";
            this.lblMaXacDinhMucThueNhapKhau.Text = "XXXXXXXXXE";
            this.lblMaXacDinhMucThueNhapKhau.Weight = 14.543374799088461;
            // 
            // xrTableRow16
            // 
            this.xrTableRow16.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell149,
            this.xrTableCell150,
            this.xrTableCell151,
            this.lblSoTienThueNK,
            this.xrTableCell153,
            this.xrTableCell154,
            this.xrTableCell155,
            this.xrTableCell156,
            this.lblMaNuocXuatXu,
            this.xrTableCell158,
            this.lblTenNoiXuatXu,
            this.xrTableCell169,
            this.lblMaBieuThueNK});
            this.xrTableRow16.Name = "xrTableRow16";
            this.xrTableRow16.Weight = 0.3105881851505996;
            // 
            // xrTableCell149
            // 
            this.xrTableCell149.Name = "xrTableCell149";
            this.xrTableCell149.Weight = 2.0256615792548782;
            // 
            // xrTableCell150
            // 
            this.xrTableCell150.Name = "xrTableCell150";
            this.xrTableCell150.Text = "Số tiền thuế";
            this.xrTableCell150.Weight = 6.6218561414423434;
            // 
            // xrTableCell151
            // 
            this.xrTableCell151.Name = "xrTableCell151";
            this.xrTableCell151.Weight = 0.82743433356030138;
            // 
            // lblSoTienThueNK
            // 
            this.lblSoTienThueNK.Name = "lblSoTienThueNK";
            this.lblSoTienThueNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienThueNK.StylePriority.UsePadding = false;
            this.lblSoTienThueNK.StylePriority.UseTextAlignment = false;
            this.lblSoTienThueNK.Text = "1.234.567.890.123.456";
            this.lblSoTienThueNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienThueNK.Weight = 11.356606949507098;
            // 
            // xrTableCell153
            // 
            this.xrTableCell153.Name = "xrTableCell153";
            this.xrTableCell153.Weight = 1.3187187707514614;
            // 
            // xrTableCell154
            // 
            this.xrTableCell154.Name = "xrTableCell154";
            this.xrTableCell154.Text = "VND";
            this.xrTableCell154.Weight = 3.4427368057084591;
            // 
            // xrTableCell155
            // 
            this.xrTableCell155.Name = "xrTableCell155";
            this.xrTableCell155.Text = "Nước xuất xứ";
            this.xrTableCell155.Weight = 4.9911242871215746;
            // 
            // xrTableCell156
            // 
            this.xrTableCell156.Name = "xrTableCell156";
            this.xrTableCell156.Weight = 0.89296406548046436;
            // 
            // lblMaNuocXuatXu
            // 
            this.lblMaNuocXuatXu.Name = "lblMaNuocXuatXu";
            this.lblMaNuocXuatXu.Text = "XE";
            this.lblMaNuocXuatXu.Weight = 1.4739648252958213;
            // 
            // xrTableCell158
            // 
            this.xrTableCell158.Name = "xrTableCell158";
            this.xrTableCell158.Text = "-";
            this.xrTableCell158.Weight = 0.98787474527545194;
            // 
            // lblTenNoiXuatXu
            // 
            this.lblTenNoiXuatXu.Name = "lblTenNoiXuatXu";
            this.lblTenNoiXuatXu.Text = "XXXXXXE";
            this.lblTenNoiXuatXu.Weight = 3.8456090866668524;
            // 
            // xrTableCell169
            // 
            this.xrTableCell169.Name = "xrTableCell169";
            this.xrTableCell169.Text = "-";
            this.xrTableCell169.Weight = 0.93768983998224043;
            // 
            // lblMaBieuThueNK
            // 
            this.lblMaBieuThueNK.Name = "lblMaBieuThueNK";
            this.lblMaBieuThueNK.Text = "XXE";
            this.lblMaBieuThueNK.Weight = 10.599137420018316;
            // 
            // xrTableRow15
            // 
            this.xrTableRow15.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell137,
            this.xrTableCell139,
            this.xrTableCell140,
            this.lblSoTienGiamThueNK,
            this.xrTableCell142,
            this.xrTableCell143,
            this.xrTableCell144,
            this.xrTableCell145,
            this.lblMaNgoaiHanNgach});
            this.xrTableRow15.Name = "xrTableRow15";
            this.xrTableRow15.Weight = 0.3105881851505996;
            // 
            // xrTableCell137
            // 
            this.xrTableCell137.Name = "xrTableCell137";
            this.xrTableCell137.Weight = 2.0256615792548782;
            // 
            // xrTableCell139
            // 
            this.xrTableCell139.Name = "xrTableCell139";
            this.xrTableCell139.Text = "Số tiền miễn giảm";
            this.xrTableCell139.Weight = 6.6218561414423434;
            // 
            // xrTableCell140
            // 
            this.xrTableCell140.Name = "xrTableCell140";
            this.xrTableCell140.Weight = 0.82743433356030138;
            // 
            // lblSoTienGiamThueNK
            // 
            this.lblSoTienGiamThueNK.Name = "lblSoTienGiamThueNK";
            this.lblSoTienGiamThueNK.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblSoTienGiamThueNK.StylePriority.UsePadding = false;
            this.lblSoTienGiamThueNK.StylePriority.UseTextAlignment = false;
            this.lblSoTienGiamThueNK.Text = "1.234.567.890.123.456";
            this.lblSoTienGiamThueNK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblSoTienGiamThueNK.Weight = 11.356606949507098;
            // 
            // xrTableCell142
            // 
            this.xrTableCell142.Name = "xrTableCell142";
            this.xrTableCell142.Weight = 1.3187187707514614;
            // 
            // xrTableCell143
            // 
            this.xrTableCell143.Name = "xrTableCell143";
            this.xrTableCell143.Text = "VND";
            this.xrTableCell143.Weight = 3.4427368057084591;
            // 
            // xrTableCell144
            // 
            this.xrTableCell144.Name = "xrTableCell144";
            this.xrTableCell144.Text = "Mã ngoài hạn ngạch";
            this.xrTableCell144.Weight = 7.3580549915715121;
            // 
            // xrTableCell145
            // 
            this.xrTableCell145.Name = "xrTableCell145";
            this.xrTableCell145.Weight = 2.6660017229337276;
            // 
            // lblMaNgoaiHanNgach
            // 
            this.lblMaNgoaiHanNgach.Name = "lblMaNgoaiHanNgach";
            this.lblMaNgoaiHanNgach.Text = "X";
            this.lblMaNgoaiHanNgach.Weight = 13.704307555335484;
            // 
            // xrTableRow18
            // 
            this.xrTableRow18.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell146,
            this.xrTableCell147,
            this.xrTableCell175,
            this.lblSoThuTuDongHangToKhai});
            this.xrTableRow18.Name = "xrTableRow18";
            this.xrTableRow18.Weight = 0.3105881851505996;
            // 
            // xrTableCell146
            // 
            this.xrTableCell146.Name = "xrTableCell146";
            this.xrTableCell146.Weight = 2.0256615792548782;
            // 
            // xrTableCell147
            // 
            this.xrTableCell147.Name = "xrTableCell147";
            this.xrTableCell147.Text = "Số thứ tự của dòng hàng trên tờ khai tạm nhập tái xuất tương ứng";
            this.xrTableCell147.Weight = 22.868024827516429;
            // 
            // xrTableCell175
            // 
            this.xrTableCell175.Name = "xrTableCell175";
            this.xrTableCell175.Weight = 0.6993285946875698;
            // 
            // lblSoThuTuDongHangToKhai
            // 
            this.lblSoThuTuDongHangToKhai.Name = "lblSoThuTuDongHangToKhai";
            this.lblSoThuTuDongHangToKhai.Text = "XE";
            this.lblSoThuTuDongHangToKhai.Weight = 23.728363848606389;
            // 
            // xrTableRow19
            // 
            this.xrTableRow19.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell176,
            this.xrTableCell178,
            this.xrTableCell181,
            this.lblSoDKDanhMucMienThue,
            this.xrTableCell182,
            this.lblSoDongTuongUngDMMienThue});
            this.xrTableRow19.Name = "xrTableRow19";
            this.xrTableRow19.Weight = 0.3105881851505996;
            // 
            // xrTableCell176
            // 
            this.xrTableCell176.Name = "xrTableCell176";
            this.xrTableCell176.Weight = 2.0256615792548782;
            // 
            // xrTableCell178
            // 
            this.xrTableCell178.Name = "xrTableCell178";
            this.xrTableCell178.Text = "Danh mục miễn thuế nhập khẩu";
            this.xrTableCell178.Weight = 10.547368220229997;
            // 
            // xrTableCell181
            // 
            this.xrTableCell181.Name = "xrTableCell181";
            this.xrTableCell181.Weight = 1.0739936364865077;
            // 
            // lblSoDKDanhMucMienThue
            // 
            this.lblSoDKDanhMucMienThue.Name = "lblSoDKDanhMucMienThue";
            this.lblSoDKDanhMucMienThue.Text = "NNNNNNNNN1NE";
            this.lblSoDKDanhMucMienThue.Weight = 7.1845369731061846;
            // 
            // xrTableCell182
            // 
            this.xrTableCell182.Name = "xrTableCell182";
            this.xrTableCell182.Text = "-";
            this.xrTableCell182.Weight = 1.3187157816924771;
            // 
            // lblSoDongTuongUngDMMienThue
            // 
            this.lblSoDongTuongUngDMMienThue.Name = "lblSoDongTuongUngDMMienThue";
            this.lblSoDongTuongUngDMMienThue.Text = "XXE";
            this.lblSoDongTuongUngDMMienThue.Weight = 27.171102659295222;
            // 
            // xrTableRow20
            // 
            this.xrTableRow20.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell183,
            this.xrTableCell184,
            this.xrTableCell185});
            this.xrTableRow20.Name = "xrTableRow20";
            this.xrTableRow20.Weight = 0.3105881851505996;
            // 
            // xrTableCell183
            // 
            this.xrTableCell183.Name = "xrTableCell183";
            this.xrTableCell183.Weight = 1.372485497005969;
            // 
            // xrTableCell184
            // 
            this.xrTableCell184.Name = "xrTableCell184";
            this.xrTableCell184.Text = "Miễn / Giảm / Không chịu thuế nhập khẩu";
            this.xrTableCell184.Weight = 19.459073972648618;
            // 
            // xrTableCell185
            // 
            this.xrTableCell185.Name = "xrTableCell185";
            this.xrTableCell185.Weight = 28.489819380410673;
            // 
            // xrTableRow21
            // 
            this.xrTableRow21.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell186,
            this.lblMaMienGiamThueNK,
            this.lblDieuKhoanMienGiam});
            this.xrTableRow21.Name = "xrTableRow21";
            this.xrTableRow21.Weight = 0.3105881851505996;
            // 
            // xrTableCell186
            // 
            this.xrTableCell186.Name = "xrTableCell186";
            this.xrTableCell186.Weight = 3.9542166311805724;
            // 
            // lblMaMienGiamThueNK
            // 
            this.lblMaMienGiamThueNK.Name = "lblMaMienGiamThueNK";
            this.lblMaMienGiamThueNK.Text = "XXXXE";
            this.lblMaMienGiamThueNK.Weight = 4.0491468011713092;
            // 
            // lblDieuKhoanMienGiam
            // 
            this.lblDieuKhoanMienGiam.Name = "lblDieuKhoanMienGiam";
            this.lblDieuKhoanMienGiam.Text = "XXXXXXXXX1XXXXXXXXX2XXXXXXXXX3XXXXXXXXX4XXXXXXXXX5XXXXXXXXXE";
            this.lblDieuKhoanMienGiam.Weight = 41.318015417713383;
            // 
            // xrTableRow22
            // 
            this.xrTableRow22.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell189,
            this.xrTableCell190,
            this.xrTableCell191});
            this.xrTableRow22.Name = "xrTableRow22";
            this.xrTableRow22.Weight = 0.3105881851505996;
            // 
            // xrTableCell189
            // 
            this.xrTableCell189.Name = "xrTableCell189";
            this.xrTableCell189.Weight = 1.3724864424636856;
            // 
            // xrTableCell190
            // 
            this.xrTableCell190.Name = "xrTableCell190";
            this.xrTableCell190.Text = "Thuế và thu khác";
            this.xrTableCell190.Weight = 6.6308769898881961;
            // 
            // xrTableCell191
            // 
            this.xrTableCell191.Name = "xrTableCell191";
            this.xrTableCell191.Weight = 41.318015417713383;
            // 
            // xrTableRow23
            // 
            this.xrTableRow23.Borders = DevExpress.XtraPrinting.BorderSide.Top;
            this.xrTableRow23.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell192,
            this.xrTableCell196,
            this.xrTableCell193,
            this.xrTableCell197,
            this.xrTableCell198,
            this.lblTenKhoanMucThueVaThuKhac1,
            this.xrTableCell200,
            this.xrTableCell201,
            this.xrTableCell199,
            this.lblMaApDungThueSuatThueVaThuKhac1});
            this.xrTableRow23.Name = "xrTableRow23";
            this.xrTableRow23.StylePriority.UseBorders = false;
            this.xrTableRow23.Weight = 0.3105881851505996;
            // 
            // xrTableCell192
            // 
            this.xrTableCell192.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell192.Name = "xrTableCell192";
            this.xrTableCell192.StylePriority.UseBorders = false;
            this.xrTableCell192.Weight = 1.1153072887444959;
            // 
            // xrTableCell196
            // 
            this.xrTableCell196.Borders = DevExpress.XtraPrinting.BorderSide.None;
            this.xrTableCell196.Name = "xrTableCell196";
            this.xrTableCell196.StylePriority.UseBorders = false;
            this.xrTableCell196.Text = "1";
            this.xrTableCell196.Weight = 0.91035431703856384;
            // 
            // xrTableCell193
            // 
            this.xrTableCell193.Name = "xrTableCell193";
            this.xrTableCell193.Weight = 0.61961519618361738;
            // 
            // xrTableCell197
            // 
            this.xrTableCell197.Name = "xrTableCell197";
            this.xrTableCell197.Text = "Tên";
            this.xrTableCell197.Weight = 4.570660388635325;
            // 
            // xrTableCell198
            // 
            this.xrTableCell198.Name = "xrTableCell198";
            this.xrTableCell198.Weight = 0.78743113265388542;
            // 
            // lblTenKhoanMucThueVaThuKhac1
            // 
            this.lblTenKhoanMucThueVaThuKhac1.Name = "lblTenKhoanMucThueVaThuKhac1";
            this.lblTenKhoanMucThueVaThuKhac1.Text = "WWWWWWWWE";
            this.lblTenKhoanMucThueVaThuKhac1.Weight = 15.411955469183972;
            // 
            // xrTableCell200
            // 
            this.xrTableCell200.Name = "xrTableCell200";
            this.xrTableCell200.Weight = 1.4783631233882391;
            // 
            // xrTableCell201
            // 
            this.xrTableCell201.Name = "xrTableCell201";
            this.xrTableCell201.Text = "Mã áp dụng thuế suất";
            this.xrTableCell201.Weight = 7.293389441309003;
            // 
            // xrTableCell199
            // 
            this.xrTableCell199.Name = "xrTableCell199";
            this.xrTableCell199.Weight = 0.82594432550170582;
            // 
            // lblMaApDungThueSuatThueVaThuKhac1
            // 
            this.lblMaApDungThueSuatThueVaThuKhac1.Name = "lblMaApDungThueSuatThueVaThuKhac1";
            this.lblMaApDungThueSuatThueVaThuKhac1.Text = "XXXXXXXXXE";
            this.lblMaApDungThueSuatThueVaThuKhac1.Weight = 16.308358167426462;
            // 
            // xrTableRow24
            // 
            this.xrTableRow24.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell202,
            this.xrTableCell203,
            this.xrTableCell204,
            this.xrTableCell205,
            this.lblTriGiaTinhThueVaThuKhac1,
            this.xrTableCell215,
            this.xrTableCell210,
            this.xrTableCell211,
            this.xrTableCell212,
            this.xrTableCell213,
            this.lblSoLuongTinhThueVaThuKhac1,
            this.xrTableCell217,
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1});
            this.xrTableRow24.Name = "xrTableRow24";
            this.xrTableRow24.Weight = 0.3105881851505996;
            // 
            // xrTableCell202
            // 
            this.xrTableCell202.Name = "xrTableCell202";
            this.xrTableCell202.Weight = 1.1153071705622812;
            // 
            // xrTableCell203
            // 
            this.xrTableCell203.Name = "xrTableCell203";
            this.xrTableCell203.Weight = 0.91035443522077852;
            // 
            // xrTableCell204
            // 
            this.xrTableCell204.Name = "xrTableCell204";
            this.xrTableCell204.Weight = 0.61961519618361738;
            // 
            // xrTableCell205
            // 
            this.xrTableCell205.Name = "xrTableCell205";
            this.xrTableCell205.Text = "Trị giá tính thuế";
            this.xrTableCell205.Weight = 6.8296756037669626;
            // 
            // lblTriGiaTinhThueVaThuKhac1
            // 
            this.lblTriGiaTinhThueVaThuKhac1.Name = "lblTriGiaTinhThueVaThuKhac1";
            this.lblTriGiaTinhThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblTriGiaTinhThueVaThuKhac1.StylePriority.UsePadding = false;
            this.lblTriGiaTinhThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblTriGiaTinhThueVaThuKhac1.Text = "12.345.678.901.234.567";
            this.lblTriGiaTinhThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblTriGiaTinhThueVaThuKhac1.Weight = 11.356607713899845;
            // 
            // xrTableCell215
            // 
            this.xrTableCell215.Name = "xrTableCell215";
            this.xrTableCell215.Weight = 1.318719001440062;
            // 
            // xrTableCell210
            // 
            this.xrTableCell210.Name = "xrTableCell210";
            this.xrTableCell210.Text = "VND";
            this.xrTableCell210.Weight = 1.8891739288933296;
            // 
            // xrTableCell211
            // 
            this.xrTableCell211.Name = "xrTableCell211";
            this.xrTableCell211.Weight = 0.85423197494578718;
            // 
            // xrTableCell212
            // 
            this.xrTableCell212.Name = "xrTableCell212";
            this.xrTableCell212.Text = "Số lượng tính thuế";
            this.xrTableCell212.Weight = 6.5834149866891067;
            // 
            // xrTableCell213
            // 
            this.xrTableCell213.Name = "xrTableCell213";
            this.xrTableCell213.Weight = 1.5359197255793196;
            // 
            // lblSoLuongTinhThueVaThuKhac1
            // 
            this.lblSoLuongTinhThueVaThuKhac1.Name = "lblSoLuongTinhThueVaThuKhac1";
            this.lblSoLuongTinhThueVaThuKhac1.Text = "123.456.789.012";
            this.lblSoLuongTinhThueVaThuKhac1.Weight = 5.7092294175211347;
            // 
            // xrTableCell217
            // 
            this.xrTableCell217.Name = "xrTableCell217";
            this.xrTableCell217.Weight = 0.90100610459912522;
            // 
            // lblMaDonViTinhChuanDanhThueVaThuKhac1
            // 
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.Name = "lblMaDonViTinhChuanDanhThueVaThuKhac1";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.SnapLineMargin = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.Text = "XXXE";
            this.lblMaDonViTinhChuanDanhThueVaThuKhac1.Weight = 9.6981235907639185;
            // 
            // xrTableRow25
            // 
            this.xrTableRow25.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell219,
            this.xrTableCell220,
            this.xrTableCell221,
            this.xrTableCell222,
            this.xrTableCell223,
            this.lblThueSuatThueVaThuKhac1,
            this.xrTableCell225,
            this.xrTableCell226,
            this.xrTableCell227,
            this.xrTableCell229,
            this.xrTableCell231});
            this.xrTableRow25.Name = "xrTableRow25";
            this.xrTableRow25.Weight = 0.3105881851505996;
            // 
            // xrTableCell219
            // 
            this.xrTableCell219.Name = "xrTableCell219";
            this.xrTableCell219.Weight = 1.1153071705622812;
            // 
            // xrTableCell220
            // 
            this.xrTableCell220.Name = "xrTableCell220";
            this.xrTableCell220.Weight = 0.91035443522077852;
            // 
            // xrTableCell221
            // 
            this.xrTableCell221.Name = "xrTableCell221";
            this.xrTableCell221.Weight = 0.61961519618361738;
            // 
            // xrTableCell222
            // 
            this.xrTableCell222.Name = "xrTableCell222";
            this.xrTableCell222.Text = "Thuế suất";
            this.xrTableCell222.Weight = 4.570660388635325;
            // 
            // xrTableCell223
            // 
            this.xrTableCell223.Name = "xrTableCell223";
            this.xrTableCell223.Weight = 0.78742640536529862;
            // 
            // lblThueSuatThueVaThuKhac1
            // 
            this.lblThueSuatThueVaThuKhac1.Name = "lblThueSuatThueVaThuKhac1";
            this.lblThueSuatThueVaThuKhac1.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 2, 0, 0, 100F);
            this.lblThueSuatThueVaThuKhac1.StylePriority.UsePadding = false;
            this.lblThueSuatThueVaThuKhac1.StylePriority.UseTextAlignment = false;
            this.lblThueSuatThueVaThuKhac1.Text = "XXXXXXXXX1XXXXXXXXX2XXXXE";
            this.lblThueSuatThueVaThuKhac1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight;
            this.lblThueSuatThueVaThuKhac1.Weight = 12.828196523666183;
            // 
            // xrTableCell225
            // 
            this.xrTableCell225.Name = "xrTableCell225";
            this.xrTableCell225.Weight = 1.318719001440062;
            // 
            // xrTableCell226
            // 
            this.xrTableCell226.Name = "xrTableCell226";
            this.xrTableCell226.Weight = 1.8891739288933296;
            // 
            // xrTableCell227
            // 
            this.xrTableCell227.Name = "xrTableCell227";
            this.xrTableCell227.Weight = 0.85423197494578718;
            // 
            // xrTableCell229
            // 
            this.xrTableCell229.Name = "xrTableCell229";
            this.xrTableCell229.Text = "Miễn / Giảm / Không chịu thuế và thu khác";
            this.xrTableCell229.Weight = 13.828556775223014;
            // 
            // xrTableCell231
            // 
            this.xrTableCell231.Name = "xrTableCell231";
            this.xrTableCell231.Weight = 10.599137049929588;
            // 
            // ToKhaiTamHangHoaNhapKhau3
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.DetailReport});
            this.Margins = new System.Drawing.Printing.Margins(14, 15, 19, 58);
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.DetailReportBand DetailReport;
        private DevExpress.XtraReports.UI.DetailBand Detail1;
        internal DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell68;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell69;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell74;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell73;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell75;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell72;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiDauTien;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell76;
        private DevExpress.XtraReports.UI.XRTableCell lblSoNhanhToKhaiChiaNho;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell78;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoToKhaiChiaNho;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell17;
        private DevExpress.XtraReports.UI.XRTableCell lblSoToKhaiTamNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell27;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiKiemTra;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell31;
        private DevExpress.XtraReports.UI.XRTableCell lblMaLoaiHinh;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiHangHoa;
        private DevExpress.XtraReports.UI.XRTableCell lblMaHieuPhuongThucVanChuyen;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell35;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiCaNhanToChuc;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell22;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSoHangHoaDaiDienToKhai;
        internal DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell38;
        private DevExpress.XtraReports.UI.XRTableCell lblTenCoQuanHaiQuanTiepNhanToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell23;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBoPhanXuLyToKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell51;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell15;
        private DevExpress.XtraReports.UI.XRTableCell lblGioDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell54;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell55;
        private DevExpress.XtraReports.UI.XRTableCell lblNgayThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRTableCell lblGioThayDoiDangKy;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell58;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell59;
        private DevExpress.XtraReports.UI.XRTableCell lblThoiHanTaiNhapTaiXuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell42;
        private DevExpress.XtraReports.UI.XRTableCell lblBieuThiTHHetHan;
        private DevExpress.XtraReports.UI.XRTable xrTable5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow36;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTrang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell208;
        private DevExpress.XtraReports.UI.XRTableCell lblTongSoTrang;
        private DevExpress.XtraReports.UI.XRLabel lblTenThongTinXuat;
        private DevExpress.XtraReports.UI.XRTable xrTable2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell84;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell85;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDong;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell87;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell86;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell lblMaSoHangHoa;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell lblMaQuanLyRieng;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell43;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiTaiXacNhanGia;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell61;
        private DevExpress.XtraReports.UI.XRTableCell lblMoTaHangHoa;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell62;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell63;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell66;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell65;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell67;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell82;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDonViTinh1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell83;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell88;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell89;
        private DevExpress.XtraReports.UI.XRTableCell lblSoMucKhaiKhoangDieuChinh1;
        private DevExpress.XtraReports.UI.XRTableCell lblSoMucKhaiKhoangDieuChinh2;
        private DevExpress.XtraReports.UI.XRTableCell lblSoMucKhaiKhoangDieuChinh3;
        private DevExpress.XtraReports.UI.XRTableCell lblSoMucKhaiKhoangDieuChinh4;
        private DevExpress.XtraReports.UI.XRTableCell lblSoMucKhaiKhoangDieuChinh5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell91;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell92;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuong2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell94;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDonViTinh2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow11;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell100;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell101;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell102;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell104;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell107;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell108;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell109;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaHoaDon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell111;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDongTienCuaDonGia;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell105;
        private DevExpress.XtraReports.UI.XRTableCell lblDonViCuaDonGiaVaSoLuong;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow12;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell113;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell114;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell115;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow13;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell126;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell127;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell128;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTinhThueS;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell130;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell131;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell132;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDongTienCuaGiaTinhThue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell135;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTinhThueM;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow14;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell116;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell117;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell118;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTinhThue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell120;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDonViTinhChuanDanhThue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell122;
        private DevExpress.XtraReports.UI.XRTableCell lblDonGiaTinhThue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell125;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell172;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell171;
        private DevExpress.XtraReports.UI.XRTableCell lblDonViSoLuongTrongDonGiaTinhThue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow17;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell160;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell161;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell162;
        private DevExpress.XtraReports.UI.XRTableCell lblMaPhanLoaiThueSuat;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatThueNhapKhau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell164;
        private DevExpress.XtraReports.UI.XRTableCell lblPhanLoaiNhapThueSuat;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell166;
        private DevExpress.XtraReports.UI.XRTableCell lblMaXacDinhMucThueNhapKhau;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow16;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell149;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell150;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell151;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell153;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell154;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell155;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell156;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNuocXuatXu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell158;
        private DevExpress.XtraReports.UI.XRTableCell lblTenNoiXuatXu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell169;
        private DevExpress.XtraReports.UI.XRTableCell lblMaBieuThueNK;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow15;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell137;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell139;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell140;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienGiamThueNK;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell142;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell143;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell144;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell145;
        private DevExpress.XtraReports.UI.XRTableCell lblMaNgoaiHanNgach;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow18;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell146;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell147;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell175;
        private DevExpress.XtraReports.UI.XRTableCell lblSoThuTuDongHangToKhai;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow19;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell176;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell178;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell181;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDKDanhMucMienThue;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell182;
        private DevExpress.XtraReports.UI.XRTableCell lblSoDongTuongUngDMMienThue;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow20;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell183;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell184;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell185;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow21;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell186;
        private DevExpress.XtraReports.UI.XRTableCell lblMaMienGiamThueNK;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKhoanMienGiam;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow22;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell189;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell190;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell191;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow23;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell192;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell196;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell193;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell197;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell198;
        private DevExpress.XtraReports.UI.XRTableCell lblTenKhoanMucThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell200;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell201;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell199;
        private DevExpress.XtraReports.UI.XRTableCell lblMaApDungThueSuatThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow24;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell202;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell203;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell204;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell205;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTinhThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell215;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell210;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell211;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell212;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell213;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTinhThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell217;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDonViTinhChuanDanhThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow25;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell219;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell220;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell221;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell222;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell223;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell225;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell226;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell227;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell229;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell231;
        private DevExpress.XtraReports.UI.XRTable xrTable6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow27;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell233;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell234;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell236;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell238;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell239;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow30;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell240;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell241;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell242;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienGiamThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell244;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell245;
        private DevExpress.XtraReports.UI.XRTable xrTable9;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow35;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell282;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell283;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell284;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell286;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell287;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow37;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell288;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell289;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell290;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienGiamThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell292;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell293;
        private DevExpress.XtraReports.UI.XRTable xrTable14;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow42;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell322;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell323;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell310;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell311;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell312;
        private DevExpress.XtraReports.UI.XRTableCell lblTenKhoanMucThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell314;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell324;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell326;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell327;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell325;
        private DevExpress.XtraReports.UI.XRTableCell lblMaApDungThueSuatThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow43;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell316;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell317;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell318;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTinhThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell320;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell328;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell330;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell331;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell329;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTinhThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell333;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDonViTinhChuanDanhThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow44;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell334;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell335;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell336;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell338;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell339;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell340;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell341;
        private DevExpress.XtraReports.UI.XRTable xrTable10;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow38;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell294;
        private DevExpress.XtraReports.UI.XRTableCell lblMaMienGiamThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell296;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKhoanMienGiamThueVaThuKhac2;
        private DevExpress.XtraReports.UI.XRTable xrTable15;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow45;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell342;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell343;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell344;
        private DevExpress.XtraReports.UI.XRTableCell aa;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell346;
        private DevExpress.XtraReports.UI.XRTableCell lblTenKhoanMucThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell348;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell349;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell350;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell351;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell352;
        private DevExpress.XtraReports.UI.XRTableCell lblMaApDungThueSuatThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow46;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell354;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell355;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell356;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTinhThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell358;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell359;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell360;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell361;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell362;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTinhThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell364;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDonViTinhChuanDanhThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow47;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell366;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell367;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell368;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell370;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell371;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell372;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell373;
        private DevExpress.XtraReports.UI.XRTable xrTable17;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow51;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell406;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell407;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell408;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell409;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell410;
        private DevExpress.XtraReports.UI.XRTableCell lblTenKhoanMucThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell412;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell413;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell414;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell415;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell416;
        private DevExpress.XtraReports.UI.XRTableCell lblMaApDungThueSuatThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow52;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell418;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell419;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell420;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTinhThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell422;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell423;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell424;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell425;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell426;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTinhThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell428;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDonViTinhChuanDanhThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow53;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell430;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell431;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell432;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell434;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell435;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell436;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell437;
        private DevExpress.XtraReports.UI.XRTable xrTable16;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow48;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell374;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell375;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell376;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell377;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell378;
        private DevExpress.XtraReports.UI.XRTableCell lblTenKhoanMucThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell380;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell381;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell382;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell383;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell384;
        private DevExpress.XtraReports.UI.XRTableCell lblMaApDungThueSuatThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow49;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell386;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell387;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell388;
        private DevExpress.XtraReports.UI.XRTableCell lblTriGiaTinhThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell390;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell391;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell392;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell393;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell394;
        private DevExpress.XtraReports.UI.XRTableCell lblSoLuongTinhThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell396;
        private DevExpress.XtraReports.UI.XRTableCell lblMaDonViTinhChuanDanhThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow50;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell398;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell399;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell400;
        private DevExpress.XtraReports.UI.XRTableCell lblThueSuatThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell402;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell403;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell404;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell405;
        private DevExpress.XtraReports.UI.XRTable xrTable11;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow39;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell298;
        private DevExpress.XtraReports.UI.XRTableCell lblMaMienGiamThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell300;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKhoanMienGiamThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRTable xrTable7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow31;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell246;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell247;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell248;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell250;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell252;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow32;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell264;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell265;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell266;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienGiamThueVaThuKhac3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell268;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell269;
        private DevExpress.XtraReports.UI.XRTable xrTable4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow26;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell228;
        private DevExpress.XtraReports.UI.XRTableCell lblMaMienGiamThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell232;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKhoanMienGiamThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRTable xrTable8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow33;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell270;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell271;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell272;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell274;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell275;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow34;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell276;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell277;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell278;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienGiamThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell280;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell281;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow28;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell255;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell256;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell253;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell257;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell254;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow29;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell258;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell259;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell260;
        private DevExpress.XtraReports.UI.XRTableCell lblSoTienGiamThueVaThuKhac1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell262;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell263;
        private DevExpress.XtraReports.UI.XRTable xrTable12;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow40;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell302;
        private DevExpress.XtraReports.UI.XRTableCell lblMaMienGiamThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell304;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKhoanMienGiamThueVaThuKhac4;
        private DevExpress.XtraReports.UI.XRTable xrTable13;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow41;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell306;
        private DevExpress.XtraReports.UI.XRTableCell lblMaMienGiamThueVaThuKhac5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell308;
        private DevExpress.XtraReports.UI.XRTableCell lblDieuKhoanMienGiamThueVaThuKhac5;
    }
}
