﻿using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using Company.BLL;
using DuLieuChuan = Company.KDT.SHARE.Components.DuLieuChuan;
using Company.Controls;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Collections;
using System.Collections.Generic;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.GridEX;
using Janus.Windows.UI.Dock;
using Janus.Windows.ExplorerBar;
using System;
using Janus.Windows.FilterEditor;
using Janus.Windows.EditControls;
using Janus.Windows.UI.StatusBar;
using Company.Controls.CustomValidation;
using DongBoDuLieu.Controls;
using Company.KDT.SHARE.Components;

namespace DongBoDuLieu
{
    public partial class BaseForm : Form
    {
        public WS.Service myService = new WS.Service();
        public string MaDoanhNghiep;
        public string MaHaiQuan;
        public OpenFormType OpenType;
        public string CalledForm = string.Empty;
        public int langCheck = 0;

        protected DongBoDuLieu.Controls.MessageBoxControl _MsgBox;

        private IContainer components = null;
        ResourceManager resource = null;

        public string ShowMessage(string message, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog();
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }
        public string Message(string key, string value, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            string message = "";
            if (resource.GetString(key) != null && resource.GetString(key).Trim() != "")
            {
                // resource asign in InitCulture() 
                message = resource.GetString(key);
                message = message.Replace("$", value);
            }
            this._MsgBox.MessageString = message;
            this._MsgBox.ShowDialog();
            string st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();
            return st;
        }

        public string MLMessages(string msg, string key, string value, bool showYesNoButton)
        {
            this._MsgBox = new MessageBoxControl();
            this._MsgBox.ShowYesNoButton = showYesNoButton;
            string message = msg;
            string st = "";
            //if (Properties.Settings.Default.ngonngu == "0")
            //{
            this._MsgBox.MessageString = msg;
            this._MsgBox.ShowDialog();
            st = this._MsgBox.ReturnValue;
            _MsgBox.Dispose();

            //}
            //else if (Properties.Settings.Default.ngonngu == "1")
            //{
            //    if (resource.GetString(key) != null && resource.GetString(key).Trim() != "")
            //    {
            //        // resource asign in InitCulture() 
            //        message = resource.GetString(key);
            //        message = message.Replace("$", value);
            //    }
            //    this._MsgBox.MessageString = message;
            //    this._MsgBox.ShowDialog();
            //    st = this._MsgBox.ReturnValue;
            //    _MsgBox.Dispose();
            //}

            return st;
        }

        public BaseForm()
        {
            InitializeComponent();

        }
        private void BaseForm_Load(object sender, System.EventArgs e)
        {
            //this.InitCulture("en-US");
        }

        public bool checkService()
        {
            try
            {
                Company.KDT.SHARE.Components.DaiLy daiLy = myService.LoginDaiLy("", "");

                if (daiLy == null)
                {
                    MLMessages("Không thể kết nối đến webservice!", "", "", false);
                }

                return daiLy != null;
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Unable to connect to the remote server"))
                    MLMessages("Không thể kết nối đến webservice!", "", "", false);
                else if (ex.ToString().Contains("time out"))
                    MLMessages("Thời gian kết nối đến service đồng bộ dữ liệu quá lâu!", "", "", false);
                else
                    MLMessages("Xảy ra lỗi: " + ex.ToString(), "", "", false);
                return false;
            }
        }
    }
}