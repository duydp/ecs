﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_ManufactureFactory : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long StorageAreasProduction_ID { set; get; }
		public decimal LoaiCSSX { set; get; }
		public string DiaChiCSSX { set; get; }
		public decimal LoaiDiaChiCSSX { set; get; }
		public decimal SoLuongSoHuu { set; get; }
		public decimal SoLuongDiThue { set; get; }
		public decimal SoLuongKhac { set; get; }
		public decimal TongSoLuong { set; get; }
		public string NangLucSanXuat { set; get; }
		public decimal DienTichNhaXuong { set; get; }
		public decimal SoLuongCongNhan { set; get; }

        public List<KDT_VNACCS_Careery> CareeryCollection = new List<KDT_VNACCS_Careery>();

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_ManufactureFactory> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_ManufactureFactory> collection = new List<KDT_VNACCS_ManufactureFactory>();
			while (reader.Read())
			{
				KDT_VNACCS_ManufactureFactory entity = new KDT_VNACCS_ManufactureFactory();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("StorageAreasProduction_ID"))) entity.StorageAreasProduction_ID = reader.GetInt64(reader.GetOrdinal("StorageAreasProduction_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiCSSX"))) entity.LoaiCSSX = reader.GetDecimal(reader.GetOrdinal("LoaiCSSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiCSSX"))) entity.DiaChiCSSX = reader.GetString(reader.GetOrdinal("DiaChiCSSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiDiaChiCSSX"))) entity.LoaiDiaChiCSSX = reader.GetDecimal(reader.GetOrdinal("LoaiDiaChiCSSX"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongSoHuu"))) entity.SoLuongSoHuu = reader.GetDecimal(reader.GetOrdinal("SoLuongSoHuu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongDiThue"))) entity.SoLuongDiThue = reader.GetDecimal(reader.GetOrdinal("SoLuongDiThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongKhac"))) entity.SoLuongKhac = reader.GetDecimal(reader.GetOrdinal("SoLuongKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoLuong"))) entity.TongSoLuong = reader.GetDecimal(reader.GetOrdinal("TongSoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NangLucSanXuat"))) entity.NangLucSanXuat = reader.GetString(reader.GetOrdinal("NangLucSanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("DienTichNhaXuong"))) entity.DienTichNhaXuong = reader.GetDecimal(reader.GetOrdinal("DienTichNhaXuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongCongNhan"))) entity.SoLuongCongNhan = reader.GetDecimal(reader.GetOrdinal("SoLuongCongNhan"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_ManufactureFactory> collection, long id)
        {
            foreach (KDT_VNACCS_ManufactureFactory item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_ManufactureFactory VALUES(@StorageAreasProduction_ID, @LoaiCSSX, @DiaChiCSSX, @LoaiDiaChiCSSX, @SoLuongSoHuu, @SoLuongDiThue, @SoLuongKhac, @TongSoLuong, @NangLucSanXuat, @DienTichNhaXuong, @SoLuongCongNhan)";
            string update = "UPDATE t_KDT_VNACCS_ManufactureFactory SET StorageAreasProduction_ID = @StorageAreasProduction_ID, LoaiCSSX = @LoaiCSSX, DiaChiCSSX = @DiaChiCSSX, LoaiDiaChiCSSX = @LoaiDiaChiCSSX, SoLuongSoHuu = @SoLuongSoHuu, SoLuongDiThue = @SoLuongDiThue, SoLuongKhac = @SoLuongKhac, TongSoLuong = @TongSoLuong, NangLucSanXuat = @NangLucSanXuat, DienTichNhaXuong = @DienTichNhaXuong, SoLuongCongNhan = @SoLuongCongNhan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_ManufactureFactory WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiCSSX", SqlDbType.Decimal, "LoaiCSSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiCSSX", SqlDbType.NVarChar, "DiaChiCSSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiDiaChiCSSX", SqlDbType.Decimal, "LoaiDiaChiCSSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongSoHuu", SqlDbType.Decimal, "SoLuongSoHuu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDiThue", SqlDbType.Decimal, "SoLuongDiThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongKhac", SqlDbType.Decimal, "SoLuongKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoLuong", SqlDbType.Decimal, "TongSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NangLucSanXuat", SqlDbType.NVarChar, "NangLucSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DienTichNhaXuong", SqlDbType.Decimal, "DienTichNhaXuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCongNhan", SqlDbType.Decimal, "SoLuongCongNhan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiCSSX", SqlDbType.Decimal, "LoaiCSSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiCSSX", SqlDbType.NVarChar, "DiaChiCSSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiDiaChiCSSX", SqlDbType.Decimal, "LoaiDiaChiCSSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongSoHuu", SqlDbType.Decimal, "SoLuongSoHuu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDiThue", SqlDbType.Decimal, "SoLuongDiThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongKhac", SqlDbType.Decimal, "SoLuongKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoLuong", SqlDbType.Decimal, "TongSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NangLucSanXuat", SqlDbType.NVarChar, "NangLucSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DienTichNhaXuong", SqlDbType.Decimal, "DienTichNhaXuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCongNhan", SqlDbType.Decimal, "SoLuongCongNhan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_ManufactureFactory VALUES(@StorageAreasProduction_ID, @LoaiCSSX, @DiaChiCSSX, @LoaiDiaChiCSSX, @SoLuongSoHuu, @SoLuongDiThue, @SoLuongKhac, @TongSoLuong, @NangLucSanXuat, @DienTichNhaXuong, @SoLuongCongNhan)";
            string update = "UPDATE t_KDT_VNACCS_ManufactureFactory SET StorageAreasProduction_ID = @StorageAreasProduction_ID, LoaiCSSX = @LoaiCSSX, DiaChiCSSX = @DiaChiCSSX, LoaiDiaChiCSSX = @LoaiDiaChiCSSX, SoLuongSoHuu = @SoLuongSoHuu, SoLuongDiThue = @SoLuongDiThue, SoLuongKhac = @SoLuongKhac, TongSoLuong = @TongSoLuong, NangLucSanXuat = @NangLucSanXuat, DienTichNhaXuong = @DienTichNhaXuong, SoLuongCongNhan = @SoLuongCongNhan WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_ManufactureFactory WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiCSSX", SqlDbType.Decimal, "LoaiCSSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiCSSX", SqlDbType.NVarChar, "DiaChiCSSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiDiaChiCSSX", SqlDbType.Decimal, "LoaiDiaChiCSSX", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongSoHuu", SqlDbType.Decimal, "SoLuongSoHuu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongDiThue", SqlDbType.Decimal, "SoLuongDiThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongKhac", SqlDbType.Decimal, "SoLuongKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoLuong", SqlDbType.Decimal, "TongSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NangLucSanXuat", SqlDbType.NVarChar, "NangLucSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DienTichNhaXuong", SqlDbType.Decimal, "DienTichNhaXuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongCongNhan", SqlDbType.Decimal, "SoLuongCongNhan", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, "StorageAreasProduction_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiCSSX", SqlDbType.Decimal, "LoaiCSSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiCSSX", SqlDbType.NVarChar, "DiaChiCSSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiDiaChiCSSX", SqlDbType.Decimal, "LoaiDiaChiCSSX", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongSoHuu", SqlDbType.Decimal, "SoLuongSoHuu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongDiThue", SqlDbType.Decimal, "SoLuongDiThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongKhac", SqlDbType.Decimal, "SoLuongKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoLuong", SqlDbType.Decimal, "TongSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NangLucSanXuat", SqlDbType.NVarChar, "NangLucSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DienTichNhaXuong", SqlDbType.Decimal, "DienTichNhaXuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongCongNhan", SqlDbType.Decimal, "SoLuongCongNhan", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_ManufactureFactory Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_ManufactureFactory_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_ManufactureFactory> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_ManufactureFactory> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_ManufactureFactory> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_VNACCS_ManufactureFactory> SelectCollectionBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
            IDataReader reader = SelectReaderBy_StorageAreasProduction_ID(storageAreasProduction_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_ManufactureFactory_SelectBy_StorageAreasProduction_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, storageAreasProduction_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_ManufactureFactory_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_ManufactureFactory_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_ManufactureFactory_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_ManufactureFactory_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
			const string spName = "p_KDT_VNACCS_ManufactureFactory_SelectBy_StorageAreasProduction_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, storageAreasProduction_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_ManufactureFactory(long storageAreasProduction_ID, decimal loaiCSSX, string diaChiCSSX, decimal loaiDiaChiCSSX, decimal soLuongSoHuu, decimal soLuongDiThue, decimal soLuongKhac, decimal tongSoLuong, string nangLucSanXuat, decimal dienTichNhaXuong, decimal soLuongCongNhan)
		{
			KDT_VNACCS_ManufactureFactory entity = new KDT_VNACCS_ManufactureFactory();	
			entity.StorageAreasProduction_ID = storageAreasProduction_ID;
			entity.LoaiCSSX = loaiCSSX;
			entity.DiaChiCSSX = diaChiCSSX;
			entity.LoaiDiaChiCSSX = loaiDiaChiCSSX;
			entity.SoLuongSoHuu = soLuongSoHuu;
			entity.SoLuongDiThue = soLuongDiThue;
			entity.SoLuongKhac = soLuongKhac;
			entity.TongSoLuong = tongSoLuong;
			entity.NangLucSanXuat = nangLucSanXuat;
			entity.DienTichNhaXuong = dienTichNhaXuong;
			entity.SoLuongCongNhan = soLuongCongNhan;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_ManufactureFactory_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, StorageAreasProduction_ID);
			db.AddInParameter(dbCommand, "@LoaiCSSX", SqlDbType.Decimal, LoaiCSSX);
			db.AddInParameter(dbCommand, "@DiaChiCSSX", SqlDbType.NVarChar, DiaChiCSSX);
			db.AddInParameter(dbCommand, "@LoaiDiaChiCSSX", SqlDbType.Decimal, LoaiDiaChiCSSX);
			db.AddInParameter(dbCommand, "@SoLuongSoHuu", SqlDbType.Decimal, SoLuongSoHuu);
			db.AddInParameter(dbCommand, "@SoLuongDiThue", SqlDbType.Decimal, SoLuongDiThue);
			db.AddInParameter(dbCommand, "@SoLuongKhac", SqlDbType.Decimal, SoLuongKhac);
			db.AddInParameter(dbCommand, "@TongSoLuong", SqlDbType.Decimal, TongSoLuong);
			db.AddInParameter(dbCommand, "@NangLucSanXuat", SqlDbType.NVarChar, NangLucSanXuat);
			db.AddInParameter(dbCommand, "@DienTichNhaXuong", SqlDbType.Decimal, DienTichNhaXuong);
			db.AddInParameter(dbCommand, "@SoLuongCongNhan", SqlDbType.Decimal, SoLuongCongNhan);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_ManufactureFactory> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_ManufactureFactory item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_ManufactureFactory(long id, long storageAreasProduction_ID, decimal loaiCSSX, string diaChiCSSX, decimal loaiDiaChiCSSX, decimal soLuongSoHuu, decimal soLuongDiThue, decimal soLuongKhac, decimal tongSoLuong, string nangLucSanXuat, decimal dienTichNhaXuong, decimal soLuongCongNhan)
		{
			KDT_VNACCS_ManufactureFactory entity = new KDT_VNACCS_ManufactureFactory();			
			entity.ID = id;
			entity.StorageAreasProduction_ID = storageAreasProduction_ID;
			entity.LoaiCSSX = loaiCSSX;
			entity.DiaChiCSSX = diaChiCSSX;
			entity.LoaiDiaChiCSSX = loaiDiaChiCSSX;
			entity.SoLuongSoHuu = soLuongSoHuu;
			entity.SoLuongDiThue = soLuongDiThue;
			entity.SoLuongKhac = soLuongKhac;
			entity.TongSoLuong = tongSoLuong;
			entity.NangLucSanXuat = nangLucSanXuat;
			entity.DienTichNhaXuong = dienTichNhaXuong;
			entity.SoLuongCongNhan = soLuongCongNhan;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_ManufactureFactory_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, StorageAreasProduction_ID);
			db.AddInParameter(dbCommand, "@LoaiCSSX", SqlDbType.Decimal, LoaiCSSX);
			db.AddInParameter(dbCommand, "@DiaChiCSSX", SqlDbType.NVarChar, DiaChiCSSX);
			db.AddInParameter(dbCommand, "@LoaiDiaChiCSSX", SqlDbType.Decimal, LoaiDiaChiCSSX);
			db.AddInParameter(dbCommand, "@SoLuongSoHuu", SqlDbType.Decimal, SoLuongSoHuu);
			db.AddInParameter(dbCommand, "@SoLuongDiThue", SqlDbType.Decimal, SoLuongDiThue);
			db.AddInParameter(dbCommand, "@SoLuongKhac", SqlDbType.Decimal, SoLuongKhac);
			db.AddInParameter(dbCommand, "@TongSoLuong", SqlDbType.Decimal, TongSoLuong);
			db.AddInParameter(dbCommand, "@NangLucSanXuat", SqlDbType.NVarChar, NangLucSanXuat);
			db.AddInParameter(dbCommand, "@DienTichNhaXuong", SqlDbType.Decimal, DienTichNhaXuong);
			db.AddInParameter(dbCommand, "@SoLuongCongNhan", SqlDbType.Decimal, SoLuongCongNhan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_ManufactureFactory> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_ManufactureFactory item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_ManufactureFactory(long id, long storageAreasProduction_ID, decimal loaiCSSX, string diaChiCSSX, decimal loaiDiaChiCSSX, decimal soLuongSoHuu, decimal soLuongDiThue, decimal soLuongKhac, decimal tongSoLuong, string nangLucSanXuat, decimal dienTichNhaXuong, decimal soLuongCongNhan)
		{
			KDT_VNACCS_ManufactureFactory entity = new KDT_VNACCS_ManufactureFactory();			
			entity.ID = id;
			entity.StorageAreasProduction_ID = storageAreasProduction_ID;
			entity.LoaiCSSX = loaiCSSX;
			entity.DiaChiCSSX = diaChiCSSX;
			entity.LoaiDiaChiCSSX = loaiDiaChiCSSX;
			entity.SoLuongSoHuu = soLuongSoHuu;
			entity.SoLuongDiThue = soLuongDiThue;
			entity.SoLuongKhac = soLuongKhac;
			entity.TongSoLuong = tongSoLuong;
			entity.NangLucSanXuat = nangLucSanXuat;
			entity.DienTichNhaXuong = dienTichNhaXuong;
			entity.SoLuongCongNhan = soLuongCongNhan;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_ManufactureFactory_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, StorageAreasProduction_ID);
			db.AddInParameter(dbCommand, "@LoaiCSSX", SqlDbType.Decimal, LoaiCSSX);
			db.AddInParameter(dbCommand, "@DiaChiCSSX", SqlDbType.NVarChar, DiaChiCSSX);
			db.AddInParameter(dbCommand, "@LoaiDiaChiCSSX", SqlDbType.Decimal, LoaiDiaChiCSSX);
			db.AddInParameter(dbCommand, "@SoLuongSoHuu", SqlDbType.Decimal, SoLuongSoHuu);
			db.AddInParameter(dbCommand, "@SoLuongDiThue", SqlDbType.Decimal, SoLuongDiThue);
			db.AddInParameter(dbCommand, "@SoLuongKhac", SqlDbType.Decimal, SoLuongKhac);
			db.AddInParameter(dbCommand, "@TongSoLuong", SqlDbType.Decimal, TongSoLuong);
			db.AddInParameter(dbCommand, "@NangLucSanXuat", SqlDbType.NVarChar, NangLucSanXuat);
			db.AddInParameter(dbCommand, "@DienTichNhaXuong", SqlDbType.Decimal, DienTichNhaXuong);
			db.AddInParameter(dbCommand, "@SoLuongCongNhan", SqlDbType.Decimal, SoLuongCongNhan);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_ManufactureFactory> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_ManufactureFactory item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public int DeleteKDT_VNACCS_ManufactureFactory(long id)
		{
			KDT_VNACCS_ManufactureFactory entity = new KDT_VNACCS_ManufactureFactory();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_ManufactureFactory_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int DeleteBy_StorageAreasProduction_ID(long storageAreasProduction_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteBy_StorageAreasProduction_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@StorageAreasProduction_ID", SqlDbType.BigInt, storageAreasProduction_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_ManufactureFactory_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_ManufactureFactory> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_ManufactureFactory item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

        public void InsertUpdateFull()
        {
                try
                {
                    if (this.ID == 0)
                    {
                        this.ID = this.Insert();
                    }
                    else
                    {
                        this.Update();
                    }

                    foreach (KDT_VNACCS_Careery item in CareeryCollection)
                    {
                        item.ManufactureFactory_ID = this.ID;
                        item.InsertUpdateFull();
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                }

        }
        public void DeleteFull()
        {
            try
            {
                foreach (KDT_VNACCS_Careery item in CareeryCollection)
                {
                    item.ID = this.ID;
                    item.DeleteFull();
                }
                this.DeleteBy_StorageAreasProduction_ID(this.ID);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
            }
        }
	}	
}