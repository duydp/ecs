﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.KDT.GC
{
    public partial class CapNhatLenhSanXuatGCForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_GC_DinhMuc DM = new KDT_GC_DinhMuc();
        //public KDT_GC_LenhSanXuat lenhSanXuat = new KDT_GC_LenhSanXuat();
        //public KDT_GC_LenhSanXuat_SP lenhSanXuatSP = new KDT_GC_LenhSanXuat_SP();
        //public KDT_GC_LenhSanXuat_DinhMuc lenhSanXuatDM = new KDT_GC_LenhSanXuat_DinhMuc();
        public KDT_GC_DinhMucDangKy DMDK = new KDT_GC_DinhMucDangKy();
        public KDT_GC_DinhMucThucTeDangKy dinhMucThucTeDangKy = new KDT_GC_DinhMucThucTeDangKy();
        public KDT_GC_DinhMucThucTe_SP DinhMucThucTeSP = new KDT_GC_DinhMucThucTe_SP();
        public KDT_GC_DinhMucThucTe_DinhMuc DinhMucThucTeDinhMuc = new KDT_GC_DinhMucThucTe_DinhMuc();
        public KDT_LenhSanXuat lenhSanXuat = new KDT_LenhSanXuat();
        public KDT_LenhSanXuat_SP lenhSanXuatSP = new KDT_LenhSanXuat_SP();
        public List<Company.GC.BLL.GC.GC_DinhMuc> DinhMucCollection = new List<Company.GC.BLL.GC.GC_DinhMuc>();
        public HopDong HD = new HopDong();
        public bool isExits = false;
        public CapNhatLenhSanXuatGCForm()
        {
            InitializeComponent();
            cbHopDong.DataSource = HopDong.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "ID");
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
        }

        private void CapNhatLenhSanXuatGCForm_Load(object sender, EventArgs e)
        {

        }
        private void BindDataSP()
        {
            try
            {
                grListSP.DataSource = null;
                grListSP.Refetch();
                grListSP.DataSource = KDT_GC_DinhMuc.SelectDistinctCollectionDynamicSP(" Master_ID = " + (cbbLenhSanXuat.Value == null ? "0" : cbbLenhSanXuat.Value.ToString()) + "", "");
                grListSP.Refresh(); 
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void LoadData()
        {
            try
            {
                cbbLenhSanXuat.Text = String.Empty;
                cbbLenhSanXuat.DataSource = KDT_GC_DinhMuc.SelectDistinctCollectionDynamicLSX(" Master_ID IN (SELECT ID FROM dbo.t_KDT_GC_DinhMucDangKy WHERE ID_HopDong = " + Convert.ToInt64(cbHopDong.Value) + " AND TrangThaiXuLy IN (0,1)) AND MaDDSX NOT IN (SELECT LSX.SoLenhSanXuat FROM dbo.t_KDT_LenhSanXuat LSX INNER JOIN dbo.t_KDT_LenhSanXuat_SP SP ON SP.LenhSanXuat_ID = LSX.ID  WHERE  LSX.HopDong_ID = " + Convert.ToInt64(cbHopDong.Value) + ") AND MaDDSX IS NOT NULL", "");
                cbbLenhSanXuat.DisplayMember = "MaDDSX";
                cbbLenhSanXuat.ValueMember = "Master_ID";                       
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataNPL()
        {
            try
            {
                dgListNPL.DataSource = null;
                List<KDT_GC_DinhMuc> DMCollection = new List<KDT_GC_DinhMuc>();
                DMCollection = KDT_GC_DinhMuc.SelectCollectionDynamic("Master_ID = '" + cbbLenhSanXuat.Value.ToString() + "' AND MaSanPham ='" + DM.MaSanPham + "'", "");
                dgListNPL.Refetch();
                dgListNPL.DataSource = DMCollection;
                dgListNPL.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void grListSP_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grListSP.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DM = new KDT_GC_DinhMuc();
                        DM = (KDT_GC_DinhMuc)i.GetRow().DataRow;
                    }
                }
                BindDataNPL();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(grListSP.RootTable.Columns["MaSanPham"], ConditionOperator.Contains, txtMaSP.Text);
                grListSP.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgListNPL_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                    DM = new KDT_GC_DinhMuc();
                    DM = (KDT_GC_DinhMuc)e.Row.DataRow;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool Validate(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbbLenhSanXuat, errorProvider, "LỆNH SẢN XUẤT", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcTuNgay, errorProvider, "TỪ NGÀY", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(clcDenNgay, errorProvider, "ĐẾN NGÀY", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbTinhTrang, errorProvider, "TÌNH TRẠNG", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void GetValue()
        {
            try
            {
                if (lenhSanXuat==null)
                    lenhSanXuat = new KDT_LenhSanXuat();
                lenhSanXuat.SoLenhSanXuat = cbbLenhSanXuat.Value.ToString();
                lenhSanXuat.TuNgay = new DateTime(clcTuNgay.Value.Year, clcTuNgay.Value.Month, clcTuNgay.Value.Day, 00, 00, 00);
                lenhSanXuat.DenNgay = new DateTime(clcDenNgay.Value.Year, clcDenNgay.Value.Month, clcDenNgay.Value.Day, 23, 59, 59);
                lenhSanXuat.SoDonHang = txtPO.Text;
                lenhSanXuat.TinhTrang = Convert.ToInt32(cbbTinhTrang.SelectedValue);
                lenhSanXuat.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                lenhSanXuat.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                lenhSanXuat.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                lenhSanXuat.GhiChu = txtGhiChu.Text.ToString();
                lenhSanXuat.HopDong_ID = Convert.ToInt64(cbHopDong.Value.ToString());                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void Get()
        {
            try
            {
                if (DMDK.ID > 0)
                {
                    dinhMucThucTeDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    dinhMucThucTeDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    dinhMucThucTeDangKy.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                    dinhMucThucTeDangKy.LenhSanXuat_ID = lenhSanXuat.ID;
                    dinhMucThucTeDangKy.HopDong_ID = HD.ID;
                    dinhMucThucTeDangKy.SoTiepNhan = DMDK.SoTiepNhan;
                    dinhMucThucTeDangKy.NgayTiepNhan = DMDK.NgayTiepNhan;
                    dinhMucThucTeDangKy.TrangThaiXuLy = DMDK.TrangThaiXuLy;
                    dinhMucThucTeDangKy.GuidString = String.IsNullOrEmpty(DMDK.GUIDSTR) ? Guid.NewGuid().ToString().ToUpper() : DMDK.GUIDSTR;
                }
                else
                {
                    dinhMucThucTeDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    dinhMucThucTeDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    dinhMucThucTeDangKy.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                    dinhMucThucTeDangKy.LenhSanXuat_ID = lenhSanXuat.ID;
                    dinhMucThucTeDangKy.HopDong_ID = HD.ID;
                    dinhMucThucTeDangKy.NgayTiepNhan = DateTime.Now;
                    dinhMucThucTeDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!Validate(false))
                return;
            List<KDT_GC_DinhMuc> SPCollection = new List<KDT_GC_DinhMuc>();
            SPCollection = KDT_GC_DinhMuc.SelectDistinctCollectionDynamicSP(" Master_ID = " + cbbLenhSanXuat.Value.ToString() + "", "");
            if (SPCollection.Count==0)
            {
                ShowMessageTQDT("LỆNH SẢN XUẤT NÀY CHƯA CÓ DANH SÁCH SẢN PHẨM ĐỂ CẬP NHẬT . DOANH NGHIỆP HÃY KIỂM TRA LẠI",false);
                return;
            }
            List<KDT_LenhSanXuat> LenhSanXuatCollection = new List<KDT_LenhSanXuat>();
            LenhSanXuatCollection = KDT_LenhSanXuat.SelectCollectionDynamic("HopDong_ID = " + HD.ID + "", "");
            KDT_LenhSanXuat LenhSanXuatFind = new KDT_LenhSanXuat();
            LenhSanXuatFind = LenhSanXuatCollection.Find(item => item.SoLenhSanXuat == cbbLenhSanXuat.Text.ToString());
            if (!String.IsNullOrEmpty(LenhSanXuatFind.SoLenhSanXuat))
            {
                LenhSanXuatFind.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(LenhSanXuatFind.ID);
                foreach (KDT_GC_DinhMuc item in SPCollection)
                {
                    //Cập nhật lệnh sản xuất 
                    lenhSanXuatSP = new KDT_LenhSanXuat_SP();
                    lenhSanXuatSP.MaSanPham = item.MaSanPham;
                    lenhSanXuatSP.TenSanPham = item.TenSanPham;
                    foreach (Company.GC.BLL.KDT.GC.SanPham sp in HD.SPCollection)
                    {
                        isExits = false;
                        if (lenhSanXuatSP.MaSanPham == sp.Ma)
                        {
                            lenhSanXuatSP.DVT_ID = sp.DVT_ID;
                            lenhSanXuatSP.MaHS = sp.MaHS;
                            isExits = true;
                            break;
                        }
                    }
                    if (!isExits)
                    {
                        foreach (GC_SanPham sp in GC_SanPham.SelectCollectionBy_HopDong_ID(Convert.ToInt64(cbHopDong.Value)))
                        {
                            isExits = false;
                            if (lenhSanXuatSP.MaSanPham == sp.Ma)
                            {
                                lenhSanXuatSP.DVT_ID = sp.DVT_ID;
                                lenhSanXuatSP.MaHS = sp.MaHS;
                                isExits = true;
                                break;
                            }
                        }
                    }
                    LenhSanXuatFind.SPCollection.Add(lenhSanXuatSP);
                    //Cập nhật vào định mức thực tế theo từng giai đoạn
                    DMDK = new KDT_GC_DinhMucDangKy();
                    DMDK = KDT_GC_DinhMucDangKy.Load(item.Master_ID);

                    DinhMucThucTeSP = new KDT_GC_DinhMucThucTe_SP();
                    DinhMucThucTeSP.MaSanPham = lenhSanXuatSP.MaSanPham;
                    DinhMucThucTeSP.TenSanPham = lenhSanXuatSP.TenSanPham;
                    DinhMucThucTeSP.DVT_ID = lenhSanXuatSP.DVT_ID;
                    DinhMucThucTeSP.MaHS = lenhSanXuatSP.MaHS;

                    List<KDT_GC_DinhMuc> DMCollection = new List<KDT_GC_DinhMuc>();
                    DMCollection = KDT_GC_DinhMuc.SelectCollectionDynamic("Master_ID = '" + cbbLenhSanXuat.Value.ToString() + "' AND MaSanPham ='" + item.MaSanPham + "'", "");
                    foreach (KDT_GC_DinhMuc dinhmuc in DMCollection)
                    {
                        DinhMucThucTeDinhMuc = new KDT_GC_DinhMucThucTe_DinhMuc();
                        DinhMucThucTeDinhMuc.MaSanPham = dinhmuc.MaSanPham;
                        DinhMucThucTeDinhMuc.TenSanPham = dinhmuc.TenSanPham;
                        DinhMucThucTeDinhMuc.DVT_SP = lenhSanXuatSP.DVT_ID;
                        DinhMucThucTeDinhMuc.MaNPL = dinhmuc.MaNguyenPhuLieu;
                        DinhMucThucTeDinhMuc.TenNPL = dinhmuc.TenNPL;
                        foreach (Company.GC.BLL.KDT.GC.NguyenPhuLieu npl in HD.NPLCollection)
                        {
                            isExits = false;
                            if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                            {
                                DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            foreach (GC_NguyenPhuLieu npl in GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(Convert.ToInt64(cbHopDong.Value)))
                            {
                                isExits = false;
                                if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                                {
                                    DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                    DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                    isExits = true;
                                    break;
                                }
                            }
                        }
                        DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                        if (dinhmuc.TyLeHaoHut > 0)
                        {
                            DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung + dinhmuc.DinhMucSuDung * dinhmuc.TyLeHaoHut / 100;
                            DinhMucThucTeDinhMuc.TyLeHaoHut = 0;
                        }
                        DinhMucThucTeSP.DMCollection.Add(DinhMucThucTeDinhMuc);
                    }
                    dinhMucThucTeDangKy.SPCollection.Add(DinhMucThucTeSP);
                }
                LenhSanXuatFind.InsertUpdateFull();
                Get();
                dinhMucThucTeDangKy.InsertUpdateFull();
                DinhMucCollection = Company.GC.BLL.GC.GC_DinhMuc.SelectCollectionBy_HopDong_ID(HD.ID);
                foreach (Company.GC.BLL.GC.GC_DinhMuc item in DinhMucCollection)
                {
                    foreach (KDT_LenhSanXuat_SP sp in LenhSanXuatFind.SPCollection)
                    {
                        if (sp.MaSanPham == item.MaSanPham)
                        {
                            if (item.LenhSanXuat_ID == 0)
                            {
                                item.LenhSanXuat_ID = sp.ID;
                                item.UpdateLenhSanXuat();
                            }
                        }
                    }
                }
            }
            else
            {
                foreach (KDT_LenhSanXuat item in LenhSanXuatCollection)
                {
                    item.SPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(item.ID);
                    foreach (KDT_LenhSanXuat_SP ite in item.SPCollection)
                    {
                        foreach (KDT_GC_DinhMuc dm in SPCollection)
                        {
                            if (ite.MaSanPham == dm.MaSanPham)
                            {
                                if (clcTuNgay.Value >= item.TuNgay && clcTuNgay.Value <= item.DenNgay || clcDenNgay.Value >= item.TuNgay && clcDenNgay.Value <= item.DenNgay || clcTuNgay.Value <= item.TuNgay && clcDenNgay.Value >= item.DenNgay)
                                {
                                    ShowMessageTQDT("Thông báo từ Hệ thống", String.Format("LỆNH SẢN XUẤT CỦA SP NÀY CÓ THÔNG TIN TỪ NGÀY VÀ ĐẾN NGÀY NẰM TRONG KHOẢNG TỪ NGÀY VÀ ĐẾN NGÀY CỦA LỆNH SẢN XUẤT KHÁC CỦA SP NÀY TRƯỚC ĐÓ VỚI THÔNG TIN  \r\n ID - SẢN PHẨM - LỆNH SẢN XUẤT TRƯỚC - TỪ NGÀY - ĐẾN NGÀY - SẢN PHẨM - LỆNH SẢN XUẤT HIỆN TẠI - TỪ NGÀY - ĐẾN NGÀY  \r\n - {0} - {1} - {2} - {3} - {4} - {5} - {6} - {7} - {8} \n ", item.ID, ite.MaSanPham, item.SoLenhSanXuat, item.TuNgay.ToString("dd/MM/yyyy"), item.DenNgay.ToString("dd/MM/yyyy"), dm.MaSanPham, cbbLenhSanXuat.Value.ToString(), clcTuNgay.Value.ToString("dd/MM/yyyy"), clcDenNgay.Value.ToString("dd/MM/yyyy")), false);
                                    return;
                                }
                            }
                        }
                    }

                }
                foreach (KDT_GC_DinhMuc item in SPCollection)
                {
                    //Cập nhật lệnh sản xuất 
                    lenhSanXuatSP = new KDT_LenhSanXuat_SP();
                    lenhSanXuatSP.MaSanPham = item.MaSanPham;
                    lenhSanXuatSP.TenSanPham = item.TenSanPham;
                    foreach (Company.GC.BLL.KDT.GC.SanPham sp in HD.SPCollection)
                    {
                        isExits = false;
                        if (lenhSanXuatSP.MaSanPham == sp.Ma)
                        {
                            lenhSanXuatSP.DVT_ID = sp.DVT_ID;
                            lenhSanXuatSP.MaHS = sp.MaHS;
                            isExits = true;
                            break;
                        }
                    }
                    if (!isExits)
                    {
                        foreach (GC_SanPham sp in GC_SanPham.SelectCollectionBy_HopDong_ID(Convert.ToInt64(cbHopDong.Value)))
                        {
                            isExits = false;
                            if (lenhSanXuatSP.MaSanPham == sp.Ma)
                            {
                                lenhSanXuatSP.DVT_ID = sp.DVT_ID;
                                lenhSanXuatSP.MaHS = sp.MaHS;
                                isExits = true;
                                break;
                            }
                        }
                    }
                    lenhSanXuat.SPCollection.Add(lenhSanXuatSP);
                    if (DMDK.ID == 0)
                    {
                        //Cập nhật vào định mức thực tế theo từng giai đoạn
                        DMDK = new KDT_GC_DinhMucDangKy();
                        DMDK = KDT_GC_DinhMucDangKy.Load(item.Master_ID);
                    }
                    DinhMucThucTeSP = new KDT_GC_DinhMucThucTe_SP();
                    DinhMucThucTeSP.MaSanPham = lenhSanXuatSP.MaSanPham;
                    DinhMucThucTeSP.TenSanPham = lenhSanXuatSP.TenSanPham;
                    DinhMucThucTeSP.DVT_ID = lenhSanXuatSP.DVT_ID;
                    DinhMucThucTeSP.MaHS = lenhSanXuatSP.MaHS;

                    List<KDT_GC_DinhMuc> DMCollection = new List<KDT_GC_DinhMuc>();
                    DMCollection = KDT_GC_DinhMuc.SelectCollectionDynamic("Master_ID = '" + cbbLenhSanXuat.Value.ToString() + "' AND MaSanPham ='" + item.MaSanPham + "'", "");
                    foreach (KDT_GC_DinhMuc dinhmuc in DMCollection)
                    {
                        DinhMucThucTeDinhMuc = new KDT_GC_DinhMucThucTe_DinhMuc();
                        DinhMucThucTeDinhMuc.MaSanPham = dinhmuc.MaSanPham;
                        DinhMucThucTeDinhMuc.TenSanPham = dinhmuc.TenSanPham;
                        DinhMucThucTeDinhMuc.DVT_SP = lenhSanXuatSP.DVT_ID;
                        DinhMucThucTeDinhMuc.MaNPL = dinhmuc.MaNguyenPhuLieu;
                        DinhMucThucTeDinhMuc.TenNPL = dinhmuc.TenNPL;
                        foreach (Company.GC.BLL.KDT.GC.NguyenPhuLieu npl in HD.NPLCollection)
                        {
                            isExits = false;
                            if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                            {
                                DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            foreach (GC_NguyenPhuLieu npl in GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(Convert.ToInt64(cbHopDong.Value)))
                            {
                                isExits = false;
                                if (DinhMucThucTeDinhMuc.MaNPL == npl.Ma)
                                {
                                    DinhMucThucTeDinhMuc.MaHS = npl.MaHS;
                                    DinhMucThucTeDinhMuc.DVT_NPL = npl.DVT_ID;
                                    isExits = true;
                                    break;
                                }
                            }
                        }
                        DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung;
                        if (dinhmuc.TyLeHaoHut > 0)
                        {
                            DinhMucThucTeDinhMuc.DinhMucSuDung = dinhmuc.DinhMucSuDung + dinhmuc.DinhMucSuDung * dinhmuc.TyLeHaoHut / 100;
                            DinhMucThucTeDinhMuc.TyLeHaoHut = 0;
                        }
                        DinhMucThucTeSP.DMCollection.Add(DinhMucThucTeDinhMuc);
                    }
                    dinhMucThucTeDangKy.SPCollection.Add(DinhMucThucTeSP);
                }
                GetValue();
                lenhSanXuat.InsertUpdateFull();
                Get();
                dinhMucThucTeDangKy.InsertUpdateFull();
                DinhMucCollection = Company.GC.BLL.GC.GC_DinhMuc.SelectCollectionBy_HopDong_ID(HD.ID);
                foreach (Company.GC.BLL.GC.GC_DinhMuc item in DinhMucCollection)
                {
                    foreach (KDT_LenhSanXuat_SP sp in lenhSanXuat.SPCollection)
                    {
                        if (sp.MaSanPham == item.MaSanPham)
                        {
                            if (item.LenhSanXuat_ID == 0)
                            {
                                item.LenhSanXuat_ID = sp.ID;
                                item.UpdateLenhSanXuat();
                            }
                        }
                    }
                }
            }            
            ShowMessageTQDT("Thông báo từ Hệ thống","Cập nhật thành công",false);
            cbbLenhSanXuat.Text = String.Empty;
            txtPO.Text = String.Empty;
            txtGhiChu.Text = String.Empty;
            DMDK = new KDT_GC_DinhMucDangKy();
            dinhMucThucTeDangKy = new KDT_GC_DinhMucThucTeDangKy();
            lenhSanXuat = new KDT_LenhSanXuat();
            LoadData();
            cbbLenhSanXuat_ValueChanged(null,null);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                LoadData();
                HD = new HopDong();
                HD.ID = Convert.ToInt64(cbHopDong.Value);
                HD = HopDong.Load(HD.ID);
                HD.LoadCollection();
                txtSoHopDong.Text = HD.SoHopDong.ToString();
                clcNgayHĐ.Value = HD.NgayKy;
                clcNgayHH.Value = HD.NgayGiaHan.Year == 1900 || HD.NgayGiaHan.Year == 1 ? HD.NgayHetHan : HD.NgayGiaHan;
                clcTuNgay.Value = HD.NgayKy;
                clcDenNgay.Value = HD.NgayGiaHan.Year == 1900 || HD.NgayGiaHan.Year == 1 ? HD.NgayHetHan : HD.NgayGiaHan;
                cbbLenhSanXuat_ValueChanged(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLenhSanXuat_ValueChanged(object sender, EventArgs e)
        {
            BindDataSP();
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            AutoUpdateLenhSanXuatForm f = new AutoUpdateLenhSanXuatForm();
            f.Show(this);
        }
    }
}
