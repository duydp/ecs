
ALTER TABLE t_SXXK_NguyenPhuLieu
DROP CONSTRAINT  [PK_t_SXXK_NguyenPhuLieu]
GO
ALTER TABLE t_SXXK_NguyenPhuLieu
ALTER COLUMN Ma NVARCHAR(50) NOT NULL

GO
ALTER TABLE t_SXXK_NguyenPhuLieu
ALTER COLUMN Ten NVARCHAR(254) NOT NULL

GO
ALTER TABLE [dbo].[t_SXXK_NguyenPhuLieu] ADD CONSTRAINT [PK_t_SXXK_NguyenPhuLieu] PRIMARY KEY CLUSTERED ([MaHaiQuan], [MaDoanhNghiep], [Ma]) ON [PRIMARY]
GO
ALTER TABLE t_KDT_SXXK_NguyenPhuLieu
ALTER COLUMN Ma NVARCHAR(50) NOT NULL

GO
ALTER TABLE t_KDT_SXXK_NguyenPhuLieu
ALTER COLUMN Ten NVARCHAR(254) NOT NULL

GO
ALTER TABLE t_SXXK_SanPham
DROP CONSTRAINT  [PK_t_SXXK_SanPham]
GO
ALTER TABLE t_SXXK_SanPham
ALTER COLUMN Ma NVARCHAR(50) NOT NULL
GO
ALTER TABLE t_SXXK_SanPham
ALTER COLUMN Ten NVARCHAR(254) NOT NULL
GO
ALTER TABLE [dbo].[t_SXXK_SanPham] ADD CONSTRAINT [PK_t_SXXK_SanPham] PRIMARY KEY CLUSTERED ([MaDoanhNghiep], [Ma], [MaHaiQuan]) ON [PRIMARY]
GO
ALTER TABLE t_KDT_SXXK_SanPham
ALTER COLUMN Ma NVARCHAR(50) NOT NULL

GO
ALTER TABLE t_KDT_SXXK_SanPham
ALTER COLUMN Ten NVARCHAR(254) NOT NULL
GO
ALTER TABLE t_SXXK_DinhMuc
DROP CONSTRAINT [PK_t_SXXK_DinhMuc]
GO
ALTER TABLE t_SXXK_DinhMuc
DROP CONSTRAINT [FK_t_SXXK_DinhMuc_t_SXXK_ThongTinDinhMuc]
GO
ALTER TABLE t_SXXK_ThongTinDinhMuc
DROP CONSTRAINT  [PK_t_SXXK_ThongTinDinhMuc]
GO
ALTER TABLE t_SXXK_ThongTinDinhMuc
ALTER COLUMN MaSanPham NVARCHAR(50) NOT NULL
GO
ALTER TABLE [dbo].[t_SXXK_ThongTinDinhMuc] ADD CONSTRAINT [PK_t_SXXK_ThongTinDinhMuc] PRIMARY KEY CLUSTERED ([MaSanPham], [MaDoanhNghiep], [MaHaiQuan]) ON [PRIMARY]
GO
ALTER TABLE t_SXXK_DinhMuc
ALTER COLUMN MaSanPham NVARCHAR(50) NOT NULL
GO
ALTER TABLE t_SXXK_DinhMuc
ALTER COLUMN MaNguyenPhuLieu NVARCHAR(50) NOT NULL
GO
ALTER TABLE [dbo].[t_SXXK_DinhMuc] ADD CONSTRAINT [PK_t_SXXK_DinhMuc] PRIMARY KEY CLUSTERED ([MaHaiQuan], [MaDoanhNghiep], [MaSanPham], [MaNguyenPhuLieu]) ON [PRIMARY]
GO
ALTER TABLE [dbo].[t_SXXK_DinhMuc] ADD CONSTRAINT [FK_t_SXXK_DinhMuc_t_SXXK_ThongTinDinhMuc] FOREIGN KEY ([MaSanPham], [MaDoanhNghiep], [MaHaiQuan]) REFERENCES [dbo].[t_SXXK_ThongTinDinhMuc] ([MaSanPham], [MaDoanhNghiep], [MaHaiQuan]) ON DELETE CASCADE ON UPDATE CASCADE
GO

ALTER TABLE t_KDT_SXXK_DinhMuc
DROP CONSTRAINT [FK_t_KDT_SXXK_DinhMuc_t_KDT_SXXK_DinhMucDangKy]
GO
ALTER TABLE t_KDT_SXXK_DinhMuc
ALTER COLUMN MaSanPham NVARCHAR(50) NOT NULL
GO
ALTER TABLE t_KDT_SXXK_DinhMuc
ALTER COLUMN MaNguyenPhuLieu NVARCHAR(50) NOT NULL
GO
ALTER TABLE [dbo].[t_KDT_SXXK_DinhMuc] ADD CONSTRAINT [FK_t_KDT_SXXK_DinhMuc_t_KDT_SXXK_DinhMucDangKy] FOREIGN KEY ([Master_ID]) REFERENCES [dbo].[t_KDT_SXXK_DinhMucDangKy] ([ID]) ON DELETE CASCADE ON UPDATE CASCADE
GO

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '38.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('38.1',GETDATE(), N'CẬP NHẬT THAY ĐỔI KIỂU DỮ LIỆU MỘT SỐ TRƯỜNG CỦA NGUYÊN PHỤ LIỆU - SẢN PHẨM -ĐỊNH MỨC')
END