﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.Maper;
using System.Threading;
using Company.KDT.SHARE.VNACCS.LogMessages;

namespace Company.Interface
{
    public partial class VNACC_HoaDonForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_HoaDon HoaDon = new KDT_VNACC_HoaDon();
        private DateTime dayMin = new DateTime(1900, 1, 1);
        public VNACC_HoaDonForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EInvoice.IVA.ToString());
            BoSungNghiepVu();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdThemHang":
                    this.Show_ThemHang();
                    break;
                case "cmdPhanLoaiHoaDon":
                    this.Show_PhanLoaiChungTu();
                    break;
                case "cmdLuu":
                    this.SaveHoaDon();
                    break;
                case "cmdIVA":
                    this.SendVnaccs(false);
                    break;
                case "cmdIVA01":
                    this.SendVnaccs(true);
                    break;
                case "cmdIIV":
                    this.GetIIV();
                    break;
                case "cmdLayPhanHoi":
                    TuDongCapNhatThongTin();
                    break;
            }

        }

        private void Show_ThemHang()
        {
            VNACC_HaoDon_HangHoaForm f = new VNACC_HaoDon_HangHoaForm();
            f.hoaDon = HoaDon;
            f.ShowDialog();

        }
        private void Show_PhanLoaiChungTu()
        {
            VNACC_HoaDon_PhanLoaiForm f = new VNACC_HoaDon_PhanLoaiForm();
            f.hoaDon = HoaDon;
            f.ShowDialog();
        }
        private void SaveHoaDon()
        {
            try
            {
                if (HoaDon.HangCollection.Count == 0 || HoaDon.HangCollection == null)
                {
                    ShowMessage("Chưa nhập thông tin hàng", false);
                }
                else
                {
                    GetHoaDon();
                    //lưu vào csdl
                    HoaDon.InsertUpdateFull();
                    //string boundary = string.Empty;
                    ////string mess = HelperVNACCS.GetMIMEMessages(HelperVNACCS.BuildEdiMessages<IVA>(VNACCMaper.IVAMaper(HoaDon)).ToString(),ref boundary);

                    ////string mess =HelperVNACCS.BuildEdiMessages<IVA>(VNACCMaper.IVAMaper(HoaDon)).ToString();

                    //IVA iva1 = new IVA(); //VNACCMaper.IVAMaper(HoaDon);
                    //string mess1 = iva1.BuildEdiMessages<IVA>(new StringBuilder(),true,Application.StartupPath + "\\Config","IVA").ToString();
                    //HelperVNACCS.SaveFileEdi(Application.StartupPath + "\\log",new StringBuilder().Append(mess1),"IVA");
                    //Thread.Sleep(1000);
                    ////IVA iva2 = new IVA();
                    ////iva2.GetObject<IVA>(mess1, true, Application.StartupPath + "\\Config", "IVA",false);

                    ////string mess2 = iva2.BuildEdiMessages<IVA>(new StringBuilder(), true, Application.StartupPath + "\\Config", "IVA").ToString();
                    ////HelperVNACCS.SaveFileEdi(Application.StartupPath + "\\log", new StringBuilder().Append(mess2), "IVA");
                    ////SendVNACCFrm f = new SendVNACCFrm();
                    ////f.MessagesSend = mess;
                    ////f.boundary = boundary;
                    ////f.ShowDialog();

                    ShowMessage("Lưu thành công", false);
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                ShowMessage("Lưu không thành công", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
            }
        }
        private void GetHoaDon()
        {

            this.Cursor = Cursors.WaitCursor;
            HoaDon.SoTiepNhan = Convert.ToDecimal(txtSoTiepNhan.Text);
            // HoaDon.NgayTiepNhan = clcNgayTiepNhan.Value;
            HoaDon.PhanLoaiXuatNhap = cbbPhanLoaiXuatNhap.SelectedValue.ToString();
            HoaDon.MaDaiLyHQ = txtMaDaiLy_HQ.Text;
            HoaDon.SoHoaDon = txtSoHoaDon.Text;
            HoaDon.NgayLapHoaDon = (clcNgayLapHD.Text == "") ? dayMin : clcNgayLapHD.Value;
            HoaDon.DiaDiemLapHoaDon = txtDiaDiemLap_HD.Text;
            HoaDon.PhuongThucThanhToan = ctrPTTT.Code;
            HoaDon.MaNguoiXNK = txtMa_XNK.Text;
            HoaDon.TenNguoiXNK = txtTen_XNK.Text;
            HoaDon.MaBuuChinh_NguoiXNK = txtMaBuuChinh_XNK.Text;
            HoaDon.DiaChi_NguoiXNK = txtDiaChi_XNK.Text;
            HoaDon.SoDienThoai_NguoiXNK = txtDienThoai_XNK.Text;
            HoaDon.NguoiLapHoaDon = txtNguoiLap_HD.Text;
            HoaDon.MaNguoiGuiNhan = txtMa_GuiNhan.Text;
            HoaDon.TenNguoiGuiNhan = txtten_GuiNhan.Text;
            HoaDon.MaBuuChinhNguoiGuiNhan = txtMaBuuChinh_GuiNhan.Text;
            HoaDon.DiaChiNguoiNhanGui1 = txtDiaChi_1.Text;
            HoaDon.DiaChiNguoiNhanGui2 = txtDaiChi_2.Text;
            HoaDon.DiaChiNguoiNhanGui3 = txtTinh_Thanh.Text;
            HoaDon.DiaChiNguoiNhanGui4 = txtQuocGia.Text;
            HoaDon.SoDienThoaiNhanGui = txtDienThoai_GuiNhan.Text;
            HoaDon.NuocSoTaiNhanGui = txtMaNuoc.Text;
            HoaDon.MaKyHieu = txtMaKyHieu.Text;
            HoaDon.PhanLoaiVanChuyen = ctrLoaiVanChuyen.Code;
            HoaDon.TenPTVC = txtTen_PTVC.Text;
            HoaDon.SoHieuChuyenDi = txtSoHieuChuyenDi.Text;
            HoaDon.MaDiaDiemXepHang = ctrDiaDiemXemHang.Ma.ToString();
            HoaDon.TenDiaDiemXepHang = ctrDiaDiemXemHang.Ten.ToString();
            HoaDon.ThoiKyXephang = (clcNgayXepHang.Text == "") ? dayMin : clcNgayXepHang.Value;
            HoaDon.MaDiaDiemDoHang = ctrDiaDiemDoHang.Ma.ToString();
            HoaDon.TenDiaDiemDoHang = ctrDiaDiemDoHang.Ten.ToString();
            HoaDon.MaDiaDiemTrungChuyen = ctrDiaDiemTrungChuyen.Ma.ToString();
            HoaDon.TenDiaDiemTrungChuyen = ctrDiaDiemTrungChuyen.Ten.ToString();
            HoaDon.TrongLuongGross = Convert.ToDecimal(txtTrongLuong_Gross.Value);
            HoaDon.MaDVT_TrongLuongGross = (cbbDVT_TrongLuongGross.SelectedValue != null) ? cbbDVT_TrongLuongGross.SelectedValue.ToString() : string.Empty;
            HoaDon.TrongLuongThuan = Convert.ToDecimal(txtTrongLuongThuan.Value);
            HoaDon.MaDVT_TrongLuongThuan = (cbbDVT_TrongLuongThuan.SelectedValue != null) ? cbbDVT_TrongLuongThuan.SelectedValue.ToString() : string.Empty;
            HoaDon.TongTheTich = Convert.ToDecimal(txtTongTheTich.Text);
            HoaDon.MaDVT_TheTich = (cbbDVT_TheTich.SelectedValue != null) ? cbbDVT_TheTich.SelectedValue.ToString() : string.Empty;
            HoaDon.TongSoKienHang = Convert.ToDecimal(txtTongSoKien.Text);
            HoaDon.MaDVT_KienHang = (cbbDVT_KienHang.SelectedValue != null) ? cbbDVT_KienHang.SelectedValue.ToString() : string.Empty;
            HoaDon.GhiChuChuHang = txtGhiChu_ChuHang.Text;
            HoaDon.SoPL = txtSoPL.Text;
            HoaDon.NganHangLC = txtNganHang_PL.Text;
            HoaDon.TriGiaFOB = Convert.ToDecimal(txtTriGiaFOB.Text);
            HoaDon.MaTT_FOB = ctrMaTT_TriGiaFOB.Code;
            HoaDon.SoTienFOB = Convert.ToDecimal(txtSoTienDieuChinh_FOB.Text);
            HoaDon.MaTT_TienFOB = ctrMaTT_SoTien_FOB.Code;
            HoaDon.PhiVanChuyen = Convert.ToDecimal(txtPhiVanChuyen.Text);
            HoaDon.MaTT_PhiVC = ctrMaTT_PhiVanChuyen.Code;
            HoaDon.NoiThanhToanPhiVC = txtNoiThanhToan_PVC.Text;
            HoaDon.ChiPhiXepHang1 = Convert.ToDecimal(txtChiPhiXepHang1.Text);
            HoaDon.MaTT_ChiPhiXepHang1 = ctrMaTT_PhiXepHang1.Code;
            HoaDon.LoaiChiPhiXepHang1 = txtLoaiPhiXepHang1.Text;
            HoaDon.ChiPhiXepHang2 = Convert.ToDecimal(txtChiPhiXepHang2.Text);
            HoaDon.MaTT_ChiPhiXepHang2 = ctrMaTT_PhiXepHang2.Code;
            HoaDon.LoaiChiPhiXepHang2 = txtLoaiPhiXepHang2.Text;
            HoaDon.PhiVC_DuongBo = Convert.ToDecimal(txtPhiVC_NoiDia.Text);
            HoaDon.MaTT_PhiVC_DuongBo = ctrMaTT_PVC_NoiDia.Code;
            HoaDon.PhiBaoHiem = Convert.ToDecimal(txtPhiBaoHiem.Text);
            HoaDon.MaTT_PhiBaoHiem = ctrMaTT_PhiBaoHiem.Code;
            HoaDon.SoTienPhiBaoHiem = Convert.ToDecimal(txtSoTienDieuChinh_PhiBH.Text);
            HoaDon.MaTT_TienPhiBaoHiem = ctrMaTT_SoTien_PhiBH.Code;
            HoaDon.SoTienKhauTru = Convert.ToDecimal(txtTienKhauTru.Text);
            HoaDon.MaTT_TienKhauTru = ctrMaTT_KhauTru.Code;
            HoaDon.LoaiKhauTru = txtLoaiKhauTru.Text;
            HoaDon.SoTienKhac = Convert.ToDecimal(txtDieuChinhKhac.Text);
            HoaDon.MaTT_TienKhac = ctrMaTT_DieuChinhKhac.Code;
            HoaDon.LoaiSoTienKhac = txtLoaiDieuChinhKhac.Text;
            HoaDon.TongTriGiaHoaDon = Convert.ToDecimal(txtTongTriGia_HD.Text);
            HoaDon.MaTT_TongTriGia = ctrMaTT_TongTriGia_HD.Code;
            HoaDon.DieuKienGiaHoaDon = txtDieuKienGia_HD.Text;
            HoaDon.DiaDiemGiaoHang = txtDiaDiemGiaoHang.Text;
            HoaDon.GhiChuDacBiet = txtGhiChu.Text;
            HoaDon.TongSoDongHang = Convert.ToDecimal(txtTongDongHang.Text);
        }
        private void SetHoaDon()
        {
            txtSoTiepNhan.Text = Convert.ToString(HoaDon.SoTiepNhan);
            if (HoaDon.NgayTiepNhan <= dayMin)
                clcNgayTiepNhan.Text = "";
            else
                clcNgayTiepNhan.Value = HoaDon.NgayTiepNhan;
            cbbPhanLoaiXuatNhap.SelectedValue = HoaDon.PhanLoaiXuatNhap;
            txtMaDaiLy_HQ.Text = HoaDon.MaDaiLyHQ;
            txtSoHoaDon.Text = HoaDon.SoHoaDon;
            clcNgayLapHD.Value = HoaDon.NgayLapHoaDon;
            txtDiaDiemLap_HD.Text = HoaDon.DiaDiemLapHoaDon;
            ctrPTTT.Code = HoaDon.PhuongThucThanhToan;
            txtMa_XNK.Text = HoaDon.MaNguoiXNK;
            txtTen_XNK.Text = HoaDon.TenNguoiXNK;
            txtMaBuuChinh_XNK.Text = HoaDon.MaBuuChinh_NguoiXNK;
            txtDiaChi_XNK.Text = HoaDon.DiaChi_NguoiXNK;
            txtDienThoai_XNK.Text = HoaDon.SoDienThoai_NguoiXNK;
            txtNguoiLap_HD.Text = HoaDon.NguoiLapHoaDon;
            txtMa_GuiNhan.Text = HoaDon.MaNguoiGuiNhan;
            txtten_GuiNhan.Text = HoaDon.TenNguoiGuiNhan;
            txtMaBuuChinh_GuiNhan.Text = HoaDon.MaBuuChinhNguoiGuiNhan;
            txtDiaChi_1.Text = HoaDon.DiaChiNguoiNhanGui1;
            txtDaiChi_2.Text = HoaDon.DiaChiNguoiNhanGui2;
            txtTinh_Thanh.Text = HoaDon.DiaChiNguoiNhanGui3;
            txtQuocGia.Text = HoaDon.DiaChiNguoiNhanGui4;
            txtDienThoai_GuiNhan.Text = HoaDon.SoDienThoaiNhanGui;
            txtMaNuoc.Text = HoaDon.NuocSoTaiNhanGui;
            txtMaKyHieu.Text = HoaDon.MaKyHieu;
            ctrLoaiVanChuyen.Code = HoaDon.PhanLoaiVanChuyen;
            txtTen_PTVC.Text = HoaDon.TenPTVC;
            txtSoHieuChuyenDi.Text = HoaDon.SoHieuChuyenDi;
            ctrDiaDiemXemHang.Ma = HoaDon.MaDiaDiemXepHang;
            ctrDiaDiemXemHang.Ten = HoaDon.TenDiaDiemXepHang;
            if (HoaDon.ThoiKyXephang <= dayMin)
                clcNgayXepHang.Text = "";
            else
                clcNgayXepHang.Value = HoaDon.ThoiKyXephang;
            ctrDiaDiemDoHang.Ma = HoaDon.MaDiaDiemDoHang;
            ctrDiaDiemDoHang.Ten = HoaDon.TenDiaDiemDoHang;
            ctrDiaDiemTrungChuyen.Ma = HoaDon.MaDiaDiemTrungChuyen;
            ctrDiaDiemTrungChuyen.Ten = HoaDon.TenDiaDiemTrungChuyen;
            txtTrongLuong_Gross.Text = HoaDon.TrongLuongGross.ToString();
            cbbDVT_TrongLuongGross.SelectedValue = HoaDon.MaDVT_TrongLuongGross;
            txtTrongLuongThuan.Text = HoaDon.TrongLuongThuan.ToString();
            cbbDVT_TrongLuongThuan.SelectedValue = HoaDon.MaDVT_TrongLuongThuan;
            txtTongTheTich.Text = HoaDon.TongTheTich.ToString();
            cbbDVT_TheTich.SelectedValue = HoaDon.MaDVT_TheTich;
            txtTongSoKien.Text = HoaDon.TongSoKienHang.ToString();
            cbbDVT_KienHang.SelectedValue = HoaDon.MaDVT_KienHang;
            txtGhiChu_ChuHang.Text = HoaDon.GhiChuChuHang;
            txtSoPL.Text = HoaDon.SoPL;
            txtNganHang_PL.Text = HoaDon.NganHangLC;
            txtTriGiaFOB.Text = HoaDon.TriGiaFOB.ToString();
            ctrMaTT_TriGiaFOB.Code = HoaDon.MaTT_FOB;
            txtSoTienDieuChinh_FOB.Text = HoaDon.SoTienFOB.ToString();
            ctrMaTT_SoTien_FOB.Code = HoaDon.MaTT_TienFOB;
            txtPhiVanChuyen.Text = HoaDon.PhiVanChuyen.ToString();
            ctrMaTT_PhiVanChuyen.Code = HoaDon.MaTT_PhiVC;
            txtNoiThanhToan_PVC.Text = HoaDon.NoiThanhToanPhiVC;
            txtChiPhiXepHang1.Text = HoaDon.ChiPhiXepHang1.ToString();
            ctrMaTT_PhiXepHang1.Code = HoaDon.MaTT_ChiPhiXepHang1;
            txtLoaiPhiXepHang1.Text = HoaDon.LoaiChiPhiXepHang1;
            txtChiPhiXepHang2.Text = HoaDon.ChiPhiXepHang2.ToString();
            ctrMaTT_PhiXepHang2.Code = HoaDon.MaTT_ChiPhiXepHang2;
            txtLoaiPhiXepHang2.Text = HoaDon.LoaiChiPhiXepHang2;
            txtPhiVC_NoiDia.Text = HoaDon.PhiVC_DuongBo.ToString();
            ctrMaTT_PVC_NoiDia.Code = HoaDon.MaTT_PhiVC_DuongBo;
            txtPhiBaoHiem.Text = HoaDon.PhiBaoHiem.ToString();
            ctrMaTT_PhiBaoHiem.Code = HoaDon.MaTT_PhiBaoHiem;
            txtSoTienDieuChinh_PhiBH.Text = HoaDon.SoTienPhiBaoHiem.ToString();
            ctrMaTT_SoTien_PhiBH.Code = HoaDon.MaTT_TienPhiBaoHiem;
            txtTienKhauTru.Text = HoaDon.SoTienKhauTru.ToString();
            ctrMaTT_KhauTru.Code = HoaDon.MaTT_TienKhauTru;
            txtLoaiKhauTru.Text = HoaDon.LoaiKhauTru;
            txtDieuChinhKhac.Text = HoaDon.SoTienKhac.ToString();
            ctrMaTT_DieuChinhKhac.Code = HoaDon.MaTT_TienKhac;
            txtLoaiDieuChinhKhac.Text = HoaDon.LoaiSoTienKhac;
            txtTongTriGia_HD.Text = HoaDon.TongTriGiaHoaDon.ToString();
            ctrMaTT_TongTriGia_HD.Code = HoaDon.MaTT_TongTriGia;
            txtDieuKienGia_HD.Text = HoaDon.DieuKienGiaHoaDon;
            txtDiaDiemGiaoHang.Text = HoaDon.DiaDiemGiaoHang;
            txtGhiChu.Text = HoaDon.GhiChuDacBiet;
            txtTongDongHang.Text = HoaDon.TongSoDongHang.ToString();
        }

        private void VNACC_HoaDonForm_Load(object sender, EventArgs e)
        {
            SetIDControl();
            if (HoaDon.ID != 0)
                SetHoaDon();
            TuDongCapNhatThongTin();
        }

        #region Send VNACCS
        private void SendVnaccs(bool KhaiBaoSua)
        {
            try
            {
                if (HoaDon.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
                    IVA iva = VNACCMaperFromObject.IVAMapper(HoaDon);
                    if (iva == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    if (!KhaiBaoSua)
                    {
                        HoaDon.InputMessageID = HelperVNACCS.NewInputMSGID();
                        HoaDon.InsertUpdateFull();
                        msg = MessagesSend.Load<IVA>(iva, HoaDon.InputMessageID);
                    }
                    else
                        msg = MessagesSend.Load<IVA01>(VNACCMaperFromObject.IVA01Maper(HoaDon.SoTiepNhan), HoaDon.InputMessageID);

                    MsgLog.SaveMessages(msg, HoaDon.ID, EnumThongBao.SendMess, "");
                    SendVNACCFrm f = new SendVNACCFrm(msg);
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        this.ShowMessage(f.msgFeedBack, false);
                    }
                    else
                    {
                        if (f.feedback.Error == null || f.feedback.Error.Count == 0)
                        {
                            if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
                            {
                                CapNhatThongTin(f.feedback.ResponseData);
                                f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
                            }
                            else
                            {
                                if (f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(0, 15) == "00000-0000-0000")
                                {
                                    try
                                    {
                                        decimal SoToKhai = System.Convert.ToDecimal(f.feedback.header.VungDuTru_2.GetValue(false).ToString().Substring(52, 12));
                                        HoaDon.SoTiepNhan = SoToKhai;
                                        HoaDon.InsertUpdateFull();
                                        SetHoaDon();
                                    }
                                    catch (System.Exception ex)
                                    {
                                        f.msgFeedBack += Environment.NewLine + "Lỗi cập nhật số tờ khai: " + Environment.NewLine + ex.Message;
                                        Logger.LocalLogger.Instance().WriteMessage(ex);
                                    }
                                }
                            }
                        }
                        else
                        {
                            //foreach (ErrorVNACCS error in f.feedback.Error)
                            //{
                            //    f.msgFeedBack += Environment.NewLine + "Lỗi : " + error.loadError();
                            //}
                        }
                        this.ShowMessageTQDT(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        private void GetIIV()
        {
            try
            {
                if (HoaDon.ID == 0)
                {
                    this.ShowMessage("Vui lòng lưu thông tin trước khi khai báo", false);
                    return;
                }
                if (this.ShowMessage("Bạn chắc chắn muốn khai báo thông tin này đến HQ ? ", true) == "Yes")
                {
                    IIV iiv = VNACCMaperFromObject.IIVMaper(HoaDon.SoTiepNhan);
                    if (iiv == null)
                    {
                        this.ShowMessage("Lỗi khi tạo messages !", false);
                        return;
                    }
                    MessagesSend msg;
                    msg = MessagesSend.Load<IIV>(iiv, HoaDon.InputMessageID);

                    MsgLog.SaveMessages(msg, HoaDon.ID, EnumThongBao.SendMess, "");
                    SendVNACCFrm f = new SendVNACCFrm(msg);
                    f.ShowDialog(this);
                    if (f.DialogResult == DialogResult.Cancel)
                    {
                        this.ShowMessage(f.msgFeedBack, false);
                    }
                    else
                    {
                        if (f.feedback.Error == null || f.feedback.Error.Count == 0)
                        {
                            if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
                            {
                                CapNhatThongTin(f.feedback.ResponseData);
                                f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
                            }
                        }
                        this.ShowMessageTQDT(f.msgFeedBack, false);
                    }
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }
        #endregion

        #region Cập nhật thông tin
        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            HoaDon = (KDT_VNACC_HoaDon)ProcessMessages.GetDataResult_HoaDon(msgResult, "", HoaDon);
            //             if (HoaDonNew.GetType() == typeof(KDT_VNACC_HoaDon))
            //             {
            //                 HoaDon = HelperVNACCS.UpdateObject<KDT_VNACC_HoaDon>(HoaDon, HoaDonNew);   
            //             }
            //             else 
            //if(HoaDonNew.GetType() == typeof(VAL0880))
            //{
            //    VAL0880 val0880 = HoaDonNew as VAL0880;
            //    HoaDon.SoTiepNhan = System.Convert.ToDecimal(val0880.SOTIEPNHANHOADONDIENTU.GetValue());
            //    HoaDon.MaNguoiXNK = val0880.MANGUOIXUATKHAU.GetValue().ToString();
            //    HoaDon.NgayTiepNhan = System.Convert.ToDateTime(val0880.NGAYTHANGNAM.GetValue());
            //    //HoaDon.NguoiSU  val0880.NGUOISUDUNG.GetValue().ToString();
            //    HoaDon.PhanLoaiVanChuyen = val0880.PHANLOAIVANCHUYEN.GetValue().ToString();
            //    HoaDon.PhanLoaiXuatNhap = val0880.PHANLOAIXUATNHAPKHAU.GetValue().ToString();
            //    HoaDon.SoHoaDon = val0880.SOHOADON.GetValue().ToString();
            //    HoaDon.SoPL =  val0880.SOPL.GetValue().ToString();
            //    HoaDon.TenNguoiXNK = val0880.TENNGUOIXUATKHAU.GetValue().ToString();
            //}

            HoaDon.InsertUpdatePhanHoiFull();
            SetHoaDon();
        }
        #endregion
        private void TuDongCapNhatThongTin()
        {
            if (HoaDon != null && HoaDon.SoTiepNhan > 0 && HoaDon.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}'", HoaDon.SoTiepNhan.ToString()), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = "E";
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.Delete();
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = "E";
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }

            }
        }

        #region Bổ sung nghiệp vụ

        private Janus.Windows.UI.CommandBars.UICommand cmdIVA;
        private Janus.Windows.UI.CommandBars.UICommand cmdIVA01;
        private Janus.Windows.UI.CommandBars.UICommand cmdIIV;
        private void BoSungNghiepVu()
        {
            this.cmdIVA = new Janus.Windows.UI.CommandBars.UICommand("cmdIVA");
            this.cmdIVA.Key = "cmdIVA";
            this.cmdIVA.Name = "cmdIVA";
            this.cmdIVA.Text = "nghiệp vụ IVA (Khai báo hóa đơn)";

            this.cmdIVA01 = new Janus.Windows.UI.CommandBars.UICommand("cmdIVA01");
            this.cmdIVA01.Key = "cmdIVA01";
            this.cmdIVA01.Name = "cmdIVA01";
            this.cmdIVA01.Text = "nghiệp vụ IVA01 (Khai báo sửa hóa đơn)";

            this.cmdIIV = new Janus.Windows.UI.CommandBars.UICommand("cmdIIV");
            this.cmdIIV.Key = "cmdIIV";
            this.cmdIIV.Name = "cmdIIV";
            this.cmdIIV.Text = "nghiệp vụ IIV (Gọi hóa đơn)";

            this.cmdLayPhanHoi.Text = "Cập nhật thông tin ";
            this.cmdKhaiBao.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] 
            {
                this.cmdIVA,
                this.cmdIVA01,
                this.cmdIIV
            });
        }

        #endregion Bổ sung nghiệp vụ
        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                txtSoTiepNhan.Tag = "NIV"; //Số tiếp nhận hóa đơn điện tử
                cbbPhanLoaiXuatNhap.Tag = "YNK"; //Phân loại xuất nhập khẩu
                txtMaDaiLy_HQ.Tag = "TII"; //Mã đại lý Hải quan
                txtSoHoaDon.Tag = "IVN"; //Số hóa đơn 
                clcNgayLapHD.Tag = "IVD"; //Ngày lập hóa đơn
                txtDiaDiemLap_HD.Tag = "IVB"; //Địa điểm lập hóa đơn
                //txtMaSoPhanLoai.Tag = "N__"; //Mã số phân loại
                //txtSo.Tag = "O__"; //Số
                //txtNgayThangNam.Tag = "M__"; //Ngày tháng năm
                ctrPTTT.TagName = "HOS"; //Phương thức thanh toán
                // txtPhanLoaiBenLienQuan(PartyType).Tag = "AT_"; //Phân loại bên liên quan (Party type)
                txtMa_XNK.Tag = "IMC"; //Mã người xuất nhập khẩu
                txtTen_XNK.Tag = "IMN"; //Tên người xuất nhập khẩu
                txtMaBuuChinh_XNK.Tag = "IMY"; //Mã bưu chính của người xuất nhập khẩu
                txtDiaChi_XNK.Tag = "IMA"; //Địa chỉ người xuất nhập khẩu
                txtDienThoai_XNK.Tag = "IMT"; //Số điện thoại của người xuất nhập khẩu
                txtNguoiLap_HD.Tag = "SAA"; //Người lập hóa đơn (bao gồm chức vụ)
                txtMa_GuiNhan.Tag = "EPC"; //Mã người gửi (người nhận)
                txtten_GuiNhan.Tag = "EPN"; //Tên người gửi (người nhận)
                txtMaBuuChinh_GuiNhan.Tag = "EP1"; //Mã bưu chính của người gửi (người nhận)
                txtDiaChi_1.Tag = "EPA"; //Địa chỉ người nhận (người gửi) 1 (Tên phố, số nhà, số hòm thư)
                txtDaiChi_2.Tag = "EP2"; //Địa chỉ người nhận (người gửi) 2 (Tên phố, số nhà, số hòm thư)
                txtTinh_Thanh.Tag = "EP3"; //Địa chỉ người nhận (người gửi) 3 (Tên tình, thành phố)
                txtQuocGia.Tag = "EP4"; //Địa chỉ  người nhận (người gửi) 4 (Tên quốc gia, vùng lãnh thổ)
                txtDienThoai_GuiNhan.Tag = "EP6"; //Số điện thoại người nhận (người gửi)
                txtMaNuoc.Tag = "EEE"; //Nước sở tại của người nhận (người gửi)
                txtMaKyHieu.Tag = "KNO"; //Mã ký hiệu
                ctrLoaiVanChuyen.TagName = "TTP"; //Phân loại vận chuyển
                txtTen_PTVC.Tag = "VSS"; //Tên phương tiện vận chuyển
                txtSoHieuChuyenDi.Tag = "VNO"; //Số hiệu chuyến đi
                ctrDiaDiemXemHang.Tag = "PSC"; //Mã địa điểm xếp hàng
                //txtTenDiaDiemXepHang.Tag = "PSN"; //Tên địa điểm xếp hàng
                clcNgayXepHang.Tag = "PSD"; //Thời kỳ xếp hàng
                ctrDiaDiemDoHang.Tag = "DST"; //Mã địa điểm dỡ hàng
                //txtTenDiaDiemDoHang.Tag = "DCN"; //Tên địa điểm dỡ hàng
                ctrDiaDiemTrungChuyen.Tag = "KYC"; //Mã địa điểm trung chuyển
                //txtTenDiaDiemTrungChuyen.Tag = "KYN"; //Tên địa điểm trung chuyển
                txtTrongLuong_Gross.Tag = "GWJ"; //Tổng trọng lượng hàng (Gross)
                cbbDVT_TrongLuongGross.Tag = "GWT"; //Mã đơn vị tính tổng trọng lượng
                txtTrongLuongThuan.Tag = "JWJ"; //Trọng lượng thuần
                cbbDVT_TrongLuongThuan.Tag = "JWT"; //Mã đơn vị tính trọng lượng thuần
                txtTongTheTich.Tag = "STJ"; //Tổng thể tích
                cbbDVT_TheTich.Tag = "STT"; //Mã đơn vị tính tổng thể tích
                txtTongSoKien.Tag = "NOJ"; //Tổng số kiện hàng
                cbbDVT_KienHang.Tag = "NOT"; //Mã đơn vị tính tổng số kiện hàng
                txtGhiChu_ChuHang.Tag = "NT3"; //Ghi chú của chủ hàng
                txtSoPL.Tag = "PLN"; //Số P/L
                txtNganHang_PL.Tag = "LCB"; //Ngân hàng L/C
                txtTriGiaFOB.Tag = "FON"; //Trị giá FOB
                ctrMaTT_TriGiaFOB.TagName = "FOT"; //Mã tiền tệ của FOB
                txtSoTienDieuChinh_FOB.Tag = "FKK"; //Số tiền điều chỉnh FOB
                ctrMaTT_SoTien_FOB.TagName = "FKT"; //Mã tiền tệ số tiền điều chỉnh FOB
                txtPhiVanChuyen.Tag = "FR3"; //Phí vận chuyển
                ctrMaTT_PhiVanChuyen.TagName = "FR2"; //Mã tiền tệ phí vận chuyển
                txtNoiThanhToan_PVC.Tag = "FR4"; //Nơi thanh toán phí vận chuyển
                txtChiPhiXepHang1.Tag = "FS1"; //Chi phí xếp hàng 1
                ctrMaTT_PhiXepHang1.TagName = "FT1"; //Mã tiền tệ chi phí xếp hàng 1
                txtLoaiPhiXepHang1.Tag = "FH1"; //Loại chi phí xếp hàng 1
                txtChiPhiXepHang2.Tag = "FS2"; //Chi phí xếp hàng 2
                ctrMaTT_PhiXepHang2.TagName = "FT2"; //Mã tiền tệ chi phí xếp hàng 2
                txtLoaiPhiXepHang2.Tag = "FH2"; //Loại chi phí xếp hàng 2
                txtPhiVC_NoiDia.Tag = "NUH"; //Phí vận chuyển đường bộ nội địa
                ctrMaTT_PVC_NoiDia.TagName = "NTK"; //Mã tiền tệ chi phí vận chuyển đường bộ nội địa
                txtPhiBaoHiem.Tag = "IN3"; //Phí bảo hiểm
                ctrMaTT_PhiBaoHiem.TagName = "IN2"; //Mã tiền tệ của phí bảo hiểm
                txtSoTienDieuChinh_PhiBH.Tag = "IK1"; //Số tiền điều chỉnh phí bảo hiểm
                ctrMaTT_SoTien_PhiBH.TagName = "IK2"; //Mã tiền tệ số tiền điều chỉnh phí bảo hiểm
                txtTienKhauTru.Tag = "NBG"; //Số tiền khấu trừ
                ctrMaTT_KhauTru.TagName = "NBC"; //Mã tiền tệ của số tiền khấu trừ
                txtLoaiKhauTru.Tag = "NBS"; //Loại khấu trừ
                txtDieuChinhKhac.Tag = "SKG"; //Số tiền điều chỉnh khác
                ctrMaTT_DieuChinhKhac.TagName = "SKC"; //Mã tiền tệ số tiền điều chỉnh khác
                txtLoaiDieuChinhKhac.Tag = "SKS"; //Loại số tiền điều chỉnh khác
                txtTongTriGia_HD.Tag = "IP4"; //Tổng trị giá hóa đơn
                ctrMaTT_TongTriGia_HD.TagName = "IP3"; //Mã tiền tệ tổng trị giá hóa đơn
                txtDieuKienGia_HD.Tag = "IP2"; //Điều kiện giá hóa đơn
                txtDiaDiemGiaoHang.Tag = "HWT"; //Địa điểm giao hàng
                txtGhiChu.Tag = "NT1"; //Ghi chú đặc biệt
                txtTongDongHang.Tag = "SNM"; //Tổng số dòng hàng

                //txtSoDong.Tag = "RNO"; //Số dòng
                //txtMaHangHoa.Tag = "SNO"; //Mã hàng hóa
                //txtMaSoHangHoa.Tag = "CMD"; //Mã số hàng hóa
                //txtMoTaHangHoa.Tag = "CMN"; //Mô tả hàng hóa
                //txtMaNuocXuatXu.Tag = "ORC"; //Mã nước xuất xứ
                //txtTenNuocXuatXu.Tag = "ORN"; //Tên nước xuất xứ
                //txtSoKienHang.Tag = "KBN"; //Số kiện hàng
                //txtSoLuong(1).Tag = "QN1"; //Số lượng (1)
                //txtMaDonViTinhSoLuong(1).Tag = "QT1"; //Mã đơn vị tính số lượng (1)
                //txtSoLuong(2).Tag = "QN2"; //Số lượng (2)
                //txtMaDonViTinhSoLuong(2).Tag = "QT2"; //Mã đơn vị tính số lượng (2)
                //txtDonGiaHoaDon.Tag = "TNK"; //Đơn giá hóa đơn
                //txtMaTienTeCuaDonGiaHoaDon.Tag = "TNC"; //Mã tiền tệ của đơn giá hóa đơn
                //txtDonViCuaDonGiaHoaDonVaSoLuong.Tag = "TSC"; //Đơn vị của đơn giá hóa đơn và số lượng
                //txtTriGiaHoaDon.Tag = "KKT"; //Trị giá hóa đơn
                //txtMaTienTeGiaTien.Tag = "KKC"; //Mã tiền tệ giá tiền
                //"txtLoaiKhauTru(baoGomCaPhanLoaiMienTraTien)
                //.Tag = ""NSR""; //Loại khấu trừ (bao gồm cả phân loại miễn trả tiền)
                //"
                //txtSoTienKhauTru.Tag = "NGA"; //Số tiền khấu trừ
                //txtMaTienTeSoTienKhauTru.Tag = "NTC"; //Mã tiền tệ số tiền khấu trừ

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }
    }
}
