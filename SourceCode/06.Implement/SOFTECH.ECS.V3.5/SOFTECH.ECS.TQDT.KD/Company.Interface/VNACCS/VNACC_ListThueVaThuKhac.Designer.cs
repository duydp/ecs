﻿namespace Company.Interface.VNACCS
{
    partial class VNACC_ListThueVaThuKhac
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Janus.Windows.GridEX.GridEXLayout grListThueThuKhac_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_ListThueVaThuKhac));
            this.grListThueThuKhac = new Janus.Windows.GridEX.GridEX();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grListThueThuKhac)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.grListThueThuKhac);
            this.grbMain.Size = new System.Drawing.Size(786, 192);
            // 
            // grListThueThuKhac
            // 
            this.grListThueThuKhac.AllowAddNew = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListThueThuKhac.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListThueThuKhac.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            grListThueThuKhac_DesignTimeLayout.LayoutString = resources.GetString("grListThueThuKhac_DesignTimeLayout.LayoutString");
            this.grListThueThuKhac.DesignTimeLayout = grListThueThuKhac_DesignTimeLayout;
            this.grListThueThuKhac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grListThueThuKhac.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.grListThueThuKhac.FrozenColumns = 4;
            this.grListThueThuKhac.GroupByBoxVisible = false;
            this.grListThueThuKhac.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.grListThueThuKhac.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.grListThueThuKhac.Location = new System.Drawing.Point(0, 0);
            this.grListThueThuKhac.Name = "grListThueThuKhac";
            this.grListThueThuKhac.RecordNavigator = true;
            this.grListThueThuKhac.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.grListThueThuKhac.Size = new System.Drawing.Size(786, 192);
            this.grListThueThuKhac.TabIndex = 1;
            this.grListThueThuKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.grListThueThuKhac.VisualStyleManager = this.vsmMain;
            this.grListThueThuKhac.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.grListThueThuKhac_RowDoubleClick);
            // 
            // VNACC_ListThueVaThuKhac
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(786, 192);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_ListThueVaThuKhac";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Danh sách thuế và thu khác";
            this.Load += new System.EventHandler(this.VNACC_ListThueVaThuKhac_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grListThueThuKhac)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.GridEX.GridEX grListThueThuKhac;
    }
}