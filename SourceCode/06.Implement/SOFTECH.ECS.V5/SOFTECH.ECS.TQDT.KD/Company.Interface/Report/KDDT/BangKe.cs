using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.SXXK.ToKhai;
using System.Data;

namespace Company.Interface.Report.KDDT
{
    public partial class BangKe : DevExpress.XtraReports.UI.XtraReport
    {
        public DataTable DTSource;
        public DateTime thoigian;
        public string NguoiLapBieu;
        public string NguoiDuyetBieu;
        public string DonViBaoCao;

        public Font MyFont = new Font("Times New Roman", 10, FontStyle.Regular);
        public BangKe()
        {
            InitializeComponent();
        }
        public void Binddata()
        {
            this.DataSource = this.DTSource ;
            DataTable dt = this.DTSource;
            DateTime thoigian = this.thoigian;
            Int64 tonggiatri =0;
            Int64 tonggiatriDP = 0;
            Int64 tongsoluong = 0;
            Int64 tongsoluongDP = 0;
            Int64 tonggiatriDN = 0;
            Int64 tonggiatriDPDN = 0;
            Int64 tongsoluongDN = 0;
            Int64 tongsoluongDPDN = 0;

            this.xrTable9.BorderWidth = 1;           
            this.lblTen.Font = MyFont;

            foreach(DataRow dr in dt.Rows ){
                if(dr["Ten"].ToString().Contains("/")==false){                
                    tonggiatri += Convert.ToInt64(dr["TongTriGia"]);
                    tonggiatriDP += Convert.ToInt64(dr["TongTriGiaDP"]);
                    tongsoluong += Convert.ToInt64(dr["TongSoLuong"]);
                    tongsoluongDP += Convert.ToInt64(dr["TongSoLuongDP"]);
                    tonggiatriDN += Convert.ToInt64(dr["TongTriGiaTuDN"]);
                    tonggiatriDPDN += Convert.ToInt64(dr["TongTriGiaDPTuDN"]);
                    tongsoluongDN += Convert.ToInt64(dr["TongSoLuongTuDN"]);
                    tongsoluongDPDN += Convert.ToInt64(dr["TongSoLuongDPTuDN"]);   
                }
            }
            
            //group
            //lblGroupHeader.DataBindings.Add("Text", this.DataSource, "DVT");
            //GroupField groupfield = new GroupField("DVT");
            //this.GroupHeader1.GroupFields.Add(groupfield);
            
            lblTen.DataBindings.Add("Text", this.DataSource ,"Ten");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT");
            lblSoluong.DataBindings.Add("Text", this.DataSource, "TongSoLuong", "{0:N2}");// định dạng số tp{0:N2}
            lblTrigia.DataBindings.Add("Text", this.DataSource, "TongTriGia", "{0:N2}");
            lblSoLuongDP.DataBindings.Add("Text", this.DataSource, "TongSoLuongDP", "{0:N2}");
            lblTriGiaDP.DataBindings.Add("Text", this.DataSource, "TongTriGiaDP", "{0:N2}");
            lblSoluongDN.DataBindings.Add("Text", this.DataSource, "TongSoLuongTuDN", "{0:N2}");
            lblTriGiaDN.DataBindings.Add("Text", this.DataSource, "TongTriGiaTuDN", "{0:N2}");
            lblSoLuongDPDN.DataBindings.Add("Text", this.DataSource, "TongSoLuongDPTuDN", "{0:N2}");
            lblTriGiaDPDN.DataBindings.Add("Text", this.DataSource, "TongTriGiaDPTuDN", "{0:N2}");

            //Sum so luong
            DataTable dtSum = new DataTable();
            DataRow row;
            dtSum.Columns.Add("TongSL1", typeof(decimal));
            dtSum.Columns.Add("TongTG1", typeof(decimal));
            dtSum.Columns.Add("TongSL2", typeof(decimal));
            dtSum.Columns.Add("TongTG2", typeof(decimal));
            dtSum.Columns.Add("TongSL3", typeof(decimal));
            dtSum.Columns.Add("TongTG3", typeof(decimal));
            dtSum.Columns.Add("TongSL4", typeof(decimal));
            dtSum.Columns.Add("TongTG4", typeof(decimal));
            row = dtSum.NewRow();
            row["TongSL1"] = tongsoluong;
            row["TongTG1"] = tonggiatri;
            row["TongSL2"] = tongsoluongDP;
            row["TongTG2"] = tonggiatriDP;
            row["TongSL3"] = tongsoluongDN;
            row["TongTG3"] = tonggiatriDN;
            row["TongSL4"] = tongsoluongDPDN;
            row["TongTG4"] = tonggiatriDPDN;
            dtSum.Rows.Add(row);
            
            lblTongSL1.DataBindings.Add("Text",dtSum,"TongSL1","{0:N2}");
            lblTongTG1.DataBindings.Add("Text", dtSum, "TongTG1", "{0:N2}");
            lblTongSL2.DataBindings.Add("Text", dtSum, "TongSL2", "{0:N2}");
            lblTongTG21.DataBindings.Add("Text", dtSum, "TongTG2", "{0:N2}");
            lblTongSL3.DataBindings.Add("Text", dtSum, "TongSL3", "{0:N2}");
            lblTongTG31.DataBindings.Add("Text", dtSum, "TongTG3", "{0:N2}");
            lblTongSL4.DataBindings.Add("Text", dtSum, "TongSL4", "{0:N2}");
            lblTongTG4.DataBindings.Add("Text", dtSum, "TongTG4", "{0:N2}");
            
            DateTime today = new DateTime();
            today = DateTime.Now;
            lbNgay.Text = "Tháng " + thoigian.Month.ToString() + " năm " + thoigian.Year.ToString();
            lbNgayKy.Text ="..........., ngày........tháng...... năm........ ";
            lblNguoiLapBieu.Text = this.NguoiLapBieu.ToUpper();
            lblNguoiDuyetBieu.Text = this.NguoiDuyetBieu.ToUpper();
            lblTenNganDN.Text = "ĐVBC : " + this.DonViBaoCao.ToUpper();

            lblTenDN.Text = GlobalSettings.TEN_DON_VI.ToUpper();
            lblMaDN.Text =  "Mã số : "+ GlobalSettings.MA_DON_VI;
            
            
        }

        private void lblTen_TextChanged(object sender, EventArgs e)
        {
            if (this.lblTen.Text.Trim().Contains("B.Mặt hàng/Nước "))
            { Font MyFont2 = new Font("Times New Roman", 10, FontStyle.Bold);                               
                this.xrTable9.Visible = true;
                this.xrTable1.Visible = false;
                this.xrTable9.Borders = DevExpress.XtraPrinting.BorderSide.All; 
                this.xrTable9.BorderWidth = 2;
                this.xrTableCell9.DataBindings.Add("Text", this.DataSource, "Ten");
                this.xrTableCell9.Font = MyFont2;          
            }
            else
            {
                this.xrTable1.Visible = true;             
                this.xrTable9.Visible = false;                             
            }
            
        }

       
        

    }
}
