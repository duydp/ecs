using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.Report.SXXK
{
    public partial class ToKhaiCT: DevExpress.XtraReports.UI.XtraReport
    {
        public ToKhaiChuyenTiep TKCT = new ToKhaiChuyenTiep();
        public ReportViewTKCTForm report;
        public ToKhaiCT()
        {
            InitializeComponent();
        }
        public void BindReport()
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            DateTime minDate = new DateTime(1900, 1, 1);
            if (this.TKCT.SoTiepNhan != 0)
                this.lblSoTiepNhan.Text = "Số TNDKDT: " + this.TKCT.SoTiepNhan;
            if (this.TKCT.MaLoaiHinh.Contains("X"))
            {
                lblMaNguoiGiaoHang.Text = ToStringForReport(this.TKCT.MaDoanhNghiep, "   ");
                lblTenNguoiGiaoHang.Text = GlobalSettings.TEN_DON_VI.ToUpper() + "\r\n" + GlobalSettings.DIA_CHI.ToUpper(); ;
                lblMaNguoiNhanHang.Text = ToStringForReport(this.TKCT.MaKhachHang, "   ");
                lblTenNguoiNhanHang.Text = this.TKCT.TenKH;
                lblSoHopDongGiao.Text = this.TKCT.SoHopDongDV;
                if(this.TKCT.NgayHDDV.Year > 1900)
                    lblNgayHDGiao.Text = this.TKCT.NgayHDDV.ToString("dd/MM/yyyy");
                if (this.TKCT.NgayHetHanHDDV.Year > 1900)
                    lblNgayHetHanHDGiao.Text = this.TKCT.NgayHetHanHDDV.ToString("dd/MM/yyyy");
                lblSoHopDongNhan.Text = this.TKCT.SoHDKH;
                if (this.TKCT.NgayHDKH.Year > 1900)
                    lblNgayHDNhan.Text = this.TKCT.NgayHDKH.ToString("dd/MM/yyyy");
                if (this.TKCT.NgayHetHanHDKH.Year > 1900)
                    lblNgayHetHanHDNhan.Text = this.TKCT.NgayHetHanHDKH.ToString("dd/MM/yyyy");
                lblNguoiChiDinhGiaoHang.Text = this.TKCT.NguoiChiDinhDV;
                lblNguoiChiDinhNhanHang.Text = this.TKCT.NguoiChiDinhKH;

            }
            else
            {
                lblMaNguoiNhanHang.Text = ToStringForReport(this.TKCT.MaDoanhNghiep, "   ");
                lblTenNguoiNhanHang.Text = GlobalSettings.TEN_DON_VI.ToUpper() + "\r\n" + GlobalSettings.DIA_CHI.ToUpper(); ;
                lblMaNguoiGiaoHang.Text = ToStringForReport(this.TKCT.MaKhachHang, "   ");
                lblTenNguoiGiaoHang.Text = this.TKCT.TenKH;
                lblSoHopDongNhan.Text = this.TKCT.SoHopDongDV;
                if (this.TKCT.NgayHDDV.Year > 1900)
                    lblNgayHDNhan.Text = this.TKCT.NgayHDDV.ToString("dd/MM/yyyy");
                if (this.TKCT.NgayHetHanHDDV.Year > 1900)
                    lblNgayHetHanHDNhan.Text = this.TKCT.NgayHetHanHDDV.ToString("dd/MM/yyyy");
                lblSoHopDongGiao.Text = this.TKCT.SoHDKH;
                if (this.TKCT.NgayHDKH.Year > 1900)
                    lblNgayHDGiao.Text = this.TKCT.NgayHDKH.ToString("dd/MM/yyyy");
                if (this.TKCT.NgayHetHanHDKH.Year > 1900)
                    lblNgayHetHanHDGiao.Text = this.TKCT.NgayHetHanHDKH.ToString("dd/MM/yyyy");
                lblNguoiChiDinhGiaoHang.Text = this.TKCT.NguoiChiDinhKH;
                lblNguoiChiDinhNhanHang.Text = this.TKCT.NguoiChiDinhDV;
            }
            if (this.TKCT.MaLoaiHinh.Contains("PL"))
                lblGiaoNPL.Visible = lblNhanNPL.Visible = true;
            else if (this.TKCT.MaLoaiHinh.Contains("SP"))
                lblNhanSP.Visible = lblGiaoSP.Visible = true;
            else
                lblGiaoTB.Visible = lblNhanTB.Visible = true;
            lblDiaDiemGiaoHang.Text = this.TKCT.DiaDiemXepHang;
            lblMaDaiLyTTHQ.Text = ToStringForReport(this.TKCT.MaDaiLy,"   ");
            lblTenDaiLyTTHQ.Text = "";
            lblNguyenTe1.Text = lblNguyenTe2.Text = this.TKCT.NguyenTe_ID;
            int limit = this.TKCT.HCTCollection.Count;
            if (limit <= 20)
            {
                for (int i = 0; i < limit; i++)
                {
                    XRControl control = new XRControl();
                    
                    HangChuyenTiep hct = this.TKCT.HCTCollection[i];
  
                    control = this.xrTable1.Rows[i].Controls["TenHang" + (i + 1)];
                    control.Text = hct.TenHang;
                    control = this.xrTable1.Rows[i].Controls["MaHS" + (i + 1)];
                    control.Text = hct.MaHS;
                    control = this.xrTable1.Rows[i].Controls["DVT" + (i + 1)];
                    control.Text = hct.SoLuong.ToString("G15");
                    control = this.xrTable1.Rows[i].Controls["Luong" + (i + 1)];
                    control.Text = DonViTinh.GetName((object)hct.ID_DVT);
                    control = this.xrTable1.Rows[i].Controls["DonGiaNT" + (i + 1)];
                    control.Text = hct.DonGia.ToString("G10");
                    control = this.xrTable1.Rows[i].Controls["TriGiaNT" + (i + 1)];
                    control.Text = hct.TriGia.ToString("N2");
                }
            }
            else
            {
                TenHang1.Text = "HÀNG CHUYỂN TIẾP";
                TenHang2.Text = "(PHỤ LỤC ĐÍNH KÈM)";
            }
            
        }
        private string ToStringForReport(string s, string patern)
        {
            s = s.Trim();
            if (s.Length == 0) return "";
            string temp = "";
            for (int i = 0; i < s.Length - 1; i++)
                temp += s[i] + patern;
            temp += s[s.Length-1];
            return temp;
        }
        public void setVisibleImage(bool t)
        {
            ptbImage.Visible = t;
        }

        private void TenHang_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRTableCell cell = (XRTableCell)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
        public void setNhomHang(XRTableCell cell, string tenHang)
        {
            cell.Text = tenHang;
        }

    }
}
