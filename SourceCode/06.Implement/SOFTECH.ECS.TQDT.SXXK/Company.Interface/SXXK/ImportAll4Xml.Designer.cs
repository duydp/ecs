namespace Company.Interface.SXXK
{
    partial class ImportAll4Xml
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgListNPLDN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportAll4Xml));
            Janus.Windows.GridEX.GridEXLayout dgListSPDN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListDMDN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgListTKDN_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.chkOverwrite = new Janus.Windows.EditControls.UICheckBox();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvFilePath = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.tabDN = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.dgListNPLDN = new Janus.Windows.GridEX.GridEX();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dgListSPDN = new Janus.Windows.GridEX.GridEX();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dgListDMDN = new Janus.Windows.GridEX.GridEX();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dgListTKDN = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.label7 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.tabDN.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLDN)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPDN)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListDMDN)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKDN)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.label7);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(814, 440);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = " (*.xml)|*.xml";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiButton1);
            this.uiGroupBox2.Controls.Add(this.chkOverwrite);
            this.uiGroupBox2.Controls.Add(this.btnSave);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 3);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(787, 60);
            this.uiGroupBox2.TabIndex = 213;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(472, 23);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(89, 23);
            this.uiButton1.TabIndex = 216;
            this.uiButton1.Text = "Đọc file";
            this.uiButton1.VisualStyleManager = this.vsmMain;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // chkOverwrite
            // 
            this.chkOverwrite.BackColor = System.Drawing.Color.Transparent;
            this.chkOverwrite.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOverwrite.Location = new System.Drawing.Point(567, 23);
            this.chkOverwrite.Name = "chkOverwrite";
            this.chkOverwrite.Size = new System.Drawing.Size(65, 23);
            this.chkOverwrite.TabIndex = 218;
            this.chkOverwrite.Text = "Ghi đè";
            this.chkOverwrite.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(638, 23);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(70, 23);
            this.btnSave.TabIndex = 215;
            this.btnSave.Text = "Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtFilePath.Location = new System.Drawing.Point(6, 23);
            this.txtFilePath.Multiline = true;
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(460, 23);
            this.txtFilePath.TabIndex = 188;
            this.txtFilePath.VisualStyleManager = this.vsmMain;
            this.txtFilePath.ButtonClick += new System.EventHandler(this.txtFilePath_ButtonClick);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(714, 23);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(70, 23);
            this.btnClose.TabIndex = 214;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this.grbMain;
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvFilePath
            // 
            this.rfvFilePath.ControlToValidate = this.txtFilePath;
            this.rfvFilePath.ErrorMessage = "\"Cột đơn vị tính\" bắt buộc phải chọn.";
            this.rfvFilePath.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvFilePath.Icon")));
            this.rfvFilePath.Tag = "rfvFilePath";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.tabDN);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(12, 69);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(787, 338);
            this.uiGroupBox3.TabIndex = 217;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // tabDN
            // 
            this.tabDN.Controls.Add(this.tabPage1);
            this.tabDN.Controls.Add(this.tabPage2);
            this.tabDN.Controls.Add(this.tabPage3);
            this.tabDN.Controls.Add(this.tabPage4);
            this.tabDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabDN.Location = new System.Drawing.Point(3, 8);
            this.tabDN.Name = "tabDN";
            this.tabDN.SelectedIndex = 0;
            this.tabDN.Size = new System.Drawing.Size(781, 327);
            this.tabDN.TabIndex = 26;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.dgListNPLDN);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(773, 301);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Nguyên Phụ Liệu";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // dgListNPLDN
            // 
            this.dgListNPLDN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListNPLDN.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListNPLDN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListNPLDN_DesignTimeLayout.LayoutString = resources.GetString("dgListNPLDN_DesignTimeLayout.LayoutString");
            this.dgListNPLDN.DesignTimeLayout = dgListNPLDN_DesignTimeLayout;
            this.dgListNPLDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListNPLDN.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListNPLDN.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLDN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListNPLDN.GroupByBoxVisible = false;
            this.dgListNPLDN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLDN.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListNPLDN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLDN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListNPLDN.Hierarchical = true;
            this.dgListNPLDN.Location = new System.Drawing.Point(3, 3);
            this.dgListNPLDN.Name = "dgListNPLDN";
            this.dgListNPLDN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListNPLDN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListNPLDN.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListNPLDN.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLDN.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListNPLDN.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListNPLDN.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListNPLDN.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListNPLDN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListNPLDN.Size = new System.Drawing.Size(767, 295);
            this.dgListNPLDN.TabIndex = 24;
            this.dgListNPLDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dgListSPDN);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(773, 301);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Sản Phẩm";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dgListSPDN
            // 
            this.dgListSPDN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListSPDN.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListSPDN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListSPDN_DesignTimeLayout.LayoutString = resources.GetString("dgListSPDN_DesignTimeLayout.LayoutString");
            this.dgListSPDN.DesignTimeLayout = dgListSPDN_DesignTimeLayout;
            this.dgListSPDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListSPDN.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListSPDN.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPDN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListSPDN.GroupByBoxVisible = false;
            this.dgListSPDN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPDN.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListSPDN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPDN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListSPDN.Hierarchical = true;
            this.dgListSPDN.Location = new System.Drawing.Point(3, 3);
            this.dgListSPDN.Name = "dgListSPDN";
            this.dgListSPDN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListSPDN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListSPDN.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListSPDN.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPDN.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListSPDN.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListSPDN.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListSPDN.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListSPDN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListSPDN.Size = new System.Drawing.Size(767, 295);
            this.dgListSPDN.TabIndex = 25;
            this.dgListSPDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dgListDMDN);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(773, 301);
            this.tabPage3.TabIndex = 4;
            this.tabPage3.Text = "Định mức";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dgListDMDN
            // 
            this.dgListDMDN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListDMDN.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListDMDN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListDMDN_DesignTimeLayout.LayoutString = resources.GetString("dgListDMDN_DesignTimeLayout.LayoutString");
            this.dgListDMDN.DesignTimeLayout = dgListDMDN_DesignTimeLayout;
            this.dgListDMDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListDMDN.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDMDN.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMDN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListDMDN.GroupByBoxVisible = false;
            this.dgListDMDN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMDN.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListDMDN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMDN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListDMDN.Hierarchical = true;
            this.dgListDMDN.Location = new System.Drawing.Point(3, 3);
            this.dgListDMDN.Name = "dgListDMDN";
            this.dgListDMDN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListDMDN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListDMDN.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListDMDN.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMDN.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDMDN.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListDMDN.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListDMDN.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListDMDN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListDMDN.Size = new System.Drawing.Size(767, 295);
            this.dgListDMDN.TabIndex = 25;
            this.dgListDMDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dgListTKDN);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(773, 301);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Tờ khai";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dgListTKDN
            // 
            this.dgListTKDN.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgListTKDN.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgListTKDN.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgListTKDN_DesignTimeLayout.LayoutString = resources.GetString("dgListTKDN_DesignTimeLayout.LayoutString");
            this.dgListTKDN.DesignTimeLayout = dgListTKDN_DesignTimeLayout;
            this.dgListTKDN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgListTKDN.FlatBorderColor = System.Drawing.SystemColors.MenuHighlight;
            this.dgListTKDN.FocusCellFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKDN.FocusCellFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKDN.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgListTKDN.GroupByBoxVisible = false;
            this.dgListTKDN.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKDN.GroupRowFormatStyle.ForeColor = System.Drawing.Color.Blue;
            this.dgListTKDN.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKDN.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgListTKDN.Hierarchical = true;
            this.dgListTKDN.Location = new System.Drawing.Point(3, 3);
            this.dgListTKDN.Name = "dgListTKDN";
            this.dgListTKDN.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgListTKDN.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgListTKDN.SelectedFormatStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.dgListTKDN.SelectedFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKDN.SelectedFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKDN.SelectedInactiveFormatStyle.BackColor = System.Drawing.Color.Empty;
            this.dgListTKDN.SelectedInactiveFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgListTKDN.SelectedInactiveFormatStyle.ForeColor = System.Drawing.Color.Empty;
            this.dgListTKDN.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgListTKDN.Size = new System.Drawing.Size(767, 295);
            this.dgListTKDN.TabIndex = 25;
            this.dgListTKDN.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Firebrick;
            this.label7.Location = new System.Drawing.Point(12, 418);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(165, 13);
            this.label7.TabIndex = 220;
            this.label7.Text = "Những dòng màu đỏ là đã có";
            // 
            // ImportAll4Xml
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(814, 440);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MinimizeBox = false;
            this.Name = "ImportAll4Xml";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Import định mức từ Xml";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.tabDN.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListNPLDN)).EndInit();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListSPDN)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListDMDN)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgListTKDN)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvFilePath;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.ImageList ImageList1;
        private Janus.Windows.EditControls.UICheckBox chkOverwrite;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private System.Windows.Forms.TabControl tabDN;
        private System.Windows.Forms.TabPage tabPage1;
        private Janus.Windows.GridEX.GridEX dgListNPLDN;
        private System.Windows.Forms.TabPage tabPage2;
        private Janus.Windows.GridEX.GridEX dgListSPDN;
        private System.Windows.Forms.TabPage tabPage3;
        private Janus.Windows.GridEX.GridEX dgListDMDN;
        private System.Windows.Forms.TabPage tabPage4;
        private Janus.Windows.GridEX.GridEX dgListTKDN;
    }
}