﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;


namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class GC_DNGiamSatTieuHuy : DeclarationBase
    {
        /// <summary>
        /// Hợp đồng
        /// </summary>
        [XmlElement("ContractReference")]
        public ContractDocument ContractReference { get; set; }
        /// <summary>
        /// ???
        /// </summary>
        [XmlElement("SubContructReference")]
        public List<ContractDocument> SubContructReference { get; set; }
        /// <summary>
        /// Giấy phép
        /// </summary>
        [XmlElement("License")]
        public License License { get; set; }
        /// <summary>
        /// Các bên tham gia tiêu hủy an..256
        /// </summary>
        [XmlElement("userAttends")]
        public string UserAttends { get; set; }
        /// <summary>
        /// Thời gian tiêu huỷ YYYY-MM-DD
        /// </summary>
        [XmlElement("time")]
        public string Time { get; set; }        
        /// <summary>
        /// Địa điểm tiêu huỷ an..256
        /// </summary>
        [XmlElement("location")]
        public string Location { get; set; }
        /// <summary>
        /// Thông tin phế liệu, phế phẩm tiêu huỷ hoặc sản phẩm, bán thành phẩm gia công
        /// </summary>
        [XmlElement("Scrap")]
        public List<Scrap> Scraps { get; set; }

    }
}
