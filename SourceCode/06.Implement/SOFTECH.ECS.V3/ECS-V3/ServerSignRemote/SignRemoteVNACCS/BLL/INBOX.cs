using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace SignRemote
{
    public partial class INBOX
    {
#if !SERVICE_SIGN
        public static void InsertSignContent(string cnnString, string guidID, string content, int Status)
        {
            SqlConnection cnn = new SqlConnection(cnnString);
            cnn.Open();
            string cmdText = string.Format("Insert into t_MSG_INBOX(GUIDSTR,MSG_ORIGIN, STATUS) values ('{0}','{1}',{2})", guidID, content,Status);
            SqlCommand sqlCmd = new SqlCommand(cmdText, cnn);

            sqlCmd.ExecuteNonQuery();
            cnn.Close();
        }
        public static void InsertSignContent(string cnnString, string guidID, string content)
        {
            SqlConnection cnn = new SqlConnection(cnnString);
            cnn.Open();
            string cmdText = string.Format("Insert into t_MSG_INBOX(GUIDSTR,MSG_ORIGIN) values ('{0}','{1}')", guidID, content);
            SqlCommand sqlCmd = new SqlCommand(cmdText, cnn);
          
            sqlCmd.ExecuteNonQuery();
            cnn.Close();
        }
        public static INBOX GetTopDataContent()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();//(Company.KDT.SHARE.Components.Globals.DataSignLan);
            string cmd = string.Format("Select top(1) * from t_MSG_INBOX order by CREATE_TIME desc");
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(cmd);
            IDataReader reader = db.ExecuteReader(dbCommand);
            INBOX inbox = null;
            if (reader.Read())
            {
                inbox = new INBOX();
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_ORIGIN"))) inbox.MSG_ORIGIN = reader.GetString(reader.GetOrdinal("MSG_ORIGIN"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) inbox.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("STATUS"))) inbox.STATUS = reader.GetInt32(reader.GetOrdinal("STATUS"));
            }
            reader.Close();
            if (inbox != null)
            {
                cmd = string.Format("Delete t_MSG_INBOX where GUIDSTR='{0}'", inbox.GUIDSTR);
                dbCommand = (SqlCommand)db.GetSqlStringCommand(cmd);
                db.ExecuteNonQuery(dbCommand);
            }
            return inbox;
        }
#endif
        public static INBOX SelectTop1(string whereCondition, string orderByExpression)
        {

            string spName = "[dbo].[p_MSG_Top1]";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@From", SqlDbType.NVarChar, "t_MSG_INBOX");
            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            INBOX entity = null;
            if (reader.Read())
            {
                entity = new INBOX();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_FROM"))) entity.MSG_FROM = reader.GetString(reader.GetOrdinal("MSG_FROM"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_TO"))) entity.MSG_TO = reader.GetString(reader.GetOrdinal("MSG_TO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_ORIGIN"))) entity.MSG_ORIGIN = reader.GetString(reader.GetOrdinal("MSG_ORIGIN"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_DATA"))) entity.MSG_SIGN_DATA = reader.GetString(reader.GetOrdinal("MSG_SIGN_DATA"));
                if (!reader.IsDBNull(reader.GetOrdinal("MSG_SIGN_CERT"))) entity.MSG_SIGN_CERT = reader.GetString(reader.GetOrdinal("MSG_SIGN_CERT"));
                if (!reader.IsDBNull(reader.GetOrdinal("CREATE_TIME"))) entity.CREATE_TIME = reader.GetDateTime(reader.GetOrdinal("CREATE_TIME"));
                if (!reader.IsDBNull(reader.GetOrdinal("STATUS"))) entity.STATUS = reader.GetInt32(reader.GetOrdinal("STATUS"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));

            }

            reader.Close();
            return entity;
        }
    }

}