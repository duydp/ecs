﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Net;
using System.ServiceProcess;
using Microsoft.Win32;
using System.Diagnostics;
using System.ComponentModel;
using System.Windows.Forms;

namespace Company.KDT.SHARE.Components
{
    public class InstallSQLExpress
    {
        private static string AppDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
        // Methods
        public static bool CheckServiceExist(string instanceName, ref string serverAddr)
        {
            bool flag2;
            try
            {
                String machineName = "";
                if (GetIPAddress().Trim().Length == 0)
                {
                    machineName = Environment.MachineName;
                }
                ServiceController controller = null;
                bool flag = false;
                controller = new ServiceController("MSSQL$" + instanceName);
                try
                {
                    string serviceName = controller.ServiceName;
                    flag = true;
                }
                catch
                {
                    flag = false;
                }
                if (!flag)
                {
                    flag2 = false;
                }
                else
                {
                    string[] strArray = new string[] { machineName, @"\", instanceName, ",", Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\" + instanceName + @"\MSSQLServer\SuperSocketNetLib\Tcp", true).GetValue("TcpPort").ToString() };
                    serverAddr = string.Concat(strArray);
                    flag2 = true;
                }
            }
            catch (Exception)
            {
                flag2 = false;
            }
            return flag2;
        }

        private static string GetIPAddress()
        {
            try
            {
                return Dns.GetHostByName(Dns.GetHostName()).AddressList[0].ToString();
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static bool Install(bool hienProgress, string instanceName, ref string serverAddr, ref string ketQua)
        {
            bool flag3;
            string[] strArray;
            try
            {
                string path = Path.Combine(AppDir, "SQLEXPR_x86_SP2_R2.exe");
                if (!File.Exists(path))
                {
                    ketQua = "Không tồn tại bộ cài SQL Express 2008 SP2 R2 tại đường dẫn: " + path;
                    flag3 = false;
                }
                else
                {
                    string machineName = "";
                    string serviceName;
                    if (GetIPAddress().Trim().Length == 0)
                    {
                        machineName = Environment.MachineName;
                    }
                    ServiceController controller = null;
                    bool flag = false;
                    controller = new ServiceController("MSSQL$" + instanceName);
                    try
                    {
                        serviceName = controller.ServiceName;
                        flag = true;
                    }
                    catch
                    {
                        flag = false;
                    }
                    if (flag)
                    {
                        strArray = new string[] { machineName, @"\", instanceName, ",", Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\" + instanceName + @"\MSSQLServer\SuperSocketNetLib\Tcp", true).GetValue("TcpPort").ToString() };
                        serverAddr = string.Concat(strArray);
                        ketQua = "Đã tồn tại Instance " + instanceName;
                        flag3 = true;
                    }
                    else
                    {
                        string arguments = "";
                        arguments = !hienProgress ? ("/q /ACTION=INSTALL /FEATURES=SQL /INSTANCENAME=" + instanceName + " /SQLSVCSTARTUPTYPE=Automatic /SQLSVCACCOUNT=\"NT AUTHORITY\\NETWORK SERVICE\" /SQLSYSADMINACCOUNTS=\"BUILTIN\\Administrators\" /AGTSVCACCOUNT=\"NT AUTHORITY\\Network Service\" /ADDCURRENTUSERASSQLADMIN=true /TCPENABLED=1 /NPENABLED=1 /SECURITYMODE=SQL /SAPWD=123456 /IACCEPTSQLSERVERLICENSETERMS=true /HIDECONSOLE") : ("/qs /ACTION=INSTALL /FEATURES=SQL /INSTANCENAME=" + instanceName + " /SQLSVCSTARTUPTYPE=Automatic /SQLSVCACCOUNT=\"NT AUTHORITY\\NETWORK SERVICE\" /SQLSYSADMINACCOUNTS=\"BUILTIN\\Administrators\" /AGTSVCACCOUNT=\"NT AUTHORITY\\Network Service\" /ADDCURRENTUSERASSQLADMIN=true /TCPENABLED=1 /NPENABLED=1 /SECURITYMODE=SQL /SAPWD=123456 /IACCEPTSQLSERVERLICENSETERMS=true /HIDECONSOLE");
                        ProcessStartInfo info = new ProcessStartInfo(path, arguments)
                        {
                            UseShellExecute = true,
                            Verb = "runas"
                        };
                        Process process = new Process
                        {
                            StartInfo = info
                        };
                        process.Start();
                        process.WaitForExit();
                        bool flag2 = false;
                        controller = new ServiceController("MSSQL$" + instanceName);
                        try
                        {
                            serviceName = controller.ServiceName;
                            flag2 = true;
                        }
                        catch
                        {
                            flag2 = false;
                        }
                        if (!flag2)
                        {
                            strArray = new string[] { "Cài đặt SQL không thành công", Environment.NewLine, path, " ", arguments };
                            ketQua = string.Concat(strArray);
                            flag3 = false;
                        }
                        else
                        {
                            string str6 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\" + instanceName + @"\MSSQLServer\SuperSocketNetLib\Tcp", true).GetValue("TcpPort").ToString();
                            StringBuilder builder = new StringBuilder();
                            builder.AppendLine("netsh advfirewall firewall delete rule name = SQLPort_eBH");
                            builder.AppendLine("netsh advfirewall firewall add rule name = SQLPort_eBH dir = in protocol = tcp action = allow localport = " + str6 + " remoteip = localsubnet profile = ANY");
                            File.WriteAllText(Path.Combine(AppDir, "InstallRule.bat"), builder.ToString());
                            ProcessStartInfo info2 = new ProcessStartInfo(Path.Combine(AppDir, "InstallRule.bat"))
                            {
                                UseShellExecute = true,
                                Verb = "runas"
                            };
                            Process process2 = new Process
                            {
                                StartInfo = { CreateNoWindow = true }
                            };
                            process2.StartInfo = info2;
                            process2.Start();
                            process2.WaitForExit();
                            strArray = new string[] { machineName, @"\", instanceName, ",", str6 };
                            serverAddr = string.Concat(strArray);
                            ketQua = "Cài đặt thành công";
                            flag3 = true;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                if (!ReferenceEquals(exception.InnerException, null))
                {
                    object[] objArray = new object[] { "Message : ", exception.Message, Environment.NewLine, "Inner : ", exception.InnerException, Environment.NewLine, "Stack Trace : ", exception.StackTrace };
                    ketQua = string.Concat(objArray);
                }
                else
                {
                    strArray = new string[] { "Message : ", exception.Message, Environment.NewLine, "Stack Trace : ", exception.StackTrace };
                    ketQua = string.Concat(strArray);
                }
                flag3 = false;
            }
            return flag3;
        }
        private static void WebClientDownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {

        }

        private static void WebClientDownloadCompleted(object sender, AsyncCompletedEventArgs args)
        {

        }
        public static bool Install(string instanceName, ref string serverAddr, ref string ketQua, BackgroundWorker bgw, Label lblStatus)
        {
            bool flag3 = true;
            string[] strArray;
            try
            {
                UpdateLabel(lblStatus, "Đang kiểm tra bộ cài SQL...");
                string path = Path.Combine(AppDir, "SQLEXPR_x86_SP2_R2.exe");
                if (!File.Exists(path))
                {
                    try
                    {
                        UpdateLabel(lblStatus, "Đang tiến hành tải bộ cài đặt SQL từ trang chủ .Vui lòng không tắt kết nối Internet ...");
                        using (WebClient client = new WebClient())
                        {
                            var ur = new Uri("http://ecs.softech.cloud/Tool/SQLEXPR_x86_SP2_R2.exe");
                            //client.DownloadProgressChanged += WebClientDownloadProgressChanged;
                            //client.DownloadFileCompleted += WebClientDownloadCompleted;
                            client.DownloadFileAsync(ur, path);                       
                        }
                        flag3 = true;
                    }
                    catch
                    {
                        ketQua = "Không thể tải bộ cài đặt SQL Express 2008 SP2 R2 từ trang chủ .Vui lòng kiểm tra lại kết nối Internet rồi sau đó thử lại ...";
                        UpdateLabel(lblStatus, ketQua);
                        bgw.ReportProgress(100);
                        flag3 = false;
                    }
                    //ketQua = "Không tồn tại bộ cài SQL Express 2008 SP2 R2 tại đường dẫn: " + path;
                    //UpdateLabel(lblStatus, ketQua);
                    //bgw.ReportProgress(100);
                    //flag3 = false;
                }
                if(flag3)
                {
                    string machineName = "";
                    string serviceName;
                    bgw.ReportProgress(10);
                    UpdateLabel(lblStatus, "Đang kiểm tra địa chỉ IP máy nhánh...");
                    if (GetIPAddress().Trim().Length == 0)
                    {
                        machineName = Environment.MachineName;
                    }
                    bgw.ReportProgress(20);
                    UpdateLabel(lblStatus, "Đang tiến hành kiểm tra SQL Service...");
                    ServiceController controller = null;
                    bool flag = false;
                    controller = new ServiceController("MSSQL$" + instanceName);
                    try
                    {
                        serviceName = controller.ServiceName;
                        flag = true;
                    }
                    catch
                    {
                        flag = false;
                    }
                    if (flag)
                    {
                        strArray = new string[] { machineName, @"\", instanceName, ",", Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\" + instanceName + @"\MSSQLServer\SuperSocketNetLib\Tcp", true).GetValue("TcpPort").ToString() };
                        serverAddr = string.Concat(strArray);
                        ketQua = "Đã tồn tại Instance " + instanceName;
                        UpdateLabel(lblStatus, ketQua);
                        bgw.ReportProgress(100);
                        flag3 = true;
                    }
                    else
                    {
                        bgw.ReportProgress(40);
                        UpdateLabel(lblStatus, "Đang tiến hành cài đặt SQL Server 2008...");
                        string arguments = "";
                        arguments = "/q /ACTION=INSTALL /FEATURES=SQL /INSTANCENAME=" + instanceName + " /SQLSVCSTARTUPTYPE=Automatic /SQLSVCACCOUNT=\"NT AUTHORITY\\NETWORK SERVICE\" /SQLSYSADMINACCOUNTS=\"BUILTIN\\Administrators\" /AGTSVCACCOUNT=\"NT AUTHORITY\\Network Service\" /ADDCURRENTUSERASSQLADMIN=true /TCPENABLED=1 /NPENABLED=1 /SECURITYMODE=SQL /SAPWD=123456 /IACCEPTSQLSERVERLICENSETERMS=true /HIDECONSOLE";
                        ProcessStartInfo info = new ProcessStartInfo(path, arguments)
                        {
                            UseShellExecute = true,
                            Verb = "runas"
                        };
                        Process process = new Process
                        {
                            StartInfo = info
                        };
                        process.Start();
                        process.WaitForExit();
                        bgw.ReportProgress(70);
                        UpdateLabel(lblStatus, "Đang tiến hành kiểm tra SQL Service...");
                        bool flag2 = false;
                        controller = new ServiceController("MSSQL$" + instanceName);
                        try
                        {
                            serviceName = controller.ServiceName;
                            flag2 = true;
                        }
                        catch
                        {
                            flag2 = false;
                        }
                        if (!flag2)
                        {
                            strArray = new string[] { "Cài đặt SQL không thành công", Environment.NewLine, path, " ", arguments };
                            ketQua = string.Concat(strArray);
                            UpdateLabel(lblStatus, ketQua);
                            bgw.ReportProgress(100);
                            flag3 = false;
                        }
                        else
                        {
                            bgw.ReportProgress(80);
                            UpdateLabel(lblStatus, "Đang tiến hành kiểm tra cấu hình SQL Server...");
                            string str6 = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Microsoft SQL Server\" + instanceName + @"\MSSQLServer\SuperSocketNetLib\Tcp", true).GetValue("TcpPort").ToString();
                            bgw.ReportProgress(90);
                            UpdateLabel(lblStatus, "Đang tiến hành tạo rule firewall cho SQL Server...");
                            StringBuilder builder = new StringBuilder();
                            builder.AppendLine("netsh advfirewall firewall delete rule name = SQLPort_eBH");
                            builder.AppendLine("netsh advfirewall firewall add rule name = SQLPort_eBH dir = in protocol = tcp action = allow localport = " + str6 + " remoteip = localsubnet profile = ANY");
                            File.WriteAllText(Path.Combine(AppDir, "InstallRule.bat"), builder.ToString());
                            ProcessStartInfo info2 = new ProcessStartInfo(Path.Combine(AppDir, "InstallRule.bat"))
                            {
                                UseShellExecute = true,
                                Verb = "runas"
                            };
                            Process process2 = new Process
                            {
                                StartInfo = { CreateNoWindow = true }
                            };
                            process2.StartInfo = info2;
                            process2.Start();
                            process2.WaitForExit();
                            bgw.ReportProgress(100);
                            strArray = new string[] { machineName, @"\", instanceName, ",", str6 };
                            serverAddr = string.Concat(strArray);
                            ketQua = "Cài  đặt thành công";
                            UpdateLabel(lblStatus, ketQua);
                            bgw.ReportProgress(100);
                            flag3 = true;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                if (!ReferenceEquals(exception.InnerException, null))
                {
                    object[] objArray = new object[] { "Message : ", exception.Message, Environment.NewLine, "Inner : ", exception.InnerException, Environment.NewLine, "Stack Trace : ", exception.StackTrace };
                    ketQua = string.Concat(objArray);
                }
                else
                {
                    strArray = new string[] { "Message : ", exception.Message, Environment.NewLine, "Stack Trace : ", exception.StackTrace };
                    ketQua = string.Concat(strArray);
                }
                UpdateLabel(lblStatus, ketQua);
                bgw.ReportProgress(100);
                flag3 = false;
            }
            return flag3;
        }

        private static void UpdateLabel(Label lbl, string text)
        {
            Action method = () => lbl.Text = text;
            lbl.Invoke(method);
        }

        private static void UpdateProgressBar(ProgressBar pgb, int Value)
        {
            Action method = () => pgb.Value = Value;
            pgb.Invoke(method);
        }
    }
}
