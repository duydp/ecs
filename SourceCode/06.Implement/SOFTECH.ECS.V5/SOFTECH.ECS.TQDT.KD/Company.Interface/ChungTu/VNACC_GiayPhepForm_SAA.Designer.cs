﻿namespace Company.Interface
{
    partial class VNACC_GiayPhepForm_SAA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VNACC_GiayPhepForm_SAA));
            this.cmbMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdCangTrungGian1 = new Janus.Windows.UI.CommandBars.UICommand("cmdCangTrungGian");
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdLuu1 = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdChiThiHaiQuan1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHaiQuan");
            this.cmdTinhTrangThamTra1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTinhTrangThamTra");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdLuu = new Janus.Windows.UI.CommandBars.UICommand("cmdLuu");
            this.cmdKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdKhaiBao");
            this.cmdlayPhanHoi = new Janus.Windows.UI.CommandBars.UICommand("cmdlayPhanHoi");
            this.cmdChiThiHaiQuan = new Janus.Windows.UI.CommandBars.UICommand("cmdChiThiHaiQuan");
            this.cmdCangTrungGian = new Janus.Windows.UI.CommandBars.UICommand("cmdCangTrungGian");
            this.cmdTinhTrangThamTra = new Janus.Windows.UI.CommandBars.UICommand("cmdTinhTrangThamTra");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmdToolBar = new Janus.Windows.UI.CommandBars.UIRebar();
            this.txtLoaiGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtTenDonViCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtSoGiayPhepKQXL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label42 = new System.Windows.Forms.Label();
            this.txtSoDonXinCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.dtHieuLucDenNgayKQXL = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtHieuLucTuNgayKQXL = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayCapKQXL = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label23 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.txtMaKQXL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label41 = new System.Windows.Forms.Label();
            this.txtMaDonViCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucMaQuocGiaNhapKhau = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtMaThuongNhanNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtSoDangKyKinhDoanh = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenThuongNhanNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtMaBuuChinhNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.txtDiaChiThuongNhanNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.txtEmailNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoFaxNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoDienThoaiNhapKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.ucMaQuocGiaXuatKhau = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtTenThuongNhanXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMaBuuChinhXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtQuanHuyenXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoNhaTenDuongXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtEmailXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoFaxXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoDienThoaiXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTinhThanhPhoXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhuongXaXuatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaBuuChinhNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.txtDiaChiNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.txtEmailNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoFaxNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoDienThoaiNguoiKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.txtSoGiayChungNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiTab1 = new Janus.Windows.UI.Tab.UITab();
            this.uiTabPage1 = new Janus.Windows.UI.Tab.UITabPage();
            this.label24 = new System.Windows.Forms.Label();
            this.dtNgayKiemDich = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label88 = new System.Windows.Forms.Label();
            this.dtThoiGianKiemDich = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtBenhMienDich = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaDiemKiemDich = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ucCuaKhauNhap = new Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap();
            this.ucMaNuocNguoiKhai = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucPhuongTienVanChuyen = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucNuocQuaCanh = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.ucNuocXuatKhau = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend();
            this.label26 = new System.Windows.Forms.Label();
            this.dtNgayTiemPhong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ucPhanLoaiTraCuu = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucChucNangChungTu = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label27 = new System.Windows.Forms.Label();
            this.uiTabPage2 = new Janus.Windows.UI.Tab.UITabPage();
            this.ucDonViTinhKhoiLuongHangCanBoc = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucDonViTinhKhoiLuongHangCangDauTien = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.txtKhoiLuongHangCanBoc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtKhoiLuongHangCangDauTien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label87 = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.ucDonViTinhSoLuongHangCanBoc = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucDonViTinhSoLuongHangCangDauTien = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.ucQuocTich = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.label70 = new System.Windows.Forms.Label();
            this.pnlHoSoKemTheo = new System.Windows.Forms.Panel();
            this.label40 = new System.Windows.Forms.Label();
            this.txt10 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label35 = new System.Windows.Forms.Label();
            this.txt9 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txt8 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label33 = new System.Windows.Forms.Label();
            this.txt7 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txt6 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txt5 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label37 = new System.Windows.Forms.Label();
            this.txt4 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label39 = new System.Windows.Forms.Label();
            this.txt3 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label46 = new System.Windows.Forms.Label();
            this.txt2 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txt1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblHoSoKemTheo = new System.Windows.Forms.Label();
            this.txtLyDoKhongDat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtKetQuaKiemTra = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtHoSoLienQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label58 = new System.Windows.Forms.Label();
            this.txtSoLuongHangCanBoc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoLuongHangCangDauTien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label86 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.txtSoHanhKhach = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label74 = new System.Windows.Forms.Label();
            this.txtSoThuyenVien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label73 = new System.Windows.Forms.Label();
            this.txtSoBanCanCap = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label31 = new System.Windows.Forms.Label();
            this.dtNgayRoiCanh = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label78 = new System.Windows.Forms.Label();
            this.dtThoiGianGiamSat = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.dtNgayGiamSat = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label25 = new System.Windows.Forms.Label();
            this.txtDiaDiemNuoiTrong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtNongDo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtKhuTrung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.txtTenBacSi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenThuyenTruong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label72 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.txtCangBocHangDauTien = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label77 = new System.Windows.Forms.Label();
            this.txtTenHangCanBoc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label85 = new System.Windows.Forms.Label();
            this.txtTenHangCangDauTien = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label79 = new System.Windows.Forms.Label();
            this.txtCangDenTiepTheo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label76 = new System.Windows.Forms.Label();
            this.txtCangRoiCuoiCung = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label75 = new System.Windows.Forms.Label();
            this.txtTenTau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtDaiDienDonViCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label68 = new System.Windows.Forms.Label();
            this.txtNguoiKiemTra = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label65 = new System.Windows.Forms.Label();
            this.txtNoiChuyenDen = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label64 = new System.Windows.Forms.Label();
            this.txtVatDungKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label63 = new System.Windows.Forms.Label();
            this.txtDiaDiemGiamSat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label48 = new System.Windows.Forms.Label();
            this.numericEditBox1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label82 = new System.Windows.Forms.Label();
            this.numericEditBox2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label83 = new System.Windows.Forms.Label();
            this.editBox1 = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label84 = new System.Windows.Forms.Label();
            this.ucCategory1 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            this.ucCategory2 = new Company.KDT.SHARE.VNACCS.Controls.ucCategory();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).BeginInit();
            this.uiPanelGuide.SuspendLayout();
            this.uiPanelGuideContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToolBar)).BeginInit();
            this.cmdToolBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).BeginInit();
            this.uiTab1.SuspendLayout();
            this.uiTabPage1.SuspendLayout();
            this.uiTabPage2.SuspendLayout();
            this.pnlHoSoKemTheo.SuspendLayout();
            this.SuspendLayout();
            this.uiPanelGuide.Id = new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed");
            this.uiPanelManager.Panels.Add(this.uiPanelGuide);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager.BeginPanelInfo();
            this.uiPanelManager.AddDockPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(200, 807), true);
            this.uiPanelManager.AddFloatingPanelInfo(new System.Guid("8838613e-1ead-4dfa-8f98-a5042d5272ed"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager.EndPanelInfo();
            // 
            // uiPanelGuide
            // 
            this.uiPanelGuide.Location = new System.Drawing.Point(3, 35);
            this.uiPanelGuide.Size = new System.Drawing.Size(200, 807);
            // 
            // txtGuide
            // 
            this.txtGuide.Size = new System.Drawing.Size(194, 783);
            // 
            // uiPanelGuideContainer
            // 
            this.uiPanelGuideContainer.Size = new System.Drawing.Size(194, 783);
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiTab1);
            this.grbMain.Location = new System.Drawing.Point(203, 35);
            this.grbMain.Size = new System.Drawing.Size(767, 807);
            // 
            // cmbMain
            // 
            this.cmbMain.BottomRebar = this.BottomRebar1;
            this.cmbMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmbMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang,
            this.cmdLuu,
            this.cmdKhaiBao,
            this.cmdlayPhanHoi,
            this.cmdChiThiHaiQuan,
            this.cmdCangTrungGian,
            this.cmdTinhTrangThamTra});
            this.cmbMain.ContainerControl = this;
            this.cmbMain.Id = new System.Guid("7efa1b81-a632-4adb-89e9-9280c46f7b4f");
            this.cmbMain.LeftRebar = this.LeftRebar1;
            this.cmbMain.RightRebar = this.RightRebar1;
            this.cmbMain.Tag = null;
            this.cmbMain.TopRebar = this.cmdToolBar;
            this.cmbMain.VisualStyleManager = this.vsmMain;
            this.cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmbMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmbMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmbMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdCangTrungGian1,
            this.cmdThemHang1,
            this.cmdLuu1,
            this.Separator1,
            this.cmdKhaiBao1,
            this.Separator2,
            this.cmdChiThiHaiQuan1,
            this.cmdTinhTrangThamTra1});
            this.uiCommandBar1.FullRow = true;
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(973, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            this.uiCommandBar1.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            // 
            // cmdCangTrungGian1
            // 
            this.cmdCangTrungGian1.Key = "cmdCangTrungGian";
            this.cmdCangTrungGian1.Name = "cmdCangTrungGian1";
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // cmdLuu1
            // 
            this.cmdLuu1.Key = "cmdLuu";
            this.cmdLuu1.Name = "cmdLuu1";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdKhaiBao1
            // 
            this.cmdKhaiBao1.Key = "cmdKhaiBao";
            this.cmdKhaiBao1.Name = "cmdKhaiBao1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // cmdChiThiHaiQuan1
            // 
            this.cmdChiThiHaiQuan1.Key = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan1.Name = "cmdChiThiHaiQuan1";
            // 
            // cmdTinhTrangThamTra1
            // 
            this.cmdTinhTrangThamTra1.Key = "cmdTinhTrangThamTra";
            this.cmdTinhTrangThamTra1.Name = "cmdTinhTrangThamTra1";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThemHang.Icon")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdLuu
            // 
            this.cmdLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdLuu.Icon")));
            this.cmdLuu.Key = "cmdLuu";
            this.cmdLuu.Name = "cmdLuu";
            this.cmdLuu.Text = "Lưu";
            // 
            // cmdKhaiBao
            // 
            this.cmdKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdKhaiBao.Icon")));
            this.cmdKhaiBao.Key = "cmdKhaiBao";
            this.cmdKhaiBao.Name = "cmdKhaiBao";
            this.cmdKhaiBao.Text = "Khai báo";
            // 
            // cmdlayPhanHoi
            // 
            this.cmdlayPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdlayPhanHoi.Icon")));
            this.cmdlayPhanHoi.Key = "cmdlayPhanHoi";
            this.cmdlayPhanHoi.Name = "cmdlayPhanHoi";
            this.cmdlayPhanHoi.Text = "Lấy phản hồi";
            // 
            // cmdChiThiHaiQuan
            // 
            this.cmdChiThiHaiQuan.Image = ((System.Drawing.Image)(resources.GetObject("cmdChiThiHaiQuan.Image")));
            this.cmdChiThiHaiQuan.Key = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan.Name = "cmdChiThiHaiQuan";
            this.cmdChiThiHaiQuan.Text = "Chỉ thị Hải quan";
            // 
            // cmdCangTrungGian
            // 
            this.cmdCangTrungGian.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdCangTrungGian.Icon")));
            this.cmdCangTrungGian.Key = "cmdCangTrungGian";
            this.cmdCangTrungGian.Name = "cmdCangTrungGian";
            this.cmdCangTrungGian.Text = "Cảng trung gian";
            // 
            // cmdTinhTrangThamTra
            // 
            this.cmdTinhTrangThamTra.Image = ((System.Drawing.Image)(resources.GetObject("cmdTinhTrangThamTra.Image")));
            this.cmdTinhTrangThamTra.Key = "cmdTinhTrangThamTra";
            this.cmdTinhTrangThamTra.Name = "cmdTinhTrangThamTra";
            this.cmdTinhTrangThamTra.Text = "Thông tin tình trạng thẩm tra/ kiểm tra";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmbMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmbMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmdToolBar
            // 
            this.cmdToolBar.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdToolBar.CommandManager = this.cmbMain;
            this.cmdToolBar.Controls.Add(this.uiCommandBar1);
            this.cmdToolBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmdToolBar.Name = "cmdToolBar";
            this.cmdToolBar.Size = new System.Drawing.Size(973, 32);
            // 
            // txtLoaiGiayPhep
            // 
            this.txtLoaiGiayPhep.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtLoaiGiayPhep.Location = new System.Drawing.Point(434, 144);
            this.txtLoaiGiayPhep.Name = "txtLoaiGiayPhep";
            this.txtLoaiGiayPhep.Size = new System.Drawing.Size(137, 21);
            this.txtLoaiGiayPhep.TabIndex = 11;
            this.txtLoaiGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Location = new System.Drawing.Point(14, 152);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(126, 13);
            this.label19.TabIndex = 6;
            this.label19.Text = "Chức năng của chứng từ";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Location = new System.Drawing.Point(442, 237);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(52, 13);
            this.label45.TabIndex = 21;
            this.label45.Text = "đến ngày";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Location = new System.Drawing.Point(131, 237);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(150, 13);
            this.label44.TabIndex = 19;
            this.label44.Text = "Giấy phép có hiệu lực từ ngày";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Location = new System.Drawing.Point(415, 210);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(79, 13);
            this.label43.TabIndex = 17;
            this.label43.Text = "Ngày cấp phép";
            // 
            // txtTenDonViCapPhep
            // 
            this.txtTenDonViCapPhep.Location = new System.Drawing.Point(265, 173);
            this.txtTenDonViCapPhep.Name = "txtTenDonViCapPhep";
            this.txtTenDonViCapPhep.Size = new System.Drawing.Size(465, 21);
            this.txtTenDonViCapPhep.TabIndex = 13;
            // 
            // txtTenNguoiKhai
            // 
            this.txtTenNguoiKhai.Location = new System.Drawing.Point(265, 8);
            this.txtTenNguoiKhai.Name = "txtTenNguoiKhai";
            this.txtTenNguoiKhai.Size = new System.Drawing.Size(465, 21);
            this.txtTenNguoiKhai.TabIndex = 1;
            // 
            // txtMaNguoiKhai
            // 
            this.txtMaNguoiKhai.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaNguoiKhai.Location = new System.Drawing.Point(145, 8);
            this.txtMaNguoiKhai.Name = "txtMaNguoiKhai";
            this.txtMaNguoiKhai.Size = new System.Drawing.Size(114, 21);
            this.txtMaNguoiKhai.TabIndex = 0;
            this.txtMaNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(14, 12);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Mã người khai";
            // 
            // txtSoGiayPhepKQXL
            // 
            this.txtSoGiayPhepKQXL.Enabled = false;
            this.txtSoGiayPhepKQXL.Location = new System.Drawing.Point(287, 206);
            this.txtSoGiayPhepKQXL.Name = "txtSoGiayPhepKQXL";
            this.txtSoGiayPhepKQXL.Size = new System.Drawing.Size(109, 21);
            this.txtSoGiayPhepKQXL.TabIndex = 15;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Location = new System.Drawing.Point(212, 210);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(69, 13);
            this.label42.TabIndex = 15;
            this.label42.Text = "Số giấy phép";
            // 
            // txtSoDonXinCapPhep
            // 
            this.txtSoDonXinCapPhep.Location = new System.Drawing.Point(145, 120);
            this.txtSoDonXinCapPhep.Name = "txtSoDonXinCapPhep";
            this.txtSoDonXinCapPhep.Size = new System.Drawing.Size(114, 21);
            this.txtSoDonXinCapPhep.TabIndex = 8;
            this.txtSoDonXinCapPhep.VisualStyleManager = this.vsmMain;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(14, 124);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(104, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "Số đơn xin cấp phép";
            // 
            // dtHieuLucDenNgayKQXL
            // 
            // 
            // 
            // 
            this.dtHieuLucDenNgayKQXL.DropDownCalendar.Name = "";
            this.dtHieuLucDenNgayKQXL.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtHieuLucDenNgayKQXL.Enabled = false;
            this.dtHieuLucDenNgayKQXL.IsNullDate = true;
            this.dtHieuLucDenNgayKQXL.Location = new System.Drawing.Point(500, 233);
            this.dtHieuLucDenNgayKQXL.Name = "dtHieuLucDenNgayKQXL";
            this.dtHieuLucDenNgayKQXL.Size = new System.Drawing.Size(100, 21);
            this.dtHieuLucDenNgayKQXL.TabIndex = 18;
            this.dtHieuLucDenNgayKQXL.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtHieuLucTuNgayKQXL
            // 
            // 
            // 
            // 
            this.dtHieuLucTuNgayKQXL.DropDownCalendar.Name = "";
            this.dtHieuLucTuNgayKQXL.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtHieuLucTuNgayKQXL.Enabled = false;
            this.dtHieuLucTuNgayKQXL.IsNullDate = true;
            this.dtHieuLucTuNgayKQXL.Location = new System.Drawing.Point(287, 233);
            this.dtHieuLucTuNgayKQXL.Name = "dtHieuLucTuNgayKQXL";
            this.dtHieuLucTuNgayKQXL.Size = new System.Drawing.Size(109, 21);
            this.dtHieuLucTuNgayKQXL.TabIndex = 17;
            this.dtHieuLucTuNgayKQXL.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtNgayCapKQXL
            // 
            // 
            // 
            // 
            this.dtNgayCapKQXL.DropDownCalendar.Name = "";
            this.dtNgayCapKQXL.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayCapKQXL.Enabled = false;
            this.dtNgayCapKQXL.IsNullDate = true;
            this.dtNgayCapKQXL.Location = new System.Drawing.Point(500, 206);
            this.dtNgayCapKQXL.Name = "dtNgayCapKQXL";
            this.dtNgayCapKQXL.Size = new System.Drawing.Size(100, 21);
            this.dtNgayCapKQXL.TabIndex = 16;
            this.dtNgayCapKQXL.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Location = new System.Drawing.Point(340, 122);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(88, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "Phân loại tra cứu";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(352, 149);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(76, 13);
            this.label17.TabIndex = 8;
            this.label17.Text = "Loại giấy phép";
            // 
            // txtMaKQXL
            // 
            this.txtMaKQXL.Enabled = false;
            this.txtMaKQXL.Location = new System.Drawing.Point(145, 206);
            this.txtMaKQXL.Name = "txtMaKQXL";
            this.txtMaKQXL.Size = new System.Drawing.Size(52, 21);
            this.txtMaKQXL.TabIndex = 14;
            this.txtMaKQXL.VisualStyleManager = this.vsmMain;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Location = new System.Drawing.Point(14, 210);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(87, 13);
            this.label41.TabIndex = 13;
            this.label41.Text = "Mã kết quả xử lý";
            // 
            // txtMaDonViCapPhep
            // 
            this.txtMaDonViCapPhep.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaDonViCapPhep.Location = new System.Drawing.Point(145, 173);
            this.txtMaDonViCapPhep.Name = "txtMaDonViCapPhep";
            this.txtMaDonViCapPhep.Size = new System.Drawing.Size(114, 21);
            this.txtMaDonViCapPhep.TabIndex = 12;
            this.txtMaDonViCapPhep.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(14, 178);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(100, 13);
            this.label14.TabIndex = 10;
            this.label14.Text = "Mã đơn vị cấp phép";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.ucMaQuocGiaNhapKhau);
            this.uiGroupBox4.Controls.Add(this.txtMaThuongNhanNhapKhau);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.txtSoDangKyKinhDoanh);
            this.uiGroupBox4.Controls.Add(this.txtTenThuongNhanNhapKhau);
            this.uiGroupBox4.Controls.Add(this.label20);
            this.uiGroupBox4.Controls.Add(this.txtMaBuuChinhNhapKhau);
            this.uiGroupBox4.Controls.Add(this.label52);
            this.uiGroupBox4.Controls.Add(this.label53);
            this.uiGroupBox4.Controls.Add(this.label54);
            this.uiGroupBox4.Controls.Add(this.label55);
            this.uiGroupBox4.Controls.Add(this.txtDiaChiThuongNhanNhapKhau);
            this.uiGroupBox4.Controls.Add(this.label56);
            this.uiGroupBox4.Controls.Add(this.label57);
            this.uiGroupBox4.Controls.Add(this.txtEmailNhapKhau);
            this.uiGroupBox4.Controls.Add(this.txtSoFaxNhapKhau);
            this.uiGroupBox4.Controls.Add(this.txtSoDienThoaiNhapKhau);
            this.uiGroupBox4.Location = new System.Drawing.Point(9, 479);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(738, 153);
            this.uiGroupBox4.TabIndex = 22;
            this.uiGroupBox4.Text = "Tổ chức/ cá nhân Nhập khẩu";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // ucMaQuocGiaNhapKhau
            // 
            this.ucMaQuocGiaNhapKhau.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaQuocGiaNhapKhau.Code = "";
            this.ucMaQuocGiaNhapKhau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaQuocGiaNhapKhau.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaQuocGiaNhapKhau.IsValidate = true;
            this.ucMaQuocGiaNhapKhau.Location = new System.Drawing.Point(115, 126);
            this.ucMaQuocGiaNhapKhau.Name = "ucMaQuocGiaNhapKhau";
            this.ucMaQuocGiaNhapKhau.Name_VN = "";
            this.ucMaQuocGiaNhapKhau.SetValidate = false;
            this.ucMaQuocGiaNhapKhau.ShowColumnCode = true;
            this.ucMaQuocGiaNhapKhau.ShowColumnName = false;
            this.ucMaQuocGiaNhapKhau.Size = new System.Drawing.Size(58, 26);
            this.ucMaQuocGiaNhapKhau.TabIndex = 9;
            this.ucMaQuocGiaNhapKhau.TagName = "";
            this.ucMaQuocGiaNhapKhau.WhereCondition = "";
            this.ucMaQuocGiaNhapKhau.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtMaThuongNhanNhapKhau
            // 
            this.txtMaThuongNhanNhapKhau.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaThuongNhanNhapKhau.Location = new System.Drawing.Point(115, 19);
            this.txtMaThuongNhanNhapKhau.Name = "txtMaThuongNhanNhapKhau";
            this.txtMaThuongNhanNhapKhau.Size = new System.Drawing.Size(162, 21);
            this.txtMaThuongNhanNhapKhau.TabIndex = 1;
            this.txtMaThuongNhanNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(6, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(21, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Mã";
            // 
            // txtSoDangKyKinhDoanh
            // 
            this.txtSoDangKyKinhDoanh.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoDangKyKinhDoanh.Location = new System.Drawing.Point(115, 46);
            this.txtSoDangKyKinhDoanh.Name = "txtSoDangKyKinhDoanh";
            this.txtSoDangKyKinhDoanh.Size = new System.Drawing.Size(234, 21);
            this.txtSoDangKyKinhDoanh.TabIndex = 3;
            this.txtSoDangKyKinhDoanh.VisualStyleManager = this.vsmMain;
            // 
            // txtTenThuongNhanNhapKhau
            // 
            this.txtTenThuongNhanNhapKhau.Location = new System.Drawing.Point(283, 19);
            this.txtTenThuongNhanNhapKhau.Name = "txtTenThuongNhanNhapKhau";
            this.txtTenThuongNhanNhapKhau.Size = new System.Drawing.Size(446, 21);
            this.txtTenThuongNhanNhapKhau.TabIndex = 3;
            this.txtTenThuongNhanNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 50);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(115, 13);
            this.label20.TabIndex = 2;
            this.label20.Text = "Số đăng ký kinh doanh";
            // 
            // txtMaBuuChinhNhapKhau
            // 
            this.txtMaBuuChinhNhapKhau.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaBuuChinhNhapKhau.Location = new System.Drawing.Point(115, 73);
            this.txtMaBuuChinhNhapKhau.Name = "txtMaBuuChinhNhapKhau";
            this.txtMaBuuChinhNhapKhau.Size = new System.Drawing.Size(162, 21);
            this.txtMaBuuChinhNhapKhau.TabIndex = 5;
            this.txtMaBuuChinhNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(355, 131);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(40, 13);
            this.label52.TabIndex = 12;
            this.label52.Text = "Số Fax";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(6, 104);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(39, 13);
            this.label53.TabIndex = 6;
            this.label53.Text = "Địa chỉ";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(546, 132);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(31, 13);
            this.label54.TabIndex = 14;
            this.label54.Text = "Email";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(6, 78);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(71, 13);
            this.label55.TabIndex = 4;
            this.label55.Text = "Mã bưu chính";
            // 
            // txtDiaChiThuongNhanNhapKhau
            // 
            this.txtDiaChiThuongNhanNhapKhau.Location = new System.Drawing.Point(115, 100);
            this.txtDiaChiThuongNhanNhapKhau.Name = "txtDiaChiThuongNhanNhapKhau";
            this.txtDiaChiThuongNhanNhapKhau.Size = new System.Drawing.Size(614, 21);
            this.txtDiaChiThuongNhanNhapKhau.TabIndex = 7;
            this.txtDiaChiThuongNhanNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(179, 131);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(56, 13);
            this.label56.TabIndex = 10;
            this.label56.Text = "Điện thoại";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(6, 131);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(64, 13);
            this.label57.TabIndex = 8;
            this.label57.Text = "Mã quốc gia";
            // 
            // txtEmailNhapKhau
            // 
            this.txtEmailNhapKhau.Location = new System.Drawing.Point(579, 127);
            this.txtEmailNhapKhau.Name = "txtEmailNhapKhau";
            this.txtEmailNhapKhau.Size = new System.Drawing.Size(150, 21);
            this.txtEmailNhapKhau.TabIndex = 15;
            this.txtEmailNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtSoFaxNhapKhau
            // 
            this.txtSoFaxNhapKhau.Location = new System.Drawing.Point(404, 127);
            this.txtSoFaxNhapKhau.Name = "txtSoFaxNhapKhau";
            this.txtSoFaxNhapKhau.Size = new System.Drawing.Size(137, 21);
            this.txtSoFaxNhapKhau.TabIndex = 13;
            this.txtSoFaxNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtSoDienThoaiNhapKhau
            // 
            this.txtSoDienThoaiNhapKhau.Location = new System.Drawing.Point(236, 127);
            this.txtSoDienThoaiNhapKhau.Name = "txtSoDienThoaiNhapKhau";
            this.txtSoDienThoaiNhapKhau.Size = new System.Drawing.Size(113, 21);
            this.txtSoDienThoaiNhapKhau.TabIndex = 11;
            this.txtSoDienThoaiNhapKhau.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.ucMaQuocGiaXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtTenThuongNhanXuatKhau);
            this.uiGroupBox3.Controls.Add(this.label2);
            this.uiGroupBox3.Controls.Add(this.txtMaBuuChinhXuatKhau);
            this.uiGroupBox3.Controls.Add(this.label21);
            this.uiGroupBox3.Controls.Add(this.label22);
            this.uiGroupBox3.Controls.Add(this.label11);
            this.uiGroupBox3.Controls.Add(this.label1);
            this.uiGroupBox3.Controls.Add(this.label6);
            this.uiGroupBox3.Controls.Add(this.label8);
            this.uiGroupBox3.Controls.Add(this.label5);
            this.uiGroupBox3.Controls.Add(this.txtQuanHuyenXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtSoNhaTenDuongXuatKhau);
            this.uiGroupBox3.Controls.Add(this.label9);
            this.uiGroupBox3.Controls.Add(this.label7);
            this.uiGroupBox3.Controls.Add(this.txtEmailXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtSoFaxXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtSoDienThoaiXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtTinhThanhPhoXuatKhau);
            this.uiGroupBox3.Controls.Add(this.txtPhuongXaXuatKhau);
            this.uiGroupBox3.Location = new System.Drawing.Point(9, 287);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(738, 154);
            this.uiGroupBox3.TabIndex = 20;
            this.uiGroupBox3.Text = "Tổ chức/ cá nhân Xuất khẩu";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // ucMaQuocGiaXuatKhau
            // 
            this.ucMaQuocGiaXuatKhau.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaQuocGiaXuatKhau.Code = "";
            this.ucMaQuocGiaXuatKhau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaQuocGiaXuatKhau.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaQuocGiaXuatKhau.IsValidate = true;
            this.ucMaQuocGiaXuatKhau.Location = new System.Drawing.Point(115, 126);
            this.ucMaQuocGiaXuatKhau.Name = "ucMaQuocGiaXuatKhau";
            this.ucMaQuocGiaXuatKhau.Name_VN = "";
            this.ucMaQuocGiaXuatKhau.SetValidate = false;
            this.ucMaQuocGiaXuatKhau.ShowColumnCode = true;
            this.ucMaQuocGiaXuatKhau.ShowColumnName = false;
            this.ucMaQuocGiaXuatKhau.Size = new System.Drawing.Size(58, 26);
            this.ucMaQuocGiaXuatKhau.TabIndex = 6;
            this.ucMaQuocGiaXuatKhau.TagName = "";
            this.ucMaQuocGiaXuatKhau.WhereCondition = "";
            this.ucMaQuocGiaXuatKhau.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtTenThuongNhanXuatKhau
            // 
            this.txtTenThuongNhanXuatKhau.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenThuongNhanXuatKhau.Location = new System.Drawing.Point(115, 20);
            this.txtTenThuongNhanXuatKhau.Name = "txtTenThuongNhanXuatKhau";
            this.txtTenThuongNhanXuatKhau.Size = new System.Drawing.Size(614, 21);
            this.txtTenThuongNhanXuatKhau.TabIndex = 0;
            this.txtTenThuongNhanXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(25, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên";
            // 
            // txtMaBuuChinhXuatKhau
            // 
            this.txtMaBuuChinhXuatKhau.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaBuuChinhXuatKhau.Location = new System.Drawing.Point(115, 47);
            this.txtMaBuuChinhXuatKhau.Name = "txtMaBuuChinhXuatKhau";
            this.txtMaBuuChinhXuatKhau.Size = new System.Drawing.Size(162, 21);
            this.txtMaBuuChinhXuatKhau.TabIndex = 1;
            this.txtMaBuuChinhXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 105);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(70, 13);
            this.label21.TabIndex = 8;
            this.label21.Text = "Quận, huyện";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(414, 105);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(83, 13);
            this.label22.TabIndex = 10;
            this.label22.Text = "Tỉnh, thành phố";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(355, 131);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 13);
            this.label11.TabIndex = 16;
            this.label11.Text = "Số Fax";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(417, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Phường, xã";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 78);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(97, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Số nhà, tên đường";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(546, 132);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(31, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Email";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Mã bưu chính";
            // 
            // txtQuanHuyenXuatKhau
            // 
            this.txtQuanHuyenXuatKhau.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtQuanHuyenXuatKhau.Location = new System.Drawing.Point(115, 101);
            this.txtQuanHuyenXuatKhau.Name = "txtQuanHuyenXuatKhau";
            this.txtQuanHuyenXuatKhau.Size = new System.Drawing.Size(296, 21);
            this.txtQuanHuyenXuatKhau.TabIndex = 4;
            this.txtQuanHuyenXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtSoNhaTenDuongXuatKhau
            // 
            this.txtSoNhaTenDuongXuatKhau.Location = new System.Drawing.Point(115, 74);
            this.txtSoNhaTenDuongXuatKhau.Name = "txtSoNhaTenDuongXuatKhau";
            this.txtSoNhaTenDuongXuatKhau.Size = new System.Drawing.Size(296, 21);
            this.txtSoNhaTenDuongXuatKhau.TabIndex = 2;
            this.txtSoNhaTenDuongXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(179, 131);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 14;
            this.label9.Text = "Điện thoại";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 131);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(64, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Mã quốc gia";
            // 
            // txtEmailXuatKhau
            // 
            this.txtEmailXuatKhau.Location = new System.Drawing.Point(583, 127);
            this.txtEmailXuatKhau.Name = "txtEmailXuatKhau";
            this.txtEmailXuatKhau.Size = new System.Drawing.Size(146, 21);
            this.txtEmailXuatKhau.TabIndex = 9;
            this.txtEmailXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtSoFaxXuatKhau
            // 
            this.txtSoFaxXuatKhau.Location = new System.Drawing.Point(404, 127);
            this.txtSoFaxXuatKhau.Name = "txtSoFaxXuatKhau";
            this.txtSoFaxXuatKhau.Size = new System.Drawing.Size(137, 21);
            this.txtSoFaxXuatKhau.TabIndex = 8;
            this.txtSoFaxXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtSoDienThoaiXuatKhau
            // 
            this.txtSoDienThoaiXuatKhau.Location = new System.Drawing.Point(236, 127);
            this.txtSoDienThoaiXuatKhau.Name = "txtSoDienThoaiXuatKhau";
            this.txtSoDienThoaiXuatKhau.Size = new System.Drawing.Size(113, 21);
            this.txtSoDienThoaiXuatKhau.TabIndex = 7;
            this.txtSoDienThoaiXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtTinhThanhPhoXuatKhau
            // 
            this.txtTinhThanhPhoXuatKhau.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTinhThanhPhoXuatKhau.Location = new System.Drawing.Point(499, 101);
            this.txtTinhThanhPhoXuatKhau.Name = "txtTinhThanhPhoXuatKhau";
            this.txtTinhThanhPhoXuatKhau.Size = new System.Drawing.Size(230, 21);
            this.txtTinhThanhPhoXuatKhau.TabIndex = 5;
            this.txtTinhThanhPhoXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtPhuongXaXuatKhau
            // 
            this.txtPhuongXaXuatKhau.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPhuongXaXuatKhau.Location = new System.Drawing.Point(499, 74);
            this.txtPhuongXaXuatKhau.Name = "txtPhuongXaXuatKhau";
            this.txtPhuongXaXuatKhau.Size = new System.Drawing.Size(230, 21);
            this.txtPhuongXaXuatKhau.TabIndex = 3;
            this.txtPhuongXaXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(8, 645);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Nước quá cảnh";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Location = new System.Drawing.Point(18, 454);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(83, 13);
            this.label38.TabIndex = 5;
            this.label38.Text = "Nước xuất khẩu";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Location = new System.Drawing.Point(14, 265);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Số hợp đồng";
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(145, 260);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(251, 21);
            this.txtSoHopDong.TabIndex = 19;
            this.txtSoHopDong.VisualStyleManager = this.vsmMain;
            // 
            // txtMaBuuChinhNguoiKhai
            // 
            this.txtMaBuuChinhNguoiKhai.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaBuuChinhNguoiKhai.Location = new System.Drawing.Point(145, 35);
            this.txtMaBuuChinhNguoiKhai.Name = "txtMaBuuChinhNguoiKhai";
            this.txtMaBuuChinhNguoiKhai.Size = new System.Drawing.Size(114, 21);
            this.txtMaBuuChinhNguoiKhai.TabIndex = 2;
            this.txtMaBuuChinhNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Location = new System.Drawing.Point(385, 93);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(40, 13);
            this.label49.TabIndex = 31;
            this.label49.Text = "Số Fax";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Location = new System.Drawing.Point(14, 66);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(39, 13);
            this.label50.TabIndex = 25;
            this.label50.Text = "Địa chỉ";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Location = new System.Drawing.Point(576, 94);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(31, 13);
            this.label51.TabIndex = 33;
            this.label51.Text = "Email";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Location = new System.Drawing.Point(14, 40);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(71, 13);
            this.label59.TabIndex = 23;
            this.label59.Text = "Mã bưu chính";
            // 
            // txtDiaChiNguoiKhai
            // 
            this.txtDiaChiNguoiKhai.Location = new System.Drawing.Point(145, 62);
            this.txtDiaChiNguoiKhai.Name = "txtDiaChiNguoiKhai";
            this.txtDiaChiNguoiKhai.Size = new System.Drawing.Size(585, 21);
            this.txtDiaChiNguoiKhai.TabIndex = 3;
            this.txtDiaChiNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Location = new System.Drawing.Point(209, 93);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(56, 13);
            this.label60.TabIndex = 29;
            this.label60.Text = "Điện thoại";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Location = new System.Drawing.Point(14, 93);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(64, 13);
            this.label61.TabIndex = 27;
            this.label61.Text = "Mã quốc gia";
            // 
            // txtEmailNguoiKhai
            // 
            this.txtEmailNguoiKhai.Location = new System.Drawing.Point(613, 89);
            this.txtEmailNguoiKhai.Name = "txtEmailNguoiKhai";
            this.txtEmailNguoiKhai.Size = new System.Drawing.Size(117, 21);
            this.txtEmailNguoiKhai.TabIndex = 7;
            this.txtEmailNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // txtSoFaxNguoiKhai
            // 
            this.txtSoFaxNguoiKhai.Location = new System.Drawing.Point(434, 89);
            this.txtSoFaxNguoiKhai.Name = "txtSoFaxNguoiKhai";
            this.txtSoFaxNguoiKhai.Size = new System.Drawing.Size(137, 21);
            this.txtSoFaxNguoiKhai.TabIndex = 6;
            this.txtSoFaxNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // txtSoDienThoaiNguoiKhai
            // 
            this.txtSoDienThoaiNguoiKhai.Location = new System.Drawing.Point(266, 89);
            this.txtSoDienThoaiNguoiKhai.Name = "txtSoDienThoaiNguoiKhai";
            this.txtSoDienThoaiNguoiKhai.Size = new System.Drawing.Size(113, 21);
            this.txtSoDienThoaiNguoiKhai.TabIndex = 5;
            this.txtSoDienThoaiNguoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(458, 645);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(124, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Phương tiện vận chuyển";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.Location = new System.Drawing.Point(8, 672);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(80, 13);
            this.label62.TabIndex = 37;
            this.label62.Text = "Cửa khẩu nhập";
            // 
            // txtSoGiayChungNhan
            // 
            this.txtSoGiayChungNhan.Location = new System.Drawing.Point(588, 667);
            this.txtSoGiayChungNhan.Name = "txtSoGiayChungNhan";
            this.txtSoGiayChungNhan.Size = new System.Drawing.Size(150, 21);
            this.txtSoGiayChungNhan.TabIndex = 26;
            this.txtSoGiayChungNhan.VisualStyleManager = this.vsmMain;
            // 
            // uiTab1
            // 
            this.uiTab1.BackColor = System.Drawing.Color.Transparent;
            this.uiTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiTab1.Location = new System.Drawing.Point(0, 0);
            this.uiTab1.Name = "uiTab1";
            this.uiTab1.Size = new System.Drawing.Size(767, 807);
            this.uiTab1.TabIndex = 0;
            this.uiTab1.TabPages.AddRange(new Janus.Windows.UI.Tab.UITabPage[] {
            this.uiTabPage1,
            this.uiTabPage2});
            this.uiTab1.VisualStyle = Janus.Windows.UI.Tab.TabVisualStyle.Office2007;
            // 
            // uiTabPage1
            // 
            this.uiTabPage1.AutoScroll = true;
            this.uiTabPage1.Controls.Add(this.label24);
            this.uiTabPage1.Controls.Add(this.dtNgayKiemDich);
            this.uiTabPage1.Controls.Add(this.label88);
            this.uiTabPage1.Controls.Add(this.dtThoiGianKiemDich);
            this.uiTabPage1.Controls.Add(this.txtBenhMienDich);
            this.uiTabPage1.Controls.Add(this.txtDiaDiemKiemDich);
            this.uiTabPage1.Controls.Add(this.label4);
            this.uiTabPage1.Controls.Add(this.label10);
            this.uiTabPage1.Controls.Add(this.ucCuaKhauNhap);
            this.uiTabPage1.Controls.Add(this.label12);
            this.uiTabPage1.Controls.Add(this.label62);
            this.uiTabPage1.Controls.Add(this.ucMaNuocNguoiKhai);
            this.uiTabPage1.Controls.Add(this.ucPhuongTienVanChuyen);
            this.uiTabPage1.Controls.Add(this.txtMaNguoiKhai);
            this.uiTabPage1.Controls.Add(this.ucNuocQuaCanh);
            this.uiTabPage1.Controls.Add(this.txtMaBuuChinhNguoiKhai);
            this.uiTabPage1.Controls.Add(this.uiGroupBox4);
            this.uiTabPage1.Controls.Add(this.ucNuocXuatKhau);
            this.uiTabPage1.Controls.Add(this.label26);
            this.uiTabPage1.Controls.Add(this.label16);
            this.uiTabPage1.Controls.Add(this.label14);
            this.uiTabPage1.Controls.Add(this.label3);
            this.uiTabPage1.Controls.Add(this.label49);
            this.uiTabPage1.Controls.Add(this.txtMaDonViCapPhep);
            this.uiTabPage1.Controls.Add(this.label50);
            this.uiTabPage1.Controls.Add(this.txtSoGiayChungNhan);
            this.uiTabPage1.Controls.Add(this.uiGroupBox3);
            this.uiTabPage1.Controls.Add(this.label41);
            this.uiTabPage1.Controls.Add(this.label51);
            this.uiTabPage1.Controls.Add(this.label38);
            this.uiTabPage1.Controls.Add(this.txtMaKQXL);
            this.uiTabPage1.Controls.Add(this.label59);
            this.uiTabPage1.Controls.Add(this.label17);
            this.uiTabPage1.Controls.Add(this.txtDiaChiNguoiKhai);
            this.uiTabPage1.Controls.Add(this.label23);
            this.uiTabPage1.Controls.Add(this.label60);
            this.uiTabPage1.Controls.Add(this.txtSoHopDong);
            this.uiTabPage1.Controls.Add(this.label61);
            this.uiTabPage1.Controls.Add(this.dtNgayCapKQXL);
            this.uiTabPage1.Controls.Add(this.txtEmailNguoiKhai);
            this.uiTabPage1.Controls.Add(this.dtHieuLucTuNgayKQXL);
            this.uiTabPage1.Controls.Add(this.txtSoFaxNguoiKhai);
            this.uiTabPage1.Controls.Add(this.dtNgayTiemPhong);
            this.uiTabPage1.Controls.Add(this.dtHieuLucDenNgayKQXL);
            this.uiTabPage1.Controls.Add(this.txtSoDienThoaiNguoiKhai);
            this.uiTabPage1.Controls.Add(this.label15);
            this.uiTabPage1.Controls.Add(this.ucPhanLoaiTraCuu);
            this.uiTabPage1.Controls.Add(this.txtSoDonXinCapPhep);
            this.uiTabPage1.Controls.Add(this.ucChucNangChungTu);
            this.uiTabPage1.Controls.Add(this.label42);
            this.uiTabPage1.Controls.Add(this.txtLoaiGiayPhep);
            this.uiTabPage1.Controls.Add(this.txtSoGiayPhepKQXL);
            this.uiTabPage1.Controls.Add(this.label19);
            this.uiTabPage1.Controls.Add(this.label27);
            this.uiTabPage1.Controls.Add(this.label13);
            this.uiTabPage1.Controls.Add(this.label45);
            this.uiTabPage1.Controls.Add(this.txtTenNguoiKhai);
            this.uiTabPage1.Controls.Add(this.label44);
            this.uiTabPage1.Controls.Add(this.txtTenDonViCapPhep);
            this.uiTabPage1.Controls.Add(this.label43);
            this.uiTabPage1.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage1.Name = "uiTabPage1";
            this.uiTabPage1.Size = new System.Drawing.Size(765, 785);
            this.uiTabPage1.TabStop = true;
            this.uiTabPage1.Text = "Thông tin chung";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Location = new System.Drawing.Point(128, 777);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(16, 13);
            this.label24.TabIndex = 42;
            this.label24.Text = "   ";
            // 
            // dtNgayKiemDich
            // 
            // 
            // 
            // 
            this.dtNgayKiemDich.DropDownCalendar.Name = "";
            this.dtNgayKiemDich.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayKiemDich.Location = new System.Drawing.Point(124, 724);
            this.dtNgayKiemDich.Name = "dtNgayKiemDich";
            this.dtNgayKiemDich.Size = new System.Drawing.Size(91, 21);
            this.dtNgayKiemDich.TabIndex = 28;
            this.dtNgayKiemDich.Value = new System.DateTime(1, 2, 1, 0, 0, 0, 0);
            this.dtNgayKiemDich.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.BackColor = System.Drawing.Color.Transparent;
            this.label88.Location = new System.Drawing.Point(8, 729);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(96, 13);
            this.label88.TabIndex = 41;
            this.label88.Text = "Thời gian kiểm dịch";
            // 
            // dtThoiGianKiemDich
            // 
            this.dtThoiGianKiemDich.CustomFormat = "hh:mm tt";
            this.dtThoiGianKiemDich.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtThoiGianKiemDich.DropDownCalendar.Name = "";
            this.dtThoiGianKiemDich.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtThoiGianKiemDich.Location = new System.Drawing.Point(221, 724);
            this.dtThoiGianKiemDich.Name = "dtThoiGianKiemDich";
            this.dtThoiGianKiemDich.Size = new System.Drawing.Size(72, 21);
            this.dtThoiGianKiemDich.TabIndex = 29;
            this.dtThoiGianKiemDich.Value = new System.DateTime(1, 2, 1, 0, 0, 0, 0);
            this.dtThoiGianKiemDich.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // txtBenhMienDich
            // 
            this.txtBenhMienDich.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBenhMienDich.Location = new System.Drawing.Point(124, 751);
            this.txtBenhMienDich.Name = "txtBenhMienDich";
            this.txtBenhMienDich.Size = new System.Drawing.Size(423, 21);
            this.txtBenhMienDich.TabIndex = 30;
            this.txtBenhMienDich.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaDiemKiemDich
            // 
            this.txtDiaDiemKiemDich.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDiaDiemKiemDich.Location = new System.Drawing.Point(124, 697);
            this.txtDiaDiemKiemDich.Name = "txtDiaDiemKiemDich";
            this.txtDiaDiemKiemDich.Size = new System.Drawing.Size(423, 21);
            this.txtDiaDiemKiemDich.TabIndex = 27;
            this.txtDiaDiemKiemDich.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(8, 755);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 13);
            this.label4.TabIndex = 39;
            this.label4.Text = "Bệnh miễn dịch";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Location = new System.Drawing.Point(8, 701);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(94, 13);
            this.label10.TabIndex = 39;
            this.label10.Text = "Địa điểm kiểm dịch";
            // 
            // ucCuaKhauNhap
            // 
            this.ucCuaKhauNhap.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucCuaKhauNhap.Appearance.Options.UseBackColor = true;
            this.ucCuaKhauNhap.Code = "";
            this.ucCuaKhauNhap.CountryCode = null;
            this.ucCuaKhauNhap.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCuaKhauNhap.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCuaKhauNhap.IsValidate = true;
            this.ucCuaKhauNhap.Location = new System.Drawing.Point(124, 665);
            this.ucCuaKhauNhap.Name = "ucCuaKhauNhap";
            this.ucCuaKhauNhap.Name_VN = "";
            this.ucCuaKhauNhap.SetValidate = false;
            this.ucCuaKhauNhap.ShowColumnCode = true;
            this.ucCuaKhauNhap.ShowColumnName = true;
            this.ucCuaKhauNhap.Size = new System.Drawing.Size(267, 26);
            this.ucCuaKhauNhap.TabIndex = 25;
            this.ucCuaKhauNhap.TagCode = "";
            this.ucCuaKhauNhap.TagName = "";
            this.ucCuaKhauNhap.WhereCondition = "";
            this.ucCuaKhauNhap.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucMaNuocNguoiKhai
            // 
            this.ucMaNuocNguoiKhai.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucMaNuocNguoiKhai.Appearance.Options.UseBackColor = true;
            this.ucMaNuocNguoiKhai.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucMaNuocNguoiKhai.Code = "";
            this.ucMaNuocNguoiKhai.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucMaNuocNguoiKhai.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucMaNuocNguoiKhai.IsValidate = true;
            this.ucMaNuocNguoiKhai.Location = new System.Drawing.Point(145, 88);
            this.ucMaNuocNguoiKhai.Name = "ucMaNuocNguoiKhai";
            this.ucMaNuocNguoiKhai.Name_VN = "";
            this.ucMaNuocNguoiKhai.SetValidate = false;
            this.ucMaNuocNguoiKhai.ShowColumnCode = true;
            this.ucMaNuocNguoiKhai.ShowColumnName = false;
            this.ucMaNuocNguoiKhai.Size = new System.Drawing.Size(58, 26);
            this.ucMaNuocNguoiKhai.TabIndex = 4;
            this.ucMaNuocNguoiKhai.TagName = "";
            this.ucMaNuocNguoiKhai.WhereCondition = "";
            this.ucMaNuocNguoiKhai.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucPhuongTienVanChuyen
            // 
            this.ucPhuongTienVanChuyen.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucPhuongTienVanChuyen.Appearance.Options.UseBackColor = true;
            this.ucPhuongTienVanChuyen.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E027;
            this.ucPhuongTienVanChuyen.Code = "";
            this.ucPhuongTienVanChuyen.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucPhuongTienVanChuyen.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucPhuongTienVanChuyen.IsValidate = true;
            this.ucPhuongTienVanChuyen.Location = new System.Drawing.Point(588, 638);
            this.ucPhuongTienVanChuyen.Name = "ucPhuongTienVanChuyen";
            this.ucPhuongTienVanChuyen.Name_VN = "";
            this.ucPhuongTienVanChuyen.SetValidate = false;
            this.ucPhuongTienVanChuyen.ShowColumnCode = true;
            this.ucPhuongTienVanChuyen.ShowColumnName = true;
            this.ucPhuongTienVanChuyen.Size = new System.Drawing.Size(146, 26);
            this.ucPhuongTienVanChuyen.TabIndex = 24;
            this.ucPhuongTienVanChuyen.TagName = "";
            this.ucPhuongTienVanChuyen.WhereCondition = "";
            this.ucPhuongTienVanChuyen.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucNuocQuaCanh
            // 
            this.ucNuocQuaCanh.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucNuocQuaCanh.Appearance.Options.UseBackColor = true;
            this.ucNuocQuaCanh.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucNuocQuaCanh.Code = "";
            this.ucNuocQuaCanh.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucNuocQuaCanh.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucNuocQuaCanh.IsValidate = true;
            this.ucNuocQuaCanh.Location = new System.Drawing.Point(124, 638);
            this.ucNuocQuaCanh.Name = "ucNuocQuaCanh";
            this.ucNuocQuaCanh.Name_VN = "";
            this.ucNuocQuaCanh.SetValidate = false;
            this.ucNuocQuaCanh.ShowColumnCode = true;
            this.ucNuocQuaCanh.ShowColumnName = true;
            this.ucNuocQuaCanh.Size = new System.Drawing.Size(267, 26);
            this.ucNuocQuaCanh.TabIndex = 23;
            this.ucNuocQuaCanh.TagCode = "";
            this.ucNuocQuaCanh.TagName = "";
            this.ucNuocQuaCanh.WhereCondition = "";
            this.ucNuocQuaCanh.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucNuocXuatKhau
            // 
            this.ucNuocXuatKhau.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucNuocXuatKhau.Appearance.Options.UseBackColor = true;
            this.ucNuocXuatKhau.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucNuocXuatKhau.Code = "";
            this.ucNuocXuatKhau.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucNuocXuatKhau.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucNuocXuatKhau.IsValidate = true;
            this.ucNuocXuatKhau.Location = new System.Drawing.Point(124, 447);
            this.ucNuocXuatKhau.Name = "ucNuocXuatKhau";
            this.ucNuocXuatKhau.Name_VN = "";
            this.ucNuocXuatKhau.SetValidate = false;
            this.ucNuocXuatKhau.ShowColumnCode = true;
            this.ucNuocXuatKhau.ShowColumnName = true;
            this.ucNuocXuatKhau.Size = new System.Drawing.Size(267, 26);
            this.ucNuocXuatKhau.TabIndex = 21;
            this.ucNuocXuatKhau.TagCode = "";
            this.ucNuocXuatKhau.TagName = "";
            this.ucNuocXuatKhau.WhereCondition = "";
            this.ucNuocXuatKhau.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend.EnterHandle(this.ucCategory_OnEnter);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Location = new System.Drawing.Point(458, 672);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(102, 13);
            this.label26.TabIndex = 8;
            this.label26.Text = "Số giấy chứng nhận";
            // 
            // dtNgayTiemPhong
            // 
            // 
            // 
            // 
            this.dtNgayTiemPhong.DropDownCalendar.Name = "";
            this.dtNgayTiemPhong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayTiemPhong.Location = new System.Drawing.Point(647, 751);
            this.dtNgayTiemPhong.Name = "dtNgayTiemPhong";
            this.dtNgayTiemPhong.Size = new System.Drawing.Size(91, 21);
            this.dtNgayTiemPhong.TabIndex = 31;
            this.dtNgayTiemPhong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // ucPhanLoaiTraCuu
            // 
            this.ucPhanLoaiTraCuu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucPhanLoaiTraCuu.Appearance.Options.UseBackColor = true;
            this.ucPhanLoaiTraCuu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E026;
            this.ucPhanLoaiTraCuu.Code = "";
            this.ucPhanLoaiTraCuu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucPhanLoaiTraCuu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucPhanLoaiTraCuu.IsValidate = true;
            this.ucPhanLoaiTraCuu.Location = new System.Drawing.Point(434, 114);
            this.ucPhanLoaiTraCuu.Name = "ucPhanLoaiTraCuu";
            this.ucPhanLoaiTraCuu.Name_VN = "";
            this.ucPhanLoaiTraCuu.SetValidate = false;
            this.ucPhanLoaiTraCuu.ShowColumnCode = true;
            this.ucPhanLoaiTraCuu.ShowColumnName = false;
            this.ucPhanLoaiTraCuu.Size = new System.Drawing.Size(137, 26);
            this.ucPhanLoaiTraCuu.TabIndex = 9;
            this.ucPhanLoaiTraCuu.TagName = "";
            this.ucPhanLoaiTraCuu.WhereCondition = "";
            this.ucPhanLoaiTraCuu.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucChucNangChungTu
            // 
            this.ucChucNangChungTu.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucChucNangChungTu.Appearance.Options.UseBackColor = true;
            this.ucChucNangChungTu.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.E025;
            this.ucChucNangChungTu.Code = "";
            this.ucChucNangChungTu.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucChucNangChungTu.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucChucNangChungTu.IsValidate = true;
            this.ucChucNangChungTu.Location = new System.Drawing.Point(145, 144);
            this.ucChucNangChungTu.Name = "ucChucNangChungTu";
            this.ucChucNangChungTu.Name_VN = "";
            this.ucChucNangChungTu.SetValidate = false;
            this.ucChucNangChungTu.ShowColumnCode = true;
            this.ucChucNangChungTu.ShowColumnName = true;
            this.ucChucNangChungTu.Size = new System.Drawing.Size(114, 26);
            this.ucChucNangChungTu.TabIndex = 10;
            this.ucChucNangChungTu.TagName = "";
            this.ucChucNangChungTu.WhereCondition = "";
            this.ucChucNangChungTu.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Location = new System.Drawing.Point(553, 755);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(88, 13);
            this.label27.TabIndex = 21;
            this.label27.Text = "Ngày tiêm phòng";
            // 
            // uiTabPage2
            // 
            this.uiTabPage2.AutoScroll = true;
            this.uiTabPage2.Controls.Add(this.ucDonViTinhKhoiLuongHangCanBoc);
            this.uiTabPage2.Controls.Add(this.ucDonViTinhKhoiLuongHangCangDauTien);
            this.uiTabPage2.Controls.Add(this.txtKhoiLuongHangCanBoc);
            this.uiTabPage2.Controls.Add(this.txtKhoiLuongHangCangDauTien);
            this.uiTabPage2.Controls.Add(this.label87);
            this.uiTabPage2.Controls.Add(this.label81);
            this.uiTabPage2.Controls.Add(this.ucDonViTinhSoLuongHangCanBoc);
            this.uiTabPage2.Controls.Add(this.ucDonViTinhSoLuongHangCangDauTien);
            this.uiTabPage2.Controls.Add(this.linkLabel1);
            this.uiTabPage2.Controls.Add(this.ucQuocTich);
            this.uiTabPage2.Controls.Add(this.label70);
            this.uiTabPage2.Controls.Add(this.pnlHoSoKemTheo);
            this.uiTabPage2.Controls.Add(this.lblHoSoKemTheo);
            this.uiTabPage2.Controls.Add(this.txtLyDoKhongDat);
            this.uiTabPage2.Controls.Add(this.txtKetQuaKiemTra);
            this.uiTabPage2.Controls.Add(this.txtHoSoLienQuan);
            this.uiTabPage2.Controls.Add(this.label58);
            this.uiTabPage2.Controls.Add(this.txtSoLuongHangCanBoc);
            this.uiTabPage2.Controls.Add(this.txtSoLuongHangCangDauTien);
            this.uiTabPage2.Controls.Add(this.label86);
            this.uiTabPage2.Controls.Add(this.label80);
            this.uiTabPage2.Controls.Add(this.txtSoHanhKhach);
            this.uiTabPage2.Controls.Add(this.label74);
            this.uiTabPage2.Controls.Add(this.txtSoThuyenVien);
            this.uiTabPage2.Controls.Add(this.label73);
            this.uiTabPage2.Controls.Add(this.txtSoBanCanCap);
            this.uiTabPage2.Controls.Add(this.label31);
            this.uiTabPage2.Controls.Add(this.dtNgayRoiCanh);
            this.uiTabPage2.Controls.Add(this.label78);
            this.uiTabPage2.Controls.Add(this.dtThoiGianGiamSat);
            this.uiTabPage2.Controls.Add(this.dtNgayGiamSat);
            this.uiTabPage2.Controls.Add(this.label25);
            this.uiTabPage2.Controls.Add(this.txtDiaDiemNuoiTrong);
            this.uiTabPage2.Controls.Add(this.label30);
            this.uiTabPage2.Controls.Add(this.txtNongDo);
            this.uiTabPage2.Controls.Add(this.label29);
            this.uiTabPage2.Controls.Add(this.txtKhuTrung);
            this.uiTabPage2.Controls.Add(this.label67);
            this.uiTabPage2.Controls.Add(this.label28);
            this.uiTabPage2.Controls.Add(this.label66);
            this.uiTabPage2.Controls.Add(this.txtTenBacSi);
            this.uiTabPage2.Controls.Add(this.txtTenThuyenTruong);
            this.uiTabPage2.Controls.Add(this.label72);
            this.uiTabPage2.Controls.Add(this.label71);
            this.uiTabPage2.Controls.Add(this.txtCangBocHangDauTien);
            this.uiTabPage2.Controls.Add(this.label77);
            this.uiTabPage2.Controls.Add(this.txtTenHangCanBoc);
            this.uiTabPage2.Controls.Add(this.label85);
            this.uiTabPage2.Controls.Add(this.txtTenHangCangDauTien);
            this.uiTabPage2.Controls.Add(this.label79);
            this.uiTabPage2.Controls.Add(this.txtCangDenTiepTheo);
            this.uiTabPage2.Controls.Add(this.label76);
            this.uiTabPage2.Controls.Add(this.txtCangRoiCuoiCung);
            this.uiTabPage2.Controls.Add(this.label75);
            this.uiTabPage2.Controls.Add(this.txtTenTau);
            this.uiTabPage2.Controls.Add(this.label69);
            this.uiTabPage2.Controls.Add(this.txtDaiDienDonViCapPhep);
            this.uiTabPage2.Controls.Add(this.label68);
            this.uiTabPage2.Controls.Add(this.txtNguoiKiemTra);
            this.uiTabPage2.Controls.Add(this.label65);
            this.uiTabPage2.Controls.Add(this.txtNoiChuyenDen);
            this.uiTabPage2.Controls.Add(this.label64);
            this.uiTabPage2.Controls.Add(this.txtVatDungKhac);
            this.uiTabPage2.Controls.Add(this.label63);
            this.uiTabPage2.Controls.Add(this.txtDiaDiemGiamSat);
            this.uiTabPage2.Controls.Add(this.label48);
            this.uiTabPage2.Location = new System.Drawing.Point(1, 21);
            this.uiTabPage2.Name = "uiTabPage2";
            this.uiTabPage2.Size = new System.Drawing.Size(765, 785);
            this.uiTabPage2.TabStop = true;
            this.uiTabPage2.Text = "Thông tin chung (tiếp theo)";
            // 
            // ucDonViTinhKhoiLuongHangCanBoc
            // 
            this.ucDonViTinhKhoiLuongHangCanBoc.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucDonViTinhKhoiLuongHangCanBoc.Appearance.Options.UseBackColor = true;
            this.ucDonViTinhKhoiLuongHangCanBoc.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucDonViTinhKhoiLuongHangCanBoc.Code = "";
            this.ucDonViTinhKhoiLuongHangCanBoc.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhKhoiLuongHangCanBoc.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhKhoiLuongHangCanBoc.IsValidate = true;
            this.ucDonViTinhKhoiLuongHangCanBoc.Location = new System.Drawing.Point(424, 729);
            this.ucDonViTinhKhoiLuongHangCanBoc.Name = "ucDonViTinhKhoiLuongHangCanBoc";
            this.ucDonViTinhKhoiLuongHangCanBoc.Name_VN = "";
            this.ucDonViTinhKhoiLuongHangCanBoc.SetValidate = false;
            this.ucDonViTinhKhoiLuongHangCanBoc.ShowColumnCode = true;
            this.ucDonViTinhKhoiLuongHangCanBoc.ShowColumnName = false;
            this.ucDonViTinhKhoiLuongHangCanBoc.Size = new System.Drawing.Size(69, 26);
            this.ucDonViTinhKhoiLuongHangCanBoc.TabIndex = 35;
            this.ucDonViTinhKhoiLuongHangCanBoc.TagName = "";
            this.ucDonViTinhKhoiLuongHangCanBoc.WhereCondition = "";
            this.ucDonViTinhKhoiLuongHangCanBoc.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucDonViTinhKhoiLuongHangCangDauTien
            // 
            this.ucDonViTinhKhoiLuongHangCangDauTien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucDonViTinhKhoiLuongHangCangDauTien.Appearance.Options.UseBackColor = true;
            this.ucDonViTinhKhoiLuongHangCangDauTien.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucDonViTinhKhoiLuongHangCangDauTien.Code = "";
            this.ucDonViTinhKhoiLuongHangCangDauTien.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhKhoiLuongHangCangDauTien.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhKhoiLuongHangCangDauTien.IsValidate = true;
            this.ucDonViTinhKhoiLuongHangCangDauTien.Location = new System.Drawing.Point(424, 675);
            this.ucDonViTinhKhoiLuongHangCangDauTien.Name = "ucDonViTinhKhoiLuongHangCangDauTien";
            this.ucDonViTinhKhoiLuongHangCangDauTien.Name_VN = "";
            this.ucDonViTinhKhoiLuongHangCangDauTien.SetValidate = false;
            this.ucDonViTinhKhoiLuongHangCangDauTien.ShowColumnCode = true;
            this.ucDonViTinhKhoiLuongHangCangDauTien.ShowColumnName = false;
            this.ucDonViTinhKhoiLuongHangCangDauTien.Size = new System.Drawing.Size(69, 26);
            this.ucDonViTinhKhoiLuongHangCangDauTien.TabIndex = 30;
            this.ucDonViTinhKhoiLuongHangCangDauTien.TagName = "";
            this.ucDonViTinhKhoiLuongHangCangDauTien.WhereCondition = "";
            this.ucDonViTinhKhoiLuongHangCangDauTien.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // txtKhoiLuongHangCanBoc
            // 
            this.txtKhoiLuongHangCanBoc.DecimalDigits = 3;
            this.txtKhoiLuongHangCanBoc.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtKhoiLuongHangCanBoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKhoiLuongHangCanBoc.Location = new System.Drawing.Point(337, 732);
            this.txtKhoiLuongHangCanBoc.MaxLength = 10;
            this.txtKhoiLuongHangCanBoc.Name = "txtKhoiLuongHangCanBoc";
            this.txtKhoiLuongHangCanBoc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtKhoiLuongHangCanBoc.Size = new System.Drawing.Size(81, 21);
            this.txtKhoiLuongHangCanBoc.TabIndex = 34;
            this.txtKhoiLuongHangCanBoc.Text = "0.000";
            this.txtKhoiLuongHangCanBoc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKhoiLuongHangCanBoc.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtKhoiLuongHangCanBoc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtKhoiLuongHangCangDauTien
            // 
            this.txtKhoiLuongHangCangDauTien.DecimalDigits = 3;
            this.txtKhoiLuongHangCangDauTien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtKhoiLuongHangCangDauTien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtKhoiLuongHangCangDauTien.Location = new System.Drawing.Point(337, 678);
            this.txtKhoiLuongHangCangDauTien.MaxLength = 10;
            this.txtKhoiLuongHangCangDauTien.Name = "txtKhoiLuongHangCangDauTien";
            this.txtKhoiLuongHangCangDauTien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtKhoiLuongHangCangDauTien.Size = new System.Drawing.Size(81, 21);
            this.txtKhoiLuongHangCangDauTien.TabIndex = 29;
            this.txtKhoiLuongHangCangDauTien.Text = "0.000";
            this.txtKhoiLuongHangCangDauTien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtKhoiLuongHangCangDauTien.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtKhoiLuongHangCangDauTien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.BackColor = System.Drawing.Color.Transparent;
            this.label87.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(274, 737);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(57, 13);
            this.label87.TabIndex = 38;
            this.label87.Text = "Khối lượng";
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.BackColor = System.Drawing.Color.Transparent;
            this.label81.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(274, 683);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(57, 13);
            this.label81.TabIndex = 38;
            this.label81.Text = "Khối lượng";
            // 
            // ucDonViTinhSoLuongHangCanBoc
            // 
            this.ucDonViTinhSoLuongHangCanBoc.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucDonViTinhSoLuongHangCanBoc.Appearance.Options.UseBackColor = true;
            this.ucDonViTinhSoLuongHangCanBoc.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ucDonViTinhSoLuongHangCanBoc.Code = "";
            this.ucDonViTinhSoLuongHangCanBoc.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhSoLuongHangCanBoc.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhSoLuongHangCanBoc.IsValidate = true;
            this.ucDonViTinhSoLuongHangCanBoc.Location = new System.Drawing.Point(199, 729);
            this.ucDonViTinhSoLuongHangCanBoc.Name = "ucDonViTinhSoLuongHangCanBoc";
            this.ucDonViTinhSoLuongHangCanBoc.Name_VN = "";
            this.ucDonViTinhSoLuongHangCanBoc.SetValidate = false;
            this.ucDonViTinhSoLuongHangCanBoc.ShowColumnCode = true;
            this.ucDonViTinhSoLuongHangCanBoc.ShowColumnName = false;
            this.ucDonViTinhSoLuongHangCanBoc.Size = new System.Drawing.Size(69, 26);
            this.ucDonViTinhSoLuongHangCanBoc.TabIndex = 33;
            this.ucDonViTinhSoLuongHangCanBoc.TagName = "";
            this.ucDonViTinhSoLuongHangCanBoc.WhereCondition = "";
            this.ucDonViTinhSoLuongHangCanBoc.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // ucDonViTinhSoLuongHangCangDauTien
            // 
            this.ucDonViTinhSoLuongHangCangDauTien.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucDonViTinhSoLuongHangCangDauTien.Appearance.Options.UseBackColor = true;
            this.ucDonViTinhSoLuongHangCangDauTien.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ucDonViTinhSoLuongHangCangDauTien.Code = "";
            this.ucDonViTinhSoLuongHangCangDauTien.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucDonViTinhSoLuongHangCangDauTien.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucDonViTinhSoLuongHangCangDauTien.IsValidate = true;
            this.ucDonViTinhSoLuongHangCangDauTien.Location = new System.Drawing.Point(199, 675);
            this.ucDonViTinhSoLuongHangCangDauTien.Name = "ucDonViTinhSoLuongHangCangDauTien";
            this.ucDonViTinhSoLuongHangCangDauTien.Name_VN = "";
            this.ucDonViTinhSoLuongHangCangDauTien.SetValidate = false;
            this.ucDonViTinhSoLuongHangCangDauTien.ShowColumnCode = true;
            this.ucDonViTinhSoLuongHangCangDauTien.ShowColumnName = false;
            this.ucDonViTinhSoLuongHangCangDauTien.Size = new System.Drawing.Size(69, 26);
            this.ucDonViTinhSoLuongHangCangDauTien.TabIndex = 28;
            this.ucDonViTinhSoLuongHangCangDauTien.TagName = "";
            this.ucDonViTinhSoLuongHangCangDauTien.WhereCondition = "";
            this.ucDonViTinhSoLuongHangCangDauTien.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.BackColor = System.Drawing.Color.Transparent;
            this.linkLabel1.Location = new System.Drawing.Point(8, 637);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(74, 13);
            this.linkLabel1.TabIndex = 36;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Cảng đầu tiên";
            // 
            // ucQuocTich
            // 
            this.ucQuocTich.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ucQuocTich.Appearance.Options.UseBackColor = true;
            this.ucQuocTich.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A015;
            this.ucQuocTich.Code = "";
            this.ucQuocTich.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucQuocTich.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucQuocTich.IsValidate = true;
            this.ucQuocTich.Location = new System.Drawing.Point(685, 436);
            this.ucQuocTich.Name = "ucQuocTich";
            this.ucQuocTich.Name_VN = "";
            this.ucQuocTich.SetValidate = false;
            this.ucQuocTich.ShowColumnCode = true;
            this.ucQuocTich.ShowColumnName = false;
            this.ucQuocTich.Size = new System.Drawing.Size(58, 26);
            this.ucQuocTich.TabIndex = 17;
            this.ucQuocTich.TagName = "";
            this.ucQuocTich.WhereCondition = "";
            this.ucQuocTich.OnEnter += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EnterHandle(this.ucCategory_OnEnter);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.BackColor = System.Drawing.Color.Transparent;
            this.label70.Location = new System.Drawing.Point(626, 443);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(52, 13);
            this.label70.TabIndex = 34;
            this.label70.Text = "Quốc tịch";
            // 
            // pnlHoSoKemTheo
            // 
            this.pnlHoSoKemTheo.BackColor = System.Drawing.Color.Transparent;
            this.pnlHoSoKemTheo.Controls.Add(this.label40);
            this.pnlHoSoKemTheo.Controls.Add(this.txt10);
            this.pnlHoSoKemTheo.Controls.Add(this.label35);
            this.pnlHoSoKemTheo.Controls.Add(this.txt9);
            this.pnlHoSoKemTheo.Controls.Add(this.label34);
            this.pnlHoSoKemTheo.Controls.Add(this.txt8);
            this.pnlHoSoKemTheo.Controls.Add(this.label33);
            this.pnlHoSoKemTheo.Controls.Add(this.txt7);
            this.pnlHoSoKemTheo.Controls.Add(this.label32);
            this.pnlHoSoKemTheo.Controls.Add(this.txt6);
            this.pnlHoSoKemTheo.Controls.Add(this.label36);
            this.pnlHoSoKemTheo.Controls.Add(this.txt5);
            this.pnlHoSoKemTheo.Controls.Add(this.label37);
            this.pnlHoSoKemTheo.Controls.Add(this.txt4);
            this.pnlHoSoKemTheo.Controls.Add(this.label39);
            this.pnlHoSoKemTheo.Controls.Add(this.txt3);
            this.pnlHoSoKemTheo.Controls.Add(this.label46);
            this.pnlHoSoKemTheo.Controls.Add(this.txt2);
            this.pnlHoSoKemTheo.Controls.Add(this.label47);
            this.pnlHoSoKemTheo.Controls.Add(this.txt1);
            this.pnlHoSoKemTheo.Enabled = false;
            this.pnlHoSoKemTheo.Location = new System.Drawing.Point(112, 225);
            this.pnlHoSoKemTheo.Name = "pnlHoSoKemTheo";
            this.pnlHoSoKemTheo.Size = new System.Drawing.Size(443, 27);
            this.pnlHoSoKemTheo.TabIndex = 10;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(376, 7);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(23, 13);
            this.label40.TabIndex = 1;
            this.label40.Text = "10.";
            // 
            // txt10
            // 
            this.txt10.Location = new System.Drawing.Point(399, 3);
            this.txt10.Name = "txt10";
            this.txt10.Size = new System.Drawing.Size(19, 21);
            this.txt10.TabIndex = 11;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(334, 7);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(17, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "9.";
            // 
            // txt9
            // 
            this.txt9.Location = new System.Drawing.Point(351, 3);
            this.txt9.Name = "txt9";
            this.txt9.Size = new System.Drawing.Size(19, 21);
            this.txt9.TabIndex = 11;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(292, 7);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(17, 13);
            this.label34.TabIndex = 1;
            this.label34.Text = "8.";
            // 
            // txt8
            // 
            this.txt8.Location = new System.Drawing.Point(309, 3);
            this.txt8.Name = "txt8";
            this.txt8.Size = new System.Drawing.Size(19, 21);
            this.txt8.TabIndex = 11;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(250, 7);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(17, 13);
            this.label33.TabIndex = 1;
            this.label33.Text = "7.";
            // 
            // txt7
            // 
            this.txt7.Location = new System.Drawing.Point(267, 3);
            this.txt7.Name = "txt7";
            this.txt7.Size = new System.Drawing.Size(19, 21);
            this.txt7.TabIndex = 11;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(208, 7);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(17, 13);
            this.label32.TabIndex = 1;
            this.label32.Text = "6.";
            // 
            // txt6
            // 
            this.txt6.Location = new System.Drawing.Point(225, 3);
            this.txt6.Name = "txt6";
            this.txt6.Size = new System.Drawing.Size(19, 21);
            this.txt6.TabIndex = 11;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(166, 7);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(17, 13);
            this.label36.TabIndex = 1;
            this.label36.Text = "5.";
            // 
            // txt5
            // 
            this.txt5.Location = new System.Drawing.Point(183, 3);
            this.txt5.Name = "txt5";
            this.txt5.Size = new System.Drawing.Size(19, 21);
            this.txt5.TabIndex = 11;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(126, 7);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(17, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "4.";
            // 
            // txt4
            // 
            this.txt4.Location = new System.Drawing.Point(143, 3);
            this.txt4.Name = "txt4";
            this.txt4.Size = new System.Drawing.Size(19, 21);
            this.txt4.TabIndex = 11;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(84, 7);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(17, 13);
            this.label39.TabIndex = 1;
            this.label39.Text = "3.";
            // 
            // txt3
            // 
            this.txt3.Location = new System.Drawing.Point(101, 3);
            this.txt3.Name = "txt3";
            this.txt3.Size = new System.Drawing.Size(19, 21);
            this.txt3.TabIndex = 11;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(45, 7);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(17, 13);
            this.label46.TabIndex = 1;
            this.label46.Text = "2.";
            // 
            // txt2
            // 
            this.txt2.Location = new System.Drawing.Point(62, 3);
            this.txt2.Name = "txt2";
            this.txt2.Size = new System.Drawing.Size(19, 21);
            this.txt2.TabIndex = 11;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(3, 7);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(17, 13);
            this.label47.TabIndex = 1;
            this.label47.Text = "1.";
            // 
            // txt1
            // 
            this.txt1.Location = new System.Drawing.Point(20, 3);
            this.txt1.Name = "txt1";
            this.txt1.Size = new System.Drawing.Size(19, 21);
            this.txt1.TabIndex = 11;
            // 
            // lblHoSoKemTheo
            // 
            this.lblHoSoKemTheo.AutoSize = true;
            this.lblHoSoKemTheo.BackColor = System.Drawing.Color.Transparent;
            this.lblHoSoKemTheo.Location = new System.Drawing.Point(9, 225);
            this.lblHoSoKemTheo.Name = "lblHoSoKemTheo";
            this.lblHoSoKemTheo.Size = new System.Drawing.Size(107, 26);
            this.lblHoSoKemTheo.TabIndex = 9;
            this.lblHoSoKemTheo.Text = "Danh sách chứng từ \r\nđi kèm";
            // 
            // txtLyDoKhongDat
            // 
            this.txtLyDoKhongDat.Enabled = false;
            this.txtLyDoKhongDat.Location = new System.Drawing.Point(112, 362);
            this.txtLyDoKhongDat.Multiline = true;
            this.txtLyDoKhongDat.Name = "txtLyDoKhongDat";
            this.txtLyDoKhongDat.Size = new System.Drawing.Size(630, 44);
            this.txtLyDoKhongDat.TabIndex = 14;
            this.txtLyDoKhongDat.VisualStyleManager = this.vsmMain;
            // 
            // txtKetQuaKiemTra
            // 
            this.txtKetQuaKiemTra.Location = new System.Drawing.Point(112, 312);
            this.txtKetQuaKiemTra.Multiline = true;
            this.txtKetQuaKiemTra.Name = "txtKetQuaKiemTra";
            this.txtKetQuaKiemTra.Size = new System.Drawing.Size(630, 44);
            this.txtKetQuaKiemTra.TabIndex = 13;
            this.txtKetQuaKiemTra.VisualStyleManager = this.vsmMain;
            // 
            // txtHoSoLienQuan
            // 
            this.txtHoSoLienQuan.Location = new System.Drawing.Point(112, 175);
            this.txtHoSoLienQuan.Multiline = true;
            this.txtHoSoLienQuan.Name = "txtHoSoLienQuan";
            this.txtHoSoLienQuan.Size = new System.Drawing.Size(630, 44);
            this.txtHoSoLienQuan.TabIndex = 9;
            this.txtHoSoLienQuan.VisualStyleManager = this.vsmMain;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Location = new System.Drawing.Point(11, 191);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(80, 13);
            this.label58.TabIndex = 30;
            this.label58.Text = "Hồ sơ liên quan";
            // 
            // txtSoLuongHangCanBoc
            // 
            this.txtSoLuongHangCanBoc.DecimalDigits = 0;
            this.txtSoLuongHangCanBoc.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongHangCanBoc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongHangCanBoc.Location = new System.Drawing.Point(112, 732);
            this.txtSoLuongHangCanBoc.MaxLength = 8;
            this.txtSoLuongHangCanBoc.Name = "txtSoLuongHangCanBoc";
            this.txtSoLuongHangCanBoc.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongHangCanBoc.Size = new System.Drawing.Size(81, 21);
            this.txtSoLuongHangCanBoc.TabIndex = 32;
            this.txtSoLuongHangCanBoc.Text = "0";
            this.txtSoLuongHangCanBoc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongHangCanBoc.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongHangCanBoc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuongHangCangDauTien
            // 
            this.txtSoLuongHangCangDauTien.DecimalDigits = 0;
            this.txtSoLuongHangCangDauTien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoLuongHangCangDauTien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongHangCangDauTien.Location = new System.Drawing.Point(112, 678);
            this.txtSoLuongHangCangDauTien.MaxLength = 8;
            this.txtSoLuongHangCangDauTien.Name = "txtSoLuongHangCangDauTien";
            this.txtSoLuongHangCangDauTien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongHangCangDauTien.Size = new System.Drawing.Size(81, 21);
            this.txtSoLuongHangCangDauTien.TabIndex = 27;
            this.txtSoLuongHangCangDauTien.Text = "0";
            this.txtSoLuongHangCangDauTien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongHangCangDauTien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoLuongHangCangDauTien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.BackColor = System.Drawing.Color.Transparent;
            this.label86.Location = new System.Drawing.Point(57, 737);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(49, 13);
            this.label86.TabIndex = 28;
            this.label86.Text = "Số lượng";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.BackColor = System.Drawing.Color.Transparent;
            this.label80.Location = new System.Drawing.Point(57, 683);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(49, 13);
            this.label80.TabIndex = 28;
            this.label80.Text = "Số lượng";
            // 
            // txtSoHanhKhach
            // 
            this.txtSoHanhKhach.DecimalDigits = 0;
            this.txtSoHanhKhach.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoHanhKhach.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHanhKhach.Location = new System.Drawing.Point(298, 493);
            this.txtSoHanhKhach.MaxLength = 4;
            this.txtSoHanhKhach.Name = "txtSoHanhKhach";
            this.txtSoHanhKhach.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoHanhKhach.Size = new System.Drawing.Size(81, 21);
            this.txtSoHanhKhach.TabIndex = 21;
            this.txtSoHanhKhach.Text = "0";
            this.txtSoHanhKhach.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHanhKhach.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoHanhKhach.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.BackColor = System.Drawing.Color.Transparent;
            this.label74.Location = new System.Drawing.Point(215, 498);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(77, 13);
            this.label74.TabIndex = 28;
            this.label74.Text = "Số hành khách";
            // 
            // txtSoThuyenVien
            // 
            this.txtSoThuyenVien.DecimalDigits = 0;
            this.txtSoThuyenVien.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoThuyenVien.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoThuyenVien.Location = new System.Drawing.Point(112, 493);
            this.txtSoThuyenVien.MaxLength = 4;
            this.txtSoThuyenVien.Name = "txtSoThuyenVien";
            this.txtSoThuyenVien.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoThuyenVien.Size = new System.Drawing.Size(81, 21);
            this.txtSoThuyenVien.TabIndex = 20;
            this.txtSoThuyenVien.Text = "0";
            this.txtSoThuyenVien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoThuyenVien.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoThuyenVien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.BackColor = System.Drawing.Color.Transparent;
            this.label73.Location = new System.Drawing.Point(9, 498);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(79, 13);
            this.label73.TabIndex = 28;
            this.label73.Text = "Số thuyền viên";
            // 
            // txtSoBanCanCap
            // 
            this.txtSoBanCanCap.DecimalDigits = 0;
            this.txtSoBanCanCap.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.txtSoBanCanCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoBanCanCap.Location = new System.Drawing.Point(382, 121);
            this.txtSoBanCanCap.MaxLength = 2;
            this.txtSoBanCanCap.Name = "txtSoBanCanCap";
            this.txtSoBanCanCap.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoBanCanCap.Size = new System.Drawing.Size(81, 21);
            this.txtSoBanCanCap.TabIndex = 7;
            this.txtSoBanCanCap.Text = "0";
            this.txtSoBanCanCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoBanCanCap.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtSoBanCanCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Location = new System.Drawing.Point(295, 126);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(80, 13);
            this.label31.TabIndex = 28;
            this.label31.Text = "Số bản cần cấp";
            // 
            // dtNgayRoiCanh
            // 
            // 
            // 
            // 
            this.dtNgayRoiCanh.DropDownCalendar.Name = "";
            this.dtNgayRoiCanh.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayRoiCanh.Location = new System.Drawing.Point(112, 601);
            this.dtNgayRoiCanh.Name = "dtNgayRoiCanh";
            this.dtNgayRoiCanh.Size = new System.Drawing.Size(90, 21);
            this.dtNgayRoiCanh.TabIndex = 25;
            this.dtNgayRoiCanh.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.BackColor = System.Drawing.Color.Transparent;
            this.label78.Location = new System.Drawing.Point(8, 609);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(73, 13);
            this.label78.TabIndex = 24;
            this.label78.Text = "Ngày rời cảnh";
            // 
            // dtThoiGianGiamSat
            // 
            this.dtThoiGianGiamSat.CustomFormat = "hh:mm tt";
            this.dtThoiGianGiamSat.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.dtThoiGianGiamSat.DropDownCalendar.Name = "";
            this.dtThoiGianGiamSat.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtThoiGianGiamSat.Location = new System.Drawing.Point(208, 121);
            this.dtThoiGianGiamSat.Name = "dtThoiGianGiamSat";
            this.dtThoiGianGiamSat.Size = new System.Drawing.Size(75, 21);
            this.dtThoiGianGiamSat.TabIndex = 6;
            this.dtThoiGianGiamSat.Value = new System.DateTime(2013, 9, 30, 11, 45, 58, 0);
            this.dtThoiGianGiamSat.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // dtNgayGiamSat
            // 
            // 
            // 
            // 
            this.dtNgayGiamSat.DropDownCalendar.Name = "";
            this.dtNgayGiamSat.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.dtNgayGiamSat.Location = new System.Drawing.Point(112, 121);
            this.dtNgayGiamSat.Name = "dtNgayGiamSat";
            this.dtNgayGiamSat.Size = new System.Drawing.Size(90, 21);
            this.dtNgayGiamSat.TabIndex = 5;
            this.dtNgayGiamSat.Value = new System.DateTime(2013, 9, 30, 11, 46, 6, 0);
            this.dtNgayGiamSat.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Location = new System.Drawing.Point(9, 126);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(93, 13);
            this.label25.TabIndex = 24;
            this.label25.Text = "Thời gian giám sát";
            // 
            // txtDiaDiemNuoiTrong
            // 
            this.txtDiaDiemNuoiTrong.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDiaDiemNuoiTrong.Location = new System.Drawing.Point(112, 67);
            this.txtDiaDiemNuoiTrong.Name = "txtDiaDiemNuoiTrong";
            this.txtDiaDiemNuoiTrong.Size = new System.Drawing.Size(630, 21);
            this.txtDiaDiemNuoiTrong.TabIndex = 3;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Location = new System.Drawing.Point(9, 71);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(100, 13);
            this.label30.TabIndex = 26;
            this.label30.Text = "Địa điểm nuôi trồng";
            // 
            // txtNongDo
            // 
            this.txtNongDo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNongDo.Location = new System.Drawing.Point(112, 40);
            this.txtNongDo.Name = "txtNongDo";
            this.txtNongDo.Size = new System.Drawing.Size(630, 21);
            this.txtNongDo.TabIndex = 2;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Location = new System.Drawing.Point(9, 44);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(47, 13);
            this.label29.TabIndex = 26;
            this.label29.Text = "Nồng độ";
            // 
            // txtKhuTrung
            // 
            this.txtKhuTrung.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtKhuTrung.Location = new System.Drawing.Point(112, 13);
            this.txtKhuTrung.Name = "txtKhuTrung";
            this.txtKhuTrung.Size = new System.Drawing.Size(630, 21);
            this.txtKhuTrung.TabIndex = 1;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.BackColor = System.Drawing.Color.Transparent;
            this.label67.Location = new System.Drawing.Point(9, 377);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(84, 13);
            this.label67.TabIndex = 26;
            this.label67.Text = "Lý do không đạt";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Location = new System.Drawing.Point(9, 17);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(55, 13);
            this.label28.TabIndex = 0;
            this.label28.Text = "Khử trùng";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.BackColor = System.Drawing.Color.Transparent;
            this.label66.Location = new System.Drawing.Point(9, 327);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(85, 13);
            this.label66.TabIndex = 26;
            this.label66.Text = "Kết quả kiểm tra";
            // 
            // txtTenBacSi
            // 
            this.txtTenBacSi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenBacSi.Location = new System.Drawing.Point(476, 466);
            this.txtTenBacSi.Name = "txtTenBacSi";
            this.txtTenBacSi.Size = new System.Drawing.Size(266, 21);
            this.txtTenBacSi.TabIndex = 19;
            this.txtTenBacSi.VisualStyleManager = this.vsmMain;
            // 
            // txtTenThuyenTruong
            // 
            this.txtTenThuyenTruong.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenThuyenTruong.Location = new System.Drawing.Point(112, 466);
            this.txtTenThuyenTruong.Name = "txtTenThuyenTruong";
            this.txtTenThuyenTruong.Size = new System.Drawing.Size(267, 21);
            this.txtTenThuyenTruong.TabIndex = 18;
            this.txtTenThuyenTruong.VisualStyleManager = this.vsmMain;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.BackColor = System.Drawing.Color.Transparent;
            this.label72.Location = new System.Drawing.Point(415, 470);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(55, 13);
            this.label72.TabIndex = 26;
            this.label72.Text = "Tên bác sĩ";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.BackColor = System.Drawing.Color.Transparent;
            this.label71.Location = new System.Drawing.Point(9, 470);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(98, 13);
            this.label71.TabIndex = 26;
            this.label71.Text = "Tên thuyền trưởng";
            // 
            // txtCangBocHangDauTien
            // 
            this.txtCangBocHangDauTien.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCangBocHangDauTien.Location = new System.Drawing.Point(112, 574);
            this.txtCangBocHangDauTien.Name = "txtCangBocHangDauTien";
            this.txtCangBocHangDauTien.Size = new System.Drawing.Size(630, 21);
            this.txtCangBocHangDauTien.TabIndex = 24;
            this.txtCangBocHangDauTien.VisualStyleManager = this.vsmMain;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.BackColor = System.Drawing.Color.Transparent;
            this.label77.Location = new System.Drawing.Point(9, 571);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(82, 26);
            this.label77.TabIndex = 26;
            this.label77.Text = "Cảng bốc hàng \r\nđầu tiên";
            // 
            // txtTenHangCanBoc
            // 
            this.txtTenHangCanBoc.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHangCanBoc.Location = new System.Drawing.Point(112, 705);
            this.txtTenHangCanBoc.Name = "txtTenHangCanBoc";
            this.txtTenHangCanBoc.Size = new System.Drawing.Size(630, 21);
            this.txtTenHangCanBoc.TabIndex = 31;
            this.txtTenHangCanBoc.VisualStyleManager = this.vsmMain;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.BackColor = System.Drawing.Color.Transparent;
            this.label85.Location = new System.Drawing.Point(12, 709);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(92, 13);
            this.label85.TabIndex = 26;
            this.label85.Text = "Tên hàng cần bốc";
            // 
            // txtTenHangCangDauTien
            // 
            this.txtTenHangCangDauTien.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHangCangDauTien.Location = new System.Drawing.Point(112, 651);
            this.txtTenHangCangDauTien.Name = "txtTenHangCangDauTien";
            this.txtTenHangCangDauTien.Size = new System.Drawing.Size(630, 21);
            this.txtTenHangCangDauTien.TabIndex = 26;
            this.txtTenHangCangDauTien.VisualStyleManager = this.vsmMain;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.BackColor = System.Drawing.Color.Transparent;
            this.label79.Location = new System.Drawing.Point(55, 655);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(52, 13);
            this.label79.TabIndex = 26;
            this.label79.Text = "Tên hàng";
            // 
            // txtCangDenTiepTheo
            // 
            this.txtCangDenTiepTheo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCangDenTiepTheo.Location = new System.Drawing.Point(112, 547);
            this.txtCangDenTiepTheo.Name = "txtCangDenTiepTheo";
            this.txtCangDenTiepTheo.Size = new System.Drawing.Size(630, 21);
            this.txtCangDenTiepTheo.TabIndex = 23;
            this.txtCangDenTiepTheo.VisualStyleManager = this.vsmMain;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.BackColor = System.Drawing.Color.Transparent;
            this.label76.Location = new System.Drawing.Point(9, 551);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(99, 13);
            this.label76.TabIndex = 26;
            this.label76.Text = "Cảng đến tiếp theo";
            // 
            // txtCangRoiCuoiCung
            // 
            this.txtCangRoiCuoiCung.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtCangRoiCuoiCung.Location = new System.Drawing.Point(112, 520);
            this.txtCangRoiCuoiCung.Name = "txtCangRoiCuoiCung";
            this.txtCangRoiCuoiCung.Size = new System.Drawing.Size(630, 21);
            this.txtCangRoiCuoiCung.TabIndex = 22;
            this.txtCangRoiCuoiCung.VisualStyleManager = this.vsmMain;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.BackColor = System.Drawing.Color.Transparent;
            this.label75.Location = new System.Drawing.Point(9, 524);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(95, 13);
            this.label75.TabIndex = 26;
            this.label75.Text = "Cảng ròi cuối cùng";
            // 
            // txtTenTau
            // 
            this.txtTenTau.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenTau.Location = new System.Drawing.Point(112, 439);
            this.txtTenTau.Name = "txtTenTau";
            this.txtTenTau.Size = new System.Drawing.Size(508, 21);
            this.txtTenTau.TabIndex = 16;
            this.txtTenTau.VisualStyleManager = this.vsmMain;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.BackColor = System.Drawing.Color.Transparent;
            this.label69.Location = new System.Drawing.Point(9, 443);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(44, 13);
            this.label69.TabIndex = 26;
            this.label69.Text = "Tên tàu";
            // 
            // txtDaiDienDonViCapPhep
            // 
            this.txtDaiDienDonViCapPhep.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDaiDienDonViCapPhep.Enabled = false;
            this.txtDaiDienDonViCapPhep.Location = new System.Drawing.Point(112, 412);
            this.txtDaiDienDonViCapPhep.Name = "txtDaiDienDonViCapPhep";
            this.txtDaiDienDonViCapPhep.Size = new System.Drawing.Size(630, 21);
            this.txtDaiDienDonViCapPhep.TabIndex = 15;
            this.txtDaiDienDonViCapPhep.VisualStyleManager = this.vsmMain;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.BackColor = System.Drawing.Color.Transparent;
            this.label68.Location = new System.Drawing.Point(8, 407);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(81, 26);
            this.label68.TabIndex = 26;
            this.label68.Text = "Đại diện đơn vị \r\ncấp phép";
            // 
            // txtNguoiKiemTra
            // 
            this.txtNguoiKiemTra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNguoiKiemTra.Location = new System.Drawing.Point(112, 285);
            this.txtNguoiKiemTra.Name = "txtNguoiKiemTra";
            this.txtNguoiKiemTra.Size = new System.Drawing.Size(630, 21);
            this.txtNguoiKiemTra.TabIndex = 12;
            this.txtNguoiKiemTra.VisualStyleManager = this.vsmMain;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.BackColor = System.Drawing.Color.Transparent;
            this.label65.Location = new System.Drawing.Point(9, 289);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(76, 13);
            this.label65.TabIndex = 26;
            this.label65.Text = "Người kiểm tra";
            // 
            // txtNoiChuyenDen
            // 
            this.txtNoiChuyenDen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNoiChuyenDen.Location = new System.Drawing.Point(112, 258);
            this.txtNoiChuyenDen.Name = "txtNoiChuyenDen";
            this.txtNoiChuyenDen.Size = new System.Drawing.Size(630, 21);
            this.txtNoiChuyenDen.TabIndex = 11;
            this.txtNoiChuyenDen.VisualStyleManager = this.vsmMain;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.Location = new System.Drawing.Point(9, 262);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(81, 13);
            this.label64.TabIndex = 26;
            this.label64.Text = "Nơi chuyển đến";
            // 
            // txtVatDungKhac
            // 
            this.txtVatDungKhac.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtVatDungKhac.Location = new System.Drawing.Point(112, 148);
            this.txtVatDungKhac.Name = "txtVatDungKhac";
            this.txtVatDungKhac.Size = new System.Drawing.Size(630, 21);
            this.txtVatDungKhac.TabIndex = 8;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Location = new System.Drawing.Point(9, 152);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(75, 13);
            this.label63.TabIndex = 26;
            this.label63.Text = "Vật dụng khác";
            // 
            // txtDiaDiemGiamSat
            // 
            this.txtDiaDiemGiamSat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDiaDiemGiamSat.Location = new System.Drawing.Point(112, 94);
            this.txtDiaDiemGiamSat.Name = "txtDiaDiemGiamSat";
            this.txtDiaDiemGiamSat.Size = new System.Drawing.Size(630, 21);
            this.txtDiaDiemGiamSat.TabIndex = 4;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Location = new System.Drawing.Point(9, 98);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(91, 13);
            this.label48.TabIndex = 26;
            this.label48.Text = "Địa điểm giám sát";
            // 
            // numericEditBox1
            // 
            this.numericEditBox1.DecimalDigits = 3;
            this.numericEditBox1.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox1.Location = new System.Drawing.Point(295, 39);
            this.numericEditBox1.MaxLength = 15;
            this.numericEditBox1.Name = "numericEditBox1";
            this.numericEditBox1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox1.Size = new System.Drawing.Size(81, 21);
            this.numericEditBox1.TabIndex = 43;
            this.numericEditBox1.Text = "0.000";
            this.numericEditBox1.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox1.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.numericEditBox1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.BackColor = System.Drawing.Color.Transparent;
            this.label82.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(232, 44);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(57, 13);
            this.label82.TabIndex = 46;
            this.label82.Text = "Khối lượng";
            // 
            // numericEditBox2
            // 
            this.numericEditBox2.DecimalDigits = 0;
            this.numericEditBox2.EditMode = Janus.Windows.GridEX.NumericEditMode.Value;
            this.numericEditBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numericEditBox2.Location = new System.Drawing.Point(70, 39);
            this.numericEditBox2.MaxLength = 8;
            this.numericEditBox2.Name = "numericEditBox2";
            this.numericEditBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.numericEditBox2.Size = new System.Drawing.Size(81, 21);
            this.numericEditBox2.TabIndex = 40;
            this.numericEditBox2.Text = "0";
            this.numericEditBox2.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.numericEditBox2.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericEditBox2.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.BackColor = System.Drawing.Color.Transparent;
            this.label83.Location = new System.Drawing.Point(15, 44);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(49, 13);
            this.label83.TabIndex = 44;
            this.label83.Text = "Số lượng";
            // 
            // editBox1
            // 
            this.editBox1.Enabled = false;
            this.editBox1.Location = new System.Drawing.Point(70, 12);
            this.editBox1.Name = "editBox1";
            this.editBox1.Size = new System.Drawing.Size(630, 20);
            this.editBox1.TabIndex = 39;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.BackColor = System.Drawing.Color.Transparent;
            this.label84.Location = new System.Drawing.Point(13, 16);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(53, 13);
            this.label84.TabIndex = 41;
            this.label84.Text = "Tên hàng";
            // 
            // ucCategory1
            // 
            this.ucCategory1.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A501;
            this.ucCategory1.Code = "";
            this.ucCategory1.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory1.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory1.IsValidate = true;
            this.ucCategory1.Location = new System.Drawing.Point(382, 36);
            this.ucCategory1.Name = "ucCategory1";
            this.ucCategory1.Name_VN = "";
            this.ucCategory1.SetValidate = false;
            this.ucCategory1.ShowColumnCode = true;
            this.ucCategory1.ShowColumnName = false;
            this.ucCategory1.Size = new System.Drawing.Size(53, 26);
            this.ucCategory1.TabIndex = 45;
            this.ucCategory1.TagName = "";
            this.ucCategory1.WhereCondition = "";
            // 
            // ucCategory2
            // 
            this.ucCategory2.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A316;
            this.ucCategory2.Code = "";
            this.ucCategory2.Cursor = System.Windows.Forms.Cursors.Default;
            this.ucCategory2.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ucCategory2.IsValidate = true;
            this.ucCategory2.Location = new System.Drawing.Point(157, 36);
            this.ucCategory2.Name = "ucCategory2";
            this.ucCategory2.Name_VN = "";
            this.ucCategory2.SetValidate = false;
            this.ucCategory2.ShowColumnCode = true;
            this.ucCategory2.ShowColumnName = false;
            this.ucCategory2.Size = new System.Drawing.Size(53, 26);
            this.ucCategory2.TabIndex = 42;
            this.ucCategory2.TagName = "";
            this.ucCategory2.WhereCondition = "";
            // 
            // VNACC_GiayPhepForm_SAA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 845);
            this.Controls.Add(this.cmdToolBar);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "VNACC_GiayPhepForm_SAA";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin giấy phép (SAA: Application for Quarantine certificate of animals/ anim" +
                "al products)";
            this.Load += new System.EventHandler(this.VNACC_GiayPhepForm_Load);
            this.Controls.SetChildIndex(this.cmdToolBar, 0);
            this.Controls.SetChildIndex(this.uiPanelGuide, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelGuide)).EndInit();
            this.uiPanelGuide.ResumeLayout(false);
            this.uiPanelGuideContainer.ResumeLayout(false);
            this.uiPanelGuideContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdToolBar)).EndInit();
            this.cmdToolBar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiTab1)).EndInit();
            this.uiTab1.ResumeLayout(false);
            this.uiTabPage1.ResumeLayout(false);
            this.uiTabPage1.PerformLayout();
            this.uiTabPage2.ResumeLayout(false);
            this.uiTabPage2.PerformLayout();
            this.pnlHoSoKemTheo.ResumeLayout(false);
            this.pnlHoSoKemTheo.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.UI.CommandBars.UICommandManager cmbMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar cmdToolBar;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao;
        private Janus.Windows.UI.CommandBars.UICommand cmdlayPhanHoi;
        private Janus.Windows.UI.CommandBars.UICommand cmdThemHang1;
        private Janus.Windows.UI.CommandBars.UICommand cmdLuu1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKhaiBao1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoFaxXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtEmailXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenThuongNhanXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNguoiKhai;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDonXinCapPhep;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.GridEX.EditControls.EditBox txtLoaiGiayPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDonViCapPhep;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label38;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucChucNangChungTu;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaQuocGiaXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNguoiKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonViCapPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayPhepKQXL;
        private System.Windows.Forms.Label label42;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaKQXL;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private Janus.Windows.CalendarCombo.CalendarCombo dtHieuLucDenNgayKQXL;
        private Janus.Windows.CalendarCombo.CalendarCombo dtHieuLucTuNgayKQXL;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayCapKQXL;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHaiQuan;
        private Janus.Windows.UI.CommandBars.UICommand Separator1;
        private Janus.Windows.UI.CommandBars.UICommand Separator2;
        private Janus.Windows.UI.CommandBars.UICommand cmdChiThiHaiQuan1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaQuocGiaNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaThuongNhanNhapKhau;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenThuongNhanNhapKhau;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhNhapKhau;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiThuongNhanNhapKhau;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private Janus.Windows.GridEX.EditControls.EditBox txtEmailNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoFaxNhapKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiNhapKhau;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoNhaTenDuongXuatKhau;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtQuanHuyenXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtTinhThanhPhoXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhuongXaXuatKhau;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucPhanLoaiTraCuu;
        private System.Windows.Forms.Label label23;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucMaNuocNguoiKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaBuuChinhNguoiKhai;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label59;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiKhai;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label61;
        private Janus.Windows.GridEX.EditControls.EditBox txtEmailNguoiKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoFaxNguoiKhai;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDienThoaiNguoiKhai;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucNuocXuatKhau;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoDangKyKinhDoanh;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryExtend ucNuocQuaCanh;
        private System.Windows.Forms.Label label16;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucPhuongTienVanChuyen;
        private Company.KDT.SHARE.VNACCS.Controls.ucCuaKhauXuatNhap ucCuaKhauNhap;
        private System.Windows.Forms.Label label62;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGiayChungNhan;
        private Janus.Windows.UI.Tab.UITab uiTab1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage1;
        private Janus.Windows.UI.Tab.UITabPage uiTabPage2;
        private System.Windows.Forms.Label label26;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayGiamSat;
        private System.Windows.Forms.Label label25;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemGiamSat;
        private System.Windows.Forms.Label label48;
        private Janus.Windows.GridEX.EditControls.EditBox txtBenhMienDich;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemKiemDich;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayTiemPhong;
        private System.Windows.Forms.Label label27;
        private Janus.Windows.GridEX.EditControls.EditBox txtNongDo;
        private System.Windows.Forms.Label label29;
        private Janus.Windows.GridEX.EditControls.EditBox txtKhuTrung;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemNuoiTrong;
        private System.Windows.Forms.Label label30;
        private Janus.Windows.CalendarCombo.CalendarCombo dtThoiGianKiemDich;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoBanCanCap;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Panel pnlHoSoKemTheo;
        private System.Windows.Forms.Label label40;
        private Janus.Windows.GridEX.EditControls.EditBox txt10;
        private System.Windows.Forms.Label label35;
        private Janus.Windows.GridEX.EditControls.EditBox txt9;
        private System.Windows.Forms.Label label34;
        private Janus.Windows.GridEX.EditControls.EditBox txt8;
        private System.Windows.Forms.Label label33;
        private Janus.Windows.GridEX.EditControls.EditBox txt7;
        private System.Windows.Forms.Label label32;
        private Janus.Windows.GridEX.EditControls.EditBox txt6;
        private System.Windows.Forms.Label label36;
        private Janus.Windows.GridEX.EditControls.EditBox txt5;
        private System.Windows.Forms.Label label37;
        private Janus.Windows.GridEX.EditControls.EditBox txt4;
        private System.Windows.Forms.Label label39;
        private Janus.Windows.GridEX.EditControls.EditBox txt3;
        private System.Windows.Forms.Label label46;
        private Janus.Windows.GridEX.EditControls.EditBox txt2;
        private System.Windows.Forms.Label label47;
        private Janus.Windows.GridEX.EditControls.EditBox txt1;
        private System.Windows.Forms.Label lblHoSoKemTheo;
        private Janus.Windows.GridEX.EditControls.EditBox txtHoSoLienQuan;
        private System.Windows.Forms.Label label58;
        private Janus.Windows.GridEX.EditControls.EditBox txtVatDungKhac;
        private System.Windows.Forms.Label label63;
        private Janus.Windows.GridEX.EditControls.EditBox txtKetQuaKiemTra;
        private System.Windows.Forms.Label label66;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiKiemTra;
        private System.Windows.Forms.Label label65;
        private Janus.Windows.GridEX.EditControls.EditBox txtNoiChuyenDen;
        private System.Windows.Forms.Label label64;
        private Janus.Windows.GridEX.EditControls.EditBox txtLyDoKhongDat;
        private System.Windows.Forms.Label label67;
        private Janus.Windows.GridEX.EditControls.EditBox txtDaiDienDonViCapPhep;
        private System.Windows.Forms.Label label68;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenTau;
        private System.Windows.Forms.Label label69;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucQuocTich;
        private System.Windows.Forms.Label label70;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenThuyenTruong;
        private System.Windows.Forms.Label label71;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenBacSi;
        private System.Windows.Forms.Label label72;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoHanhKhach;
        private System.Windows.Forms.Label label74;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoThuyenVien;
        private System.Windows.Forms.Label label73;
        private Janus.Windows.GridEX.EditControls.EditBox txtCangDenTiepTheo;
        private System.Windows.Forms.Label label76;
        private Janus.Windows.GridEX.EditControls.EditBox txtCangRoiCuoiCung;
        private System.Windows.Forms.Label label75;
        private Janus.Windows.GridEX.EditControls.EditBox txtCangBocHangDauTien;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongHangCangDauTien;
        private System.Windows.Forms.Label label80;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayRoiCanh;
        private System.Windows.Forms.Label label78;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangCangDauTien;
        private System.Windows.Forms.Label label79;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhSoLuongHangCangDauTien;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhKhoiLuongHangCangDauTien;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtKhoiLuongHangCangDauTien;
        private System.Windows.Forms.Label label81;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox1;
        private System.Windows.Forms.Label label82;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucCategory2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox2;
        private System.Windows.Forms.Label label83;
        private Janus.Windows.GridEX.EditControls.EditBox editBox1;
        private System.Windows.Forms.Label label84;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhKhoiLuongHangCanBoc;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtKhoiLuongHangCanBoc;
        private System.Windows.Forms.Label label87;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategory ucDonViTinhSoLuongHangCanBoc;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongHangCanBoc;
        private System.Windows.Forms.Label label86;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangCanBoc;
        private System.Windows.Forms.Label label85;
        private Janus.Windows.UI.CommandBars.UICommand cmdCangTrungGian1;
        private Janus.Windows.UI.CommandBars.UICommand cmdCangTrungGian;
        private Janus.Windows.CalendarCombo.CalendarCombo dtNgayKiemDich;
        private System.Windows.Forms.Label label88;
        private Janus.Windows.CalendarCombo.CalendarCombo dtThoiGianGiamSat;
        private System.Windows.Forms.Label label24;
        private Janus.Windows.UI.CommandBars.UICommand cmdTinhTrangThamTra;
        private Janus.Windows.UI.CommandBars.UICommand cmdTinhTrangThamTra1;
    }
}