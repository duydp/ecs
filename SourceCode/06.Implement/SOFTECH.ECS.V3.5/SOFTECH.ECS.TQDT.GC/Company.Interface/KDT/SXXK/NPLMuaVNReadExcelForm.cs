using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.KDT.GC;
using System.Globalization;
namespace Company.Interface.KDT.SXXK
{
    public partial class NPLMuaVNReadExcelForm : BaseForm
    {
        public LoaiPhuKien LoaiPKMuaVN=new LoaiPhuKien();
        public PhuKienDangKy PKDK;
        public NPLMuaVNReadExcelForm()
        {
            InitializeComponent();
        }

        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch) 
        {
            return ch - 'A';
        }
        private bool checkNPLExit(string maNPL,LoaiPhuKien LPKTmp)
        {
            foreach (HangPhuKien npl in LPKTmp.HPKCollection)
            {
                if (npl.MaHang.Trim().ToUpper() == maNPL.ToUpper().Trim())
                    return true;
            }
            foreach (HangPhuKien npl in LoaiPKMuaVN.HPKCollection)
            {
                if (npl.MaHang.Trim().ToUpper() == maNPL.ToUpper().Trim())
                    return true;
            }
            return false;
        }
        private bool EmptyCheck( string st)
        {
            if (st == "") return true;
            return false;
        }
        private bool CheckNumber( decimal  so)
        {
            if (so < 0 || so > 1000000000000) return true ;
            return false;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if(!cvError.IsValid)return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;           
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {
                showMsg("MSG_0203008");
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại", false);
                return;
            }
            try
            {
               ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                showMsg("MSG_EXC01", txtSheet.Text);
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                return;
            }
            LoaiPhuKien LPKTmp = new LoaiPhuKien();
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn ;
            int maHangCol = 0;      
            char dongiaColumn ;
            int dongiaCol = 0;
            char SoLuongColumn ;
            int SoLuongCol = 0;
            int j=0;
            try{
                  maHangColumn = Convert.ToChar(txtMaHangColumn.Text);
                  maHangCol = ConvertCharToInt(maHangColumn);                 
                  dongiaColumn = Convert.ToChar(txtDonGiaColumn.Text);
                  dongiaCol = ConvertCharToInt(dongiaColumn);
                  SoLuongColumn = Convert.ToChar(txtSoLuong.Text);
                  SoLuongCol = ConvertCharToInt(SoLuongColumn);
            }
            catch { return; }
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                        npl.Ma = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        npl.HopDong_ID = PKDK.HopDong_ID;
                        if (!npl.Load())
                        {
                            string st=ShowMessage("Nguyên phụ liệu : " + npl.Ma + " chưa khai báo trong hợp đồng.Bạn có muốn tiếp tục không ?", true);
                            if (st == "Yes")
                                continue;
                            else
                                return; 
                        }
                        if (npl.TrangThai == 2)
                        {
                            string st=ShowMessage("Nguyên phụ liệu : " + npl.Ma + " đang được điều chỉnh nhưng chưa được hải quan duyệt nên không thể khai cung ứng được nên sẽ được bỏ qua.Bạn có muốn tiếp tục không ?",true);
                            if (st == "Yes")
                                continue;
                            else
                                return;
                        }
                        if (checkNPLExit(npl.Ma,LPKTmp))
                        {
                            j = wsr.Index + 1;
                            string st = ShowMessage("Nguyên phụ liệu : " + npl.Ma + "đã có trên lưới nên sẽ được bỏ qua.Bạn có muốn tiếp tục không ?", true);
                            if (st == "Yes")
                                continue;
                            else
                                return;
                        }
                        else
                        {
                            HangPhuKien HangPK = new HangPhuKien();
                            HangPK.DonGia = Convert.ToDouble(wsr.Cells[dongiaCol].Value);
                            HangPK.MaHang = npl.Ma;
                            HangPK.TenHang = npl.Ten;
                            HangPK.MaHS = npl.MaHS;
                            HangPK.DVT_ID = npl.DVT_ID;
                            HangPK.SoLuong = Convert.ToDecimal(wsr.Cells[SoLuongCol].Value);
                            HangPK.TriGia = HangPK.DonGia * Convert.ToDouble(HangPK.SoLuong);
                            NumberFormatInfo f = new NumberFormatInfo();
                            f.NumberDecimalSeparator = ".";
                            f.NumberGroupSeparator = ",";
                            HangPK.ThongTinCu = npl.SoLuongCungUng.ToString(f);
                            LPKTmp.HPKCollection.Add(HangPK);
                        }
                        //}
                    }
                    catch
                    {
                        j = wsr.Index + 1;
                        if (showMsg("MSG_0203014", wsr.Index + 1, true) != "Yes")
                        //if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?","MSG_EXC06",j.ToString(), true) != "Yes") 
                        {
                            return;
                        }
                    }
                }
            }
            foreach (HangPhuKien HPK in LPKTmp.HPKCollection)
            {
                LoaiPKMuaVN.HPKCollection.Add(HPK);
            }
            try
            {
                LoaiPKMuaVN.MaPhuKien = "N11";
                LoaiPKMuaVN.NoiDung = LoaiPhuKien_GetName("N11");
                LoaiPKMuaVN.Master_ID = this.PKDK.ID;
                LoaiPKMuaVN.InsertUpdateMuaVN(PKDK.HopDong_ID);
            }
            catch(Exception ex)
            {
                ShowMessage("Lỗi : "+ex.Message, false);
            }
            this.Close();
        }

        private void NguyenPhuLieuReadExcelForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
        }
       
    }
}