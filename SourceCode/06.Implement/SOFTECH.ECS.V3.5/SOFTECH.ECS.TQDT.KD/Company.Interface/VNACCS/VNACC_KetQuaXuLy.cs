﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS.LogMessages;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Janus.Windows.GridEX;
using System.IO;

namespace Company.Interface.VNACCS
{
    public partial class VNACC_KetQuaXuLy : BaseForm
    {
        long Item_id;
        string TypeMsg = "";
        bool hys = false;
        public VNACC_KetQuaXuLy(long itemID, string typeKQ)
        {
            InitializeComponent();
            this.Item_id = itemID;
            this.TypeMsg = typeKQ;
        }
        public VNACC_KetQuaXuLy(long itemID)
        {
            InitializeComponent();
            this.Item_id = itemID;
            this.hys = true;
        }
        public long master_id;
        public string inputMsgId;
        private void VNACC_KetQuaXuLy_Load(object sender, EventArgs e)
        {
            try
            {
                dgList.RootTable.Columns["TieuDeThongBao"].Width = 150;
                dgList.RootTable.Columns["Ngay"].Width = 130;

                if (Item_id > 0)
                {
                    DataTable dt;
                    if (hys)
                    {
                        dt = MsgLog.GetKetQuaXuLy(this.Item_id); //MsgPhanBo.SelectDynamic("MessagesInputID = '" + inputMsgId + "'", "CreatedTime").Tables[0];
                    }
                    else
                    {
                        dt = MsgLog.GetKetQuaXuLy(this.Item_id, TypeMsg); //MsgPhanBo.SelectDynamic("MessagesInputID = '" + inputMsgId + "'", "CreatedTime").Tables[0];
                    }

                    if (dt.Rows.Count > 0)
                    {
                        dt.Columns.Add("NoiDungThongBao", typeof(string));
                        foreach (DataRow dr in dt.Rows)
                        {
                            dr["NoiDungThongBao"] = Company.KDT.SHARE.VNACCS.ClassVNACC.ReturnMessages.GetThongBaoPhanHoi(dr["MaNghiepVu"].ToString().Trim());
                        }
                    }
                    dgList.DataSource = dt;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                string Mess = string.Empty;
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    DataRowView dr = (DataRowView)e.Row.DataRow;
                    MsgLog msg = MsgLog.Load(System.Convert.ToInt32(dr["ID"]));
                    if (msg != null)
                    {
                        ResponseVNACCS feedback = new ResponseVNACCS(msg.Log_Messages);
                        feedback.GetError();
                        if (feedback.Error != null && feedback.Error.Count > 0)
                        {
                            string maNV = dr["MaNghiepVu"].ToString().Trim();
                            if (maNV.Contains("01") && maNV.Length >= 5)
                                maNV = maNV.Substring(0, 5);
                            else
                                maNV = maNV.Substring(0, 3);
                            Helper.Controls.ErrorMessageBoxForm_VNACC.ShowMessage(maNV, feedback.Error, false, Helper.Controls.MessageType.Error);
                        }
                        else
                        {
                            string thongbao = "<Input Messsages ID: " + dr["InputMessagesID"].ToString() + "> :\r\n";
                            thongbao += dr["NoiDungThongBao"];
                            ShowMessage(thongbao, false);
                        }
                    }
                    // ShowMessagesEDI f = new ShowMessagesEDI(Mess);
                    // f.ShowDialog(this);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        private void messagesGốcToolStripMenuItem_Click(object sender, EventArgs ev)
        {
            try
            {
                GridEXSelectedItem e = dgList.SelectedItems[0];
                if (e.GetRow().RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    DataRowView dr = (DataRowView)e.GetRow().DataRow;
                    MsgLog msg = MsgLog.Load(System.Convert.ToInt32(dr["ID"]));
                    if (msg != null)
                    {
                        ShowMessagesEDI f = new ShowMessagesEDI(msg.Log_Messages);
                        f.ShowDialog(this);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnEDI_Output_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> list = ChonMessageIDAOrEDI();
                GhiMessageRaText(list);  
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }        
        }

        private void GhiMessageRaText(List<string> list)
        {
            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.DefaultExt = "txt";
                saveFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                saveFileDialog1.AddExtension = true;
                saveFileDialog1.RestoreDirectory = true;
                saveFileDialog1.Title = "Where do you want to save the file?";
                saveFileDialog1.InitialDirectory = @"C:/";
                if (list[0] == "IDA")
                    saveFileDialog1.FileName = "EDI_IDA_";
                else
                    saveFileDialog1.FileName = "EDI_EDA_";
                if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                    StreamWriter sw = new StreamWriter(fs);
                    try
                    {
                        sw.Write(list[1]);
                        sw.Flush();
                       ShowMessage("Bạn đã tạo EDI thành công.",false);
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        //MessageBox.Show("Có lỗi trong quá trình tạo EDI.");
                    }
                    finally
                    {
                        sw.Close();
                        fs.Close();
                    }

                }
                else
                {
                    ShowMessage("You hit cancel or closed the dialog.", false);
                    //MessageBox.Show("You hit cancel or closed the dialog.");
                }
                saveFileDialog1.Dispose();
                saveFileDialog1 = null;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }


        private List<string> ChonMessageIDAOrEDI()
        {
            try
            {
                DataTable data = (DataTable)this.dgList.DataSource;

                int rs = 0;

                List<string> list = new List<string>() { "", "" };

                //string result = "";

                if (data.Rows.Count > 0)
                {
                    //Tim ID lon nhat
                    foreach (DataRow dr in data.Rows)
                    {
                        int temp = Convert.ToInt32(dr["ID"]);
                        if ((dr["MaNghiepVu"].ToString() == "IDA" || dr["MaNghiepVu"].ToString() == "EDA") && temp > rs)
                        {
                            list[0] = dr["MaNghiepVu"].ToString();
                            rs = temp;
                        }

                    }
                }

                MsgLog msg = MsgLog.Load(rs);
                list[1] = msg.Log_Messages;
                return list; 
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

                return null;
            }
        }
    }
}
