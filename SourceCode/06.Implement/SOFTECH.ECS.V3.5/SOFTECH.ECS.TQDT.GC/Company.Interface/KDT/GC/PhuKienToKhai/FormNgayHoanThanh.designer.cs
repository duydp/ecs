﻿namespace Company.Interface.KDT.GC
{
    partial class FormNgayHoanThanh
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNgayHoanThanh));
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnLuu = new Janus.Windows.EditControls.UIButton();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.clcNgayHoanThanHMoi = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayHoanThanhCu = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.label2);
            this.grbMain.Controls.Add(this.label1);
            this.grbMain.Controls.Add(this.clcNgayHoanThanHMoi);
            this.grbMain.Controls.Add(this.clcNgayHoanThanhCu);
            this.grbMain.Controls.Add(this.btnLuu);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Size = new System.Drawing.Size(347, 120);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(269, 95);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 9;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnLuu
            // 
            this.btnLuu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLuu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLuu.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLuu.Icon")));
            this.btnLuu.Location = new System.Drawing.Point(190, 94);
            this.btnLuu.Name = "btnLuu";
            this.btnLuu.Size = new System.Drawing.Size(72, 23);
            this.btnLuu.TabIndex = 10;
            this.btnLuu.Text = "Lưu";
            this.btnLuu.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnLuu.VisualStyleManager = this.vsmMain;
            this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(24, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Ngày hoàn thành hệ thống";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(24, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Ngày hoàn thành sửa đổi";
            // 
            // clcNgayHoanThanHMoi
            // 
            this.clcNgayHoanThanHMoi.Location = new System.Drawing.Point(190, 54);
            this.clcNgayHoanThanHMoi.Name = "clcNgayHoanThanHMoi";
            this.clcNgayHoanThanHMoi.ReadOnly = false;
            this.clcNgayHoanThanHMoi.Size = new System.Drawing.Size(130, 21);
            this.clcNgayHoanThanHMoi.TabIndex = 11;
            this.clcNgayHoanThanHMoi.TagName = "";
            this.clcNgayHoanThanHMoi.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayHoanThanHMoi.WhereCondition = "";
            // 
            // clcNgayHoanThanhCu
            // 
            this.clcNgayHoanThanhCu.Location = new System.Drawing.Point(190, 24);
            this.clcNgayHoanThanhCu.Name = "clcNgayHoanThanhCu";
            this.clcNgayHoanThanhCu.ReadOnly = false;
            this.clcNgayHoanThanhCu.Size = new System.Drawing.Size(130, 21);
            this.clcNgayHoanThanhCu.TabIndex = 11;
            this.clcNgayHoanThanhCu.TagName = "";
            this.clcNgayHoanThanhCu.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayHoanThanhCu.WhereCondition = "";
            // 
            // FormNgayHoanThanh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(347, 120);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormNgayHoanThanh";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Phụ kiện sửa thông tin ngày đưa vào thanh khoản";
            this.Load += new System.EventHandler(this.FormNgayHoanThanh_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnLuu;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHoanThanHMoi;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHoanThanhCu;

    }
}