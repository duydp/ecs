﻿namespace CA_QuanLyGCN.Report
{
    partial class CauHinhFont
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CauHinhFont));
            Janus.Windows.Common.JanusColorScheme janusColorScheme2 = new Janus.Windows.Common.JanusColorScheme();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiButton2 = new Janus.Windows.EditControls.UIButton();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.numericEditBox20 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox19 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox18 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox17 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox16 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox15 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox14 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox13 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox12 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox11 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox10 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox9 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox8 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox7 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox6 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox5 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox4 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox3 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox2 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.numericEditBox1 = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.uiCheckBox1 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox2 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox3 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox4 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox5 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox6 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox7 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox8 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox9 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox10 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox11 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox12 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox13 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox14 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox15 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox16 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox17 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox18 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox19 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox20 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox21 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox22 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox23 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox24 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox25 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox26 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox27 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox28 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox29 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox30 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox31 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox32 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox33 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox34 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox35 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox36 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox37 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox38 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox39 = new Janus.Windows.EditControls.UICheckBox();
            this.uiCheckBox40 = new Janus.Windows.EditControls.UICheckBox();
            this.chkCheckInDam = new Janus.Windows.EditControls.UICheckBox();
            this.chkCheckInNghieng = new Janus.Windows.EditControls.UICheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Tên doanh nghiệp";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(35, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(108, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Địa chỉ doanh nghiệp";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(35, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mã Doanh Nghiệp";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(35, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(153, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Cơ quan cấp mã doanh nghiệp";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 144);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(138, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Ngày cấp mã doanh nghiệp";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(35, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên người đại diện";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(35, 204);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Quốc tịch";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(35, 234);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(59, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Chức danh";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(35, 264);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Số CMND";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(35, 294);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Ngày cấp CMND";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(35, 324);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(103, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Cơ quan cấp CMND";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(35, 354);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(158, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Hộ Khẩu đại diện doanh nghiệp";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(35, 384);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(171, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Tên cơ quan cấp giấy chứng nhận";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(35, 414);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(183, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Tên phòng ban cấp giấy chứng nhận";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(35, 445);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(178, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Tên đầy đủ phòng ban cấp giấy CN";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(35, 475);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(102, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Số giấy chứng nhận";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(35, 505);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(111, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Tên cơ sở kinh doanh";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.uiGroupBox1.Controls.Add(this.uiCheckBox40);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox38);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox36);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox34);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox32);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox30);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox28);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox26);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox24);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox22);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox20);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox18);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox16);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox14);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox12);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox10);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox8);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox6);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox4);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox2);
            this.uiGroupBox1.Controls.Add(this.chkCheckInNghieng);
            this.uiGroupBox1.Controls.Add(this.chkCheckInDam);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox39);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox37);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox35);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox33);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox31);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox29);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox27);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox25);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox23);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox21);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox19);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox17);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox15);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox13);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox11);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox9);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox7);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox5);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox3);
            this.uiGroupBox1.Controls.Add(this.uiCheckBox1);
            this.uiGroupBox1.Controls.Add(this.uiButton2);
            this.uiGroupBox1.Controls.Add(this.uiButton1);
            this.uiGroupBox1.Controls.Add(this.numericEditBox20);
            this.uiGroupBox1.Controls.Add(this.numericEditBox19);
            this.uiGroupBox1.Controls.Add(this.numericEditBox18);
            this.uiGroupBox1.Controls.Add(this.numericEditBox17);
            this.uiGroupBox1.Controls.Add(this.numericEditBox16);
            this.uiGroupBox1.Controls.Add(this.numericEditBox15);
            this.uiGroupBox1.Controls.Add(this.numericEditBox14);
            this.uiGroupBox1.Controls.Add(this.numericEditBox13);
            this.uiGroupBox1.Controls.Add(this.numericEditBox12);
            this.uiGroupBox1.Controls.Add(this.numericEditBox11);
            this.uiGroupBox1.Controls.Add(this.numericEditBox10);
            this.uiGroupBox1.Controls.Add(this.numericEditBox9);
            this.uiGroupBox1.Controls.Add(this.numericEditBox8);
            this.uiGroupBox1.Controls.Add(this.numericEditBox7);
            this.uiGroupBox1.Controls.Add(this.numericEditBox6);
            this.uiGroupBox1.Controls.Add(this.numericEditBox5);
            this.uiGroupBox1.Controls.Add(this.numericEditBox4);
            this.uiGroupBox1.Controls.Add(this.numericEditBox3);
            this.uiGroupBox1.Controls.Add(this.numericEditBox2);
            this.uiGroupBox1.Controls.Add(this.numericEditBox1);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label20);
            this.uiGroupBox1.Controls.Add(this.label19);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.label17);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label16);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label12);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(485, 672);
            this.uiGroupBox1.TabIndex = 1;
            this.uiGroupBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiButton2
            // 
            this.uiButton2.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton2.Icon")));
            this.uiButton2.Location = new System.Drawing.Point(12, 643);
            this.uiButton2.Name = "uiButton2";
            this.uiButton2.Size = new System.Drawing.Size(158, 23);
            this.uiButton2.TabIndex = 2;
            this.uiButton2.Text = "Load cấu hình mặc định";
            this.uiButton2.VisualStyleManager = this.visualStyleManager1;
            this.uiButton2.Click += new System.EventHandler(this.uiButton2_Click);
            // 
            // visualStyleManager1
            // 
            janusColorScheme2.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme2.Name = "Scheme0";
            janusColorScheme2.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme2.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme2);
            // 
            // uiButton1
            // 
            this.uiButton1.Icon = ((System.Drawing.Icon)(resources.GetObject("uiButton1.Icon")));
            this.uiButton1.Location = new System.Drawing.Point(373, 643);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(100, 23);
            this.uiButton1.TabIndex = 2;
            this.uiButton1.Text = "Lưu cấu hình";
            this.uiButton1.VisualStyleManager = this.visualStyleManager1;
            this.uiButton1.Click += new System.EventHandler(this.uiButton1_Click);
            // 
            // numericEditBox20
            // 
            this.numericEditBox20.Location = new System.Drawing.Point(235, 589);
            this.numericEditBox20.Name = "numericEditBox20";
            this.numericEditBox20.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox20.TabIndex = 1;
            this.numericEditBox20.Text = "0.00";
            this.numericEditBox20.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox20.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox19
            // 
            this.numericEditBox19.Location = new System.Drawing.Point(235, 560);
            this.numericEditBox19.Name = "numericEditBox19";
            this.numericEditBox19.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox19.TabIndex = 1;
            this.numericEditBox19.Text = "0.00";
            this.numericEditBox19.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox19.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox18
            // 
            this.numericEditBox18.Location = new System.Drawing.Point(235, 530);
            this.numericEditBox18.Name = "numericEditBox18";
            this.numericEditBox18.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox18.TabIndex = 1;
            this.numericEditBox18.Text = "0.00";
            this.numericEditBox18.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox18.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox17
            // 
            this.numericEditBox17.Location = new System.Drawing.Point(235, 500);
            this.numericEditBox17.Name = "numericEditBox17";
            this.numericEditBox17.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox17.TabIndex = 1;
            this.numericEditBox17.Text = "0.00";
            this.numericEditBox17.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox17.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox16
            // 
            this.numericEditBox16.Location = new System.Drawing.Point(235, 470);
            this.numericEditBox16.Name = "numericEditBox16";
            this.numericEditBox16.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox16.TabIndex = 1;
            this.numericEditBox16.Text = "0.00";
            this.numericEditBox16.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox16.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox15
            // 
            this.numericEditBox15.Location = new System.Drawing.Point(235, 440);
            this.numericEditBox15.Name = "numericEditBox15";
            this.numericEditBox15.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox15.TabIndex = 1;
            this.numericEditBox15.Text = "0.00";
            this.numericEditBox15.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox15.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox14
            // 
            this.numericEditBox14.Location = new System.Drawing.Point(235, 410);
            this.numericEditBox14.Name = "numericEditBox14";
            this.numericEditBox14.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox14.TabIndex = 1;
            this.numericEditBox14.Text = "0.00";
            this.numericEditBox14.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox14.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox13
            // 
            this.numericEditBox13.Location = new System.Drawing.Point(235, 380);
            this.numericEditBox13.Name = "numericEditBox13";
            this.numericEditBox13.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox13.TabIndex = 1;
            this.numericEditBox13.Text = "0.00";
            this.numericEditBox13.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox13.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox12
            // 
            this.numericEditBox12.Location = new System.Drawing.Point(235, 350);
            this.numericEditBox12.Name = "numericEditBox12";
            this.numericEditBox12.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox12.TabIndex = 1;
            this.numericEditBox12.Text = "0.00";
            this.numericEditBox12.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox12.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox11
            // 
            this.numericEditBox11.Location = new System.Drawing.Point(235, 320);
            this.numericEditBox11.Name = "numericEditBox11";
            this.numericEditBox11.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox11.TabIndex = 1;
            this.numericEditBox11.Text = "0.00";
            this.numericEditBox11.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox11.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox10
            // 
            this.numericEditBox10.Location = new System.Drawing.Point(235, 290);
            this.numericEditBox10.Name = "numericEditBox10";
            this.numericEditBox10.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox10.TabIndex = 1;
            this.numericEditBox10.Text = "0.00";
            this.numericEditBox10.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox10.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox9
            // 
            this.numericEditBox9.Location = new System.Drawing.Point(235, 260);
            this.numericEditBox9.Name = "numericEditBox9";
            this.numericEditBox9.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox9.TabIndex = 1;
            this.numericEditBox9.Text = "0.00";
            this.numericEditBox9.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox9.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox8
            // 
            this.numericEditBox8.Location = new System.Drawing.Point(235, 230);
            this.numericEditBox8.Name = "numericEditBox8";
            this.numericEditBox8.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox8.TabIndex = 1;
            this.numericEditBox8.Text = "0.00";
            this.numericEditBox8.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox8.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox7
            // 
            this.numericEditBox7.Location = new System.Drawing.Point(235, 200);
            this.numericEditBox7.Name = "numericEditBox7";
            this.numericEditBox7.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox7.TabIndex = 1;
            this.numericEditBox7.Text = "0.00";
            this.numericEditBox7.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox7.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox6
            // 
            this.numericEditBox6.Location = new System.Drawing.Point(235, 170);
            this.numericEditBox6.Name = "numericEditBox6";
            this.numericEditBox6.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox6.TabIndex = 1;
            this.numericEditBox6.Text = "0.00";
            this.numericEditBox6.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox6.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox5
            // 
            this.numericEditBox5.Location = new System.Drawing.Point(235, 140);
            this.numericEditBox5.Name = "numericEditBox5";
            this.numericEditBox5.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox5.TabIndex = 1;
            this.numericEditBox5.Text = "0.00";
            this.numericEditBox5.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox5.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox4
            // 
            this.numericEditBox4.Location = new System.Drawing.Point(235, 110);
            this.numericEditBox4.Name = "numericEditBox4";
            this.numericEditBox4.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox4.TabIndex = 1;
            this.numericEditBox4.Text = "0.00";
            this.numericEditBox4.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox4.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox3
            // 
            this.numericEditBox3.Location = new System.Drawing.Point(235, 80);
            this.numericEditBox3.Name = "numericEditBox3";
            this.numericEditBox3.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox3.TabIndex = 1;
            this.numericEditBox3.Text = "0.00";
            this.numericEditBox3.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox3.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox2
            // 
            this.numericEditBox2.Location = new System.Drawing.Point(235, 50);
            this.numericEditBox2.Name = "numericEditBox2";
            this.numericEditBox2.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox2.TabIndex = 1;
            this.numericEditBox2.Text = "0.00";
            this.numericEditBox2.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox2.VisualStyleManager = this.visualStyleManager1;
            // 
            // numericEditBox1
            // 
            this.numericEditBox1.Location = new System.Drawing.Point(235, 19);
            this.numericEditBox1.Name = "numericEditBox1";
            this.numericEditBox1.Size = new System.Drawing.Size(100, 20);
            this.numericEditBox1.TabIndex = 1;
            this.numericEditBox1.Text = "0.00";
            this.numericEditBox1.Value = new decimal(new int[] {
            0,
            0,
            0,
            131072});
            this.numericEditBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(35, 594);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(135, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Ngày cấp giấy chứng nhận";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(35, 565);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(125, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Địa chỉ cơ sở kinh doanh";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(35, 535);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(122, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Ngành nghề kinh doanh";
            // 
            // uiCheckBox1
            // 
            this.uiCheckBox1.Location = new System.Drawing.Point(341, 19);
            this.uiCheckBox1.Name = "uiCheckBox1";
            this.uiCheckBox1.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox1.TabIndex = 3;
            this.uiCheckBox1.Text = "In đậm";
            this.uiCheckBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox2
            // 
            this.uiCheckBox2.Location = new System.Drawing.Point(407, 19);
            this.uiCheckBox2.Name = "uiCheckBox2";
            this.uiCheckBox2.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox2.TabIndex = 3;
            this.uiCheckBox2.Text = "In nghiêng";
            this.uiCheckBox2.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox3
            // 
            this.uiCheckBox3.Location = new System.Drawing.Point(341, 49);
            this.uiCheckBox3.Name = "uiCheckBox3";
            this.uiCheckBox3.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox3.TabIndex = 3;
            this.uiCheckBox3.Text = "In đậm";
            this.uiCheckBox3.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox4
            // 
            this.uiCheckBox4.Location = new System.Drawing.Point(407, 49);
            this.uiCheckBox4.Name = "uiCheckBox4";
            this.uiCheckBox4.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox4.TabIndex = 3;
            this.uiCheckBox4.Text = "In nghiêng";
            this.uiCheckBox4.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox5
            // 
            this.uiCheckBox5.Location = new System.Drawing.Point(341, 79);
            this.uiCheckBox5.Name = "uiCheckBox5";
            this.uiCheckBox5.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox5.TabIndex = 3;
            this.uiCheckBox5.Text = "In đậm";
            this.uiCheckBox5.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox6
            // 
            this.uiCheckBox6.Location = new System.Drawing.Point(407, 79);
            this.uiCheckBox6.Name = "uiCheckBox6";
            this.uiCheckBox6.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox6.TabIndex = 3;
            this.uiCheckBox6.Text = "In nghiêng";
            this.uiCheckBox6.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox7
            // 
            this.uiCheckBox7.Location = new System.Drawing.Point(341, 109);
            this.uiCheckBox7.Name = "uiCheckBox7";
            this.uiCheckBox7.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox7.TabIndex = 3;
            this.uiCheckBox7.Text = "In đậm";
            this.uiCheckBox7.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox8
            // 
            this.uiCheckBox8.Location = new System.Drawing.Point(407, 109);
            this.uiCheckBox8.Name = "uiCheckBox8";
            this.uiCheckBox8.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox8.TabIndex = 3;
            this.uiCheckBox8.Text = "In nghiêng";
            this.uiCheckBox8.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox9
            // 
            this.uiCheckBox9.Location = new System.Drawing.Point(341, 139);
            this.uiCheckBox9.Name = "uiCheckBox9";
            this.uiCheckBox9.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox9.TabIndex = 3;
            this.uiCheckBox9.Text = "In đậm";
            this.uiCheckBox9.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox10
            // 
            this.uiCheckBox10.Location = new System.Drawing.Point(407, 139);
            this.uiCheckBox10.Name = "uiCheckBox10";
            this.uiCheckBox10.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox10.TabIndex = 3;
            this.uiCheckBox10.Text = "In nghiêng";
            this.uiCheckBox10.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox11
            // 
            this.uiCheckBox11.Location = new System.Drawing.Point(341, 168);
            this.uiCheckBox11.Name = "uiCheckBox11";
            this.uiCheckBox11.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox11.TabIndex = 3;
            this.uiCheckBox11.Text = "In đậm";
            this.uiCheckBox11.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox12
            // 
            this.uiCheckBox12.Location = new System.Drawing.Point(407, 168);
            this.uiCheckBox12.Name = "uiCheckBox12";
            this.uiCheckBox12.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox12.TabIndex = 3;
            this.uiCheckBox12.Text = "In nghiêng";
            this.uiCheckBox12.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox13
            // 
            this.uiCheckBox13.Location = new System.Drawing.Point(341, 200);
            this.uiCheckBox13.Name = "uiCheckBox13";
            this.uiCheckBox13.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox13.TabIndex = 3;
            this.uiCheckBox13.Text = "In đậm";
            this.uiCheckBox13.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox14
            // 
            this.uiCheckBox14.Location = new System.Drawing.Point(407, 200);
            this.uiCheckBox14.Name = "uiCheckBox14";
            this.uiCheckBox14.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox14.TabIndex = 3;
            this.uiCheckBox14.Text = "In nghiêng";
            this.uiCheckBox14.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox15
            // 
            this.uiCheckBox15.Location = new System.Drawing.Point(341, 229);
            this.uiCheckBox15.Name = "uiCheckBox15";
            this.uiCheckBox15.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox15.TabIndex = 3;
            this.uiCheckBox15.Text = "In đậm";
            this.uiCheckBox15.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox16
            // 
            this.uiCheckBox16.Location = new System.Drawing.Point(407, 229);
            this.uiCheckBox16.Name = "uiCheckBox16";
            this.uiCheckBox16.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox16.TabIndex = 3;
            this.uiCheckBox16.Text = "In nghiêng";
            this.uiCheckBox16.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox17
            // 
            this.uiCheckBox17.Location = new System.Drawing.Point(341, 259);
            this.uiCheckBox17.Name = "uiCheckBox17";
            this.uiCheckBox17.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox17.TabIndex = 3;
            this.uiCheckBox17.Text = "In đậm";
            this.uiCheckBox17.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox18
            // 
            this.uiCheckBox18.Location = new System.Drawing.Point(407, 259);
            this.uiCheckBox18.Name = "uiCheckBox18";
            this.uiCheckBox18.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox18.TabIndex = 3;
            this.uiCheckBox18.Text = "In nghiêng";
            this.uiCheckBox18.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox19
            // 
            this.uiCheckBox19.Location = new System.Drawing.Point(341, 289);
            this.uiCheckBox19.Name = "uiCheckBox19";
            this.uiCheckBox19.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox19.TabIndex = 3;
            this.uiCheckBox19.Text = "In đậm";
            this.uiCheckBox19.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox20
            // 
            this.uiCheckBox20.Location = new System.Drawing.Point(407, 289);
            this.uiCheckBox20.Name = "uiCheckBox20";
            this.uiCheckBox20.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox20.TabIndex = 3;
            this.uiCheckBox20.Text = "In nghiêng";
            this.uiCheckBox20.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox21
            // 
            this.uiCheckBox21.Location = new System.Drawing.Point(341, 319);
            this.uiCheckBox21.Name = "uiCheckBox21";
            this.uiCheckBox21.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox21.TabIndex = 3;
            this.uiCheckBox21.Text = "In đậm";
            this.uiCheckBox21.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox22
            // 
            this.uiCheckBox22.Location = new System.Drawing.Point(407, 319);
            this.uiCheckBox22.Name = "uiCheckBox22";
            this.uiCheckBox22.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox22.TabIndex = 3;
            this.uiCheckBox22.Text = "In nghiêng";
            this.uiCheckBox22.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox23
            // 
            this.uiCheckBox23.Location = new System.Drawing.Point(341, 349);
            this.uiCheckBox23.Name = "uiCheckBox23";
            this.uiCheckBox23.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox23.TabIndex = 3;
            this.uiCheckBox23.Text = "In đậm";
            this.uiCheckBox23.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox24
            // 
            this.uiCheckBox24.Location = new System.Drawing.Point(407, 349);
            this.uiCheckBox24.Name = "uiCheckBox24";
            this.uiCheckBox24.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox24.TabIndex = 3;
            this.uiCheckBox24.Text = "In nghiêng";
            this.uiCheckBox24.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox25
            // 
            this.uiCheckBox25.Location = new System.Drawing.Point(341, 379);
            this.uiCheckBox25.Name = "uiCheckBox25";
            this.uiCheckBox25.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox25.TabIndex = 3;
            this.uiCheckBox25.Text = "In đậm";
            this.uiCheckBox25.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox26
            // 
            this.uiCheckBox26.Location = new System.Drawing.Point(407, 379);
            this.uiCheckBox26.Name = "uiCheckBox26";
            this.uiCheckBox26.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox26.TabIndex = 3;
            this.uiCheckBox26.Text = "In nghiêng";
            this.uiCheckBox26.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox27
            // 
            this.uiCheckBox27.Location = new System.Drawing.Point(341, 409);
            this.uiCheckBox27.Name = "uiCheckBox27";
            this.uiCheckBox27.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox27.TabIndex = 3;
            this.uiCheckBox27.Text = "In đậm";
            this.uiCheckBox27.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox28
            // 
            this.uiCheckBox28.Location = new System.Drawing.Point(407, 409);
            this.uiCheckBox28.Name = "uiCheckBox28";
            this.uiCheckBox28.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox28.TabIndex = 3;
            this.uiCheckBox28.Text = "In nghiêng";
            this.uiCheckBox28.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox29
            // 
            this.uiCheckBox29.Location = new System.Drawing.Point(341, 440);
            this.uiCheckBox29.Name = "uiCheckBox29";
            this.uiCheckBox29.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox29.TabIndex = 3;
            this.uiCheckBox29.Text = "In đậm";
            this.uiCheckBox29.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox30
            // 
            this.uiCheckBox30.Location = new System.Drawing.Point(407, 440);
            this.uiCheckBox30.Name = "uiCheckBox30";
            this.uiCheckBox30.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox30.TabIndex = 3;
            this.uiCheckBox30.Text = "In nghiêng";
            this.uiCheckBox30.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox31
            // 
            this.uiCheckBox31.Location = new System.Drawing.Point(341, 469);
            this.uiCheckBox31.Name = "uiCheckBox31";
            this.uiCheckBox31.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox31.TabIndex = 3;
            this.uiCheckBox31.Text = "In đậm";
            this.uiCheckBox31.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox32
            // 
            this.uiCheckBox32.Location = new System.Drawing.Point(407, 469);
            this.uiCheckBox32.Name = "uiCheckBox32";
            this.uiCheckBox32.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox32.TabIndex = 3;
            this.uiCheckBox32.Text = "In nghiêng";
            this.uiCheckBox32.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox33
            // 
            this.uiCheckBox33.Location = new System.Drawing.Point(341, 499);
            this.uiCheckBox33.Name = "uiCheckBox33";
            this.uiCheckBox33.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox33.TabIndex = 3;
            this.uiCheckBox33.Text = "In đậm";
            this.uiCheckBox33.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox34
            // 
            this.uiCheckBox34.Location = new System.Drawing.Point(407, 499);
            this.uiCheckBox34.Name = "uiCheckBox34";
            this.uiCheckBox34.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox34.TabIndex = 3;
            this.uiCheckBox34.Text = "In nghiêng";
            this.uiCheckBox34.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox35
            // 
            this.uiCheckBox35.Location = new System.Drawing.Point(341, 530);
            this.uiCheckBox35.Name = "uiCheckBox35";
            this.uiCheckBox35.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox35.TabIndex = 3;
            this.uiCheckBox35.Text = "In đậm";
            this.uiCheckBox35.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox36
            // 
            this.uiCheckBox36.Location = new System.Drawing.Point(407, 530);
            this.uiCheckBox36.Name = "uiCheckBox36";
            this.uiCheckBox36.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox36.TabIndex = 3;
            this.uiCheckBox36.Text = "In nghiêng";
            this.uiCheckBox36.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox37
            // 
            this.uiCheckBox37.Location = new System.Drawing.Point(341, 559);
            this.uiCheckBox37.Name = "uiCheckBox37";
            this.uiCheckBox37.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox37.TabIndex = 3;
            this.uiCheckBox37.Text = "In đậm";
            this.uiCheckBox37.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox38
            // 
            this.uiCheckBox38.Location = new System.Drawing.Point(407, 559);
            this.uiCheckBox38.Name = "uiCheckBox38";
            this.uiCheckBox38.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox38.TabIndex = 3;
            this.uiCheckBox38.Text = "In nghiêng";
            this.uiCheckBox38.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox39
            // 
            this.uiCheckBox39.Location = new System.Drawing.Point(341, 589);
            this.uiCheckBox39.Name = "uiCheckBox39";
            this.uiCheckBox39.Size = new System.Drawing.Size(60, 23);
            this.uiCheckBox39.TabIndex = 3;
            this.uiCheckBox39.Text = "In đậm";
            this.uiCheckBox39.VisualStyleManager = this.visualStyleManager1;
            // 
            // uiCheckBox40
            // 
            this.uiCheckBox40.Location = new System.Drawing.Point(407, 589);
            this.uiCheckBox40.Name = "uiCheckBox40";
            this.uiCheckBox40.Size = new System.Drawing.Size(66, 23);
            this.uiCheckBox40.TabIndex = 3;
            this.uiCheckBox40.Text = "In nghiêng";
            this.uiCheckBox40.VisualStyleManager = this.visualStyleManager1;
            // 
            // chkCheckInDam
            // 
            this.chkCheckInDam.Location = new System.Drawing.Point(341, 615);
            this.chkCheckInDam.Name = "chkCheckInDam";
            this.chkCheckInDam.Size = new System.Drawing.Size(60, 23);
            this.chkCheckInDam.TabIndex = 3;
            this.chkCheckInDam.Text = "Check all";
            this.chkCheckInDam.VisualStyleManager = this.visualStyleManager1;
            this.chkCheckInDam.CheckedChanged += new System.EventHandler(this.chkCheckInDam_CheckedChanged);
            // 
            // chkCheckInNghieng
            // 
            this.chkCheckInNghieng.Location = new System.Drawing.Point(407, 615);
            this.chkCheckInNghieng.Name = "chkCheckInNghieng";
            this.chkCheckInNghieng.Size = new System.Drawing.Size(66, 23);
            this.chkCheckInNghieng.TabIndex = 3;
            this.chkCheckInNghieng.Text = "Check all";
            this.chkCheckInNghieng.VisualStyleManager = this.visualStyleManager1;
            this.chkCheckInNghieng.CheckedChanged += new System.EventHandler(this.uiCheckBox42_CheckedChanged);
            // 
            // CauHinhFont
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 672);
            this.Controls.Add(this.uiGroupBox1);
            this.Name = "CauHinhFont";
            this.Text = "Cấu hình cỡ chữ trên bản in";
            this.Load += new System.EventHandler(this.CauHinhFont_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox20;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox19;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox18;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox17;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox16;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox15;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox14;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox13;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox12;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox11;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox10;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox9;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox8;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox7;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox5;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox numericEditBox1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private Janus.Windows.EditControls.UIButton uiButton2;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox2;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox1;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox40;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox38;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox36;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox34;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox32;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox30;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox28;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox26;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox24;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox22;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox20;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox18;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox16;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox14;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox12;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox10;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox8;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox6;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox4;
        private Janus.Windows.EditControls.UICheckBox chkCheckInNghieng;
        private Janus.Windows.EditControls.UICheckBox chkCheckInDam;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox39;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox37;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox35;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox33;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox31;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox29;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox27;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox25;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox23;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox21;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox19;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox17;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox15;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox13;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox11;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox9;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox7;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox5;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox3;
    }
}