-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteBy_AnDinhThue_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteBy_AnDinhThue_ID]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Insert]
	@AnDinhThue_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@SoThueKhaiBao numeric(11, 0),
	@SoThueAnDinh numeric(11, 0),
	@SoThueChenhLech numeric(12, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
(
	[AnDinhThue_ID],
	[TenSacThue],
	[TieuMuc],
	[SoThueKhaiBao],
	[SoThueAnDinh],
	[SoThueChenhLech]
)
VALUES 
(
	@AnDinhThue_ID,
	@TenSacThue,
	@TieuMuc,
	@SoThueKhaiBao,
	@SoThueAnDinh,
	@SoThueChenhLech
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Update]
	@ID bigint,
	@AnDinhThue_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@SoThueKhaiBao numeric(11, 0),
	@SoThueAnDinh numeric(11, 0),
	@SoThueChenhLech numeric(12, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
SET
	[AnDinhThue_ID] = @AnDinhThue_ID,
	[TenSacThue] = @TenSacThue,
	[TieuMuc] = @TieuMuc,
	[SoThueKhaiBao] = @SoThueKhaiBao,
	[SoThueAnDinh] = @SoThueAnDinh,
	[SoThueChenhLech] = @SoThueChenhLech
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_InsertUpdate]
	@ID bigint,
	@AnDinhThue_ID bigint,
	@TenSacThue nvarchar(27),
	@TieuMuc numeric(4, 0),
	@SoThueKhaiBao numeric(11, 0),
	@SoThueAnDinh numeric(11, 0),
	@SoThueChenhLech numeric(12, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue] 
		SET
			[AnDinhThue_ID] = @AnDinhThue_ID,
			[TenSacThue] = @TenSacThue,
			[TieuMuc] = @TieuMuc,
			[SoThueKhaiBao] = @SoThueKhaiBao,
			[SoThueAnDinh] = @SoThueAnDinh,
			[SoThueChenhLech] = @SoThueChenhLech
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
		(
			[AnDinhThue_ID],
			[TenSacThue],
			[TieuMuc],
			[SoThueKhaiBao],
			[SoThueAnDinh],
			[SoThueChenhLech]
		)
		VALUES 
		(
			@AnDinhThue_ID,
			@TenSacThue,
			@TieuMuc,
			@SoThueKhaiBao,
			@SoThueAnDinh,
			@SoThueChenhLech
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteBy_AnDinhThue_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteBy_AnDinhThue_ID]
	@AnDinhThue_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
WHERE
	[AnDinhThue_ID] = @AnDinhThue_ID

GO

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[AnDinhThue_ID],
	[TenSacThue],
	[TieuMuc],
	[SoThueKhaiBao],
	[SoThueAnDinh],
	[SoThueChenhLech]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectBy_AnDinhThue_ID]
	@AnDinhThue_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[AnDinhThue_ID],
	[TenSacThue],
	[TieuMuc],
	[SoThueKhaiBao],
	[SoThueAnDinh],
	[SoThueChenhLech]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]
WHERE
	[AnDinhThue_ID] = @AnDinhThue_ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[AnDinhThue_ID],
	[TenSacThue],
	[TieuMuc],
	[SoThueKhaiBao],
	[SoThueAnDinh],
	[SoThueChenhLech]
FROM [dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[AnDinhThue_ID],
	[TenSacThue],
	[TieuMuc],
	[SoThueKhaiBao],
	[SoThueAnDinh],
	[SoThueChenhLech]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiMauDich_AnDinhThueSacThue]	

GO

