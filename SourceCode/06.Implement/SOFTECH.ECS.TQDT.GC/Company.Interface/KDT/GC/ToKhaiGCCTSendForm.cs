﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;

namespace Company.Interface.KDT.GC
{
    public partial class ToKhaiGCCTSendForm : BaseForm
    {
        public ToKhaiGCCTSendForm()
        {
            InitializeComponent();
        }

        private ToKhaiChuyenTiepCollection tkctCollection = new ToKhaiChuyenTiepCollection();
        
        private void khoitao_DuLieuChuan()
        {

            this._DonViHaiQuan = DonViHaiQuan.SelectAll();
        }

        private void ToKhaiGCCTSendFormForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
        }

        private void display_ToKhaiChuyenTiep_By_MaHaiQuanTiepNhan()
        {
            string where = string.Format("TrangThai = -1");
            this.tkctCollection = new ToKhaiChuyenTiep().SelectCollectionDynamic(where, "");
            dgList.DataSource = this.tkctCollection;
            dgList.Refetch();
        }

        private ToKhaiChuyenTiep search(object id)
        {
            foreach (ToKhaiChuyenTiep tkct in this.tkctCollection)
            {
                if (tkct.ID == Convert.ToInt64(id))
                {
                    return tkct;
                }
            }
            return null;
        }
        
        private void btnSend_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count == 0) return;
            WSForm wsForm = new WSForm();
            wsForm.ShowDialog(this);
            if (!wsForm.IsReady) return;
            foreach (GridEXSelectedItem i in items)
            {
                    //if (i.RowType == RowType.Record)
                    //{
                    //    ToKhaiChuyenTiep tkct = this.search(i.GetRow().Cells["ID"].Value);
                    //    long soTiepNhan = tkct.WSSend();
                    //    if (soTiepNhan > 0)
                    //        ShowMessage("Khai báo tờ khai chuyển tiếp thành công.\nSố tiếp nhận: " + soTiepNhan, false);
                    //    else
                    //    {
                    //        if (ShowMessage("Khai báo không thành công.\nBạn có muốn đưa vào hàng đợi không?", true) == "Yes")
                    //        {
                    //            HangDoi hd = new HangDoi();
                    //            hd.ID = tkct.ID;
                    //            hd.LoaiToKhai = LoaiToKhai.TO_KHAI_CHUYEN_TIEP;
                    //            hd.TrangThai = tkct.TrangThai;
                    //            hd.ChucNang = ChucNang.KHAI_BAO;
                    //            MainForm.AddToQueueForm(hd);
                    //            MainForm.ShowQueueForm();
                    //        }
                    //        else
                    //        {

                    //        }
                    //    }
                    //}
                
            }
            this.display_ToKhaiChuyenTiep_By_MaHaiQuanTiepNhan();
            this.Cursor = Cursors.Default;
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.display_ToKhaiChuyenTiep_By_MaHaiQuanTiepNhan();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
        }

        private ToKhaiChuyenTiep getTKCTByID(long id)
        {
            foreach (ToKhaiChuyenTiep tk in this.tkctCollection)
            {
                if (tk.ID == id) return tk;
            }
            return null;
        }
        
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
                ToKhaiGCChuyenTiepNhapForm f = new ToKhaiGCChuyenTiepNhapForm();
                f.OpenType = OpenFormType.Edit;
                f.TKCT = this.getTKCTByID(id);                
                f.ShowDialog();
            }
        }

        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
         //   if (ShowMessage("Bạn có muốn xóa tờ khai này không", true) == "Yes")
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                if (e.Row.RowType == RowType.Record)
                {
                    long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
                    ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                    tkct.ID = id;
                    tkct.Delete();
                }
            }
        }
    }
}