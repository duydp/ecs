using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors;
using Activation.Server;

namespace Licenses_v2.Module
{
    public partial class FrmProductInfo : DevExpress.XtraEditors.XtraForm
    {
        private Product Prod;
        public bool isEdit = false;
        public FrmProductInfo(string ID)
        {
            InitializeComponent();
            if (ID == null)
            {
                txtProdID.Text = Math.Abs(DateTime.Now.GetHashCode()).ToString();
                this.Text = "Thêm sản phẩm: " + txtProdID.Text;
                lblCaption.Text = "Thêm sản phẩm: " + txtProdID.Text;
                Prod = new Product();
                this.Name = "Prod_New";
                this.isEdit = true;
            }
            else
            {
                txtProdID.Text = ID;
                this.Text = "Sản phẩm: " + txtProdID.Text;
                lblCaption.Text = "Sản phẩm: " + txtProdID.Text;
                Prod = Product.Load(ID);
                this.Name = "Prod_" + ID;
                this.FillData();
            }
        }

        #region Add Methods.
        private void GetData()
        {
            Prod.ID = txtProdID.Text;
            Prod.Name = txtProdName.Text;
            Prod.Description = txtProdDesc.Text;
        }

        private void FillData()
        {
            txtProdID.Text = Prod.ID;
            txtProdName.Text = Prod.Name;
            txtProdDesc.Text = Prod.Description;
        }

        private void DisableButton(Boolean val)
        {
            btnCancel.Enabled = val;
            btnSave.Enabled = val;
            groupProdInfo.Enabled = val;
            btnEdit.Enabled = !val;
        }
        #endregion

        private void FrmProductInfo_Load(object sender, EventArgs e)
        {
            this.DisableButton(this.isEdit);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.Name == "Prod_New")
            {
                this.Close();
            }
            else
            {
                this.FillData();
                this.DisableButton(false);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            this.isEdit = true;
            this.DisableButton(true);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.GetData();
            if (this.Name == "Prod_New")
            {
                Prod.Insert();
                this.Text = "Sản phẩm: " + Prod.ID;
                lblCaption.Text = "Sản phẩm: " + Prod.ID;
                this.Name = "Prod_" + Prod.ID;
            }
            else
            {
                Prod.Update();
            }
            this.DisableButton(false);
            if (ClassLib.IsFormShowed("FrmCustomerManager"))
            {
                FrmCustomerManager obj = (FrmCustomerManager)Application.OpenForms["FrmCustomerManager"];
                obj.RefreshData();
            }
        }
    }
}