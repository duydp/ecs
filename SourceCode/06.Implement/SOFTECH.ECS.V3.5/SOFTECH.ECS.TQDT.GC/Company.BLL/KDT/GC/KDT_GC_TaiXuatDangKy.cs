﻿using System;
using System.Data.SqlClient;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Xml;
using System.Globalization;
using Company.GC.BLL.Utils;
using Company.GC.BLL.GC;
using System.Threading;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Common;
namespace Company.GC.BLL.KDT.GC
{
    public partial class KDT_GC_TaiXuatDangKy
    {
        private List<KDT_GC_TaiXuat> _ListTaiXuat = new List<KDT_GC_TaiXuat>();

        public List<KDT_GC_TaiXuat> TaiXuat_List
        {
            set { this._ListTaiXuat = value; }
            get { return this._ListTaiXuat; }
        }
        public void DeleteFull(long id)
        {
            this.ID = id;
            List<KDT_GC_TaiXuat> list = KDT_GC_TaiXuat.SelectCollectionDynamic("Master_ID = " + id, "");
            foreach (KDT_GC_TaiXuat item in list)
            {
                item.Delete();
            }
            this.Delete();
        }
        public static KDT_GC_TaiXuatDangKy LoadFull(long id)
        {
            KDT_GC_TaiXuatDangKy TXDK = new KDT_GC_TaiXuatDangKy();
            TXDK = KDT_GC_TaiXuatDangKy.Load(id);
            if (TXDK.ID == id)
            {
                List<KDT_GC_TaiXuat> listTaiXuat = new List<KDT_GC_TaiXuat>();
                listTaiXuat = KDT_GC_TaiXuat.SelectCollectionDynamic("Master_ID = " + TXDK.ID, "");
                foreach (KDT_GC_TaiXuat taiXuat in listTaiXuat)
                {
                    TXDK.TaiXuat_List.Add(taiXuat);
                }
                return TXDK;
            }
            else
                return null;
        }
        public bool InsertUpdateFull()
        {
            bool ret = false;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.TrangThaiXuLy = -1;
                        this.ID = this.Insert(transaction);
                    }
                    else
                        this.Update(transaction);
                    foreach (KDT_GC_TaiXuat item in this.TaiXuat_List)
                    {
                        item.Master_ID = this.ID;
                        if (item.ID == 0)
                        {
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                       
                    }
                    transaction.Commit();
                    ret = true;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    this.ID = 0;
                    connection.Close();
                    throw new Exception(ex.Message);
                    throw;
                }
            }
            return ret;
        }

        public static DataSet SelectTKXTheoHD(long IDHopDong)
        {
            const string spName = "[dbo].[p_KDT_GC_ToKhaiTaiXuat_SelectByHopDong]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@HopDongID", SqlDbType.Int, IDHopDong);
            return db.ExecuteDataSet(dbCommand);
        }
    }
  
}
