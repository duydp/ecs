﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using Company.KDT.SHARE.Components;

namespace Company.Interface
{
    public partial class ViewMessagesForm : Form
    {
        public XmlDocument doc;
        private BackgroundWorker bgw = new BackgroundWorker();
        public ViewMessagesForm()
        {
            InitializeComponent();
        }

        private void ViewMessagesForm_Load(object sender, EventArgs e)
        {
            try
            {
                DisplayXML2();
                //btnTimKiem.Enabled = false;
                //bgw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bgw_RunWorkerCompleted);
                //bgw.DoWork += new DoWorkEventHandler(bgw_DoWork);
                ////DisplayXML();

                //timer1.Interval = 50;
                //lblLoading.Text = "Đang load dữ liệu";
                //timer1.Start();
                //bgw.RunWorkerAsync();
            }
            catch (System.Exception ex)
            {
             /*   ShowMessage(ex.Message,false);*/
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            
        }

       
       
        private void DisplayXML()
        {
           /* treeView1.Nodes.Clear();*/
            try
            {
                if (doc != null && !string.IsNullOrEmpty(doc.InnerXml))
                {
                    XmlNode body = doc.SelectNodes("Envelope/Body")[0];
                    body.InnerXml = Helpers.ConvertFromBase64(body.InnerText);
                    addNode(doc);
                    //                 ConvertXmlNodeToTreeNode(doc,treeView1.Nodes);
                    //                 treeView1.Nodes[0].Text = "Thông tin Messages";
                    //                 treeView1.Nodes[0].Expand();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void DisplayXML2()
        {
            if (doc != null && !string.IsNullOrEmpty(doc.InnerXml))
            {
                treeView1.Nodes.Clear();
                XmlNode body = doc.SelectNodes("Envelope/Body/Content")[0];
                body.InnerXml = Helpers.ConvertFromBase64(body.InnerText);
                TreeNode nodeMess = treeView1.Nodes.Add("Messages","Messages");
                nodeMess.Text = "Messages";
                convertTreeview(doc, treeView1.Nodes[0]);
                //ConvertXmlNodeToTreeNode(doc, treeView1.Nodes);
                treeView1.ExpandAll(); 
                //treeView1.Nodes[0].Expand();                
            }
        }
        delegate void AddNodeXml(XmlNode xmlNode);
        private void addNode(XmlNode xmlNode)
        {
            try
            {
                if (treeView1.InvokeRequired)
                {
                    AddNodeXml d = new AddNodeXml(addNode);
                    treeView1.Invoke(d, new object[] { xmlNode });
                }
                else
                {
                    treeView1.Nodes.Clear();
                    ConvertXmlNodeToTreeNode(doc, treeView1.Nodes);
                    treeView1.Nodes[0].Text = "Thông tin Messages";
                    treeView1.Nodes[0].Expand();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Có lỗi trong quá trình xử lý  : \n\r" + ex.Message,false);
            }
            
        }
        private void ConvertXmlNodeToTreeNode(XmlNode xmlNode, TreeNodeCollection treenodecollection)
        {
            try
            {
                TreeNode newTreeNode = treenodecollection.Add(xmlNode.Name);

                switch (xmlNode.NodeType)
                {
                    case XmlNodeType.ProcessingInstruction:
                    case XmlNodeType.XmlDeclaration:
                        newTreeNode.Text = "<?" + xmlNode.Name + " " +
                          xmlNode.Value + "?>";
                        break;
                    case XmlNodeType.Element:
                        newTreeNode.Text = xmlNode.Name;
                        break;
                    case XmlNodeType.Attribute:
                        newTreeNode.Text = "ATTRIBUTE: " + xmlNode.Name;
                        break;
                    case XmlNodeType.Text:
                    case XmlNodeType.CDATA:
                        newTreeNode.Text = xmlNode.Value;
                        break;
                    case XmlNodeType.Comment:
                        newTreeNode.Text = "<!--" + xmlNode.Value + "-->";
                        break;
                }

                if (xmlNode.Attributes != null)
                {
                    foreach (XmlAttribute attribute in xmlNode.Attributes)
                    {
                        ConvertXmlNodeToTreeNode(attribute, newTreeNode.Nodes);
                    }
                }
                foreach (XmlNode childNode in xmlNode.ChildNodes)
                {
                    ConvertXmlNodeToTreeNode(childNode, newTreeNode.Nodes);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Có lỗi trong quá trình xử lý  : \n\r" + ex.Message,false);
            }

        }

        private void btnExportXML_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveForm = new SaveFileDialog();
                saveForm.CheckFileExists = false;
                saveForm.CheckPathExists = true;
                saveForm.DefaultExt = "xml";
                saveForm.Filter = "XML files (*.xml)|*.xml";
                saveForm.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                saveForm.ShowDialog();
                if (string.IsNullOrEmpty(saveForm.FileName)) return;
                doc.Save(saveForm.FileName);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Có lỗi trong quá trình xử lý  : \n\r" + ex.Message,false);
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
        }

        private delegate void SetTex();
//         private void setText()
//         {
//             if (lblLoading.InvokeRequired)
//             {
//                 SetTex d = new SetTex(setText);
//                 lblLoading.Invoke(d);
//             }
//             else
//             {
//                 if (lblLoading.Text != "Đang load dữ liệu ...")
//                     lblLoading.Text = lblLoading.Text + " .";
//             }
//         }

        private void convertTreeview(XmlNode xmlnode, TreeNode treenode)
        {
            try
            {
                treenode.Nodes.Clear();
                if (xmlnode.Attributes != null && xmlnode.Attributes.Count > 0)
                {
                    foreach (XmlNode node in xmlnode.Attributes)
                    {
                        TreeNode newTreeNode = treenode.Nodes.Add(node.Name);
                        newTreeNode.Text = "ATTRIBUTE: " + node.Name;
                    }
                }
                foreach (XmlNode node in xmlnode.ChildNodes)
                {
                    TreeNode newTreeNode = treenode.Nodes.Add(node.Name);

                    switch (node.NodeType)
                    {
                        case XmlNodeType.ProcessingInstruction:
                        case XmlNodeType.XmlDeclaration:
                            newTreeNode.Text = "<?" + node.Name + " " +
                              node.Value + "?>";
                            break;
                        case XmlNodeType.Element:
                            newTreeNode.Text = node.Name;
                            break;
                        case XmlNodeType.Attribute:
                            newTreeNode.Text = "ATTRIBUTE: " + node.Name;
                            break;
                        case XmlNodeType.Text:
                        case XmlNodeType.CDATA:
                            newTreeNode.Text = node.Value;
                            break;
                        case XmlNodeType.Comment:
                            newTreeNode.Text = "<!--" + node.Value + "-->";
                            break;
                    }
                    newTreeNode.Tag = node.Name;
                    if ((node.Attributes != null && node.Attributes.Count > 0) || (node.ChildNodes != null && node.ChildNodes.Count > 0))
                    {
                        newTreeNode.ImageIndex = 0;
                    }
                    else
                        newTreeNode.ImageIndex = 1;


                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Có lỗi trong quá trình xử lý  : \n\r" + ex.Message,false);
            }

        }

        private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
//             e.Node.ImageIndex = 1;
//             XmlNode xmlnode = doc.SelectSingleNode(string.Format(@"{0}",e.Node.FullPath.Remove(0,9)).Replace(@"\",@"/"));
//             convertTreeview(xmlnode, e.Node);
//             e.Node.Expand();
        }

        private void treeView1_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                // treeView1.SelectedNode.ImageIndex = 1;
            string xmlPath = string.Format(@"{0}", treeView1.SelectedNode.FullPath.Remove(0, 9)).Replace(@"\", @"/");
            if (xmlPath.Contains(" "))
                return;
            XmlNode xmlnode = doc.SelectSingleNode(xmlPath);
            convertTreeview(xmlnode, treeView1.SelectedNode);
            treeView1.SelectedNode.Expand();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);            	
            }
           
        }

        private void treeView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.C))
            {
                if (treeView1.SelectedNode != null)
                {
                    Clipboard.SetText(treeView1.SelectedNode.Text);
                }
                e.SuppressKeyPress = true;
            }
        }

    }
}
