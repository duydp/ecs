ALTER PROCEDURE [dbo].[p_GC_BC05TT74]   
 -- Add the parameters for the stored procedure here  
 @HopDong_ID BIGINT,  
 @MaHaiQuan CHAR(6),  
 @MaDoanhNghiep VARCHAR(14)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
/*ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ma) AS STT*/  
  
SELECT   ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS STT,  t_GC_NguyenPhuLieu.Ten AS TenNPL, t_GC_NguyenPhuLieu.Ma AS MaNPL, t_GC_SanPham.Ten AS TenSP, t_GC_SanPham.Ma AS MaSP, 
                      t_HaiQuan_DonViTinh.Ten AS DVT, SUM(t_GC_SanPham.SoLuongDaXuat) AS SoLuongDaXuat, t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) 
                      / 100 AS DinhMucGomTiLeHH, t_GC_SanPham.SoLuongDaXuat * (t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) / 100) AS LuongSuDung, 
                      SUM(t_GC_NguyenPhuLieu.SoLuongDaDung) AS TongSoLuongDaDung, t_GC_DinhMuc.TyLeHaoHut, t_GC_DinhMuc.DinhMucSuDung
FROM         t_GC_NguyenPhuLieu INNER JOIN
                      t_KDT_GC_HopDong ON t_GC_NguyenPhuLieu.HopDong_ID = t_KDT_GC_HopDong.ID INNER JOIN
                      t_GC_DinhMuc ON t_KDT_GC_HopDong.ID = t_GC_DinhMuc.HopDong_ID AND t_GC_NguyenPhuLieu.Ma = t_GC_DinhMuc.MaNguyenPhuLieu INNER JOIN
                      t_GC_SanPham ON t_KDT_GC_HopDong.ID = t_GC_SanPham.HopDong_ID AND t_GC_DinhMuc.MaSanPham = t_GC_SanPham.Ma INNER JOIN
                      t_HaiQuan_DonViTinh ON t_GC_SanPham.DVT_ID = t_HaiQuan_DonViTinh.ID
WHERE     (t_GC_NguyenPhuLieu.HopDong_ID = @HopDong_ID)
GROUP BY t_KDT_GC_HopDong.MaHaiQuan, t_KDT_GC_HopDong.MaDoanhNghiep, t_GC_NguyenPhuLieu.Ten, t_GC_NguyenPhuLieu.Ma, t_GC_SanPham.Ten, 
                      t_GC_SanPham.Ma, t_HaiQuan_DonViTinh.Ten, t_GC_SanPham.SoLuongDaXuat * (t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) / 100), 
                      t_GC_DinhMuc.DinhMucSuDung * (t_GC_DinhMuc.TyLeHaoHut + 100) / 100, t_GC_DinhMuc.TyLeHaoHut, t_GC_DinhMuc.DinhMucSuDung
HAVING      (SUM(t_GC_SanPham.SoLuongDaXuat) > 0) AND (t_KDT_GC_HopDong.MaHaiQuan = @MaHaiQuan) AND (t_KDT_GC_HopDong.MaDoanhNghiep = @MaDoanhNghiep)
ORDER BY MaNPL
END  

GO


UPDATE dbo.t_HaiQuan_Version SET [Version] = '9.4', [Date] = GETDATE(), Notes = N'Cap nhat mau bao cao 05/117'
