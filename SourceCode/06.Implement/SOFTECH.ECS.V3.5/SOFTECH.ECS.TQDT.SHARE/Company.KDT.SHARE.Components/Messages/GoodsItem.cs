﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages;
using Company.KDT.SHARE.Components.Messages.GoodsItem;

namespace Company.KDT.SHARE.Components
{
    public class GoodsItem : IssueBase
    {
        /// <summary>
        /// Số thứ tự
        /// </summary>
        [XmlElement("sequence")]
        public string Sequence { get; set; }

        /// <summary>
        /// Loại hàng hóa
        /// 1: Nguyên liệu, vật liệu nhập khẩu 
        /// 2. Thành phẩm được sản xuất từ nguồn nhập
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }

        /// <summary>
        /// Tài khoản
        /// Loại hàng hóa =1
        /// Trường hợp báo cáo theo trị giá -> lấy giá trị tại tài khoản 152
        /// Loại hàng hóa =2
        /// Trường hợp báo cáo theo trị giá -> lấy giá trị tại tài khoản 155
        /// </summary>
        [XmlElement("account")]
        public string Account { get; set; }

        /// <summary>
        /// Tên, quy cách nguyên vật liệu, hàng hóa
        /// </summary>
        [XmlElement("description")]
        public string Description { get; set; }

        /// <summary>
        /// Tên, quy cách nguyên vật liệu, hàng hóa
        /// </summary>
        [XmlElement("descriptionMaterial")]
        public string DescriptionMaterial { get; set; }

        /// <summary>
        /// Mã hàng do doanh nghiệp khai
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }

        /// <summary>
        /// Mã hàng do doanh nghiệp khai
        /// </summary>
        [XmlElement("identificationMaterial")]
        public string IdentificationMaterial { get; set; }

        /// <summary>
        /// Mã đơn vị tính
        /// </summary>
        [XmlElement("measureUnit")]
        public string MeasureUnit { get; set; }

        /// <summary>
        /// Mã đơn vị tính
        /// </summary>
        [XmlElement("measureUnitMaterial")]
        public string MeasureUnitMaterial { get; set; }

        /// <summary>
        /// Số lượng tồn kho sổ sách
        /// </summary>
        [XmlElement("quantity1")]
        public decimal Quantity1 { get; set; }

        /// <summary>
        /// Số lượng tồn kho thực tế
        /// </summary>
        [XmlElement("quantity2")]
        public decimal Quantity2 { get; set; }


        /// <summary>
        /// Tồn đầu kỳ
        /// </summary>
        [XmlElement("quantityBegin")]
        public decimal QuantityBegin { get; set; }

        /// <summary>
        /// Tồn đầu kỳ
        /// </summary>
        [XmlElement("quantityBeginMaterial")]
        public decimal QuantityBeginMaterial { get; set; }

        /// <summary>
        /// Nhập trong kỳ
        /// </summary>
        [XmlElement("quantityImport")]
        public decimal QuantityImport { get; set; }

        /// <summary>
        /// Nhập trong kỳ
        /// </summary>
        [XmlElement("quantityImportMaterial")]
        public decimal QuantityImportMaterial { get; set; }

        /// <summary>
        /// Xuất trong kỳ
        /// </summary>
        [XmlElement("quantityExport")]
        public decimal QuantityExport { get; set; }


        /// <summary>
        /// Lượng tái xuất
        /// </summary>
        [XmlElement("quantityReExportMaterial")]
        public decimal QuantityReExportMaterial { get; set; }

        /// <summary>
        /// Lượng chuyển mục đích sử dụng
        /// </summary>
        [XmlElement("quantityRePurposeMaterial")]
        public decimal QuantityRePurposeMaterial { get; set; }


        /// <summary>
        /// Lượng  xuất khẩu
        /// </summary>
        [XmlElement("quantityExportProduct")]
        public decimal QuantityExportProduct { get; set; }


        /// <summary>
        /// Lượng xuất khác
        /// </summary>
        [XmlElement("quantityExportOther")]
        public decimal QuantityExportOther { get; set; }

        /// <summary>
        /// Lượng tồn cuối kỳ
        /// </summary>
        [XmlElement("quantityExcessMaterial")]
        public decimal QuantityExcessMaterial { get; set; }


        /// <summary>
        /// Tồn cuối kỳ
        /// </summary>
        [XmlElement("quantityExcess")]
        public decimal QuantityExcess { get; set; }

        /// <summary>
        /// Ghi chú
        /// </summary>
        [XmlElement("content")]
        public string Content { get; set; }

        /// <summary>
        /// Lượng nguyên liệu vật tư xuất khẩu
        /// </summary>
        [XmlElement("quantityMaterialExport")]
        public decimal QuantityMaterialExport { get; set; }


        /// <summary>
        /// Lượng nguyên liệu, vật tư còn tồn tại bên nhận gia công
        /// </summary>
        [XmlElement("quantityMaterialOutsourcing")]
        public decimal QuantityMaterialOutsourcing { get; set; }


        /// <summary>
        /// Lượng sản phẩm gia công hoàn chỉnh nhập khẩu
        /// </summary>
        [XmlElement("quantityProductImport")]
        public decimal QuantityProductImport { get; set; }

        /// <summary>
        /// Lượng sản phẩm gia công hoàn chỉnh bán tại nước ngoài
        /// </summary>
        [XmlElement("quantityProductExport")]
        public decimal QuantityProductExport { get; set; }

        /// <summary>
        /// Trị giá tính thuế n..16
        /// </summary>
        [XmlElement("statisticalValue")]
        public string StatisticalValue { get; set; }
        /// <summary>
        /// Nguyên tệ a3
        /// </summary>
        [XmlElement("CurrencyExchange")]
        public CurrencyExchange CurrencyExchange { get; set; }
        /// <summary>
        /// Thông tin kiện
        /// </summary>
        [XmlElement("ConsignmentItemPackaging")]
        public Packaging ConsignmentItemPackaging { get; set; }

        /// <summary>
        /// Chi tiết hàng
        /// </summary>
        [XmlElement("Commodity")]
        public Commodity Commodity { get; set; }

        /// <summary>
        /// Thông tin lượng
        /// </summary>
        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }

        /// <summary>
        /// Thông tin lượng
        /// </summary>
        [XmlElement("QuantityForward")]
        public QuantityForward QuantityForward { get; set; }

        /// <summary>
        /// Lượng NL, VT đã xuất khẩu nhưng phải thanh lý ở nước ngoài 
        /// </summary>
        [XmlElement("QuantityExportLiquidate")]
        public QuantityExportLiquidate QuantityExportLiquidate { get; set; }

        /// <summary>
        /// Ghi chú khác
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        /// <summary>
        /// Xuất xứ
        /// </summary>
        [XmlElement("Origin")]
        public Origin Origin { get; set; }
        /// <summary>
        /// Hóa đơn thương mại
        /// </summary>
        [XmlElement("Invoice")]
        public Invoice Invoice { get; set; }


        #region Giấy đăng ký kiểm tra
        /// <summary>
        /// chung tu kem theo
        /// </summary>
        [XmlElement("AdditionalDocument")]
        public AdditionalDocument AdditionalDocument { get; set; }

        /// <summary>
        /// dia diem kiem tra
        /// </summary>
        [XmlElement("Examination")]
        public Examination Examination { get; set; }


        /// <summary>
        /// Co quan kiem tra
        /// </summary>
        [XmlElement("Examiner")]
        public NameBase Examiner { get; set; }
        #endregion

        #region Chứng thư giám định
        /// <summary>
        /// Van don cua hang giam dinh
        /// </summary>
        [XmlElement("BillOfLading")]
        public BillOfLading BillOfLading { get; set; }

        /// <summary>
        /// container hang giam dinh
        /// </summary>
        [XmlElement("EquipmentIdentification")]
        public EquipmentIdentification EquipmentIdentification { get; set; }

        #endregion

    }
}
