﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.DanhMucChuan
{
    public partial class LoaiPhiChungTuThanhToanForm : Company.Interface.BaseForm
    {
        DataSet ds = new DataSet();
        public LoaiPhiChungTuThanhToanForm()
        {
            InitializeComponent();
        }

        private void PTVTForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            ds = LoaiPhiChungTuThanhToan.SelectAll();
            dgList.DataSource = ds;
            dgList.DataMember = ds.Tables[0].TableName;
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                #region GetChanges

                //Updated by Hungtq, 10/08/2012.
                if (ds.Tables[0].GetChanges() != null)
                {
                    DataRow[] rows;

                    foreach (DataRow dr in ds.Tables[0].GetChanges().Rows)
                    {
                        if (dr.RowState == DataRowState.Added)
                        {
                            rows = ds.Tables[0].Select(" ID= '" + dr["ID"].ToString().Trim() + "'");

                            for (int i = 0; i < rows.Length; i++)
                            {
                                rows[i]["DateCreated"] = System.DateTime.Now;
                                rows[i]["DateModified"] = System.DateTime.Now;
                            }
                        }
                        else if (dr.RowState == DataRowState.Modified)
                        {
                            rows = ds.Tables[0].Select(" ID= '" + dr["ID"].ToString().Trim() + "'");

                            for (int i = 0; i < rows.Length; i++)
                            {
                                rows[i]["DateModified"] = System.DateTime.Now;
                            }
                        }
                    }
                }

                #endregion

                LoaiPhiChungTuThanhToan.Update(ds);
               // ShowMessage("Cập nhật thành công.", false);
                MLMessages("Lưu thành công", "MSG_SAV02", "", false); 
            }
            catch (Exception ex)
            {
               // ShowMessage("Lỗi: " + ex.Message, false);
                MLMessages("Lưu không thành công", "MSG_SAV01", "", false);
            }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
           // if (ShowMessage("Bạn có muốn xóa các loại phí chứng từ thanh toán này không?", true) != "Yes") e.Cancel = true;
            if (MLMessages("Bạn có muốn xóa các loại phí chứng từ thanh toán này không?", "MSG_DEL01", "", true) != "Yes") e.Cancel = true;
        }
        private bool CheckID(string Id)
        {
            if (ds.Tables[0].Select(" ID= '" + Id.Trim() + "'").Length == 0) return false;
            else return true;
        }
        private void dgList_UpdatingCell(object sender, Janus.Windows.GridEX.UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "ID")
            {
                string s = e.Value.ToString();
                if (CheckID(s))
                {
                    //ShowMessage("Mã loại phí chứng từ thanh toán này đã có.", false);
                    MLMessages("Mã loại phí chứng từ thanh toán này đã có.", "MSG_PUB10", "", false); 
                    e.Cancel = true;
                }
                if (s.Trim().Length == 0)
                {
                    //ShowMessage("Mã loại phí chứng từ thanh toán không được rỗng.", false);
                    MLMessages("Mã loại phí chứng từ thanh toán không được rỗng.", "MSG_CAL01", "", false);
                    e.Cancel = true;
                }
            }
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}

