﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;


namespace Company.Interface
{
    public partial class GiayPhepForm : Company.Interface.BaseForm
    {
        public ToKhaiMauDich TKMD;
        public GiayPhep giayPhep = new GiayPhep();
        public bool isKhaiBoSung = false;
        public GiayPhepForm()
        {
            InitializeComponent();
        }

        private void BindData()
        {
            Company.KD.BLL.KDT.HangMauDich hmd = new Company.KD.BLL.KDT.HangMauDich();
            hmd.TKMD_ID = TKMD.ID;

            dgList.DataSource = giayPhep.ConvertListToDataSet(hmd.SelectBy_TKMD_ID().Tables[0]);
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }
        private void GiayPhepForm_Load(object sender, EventArgs e)
        {
            if(this._NguyenTe==null)
                _NguyenTe = Company.KD.BLL.DuLieuChuan.NguyenTe.SelectAll().Tables[0];
            foreach (DataRow rowNuoc in this._NguyenTe.Rows)
                dgList.RootTable.Columns["MaNguyenTe"].ValueList.Add(rowNuoc["ID"].ToString(), rowNuoc["Ten"].ToString());

            if (giayPhep.ID <= 0)
            { 
                //DATLMQ bổ sung GP lấy từ TKMD 13/12/2010
                giayPhep.SoGiayPhep = TKMD.SoGiayPhep;
                txtSoGiayPhep.Text = giayPhep.SoGiayPhep;
                giayPhep.NgayGiayPhep = TKMD.NgayGiayPhep;
                ccNgayGiayPhep.Text = giayPhep.NgayGiayPhep.ToShortDateString();
                giayPhep.NgayHetHan = TKMD.NgayHetHanGiayPhep;
                ccNgayHHGiayPhep.Text = giayPhep.NgayHetHan.ToShortDateString();
                //DATLMQ bổ sung Mã đơn vị được cấp 17/01/2011
                txtMaDVdcCap.Text = TKMD.MaDoanhNghiep;
                txtTenDVdcCap.Text = TKMD.TenDoanhNghiep;
            }
            else if (giayPhep.ID > 0)
            {
                txtMaCQCap.Text = giayPhep.MaCoQuanCap;
                txtMaDVdcCap.Text = giayPhep.MaDonViDuocCap;
                ccNgayGiayPhep.Text = giayPhep.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = giayPhep.NgayHetHan.ToShortDateString();
                txtNguoiCap.Text = giayPhep.NguoiCap;
                txtNoiCap.Text = giayPhep.NoiCap;
                txtSoGiayPhep.Text = giayPhep.SoGiayPhep;
                txtTenDVdcCap.Text = giayPhep.TenDonViDuocCap;
                txtTenCQCap.Text = giayPhep.TenQuanCap;
                txtThongTinKhac.Text = giayPhep.ThongTinKhac;
                BindData();
            }
            if (TKMD.ID <= 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = false;
            }
            else if (giayPhep.SoTiepNhan > 0)
            {
                btnKhaiBao.Enabled = false;
                btnLayPhanHoi.Enabled = true;
                ccNgayTiepNhan.Text = giayPhep.NgayTiepNhan.ToShortDateString();
                txtSoTiepNhan.Text = giayPhep.SoTiepNhan + "";
            }
            else if (TKMD.SoToKhai > 0 && int.Parse(TKMD.PhanLuong != "" ? TKMD.PhanLuong : "0") == 0)
            {
                btnKhaiBao.Enabled = true;
                btnLayPhanHoi.Enabled = true;
            }
            else if (TKMD.PhanLuong != "")
            {
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            SetButtonStateGIAYPHEP(TKMD, isKhaiBoSung, giayPhep);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

     
        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<HangGiayPhepDetail> HangGiayPhepDetailCollection = new List<HangGiayPhepDetail>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HangGiayPhepDetail hgpDetail = new HangGiayPhepDetail();
                        DataRowView row = (DataRowView)i.GetRow().DataRow;
                        hgpDetail.HMD_ID = Convert.ToInt64(row["HMD_ID"]);
                        hgpDetail.ID = Convert.ToInt64(row["ID"]);
                        HangGiayPhepDetailCollection.Add(hgpDetail);
                    }
                }
                foreach (HangGiayPhepDetail hgpDetailTMP in HangGiayPhepDetailCollection)
                {
                    try
                    {
                        if (hgpDetailTMP.ID > 0)
                        {
                            hgpDetailTMP.Delete();
                        }
                        foreach (HangGiayPhepDetail hgpDetail in giayPhep.ListHMDofGiayPhep)
                        {
                            if (hgpDetail.HMD_ID == hgpDetailTMP.HMD_ID)
                            {
                                giayPhep.ListHMDofGiayPhep.Remove(hgpDetail);
                                break;
                            }
                        }
                    }
                    catch { }
                }
            }
            BindData();
        }

        private bool checkSoGiayPhep(string soGiayPhep)
        {
            foreach (GiayPhep gpTMP in TKMD.GiayPhepCollection)
            {
                if (gpTMP.SoGiayPhep.Trim().ToUpper() == soGiayPhep.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void btnGhi_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            bool isValidate = true;
            isValidate = Globals.ValidateLength(txtMaCQCap, 7, epError, "Mã cơ quan cấp");
            if (!isValidate) return;
            TKMD.GiayPhepCollection.Remove(giayPhep);

            if (checkSoGiayPhep(txtSoGiayPhep.Text))
            {
                if (giayPhep.ID > 0)
                    TKMD.GiayPhepCollection.Add(giayPhep);
                ShowMessage("Số giấy phép này đã tồn tại.", false);
                return;
            }

            giayPhep.MaCoQuanCap = txtMaCQCap.Text.Trim();
            giayPhep.MaDonViDuocCap = txtMaDVdcCap.Text.Trim();
            giayPhep.NgayGiayPhep = ccNgayGiayPhep.Value;
            giayPhep.NgayHetHan = ccNgayHHGiayPhep.Value;
            giayPhep.NguoiCap = txtNguoiCap.Text.Trim();
            giayPhep.NoiCap = txtNoiCap.Text.Trim();
            giayPhep.SoGiayPhep = txtSoGiayPhep.Text.Trim();
            giayPhep.TenDonViDuocCap = txtTenDVdcCap.Text.Trim();
            giayPhep.TenQuanCap = txtTenCQCap.Text.Trim();
            giayPhep.ThongTinKhac = txtThongTinKhac.Text;
            giayPhep.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            giayPhep.TKMD_ID = TKMD.ID;
            if (string.IsNullOrEmpty(giayPhep.GuidStr)) giayPhep.GuidStr = Guid.NewGuid().ToString();
            if (isKhaiBoSung)
                giayPhep.LoaiKB = 1;
            else
                giayPhep.LoaiKB = 0;
            GridEXRow[] rowCollection = dgList.GetRows();
            foreach (GridEXRow row in rowCollection)
            {
                DataRowView rowview = (DataRowView)row.DataRow;
                foreach (HangGiayPhepDetail item in giayPhep.ListHMDofGiayPhep)
                {
                    if (rowview["HMD_ID"].ToString().Trim() == item.HMD_ID.ToString().Trim())
                    {
                        item.GhiChu = row.Cells["GhiChu"].Text;
                        item.MaChuyenNganh = row.Cells["MaChuyenNganh"].Value.ToString();
                        if (row.Cells["MaNguyenTe"].Text != null)
                            try
                            {
                                item.MaNguyenTe = row.Cells["MaNguyenTe"].Value.ToString();
                            }
                            catch { item.MaNguyenTe = "VND"; }
                        else
                        {
                            ShowMessage("Chưa chọn nguyên tệ cho hàng : "+(row.RowIndex + 1), false);
                            return;
                        }
                        break;
                    }
                }
            }

            try
            {
                giayPhep.InsertUpdateFull();
                TKMD.GiayPhepCollection.Add(giayPhep);
                BindData();
                ShowMessage("Lưu thành công.", false);
            }
            catch(Exception ex)
            {
                ShowMessage("Lỗi : "+ex.Message, false);
                return;
            }
            
        }
        public HangMauDichCollection ConvertListToHangMauDichCollection(List<HangGiayPhepDetail> listHangGiayPhep, HangMauDichCollection hangCollection)
        {

            HangMauDichCollection tmp = new HangMauDichCollection();

            foreach (HangGiayPhepDetail item in listHangGiayPhep)
            {
                foreach (Company.KD.BLL.KDT.HangMauDich HMDTMP in hangCollection)
                {
                    if (HMDTMP.ID == item.HMD_ID)
                    {
                        tmp.Add(HMDTMP);
                        break;
                    }
                }
            }
            return tmp;

        }
        private void btnAddNew_Click(object sender, EventArgs e)
        {
            SelectHangMauDichForm f = new SelectHangMauDichForm();
            f.TKMD = TKMD;
            //Linhhtn - Không dùng đoạn này vì sẽ gây mất hàng khi kích nút Chọn hàng
            //if (giayPhep.ListHMDofGiayPhep.Count > 0)
            //{
            //    f.TKMD.HMDCollection = ConvertListToHangMauDichCollection(giayPhep.ListHMDofGiayPhep,TKMD.HMDCollection);
            //}
            f.ShowDialog();
            if (f.HMDTMPCollection.Count > 0)
            {
                foreach (Company.KD.BLL.KDT.HangMauDich HMD in f.HMDTMPCollection)
                {
                    bool ok = false;
                    foreach (HangGiayPhepDetail hangGiayPhepDetail in giayPhep.ListHMDofGiayPhep)
                    {
                        if (hangGiayPhepDetail.HMD_ID == HMD.ID)
                        {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                    {
                        HangGiayPhepDetail hgpDetail = new HangGiayPhepDetail();
                        hgpDetail.HMD_ID = HMD.ID;
                        hgpDetail.GiayPhep_ID = giayPhep.ID;
                        hgpDetail.MaPhu = HMD.MaPhu;
                        hgpDetail.MaHS = HMD.MaHS;
                        hgpDetail.TenHang = HMD.TenHang;
                        hgpDetail.DVT_ID = HMD.DVT_ID;
                        hgpDetail.SoThuTuHang = HMD.SoThuTuHang;
                        hgpDetail.SoLuong = HMD.SoLuong;
                        hgpDetail.NuocXX_ID = HMD.NuocXX_ID;
                        hgpDetail.MaNguyenTe = TKMD.NguyenTe_ID;
                        hgpDetail.DonGiaKB = Convert.ToDouble(HMD.DonGiaKB);
                        hgpDetail.TriGiaKB = Convert.ToDouble(HMD.TriGiaKB);
                        giayPhep.ListHMDofGiayPhep.Add(hgpDetail);
                    }

                }
            }
            try
            {
                BindData();
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi : " + ex.Message, false);
            }
        }

        private void btnChonGP_Click(object sender, EventArgs e)
        {
            ManageGiayPhepForm f = new ManageGiayPhepForm();
            f.isBrower = true;
            f.TKMD_ID = TKMD.ID;
            f.ShowDialog();
            if (f.giayPhep != null)
            {
                giayPhep = f.giayPhep;
                txtMaCQCap.Text = giayPhep.MaCoQuanCap;
                txtMaDVdcCap.Text = giayPhep.MaDonViDuocCap;
                ccNgayGiayPhep.Text = giayPhep.NgayGiayPhep.ToShortDateString();
                ccNgayHHGiayPhep.Text = giayPhep.NgayHetHan.ToShortDateString();
                txtNguoiCap.Text = giayPhep.NguoiCap;
                txtNoiCap.Text = giayPhep.NoiCap;
                txtSoGiayPhep.Text = giayPhep.SoGiayPhep;
                txtTenDVdcCap.Text = giayPhep.TenDonViDuocCap;
                txtTenCQCap.Text = giayPhep.TenQuanCap;
                txtThongTinKhac.Text = giayPhep.ThongTinKhac;
                giayPhep.ID = 0;
                giayPhep.GuidStr = "";
                giayPhep.SoTiepNhan = 0;
                giayPhep.NamTiepNhan = 0;
                giayPhep.NgayTiepNhan = new DateTime(1900, 1, 1);
                BindData();
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value);
        }

        private void btnKhaiBao_Click(object sender, EventArgs e)
        {
            if (giayPhep.ID == 0)
            {
                ShowMessage("Lưu thông tin trước khi khai báo", false);
                return;
            }
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                bool thanhcong = giayPhep.WSKhaiBaoGP(password, TKMD.MaHaiQuan, (long)TKMD.SoToKhai, TKMD.MaLoaiHinh, TKMD.NamDK, TKMD.ID, TKMD.MaDoanhNghiep, null, MessageTypes.ToKhaiNhap, MessageFunctions.KhaiBao, TKMD.ConvertHMDKDToHangMauDich());
                if (thanhcong)
                {
                    this.ShowMessage("Hệ thống Hải quan đã nhận được thông tin nhưng chưa có thông tin phản hồi. Bấm nút [Lấy phản hồi] để nhận thông tin phản hồi.", false);

                    btnKhaiBao.Enabled = false;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void btnLayPhanHoi_Click(object sender, EventArgs e)
        {
            string password = "";
            WSForm wsForm = new WSForm();
            try
            {
                if (GlobalSettings.PassWordDT == "")
                {
                    wsForm.ShowDialog(this);
                    if (!wsForm.IsReady) return;
                }
                password = GlobalSettings.PassWordDT != "" ? GlobalSettings.PassWordDT : wsForm.txtMatKhau.Text.Trim();
                if (this.giayPhep.SoTiepNhan == 0)
                {
                    this.LayPhanHoiKhaiBao(password);
                }
                else if (this.giayPhep.SoTiepNhan > 0)
                {
                    this.LayPhanHoiDuyet(password);
                }
            }
            catch (Exception ex)
            {
                ShowMessageTQDT("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiKhaiBao(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = giayPhep.WSLaySoTiepNhan(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnKhaiBao.Enabled = true;
                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.giayPhep.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungGiayPhepThanhCong);
                    this.ShowMessage(message, false);

                    btnKhaiBao.Enabled = false;
                    txtSoTiepNhan.Text = this.giayPhep.SoTiepNhan.ToString("N0");
                    ccNgayTiepNhan.Value = this.giayPhep.NgayTiepNhan;
                    ccNgayTiepNhan.Text = this.giayPhep.NgayTiepNhan.ToShortDateString();
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void LayPhanHoiDuyet(string password)
        {
        StartInvoke:
            try
            {
                bool thanhcong = giayPhep.WSLayPhanHoi(password, EntityBase.GetPathProgram(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);

                // Thực hiện kiểm tra.  
                if (thanhcong == false)
                {
                    if (this.ShowMessage("Chưa có phản hồi từ hệ thống Hải quan.\r\nBạn có muốn tiếp tục lấy thông tin không?", true) == "Yes")
                    {
                        this.Refresh();
                        goto StartInvoke;
                    }

                    btnLayPhanHoi.Enabled = true;
                }
                else
                {
                    string message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.giayPhep.ID, Company.KDT.SHARE.Components.MessageTitle.KhaiBaoBoSungGiayPhepDuocChapNhan);
                    if (message.Length == 0)
                    {
                        message = Company.KDT.SHARE.Components.Message.LayThongDiep(this.giayPhep.ID, Company.KDT.SHARE.Components.MessageTitle.TuChoiTiepNhan);
                        txtSoTiepNhan.Text = "";
                        ccNgayTiepNhan.Value = new DateTime(1900, 1, 1);
                        ccNgayTiepNhan.Text = "";
                    }
                    else
                    {
                        btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
                    }
                    this.ShowMessage(message, false);

                }
            }
            catch (Exception ex)
            {
                this.ShowMessage("Xảy ra lỗi :" + ex.Message.ToString(), false);
            }
        }

        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            if (giayPhep.GuidStr != null && giayPhep.GuidStr != "")
                Globals.ShowKetQuaXuLyBoSung(giayPhep.GuidStr);
            else
                Globals.ShowMessageTQDT("Không có thông tin", false);
        }

        #region Begin Set Button State

        /// <summary>
        /// Thiết lập trạng thái các nút trên form GIAY PHEP.
        /// </summary>
        /// <param name="tkmd"></param>
        /// HUNGTQ, Update 07/06/2010.
        private bool SetButtonStateGIAYPHEP(Company.KD.BLL.KDT.ToKhaiMauDich tkmd, bool isKhaiBoSung, Company.KDT.SHARE.QuanLyChungTu.GiayPhep giayphep)
        {
            if (giayphep == null)
                return false;

            bool status = false;

            //Khai bao moi
            if (isKhaiBoSung == false)
            {
                //Nếu chưa có số tiếp nhận - chưa đăng ký - cho phép cập nhật/ sửa/ xóa thông tin.
                status = (tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.SUATKDADUYET
                    || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET
                     || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.DA_HUY
                      || tkmd.TrangThaiXuLy == Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO);

                btnXoa.Enabled = status;
                btnChonGP.Enabled = status;
                btnAddNew.Enabled = status;
                btnGhi.Enabled = status;
                btnKetQuaXuLy.Enabled = false;
                btnKhaiBao.Enabled = btnLayPhanHoi.Enabled = false;
            }
            //Khai bao bo sung
            else
            {
                //Chi cho phep cap nhat/ them moi/ xoa chung tu bo sung khi da duoc dang ky - co so to khai.
                if (tkmd.SoToKhai == 0)
                {
                    //string msg = "Tờ khai chưa được cấp số. Bạn không thể bổ sung chứng từ.";
                    //Globals.ShowMessageTQDT(msg, false);

                    //return false;
                }
                else
                {
                    status = true;

                    btnXoa.Enabled = status;
                    btnChonGP.Enabled = status;
                    btnAddNew.Enabled = status;
                    btnGhi.Enabled = status;
                    btnKetQuaXuLy.Enabled = true;
                    //Neu chua co so tiep nhan -> phai khai bao
                    if (giayphep.SoTiepNhan == 0)
                    {
                        btnKhaiBao.Enabled = true;
                        btnLayPhanHoi.Enabled = false;
                    }
                    //Neu da co so tiep nhan -> co the lay phan hoi
                    else
                    {
                        btnKhaiBao.Enabled = false;
                        btnLayPhanHoi.Enabled = true;
                    }


                }
            }

            return true;
        }

        #endregion

        private void dgList_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 46)
            {
                btnXoa_Click(sender, null);
            }
        }
       
       

    }
}

