﻿namespace Project_SignatureXML
{
    partial class frMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frMain));
            this.btSign = new System.Windows.Forms.Button();
            this.btVerify = new System.Windows.Forms.Button();
            this.axCMSActivex1 = new AxCMSActivexLib.AxCMSActivex();
            ((System.ComponentModel.ISupportInitialize)(this.axCMSActivex1)).BeginInit();
            this.SuspendLayout();
            // 
            // btSign
            // 
            this.btSign.Location = new System.Drawing.Point(165, 28);
            this.btSign.Margin = new System.Windows.Forms.Padding(2);
            this.btSign.Name = "btSign";
            this.btSign.Size = new System.Drawing.Size(87, 41);
            this.btSign.TabIndex = 0;
            this.btSign.Text = "Ký";
            this.btSign.UseVisualStyleBackColor = true;
            this.btSign.Click += new System.EventHandler(this.btSign_Click);
            // 
            // btVerify
            // 
            this.btVerify.Location = new System.Drawing.Point(165, 91);
            this.btVerify.Margin = new System.Windows.Forms.Padding(2);
            this.btVerify.Name = "btVerify";
            this.btVerify.Size = new System.Drawing.Size(87, 42);
            this.btVerify.TabIndex = 1;
            this.btVerify.Text = "Verify";
            this.btVerify.UseVisualStyleBackColor = true;
            this.btVerify.Click += new System.EventHandler(this.btVerify_Click);
            // 
            // axCMSActivex1
            // 
            this.axCMSActivex1.Enabled = true;
            this.axCMSActivex1.Location = new System.Drawing.Point(334, 45);
            this.axCMSActivex1.Name = "axCMSActivex1";
            this.axCMSActivex1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axCMSActivex1.OcxState")));
            this.axCMSActivex1.Size = new System.Drawing.Size(100, 50);
            this.axCMSActivex1.TabIndex = 2;
            // 
            // frMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(448, 172);
            this.Controls.Add(this.axCMSActivex1);
            this.Controls.Add(this.btVerify);
            this.Controls.Add(this.btSign);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chương trình ký/verify dữ liệu";
            this.Load += new System.EventHandler(this.frMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.axCMSActivex1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btSign;
        private System.Windows.Forms.Button btVerify;
        private AxCMSActivexLib.AxCMSActivex axCMSActivex1;
    }
}

