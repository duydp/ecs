﻿namespace Company.Interface
{
    partial class ListContainerTKMDForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgLePhi_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListContainerTKMDForm));
            Janus.Windows.Common.JanusColorScheme janusColorScheme1 = new Janus.Windows.Common.JanusColorScheme();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgLePhi = new Janus.Windows.GridEX.GridEX();
            this.visualStyleManager1 = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgLePhi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.uiGroupBox1.Controls.Add(this.dgLePhi);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(580, 289);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyleManager = this.visualStyleManager1;
            // 
            // dgLePhi
            // 
            this.dgLePhi.ColumnAutoResize = true;
            dgLePhi_DesignTimeLayout.LayoutString = resources.GetString("dgLePhi_DesignTimeLayout.LayoutString");
            this.dgLePhi.DesignTimeLayout = dgLePhi_DesignTimeLayout;
            this.dgLePhi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgLePhi.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgLePhi.GroupByBoxVisible = false;
            this.dgLePhi.Location = new System.Drawing.Point(3, 8);
            this.dgLePhi.Name = "dgLePhi";
            this.dgLePhi.Size = new System.Drawing.Size(574, 246);
            this.dgLePhi.TabIndex = 0;
            this.dgLePhi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgLePhi.VisualStyleManager = this.visualStyleManager1;
            this.dgLePhi.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgLePhi_LoadingRow);
            this.dgLePhi.FormattingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgLePhi_FormattingRow);
            // 
            // visualStyleManager1
            // 
            janusColorScheme1.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme1.Name = "office2007";
            janusColorScheme1.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme1.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            this.visualStyleManager1.ColorSchemes.Add(janusColorScheme1);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 254);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(574, 32);
            this.uiGroupBox2.TabIndex = 3;
            this.uiGroupBox2.VisualStyleManager = this.visualStyleManager1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(398, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "(*) Để bổ sung thông tin container vui lòng vào vận đơn chọn mục Thêm Container";
            // 
            // ListContainerTKMDForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(580, 289);
            this.Controls.Add(this.uiGroupBox1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ListContainerTKMDForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin container trong vận đơn";
            this.Load += new System.EventHandler(this.LePhiHQ_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.LePhiHQ_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgLePhi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.GridEX dgLePhi;
        private Janus.Windows.Common.VisualStyleManager visualStyleManager1;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
    }
}