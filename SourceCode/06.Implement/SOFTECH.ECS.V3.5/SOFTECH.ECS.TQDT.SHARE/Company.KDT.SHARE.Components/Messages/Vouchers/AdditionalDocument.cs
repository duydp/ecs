﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.Vouchers
{
   public class AdditionalDocument
    {
        /// <summary>
        /// Số chứng từ
        /// </summary>
        [XmlElement("reference")]
        public string Reference { get; set; }

        /// <summary>
        /// Tên chứng từ
        /// </summary>
        [XmlElement("description")]
        public string Description { get; set; }

        /// <summary>
        /// Ngày chứng từ
        /// </summary>
        [XmlElement("issue")]
        public string Issue { get; set; }

        /// <summary>
        /// Nơi phát hành 
        /// </summary>
        [XmlElement("issueLocation")]
        public string IssueLocation { get; set; }

        /// <summary>
        /// Ghi chú khác
        /// </summary>
        [XmlElement("content")]
        public string Content { get; set; }

        /// <summary>
        /// AttachedFile
        /// </summary>
        [XmlElement("AttachedFile")]
        public AttachedFile AttachedFile { get; set; }
    }
}
