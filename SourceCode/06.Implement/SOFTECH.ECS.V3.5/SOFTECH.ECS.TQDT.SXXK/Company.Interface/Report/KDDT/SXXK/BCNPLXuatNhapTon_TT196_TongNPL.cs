using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Linq;

namespace Company.Interface.Report.SXXK
{
    public partial class BCNPLXuatNhapTon_TT196_TongNPL : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 0;
        private int STTBanLuu = 0;
        public int SoHoSo;
        public bool isHoanThue = false;
        private DataTable DtGhiChu = new DataTable();
        private DataTable dt = new DataTable();
        private int SoToKhaiNhap = 0;
        private int SoToKhaiXuat = 0;
        private decimal temp = 0;

        private string MaNPLTemp = "";
        private string MaNPLBanLuu = "";
        private int cnt = 1; //Gia tri phai thiet lap mac dinh ban dau = 1.
        private DataTable dtMaNPLTemp;

        private string MaSPTemp = "";
        private string MaSPBanLuu = "";

        private decimal _TongLuongNhap = 0;
        private decimal _TongLuongTonDau = 0;
        private decimal _TongLuongNPLSuDung = 0;
        private decimal _TongLuongTonCuoi = 0;
        private decimal _TongLuongNPLTaiXuat = 0;

        public BCNPLXuatNhapTon_TT196_TongNPL()
        {
            InitializeComponent();
        }

        public void BindReport(string where)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            string order = "";
            int sapxeptkx = Properties.Settings.Default.TKX;
            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                if (sapxeptkx == 2)
                    order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    //order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                    //TODO: Update by Hungtq 08/06/2012. Bỏ sắp xếp theo "ngayhoanthanhxuat" 
                    //dùng hiển thị đúng thứ tự tờ khai âm kế tiếp. Ngày của tờ khai âm kế tiếp có thế < tờ khai trước đó.
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
                else
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
            }
            else
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";

            }

            dtMaNPLTemp = BLL.SXXK.ThanhKhoan.BCXuatNhapTon.GetSoLanNPLThamGiaThanhKhoan(GlobalSettings.MA_DON_VI, this.LanThanhLy);

            dt = new Company.BLL.KDT.SXXK.BCXuatNhapTon().SelectDynamic("(MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND LanThanhLy = " + this.LanThanhLy + ")" + where, order).Tables[0];
            int i = 1;
            string maNPL = "";
            DateTime ngayDangKyNhap = DateTime.Today;
            long soToKhaiNhap = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (maNPL == dr["MaNPL"].ToString() && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == Convert.ToInt64(dr["SoToKhaiNhap"]))
                {
                    dr["STT"] = i;
                }
                else
                {
                    maNPL = dr["MaNPL"].ToString();
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = Convert.ToInt64(dr["SoToKhaiNhap"]);
                    i++;
                    dr["STT"] = i;
                }
            }
            dt.TableName = "BCXuatNhapTon";
            this.DataSource = dt;

            //Updated by Hungtq 18/09/2012
            //lblTongLuongNPLSuDung.Summary.Running = SummaryRunning.Group;
            //lblTongLuongNPLSuDung.Summary.Func = SummaryFunc.Sum;
            //lblTongLuongNPLSuDung.Summary.FormatString = "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}";

            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap")});
                //new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                //new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                //new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")
            }
            else
            {
                this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});

            }
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
            if (!string.IsNullOrEmpty(GlobalSettings.NoiDungSoHoSoThanhKhoan))
            {
                lblSHSTK.Text = lblSHSTK.Text + GlobalSettings.NoiDungSoHoSoThanhKhoan;
            }
            lblSHSTK.Width = 300;
            //lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            //lblMaNPL.DataBindings.Add("Text", this.DataSource,  dt.TableName + ".MaNPL");
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
           // lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            lblLuongTonDau.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongTonDau", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");

            //HQ
            lblNgayThangHQ.Text = Properties.Settings.Default.TieudeNgay;
            //DN
            lblNgayThangDN.Text = GlobalSettings.TieudeNgay;

            //Bo sung them cot Index tren table
            DataColumn colIndex = new DataColumn("Index", typeof(int));
            dt.Columns.Add(colIndex);

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                dt.Rows[j]["Index"] = j;
            }
        }

        public void BindReportByMaNPL(string where, string MaNPL)
        {
            GlobalSettings.KhoiTao_GiaTriMacDinh();
            this.PrintingSystem.ShowMarginsWarning = false;
            string order = "";
            int sapxeptkx = Properties.Settings.Default.TKX;
            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                if (sapxeptkx == 2)
                    order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    //order = "MaNPL,ngayhoanthanhnhap,ngayhoanthanhxuat,SoToKhaiNhap,LuongTonCuoi DESC";
                    //TODO: Update by Hungtq 08/06/2012. Bỏ sắp xếp theo "ngayhoanthanhxuat" 
                    //dùng hiển thị đúng thứ tự tờ khai âm kế tiếp. Ngày của tờ khai âm kế tiếp có thế < tờ khai trước đó.
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
                else
                    order = "MaNPL,ngayhoanthanhnhap,SoToKhaiNhap,LuongTonCuoi DESC";
            }
            else
            {
                if (sapxeptkx == 2)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngayhoanthanhxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else if (sapxeptkx == 0)
                    order = " ngayhoanthanhnhap,ngaydangkynhap,ngaydangkyxuat,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";
                else
                    order = " ngayhoanthanhnhap,SoToKhaiNhap,MaNPL,LuongTonCuoi DESC";

            }

            dtMaNPLTemp = BLL.SXXK.ThanhKhoan.BCXuatNhapTon.GetSoLanNPLThamGiaThanhKhoan(GlobalSettings.MA_DON_VI, this.LanThanhLy);

            dt = new Company.BLL.KDT.SXXK.BCXuatNhapTon().SelectDynamic("(MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaNPL ='" + MaNPL + "' AND LanThanhLy = " + this.LanThanhLy + ")" + where, order).Tables[0];
            int i = 1;
            string maNPL = "";
            DateTime ngayDangKyNhap = DateTime.Today;
            long soToKhaiNhap = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (maNPL == dr["MaNPL"].ToString() && ngayDangKyNhap == Convert.ToDateTime(dr["NgayDangKyNhap"]) && soToKhaiNhap == Convert.ToInt64(dr["SoToKhaiNhap"]))
                {
                    dr["STT"] = i;
                }
                else
                {
                    maNPL = dr["MaNPL"].ToString();
                    ngayDangKyNhap = Convert.ToDateTime(dr["NgayDangKyNhap"]);
                    soToKhaiNhap = Convert.ToInt64(dr["SoToKhaiNhap"]);
                    i++;
                    dr["STT"] = i;
                }
            }
            dt.TableName = "BCXuatNhapTon";
            this.DataSource = dt;
          
            if (GlobalSettings.SoThapPhan.SapXepTheoTK == 1)
            {
                this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});
            }
            else if (GlobalSettings.SoThapPhan.SapXepTheoTK == 0)
            {
                this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap")});
                //new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                //new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                //new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")
            }
            else
            {
                this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("NgayHoanThanhNhap"),
                new DevExpress.XtraReports.UI.GroupField("NgayDangKyNhap"),
                new DevExpress.XtraReports.UI.GroupField("SoToKhaiNhap"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                new DevExpress.XtraReports.UI.GroupField("LuongNhap"),
                new DevExpress.XtraReports.UI.GroupField("LuongTonDau"),
                new DevExpress.XtraReports.UI.GroupField("TenDVT_NPL")});

            }
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
            if (!string.IsNullOrEmpty(GlobalSettings.NoiDungSoHoSoThanhKhoan))
            {
                lblSHSTK.Text = lblSHSTK.Text + GlobalSettings.NoiDungSoHoSoThanhKhoan;
            }
            lblSHSTK.Width = 300;
            lblSoToKhaiNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhaiNhap");
            //lblNgayDangKyNhap.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKyNhap", "{0:dd/MM/yy}");
            lblTenDVT_NPL.DataBindings.Add("Text", this.DataSource, dt.TableName + ".TenDVT_NPL");
            lblTongLuongNPLTaiXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLTaiXuat", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblTongLuongNPLSuDung.DataBindings.Add("Text", this.DataSource, dt.TableName + ".LuongNPLSuDung", "{0:n" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            //HQ
            lblNgayThangHQ.Text = Properties.Settings.Default.TieudeNgay;
            //DN
            lblNgayThangDN.Text = GlobalSettings.TieudeNgay;

            //Bo sung them cot Index tren table
            DataColumn colIndex = new DataColumn("Index", typeof(int));
            dt.Columns.Add(colIndex);

            for (int j = 0; j < dt.Rows.Count; j++)
            {
                dt.Rows[j]["Index"] = j;
            }
        }

        

      
      private void BCNPLXuatNhapTon_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
            this.DtGhiChu = new DataTable();
            DataColumn[] cols = new DataColumn[3];
            cols[0] = new DataColumn("MaNPL", typeof(string));
            cols[1] = new DataColumn("From", typeof(int));
            cols[2] = new DataColumn("To", typeof(int));
            this.DtGhiChu.Columns.AddRange(cols);
        }

        private void GroupHeader1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (MaNPLBanLuu != GetCurrentColumnValue("MaNPL").ToString())
            {
                this.STT++;
            }

            temp = Convert.ToDecimal(GetCurrentColumnValue("LuongTonDau"));
            if (lblSoToKhaiNhap.Text != "")
                SoToKhaiNhap = Convert.ToInt32(lblSoToKhaiNhap.Text);
        }

        private void GroupHeader1_AfterPrint(object sender, EventArgs e)
        {
            //TODO: Updated by HungTQ, 17/09/2012.
            //Visible Group Footer
            MaNPLTemp = GetCurrentColumnValue("MaNPL").ToString(); //Da su dung dong nay tai vi tri Detail_BeforePrint
            MaSPTemp = GetCurrentColumnValue("MaSP").ToString() + "/" + GetCurrentColumnValue("SoToKhaiXuat").ToString();
            if (MaNPLBanLuu != MaNPLTemp)
            {
                MaNPLBanLuu = MaNPLTemp;
                cnt = 1;

                _TongLuongNhap = (System.Decimal)GetCurrentColumnValue("LuongNhap");
                _TongLuongTonDau = (System.Decimal)GetCurrentColumnValue("LuongTonDau");
            }
            else
            {
                cnt += 1;

                _TongLuongNhap += (System.Decimal)GetCurrentColumnValue("LuongNhap");
                _TongLuongTonDau += (System.Decimal)GetCurrentColumnValue("LuongTonDau");
            }

            //Visible Group Footer            
           
        }

        private int LaySoLanMaNPLThamGiaThanhKhoan(string maNPL)
        {
            return dtMaNPLTemp.Select("MaNPL = '" + maNPL + "'").Length;
        }

        private int GetFrom(string maNPL, int to)
        {
            foreach (DataRow dr in this.DtGhiChu.Rows)
            {
                if (dr["MaNPL"].ToString() == maNPL && to == Convert.ToInt32(dr["To"]))
                    return Convert.ToInt32(dr["From"]);
            }
            return 0;
        }

       
        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (STTBanLuu != STT)
            {
                lblSTT.Text = this.STT + "";

                STTBanLuu = STT;
            }
            else
                lblSTT.Text = "";
        }

        private void lblMaNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblMaNPL.Text = GetCurrentColumnValue("TenNPL").ToString() + " / " + GetCurrentColumnValue("MaNPL").ToString();
        }

        private void lblTongLuongNPLTaiXuat_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (Convert.ToDecimal(GetCurrentColumnValue("LuongNPLTaiXuat")) == 0)
                lblTongLuongNPLTaiXuat.Text = "";
        }

        private void lblXuLyNPLTonCuoiKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (lblXuLyNPLTonCuoiKy.Text.Contains("Chuyển TK"))
            {
                DataRow dr = this.DtGhiChu.NewRow();
                dr["MaNPL"] = GetCurrentColumnValue("MaNPL").ToString();
                dr["From"] = Convert.ToInt32(GetCurrentColumnValue("SoToKhaiNhap"));
                dr["To"] = Convert.ToInt32(lblXuLyNPLTonCuoiKy.Text.Replace("Chuyển TK ", ""));
                this.DtGhiChu.Rows.Add(dr);
            }
        }

        private void GroupHeader2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblLuongTonDau.Text = _TongLuongTonDau.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            lblTongLuongNPLSuDung.Text = _TongLuongNPLSuDung.ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            lblTongTonCuoi.Text = (_TongLuongTonDau - _TongLuongNPLSuDung - _TongLuongNPLTaiXuat).ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);  
        }

       

       

    }
}
