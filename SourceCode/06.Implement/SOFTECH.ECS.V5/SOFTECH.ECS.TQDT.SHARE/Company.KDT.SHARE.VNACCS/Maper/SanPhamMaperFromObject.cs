﻿using System.Data;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using System;
using System.Runtime.InteropServices;
namespace Company.KDT.SHARE.VNACCS.Maper
{
    public partial class SanPhamMaperFromObject
    {
        #region Private members.

        protected string _MaHaiQuan = String.Empty;
        protected string _MaDoanhNghiep = String.Empty;
        protected string _Ma = String.Empty;
        protected string _Ten = String.Empty;
        protected string _MaHS = String.Empty;
        protected string _DVT_ID = String.Empty;
        protected int _IsExist = 0;
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value.Trim(); }
            get { return this._MaHaiQuan.Trim(); }
        }

        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value.Trim(); }
            get { return this._MaDoanhNghiep.Trim(); }
        }

        public string Ma
        {
            set { this._Ma = value; }
            get { return this._Ma; }
        }

        public string Ten
        {
            set { this._Ten = value; }
            get { return this._Ten; }
        }

        public string MaHS
        {
            set { this._MaHS = value; }
            get { return this._MaHS; }
        }

        public string DVT_ID
        {
            set { this._DVT_ID = value; }
            get { return this._DVT_ID; }
        }

        //---------------------------------------------------------------------------------------------

        public int IsExist
        {
            set { this._IsExist = value; }
            get { return this._IsExist; }
        }

        #endregion
        public bool LoadSP_SXXK(string Ma, string MaHS)
        {
            string sql = " SELECT TOP 1 "
                         + " FROM t_SXXK_SanPham  "
                         + " Where Ma = @Ma AND MaHS=@MaHS";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return false;
            else
                return true;
        }
        public bool LoadSP_GC(string Ma, string MaHS)
        {
            string sql = " SELECT TOP 1 "
                         + " FROM t_SXXK_SanPham  "
                         + " Where Ma = @Ma AND MaHS=@MaHS";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return false;
            else
                return true;
        }
        public string GetMaSP_SXXK(string Ten,string MaHS)
        {
            string sql = " SELECT Ma"
                         + " FROM t_SXXK_SanPham  "
                         + " Where Ten = @Ten AND MaHS=@MaHS";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NVarChar, MaHS);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return null;
            else
                return (String)obj;
        }
        public string GetMaSP_GC(string Ten, string MaHS)
        {
            string sql = " SELECT Ma"
                         + " FROM t_SXXK_SanPham  "
                         + " Where Ten = @Ten AND MaHS=@MaHS";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NVarChar, MaHS);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return null;
            else
                return (String)obj;
        }
        public bool LoadNPL_SXXK(string Ma, string MaHS)
        {
            string sql = " SELECT TOP 1 "
                         + " FROM t_SXXK_NguyenPhuLieu  "
                         + " Where Ma = @Ma AND MaHS=@MaHS";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return false;
            else
                return true;
        }
        public bool LoadNPL_GC(string Ma, string MaHS)
        {
            string sql = " SELECT TOP 1 "
                         + " FROM t_SXXK_NguyenPhuLieu  "
                         + " Where Ma = @Ma AND MaHS=@MaHS";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(sql);
            db.AddInParameter(dbCommand, "@Ma", SqlDbType.VarChar, Ma);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return false;
            else
                return true;
        }
        public string GetMaNPL_SXXK(string Ten, string MaHS)
        {
            string sql = " SELECT Ma"
                         + " FROM t_SXXK_NguyenPhuLieu  "
                         + " Where Ten = @Ten AND MaHS=@MaHS";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NVarChar, MaHS);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return null;
            else
                return (String)obj;
        }
        public string GetMaNPL_GC(string Ten, string MaHS)
        {
            string sql = " SELECT Ma"
                         + " FROM t_SXXK_NguyenPhuLieu  "
                         + " Where Ten = @Ten AND MaHS=@MaHS";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@Ten", SqlDbType.NVarChar, Ten);
            db.AddInParameter(dbCommand, "@MaHS", SqlDbType.NVarChar, MaHS);
            object obj = db.ExecuteScalar(dbCommand);
            if (obj == null || string.IsNullOrEmpty(obj.ToString()))
                return null;
            else
                return (String)obj;
        }
    }
}