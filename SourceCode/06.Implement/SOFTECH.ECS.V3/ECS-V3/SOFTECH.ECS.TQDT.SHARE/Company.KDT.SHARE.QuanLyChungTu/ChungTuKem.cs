﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components.Utils;

namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class ChungTuKem
    {
        public List<ChungTuKemChiTiet> listCTChiTiet = new List<ChungTuKemChiTiet>();
        public void LoadListCTChiTiet()
        {
            listCTChiTiet = ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(this.ID);
        }
        public void InsertUpdateFull(List<ChungTuKemChiTiet> listCTDK)
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);


                        foreach (ChungTuKemChiTiet ctct in listCTDK)
                        {
                            ctct.ChungTuKemID = this.ID;
                            if (id == 0)
                                ctct.ID = 0;

                            if (ctct.ID == 0)
                                ctct.Insert(transaction);
                            else
                                ctct.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }
        public static XmlNode COnvertXML(XmlDocument xmlDoc)
        {
            XmlDeclaration xmlDeclaration = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);

            // Create the root element
            XmlElement rootNode = xmlDoc.CreateElement("CategoryList");
            xmlDoc.InsertBefore(xmlDeclaration, xmlDoc.DocumentElement);
            xmlDoc.AppendChild(rootNode);

            // Create a new <Category> element and add it to the root node
            XmlElement parentNode = xmlDoc.CreateElement("Category");

            // Set attribute name and value!
            parentNode.SetAttribute("ID", "01");

            xmlDoc.DocumentElement.PrependChild(parentNode);

            // Create the required nodes
            XmlElement mainNode = xmlDoc.CreateElement("MainCategory");
            XmlElement descNode = xmlDoc.CreateElement("Description");
            XmlElement activeNode = xmlDoc.CreateElement("Active");

            // retrieve the text 
            XmlText categoryText = xmlDoc.CreateTextNode("XML");
            XmlText descText = xmlDoc.CreateTextNode("This is a list my XML articles.");
            XmlText activeText = xmlDoc.CreateTextNode("true");

            // append the nodes to the parentNode without the value
            parentNode.AppendChild(mainNode);
            parentNode.AppendChild(descNode);
            parentNode.AppendChild(activeNode);

            // save the value of the fields into the nodes
            mainNode.AppendChild(categoryText);
            descNode.AppendChild(descText);
            activeNode.AppendChild(activeText);

            return rootNode;
        }
        public static XmlNode ConvertCollectionCTDinhKemToXML(XmlDocument doc, List<ChungTuKem> COCollection, long TKMD_ID, List<ChungTuKemChiTiet> ListCTCT)
        {
            try
            {
                //List<ChungTuKemChiTiet> ListCTCT = new List<ChungTuKemChiTiet>();
                //ListCTCT =ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(tk)
                if (COCollection == null || COCollection.Count == 0)
                {
                    COCollection = ChungTuKem.SelectCollectionBy_TKMDID(TKMD_ID);
                }
                XmlElement CHUNG_TU_ANH = doc.CreateElement("CHUNG_TU_KEM");
                foreach (ChungTuKem chungTuKem in COCollection)
                {
                    XmlElement CoItem = doc.CreateElement("CHUNG_TU_KEM.ITEM");
                    CHUNG_TU_ANH.AppendChild(CoItem);

                    XmlElement SO_CT = doc.CreateElement("SO_CT");
                    SO_CT.InnerText = FontConverter.Unicode2TCVN(chungTuKem.SO_CT);
                    CoItem.AppendChild(SO_CT);

                    XmlElement NGAY_CAP_CO = doc.CreateElement("NGAY_CT");
                    NGAY_CAP_CO.InnerText = chungTuKem.NGAY_CT.ToString("yyyy-MM-dd");
                    CoItem.AppendChild(NGAY_CAP_CO);

                    //CHUA chungTuKem PHAI BO SUNG VA KO DC TRONG
                    XmlElement NGUOI_KY = doc.CreateElement("MA_LOAI_CT");
                    NGUOI_KY.InnerText = chungTuKem.MA_LOAI_CT;
                    CoItem.AppendChild(NGUOI_KY);

                    //NOI PHAT HANH LA TO CHUC CAP
                    XmlElement NOI_PHAT_HANH = doc.CreateElement("DIENGIAI");
                    NOI_PHAT_HANH.InnerText = FontConverter.Unicode2TCVN(chungTuKem.DIENGIAI);
                    CoItem.AppendChild(NOI_PHAT_HANH);

                    XmlElement DINH_KEM = doc.CreateElement("DINH_KEM");
                    //doc.DocumentElement.PrependChild(DINH_KEM);
                    CoItem.AppendChild(DINH_KEM);
                    //XmlElement DINHKEM_ITEM;
                    //XmlElement DINHKEM_ITEM_CT;//
                    //XmlText DINHKEM_ITEMtext;
                    foreach (ChungTuKemChiTiet chungTuKemChitiet in chungTuKem.listCTChiTiet)
                    {

                        XmlElement DINHKEM_ITEM = doc.CreateElement("DINH_KEM.ITEM");
                        DINH_KEM.AppendChild(DINHKEM_ITEM);

                        XmlElement TenFile = doc.CreateElement("TENFILE");
                        TenFile.InnerText = chungTuKemChitiet.FileName.Trim();
                        DINHKEM_ITEM.AppendChild(TenFile);

                        XmlElement NoiDung = doc.CreateElement("NOIDUNG");
                        //DINHKEM_ITEM.Value = "xmlns:dt=" ;//urn:schemas-microsoft-com:datatypes" dt:dt="bin.base64"";
                        // string 
                        // NoiDung.InnerText = System.Convert.ToBase64String(chungTuKemChitiet.NoiDung, 0, chungTuKemChitiet.NoiDung.Length-1, Base64FormattingOptions.InsertLineBreaks);
                        NoiDung.InnerText = System.Convert.ToBase64String(chungTuKemChitiet.NoiDung, Base64FormattingOptions.None);
                        DINHKEM_ITEM.AppendChild(NoiDung);
                        //string tempp = System.Convert.ToBase64String(chungTuKemChitiet.NoiDung);

                        XmlAttribute AttNoidung1 = doc.CreateAttribute("xmlns:dt");
                        AttNoidung1.Value = "urn:schemas-microsoft-com:datatypes";
                        NoiDung.Attributes.Append(AttNoidung1);
                        // NoiDung.SetAttributeNode(AttNoidung1.LocalName, "");

                        XmlAttribute AttNoidung2 = doc.CreateAttribute(@"dt:dt");
                        AttNoidung2.Value = "bin.base64";
                        //AttNoidung2.InnerXml.Replace("dt", "dt:dt");
                        NoiDung.Attributes.Append(AttNoidung2);
                    }

                    // Lưu ý: Nếu không thay thế sẽ bị lỗi.
                    DINH_KEM.InnerXml = DINH_KEM.InnerXml.Replace("dt=\"bin.base64\"", "dt:dt=\"bin.base64\"");
                }
                if (CHUNG_TU_ANH.ChildNodes.Count == 0)
                    return null;
                return CHUNG_TU_ANH;
            }
            catch (Exception ex) {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw ex; }

            return null;
        }
        public static XmlElement ConvertCollectionCTDinhKemToXMLMoi(XmlDocument doc, List<ChungTuKem> COCollection, long TKMD_ID, List<ChungTuKemChiTiet> ListCTCT, XmlElement CHUNG_TU_CO)
        {
            //List<ChungTuKemChiTiet> ListCTCT = new List<ChungTuKemChiTiet>();
            //ListCTCT =ChungTuKemChiTiet.SelectCollectionBy_ChungTuKemID(tk)
            if (COCollection == null || COCollection.Count == 0)
            {
                COCollection = ChungTuKem.SelectCollectionBy_TKMDID(TKMD_ID);
            }
            //XmlNode  CHUNG_TU_CO = doc.GetElementsByTagName("CHUNG_TU_KEM")[0];
            foreach (ChungTuKem co in COCollection)
            {
                XmlElement CoItem = doc.CreateElement("CHUNG_TU_KEM.ITEM");
                CHUNG_TU_CO.AppendChild(CoItem);

                XmlElement SO_CT = doc.CreateElement("SO_CT");
                SO_CT.InnerText = FontConverter.Unicode2TCVN(co.SO_CT);
                CoItem.AppendChild(SO_CT);

                // XmlAttribute SO_CO = doc.CreateAttribute("SO_CT");
                //SO_CT.Value = co.SO_CT;
                //CoItem.Attributes.Append(SO_CT);              

                XmlElement NGAY_CAP_CO = doc.CreateElement("NGAY_CT");
                NGAY_CAP_CO.InnerText = co.NGAY_CT.ToString("yyyy-MM-dd");
                CoItem.AppendChild(NGAY_CAP_CO);

                //CHUA CO PHAI BO SUNG VA KO DC TRONG
                XmlElement NGUOI_KY = doc.CreateElement("MA_LOAI_CT");
                NGUOI_KY.InnerText = co.MA_LOAI_CT;
                CoItem.AppendChild(NGUOI_KY);

                //NOI PHAT HANH LA TO CHUC CAP
                XmlElement NOI_PHAT_HANH = doc.CreateElement("DIENGIAI");
                NOI_PHAT_HANH.InnerText = FontConverter.Unicode2TCVN(co.DIENGIAI);
                CoItem.AppendChild(NOI_PHAT_HANH);

                XmlElement DINH_KEM = doc.CreateElement("DINH_KEM");
                //doc.DocumentElement.PrependChild(DINH_KEM);
                CoItem.AppendChild(DINH_KEM);
                //XmlElement DINHKEM_ITEM;
                //XmlElement DINHKEM_ITEM_CT;//
                //XmlText DINHKEM_ITEMtext;
                foreach (ChungTuKemChiTiet ctct in ListCTCT)
                {

                    XmlElement DINHKEM_ITEM = doc.CreateElement("DINH_KEM.ITEM");
                    DINH_KEM.AppendChild(DINHKEM_ITEM);

                    XmlElement TenFile = doc.CreateElement("TENFILE");
                    TenFile.InnerText = ctct.FileName.Trim();
                    DINHKEM_ITEM.AppendChild(TenFile);

                    XmlElement NoiDung = doc.CreateElement("NOIDUNG");
                    //DINHKEM_ITEM.Value = "xmlns:dt=" ;//urn:schemas-microsoft-com:datatypes" dt:dt="bin.base64"";
                    // string 
                    // NoiDung.InnerText = System.Convert.ToBase64String(ctct.NoiDung, 0, ctct.NoiDung.Length-1, Base64FormattingOptions.InsertLineBreaks);
                    NoiDung.InnerText = System.Convert.ToBase64String(ctct.NoiDung, Base64FormattingOptions.InsertLineBreaks);
                    DINHKEM_ITEM.AppendChild(NoiDung);
                    //string tempp = System.Convert.ToBase64String(ctct.NoiDung);

                    XmlAttribute AttNoidung1 = doc.CreateAttribute("xmlns:dt");
                    AttNoidung1.Value = "urn:schemas-microsoft-com:datatypes";
                    NoiDung.Attributes.Append(AttNoidung1);
                    // NoiDung.SetAttributeNode(AttNoidung1.LocalName, "");

                    XmlAttribute AttNoidung2 = doc.CreateAttribute(@"dt:dt");
                    AttNoidung2.Value = "bin.base64";
                    //AttNoidung2.InnerXml.Replace("dt", "dt:dt");
                    NoiDung.Attributes.Append(AttNoidung2);

                }

            }
            if (CHUNG_TU_CO.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_CO;

        }
        public string send(string password, string maHQ, string tenHQ, long soTK, string maLH, int namDK,
            long TKMD_ID, string maDN, string tenDN, List<ChungTuKemChiTiet> list, int messgaseTypeKB, int messgaseFunctionKB)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            //set thong tin doanh nghiep
            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            XmlNode nodeFromName = nodeFrom.ChildNodes[0];
            nodeFromName.InnerText = tenDN;
            XmlNode nodeFromIdentity = nodeFrom.ChildNodes[1];
            nodeFromIdentity.InnerText = maDN;

            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = tenHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            //set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = messgaseTypeKB + "";
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = messgaseFunctionKB + "";


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GUIDSTR;

            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            if (GUIDSTR == "")
            {
                GUIDSTR = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GUIDSTR;
                this.Update();
            }

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDN;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = tenDN;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = tenHQ;


            XmlNode nodeTO_KHAI = doc.GetElementsByTagName("TO_KHAI")[0];

            nodeTO_KHAI.Attributes["MA_LH"].Value = maLH;
            nodeTO_KHAI.Attributes["MA_HQ"].Value = maHQ;
            nodeTO_KHAI.Attributes["SOTK"].Value = soTK + "";
            nodeTO_KHAI.Attributes["NAMDK"].Value = namDK + "";


            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            List<ChungTuKem> listChungTuKem = new List<ChungTuKem>();
            listChungTuKem.Add(this);
            nodeDulieu.AppendChild(ChungTuKem.ConvertCollectionCTDinhKemToXML(doc, listChungTuKem, TKMD_ID, list));


            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        public bool WSKhaiBaoChungTuDinhKem(string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long TKMD_ID, string maDoanhNghiep, List<ChungTuKemChiTiet> list, MessageTypes messageType, MessageFunctions messageFunction)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep.Trim();
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep.Trim();

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan.Trim();
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan.Trim();

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];
            if (string.IsNullOrEmpty(GUIDSTR)) GUIDSTR = Guid.NewGuid().ToString();
            nodeReference.InnerText = GUIDSTR;
            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep.Trim();
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep.Trim();

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan.Trim();

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";


            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<ChungTuKem> listChungTuKem = new List<ChungTuKem>();
            listChungTuKem.Add(this);
            xmlNodeDulieu.AppendChild(ChungTuKem.ConvertCollectionCTDinhKemToXML(xmlDocument, listChungTuKem, TKMD_ID, list));

            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {                
                // Khai báo.
               
                kq = kdt.Send(xmlDocument.InnerXml, password);
                xmlDocument.InnerXml.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungChungTuDinhKem);

            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);

                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        //string noiDung = FontConverter.TCVN2Unicode(docResult.GetElementsByTagName("Root")[0].InnerText.Trim());
                        //XmlNode node1 = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                        //XmlNode nodeRoot = xmlDocument.ChildNodes[1].SelectSingleNode("Body/Content");
                        //kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoBoSungLayPhanHoiChungTuDinhKem);
                        return true;
                    }
                }
                else
                {
                    
                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL";                    
                    throw new Exception(msgError);
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    kq.XmlSaveMessage(ID, MessageTitle.Error,msgError);
                else
                Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }
            return false;

        }

        /// <summary>
        /// Lấy số tiếp nhận khai báo.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLaySoTiepNhan(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GUIDSTR), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GUIDSTR), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                        xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }
            
            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);
                XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
                if (xmlNodeResult.Attributes["Err"].Value == "yes")
                {
                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                    else
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);

                    }
                }

                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                    throw new Exception(msgError);
                }

                if (xmlNodeResult.Attributes["SO_TN"] != null)
                {
                    this.SOTN = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                    this.NGAYTN = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                    this.Update();

                    kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoBoSungChungTuDinhKemThanhCong, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SOTN, this.NGAYTN.ToShortDateString()));
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                else Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }
            return false;
        }

        public string GenerateToXmlEnvelope(MessageTypes messageType, MessageFunctions messageFunction, Guid referenceId, string path, string maHQ, string maDN)
        {
            XmlDocument doc = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");
                else
                    doc.Load(path + @"\TemplateXML\PhongBi.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = Globals.trimMaHaiQuan(maHQ);

            // Subject.
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)messageType).ToString();

            // Function.
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference ID.
            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = referenceId.ToString();

            // Message ID.
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = System.Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = maDN;
            nodeFrom.ChildNodes[0].InnerText = maDN;

            this.GUIDSTR = referenceId.ToString();
            this.Update();
            return doc.InnerXml;
        }

        /// <summary>
        /// Nhận phản hồi thông quan điện tử sau khi khai báo đã có số tiếp nhận.
        /// </summary>
        /// <param name="password">password</param>
        /// <returns></returns>
        public bool WSLayPhanHoi(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GUIDSTR), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GUIDSTR), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                    xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }
            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GUIDSTR.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
                Globals.SaveMessage(xmlDocument.InnerXml, this.ID, MessageTitle.KhaiBaoBoSungAnh);

            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);
                XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
                if (xmlNodeResult.Attributes["Err"].Value == "yes")
                {
                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                    else
                    {
                        msgError = "Thông báo từ hải quan " +FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);

                    }
                }

                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                    throw new Exception(msgError);
                }

                if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
                {

                    kq.XmlSaveMessage(ID, MessageTitle.TuChoiTiepNhan,
                        string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SOTN, this.NGAYTN.ToShortDateString(), xmlNodeResult.InnerText));
                    // Cập lại số tiếp nhận
                    this.SOTN = 0;
                    this.NGAYTN = new DateTime(1900, 1, 1);
                    this.Update();
                    //this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.KHONG_PHE_DUYET.ToString();
                    return true;
                }
                else if (xmlNodeResult.Attributes["SO_TN"] != null)
                {
                    this.SOTN = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                    this.NGAYTN = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                    this.Update();

                    kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoBoSungChungTuDinhKemThanhCong,
                        string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SOTN, this.NGAYTN.ToShortDateString()));
                    return true;
                }
                else if (xmlNodeResult.Attributes["SOTN"] != null)
                {

                    kq.XmlSaveMessage(ID, MessageTitle.KhaiBaoBoSungChungTuDinhKemDuocChapNhan,
                           string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã được Hải quan chấp nhận.", this.SOTN, this.NGAYTN.ToShortDateString()));

                    //Cập nhật trạng thái chứng từ kèm
                    this.TrangThaiXuLy = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET.ToString();
                    this.Update();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    kq.XmlSaveMessage(ID, MessageTitle.Error, msgError);
                else Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }

            return false;
        }

        public static ChungTuKem Load(string soCT, DateTime ngayCT, string maLoaiCT, long idTKMD, SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_ChungTuKem_LoadBy]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SO_CT", SqlDbType.VarChar, soCT);
            db.AddInParameter(dbCommand, "@NGAY_CT", SqlDbType.DateTime, ngayCT);
            db.AddInParameter(dbCommand, "@MA_LOAI_CT", SqlDbType.VarChar, maLoaiCT);
            db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, idTKMD);

            ChungTuKem entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                entity = new ChungTuKem();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SO_CT"))) entity.SO_CT = reader.GetString(reader.GetOrdinal("SO_CT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAY_CT"))) entity.NGAY_CT = reader.GetDateTime(reader.GetOrdinal("NGAY_CT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MA_LOAI_CT"))) entity.MA_LOAI_CT = reader.GetString(reader.GetOrdinal("MA_LOAI_CT"));
                if (!reader.IsDBNull(reader.GetOrdinal("DIENGIAI"))) entity.DIENGIAI = reader.GetString(reader.GetOrdinal("DIENGIAI"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetString(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetString(reader.GetOrdinal("TrangThaiXuLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("Tempt"))) entity.Tempt = reader.GetDecimal(reader.GetOrdinal("Tempt"));
                if (!reader.IsDBNull(reader.GetOrdinal("MessageID"))) entity.MessageID = reader.GetString(reader.GetOrdinal("MessageID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GUIDSTR"))) entity.GUIDSTR = reader.GetString(reader.GetOrdinal("GUIDSTR"));
                if (!reader.IsDBNull(reader.GetOrdinal("KDT_WAITING"))) entity.KDT_WAITING = reader.GetString(reader.GetOrdinal("KDT_WAITING"));
                if (!reader.IsDBNull(reader.GetOrdinal("KDT_LASTINFO"))) entity.KDT_LASTINFO = reader.GetString(reader.GetOrdinal("KDT_LASTINFO"));
                if (!reader.IsDBNull(reader.GetOrdinal("SOTN"))) entity.SOTN = reader.GetDecimal(reader.GetOrdinal("SOTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("NGAYTN"))) entity.NGAYTN = reader.GetDateTime(reader.GetOrdinal("NGAYTN"));
                if (!reader.IsDBNull(reader.GetOrdinal("TotalSize"))) entity.TotalSize = reader.GetDecimal(reader.GetOrdinal("TotalSize"));
                if (!reader.IsDBNull(reader.GetOrdinal("Phanluong"))) entity.Phanluong = reader.GetString(reader.GetOrdinal("Phanluong"));
                if (!reader.IsDBNull(reader.GetOrdinal("HuongDan"))) entity.HuongDan = reader.GetString(reader.GetOrdinal("HuongDan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMDID"))) entity.TKMDID = reader.GetInt64(reader.GetOrdinal("TKMDID"));
            }
            reader.Close();
            return entity;
        }


        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_ChungTuKem_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SO_CT", SqlDbType.VarChar, SO_CT);
            db.AddInParameter(dbCommand, "@NGAY_CT", SqlDbType.DateTime, NGAY_CT.Year <= 1753 ? DBNull.Value : (object)NGAY_CT);
            db.AddInParameter(dbCommand, "@MA_LOAI_CT", SqlDbType.VarChar, MA_LOAI_CT);
            db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.VarChar, DIENGIAI);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.VarChar, LoaiKB);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@Tempt", SqlDbType.Decimal, Tempt);
            db.AddInParameter(dbCommand, "@MessageID", SqlDbType.VarChar, MessageID);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.VarChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@KDT_WAITING", SqlDbType.VarChar, KDT_WAITING);
            db.AddInParameter(dbCommand, "@KDT_LASTINFO", SqlDbType.VarChar, KDT_LASTINFO);
            db.AddInParameter(dbCommand, "@SOTN", SqlDbType.Decimal, SOTN);
            db.AddInParameter(dbCommand, "@NGAYTN", SqlDbType.DateTime, NGAYTN.Year <= 1753 ? DBNull.Value : (object)NGAYTN);
            db.AddInParameter(dbCommand, "@TotalSize", SqlDbType.Decimal, TotalSize);
            db.AddInParameter(dbCommand, "@Phanluong", SqlDbType.NVarChar, Phanluong);
            db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
            db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, TKMDID);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_ChungTuKem_Update]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SO_CT", SqlDbType.VarChar, SO_CT);
            db.AddInParameter(dbCommand, "@NGAY_CT", SqlDbType.DateTime, NGAY_CT.Year == 1753 ? DBNull.Value : (object)NGAY_CT);
            db.AddInParameter(dbCommand, "@MA_LOAI_CT", SqlDbType.VarChar, MA_LOAI_CT);
            db.AddInParameter(dbCommand, "@DIENGIAI", SqlDbType.VarChar, DIENGIAI);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.VarChar, LoaiKB);
            db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.VarChar, TrangThaiXuLy);
            db.AddInParameter(dbCommand, "@Tempt", SqlDbType.Decimal, Tempt);
            db.AddInParameter(dbCommand, "@MessageID", SqlDbType.VarChar, MessageID);
            db.AddInParameter(dbCommand, "@GUIDSTR", SqlDbType.VarChar, GUIDSTR);
            db.AddInParameter(dbCommand, "@KDT_WAITING", SqlDbType.VarChar, KDT_WAITING);
            db.AddInParameter(dbCommand, "@KDT_LASTINFO", SqlDbType.VarChar, KDT_LASTINFO);
            db.AddInParameter(dbCommand, "@SOTN", SqlDbType.Decimal, SOTN);
            db.AddInParameter(dbCommand, "@NGAYTN", SqlDbType.DateTime, NGAYTN.Year == 1753 ? DBNull.Value : (object)NGAYTN);
            db.AddInParameter(dbCommand, "@TotalSize", SqlDbType.Decimal, TotalSize);
            db.AddInParameter(dbCommand, "@Phanluong", SqlDbType.NVarChar, Phanluong);
            db.AddInParameter(dbCommand, "@HuongDan", SqlDbType.VarChar, HuongDan);
            db.AddInParameter(dbCommand, "@TKMDID", SqlDbType.BigInt, TKMDID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

    }
}
