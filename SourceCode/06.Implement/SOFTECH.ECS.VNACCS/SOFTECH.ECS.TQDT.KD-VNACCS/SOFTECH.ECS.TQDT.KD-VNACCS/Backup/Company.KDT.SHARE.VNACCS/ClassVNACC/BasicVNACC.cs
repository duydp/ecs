﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.IO;

namespace Company.KDT.SHARE.VNACCS.ClassVNACC
{
    public class BasicVNACC
    {
 
        /// <summary>
        /// Không sử dụng
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="StrBuild"></param>
        /// <param name="SortIndex"></param>
        /// <returns></returns>
        public StringBuilder BuildEdiMessages<T>(StringBuilder StrBuild, bool SortIndex)
        {
            StringBuilder str = StrBuild;
            PropertyInfo[] propertiesInfos = typeof(T).GetProperties(BindingFlags.Public);
            if(SortIndex)
            { 
            Array.Sort(propertiesInfos,
                delegate(PropertyInfo propety1, PropertyInfo property2)
                {
                    AttributeIndex index1 = propety1.GetCustomAttributes(typeof(AttributeIndex), true)[0] as AttributeIndex;
                    AttributeIndex index2 = property2.GetCustomAttributes(typeof(AttributeIndex), true)[0] as AttributeIndex;
                    return index1.STT.CompareTo(index2.STT);
                }
                    );
            }
            for (int i = 0; i < propertiesInfos.Length; i++)
            {
                if (propertiesInfos[i].GetType() == typeof(PropertiesAttribute))
                {
                    PropertiesAttribute properties = (PropertiesAttribute)propertiesInfos[i].GetValue(this, null);
                    str = properties.ToEDI(StrBuild,true);
                }
                else if (propertiesInfos[i].GetType() == typeof(GroupAttribute))
                {
                    GroupAttribute group = (GroupAttribute)propertiesInfos[i].GetValue(this, null);
                    str = group.ToEDI(StrBuild);
                }
            }
            return str;
        }

        /// <summary>
        /// Tao messages EDI
        /// </summary>
        /// <typeparam name="T"> Lop can tao </typeparam>
        /// <param name="StrBuild">messages tao duoc se them vao chuoi string nay </param>
        /// <param name="Appline">xuong dong o moi chi tieu (neu la header thi khong xuong dong) </param>
        /// <param name="pathConfig">duong dan chua file config </param>
        /// <param name="MaNghiepVu"> ma nghiep vu dang su dung </param>
        /// <returns></returns>
        public StringBuilder BuildEdiMessages<T>(StringBuilder StrBuild, bool Appline, string pathConfig, string MaNghiepVu)
        {
            StringBuilder str = StrBuild;
            string IDLoi = string.Empty;
            try
            {

                FileInfo finfo = new FileInfo(pathConfig + "\\" + MaNghiepVu + ".txt");
                if (!finfo.Exists)
                {
                    Exception ex = new Exception("Thiếu file config : " + MaNghiepVu + ".txt");
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw ex;
                }
                string[] lines = System.IO.File.ReadAllLines(pathConfig + "\\" + MaNghiepVu + ".txt");
                string groupName = string.Empty;
                for (int i = 0; i < lines.Length; i++)
                {
                    if (!string.IsNullOrEmpty(lines[i].Split('-')[0].Trim()))
                    {
                        if (lines[i].Split('-')[0].Trim().ToUpper() == groupName.ToUpper())
                            continue;
                        PropertyInfo proterty = typeof(T).GetProperty(lines[i].Split('-')[0].Trim());
                        if (proterty != null)
                        {
                            IDLoi = proterty.Name;
                            //if(IDLoi == "DTI")
                            //{ }
                            if (proterty.PropertyType == typeof(PropertiesAttribute))
                            {
                                PropertiesAttribute properties = (PropertiesAttribute)proterty.GetValue(this, null);
                                if (properties != null)
                                    str = properties.ToEDI(StrBuild, Appline);
                            }
                            else if (proterty.PropertyType == typeof(GroupAttribute))
                            {
                                GroupAttribute group = (GroupAttribute)proterty.GetValue(this, null);
                                str = group.ToEDI(StrBuild);
                                groupName = lines[i].Split('-')[0].Trim();
                            }
                        }
                    }
                }
                if (MaNghiepVu == "A2") str.AppendLine();
            }
            catch (System.Exception ex)
            {
                Exception e = new Exception("Lỗi tại ID : " + IDLoi + Environment.NewLine + ex.Message);
                Logger.LocalLogger.Instance().WriteMessage(e);
                throw e;
            }
            return str;

        }


        public void GetObject<T>(string StrBuild, string MaNghiepVu, bool readAll, int TongSoByte)
        {
            GetObject<T>(StrBuild, true, GlobalVNACC.PathConfig, MaNghiepVu, readAll, TongSoByte);
        }
        /// <summary>
        /// Load object tu chuoi string ket qua tra ve tu he thong vnacc
        /// </summary>
        /// <typeparam name="T"> lop can load </typeparam>
        /// <param name="StrBuild"> chuoi string ket qua </param>
        /// <param name="Appline"> xuong dong o moi chi tieu </param>
        /// <param name="pathConfig"> duong dan chua file config </param>
        /// <param name="MaNghiepVu"> nghiep vu dang su dung </param>
        /// <param name="readAll"> co doc het khong hay chi doc den TongSoByteChung </param>
        public void GetObject<T>(string StrBuild, bool Appline, string pathConfig, string MaNghiepVu, bool readAll, int TongSoByte)
        {
            
            if (this == null)
            {
                throw new Exception("Đối tượng " + MaNghiepVu + " không được rỗng");
            }
            else
            {
            if (!readAll)
            {
                StrBuild = HelperVNACCS.GetStringByBytes(StrBuild, TongSoByte);
            }

                string[] StringLines = StrBuild.Split(new string[] { Environment.NewLine }, StringSplitOptions.None); 

                PropertyInfo property = null;
                int IndexLine = 0;
                try
                {

                    FileInfo finfo = new FileInfo(pathConfig + "\\" + MaNghiepVu + ".txt");
                    if (!finfo.Exists)
                    {
                        Exception ex = new Exception("Thiếu file config : " + MaNghiepVu + ".txt");
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        throw ex;
                    }
                    
                    string[] lines = System.IO.File.ReadAllLines(pathConfig + "\\" + MaNghiepVu + ".txt");
                    bool inGroup = false; // Đang trong vòng lặp
                    int indexloop = 0; // Vòng lặp thứ "indexLoop"
                    int indexPropertyInGroup = 0; // thuộc tính thứ "indexPropertyInGroup" trong Group
                    int indexLineConfig = 0;
                    string NameGroup = string.Empty;
                    for (int i = 0; i < StringLines.Length; i++)
                    {
                        IndexLine = i;
                       
                        string value = StringLines[i];
                        if (string.IsNullOrEmpty(value) || value.Length == 0)
                            break;
                        if (indexLineConfig >= lines.Length)
                        {
                            return;
                        }
                        if (!inGroup)
                        {
#region Debug
                            //if (IndexLine == 85)
                            //{

                            //}
#endregion
                            if (!string.IsNullOrEmpty(lines[indexLineConfig].Split('-')[0].Trim()))
                            {

                                property = typeof(T).GetProperty(lines[indexLineConfig].Split('-')[0].Trim());
                                while (property.Name == NameGroup && indexLineConfig < lines.Length - 1)
                                {
                                    indexLineConfig++;
                                    property = typeof(T).GetProperty(lines[indexLineConfig].Split('-')[0].Trim());
                                }
                                if (property != null)
                                {
                                    if (property.PropertyType == typeof(PropertiesAttribute))
                                    {

                                        PropertiesAttribute properties = (PropertiesAttribute)property.GetValue(this, null);
                                        properties.SetValue(value);
                                        indexLineConfig++;
                                        //propertyDetail.SetValue(this, value, null);
                                        //property.SetValue(this,Convert.ChangeType())
                                    }
                                    else if (property.PropertyType == typeof(GroupAttribute))
                                    {
                                        inGroup = true;
                                        NameGroup = property.Name;
                                    }
                                }
                            }
                        }
                        if (inGroup)
                        {
                            GroupAttribute group = (GroupAttribute)property.GetValue(this, null);
#if Debug
                            if(group.LoopID == "KF1")
                            {

                            }
#endif

                            if (indexloop < group.Repetition)
                            {
                                if (indexPropertyInGroup < group.listAttribute.Count)
                                {

                                    group.listAttribute[indexPropertyInGroup].SetValueList(value,indexloop); // Add dữ liệu lần lặp thứ indexlop với thuộc tính thứ indexPropertyInGroup
                                    indexPropertyInGroup++; // tiếp tục add dữ liệu cho thuộc tính tiếp theo
                                }
                                if (indexPropertyInGroup  == group.listAttribute.Count && indexloop == group.Repetition - 1) // Thuộc tính cuối cùng của vòng lặp cuối cùng
                                {
                                    indexPropertyInGroup = 0;
                                    indexloop = 0;
                                    inGroup = false; // Thoát khỏi group
                                    indexLineConfig++;
                                }
                                if (indexPropertyInGroup == group.listAttribute.Count) // Thuộc tính cuối cùng trong group
                                {
                                    indexPropertyInGroup = 0; // Quay ngược lại thuộc tính đầu tiên
                                    indexloop++; // Tăng vòng lặp cho group
                                }
                                
                            }

                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Exception e;
                    if (!ex.Message.Contains("file config"))
                        e = new Exception("Lỗi tại dòng thứ " + (IndexLine + 1).ToString() + " của " + property.Name + ": " + ex.Message);
                    else
                        e = ex;
                    if(property != null)
                        Logger.LocalLogger.Instance().WriteMessage(e);
                    else
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw e;
                }
            }
        }

        
    }
}
