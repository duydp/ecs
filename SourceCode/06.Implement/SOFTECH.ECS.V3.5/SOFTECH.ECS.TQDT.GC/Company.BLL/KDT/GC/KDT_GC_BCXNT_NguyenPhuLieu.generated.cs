﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class KDT_GC_BCXNT_NguyenPhuLieu : ICloneable
	{
		#region Properties.
		
		public int STTHang { set; get; }
		public long HopDong_ID { set; get; }
		public string MaNPL { set; get; }
		public string TenNPL { set; get; }
		public string MaHS { set; get; }
		public string DVT { set; get; }
		public decimal LuongNhap { set; get; }
		public decimal LuongXuat { set; get; }
		public decimal LuongCungUng { set; get; }
		public decimal LuongTon { set; get; }
		public decimal TongNhuCau { set; get; }
		public int TrangThai { set; get; }
		public DateTime TuNgay { set; get; }
		public DateTime DenNgay { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_BCXNT_NguyenPhuLieu> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_BCXNT_NguyenPhuLieu> collection = new List<KDT_GC_BCXNT_NguyenPhuLieu>();
			while (reader.Read())
			{
				KDT_GC_BCXNT_NguyenPhuLieu entity = new KDT_GC_BCXNT_NguyenPhuLieu();
				if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt64(reader.GetOrdinal("HopDong_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) entity.LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongXuat"))) entity.LuongXuat = reader.GetDecimal(reader.GetOrdinal("LuongXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDecimal(reader.GetOrdinal("LuongCungUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTon"))) entity.LuongTon = reader.GetDecimal(reader.GetOrdinal("LuongTon"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongNhuCau"))) entity.TongNhuCau = reader.GetDecimal(reader.GetOrdinal("TongNhuCau"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("TuNgay"))) entity.TuNgay = reader.GetDateTime(reader.GetOrdinal("TuNgay"));
				if (!reader.IsDBNull(reader.GetOrdinal("DenNgay"))) entity.DenNgay = reader.GetDateTime(reader.GetOrdinal("DenNgay"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_BCXNT_NguyenPhuLieu VALUES(@STTHang, @HopDong_ID, @MaNPL, @TenNPL, @MaHS, @DVT, @LuongNhap, @LuongXuat, @LuongCungUng, @LuongTon, @TongNhuCau, @TrangThai, @TuNgay, @DenNgay)";
            string update = "UPDATE t_KDT_GC_BCXNT_NguyenPhuLieu SET STTHang = @STTHang, HopDong_ID = @HopDong_ID, MaNPL = @MaNPL, TenNPL = @TenNPL, MaHS = @MaHS, DVT = @DVT, LuongNhap = @LuongNhap, LuongXuat = @LuongXuat, LuongCungUng = @LuongCungUng, LuongTon = @LuongTon, TongNhuCau = @TongNhuCau, TrangThai = @TrangThai, TuNgay = @TuNgay, DenNgay = @DenNgay WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_BCXNT_NguyenPhuLieu WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.Char, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhap", SqlDbType.Decimal, "LuongNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuat", SqlDbType.Decimal, "LuongXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongCungUng", SqlDbType.Decimal, "LuongCungUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTon", SqlDbType.Decimal, "LuongTon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongNhuCau", SqlDbType.Decimal, "TongNhuCau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.Char, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhap", SqlDbType.Decimal, "LuongNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuat", SqlDbType.Decimal, "LuongXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongCungUng", SqlDbType.Decimal, "LuongCungUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTon", SqlDbType.Decimal, "LuongTon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongNhuCau", SqlDbType.Decimal, "TongNhuCau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_BCXNT_NguyenPhuLieu VALUES(@STTHang, @HopDong_ID, @MaNPL, @TenNPL, @MaHS, @DVT, @LuongNhap, @LuongXuat, @LuongCungUng, @LuongTon, @TongNhuCau, @TrangThai, @TuNgay, @DenNgay)";
            string update = "UPDATE t_KDT_GC_BCXNT_NguyenPhuLieu SET STTHang = @STTHang, HopDong_ID = @HopDong_ID, MaNPL = @MaNPL, TenNPL = @TenNPL, MaHS = @MaHS, DVT = @DVT, LuongNhap = @LuongNhap, LuongXuat = @LuongXuat, LuongCungUng = @LuongCungUng, LuongTon = @LuongTon, TongNhuCau = @TongNhuCau, TrangThai = @TrangThai, TuNgay = @TuNgay, DenNgay = @DenNgay WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_BCXNT_NguyenPhuLieu WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.Char, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongNhap", SqlDbType.Decimal, "LuongNhap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongXuat", SqlDbType.Decimal, "LuongXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongCungUng", SqlDbType.Decimal, "LuongCungUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTon", SqlDbType.Decimal, "LuongTon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongNhuCau", SqlDbType.Decimal, "TongNhuCau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@STTHang", SqlDbType.Int, "STTHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenNPL", SqlDbType.NVarChar, "TenNPL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.Char, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongNhap", SqlDbType.Decimal, "LuongNhap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongXuat", SqlDbType.Decimal, "LuongXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongCungUng", SqlDbType.Decimal, "LuongCungUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTon", SqlDbType.Decimal, "LuongTon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongNhuCau", SqlDbType.Decimal, "TongNhuCau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TuNgay", SqlDbType.DateTime, "TuNgay", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DenNgay", SqlDbType.DateTime, "DenNgay", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@HopDong_ID", SqlDbType.BigInt, "HopDong_ID", DataRowVersion.Current);
			db.AddInParameter(DeleteCommand, "@MaNPL", SqlDbType.VarChar, "MaNPL", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_BCXNT_NguyenPhuLieu Load(long hopDong_ID, string maNPL)
		{
			const string spName = "[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, maNPL);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_BCXNT_NguyenPhuLieu> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_BCXNT_NguyenPhuLieu> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_BCXNT_NguyenPhuLieu> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static List<KDT_GC_BCXNT_NguyenPhuLieu> SelectCollectionBy_HopDong_ID(long hopDong_ID)
		{
            IDataReader reader = SelectReaderBy_HopDong_ID(hopDong_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_HopDong_ID(long hopDong_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectBy_HopDong_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_HopDong_ID(long hopDong_ID)
		{
			const string spName = "p_KDT_GC_BCXNT_NguyenPhuLieu_SelectBy_HopDong_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertKDT_GC_BCXNT_NguyenPhuLieu(int sTTHang, long hopDong_ID, string maNPL, string tenNPL, string maHS, string dVT, decimal luongNhap, decimal luongXuat, decimal luongCungUng, decimal luongTon, decimal tongNhuCau, int trangThai, DateTime tuNgay, DateTime denNgay)
		{
			KDT_GC_BCXNT_NguyenPhuLieu entity = new KDT_GC_BCXNT_NguyenPhuLieu();	
			entity.STTHang = sTTHang;
			entity.HopDong_ID = hopDong_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT = dVT;
			entity.LuongNhap = luongNhap;
			entity.LuongXuat = luongXuat;
			entity.LuongCungUng = luongCungUng;
			entity.LuongTon = luongTon;
			entity.TongNhuCau = tongNhuCau;
			entity.TrangThai = trangThai;
			entity.TuNgay = tuNgay;
			entity.DenNgay = denNgay;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.Char, DVT);
			db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, LuongNhap);
			db.AddInParameter(dbCommand, "@LuongXuat", SqlDbType.Decimal, LuongXuat);
			db.AddInParameter(dbCommand, "@LuongCungUng", SqlDbType.Decimal, LuongCungUng);
			db.AddInParameter(dbCommand, "@LuongTon", SqlDbType.Decimal, LuongTon);
			db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, TongNhuCau);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay.Year <= 1753 ? DBNull.Value : (object) TuNgay);
			db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay.Year <= 1753 ? DBNull.Value : (object) DenNgay);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_BCXNT_NguyenPhuLieu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_BCXNT_NguyenPhuLieu item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_BCXNT_NguyenPhuLieu(int sTTHang, long hopDong_ID, string maNPL, string tenNPL, string maHS, string dVT, decimal luongNhap, decimal luongXuat, decimal luongCungUng, decimal luongTon, decimal tongNhuCau, int trangThai, DateTime tuNgay, DateTime denNgay)
		{
			KDT_GC_BCXNT_NguyenPhuLieu entity = new KDT_GC_BCXNT_NguyenPhuLieu();			
			entity.STTHang = sTTHang;
			entity.HopDong_ID = hopDong_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT = dVT;
			entity.LuongNhap = luongNhap;
			entity.LuongXuat = luongXuat;
			entity.LuongCungUng = luongCungUng;
			entity.LuongTon = luongTon;
			entity.TongNhuCau = tongNhuCau;
			entity.TrangThai = trangThai;
			entity.TuNgay = tuNgay;
			entity.DenNgay = denNgay;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_BCXNT_NguyenPhuLieu_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.Char, DVT);
			db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, LuongNhap);
			db.AddInParameter(dbCommand, "@LuongXuat", SqlDbType.Decimal, LuongXuat);
			db.AddInParameter(dbCommand, "@LuongCungUng", SqlDbType.Decimal, LuongCungUng);
			db.AddInParameter(dbCommand, "@LuongTon", SqlDbType.Decimal, LuongTon);
			db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, TongNhuCau);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay.Year <= 1753 ? DBNull.Value : (object) TuNgay);
			db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay.Year <= 1753 ? DBNull.Value : (object) DenNgay);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_BCXNT_NguyenPhuLieu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_BCXNT_NguyenPhuLieu item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_BCXNT_NguyenPhuLieu(int sTTHang, long hopDong_ID, string maNPL, string tenNPL, string maHS, string dVT, decimal luongNhap, decimal luongXuat, decimal luongCungUng, decimal luongTon, decimal tongNhuCau, int trangThai, DateTime tuNgay, DateTime denNgay)
		{
			KDT_GC_BCXNT_NguyenPhuLieu entity = new KDT_GC_BCXNT_NguyenPhuLieu();			
			entity.STTHang = sTTHang;
			entity.HopDong_ID = hopDong_ID;
			entity.MaNPL = maNPL;
			entity.TenNPL = tenNPL;
			entity.MaHS = maHS;
			entity.DVT = dVT;
			entity.LuongNhap = luongNhap;
			entity.LuongXuat = luongXuat;
			entity.LuongCungUng = luongCungUng;
			entity.LuongTon = luongTon;
			entity.TongNhuCau = tongNhuCau;
			entity.TrangThai = trangThai;
			entity.TuNgay = tuNgay;
			entity.DenNgay = denNgay;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, STTHang);
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, TenNPL);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.Char, DVT);
			db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, LuongNhap);
			db.AddInParameter(dbCommand, "@LuongXuat", SqlDbType.Decimal, LuongXuat);
			db.AddInParameter(dbCommand, "@LuongCungUng", SqlDbType.Decimal, LuongCungUng);
			db.AddInParameter(dbCommand, "@LuongTon", SqlDbType.Decimal, LuongTon);
			db.AddInParameter(dbCommand, "@TongNhuCau", SqlDbType.Decimal, TongNhuCau);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@TuNgay", SqlDbType.DateTime, TuNgay.Year <= 1753 ? DBNull.Value : (object) TuNgay);
			db.AddInParameter(dbCommand, "@DenNgay", SqlDbType.DateTime, DenNgay.Year <= 1753 ? DBNull.Value : (object) DenNgay);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_BCXNT_NguyenPhuLieu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_BCXNT_NguyenPhuLieu item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_BCXNT_NguyenPhuLieu(long hopDong_ID, string maNPL)
		{
			KDT_GC_BCXNT_NguyenPhuLieu entity = new KDT_GC_BCXNT_NguyenPhuLieu();
			entity.HopDong_ID = hopDong_ID;
			entity.MaNPL = maNPL;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, HopDong_ID);
			db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, MaNPL);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_HopDong_ID(long hopDong_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteBy_HopDong_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.BigInt, hopDong_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_BCXNT_NguyenPhuLieu_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_BCXNT_NguyenPhuLieu> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_BCXNT_NguyenPhuLieu item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion

	}	
}