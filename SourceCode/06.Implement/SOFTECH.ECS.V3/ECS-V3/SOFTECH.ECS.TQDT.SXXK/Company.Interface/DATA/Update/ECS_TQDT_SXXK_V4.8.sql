﻿/*
Run this script on:

        192.168.72.100.ECS_TQDT_SXXK_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_SXXK_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 09/19/2012 8:17:50 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[p_SXXK_HangMauDich_InsertUpdate]'
GO
    
------------------------------------------------------------------------------------------------------------------------            
-- Stored procedure name: [dbo].[p_SXXK_HangMauDich_InsertUpdate]            
-- Database: HaiQuan            
-- Author: Ngo Thanh Tung            
-- Time created: Tuesday, October 28, 2008            
------------------------------------------------------------------------------------------------------------------------            
            
ALTER PROCEDURE [dbo].[p_SXXK_HangMauDich_InsertUpdate]        
    @SoToKhai INT ,        
    @MaLoaiHinh CHAR(5) ,        
    @MaHaiQuan CHAR(6) ,        
    @NamDangKy SMALLINT ,        
    @SoThuTuHang SMALLINT ,        
    @MaHS VARCHAR(12) ,        
    @MaPhu VARCHAR(30) ,        
    @TenHang NVARCHAR(80) ,        
    @NuocXX_ID CHAR(3) ,        
    @DVT_ID CHAR(3) ,        
    @SoLuong NUMERIC(18, 5) ,        
    @DonGiaKB NUMERIC(38, 15) ,        
    @DonGiaTT NUMERIC(38, 15) ,        
    @TriGiaKB NUMERIC(38, 15) ,        
    @TriGiaTT NUMERIC(38, 15) ,        
    @TriGiaKB_VND NUMERIC(38, 15) ,        
    @ThueSuatXNK NUMERIC(5, 2) ,        
    @ThueSuatTTDB NUMERIC(5, 2) ,        
    @ThueSuatGTGT NUMERIC(5, 2) ,        
    @ThueXNK MONEY ,        
    @ThueTTDB MONEY ,        
    @ThueGTGT MONEY ,        
    @PhuThu MONEY ,        
    @TyLeThuKhac NUMERIC(5, 2) ,        
    @TriGiaThuKhac MONEY ,        
    @MienThue TINYINT        
AS         
    IF EXISTS ( SELECT  [SoToKhai] ,        
                        [MaLoaiHinh] ,        
                        [MaHaiQuan] ,        
                        [NamDangKy] ,        
                        [SoThuTuHang]        
                FROM    [dbo].[t_SXXK_HangMauDich]        
                WHERE   [SoToKhai] = @SoToKhai        
                        AND [MaLoaiHinh] = @MaLoaiHinh        
                        AND [MaHaiQuan] = @MaHaiQuan        
                        AND [NamDangKy] = @NamDangKy
                        AND SoThuTuHang = @SoThuTuHang        
                        AND MaPhu = @MaPhu )         
        BEGIN            
            UPDATE  [dbo].[t_SXXK_HangMauDich]        
            SET     [MaHS] = @MaHS ,        
                    [MaPhu] = @MaPhu ,        
                    [TenHang] = @TenHang ,        
                    [NuocXX_ID] = @NuocXX_ID ,        
                    [DVT_ID] = @DVT_ID ,        
                    [SoLuong] = @SoLuong ,        
                    [DonGiaKB] = @DonGiaKB ,        
                    [DonGiaTT] = @DonGiaTT ,        
                    [TriGiaKB] = @TriGiaKB ,        
                    [TriGiaTT] = @TriGiaTT ,        
                    [TriGiaKB_VND] = @TriGiaKB_VND ,        
                    [ThueSuatXNK] = @ThueSuatXNK ,        
                    [ThueSuatTTDB] = @ThueSuatTTDB ,        
                    [ThueSuatGTGT] = @ThueSuatGTGT ,        
                    [ThueXNK] = @ThueXNK ,        
                    [ThueTTDB] = @ThueTTDB ,        
                    [ThueGTGT] = @ThueGTGT ,        
                    [PhuThu] = @PhuThu ,        
                    [TyLeThuKhac] = @TyLeThuKhac ,        
                    [TriGiaThuKhac] = @TriGiaThuKhac ,        
                    [MienThue] = @MienThue        
            WHERE   [SoToKhai] = @SoToKhai        
                    AND [MaLoaiHinh] = @MaLoaiHinh        
                    AND [MaHaiQuan] = @MaHaiQuan        
                    AND [NamDangKy] = @NamDangKy 
                    AND SoThuTuHang = @SoThuTuHang        
                    AND MaPhu = @MaPhu        
        END            
    ELSE         
        BEGIN            
            INSERT  INTO [dbo].[t_SXXK_HangMauDich]        
                    ( [SoToKhai] ,        
                      [MaLoaiHinh] ,        
                      [MaHaiQuan] ,        
                      [NamDangKy] ,        
                      [SoThuTuHang] ,        
                      [MaHS] ,        
                      [MaPhu] ,        
                      [TenHang] ,        
                      [NuocXX_ID] ,        
     [DVT_ID] ,        
                      [SoLuong] ,        
                      [DonGiaKB] ,        
                      [DonGiaTT] ,        
                      [TriGiaKB] ,        
                   [TriGiaTT] ,        
                      [TriGiaKB_VND] ,        
                      [ThueSuatXNK] ,        
                      [ThueSuatTTDB] ,        
     [ThueSuatGTGT] ,        
                      [ThueXNK] ,        
                      [ThueTTDB] ,        
                      [ThueGTGT] ,        
                      [PhuThu] ,        
                      [TyLeThuKhac] ,        
                      [TriGiaThuKhac] ,        
                      [MienThue]            
                    )        
            VALUES  ( @SoToKhai ,        
                      @MaLoaiHinh ,        
                      @MaHaiQuan ,        
                      @NamDangKy ,        
                      @SoThuTuHang ,        
                      @MaHS ,        
                      @MaPhu ,        
                      @TenHang ,        
                      @NuocXX_ID ,        
                      @DVT_ID ,        
                      @SoLuong ,        
                      @DonGiaKB ,        
                      @DonGiaTT ,        
                      @TriGiaKB ,        
                      @TriGiaTT ,        
                      @TriGiaKB_VND ,        
                      @ThueSuatXNK ,        
                      @ThueSuatTTDB ,        
                      @ThueSuatGTGT ,        
                      @ThueXNK ,        
                      @ThueTTDB ,        
                      @ThueGTGT ,        
                      @PhuThu ,        
                      @TyLeThuKhac ,        
                      @TriGiaThuKhac ,        
                      @MienThue            
                    )             
        END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION

if( (select count(*) from dbo.t_HaiQuan_Version WHERE [Version] ='4.8') = 0)
	begin
		insert into dbo.t_HaiQuan_Version values('4.8', getdate(), N'Cập nhật cấu trúc dữ liệu.')
	end 
	
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO
