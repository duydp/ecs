﻿using System;
using System.Windows.Forms;
using System.Threading;
using System.Data;
using SQLDMO;
using System.Reflection;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;

namespace Company.Interface
{
    public partial class BackUpAndReStoreForm : BaseForm
    {
        public bool isBackUp = true;
        public static DateTime lastBackUp = new DateTime(1900, 01, 01);
        SQLDMO.BackupClass backupClass;
        //SQLDMO.RestoreClass restoreClass = new SQLDMO.RestoreClass();

        public BackUpAndReStoreForm()
        {
            InitializeComponent();
        }

        private void BackUpAndReStoreForm_Load(object sender, EventArgs e)
        {
            try
            {
                if (!GlobalSettings.PathBackup.Equals(""))
                    txtPath.Text = GlobalSettings.PathBackup.Trim();
                editBox1.Text =string.Format("{0}_{1}", GlobalSettings.DATABASE_NAME ,DateTime.Now.ToString("dd_MM_yyyy_[HH_mm].bak"));
                if (GlobalSettings.NGAYSAOLUU == "")
                    label3.Enabled = label4.Enabled = uiComboBox1.Enabled = false;

                string nhacNhoSaoLuu = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("NHAC_NHO_SAO_LUU");

                if (GlobalSettings.NGON_NGU == "0")
                {
                    if (isBackUp)
                    {
                        uiComboBox1.SelectedValue = "7";
                        //uiButton1.Text = "Sao lưu";
                    }
                    //else
                    //{
                    //    label2.Visible = label3.Visible = label4.Visible = uiComboBox1.Visible = false;
                    //    uiButton1.Text = "Phục hồi";
                    //}
                }
                else
                {
                    if (isBackUp)
                    {
                        uiComboBox1.SelectedValue = "7";
                        //uiButton1.Text = "Backup";
                    }
                    //else
                    //{
                    //    label2.Visible = label3.Visible = label4.Visible = uiComboBox1.Visible = false;
                    //    uiButton1.Text = "Restore";
                    //}

                }

                uiCheckBox1.Checked = nhacNhoSaoLuu != null || nhacNhoSaoLuu != "" ? true : false;

                uiCheckBox1_CheckedChanged(null, EventArgs.Empty);

                backupClass = new SQLDMO.BackupClass();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;



                if (string.IsNullOrEmpty(txtPath.Text) ||string.IsNullOrEmpty(editBox1.Text))
                    ShowMessage("Đường dẫn hoặc tên file không được để trống", false);
                else
                {


                    if (isBackUp)
                    {

                        using (SqlConnection cnn = new SqlConnection())
                        {
                            cnn.ConnectionString = string.Format("Server={0};Database={1};Uid={2};Pwd={3}", new object[] { GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS });
                            cnn.Open();
                            string cmdText = @"
			                                --Default backup database LanNT                                            
                                            --Create 10-05-2012
                                            BEGIN

                                            DECLARE @filedata  AS NVARCHAR(255)
                                            DECLARE @filename  AS NVARCHAR(255)
                                            DECLARE @dbName AS NVARCHAR(255)
                                           
                                            SET @dbName = '" + GlobalSettings.DATABASE_NAME + @"'
                                            SET @filename ='" +Path.Combine(txtPath.Text,editBox1.Text)+@"'		                                
                                            BACKUP DATABASE @dbName
                                            TO DISK =@filename
                                            WITH FORMAT,INIT,MEDIANAME = 'Z_SQLServerBackups',
                                            NAME = 'Full Backup of AdventureWorks',
                                            SKIP,NOREWIND,NOUNLOAD,STATS = 10;
                                            END
                                         ";
                            SqlCommand sqlCmd = new SqlCommand(cmdText);
                            sqlCmd.CommandType = CommandType.Text;
                            sqlCmd.Connection = cnn;
                            sqlCmd.ExecuteNonQuery();
                        }

                    }

                    if (uiCheckBox1.Checked)
                    {
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", DateTime.Today.ToString("dd/MM/yyyy"));
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", uiComboBox1.SelectedValue.ToString());
                    }
                    else
                    {
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", "");
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", "");
                    }

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("LastBackup", DateTime.Now.ToString("dd/MM/yyyy"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PathBackup", txtPath.Text.Trim());
                    this.Cursor = Cursors.Default;
                    ShowMessage("Sao lưu dữ liệu thành công.", false);
                    GlobalSettings.RefreshKey();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                string st = "";
                if (isBackUp)
                    st = "Lỗi khi sao lưu dữ liệu.";
                else
                    st = "Lỗi khi phục hồi dữ liệu.";
                ShowMessage(st + " " + ex.Message, false);

                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void editBox1_ButtonClick(object sender, EventArgs e)
        {

        }

        private void uiCheckBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (uiCheckBox1.Checked == false)
            {
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = false;
            }
            else
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = true;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            FrmFolderBrowse frm = new FrmFolderBrowse(GlobalSettings.USER, GlobalSettings.PASS, GlobalSettings.SERVER_NAME);
            frm.ShowDialog(this);
            txtPath.Text = FrmFolderBrowse.pathSelected;
        }

        private void uiCheckBox1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (uiCheckBox1.Checked == false)
            {
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = false;
            }
            else
                label3.Enabled = label4.Enabled = uiComboBox1.Enabled = true;
        }

        private void btnSaveConfig_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                if (uiCheckBox1.Checked)
                {
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", DateTime.Today.ToString("dd/MM/yyyy"));
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", uiComboBox1.SelectedValue.ToString());
                }
                else
                {
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NgaySaoLuu", "");
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("NHAC_NHO_SAO_LUU", "");
                }

                Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PathBackup", txtPath.Text.Trim());

                ShowMessage("Lưu cấu hình thành công.", false);
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

    }
}