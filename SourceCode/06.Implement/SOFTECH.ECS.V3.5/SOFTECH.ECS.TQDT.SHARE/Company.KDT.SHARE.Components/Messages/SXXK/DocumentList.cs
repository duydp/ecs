﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class DocumentList : DeclarationBase
    {

        [XmlElement("PreviousCustomsDocument")]
        public PreviousCustomsDocument PreviousCustomsDocument { get; set; }

        [XmlElement("DeclarationDocument")]
        public List<SXXK_ContractDocument> ContractDocuments { get; set; }

        [XmlElement("ImportDeclarationDocument")]
        public List<SXXK_ContractDocument> ImportDeclarationDocuments { get; set; }


        [XmlElement("ExportDeclarationDocument")]
        public List<SXXK_ContractDocument> ExportDeclarationDocument { get; set; }
    }
}
