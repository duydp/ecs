﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK;
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class ReportSanPham_TT128_2013 : DevExpress.XtraReports.UI.XtraReport
    {
        public SanPhamCollection SPCollection = new SanPhamCollection();
        private int STT = 0;
        public ReportSanPham_TT128_2013() 
        {
            InitializeComponent();
        }
        public void BindReportDinhMucDaDangKy()
        {
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            //lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            this.DataSource = this.SPCollection;
            lblMaSP.DataBindings.Add("Text", this.DataSource, "Ma");
            lblTenSP.DataBindings.Add("Text", this.DataSource, "Ten");
            lblMaHS.DataBindings.Add("Text", this.DataSource, "MaHS");
            xrLabel5.Text = xrLabel76.Text = Properties.Settings.Default.TieudeNgay;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }

        private void lblDVT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            lblDVT.Text = DonViTinh.GetName(GetCurrentColumnValue("DVT_ID"));
        }

    }
}
