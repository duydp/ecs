﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Collections.Generic;
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
#elif SXXK_V3 || SXXK_V4
using Company.BLL.KDT;
#elif GC_V3 || GC_V4
using Company.GC.BLL.KDT;

#endif
using System.Data;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.QuanLyChungTu;


//using Company.BLL.KDT;

namespace Company.Interface.Report.SXXK
{
    public partial class BangKe_HD_TheoTK : DevExpress.XtraReports.UI.XtraReport
    {        
        public string phieu;
        public string soTN ;
        public string ngayTN ;
        public string maHaiQuan;
        int STT = 0;
        public    ToKhaiMauDich tkmd = new ToKhaiMauDich();

        public BangKe_HD_TheoTK()
        {
            InitializeComponent();
        }
        public void BindReport_HopDong()
        {
            lblnguoikhaihq.Text = tkmd.TenDoanhNghiep.ToString();
            lblMaDoanhNghiep.Text = tkmd.MaDoanhNghiep.ToString();
            lblSoToKhai.Text = tkmd.SoToKhai.ToString();
            lblNgayKy.Text = tkmd.NgayDangKy.ToString("dd/MM/yyy");

            List<HopDongThuongMai> list_HD= new List<HopDongThuongMai>();
            list_HD = tkmd.HopDongThuongMaiCollection;
            List<BangKeHopDong> listBK = new List<BangKeHopDong>();
             foreach (HopDongThuongMai hd in list_HD)
            {
                BangKeHopDong Hopdong = new BangKeHopDong();
                Hopdong.SoHopDong = hd.SoHopDongTM;
                Hopdong.NgayHopDong = hd.NgayHopDongTM.ToString("dd/MM/yyy");
                Hopdong.GhiChu = hd.ThongTinKhac;
                decimal TongLuongHang = 0;
                foreach (HopDongThuongMaiDetail item in hd.ListHangMDOfHopDong)
                {
                    TongLuongHang = TongLuongHang + item.SoLuong;
                }
                Hopdong.TongLuongHang = TongLuongHang.ToString("N2");
                listBK.Add(Hopdong);
            }

           // DataTable dt = tkmd.HopDongThuongMaiCollection;
             this.DataSource = listBK;
            lblnguoikhaihq.Text = tkmd.TenDoanhNghiep;
            lblMaDoanhNghiep.Text = tkmd.MaDoanhNghiep;             
            if (tkmd.NgayDangKy.Year > 1900)
                lblNgayKy.Text = tkmd.NgayDangKy.ToString("dd/MM/yyy hh:mm:ss");
            else
                lblNgayKy.Text = "";
            if (tkmd.SoToKhai > 0)
                lblSoToKhai.Text = tkmd.SoToKhai + "";
            else
                lblSoToKhai.Text = "";

            lblSoHopDong.DataBindings.Add("Text", this.DataSource, "SoHopDong");
            lblNgay_HD.DataBindings.Add("Text", this.DataSource, "NgayHopDong");
            lblTongLuongHang.DataBindings.Add("Text", this.DataSource, "TongLuongHang", Company.KDT.SHARE.Components.Globals.FormatNumber(1));
            lblGhichu.DataBindings.Add("Text", this.DataSource, "GhiChu");
           // lblHieuKien.DataBindings.Add("", dt, ".SoHieu", "");
          //  lblHieuKien.DataBindings.Add("", dt, ".SoHieu", "");
        }
     

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + ""; 
        }

          
    }
    public class BangKeHopDong
    {
        public string SoHopDong{get;set;}
        public string NgayHopDong { get; set; }
        public string TongLuongHang { get; set; }
        public string GhiChu { get; set; }
    }
}
