﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;
using System.Reflection;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.IO;

namespace Company.Interface.GC
{
    public partial class UpdateNPLQuyetToan : BaseForm
    {
         public Company.GC.BLL.KDT.GC.HopDong HD;
        T_GC_NPL_QUYETOAN NPLQuyeToan = new T_GC_NPL_QUYETOAN();
        public List<T_GC_NPL_QUYETOAN> ListNPLQuyetToan = new List<T_GC_NPL_QUYETOAN>();
        public string MaNPL;
        public UpdateNPLQuyetToan()
        {
            try
            {
                InitializeComponent();
                this._DonViTinh = Company.KDT.SHARE.Components.DuLieuChuan.DonViTinh.SelectAll().Tables[0];
                txtDonViTinhNPL.ReadOnly = false;
                txtDonViTinhNPL.DataSource = this._DonViTinh;
                txtDonViTinhNPL.DisplayMember = "Ten";
                txtDonViTinhNPL.ValueMember = "ID";
                txtDonViTinhNPL.SelectedValue = GlobalSettings.DVT_MAC_DINH.PadRight(3);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void txtMaHangHoa_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                NguyenPhuLieuRegistedForm NPLRegistedForm = new NguyenPhuLieuRegistedForm();
                NPLRegistedForm.isBrower = true;
                NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = HD.ID;
                NPLRegistedForm.ShowDialog();
                if (!string.IsNullOrEmpty(NPLRegistedForm.NguyenPhuLieuSelected.Ma))
                {
                    txtMaHangHoa.Text = NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                    txtTenHang.Text = NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
        public void LoadData()
        {
            try
            {
                dgList.DataSource = T_GC_NPL_QUYETOAN.SelectCollectionDynamic("HOPDONG_ID =" + HD.ID, "MANPL");
                dgList.Refresh();
                dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            }
            catch (Exception ex)
            {
                dgList.Refresh();
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void txtMaHangHoa_Leave(object sender, EventArgs e)
        {
            try
            {
                NguyenPhuLieu NPL = new NguyenPhuLieu();
                NPL.HopDong_ID = HD.ID;
                NPL.Ma = txtMaHangHoa.Text.Trim();
                if (NPL.Load())
                {
                    txtMaHangHoa.Text = NPL.Ma;
                    txtTenHang.Text = NPL.Ten;
                    txtDonViTinhNPL.Text = DonViTinh_GetName(NPL.DVT_ID);
                }
                else
                {
                    txtTenHang.Text = String.Empty;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    isAddNew = false;
                    NPLQuyeToan = (T_GC_NPL_QUYETOAN)dgList.CurrentRow.DataRow;
                    txtMaHangHoa.Text = NPLQuyeToan.MANPL;
                    txtTenHang.Text = NPLQuyeToan.TENNPL;
                    txtDonViTinhNPL.Text = NPLQuyeToan.DVT;
                    txtSoLuong.Text = NPLQuyeToan.LUONGTONDK.ToString();
                    txtTriGia.Text = NPLQuyeToan.TRIGIATONDK.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        bool isAddNew = true;
        private void btnGhi_Click(object sender, EventArgs e)
        {
            try
            {
                NPLQuyeToan.MANPL = txtMaHangHoa.Text.ToString();
                NPLQuyeToan.TENNPL = txtTenHang.Text.ToString();
                NPLQuyeToan.DVT = txtDonViTinhNPL.Text.ToString();
                NPLQuyeToan.LUONGTONDK = Convert.ToDecimal(txtSoLuong.Text);
                NPLQuyeToan.TRIGIATONDK = Convert.ToDecimal(txtTriGia.Text);
                foreach (GridEXRow item in dgList.GetRows())
                {
                    if (item.Cells["MANPL"].Text == NPLQuyeToan.MANPL)
                    {
                        SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                        using (SqlConnection connection = (SqlConnection)db.CreateConnection())
                        {
                            connection.Open();
                            SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
                            try
                            {
                                isAddNew = false;
                                NPLQuyeToan.LUONGTONDK = Convert.ToDecimal(txtSoLuong.Text);
                                NPLQuyeToan.TRIGIATONDK = Convert.ToDecimal(txtTriGia.Text);
                                NPLQuyeToan.HOPDONG_ID = HD.ID;
                                NPLQuyeToan.UPDATE_TONDK(transaction);
                                transaction.Commit();
                            }
                            catch (Exception ex)
                            {
                                transaction.Rollback();
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                }
                LoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<T_GC_NPL_QUYETOAN> ItemColl = new List<T_GC_NPL_QUYETOAN>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((T_GC_NPL_QUYETOAN)i.GetRow().DataRow);
                        }

                    }
                    foreach (T_GC_NPL_QUYETOAN item in ItemColl)
                    {
                            ListNPLQuyetToan.Remove(item);
                    }
                     LoadData();
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;
                
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
           SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
           using (SqlConnection connection = (SqlConnection)db.CreateConnection())
           {
               connection.Open();
               SqlTransaction transaction = connection.BeginTransaction(IsolationLevel.RepeatableRead);
            try
            {
                foreach (T_GC_NPL_QUYETOAN item in ListNPLQuyetToan)
                {
                    MaNPL = item.MANPL;
                    item.UPDATE_TONDK(transaction );
                    transaction.Commit();
                }
            }
            catch (Exception ex)
            {
                StreamWriter write = File.AppendText("Error.txt");
                write.WriteLine("--------------------------------");
                write.WriteLine("Thời gian thực hiện : " + DateTime.Now.ToString());
                write.WriteLine(ex.StackTrace);
                write.WriteLine("Lỗi là : ");
                write.WriteLine(ex.Message);
                write.WriteLine("Lỗi tại Mã NPL : " + MaNPL.ToString());
                write.WriteLine("--------------------------------");
                write.Flush();
                write.Close();
                transaction.Rollback();
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
           }
        }

        private void UpdateNPLQuyetToan_Load(object sender, EventArgs e)
        {
            try
            {
            btnDelete.Visible = false;
            dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            LoadData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName ="NGUYÊN PHỤ LIỆU TỒN ĐẦU KỲ_" + HD.SoHopDong + "_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void mnuExportExcel_Click(object sender, EventArgs e)
        {
            ExportExcel(dgList);
        }

        private void btnReadExcel_Click(object sender, EventArgs e)
        {
            try
            {
                ReadExcelFormNPL f = new ReadExcelFormNPL();
                f.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
    }
}
