﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using System.Data;
using Company.BLL.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class rpCCK : DevExpress.XtraReports.UI.XtraReport
    {
        //private ToKhaiMauDich TKMD = null;

        public rpCCK(ToKhaiMauDich tkmd)
        {
            InitializeComponent();
            //lblNgay.Text = DateTime.Today.Day.ToString("00");
            //lblThang.Text = DateTime.Today.Month.ToString("00");
            //lblNam.Text = DateTime.Today.Year.ToString("0000");
            lblNgaythang.Text  = GlobalSettings.TieudeNgay;
            lbl3.Text = "- " + DonViHaiQuan.GetName(tkmd.MaHaiQuan).ToUpper();
            lbl5.Text = lbl5.Text.Replace("#", GlobalSettings.TEN_DON_VI);
            lbl7.Text = tkmd.SoVanDon;
            lbl8.Text = PhuongThucVanTai.getName(tkmd.PTVT_ID);
            lbl9.Text = tkmd.NgayVanDon.ToString("dd/MM/yyyy");
            lbl20.Text = lbl5.Text.Replace("#", GlobalSettings.TEN_DON_VI);
            lbl24.Text = DonViHaiQuan.GetName(tkmd.MaHaiQuan);
            lbl25.Text = lbl5.Text.Replace("#", GlobalSettings.TEN_DON_VI);
            lbl26.Text = tkmd.SoToKhai.ToString();
            lbl27.Text = tkmd.NgayDangKy.ToString("dd/MM/yyyy");            
            this.dispReport(tkmd);
        }

        private void dispReport(ToKhaiMauDich TKMD)
        { 
            TKMD.LoadHMDCollection();
            if (TKMD.HMDCollection.Count > 0)
            {
                HangMauDich hmd = TKMD.HMDCollection[0];
                lbl10.Text = hmd.TenHang;
                lbl11.Text = hmd.SoLuong.ToString() + " " + DonViTinh.GetName(hmd.DVT_ID);
                lbl12.Text = TKMD.TyGiaTinhThue.ToString("dd/MM/yyyy");
                lbl12.Text = TKMD.TrongLuong.ToString();
                decimal soCon = TKMD.SoContainer20 + TKMD.SoContainer40;
                lbl14.Text = soCon.ToString();
                lbl16.Text = TKMD.SoHieuPTVT;
                lbl19.Text = TKMD.TenDonViDoiTac;
            }
        }

        private void lbl_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            DoiNoiDung obj = new DoiNoiDung(lbl.Text);
            obj.ShowDialog();
            if (obj.NoiDungMoi != "")
            {
                lbl.Text = obj.NoiDungMoi.Replace(@"\n", "\n");
                this.CreateDocument();
            }
        }

        public void dispBackColor(bool val)
        {
            if (val)
            {
                lblNgaythang.BackColor = Color.Silver;
                //lblNgay.BackColor = Color.Silver;
                //lblThang.BackColor = Color.Silver;
                //lblNam.BackColor = Color.Silver;
                lbl1.BackColor = Color.Silver;
                lbl3.BackColor = Color.Silver;
                lbl4.BackColor = Color.Silver;
                lbl5.BackColor = Color.Silver;
                lbl6.BackColor = Color.Silver;
                lbl7.BackColor = Color.Silver;
                lbl8.BackColor = Color.Silver;
                lbl9.BackColor = Color.Silver;
                lbl10.BackColor = Color.Silver;
                lbl11.BackColor = Color.Silver;
                lbl12.BackColor = Color.Silver;
                lbl13.BackColor = Color.Silver;
                lbl14.BackColor = Color.Silver;
                lbl15.BackColor = Color.Silver;
                lbl16.BackColor = Color.Silver;
                lbl17.BackColor = Color.Silver;
                lbl18.BackColor = Color.Silver;
                lbl19.BackColor = Color.Silver;
                lbl20.BackColor = Color.Silver;
                lbl21.BackColor = Color.Silver;
                lbl22.BackColor = Color.Silver;
                lbl24.BackColor = Color.Silver;
                lbl25.BackColor = Color.Silver;
                lbl26.BackColor = Color.Silver;
                lbl27.BackColor = Color.Silver;
                lbl28.BackColor = Color.Silver;
            }
            else
            {
                lblNgaythang.BackColor = Color.Transparent;
                ////lblNgay.BackColor = Color.Transparent;
                ////lblThang.BackColor = Color.Transparent;
                //lblNam.BackColor = Color.Transparent;
                lbl1.BackColor = Color.Transparent;
                lbl3.BackColor = Color.Transparent;
                lbl4.BackColor = Color.Transparent;
                lbl5.BackColor = Color.Transparent;
                lbl6.BackColor = Color.Transparent;
                lbl7.BackColor = Color.Transparent;
                lbl8.BackColor = Color.Transparent;
                lbl9.BackColor = Color.Transparent;
                lbl10.BackColor = Color.Transparent;
                lbl11.BackColor = Color.Transparent;
                lbl12.BackColor = Color.Transparent;
                lbl13.BackColor = Color.Transparent;
                lbl14.BackColor = Color.Transparent;
                lbl15.BackColor = Color.Transparent;
                lbl16.BackColor = Color.Transparent;
                lbl17.BackColor = Color.Transparent;
                lbl18.BackColor = Color.Transparent;
                lbl19.BackColor = Color.Transparent;
                lbl20.BackColor = Color.Transparent;
                lbl21.BackColor = Color.Transparent;
                lbl22.BackColor = Color.Transparent;
                lbl24.BackColor = Color.Transparent;
                lbl25.BackColor = Color.Transparent;
                lbl26.BackColor = Color.Transparent;
                lbl27.BackColor = Color.Transparent;
                lbl28.BackColor = Color.Transparent;
            }
            this.CreateDocument();
        }
    }
}
