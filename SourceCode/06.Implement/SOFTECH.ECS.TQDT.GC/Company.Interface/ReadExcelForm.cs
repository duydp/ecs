﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.GC.BLL.KDT;
//using Company.GC.BLL.SXXK;
namespace Company.Interface
{
    public partial class ReadExcelForm : BaseForm
    {
        public ToKhaiMauDich TKMD;
        public Decimal TiGiaTT;
        public ReadExcelForm()
        {
            InitializeComponent();
        }

        private void editBox8_TextChanged(object sender, EventArgs e)
        {

        }
        //Hien thi Dialog box 
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }

        private int checkNPLExit(string maNPL)
        {
            for (int i = 0; i < this.TKMD.HMDCollection.Count; i++)
            {
                if (this.TKMD.HMDCollection[i].MaPhu.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;
        }


        private bool EmptyCheck(string st)
        {
            if (st == "") return true;
            return false;
        }
        private bool CheckNumber(decimal so)
        {
            if (so < 0) return true;
            return false;
        }
        private bool isSoLuongLimited(decimal so)
        {
            if (so < 0 || so > 10000000000000) return true;
            return false;
        }
        private bool isDonGiaLimited(decimal  so)
        {
            if (so < 0 || so > 10000000000000) return true;
            return false;
        }
        private bool CheckNameDVT(string varcheck)
        {
            DataTable dt = Company.GC.BLL.DuLieuChuan.DonViTinh.SelectAll();
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Equals(drcheck["Ten"].ToString()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;
        }
        private bool CheckNuocXX(string varcheck)
        {
            DataTable dt = Company.GC.BLL.DuLieuChuan.Nuoc.SelectAll();
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Equals(drcheck["Ten"].ToString()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;

        }
        private bool CheckNguyenTe(string varcheck)
        {
            DataTable dt = Company.GC.BLL.DuLieuChuan.NguyenTe.SelectAll();
            bool ok = false;
            foreach (DataRow drcheck in dt.Rows)
            {
                if (varcheck.Equals(drcheck["Ten"].ToString()))
                {
                    ok = true;
                    break;
                }
            }
            return ok;

        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                error.SetError(txtRow,setText("Dòng bắt đầu phải lớn hơn 0","This value must be greater than 0 "));
                error.SetIconPadding(txtRow, 8);
                return;

            }
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {

                ShowMessage(ex.Message+" ====", false);
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
                showMsg("MSG_EXC01", txtSheet.Text);
                //ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                return;
            }
            //editing :
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn = Convert.ToChar(txtMaHangColumn.Text.Trim());
            int maHangCol = ConvertCharToInt(maHangColumn);

            char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text.Trim());
            int tenHangCol = ConvertCharToInt(tenHangColumn);

            char maHSColumn = Convert.ToChar(txtMaHSColumn.Text.Trim());
            int maHSCol = ConvertCharToInt(maHSColumn);

            char dVTColumn = Convert.ToChar(txtDVTColumn.Text.Trim());
            int dVTCol = ConvertCharToInt(dVTColumn);

            char soLuongColumn = Convert.ToChar(txtSoLuongColumn.Text.Trim());
            int soLuongCol = ConvertCharToInt(soLuongColumn);

            char donGiaColumn = Convert.ToChar(txtDonGiaColumn.Text.Trim());
            int donGiaCol = ConvertCharToInt(donGiaColumn);

            char tsXNKColumn = Convert.ToChar(txtTSXNK.Text.Trim());
            int tsXNKCol = ConvertCharToInt(tsXNKColumn);


            char xuatXuColumn = Convert.ToChar(txtXuatXuColumn.Text.Trim());
            int xuatXuCol = ConvertCharToInt(xuatXuColumn);

            char triGiaTTColumn = Convert.ToChar(txtTGTT.Text);
            int triGiaTTCol = ConvertCharToInt(triGiaTTColumn);

         

            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        //NguyenPhuLieu npl = new NguyenPhuLieu();                       
                        HangMauDich hmd = new HangMauDich();
                        //EmptyCheck(Convert.ToString(wsr.Cells[TinhTrangCol].Value.ToString()))
                        //if (EmptyCheck(Convert.ToString(wsr.Cells[maHangCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[xuatXuCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[dVTCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[soLuongCol].Value)) || EmptyCheck(Convert.ToString(wsr.Cells[maHSCol].Value)) || isDonGiaLimited(Convert.ToDecimal(wsr.Cells[donGiaCol].Value)) || isSoLuongLimited(Convert.ToDecimal(wsr.Cells[soLuongCol].Value)) || CheckNuocXX(Convert.ToString(wsr.Cells[xuatXuCol].Value.ToString())) || CheckNameDVT(Convert.ToString(wsr.Cells[dVTCol].Value)))
                        //{
                        //    ShowMessage(" Dữ liệu tài dòng " + (wsr.Index + 1) + " sai, được bỏ qua", false);
                        //    continue;
                        //}
                        hmd.MaPhu = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        if (hmd.MaPhu.Trim() == "") continue;
                        if (TKMD.LoaiHangHoa == "N")
                        {
                            Company.GC.BLL.GC.NguyenPhuLieu npl = new Company.GC.BLL.GC.NguyenPhuLieu();
                            npl.Ma = hmd.MaPhu;
                            npl.HopDong_ID = TKMD.IDHopDong;
                            //temp
                            if (!npl.Load())
                            {
                                showMsg("MSG_240245", hmd.MaPhu);
                                //ShowMessage("Mặt hàng : " + hmd.TenHang + " chưa khai báo trong hợp đồng nên sẽ bị bỏ qua.", false);
                                continue;
                            }
                            hmd.TenHang = npl.Ten;
                            hmd.MaHS = npl.MaHS;
                            hmd.DVT_ID = npl.DVT_ID;
                        }
                        else if (TKMD.LoaiHangHoa == "S")
                        {
                            Company.GC.BLL.GC.SanPham sp = new Company.GC.BLL.GC.SanPham();
                            sp.Ma = hmd.MaPhu;
                            sp.HopDong_ID = TKMD.IDHopDong;
                            if (!sp.Load())
                            {
                                showMsg("MSG_240245", hmd.MaPhu);
                                //ShowMessage("Mặt hàng : " + hmd.TenHang + " chưa khai báo trong hợp đồng nên sẽ bị bỏ qua.", false);
                                continue;
                            }
                            hmd.TenHang = sp.Ten;
                            hmd.MaHS = sp.MaHS;
                            hmd.DVT_ID = sp.DVT_ID;
                        }
                        else if (TKMD.LoaiHangHoa == "T")
                        {
                            Company.GC.BLL.GC.ThietBi tb = new Company.GC.BLL.GC.ThietBi();
                            tb.Ma = hmd.MaPhu;
                            tb.HopDong_ID = TKMD.IDHopDong;
                            if (!tb.Load())
                            {
                                //ShowMessage("Mặt hàng : " + hmd.TenHang + " chưa khai báo trong hợp đồng nên sẽ bị bỏ qua.", false);
                                showMsg("MSG_240245", hmd.MaPhu);
                                continue;
                            }
                            hmd.TenHang = tb.Ten;
                            hmd.MaHS = tb.MaHS;
                            hmd.DVT_ID = tb.DVT_ID;
                        }
                        hmd.SoLuong = Convert.ToDecimal(wsr.Cells[soLuongCol].Value);
                        hmd.DonGiaKB = Convert.ToDecimal(wsr.Cells[donGiaCol].Value);                        
                        hmd.NuocXX_ID = Convert.ToString(wsr.Cells[xuatXuCol].Value).Trim();
                        hmd.DonGiaTT = hmd.DonGiaKB * TKMD.TyGiaTinhThue; ;
                        hmd.TriGiaKB = hmd.SoLuong * hmd.DonGiaKB;
                        hmd.TriGiaKB_VND = hmd.TriGiaKB * TKMD.TyGiaTinhThue;
                        hmd.TriGiaTT = Convert.ToDecimal(wsr.Cells[triGiaTTCol].Value);
                        hmd.ThueSuatXNK = Convert.ToDecimal(wsr.Cells[tsXNKCol].Value);
                        hmd.ThueXNK = hmd.TriGiaTT * hmd.ThueSuatXNK / 100;
                        int i = checkNPLExit(hmd.MaPhu);
                        if (i >= 0)
                        {
                            int dong = wsr.Index + 1;
                            //if (ShowMessage("Sản phẩm có mã \"" + hmd.MaPhu + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế sản phẩm trên lưới bằng sản phẩm này?", true) == "Yes")
                            if (showMsg("MSG_0203013", new String[] { hmd.MaPhu, dong.ToString()}, true) == "Yes")
                                this.TKMD.HMDCollection[i] = hmd;

                        }
                        else this.TKMD.HMDCollection.Add(hmd);
                        this.SaveDefault();

                    }
                    catch
                    {
                        //if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                        if (showMsg("MSG_0203014", wsr.Index + 1, true) == "Yes")
                        {
                            this.TKMD.HMDCollection.Clear();
                            return;
                        }
                    }
                }
            }
            this.Close();

        }
        private void SaveDefault()
        {
            DocExcel docExcel = new DocExcel();
            docExcel.insertThongSoHangTK(txtSheet.Text.Trim(),
                                                                  txtRow.Text.Trim(),
                                                                  txtMaHangColumn.Text.Trim(),
                                                                  txtTenHangColumn.Text.Trim(),
                                                                  txtMaHSColumn.Text.Trim(),
                                                                  txtDVTColumn.Text.Trim(),
                                                                  txtSoLuongColumn.Text.Trim(),
                                                                  txtDonGiaColumn.Text.Trim(),
                                                                  txtTSXNK.Text.Trim(),
                                                                  txtXuatXuColumn.Text.Trim());

        }
        private void ReadDefault()
        {
            DocExcel docExcel = new DocExcel();
            docExcel.ReadDefault(txtSheet,
                                                                  txtRow,
                                                                  txtMaHangColumn,
                                                                  txtTenHangColumn,
                                                                  txtMaHSColumn,
                                                                  txtDVTColumn,
                                                                  txtSoLuongColumn,
                                                                  txtDonGiaColumn,
                                                                  txtTSXNK,
                                                                  txtXuatXuColumn);
        }

        private void ReadExcelForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
            this.ReadDefault();
        }
    }
}