﻿namespace Install
{
    using ICSharpCode.SharpZipLib.Zip;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Configuration.Install;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Security.AccessControl;
    using System.Security.Principal;
    using System.Windows.Forms;

    [RunInstaller(true)]
    public class NacInstaller : Installer
    {
        private const string APPLICATION = "Application";
        private IContainer components;
        private List<FileSystemInfo> deleteList;
        private const string EXE_ZIP_FILENAME = @"Application\Install\Application.zip";
        private const int HWND_BOTTOM = 1;
        private const int HWND_NOTOPMOST = -2;
        private const int HWND_TOP = 0;
        private const int HWND_TOPMOST = -1;
        private const string IDENTITY = "everyone";
        private const string INSTALL = "Install";
        private const string INSTALLER_DLL_FILENAME = "InstallAction.dll";
        private const string LAUNCHER_EXE_FILENAME = "NaccsLauncher.exe";
        private const string LAUNCHER_EXE_XML_FILENAME = "NaccsLauncher.XmlSerializers.dll";
        private const string NACCS_TEMPLATE_ZIP_FILENAME = @"Application\Install\Template.zip";
        private const string NET_DLL_FILENAME = "Naccs.Net.dll";
        private const string NETHTTP_DLL_FILENAME = "NaccsNetHttp.dll";
        private const string ZIP_LIB_DLL_FILENAME = "ICSharpCode.SharpZipLib.dll";

        public NacInstaller()
        {
            this.InitializeComponent();
        }

        private void AddDeleteList(DirectoryInfo di)
        {
            foreach (DirectoryInfo info in di.GetDirectories())
            {
                this.AddDeleteList(info);
            }
            this.deleteList.AddRange(di.GetFiles());
            this.deleteList.Add(di);
        }

        private void AddDeleteList(string dir)
        {
            if (!string.IsNullOrEmpty(dir))
            {
                DirectoryInfo info = new DirectoryInfo(dir);
                if (info.Exists)
                {
                    foreach (DirectoryInfo info2 in info.GetDirectories())
                    {
                        this.AddDeleteList(info2);
                    }
                    foreach (FileInfo info3 in info.GetFiles())
                    {
                        if (info3.Exists && this.IsInstallFile(info3))
                        {
                            this.deleteList.Add(info3);
                        }
                    }
                }
            }
        }

        public override void Commit(IDictionary savedState)
        {
            base.Commit(savedState);
            string dirPath = base.Context.Parameters["dir"];
            this.DirectorySecurityChange(dirPath);
            this.ExtractFiles(dirPath, @"Application\Install\Application.zip", "Decompressing ... Application");
            this.ExtractFiles(dirPath, @"Application\Install\Template.zip", "Decompressing ... VNACCS Template");
        }

        private void DeleteFiles(string dir)
        {
            try
            {
                this.deleteList = new List<FileSystemInfo>();
                using (ProgressDialog dialog = new ProgressDialog())
                {
                    dialog.Message = "Searching ...";
                    dialog.TopMost = true;
                    dialog.Show();
                    this.AddDeleteList(dir);
                    dialog.Message = "Deleting ...";
                    dialog.Minimum = 0L;
                    dialog.Maximum = this.deleteList.Count;
                    dialog.Refresh();
                    foreach (FileSystemInfo info in this.deleteList)
                    {
                        try
                        {
                            if (((info.Attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) || ((info.Attributes & FileAttributes.Hidden) == FileAttributes.Hidden))
                            {
                                info.Attributes = FileAttributes.Normal;
                            }
                            if ("InstallAction.dll".Equals(info.Name) || (("Application".Equals(info.Name) || "Install".Equals(info.Name)) && info.Attributes.Equals(FileAttributes.Directory)))
                            {
                                continue;
                            }
                            info.Delete();
                        }
                        catch (FileNotFoundException exception)
                        {
                            MessageBox.Show("Deleting error : File" + Environment.NewLine + exception.Message + Environment.NewLine + exception.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                        catch (DirectoryNotFoundException exception2)
                        {
                            MessageBox.Show("Deleting error : Directory" + Environment.NewLine + exception2.Message + Environment.NewLine + exception2.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                        catch (Exception exception3)
                        {
                            MessageBox.Show("Deleting error : File or Directory" + Environment.NewLine + exception3.Message + Environment.NewLine + exception3.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                        dialog.FileName = "Deleted : " + info.Name;
                        dialog.Value += 1L;
                        dialog.Refresh();
                        Application.DoEvents();
                    }
                }
            }
            catch (Exception exception4)
            {
                MessageBox.Show("File deleting error" + Environment.NewLine + exception4.Message + Environment.NewLine + exception4.StackTrace, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
            finally
            {
                this.deleteList.Clear();
                this.deleteList = null;
            }
        }

        private void DirectorySecurityChange(string dirPath)
        {
            DirectoryInfo info = new DirectoryInfo(dirPath);
            try
            {
                DirectorySecurity accessControl = info.GetAccessControl();
                foreach (FileSystemAccessRule rule in accessControl.GetAccessRules(true, true, typeof(NTAccount)))
                {
                    if ("everyone".ToUpper() == rule.IdentityReference.ToString().ToUpper())
                    {
                        return;
                    }
                }
                accessControl.AddAccessRule(new FileSystemAccessRule("everyone", FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
                info.SetAccessControl(accessControl);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void ExtractFiles(string dir, string archive, string message)
        {
            string path = Path.Combine(dir, archive);
            if (!File.Exists(path))
            {
                MessageBox.Show("The file could not be found." + Environment.NewLine + path, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                throw new InstallException("The file could not be found." + Environment.NewLine + path);
            }
            try
            {
                using (ProgressDialog dialog = new ProgressDialog())
                {
                    using (ZipFile file = new ZipFile(File.OpenRead(path)))
                    {
                        dialog.Maximum = file.Count;
                    }
                    dialog.Message = message;
                    dialog.TopMost = true;
                    dialog.Show();
                    SetWindowPos(dialog.Handle, -1, 0, 0, 0, 0, SWP.SHOWWINDOW | SWP.NOMOVE | SWP.NOSIZE);
                    using (ZipInputStream stream = new ZipInputStream(File.OpenRead(path)))
                    {
                        ZipEntry entry;
                        while ((entry = stream.GetNextEntry()) != null)
                        {
                            string directoryName = Path.GetDirectoryName(entry.Name);
                            string fileName = Path.GetFileName(entry.Name);
                            directoryName = Path.Combine(dir, directoryName);
                            if ((directoryName.Length > 0) && !Directory.Exists(directoryName))
                            {
                                Directory.CreateDirectory(directoryName);
                            }
                            if (!(fileName != string.Empty))
                            {
                                goto Label_016C;
                            }
                            fileName = Path.Combine(dir, entry.Name);
                            using (FileStream stream2 = File.Create(fileName))
                            {
                                int count = 0x800;
                                byte[] buffer = new byte[0x800];
                                while (true)
                                {
                                    count = stream.Read(buffer, 0, buffer.Length);
                                    if (count <= 0)
                                    {
                                        goto Label_0150;
                                    }
                                    stream2.Write(buffer, 0, count);
                                }
                            }
                        Label_0150:
                            File.SetCreationTime(fileName, entry.DateTime);
                            File.SetLastWriteTime(fileName, entry.DateTime);
                        Label_016C:
                            dialog.FileName = "Installing ..." + entry.Name;
                            dialog.Value += 1L;
                            dialog.Refresh();
                            Application.DoEvents();
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                throw new InstallException("Decompressing error : File" + Environment.NewLine + path, exception);
            }
            finally
            {
                Console.WriteLine("Installer.ExtractFiles end");
            }
        }

        private void InitializeComponent()
        {
            this.components = new Container();
        }

        public override void Install(IDictionary stateSaver)
        {
            base.Install(stateSaver);
        }

        private bool IsInstallFile(FileInfo fileInfo)
        {
            return ((((string.Compare(fileInfo.Name, "ICSharpCode.SharpZipLib.dll", true) != 0) && (string.Compare(fileInfo.Name, "NaccsLauncher.exe", true) != 0)) && ((string.Compare(fileInfo.Name, "NaccsLauncher.XmlSerializers.dll", true) != 0) && (string.Compare(fileInfo.Name, "Naccs.Net.dll", true) != 0))) && ((string.Compare(fileInfo.Name, "NaccsNetHttp.dll", true) != 0) && (string.Compare(fileInfo.Name, "InstallAction.dll", true) != 0)));
        }

        public override void Rollback(IDictionary savedState)
        {
            base.Rollback(savedState);
        }

        [DllImport("user32.dll")]
        private static extern bool SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int X, int Y, int cx, int cy, SWP flags);
        public override void Uninstall(IDictionary savedState)
        {
            Form form = null;
            try
            {
                base.Uninstall(savedState);
                string dir = base.Context.Parameters["dir"];
                this.DeleteFiles(dir);
                form = new Form();
                form.Opacity = 0.0;
                form.ControlBox = false;
                form.StartPosition = FormStartPosition.CenterScreen;
                form.Show();
                form.TopMost = true;
                MessageBox.Show("uninstall completed successfully", "Private Terminal Software", MessageBoxButtons.OK);
            }
            catch
            {
                MessageBox.Show("uninstall failed", "Private Terminal Software", MessageBoxButtons.OK);
            }
            finally
            {
                if (form != null)
                {
                    form.Dispose();
                    form = null;
                }
            }
        }

        private enum SWP
        {
            FRAMECHANGED = 0x20,
            HIDEWINDOW = 0x80,
            NOACTIVATE = 0x10,
            NOCOPYBITS = 0x100,
            NOMOVE = 2,
            NOOWNERZORDER = 0x200,
            NOREDRAW = 8,
            NOSENDCHANGING = 0x4000,
            NOSIZE = 1,
            NOZORDER = 4,
            SHOWWINDOW = 0x40
        }
    }
}

