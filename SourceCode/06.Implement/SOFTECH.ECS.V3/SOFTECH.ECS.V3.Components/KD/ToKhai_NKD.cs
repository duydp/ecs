﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using SOFTECH.ECS.V3.Components.SHARE;
using SOFTECH.ECS.V3.Components.KD;
using System.IO;
using Company.KDT.SHARE.Components.Nodes.KD;
using SOFTECH.ECS.V3.Components.KD.SHARE;

// SiHV
namespace SOFTECH.ECS.V3.Components.KD
{
    public partial class ToKhai_NKD
    {
        public static string CofigToKhaiNKD(string pathToKhaiNKD, long TKMD_ID, int issuer, int function, int status, int declarationOffice)
        {
            ToKhaiMauDich tkmd = ToKhaiMauDich.Load(TKMD_ID);
            XDocument docXML = XDocument.Load("XML/KD/ToKhaiNhapKhau.xml");
            if (docXML != null && tkmd != null)
            {
                bool AttachDocuments = false;
                XElement Root = docXML.Root;
                if (Root != null && Root.Elements().Count() > 0)
                {
                    Root = ProcessXML.AddVouchers(Root, tkmd, issuer, function, status, declarationOffice);
                    foreach (XElement element in Root.Elements())
                    {
                        //CurrencyExchange
                        if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.CurrencyExchange.ToUpper()))
                            ProcessXML.SetValueCurrencyExchange(element, tkmd);
                        //DeclarationPackaging
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.DeclarationPackaging.ToUpper()))
                            ProcessXML.SetValueDeclarationPackaging(element, tkmd);
                        //Invoice
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.Invoice.ToUpper()))
                            ProcessXML.SetValueInvoice(element, tkmd);
                        //RepresentativePerson
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.RepresentativePerson.ToUpper()))
                            ProcessXML.SetValueRepresentativePerson(element, tkmd);
                        //AdditionalDocument
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.AdditionalDocument.ToUpper()))
                        {
                            ProcessXML.SetValue_ToKhai_HopDongOrVanDon(element, tkmd);
                        }
                        //GoodsShipment
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.GoodsShipment.ToUpper()))
                        {
                            ProcessXML.SetValue_GoodsShipment(element, tkmd, true);
                        }
                        // Giay phep
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.Licenses.ToUpper()))
                        {
                            string gp = GiayPhep.ConfigGiayPhep(tkmd, Message_Types.Giay_phep_nhap_khau, Message_Functions.Khai_bao, status, declarationOffice);
                            if (!string.IsNullOrEmpty(gp))
                            {
                                XDocument xdoc = XDocument.Load(new StringReader(gp));
                                XElement gpElement = xdoc.Root.Element(NodeToKhai_NKD.Licenses);
                                if (gpElement != null)
                                {
                                    element.Add(gpElement.Elements());
                                }
                            }
                        }
                        //Hop Dong
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.ContractDocuments.ToUpper()))
                        {
                            string hd = HopDongThuongMai.ConfigHopDongThuongMai(tkmd, Message_Types.Hop_Dong, Message_Functions.Khai_bao, status, declarationOffice);
                            if (!string.IsNullOrEmpty(hd))
                            {
                                XDocument xdoc = XDocument.Load(new StringReader(hd));
                                XElement hdElement = xdoc.Root.Element(NodeToKhai_NKD.ContractDocuments);
                                if (hdElement != null)
                                {
                                    element.Add(hdElement.Elements());
                                }
                            }
                        }
                        //Hoa Don Thuong Mai
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.CommercialInvoices.ToUpper()))
                        {
                            string hdtm = HoaDonThuongMai.ConfigHDThuongMai(tkmd, Message_Types.Hoa_don_thuong_mai, Message_Functions.Khai_bao, status, declarationOffice);
                            if (!string.IsNullOrEmpty(hdtm))
                            {
                                XDocument xdoc = XDocument.Load(new StringReader(hdtm));
                                XElement hdtmElement = xdoc.Root.Element(NodeToKhai_NKD.CommercialInvoices);
                                if (hdtmElement != null)
                                {
                                    element.Add(hdtmElement.Elements());
                                }
                            }
                        }
                        //CO
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.CertificateOfOrigins.ToUpper()))
                        {
                            string co = CO.ConfigCO(tkmd, issuer, Message_Functions.Khai_bao, status, declarationOffice);
                            if (!string.IsNullOrEmpty(co))
                            {
                                XDocument xdoc = XDocument.Load(new StringReader(co));
                                XElement coElement = xdoc.Root.Element(NodeToKhai_NKD.CertificateOfOrigins);
                                if (coElement != null)
                                {
                                    element.Add(coElement.Elements());
                                }
                            }
                        }
                        //Danh sách vận đơn đi kèm
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.BillOfLadings.ToUpper()))
                        {
                            string vd = VanDon.ConfigVanDon(tkmd, issuer, function, status, declarationOffice);
                            XDocument xdoc = XDocument.Load(new StringReader(vd));
                            XElement coElement = xdoc.Root.Element(NodeToKhai_NKD.BillOfLadings);
                            if (coElement != null)
                            {
                                element.Add(coElement.Elements());
                            }
                        }
                        //Danh sách chứng từ đính kèm, có thể lặp lại
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.AttachDocuments.ToUpper()))
                        {
                            if (AttachDocuments == false)
                            {
                                IList<ChungTuKem> lCTK = ChungTuKem.SelectCollectionBy_TKMDID(tkmd.ID);
                                if (lCTK != null && lCTK.Count > 0)
                                {
                                    if (lCTK.Count == 1)
                                    {
                                        foreach (XElement child in element.Elements())
                                        {
                                            if (child.Name.ToString().ToUpper().Equals(NodeName.AttachDocumentItem.ToUpper()))
                                            {
                                                ProcessXML.SetValueAttachDocumentItem(child, lCTK[0], 1);
                                            }
                                        }
                                    }
                                    else if (lCTK.Count > 1)
                                    {
                                        XElement TempAttachDocumentItem = new XElement(element.Element(NodeName.AttachDocumentItem));
                                        foreach (XElement child in element.Elements())
                                        {
                                            if (child.Name.ToString().ToUpper().Equals(NodeName.AttachDocumentItem.ToUpper()))
                                            {
                                                //TempAttachDocumentItem = child;
                                                ProcessXML.SetValueAttachDocumentItem(child, lCTK[0], 1);
                                            }
                                        }
                                        for (int i = 1; i < lCTK.Count; i++)
                                        {
                                            XElement NewAttachDocumentItem = new XElement(TempAttachDocumentItem);
                                            element.Add(ProcessXML.SetValueAttachDocumentItem(NewAttachDocumentItem, lCTK[i], i + 1));
                                        }
                                    }
                                }
                            }
                            AttachDocuments = true;
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.PreviousCustomsprocedure.ToUpper()))
                        {
                            // Chứng từ hải quan trước đó	0	none	
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.issue.ToUpper()))
                                    child.SetValue("");     // an10	YYYY-MM-DD
                                if (child.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.reference.ToUpper()))
                                    child.SetValue("");     // 0	an..35	
                                if (child.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.type.ToUpper()))
                                    child.SetValue("");     //0	an..3	
                                if (child.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.measureUnit.ToUpper()))
                                    child.SetValue("");     //Đơn vị tính hàng quản lý chuyên ngành	1	an..3	VD: Đơn vị tính HTS, …
                                if (child.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.unitPrice.ToUpper()))
                                    child.SetValue("");     // Đơn giá hàng quản lý chuyên ngành	1	n..16,2	VD: Đơn giá HTS, …
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.CustomsProcedure.ToUpper()))
                        {
                            // Thủ tục hải quan trước đó	0	none		
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.current.ToUpper()))
                                    child.SetValue("");     // ??????????????
                                if (child.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.previous.ToUpper()))
                                    child.SetValue("");     // ???????????????
                            }
                        }
                        else if (element.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.ExaminationPlace.ToUpper()))
                        {
                            // Địa điểm kiểm tra	0	none	
                            foreach (XElement child in element.Elements())
                            {
                                if (child.Name.ToString().ToUpper().Equals(NodeToKhai_NKD.code.ToUpper()))
                                    child.SetValue("");     // ??????????????
                            }
                        }
                    }
                }
            }
            //docXML.Save(@"D:\NewToKhaiNKD.xml");
            //docXML.Save(@"../../../SOFTECH.ECS.V3.Components/XML/LoadKD/NewToKhaiNKD.xml");
            return docXML.ToString();
        }
    }
}
