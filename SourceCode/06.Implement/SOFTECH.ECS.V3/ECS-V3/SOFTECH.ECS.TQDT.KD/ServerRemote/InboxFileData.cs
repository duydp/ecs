﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;

namespace ServerRemote
{
    class InboxFileData
    {
        private static string m_ConnectionString = "Server=" + Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ServerName").ToString() + ";Database=" + Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("DATABASE_NAME").ToString() + ";user=" + Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("user").ToString() + ";pwd=" + Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("pass").ToString();//"Server=192.168.72.151\\SQLSERVER;Database=ECS_SIGNREMOTE;User ID=sa;pwd=123456";
        private SqlConnection m_sqlConn = null;

        public delegate void NewMessage();
        public event NewMessage OnNewMessage;

        /// <summary>
        /// Constructor
        /// </summary>
        public InboxFileData()
        {
            // Stop an existing services on this connection string
            // just be sure
            SqlDependency.Stop(m_ConnectionString);

            // Start the dependency
            // User must have SUBSCRIBE QUERY NOTIFICATIONS permission
            // Database must also have SSB enabled
            // ALTER DATABASE Chatter SET ENABLE_BROKER
            SqlDependency.Start(m_ConnectionString);

            // Create the connection
            m_sqlConn = new SqlConnection(m_ConnectionString);
        }

        /// <summary>
        /// Destructor
        /// </summary>
        ~InboxFileData()
        {
            // Stop the dependency before exiting
            SqlDependency.Stop(m_ConnectionString);
        }

        /// <summary>
        /// Retreive messages from database
        /// </summary>
        /// <returns></returns>
        public DataTable GetInboxMessages()
        {
            DataTable dt = new DataTable();

            try
            {
                SqlCommand cmd = new SqlCommand("GetFileMessages", m_sqlConn);
                cmd.CommandType = CommandType.StoredProcedure;

                // Clear any existing notifications
                cmd.Notification = null;

                // Create the dependency for this command
                SqlDependency dependency = new SqlDependency(cmd);

                // Add the event handler
                dependency.OnChange += new OnChangeEventHandler(OnChange);

                // Open the connection if necessary
                if (m_sqlConn.State == ConnectionState.Closed)
                    m_sqlConn.Open();

                // Get the messages
                dt.Load(cmd.ExecuteReader(CommandBehavior.CloseConnection));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return dt;
        }

        /// <summary>
        /// Handler for the SqlDependency OnChange event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnChange(object sender, SqlNotificationEventArgs e)
        {
            SqlDependency dependency = sender as SqlDependency;

            // Notices are only a one shot deal
            // so remove the existing one so a new 
            // one can be added
            dependency.OnChange -= OnChange;

            // Fire the event
            if (OnNewMessage != null)
            {
                OnNewMessage();
            }
        }
    }
}
