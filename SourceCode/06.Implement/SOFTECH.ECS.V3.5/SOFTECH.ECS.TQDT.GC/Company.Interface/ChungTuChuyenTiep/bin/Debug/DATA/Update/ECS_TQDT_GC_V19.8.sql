-- =============================================  
-- Author:  Phiph 
-- Create date:   
-- Description:   
-- =============================================  
  
Alter PROCEDURE [dbo].[p_GC_BC07HSTK_GC_TT117]   
 -- Add the parameters for the stored procedure here  
 @HD_ID BIGINT,  
 @MaHaiQuan CHAR(6),  
 @MaDoanhNghiep VARCHAR(14)   
   
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;    
  --  ROW_NUMBER() OVER (ORDER BY t_GC_NguyenPhuLieu.Ten) AS 'STT',  
    
--DECLARE @HD_ID BIGINT; DECLARE @MaHaiQuan CHAR(6); DECLARE @MaDoanhNghiep VARCHAR(14)    
--SET @HD_ID=492; SET @MaDoanhNghiep='0400101556'; SET @MaHaiQuan='C34C';  
   
 -- LanNT 11HT-MC2010  
--Tập hợp những mã loại hình  nhập và xuất tờ khai  
--Tờ khai nhập  
  
SELECT  ROW_NUMBER() OVER (ORDER BY ThietBi.TenHang) AS 'STT',   ThietBi.TenHang, t_HaiQuan_DonViTinh.Ten AS DVT, ThietBi.SoLuongTamNhap, ThietBi.SoLuongTaiXuat,   
                      SUM(ABS(ThietBi.SoLuongTamNhap - ThietBi.SoLuongTaiXuat)) AS ConLai  
FROM         (SELECT     t_KDT_HangMauDich.ID, t_KDT_HangMauDich.TKMD_ID AS ID_ToKhai, t_KDT_HangMauDich.MaPhu AS MaHang, t_KDT_HangMauDich.TenHang,   
                                              t_KDT_HangMauDich.DVT_ID AS DVT, t_KDT_ToKhaiMauDich.SoToKhai, t_KDT_ToKhaiMauDich.NgayDangKy,   
                                              t_KDT_HangMauDich.SoLuong AS SoLuongTamNhap, 0 AS SoLuongTaiXuat, t_KDT_ToKhaiMauDich.MaLoaiHinh, t_KDT_ToKhaiMauDich.MaHaiQuan,   
                                              t_KDT_ToKhaiMauDich.MaDoanhNghiep  
                       FROM          t_KDT_HangMauDich INNER JOIN  
                                              t_KDT_ToKhaiMauDich ON t_KDT_HangMauDich.TKMD_ID = t_KDT_ToKhaiMauDich.ID  
                       WHERE      (t_KDT_ToKhaiMauDich.LoaiHangHoa = 'T') AND (t_KDT_ToKhaiMauDich.IDHopDong = @HD_ID) AND (t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%NGC%'or t_KDT_ToKhaiMauDich.MaLoaiHinh LIKE '%NV%')  
                       UNION  
                       SELECT     t_KDT_GC_HangChuyenTiep.ID, t_KDT_GC_HangChuyenTiep.Master_ID AS ID_ToKhai, t_KDT_GC_HangChuyenTiep.MaHang,   
                                             t_KDT_GC_HangChuyenTiep.TenHang, t_KDT_GC_HangChuyenTiep.ID_DVT AS DVT, t_KDT_GC_ToKhaiChuyenTiep.SoToKhai,   
                                             t_KDT_GC_ToKhaiChuyenTiep.NgayDangKy, 0 AS SoLuongTamNhap, t_KDT_GC_HangChuyenTiep.SoLuong AS SoLuongTaiXuat,   
                                             t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh, t_KDT_GC_ToKhaiChuyenTiep.MaHaiQuanTiepNhan AS MaHaiQuan,   
                                             t_KDT_GC_ToKhaiChuyenTiep.MaDoanhNghiep  
                       FROM         t_KDT_GC_ToKhaiChuyenTiep INNER JOIN  
                                             t_KDT_GC_HangChuyenTiep ON t_KDT_GC_ToKhaiChuyenTiep.ID = t_KDT_GC_HangChuyenTiep.Master_ID  
                       WHERE     (t_KDT_GC_ToKhaiChuyenTiep.IDHopDong = @HD_ID) AND (t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%TBN%' OR  
                                             t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%NGC20%' OR  
                                             t_KDT_GC_ToKhaiChuyenTiep.MaLoaiHinh LIKE N'%XGC20%')OR (t_KDT_GC_ToKhaiChuyenTiep.LoaiHangHoa='T')) AS ThietBi INNER JOIN  
                      t_HaiQuan_DonViTinh ON ThietBi.DVT = t_HaiQuan_DonViTinh.ID  
GROUP BY ThietBi.TenHang, t_HaiQuan_DonViTinh.Ten, ThietBi.SoLuongTamNhap, ThietBi.SoLuongTaiXuat, ThietBi.MaHaiQuan, ThietBi.MaDoanhNghiep  
HAVING      (ThietBi.MaHaiQuan = @MaHaiQuan) AND (ThietBi.MaDoanhNghiep = @MaDoanhNghiep)  
ORDER BY ThietBi.TenHang  
  
END  

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.8') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.8',GETDATE(), N' Cập nhật store Bao cáo 07_TT113')
END	
