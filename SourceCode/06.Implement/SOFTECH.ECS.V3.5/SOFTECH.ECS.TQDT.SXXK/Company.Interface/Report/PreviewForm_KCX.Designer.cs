﻿namespace Company.Interface.Report
{
    partial class PreviewForm_KCX
    {
        /// <summary>
        /// Required designer variable.//code
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.ExplorerBar.ExplorerBarGroup explorerBarGroup1 = new Janus.Windows.ExplorerBar.ExplorerBarGroup();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem1 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem2 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem3 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem4 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            Janus.Windows.ExplorerBar.ExplorerBarItem explorerBarItem5 = new Janus.Windows.ExplorerBar.ExplorerBarItem();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PreviewForm_KCX));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.defaultLookAndFeel1 = new DevExpress.LookAndFeel.DefaultLookAndFeel(this.components);
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel0Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.explorerBar1 = new Janus.Windows.ExplorerBar.ExplorerBar();
            this.printControl1 = new DevExpress.XtraPrinting.Control.PrintControl();
            this.printBarManager1 = new DevExpress.XtraPrinting.Preview.PrintBarManager();
            this.previewBar1 = new DevExpress.XtraPrinting.Preview.PreviewBar();
            this.printPreviewBarItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem5 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem6 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem7 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem8 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem9 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem10 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem11 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.zoomBarEditItem1 = new DevExpress.XtraPrinting.Preview.ZoomBarEditItem();
            this.printPreviewRepositoryItemComboBox1 = new DevExpress.XtraPrinting.Preview.PrintPreviewRepositoryItemComboBox();
            this.printPreviewBarItem12 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem13 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem14 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem15 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem16 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem17 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem18 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem19 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem20 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem21 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem22 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.previewBar2 = new DevExpress.XtraPrinting.Preview.PreviewBar();
            this.printPreviewStaticItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.printPreviewStaticItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.printPreviewStaticItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem();
            this.previewBar3 = new DevExpress.XtraPrinting.Preview.PreviewBar();
            this.printPreviewSubItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.printPreviewSubItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.printPreviewSubItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.printPreviewBarItem23 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.printPreviewBarItem24 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarItem();
            this.barToolbarsListItem1 = new DevExpress.XtraBars.BarToolbarsListItem();
            this.printPreviewSubItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewSubItem();
            this.barEditItem1 = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.printPreviewBarCheckItem1 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem2 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem3 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem4 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem5 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem6 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem7 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem8 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.mniPDFFile = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem10 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem11 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem12 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.mniExcelFile = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem14 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.printPreviewBarCheckItem15 = new DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem();
            this.cboToKhai = new Janus.Windows.EditControls.UIComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            this.uiPanel0Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printBarManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printPreviewRepositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.printControl1);
            this.grbMain.Location = new System.Drawing.Point(183, 53);
            this.grbMain.Size = new System.Drawing.Size(645, 337);
            // 
            // defaultLookAndFeel1
            // 
            this.defaultLookAndFeel1.LookAndFeel.SkinName = "Office 2007 Blue";
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.Tag = null;
            this.uiPanelManager1.VisualStyleManager = this.vsmMain;
            this.uiPanel0.Id = new System.Guid("91deb5ed-9706-4ef5-af6c-d61a3b48b640");
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("91deb5ed-9706-4ef5-af6c-d61a3b48b640"), Janus.Windows.UI.Dock.PanelDockStyle.Left, new System.Drawing.Size(180, 337), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("91deb5ed-9706-4ef5-af6c-d61a3b48b640"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel0
            // 
            this.uiPanel0.InnerContainer = this.uiPanel0Container;
            this.uiPanel0.Location = new System.Drawing.Point(3, 53);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(180, 337);
            this.uiPanel0.TabIndex = 4;
            this.uiPanel0.Text = "Danh sách báo cáo";
            // 
            // uiPanel0Container
            // 
            this.uiPanel0Container.Controls.Add(this.explorerBar1);
            this.uiPanel0Container.Location = new System.Drawing.Point(1, 23);
            this.uiPanel0Container.Name = "uiPanel0Container";
            this.uiPanel0Container.Size = new System.Drawing.Size(174, 313);
            this.uiPanel0Container.TabIndex = 0;
            // 
            // explorerBar1
            // 
            this.explorerBar1.Dock = System.Windows.Forms.DockStyle.Fill;
            explorerBarItem1.Key = "BKTKN";
            explorerBarItem1.Text = "Mẫu 10/HSTK-CX (Bảng kê tờ khai nhập khẩu NPL)";
            explorerBarItem2.Key = "BKTKX";
            explorerBarItem2.Text = "Mẫu 11/HSTK-CX (Bảng kê tờ khai xuất khẩu SP)";
            explorerBarItem3.Key = "BCNPLXNT";
            explorerBarItem3.Text = "Mẫu 10/HSTK-CX (Báo cáo xuất nhập tồn) -TT194";
            explorerBarItem4.Key = "BCThueNK";
            explorerBarItem4.Text = "Mẫu 19/HSTK-SXXK(Báo cáo thuế trên NPL nhập khẩu)";
            explorerBarItem4.Visible = false;
            explorerBarItem5.Key = "BCNPLXNT_79";
            explorerBarItem5.Text = "Mẫu 12/HSTK-CX (Báo cáo xuất nhập tồn)  - TT79";
            explorerBarGroup1.Items.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarItem[] {
            explorerBarItem1,
            explorerBarItem2,
            explorerBarItem3,
            explorerBarItem4,
            explorerBarItem5});
            explorerBarGroup1.Key = "Group1";
            explorerBarGroup1.Text = "Báo cáo thông tư 79";
            this.explorerBar1.Groups.AddRange(new Janus.Windows.ExplorerBar.ExplorerBarGroup[] {
            explorerBarGroup1});
            this.explorerBar1.Location = new System.Drawing.Point(0, 0);
            this.explorerBar1.Name = "explorerBar1";
            this.explorerBar1.Size = new System.Drawing.Size(174, 313);
            this.explorerBar1.TabIndex = 0;
            this.explorerBar1.VisualStyleManager = this.vsmMain;
            this.explorerBar1.ItemClick += new Janus.Windows.ExplorerBar.ItemEventHandler(this.explorerBar1_ItemClick);
            // 
            // printControl1
            // 
            this.printControl1.BackColor = System.Drawing.Color.Empty;
            this.printControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.printControl1.ForeColor = System.Drawing.Color.Empty;
            this.printControl1.IsMetric = false;
            this.printControl1.Location = new System.Drawing.Point(0, 0);
            this.printControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Office2003;
            this.printControl1.Name = "printControl1";
            this.printControl1.Size = new System.Drawing.Size(645, 337);
            this.printControl1.TabIndex = 0;
            this.printControl1.TooltipFont = new System.Drawing.Font("Tahoma", 8.25F);
            // 
            // printBarManager1
            // 
            this.printBarManager1.Bars.AddRange(new DevExpress.XtraBars.Bar[] {
            this.previewBar1,
            this.previewBar2,
            this.previewBar3});
            this.printBarManager1.DockControls.Add(this.barDockControlTop);
            this.printBarManager1.DockControls.Add(this.barDockControlBottom);
            this.printBarManager1.DockControls.Add(this.barDockControlLeft);
            this.printBarManager1.DockControls.Add(this.barDockControlRight);
            this.printBarManager1.Form = this;
            this.printBarManager1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("printBarManager1.ImageStream")));
            this.printBarManager1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.printPreviewStaticItem1,
            this.printPreviewStaticItem2,
            this.printPreviewStaticItem3,
            this.printPreviewBarItem1,
            this.printPreviewBarItem2,
            this.printPreviewBarItem3,
            this.printPreviewBarItem4,
            this.printPreviewBarItem5,
            this.printPreviewBarItem6,
            this.printPreviewBarItem7,
            this.printPreviewBarItem8,
            this.printPreviewBarItem9,
            this.printPreviewBarItem10,
            this.printPreviewBarItem11,
            this.zoomBarEditItem1,
            this.printPreviewBarItem12,
            this.printPreviewBarItem13,
            this.printPreviewBarItem14,
            this.printPreviewBarItem15,
            this.printPreviewBarItem16,
            this.printPreviewBarItem17,
            this.printPreviewBarItem18,
            this.printPreviewBarItem19,
            this.printPreviewBarItem20,
            this.printPreviewBarItem21,
            this.printPreviewBarItem22,
            this.printPreviewSubItem1,
            this.printPreviewSubItem2,
            this.printPreviewSubItem3,
            this.printPreviewSubItem4,
            this.printPreviewBarItem23,
            this.printPreviewBarItem24,
            this.barToolbarsListItem1,
            this.printPreviewBarCheckItem1,
            this.printPreviewBarCheckItem2,
            this.printPreviewBarCheckItem3,
            this.printPreviewBarCheckItem4,
            this.printPreviewBarCheckItem5,
            this.printPreviewBarCheckItem6,
            this.printPreviewBarCheckItem7,
            this.printPreviewBarCheckItem8,
            this.mniPDFFile,
            this.printPreviewBarCheckItem10,
            this.printPreviewBarCheckItem11,
            this.printPreviewBarCheckItem12,
            this.mniExcelFile,
            this.printPreviewBarCheckItem14,
            this.printPreviewBarCheckItem15,
            this.barEditItem1});
            this.printBarManager1.MainMenu = this.previewBar3;
            this.printBarManager1.MaxItemId = 49;
            this.printBarManager1.PreviewBar = this.previewBar1;
            this.printBarManager1.PrintControl = this.printControl1;
            this.printBarManager1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.printPreviewRepositoryItemComboBox1,
            this.repositoryItemComboBox1});
            this.printBarManager1.StatusBar = this.previewBar2;
            this.printBarManager1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.printBarManager1_ItemClick);
            // 
            // previewBar1
            // 
            this.previewBar1.BarName = "Toolbar";
            this.previewBar1.DockCol = 0;
            this.previewBar1.DockRow = 1;
            this.previewBar1.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.previewBar1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem3, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem4, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem7),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem8),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem9, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem10),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem11, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.zoomBarEditItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem12),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem13, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem14),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem15),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem16),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem17, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem18),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem19),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem20, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem21),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem22, true)});
            this.previewBar1.Text = "Toolbar";
            // 
            // printPreviewBarItem1
            // 
            this.printPreviewBarItem1.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem1.Caption = "Document Map";
            this.printPreviewBarItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.DocumentMap;
            this.printPreviewBarItem1.Enabled = false;
            this.printPreviewBarItem1.Hint = "Document Map";
            this.printPreviewBarItem1.Id = 3;
            this.printPreviewBarItem1.ImageIndex = 19;
            this.printPreviewBarItem1.Name = "printPreviewBarItem1";
            this.printPreviewBarItem1.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // printPreviewBarItem2
            // 
            this.printPreviewBarItem2.Caption = "Search";
            this.printPreviewBarItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Find;
            this.printPreviewBarItem2.Enabled = false;
            this.printPreviewBarItem2.Hint = "Search";
            this.printPreviewBarItem2.Id = 4;
            this.printPreviewBarItem2.ImageIndex = 20;
            this.printPreviewBarItem2.Name = "printPreviewBarItem2";
            // 
            // printPreviewBarItem3
            // 
            this.printPreviewBarItem3.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem3.Caption = "Customize";
            this.printPreviewBarItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Customize;
            this.printPreviewBarItem3.Enabled = false;
            this.printPreviewBarItem3.Hint = "Customize";
            this.printPreviewBarItem3.Id = 5;
            this.printPreviewBarItem3.ImageIndex = 14;
            this.printPreviewBarItem3.Name = "printPreviewBarItem3";
            // 
            // printPreviewBarItem4
            // 
            this.printPreviewBarItem4.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem4.Caption = "&Print...";
            this.printPreviewBarItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Print;
            this.printPreviewBarItem4.Enabled = false;
            this.printPreviewBarItem4.Hint = "Print";
            this.printPreviewBarItem4.Id = 6;
            this.printPreviewBarItem4.ImageIndex = 0;
            this.printPreviewBarItem4.Name = "printPreviewBarItem4";
            // 
            // printPreviewBarItem5
            // 
            this.printPreviewBarItem5.Caption = "P&rint";
            this.printPreviewBarItem5.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PrintDirect;
            this.printPreviewBarItem5.Enabled = false;
            this.printPreviewBarItem5.Hint = "Quick Print";
            this.printPreviewBarItem5.Id = 7;
            this.printPreviewBarItem5.ImageIndex = 1;
            this.printPreviewBarItem5.Name = "printPreviewBarItem5";
            // 
            // printPreviewBarItem6
            // 
            this.printPreviewBarItem6.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem6.Caption = "Page Set&up...";
            this.printPreviewBarItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageSetup;
            this.printPreviewBarItem6.Enabled = false;
            this.printPreviewBarItem6.Hint = "Page Setup";
            this.printPreviewBarItem6.Id = 8;
            this.printPreviewBarItem6.ImageIndex = 2;
            this.printPreviewBarItem6.Name = "printPreviewBarItem6";
            // 
            // printPreviewBarItem7
            // 
            this.printPreviewBarItem7.Caption = "Header And Footer";
            this.printPreviewBarItem7.Command = DevExpress.XtraPrinting.PrintingSystemCommand.EditPageHF;
            this.printPreviewBarItem7.Enabled = false;
            this.printPreviewBarItem7.Hint = "Header And Footer";
            this.printPreviewBarItem7.Id = 9;
            this.printPreviewBarItem7.ImageIndex = 15;
            this.printPreviewBarItem7.Name = "printPreviewBarItem7";
            // 
            // printPreviewBarItem8
            // 
            this.printPreviewBarItem8.ActAsDropDown = true;
            this.printPreviewBarItem8.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem8.Caption = "Scale";
            this.printPreviewBarItem8.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Scale;
            this.printPreviewBarItem8.Enabled = false;
            this.printPreviewBarItem8.Hint = "Scale";
            this.printPreviewBarItem8.Id = 10;
            this.printPreviewBarItem8.ImageIndex = 22;
            this.printPreviewBarItem8.Name = "printPreviewBarItem8";
            // 
            // printPreviewBarItem9
            // 
            this.printPreviewBarItem9.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem9.Caption = "Hand Tool";
            this.printPreviewBarItem9.Command = DevExpress.XtraPrinting.PrintingSystemCommand.HandTool;
            this.printPreviewBarItem9.Enabled = false;
            this.printPreviewBarItem9.Hint = "Hand Tool";
            this.printPreviewBarItem9.Id = 11;
            this.printPreviewBarItem9.ImageIndex = 16;
            this.printPreviewBarItem9.Name = "printPreviewBarItem9";
            // 
            // printPreviewBarItem10
            // 
            this.printPreviewBarItem10.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem10.Caption = "Magnifier";
            this.printPreviewBarItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Magnifier;
            this.printPreviewBarItem10.Enabled = false;
            this.printPreviewBarItem10.Hint = "Magnifier";
            this.printPreviewBarItem10.Id = 12;
            this.printPreviewBarItem10.ImageIndex = 3;
            this.printPreviewBarItem10.Name = "printPreviewBarItem10";
            // 
            // printPreviewBarItem11
            // 
            this.printPreviewBarItem11.Caption = "Zoom Out";
            this.printPreviewBarItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomOut;
            this.printPreviewBarItem11.Enabled = false;
            this.printPreviewBarItem11.Hint = "Zoom Out";
            this.printPreviewBarItem11.Id = 13;
            this.printPreviewBarItem11.ImageIndex = 5;
            this.printPreviewBarItem11.Name = "printPreviewBarItem11";
            // 
            // zoomBarEditItem1
            // 
            this.zoomBarEditItem1.Caption = "Zoom";
            this.zoomBarEditItem1.Edit = this.printPreviewRepositoryItemComboBox1;
            this.zoomBarEditItem1.EditValue = "100%";
            this.zoomBarEditItem1.Enabled = false;
            this.zoomBarEditItem1.Hint = "Zoom";
            this.zoomBarEditItem1.Id = 14;
            this.zoomBarEditItem1.Name = "zoomBarEditItem1";
            this.zoomBarEditItem1.Width = 70;
            // 
            // printPreviewRepositoryItemComboBox1
            // 
            this.printPreviewRepositoryItemComboBox1.AutoComplete = false;
            this.printPreviewRepositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.printPreviewRepositoryItemComboBox1.DropDownRows = 11;
            this.printPreviewRepositoryItemComboBox1.Name = "printPreviewRepositoryItemComboBox1";
            // 
            // printPreviewBarItem12
            // 
            this.printPreviewBarItem12.Caption = "Zoom In";
            this.printPreviewBarItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ZoomIn;
            this.printPreviewBarItem12.Enabled = false;
            this.printPreviewBarItem12.Hint = "Zoom In";
            this.printPreviewBarItem12.Id = 15;
            this.printPreviewBarItem12.ImageIndex = 4;
            this.printPreviewBarItem12.Name = "printPreviewBarItem12";
            // 
            // printPreviewBarItem13
            // 
            this.printPreviewBarItem13.Caption = "First Page";
            this.printPreviewBarItem13.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowFirstPage;
            this.printPreviewBarItem13.Enabled = false;
            this.printPreviewBarItem13.Hint = "First Page";
            this.printPreviewBarItem13.Id = 16;
            this.printPreviewBarItem13.ImageIndex = 7;
            this.printPreviewBarItem13.Name = "printPreviewBarItem13";
            // 
            // printPreviewBarItem14
            // 
            this.printPreviewBarItem14.Caption = "Previous Page";
            this.printPreviewBarItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowPrevPage;
            this.printPreviewBarItem14.Enabled = false;
            this.printPreviewBarItem14.Hint = "Previous Page";
            this.printPreviewBarItem14.Id = 17;
            this.printPreviewBarItem14.ImageIndex = 8;
            this.printPreviewBarItem14.Name = "printPreviewBarItem14";
            // 
            // printPreviewBarItem15
            // 
            this.printPreviewBarItem15.Caption = "Next Page";
            this.printPreviewBarItem15.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowNextPage;
            this.printPreviewBarItem15.Enabled = false;
            this.printPreviewBarItem15.Hint = "Next Page";
            this.printPreviewBarItem15.Id = 18;
            this.printPreviewBarItem15.ImageIndex = 9;
            this.printPreviewBarItem15.Name = "printPreviewBarItem15";
            // 
            // printPreviewBarItem16
            // 
            this.printPreviewBarItem16.Caption = "Last Page";
            this.printPreviewBarItem16.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ShowLastPage;
            this.printPreviewBarItem16.Enabled = false;
            this.printPreviewBarItem16.Hint = "Last Page";
            this.printPreviewBarItem16.Id = 19;
            this.printPreviewBarItem16.ImageIndex = 10;
            this.printPreviewBarItem16.Name = "printPreviewBarItem16";
            // 
            // printPreviewBarItem17
            // 
            this.printPreviewBarItem17.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem17.Caption = "Multiple Pages";
            this.printPreviewBarItem17.Command = DevExpress.XtraPrinting.PrintingSystemCommand.MultiplePages;
            this.printPreviewBarItem17.Enabled = false;
            this.printPreviewBarItem17.Hint = "Multiple Pages";
            this.printPreviewBarItem17.Id = 20;
            this.printPreviewBarItem17.ImageIndex = 11;
            this.printPreviewBarItem17.Name = "printPreviewBarItem17";
            // 
            // printPreviewBarItem18
            // 
            this.printPreviewBarItem18.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem18.Caption = "&Color...";
            this.printPreviewBarItem18.Command = DevExpress.XtraPrinting.PrintingSystemCommand.FillBackground;
            this.printPreviewBarItem18.Enabled = false;
            this.printPreviewBarItem18.Hint = "Background";
            this.printPreviewBarItem18.Id = 21;
            this.printPreviewBarItem18.ImageIndex = 12;
            this.printPreviewBarItem18.Name = "printPreviewBarItem18";
            // 
            // printPreviewBarItem19
            // 
            this.printPreviewBarItem19.Caption = "&Watermark...";
            this.printPreviewBarItem19.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Watermark;
            this.printPreviewBarItem19.Enabled = false;
            this.printPreviewBarItem19.Hint = "Watermark";
            this.printPreviewBarItem19.Id = 22;
            this.printPreviewBarItem19.ImageIndex = 21;
            this.printPreviewBarItem19.Name = "printPreviewBarItem19";
            // 
            // printPreviewBarItem20
            // 
            this.printPreviewBarItem20.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem20.Caption = "Xuất báo cáo...";
            this.printPreviewBarItem20.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportFile;
            this.printPreviewBarItem20.Enabled = false;
            this.printPreviewBarItem20.Hint = "Xuất báo cáo...";
            this.printPreviewBarItem20.Id = 23;
            this.printPreviewBarItem20.ImageIndex = 18;
            this.printPreviewBarItem20.Name = "printPreviewBarItem20";
            // 
            // printPreviewBarItem21
            // 
            this.printPreviewBarItem21.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.DropDown;
            this.printPreviewBarItem21.Caption = "Send via E-Mail...";
            this.printPreviewBarItem21.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendFile;
            this.printPreviewBarItem21.Enabled = false;
            this.printPreviewBarItem21.Hint = "Send via E-Mail...";
            this.printPreviewBarItem21.Id = 24;
            this.printPreviewBarItem21.ImageIndex = 17;
            this.printPreviewBarItem21.Name = "printPreviewBarItem21";
            // 
            // printPreviewBarItem22
            // 
            this.printPreviewBarItem22.Caption = "E&xit";
            this.printPreviewBarItem22.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ClosePreview;
            this.printPreviewBarItem22.Enabled = false;
            this.printPreviewBarItem22.Hint = "Close Preview";
            this.printPreviewBarItem22.Id = 25;
            this.printPreviewBarItem22.ImageIndex = 13;
            this.printPreviewBarItem22.Name = "printPreviewBarItem22";
            // 
            // previewBar2
            // 
            this.previewBar2.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.previewBar2.Appearance.Options.UseBackColor = true;
            this.previewBar2.BarName = "Status Bar";
            this.previewBar2.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.previewBar2.DockCol = 0;
            this.previewBar2.DockRow = 0;
            this.previewBar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom;
            this.previewBar2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewStaticItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewStaticItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewStaticItem3)});
            this.previewBar2.OptionsBar.AllowQuickCustomization = false;
            this.previewBar2.OptionsBar.DrawDragBorder = false;
            this.previewBar2.OptionsBar.UseWholeRow = true;
            this.previewBar2.Text = "Status Bar";
            // 
            // printPreviewStaticItem1
            // 
            this.printPreviewStaticItem1.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.printPreviewStaticItem1.Caption = "Current Page No: none";
            this.printPreviewStaticItem1.Id = 0;
            this.printPreviewStaticItem1.LeftIndent = 1;
            this.printPreviewStaticItem1.Name = "printPreviewStaticItem1";
            this.printPreviewStaticItem1.RightIndent = 1;
            this.printPreviewStaticItem1.TextAlignment = System.Drawing.StringAlignment.Near;
            this.printPreviewStaticItem1.Type = "CurrentPageNo";
            this.printPreviewStaticItem1.Width = 200;
            // 
            // printPreviewStaticItem2
            // 
            this.printPreviewStaticItem2.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.printPreviewStaticItem2.Caption = "Total Page No: 0";
            this.printPreviewStaticItem2.Id = 1;
            this.printPreviewStaticItem2.LeftIndent = 1;
            this.printPreviewStaticItem2.Name = "printPreviewStaticItem2";
            this.printPreviewStaticItem2.RightIndent = 1;
            this.printPreviewStaticItem2.TextAlignment = System.Drawing.StringAlignment.Near;
            this.printPreviewStaticItem2.Type = "TotalPageNo";
            this.printPreviewStaticItem2.Width = 200;
            // 
            // printPreviewStaticItem3
            // 
            this.printPreviewStaticItem3.AutoSize = DevExpress.XtraBars.BarStaticItemSize.Spring;
            this.printPreviewStaticItem3.Caption = "100%";
            this.printPreviewStaticItem3.Id = 2;
            this.printPreviewStaticItem3.LeftIndent = 1;
            this.printPreviewStaticItem3.Name = "printPreviewStaticItem3";
            this.printPreviewStaticItem3.RightIndent = 1;
            this.printPreviewStaticItem3.TextAlignment = System.Drawing.StringAlignment.Near;
            this.printPreviewStaticItem3.Type = "ZoomFactor";
            this.printPreviewStaticItem3.Width = 200;
            // 
            // previewBar3
            // 
            this.previewBar3.BarName = "Main Menu";
            this.previewBar3.DockCol = 0;
            this.previewBar3.DockRow = 0;
            this.previewBar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Top;
            this.previewBar3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewSubItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewSubItem2),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewSubItem3),
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditItem1)});
            this.previewBar3.OptionsBar.MultiLine = true;
            this.previewBar3.OptionsBar.UseWholeRow = true;
            this.previewBar3.Text = "Main Menu";
            // 
            // printPreviewSubItem1
            // 
            this.printPreviewSubItem1.Caption = "&File";
            this.printPreviewSubItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.File;
            this.printPreviewSubItem1.Id = 26;
            this.printPreviewSubItem1.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem6),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem4),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem5),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem20, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem21),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem22, true)});
            this.printPreviewSubItem1.Name = "printPreviewSubItem1";
            // 
            // printPreviewSubItem2
            // 
            this.printPreviewSubItem2.Caption = "&View";
            this.printPreviewSubItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.View;
            this.printPreviewSubItem2.Id = 27;
            this.printPreviewSubItem2.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewSubItem4, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barToolbarsListItem1, true)});
            this.printPreviewSubItem2.Name = "printPreviewSubItem2";
            // 
            // printPreviewSubItem4
            // 
            this.printPreviewSubItem4.Caption = "&Page Layout";
            this.printPreviewSubItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayout;
            this.printPreviewSubItem4.Id = 29;
            this.printPreviewSubItem4.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem23),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem24)});
            this.printPreviewSubItem4.Name = "printPreviewSubItem4";
            // 
            // printPreviewBarItem23
            // 
            this.printPreviewBarItem23.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem23.Caption = "&Facing";
            this.printPreviewBarItem23.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayoutFacing;
            this.printPreviewBarItem23.Enabled = false;
            this.printPreviewBarItem23.GroupIndex = 100;
            this.printPreviewBarItem23.Id = 30;
            this.printPreviewBarItem23.Name = "printPreviewBarItem23";
            // 
            // printPreviewBarItem24
            // 
            this.printPreviewBarItem24.ButtonStyle = DevExpress.XtraBars.BarButtonStyle.Check;
            this.printPreviewBarItem24.Caption = "&Continuous";
            this.printPreviewBarItem24.Command = DevExpress.XtraPrinting.PrintingSystemCommand.PageLayoutContinuous;
            this.printPreviewBarItem24.Enabled = false;
            this.printPreviewBarItem24.GroupIndex = 100;
            this.printPreviewBarItem24.Id = 31;
            this.printPreviewBarItem24.Name = "printPreviewBarItem24";
            // 
            // barToolbarsListItem1
            // 
            this.barToolbarsListItem1.Caption = "Bars";
            this.barToolbarsListItem1.Id = 32;
            this.barToolbarsListItem1.Name = "barToolbarsListItem1";
            // 
            // printPreviewSubItem3
            // 
            this.printPreviewSubItem3.Caption = "&Background";
            this.printPreviewSubItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.Background;
            this.printPreviewSubItem3.Id = 28;
            this.printPreviewSubItem3.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem18),
            new DevExpress.XtraBars.LinkPersistInfo(this.printPreviewBarItem19)});
            this.printPreviewSubItem3.Name = "printPreviewSubItem3";
            // 
            // barEditItem1
            // 
            this.barEditItem1.Caption = "barEditItem1";
            this.barEditItem1.Edit = this.repositoryItemComboBox1;
            this.barEditItem1.Id = 48;
            this.barEditItem1.Name = "barEditItem1";
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(831, 50);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 393);
            this.barDockControlBottom.Size = new System.Drawing.Size(831, 27);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 50);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 343);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(831, 50);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 343);
            // 
            // printPreviewBarCheckItem1
            // 
            this.printPreviewBarCheckItem1.Caption = "PDF File";
            this.printPreviewBarCheckItem1.Checked = true;
            this.printPreviewBarCheckItem1.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportPdf;
            this.printPreviewBarCheckItem1.Enabled = false;
            this.printPreviewBarCheckItem1.GroupIndex = 2;
            this.printPreviewBarCheckItem1.Hint = "PDF File";
            this.printPreviewBarCheckItem1.Id = 33;
            this.printPreviewBarCheckItem1.Name = "printPreviewBarCheckItem1";
            // 
            // printPreviewBarCheckItem2
            // 
            this.printPreviewBarCheckItem2.Caption = "HTML File";
            this.printPreviewBarCheckItem2.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportHtm;
            this.printPreviewBarCheckItem2.Enabled = false;
            this.printPreviewBarCheckItem2.GroupIndex = 2;
            this.printPreviewBarCheckItem2.Hint = "HTML File";
            this.printPreviewBarCheckItem2.Id = 34;
            this.printPreviewBarCheckItem2.Name = "printPreviewBarCheckItem2";
            // 
            // printPreviewBarCheckItem3
            // 
            this.printPreviewBarCheckItem3.Caption = "Text File";
            this.printPreviewBarCheckItem3.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportTxt;
            this.printPreviewBarCheckItem3.Enabled = false;
            this.printPreviewBarCheckItem3.GroupIndex = 2;
            this.printPreviewBarCheckItem3.Hint = "Text File";
            this.printPreviewBarCheckItem3.Id = 35;
            this.printPreviewBarCheckItem3.Name = "printPreviewBarCheckItem3";
            // 
            // printPreviewBarCheckItem4
            // 
            this.printPreviewBarCheckItem4.Caption = "CSV File";
            this.printPreviewBarCheckItem4.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportCsv;
            this.printPreviewBarCheckItem4.Enabled = false;
            this.printPreviewBarCheckItem4.GroupIndex = 2;
            this.printPreviewBarCheckItem4.Hint = "CSV File";
            this.printPreviewBarCheckItem4.Id = 36;
            this.printPreviewBarCheckItem4.Name = "printPreviewBarCheckItem4";
            // 
            // printPreviewBarCheckItem5
            // 
            this.printPreviewBarCheckItem5.Caption = "MHT File";
            this.printPreviewBarCheckItem5.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportMht;
            this.printPreviewBarCheckItem5.Enabled = false;
            this.printPreviewBarCheckItem5.GroupIndex = 2;
            this.printPreviewBarCheckItem5.Hint = "MHT File";
            this.printPreviewBarCheckItem5.Id = 37;
            this.printPreviewBarCheckItem5.Name = "printPreviewBarCheckItem5";
            // 
            // printPreviewBarCheckItem6
            // 
            this.printPreviewBarCheckItem6.Caption = "Excel File";
            this.printPreviewBarCheckItem6.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportXls;
            this.printPreviewBarCheckItem6.Enabled = false;
            this.printPreviewBarCheckItem6.GroupIndex = 2;
            this.printPreviewBarCheckItem6.Hint = "Excel File";
            this.printPreviewBarCheckItem6.Id = 38;
            this.printPreviewBarCheckItem6.Name = "printPreviewBarCheckItem6";
            // 
            // printPreviewBarCheckItem7
            // 
            this.printPreviewBarCheckItem7.Caption = "RTF File";
            this.printPreviewBarCheckItem7.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportRtf;
            this.printPreviewBarCheckItem7.Enabled = false;
            this.printPreviewBarCheckItem7.GroupIndex = 2;
            this.printPreviewBarCheckItem7.Hint = "RTF File";
            this.printPreviewBarCheckItem7.Id = 39;
            this.printPreviewBarCheckItem7.Name = "printPreviewBarCheckItem7";
            // 
            // printPreviewBarCheckItem8
            // 
            this.printPreviewBarCheckItem8.Caption = "Image File";
            this.printPreviewBarCheckItem8.Command = DevExpress.XtraPrinting.PrintingSystemCommand.ExportGraphic;
            this.printPreviewBarCheckItem8.Enabled = false;
            this.printPreviewBarCheckItem8.GroupIndex = 2;
            this.printPreviewBarCheckItem8.Hint = "Image File";
            this.printPreviewBarCheckItem8.Id = 40;
            this.printPreviewBarCheckItem8.Name = "printPreviewBarCheckItem8";
            // 
            // mniPDFFile
            // 
            this.mniPDFFile.Caption = "PDF File";
            this.mniPDFFile.Checked = true;
            this.mniPDFFile.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendPdf;
            this.mniPDFFile.Enabled = false;
            this.mniPDFFile.GroupIndex = 1;
            this.mniPDFFile.Hint = "PDF File";
            this.mniPDFFile.Id = 41;
            this.mniPDFFile.Name = "mniPDFFile";
            // 
            // printPreviewBarCheckItem10
            // 
            this.printPreviewBarCheckItem10.Caption = "Text File";
            this.printPreviewBarCheckItem10.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendTxt;
            this.printPreviewBarCheckItem10.Enabled = false;
            this.printPreviewBarCheckItem10.GroupIndex = 1;
            this.printPreviewBarCheckItem10.Hint = "Text File";
            this.printPreviewBarCheckItem10.Id = 42;
            this.printPreviewBarCheckItem10.Name = "printPreviewBarCheckItem10";
            // 
            // printPreviewBarCheckItem11
            // 
            this.printPreviewBarCheckItem11.Caption = "CSV File";
            this.printPreviewBarCheckItem11.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendCsv;
            this.printPreviewBarCheckItem11.Enabled = false;
            this.printPreviewBarCheckItem11.GroupIndex = 1;
            this.printPreviewBarCheckItem11.Hint = "CSV File";
            this.printPreviewBarCheckItem11.Id = 43;
            this.printPreviewBarCheckItem11.Name = "printPreviewBarCheckItem11";
            // 
            // printPreviewBarCheckItem12
            // 
            this.printPreviewBarCheckItem12.Caption = "MHT File";
            this.printPreviewBarCheckItem12.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendMht;
            this.printPreviewBarCheckItem12.Enabled = false;
            this.printPreviewBarCheckItem12.GroupIndex = 1;
            this.printPreviewBarCheckItem12.Hint = "MHT File";
            this.printPreviewBarCheckItem12.Id = 44;
            this.printPreviewBarCheckItem12.Name = "printPreviewBarCheckItem12";
            // 
            // mniExcelFile
            // 
            this.mniExcelFile.Caption = "Excel File";
            this.mniExcelFile.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendXls;
            this.mniExcelFile.Enabled = false;
            this.mniExcelFile.GroupIndex = 1;
            this.mniExcelFile.Hint = "Excel File";
            this.mniExcelFile.Id = 45;
            this.mniExcelFile.Name = "mniExcelFile";
            // 
            // printPreviewBarCheckItem14
            // 
            this.printPreviewBarCheckItem14.Caption = "RTF File";
            this.printPreviewBarCheckItem14.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendRtf;
            this.printPreviewBarCheckItem14.Enabled = false;
            this.printPreviewBarCheckItem14.GroupIndex = 1;
            this.printPreviewBarCheckItem14.Hint = "RTF File";
            this.printPreviewBarCheckItem14.Id = 46;
            this.printPreviewBarCheckItem14.Name = "printPreviewBarCheckItem14";
            // 
            // printPreviewBarCheckItem15
            // 
            this.printPreviewBarCheckItem15.Caption = "Image File";
            this.printPreviewBarCheckItem15.Command = DevExpress.XtraPrinting.PrintingSystemCommand.SendGraphic;
            this.printPreviewBarCheckItem15.Enabled = false;
            this.printPreviewBarCheckItem15.GroupIndex = 1;
            this.printPreviewBarCheckItem15.Hint = "Image File";
            this.printPreviewBarCheckItem15.Id = 47;
            this.printPreviewBarCheckItem15.Name = "printPreviewBarCheckItem15";
            // 
            // cboToKhai
            // 
            this.cboToKhai.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Tờ khai chính";
            uiComboBoxItem1.Value = 0;
            this.cboToKhai.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1});
            this.cboToKhai.Location = new System.Drawing.Point(348, 0);
            this.cboToKhai.Name = "cboToKhai";
            this.cboToKhai.Size = new System.Drawing.Size(110, 21);
            this.cboToKhai.TabIndex = 2;
            this.cboToKhai.VisualStyleManager = this.vsmMain;
            this.cboToKhai.SelectedIndexChanged += new System.EventHandler(this.cboToKhai_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(297, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Mã NPL";
            // 
            // PreviewForm_KCX
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 420);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboToKhai);
            this.Controls.Add(this.uiPanel0);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "PreviewForm_KCX";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Xem báo cáo thông tư 194 - Loại hình Khu chế xuất";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PreviewForm_Load);
            this.Controls.SetChildIndex(this.barDockControlTop, 0);
            this.Controls.SetChildIndex(this.barDockControlBottom, 0);
            this.Controls.SetChildIndex(this.barDockControlRight, 0);
            this.Controls.SetChildIndex(this.barDockControlLeft, 0);
            this.Controls.SetChildIndex(this.uiPanel0, 0);
            this.Controls.SetChildIndex(this.cboToKhai, 0);
            this.Controls.SetChildIndex(this.label1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            this.uiPanel0Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.explorerBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printBarManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printPreviewRepositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.LookAndFeel.DefaultLookAndFeel defaultLookAndFeel1;
        private Janus.Windows.UI.Dock.UIPanelManager uiPanelManager1;
        private Janus.Windows.UI.Dock.UIPanel uiPanel0;
        private Janus.Windows.UI.Dock.UIPanelInnerContainer uiPanel0Container;
        private Janus.Windows.ExplorerBar.ExplorerBar explorerBar1;
        private DevExpress.XtraPrinting.Control.PrintControl printControl1;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraPrinting.Preview.PrintBarManager printBarManager1;
        private DevExpress.XtraPrinting.Preview.PreviewBar previewBar1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem7;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem8;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem9;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem10;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem11;
        private DevExpress.XtraPrinting.Preview.ZoomBarEditItem zoomBarEditItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewRepositoryItemComboBox printPreviewRepositoryItemComboBox1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem12;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem13;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem14;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem15;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem16;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem17;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem18;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem19;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem20;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem21;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem22;
        private DevExpress.XtraPrinting.Preview.PreviewBar previewBar2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewStaticItem printPreviewStaticItem3;
        private DevExpress.XtraPrinting.Preview.PreviewBar previewBar3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem printPreviewSubItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem printPreviewSubItem2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem printPreviewSubItem4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem23;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarItem printPreviewBarItem24;
        private DevExpress.XtraBars.BarToolbarsListItem barToolbarsListItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewSubItem printPreviewSubItem3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem1;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem2;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem3;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem4;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem5;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem6;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem7;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem8;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem mniPDFFile;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem10;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem11;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem12;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem mniExcelFile;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem14;
        private DevExpress.XtraPrinting.Preview.PrintPreviewBarCheckItem printPreviewBarCheckItem15;
        private DevExpress.XtraBars.BarEditItem barEditItem1;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private Janus.Windows.EditControls.UIComboBox cboToKhai;
        private System.Windows.Forms.Label label1;
    }
}
