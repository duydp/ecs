using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using KeySecurity;
using License = KeySecurity.License;
namespace Company.Interface.ProdInfo
{
    public partial class frmRegister : BaseForm
    {
        private string ProductID;
#if KD_V3 || KD_V4
        string AsemblyName = "SOFTECH.ECS.TQDT.KD";
        string imageName = "KD";
#elif SXXK_V3 || SXXK_V4
        string AsemblyName = "SOFTECH.ECS.TQDT.SXXK";
        string imageName = "SXXK";
#elif GC_V3 || GC_V4
        string AsemblyName = "SOFTECH.ECS.TQDT.GC";
        string imageName = "GC";
#elif KD_V2
        string AsemblyName = "SOFTECH.ECS.TQDT.KD";
        string imageName = "KD";
#elif GC_V2
        string AsemblyName = "SOFTECH.ECS.TQDT.GC";
        string imageName = "GC";
#elif SXXK_V2
        string AsemblyName = "SOFTECH.ECS.TQDT.SXXK";
        string imageName = "SXXK";
#endif
        public frmRegister()
        {
            InitializeComponent();
        }

        private void frmRegister_Load(object sender, EventArgs e)
        {
            #region Load image from  this form's resource

            // Gets a reference to the same assembly that 
            // contains the type that is creating the ResourceManager.
            System.Reflection.Assembly myAssembly;
            myAssembly = this.GetType().Assembly;

            // Gets a reference to a different assembly.
            System.Reflection.Assembly myOtherAssembly;
            myOtherAssembly = System.Reflection.Assembly.Load(AsemblyName);

            // Creates the ResourceManager.
            System.Resources.ResourceManager myManager = new
               System.Resources.ResourceManager("Company.Interface.ProdInfo.frmRegisteOnline",
               myAssembly);

            // Retrieves String and Image resources.
            System.Drawing.Image myImage;
            myImage = (System.Drawing.Image)myManager.GetObject(imageName);
            #endregion

            pictureBox1.Image = myImage;
        }

        private void btnActivate_Click(object sender, EventArgs e)
        {

            errorProvider1.Clear();
            try
            {
                Guid g = new Guid(txtCDKey.Text.Trim());
            }
            catch
            {
                errorProvider1.SetError(txtCDKey, "Số serial không hợp lệ");
                return;
            }
            License lic = Active.Install.ActiveKey(txtCDKey.Text.Trim());
            lbStatus.ForeColor = Color.Black;
            lbStatus.Text = lic.Message;
            switch (lic.Status)
            {
                case LicenseStatus.Expired:
                    lbStatus.ForeColor = Color.Red;
                    break;
                case LicenseStatus.MachineHashMismatch:
                    break;
                case LicenseStatus.NotFound:
                    lbStatus.ForeColor = Color.Red;
                    break;
                case LicenseStatus.Invalid:
                    lbStatus.ForeColor = Color.Red;
                    break;
                case LicenseStatus.InternalError:
                    lbStatus.ForeColor = Color.Red;
                    break;
                //case LicenseStatus.Lock:
                //    lbStatus.ForeColor = Color.Red;
                //    break;
                default:
                    DialogResult = DialogResult.OK;
                    break;
            }
            if (lic != null && lic.Status == LicenseStatus.Licensed)
            {
                Company.Interface.ProdInfo.frmInfo f = new Company.Interface.ProdInfo.frmInfo();
                f.lic = lic;
                f.register = true;
                f.ShowDialog();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDangKy_Click(object sender, EventArgs e)
        {
            if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ismail").Trim() == "1")
            {
                string notice;
                notice = " Bạn đã đăng ký thử nghiệm với chúng tôi. Vui lòng kiểm tra hộp thư để nhận CD Key.";
                notice += "\r\n Mọi chi tiết xin liên hệ: CÔNG TY CP SOFTECH - 15 QUANG TRUNG - Tp ĐÀ NẴNG";
                notice += "\r\n Số điện thoại: (0511) 3840888";
                ShowMessageTQDT(notice, false);
                return;
            }
            else
            {
                frmRegisteOnline fdangky = new frmRegisteOnline();
                fdangky.ShowDialog();
            }
        }
    }
}