

if not EXISTS( SELECT * FROM t_VNACC_Category_Common WHERE ReferenceDB = 'A521' AND Code = 'VK120')
BEGIN
INSERT INTO t_VNACC_Category_Common (ReferenceDB,Code,Name_VN) VALUES ('A521','VK120','Miễn thuế VAT')

END

UPDATE t_VNACC_Category_Common SET Name_VN = 'Thuế GTGT' WHERE ReferenceDB = 'A522' AND Code = 'VB105'


GO
UPDATE t_HaiQuan_Cuc
SET
	IPServiceCKS = '113.160.178.133',
	ServicePathCKS = 'KDTServiceCKS/Service.asmx'
WHERE ID = 30

UPDATE t_VNACC_Category_Common SET Name_VN = 'Thuế GTGT' WHERE ReferenceDB = 'A522' AND Code = 'VB105'

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '13.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('13.3', GETDATE(), N'Cap nhat danh muc VNACCS, Cap nhat dia chi chu ky số')
END	
