-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiKien_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiKien_Insert]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiKien_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiKien_Update]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiKien_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiKien_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiKien_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiKien_Delete]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiKien_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiKien_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiKien_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiKien_Load]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiKien_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiKien_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_HaiQuan_LoaiKien_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_HaiQuan_LoaiKien_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiKien_Insert]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiKien_Insert]
	@MaLoaiKien varchar(50),
	@TenLoaiKien nvarchar(2048),
	@MoTa nvarchar(max),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_LoaiKien]
(
	[MaLoaiKien],
	[TenLoaiKien],
	[MoTa],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@MaLoaiKien,
	@TenLoaiKien,
	@MoTa,
	@DateCreated,
	@DateModified
)

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiKien_Update]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiKien_Update]
	@MaLoaiKien varchar(50),
	@TenLoaiKien nvarchar(2048),
	@MoTa nvarchar(max),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_LoaiKien]
SET
	[TenLoaiKien] = @TenLoaiKien,
	[MoTa] = @MoTa,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[MaLoaiKien] = @MaLoaiKien

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiKien_InsertUpdate]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiKien_InsertUpdate]
	@MaLoaiKien varchar(50),
	@TenLoaiKien nvarchar(2048),
	@MoTa nvarchar(max),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [MaLoaiKien] FROM [dbo].[t_HaiQuan_LoaiKien] WHERE [MaLoaiKien] = @MaLoaiKien)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_LoaiKien] 
		SET
			[TenLoaiKien] = @TenLoaiKien,
			[MoTa] = @MoTa,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[MaLoaiKien] = @MaLoaiKien
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_LoaiKien]
	(
			[MaLoaiKien],
			[TenLoaiKien],
			[MoTa],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@MaLoaiKien,
			@TenLoaiKien,
			@MoTa,
			@DateCreated,
			@DateModified
	)	
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiKien_Delete]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiKien_Delete]
	@MaLoaiKien varchar(50)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_LoaiKien]
WHERE
	[MaLoaiKien] = @MaLoaiKien

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiKien_DeleteDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiKien_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_LoaiKien] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiKien_Load]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiKien_Load]
	@MaLoaiKien varchar(50)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaLoaiKien],
	[TenLoaiKien],
	[MoTa],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_LoaiKien]
WHERE
	[MaLoaiKien] = @MaLoaiKien
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiKien_SelectDynamic]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiKien_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MaLoaiKien],
	[TenLoaiKien],
	[MoTa],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_LoaiKien] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_LoaiKien_SelectAll]
-- Database: ECS_TQDT_KD_V3
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 28, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_LoaiKien_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MaLoaiKien],
	[TenLoaiKien],
	[MoTa],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_LoaiKien]	

GO

