﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.KDT.SHARE.Components.Messages.Send;
/* LaNNT
 * Form dùng chung cho 3 phân hệ KD-SXXK-GC
 * Không được xóa default cấu hình #if KD_V3 
 * Đây là cấu hình kiểm tra
 */
#if KD_V3 || KD_V4
using Company.KD.BLL;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.KD.BLL.KDT.HangMauDich;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
using Company.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using HangMauDich = Company.BLL.KDT.HangMauDich;
#elif GC_V3 || GC_V4
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL;
using Company.GC.BLL.Utils;
using HangMauDich = Company.GC.BLL.KDT.HangMauDich;
#endif
namespace Company.Interface
{

    public partial class ChungTuNoForm : Company.Interface.BaseForm
    {
        public bool isAddNew = true;
        public ToKhaiMauDich TKMD;
        public ChungTuNo CTN = new ChungTuNo() { TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO };
        public ChungTuNoForm()
        {
            InitializeComponent();
        }
        private void Set()
        {
            txtSoChungTu.Text = CTN.SO_CT;
            ccNgayChungTu.Text = CTN.NGAY_CT.ToShortDateString();
            ccNgayHetHan.Text = CTN.NgayHetHan.ToShortDateString();
            cbLoaiCT.SelectedValue = CTN.MA_LOAI_CT;
            txtNoiCap.Text = CTN.NOI_CAP;
            txtToChucCap.Text = CTN.TO_CHUC_CAP;
            txtGhiChu.Text = CTN.DIENGIAI;
            chIsChungTuNo.Checked = CTN.IsNoChungTu;
            if (CTN.ThoiHanNop.Year > 1900)
                ccThoiHanNop.Text = CTN.ThoiHanNop.ToShortDateString();
            else
                ccThoiHanNop.Text = string.Empty;

            if (CTN.ID == 0)
            {
                ccNgayHetHan.Text = ccNgayChungTu.Text = DateTime.Now.ToShortDateString();
            }
        }
        private void Get()
        {
            CTN.SO_CT = txtSoChungTu.Text;
            CTN.NGAY_CT = Convert.ToDateTime(ccNgayChungTu.Value);
            CTN.NgayHetHan = Convert.ToDateTime(ccNgayHetHan.Value);
            CTN.MA_LOAI_CT = cbLoaiCT.SelectedValue.ToString();
            CTN.NOI_CAP = txtNoiCap.Text;
            CTN.TO_CHUC_CAP = txtToChucCap.Text;
            CTN.DIENGIAI = txtGhiChu.Text;
            CTN.IsNoChungTu = chIsChungTuNo.Checked;
            if (!string.IsNullOrEmpty(ccThoiHanNop.Text))
                CTN.ThoiHanNop = Convert.ToDateTime(ccThoiHanNop.Value);
            else CTN.ThoiHanNop = new DateTime(1900, 1, 1);
            if (CTN.NGAYTN.Year < 1900)
                CTN.NGAYTN = new DateTime(1900, 1, 1);
        }
        private void ChungTuNoForm_Load(object sender, EventArgs e)
        {
            //Load danh sach Loai chung tu
            DataView dvLCT = LoaiChungTu.SelectAll().Tables[0].DefaultView;
            dvLCT.Sort = "ID ASC";
            cbLoaiCT.Items.Clear();
            for (int i = 0; i < dvLCT.Count; i++)
            {
                cbLoaiCT.Items.Add(dvLCT[i]["Ten"].ToString(), (int)dvLCT[i]["ID"]);
            }
            cbLoaiCT.SelectedIndex = cbLoaiCT.Items.Count > 0 ? 0 : -1;

            if (CTN != null) Set();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (ShowMessage("Bạn có thật sự muốn xóa không?", true) == "Yes")
            {
                CTN.Delete();
                TKMD.ChungTuNoCollection.Remove(CTN);
                CTN = new ChungTuNo();
                Set();
            }
        }

        private void btnGhi_Click(object sender, EventArgs e)
        {
            bool isValidate = ValidateControl.ValidateNull(txtSoChungTu, error, "Số chứng từ");
            isValidate &= ValidateControl.ValidateDate(ccNgayChungTu, error, "Ngày chứng từ");
            isValidate &= ValidateControl.ValidateDate(ccNgayHetHan, error, "Ngày hết hạn");
            isValidate &= ValidateControl.ValidateNull(cbLoaiCT, error, "Loại chứng từ");
            isValidate &= ValidateControl.ValidateNull(txtNoiCap, error, "Nơi cấp");
            isValidate &= ValidateControl.ValidateNull(txtToChucCap, error, "Tổ chức cấp");
            if (!string.IsNullOrEmpty(ccThoiHanNop.Text))
                isValidate = ValidateControl.ValidateDate(ccThoiHanNop, error, "Thời hạn nộp");
            if (!isValidate) return;
            Get();
            if (CTN.ID == 0)
                TKMD.ChungTuNoCollection.Add(CTN);
            ShowMessage("Vui lòng chọn Lưu tờ khai trước khi khai báo", false);
            DialogResult = DialogResult.OK;
        }
    }
}
