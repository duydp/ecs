
IF  NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_BKNPLCungUngDangKy]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_KDT_SXXK_BKNPLCungUngDangKy](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoTiepNhan] [bigint] NULL,
	[NgayTiepNhan] [datetime] NULL,
	[MaHaiQuan] [varchar](6) NULL,
	[MaDoanhNghiep] [varchar](20) NULL,
	[LanThanhLy] [int] NULL,
	[TrangThaiXuLy] [int] NULL,
	[GUIDSTR] [nvarchar](200) NULL,
 CONSTRAINT [PK_T_KDT_SXXK_BKNPLCungUngDangKy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

END
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_BKNPLCungUng]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_KDT_SXXK_BKNPLCungUng](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_id] [bigint] NULL,
	[LanThanhLy] [int] NULL,
	[MaDoanhNghiep] [varchar](15) NULL,
	[MaHaiQuan] [varchar](6) NULL,
	[SoToKhaiXuat] [varchar](50) NOT NULL,
	[MaLoaiHinh] [varchar](10) NULL,
	[NgayDangKy] [datetime] NULL,
	[MaSanPham] [varchar](50) NOT NULL,
	[TenSanPham] [nvarchar](255) NULL,
	[DVT_ID] [varchar](10) NULL,
	[SoTTDongHang] [int] NOT NULL,
 CONSTRAINT [PK_t_KDT_SXXK_BKNPLCungUng_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO


IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_BKNPLCungUng_Detail]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_KDT_SXXK_BKNPLCungUng_Detail](
		[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_id] [bigint] NULL,
	[MaNPL] [varchar](50) NULL,
	[TenNPL] [nvarchar](500) NULL,
	[DVT_ID] [varchar](10) NULL,
	[SoChungTu] [varchar](50) NULL,
	[ThueXNK] [numeric](18, 4) NULL,
	[DonGiaTT] [numeric](29, 6) NULL,
	[NgayChungTu] [datetime] NULL,
	[LuongNPL] [numeric](22, 4) NULL,
	[NhomLyDo] [int] NULL,
	[GiaiTrinh] [nvarchar](250) NULL,
 CONSTRAINT [PK_t_KDT_SXXK_BKNPLCungUng_Detail] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_BKNPLCungUngDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@LanThanhLy,
	@TrangThaiXuLy,
	@GUIDSTR
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200)
AS

UPDATE
	[dbo].[t_KDT_SXXK_BKNPLCungUngDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[LanThanhLy] = @LanThanhLy,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GUIDSTR] = @GUIDSTR
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_BKNPLCungUngDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_BKNPLCungUngDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[LanThanhLy] = @LanThanhLy,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GUIDSTR] = @GUIDSTR
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_BKNPLCungUngDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[LanThanhLy],
			[TrangThaiXuLy],
			[GUIDSTR]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@LanThanhLy,
			@TrangThaiXuLy,
			@GUIDSTR
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_BKNPLCungUngDangKy]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_BKNPLCungUngDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_SXXK_BKNPLCungUngDangKy]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR]
FROM [dbo].[t_KDT_SXXK_BKNPLCungUngDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUngDangKy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR]
FROM
	[dbo].[t_KDT_SXXK_BKNPLCungUngDangKy]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Insert]
	@Master_id bigint,
	@LanThanhLy int,
	@MaDoanhNghiep varchar(15),
	@MaHaiQuan varchar(6),
	@SoToKhaiXuat varchar(50),
	@MaLoaiHinh varchar(10),
	@NgayDangKy datetime,
	@MaSanPham varchar(50),
	@TenSanPham nvarchar(255),
	@DVT_ID varchar(10),
	@SoTTDongHang int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_BKNPLCungUng]
(
	[Master_id],
	[LanThanhLy],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoToKhaiXuat],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaSanPham],
	[TenSanPham],
	[DVT_ID],
	[SoTTDongHang]
)
VALUES 
(
	@Master_id,
	@LanThanhLy,
	@MaDoanhNghiep,
	@MaHaiQuan,
	@SoToKhaiXuat,
	@MaLoaiHinh,
	@NgayDangKy,
	@MaSanPham,
	@TenSanPham,
	@DVT_ID,
	@SoTTDongHang
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Update]
	@ID bigint,
	@Master_id bigint,
	@LanThanhLy int,
	@MaDoanhNghiep varchar(15),
	@MaHaiQuan varchar(6),
	@SoToKhaiXuat varchar(50),
	@MaLoaiHinh varchar(10),
	@NgayDangKy datetime,
	@MaSanPham varchar(50),
	@TenSanPham nvarchar(255),
	@DVT_ID varchar(10),
	@SoTTDongHang int
AS

UPDATE
	[dbo].[t_KDT_SXXK_BKNPLCungUng]
SET
	[Master_id] = @Master_id,
	[LanThanhLy] = @LanThanhLy,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[MaHaiQuan] = @MaHaiQuan,
	[SoToKhaiXuat] = @SoToKhaiXuat,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaSanPham] = @MaSanPham,
	[TenSanPham] = @TenSanPham,
	[DVT_ID] = @DVT_ID,
	[SoTTDongHang] = @SoTTDongHang
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@LanThanhLy int,
	@MaDoanhNghiep varchar(15),
	@MaHaiQuan varchar(6),
	@SoToKhaiXuat varchar(50),
	@MaLoaiHinh varchar(10),
	@NgayDangKy datetime,
	@MaSanPham varchar(50),
	@TenSanPham nvarchar(255),
	@DVT_ID varchar(10),
	@SoTTDongHang int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_BKNPLCungUng] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_BKNPLCungUng] 
		SET
			[Master_id] = @Master_id,
			[LanThanhLy] = @LanThanhLy,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[MaHaiQuan] = @MaHaiQuan,
			[SoToKhaiXuat] = @SoToKhaiXuat,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaSanPham] = @MaSanPham,
			[TenSanPham] = @TenSanPham,
			[DVT_ID] = @DVT_ID,
			[SoTTDongHang] = @SoTTDongHang
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_BKNPLCungUng]
		(
			[Master_id],
			[LanThanhLy],
			[MaDoanhNghiep],
			[MaHaiQuan],
			[SoToKhaiXuat],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaSanPham],
			[TenSanPham],
			[DVT_ID],
			[SoTTDongHang]
		)
		VALUES 
		(
			@Master_id,
			@LanThanhLy,
			@MaDoanhNghiep,
			@MaHaiQuan,
			@SoToKhaiXuat,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaSanPham,
			@TenSanPham,
			@DVT_ID,
			@SoTTDongHang
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_BKNPLCungUng]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_BKNPLCungUng] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[LanThanhLy],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoToKhaiXuat],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaSanPham],
	[TenSanPham],
	[DVT_ID],
	[SoTTDongHang]
FROM
	[dbo].[t_KDT_SXXK_BKNPLCungUng]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[LanThanhLy],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoToKhaiXuat],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaSanPham],
	[TenSanPham],
	[DVT_ID],
	[SoTTDongHang]
FROM [dbo].[t_KDT_SXXK_BKNPLCungUng] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, August 13, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[LanThanhLy],
	[MaDoanhNghiep],
	[MaHaiQuan],
	[SoToKhaiXuat],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaSanPham],
	[TenSanPham],
	[DVT_ID],
	[SoTTDongHang]
FROM
	[dbo].[t_KDT_SXXK_BKNPLCungUng]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Insert]
	@Master_id bigint,
	@MaNPL varchar(50),
	@TenNPL nvarchar(500),
	@DVT_ID varchar(10),
	@SoChungTu varchar(50),
	@ThueXNK numeric(18, 4),
	@DonGiaTT numeric(29, 6),
	@NgayChungTu datetime,
	@LuongNPL numeric(22, 4),
	@NhomLyDo int,
	@GiaiTrinh nvarchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_BKNPLCungUng_Detail]
(
	[Master_id],
	[MaNPL],
	[TenNPL],
	[DVT_ID],
	[SoChungTu],
	[ThueXNK],
	[DonGiaTT],
	[NgayChungTu],
	[LuongNPL],
	[NhomLyDo],
	[GiaiTrinh]
)
VALUES 
(
	@Master_id,
	@MaNPL,
	@TenNPL,
	@DVT_ID,
	@SoChungTu,
	@ThueXNK,
	@DonGiaTT,
	@NgayChungTu,
	@LuongNPL,
	@NhomLyDo,
	@GiaiTrinh
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Update]
	@ID bigint,
	@Master_id bigint,
	@MaNPL varchar(50),
	@TenNPL nvarchar(500),
	@DVT_ID varchar(10),
	@SoChungTu varchar(50),
	@ThueXNK numeric(18, 4),
	@DonGiaTT numeric(29, 6),
	@NgayChungTu datetime,
	@LuongNPL numeric(22, 4),
	@NhomLyDo int,
	@GiaiTrinh nvarchar(250)
AS

UPDATE
	[dbo].[t_KDT_SXXK_BKNPLCungUng_Detail]
SET
	[Master_id] = @Master_id,
	[MaNPL] = @MaNPL,
	[TenNPL] = @TenNPL,
	[DVT_ID] = @DVT_ID,
	[SoChungTu] = @SoChungTu,
	[ThueXNK] = @ThueXNK,
	[DonGiaTT] = @DonGiaTT,
	[NgayChungTu] = @NgayChungTu,
	[LuongNPL] = @LuongNPL,
	[NhomLyDo] = @NhomLyDo,
	[GiaiTrinh] = @GiaiTrinh
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@MaNPL varchar(50),
	@TenNPL nvarchar(500),
	@DVT_ID varchar(10),
	@SoChungTu varchar(50),
	@ThueXNK numeric(18, 4),
	@DonGiaTT numeric(29, 6),
	@NgayChungTu datetime,
	@LuongNPL numeric(22, 4),
	@NhomLyDo int,
	@GiaiTrinh nvarchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_BKNPLCungUng_Detail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_BKNPLCungUng_Detail] 
		SET
			[Master_id] = @Master_id,
			[MaNPL] = @MaNPL,
			[TenNPL] = @TenNPL,
			[DVT_ID] = @DVT_ID,
			[SoChungTu] = @SoChungTu,
			[ThueXNK] = @ThueXNK,
			[DonGiaTT] = @DonGiaTT,
			[NgayChungTu] = @NgayChungTu,
			[LuongNPL] = @LuongNPL,
			[NhomLyDo] = @NhomLyDo,
			[GiaiTrinh] = @GiaiTrinh
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_BKNPLCungUng_Detail]
		(
			[Master_id],
			[MaNPL],
			[TenNPL],
			[DVT_ID],
			[SoChungTu],
			[ThueXNK],
			[DonGiaTT],
			[NgayChungTu],
			[LuongNPL],
			[NhomLyDo],
			[GiaiTrinh]
		)
		VALUES 
		(
			@Master_id,
			@MaNPL,
			@TenNPL,
			@DVT_ID,
			@SoChungTu,
			@ThueXNK,
			@DonGiaTT,
			@NgayChungTu,
			@LuongNPL,
			@NhomLyDo,
			@GiaiTrinh
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_BKNPLCungUng_Detail]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_BKNPLCungUng_Detail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[MaNPL],
	[TenNPL],
	[DVT_ID],
	[SoChungTu],
	[ThueXNK],
	[DonGiaTT],
	[NgayChungTu],
	[LuongNPL],
	[NhomLyDo],
	[GiaiTrinh]
FROM
	[dbo].[t_KDT_SXXK_BKNPLCungUng_Detail]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[MaNPL],
	[TenNPL],
	[DVT_ID],
	[SoChungTu],
	[ThueXNK],
	[DonGiaTT],
	[NgayChungTu],
	[LuongNPL],
	[NhomLyDo],
	[GiaiTrinh]
FROM [dbo].[t_KDT_SXXK_BKNPLCungUng_Detail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, August 14, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BKNPLCungUng_Detail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[MaNPL],
	[TenNPL],
	[DVT_ID],
	[SoChungTu],
	[ThueXNK],
	[DonGiaTT],
	[NgayChungTu],
	[LuongNPL],
	[NhomLyDo],
	[GiaiTrinh]
FROM
	[dbo].[t_KDT_SXXK_BKNPLCungUng_Detail]	

GO




/****** Object:  UserDefinedFunction [dbo].[f_Convert_GhiChuTK]    Script Date: 08/14/2014 10:06:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[f_Convert_GhiChuTK]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [dbo].[f_Convert_GhiChuTK]
GO

/****** Object:  UserDefinedFunction [dbo].[f_Convert_GhiChuTK]    Script Date: 08/14/2014 10:06:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[f_Convert_GhiChuTK]
    (
      @StringGhiChu NVARCHAR(1000) ,
      @LanThanhLy INT 
      
    )
RETURNS NVARCHAR(1000)
AS 
    BEGIN
/*
HungTQ, Created 05/04/2011.
Theo doi to khai nhap co tham gia thanh khoan va da dong ho so khong?
*/
		DECLARE @Return NVARCHAR(1000)
		SET @Return = @StringGhiChu
		IF(CHARINDEX(N'Chuyển TK ',@Return) > 0)
		BEGIN 
			DECLARE @temp bigint
				
			SET @temp = Convert(bigint,SUBSTRING(@Return,10,LEN(@Return)))
			
			IF exists (SELECT * FROM t_KDT_SXXK_BKToKhaiNhap 
			WHERE SoToKhai = @temp AND  MaLoaiHinh LIKE '%V%' AND  BangKeHoSoThanhLy_ID = (SELECT TOP 1 id FROM t_KDT_SXXK_BangKeHoSoThanhLy 
			                              WHERE t_KDT_SXXK_BangKeHoSoThanhLy.MaBangKe = 'DTLTKN' AND t_KDT_SXXK_BangKeHoSoThanhLy.MaterID = (SELECT TOP 1 id FROM t_KDT_SXXK_HoSoThanhLyDangKy WHERE LanThanhLy = @LanThanhLy)))
			 BEGIN 
			 	SELECT @Return = N'Chuyển TK ' +  + Convert(nvarchar(12),SoTKVNACCS) FROM t_VNACCS_CapSoToKhai WHERE SoTK = @temp
			 	END
			END

                
        RETURN @Return
    END        

GO



/****** Object:  StoredProcedure [dbo].[p_SXXK_BCXuatNhapTon_TT22]    Script Date: 08/14/2014 10:07:20 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_BCXuatNhapTon_TT22]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_BCXuatNhapTon_TT22]
GO
/****** Object:  StoredProcedure [dbo].[p_SXXK_BCXuatNhapTon_TT22]    Script Date: 08/14/2014 10:07:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_BCXuatNhapTon_TT196]
-- Database: ECS_TQ_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_BCXuatNhapTon_TT22]
		@LanThanhLy bigint
		,@NamThanhLy int
		,@MaDoanhNghiep varchar(50)
		,@SapXepNgayDangKyTKN bit
AS
BEGIN
	
SET NOCOUNT ON
--SET TRANSACTION ISOLATION LEVEL READ COMMITTED

if OBJECT_ID('tempdb..#BCNPLXuatNhapTon_TT196') is not null drop table #BCNPLXuatNhapTon_TT196

CREATE TABLE #BCNPLXuatNhapTon_TT196
(
	STT BIGINT
	,TenNPL NVARCHAR(1000)
	,DVT NVARCHAR(30)
	,SoToKhai NVARCHAR(50)
	,LuongNhap DECIMAL(18,8)
	,LuongTonDau DECIMAL(18,8)
	,LuongSuDung DECIMAL(18,8)
	,LuongTaiXuat DECIMAL(18,8)
	,LuongTonCuoi DECIMAL(18,8)
	,XuLy NVARCHAR(255)
	,MaNPL NVARCHAR(50)
	,STK BIGINT
	,MaLoaiHinh NVARCHAR(20)
	,NamDangKy bigint
)
IF(@SapXepNgayDangKyTKN = 0)
BEGIN -- Sắp xếp theo mã NPL
	
INSERT INTO #BCNPLXuatNhapTon_TT196
(
	STT,
	TenNPL,
	DVT,
	SoToKhai,
	LuongNhap,
	LuongSuDung,
	LuongTonDau,
	LuongTaiXuat,
	LuongTonCuoi,
	XuLy,
	MaNPL ,
	STK,
	MaLoaiHinh, 
	NamDangKy
)


SELECT 
	  (ROW_NUMBER () OVER( ORDER BY MaNPL,NgayDangKyNhap,SoToKhaiNhap)) AS STT
      ,(MAX([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL
      ,MAX(TenDVT_NPL) AS DVT
      ,(Convert(Nvarchar(12),Case when MaLoaiHinhNhap like '%V%' THEN (SELECT SoTKVNACCS FROM t_vnaccs_capsotokhai WHERE SoTK =  SoToKhaiNhap ) ELSE SoToKhaiNhap END ) + ';' 
       + (Case when MaLoaiHinhNhap like '%V%' THEN (SELECT Ten_VT FROM  t_HaiQuan_LoaiHinhMauDich WHERE ID =  MaLoaiHinhNhap )ELSE MaLoaiHinhNhap END ) + ';' + CONVERT(NVARCHAR(20),NgayDangKyNhap,103)) AS SoToKhai
      ,MAX(CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end )as LuongNhap 
      ,Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung
      ,MAX([LuongTonDau]) AS LuongTonDau
      --,SUM([LuongNPLSuDung]) AS LuongSuDung 
      ,SUM([LuongNPLTaiXuat]) AS LuongTaiXuat
      ,MIN([LuongTonCuoi]) AS LuongTonCuoi
	  ,'' AS  XuLy
	  ,MaNPL AS MaNPL
	  ,SoToKhaiNhap AS STK
	  ,MaLoaiHinhNhap AS MaLoaiHinh
	  ,YEAR(NgayDangKyNhap) AS NamDangKy
	  
  FROM [t_KDT_SXXK_BCXuatNhapTon]
  where LanThanhLy = @LanThanhLy and NamThanhLy = @NamThanhLy and MaDoanhNghiep = @MaDoanhNghiep
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap 
ORDER BY MaNPL,NgayDangKyNhap,SoToKhaiNhap

END
ELSE
	BEGIN -- sắp xếp theo ngày đăng ký
			
INSERT INTO #BCNPLXuatNhapTon_TT196
(
	STT,
	TenNPL,
	DVT,
	SoToKhai,
	LuongNhap,
	LuongSuDung,
	LuongTonDau,
	LuongTaiXuat,
	LuongTonCuoi,
	XuLy,
	MaNPL ,
	STK,
	MaLoaiHinh, 
	NamDangKy
)


SELECT 
	  (ROW_NUMBER () OVER( ORDER BY MaNPL,NgayDangKyNhap,SoToKhaiNhap)) AS STT
      ,(MAX([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL
      ,MAX(TenDVT_NPL) AS DVT
      ,(Convert(Nvarchar(12),
       Case when MaLoaiHinhNhap like '%V%' THEN (SELECT SoTKVNACCS FROM t_vnaccs_capsotokhai WHERE SoTK =  SoToKhaiNhap ) ELSE SoToKhaiNhap END ) + ';' 
       + (Case when MaLoaiHinhNhap like '%V%' THEN (SELECT Ten_VT FROM  t_HaiQuan_LoaiHinhMauDich WHERE ID =  MaLoaiHinhNhap )ELSE MaLoaiHinhNhap END ) + ';' + CONVERT(NVARCHAR(20),NgayDangKyNhap,103)) AS SoToKhai
      ,MAX(CASE when  LuongNhap = LuongTonDau then LuongNhap else 0 end )as LuongNhap 
      ,Sum(Case when ( LuongNPLSuDung + LuongTonCuoi) > LuongTonDau then LuongTonDau - LuongTonCuoi else LuongNPLSuDung end)  as LuongNPLSuDung
      ,MAX([LuongTonDau]) AS LuongTonDau
      --,SUM([LuongNPLSuDung]) AS LuongSuDung 
      ,SUM([LuongNPLTaiXuat]) AS LuongTaiXuat
      ,MIN([LuongTonCuoi]) AS LuongTonCuoi
	  ,'' AS  XuLy
	  ,MaNPL AS MaNPL
	  ,SoToKhaiNhap AS STK
	  ,MaLoaiHinhNhap AS MaLoaiHinh
	  ,YEAR(NgayDangKyNhap) AS NamDangKy
	  
  FROM [t_KDT_SXXK_BCXuatNhapTon]
  where LanThanhLy = @LanThanhLy and NamThanhLy = @NamThanhLy and MaDoanhNghiep = @MaDoanhNghiep
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap 
ORDER BY NgayDangKyNhap,SoToKhaiNhap,MaNPL

	END


UPDATE #BCNPLXuatNhapTon_TT196 
SET XuLy = dbo.f_Convert_GhiChuTK((SELECT TOP 1 ThanhKhoanTiep FROM [t_KDT_SXXK_BCXuatNhapTon] bc
            WHERE bc.MaNPL = #BCNPLXuatNhapTon_TT196.MaNPL AND bc.MaLoaiHinhNhap = #BCNPLXuatNhapTon_TT196.MaLoaiHinh AND bc.SoToKhaiNhap = #BCNPLXuatNhapTon_TT196.STK AND
				  Year(bc.NgayDangKyNhap) = #BCNPLXuatNhapTon_TT196.NamDangKy AND bc.LuongTonCuoi = #BCNPLXuatNhapTon_TT196.LuongTonCuoi),@LanThanhLy)

SELECT 
STT,
TenNPL,
DVT,
SoToKhai,
LuongNhap,
LuongTonDau,
LuongSuDung,
LuongSuDung,
LuongTaiXuat,
LuongTonCuoi,
XuLy
  FROM #BCNPLXuatNhapTon_TT196 				  
	
DROP TABLE #BCNPLXuatNhapTon_TT196

END
----------------------------------------------------------

GO



/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectDynamic]    Script Date: 08/14/2014 10:08:34 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectDynamic]    Script Date: 08/14/2014 10:08:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[p_KDT_SXXK_BCXuatNhapTon_SelectDynamic]
	@WhereCondition nvarchar(max),
	@OrderByExpression nvarchar(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL nvarchar(max)

SET @SQL = 'SELECT 	[ID],
	[STT],
	[LanThanhLy],
	[NamThanhLy],
	[MaDoanhNghiep],
	UPPER([MaNPL]) AS MaNPL,
	[TenNPL],
	Case When MaLoaiHinhNhap like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiNhap]) else [SoToKhaiNhap] end as SoToKhaiNhap ,
	[NgayDangKyNhap],
	case WHEN year(NgayHoanThanhNhap)<=1900 then NgayDangKyNhap else NgayHoanThanhNhap end as NgayHoanThanhNhap,
	Case When MaLoaiHinhNhap like ''%V%'' Then Substring(MaLoaiHinhNhap,3,3) else MaLoaiHinhNhap end as MaLoaiHinhNhap,
	[LuongNhap],
	[LuongTonDau],
	[TenDVT_NPL],
	[MaSP],
	[TenSP],
	Case When MaLoaiHinhXuat like ''%V%'' Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiXuat]) else [SoToKhaiXuat] end as SoToKhaiXuat,
	[NgayDangKyXuat],
	case WHEN year(NgayHoanThanhXuat)<=1900 then NgayDangKyXuat else NgayHoanThanhXuat end as NgayHoanThanhXuat,
	Case When MaLoaiHinhXuat like ''%V%'' Then Substring(MaLoaiHinhXuat,3,3) else MaLoaiHinhXuat end as MaLoaiHinhXuat,
	[LuongSPXuat],
	[TenDVT_SP],
	[DinhMuc],
	[LuongNPLSuDung],
	--Case when [SoToKhaiTaiXuat]  > 0 Then (Select TOP 1 SoTKVNACCS From t_VNACCS_CapSoToKhai where SoTK = [SoToKhaiTaiXuat]) else 0 end as SoToKhaiTaiXuat,
	[SoToKhaiTaiXuat],
	[NgayTaiXuat],
	[LuongNPLTaiXuat],
	[LuongTonCuoi],
	dbo.[f_Convert_GhiChuTK]([ThanhKhoanTiep],[LanThanhLy]) as ThanhKhoanTiep,
	[ChuyenMucDichKhac],
	[DonGiaTT],
	[TyGiaTT],
	[ThueSuat],
	[ThueXNK],
	[ThueXNKTon],
[NgayThucXuat]
 FROM [dbo].[t_KDT_SXXK_BCXuatNhapTon] WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL
GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '19.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('19.1',GETDATE(), N' Cập nhật table NPL tu cung ung, ')
END	









