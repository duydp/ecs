﻿using System;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
using System.Threading;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Xml;
using Company.KDT.SHARE.Components;

namespace DongBoDuLieu
{
    public partial class ThietLapThongSoKBForm : BaseForm
    {        
        public ThietLapThongSoKBForm()
        {
            InitializeComponent();
        }

        private void SendForm_Load(object sender, EventArgs e)
        {
            Company.KDT.SHARE.Components.Globals.ReadNodeXmlConnectionStrings();
            txtServerName.Text = WebService.LoadConfigure("SERVER_NAME");
            txtDatabase.Text = WebService.LoadConfigure("DATABASE_NAME");
            txtSa.Text = WebService.LoadConfigure("USER");
            txtPass.Text = WebService.LoadConfigure("PASS");
            txtDiaChiHQ.Text = WebService.LoadConfigure("WS_URL");
        }
      
        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            containerValidator1.Validate();
            if (!containerValidator1.IsValid)
                return;
            
            //Cấu hình lại connectionString.
            string st = "Server=" + txtServerName.Text.Trim() + ";Database=" + txtDatabase.Text.Trim() + ";Uid=" + txtSa.Text.Trim() + ";Pwd=" + txtPass.Text.Trim();
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.ConnectionStrings.ConnectionStrings["MSSQL"].ConnectionString = st;            
            
            SqlConnection con = new SqlConnection(st);
            try
            {
                con.Open();
            }
            catch
            {
                MLMessages("Không kết nối được tới cơ sở dữ liệu trên máy chủ này.","MSG_DAB01","", false);
                return;
            }
            ConfigurationSection section = config.ConnectionStrings;
            if ((section.SectionInformation.IsProtected == false) &&
                (section.ElementInformation.IsLocked == false))
            {
                // Save the encrypted section.
                section.SectionInformation.ForceSave = true;
                config.Save(ConfigurationSaveMode.Modified);
            }
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection(section.SectionInformation.Name);

            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DATABASE_NAME", txtDatabase.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PASS", txtPass.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SERVER_NAME", txtServerName.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("USER", txtSa.Text.Trim());
            Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("WS_URL", txtDiaChiHQ.Text.Trim());
            this.Close();
            Application.Restart();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}