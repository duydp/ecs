﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class Payer:NameBase
    {
        // Địa chỉ
        [XmlElement("Address")]
        public DeliveryDestination AddressGNT { get; set; }
        // Tài khoản ngân hàng
        [XmlElement("Account")]
        public Account Account { get; set; }

    }
}
