using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.BLL.SXXK.ThanhKhoan;

namespace Company.Interface.Report.SXXK
{
    public partial class BangKeToKhaiXuat : DevExpress.XtraReports.UI.XtraReport
    {
        public int SoHoSo;
        public bool isHoanThue = false;

        public BangKeToKhaiXuat()
        {
            InitializeComponent();
        }
        public void BindReport(int lanThanhLy)
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            DataTable dt = new Company.BLL.KDT.SXXK.BKToKhaiXuat().getBKToKhaiXuatForReport(lanThanhLy, GlobalSettings.MA_DON_VI,GlobalSettings.MA_HAI_QUAN).Tables[0];
            dt.TableName = "BKToKhaiXuat";
            this.DataSource = dt;
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (SoHoSo > 0)
            {
                lblSHSTK.Text = "Số " + SoHoSo + (lanThanhLy.ToString() != "" ? " - LẦN " + lanThanhLy : "") + "/ XIN HOÀN";
                lblSHSTK.Width = 200;
            }
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblSoToKhai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhai");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKy", "{0:dd/MM/yy}");
            lblNgayThucXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayThucXuat", "{0:dd/MM/yy}");
            lblTen.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ten");
            xrLabel76.Text = xrLabel77.Text = GlobalSettings.TieudeNgay; ;
        }
    }
}
