﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace TestVNACC
{
    public partial class GenClassfrm : Form
    {
        public GenClassfrm()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog f = new OpenFileDialog();
            f.Filter = "text file | *.txt";
            if(f.ShowDialog(this)==DialogResult.OK)
            {
                textBox1.Text = f.FileName;
            }
        }
        private string GetNameClass(string fileName)
        {
            string[] pathname = fileName.Split('\\');
            string name = pathname[pathname.Length - 1];
            return name.Split('.')[0];
        }

        private void btnDo_Click(object sender, EventArgs e)
        {
            FileInfo file = new FileInfo(textBox1.Text.Trim());
            if (!file.Exists)
            {
                MessageBox.Show("Không tìm thấy file");
                return;
            }
            else
            {
                string strFile = string.Empty;
                using (StreamReader strema = new StreamReader(textBox1.Text.Trim()))
                {
                    strFile =  strema.ReadToEnd();
                    strema.Close();
                }
                string[] StringLines;
                if (!string.IsNullOrEmpty(strFile))
                    StringLines = strFile.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                else
                {
                    MessageBox.Show("File rỗng");
                    return;
                }
                
                GenClass genClass = new GenClass(GetNameClass(textBox1.Text), StringLines);
                using (StreamWriter write = new StreamWriter(file.DirectoryName + "\\" + GetNameClass(textBox1.Text) + ".generated" + ".cs"))
                {
                    write.WriteLine(genClass.VnaccsClass);
                    write.Close();
                };
            } 
            MessageBox.Show("Thành công");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FileInfo file = new FileInfo(textBox1.Text.Trim());
            if (!string.IsNullOrEmpty(file.DirectoryName))
                Process.Start(file.DirectoryName);
        }

    }
}
