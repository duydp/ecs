using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class VanDon
    {
        public List<Container> ContainerCollection = new List<Container>();

        public void LoadContainerCollection()
        {
            ContainerCollection = Container.SelectCollectionBy_VanDon_ID(this.ID);
        }

        //---------------------------------------------------------------------------------------------

        public static VanDon Load(string soVanDon, DateTime ngayVanDon, long idTKMD, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_VanDon_LoadBy]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, soVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, ngayVanDon);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, idTKMD);

            VanDon entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new VanDon();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("HangRoi"))) entity.HangRoi = reader.GetBoolean(reader.GetOrdinal("HangRoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangVT"))) entity.MaHangVT = reader.GetString(reader.GetOrdinal("MaHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangVT"))) entity.TenHangVT = reader.GetString(reader.GetOrdinal("TenHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenPTVT"))) entity.TenPTVT = reader.GetString(reader.GetOrdinal("TenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT"))) entity.QuocTichPTVT = reader.GetString(reader.GetOrdinal("QuocTichPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXuat_ID"))) entity.NuocXuat_ID = reader.GetString(reader.GetOrdinal("NuocXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGiaoHang"))) entity.MaNguoiGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGiaoHang"))) entity.TenNguoiGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauNhap_ID"))) entity.CuaKhauNhap_ID = reader.GetString(reader.GetOrdinal("CuaKhauNhap_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauXuat"))) entity.CuaKhauXuat = reader.GetString(reader.GetOrdinal("CuaKhauXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHangTrungGian"))) entity.MaNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("MaNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHangTrungGian"))) entity.TenNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("TenNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangXepHang"))) entity.MaCangXepHang = reader.GetString(reader.GetOrdinal("MaCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangXepHang"))) entity.TenCangXepHang = reader.GetString(reader.GetOrdinal("TenCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangDoHang"))) entity.MaCangDoHang = reader.GetString(reader.GetOrdinal("MaCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangDoHang"))) entity.TenCangDoHang = reader.GetString(reader.GetOrdinal("TenCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDi"))) entity.NoiDi = reader.GetString(reader.GetOrdinal("NoiDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuChuyenDi"))) entity.SoHieuChuyenDi = reader.GetString(reader.GetOrdinal("SoHieuChuyenDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.VarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.VarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year <= 1753 ? DBNull.Value : (object)NgayKhoiHanh);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }
        public long InsertV3(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year <= 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year <= 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.VarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.VarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year <= 1753 ? DBNull.Value : (object)NgayKhoiHanh);
            db.AddInParameter(dbCommand, "@TongSoKien", SqlDbType.Int, 0);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, "");
            db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, "");
            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }
        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Update]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year == 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year == 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.VarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.VarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year == 1753 ? DBNull.Value : (object)NgayKhoiHanh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public int UpdateV3(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_VanDon_Update]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year == 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year == 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.VarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.VarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year == 1753 ? DBNull.Value : (object)NgayKhoiHanh);
            db.AddInParameter(dbCommand, "@TongSoKien", SqlDbType.Int, 0);
            db.AddInParameter(dbCommand, "@LoaiKien", SqlDbType.NVarChar, "");
            db.AddInParameter(dbCommand, "@SoHieuKien", SqlDbType.NVarChar, 0);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }
        public int InsertUpdateBy(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "p_KDT_VanDon_InsertUpdateBy";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoVanDon", SqlDbType.VarChar, SoVanDon);
            db.AddInParameter(dbCommand, "@NgayVanDon", SqlDbType.DateTime, NgayVanDon.Year == 1753 ? DBNull.Value : (object)NgayVanDon);
            db.AddInParameter(dbCommand, "@HangRoi", SqlDbType.Bit, HangRoi);
            db.AddInParameter(dbCommand, "@SoHieuPTVT", SqlDbType.VarChar, SoHieuPTVT);
            db.AddInParameter(dbCommand, "@NgayDenPTVT", SqlDbType.DateTime, NgayDenPTVT.Year == 1753 ? DBNull.Value : (object)NgayDenPTVT);
            db.AddInParameter(dbCommand, "@MaHangVT", SqlDbType.VarChar, MaHangVT);
            db.AddInParameter(dbCommand, "@TenHangVT", SqlDbType.NVarChar, TenHangVT);
            db.AddInParameter(dbCommand, "@TenPTVT", SqlDbType.NVarChar, TenPTVT);
            db.AddInParameter(dbCommand, "@QuocTichPTVT", SqlDbType.VarChar, QuocTichPTVT);
            db.AddInParameter(dbCommand, "@NuocXuat_ID", SqlDbType.VarChar, NuocXuat_ID);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHang", SqlDbType.VarChar, MaNguoiNhanHang);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHang", SqlDbType.NVarChar, TenNguoiNhanHang);
            db.AddInParameter(dbCommand, "@MaNguoiGiaoHang", SqlDbType.VarChar, MaNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@TenNguoiGiaoHang", SqlDbType.NVarChar, TenNguoiGiaoHang);
            db.AddInParameter(dbCommand, "@CuaKhauNhap_ID", SqlDbType.VarChar, CuaKhauNhap_ID);
            db.AddInParameter(dbCommand, "@CuaKhauXuat", SqlDbType.NVarChar, CuaKhauXuat);
            db.AddInParameter(dbCommand, "@MaNguoiNhanHangTrungGian", SqlDbType.VarChar, MaNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@TenNguoiNhanHangTrungGian", SqlDbType.NVarChar, TenNguoiNhanHangTrungGian);
            db.AddInParameter(dbCommand, "@MaCangXepHang", SqlDbType.VarChar, MaCangXepHang);
            db.AddInParameter(dbCommand, "@TenCangXepHang", SqlDbType.VarChar, TenCangXepHang);
            db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
            db.AddInParameter(dbCommand, "@TenCangDoHang", SqlDbType.VarChar, TenCangDoHang);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@DiaDiemGiaoHang", SqlDbType.NVarChar, DiaDiemGiaoHang);
            db.AddInParameter(dbCommand, "@NoiDi", SqlDbType.NVarChar, NoiDi);
            db.AddInParameter(dbCommand, "@SoHieuChuyenDi", SqlDbType.NVarChar, SoHieuChuyenDi);
            db.AddInParameter(dbCommand, "@NgayKhoiHanh", SqlDbType.DateTime, NgayKhoiHanh.Year == 1753 ? DBNull.Value : (object)NgayKhoiHanh);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public void InsertUpdateFullTrasaction(SqlTransaction transaction)
        {
            if (this.ID == 0)
                this.Insert(transaction);
            else
                this.Update(transaction);
            foreach (Container item in ContainerCollection)
            {
                if (item.ID == 0)
                    item.Insert(transaction);
                else
                    item.Update(transaction);
            }
        }

        public void InsertUpdateFullTrasaction(SqlTransaction transaction, SqlDatabase db)
        {
            //Kiem tra ton tai To khai tren Database Target
            VanDon vdTemp = VanDon.Load(this.SoVanDon, this.NgayVanDon, this.TKMD_ID, db);

            if (vdTemp == null || vdTemp.ID == 0)
                this.ID = Insert(transaction, db);
            else
            {
                this.ID = vdTemp.ID;
                Update(transaction, db);
            }

            foreach (Container item in ContainerCollection)
            {
                item.VanDon_ID = this.ID;

                item.InsertUpdateBy(transaction, db);
            }
        }
        public void InsertFullTrasactionV3(SqlTransaction transaction, SqlDatabase db)
        {
             this.ID = InsertV3(transaction, db);
            foreach (Container item in ContainerCollection)
            {
                item.VanDon_ID = this.ID;

                item.InsertUpdateBy(transaction, db);
            }
        }
        public XmlNode ConvertVanDonToXML(XmlDocument doc)
        {
            #region VanTaiDon


            XmlElement ChungTuVanDon = doc.CreateElement("CHUNG_TU_VAN_DON");

            XmlAttribute SO_CHUNG_TU = doc.CreateAttribute("SO_CHUNG_TU");
            SO_CHUNG_TU.Value = SoVanDon.Trim();
            ChungTuVanDon.Attributes.Append(SO_CHUNG_TU);

            XmlAttribute NGAY_CHUNG_TU = doc.CreateAttribute("NGAY_CHUNG_TU");
            NGAY_CHUNG_TU.Value = NgayVanDon.ToString("yyyy-MM-dd");
            ChungTuVanDon.Attributes.Append(NGAY_CHUNG_TU);

            XmlAttribute NGAY_HH = doc.CreateAttribute("NGAY_HH");
            NGAY_HH.Value = ""; //NgayHetHan.ToString("yyyy-MM-dd");
            ChungTuVanDon.Attributes.Append(NGAY_HH);

            XmlAttribute TEN_NGUOI_NHAN_HANG = doc.CreateAttribute("TEN_NGUOI_NHAN_HANG");
            TEN_NGUOI_NHAN_HANG.Value = TenNguoiNhanHang.Trim();
            ChungTuVanDon.Attributes.Append(TEN_NGUOI_NHAN_HANG);

            XmlAttribute MA_NGUOI_NHAN_HANG = doc.CreateAttribute("MA_NGUOI_NHAN_HANG");
            MA_NGUOI_NHAN_HANG.Value = MaNguoiNhanHang.Trim();
            ChungTuVanDon.Attributes.Append(MA_NGUOI_NHAN_HANG);

            XmlAttribute TEN_NGUOI_GIAO_HANG = doc.CreateAttribute("TEN_NGUOI_GIAO_HANG");
            TEN_NGUOI_GIAO_HANG.Value = TenNguoiGiaoHang.Trim();
            ChungTuVanDon.Attributes.Append(TEN_NGUOI_GIAO_HANG);

            XmlAttribute MA_NGUOI_GIAO_HANG = doc.CreateAttribute("MA_NGUOI_GIAO_HANG");
            MA_NGUOI_GIAO_HANG.Value = MaNguoiGiaoHang.Trim();
            ChungTuVanDon.Attributes.Append(MA_NGUOI_GIAO_HANG);

            XmlAttribute CUA_KHAU_NHAP = doc.CreateAttribute("CUA_KHAU_NHAP");
            CUA_KHAU_NHAP.Value = CuaKhauNhap_ID.Trim();
            ChungTuVanDon.Attributes.Append(CUA_KHAU_NHAP);

            XmlAttribute CUA_KHAU_XUAT = doc.CreateAttribute("CUA_KHAU_XUAT");
            CUA_KHAU_XUAT.Value = CuaKhauXuat.Trim();
            ChungTuVanDon.Attributes.Append(CUA_KHAU_XUAT);

            XmlAttribute MA_DKGH = doc.CreateAttribute("MA_DKGH");
            MA_DKGH.Value = DKGH_ID.Trim();
            ChungTuVanDon.Attributes.Append(MA_DKGH);

            XmlAttribute TEN_NGUOI_NHAN_HANG_TG = doc.CreateAttribute("TEN_NGUOI_NHAN_HANG_TG");
            TEN_NGUOI_NHAN_HANG_TG.Value = TenNguoiNhanHangTrungGian.Trim();
            ChungTuVanDon.Attributes.Append(TEN_NGUOI_NHAN_HANG_TG);

            XmlAttribute MA_NGUOI_NHAN_HANG_TG = doc.CreateAttribute("MA_NGUOI_NHAN_HANG_TG");
            MA_NGUOI_NHAN_HANG_TG.Value = MaNguoiNhanHangTrungGian.Trim();
            ChungTuVanDon.Attributes.Append(MA_NGUOI_NHAN_HANG_TG);

            XmlAttribute DIA_DIEM_GIAO_HANG = doc.CreateAttribute("DIA_DIEM_GIAO_HANG");
            DIA_DIEM_GIAO_HANG.Value = DiaDiemGiaoHang.Trim();
            ChungTuVanDon.Attributes.Append(DIA_DIEM_GIAO_HANG);

            XmlAttribute CONTAINER = doc.CreateAttribute("CONTAINER");
            CONTAINER.Value = ContainerCollection.Count.ToString(); //kiem tra lai  TS DE =0
            ChungTuVanDon.Attributes.Append(CONTAINER);

            XmlAttribute SO_HIEU_PTVT = doc.CreateAttribute("SO_HIEU_PTVT");
            SO_HIEU_PTVT.Value = SoHieuPTVT.Trim();
            ChungTuVanDon.Attributes.Append(SO_HIEU_PTVT);

            XmlAttribute TEN_PTVT = doc.CreateAttribute("TEN_PTVT");
            TEN_PTVT.Value = TenPTVT.Trim();
            ChungTuVanDon.Attributes.Append(TEN_PTVT);

            XmlAttribute SO_HIEU_CHUYEN_DI = doc.CreateAttribute("SO_HIEU_CHUYEN_DI");
            SO_HIEU_CHUYEN_DI.Value = this.SoHieuChuyenDi;
            ChungTuVanDon.Attributes.Append(SO_HIEU_CHUYEN_DI);

            XmlAttribute QUOC_TICH_PTVT = doc.CreateAttribute("QUOC_TICH_PTVT");
            QUOC_TICH_PTVT.Value = QuocTichPTVT.Trim();
            ChungTuVanDon.Attributes.Append(QUOC_TICH_PTVT);


            XmlAttribute TEN_CANG_DO_HANG = doc.CreateAttribute("TEN_CANG_DO_HANG");
            TEN_CANG_DO_HANG.Value = TenCangDoHang.Trim();
            ChungTuVanDon.Attributes.Append(TEN_CANG_DO_HANG);

            XmlAttribute MA_CANG_DO_HANG = doc.CreateAttribute("MA_CANG_DO_HANG");
            MA_CANG_DO_HANG.Value = MaCangDoHang.Trim();
            ChungTuVanDon.Attributes.Append(MA_CANG_DO_HANG);

            XmlAttribute NGAY_DEN_PTVT = doc.CreateAttribute("NGAY_DEN_PTVT");
            NGAY_DEN_PTVT.Value = SoVanDon.Trim() != "" ? NgayDenPTVT.ToString("yyyy-MM-dd") : null;
            ChungTuVanDon.Attributes.Append(NGAY_DEN_PTVT);

            XmlAttribute TEN_CANG_XEP_HANG = doc.CreateAttribute("TEN_CANG_XEP_HANG");
            TEN_CANG_XEP_HANG.Value = TenCangXepHang.Trim();
            ChungTuVanDon.Attributes.Append(TEN_CANG_XEP_HANG);

            XmlAttribute MA_CANG_XEP_HANG = doc.CreateAttribute("MA_CANG_XEP_HANG");
            MA_CANG_XEP_HANG.Value = MaCangXepHang.Trim();
            ChungTuVanDon.Attributes.Append(MA_CANG_XEP_HANG);

            XmlAttribute TEN_HANG_VAN_TAI = doc.CreateAttribute("TEN_HANG_VAN_TAI");
            TEN_HANG_VAN_TAI.Value = TenHangVT.Trim();
            ChungTuVanDon.Attributes.Append(TEN_HANG_VAN_TAI);

            XmlAttribute MA_HANG_VAN_TAI = doc.CreateAttribute("MA_HANG_VAN_TAI");
            MA_HANG_VAN_TAI.Value = MaHangVT.Trim();
            ChungTuVanDon.Attributes.Append(MA_HANG_VAN_TAI);

            XmlAttribute NUOC_XN = doc.CreateAttribute("NUOC_XN");
            NUOC_XN.Value = NuocXuat_ID.Trim();
            ChungTuVanDon.Attributes.Append(NUOC_XN);

            XmlAttribute NOI_PHAT_HANH = doc.CreateAttribute("NOI_PHAT_HANH");
            NOI_PHAT_HANH.Value = this.NoiDi;
            ChungTuVanDon.Attributes.Append(NOI_PHAT_HANH);

            XmlAttribute NGAY_KHOI_HANH = doc.CreateAttribute("NGAY_KHOI_HANH");
            NGAY_KHOI_HANH.Value = SoVanDon.Trim() != "" ? NgayKhoiHanh.ToString("yyyy-MM-dd") : null;
            ChungTuVanDon.Attributes.Append(NGAY_KHOI_HANH);

            XmlAttribute TONG_SO_KIEN = doc.CreateAttribute("TONG_SO_KIEN");
            TONG_SO_KIEN.Value = "";
            ChungTuVanDon.Attributes.Append(TONG_SO_KIEN);

            XmlAttribute KIEU_DONG_GOI = doc.CreateAttribute("KIEU_DONG_GOI");
            KIEU_DONG_GOI.Value = "";
            ChungTuVanDon.Attributes.Append(KIEU_DONG_GOI);

            XmlAttribute MA_PTVT = doc.CreateAttribute("MA_PTVT");
            MA_PTVT.Value = "";
            ChungTuVanDon.Attributes.Append(MA_PTVT);

            if (this.ContainerCollection.Count == null || this.ContainerCollection.Count == 0)
                this.LoadContainerCollection();
            foreach (Container c in ContainerCollection)
            {
                XmlElement CONT = doc.CreateElement("CHUNG_TU_VAN_DON.CONT");
                ChungTuVanDon.AppendChild(CONT);

                XmlAttribute CONTAINER_NO = doc.CreateAttribute("CONTAINER_NO");
                CONTAINER_NO.Value = c.SoHieu;
                CONT.Attributes.Append(CONTAINER_NO);

                XmlAttribute CONTAINER_TYPE = doc.CreateAttribute("CONTAINER_TYPE");
                CONTAINER_TYPE.Value = c.LoaiContainer;
                CONT.Attributes.Append(CONTAINER_TYPE);

                XmlAttribute SEAL_NO = doc.CreateAttribute("SEAL_NO");
                SEAL_NO.Value = c.Seal_No;
                CONT.Attributes.Append(SEAL_NO);

                XmlAttribute TRANG_THAI = doc.CreateAttribute("TRANG_THAI");
                TRANG_THAI.Value = c.Trang_thai.ToString();
                CONT.Attributes.Append(TRANG_THAI);
            }
            #endregion VanTaiDon

            return ChungTuVanDon;
        }

        public static List<VanDon> SelectCollectionDynamic(string whereCondition, string orderByExpression, SqlTransaction trangsaction, SqlDatabase db)
        {
            List<VanDon> collection = new List<VanDon>();

            const string spName = "[dbo].[p_KDT_VanDon_SelectDynamic]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand,trangsaction);
            while (reader.Read())
            {
                VanDon entity = new VanDon();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoVanDon"))) entity.SoVanDon = reader.GetString(reader.GetOrdinal("SoVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayVanDon"))) entity.NgayVanDon = reader.GetDateTime(reader.GetOrdinal("NgayVanDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("HangRoi"))) entity.HangRoi = reader.GetBoolean(reader.GetOrdinal("HangRoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuPTVT"))) entity.SoHieuPTVT = reader.GetString(reader.GetOrdinal("SoHieuPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDenPTVT"))) entity.NgayDenPTVT = reader.GetDateTime(reader.GetOrdinal("NgayDenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHangVT"))) entity.MaHangVT = reader.GetString(reader.GetOrdinal("MaHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHangVT"))) entity.TenHangVT = reader.GetString(reader.GetOrdinal("TenHangVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenPTVT"))) entity.TenPTVT = reader.GetString(reader.GetOrdinal("TenPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("QuocTichPTVT"))) entity.QuocTichPTVT = reader.GetString(reader.GetOrdinal("QuocTichPTVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXuat_ID"))) entity.NuocXuat_ID = reader.GetString(reader.GetOrdinal("NuocXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHang"))) entity.MaNguoiNhanHang = reader.GetString(reader.GetOrdinal("MaNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHang"))) entity.TenNguoiNhanHang = reader.GetString(reader.GetOrdinal("TenNguoiNhanHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiGiaoHang"))) entity.MaNguoiGiaoHang = reader.GetString(reader.GetOrdinal("MaNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiGiaoHang"))) entity.TenNguoiGiaoHang = reader.GetString(reader.GetOrdinal("TenNguoiGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauNhap_ID"))) entity.CuaKhauNhap_ID = reader.GetString(reader.GetOrdinal("CuaKhauNhap_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("CuaKhauXuat"))) entity.CuaKhauXuat = reader.GetString(reader.GetOrdinal("CuaKhauXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguoiNhanHangTrungGian"))) entity.MaNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("MaNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNguoiNhanHangTrungGian"))) entity.TenNguoiNhanHangTrungGian = reader.GetString(reader.GetOrdinal("TenNguoiNhanHangTrungGian"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangXepHang"))) entity.MaCangXepHang = reader.GetString(reader.GetOrdinal("MaCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangXepHang"))) entity.TenCangXepHang = reader.GetString(reader.GetOrdinal("TenCangXepHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaCangDoHang"))) entity.MaCangDoHang = reader.GetString(reader.GetOrdinal("MaCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenCangDoHang"))) entity.TenCangDoHang = reader.GetString(reader.GetOrdinal("TenCangDoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemGiaoHang"))) entity.DiaDiemGiaoHang = reader.GetString(reader.GetOrdinal("DiaDiemGiaoHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoiDi"))) entity.NoiDi = reader.GetString(reader.GetOrdinal("NoiDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHieuChuyenDi"))) entity.SoHieuChuyenDi = reader.GetString(reader.GetOrdinal("SoHieuChuyenDi"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayKhoiHanh"))) entity.NgayKhoiHanh = reader.GetDateTime(reader.GetOrdinal("NgayKhoiHanh"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }



    }
}