﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Components
{
	public partial class KDT_VNACC_HopDongDangKy_ChiTiet : ICloneable
	{
		#region Properties.
		
		public int ID { set; get; }
		public string FileName { set; get; }
		public decimal FileSize { set; get; }
		public byte[] NoiDung { set; get; }
		public int HopDongDangKy_ID { set; get; }
		
		#endregion

		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HopDongDangKy_ChiTiet> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HopDongDangKy_ChiTiet> collection = new List<KDT_VNACC_HopDongDangKy_ChiTiet>();
			while (reader.Read())
			{
				KDT_VNACC_HopDongDangKy_ChiTiet entity = new KDT_VNACC_HopDongDangKy_ChiTiet();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileName"))) entity.FileName = reader.GetString(reader.GetOrdinal("FileName"));
				if (!reader.IsDBNull(reader.GetOrdinal("FileSize"))) entity.FileSize = reader.GetDecimal(reader.GetOrdinal("FileSize"));
				//if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = reader.GetBytes(reader.GetOrdinal("NoiDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("HopDongDangKy_ID"))) entity.HopDongDangKy_ID = reader.GetInt32(reader.GetOrdinal("HopDongDangKy_ID"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HopDongDangKy_ChiTiet> collection, int id)
        {
            foreach (KDT_VNACC_HopDongDangKy_ChiTiet item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HopDongDangKy_ChiTiet VALUES(@FileName, @FileSize, @NoiDung, @HopDongDangKy_ID)";
            string update = "UPDATE t_KDT_VNACC_HopDongDangKy_ChiTiet SET FileName = @FileName, FileSize = @FileSize, NoiDung = @NoiDung, HopDongDangKy_ID = @HopDongDangKy_ID WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HopDongDangKy_ChiTiet WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FileSize", SqlDbType.Decimal, "FileSize", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDung", SqlDbType.Image, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDongDangKy_ID", SqlDbType.Int, "HopDongDangKy_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FileSize", SqlDbType.Decimal, "FileSize", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDung", SqlDbType.Image, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDongDangKy_ID", SqlDbType.Int, "HopDongDangKy_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HopDongDangKy_ChiTiet VALUES(@FileName, @FileSize, @NoiDung, @HopDongDangKy_ID)";
            string update = "UPDATE t_KDT_VNACC_HopDongDangKy_ChiTiet SET FileName = @FileName, FileSize = @FileSize, NoiDung = @NoiDung, HopDongDangKy_ID = @HopDongDangKy_ID WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HopDongDangKy_ChiTiet WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FileSize", SqlDbType.Decimal, "FileSize", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDung", SqlDbType.Image, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HopDongDangKy_ID", SqlDbType.Int, "HopDongDangKy_ID", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FileName", SqlDbType.NVarChar, "FileName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FileSize", SqlDbType.Decimal, "FileSize", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDung", SqlDbType.Image, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HopDongDangKy_ID", SqlDbType.Int, "HopDongDangKy_ID", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HopDongDangKy_ChiTiet Load(int id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_ChiTiet_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HopDongDangKy_ChiTiet> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_HopDongDangKy_ID(int hopDongDangKy_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_ChiTiet_SelectBy_HopDongDangKy_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDongDangKy_ID", SqlDbType.Int, hopDongDangKy_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

        public static List<KDT_VNACC_HopDongDangKy_ChiTiet> SelectCollectionBy_HopDongID(int hopDongID)
        {
            IDataReader reader = SelectReaderBy_HopDongDangKy_ID(hopDongID);
            return ConvertToCollection(reader);
        }
		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_ChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_ChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_ChiTiet_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_ChiTiet_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_HopDongDangKy_ID(int hopDongDangKy_ID)
		{
			const string spName = "p_KDT_VNACC_HopDongDangKy_ChiTiet_SelectBy_HopDongDangKy_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDongDangKy_ID", SqlDbType.Int, hopDongDangKy_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertKDT_VNACC_HopDongDangKy_ChiTiet(string fileName, decimal fileSize, byte[] noiDung, int hopDongDangKy_ID)
		{
			KDT_VNACC_HopDongDangKy_ChiTiet entity = new KDT_VNACC_HopDongDangKy_ChiTiet();	
			entity.FileName = fileName;
			entity.FileSize = fileSize;
			entity.NoiDung = noiDung;
			entity.HopDongDangKy_ID = hopDongDangKy_ID;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_ChiTiet_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@FileName", SqlDbType.NVarChar, FileName);
			db.AddInParameter(dbCommand, "@FileSize", SqlDbType.Decimal, FileSize);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, NoiDung);
			db.AddInParameter(dbCommand, "@HopDongDangKy_ID", SqlDbType.Int, HopDongDangKy_ID);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HopDongDangKy_ChiTiet(int id, string fileName, decimal fileSize, byte[] noiDung, int hopDongDangKy_ID)
		{
			KDT_VNACC_HopDongDangKy_ChiTiet entity = new KDT_VNACC_HopDongDangKy_ChiTiet();			
			entity.ID = id;
			entity.FileName = fileName;
			entity.FileSize = fileSize;
			entity.NoiDung = noiDung;
			entity.HopDongDangKy_ID = hopDongDangKy_ID;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HopDongDangKy_ChiTiet_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@FileName", SqlDbType.NVarChar, FileName);
			db.AddInParameter(dbCommand, "@FileSize", SqlDbType.Decimal, FileSize);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, NoiDung);
			db.AddInParameter(dbCommand, "@HopDongDangKy_ID", SqlDbType.Int, HopDongDangKy_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HopDongDangKy_ChiTiet(int id, string fileName, decimal fileSize, byte[] noiDung, int hopDongDangKy_ID)
		{
			KDT_VNACC_HopDongDangKy_ChiTiet entity = new KDT_VNACC_HopDongDangKy_ChiTiet();			
			entity.ID = id;
			entity.FileName = fileName;
			entity.FileSize = fileSize;
			entity.NoiDung = noiDung;
			entity.HopDongDangKy_ID = hopDongDangKy_ID;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_ChiTiet_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@FileName", SqlDbType.NVarChar, FileName);
			db.AddInParameter(dbCommand, "@FileSize", SqlDbType.Decimal, FileSize);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.Image, NoiDung);
			db.AddInParameter(dbCommand, "@HopDongDangKy_ID", SqlDbType.Int, HopDongDangKy_ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HopDongDangKy_ChiTiet(int id)
		{
			KDT_VNACC_HopDongDangKy_ChiTiet entity = new KDT_VNACC_HopDongDangKy_ChiTiet();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_ChiTiet_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_HopDongDangKy_ID(int hopDongDangKy_ID)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_ChiTiet_DeleteBy_HopDongDangKy_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@HopDongDangKy_ID", SqlDbType.Int, hopDongDangKy_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HopDongDangKy_ChiTiet_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}