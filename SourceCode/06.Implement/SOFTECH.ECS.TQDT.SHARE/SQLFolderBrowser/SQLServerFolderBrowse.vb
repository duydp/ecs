Option Strict On
Option Explicit On 

Imports System.Data.SqlClient

Public Class SQLServerFolderBrowse

#Region " Variables "
    'Connection property
    Private strServer As String
    Private strUsername As String
    Private strPassword As String
    Private lngTimeout As Long = 600

    'Connection variables
    Private sqlcFolders As SqlConnection    'to establish Connection
    Private drFolders As SqlDataReader      ' Since we only need to read the folder structure using a light weight provider makes sense.
    Private sqlFixedDrives As SqlCommand    'Sql command to get all the fixed drives on the MSSQL computer
    Private sqlAvailableDrives As SqlCommand    'Sql command to get all the available drives on the MSSQL computer
    Private sqlSubFolders As SqlCommand     'Sql command to get all the sub folder name for the select path on the MSSQL computer
#End Region

#Region " Constants "
    'Constants
    Private Const strFixedDrivesSP As String = "xp_fixeddrives"         'stored procedure to get fixed drives, exists in master database
    Private Const strAvailableDrivesSP As String = "xp_availablemedia"  'stored procedure to get available drives, exists in master database
    Private Const strSubFoldersSP As String = "xp_subdirs"              'stored procedure to get sub directories, exists in master database
    Private Const strDatabase As String = "Master"                      'database to connect to to execute the above three stored procedures
#End Region

#Region " Properties "
    Public WriteOnly Property Server() As String
        Set(ByVal Value As String)
            strServer = Value
        End Set
    End Property

    Public WriteOnly Property Username() As String
        Set(ByVal Value As String)
            strUsername = Value
        End Set
    End Property

    Public WriteOnly Property Password() As String
        Set(ByVal Value As String)
            strPassword = Value
        End Set
    End Property

    Public WriteOnly Property Timeout() As Long
        Set(ByVal Value As Long)
            lngTimeout = Value
        End Set
    End Property
#End Region

#Region " Constructor And Destructor "
    Public Sub New()
        Try
            sqlcFolders = New SqlConnection
            sqlFixedDrives = New SqlCommand(strFixedDrivesSP)
            sqlAvailableDrives = New SqlCommand(strAvailableDrivesSP)
            sqlSubFolders = New SqlCommand
        Catch ex As Exception
            Debug.WriteLine("Initialization Error - " & ex.Message)
        Finally
        End Try
    End Sub

    Protected Overrides Sub Finalize()
        Try
            sqlcFolders = Nothing
            drFolders = Nothing
            sqlFixedDrives = Nothing
            sqlAvailableDrives = Nothing
            sqlSubFolders = Nothing

            MyBase.Finalize()
        Catch ex As Exception
        Finally
        End Try
    End Sub
#End Region

#Region " Functions "
    Public Function GetFixedDrives() As String()
        Dim strList() As String
        Dim intCounter As Integer
        Try
            InitializeConnection()
            sqlFixedDrives.Connection = sqlcFolders
            sqlFixedDrives.CommandType = CommandType.StoredProcedure
            sqlcFolders.Open()
            drFolders = sqlFixedDrives.ExecuteReader
            While drFolders.Read    'Populate the tree view with the names of fixed drives
                ReDim Preserve strList(intCounter)
                strList(intCounter) = drFolders.GetString(0)
                intCounter += 1
            End While

            sqlcFolders.Close()
            sqlcFolders.Dispose()

            Return strList
        Catch ex As Exception
            Debug.WriteLine("Get Fixed Drives Error - " & ex.Message)
        Finally
            drFolders.Close()
            sqlcFolders.Close()
            drFolders = Nothing
        End Try
    End Function

    Public Function GetAvailableDrives() As String()
        Dim strList() As String
        Dim intCounter As Integer
        Try
            InitializeConnection()
            sqlAvailableDrives.Connection = sqlcFolders
            sqlAvailableDrives.CommandType = CommandType.StoredProcedure
            sqlcFolders.Open()
            drFolders = sqlAvailableDrives.ExecuteReader
            While drFolders.Read    'Popualte the tree view with the names of available drives
                ReDim Preserve strList(intCounter)
                strList(intCounter) = drFolders.GetString(0)
                intCounter += 1
            End While

            sqlcFolders.Close()
            sqlcFolders.Dispose()

            Return strList
        Catch ex As Exception
            Debug.WriteLine("Get Available Drives Error - " & ex.Message)
        Finally
            drFolders.Close()
            sqlcFolders.Close()
            drFolders = Nothing
        End Try
    End Function

    Public Function GetSubFolders(ByVal ParentFolder As String) As String()
        Dim strList() As String
        Dim intCounter As Integer
        Try
            InitializeConnection()
            sqlSubFolders.Connection = sqlcFolders
            sqlSubFolders.CommandText = strSubFoldersSP & " '" & ParentFolder & "'"

            sqlcFolders.Open()
            drFolders = sqlSubFolders.ExecuteReader
            While drFolders.Read    'Populate the sub tree with the sub folder names
                ReDim Preserve strList(intCounter)
                strList(intCounter) = drFolders.GetString(0)
                intCounter += 1
            End While

            sqlcFolders.Close()
            sqlcFolders.Dispose()

            Return strList
        Catch ex As Exception
            Err.Clear()
        Finally
            sqlcFolders.Close()
            drFolders = Nothing
        End Try
    End Function

    Private Sub InitializeConnection()
        Try
            'Create connection string if one does not exist
            If sqlcFolders.ConnectionString.Length = 0 Then
                sqlcFolders.ConnectionString = "POOLING=FALSE;USER ID=" & strUsername & ";PASSWORD=" & strPassword & _
                                               ";DATABASE=" & strDatabase & ";SERVER=" & strServer & _
                                               ";CONNECTION TIMEOUT=" & lngTimeout
            End If
        Catch ex As Exception
            Debug.WriteLine("Connection Initialization Error - " & ex.Message)
        End Try
    End Sub
#End Region

End Class
