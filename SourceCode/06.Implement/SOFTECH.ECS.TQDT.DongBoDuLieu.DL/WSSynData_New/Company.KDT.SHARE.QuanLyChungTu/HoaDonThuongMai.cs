﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Transactions;
using System.Globalization;
using System.Xml;
using Company.KDT.SHARE.Components.WS;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Utils;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class HoaDonThuongMai
    {

        public static HoaDonThuongMai Load(string maDoanhNghiep, string soHoaDon, DateTime ngayHoaDon, long idTKMD, SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMai_LoadBy]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, soHoaDon);
            db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, ngayHoaDon);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, maDoanhNghiep);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, idTKMD);

            HoaDonThuongMai entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand, transaction);
            if (reader.Read())
            {
                entity = new HoaDonThuongMai();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHoaDon"))) entity.SoHoaDon = reader.GetString(reader.GetOrdinal("SoHoaDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoaDon"))) entity.NgayHoaDon = reader.GetDateTime(reader.GetOrdinal("NgayHoaDon"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("PTTT_ID"))) entity.PTTT_ID = reader.GetString(reader.GetOrdinal("PTTT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("DKGH_ID"))) entity.DKGH_ID = reader.GetString(reader.GetOrdinal("DKGH_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViMua"))) entity.MaDonViMua = reader.GetString(reader.GetOrdinal("MaDonViMua"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViMua"))) entity.TenDonViMua = reader.GetString(reader.GetOrdinal("TenDonViMua"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDonViBan"))) entity.MaDonViBan = reader.GetString(reader.GetOrdinal("MaDonViBan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDonViBan"))) entity.TenDonViBan = reader.GetString(reader.GetOrdinal("TenDonViBan"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
            }
            reader.Close();
            return entity;
        }

        public List<HoaDonThuongMaiDetail> ListHangMDOfHoaDon = new List<HoaDonThuongMaiDetail>();

        public void LoadListHangDMOfHoaDon()
        {
            ListHangMDOfHoaDon = HoaDonThuongMaiDetail.SelectCollectionBy_HoaDonTM_ID(this.ID);
        }

        public DataTable ConvertListToDataSet(DataTable dsHMD)
        {
            if (ListHangMDOfHoaDon != null && ListHangMDOfHoaDon.Count == 0)
            {
                LoadListHangDMOfHoaDon();
            }

            DataTable tmp = dsHMD.Copy();
            try
            {
                tmp.Rows.Clear();
                tmp.Columns.Add("GiaTriDieuChinhTang", typeof(decimal));
                tmp.Columns.Add("GiaiTriDieuChinhGiam", typeof(decimal));
                tmp.Columns.Add("GhiChu");

                dsHMD.Columns.Add("GhiChu");
                dsHMD.Columns.Add("GiaTriDieuChinhTang", typeof(decimal));
                dsHMD.Columns.Add("GiaiTriDieuChinhGiam", typeof(decimal));

                dsHMD.Columns.Add("HMD_ID");
                tmp.Columns["ID"].ColumnName = "HMD_ID";
                tmp.Columns["HMD_ID"].Caption = "HMD_ID";
                tmp.Columns.Add("ID");

                foreach (HoaDonThuongMaiDetail item in ListHangMDOfHoaDon)
                {
                    DataRow[] row = dsHMD.Select("ID=" + item.HMD_ID);
                    if (row != null && row.Length != 0)
                    {
                        row[0]["GhiChu"] = item.GhiChu;
                        row[0]["GiaTriDieuChinhTang"] = item.GiaTriDieuChinhTang;
                        row[0]["GiaiTriDieuChinhGiam"] = item.GiaiTriDieuChinhGiam;
                        row[0]["HMD_ID"] = row[0]["ID"];
                        row[0]["ID"] = item.ID.ToString();

                        row[0]["SoThuTuHang"] = item.SoThuTuHang;
                        row[0]["MaHS"] = item.MaHS;
                        row[0]["MaPhu"] = item.MaPhu;
                        row[0]["TenHang"] = item.TenHang;
                        row[0]["NuocXX_ID"] = item.NuocXX_ID;
                        row[0]["DVT_ID"] = item.DVT_ID;
                        row[0]["SoLuong"] = item.SoLuong;
                        row[0]["DonGiaKB"] = item.DonGiaKB;
                        row[0]["TriGiaKB"] = item.TriGiaKB;

                        tmp.ImportRow(row[0]);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

            return tmp;

        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMai_Insert]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
            db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year <= 1753 ? DBNull.Value : (object)NgayHoaDon);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.Char, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@MaDonViMua", SqlDbType.VarChar, MaDonViMua);
            db.AddInParameter(dbCommand, "@TenDonViMua", SqlDbType.NVarChar, TenDonViMua);
            db.AddInParameter(dbCommand, "@MaDonViBan", SqlDbType.VarChar, MaDonViBan);
            db.AddInParameter(dbCommand, "@TenDonViBan", SqlDbType.NVarChar, TenDonViBan);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction, SqlDatabase db)
        {
            const string spName = "[dbo].[p_KDT_HoaDonThuongMai_Update]";
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoHoaDon", SqlDbType.VarChar, SoHoaDon);
            db.AddInParameter(dbCommand, "@NgayHoaDon", SqlDbType.DateTime, NgayHoaDon.Year == 1753 ? DBNull.Value : (object)NgayHoaDon);
            db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, NguyenTe_ID);
            db.AddInParameter(dbCommand, "@PTTT_ID", SqlDbType.Char, PTTT_ID);
            db.AddInParameter(dbCommand, "@DKGH_ID", SqlDbType.VarChar, DKGH_ID);
            db.AddInParameter(dbCommand, "@MaDonViMua", SqlDbType.VarChar, MaDonViMua);
            db.AddInParameter(dbCommand, "@TenDonViMua", SqlDbType.NVarChar, TenDonViMua);
            db.AddInParameter(dbCommand, "@MaDonViBan", SqlDbType.VarChar, MaDonViBan);
            db.AddInParameter(dbCommand, "@TenDonViBan", SqlDbType.NVarChar, TenDonViBan);
            db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1753 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    long id = this.ID;
                    try
                    {
                        if (this.ID == 0)
                            this.Insert(transaction);
                        else
                            this.Update(transaction);
                        foreach (HoaDonThuongMaiDetail item in ListHangMDOfHoaDon)
                        {
                            item.HoaDonTM_ID = this.ID;
                            if (id == 0)
                                item.ID = 0;

                            if (item.ID == 0)
                                item.Insert(transaction);
                            else
                                item.Update(transaction);
                        }
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        this.ID = id;
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
        }

        /*
        public void InsertUpdateFull(SqlTransaction transaction, SqlDatabase db)
        {
            try
            {
                //Kiem tra ton tai HoaDonThuongMai tren Database Target
                HoaDonThuongMai hoaDonTemp = HoaDonThuongMai.Load(this._MaDoanhNghiep, this._SoHoaDon, this._NgayHoaDon, this._TKMD_ID, transaction, db);

                if (hoaDonTemp == null || hoaDonTemp.ID == 0)
                    this.ID = this.InsertTransaction(transaction, db);
                else
                {
                    this.ID = hoaDonTemp.ID;
                    this.UpdateTransaction(transaction, db);
                }

                foreach (HoaDonThuongMaiDetail item in ListHangMDOfHoaDon)
                {
                    item.HoaDonTM_ID = this.ID;

                    HoaDonThuongMaiDetail.DeleteBy_HoaDonTM_ID(item.HoaDonTM_ID, transaction, db);

                    HangMauDich hmd = new HangMauDich();

                    item.InsertUpdate(transaction, db);
                }
                transaction.Commit();
            }
            catch (Exception ex)
            {
                transaction.Rollback();

                throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
            }
        }
        */

        public int DeleteAll()
        {
            bool ret = true;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        HoaDonThuongMaiDetail.DeleteBy_HoaDonTM_ID(this.ID, transaction);
                        this.Delete(transaction);
                        transaction.Commit();
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return 1;
        }

        public static XmlNode ConvertCollectionHoaDonThuongMaiToXML_TKX(XmlDocument doc, List<HoaDonThuongMai> HoaDonThuongMaiCollection, long TKMD_ID, List<HangMauDich> listHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            if (HoaDonThuongMaiCollection == null || HoaDonThuongMaiCollection.Count == 0)
            {
                HoaDonThuongMaiCollection = (List<HoaDonThuongMai>)HoaDonThuongMai.SelectCollectionBy_TKMD_ID(TKMD_ID);
            }
            XmlElement CHUNG_TU_HOADON = doc.CreateElement("CHUNG_TU_HOADON");
            foreach (HoaDonThuongMai HoaDonTM in HoaDonThuongMaiCollection)
            {
                XmlElement hopdongTMItem = doc.CreateElement("CHUNG_TU_HOADON.ITEM");
                CHUNG_TU_HOADON.AppendChild(hopdongTMItem);

                XmlAttribute SO_CT = doc.CreateAttribute("SO_CT");
                SO_CT.Value = FontConverter.Unicode2TCVN(HoaDonTM.SoHoaDon.Trim());
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = doc.CreateAttribute("NGAY_CT");
                NGAY_CT.Value = HoaDonTM.NgayHoaDon.ToString("MM/dd/yyyy");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute MA_GH = doc.CreateAttribute("MA_GH");
                MA_GH.Value = HoaDonTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute MA_PTTT = doc.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = HoaDonTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = doc.CreateAttribute("MA_NT");
                MA_NT.Value = HoaDonTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);

                XmlAttribute MA_DV_DT = doc.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = HoaDonTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = doc.CreateAttribute("TEN_DV_DT");
                TEN_DV_DT.Value = FontConverter.Unicode2TCVN(HoaDonTM.TenDonViMua.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute MA_DV = doc.CreateAttribute("MA_DV");
                MA_DV.Value = HoaDonTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = doc.CreateAttribute("TEN_DV");
                TEN_DV.Value = FontConverter.Unicode2TCVN(HoaDonTM.TenDonViBan.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute GHI_CHU = doc.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = FontConverter.Unicode2TCVN(HoaDonTM.ThongTinKhac.Trim());
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (HoaDonTM.ListHangMDOfHoaDon == null || HoaDonTM.ListHangMDOfHoaDon.Count == 0)
                {
                    HoaDonTM.LoadListHangDMOfHoaDon();
                }

                foreach (HoaDonThuongMaiDetail hangHoaDon in HoaDonTM.ListHangMDOfHoaDon)
                {

                    HangMauDich hmd = BaseClass.GetHangMauDichByIDHMD(hangHoaDon.HMD_ID, listHang);

                    if (hmd == null) continue;

                    //Cap nhat thong tin hang thay doi cua rieng giay phep.
                    if (hmd != null)
                    {
                        hmd.MaHS = hangHoaDon.MaHS;
                        hmd.MaPhu = hangHoaDon.MaPhu;
                        hmd.TenHang = hangHoaDon.TenHang;
                        hmd.NuocXX_ID = hangHoaDon.NuocXX_ID;
                        hmd.DVT_ID = hangHoaDon.DVT_ID;
                        hmd.SoLuong = hangHoaDon.SoLuong;
                        hmd.DonGiaKB = hangHoaDon.DonGiaKB;
                        hmd.TriGiaKB = hangHoaDon.TriGiaKB;
                    }

                    XmlElement hangItem = doc.CreateElement("CHUNG_TU_HOADON.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = doc.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaPhu.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = doc.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = doc.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute NUOC_XX = doc.CreateAttribute("NUOC_XX");
                    NUOC_XX.Value = hmd.NuocXX_ID;
                    hangItem.Attributes.Append(NUOC_XX);

                    XmlAttribute MA_DVT = doc.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.DVT_ID.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = doc.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = doc.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = doc.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GIATRITANG = doc.CreateAttribute("GIATRITANG");
                    GIATRITANG.Value = BaseClass.Round(hangHoaDon.GiaTriDieuChinhTang, 9);
                    hangItem.Attributes.Append(GIATRITANG);

                    XmlAttribute GIATRIGIAM = doc.CreateAttribute("GIATRIGIAM");
                    GIATRIGIAM.Value = BaseClass.Round(hangHoaDon.GiaiTriDieuChinhGiam, 9);
                    hangItem.Attributes.Append(GIATRIGIAM);

                    XmlAttribute GHI_CHUhang = doc.CreateAttribute("GHI_CHU");
                    GHI_CHUhang.Value = FontConverter.Unicode2TCVN(hangHoaDon.GhiChu != null ? hangHoaDon.GhiChu.Trim() : "");
                    hangItem.Attributes.Append(GHI_CHUhang);
                }
            }
            if (CHUNG_TU_HOADON.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_HOADON;
        }

        public static XmlNode ConvertCollectionHoaDonThuongMaiToXML_TKN(XmlDocument doc, List<HoaDonThuongMai> HoaDonThuongMaiCollection, long TKMD_ID, List<HangMauDich> listHang)
        {
            NumberFormatInfo f = new NumberFormatInfo();
            f.NumberDecimalSeparator = ".";
            f.NumberGroupSeparator = ",";
            if (HoaDonThuongMaiCollection == null || HoaDonThuongMaiCollection.Count == 0)
            {
                HoaDonThuongMaiCollection = (List<HoaDonThuongMai>)HoaDonThuongMai.SelectCollectionBy_TKMD_ID(TKMD_ID);
            }
            XmlElement CHUNG_TU_HOADON = doc.CreateElement("CHUNG_TU_HOADON");
            foreach (HoaDonThuongMai HoaDonTM in HoaDonThuongMaiCollection)
            {
                XmlElement hopdongTMItem = doc.CreateElement("CHUNG_TU_HOADON.ITEM");
                CHUNG_TU_HOADON.AppendChild(hopdongTMItem);

                XmlAttribute SO_CT = doc.CreateAttribute("SO_CT");
                SO_CT.Value = FontConverter.Unicode2TCVN(HoaDonTM.SoHoaDon.Trim());
                hopdongTMItem.Attributes.Append(SO_CT);

                XmlAttribute NGAY_CT = doc.CreateAttribute("NGAY_CT");
                NGAY_CT.Value = HoaDonTM.NgayHoaDon.ToString("MM/dd/yyyy");
                hopdongTMItem.Attributes.Append(NGAY_CT);

                XmlAttribute MA_GH = doc.CreateAttribute("MA_GH");
                MA_GH.Value = HoaDonTM.DKGH_ID;
                hopdongTMItem.Attributes.Append(MA_GH);

                XmlAttribute MA_PTTT = doc.CreateAttribute("MA_PTTT");
                MA_PTTT.Value = HoaDonTM.PTTT_ID; ;
                hopdongTMItem.Attributes.Append(MA_PTTT);

                XmlAttribute MA_NT = doc.CreateAttribute("MA_NT");
                MA_NT.Value = HoaDonTM.NguyenTe_ID;
                hopdongTMItem.Attributes.Append(MA_NT);

                XmlAttribute MA_DV = doc.CreateAttribute("MA_DV");
                MA_DV.Value = HoaDonTM.MaDonViMua.Trim();
                hopdongTMItem.Attributes.Append(MA_DV);

                XmlAttribute TEN_DV = doc.CreateAttribute("TEN_DV");
                TEN_DV.Value = FontConverter.Unicode2TCVN(HoaDonTM.TenDonViMua.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV);

                XmlAttribute MA_DV_DT = doc.CreateAttribute("MA_DV_DT");
                MA_DV_DT.Value = HoaDonTM.MaDonViBan.Trim();
                hopdongTMItem.Attributes.Append(MA_DV_DT);

                XmlAttribute TEN_DV_DT = doc.CreateAttribute("TEN_DV_DT");
                TEN_DV_DT.Value = FontConverter.Unicode2TCVN(HoaDonTM.TenDonViBan.Trim());
                hopdongTMItem.Attributes.Append(TEN_DV_DT);

                XmlAttribute GHI_CHU = doc.CreateAttribute("GHI_CHU");
                GHI_CHU.Value = FontConverter.Unicode2TCVN(HoaDonTM.ThongTinKhac.Trim());
                hopdongTMItem.Attributes.Append(GHI_CHU);

                if (HoaDonTM.ListHangMDOfHoaDon == null || HoaDonTM.ListHangMDOfHoaDon.Count == 0)
                {
                    HoaDonTM.LoadListHangDMOfHoaDon();
                }

                foreach (HoaDonThuongMaiDetail hangHoaDon in HoaDonTM.ListHangMDOfHoaDon)
                {

                    HangMauDich hmd = BaseClass.GetHangMauDichByIDHMD(hangHoaDon.HMD_ID, listHang);

                    if (hmd == null) continue;

                    //Cap nhat thong tin hang thay doi cua rieng giay phep.
                    if (hmd != null)
                    {
                        hmd.MaHS = hangHoaDon.MaHS;
                        hmd.MaPhu = hangHoaDon.MaPhu;
                        hmd.TenHang = hangHoaDon.TenHang;
                        hmd.NuocXX_ID = hangHoaDon.NuocXX_ID;
                        hmd.DVT_ID = hangHoaDon.DVT_ID;
                        hmd.SoLuong = hangHoaDon.SoLuong;
                        hmd.DonGiaKB = hangHoaDon.DonGiaKB;
                        hmd.TriGiaKB = hangHoaDon.TriGiaKB;
                    }

                    XmlElement hangItem = doc.CreateElement("CHUNG_TU_HOADON.HANG");
                    hopdongTMItem.AppendChild(hangItem);

                    XmlAttribute MA_NPL_SP = doc.CreateAttribute("MA_NPL_SP");
                    MA_NPL_SP.Value = hmd.MaPhu.Trim();
                    hangItem.Attributes.Append(MA_NPL_SP);

                    XmlAttribute TEN_HANG = doc.CreateAttribute("TEN_HANG");
                    TEN_HANG.Value = FontConverter.Unicode2TCVN(hmd.TenHang.Trim());
                    hangItem.Attributes.Append(TEN_HANG);

                    XmlAttribute MA_HANGKB = doc.CreateAttribute("MA_HANGKB");
                    MA_HANGKB.Value = hmd.MaHS.Trim();
                    hangItem.Attributes.Append(MA_HANGKB);

                    XmlAttribute NUOC_XX = doc.CreateAttribute("NUOC_XX");
                    NUOC_XX.Value = hmd.NuocXX_ID;
                    hangItem.Attributes.Append(NUOC_XX);

                    XmlAttribute MA_DVT = doc.CreateAttribute("MA_DVT");
                    MA_DVT.Value = hmd.DVT_ID.Trim();
                    hangItem.Attributes.Append(MA_DVT);

                    XmlAttribute LUONG = doc.CreateAttribute("LUONG");
                    LUONG.Value = BaseClass.Round(hmd.SoLuong, 9);
                    hangItem.Attributes.Append(LUONG);

                    XmlAttribute DGIA_KB = doc.CreateAttribute("DGIA_KB");
                    DGIA_KB.Value = BaseClass.Round(hmd.DonGiaKB, 9);
                    hangItem.Attributes.Append(DGIA_KB);

                    XmlAttribute TRIGIA_KB = doc.CreateAttribute("TRIGIA_KB");
                    TRIGIA_KB.Value = BaseClass.Round(hmd.TriGiaKB, 9);
                    hangItem.Attributes.Append(TRIGIA_KB);

                    XmlAttribute GIATRITANG = doc.CreateAttribute("GIATRITANG");
                    GIATRITANG.Value = BaseClass.Round(hangHoaDon.GiaTriDieuChinhTang, 9);
                    hangItem.Attributes.Append(GIATRITANG);

                    XmlAttribute GIATRIGIAM = doc.CreateAttribute("GIATRIGIAM");
                    GIATRIGIAM.Value = BaseClass.Round(hangHoaDon.GiaiTriDieuChinhGiam, 9);
                    hangItem.Attributes.Append(GIATRIGIAM);

                    XmlAttribute GHI_CHUhang = doc.CreateAttribute("GHI_CHU");
                    GHI_CHUhang.Value = FontConverter.Unicode2TCVN(hangHoaDon.GhiChu != null ? hangHoaDon.GhiChu.Trim() : "");
                    hangItem.Attributes.Append(GHI_CHUhang);
                }
            }
            if (CHUNG_TU_HOADON.ChildNodes.Count == 0)
                return null;
            return CHUNG_TU_HOADON;
        }

        public string send(string password, string maHQ, long soTK, string maLH, int namDK, long TKMD_ID, List<HangMauDich> listHang)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\KhaiBoSung.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GuidStr;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;


            XmlNode nodeTO_KHAI = doc.GetElementsByTagName("TO_KHAI")[0];
            nodeTO_KHAI.ChildNodes[0].Attributes["MA_LH"].Value = maLH;
            nodeTO_KHAI.ChildNodes[0].Attributes["MA_HQ"].Value = maHQ;
            nodeTO_KHAI.ChildNodes[0].Attributes["SOTK"].Value = soTK + "";
            nodeTO_KHAI.ChildNodes[0].Attributes["NAMDK"].Value = namDK + "";


            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            List<HoaDonThuongMai> listHoaDonThuongMai = new List<HoaDonThuongMai>();
            listHoaDonThuongMai.Add(this);
            nodeDulieu.AppendChild(HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML_TKN(doc, listHoaDonThuongMai, TKMD_ID, listHang));


            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {
                kq = kdt.Send(doc.InnerXml, password);
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            XmlDocument docResult = new XmlDocument();
            docResult.LoadXml(kq);
            if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
            {
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                {
                    XmlNode node1 = doc.ChildNodes[1].SelectSingleNode("Body/Content/Root");
                    XmlNode nodeRoot = doc.ChildNodes[1].SelectSingleNode("Body/Content");
                    //nodeRoot.RemoveChild(node1);
                    return doc.InnerXml;
                }
            }
            else
            {
                throw new Exception(FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL");
            }
            return "";

        }

        public string LayPhanHoi(string password, string maHQ)
        {
            XmlDocument doc = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            doc.Load(path + @"\XMLChungTu\LayPhanHoiDaDuyet.xml");
            //set thong tin hai quan den
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = maHQ;

            ////set thong so gui di subject
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)MessageTypes.ToKhaiNhap).ToString();
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)MessageFunctions.KhaiBao).ToString();

            XmlNode nodeReference = nodeSubject.ChildNodes[2];


            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = GuidStr;

            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = this.MaDoanhNghiep;
            if (GuidStr == "")
            {
                GuidStr = (System.Guid.NewGuid().ToString()); ;
                nodeReference.InnerText = this.GuidStr;
                this.Update();
            }

            XmlNode nodeThongTin = doc.GetElementsByTagName("THONG_TIN")[0];
            nodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = this.MaDoanhNghiep;
            nodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = this.MaDoanhNghiep;

            nodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHQ;
            nodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHQ;




            XmlNode nodeDulieu = doc.GetElementsByTagName("DU_LIEU")[0];
            nodeDulieu.Attributes[0].Value = this.GuidStr;

            KDTService kdt = WebService.GetWS();

            string kq = "";
            int i = 0;
            XmlNode Result = null;
            for (i = 1; i <= 1; ++i)
            {
                try
                {

                    System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                    kq = kdt.Send(doc.InnerXml, password);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
                }
                XmlDocument docNPL = new XmlDocument();
                docNPL.LoadXml(kq);
                Result = docNPL.SelectSingleNode("Envelope/Body/Content/Root");
                if (Result.Attributes["Err"].Value == "no")
                {
                    if (Result.Attributes["TrangThai"].Value == "yes")
                    {
                        break;
                    }
                }
                else
                {
                    if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
                    {
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
                    }
                    else
                        throw new Exception(FontConverter.TCVN2Unicode(docNPL.SelectSingleNode("Envelope/Body/Content/Root").InnerText));
                }
            }

            if (i > 1)
                return doc.InnerXml;
            if (Result.Attributes["Ok"] != null && Result.Attributes["Ok"].Value != "yes")
            {
                throw new Exception(FontConverter.TCVN2Unicode(doc.SelectSingleNode("Envelope/Body/Content/Root").InnerText + "|" + ""));
            }
            return "";
        }

        public bool WSKhaiBaoHoaDonTM(string password, string maHaiQuan, long soToKhai, string maLoaiHinh, int namDangKy, long TKMD_ID, string maDoanhNghiep, List<HoaDonThuongMaiDetail> list, MessageTypes messageType, MessageFunctions messageFunction, List<HangMauDich> listHang)
        {
            XmlDocument xmlDocument = new XmlDocument();
            string path = BaseClass.GetPathProgram();
            xmlDocument.Load(path + @"\XMLChungTu\KhaiBoSung.xml");

            // Message ID.
            XmlNode xmlNodeMessageID = xmlDocument.GetElementsByTagName("messageId")[0];
            xmlNodeMessageID.InnerText = Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode xmlNodeFrom = xmlDocument.GetElementsByTagName("From")[0];

            XmlNode xmlNodeFromName = xmlNodeFrom.ChildNodes[0];
            xmlNodeFromName.InnerText = maDoanhNghiep;
            XmlNode xmlNodeFromIdentity = xmlNodeFrom.ChildNodes[1];
            xmlNodeFromIdentity.InnerText = maDoanhNghiep;

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode xmlNodeTo = xmlDocument.GetElementsByTagName("To")[0];

            XmlNode xmlNodeToName = xmlNodeTo.ChildNodes[0];
            xmlNodeToName.InnerText = maHaiQuan;
            XmlNode xmlNodeToIdentity = xmlNodeTo.ChildNodes[1];
            xmlNodeToIdentity.InnerText = maHaiQuan;

            // Subject.
            XmlNode xmlNodeSubject = xmlDocument.GetElementsByTagName("Subject")[0];

            // Message Type.
            XmlNode xmlNodeType = xmlNodeSubject.ChildNodes[0];
            xmlNodeType.InnerText = ((int)messageType).ToString();

            // Message Function.
            XmlNode xmlNodeFunction = xmlNodeSubject.ChildNodes[1];
            xmlNodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference.
            XmlNode nodeReference = xmlNodeSubject.ChildNodes[2];
            nodeReference.InnerText = this.GuidStr;

            XmlNode xmlNodeThongTin = xmlDocument.GetElementsByTagName("THONG_TIN")[0];
            xmlNodeThongTin.ChildNodes[0].Attributes["MA_DV"].Value = maDoanhNghiep;
            xmlNodeThongTin.ChildNodes[0].Attributes["TEN_DV"].Value = maDoanhNghiep;

            xmlNodeThongTin.ChildNodes[1].Attributes["MA_HQ"].Value = maHaiQuan;
            xmlNodeThongTin.ChildNodes[1].Attributes["TEN_HQ"].Value = maHaiQuan;

            XmlNode xmlNodeToKhai = xmlDocument.GetElementsByTagName("TO_KHAI")[0];

            xmlNodeToKhai.Attributes["MA_LH"].Value = maLoaiHinh;
            xmlNodeToKhai.Attributes["MA_HQ"].Value = maHaiQuan.Trim();
            xmlNodeToKhai.Attributes["SOTK"].Value = soToKhai + "";
            xmlNodeToKhai.Attributes["NAMDK"].Value = namDangKy + "";


            XmlNode xmlNodeDulieu = xmlDocument.GetElementsByTagName("DU_LIEU")[0];
            List<HoaDonThuongMai> listHoaDonThuongMai = new List<HoaDonThuongMai>();
            listHoaDonThuongMai.Add(this);
            xmlNodeDulieu.AppendChild(HoaDonThuongMai.ConvertCollectionHoaDonThuongMaiToXML_TKN(xmlDocument, listHoaDonThuongMai, TKMD_ID, listHang));

            //Service
            KDTService kdt = WebService.GetWS();

            string kq = "";
            try
            {

                // Khai báo.
                kq = kdt.Send(xmlDocument.InnerXml, password);
                xmlDocument.InnerXml.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungHoaDonTM);

            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + ex.Message + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument docResult = new XmlDocument();
                docResult.LoadXml(kq);
                if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["Err"].Value == "no")
                {
                    if (docResult.SelectSingleNode("Envelope/Body/Content/Root").Attributes["TrangThai"].Value == "yes")
                    {
                        //string noiDung = FontConverter.TCVN2Unicode(docResult.GetElementsByTagName("Root")[0].InnerText.Trim());
                        //kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungLayPhanHoiHoaDonTM, noiDung);
                        return true;
                    }
                }
                else
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.Error);
                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(docResult.SelectSingleNode("Envelope/Body/Content/Root").InnerText) + "|" + "WS_LEVEL";
                    throw new Exception();
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                {
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                    Logger.LocalLogger.Instance().WriteMessage(new Exception(xmlDocument.InnerXml));
                }
                throw ex;

            }
            return false;

        }

        public string GenerateToXmlEnvelope(MessageTypes messageType, MessageFunctions messageFunction, Guid referenceId, string path, string maHQ, string maDN)
        {
            XmlDocument doc = new XmlDocument();
            //string path = EntityBase.GetPathProgram();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    doc.Load(path + @"\XMLKinhDoanh\PhongBi.xml");
                else
                    doc.Load(path + @"\TemplateXML\PhongBi.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }

            // Thiết lập thông tin Hải quan tiếp nhận khai báo.
            XmlNode nodeTo = doc.GetElementsByTagName("To")[0];
            XmlNode nodeName = nodeTo.ChildNodes[0];
            nodeName.InnerText = maHQ;
            XmlNode nodeIdentity = nodeTo.ChildNodes[1];
            nodeIdentity.InnerText = Globals.trimMaHaiQuan(maHQ);

            // Subject.
            XmlNode nodeSubject = doc.GetElementsByTagName("Subject")[0];
            XmlNode nodeType = nodeSubject.ChildNodes[0];
            nodeType.InnerText = ((int)messageType).ToString();

            // Function.
            XmlNode nodeFunction = nodeSubject.ChildNodes[1];
            nodeFunction.InnerText = ((int)messageFunction).ToString();

            // Reference ID.
            XmlNode nodeReference = nodeSubject.ChildNodes[2];
            nodeReference.InnerText = referenceId.ToString();

            // Message ID.
            XmlNode nodeMessageID = doc.GetElementsByTagName("messageId")[0];
            nodeMessageID.InnerText = System.Guid.NewGuid().ToString();

            // Thiết lập thông tin doanh nghiệp khai báo.
            XmlNode nodeFrom = doc.GetElementsByTagName("From")[0];
            nodeFrom.ChildNodes[1].InnerText = maDN;
            nodeFrom.ChildNodes[0].InnerText = maDN;

            this.GuidStr = referenceId.ToString();
            this.Update();
            return doc.InnerXml;
        }

        public bool WSLaySoTiepNhan(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            #region Old_Code
            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            #endregion

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GuidStr), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                    xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                throw new Exception("Lỗi: " + ex.Message);
            }
            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GuidStr.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);
            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }

            XmlDocument xmlDocumentResult = new XmlDocument();
            xmlDocumentResult.LoadXml(kq);
            XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
            if (xmlNodeResult.Attributes["Err"].Value == "yes")
            {
                kq.XmlSaveMessage(this.ID, MessageTitle.Error);

                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
                }
                else
                    throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText));
            }

            if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
            {
                kq.XmlSaveMessage(this.ID, MessageTitle.Error);

                throw new Exception(FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + ""));
            }

            if (xmlNodeResult.Attributes["SO_TN"] != null)
            {
                this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                this.Update();

                kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungHoaDonTMThanhCong, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));
                return true;
            }
            return false;
        }

        public bool WSLayPhanHoi(string password, string path, string maHQ, string maDN)
        {
            KDTService kdt = WebService.GetWS();

            #region Old_Code
            // Đọc lại thông tin trả về từ Hải quan.
            //string xmlRequest = Company.KDT.SHARE.Components.Message.LayNoiDungXml(new Guid(this.GuidStr), MessageTypes.ThongTin, MessageFunctions.LayPhanHoi);

            //XmlDocument xmlDocumentRequest = new XmlDocument();
            //xmlDocumentRequest.LoadXml(xmlRequest);

            //string tmp = xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("To")[0].ChildNodes[1].InnerText = xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText;
            //xmlDocumentRequest.GetElementsByTagName("From")[0].ChildNodes[1].InnerText = tmp;

            //xmlDocumentRequest.GetElementsByTagName("function")[0].InnerText = ((int)MessageFunctions.LayPhanHoi).ToString();
            #endregion

            XmlDocument xmlDocument = new XmlDocument();
            xmlDocument.LoadXml(this.GenerateToXmlEnvelope(MessageTypes.ThongTin, MessageFunctions.LayPhanHoi, new Guid(this.GuidStr), path, maHQ, maDN));
            XmlDocument xmlDocumentTemplate = new XmlDocument();
            try
            {
                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("KD") || Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("GC"))
                    xmlDocumentTemplate.Load(path + @"\XMLKinhDoanh\LayPhanHoiDaDuyet.xml");
                else if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ProductName").Equals("SXXK"))
                    xmlDocumentTemplate.Load(path + @"\TemplateXML\LayPhanHoiDaDuyet.xml");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw new Exception("Lỗi: " + ex.Message);
            }

            XmlNode xmlNodeRoot = xmlDocument.ImportNode(xmlDocumentTemplate.SelectSingleNode("Root"), true);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["MA_DV"].Value = maDN.Trim();
            xmlNodeRoot.SelectSingleNode("THONG_TIN/DON_VI_GUI").Attributes["TEN_DV"].Value = maDN.Trim();

            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["MA_HQ"].Value = Globals.trimMaHaiQuan(maHQ);
            xmlNodeRoot.SelectSingleNode("THONG_TIN/HQ_NHAN").Attributes["TEN_HQ"].Value = Globals.trimMaHaiQuan(maHQ);

            xmlNodeRoot.SelectSingleNode("DU_LIEU").Attributes["REFERENSE"].Value = this.GuidStr.Trim();

            XmlNode xmlNodeContent = xmlDocument.GetElementsByTagName("Content")[0];
            xmlNodeContent.AppendChild(xmlNodeRoot);

            string kq = string.Empty;
            try
            {
                System.Threading.Thread.Sleep(Company.KDT.SHARE.Components.Globals.TimeDelay());
                kq = kdt.Send(xmlDocument.InnerXml, password);
            }
            catch
            {
                throw new Exception("Lỗi do hệ thống của hải quan " + " : " + "Không kết nối tới hải quan được!" + "|" + "DOTNET_LEVEL");
            }
            string msgError = string.Empty;
            try
            {
                XmlDocument xmlDocumentResult = new XmlDocument();
                xmlDocumentResult.LoadXml(kq);
                XmlNode xmlNodeResult = xmlDocumentResult.SelectSingleNode("Envelope/Body/Content/Root");
                if (xmlNodeResult.Attributes["Err"].Value == "yes")
                {
                    kq.XmlSaveMessage(this.ID, MessageTitle.Error);

                    if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                        throw new Exception(msgError);
                    }
                    else
                    {
                        msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText);
                        throw new Exception(msgError);
                    }
                }

                if (xmlNodeResult.Attributes["Ok"] != null && xmlNodeResult.Attributes["Ok"].Value != "yes")
                {

                    msgError = "Thông báo từ hải quan " + FontConverter.TCVN2Unicode(xmlNodeResult.InnerText + "|" + "");
                    throw new Exception(msgError);
                }
                if (xmlNodeResult.Attributes["TuChoi"] != null && xmlNodeResult.Attributes["TuChoi"].Value == "yes")
                {

                    kq.XmlSaveMessage(this.ID, MessageTitle.TuChoiTiepNhan, string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}\r\n\r\nChứng từ đã bị Hải quan từ chối tiếp nhận với lý do:\r\n{2}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString(), xmlNodeResult.InnerText));
                    // Cập lại số tiếp nhận
                    this.SoTiepNhan = 0;
                    this.NgayTiepNhan = new DateTime(1900, 1, 1);
                    this.Update();

                    return true;
                }
                else if (xmlNodeResult.Attributes["SO_TN"] != null)
                {
                    this.SoTiepNhan = Convert.ToInt64(xmlNodeResult.Attributes["SO_TN"].Value);
                    this.NgayTiepNhan = Convert.ToDateTime(xmlNodeResult.Attributes["Ngay_TN"].Value);
                    this.Update();

                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungHoaDonTMThanhCong,
                                      string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));

                    return true;
                }
                else if (xmlNodeResult.Attributes["SOTN"] != null)
                {

                    kq.XmlSaveMessage(this.ID, MessageTitle.KhaiBaoBoSungHoaDonTMThanhCong,
                                    string.Format("Số tiếp nhận: {0}\r\nNgày tiếp nhận: {1}", this.SoTiepNhan, this.NgayTiepNhan.ToShortDateString()));

                    this.TrangThai = Company.KDT.SHARE.Components.TrangThaiXuLy.DA_DUYET;
                    this.Update();
                    return true;
                }
            }
            catch (Exception ex)
            {
                if (msgError != string.Empty)
                    kq.XmlSaveMessage(this.ID, MessageTitle.Error);
                Logger.LocalLogger.Instance().WriteMessage(new Exception(kq));
                throw ex;
            }
            return false;
        }

        public static bool KiemTraChungTuCoBoSung(List<HoaDonThuongMai> collections)
        {
            foreach (HoaDonThuongMai item in collections)
            {
                if (item.LoaiKB == 1)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
