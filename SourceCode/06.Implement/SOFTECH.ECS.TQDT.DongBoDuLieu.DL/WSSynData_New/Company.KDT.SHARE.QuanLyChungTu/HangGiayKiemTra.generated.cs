using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class HangGiayKiemTra : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long GiayKiemTra_ID { set; get; }
		public long HMD_ID { set; get; }
		public string MaHang { set; get; }
		public string TenHang { set; get; }
		public string MaHS { set; get; }
		public string NuocXX_ID { set; get; }
		public decimal SoLuong { set; get; }
		public string DVT_ID { set; get; }
		public string GhiChu { set; get; }
		public string LoaiChungTu { set; get; }
		public string SoChungTu { set; get; }
		public string TenChungTu { set; get; }
		public DateTime NgayPhatHanh { set; get; }
		public string DiaDiemKiemTra { set; get; }
		public string MaCoQuanKT { set; get; }
		public string TenCoQuanKT { set; get; }
		public string KetQuaKT { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<HangGiayKiemTra> ConvertToCollection(IDataReader reader)
		{
			IList<HangGiayKiemTra> collection = new List<HangGiayKiemTra>();
			while (reader.Read())
			{
				HangGiayKiemTra entity = new HangGiayKiemTra();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayKiemTra_ID"))) entity.GiayKiemTra_ID = reader.GetInt64(reader.GetOrdinal("GiayKiemTra_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("HMD_ID"))) entity.HMD_ID = reader.GetInt64(reader.GetOrdinal("HMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiChungTu"))) entity.LoaiChungTu = reader.GetString(reader.GetOrdinal("LoaiChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoChungTu"))) entity.SoChungTu = reader.GetString(reader.GetOrdinal("SoChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenChungTu"))) entity.TenChungTu = reader.GetString(reader.GetOrdinal("TenChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayPhatHanh"))) entity.NgayPhatHanh = reader.GetDateTime(reader.GetOrdinal("NgayPhatHanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiemKiemTra"))) entity.DiaDiemKiemTra = reader.GetString(reader.GetOrdinal("DiaDiemKiemTra"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCoQuanKT"))) entity.MaCoQuanKT = reader.GetString(reader.GetOrdinal("MaCoQuanKT"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCoQuanKT"))) entity.TenCoQuanKT = reader.GetString(reader.GetOrdinal("TenCoQuanKT"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQuaKT"))) entity.KetQuaKT = reader.GetString(reader.GetOrdinal("KetQuaKT"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<HangGiayKiemTra> collection, long id)
        {
            foreach (HangGiayKiemTra item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GiayKiemTra_HangGiayKiemTra VALUES(@GiayKiemTra_ID, @HMD_ID, @MaHang, @TenHang, @MaHS, @NuocXX_ID, @SoLuong, @DVT_ID, @GhiChu, @LoaiChungTu, @SoChungTu, @TenChungTu, @NgayPhatHanh, @DiaDiemKiemTra, @MaCoQuanKT, @TenCoQuanKT, @KetQuaKT)";
            string update = "UPDATE t_KDT_GiayKiemTra_HangGiayKiemTra SET GiayKiemTra_ID = @GiayKiemTra_ID, HMD_ID = @HMD_ID, MaHang = @MaHang, TenHang = @TenHang, MaHS = @MaHS, NuocXX_ID = @NuocXX_ID, SoLuong = @SoLuong, DVT_ID = @DVT_ID, GhiChu = @GhiChu, LoaiChungTu = @LoaiChungTu, SoChungTu = @SoChungTu, TenChungTu = @TenChungTu, NgayPhatHanh = @NgayPhatHanh, DiaDiemKiemTra = @DiaDiemKiemTra, MaCoQuanKT = @MaCoQuanKT, TenCoQuanKT = @TenCoQuanKT, KetQuaKT = @KetQuaKT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GiayKiemTra_HangGiayKiemTra WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, "GiayKiemTra_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.VarChar, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.NVarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCoQuanKT", SqlDbType.NVarChar, "MaCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanKT", SqlDbType.NVarChar, "TenCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaKT", SqlDbType.NChar, "KetQuaKT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, "GiayKiemTra_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.VarChar, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.NVarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCoQuanKT", SqlDbType.NVarChar, "MaCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanKT", SqlDbType.NVarChar, "TenCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaKT", SqlDbType.NChar, "KetQuaKT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GiayKiemTra_HangGiayKiemTra VALUES(@GiayKiemTra_ID, @HMD_ID, @MaHang, @TenHang, @MaHS, @NuocXX_ID, @SoLuong, @DVT_ID, @GhiChu, @LoaiChungTu, @SoChungTu, @TenChungTu, @NgayPhatHanh, @DiaDiemKiemTra, @MaCoQuanKT, @TenCoQuanKT, @KetQuaKT)";
            string update = "UPDATE t_KDT_GiayKiemTra_HangGiayKiemTra SET GiayKiemTra_ID = @GiayKiemTra_ID, HMD_ID = @HMD_ID, MaHang = @MaHang, TenHang = @TenHang, MaHS = @MaHS, NuocXX_ID = @NuocXX_ID, SoLuong = @SoLuong, DVT_ID = @DVT_ID, GhiChu = @GhiChu, LoaiChungTu = @LoaiChungTu, SoChungTu = @SoChungTu, TenChungTu = @TenChungTu, NgayPhatHanh = @NgayPhatHanh, DiaDiemKiemTra = @DiaDiemKiemTra, MaCoQuanKT = @MaCoQuanKT, TenCoQuanKT = @TenCoQuanKT, KetQuaKT = @KetQuaKT WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GiayKiemTra_HangGiayKiemTra WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, "GiayKiemTra_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NuocXX_ID", SqlDbType.VarChar, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiChungTu", SqlDbType.NVarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCoQuanKT", SqlDbType.NVarChar, "MaCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanKT", SqlDbType.NVarChar, "TenCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQuaKT", SqlDbType.NChar, "KetQuaKT", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, "GiayKiemTra_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HMD_ID", SqlDbType.BigInt, "HMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHS", SqlDbType.VarChar, "MaHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NuocXX_ID", SqlDbType.VarChar, "NuocXX_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT_ID", SqlDbType.VarChar, "DVT_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiChungTu", SqlDbType.NVarChar, "LoaiChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoChungTu", SqlDbType.NVarChar, "SoChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenChungTu", SqlDbType.NVarChar, "TenChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayPhatHanh", SqlDbType.DateTime, "NgayPhatHanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, "DiaDiemKiemTra", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCoQuanKT", SqlDbType.NVarChar, "MaCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanKT", SqlDbType.NVarChar, "TenCoQuanKT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQuaKT", SqlDbType.NChar, "KetQuaKT", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HangGiayKiemTra Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<HangGiayKiemTra> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<HangGiayKiemTra> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<HangGiayKiemTra> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<HangGiayKiemTra> SelectCollectionBy_GiayKiemTra_ID(long giayKiemTra_ID)
		{
            IDataReader reader = SelectReaderBy_GiayKiemTra_ID(giayKiemTra_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_GiayKiemTra_ID(long giayKiemTra_ID)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectBy_GiayKiemTra_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, giayKiemTra_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_GiayKiemTra_ID(long giayKiemTra_ID)
		{
			const string spName = "p_KDT_GiayKiemTra_HangGiayKiemTra_SelectBy_GiayKiemTra_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, giayKiemTra_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertHangGiayKiemTra(long giayKiemTra_ID, long hMD_ID, string maHang, string tenHang, string maHS, string nuocXX_ID, decimal soLuong, string dVT_ID, string ghiChu, string loaiChungTu, string soChungTu, string tenChungTu, DateTime ngayPhatHanh, string diaDiemKiemTra, string maCoQuanKT, string tenCoQuanKT, string ketQuaKT)
		{
			HangGiayKiemTra entity = new HangGiayKiemTra();	
			entity.GiayKiemTra_ID = giayKiemTra_ID;
			entity.HMD_ID = hMD_ID;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.NuocXX_ID = nuocXX_ID;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.GhiChu = ghiChu;
			entity.LoaiChungTu = loaiChungTu;
			entity.SoChungTu = soChungTu;
			entity.TenChungTu = tenChungTu;
			entity.NgayPhatHanh = ngayPhatHanh;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.MaCoQuanKT = maCoQuanKT;
			entity.TenCoQuanKT = tenCoQuanKT;
			entity.KetQuaKT = ketQuaKT;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, GiayKiemTra_ID);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.VarChar, NuocXX_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.NVarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, TenChungTu);
			db.AddInParameter(dbCommand, "@NgayPhatHanh", SqlDbType.DateTime, NgayPhatHanh.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanh);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@MaCoQuanKT", SqlDbType.NVarChar, MaCoQuanKT);
			db.AddInParameter(dbCommand, "@TenCoQuanKT", SqlDbType.NVarChar, TenCoQuanKT);
			db.AddInParameter(dbCommand, "@KetQuaKT", SqlDbType.NChar, KetQuaKT);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<HangGiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangGiayKiemTra item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHangGiayKiemTra(long id, long giayKiemTra_ID, long hMD_ID, string maHang, string tenHang, string maHS, string nuocXX_ID, decimal soLuong, string dVT_ID, string ghiChu, string loaiChungTu, string soChungTu, string tenChungTu, DateTime ngayPhatHanh, string diaDiemKiemTra, string maCoQuanKT, string tenCoQuanKT, string ketQuaKT)
		{
			HangGiayKiemTra entity = new HangGiayKiemTra();			
			entity.ID = id;
			entity.GiayKiemTra_ID = giayKiemTra_ID;
			entity.HMD_ID = hMD_ID;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.NuocXX_ID = nuocXX_ID;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.GhiChu = ghiChu;
			entity.LoaiChungTu = loaiChungTu;
			entity.SoChungTu = soChungTu;
			entity.TenChungTu = tenChungTu;
			entity.NgayPhatHanh = ngayPhatHanh;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.MaCoQuanKT = maCoQuanKT;
			entity.TenCoQuanKT = tenCoQuanKT;
			entity.KetQuaKT = ketQuaKT;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GiayKiemTra_HangGiayKiemTra_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, GiayKiemTra_ID);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.VarChar, NuocXX_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.NVarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, TenChungTu);
			db.AddInParameter(dbCommand, "@NgayPhatHanh", SqlDbType.DateTime, NgayPhatHanh.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanh);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@MaCoQuanKT", SqlDbType.NVarChar, MaCoQuanKT);
			db.AddInParameter(dbCommand, "@TenCoQuanKT", SqlDbType.NVarChar, TenCoQuanKT);
			db.AddInParameter(dbCommand, "@KetQuaKT", SqlDbType.NChar, KetQuaKT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<HangGiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangGiayKiemTra item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHangGiayKiemTra(long id, long giayKiemTra_ID, long hMD_ID, string maHang, string tenHang, string maHS, string nuocXX_ID, decimal soLuong, string dVT_ID, string ghiChu, string loaiChungTu, string soChungTu, string tenChungTu, DateTime ngayPhatHanh, string diaDiemKiemTra, string maCoQuanKT, string tenCoQuanKT, string ketQuaKT)
		{
			HangGiayKiemTra entity = new HangGiayKiemTra();			
			entity.ID = id;
			entity.GiayKiemTra_ID = giayKiemTra_ID;
			entity.HMD_ID = hMD_ID;
			entity.MaHang = maHang;
			entity.TenHang = tenHang;
			entity.MaHS = maHS;
			entity.NuocXX_ID = nuocXX_ID;
			entity.SoLuong = soLuong;
			entity.DVT_ID = dVT_ID;
			entity.GhiChu = ghiChu;
			entity.LoaiChungTu = loaiChungTu;
			entity.SoChungTu = soChungTu;
			entity.TenChungTu = tenChungTu;
			entity.NgayPhatHanh = ngayPhatHanh;
			entity.DiaDiemKiemTra = diaDiemKiemTra;
			entity.MaCoQuanKT = maCoQuanKT;
			entity.TenCoQuanKT = tenCoQuanKT;
			entity.KetQuaKT = ketQuaKT;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, GiayKiemTra_ID);
			db.AddInParameter(dbCommand, "@HMD_ID", SqlDbType.BigInt, HMD_ID);
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
			db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, MaHS);
			db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.VarChar, NuocXX_ID);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.VarChar, DVT_ID);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@LoaiChungTu", SqlDbType.NVarChar, LoaiChungTu);
			db.AddInParameter(dbCommand, "@SoChungTu", SqlDbType.NVarChar, SoChungTu);
			db.AddInParameter(dbCommand, "@TenChungTu", SqlDbType.NVarChar, TenChungTu);
			db.AddInParameter(dbCommand, "@NgayPhatHanh", SqlDbType.DateTime, NgayPhatHanh.Year <= 1753 ? DBNull.Value : (object) NgayPhatHanh);
			db.AddInParameter(dbCommand, "@DiaDiemKiemTra", SqlDbType.NVarChar, DiaDiemKiemTra);
			db.AddInParameter(dbCommand, "@MaCoQuanKT", SqlDbType.NVarChar, MaCoQuanKT);
			db.AddInParameter(dbCommand, "@TenCoQuanKT", SqlDbType.NVarChar, TenCoQuanKT);
			db.AddInParameter(dbCommand, "@KetQuaKT", SqlDbType.NChar, KetQuaKT);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<HangGiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangGiayKiemTra item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHangGiayKiemTra(long id)
		{
			HangGiayKiemTra entity = new HangGiayKiemTra();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_GiayKiemTra_ID(long giayKiemTra_ID)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_DeleteBy_GiayKiemTra_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GiayKiemTra_ID", SqlDbType.BigInt, giayKiemTra_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GiayKiemTra_HangGiayKiemTra_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<HangGiayKiemTra> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (HangGiayKiemTra item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}