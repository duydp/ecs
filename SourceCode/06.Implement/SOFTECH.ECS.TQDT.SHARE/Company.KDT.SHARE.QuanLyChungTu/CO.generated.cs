using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
    public partial class CO
    {
        #region Private members.

        protected long _ID;
        protected string _SoCO = string.Empty;
        protected DateTime _NgayCO = new DateTime(1900, 1, 1);
        protected string _ToChucCap = string.Empty;
        protected string _NuocCapCO = string.Empty;
        protected string _MaNuocXKTrenCO = string.Empty;
        protected string _MaNuocNKTrenCO = string.Empty;
        protected string _TenDiaChiNguoiXK = string.Empty;
        protected string _TenDiaChiNguoiNK = string.Empty;
        protected string _LoaiCO = string.Empty;
        protected string _ThongTinMoTaChiTiet = string.Empty;
        protected string _MaDoanhNghiep = string.Empty;
        protected string _NguoiKy = string.Empty;
        protected int _NoCo;
        protected DateTime _ThoiHanNop = new DateTime(1900, 1, 1);
        protected long _TKMD_ID;
        protected string _GuidStr = string.Empty;
        protected int _LoaiKB;
        protected long _SoTiepNhan;
        protected DateTime _NgayTiepNhan = new DateTime(1900, 1, 1);
        protected int _TrangThai;
        protected int _NamTiepNhan;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public string SoCO
        {
            set { this._SoCO = value; }
            get { return this._SoCO; }
        }

        public DateTime NgayCO
        {
            set { this._NgayCO = value; }
            get { return this._NgayCO; }
        }

        public string ToChucCap
        {
            set { this._ToChucCap = value; }
            get { return this._ToChucCap; }
        }

        public string NuocCapCO
        {
            set { this._NuocCapCO = value; }
            get { return this._NuocCapCO; }
        }

        public string MaNuocXKTrenCO
        {
            set { this._MaNuocXKTrenCO = value; }
            get { return this._MaNuocXKTrenCO; }
        }

        public string MaNuocNKTrenCO
        {
            set { this._MaNuocNKTrenCO = value; }
            get { return this._MaNuocNKTrenCO; }
        }

        public string TenDiaChiNguoiXK
        {
            set { this._TenDiaChiNguoiXK = value; }
            get { return this._TenDiaChiNguoiXK; }
        }

        public string TenDiaChiNguoiNK
        {
            set { this._TenDiaChiNguoiNK = value; }
            get { return this._TenDiaChiNguoiNK; }
        }

        public string LoaiCO
        {
            set { this._LoaiCO = value; }
            get { return this._LoaiCO; }
        }

        public string ThongTinMoTaChiTiet
        {
            set { this._ThongTinMoTaChiTiet = value; }
            get { return this._ThongTinMoTaChiTiet; }
        }

        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }

        public string NguoiKy
        {
            set { this._NguoiKy = value; }
            get { return this._NguoiKy; }
        }

        public int NoCo
        {
            set { this._NoCo = value; }
            get { return this._NoCo; }
        }

        public DateTime ThoiHanNop
        {
            set { this._ThoiHanNop = value; }
            get { return this._ThoiHanNop; }
        }

        public long TKMD_ID
        {
            set { this._TKMD_ID = value; }
            get { return this._TKMD_ID; }
        }

        public string GuidStr
        {
            set { this._GuidStr = value; }
            get { return this._GuidStr; }
        }

        public int LoaiKB
        {
            set { this._LoaiKB = value; }
            get { return this._LoaiKB; }
        }

        public long SoTiepNhan
        {
            set { this._SoTiepNhan = value; }
            get { return this._SoTiepNhan; }
        }

        public DateTime NgayTiepNhan
        {
            set { this._NgayTiepNhan = value; }
            get { return this._NgayTiepNhan; }
        }

        public int TrangThai
        {
            set { this._TrangThai = value; }
            get { return this._TrangThai; }
        }

        public int NamTiepNhan
        {
            set { this._NamTiepNhan = value; }
            get { return this._NamTiepNhan; }
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static CO Load(long iD)
        {
            const string spName = "[dbo].[p_KDT_CO_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, iD);
            CO entity = null;
            SqlDataReader reader = (SqlDataReader)db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                entity = new CO();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayCO"))) entity.NgayCO = reader.GetDateTime(reader.GetOrdinal("NgayCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXKTrenCO"))) entity.MaNuocXKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocXKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNKTrenCO"))) entity.MaNuocNKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocNKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiXK"))) entity.TenDiaChiNguoiXK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiNK"))) entity.TenDiaChiNguoiNK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoTaChiTiet"))) entity.ThongTinMoTaChiTiet = reader.GetString(reader.GetOrdinal("ThongTinMoTaChiTiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKy"))) entity.NguoiKy = reader.GetString(reader.GetOrdinal("NguoiKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetInt32(reader.GetOrdinal("NoCo"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
            }
            reader.Close();
            return entity;
        }

        //---------------------------------------------------------------------------------------------
        public static List<CO> SelectCollectionAll()
        {
            List<CO> collection = new List<CO>();
            SqlDataReader reader = SelectReaderAll();
            while (reader.Read())
            {
                CO entity = new CO();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayCO"))) entity.NgayCO = reader.GetDateTime(reader.GetOrdinal("NgayCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXKTrenCO"))) entity.MaNuocXKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocXKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNKTrenCO"))) entity.MaNuocNKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocNKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiXK"))) entity.TenDiaChiNguoiXK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiNK"))) entity.TenDiaChiNguoiNK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoTaChiTiet"))) entity.ThongTinMoTaChiTiet = reader.GetString(reader.GetOrdinal("ThongTinMoTaChiTiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKy"))) entity.NguoiKy = reader.GetString(reader.GetOrdinal("NguoiKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetInt32(reader.GetOrdinal("NoCo"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public static List<CO> SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            List<CO> collection = new List<CO>();

            SqlDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                CO entity = new CO();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayCO"))) entity.NgayCO = reader.GetDateTime(reader.GetOrdinal("NgayCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXKTrenCO"))) entity.MaNuocXKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocXKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNKTrenCO"))) entity.MaNuocNKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocNKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiXK"))) entity.TenDiaChiNguoiXK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiNK"))) entity.TenDiaChiNguoiNK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoTaChiTiet"))) entity.ThongTinMoTaChiTiet = reader.GetString(reader.GetOrdinal("ThongTinMoTaChiTiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKy"))) entity.NguoiKy = reader.GetString(reader.GetOrdinal("NguoiKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetInt32(reader.GetOrdinal("NoCo"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static List<CO> SelectCollectionBy_TKMD_ID(long tKMD_ID)
        {
            List<CO> collection = new List<CO>();
            SqlDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
            while (reader.Read())
            {
                CO entity = new CO();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoCO"))) entity.SoCO = reader.GetString(reader.GetOrdinal("SoCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayCO"))) entity.NgayCO = reader.GetDateTime(reader.GetOrdinal("NgayCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ToChucCap"))) entity.ToChucCap = reader.GetString(reader.GetOrdinal("ToChucCap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocCapCO"))) entity.NuocCapCO = reader.GetString(reader.GetOrdinal("NuocCapCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXKTrenCO"))) entity.MaNuocXKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocXKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNuocNKTrenCO"))) entity.MaNuocNKTrenCO = reader.GetString(reader.GetOrdinal("MaNuocNKTrenCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiXK"))) entity.TenDiaChiNguoiXK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiXK"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDiaChiNguoiNK"))) entity.TenDiaChiNguoiNK = reader.GetString(reader.GetOrdinal("TenDiaChiNguoiNK"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiCO"))) entity.LoaiCO = reader.GetString(reader.GetOrdinal("LoaiCO"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinMoTaChiTiet"))) entity.ThongTinMoTaChiTiet = reader.GetString(reader.GetOrdinal("ThongTinMoTaChiTiet"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguoiKy"))) entity.NguoiKy = reader.GetString(reader.GetOrdinal("NguoiKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("NoCo"))) entity.NoCo = reader.GetInt32(reader.GetOrdinal("NoCo"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThoiHanNop"))) entity.ThoiHanNop = reader.GetDateTime(reader.GetOrdinal("ThoiHanNop"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
                if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
                if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamTiepNhan"))) entity.NamTiepNhan = reader.GetInt32(reader.GetOrdinal("NamTiepNhan"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "[dbo].[p_KDT_CO_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_CO_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_CO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static SqlDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_CO_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static SqlDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_CO_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		
        public static SqlDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
        {
            const string spName = "p_KDT_CO_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return (SqlDataReader)db.ExecuteReader(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertCO(string soCO, DateTime ngayCO, string toChucCap, string nuocCapCO, string maNuocXKTrenCO, string maNuocNKTrenCO, string tenDiaChiNguoiXK, string tenDiaChiNguoiNK, string loaiCO, string thongTinMoTaChiTiet, string maDoanhNghiep, string nguoiKy, int noCo, DateTime thoiHanNop, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan)
        {
            CO entity = new CO();
            entity.SoCO = soCO;
            entity.NgayCO = ngayCO;
            entity.ToChucCap = toChucCap;
            entity.NuocCapCO = nuocCapCO;
            entity.MaNuocXKTrenCO = maNuocXKTrenCO;
            entity.MaNuocNKTrenCO = maNuocNKTrenCO;
            entity.TenDiaChiNguoiXK = tenDiaChiNguoiXK;
            entity.TenDiaChiNguoiNK = tenDiaChiNguoiNK;
            entity.LoaiCO = loaiCO;
            entity.ThongTinMoTaChiTiet = thongTinMoTaChiTiet;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.NguoiKy = nguoiKy;
            entity.NoCo = noCo;
            entity.ThoiHanNop = thoiHanNop;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_CO_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, SoCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, NgayCO.Year <= 1900 ? DBNull.Value : (object)NgayCO);
            db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
            db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
            db.AddInParameter(dbCommand, "@MaNuocXKTrenCO", SqlDbType.VarChar, MaNuocXKTrenCO);
            db.AddInParameter(dbCommand, "@MaNuocNKTrenCO", SqlDbType.VarChar, MaNuocNKTrenCO);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiXK", SqlDbType.NVarChar, TenDiaChiNguoiXK);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiNK", SqlDbType.NVarChar, TenDiaChiNguoiNK);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, LoaiCO);
            db.AddInParameter(dbCommand, "@ThongTinMoTaChiTiet", SqlDbType.NVarChar, ThongTinMoTaChiTiet);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NguoiKy", SqlDbType.NVarChar, NguoiKy);
            db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Int, NoCo);
            db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year <= 1900 ? DBNull.Value : (object)ThoiHanNop);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1900 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertCollection(IList<CO> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CO item in collection)
                        {
                            if (item.Insert(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateCO(long iD, string soCO, DateTime ngayCO, string toChucCap, string nuocCapCO, string maNuocXKTrenCO, string maNuocNKTrenCO, string tenDiaChiNguoiXK, string tenDiaChiNguoiNK, string loaiCO, string thongTinMoTaChiTiet, string maDoanhNghiep, string nguoiKy, int noCo, DateTime thoiHanNop, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan)
        {
            CO entity = new CO();
            entity.SoCO = soCO;
            entity.NgayCO = ngayCO;
            entity.ToChucCap = toChucCap;
            entity.NuocCapCO = nuocCapCO;
            entity.MaNuocXKTrenCO = maNuocXKTrenCO;
            entity.MaNuocNKTrenCO = maNuocNKTrenCO;
            entity.TenDiaChiNguoiXK = tenDiaChiNguoiXK;
            entity.TenDiaChiNguoiNK = tenDiaChiNguoiNK;
            entity.LoaiCO = loaiCO;
            entity.ThongTinMoTaChiTiet = thongTinMoTaChiTiet;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.NguoiKy = nguoiKy;
            entity.NoCo = noCo;
            entity.ThoiHanNop = thoiHanNop;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_KDT_CO_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, SoCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, NgayCO.Year == 1900 ? DBNull.Value : (object)NgayCO);
            db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
            db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
            db.AddInParameter(dbCommand, "@MaNuocXKTrenCO", SqlDbType.VarChar, MaNuocXKTrenCO);
            db.AddInParameter(dbCommand, "@MaNuocNKTrenCO", SqlDbType.VarChar, MaNuocNKTrenCO);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiXK", SqlDbType.NVarChar, TenDiaChiNguoiXK);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiNK", SqlDbType.NVarChar, TenDiaChiNguoiNK);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, LoaiCO);
            db.AddInParameter(dbCommand, "@ThongTinMoTaChiTiet", SqlDbType.NVarChar, ThongTinMoTaChiTiet);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NguoiKy", SqlDbType.NVarChar, NguoiKy);
            db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Int, NoCo);
            db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year == 1900 ? DBNull.Value : (object)ThoiHanNop);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1900 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool InsertUpdateCollection(IList<CO> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CO item in collection)
                        {
                            if (item.InsertUpdate(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);

                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateCO(long iD, string soCO, DateTime ngayCO, string toChucCap, string nuocCapCO, string maNuocXKTrenCO, string maNuocNKTrenCO, string tenDiaChiNguoiXK, string tenDiaChiNguoiNK, string loaiCO, string thongTinMoTaChiTiet, string maDoanhNghiep, string nguoiKy, int noCo, DateTime thoiHanNop, long tKMD_ID, string guidStr, int loaiKB, long soTiepNhan, DateTime ngayTiepNhan, int trangThai, int namTiepNhan)
        {
            CO entity = new CO();
            entity.ID = iD;
            entity.SoCO = soCO;
            entity.NgayCO = ngayCO;
            entity.ToChucCap = toChucCap;
            entity.NuocCapCO = nuocCapCO;
            entity.MaNuocXKTrenCO = maNuocXKTrenCO;
            entity.MaNuocNKTrenCO = maNuocNKTrenCO;
            entity.TenDiaChiNguoiXK = tenDiaChiNguoiXK;
            entity.TenDiaChiNguoiNK = tenDiaChiNguoiNK;
            entity.LoaiCO = loaiCO;
            entity.ThongTinMoTaChiTiet = thongTinMoTaChiTiet;
            entity.MaDoanhNghiep = maDoanhNghiep;
            entity.NguoiKy = nguoiKy;
            entity.NoCo = noCo;
            entity.ThoiHanNop = thoiHanNop;
            entity.TKMD_ID = tKMD_ID;
            entity.GuidStr = guidStr;
            entity.LoaiKB = loaiKB;
            entity.SoTiepNhan = soTiepNhan;
            entity.NgayTiepNhan = ngayTiepNhan;
            entity.TrangThai = trangThai;
            entity.NamTiepNhan = namTiepNhan;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_CO_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@SoCO", SqlDbType.VarChar, SoCO);
            db.AddInParameter(dbCommand, "@NgayCO", SqlDbType.DateTime, NgayCO.Year == 1900 ? DBNull.Value : (object)NgayCO);
            db.AddInParameter(dbCommand, "@ToChucCap", SqlDbType.NVarChar, ToChucCap);
            db.AddInParameter(dbCommand, "@NuocCapCO", SqlDbType.VarChar, NuocCapCO);
            db.AddInParameter(dbCommand, "@MaNuocXKTrenCO", SqlDbType.VarChar, MaNuocXKTrenCO);
            db.AddInParameter(dbCommand, "@MaNuocNKTrenCO", SqlDbType.VarChar, MaNuocNKTrenCO);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiXK", SqlDbType.NVarChar, TenDiaChiNguoiXK);
            db.AddInParameter(dbCommand, "@TenDiaChiNguoiNK", SqlDbType.NVarChar, TenDiaChiNguoiNK);
            db.AddInParameter(dbCommand, "@LoaiCO", SqlDbType.VarChar, LoaiCO);
            db.AddInParameter(dbCommand, "@ThongTinMoTaChiTiet", SqlDbType.NVarChar, ThongTinMoTaChiTiet);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@NguoiKy", SqlDbType.NVarChar, NguoiKy);
            db.AddInParameter(dbCommand, "@NoCo", SqlDbType.Int, NoCo);
            db.AddInParameter(dbCommand, "@ThoiHanNop", SqlDbType.DateTime, ThoiHanNop.Year == 1900 ? DBNull.Value : (object)ThoiHanNop);
            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
            db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
            db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
            db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
            db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year == 1900 ? DBNull.Value : (object)NgayTiepNhan);
            db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
            db.AddInParameter(dbCommand, "@NamTiepNhan", SqlDbType.Int, NamTiepNhan);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------
        public static bool UpdateCollection(IList<CO> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CO item in collection)
                        {
                            if (item.Update(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at UpdateCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteCO(long iD)
        {
            CO entity = new CO();
            entity.ID = iD;
            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_KDT_CO_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static int DeleteBy_TKMD_ID(long tKMD_ID)
        {
            string spName = "[dbo].[p_KDT_CO_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);

            return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static bool DeleteCollection(IList<CO> collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
                {
                    try
                    {
                        bool ret01 = true;
                        foreach (CO item in collection)
                        {
                            if (item.Delete(transaction) <= 0)
                            {
                                ret01 = false;
                                break;
                            }
                        }
                        if (ret01)
                        {
                            transaction.Commit();
                            ret = true;
                        }
                        else
                        {
                            transaction.Rollback();
                            ret = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        throw new Exception("Error at DeleteCollection method: " + ex.Message);
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return ret;
        }
        #endregion
    }
}