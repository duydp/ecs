using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL.DuLieuChuan;
using Company.GC.BLL.GC;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT;

namespace Company.Interface.KDT.ToKhai
{
    public partial class NguyenPhuLieuChuyenTiepRegistedForm : BaseForm
    {
        public NguyenPhuLieu NguyenPhuLieuSelected = new NguyenPhuLieu();
        //public Company.GC.BLL.KDT.ToKhaiMauDich TKMD;
        //public HangMauDichCollection HMDCollection;

        public Company.GC.BLL.KDT.GC.ToKhaiChuyenTiep  TKCT;
        public Company.GC.BLL.KDT.GC.HangChuyenTiepCollection HCTCollection;

        public NguyenPhuLieuChuyenTiepRegistedForm()
        {
            InitializeComponent();
        }

        public void BindData()
        {
            NguyenPhuLieuSelected.HopDong_ID = TKCT.IDHopDong;
            dgList.DataSource = NguyenPhuLieuSelected.SelectCollectionBy_HopDong_ID();
        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll();          
        }

        //-----------------------------------------------------------------------------------------

        private void NguyenPhuLieuRegistedForm_Load(object sender, EventArgs e)
        {
            this.khoitao_DuLieuChuan();
            BindData();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["TenDVT"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------


        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private bool KiemTraMaHang(string MaHang)
        {
            foreach (Company.GC.BLL.KDT.GC.HangChuyenTiep  HCT in HCTCollection)
            {
                if (HCT.MaHang.Trim().ToUpper() == MaHang.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void uiButton1_Click(object sender, EventArgs e)
        {
            foreach (GridEXRow row in dgList.GetCheckedRows())
            {
                Company.GC.BLL.GC.NguyenPhuLieu NPL = (Company.GC.BLL.GC.NguyenPhuLieu)row.DataRow;
                if (KiemTraMaHang(NPL.Ma))
                {
                    string st = showMsg("MSG_WRN31", NPL.Ma);
                    //string st = MLMessages("Mặt hàng " + NPL.Ma + " đã có nên sẽ được bỏ qua.Bạn có muốn tiếp tục không ?","MSG_WRN31",NPL.Ma, true);
                    if (st == "Yes")
                    {
                        continue;
                    }
                    else
                        return;
                }
                else
                {
                    Company.GC.BLL.KDT.GC.HangChuyenTiep HCT = new Company.GC.BLL.KDT.GC.HangChuyenTiep();
                    HCT.MaHang  = NPL.Ma;
                    HCT.TenHang = NPL.Ten;
                    HCT.MaHS = NPL.MaHS;
                    HCT.ID_DVT  = NPL.DVT_ID;
                    HCTCollection.Add(HCT);
                }
            }
            this.Close();
        }

        //-----------------------------------------------------------------------------------------
    }
}