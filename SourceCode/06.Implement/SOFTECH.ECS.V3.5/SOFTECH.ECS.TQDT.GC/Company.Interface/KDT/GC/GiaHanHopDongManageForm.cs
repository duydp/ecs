﻿using System;
using System.Windows.Forms;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Data;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Company.GC.BLL.Utils;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface.KDT.GC
{
    public partial class GiaHanHopDongManageForm : BaseForm
    {
        List<GiaHanThanhKhoan> collection = new List<GiaHanThanhKhoan>();
        //GiaHanThanhKhoan giaHanThanhKhoan = new GiaHanThanhKhoan();
        private string msgInfor = string.Empty;
        // private FeedBackContent feedbackContent = null;

        public GiaHanHopDongManageForm()
        {
            InitializeComponent();
        }
        private void BindHopDong()
        {
            DataTable dt;
            {
                string where = string.Format("madoanhnghiep='{0}'", GlobalSettings.MA_DON_VI);
                HopDong HD = new HopDong();
                dt = HopDong.SelectDynamic(where, "").Tables[0];
            }
            cbHopDong.DataSource = dt;
            cbHopDong.DisplayMember = "SoHopDong";
            cbHopDong.ValueMember = "ID";
        }

        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            BindHopDong();
        }

        private void GiamSatThieuHuyGCManageForm_Load(object sender, EventArgs e)
        {

            XacNhanThongTin.Visible = Janus.Windows.UI.InheritableBoolean.False;
            this.khoitao_DuLieuChuan();
            cbStatus.SelectedIndex = 0;
            btnSearch_Click(null, null);
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {

            string st = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
            if (st == "-1")
                e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
            else if (st == "0")
                e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
            else if (st == "1")
                e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
            else if (st == "10")
                e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
            else if (st == "11")
                e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
            else if (st == "2")
                e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
            if (e.Row.Cells["NgayTiepNhan"].Value != null)
            {
                DateTime dt = Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Value);
                if (dt.Year <= 1900)
                    e.Row.Cells["NgayTiepNhan"].Text = "";
            }
            HopDong hd = new HopDong();
            hd.ID = Convert.ToInt64(e.Row.Cells["HopDong_ID"].Value);
            hd = HopDong.Load(hd.ID);
            if (hd != null)
                e.Row.Cells["HopDong_ID"].Text = hd.SoHopDong;
            this.setcommandstatus();
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            GiaHanHopDongSendForm giaHanSend = new GiaHanHopDongSendForm();
            giaHanSend.HD = HopDong.Load(Convert.ToInt64(e.Row.Cells["HopDong_ID"].Value));
            long id = Convert.ToInt64(e.Row.Cells["ID"].Value);
            giaHanSend.giaHanThanhKhoan = GiaHanThanhKhoan.Load(id);
            giaHanSend.OpenType = OpenFormType.Edit;
            giaHanSend.ShowDialog();
            btnSearch_Click(null, null);
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "KhaiBao":
                    //  this.SendV3();
                    break;
                case "cmdGet":
                    //  this.FeedBackV3();
                    break;
                case "Huy":
                    //  this.CanceldV3();
                    break;
                case "XacNhanThongTin":
                    // this.FeedBackV3();
                    break;
                case "cmdCSDaDuyet": ChuyenTrangThai(); break;
                case "cmdXuatPhuKien":
                    //XuatDuLieuPhuKienChoPhongKhai();
                    break;
                case "InPhieuTN":// this.inPhieuTN();
                    break;
            }
        }
        private void inPhieuTN()
        {
            /*   if (!(dgPhuKien.GetRows().Length > 0 && dgPhuKien.GetRow().RowType == RowType.Record)) return;
               GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
               Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
               phieuTNForm.TenPhieu = "PHỤ KIỆN";
               Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
               string[,] arrPhieuTN = new string[items.Count, 2];
               int j = 0;
               foreach (GridEXSelectedItem i in items)
               {
                   if (i.RowType == RowType.Record)
                   {
                       GiamSatTieuHuy pkDangKySelected = (GiamSatTieuHuy)i.GetRow().DataRow;
                       arrPhieuTN[j, 0] = pkDangKySelected.SoTiepNhan.ToString();
                       arrPhieuTN[j, 1] = pkDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                       j++;
                       break;
                   }
               }
               phieuTNForm.phieuTN = arrPhieuTN;
               phieuTNForm.Show();
               */
        }
        private void ChuyenTrangThai()
        {
            if (dgList.SelectedItems.Count > 0)
            {
                List<GiamSatTieuHuy> gsTieuHuys = new List<GiamSatTieuHuy>();
                foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                {
                    gsTieuHuys.Add((GiamSatTieuHuy)grItem.GetRow().DataRow);
                }

                for (int i = 0; i < gsTieuHuys.Count; i++)
                {
                    //gsTieuHuys[i].LoadCollection();

                    gsTieuHuys[i].LoadHangGSTieuHuy();
                    string[] args = new string[2];
                    args[0] = gsTieuHuys[i].ID.ToString();
                    args[1] = gsTieuHuys[i].HangGSTieuHuys.Count.ToString();
                    if (showMsg("MSG_12030722", args, true) == "Yes")
                    {
                        if (gsTieuHuys[i].TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO)
                        {
                            gsTieuHuys[i].NgayTiepNhan = DateTime.Today;
                        }
                        gsTieuHuys[i].TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                        gsTieuHuys[i].Update();
                        Company.KDT.SHARE.Components.Globals.SaveMessage(string.Empty,
                            gsTieuHuys[i].ID, gsTieuHuys[i].GUIDSTR, Company.KDT.SHARE.Components.MessageTypes.DeNghiGiamSatTieuHuy,
                             Company.KDT.SHARE.Components.MessageFunctions.ChuyenTrangThaiTay,
                             string.Format("Trước khi chuyển GUIDSTR={0},Trạng thái xử lý {1}", gsTieuHuys[i].GUIDSTR, gsTieuHuys[i].TrangThaiXuLy));

                    }
                }

                this.btnSearch_Click(null, null);
            }
            else
            {
                showMsg("MSG_240233");
                //MLMessages("Chưa có dữ liệu được chọn!","MSG_WRN11","", false);
            }
        }

        private void XuatDuLieuPhuKienChoPhongKhai()
        {
            /* GridEXSelectedItemCollection items = dgPhuKien.SelectedItems;
             if (items.Count == 0)
             {
                 showMsg("MSG_2702064");               
                 return;
             }
             try
             {
                 List<GiamSatTieuHuy> col = new List<GiamSatTieuHuy>();
                 if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                 {
                     XmlSerializer serializer = new XmlSerializer(typeof(List<GiamSatTieuHuy>));
                     FileStream fs = new FileStream(saveFileDialog1.FileName, FileMode.Create);
                     int sophukien = 0;
                     foreach (GridEXSelectedItem i in items)
                     {
                         if (i.RowType == RowType.Record)
                         {
                             GiamSatTieuHuy PK = (GiamSatTieuHuy)i.GetRow().DataRow;
                             PK.LoadCollection();
                             foreach (Company.GC.BLL.KDT.GC.LoaiPhuKien LoaiPK in PK.PKCollection)
                             {
                                 LoaiPK.LoadCollection();
                             }
                             HopDong HD = new HopDong();
                             HD.ID = PK.HopDong_ID;
                             HD = HopDong.Load(HD.ID);
                             PK.SoHopDong = HD.SoHopDong;
                             col.Add(PK);
                             sophukien++;
                         }
                     }
                     serializer.Serialize(fs, col);
                     showMsg("MSG_2702065", sophukien);
                     //MLMessages("Xuất ra file thành công " + sophukien + " phụ kiện.", "MSG_EXC05", sophukien.ToString(), false);
                     fs.Close();
                 }
             }
             catch (Exception ex)
             {
                 showMsg("MSG_2702004", ex.Message);
                 //ShowMessage("Lỗi : " + ex.Message, false);
             }
             */
        }


        private bool checkExistHD(string soHD)
        {
            HopDong hopdong = new HopDong();
            List<HopDong> hdcol = new List<HopDong>();
            hdcol = HopDong.SelectCollectionDynamic("SoHopDong='" + soHD + "'", "");
            foreach (HopDong hd in hdcol)
            {
                if (soHD.Equals(hd.SoHopDong.Trim()))
                    return true;

            }
            return false;
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {


            string where = "1=1  and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' ";
            if (txtSoTiepNhan.Text.Trim().Length > 0)
            {
                where += " and sotiepnhan=" + txtSoTiepNhan.Text;
            }
            if (cbHopDong.Text.Length > 0)
            {
                if (!checkExistHD(cbHopDong.Text.Trim().ToString()))
                {
                    showMsg("MSG_2702066");
                    //MLMessages("Không tồn tại số hợp đồng này","MSG_WRN10","", false);
                    return;
                }
                where += " and HopDong_ID=" + cbHopDong.Value.ToString();


            }

            if (txtNamTiepNhan.Text.Trim().Length > 0)
            {
                where += " and year(ngaytiepnhan)=" + txtNamTiepNhan.Text;
            }
            if (cbStatus.Text.Length > 0)
                where += " and TrangThaiXuLy=" + cbStatus.SelectedValue.ToString();
            collection = GiaHanThanhKhoan.SelectCollectionDynamic(where, "");
            dgList.DataSource = collection;
            setcommandstatus();
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }
        private void setcommandstatus()
        {
            //if (cbStatus.SelectedValue.ToString() == "-1" || cbStatus.SelectedValue.ToString() == "2" || cbStatus.SelectedValue.ToString() == "10")
            //{
            //    Huy.Enabled = Huy1.Enabled = Huy2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdGet.Enabled = cmdGet1.Enabled = cmdGet2.Enabled = cbStatus.SelectedValue.ToString() == "-1" ? Janus.Windows.UI.InheritableBoolean.True :
            //         Janus.Windows.UI.InheritableBoolean.False;
            //    KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            //    cmdXuatPhuKien.Enabled = cmdXuatPhuKien1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    btnXoa.Enabled = true;
            //    cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    InPhieuTN.Enabled = InPhieuTN1.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //}
            //else if (cbStatus.SelectedValue.ToString() == "0" || cbStatus.SelectedValue.ToString() == "11")
            //{
            //    Huy.Enabled = Huy1.Enabled = Huy2.Enabled = cbStatus.SelectedValue.ToString() == "0" ? Janus.Windows.UI.InheritableBoolean.True :
            //         Janus.Windows.UI.InheritableBoolean.False;
            //    cmdGet.Enabled = cmdGet1.Enabled = cmdGet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            //    cmdXuatPhuKien.Enabled = cmdXuatPhuKien1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    btnXoa.Enabled = false;
            //    cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    InPhieuTN.Enabled = InPhieuTN1.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //}
            //else
            //{
            //    Huy.Enabled = Huy1.Enabled = Huy2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdGet.Enabled = cmdGet1.Enabled = cmdGet2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    KhaiBao.Enabled = KhaiBao1.Enabled = KhaiBao2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
            //    cmdXuatPhuKien.Enabled = cmdXuatPhuKien1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    btnXoa.Enabled = false;
            //    cmdCSDaDuyet.Enabled = cmdCSDaDuyet1.Enabled = cmdCSDaDuyet2.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    InPhieuTN.Enabled = InPhieuTN1.Enabled = InPhieuTN2.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //}
        }


        private void dgPhuKien_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (showMsg("MSG_DEL01", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {

                        GiamSatTieuHuy pkdkDelete = (GiamSatTieuHuy)i.GetRow().DataRow;

                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = "PK";
                        sendXML.master_id = pkdkDelete.ID;
                        if (sendXML.Load())
                        {
                            j = i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                            //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Bạn có muốn xóa không?", "MSG_STN09", j.ToString(), false);
                            //if (st == "Yes")
                            //{
                            //    if (pkdkDelete.ID > 0)
                            //    {
                            //        pkdkDelete.Delete();
                            //    }
                            //}
                        }
                        else
                        {
                            if (pkdkDelete.ID > 0)
                            {
                                pkdkDelete.Delete();
                            }
                        }
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (collection.Count <= 0) return;
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            int j = 0;
            if (items.Count <= 0) return;
            if (showMsg("MSG_DEL01", true) == "Yes")
            {

                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        GiaHanThanhKhoan giaHanDel = (GiaHanThanhKhoan)i.GetRow().DataRow;
                        Company.GC.BLL.KDT.SXXK.MsgSend sendXML = new Company.GC.BLL.KDT.SXXK.MsgSend();
                        sendXML.LoaiHS = LoaiKhaiBao.GiamSatTieuHuy;
                        sendXML.master_id = giaHanDel.ID;
                        if (sendXML.Load())
                        {
                            j = i.Position + 1;
                            showMsg("MSG_2702012", i.Position + 1);
                            //MLMessages("Danh sách thứ " + (i.Position + 1) + "  đã gửi thông tin tới hải quan nhưng chưa được xác nhận của hệ thống hải quan.Bạn có muốn xóa không?", "MSG_STN09",j.ToString(), false);
                            //if (st == "Yes")
                            //{
                            //    if (pkdkDelete.ID > 0)
                            //    {
                            //        pkdkDelete.Delete();
                            //    }
                            //}
                        }
                        else
                        {
                            if (giaHanDel.ID > 0)
                            {
                                giaHanDel.Delete();
                            }

                            try
                            {
                                string whereLog = "1 = 1";
                                whereLog += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", giaHanDel.ID, Company.KDT.SHARE.Components.LoaiKhaiBao.GiamSatTieuHuy);
                                List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(whereLog, "");
                                if (listLog.Count > 0)
                                {
                                    long idLog = listLog[0].IDLog;
                                    string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                                    long idDK = listLog[0].ID_DK;
                                    string guidstr = listLog[0].GUIDSTR_DK;
                                    string userKhaiBao = listLog[0].UserNameKhaiBao;
                                    DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                                    string userSuaDoi = GlobalSettings.UserLog;
                                    DateTime ngaySuaDoi = DateTime.Now;
                                    string ghiChu = listLog[0].GhiChu;
                                    bool isDelete = true;
                                    Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                                userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                                }
                            }
                            catch (Exception ex)
                            {
                                ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                                return;
                            }
                        }
                    }
                }

                string where = "1=1  and MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' ";
                if (txtSoTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and sotiepnhan=" + txtSoTiepNhan.Text;
                }
                if (cbHopDong.Text.Length > 0)
                {
                    where += " and HopDong_ID=" + cbHopDong.Value.ToString();
                }

                if (txtNamTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and year(ngaytiepnhan)=" + txtNamTiepNhan.Text;
                }
                if (cbStatus.Text.Length > 0)
                    where += " and TrangThaiXuLy=" + cbStatus.SelectedValue.ToString();
                collection = GiaHanThanhKhoan.SelectCollectionDynamic(where, "");
                dgList.DataSource = collection;
                setcommandstatus();
            }
        }
        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            if (dgList.GetRow() != null)
            {
                GiaHanThanhKhoan ghThanhKhoan = (GiaHanThanhKhoan)dgList.GetRow().DataRow;
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = ghThanhKhoan.ID;
                form.DeclarationIssuer = DeclarationIssuer.GC_GIA_HAN_THANH_KHOAN;
                form.ShowDialog(this);
            }
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }
    }
}