using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_ToKhaiMauDich
	{
		public static List<KDT_VNACC_ToKhaiMauDich> SelectTK_KhoKeToan_PhieuXNK(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_KDT_VNACC_ToKhaiMauDich_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            var reader = db.ExecuteReader(dbCommand); 

			return ConvertToCollection(reader);		
		}
	}	
}