using System;
using System.Data;
using System.Windows.Forms;
using Company.GC.BLL;
using System.Collections.Generic;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.KDT;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.Utils;
using Company.KDT.SHARE.Components;
using System.Data.SqlClient;
using Company.Interface.Report.GC.TT39;
using Company.Interface.KDT.SXXK;


namespace Company.Interface.KDT.GC
{
    public partial class DinhMucGCManageForm : BaseForm
    {
        public bool IsBrowseForm = false;
        private Company.GC.BLL.KDT.GC.DinhMucDangKy dmdkSelect;
        private DinhMucDangKyCollection dmdkCollection = new DinhMucDangKyCollection();
        List<HopDong> hdcoll = new List<HopDong>();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public DataTable dt;
        public DinhMucGCManageForm()
        {
            InitializeComponent();
            cbStatus.SelectedIndex = 0;
        }


        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            try
            {
                // Đơn vị tính.
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                // Đơn vị Hải quan.
                this._DonViHaiQuan = DonViHaiQuan.SelectAll().Tables[0];
                ctrCoQuanHQ.Ma = GlobalSettings.MA_HAI_QUAN;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        //-----------------------------------------------------------------------------------------

        private void DinhMucGCManageForm_Load(object sender, EventArgs e)
        {
            try
            {


                this.khoitao_DuLieuChuan();
                cbStatus.SelectedIndex = 0;
                if (ckbTimKiem.Checked)
                    grbtimkiem.Enabled = true;
                else
                    grbtimkiem.Enabled = false;
                BindHopDong();
                btnSearch_Click(null, null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }

        //-----------------------------------------------------------------------------------------



        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {                    
                long ID = (long)Convert.ToInt64(e.Row.Cells["ID"].Text);

                Form[] forms = this.ParentForm.MdiChildren;
                for (int i = 0; i < forms.Length; i++)
                {
                    if (forms[i].Name.ToString().Equals("DM" + ID.ToString()))
                    {
                        forms[i].Activate();
                        return;
                    }
                }
                DinhMucDangKy dmdk = new DinhMucDangKy();
                dmdk.ID = ID;
                dmdk.Load();
                dmdk.LoadCollection();
                DinhMucGCSendForm f = new DinhMucGCSendForm();
                f.dmDangKy = dmdk;
                f.Name = "DM" + ID.ToString();
                f.ShowDialog();
                btnSearch_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void BindHopDong()
        {
            try
            {
                HopDong hd = new HopDong();
                hd.SoHopDong = "";
                hd.ID = 0;
                List<HopDong> collection;
                {
                    string where = string.Format(" MaDoanhNghiep='{0}'", GlobalSettings.MA_DON_VI);
                    collection = HopDong.SelectCollectionDynamic(where, "SoHopDong");
                }
                collection.Add(hd);
                cbHopDong.DataSource = collection;
                cbHopDong.DisplayMember = "SoHopDong";
                cbHopDong.ValueMember = "ID";
                cbHopDong.SelectedIndex = 1;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (e.Row.Cells["NgayTiepNhan"].Value != DBNull.Value)
                    {
                        if (Convert.ToDateTime(e.Row.Cells["NgayTiepNhan"].Value).Year <= 1900)
                        {
                            e.Row.Cells["NgayTiepNhan"].Text = "";
                        }
                    }
                    if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_DUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHUA_KHAI_BAO)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHO_DUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHO_HUY)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_HUY)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.KHONG_PHE_DUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.SUA_KHAI_BAO)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo sửa";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.HUY_KHAI_BAO)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo hủy";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.SUATKDADUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đang sửa";
                    HopDong hd = new HopDong();
                    hd.ID = (long)Convert.ToInt64(e.Row.Cells["ID_HopDong"].Value);
                    hd = HopDong.Load(hd.ID);
                    e.Row.Cells["ID_HopDong"].Text = hd.SoHopDong;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtMaNPL.Text == "" && txtMaSP.Text == "")
                    this.search();
                else
                    this.search_SP_NPL(txtMaSP.Text, txtMaNPL.Text);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }

        //Phiph -----------------------------------------------------------------------------------------
        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.
                string where = " MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND MaHaiQuan = '{0}'", ctrCoQuanHQ.Ma );
                if (txtSoTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and sotiepnhan like '%" + txtSoTiepNhan.Text + "%'";
                }

                try
                {
                    long idHopDong = Convert.ToInt64(cbHopDong.Value);
                }
                catch
                {
                    return;
                }
                if (cbHopDong.Text.Length > 0)
                {
                    where += " and id_HopDong like '%" + cbHopDong.Value.ToString() + "%'";
                }

                if (txtNamTiepNhan.Text.Trim().Length > 0)
                {
                    where += " and year(ngaytiepnhan)=" + txtNamTiepNhan.Text;
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (ngaytiepnhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                if (cbStatus.Text.Length > 0)
                    where += " and TrangThaiXuLy=" + cbStatus.SelectedValue.ToString();
                dmdkCollection = (new Company.GC.BLL.KDT.GC.DinhMucDangKy()).SelectCollectionDynamic(where, "");
                BindData();                    
                dt = Company.GC.BLL.KDT.GC.DinhMucDangKy.SelectDynamicFull(where,null).Tables[0];

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void search_SP_NPL(string maSP, string maNPL)
        {
            try
            {
                // Xây dựng điều kiện tìm kiếm.

                string where = "WHERE DinhMuc.MaSanPham like '%" + maSP + "%' ";
                where += " AND DinhMuc.MaNguyenPhuLieu like '%" + maNPL + "%' ";
                where += " AND DinhMuc_DK.MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";
                where += string.Format(" AND DinhMuc_DK.MaHaiQuan = '{0}'", ctrCoQuanHQ.Ma);
                if (txtSoTiepNhan.Text.Trim().Length > 0)
                {
                    where += " AND DinhMuc_DK.sotiepnhan like '%" + txtSoTiepNhan.Text + "%'";
                }

                try
                {
                    long idHopDong = Convert.ToInt64(cbHopDong.Value);
                }
                catch
                {
                    showMsg("MSG_240205");
                    return;
                }
                if (cbHopDong.Text.Length > 0)
                {
                    where += " AND DinhMuc_DK.id_HopDong like '%" + cbHopDong.Value.ToString() + "%'";
                }

                if (txtNamTiepNhan.Text.Trim().Length > 0)
                {
                    where += " AND year(DinhMuc_DK.ngaytiepnhan)=" + txtNamTiepNhan.Text;
                }
                if (ckbTimKiem.Checked)
                {
                    DateTime fromDate = clcTuNgay.Value;
                    DateTime toDate = ucCalendar1.Value;
                    if (toDate.Year <= 1900) toDate = DateTime.Now;
                    toDate = toDate.AddDays(1);
                    where = where + " AND (DinhMuc_DK.ngaytiepnhan Between '" + fromDate.ToString("yyyy-MM-dd") + "' AND '" + toDate.ToString("yyyy-MM-dd") + "')";
                }
                if (cbStatus.Text.Length > 0)
                    where += " AND DinhMuc_DK.TrangThaiXuLy=" + cbStatus.SelectedValue.ToString();
                dmdkCollection = (new Company.GC.BLL.KDT.GC.DinhMucDangKy()).SelectCollection_SP_NPL(where, "");
                BindData();
                dt = Company.GC.BLL.KDT.GC.DinhMucDangKy.SelectDynamicFull(where, null).Tables[0];
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = dmdkCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindDataExport()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = dt;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void dgList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            try
            {
                long ID = (long)Convert.ToInt64(e.Row.Cells[4].Text);
                DinhMucDangKy dmdk = new DinhMucDangKy();
                dmdk.ID = ID;
                dmdk.Load();
                if (e.Row.RowType == RowType.Record)
                {
                    if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XÓA ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                    {
                        if (dmdk.ID > 0)
                        {
                            dmdk.LoadCollection();
                            dmdk.DeleteDinhMucGC();
                            DinhMuc dmDelete = new DinhMuc();
                            dmDelete.DeleteByMasterID(dmdk.ID);
                            dmdk.Delete();
                        }
                    }
                    else
                    {
                        e.Cancel = true;
                    }
                }
                btnSearch_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdCSDaDuyet": 
                    ChuyenTrangThai(); 
                    break;
                case "InPhieuTN": 
                    this.inPhieuTN(); 
                    break;
                case "cmdExportExcel":
                    this.ExportExcel();
                    break;
                case "cmdUpdateStatus":
                    this.UpdateStatus();
                    break;
            }
        }
        private void UpdateStatus()
        {
            try
            {
                WSForm2 login = new WSForm2();
                login.ShowDialog(this);
                if (WSForm2.IsSuccess == true)
                {
                    if (dgList.SelectedItems.Count > 0)
                    {
                        DinhMucDangKyCollection dmdkColl = new DinhMucDangKyCollection();
                        foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                        {
                            dmdkColl.Add((DinhMucDangKy)grItem.GetRow().DataRow);
                        }
                        for (int i = 0; i < dmdkColl.Count; i++)
                        {
                            dmdkColl[i].LoadCollection();
                            UpdateStatusForm f = new UpdateStatusForm();
                            f.DMDK = dmdkColl[i];
                            f.formType = "DM";
                            f.ShowDialog(this);
                        }
                        this.btnSearch_Click(null, null);
                    }
                    else
                    {
                        showMsg("MSG_2702040", false);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }
        private void ExportExcel()
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH ĐỊNH MỨC_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XUẤT KÈM THEO CHI TIẾT ĐỊNH MỨC KHÔNG ?", true) == "No")
                {
                    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
                else
                {
                    if (sfNPL.ShowDialog(this) == DialogResult.Yes || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        dgList.Tables[0].Columns.Add(new GridEXColumn("MaSanPham", ColumnType.Text, EditType.NoEdit) { Caption = "MÃ SẢN PHẨM" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TenSanPham", ColumnType.Text, EditType.NoEdit) { Caption = "TÊN SẢN PHẨM" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("MaNguyenPhuLieu", ColumnType.Text, EditType.NoEdit) { Caption = "MÃ NPL" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TenNPL", ColumnType.Text, EditType.NoEdit) { Caption = "TÊN NPL" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DVT", ColumnType.Text, EditType.NoEdit) { Caption = "ĐVT" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("DinhMucSuDung", ColumnType.Text, EditType.NoEdit) { Caption = "ĐỊNH MỨC SỬ DỤNG" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("TyLeHaoHut", ColumnType.Text, EditType.NoEdit) { Caption = "TỶ LỆ HAO HỤT" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("GhiChu", ColumnType.Text, EditType.NoEdit) { Caption = "GHI CHÚ" });
                        dgList.Tables[0].Columns.Add(new GridEXColumn("STTHang", ColumnType.Text, EditType.NoEdit) { Caption = "STT" });
                        //dgList.Tables[0].Columns.Add(new GridEXColumn("SoHopDong", ColumnType.Text, EditType.NoEdit) { Caption = "Số hợp đồng" });
                        //dgList.Tables[0].Columns.Add(new GridEXColumn("TrangThai", ColumnType.Text, EditType.NoEdit) { Caption = "Trạng thái khai báo" });


                        dgList.DataSource = dt;
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        dgList.Tables[0].Columns.Remove("MaSanPham");
                        dgList.Tables[0].Columns.Remove("TenSanPham");
                        dgList.Tables[0].Columns.Remove("MaNguyenPhuLieu");
                        dgList.Tables[0].Columns.Remove("TenNPL");
                        dgList.Tables[0].Columns.Remove("DVT");
                        dgList.Tables[0].Columns.Remove("DinhMucSuDung");
                        dgList.Tables[0].Columns.Remove("TyLeHaoHut");
                        dgList.Tables[0].Columns.Remove("GhiChu");
                        dgList.Tables[0].Columns.Remove("STTHang");
                        //dgList.Tables[0].Columns.Remove("SoHopDong");
                        //dgList.Tables[0].Columns.Remove("TrangThai");
                        btnSearch_Click(null, null);
                        if (ShowMessage("DOANH NGHIỆP CÓ MUỐN MỞ FILE NÀY KHÔNG?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void inPhieuTN()
        {
            try
            {
                if (!(dgList.GetRows().Length > 0 && dgList.GetRow().RowType == RowType.Record)) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                Company.Interface.Report.PhieuTNForm phieuTNForm = new Company.Interface.Report.PhieuTNForm();
                phieuTNForm.TenPhieu = "ĐỊNH MỨC";
                Company.Interface.Report.GC.PhieuTN phieuTNAll = new Company.Interface.Report.GC.PhieuTN();
                string[,] arrPhieuTN = new string[items.Count, 2];
                int j = 0;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DinhMucDangKy dmDangKySelected = (DinhMucDangKy)i.GetRow().DataRow;
                        arrPhieuTN[j, 0] = dmDangKySelected.SoTiepNhan.ToString();
                        arrPhieuTN[j, 1] = dmDangKySelected.NgayTiepNhan.ToString("dd/MM/yyyy");
                        j++;
                        //break;
                    }
                }
                phieuTNForm.phieuTN = arrPhieuTN;
                phieuTNForm.Show();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }
        private void ChuyenTrangThai()
        {
            try
            {
                if (dgList.SelectedItems.Count > 0)
                {
                    DinhMucDangKyCollection dmDKColl = new DinhMucDangKyCollection();
                    foreach (GridEXSelectedItem grItem in dgList.SelectedItems)
                    {
                        dmDKColl.Add((DinhMucDangKy)grItem.GetRow().DataRow);
                    }
                    string msgWarning = "\nDOANH NGHIỆP CÓ MUỐN CHUYỂN DANH SÁCH KHAI BÁO ĐỊNH MỨC NÀY SANG ĐÃ DUYỆT KHÔNG?\n";
                    string msgAccept = "";
                    for (int i = 0; i < dmDKColl.Count; i++)
                    {
                        if (dmDKColl[i].TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                        {
                            dmDKColl[i].LoadCollection();
                            msgAccept += "[" + (i + 1) + "]-[" + dmDKColl[i].ID + "]-[" + dmDKColl[i].SoTiepNhan + "]-[" + dmDKColl[i].NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(dmDKColl[i].TrangThaiXuLy) + "]\n";
                        }
                    }
                    msgWarning += "\n - [STT] -[ID]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + msgAccept + "\n";
                    if (ShowMessageTQDT(" Thông báo từ hệ thống ", msgWarning, true) == "Yes")
                    {
                        foreach (DinhMucDangKy item in dmDKColl)
                        {
                            item.TrangThaiXuLy = TrangThaiXuLy.DA_DUYET;
                            item.DeleteDinhMucGC();
                            item.TransferGC();
                        }
                    }
                    ShowMessage("CHUYỂN TRẠNG THÁI THÀNH CÔNG ", false);
                    btnSearch_Click(null, null);
                }
                else
                {
                    ShowMessageTQDT("Thông báo từ hệ thống", "CHƯA CÓ DỮ LIỆU ĐƯỢC CHỌN", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XÓA ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.DinhMucDangKy dm = (Company.GC.BLL.KDT.GC.DinhMucDangKy)row.GetRow().DataRow;
                        if (dm.ID > 0)
                        {
                            try
                            {
                                dm.LoadCollection();
                                dm.DeleteDinhMucGC();
                                DinhMuc dmDelete = new DinhMuc();
                                dmDelete.DeleteByMasterID(dm.ID);
                                dm.Delete();                           
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                        }
                    }
                }
                else
                {
                    e.Cancel = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnClose_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnSearch_Click(null, null);    
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnInDMSP_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXRow row = dgList.GetRow();
                if (row == null || row.RowType != RowType.Record) return;
                DinhMucDangKy DMDK = (DinhMucDangKy)row.DataRow;
                Report.ReportViewBC03_DMDKForm reportForm = new Company.Interface.Report.ReportViewBC03_DMDKForm();
                reportForm.DMDangKy = DMDK;
                reportForm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                Company.GC.BLL.KDT.GC.DinhMucDangKyCollection dmdkColl = new DinhMucDangKyCollection();
                Company.GC.BLL.KDT.GC.DinhMucCollection dmColl = new DinhMucCollection();
                Company.GC.BLL.KDT.GC.DinhMuc dmDelete = new Company.GC.BLL.KDT.GC.DinhMuc();
                Company.GC.BLL.GC.DinhMuc dmRegister = new Company.GC.BLL.GC.DinhMuc();
                if (dgList.GetRows().Length < 1) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (items.Count < 0) return;
                if (ShowMessage("DOANH NGHIỆP CÓ MUỐN XÓA ĐỊNH MỨC NÀY KHÔNG ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem row in items)
                    {
                        Company.GC.BLL.KDT.GC.DinhMucDangKy dm = (Company.GC.BLL.KDT.GC.DinhMucDangKy)row.GetRow().DataRow;
                        dm.LoadCollection();
                        if (dm.ID > 0)
                        {
                            dm.DeleteDinhMucGC();
                            dmDelete.DeleteByMasterID(dm.ID);
                            dm.Delete();
                        }
                        dmdkColl.Add(dm);
                    }
                    foreach (Company.GC.BLL.KDT.GC.DinhMucDangKy dmm in dmdkColl)
                    {
                        dmdkCollection.Remove(dmm);
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void uiContextMenu1_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {

        }

        private void btnResultHistory_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.GetRow() == null)
                {
                    showMsg("MSG_2702040");
                    return;
                }
                dmdkSelect = (DinhMucDangKy)dgList.GetRow().DataRow;
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = dmdkSelect.ID;
                form.DeclarationIssuer = DeclarationIssuer.GC_DINH_MUC;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void ckbTimKiem_CheckedChanged(object sender, EventArgs e)
        {
            if (ckbTimKiem.Checked)
                grbtimkiem.Enabled = true;
            else
                grbtimkiem.Enabled = false;
        }

        private void cbHopDong_ValueChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

        private void btnReportĐMTT_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgList.GetRows().Length < 1) return;
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<Company.GC.BLL.KDT.GC.DinhMucDangKy> DinhMucDangKyCollection = new List<DinhMucDangKy>();
                if (items.Count < 0) return;
                foreach (GridEXSelectedItem row in items)
                {
                    if (row.RowType==RowType.Record)
                    {
                        Company.GC.BLL.KDT.GC.DinhMucDangKy dm = (Company.GC.BLL.KDT.GC.DinhMucDangKy)row.GetRow().DataRow;
                        DinhMucDangKyCollection.Add(dm);   
                    }
                }
                List<DinhMuc> DinhMucCollection = new List<DinhMuc>();
                foreach (DinhMucDangKy item in DinhMucDangKyCollection)
                {
                    item.LoadCollection();
                    List<KDT_GC_SanPham> SPCollection = KDT_GC_SanPham.SelectCollectionBy_HopDong_ID(item.ID_HopDong);
                    List<Company.GC.BLL.GC.KDT_GC_NguyenPhuLieu> NPLCollection = Company.GC.BLL.GC.KDT_GC_NguyenPhuLieu.SelectCollectionBy_HopDong_ID(item.ID_HopDong);
                    foreach (DinhMuc dm in item.DMCollection)
                    {
                        dm.HopDong_ID = item.ID_HopDong;
                        foreach (KDT_GC_SanPham SP in SPCollection)
                        {
                            if (SP.Ma == dm.MaSanPham)
                            {
                                dm.DVT = SP.DVT_ID;
                                dm.TenSanPham = SP.Ten;
                            }
                        }
                        foreach (Company.GC.BLL.GC.KDT_GC_NguyenPhuLieu NPL in NPLCollection)
                        {
                            if (NPL.Ma == dm.MaNguyenPhuLieu)
                            {
                                dm.TenNPL = NPL.Ten;
                                dm.DVT_ID = NPL.DVT_ID;
                            }
                        }
                        DinhMucCollection.Add(dm);
                    }
                }
                Report_16_ĐMTT_GSQL f = new Report_16_ĐMTT_GSQL();
                f.BindReport(DinhMucCollection,clcTuNgay.Value,ucCalendar1.Value);
                f.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }

        private void btnAuto_Click(object sender, EventArgs e)
        {
            AutoSendAndFeedbackXMLForm f = new AutoSendAndFeedbackXMLForm();
            f.Type = "DM";
            f.ShowDialog(this);
        }

    }
}
