using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Data;
using ThoughtWorks.QRCode.Codec;
using System.Windows.Forms;

namespace Company.Interface.Report.VNACCS
{
    public partial class BangKeSoContainerXuatKhau_M31_TT38 : DevExpress.XtraReports.UI.XtraReport
    {
        QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
        string encodeBase64String = string.Empty;
        public BangKeSoContainerXuatKhau_M31_TT38()
        {
            InitializeComponent();
        }
        public void BindReport(KDT_ContainerDangKy cont)
        {
            
            long id = cont.TKMD_ID;
            KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(id);
            lblTenDN.Text = TKMD.TenDonVi.ToUpper();
            lblSoToKhai.Text = Convert.ToString(TKMD.SoToKhai);
            
            
            DataTable dt = new DataTable();
            this.DataSource = cont.ListCont;
            lblSoHieuKienCont.DataBindings.Add("Text", this.DataSource, "SoContainer");
            lblSoSeal.DataBindings.Add("Text", this.DataSource, "SoSeal");
            lblGhiChu.DataBindings.Add("Text",this.DataSource,"GhiChu");

            //lblMaVach.DataBindings.Add("Text", this.DataSource, "Code");
            
            //lblNgayGio.Text = "Ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year + " " + DateTime.Now.ToString("hh:mm:ss");
        }
        int stt = 0;
        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            stt += 1;
            lblSTT.Text = stt.ToString();
            //lblMaVach.Text = "aHR0cHM6Ly93d3cuZmFjZWJvb2suY29tLw==";
        }

        private void lblMaVach_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
            //qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            ////Scale
            //qrCodeEncoder.QRCodeScale = 3;
            ////Version
            //qrCodeEncoder.QRCodeVersion = 7;

            //qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.L;

            //Image image;
            ////String data = "aHR0cHM6Ly93d3cuZmFjZWJvb2suY29tLw==";
            //String data = lblMaVach.Text;
            //encodeBase64String = qrCodeEncoder.EncodeToBase64String(data);

            //image = qrCodeEncoder.Encode(data);
            ////PictureBox pic = new PictureBox();
            ////pic.Visible = true;
            //pictureBox1.Image = image;
            
        }

    }
}
