using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Collections.Generic;

namespace Company.Interface.Report.GC.TT39
{
    public partial class Report_15b_BCQT_NLVTNN_GSQL : DevExpress.XtraReports.UI.XtraReport
    {
        public Report_15b_BCQT_NLVTNN_GSQL()
        {
            InitializeComponent();
        }
        public void BindReport(KDT_VNACCS_BaoCaoQuyetToan_TT39 BCQTTT39)
        {
            List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> ToTalHangHoaCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail>();
            foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail HD in BCQTTT39.HopDongCollection)
            {
                foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in HD.HangHoaCollection)
                {
                    ToTalHangHoaCollection.Add(item);
                }
            }
            DetailReport.DataSource = ToTalHangHoaCollection;

            lblTuNgay.Text = BCQTTT39.NgayBatDauBC.ToString("dd/MM/yyyy");
            lblDenNgay.Text = BCQTTT39.NgayKetThucBC.ToString("dd/MM/yyyy");
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            txtTenHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "TenHangHoa");
            txtMaHangHoa.DataBindings.Add("Text", DetailReport.DataSource, "MaHangHoa");
            txtDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT");
            txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonDauKy", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtTaiNhap.DataBindings.Add("Text", DetailReport.DataSource, "LuongTaiNhap", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtNhapKhac.DataBindings.Add("Text", DetailReport.DataSource, "LuongNhapKhac", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtThaydoiMDSD.DataBindings.Add("Text", DetailReport.DataSource, "LuongThaydoiMDSD", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongXuatKhau", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonCuoiKy", Company.KDT.SHARE.Components.Globals.FormatNumber(5, true));
            txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource, "GhiChu");
        }
    }
}
