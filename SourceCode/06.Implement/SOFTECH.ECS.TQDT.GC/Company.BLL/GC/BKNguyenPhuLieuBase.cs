using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.GC
{
    public partial class BKNguyenPhuLieu : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected string _TenNPL = String.Empty;
        protected string _MaNPL = String.Empty;
        protected string _DVT = String.Empty;
        protected decimal _TongNPLCU;
        protected decimal _DonGia;
        protected decimal _TriGia;
        protected string _HinhThucCU = String.Empty;
        protected string _GhiChu = String.Empty;
        protected long _OldHD_ID;
        protected int _STT;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public string TenNPL
        {
            set { this._TenNPL = value; }
            get { return this._TenNPL; }
        }
        public string MaNPL
        {
            set { this._MaNPL = value; }
            get { return this._MaNPL; }
        }
        public string DVT
        {
            set { this._DVT = value; }
            get { return this._DVT; }
        }
        public decimal TongNPLCU
        {
            set { this._TongNPLCU = value; }
            get { return this._TongNPLCU; }
        }
        public decimal DonGia
        {
            set { this._DonGia = value; }
            get { return this._DonGia; }
        }
        public decimal TriGia
        {
            set { this._TriGia = value; }
            get { return this._TriGia; }
        }
        public string HinhThucCU
        {
            set { this._HinhThucCU = value; }
            get { return this._HinhThucCU; }
        }
        public string GhiChu
        {
            set { this._GhiChu = value; }
            get { return this._GhiChu; }
        }
        public long OldHD_ID
        {
            set { this._OldHD_ID = value; }
            get { return this._OldHD_ID; }
        }
        public int STT
        {
            set { this._STT = value; }
            get { return this._STT; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_GC_BKNguyenPhuLieu_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) this._TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) this._DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNPLCU"))) this._TongNPLCU = reader.GetDecimal(reader.GetOrdinal("TongNPLCU"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this._DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) this._TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("HinhThucCU"))) this._HinhThucCU = reader.GetString(reader.GetOrdinal("HinhThucCU"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) this._GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("OldHD_ID"))) this._OldHD_ID = reader.GetInt64(reader.GetOrdinal("OldHD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) this._STT = reader.GetInt32(reader.GetOrdinal("STT"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------



        public DataSet SelectAll()
        {
            string spName = "p_GC_BKNguyenPhuLieu_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_GC_BKNguyenPhuLieu_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_BKNguyenPhuLieu_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_GC_BKNguyenPhuLieu_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public BKNguyenPhuLieuCollection SelectCollectionAll()
        {
            BKNguyenPhuLieuCollection collection = new BKNguyenPhuLieuCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                BKNguyenPhuLieu entity = new BKNguyenPhuLieu();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNPLCU"))) entity.TongNPLCU = reader.GetDecimal(reader.GetOrdinal("TongNPLCU"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("HinhThucCU"))) entity.HinhThucCU = reader.GetString(reader.GetOrdinal("HinhThucCU"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("OldHD_ID"))) entity.OldHD_ID = reader.GetInt64(reader.GetOrdinal("OldHD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public BKNguyenPhuLieuCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            BKNguyenPhuLieuCollection collection = new BKNguyenPhuLieuCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                BKNguyenPhuLieu entity = new BKNguyenPhuLieu();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
                if (!reader.IsDBNull(reader.GetOrdinal("TongNPLCU"))) entity.TongNPLCU = reader.GetDecimal(reader.GetOrdinal("TongNPLCU"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDecimal(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDecimal(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("HinhThucCU"))) entity.HinhThucCU = reader.GetString(reader.GetOrdinal("HinhThucCU"));
                if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
                if (!reader.IsDBNull(reader.GetOrdinal("OldHD_ID"))) entity.OldHD_ID = reader.GetInt64(reader.GetOrdinal("OldHD_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STT"))) entity.STT = reader.GetInt32(reader.GetOrdinal("STT"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKNguyenPhuLieu_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@TongNPLCU", SqlDbType.Decimal, this._TongNPLCU);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, this._TriGia);
            this.db.AddInParameter(dbCommand, "@HinhThucCU", SqlDbType.NVarChar, this._HinhThucCU);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, this._OldHD_ID);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(BKNguyenPhuLieuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BKNguyenPhuLieu item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, BKNguyenPhuLieuCollection collection)
        {
            foreach (BKNguyenPhuLieu item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKNguyenPhuLieu_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@TongNPLCU", SqlDbType.Decimal, this._TongNPLCU);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, this._TriGia);
            this.db.AddInParameter(dbCommand, "@HinhThucCU", SqlDbType.NVarChar, this._HinhThucCU);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, this._OldHD_ID);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(BKNguyenPhuLieuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BKNguyenPhuLieu item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKNguyenPhuLieu_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            this.db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            this.db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, this._DVT);
            this.db.AddInParameter(dbCommand, "@TongNPLCU", SqlDbType.Decimal, this._TongNPLCU);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Decimal, this._DonGia);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Decimal, this._TriGia);
            this.db.AddInParameter(dbCommand, "@HinhThucCU", SqlDbType.NVarChar, this._HinhThucCU);
            this.db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, this._GhiChu);
            this.db.AddInParameter(dbCommand, "@OldHD_ID", SqlDbType.BigInt, this._OldHD_ID);
            this.db.AddInParameter(dbCommand, "@STT", SqlDbType.Int, this._STT);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(BKNguyenPhuLieuCollection collection, SqlTransaction transaction)
        {
            foreach (BKNguyenPhuLieu item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_GC_BKNguyenPhuLieu_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(BKNguyenPhuLieuCollection collection, SqlTransaction transaction)
        {
            foreach (BKNguyenPhuLieu item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(BKNguyenPhuLieuCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BKNguyenPhuLieu item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion
    }
}