﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Thông báo thuế
    /// </summary>
    public class AdditionalDocument : IssueBase
    {
        /// <summary>
        /// Mã thông báo thuế (= 006)
        /// </summary>
        [XmlElement("type")]
        public string Type { get; set; }
        
        /// <summary>
        /// hợp đồng
        /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Ngày hết hạn thông báo thuế 
        /// / hết hạn hợp đồng
        /// </summary>
        [XmlElement("expire")]
        public string Expire { get; set; }
        
        [XmlElement("isDebt")]
        public string IsDebt { get; set; }

        [XmlElement("submit")]
        public string Submit { get; set; }

        [XmlElement("DutyTaxFee")]
        public DutyTaxFee DutyTaxFee { get; set; }        
        /// <summary>
        /// Chương, loại, khoản, mục
        /// </summary>
        [XmlElement("AdditionalInformation")]
        public AdditionalInformation AdditionalInformation { get; set; }
        /// <summary>
        ///  ....  
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }

    }
}
