using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.BLL.SXXK.ThanhKhoan;

namespace Company.Interface.Report.SXXK
{
    public partial class BangKeToKhaiXuat_TT194 : DevExpress.XtraReports.UI.XtraReport
    {
        public int SoHoSo;
        public bool isHoanThue = false;

        public BangKeToKhaiXuat_TT194()
        {
            InitializeComponent(); //
        }
        public void BindReport(int lanThanhLy)
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            DataTable dt = new Company.BLL.KDT.SXXK.BKToKhaiXuat().getBKToKhaiXuatForReport(lanThanhLy, GlobalSettings.MA_DON_VI).Tables[0];
            dt.TableName = "BKToKhaiXuat";
            this.DataSource = dt;
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (SoHoSo > 0)
            {
                lblSHSTK.Text = "Số " + SoHoSo + (lanThanhLy.ToString() != "" ? " - LẦN " + lanThanhLy : "");
                lblSHSTK.Width = 200;
            }
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblSoToKhai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhai");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKy", "{0:dd/MM/yy}");
            lblNgayThucXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayThucXuat", "{0:dd/MM/yy}");
            lblTen.DataBindings.Add("Text", this.DataSource, dt.TableName + ".Ten");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoHopDong");
            //HQ
            xrLabel76.Text = Properties.Settings.Default.TieudeNgay;
            //DN
            xrLabel77.Text = GlobalSettings.TieudeNgay;
        }
    }
}
