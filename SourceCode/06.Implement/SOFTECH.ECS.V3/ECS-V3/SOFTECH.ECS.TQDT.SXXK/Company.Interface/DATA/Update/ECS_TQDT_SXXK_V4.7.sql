﻿
/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Delete]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Insert]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Load]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Update]    Script Date: 09/14/2012 10:12:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_HaiQuan_DonViHaiQuan_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Update]
GO


/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Delete]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Delete]
	@ID char(6)
AS

DELETE FROM 
	[dbo].[t_HaiQuan_DonViHaiQuan]
WHERE
	[ID] = @ID



GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_HaiQuan_DonViHaiQuan] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL



GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Insert]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Insert]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
INSERT INTO [dbo].[t_HaiQuan_DonViHaiQuan]
(
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
)
VALUES
(
	@ID,
	@Ten,
	@DateCreated,
	@DateModified
)



GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_InsertUpdate]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_HaiQuan_DonViHaiQuan] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_HaiQuan_DonViHaiQuan] 
		SET
			[Ten] = @Ten,
			[DateCreated] = @DateCreated,
			[DateModified] = @DateModified
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[t_HaiQuan_DonViHaiQuan]
	(
			[ID],
			[Ten],
			[DateCreated],
			[DateModified]
	)
	VALUES
	(
			@ID,
			@Ten,
			@DateCreated,
			@DateModified
	)	
	END


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Load]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Load]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Load]
	@ID char(6)
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViHaiQuan]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM
	[dbo].[t_HaiQuan_DonViHaiQuan]	



GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Ten],
	[DateCreated],
	[DateModified]
FROM [dbo].[t_HaiQuan_DonViHaiQuan] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL



GO

/****** Object:  StoredProcedure [dbo].[p_HaiQuan_DonViHaiQuan_Update]    Script Date: 09/14/2012 10:12:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_HaiQuan_DonViHaiQuan_Update]
-- Database: ECS_TQDT_GC_V3
-- Author: Ngo Thanh Tung
-- Time created: Friday, August 17, 2012
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_HaiQuan_DonViHaiQuan_Update]
	@ID char(6),
	@Ten nvarchar(50),
	@DateCreated datetime,
	@DateModified datetime
AS

UPDATE
	[dbo].[t_HaiQuan_DonViHaiQuan]
SET
	[Ten] = @Ten,
	[DateCreated] = @DateCreated,
	[DateModified] = @DateModified
WHERE
	[ID] = @ID



GO

if( (select count(*) from dbo.t_HaiQuan_Version WHERE [Version] ='4.7') = 0)
	begin
		insert into dbo.t_HaiQuan_Version values('4.7', getdate(), N'Cập nhật cấu trúc dữ liệu.')
	end 