﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.SXXK
{
    public partial class XemToKhaiNhapNPLForm : Company.Interface.BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public string MaNPL = "";
        public XemToKhaiNhapNPLForm()
        {
            InitializeComponent();
        }

        private void XemDinhMucNPLForm_Load(object sender, EventArgs e)
        {
            this.Text = "Xem tờ khai nhập NPL '" + this.MaNPL + "'";
            this.dgList.Tables[0].Columns["TonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            this.dgList.Tables[0].Columns["TonDau"].TotalFormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;

            this.dgList.DataSource = this.HSTL.GetDanhSachNPLNhapTonByMaNPL(GlobalSettings.SoThapPhan.LuongNPL, this.MaNPL);
        }
    }
}

