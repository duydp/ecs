
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[T_GC_NPL_QUYETOAN]') AND TYPE IN (N'P',N'PC',N'V',N'U'))
DROP TABLE [dbo].[T_GC_NPL_QUYETOAN]
GO
CREATE TABLE [dbo].[T_GC_NPL_QUYETOAN]
(
[MANPL] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TENNPL] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LUONGTONDK] [numeric] (18, 5) NULL,
[TRIGIATONDK] [numeric] (18, 6) NULL,
[LUONGNHAPTK] [numeric] (18, 5) NULL,
[TRIGIANHAPTK] [numeric] (18, 6) NULL,
[LUONGXUATTK] [numeric] (18, 5) NULL,
[TRIGIAXUATTK] [numeric] (18, 6) NULL,
[LUONGTONCK] [numeric] (18, 5) NULL,
[TRIGIATONCK] [numeric] (18, 6) NULL,
[HOPDONG_ID] [bigint] NOT NULL,
[NAMQUYETTOAN] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
--ALTER TABLE [dbo].[T_GC_NPL_QUYETOAN] ADD CONSTRAINT [PK_T_GC_NPL_QUYETOAN] PRIMARY KEY CLUSTERED ([MANPL], [TENNPL],[DVT],[HOPDONG_ID],[NAMQUYETTOAN]) ON [PRIMARY]
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[T_GC_NPL_QUYETOAN_CHITIET]') AND TYPE IN (N'P',N'PC',N'V',N'U'))
DROP TABLE [dbo].[T_GC_NPL_QUYETOAN_CHITIET]
GO
CREATE TABLE [dbo].[T_GC_NPL_QUYETOAN_CHITIET]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TOKHAIXUAT] [decimal] (18, 0) NOT NULL,
[TOKHAINHAP] [decimal] (18, 0) NOT NULL,
[MANPL] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TENNPL] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LUONGXUAT] [numeric] (18, 5) NULL,
[TRIGIAXUAT] [numeric] (18, 6) NULL,
[LUONGNHAP] [numeric] (18, 5) NULL,
[DONGIANHAP] [numeric] (18, 6) NULL,
[TRIGIANHAP] [numeric] (18, 6) NULL,
[TYGIATINHTHUE] [int] NULL,
[HOPDONG_ID] [bigint] NOT NULL,
[NAMQUYETTOAN] [int] NOT NULL
) ON [PRIMARY]

GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[T_GC_SANPHAM_QUYETOAN]') AND TYPE IN (N'P',N'PC',N'V',N'U'))
DROP TABLE [dbo].[T_GC_SANPHAM_QUYETOAN]
GO
CREATE TABLE [dbo].[T_GC_SANPHAM_QUYETOAN]
(
[MASP] [varchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TENSP] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LUONGTONDK] [numeric] (18, 5) NULL,
[TRIGIATONDK] [numeric] (18, 6) NULL,
[LUONGNHAPTK] [numeric] (18, 5) NULL,
[TRIGIANHAPTK] [numeric] (18, 6) NULL,
[LUONGXUATTK] [numeric] (18, 5) NULL,
[TRIGIAXUATTK] [numeric] (18, 6) NULL,
[LUONGTONCK] [numeric] (18, 5) NULL,
[TRIGIATONCK] [numeric] (18, 6) NULL,
[HOPDONG_ID] [bigint] NOT NULL,
[NAMQUYETTOAN] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
IF EXISTS   (SELECT * FROM sys.objects WHERE OBJECT_ID = OBJECT_ID('[dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET]') AND TYPE IN (N'P',N'PC',N'V',N'U'))
DROP TABLE [dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET]
GO
CREATE TABLE [dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET]
(
[ID] [bigint] NOT NULL IDENTITY(1, 1),
[TOKHAIXUAT] [decimal] (18, 0) NOT NULL,
[MASP] [nvarchar] (500) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[TENSP] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[DVT] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[LUONGXUAT] [numeric] (18, 5) NULL,
[DONGIA] [numeric] (18, 6) NULL,
[TRIGIAXUAT] [numeric] (18, 6) NULL,
[TYGIATINHTHUE] [int] NULL,
[HOPDONG_ID] [bigint] NOT NULL,
[NAMQUYETTOAN] [int] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_Insert]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_Update]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_Delete]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_Load]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_Insert]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_Insert]
	@MANPL varchar(500),
	@TENNPL nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS
INSERT INTO [dbo].[T_GC_NPL_QUYETOAN]
(
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
)
VALUES
(
	@MANPL,
	@TENNPL,
	@DVT,
	@LUONGTONDK,
	@TRIGIATONDK,
	@LUONGNHAPTK,
	@TRIGIANHAPTK,
	@LUONGXUATTK,
	@TRIGIAXUATTK,
	@LUONGTONCK,
	@TRIGIATONCK,
	@HOPDONG_ID,
	@NAMQUYETTOAN
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_Update]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_Update]
	@MANPL varchar(500),
	@TENNPL nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS

UPDATE
	[dbo].[T_GC_NPL_QUYETOAN]
SET
	[TENNPL] = @TENNPL,
	[LUONGTONDK] = @LUONGTONDK,
	[TRIGIATONDK] = @TRIGIATONDK,
	[LUONGNHAPTK] = @LUONGNHAPTK,
	[TRIGIANHAPTK] = @TRIGIANHAPTK,
	[LUONGXUATTK] = @LUONGXUATTK,
	[TRIGIAXUATTK] = @TRIGIAXUATTK,
	[LUONGTONCK] = @LUONGTONCK,
	[TRIGIATONCK] = @TRIGIATONCK
WHERE
	[MANPL] = @MANPL
	AND [DVT] = @DVT
	AND [HOPDONG_ID] = @HOPDONG_ID
	AND [NAMQUYETTOAN] = @NAMQUYETTOAN

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_InsertUpdate]
	@MANPL varchar(500),
	@TENNPL nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS
IF EXISTS(SELECT [MANPL], [DVT], [HOPDONG_ID], [NAMQUYETTOAN] FROM [dbo].[T_GC_NPL_QUYETOAN] WHERE [MANPL] = @MANPL AND [DVT] = @DVT AND [HOPDONG_ID] = @HOPDONG_ID AND [NAMQUYETTOAN] = @NAMQUYETTOAN)
	BEGIN
		UPDATE
			[dbo].[T_GC_NPL_QUYETOAN] 
		SET
			[TENNPL] = @TENNPL,
			[LUONGTONDK] = @LUONGTONDK,
			[TRIGIATONDK] = @TRIGIATONDK,
			[LUONGNHAPTK] = @LUONGNHAPTK,
			[TRIGIANHAPTK] = @TRIGIANHAPTK,
			[LUONGXUATTK] = @LUONGXUATTK,
			[TRIGIAXUATTK] = @TRIGIAXUATTK,
			[LUONGTONCK] = @LUONGTONCK,
			[TRIGIATONCK] = @TRIGIATONCK
		WHERE
			[MANPL] = @MANPL
			AND [DVT] = @DVT
			AND [HOPDONG_ID] = @HOPDONG_ID
			AND [NAMQUYETTOAN] = @NAMQUYETTOAN
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[T_GC_NPL_QUYETOAN]
	(
			[MANPL],
			[TENNPL],
			[DVT],
			[LUONGTONDK],
			[TRIGIATONDK],
			[LUONGNHAPTK],
			[TRIGIANHAPTK],
			[LUONGXUATTK],
			[TRIGIAXUATTK],
			[LUONGTONCK],
			[TRIGIATONCK],
			[HOPDONG_ID],
			[NAMQUYETTOAN]
	)
	VALUES
	(
			@MANPL,
			@TENNPL,
			@DVT,
			@LUONGTONDK,
			@TRIGIATONDK,
			@LUONGNHAPTK,
			@TRIGIANHAPTK,
			@LUONGXUATTK,
			@TRIGIAXUATTK,
			@LUONGTONCK,
			@TRIGIATONCK,
			@HOPDONG_ID,
			@NAMQUYETTOAN
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_Delete]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_Delete]
	@MANPL varchar(500),
	@DVT nvarchar(10),
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS

DELETE FROM 
	[dbo].[T_GC_NPL_QUYETOAN]
WHERE
	[MANPL] = @MANPL
	AND [DVT] = @DVT
	AND [HOPDONG_ID] = @HOPDONG_ID
	AND [NAMQUYETTOAN] = @NAMQUYETTOAN

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_GC_NPL_QUYETOAN] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_Load]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_Load]
	@MANPL varchar(500),
	@DVT nvarchar(10),
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_GC_NPL_QUYETOAN]
WHERE
	[MANPL] = @MANPL
	AND [DVT] = @DVT
	AND [HOPDONG_ID] = @HOPDONG_ID
	AND [NAMQUYETTOAN] = @NAMQUYETTOAN
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM [dbo].[T_GC_NPL_QUYETOAN] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_SelectAll]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_GC_NPL_QUYETOAN]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Insert]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Update]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Delete]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Load]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Insert]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Insert]
	@TOKHAIXUAT decimal(18, 0),
	@TOKHAINHAP decimal(18, 0),
	@MANPL varchar(500),
	@TENNPL nvarchar(500),
	@DVT nvarchar(10),
	@LUONGXUAT numeric(18, 5),
	@TRIGIAXUAT numeric(18, 6),
	@LUONGNHAP numeric(18, 5),
	@DONGIANHAP numeric(18, 6),
	@TRIGIANHAP numeric(18, 6),
	@TYGIATINHTHUE int,
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_GC_NPL_QUYETOAN_CHITIET]
(
	[TOKHAIXUAT],
	[TOKHAINHAP],
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGXUAT],
	[TRIGIAXUAT],
	[LUONGNHAP],
	[DONGIANHAP],
	[TRIGIANHAP],
	[TYGIATINHTHUE],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
)
VALUES 
(
	@TOKHAIXUAT,
	@TOKHAINHAP,
	@MANPL,
	@TENNPL,
	@DVT,
	@LUONGXUAT,
	@TRIGIAXUAT,
	@LUONGNHAP,
	@DONGIANHAP,
	@TRIGIANHAP,
	@TYGIATINHTHUE,
	@HOPDONG_ID,
	@NAMQUYETTOAN
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Update]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Update]
	@ID bigint,
	@TOKHAIXUAT decimal(18, 0),
	@TOKHAINHAP decimal(18, 0),
	@MANPL varchar(500),
	@TENNPL nvarchar(500),
	@DVT nvarchar(10),
	@LUONGXUAT numeric(18, 5),
	@TRIGIAXUAT numeric(18, 6),
	@LUONGNHAP numeric(18, 5),
	@DONGIANHAP numeric(18, 6),
	@TRIGIANHAP numeric(18, 6),
	@TYGIATINHTHUE int,
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS

UPDATE
	[dbo].[T_GC_NPL_QUYETOAN_CHITIET]
SET
	[TOKHAIXUAT] = @TOKHAIXUAT,
	[TOKHAINHAP] = @TOKHAINHAP,
	[MANPL] = @MANPL,
	[TENNPL] = @TENNPL,
	[DVT] = @DVT,
	[LUONGXUAT] = @LUONGXUAT,
	[TRIGIAXUAT] = @TRIGIAXUAT,
	[LUONGNHAP] = @LUONGNHAP,
	[DONGIANHAP] = @DONGIANHAP,
	[TRIGIANHAP] = @TRIGIANHAP,
	[TYGIATINHTHUE] = @TYGIATINHTHUE,
	[HOPDONG_ID] = @HOPDONG_ID,
	[NAMQUYETTOAN] = @NAMQUYETTOAN
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_InsertUpdate]
	@ID bigint,
	@TOKHAIXUAT decimal(18, 0),
	@TOKHAINHAP decimal(18, 0),
	@MANPL varchar(500),
	@TENNPL nvarchar(500),
	@DVT nvarchar(10),
	@LUONGXUAT numeric(18, 5),
	@TRIGIAXUAT numeric(18, 6),
	@LUONGNHAP numeric(18, 5),
	@DONGIANHAP numeric(18, 6),
	@TRIGIANHAP numeric(18, 6),
	@TYGIATINHTHUE int,
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_GC_NPL_QUYETOAN_CHITIET] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_GC_NPL_QUYETOAN_CHITIET] 
		SET
			[TOKHAIXUAT] = @TOKHAIXUAT,
			[TOKHAINHAP] = @TOKHAINHAP,
			[MANPL] = @MANPL,
			[TENNPL] = @TENNPL,
			[DVT] = @DVT,
			[LUONGXUAT] = @LUONGXUAT,
			[TRIGIAXUAT] = @TRIGIAXUAT,
			[LUONGNHAP] = @LUONGNHAP,
			[DONGIANHAP] = @DONGIANHAP,
			[TRIGIANHAP] = @TRIGIANHAP,
			[TYGIATINHTHUE] = @TYGIATINHTHUE,
			[HOPDONG_ID] = @HOPDONG_ID,
			[NAMQUYETTOAN] = @NAMQUYETTOAN
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_GC_NPL_QUYETOAN_CHITIET]
		(
			[TOKHAIXUAT],
			[TOKHAINHAP],
			[MANPL],
			[TENNPL],
			[DVT],
			[LUONGXUAT],
			[TRIGIAXUAT],
			[LUONGNHAP],
			[DONGIANHAP],
			[TRIGIANHAP],
			[TYGIATINHTHUE],
			[HOPDONG_ID],
			[NAMQUYETTOAN]
		)
		VALUES 
		(
			@TOKHAIXUAT,
			@TOKHAINHAP,
			@MANPL,
			@TENNPL,
			@DVT,
			@LUONGXUAT,
			@TRIGIAXUAT,
			@LUONGNHAP,
			@DONGIANHAP,
			@TRIGIANHAP,
			@TYGIATINHTHUE,
			@HOPDONG_ID,
			@NAMQUYETTOAN
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Delete]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_GC_NPL_QUYETOAN_CHITIET]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_GC_NPL_QUYETOAN_CHITIET] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Load]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TOKHAIXUAT],
	[TOKHAINHAP],
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGXUAT],
	[TRIGIAXUAT],
	[LUONGNHAP],
	[DONGIANHAP],
	[TRIGIANHAP],
	[TYGIATINHTHUE],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_GC_NPL_QUYETOAN_CHITIET]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TOKHAIXUAT],
	[TOKHAINHAP],
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGXUAT],
	[TRIGIAXUAT],
	[LUONGNHAP],
	[DONGIANHAP],
	[TRIGIANHAP],
	[TYGIATINHTHUE],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM [dbo].[T_GC_NPL_QUYETOAN_CHITIET] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_SelectAll]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_NPL_QUYETOAN_CHITIET_SelectAll]














AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TOKHAIXUAT],
	[TOKHAINHAP],
	[MANPL],
	[TENNPL],
	[DVT],
	[LUONGXUAT],
	[TRIGIAXUAT],
	[LUONGNHAP],
	[DONGIANHAP],
	[TRIGIANHAP],
	[TYGIATINHTHUE],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_GC_NPL_QUYETOAN_CHITIET]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_Insert]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_Update]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_Delete]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_Load]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_Insert]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_Insert]
	@MASP varchar(500),
	@TENSP nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS
INSERT INTO [dbo].[T_GC_SANPHAM_QUYETOAN]
(
	[MASP],
	[TENSP],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
)
VALUES
(
	@MASP,
	@TENSP,
	@DVT,
	@LUONGTONDK,
	@TRIGIATONDK,
	@LUONGNHAPTK,
	@TRIGIANHAPTK,
	@LUONGXUATTK,
	@TRIGIAXUATTK,
	@LUONGTONCK,
	@TRIGIATONCK,
	@HOPDONG_ID,
	@NAMQUYETTOAN
)

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_Update]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_Update]
	@MASP varchar(500),
	@TENSP nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS

UPDATE
	[dbo].[T_GC_SANPHAM_QUYETOAN]
SET
	[TENSP] = @TENSP,
	[LUONGTONDK] = @LUONGTONDK,
	[TRIGIATONDK] = @TRIGIATONDK,
	[LUONGNHAPTK] = @LUONGNHAPTK,
	[TRIGIANHAPTK] = @TRIGIANHAPTK,
	[LUONGXUATTK] = @LUONGXUATTK,
	[TRIGIAXUATTK] = @TRIGIAXUATTK,
	[LUONGTONCK] = @LUONGTONCK,
	[TRIGIATONCK] = @TRIGIATONCK
WHERE
	[MASP] = @MASP
	AND [DVT] = @DVT
	AND [HOPDONG_ID] = @HOPDONG_ID
	AND [NAMQUYETTOAN] = @NAMQUYETTOAN

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_InsertUpdate]
	@MASP varchar(500),
	@TENSP nvarchar(max),
	@DVT nvarchar(10),
	@LUONGTONDK numeric(18, 5),
	@TRIGIATONDK numeric(18, 6),
	@LUONGNHAPTK numeric(18, 5),
	@TRIGIANHAPTK numeric(18, 6),
	@LUONGXUATTK numeric(18, 5),
	@TRIGIAXUATTK numeric(18, 6),
	@LUONGTONCK numeric(18, 5),
	@TRIGIATONCK numeric(18, 6),
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS
IF EXISTS(SELECT [MASP], [DVT], [HOPDONG_ID], [NAMQUYETTOAN] FROM [dbo].[T_GC_SANPHAM_QUYETOAN] WHERE [MASP] = @MASP AND [DVT] = @DVT AND [HOPDONG_ID] = @HOPDONG_ID AND [NAMQUYETTOAN] = @NAMQUYETTOAN)
	BEGIN
		UPDATE
			[dbo].[T_GC_SANPHAM_QUYETOAN] 
		SET
			[TENSP] = @TENSP,
			[LUONGTONDK] = @LUONGTONDK,
			[TRIGIATONDK] = @TRIGIATONDK,
			[LUONGNHAPTK] = @LUONGNHAPTK,
			[TRIGIANHAPTK] = @TRIGIANHAPTK,
			[LUONGXUATTK] = @LUONGXUATTK,
			[TRIGIAXUATTK] = @TRIGIAXUATTK,
			[LUONGTONCK] = @LUONGTONCK,
			[TRIGIATONCK] = @TRIGIATONCK
		WHERE
			[MASP] = @MASP
			AND [DVT] = @DVT
			AND [HOPDONG_ID] = @HOPDONG_ID
			AND [NAMQUYETTOAN] = @NAMQUYETTOAN
	END
ELSE
	BEGIN
	INSERT INTO [dbo].[T_GC_SANPHAM_QUYETOAN]
	(
			[MASP],
			[TENSP],
			[DVT],
			[LUONGTONDK],
			[TRIGIATONDK],
			[LUONGNHAPTK],
			[TRIGIANHAPTK],
			[LUONGXUATTK],
			[TRIGIAXUATTK],
			[LUONGTONCK],
			[TRIGIATONCK],
			[HOPDONG_ID],
			[NAMQUYETTOAN]
	)
	VALUES
	(
			@MASP,
			@TENSP,
			@DVT,
			@LUONGTONDK,
			@TRIGIATONDK,
			@LUONGNHAPTK,
			@TRIGIANHAPTK,
			@LUONGXUATTK,
			@TRIGIAXUATTK,
			@LUONGTONCK,
			@TRIGIATONCK,
			@HOPDONG_ID,
			@NAMQUYETTOAN
	)	
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_Delete]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_Delete]
	@MASP varchar(500),
	@DVT nvarchar(10),
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS

DELETE FROM 
	[dbo].[T_GC_SANPHAM_QUYETOAN]
WHERE
	[MASP] = @MASP
	AND [DVT] = @DVT
	AND [HOPDONG_ID] = @HOPDONG_ID
	AND [NAMQUYETTOAN] = @NAMQUYETTOAN

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_GC_SANPHAM_QUYETOAN] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_Load]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_Load]
	@MASP varchar(500),
	@DVT nvarchar(10),
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MASP],
	[TENSP],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_GC_SANPHAM_QUYETOAN]
WHERE
	[MASP] = @MASP
	AND [DVT] = @DVT
	AND [HOPDONG_ID] = @HOPDONG_ID
	AND [NAMQUYETTOAN] = @NAMQUYETTOAN
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[MASP],
	[TENSP],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM [dbo].[T_GC_SANPHAM_QUYETOAN] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectAll]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[MASP],
	[TENSP],
	[DVT],
	[LUONGTONDK],
	[TRIGIATONDK],
	[LUONGNHAPTK],
	[TRIGIANHAPTK],
	[LUONGXUATTK],
	[TRIGIAXUATTK],
	[LUONGTONCK],
	[TRIGIATONCK],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_GC_SANPHAM_QUYETOAN]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Insert]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Update]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Delete]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Load]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Insert]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Insert]
	@TOKHAIXUAT decimal(18, 0),
	@MASP nvarchar(500),
	@TENSP nvarchar(max),
	@DVT nvarchar(10),
	@LUONGXUAT numeric(18, 5),
	@DONGIA numeric(18, 6),
	@TRIGIAXUAT numeric(18, 6),
	@TYGIATINHTHUE int,
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET]
(
	[TOKHAIXUAT],
	[MASP],
	[TENSP],
	[DVT],
	[LUONGXUAT],
	[DONGIA],
	[TRIGIAXUAT],
	[TYGIATINHTHUE],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
)
VALUES 
(
	@TOKHAIXUAT,
	@MASP,
	@TENSP,
	@DVT,
	@LUONGXUAT,
	@DONGIA,
	@TRIGIAXUAT,
	@TYGIATINHTHUE,
	@HOPDONG_ID,
	@NAMQUYETTOAN
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Update]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Update]
	@ID bigint,
	@TOKHAIXUAT decimal(18, 0),
	@MASP nvarchar(500),
	@TENSP nvarchar(max),
	@DVT nvarchar(10),
	@LUONGXUAT numeric(18, 5),
	@DONGIA numeric(18, 6),
	@TRIGIAXUAT numeric(18, 6),
	@TYGIATINHTHUE int,
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS

UPDATE
	[dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET]
SET
	[TOKHAIXUAT] = @TOKHAIXUAT,
	[MASP] = @MASP,
	[TENSP] = @TENSP,
	[DVT] = @DVT,
	[LUONGXUAT] = @LUONGXUAT,
	[DONGIA] = @DONGIA,
	[TRIGIAXUAT] = @TRIGIAXUAT,
	[TYGIATINHTHUE] = @TYGIATINHTHUE,
	[HOPDONG_ID] = @HOPDONG_ID,
	[NAMQUYETTOAN] = @NAMQUYETTOAN
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_InsertUpdate]
	@ID bigint,
	@TOKHAIXUAT decimal(18, 0),
	@MASP nvarchar(500),
	@TENSP nvarchar(max),
	@DVT nvarchar(10),
	@LUONGXUAT numeric(18, 5),
	@DONGIA numeric(18, 6),
	@TRIGIAXUAT numeric(18, 6),
	@TYGIATINHTHUE int,
	@HOPDONG_ID bigint,
	@NAMQUYETTOAN int
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET] 
		SET
			[TOKHAIXUAT] = @TOKHAIXUAT,
			[MASP] = @MASP,
			[TENSP] = @TENSP,
			[DVT] = @DVT,
			[LUONGXUAT] = @LUONGXUAT,
			[DONGIA] = @DONGIA,
			[TRIGIAXUAT] = @TRIGIAXUAT,
			[TYGIATINHTHUE] = @TYGIATINHTHUE,
			[HOPDONG_ID] = @HOPDONG_ID,
			[NAMQUYETTOAN] = @NAMQUYETTOAN
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET]
		(
			[TOKHAIXUAT],
			[MASP],
			[TENSP],
			[DVT],
			[LUONGXUAT],
			[DONGIA],
			[TRIGIAXUAT],
			[TYGIATINHTHUE],
			[HOPDONG_ID],
			[NAMQUYETTOAN]
		)
		VALUES 
		(
			@TOKHAIXUAT,
			@MASP,
			@TENSP,
			@DVT,
			@LUONGXUAT,
			@DONGIA,
			@TRIGIAXUAT,
			@TYGIATINHTHUE,
			@HOPDONG_ID,
			@NAMQUYETTOAN
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Delete]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Load]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TOKHAIXUAT],
	[MASP],
	[TENSP],
	[DVT],
	[LUONGXUAT],
	[DONGIA],
	[TRIGIAXUAT],
	[TYGIATINHTHUE],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TOKHAIXUAT],
	[MASP],
	[TENSP],
	[DVT],
	[LUONGXUAT],
	[DONGIA],
	[TRIGIAXUAT],
	[TYGIATINHTHUE],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM [dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectAll]
-- Database: ECS_TQDT_GC_V4_07_04_2016_DMH
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_GC_SANPHAM_QUYETOAN_CHITIET_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TOKHAIXUAT],
	[MASP],
	[TENSP],
	[DVT],
	[LUONGXUAT],
	[DONGIA],
	[TRIGIAXUAT],
	[TYGIATINHTHUE],
	[HOPDONG_ID],
	[NAMQUYETTOAN]
FROM
	[dbo].[T_GC_SANPHAM_QUYETOAN_CHITIET]	

GO

 IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '24.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('24.6',GETDATE(), N'CẬP NHẬT KHAI BÁO BÁO BÁO CÁO QUYẾT TOÁN')
END