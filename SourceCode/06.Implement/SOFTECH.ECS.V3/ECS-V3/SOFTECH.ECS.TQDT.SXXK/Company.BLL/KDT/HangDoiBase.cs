using System;
namespace Company.BLL.KDT
{
    public partial class HangDoi : EntityBase
    {
        protected long _ID;
        protected string _LoaiToKhai;

        //gia cong
        protected string _SoHopDong;
        protected DateTime _NgayKy = new DateTime(1900, 1, 1);
        //sxxk
        int sotk;
        short namdangky;
        string maloaihinh;
        //ho so thanh khoan sxxk
        int lanthanhly;
        //chung
        protected string _MaHaiQuan;
        protected int _TrangThai;
        protected string _ChucNang;
        protected string _Pass;
        protected string xml;
        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }

        public int LanThanhLy
        {
            set { this.lanthanhly = value; }
            get { return this.lanthanhly; }
        }
        public string PassWord
        {
            set { this._Pass = value; }
            get { return this._Pass; }
        }
        public string XML
        {
            set { this.xml = value; }
            get { return this.xml; }
        }
        public string LoaiToKhai
        {
            set { this._LoaiToKhai = value; }
            get { return this._LoaiToKhai; }
        }
        public string ChucNang
        {
            set { this._ChucNang = value; }
            get { return this._ChucNang; }
        }

        public int TrangThai
        {
            set { this._TrangThai = value; }
            get { return this._TrangThai; }
        }

        public string SoHopDong
        {
            set { this._SoHopDong = value; }
            get { return this._SoHopDong; }
        }

        public int SoTK
        {
            set { this.sotk = value; }
            get { return this.sotk; }
        }

        public short NamDangKy
        {
            set { this.namdangky = value; }
            get { return this.namdangky; }
        }

        public string MaLoaiHinh
        {
            set { this.maloaihinh = value; }
            get { return this.maloaihinh; }
        }
        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value.Trim(); }
            get { return this._MaHaiQuan.Trim(); }
        }



        public DateTime NgayKy
        {
            set { this._NgayKy = value; }
            get { return this._NgayKy; }
        }
    }
}