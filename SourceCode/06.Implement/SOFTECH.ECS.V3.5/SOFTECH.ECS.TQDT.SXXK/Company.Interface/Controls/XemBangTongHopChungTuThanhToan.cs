﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.Controls
{
    public partial class XemBangTongHopChungTuThanhToan : UserControl
    {
        private List<HoSoThanhLyDangKy> collections = new List<HoSoThanhLyDangKy>();

        public HoSoThanhLyDangKy HoSoThanhLyDangKy
        {
            get
            {
                try
                {
                    return collections.Find(delegate(HoSoThanhLyDangKy o) { return o.ID == Convert.ToInt64(cboSoHoSo.SelectedItem != null ? ((DataRowView)cboSoHoSo.SelectedItem)["ID"] : -1); });
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return null;
                }
            }
        }

        public XemBangTongHopChungTuThanhToan()
        {
            InitializeComponent();
        }

        private void XemBangTongHopChungTuThanhToan_Load(object sender, EventArgs e)
        {
            if (!this.DesignMode)
                LoadData();
        }

        private void LoadData()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                // Xây dựng điều kiện tìm kiếm.
                string where = " MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "'";

                // Thực hiện tìm kiếm.            
                collections = HoSoThanhLyDangKy.SelectCollectionDynamic(where, "ID desc");

                DataTable dt = CreateDataTemplate();

                foreach (HoSoThanhLyDangKy item in collections)
                {
                    DataRow dr = dt.NewRow();
                    dr["LanThanhLy"] = item.LanThanhLy;
                    dr["SoHoSo"] = item.SoHoSo;
                    dr["TrangThaiThanhKhoan"] = Company.KDT.SHARE.Components.Globals.GetTrangThaiThanhKhoan(item.TrangThaiThanhKhoan);
                    dr["Nam"] = item.NgayBatDau.Year;
                    dr["ID"] = item.ID;

                    dt.Rows.Add(dr);
                }

                cboSoHoSo.DataSource = dt;

                cboSoHoSo.SelectedIndex = dt.Rows.Count > 0 ? 0 : -1;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private DataTable CreateDataTemplate()
        {
            DataTable dt = new DataTable("SoHoSo");
            dt.Columns.Add("LanThanhLy", typeof(Int32));
            dt.Columns.Add("SoHoSo", typeof(Int32));
            dt.Columns.Add("TrangThaiThanhKhoan", typeof(String));
            dt.Columns.Add("Nam", typeof(Int32));
            dt.Columns.Add("ID", typeof(Int64));

            return dt;
        }

        public delegate void XemBangTongHopCTTTClickHandler(object sender, EventArgs e);
        public event XemBangTongHopCTTTClickHandler XemBangTongHopClick;

        private void btnXemBangTongHopCTTT_Click(object sender, EventArgs e)
        {
            if (XemBangTongHopClick != null)
            {
                XemBangTongHopClick(sender, e);
            }
        }

    }
}
