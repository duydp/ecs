
Alter table t_KDT_VNACC_HangMauDich
Alter Column ThueSuat numeric(10,3) null
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Insert]
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 3),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 4),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(24, 6),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue varchar(12),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60),
	@MaHangHoa nvarchar(48),
	@Templ_1 varchar(250),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich]
(
	[TKMD_ID],
	[MaSoHang],
	[MaQuanLy],
	[TenHang],
	[ThueSuat],
	[ThueSuatTuyetDoi],
	[MaDVTTuyetDoi],
	[MaTTTuyetDoi],
	[NuocXuatXu],
	[SoLuong1],
	[DVTLuong1],
	[SoLuong2],
	[DVTLuong2],
	[TriGiaHoaDon],
	[DonGiaHoaDon],
	[MaTTDonGia],
	[DVTDonGia],
	[MaBieuThueNK],
	[MaHanNgach],
	[MaThueNKTheoLuong],
	[MaMienGiamThue],
	[SoTienGiamThue],
	[TriGiaTinhThue],
	[MaTTTriGiaTinhThue],
	[SoMucKhaiKhoanDC],
	[SoTTDongHangTKTNTX],
	[SoDMMienThue],
	[SoDongDMMienThue],
	[MaMienGiam],
	[SoTienMienGiam],
	[MaTTSoTienMienGiam],
	[MaVanBanPhapQuyKhac1],
	[MaVanBanPhapQuyKhac2],
	[MaVanBanPhapQuyKhac3],
	[MaVanBanPhapQuyKhac4],
	[MaVanBanPhapQuyKhac5],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam1],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam],
	[MaHangHoa],
	[Templ_1]
)
VALUES 
(
	@TKMD_ID,
	@MaSoHang,
	@MaQuanLy,
	@TenHang,
	@ThueSuat,
	@ThueSuatTuyetDoi,
	@MaDVTTuyetDoi,
	@MaTTTuyetDoi,
	@NuocXuatXu,
	@SoLuong1,
	@DVTLuong1,
	@SoLuong2,
	@DVTLuong2,
	@TriGiaHoaDon,
	@DonGiaHoaDon,
	@MaTTDonGia,
	@DVTDonGia,
	@MaBieuThueNK,
	@MaHanNgach,
	@MaThueNKTheoLuong,
	@MaMienGiamThue,
	@SoTienGiamThue,
	@TriGiaTinhThue,
	@MaTTTriGiaTinhThue,
	@SoMucKhaiKhoanDC,
	@SoTTDongHangTKTNTX,
	@SoDMMienThue,
	@SoDongDMMienThue,
	@MaMienGiam,
	@SoTienMienGiam,
	@MaTTSoTienMienGiam,
	@MaVanBanPhapQuyKhac1,
	@MaVanBanPhapQuyKhac2,
	@MaVanBanPhapQuyKhac3,
	@MaVanBanPhapQuyKhac4,
	@MaVanBanPhapQuyKhac5,
	@SoDong,
	@MaPhanLoaiTaiXacNhanGia,
	@TenNoiXuatXu,
	@SoLuongTinhThue,
	@MaDVTDanhThue,
	@DonGiaTinhThue,
	@DV_SL_TrongDonGiaTinhThue,
	@MaTTDonGiaTinhThue,
	@TriGiaTinhThueS,
	@MaTTTriGiaTinhThueS,
	@MaTTSoTienMienGiam1,
	@MaPhanLoaiThueSuatThue,
	@ThueSuatThue,
	@PhanLoaiThueSuatThue,
	@SoTienThue,
	@MaTTSoTienThueXuatKhau,
	@TienLePhi_DonGia,
	@TienBaoHiem_DonGia,
	@TienLePhi_SoLuong,
	@TienLePhi_MaDVSoLuong,
	@TienBaoHiem_SoLuong,
	@TienBaoHiem_MaDVSoLuong,
	@TienLePhi_KhoanTien,
	@TienBaoHiem_KhoanTien,
	@DieuKhoanMienGiam,
	@MaHangHoa,
	@Templ_1
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 3),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 4),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(24, 6),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue varchar(12),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60),
	@MaHangHoa nvarchar(48),
	@Templ_1 varchar(250)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich]
SET
	[TKMD_ID] = @TKMD_ID,
	[MaSoHang] = @MaSoHang,
	[MaQuanLy] = @MaQuanLy,
	[TenHang] = @TenHang,
	[ThueSuat] = @ThueSuat,
	[ThueSuatTuyetDoi] = @ThueSuatTuyetDoi,
	[MaDVTTuyetDoi] = @MaDVTTuyetDoi,
	[MaTTTuyetDoi] = @MaTTTuyetDoi,
	[NuocXuatXu] = @NuocXuatXu,
	[SoLuong1] = @SoLuong1,
	[DVTLuong1] = @DVTLuong1,
	[SoLuong2] = @SoLuong2,
	[DVTLuong2] = @DVTLuong2,
	[TriGiaHoaDon] = @TriGiaHoaDon,
	[DonGiaHoaDon] = @DonGiaHoaDon,
	[MaTTDonGia] = @MaTTDonGia,
	[DVTDonGia] = @DVTDonGia,
	[MaBieuThueNK] = @MaBieuThueNK,
	[MaHanNgach] = @MaHanNgach,
	[MaThueNKTheoLuong] = @MaThueNKTheoLuong,
	[MaMienGiamThue] = @MaMienGiamThue,
	[SoTienGiamThue] = @SoTienGiamThue,
	[TriGiaTinhThue] = @TriGiaTinhThue,
	[MaTTTriGiaTinhThue] = @MaTTTriGiaTinhThue,
	[SoMucKhaiKhoanDC] = @SoMucKhaiKhoanDC,
	[SoTTDongHangTKTNTX] = @SoTTDongHangTKTNTX,
	[SoDMMienThue] = @SoDMMienThue,
	[SoDongDMMienThue] = @SoDongDMMienThue,
	[MaMienGiam] = @MaMienGiam,
	[SoTienMienGiam] = @SoTienMienGiam,
	[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
	[MaVanBanPhapQuyKhac1] = @MaVanBanPhapQuyKhac1,
	[MaVanBanPhapQuyKhac2] = @MaVanBanPhapQuyKhac2,
	[MaVanBanPhapQuyKhac3] = @MaVanBanPhapQuyKhac3,
	[MaVanBanPhapQuyKhac4] = @MaVanBanPhapQuyKhac4,
	[MaVanBanPhapQuyKhac5] = @MaVanBanPhapQuyKhac5,
	[SoDong] = @SoDong,
	[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
	[TenNoiXuatXu] = @TenNoiXuatXu,
	[SoLuongTinhThue] = @SoLuongTinhThue,
	[MaDVTDanhThue] = @MaDVTDanhThue,
	[DonGiaTinhThue] = @DonGiaTinhThue,
	[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
	[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
	[TriGiaTinhThueS] = @TriGiaTinhThueS,
	[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
	[MaTTSoTienMienGiam1] = @MaTTSoTienMienGiam1,
	[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
	[ThueSuatThue] = @ThueSuatThue,
	[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
	[SoTienThue] = @SoTienThue,
	[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
	[TienLePhi_DonGia] = @TienLePhi_DonGia,
	[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
	[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
	[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
	[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
	[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
	[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
	[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
	[DieuKhoanMienGiam] = @DieuKhoanMienGiam,
	[MaHangHoa] = @MaHangHoa,
	[Templ_1] = @Templ_1
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@MaSoHang varchar(12),
	@MaQuanLy varchar(7),
	@TenHang nvarchar(200),
	@ThueSuat numeric(10, 3),
	@ThueSuatTuyetDoi numeric(14, 4),
	@MaDVTTuyetDoi varchar(4),
	@MaTTTuyetDoi varchar(4),
	@NuocXuatXu varchar(2),
	@SoLuong1 numeric(12, 4),
	@DVTLuong1 varchar(4),
	@SoLuong2 numeric(12, 4),
	@DVTLuong2 varchar(4),
	@TriGiaHoaDon numeric(24, 4),
	@DonGiaHoaDon numeric(24, 6),
	@MaTTDonGia varchar(4),
	@DVTDonGia varchar(3),
	@MaBieuThueNK varchar(3),
	@MaHanNgach varchar(1),
	@MaThueNKTheoLuong varchar(10),
	@MaMienGiamThue varchar(5),
	@SoTienGiamThue numeric(20, 4),
	@TriGiaTinhThue numeric(24, 4),
	@MaTTTriGiaTinhThue varchar(3),
	@SoMucKhaiKhoanDC varchar(5),
	@SoTTDongHangTKTNTX varchar(2),
	@SoDMMienThue varchar(12),
	@SoDongDMMienThue varchar(3),
	@MaMienGiam varchar(5),
	@SoTienMienGiam numeric(20, 4),
	@MaTTSoTienMienGiam varchar(3),
	@MaVanBanPhapQuyKhac1 varchar(2),
	@MaVanBanPhapQuyKhac2 varchar(2),
	@MaVanBanPhapQuyKhac3 varchar(2),
	@MaVanBanPhapQuyKhac4 varchar(2),
	@MaVanBanPhapQuyKhac5 varchar(2),
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(16, 4),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(22, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(21, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam1 varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(20, 4),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(16, 4),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(16, 4),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(20, 4),
	@TienBaoHiem_KhoanTien numeric(20, 4),
	@DieuKhoanMienGiam varchar(60),
	@MaHangHoa nvarchar(48),
	@Templ_1 varchar(250)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[MaSoHang] = @MaSoHang,
			[MaQuanLy] = @MaQuanLy,
			[TenHang] = @TenHang,
			[ThueSuat] = @ThueSuat,
			[ThueSuatTuyetDoi] = @ThueSuatTuyetDoi,
			[MaDVTTuyetDoi] = @MaDVTTuyetDoi,
			[MaTTTuyetDoi] = @MaTTTuyetDoi,
			[NuocXuatXu] = @NuocXuatXu,
			[SoLuong1] = @SoLuong1,
			[DVTLuong1] = @DVTLuong1,
			[SoLuong2] = @SoLuong2,
			[DVTLuong2] = @DVTLuong2,
			[TriGiaHoaDon] = @TriGiaHoaDon,
			[DonGiaHoaDon] = @DonGiaHoaDon,
			[MaTTDonGia] = @MaTTDonGia,
			[DVTDonGia] = @DVTDonGia,
			[MaBieuThueNK] = @MaBieuThueNK,
			[MaHanNgach] = @MaHanNgach,
			[MaThueNKTheoLuong] = @MaThueNKTheoLuong,
			[MaMienGiamThue] = @MaMienGiamThue,
			[SoTienGiamThue] = @SoTienGiamThue,
			[TriGiaTinhThue] = @TriGiaTinhThue,
			[MaTTTriGiaTinhThue] = @MaTTTriGiaTinhThue,
			[SoMucKhaiKhoanDC] = @SoMucKhaiKhoanDC,
			[SoTTDongHangTKTNTX] = @SoTTDongHangTKTNTX,
			[SoDMMienThue] = @SoDMMienThue,
			[SoDongDMMienThue] = @SoDongDMMienThue,
			[MaMienGiam] = @MaMienGiam,
			[SoTienMienGiam] = @SoTienMienGiam,
			[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
			[MaVanBanPhapQuyKhac1] = @MaVanBanPhapQuyKhac1,
			[MaVanBanPhapQuyKhac2] = @MaVanBanPhapQuyKhac2,
			[MaVanBanPhapQuyKhac3] = @MaVanBanPhapQuyKhac3,
			[MaVanBanPhapQuyKhac4] = @MaVanBanPhapQuyKhac4,
			[MaVanBanPhapQuyKhac5] = @MaVanBanPhapQuyKhac5,
			[SoDong] = @SoDong,
			[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
			[TenNoiXuatXu] = @TenNoiXuatXu,
			[SoLuongTinhThue] = @SoLuongTinhThue,
			[MaDVTDanhThue] = @MaDVTDanhThue,
			[DonGiaTinhThue] = @DonGiaTinhThue,
			[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
			[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
			[TriGiaTinhThueS] = @TriGiaTinhThueS,
			[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
			[MaTTSoTienMienGiam1] = @MaTTSoTienMienGiam1,
			[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
			[ThueSuatThue] = @ThueSuatThue,
			[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
			[SoTienThue] = @SoTienThue,
			[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
			[TienLePhi_DonGia] = @TienLePhi_DonGia,
			[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
			[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
			[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
			[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
			[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
			[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
			[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
			[DieuKhoanMienGiam] = @DieuKhoanMienGiam,
			[MaHangHoa] = @MaHangHoa,
			[Templ_1] = @Templ_1
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich]
		(
			[TKMD_ID],
			[MaSoHang],
			[MaQuanLy],
			[TenHang],
			[ThueSuat],
			[ThueSuatTuyetDoi],
			[MaDVTTuyetDoi],
			[MaTTTuyetDoi],
			[NuocXuatXu],
			[SoLuong1],
			[DVTLuong1],
			[SoLuong2],
			[DVTLuong2],
			[TriGiaHoaDon],
			[DonGiaHoaDon],
			[MaTTDonGia],
			[DVTDonGia],
			[MaBieuThueNK],
			[MaHanNgach],
			[MaThueNKTheoLuong],
			[MaMienGiamThue],
			[SoTienGiamThue],
			[TriGiaTinhThue],
			[MaTTTriGiaTinhThue],
			[SoMucKhaiKhoanDC],
			[SoTTDongHangTKTNTX],
			[SoDMMienThue],
			[SoDongDMMienThue],
			[MaMienGiam],
			[SoTienMienGiam],
			[MaTTSoTienMienGiam],
			[MaVanBanPhapQuyKhac1],
			[MaVanBanPhapQuyKhac2],
			[MaVanBanPhapQuyKhac3],
			[MaVanBanPhapQuyKhac4],
			[MaVanBanPhapQuyKhac5],
			[SoDong],
			[MaPhanLoaiTaiXacNhanGia],
			[TenNoiXuatXu],
			[SoLuongTinhThue],
			[MaDVTDanhThue],
			[DonGiaTinhThue],
			[DV_SL_TrongDonGiaTinhThue],
			[MaTTDonGiaTinhThue],
			[TriGiaTinhThueS],
			[MaTTTriGiaTinhThueS],
			[MaTTSoTienMienGiam1],
			[MaPhanLoaiThueSuatThue],
			[ThueSuatThue],
			[PhanLoaiThueSuatThue],
			[SoTienThue],
			[MaTTSoTienThueXuatKhau],
			[TienLePhi_DonGia],
			[TienBaoHiem_DonGia],
			[TienLePhi_SoLuong],
			[TienLePhi_MaDVSoLuong],
			[TienBaoHiem_SoLuong],
			[TienBaoHiem_MaDVSoLuong],
			[TienLePhi_KhoanTien],
			[TienBaoHiem_KhoanTien],
			[DieuKhoanMienGiam],
			[MaHangHoa],
			[Templ_1]
		)
		VALUES 
		(
			@TKMD_ID,
			@MaSoHang,
			@MaQuanLy,
			@TenHang,
			@ThueSuat,
			@ThueSuatTuyetDoi,
			@MaDVTTuyetDoi,
			@MaTTTuyetDoi,
			@NuocXuatXu,
			@SoLuong1,
			@DVTLuong1,
			@SoLuong2,
			@DVTLuong2,
			@TriGiaHoaDon,
			@DonGiaHoaDon,
			@MaTTDonGia,
			@DVTDonGia,
			@MaBieuThueNK,
			@MaHanNgach,
			@MaThueNKTheoLuong,
			@MaMienGiamThue,
			@SoTienGiamThue,
			@TriGiaTinhThue,
			@MaTTTriGiaTinhThue,
			@SoMucKhaiKhoanDC,
			@SoTTDongHangTKTNTX,
			@SoDMMienThue,
			@SoDongDMMienThue,
			@MaMienGiam,
			@SoTienMienGiam,
			@MaTTSoTienMienGiam,
			@MaVanBanPhapQuyKhac1,
			@MaVanBanPhapQuyKhac2,
			@MaVanBanPhapQuyKhac3,
			@MaVanBanPhapQuyKhac4,
			@MaVanBanPhapQuyKhac5,
			@SoDong,
			@MaPhanLoaiTaiXacNhanGia,
			@TenNoiXuatXu,
			@SoLuongTinhThue,
			@MaDVTDanhThue,
			@DonGiaTinhThue,
			@DV_SL_TrongDonGiaTinhThue,
			@MaTTDonGiaTinhThue,
			@TriGiaTinhThueS,
			@MaTTTriGiaTinhThueS,
			@MaTTSoTienMienGiam1,
			@MaPhanLoaiThueSuatThue,
			@ThueSuatThue,
			@PhanLoaiThueSuatThue,
			@SoTienThue,
			@MaTTSoTienThueXuatKhau,
			@TienLePhi_DonGia,
			@TienBaoHiem_DonGia,
			@TienLePhi_SoLuong,
			@TienLePhi_MaDVSoLuong,
			@TienBaoHiem_SoLuong,
			@TienBaoHiem_MaDVSoLuong,
			@TienLePhi_KhoanTien,
			@TienBaoHiem_KhoanTien,
			@DieuKhoanMienGiam,
			@MaHangHoa,
			@Templ_1
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaQuanLy],
	[TenHang],
	[ThueSuat],
	[ThueSuatTuyetDoi],
	[MaDVTTuyetDoi],
	[MaTTTuyetDoi],
	[NuocXuatXu],
	[SoLuong1],
	[DVTLuong1],
	[SoLuong2],
	[DVTLuong2],
	[TriGiaHoaDon],
	[DonGiaHoaDon],
	[MaTTDonGia],
	[DVTDonGia],
	[MaBieuThueNK],
	[MaHanNgach],
	[MaThueNKTheoLuong],
	[MaMienGiamThue],
	[SoTienGiamThue],
	[TriGiaTinhThue],
	[MaTTTriGiaTinhThue],
	[SoMucKhaiKhoanDC],
	[SoTTDongHangTKTNTX],
	[SoDMMienThue],
	[SoDongDMMienThue],
	[MaMienGiam],
	[SoTienMienGiam],
	[MaTTSoTienMienGiam],
	[MaVanBanPhapQuyKhac1],
	[MaVanBanPhapQuyKhac2],
	[MaVanBanPhapQuyKhac3],
	[MaVanBanPhapQuyKhac4],
	[MaVanBanPhapQuyKhac5],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam1],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam],
	[MaHangHoa],
	[Templ_1]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaQuanLy],
	[TenHang],
	[ThueSuat],
	[ThueSuatTuyetDoi],
	[MaDVTTuyetDoi],
	[MaTTTuyetDoi],
	[NuocXuatXu],
	[SoLuong1],
	[DVTLuong1],
	[SoLuong2],
	[DVTLuong2],
	[TriGiaHoaDon],
	[DonGiaHoaDon],
	[MaTTDonGia],
	[DVTDonGia],
	[MaBieuThueNK],
	[MaHanNgach],
	[MaThueNKTheoLuong],
	[MaMienGiamThue],
	[SoTienGiamThue],
	[TriGiaTinhThue],
	[MaTTTriGiaTinhThue],
	[SoMucKhaiKhoanDC],
	[SoTTDongHangTKTNTX],
	[SoDMMienThue],
	[SoDongDMMienThue],
	[MaMienGiam],
	[SoTienMienGiam],
	[MaTTSoTienMienGiam],
	[MaVanBanPhapQuyKhac1],
	[MaVanBanPhapQuyKhac2],
	[MaVanBanPhapQuyKhac3],
	[MaVanBanPhapQuyKhac4],
	[MaVanBanPhapQuyKhac5],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam1],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam],
	[MaHangHoa],
	[Templ_1]
FROM [dbo].[t_KDT_VNACC_HangMauDich] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, June 25, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[MaSoHang],
	[MaQuanLy],
	[TenHang],
	[ThueSuat],
	[ThueSuatTuyetDoi],
	[MaDVTTuyetDoi],
	[MaTTTuyetDoi],
	[NuocXuatXu],
	[SoLuong1],
	[DVTLuong1],
	[SoLuong2],
	[DVTLuong2],
	[TriGiaHoaDon],
	[DonGiaHoaDon],
	[MaTTDonGia],
	[DVTDonGia],
	[MaBieuThueNK],
	[MaHanNgach],
	[MaThueNKTheoLuong],
	[MaMienGiamThue],
	[SoTienGiamThue],
	[TriGiaTinhThue],
	[MaTTTriGiaTinhThue],
	[SoMucKhaiKhoanDC],
	[SoTTDongHangTKTNTX],
	[SoDMMienThue],
	[SoDongDMMienThue],
	[MaMienGiam],
	[SoTienMienGiam],
	[MaTTSoTienMienGiam],
	[MaVanBanPhapQuyKhac1],
	[MaVanBanPhapQuyKhac2],
	[MaVanBanPhapQuyKhac3],
	[MaVanBanPhapQuyKhac4],
	[MaVanBanPhapQuyKhac5],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam1],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam],
	[MaHangHoa],
	[Templ_1]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich]	

GO


/****** Object:  UserDefinedFunction [dbo].[f_KiemTra_TienTrinh_ThanhLy]    Script Date: 06/25/2014 08:10:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER FUNCTION [dbo].[f_KiemTra_TienTrinh_ThanhLy]
(
	@MaHaiQuan VARCHAR(10),
	@MaDoanhNghiep VARCHAR(50),
	@SoToKhai INT,
	@MaLoaihinh VARCHAR(10),
	@NamDangKy INT,
	@MaNPL NVARCHAR(255),
	@LuongTonHienTai NUMERIC(18,8)
	
)
RETURNS NVARCHAR(500)
AS 
    
/*
@SOFTECH
HUNGTQ, CREATED 04/08/2011.
Kiem tra tien trinh thuc hien chay thanh ly: . 
Gia tri dung phai la so ton cuoi cua lan thanh ly dau = so ton dau cua lan thanh ly tiep theo. 
*/
	
BEGIN

--SET @MaHaiQuan = 'C34C'
--SET @MaDoanhNghiep = '0400101556'
--SET @SoToKhai = 152
--SET @MaLoaihinh = 'NSX01'
--SET @NamDangKy = 2010
--SET @MaNPL = 'NEPLUNG'
			
DECLARE
	@LanThanhLy INT,
	@Luong NUMERIC(18,8),
	@Luong_BanLuu NUMERIC(18,8),
	@Luong_NhapTon NUMERIC(18,8),
	@TonDau NUMERIC(18,8),
	@TonCuoi NUMERIC(18,8),
	@TonCuoi_BanLuu NUMERIC(18,8),
	@TonDauThueXNK NUMERIC(18,8),
	@TonCuoiThueXNK NUMERIC(18,8),
	@cnt1 INT,--Bien dem
	@str NVARCHAR(500),
	@ketqua NVARCHAR(500), --hien thi ket qua
	@SaiSoLuong INT,
	@Valid BIT,
	@Dem INT,
	@isDongHoSo INT
	-- Đếm số dòng thanh lý
	SELECT  @Dem = COUNT(*)
	FROM      [dbo].[t_KDT_SXXK_NPLNhapTon]
	WHERE     SoToKhai = @SoToKhai
			AND MaLoaiHinh = @MaLoaiHinh
			AND NamDangKy = @NamDangKy
			AND MaNPL = @MaNPL
			AND TonCuoi < TonDau
			AND LanThanhLy IN ( SELECT  LanThanhLy
								FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
			AND MaHaiQuan = @MaHaiQuan
			AND MaDoanhNghiep = @MaDoanhNghiep
/*<I> Cap nhat lech luong nhap do bi lam tron so thap phan*/	
DECLARE curThanhLy CURSOR FOR
	SELECT  nplTon.LanThanhLy, Luong, TonDau, TonCuoi, TonDauThueXNK, TonCuoiThueXNK
	,CASE 
			WHEN  EXISTS(SELECT TrangThaiThanhKhoan  FROM t_KDT_SXXK_HoSoThanhLyDangKy hs WHERE hs.LanThanhLy = nplTon.LanThanhLy AND hs.TrangThaiThanhKhoan = 401) THEN 1
			ELSE  0
			END AS IsDongHoSo
	FROM      [dbo].[t_KDT_SXXK_NPLNhapTon] nplTon
	WHERE     SoToKhai = @SoToKhai
			AND MaLoaiHinh = @MaLoaiHinh
			AND NamDangKy = @NamDangKy
			AND MaNPL = @MaNPL
			AND TonCuoi < TonDau
			AND LanThanhLy IN ( SELECT  LanThanhLy
								FROM    t_KDT_SXXK_HoSoThanhLyDangKy )
			AND MaHaiQuan = @MaHaiQuan
			AND MaDoanhNghiep = @MaDoanhNghiep
	ORDER BY TonCuoi DESC
	
	
	
--BEGIN TRANSACTION T1 ;

	SET @Valid = 1
	SET @SaiSoLuong = 0	
    SET @cnt1 = 0;
    
	SET @str = '....................................................................' + char(10)
	SET @str = @str + '		STT' + ' - ' + + 'SoToKhai' + ' - ' + 'MaLoaiHinh' + ' - ' + 'NamDangKy' + ' - ' + 'MaNPL' + char(10)
	SET @str = @str + STR(@cnt1) + ' - ' + STR(@SoToKhai) + ' - ' + @MaLoaiHinh + ' - ' + str(@NamDangKy)+ ' - ' + @MaNPL + char(10)
	--PRINT @str								
	
    OPEN curThanhLy 
    FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK, @isDongHoSo
	--Fetch next record
    WHILE @@FETCH_STATUS = 0 
		BEGIN
							
			SET @cnt1 = @cnt1 + 1
						
			--PRINT STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau: ' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi: ' + cast(@TonCuoi AS VARCHAR(50))
										
			IF (@cnt1 = 1)
				BEGIN
					
					--Kiem tra so luong nhap dang ky ban dau so voi luong ton dau.
					IF (@Luong <> @TonDau)
						BEGIN
							SET @ketqua = N'Sai lượng nhập và lượng tồn đầu tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
							SET @Valid = 0
						END				
					
						  
					IF(@Valid = 1)
						BEGIN
							--Kiem tra so luong nhap dang ky ban dau so voi luong nhap luu trong ton.
							SELECT  @SaiSoLuong =  COUNT(*), @Luong_NhapTon = nt.Luong
								  FROM      [dbo].t_SXXK_ThanhLy_NPLNhapTon nt
											INNER JOIN dbo.t_SXXK_HangMauDich h ON nt.MaNPL = h.MaPhu
																				   AND nt.SoToKhai = h.SoToKhai
																				   AND nt.MaLoaiHinh = h.MaLoaiHinh
																				   AND nt.NamDangKy = h.NamDangKy
																				   AND nt.MaHaiQuan = h.MaHaiQuan
								  WHERE     nt.SoToKhai = @SoToKhai
											AND nt.MaLoaiHinh = @MaLoaiHinh
											AND nt.NamDangKy = @NamDangKy
											AND nt.MaNPL = @MaNPL
											AND nt.MaHaiQuan = @MaHaiQuan
											AND nt.MaDoanhNghiep = @MaDoanhNghiep
											AND nt.Luong <> h.SoLuong
								  GROUP BY nt.Luong
						  
							IF (@SaiSoLuong > 0 AND @Luong_NhapTon <> @Luong)
								BEGIN
									SET @ketqua = N'Sai lượng nhập tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
									SET @Valid = 0
								END
							ELSE
								BEGIN	  
									SET @ketqua = N'Thanh lý đúng'
									SET @Valid = 1
									SET @TonCuoi_BanLuu = @TonCuoi
								END
						END
				END
			
			--Kiem tra so ton
			IF (@cnt1 > 1 AND @Valid = 1) --kiem tra tu dong thu 2 tro di
				BEGIN

					SET @str = STR(@cnt1) + '	-> Lan thanh ly : ' + cast(@LanThanhLy AS VARCHAR(50)) + '	Luong' + cast(@Luong AS VARCHAR(50)) + '	-	TonDau' + cast(@TonDau AS VARCHAR(50)) + '	-	TonCuoi' + cast(@TonCuoi AS VARCHAR(50))
					
					IF (@TonDau <> @TonCuoi_BanLuu)
						BEGIN
							
							SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@TonCuoi_BanLuu AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai tại lần thanh lý ' + cast(@LanThanhLy AS VARCHAR(50))
						END
					ELSE
						SET @ketqua = N'Thanh lý đúng'
				END		
				
					--Kiem tra so ton dong cuoi				
			IF(@cnt1 = @Dem)
			BEGIN
				IF(@isDongHoSo != 1 AND  @LuongTonHienTai <> @TonDau)
				BEGIN
					SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@LuongTonHienTai AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai số liệu hiện tại' + cast(@LanThanhLy AS VARCHAR(50))
				END
				IF(@isDongHoSo = 1 AND  @LuongTonHienTai <> @TonCuoi)
				BEGIN
					SET @str = @str +  '	=> Sai so lieu ton: ' + cast(@TonDau AS VARCHAR(50)) + ' <> ' + cast(@LuongTonHienTai AS VARCHAR(50))
							--PRINT @str
							
							SET @ketqua = N'Sai số liệu hiện tại' + cast(@LanThanhLy AS VARCHAR(50))
				END
			END
			--Luu lai ton cuoi cua row truoc do
			SET @TonCuoi_BanLuu = @TonCuoi
				
			FETCH NEXT FROM curThanhLy INTO @LanThanhLy, @Luong, @TonDau, @TonCuoi, @TonDauThueXNK, @TonCuoiThueXNK, @isDongHoSo
        END
        
    CLOSE curThanhLy --Close cursor
    DEALLOCATE curThanhLy --Deallocate cursor
	
	--PRINT @ketqua
	
    --IF @@ERROR != 0 
    --    BEGIN
            --PRINT 'Qua trinh thuc hien xay ra loi.'
            --PRINT STR(@@ERROR)
            ----ROLLBACK TRANSACTION T1 ;
            --PRINT 'ROLLBACK TRANSACTION T1'
  --      END
  --  ELSE 
		--BEGIN
			----COMMIT TRANSACTION T1 ;	
			--PRINT ''
			----PRINT 'COMMIT TRANSACTION T1'
		--END
	
	RETURN @ketqua
	
END	

Go

update t_VNACC_Category_CityUNLOCODE set CityNameOrStateName='' where LOCODE like '%ZZZ'
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.1') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.1',GETDATE(), N' Cập nhật t_kdt_vnacc_HangMauDich, f_KiemTra_TienTrinh_ThanhLy')
END	