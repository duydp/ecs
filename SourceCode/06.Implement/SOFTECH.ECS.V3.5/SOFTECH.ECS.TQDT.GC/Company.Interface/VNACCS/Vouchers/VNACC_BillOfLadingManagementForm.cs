﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class VNACC_BillOfLadingManagementForm : BaseForm
    {
        public KDT_VNACCS_BillOfLadingNew billOfLadingNew = new KDT_VNACCS_BillOfLadingNew();
        public bool isBrowser = false;
        public VNACC_BillOfLadingManagementForm()
        {
            InitializeComponent();
        }

        private void VNACC_BillOfLadingManagementForm_Load(object sender, EventArgs e)
        {
            ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            clcTuNgay.Value = DateTime.Now;
            clcDenNgay.Value = clcTuNgay.Value.AddDays(30);
            btnSearch_Click(null,null);
        }
        private string GetSearchWhere()
        {
            try
            {
                string where = " 1= 1 ";
                if (!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Code.ToString()))
                    where += " AND A.MaHQ ='" + ctrCoQuanHaiQuan.Code.ToString() + "'";
                if (!String.IsNullOrEmpty(txtSoTN.Text.ToString()))
                    where += " AND A.SoTN LIKE '%" + txtSoTN.Text.ToString() + "%'";
                if (cbStatus.SelectedValue != null)
                    where += " AND A.TrangThaiXuLy =" + cbStatus.SelectedValue.ToString() + "";
                if (!String.IsNullOrEmpty(txtSoVanDonGoc.Text.ToString()))
                    where += " AND B.SoVanDonGoc LIKE '%" + txtSoVanDonGoc.Text.ToString() + "%'";
                if (!String.IsNullOrEmpty(txtMaNguoiPH.Text.ToString()))
                    where += " AND B.MaNguoiPhatHanh LIKE '%" + txtMaNguoiPH.Text.ToString() + "%'";

                where += " AND A.NgayTN BETWEEN '" + clcTuNgay.Value.ToString("yyyy-MM-dd 00:00:00") + "' AND '" + clcDenNgay.Value.ToString("yyyy-MM-dd 23:59:59") + "'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1";
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = KDT_VNACCS_BillOfLadingNew.SelectDynamicNew(GetSearchWhere(),"").Tables[0];
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    if (billOfLadingNew == null)
                    {
                        billOfLadingNew = new KDT_VNACCS_BillOfLadingNew();
                    }
                    billOfLadingNew.ID = Convert.ToInt64(dgList.GetRow().Cells["ID"].Value.ToString());
                    billOfLadingNew = KDT_VNACCS_BillOfLadingNew.Load(billOfLadingNew.ID);
                    VNACC_BillOfLadingForm f = new VNACC_BillOfLadingForm();
                    f.billOfLadingNew = billOfLadingNew;
                    if (isBrowser)
                    {
                        this.Close();
                    }
                    else
                    {
                        f.ShowDialog(this);
                    }
                } 
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách khai báo Tách vận đơn  Từ ngày_" + clcTuNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + " Đến ngày _" + clcDenNgay.Value.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessage("Doanh nghiệp có muốn xoá dòng hàng này không ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem row in items)
                    {
                        if (row.RowType == RowType.Record)
                        {
                             billOfLadingNew = new KDT_VNACCS_BillOfLadingNew();
                             billOfLadingNew = (KDT_VNACCS_BillOfLadingNew)row.GetRow().DataRow;
                             billOfLadingNew.Collection = KDT_VNACCS_BillOfLadings_Detail.SelectCollectionBy_BillOfLadings_ID(billOfLadingNew.ID);
                             foreach (KDT_VNACCS_BillOfLadings_Detail item in billOfLadingNew.Collection)
                             {
                                 item.Collection = KDT_VNACCS_BranchDetail.SelectCollectionBy_BillOfLadings_Details_ID(item.ID);
                                 foreach (KDT_VNACCS_BranchDetail branchDetail in item.Collection)
                                 {
                                     branchDetail.Collection = KDT_VNACCS_BranchDetail_TransportEquipment.SelectCollectionBy_BranchDetail_ID(branchDetail.ID);
                                 }
                             }
                             billOfLadingNew.DeleteFull();
                        }
                    }
                    btnSearch_Click(null,null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
    }
}
