using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.GC.BLL.GC
{
    public partial class PhanBoToKhaiNhap
    {
        public decimal SoToKhaiNhapVNACCS { get; set; }
        public static IList<PhanBoToKhaiNhap> SelectCollectionPhanBoToKhaiXuatOfTKMD(long ID_TKMD, int SoThapPhanNPL)
        {

            string spName = " select *,CASE WHEN MaLoaiHinhNhap LIKE '%V%' THEN SUBSTRING(MaLoaiHinhNhap,3,3) ELSE MaLoaiHinhNhap END AS MaLoaiHinhNhapV, " +
 " CASE WHEN MaLoaiHinhNhap LIKE '%V%' THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhaiNhap) ELSE SoToKhaiNhap END AS SoToKhaiVNACCS " +
                            "from t_GC_PhanBoToKhaiNhap where TKXuat_ID in " +
                            "( select id from t_GC_PhanBoToKhaiXuat where  ID_TKMD=@ID_TKMD AND MaDoanhNghiep LIKE 'X%')";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);


            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();
                
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
               
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (entity.MaLoaiHinhNhap.Contains("V"))
                    if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhapV"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhapV"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiNhapVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("MuaVN"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;

        }
        public static IList<PhanBoToKhaiNhap> SelectCollectionPhanBoToKhaiXuatOfTKCT(long ID_TKMD, int SoThapPhanNPL)
        {

            string spName = " select *,CASE WHEN MaLoaiHinhNhap LIKE '%V%' THEN SUBSTRING(MaLoaiHinhNhap,3,3) ELSE MaLoaiHinhNhap END AS MaLoaiHinhNhapV, " +
 " CASE WHEN MaLoaiHinhNhap LIKE '%V%' THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhaiNhap) ELSE SoToKhaiNhap END AS SoToKhaiVNACCS " +
                            "from t_GC_PhanBoToKhaiNhap where TKXuat_ID in " +
                            "( select id from t_GC_PhanBoToKhaiXuat where  ID_TKMD=@ID_TKMD AND ( MaDoanhNghiep LIKE 'PH%' OR MaDoanhNghiep in ('XGC18', 'XGC19','XVE54')) )";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);


            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal(
"MaLoaiHinhNhap"));
                if (entity.MaLoaiHinhNhap.Contains("V"))
                    if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhapV"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhapV"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiNhapVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));

                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("MuaVN"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;

        }
        public static DataSet SelectDanhSachNPLPhanBoToKhai(long ID_TKMD, long HopDong_ID)
        {

            //string spName = "SELECT DISTINCT a.MaNPL, b.Ten AS TenNPL, b.DVT_ID FROM v_GC_PhanBo_TKMD a " +
                            //"INNER JOIN (SELECT * FROM t_GC_NguyenPhuLieu WHERE HopDong_ID = @HopDong_ID) b ON a.MaNPL = b.Ma " +
                            //"WHERE a.ID_TKMD=@TD_TKMD";

//            string spName = string.Format(@"select a.MaNPL, b.Ten AS TenNPL, b.DVT_ID
//from 
//(SELECT DISTINCT MaNPL FROM 
//v_GC_PhanBo_TKMD where ID_TKMD= {0} ) a INNER JOIN (SELECT * FROM t_GC_NguyenPhuLieu WHERE HopDong_ID = {1}) b ON a.MaNPL = b.Ma", ID_TKMD, HopDong_ID);
//            string spName = string.Format(@"declare @TD_TKMD bigint
//		,@HopDong_ID bigint 
//set @TD_TKMD = {0}
//set @HopDong_ID = {1}			
//select a.MaNPL, b.Ten AS TenNPL, b.DVT_ID
//from 
//(SELECT DISTINCT MaNPL FROM 
//v_GC_PhanBo_TKMD where ID_TKMD=@TD_TKMD ) a INNER JOIN (SELECT * FROM t_GC_NguyenPhuLieu WHERE HopDong_ID = @HopDong_ID) b ON a.MaNPL = b.Ma
//", ID_TKMD, HopDong_ID);

//            //string spName = "p_SelectDanhSachNPLPhanBoToKhai";
//            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
//            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
//            //db.AddInParameter(dbCommand, "TD_TKMD", SqlDbType.BigInt, ID_TKMD);
//            //db.AddInParameter(dbCommand, "HopDong_ID", SqlDbType.BigInt, HopDong_ID);
//            dbCommand.CommandTimeout = 10000;
//            return db.ExecuteDataSet(dbCommand);
            const string spName = "[dbo].[p_ViewDanhSachNPLPhanBo]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.NVarChar, ID_TKMD);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.NVarChar, HopDong_ID);
            dbCommand.CommandTimeout = 600;
            return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachNPLPhanBoTKCT(long ID_TKMD, long HopDong_ID)
        {

            //string spName = "SELECT DISTINCT a.MaNPL, b.Ten AS TenNPL, b.DVT_ID FROM v_GC_PhanBo_TKCT a " +
            //                "INNER JOIN (SELECT * FROM t_GC_NguyenPhuLieu WHERE HopDong_ID = " + HopDong_ID + ") b ON a.MaNPL = b.Ma " +
            //                "WHERE a.ID_TKMD=" + ID_TKMD;
            #region sql
//            string sql = " SELECT DISTINCT a.MaNPL, b.Ten AS TenNPL, b.DVT_ID FROM (SELECT  pbtkx_1_1.MaSP ,                                                                      "+
//"         pbtkx_1_1.SoLuongXuat ,                                                                                                                        "+
//"         pbtkx_1_1.MaDoanhNghiep ,                                                                                                                      "+
//"         pbtkn_2.SoToKhaiNhap ,                                                                                                                         "+
//"         pbtkn_2.MaLoaiHinhNhap ,                                                                                                                       "+
//"         pbtkn_2.MaHaiQuanNhap ,                                                                                                                        "+
//"         pbtkn_2.NamDangKyNhap ,                                                                                                                        "+
//"         pbtkn_2.DinhMucChung ,                                                                                                                         "+
//"         pbtkn_2.LuongTonDau ,                                                                                                                          "+
//"         pbtkn_2.LuongPhanBo ,                                                                                                                          "+
//"         pbtkn_2.LuongCungUng ,                                                                                                                         "+
//"         pbtkn_2.LuongTonCuoi ,                                                                                                                         "+
//"         pbtkn_2.TKXuat_ID ,                                                                                                                            "+
//"         pbtkx_1_1.ID_TKMD ,                                                                                                                            "+
//"         pbtkn_2.MaNPL ,                                                                                                                                "+
//"         hangNhap.NgayDangKy AS NgayDangKyNhap ,                                                                                                        "+
//"         hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,                                                                                                        "+
//"         YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,                                                                                                   "+
//"         hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat ,                                                                                                  "+
//"         hangXuat.SoToKhai AS SoToKhaiXuat ,                                                                                                            "+
//"         hangNhap.TenHang AS TenNPL ,                                                                                                                   "+
//"         hangNhap.DVT_ID AS DVT_NPL ,                                                                                                                   "+
//"         hangNhap.DonGiaKB AS DonGiaTT ,                                                                                                                "+
//"         hangNhap.ThueSuatXNK AS ThueSuat ,                                                                                                             "+
//"         hangXuat.NgayDangKy AS NgayDangKyXuat ,                                                                                                        "+
//"         hangXuat.TenHang AS TenSP ,                                                                                                                    "+
//"         hangXuat.ID_DVT AS DVT_S ,                                                                                                                     "+
//"         hangNhap.TyGiaTinhThue AS TyGiaTT ,                                                                                                            "+
//"         hangXuat.NgayDangKy AS NgayThucXuat ,                                                                                                          "+
//"         hangXuat.IDHopDong ,                                                                                                                           "+
//"         hangNhap.TriGiaKB AS TriGia                                                                                                                    "+
//" FROM    ( SELECT    pb.ID ,                                                                                                                            "+
//"                     pb.ID_TKMD ,                                                                                                                       "+
//"                     pb.MaSP ,                                                                                                                          "+
//"                     pb.SoLuongXuat ,                                                                                                                   "+
//"                     pb.MaDoanhNghiep                                                                                                                   "+
//"           FROM      dbo.t_GC_PhanBoToKhaiXuat AS pb                                                                                                    "+
//"                     JOIN t_KDT_GC_ToKhaiChuyenTiep AS tkmd ON pb.ID_TKMD = tkmd.ID                                                                     "+
//"                                                               AND tkmd.IDHopDong = " + HopDong_ID + "                                                  "+
//"                                                               AND pb.ID_TKMD = " + ID_TKMD + "                                                         "+
//"           WHERE     ( pb.MaDoanhNghiep LIKE 'PH%'                                                                                                      "+
//"                       OR pb.MaDoanhNghiep LIKE '%18'                                                                                                   "+
//"                       OR pb.MaDoanhNghiep LIKE '%19'                                                                                                   "+
//"                       OR pb.MaDoanhNghiep LIKE '%20'                                                                                                   "+
//"                       OR pb.MaDoanhNghiep LIKE 'XVE54'                                                                                                 "+
//"                     )                                                                                                                                  "+
//"         ) AS pbtkx_1_1                                                                                                                                 "+
//"         INNER JOIN ( SELECT pb.ID ,                                                                                                                    "+
//"                             pb.SoToKhaiNhap ,                                                                                                          "+
//"                             pb.MaLoaiHinhNhap ,                                                                                                        "+
//"                             pb.MaHaiQuanNhap ,                                                                                                         "+
//"                             pb.NamDangKyNhap ,                                                                                                         "+
//"                             pb.MaNPL ,                                                                                                                 "+
//"                             pb.DinhMucChung ,                                                                                                          "+
//"                             pb.LuongTonDau ,                                                                                                           "+
//"                             pb.LuongPhanBo ,                                                                                                           "+
//"                             pb.LuongCungUng ,                                                                                                          "+
//"                             pb.LuongTonCuoi ,                                                                                                          "+
//"                             pb.TKXuat_ID ,                                                                                                             "+
//"                             pb.MaDoanhNghiep                                                                                                           "+
//"                      FROM   dbo.t_GC_PhanBoToKhaiNhap AS pb                                                                                            "+
//"                             JOIN t_KDT_ToKhaiMauDich AS tkmd ON pb.SoToKhaiNhap = tkmd.SoToKhai                                                        "+
//"                                                               AND pb.NamDangKyNhap = YEAR(tkmd.NgayDangKy)                                             "+
//"                                                               AND tkmd.IDHopDong = " + HopDong_ID + "                                                  "+
//"                                                               AND pb.MaLoaiHinhNhap = tkmd.MaLoaiHinh                                                  "+
//"                                                               AND ( MaLoaiHinhNhap LIKE 'N%'                                                           "+
//"                                                               AND MaLoaiHinhNhap <> 'NVE23'                                                            "+
//"                                                               AND MaLoaiHinhNhap <> 'NGC18'                                                            "+
//"                                                               AND MaLoaiHinhNhap <> 'NGC19'                                                            "+
//"                                                               AND MaLoaiHinhNhap <> 'NGC20'                                                            "+
//"                                                               )                                                                                        "+
//"                    ) AS pbtkn_2 ON pbtkx_1_1.ID = pbtkn_2.TKXuat_ID                                                                                    "+
//"         INNER JOIN ( SELECT T.ID ,                                                                                                                     "+
//"                             T.SoToKhai ,                                                                                                               "+
//"                             T.NgayDangKy ,                                                                                                             "+
//"                             T.NamDK ,                                                                                                                  "+
//"                             T.MaLoaiHinh ,                                                                                                             "+
//"                             T.TyGiaTinhThue ,                                                                                                          "+
//"                             H.TKMD_ID ,                                                                                                                "+
//"                             H.SoThuTuHang ,                                                                                                            "+
//"                             H.MaHS ,                                                                                                                   "+
//"                             H.MaPhu ,                                                                                                                  "+
//"                             H.NuocXX_ID ,                                                                                                              "+
//"                             H.TenHang ,                                                                                                                "+
//"                             H.DVT_ID ,                                                                                                                 "+
//"                             H.SoLuong ,                                                                                                                "+
//"                             H.TrongLuong AS Expr1 ,                                                                                                    "+
//"                             H.DonGiaKB ,                                                                                                               "+
//"                             H.DonGiaTT ,                                                                                                               "+
//"                             H.TriGiaKB ,                                                                                                               "+
//"                             H.TriGiaTT ,                                                                                                               "+
//"                             H.TriGiaKB_VND ,                                                                                                           "+
//"                             H.ThueSuatXNK ,                                                                                                            "+
//"                             H.ThueSuatTTDB ,                                                                                                           "+
//"                             H.ThueSuatGTGT ,                                                                                                           "+
//"                             H.ThueXNK ,                                                                                                                "+
//"                             H.ThueTTDB ,                                                                                                               "+
//"                             H.ThueGTGT ,                                                                                                               "+
//"                             H.PhuThu ,                                                                                                                 "+
//"                             H.TyLeThuKhac ,                                                                                                            "+
//"                             H.TriGiaThuKhac ,                                                                                                          "+
//"                             H.MienThue ,                                                                                                               "+
//"                             H.Ma_HTS ,                                                                                                                 "+
//"                             H.DVT_HTS ,                                                                                                                "+
//"                             H.SoLuong_HTS                                                                                                              "+
//"                      FROM   dbo.t_KDT_ToKhaiMauDich AS T                                                                                               "+
//"                             INNER JOIN dbo.t_KDT_HangMauDich AS H ON T.ID = H.TKMD_ID                                                                  "+
//"                      WHERE  YEAR(T.NgayDangKy) >= 2014                                                                                                 "+
//"                             AND T.IDHopDong = " + HopDong_ID + "                                                                                       "+
//"                    ) AS hangNhap ON pbtkn_2.SoToKhaiNhap = hangNhap.SoToKhai                                                                           "+
//"                                     AND pbtkn_2.MaLoaiHinhNhap = hangNhap.MaLoaiHinh                                                                   "+
//"                                     AND pbtkn_2.NamDangKyNhap = YEAR(hangNhap.NgayDangKy)                                                              "+
//"                                     AND pbtkn_2.MaNPL = hangNhap.MaPhu                                                                                 "+
//"         INNER JOIN ( SELECT a.ID ,                                                                                                                     "+
//"                             a.IDHopDong ,                                                                                                              "+
//"                             a.SoTiepNhan ,                                                                                                             "+
//"                             a.NgayTiepNhan ,                                                                                                           "+
//"                             a.TrangThaiXuLy ,                                                                                                          "+
//"                             a.MaHaiQuanTiepNhan ,                                                                                                      "+
//"                             a.SoToKhai ,                                                                                                               "+
//"                             a.CanBoDangKy ,                                                                                                            "+
//"                             a.NgayDangKy ,                                                                                                             "+
//"                             a.MaDoanhNghiep ,                                                                                                          "+
//"                             a.SoHopDongDV ,                                                                                                            "+
//"                             a.NgayHDDV ,                                                                                                               "+
//"                             a.NgayHetHanHDDV ,                                                                                                         "+
//"                             a.NguoiChiDinhDV ,                                                                                                         "+
//"                             a.MaKhachHang ,                                                                                                            "+
//"                             a.TenKH ,                                                                                                                  "+
//"                             a.SoHDKH ,                                                                                                                 "+
//"                             a.NgayHDKH ,                                                                                                               "+
//"                             a.NgayHetHanHDKH ,                                                                                                         "+
//"                             a.NguoiChiDinhKH ,                                                                                                         "+
//"                             a.MaDaiLy ,                                                                                                                "+
//"                             a.MaLoaiHinh ,                                                                                                             "+
//"                             a.DiaDiemXepHang ,                                                                                                         "+
//"                             a.TyGiaVND ,                                                                                                               "+
//"                             a.TyGiaUSD ,                                                                                                               "+
//"                             a.LePhiHQ ,                                                                                                                "+
//"                             a.SoBienLai ,                                                                                                              "+
//"                             a.NgayBienLai ,                                                                                                            "+
//"                             a.ChungTu ,                                                                                                                "+
//"                             a.NguyenTe_ID ,                                                                                                            "+
//"                             a.MaHaiQuanKH ,                                                                                                            "+
//"                             a.ID_Relation ,                                                                                                            "+
//"                             b.Master_ID ,                                                                                                              "+
//"                             b.SoThuTuHang ,                                                                                                            "+
//"                             b.MaHang ,                                                                                                                 "+
//"                             b.MaHS ,                                                                                                                   "+
//"                             b.SoLuong ,                                                                                                                "+
//"                             b.TenHang ,                                                                                                                "+
//"                             b.ID_NuocXX ,                                                                                                              "+
//"                             b.ID_DVT ,                                                                                                                 "+
//"                             b.DonGia ,                                                                                                                 "+
//"                             b.TriGia                                                                                                                   "+
//"                      FROM   dbo.t_KDT_GC_ToKhaiChuyenTiep AS a                                                                                         "+
//"                             INNER JOIN dbo.t_KDT_GC_HangChuyenTiep AS b ON a.ID = b.Master_ID                                                          "+
//"                      WHERE  a.IDHopDong = " + HopDong_ID + "                                                                                           "+
//"                    ) AS hangXuat ON pbtkx_1_1.ID_TKMD = hangXuat.ID                                                                                    "+
//"                                     AND pbtkx_1_1.MaSP = hangXuat.MaHang                                                                               "+
//"                                     AND hangXuat.MaLoaiHinh LIKE '%X%'                                                                                 "+
//" UNION                                                                                                                                                  "+
//" SELECT  pbtkx_2.MaSP ,                                                                                                                                 "+
//"         pbtkx_2.SoLuongXuat ,                                                                                                                          "+
//"         pbtkx_2.MaDoanhNghiep ,                                                                                                                        "+
//"         pbtkn_1_1.SoToKhaiNhap ,                                                                                                                       "+
//"         pbtkn_1_1.MaLoaiHinhNhap ,                                                                                                                     "+
//"         pbtkn_1_1.MaHaiQuanNhap ,                                                                                                                      "+
//"         pbtkn_1_1.NamDangKyNhap ,                                                                                                                      "+
//"         pbtkn_1_1.DinhMucChung ,                                                                                                                       "+
//"         pbtkn_1_1.LuongTonDau ,                                                                                                                        "+
//"         pbtkn_1_1.LuongPhanBo ,                                                                                                                        "+
//"         pbtkn_1_1.LuongCungUng ,                                                                                                                       "+
//"         pbtkn_1_1.LuongTonCuoi ,                                                                                                                       "+
//"         pbtkn_1_1.TKXuat_ID ,                                                                                                                          "+
//"         pbtkx_2.ID_TKMD ,                                                                                                                              "+
//"         pbtkn_1_1.MaNPL ,                                                                                                                              "+
//"         hangNhap.NgayDangKy AS NgayDangKyNhap ,                                                                                                        "+
//"         hangXuat.MaLoaiHinh AS MaLoaiHinhXuat ,                                                                                                        "+
//"         YEAR(hangXuat.NgayDangKy) AS NamDangKyXuat ,                                                                                                   "+
//"         hangXuat.MaHaiQuanTiepNhan AS MaHaiQuanXuat ,                                                                                                  "+
//"         hangXuat.SoToKhai AS SoToKhaiXuat ,                                                                                                            "+
//"         hangNhap.TenHang AS TenNPL ,                                                                                                                   "+
//"         hangNhap.ID_DVT AS DVT_NPL ,                                                                                                                   "+
//"         hangNhap.DonGia AS DonGiaTT ,                                                                                                                  "+
//"         0 AS ThueSuat ,                                                                                                                                "+
//"         hangXuat.NgayDangKy AS NgayDangKyXuat ,                                                                                                        "+
//"         hangXuat.TenHang AS TenSP ,                                                                                                                    "+
//"         hangXuat.ID_DVT AS DVT_SP ,                                                                                                                    "+
//"         hangNhap.TyGiaVND AS TyGiaTT ,                                                                                                                 "+
//"         hangXuat.NgayDangKy AS NgayThucXuat ,                                                                                                          "+
//"         hangXuat.IDHopDong ,                                                                                                                           "+
//"         hangNhap.TriGia AS TriGia                                                                                                                      "+
//" FROM    ( SELECT    pb.ID ,                                                                                                                            "+
//"                     pb.ID_TKMD ,                                                                                                                       "+
//"                     pb.MaSP ,                                                                                                                          "+
//"                     pb.SoLuongXuat ,                                                                                                                   "+
//"                     pb.MaDoanhNghiep                                                                                                                   "+
//"           FROM      dbo.t_GC_PhanBoToKhaiXuat AS pb                                                                                                    "+
//"                     JOIN t_KDT_GC_ToKhaiChuyenTiep AS tkmd ON pb.ID_TKMD = tkmd.ID                                                                     "+
//"                                                               AND tkmd.IDHopDong = " + HopDong_ID + "                                                  "+
//"                                                               AND pb.ID_TKMD = " + ID_TKMD + "                                                         "+
//"           WHERE     ( pb.MaDoanhNghiep LIKE 'PH%'                                                                                                      "+
//"                       OR pb.MaDoanhNghiep LIKE '%18'                                                                                                   "+
//"                       OR pb.MaDoanhNghiep LIKE '%19'                                                                                                   "+
//"                       OR pb.MaDoanhNghiep LIKE '%20'                                                                                                   "+
//"                       OR pb.MaDoanhNghiep LIKE 'XVE54'                                                                                                 "+
//"                     )                                                                                                                                  "+
//"         ) AS pbtkx_2                                                                                                                                   "+
//"         INNER JOIN ( SELECT pb.ID ,                                                                                                                    "+
//"                             pb.SoToKhaiNhap ,                                                                                                          "+
//"                             pb.MaLoaiHinhNhap ,                                                                                                        "+
//"                             pb.MaHaiQuanNhap ,                                                                                                         "+
//"                             pb.NamDangKyNhap ,                                                                                                         "+
//"                             pb.MaNPL ,                                                                                                                 "+
//"                             pb.DinhMucChung ,                                                                                                          "+
//"                             pb.LuongTonDau ,                                                                                                           "+
//"                             pb.LuongPhanBo ,                                                                                                           "+
//"                             pb.LuongCungUng ,                                                                                                          "+
//"                             pb.LuongTonCuoi ,                                                                                                          "+
//"                             pb.TKXuat_ID ,                                                                                                             "+
//"                             pb.MaDoanhNghiep                                                                                                           "+
//"                      FROM   dbo.t_GC_PhanBoToKhaiNhap AS pb                                                                                            "+
//"                             JOIN t_KDT_GC_ToKhaiChuyenTiep AS tkmd ON pb.SoToKhaiNhap = tkmd.SoToKhai                                                  "+
//"                                                               AND pb.NamDangKyNhap = YEAR(tkmd.NgayDangKy)                                             "+
//"                                                               AND tkmd.IDHopDong = " + HopDong_ID + "                                                  "+
//"                                                               AND pb.MaLoaiHinhNhap = tkmd.MaLoaiHinh                                                  "+
//"                      WHERE  ( pb.MaLoaiHinhNhap LIKE 'PH%'                                                                                             "+
//"                               OR pb.MaLoaiHinhNhap LIKE '%18'                                                                                          "+
//"                               OR pb.MaLoaiHinhNhap LIKE '%19'                                                                                          "+
//"                               OR pb.MaLoaiHinhNhap LIKE '%20'                                                                                          "+
//"                               OR pb.MaLoaiHinhNhap LIKE 'NVE23'                                                                                        "+
//"                             )                                                                                                                          "+
//"                    ) AS pbtkn_1_1 ON pbtkx_2.ID = pbtkn_1_1.TKXuat_ID                                                                                  "+
//"         INNER JOIN ( SELECT T.ID ,                                                                                                                     "+
//"                             T.SoToKhai ,                                                                                                               "+
//"                             T.NgayDangKy ,                                                                                                             "+
//"                             T.NamDK ,                                                                                                                  "+
//"                             T.MaLoaiHinh ,                                                                                                             "+
//"                             T.TyGiaTinhThue AS TyGiaVND ,                                                                                              "+
//"                             H.TKMD_ID ,                                                                                                                "+
//"                             H.SoThuTuHang ,                                                                                                            "+
//"                             H.MaHS ,                                                                                                                   "+
//"                             H.MaPhu ,                                                                                                                  "+
//"                             H.NuocXX_ID ,                                                                                                              "+
//"                             H.TenHang ,                                                                                                                "+
//"                             H.DVT_ID AS ID_DVT ,                                                                                                       "+
//"                             H.SoLuong ,                                                                                                                "+
//"                             H.TrongLuong AS Expr1 ,                                                                                                    "+
//"                             H.DonGiaKB ,                                                                                                               "+
//"                             H.DonGiaTT AS DonGia ,                                                                                                     "+
//"                             H.TriGiaKB ,                                                                                                               "+
//"                             H.TriGiaTT AS TriGia ,                                                                                                     "+
//"                             H.TriGiaKB_VND ,                                                                                                           "+
//"                             H.ThueSuatXNK ,                                                                                                            "+
//"                             H.ThueSuatTTDB ,                                                                                                           "+
//"                             H.ThueSuatGTGT ,                                                                                                           "+
//"                             H.ThueXNK ,                                                                                                                "+
//"                             H.ThueTTDB ,                                                                                                               "+
//"                             H.ThueGTGT ,                                                                                                               "+
//"                             H.PhuThu ,                                                                                                                 "+
//"                             H.TyLeThuKhac ,                                                                                                            "+
//"                             H.TriGiaThuKhac ,                                                                                                          "+
//"                             H.MienThue ,                                                                                                               "+
//"                             H.Ma_HTS ,                                                                                                                 "+
//"                             H.DVT_HTS ,                                                                                                                "+
//"                             H.SoLuong_HTS                                                                                                              "+
//"                      FROM   dbo.t_KDT_ToKhaiMauDich AS T                                                                                               "+
//"                             INNER JOIN dbo.t_KDT_HangMauDich AS H ON T.ID = H.TKMD_ID                                                                  "+
//"                      WHERE  YEAR(T.NgayDangKy) >= 2014                                                                                                 "+
//"                             AND T.IDHopDong = " + HopDong_ID + "                                                                                       "+
//"                    ) AS hangNhap ON pbtkn_1_1.SoToKhaiNhap = hangNhap.SoToKhai                                                                         "+
//"                                     AND pbtkn_1_1.MaLoaiHinhNhap = hangNhap.MaLoaiHinh                                                                 "+
//"                                     AND pbtkn_1_1.NamDangKyNhap = YEAR(hangNhap.NgayDangKy)                                                            "+
//"                                     AND pbtkn_1_1.MaNPL = hangNhap.MaPhu                                                                               "+
//"         INNER JOIN ( SELECT a.ID ,                                                                                                                     "+
//"                             a.IDHopDong ,                                                                                                              "+
//"                             a.SoTiepNhan ,                                                                                                             "+
//"                             a.NgayTiepNhan ,                                                                                                           "+
//"                             a.TrangThaiXuLy ,                                                                                                          "+
//"                             a.MaHaiQuanTiepNhan ,                                                                                                      "+
//"                             a.SoToKhai ,                                                                                                               "+
//"                             a.CanBoDangKy ,                                                                                                            "+
//"                             a.NgayDangKy ,                                                                                                             "+
//"                             a.MaDoanhNghiep ,                                                                                                          "+
//"                             a.SoHopDongDV ,                                                                                                            "+
//"                             a.NgayHDDV ,                                                                                                               "+
//"                             a.NgayHetHanHDDV ,                                                                                                         "+
//"                             a.NguoiChiDinhDV ,                                                                                                         "+
//"                             a.MaKhachHang ,                                                                                                            "+
//"                             a.TenKH ,                                                                                                                  "+
//"                             a.SoHDKH ,                                                                                                                 "+
//"                             a.NgayHDKH ,                                                                                                               "+
//"                             a.NgayHetHanHDKH ,                                                                                                         "+
//"                             a.NguoiChiDinhKH ,                                                                                                         "+
//"                             a.MaDaiLy ,                                                                                                                "+
//"                             a.MaLoaiHinh ,                                                                                                             "+
//"                             a.DiaDiemXepHang ,                                                                                                         "+
//"                             a.TyGiaVND ,                                                                                                               "+
//"                             a.TyGiaUSD ,                                                                                                               "+
//"                             a.LePhiHQ ,                                                                                                                "+
//"                             a.SoBienLai ,                                                                                                              "+
//"                             a.NgayBienLai ,                                                                                                            "+
//"                             a.ChungTu ,                                                                                                                "+
//"                             a.NguyenTe_ID ,                                                                                                            "+
//"                             a.MaHaiQuanKH ,                                                                                                            "+
//"                             a.ID_Relation ,                                                                                                            "+
//"                             b.Master_ID ,                                                                                                              "+
//"                             b.SoThuTuHang ,                                                                                                            "+
//"                             b.MaHang ,                                                                                                                 "+
//"                             b.MaHS ,                                                                                                                   "+
//"                             b.SoLuong ,                                                                                                                "+
//"                             b.TenHang ,                                                                                                                "+
//"                             b.ID_NuocXX ,                                                                                                              "+
//"                             b.ID_DVT ,                                                                                                                 "+
//"                             b.DonGia ,                                                                                                                 "+
//"                             b.TriGia                                                                                                                   "+
//"                      FROM   dbo.t_KDT_GC_ToKhaiChuyenTiep AS a                                                                                         "+
//"                             INNER JOIN dbo.t_KDT_GC_HangChuyenTiep AS b ON a.ID = b.Master_ID                                                          "+
//"                      WHERE  a.IDHopDong = " + HopDong_ID + "                                                                                           "+
//"                    ) AS hangXuat ON pbtkx_2.ID_TKMD = hangXuat.ID                                                                                      "+
//"                                     AND pbtkx_2.MaSP = hangXuat.MaHang                                                                                 "+
//"                                     AND hangXuat.MaLoaiHinh LIKE '%X%'                                                                                 "+
//"         ) a INNER JOIN (SELECT * FROM t_GC_NguyenPhuLieu WHERE HopDong_ID = " + HopDong_ID + ") b ON a.MaNPL = b.Ma --WHERE a.ID_TKMD=" + ID_TKMD;
            #endregion sql

          //sql = "";
          //sql = "  SELECT DISTINCT " +
          //" MaNPL , " +
          //"CASE WHEN MaNPL IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_GC_NguyenPhuLieu WHERE Ma = MaNPL) ELSE MaNPL END AS TenNPL, " +
          //"CASE WHEN MaNPL IS NOT NULL THEN (SELECT TOP 1 Ten FROM dbo.t_HaiQuan_DonViTinh WHERE ID= (SELECT TOP 1 DVT_ID FROM dbo.t_GC_NguyenPhuLieu WHERE Ma = MaNPL)) ELSE MaNPL END AS DVT " +
          //"FROM dbo.t_GC_PhanBoToKhaiNhap WHERE TKXuat_ID IN (SELECT ID FROM dbo.t_GC_PhanBoToKhaiXuat WHERE ID_TKMD = "+ID_TKMD+")";

          const string spName = "[dbo].[p_ViewDanhSachNPLPhanBo]";
          SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
          SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

          db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.NVarChar, ID_TKMD);
          db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.NVarChar, HopDong_ID);
          dbCommand.CommandTimeout = 600;
          return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachNPLPhanBoCungUngTK(long ID)
        {

            string spName = "SELECT ID_TKMD,MaNPL, Sum(LuongPhanBo) as LuongPhanBo , SUM(b.TriGia) AS TriGia FROM (SELECT  SUM(a.TriGia) AS TriGia, a.ID_TKMD, a.MaNPL, ISNULL(CASE SUBSTRING(a.MaLoaiHInhNhap,1,3) WHEN 'NSX' THEN sum(a.LuongPhanBo) END,0) + sum(a.LuongCungUng) as LuongPhanBo FROM v_GC_PhanBo_TKMD a " +
                            "WHERE a.IDHopDong = " + ID + "  AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)GROUP BY a.MaNPL, a.ID_TKMD, a.MaLoaiHinhNhap) b GROUP BY ID_TKMD, MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 10000;
            return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachNPLPhanBoCungUngTKCT(long ID)
        {

            string spName = "SELECT ID_TKMD,MaNPL, Sum(LuongPhanBo) as LuongPhanBo , SUM(b.TriGia) AS TriGia FROM (SELECT SUM(a.TriGia) AS TriGia, a.ID_TKMD, a.MaNPL, ISNULL(CASE SUBSTRING(a.MaLoaiHInhNhap,1,3) WHEN 'NSX' THEN sum(a.LuongPhanBo) END,0) + sum(a.LuongCungUng) as LuongPhanBo FROM v_GC_PhanBo_TKCT a " +
                            "WHERE a.IDHopDong = " + ID + " AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)GROUP BY a.MaNPL, a.ID_TKMD, a.MaLoaiHinhNhap )b GROUP BY ID_TKMD, MaNPL ";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 10000;
            DataSet ds=new DataSet();
            db.LoadDataSet(dbCommand, ds, "dd");
            return ds;

        }
        public static DataSet SelectDanhSachNPLPhanBoCuaSanPham(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT a.* FROM v_GC_PhanBo_TKMD a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD+ " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)ORDER BY a.MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            dbCommand.CommandTimeout = 10000;
            return db.ExecuteDataSet(dbCommand);

        }
        public static bool CheckSanPhamCungUng(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT count(*) FROM v_GC_PhanBo_TKMD a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD + " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return Convert.ToInt32(db.ExecuteScalar(dbCommand)) > 0;

        }
        public static bool CheckSanPhamCungUngTKCT(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT count(*) FROM v_GC_PhanBo_TKCT a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD + " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return Convert.ToInt32(db.ExecuteScalar(dbCommand)) > 0;

        }
        public static DataSet SelectDanhSachNPLPhanBoCuaSanPhamTKCT(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT a.* FROM v_GC_PhanBo_TKCT a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD + " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)ORDER BY a.MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachNPLPhanBoCuaSanPhamGroupByMaNPL(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT a.MaNPL,a.SoToKhaiNhap,a.NgayDangKyNhap,a.MaLoaiHinhNhap, ISNULL(CASE SUBSTRING(a.MaLoaiHInhNhap,1,3) WHEN 'NSX' THEN sum(a.LuongPhanBo) END,0) + sum(a.LuongCungUng) as LuongPhanBo FROM v_GC_PhanBo_TKMD a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD + " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)GROUP BY a.MaNPL,a.SoToKhaiNhap,a.NgayDangKyNhap,a.MaLoaiHinhNhap ORDER BY a.MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);

        }
        public static DataSet SelectDanhSachNPLPhanBoCuaSanPhamGroupByMaNPLTKCT(long ID_TKMD, long HopDong_ID, string maSP)
        {

            string spName = "SELECT a.MaNPL,a.SoToKhaiNhap,a.NgayDangKyNhap,a.MaLoaiHinhNhap, ISNULL(CASE SUBSTRING(a.MaLoaiHInhNhap,1,3) WHEN 'NSX' THEN sum(a.LuongPhanBo) END,0) + sum(a.LuongCungUng) as LuongPhanBo FROM v_GC_PhanBo_TKCT a " +
                            "WHERE a.ID_TKMD=" + ID_TKMD + " AND a.MaSP = '" + maSP + "' AND (a.MaLoaiHinhNhap LIKE 'NSX%' OR a.LuongCungUng > 0)GROUP BY a.MaNPL,a.SoToKhaiNhap,a.NgayDangKyNhap,a.MaLoaiHinhNhap ORDER BY a.MaNPL";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            return db.ExecuteDataSet(dbCommand);

        }
        public static IList<PhanBoToKhaiNhap> SelectCollectionPhanBoToKhaiXuatOfTKMDAndMaSP(long ID_TKMD, string MaSP, int SoThapPhanNPL)
        {
            string spName = " select *,CASE WHEN MaLoaiHinhNhap LIKE '%V%' THEN SUBSTRING(MaLoaiHinhNhap,3,3) ELSE MaLoaiHinhNhap END AS MaLoaiHinhNhapV, " +
 " CASE WHEN MaLoaiHinhNhap LIKE '%V%' THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhaiNhap) ELSE SoToKhaiNhap END AS SoToKhaiVNACCS " +
                            "from t_GC_PhanBoToKhaiNhap where TKXuat_ID in " +
                           "( select id from t_GC_PhanBoToKhaiXuat where  ID_TKMD=@ID_TKMD AND MaDoanhNghiep LIKE 'X%' and MaSP=@MaSP)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);

            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiNhapVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (entity.MaLoaiHinhNhap.Contains("V"))
                    if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhapV"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhapV"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("MuaVN"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }
        public static IList<PhanBoToKhaiNhap> SelectCollectionPhanBoToKhaiXuatOfTKMDAndMaSPTKCT(long ID_TKMD, string MaSP, int SoThapPhanNPL)
        {
            string spName = " select *,CASE WHEN MaLoaiHinhNhap LIKE '%V%' THEN SUBSTRING(MaLoaiHinhNhap,3,3) ELSE MaLoaiHinhNhap END AS MaLoaiHinhNhapV, " +
 " CASE WHEN MaLoaiHinhNhap LIKE '%V%' THEN (SELECT TOP 1 SoTKVNACCS FROM t_VNACCS_CapSoToKhai WHERE SoTK = SoToKhaiNhap) ELSE SoToKhaiNhap END AS SoToKhaiVNACCS " +
                            "from t_GC_PhanBoToKhaiNhap where TKXuat_ID in " +
                           "( select id from t_GC_PhanBoToKhaiXuat where  ID_TKMD=@ID_TKMD AND ( a.MaDoanhNghiep LIKE 'PH%' OR a.MaDoanhNghiep IN ('XGC18', 'XGC19','XVE54') ) and MaSP=@MaSP)";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);
            db.AddInParameter(dbCommand, "@SoThapPhanNPL", SqlDbType.Int, SoThapPhanNPL);
            db.AddInParameter(dbCommand, "@ID_TKMD", SqlDbType.BigInt, ID_TKMD);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, MaSP);

            IList<PhanBoToKhaiNhap> collection = new List<PhanBoToKhaiNhap>();

            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                PhanBoToKhaiNhap entity = new PhanBoToKhaiNhap();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (entity.MaLoaiHinhNhap.Contains("V"))
                    if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhapV"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhapV"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiVNACCS"))) entity.SoToKhaiNhapVNACCS = reader.GetDecimal(reader.GetOrdinal("SoToKhaiVNACCS"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuanNhap"))) entity.MaHaiQuanNhap = reader.GetString(reader.GetOrdinal("MaHaiQuanNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NamDangKyNhap"))) entity.NamDangKyNhap = reader.GetInt16(reader.GetOrdinal("NamDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucChung"))) entity.DinhMucChung = reader.GetDecimal(reader.GetOrdinal("DinhMucChung"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDouble(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongPhanBo"))) entity.LuongPhanBo = reader.GetDouble(reader.GetOrdinal("LuongPhanBo"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongCungUng"))) entity.LuongCungUng = reader.GetDouble(reader.GetOrdinal("LuongCungUng"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDouble(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("TKXuat_ID"))) entity.TKXuat_ID = reader.GetInt64(reader.GetOrdinal("TKXuat_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MuaVN"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("MuaVN"));
                collection.Add(entity);
            }

            reader.Close();
            return collection;
        }
        public static bool CheckPhanBoToKhaiNhap(int SoToKhai,string MaLoaiHinh,string MaHaiQuan,short NamDangKy, long idHopDong)        
        {
            string sql = "select IDHopDong  from t_GC_PhanBoToKhaiNhap pbnhap inner join t_GC_PhanBoToKhaiXuat pbx on pbx.id=pbnhap.TKXuat_ID inner join t_KDT_ToKhaiMauDich tkmd " +
                        " on tkmd.id=pbx.ID_TKMD  " +
                        " where SoToKhaiNhap=@SoToKhaiNhap and MaLoaiHinhNhap=@MaLoaiHinhNhap and MaHaiQuanNhap=@MaHaiQuanNhap and NamDangKyNhap=@NamDangKyNhap " +
                        " and pbnhap.ID = (select max(id) from  t_GC_PhanBoToKhaiNhap where SoToKhaiNhap=@SoToKhaiNhap and MaLoaiHinhNhap=@MaLoaiHinhNhap and MaHaiQuanNhap=@MaHaiQuanNhap and NamDangKyNhap=@NamDangKyNhap)";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuanNhap", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKyNhap", SqlDbType.SmallInt, NamDangKy);
            object o = db.ExecuteScalar(dbCommand);
            if (o == null) return false;

            if (Convert.ToInt64(o) == idHopDong)
                return true;
            sql = "select IDHopDong  from t_GC_PhanBoToKhaiNhap pbnhap inner join t_GC_PhanBoToKhaiXuat pbx on pbx.id=pbnhap.TKXuat_ID inner join t_KDT_GC_ToKhaiChuyenTiep tkct " +
                        " on tkct.id=pbx.ID_TKMD  " +
                        " where SoToKhaiNhap=@SoToKhaiNhap and MaLoaiHinhNhap=@MaLoaiHinhNhap and MaHaiQuanNhap=@MaHaiQuanNhap and NamDangKyNhap=@NamDangKyNhap "+
                        " and pbnhap.ID = (select max(id) from  t_GC_PhanBoToKhaiNhap where SoToKhaiNhap=@SoToKhaiNhap and MaLoaiHinhNhap=@MaLoaiHinhNhap and MaHaiQuanNhap=@MaHaiQuanNhap and NamDangKyNhap=@NamDangKyNhap)";
            dbCommand = (SqlCommand)db.GetSqlStringCommand(sql);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@MaHaiQuanNhap", SqlDbType.Char, MaHaiQuan);
            db.AddInParameter(dbCommand, "@NamDangKyNhap", SqlDbType.SmallInt, NamDangKy);
            o = db.ExecuteScalar(dbCommand);

            if (o == null) return false;
            if (Convert.ToInt64(o) == idHopDong)
                return true;
            else
                return false;
        }
        public static DataTable ToKhaiChuaPhanBo(int NamDangKy)
        {
            string spName = "p_ViewToKhaiChuaPhanBo";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@NamDangKy", SqlDbType.Int, NamDangKy);

            return db.ExecuteDataSet(dbCommand).Tables[0];
        }
	}	
}