﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.KDT.GC
{
	public partial class T_KDT_VNACCS_WarehouseExport : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public int TrangThaiXuLy { set; get; }
		public long SoTN { set; get; }
		public DateTime NgayTN { set; get; }
		public DateTime NgayBatDauBC { set; get; }
		public DateTime NgayKetthucBC { set; get; }
		public string MaHQ { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string TenDoanhNghiep { set; get; }
		public string TenKho { set; get; }
		public string MaKho { set; get; }
		public string SoTKChungTu { set; get; }
		public decimal Loai { set; get; }
		public string SoHopDong { set; get; }
		public DateTime NgayHopDong { set; get; }
		public string MaHQTiepNhanHD { set; get; }
		public DateTime NgayHetHanHD { set; get; }
		public string GhiChuKhac { set; get; }
		public string GuidStr { set; get; }
        public List<T_KDT_VNACCS_WarehouseExport_Detail> WarehouseExportCollection = new List<T_KDT_VNACCS_WarehouseExport_Detail>();
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<T_KDT_VNACCS_WarehouseExport> ConvertToCollection(IDataReader reader)
		{
			List<T_KDT_VNACCS_WarehouseExport> collection = new List<T_KDT_VNACCS_WarehouseExport>();
			while (reader.Read())
			{
				T_KDT_VNACCS_WarehouseExport entity = new T_KDT_VNACCS_WarehouseExport();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTN"))) entity.SoTN = reader.GetInt64(reader.GetOrdinal("SoTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTN"))) entity.NgayTN = reader.GetDateTime(reader.GetOrdinal("NgayTN"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayBatDauBC"))) entity.NgayBatDauBC = reader.GetDateTime(reader.GetOrdinal("NgayBatDauBC"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayKetthucBC"))) entity.NgayKetthucBC = reader.GetDateTime(reader.GetOrdinal("NgayKetthucBC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQ"))) entity.MaHQ = reader.GetString(reader.GetOrdinal("MaHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoanhNghiep"))) entity.TenDoanhNghiep = reader.GetString(reader.GetOrdinal("TenDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenKho"))) entity.TenKho = reader.GetString(reader.GetOrdinal("TenKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaKho"))) entity.MaKho = reader.GetString(reader.GetOrdinal("MaKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTKChungTu"))) entity.SoTKChungTu = reader.GetString(reader.GetOrdinal("SoTKChungTu"));
				if (!reader.IsDBNull(reader.GetOrdinal("Loai"))) entity.Loai = reader.GetDecimal(reader.GetOrdinal("Loai"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHopDong"))) entity.NgayHopDong = reader.GetDateTime(reader.GetOrdinal("NgayHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQTiepNhanHD"))) entity.MaHQTiepNhanHD = reader.GetString(reader.GetOrdinal("MaHQTiepNhanHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHetHanHD"))) entity.NgayHetHanHD = reader.GetDateTime(reader.GetOrdinal("NgayHetHanHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChuKhac"))) entity.GhiChuKhac = reader.GetString(reader.GetOrdinal("GhiChuKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<T_KDT_VNACCS_WarehouseExport> collection, long id)
        {
            foreach (T_KDT_VNACCS_WarehouseExport item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO T_KDT_VNACCS_WarehouseExport VALUES(@TrangThaiXuLy, @SoTN, @NgayTN, @NgayBatDauBC, @NgayKetthucBC, @MaHQ, @MaDoanhNghiep, @TenDoanhNghiep, @TenKho, @MaKho, @SoTKChungTu, @Loai, @SoHopDong, @NgayHopDong, @MaHQTiepNhanHD, @NgayHetHanHD, @GhiChuKhac, @GuidStr)";
            string update = "UPDATE T_KDT_VNACCS_WarehouseExport SET TrangThaiXuLy = @TrangThaiXuLy, SoTN = @SoTN, NgayTN = @NgayTN, NgayBatDauBC = @NgayBatDauBC, NgayKetthucBC = @NgayKetthucBC, MaHQ = @MaHQ, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, TenKho = @TenKho, MaKho = @MaKho, SoTKChungTu = @SoTKChungTu, Loai = @Loai, SoHopDong = @SoHopDong, NgayHopDong = @NgayHopDong, MaHQTiepNhanHD = @MaHQTiepNhanHD, NgayHetHanHD = @NgayHetHanHD, GhiChuKhac = @GhiChuKhac, GuidStr = @GuidStr WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_VNACCS_WarehouseExport WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDauBC", SqlDbType.DateTime, "NgayBatDauBC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetthucBC", SqlDbType.DateTime, "NgayKetthucBC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKho", SqlDbType.NVarChar, "TenKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKho", SqlDbType.NVarChar, "MaKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKChungTu", SqlDbType.NVarChar, "SoTKChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Loai", SqlDbType.Decimal, "Loai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQTiepNhanHD", SqlDbType.NVarChar, "MaHQTiepNhanHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHanHD", SqlDbType.DateTime, "NgayHetHanHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDauBC", SqlDbType.DateTime, "NgayBatDauBC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetthucBC", SqlDbType.DateTime, "NgayKetthucBC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKho", SqlDbType.NVarChar, "TenKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKho", SqlDbType.NVarChar, "MaKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKChungTu", SqlDbType.NVarChar, "SoTKChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Loai", SqlDbType.Decimal, "Loai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQTiepNhanHD", SqlDbType.NVarChar, "MaHQTiepNhanHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHanHD", SqlDbType.DateTime, "NgayHetHanHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO T_KDT_VNACCS_WarehouseExport VALUES(@TrangThaiXuLy, @SoTN, @NgayTN, @NgayBatDauBC, @NgayKetthucBC, @MaHQ, @MaDoanhNghiep, @TenDoanhNghiep, @TenKho, @MaKho, @SoTKChungTu, @Loai, @SoHopDong, @NgayHopDong, @MaHQTiepNhanHD, @NgayHetHanHD, @GhiChuKhac, @GuidStr)";
            string update = "UPDATE T_KDT_VNACCS_WarehouseExport SET TrangThaiXuLy = @TrangThaiXuLy, SoTN = @SoTN, NgayTN = @NgayTN, NgayBatDauBC = @NgayBatDauBC, NgayKetthucBC = @NgayKetthucBC, MaHQ = @MaHQ, MaDoanhNghiep = @MaDoanhNghiep, TenDoanhNghiep = @TenDoanhNghiep, TenKho = @TenKho, MaKho = @MaKho, SoTKChungTu = @SoTKChungTu, Loai = @Loai, SoHopDong = @SoHopDong, NgayHopDong = @NgayHopDong, MaHQTiepNhanHD = @MaHQTiepNhanHD, NgayHetHanHD = @NgayHetHanHD, GhiChuKhac = @GhiChuKhac, GuidStr = @GuidStr WHERE ID = @ID";
            string delete = "DELETE FROM T_KDT_VNACCS_WarehouseExport WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayBatDauBC", SqlDbType.DateTime, "NgayBatDauBC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayKetthucBC", SqlDbType.DateTime, "NgayKetthucBC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenKho", SqlDbType.NVarChar, "TenKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaKho", SqlDbType.NVarChar, "MaKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTKChungTu", SqlDbType.NVarChar, "SoTKChungTu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Loai", SqlDbType.Decimal, "Loai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQTiepNhanHD", SqlDbType.NVarChar, "MaHQTiepNhanHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHetHanHD", SqlDbType.DateTime, "NgayHetHanHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTN", SqlDbType.BigInt, "SoTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTN", SqlDbType.DateTime, "NgayTN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayBatDauBC", SqlDbType.DateTime, "NgayBatDauBC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayKetthucBC", SqlDbType.DateTime, "NgayKetthucBC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, "TenDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenKho", SqlDbType.NVarChar, "TenKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaKho", SqlDbType.NVarChar, "MaKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTKChungTu", SqlDbType.NVarChar, "SoTKChungTu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Loai", SqlDbType.Decimal, "Loai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHopDong", SqlDbType.DateTime, "NgayHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQTiepNhanHD", SqlDbType.NVarChar, "MaHQTiepNhanHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHetHanHD", SqlDbType.DateTime, "NgayHetHanHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChuKhac", SqlDbType.NVarChar, "GhiChuKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static T_KDT_VNACCS_WarehouseExport Load(long id)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<T_KDT_VNACCS_WarehouseExport> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<T_KDT_VNACCS_WarehouseExport> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<T_KDT_VNACCS_WarehouseExport> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertT_KDT_VNACCS_WarehouseExport(int trangThaiXuLy, long soTN, DateTime ngayTN, DateTime ngayBatDauBC, DateTime ngayKetthucBC, string maHQ, string maDoanhNghiep, string tenDoanhNghiep, string tenKho, string maKho, string soTKChungTu, decimal loai, string soHopDong, DateTime ngayHopDong, string maHQTiepNhanHD, DateTime ngayHetHanHD, string ghiChuKhac, string guidStr)
		{
			T_KDT_VNACCS_WarehouseExport entity = new T_KDT_VNACCS_WarehouseExport();	
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.NgayBatDauBC = ngayBatDauBC;
			entity.NgayKetthucBC = ngayKetthucBC;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.TenKho = tenKho;
			entity.MaKho = maKho;
			entity.SoTKChungTu = soTKChungTu;
			entity.Loai = loai;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.MaHQTiepNhanHD = maHQTiepNhanHD;
			entity.NgayHetHanHD = ngayHetHanHD;
			entity.GhiChuKhac = ghiChuKhac;
			entity.GuidStr = guidStr;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@NgayBatDauBC", SqlDbType.DateTime, NgayBatDauBC.Year <= 1753 ? DBNull.Value : (object) NgayBatDauBC);
			db.AddInParameter(dbCommand, "@NgayKetthucBC", SqlDbType.DateTime, NgayKetthucBC.Year <= 1753 ? DBNull.Value : (object) NgayKetthucBC);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenKho", SqlDbType.NVarChar, TenKho);
			db.AddInParameter(dbCommand, "@MaKho", SqlDbType.NVarChar, MaKho);
			db.AddInParameter(dbCommand, "@SoTKChungTu", SqlDbType.NVarChar, SoTKChungTu);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.Decimal, Loai);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object) NgayHopDong);
			db.AddInParameter(dbCommand, "@MaHQTiepNhanHD", SqlDbType.NVarChar, MaHQTiepNhanHD);
			db.AddInParameter(dbCommand, "@NgayHetHanHD", SqlDbType.DateTime, NgayHetHanHD.Year <= 1753 ? DBNull.Value : (object) NgayHetHanHD);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<T_KDT_VNACCS_WarehouseExport> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseExport item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateT_KDT_VNACCS_WarehouseExport(long id, int trangThaiXuLy, long soTN, DateTime ngayTN, DateTime ngayBatDauBC, DateTime ngayKetthucBC, string maHQ, string maDoanhNghiep, string tenDoanhNghiep, string tenKho, string maKho, string soTKChungTu, decimal loai, string soHopDong, DateTime ngayHopDong, string maHQTiepNhanHD, DateTime ngayHetHanHD, string ghiChuKhac, string guidStr)
		{
			T_KDT_VNACCS_WarehouseExport entity = new T_KDT_VNACCS_WarehouseExport();			
			entity.ID = id;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.NgayBatDauBC = ngayBatDauBC;
			entity.NgayKetthucBC = ngayKetthucBC;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.TenKho = tenKho;
			entity.MaKho = maKho;
			entity.SoTKChungTu = soTKChungTu;
			entity.Loai = loai;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.MaHQTiepNhanHD = maHQTiepNhanHD;
			entity.NgayHetHanHD = ngayHetHanHD;
			entity.GhiChuKhac = ghiChuKhac;
			entity.GuidStr = guidStr;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_T_KDT_VNACCS_WarehouseExport_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@NgayBatDauBC", SqlDbType.DateTime, NgayBatDauBC.Year <= 1753 ? DBNull.Value : (object) NgayBatDauBC);
			db.AddInParameter(dbCommand, "@NgayKetthucBC", SqlDbType.DateTime, NgayKetthucBC.Year <= 1753 ? DBNull.Value : (object) NgayKetthucBC);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenKho", SqlDbType.NVarChar, TenKho);
			db.AddInParameter(dbCommand, "@MaKho", SqlDbType.NVarChar, MaKho);
			db.AddInParameter(dbCommand, "@SoTKChungTu", SqlDbType.NVarChar, SoTKChungTu);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.Decimal, Loai);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object) NgayHopDong);
			db.AddInParameter(dbCommand, "@MaHQTiepNhanHD", SqlDbType.NVarChar, MaHQTiepNhanHD);
			db.AddInParameter(dbCommand, "@NgayHetHanHD", SqlDbType.DateTime, NgayHetHanHD.Year <= 1753 ? DBNull.Value : (object) NgayHetHanHD);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<T_KDT_VNACCS_WarehouseExport> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseExport item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateT_KDT_VNACCS_WarehouseExport(long id, int trangThaiXuLy, long soTN, DateTime ngayTN, DateTime ngayBatDauBC, DateTime ngayKetthucBC, string maHQ, string maDoanhNghiep, string tenDoanhNghiep, string tenKho, string maKho, string soTKChungTu, decimal loai, string soHopDong, DateTime ngayHopDong, string maHQTiepNhanHD, DateTime ngayHetHanHD, string ghiChuKhac, string guidStr)
		{
			T_KDT_VNACCS_WarehouseExport entity = new T_KDT_VNACCS_WarehouseExport();			
			entity.ID = id;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.SoTN = soTN;
			entity.NgayTN = ngayTN;
			entity.NgayBatDauBC = ngayBatDauBC;
			entity.NgayKetthucBC = ngayKetthucBC;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.TenDoanhNghiep = tenDoanhNghiep;
			entity.TenKho = tenKho;
			entity.MaKho = maKho;
			entity.SoTKChungTu = soTKChungTu;
			entity.Loai = loai;
			entity.SoHopDong = soHopDong;
			entity.NgayHopDong = ngayHopDong;
			entity.MaHQTiepNhanHD = maHQTiepNhanHD;
			entity.NgayHetHanHD = ngayHetHanHD;
			entity.GhiChuKhac = ghiChuKhac;
			entity.GuidStr = guidStr;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@SoTN", SqlDbType.BigInt, SoTN);
			db.AddInParameter(dbCommand, "@NgayTN", SqlDbType.DateTime, NgayTN.Year <= 1753 ? DBNull.Value : (object) NgayTN);
			db.AddInParameter(dbCommand, "@NgayBatDauBC", SqlDbType.DateTime, NgayBatDauBC.Year <= 1753 ? DBNull.Value : (object) NgayBatDauBC);
			db.AddInParameter(dbCommand, "@NgayKetthucBC", SqlDbType.DateTime, NgayKetthucBC.Year <= 1753 ? DBNull.Value : (object) NgayKetthucBC);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenDoanhNghiep", SqlDbType.NVarChar, TenDoanhNghiep);
			db.AddInParameter(dbCommand, "@TenKho", SqlDbType.NVarChar, TenKho);
			db.AddInParameter(dbCommand, "@MaKho", SqlDbType.NVarChar, MaKho);
			db.AddInParameter(dbCommand, "@SoTKChungTu", SqlDbType.NVarChar, SoTKChungTu);
			db.AddInParameter(dbCommand, "@Loai", SqlDbType.Decimal, Loai);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@NgayHopDong", SqlDbType.DateTime, NgayHopDong.Year <= 1753 ? DBNull.Value : (object) NgayHopDong);
			db.AddInParameter(dbCommand, "@MaHQTiepNhanHD", SqlDbType.NVarChar, MaHQTiepNhanHD);
			db.AddInParameter(dbCommand, "@NgayHetHanHD", SqlDbType.DateTime, NgayHetHanHD.Year <= 1753 ? DBNull.Value : (object) NgayHetHanHD);
			db.AddInParameter(dbCommand, "@GhiChuKhac", SqlDbType.NVarChar, GhiChuKhac);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<T_KDT_VNACCS_WarehouseExport> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseExport item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteT_KDT_VNACCS_WarehouseExport(long id)
		{
			T_KDT_VNACCS_WarehouseExport entity = new T_KDT_VNACCS_WarehouseExport();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_T_KDT_VNACCS_WarehouseExport_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<T_KDT_VNACCS_WarehouseExport> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (T_KDT_VNACCS_WarehouseExport item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
        public void InsertUpdateFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this.ID == 0)
                    {
                        this.TrangThaiXuLy = -1;
                        this.ID = this.Insert(transaction);
                    }
                    else
                    {
                        this.Update(transaction);
                    }
                    int STT = 1;
                    foreach (T_KDT_VNACCS_WarehouseExport_Detail item in WarehouseExportCollection)
                    {
                        if (item.WarehouseExport_ID == 0)
                        {
                            item.WarehouseExport_ID = this.ID;
                            item.STT = STT;
                            item.Insert(transaction);
                        }
                        else
                        {
                            item.Update(transaction);
                        }
                        foreach (T_KDT_VNACCS_WarehouseExport_GoodsDetail itemDetail in item.Collection)
                        {
                            if (itemDetail.WarehouseExport_Details_ID == 0)
                            {
                                itemDetail.WarehouseExport_Details_ID = item.ID;
                                itemDetail.Insert(transaction);
                            }
                            else
                            {
                                itemDetail.Update(transaction);
                            }
                        }
                        STT++;
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
        public void DeleteFull()
        {
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    this.Delete();
                    foreach (T_KDT_VNACCS_WarehouseExport_Detail item in WarehouseExportCollection)
                    {
                        item.Delete(transaction);
                        foreach (T_KDT_VNACCS_WarehouseExport_GoodsDetail itemDetail in item.Collection)
                        {
                            itemDetail.Delete(transaction);
                        }
                    }
                    transaction.Commit();
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
                finally
                {
                    connection.Close();
                }
            }

        }
	}	
}