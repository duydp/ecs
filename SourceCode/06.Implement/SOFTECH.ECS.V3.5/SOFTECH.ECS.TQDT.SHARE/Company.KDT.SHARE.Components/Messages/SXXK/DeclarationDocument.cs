﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class DeclarationDocument : DeclarationBase
    {
        /// <summary>
        /// Ngày hoàn thành thủ tục Xuất khẩu YYYY-MM-DD 
        /// </summary>
        [XmlElement("clearance")]
        public string Clearance { get; set; }

        /// <summary>
        /// Product
        /// </summary>
        [XmlElement("Product")]
        public List<SXXK_MaterialSupply_Product> Product { get; set; }


        /// <summary>
        /// Container
        /// </summary>
        [XmlElement("TransportEquipments")]
        public TransportEquipments TransportEquipments { get; set; }

    }
}
