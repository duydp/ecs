﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.DuLieuChuan;

namespace Company.Interface.DanhMucChuan
{
    public partial class DoiTacEditForm : BaseForm
    {        
        public DoiTacEditForm()
        {
            InitializeComponent();
        }

        private void PTVTForm_Load(object sender, EventArgs e)
        {
            txtMa.Focus();
        }
        private void uiButton3_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            try
            {
                DoiTac dt = new DoiTac();
                dt.DiaChi = txtDiaChi.Text.Trim();
                dt.DienThoai = txtDiaChi.Text.Trim();
                dt.Email = txtMail.Text.Trim();
                dt.Fax=txtFax.Text.Trim();
                dt.GhiChu=txtGhiChu.Text.Trim();
                dt.MaCongTy=txtMa.Text.Trim();
                dt.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                dt.TenCongTy=txtTenCongTy.Text.Trim();
                dt.Insert();
              //  ShowMessage("Cập nhật thành công.", false);
                MLMessages("Lưu thành công", "MSG_SAV02", "", false); 
                this.Close();
                
            }
            catch (Exception ex)
            {
               // ShowMessage("Lỗi: " + ex.Message, false);
                MLMessages("Lưu không thành công", "MSG_SAV01", "", false); 
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            try
            {
                DoiTac dt = new DoiTac();
                dt.DiaChi = txtDiaChi.Text.Trim();;
                dt.DienThoai = txtDiaChi.Text.Trim();;
                dt.Email = txtMail.Text.Trim();;
                dt.Fax=txtFax.Text.Trim();;
                dt.GhiChu=txtGhiChu.Text.Trim();;
                dt.MaCongTy=txtMa.Text.Trim();;
                dt.MaDoanhNghiep=GlobalSettings.MA_DON_VI;
                dt.TenCongTy=txtTenCongTy.Text.Trim();
                //ShowMessage("Cập nhật thành công.", false);
                MLMessages("Lưu thành công", "MSG_SAV02", "", false); 
                txtDiaChi.Clear();
                txtDienThoai.Clear();
                txtFax.Clear();
                txtGhiChu.Clear();
                txtMa.Text = "";
                txtMa.Focus();
                txtMail.Text = "";
                txtTenCongTy.Text = "";
                dt.Insert();
                
            }
            catch (Exception ex)
            {
               // ShowMessage("Lỗi: " + ex.Message, false);
                MLMessages("Lưu không thành công", "MSG_SAV01", "", false); 
            }
        }

       
    }
}

