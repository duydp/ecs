﻿//using Infragistics.Excel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Infragistics.Excel;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK
{
    public partial class KiemTraDuLieuTKForm : Company.Interface.BaseForm
    {        
        public KiemTraDuLieuTKForm()
        {
            InitializeComponent();
        }

        private void KiemTraDuLieuTKForm_Load(object sender, EventArgs e)
        {
            txtNamTiepNhan.Text = DateTime.Today.Year.ToString();
            cbbLoaiToKhai.SelectedIndex = 0;
            btnSearch_Click(null, null);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {           
            // Xây dựng điều kiện tìm kiếm.
            string where = " AND MaLoaiHinh LIKE '" + cbbLoaiToKhai.SelectedValue.ToString() + "%'";
            if (txtSoTiepNhan.TextLength > 0)
            {
                where += " AND SoToKhai = " + txtSoTiepNhan.Value;
            }

            if (txtNamTiepNhan.TextLength > 0)
            {
                where += " AND NamDangKy = " + txtNamTiepNhan.Value;
            }         

            if (txtTenChuHang.Text.Trim().Length > 0)
            {
                if (txtTenChuHang.Text.Trim() != "*")
                    where += " AND TenChuHang LIKE '%" + txtTenChuHang.Text.Trim() + "%'";
            }
            else
            {
                where += " AND TenChuHang LIKE ''";
            }
          
            // Thực hiện tìm kiếm.                       
            dgList.DataSource = (new Company.BLL.SXXK.ToKhai.HangMauDich()).GetHangMauDich(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, where);

        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Text);
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                ToKhaiMauDichDetailForm f = new ToKhaiMauDichDetailForm();
                f.TKMD.SoToKhai= Convert.ToInt32(e.Row.Cells["SoToKhai"].Text);
                f.TKMD.NamDangKy = Convert.ToInt16(e.Row.Cells["NamDangKy"].Text);
                f.TKMD.MaHaiQuan =GlobalSettings.MA_HAI_QUAN;
                f.TKMD.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Value.ToString();
                f.TKMD.Load();
                f.TKMD.LoadHMDCollection();
                f.NhomLoaiHinh = f.TKMD.MaLoaiHinh.Substring(0, 3);
                f.MdiParent = this.ParentForm;
                f.Show();   
            }
        }

       
    }
}

