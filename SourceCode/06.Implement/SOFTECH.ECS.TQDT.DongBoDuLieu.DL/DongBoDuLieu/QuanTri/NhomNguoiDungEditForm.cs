﻿using System;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.BLL.Utils;
using Company.BLL.DuLieuChuan;
using System.Data;
using Company.QuanTri;
using System.Text;
using System.Security.Cryptography;
using Janus.Windows.GridEX;

namespace DongBoDuLieu.QuanTri
{
    public partial class NhomNguoiDungEditForm : BaseForm
    {
        public GROUPS group = new GROUPS();
        public NhomNguoiDungEditForm()
        {
            InitializeComponent();
        }
               

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid)
                    return;
                if (group.CheckGroupName(txtTenNHom.Text.Trim()))
                {
                   // ShowMessage("Tên nhóm người dùng này đã có.Bạn hãy nhập tên khác", false);
                    MLMessages("Tên nhóm người dùng này đã tồn tại trong hệ thống", "MSG_USER02", "", false);
                    return;
                }
                group.TEN_NHOM = txtTenNHom.Text.Trim();
                group.MO_TA = txtHoTen.Text.Trim();
                group.UserList.Clear();
                foreach (GridEXRow row in dgList.GetCheckedRows())
                {
                    User user = (User)row.DataRow;
                    group.UserList.Add(user.ID);
                }
                group.InsertUpdateFull();
                this.Close();
            }
            catch(Exception ex)
            {
                ShowMessage(" "+ex.Message, false);
            }
        }      
        private void NguoiDungEditForm_Load(object sender, EventArgs e)
        {
            txtHoTen.Text = group.MO_TA;
            txtTenNHom.Text = group.TEN_NHOM;
            User user = new User();
            UserCollection collection= user.SelectCollectionAll();            
            group.LoadUserList();
            for (int i = 0; i < group.UserList.Count; ++i)
            {
                foreach (User row in collection)
                {
                    if (row.ID.ToString() == group.UserList[i].ToString())
                    {
                        row.Check = true;
                        break;
                    }
                }
            }
            dgList.DataSource = collection;
            if (group.MA_NHOM > 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.UpdateGroup)))
                {
                    btnUpdate.Visible = false;
                }
            }
            else
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSystem.CreateGroup)))
                {
                    btnUpdate.Visible = false;
                }
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.Cells["CheckData"].Value.ToString() == "True")
            {
                e.Row.CheckState = RowCheckState.Checked;
            }
            else
                e.Row.CheckState = RowCheckState.Unchecked;
        }
      
    }
}