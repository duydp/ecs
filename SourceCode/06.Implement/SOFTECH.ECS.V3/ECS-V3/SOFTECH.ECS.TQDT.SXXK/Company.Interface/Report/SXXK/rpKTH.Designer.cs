﻿namespace Company.Interface.Report.SXXK
{
    partial class rpKTH
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(rpKTH));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrLabel7 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel9 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel8 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHD = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel2 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox4 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox3 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrPictureBox1 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.xrLine2 = new DevExpress.XtraReports.UI.XRLine();
            this.lblSoLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMatHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel18 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel17 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel15 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable3 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCuc1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.lblCuc2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.cellSoCV = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabel1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCuc1 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            this.xrLabel16 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel12 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel11 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel14 = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel13 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChuKy = new DevExpress.XtraReports.UI.XRLabel();
            this.xrPictureBox2 = new DevExpress.XtraReports.UI.XRPictureBox();
            this.lblNgaythang = new DevExpress.XtraReports.UI.XRLabel();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Height = 0;
            this.Detail.Name = "Detail";
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblNgaythang,
            this.xrLabel7,
            this.lblNgayHD,
            this.xrLabel9,
            this.xrLabel8,
            this.lblSoHD,
            this.xrLabel2,
            this.xrPictureBox4,
            this.xrPictureBox3,
            this.xrPictureBox1,
            this.xrLine2,
            this.lblSoLuong,
            this.lblSoTK,
            this.lblMatHang,
            this.xrLabel18,
            this.xrLabel17,
            this.xrLabel15,
            this.xrTable3,
            this.xrLabel1,
            this.lblDoanhNghiep,
            this.lblChiCuc1,
            this.xrLabel3});
            this.ReportHeader.Height = 420;
            this.ReportHeader.Name = "ReportHeader";
            this.ReportHeader.ParentStyleUsing.UseBorders = false;
            // 
            // xrLabel7
            // 
            this.xrLabel7.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel7.Location = new System.Drawing.Point(142, 367);
            this.xrLabel7.Multiline = true;
            this.xrLabel7.Name = "xrLabel7";
            this.xrLabel7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel7.ParentStyleUsing.UseFont = false;
            this.xrLabel7.Size = new System.Drawing.Size(591, 50);
            this.xrLabel7.Text = "Do hàng nhập khẩu qua đường Bưu điện.";
            this.xrLabel7.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblNgayHD
            // 
            this.lblNgayHD.BorderColor = System.Drawing.Color.Black;
            this.lblNgayHD.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayHD.Location = new System.Drawing.Point(608, 333);
            this.lblNgayHD.Name = "lblNgayHD";
            this.lblNgayHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHD.ParentStyleUsing.UseBackColor = false;
            this.lblNgayHD.ParentStyleUsing.UseBorderColor = false;
            this.lblNgayHD.ParentStyleUsing.UseBorders = false;
            this.lblNgayHD.ParentStyleUsing.UseFont = false;
            this.lblNgayHD.Size = new System.Drawing.Size(125, 25);
            this.lblNgayHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblNgayHD.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel9
            // 
            this.xrLabel9.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel9.Location = new System.Drawing.Point(550, 333);
            this.xrLabel9.Multiline = true;
            this.xrLabel9.Name = "xrLabel9";
            this.xrLabel9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel9.ParentStyleUsing.UseFont = false;
            this.xrLabel9.Size = new System.Drawing.Size(58, 25);
            this.xrLabel9.Text = ". Ngày:";
            this.xrLabel9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel8
            // 
            this.xrLabel8.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel8.Location = new System.Drawing.Point(333, 333);
            this.xrLabel8.Multiline = true;
            this.xrLabel8.Name = "xrLabel8";
            this.xrLabel8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel8.ParentStyleUsing.UseFont = false;
            this.xrLabel8.Size = new System.Drawing.Size(116, 25);
            this.xrLabel8.Text = "thuộc hợp đồng:";
            this.xrLabel8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoHD
            // 
            this.lblSoHD.BorderColor = System.Drawing.Color.Black;
            this.lblSoHD.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHD.Location = new System.Drawing.Point(450, 333);
            this.lblSoHD.Name = "lblSoHD";
            this.lblSoHD.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHD.ParentStyleUsing.UseBackColor = false;
            this.lblSoHD.ParentStyleUsing.UseBorderColor = false;
            this.lblSoHD.ParentStyleUsing.UseBorders = false;
            this.lblSoHD.ParentStyleUsing.UseFont = false;
            this.lblSoHD.Size = new System.Drawing.Size(100, 25);
            this.lblSoHD.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSoHD.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel2
            // 
            this.xrLabel2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel2.Location = new System.Drawing.Point(83, 233);
            this.xrLabel2.Multiline = true;
            this.xrLabel2.Name = "xrLabel2";
            this.xrLabel2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel2.ParentStyleUsing.UseFont = false;
            this.xrLabel2.Size = new System.Drawing.Size(650, 42);
            this.xrLabel2.Text = "Ngày: 01/12/2008, Công ty đến Chi cục Hải quan quản lý hàng XNK ngoài đăng ký tờ " +
                "khai nhập khẩu chi tiết như sau:";
            this.xrLabel2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel2.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrPictureBox4
            // 
            this.xrPictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox4.Image")));
            this.xrPictureBox4.Location = new System.Drawing.Point(125, 308);
            this.xrPictureBox4.Name = "xrPictureBox4";
            this.xrPictureBox4.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox4.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrPictureBox3
            // 
            this.xrPictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox3.Image")));
            this.xrPictureBox3.Location = new System.Drawing.Point(125, 283);
            this.xrPictureBox3.Name = "xrPictureBox3";
            this.xrPictureBox3.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox3.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrPictureBox1
            // 
            this.xrPictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox1.Image")));
            this.xrPictureBox1.Location = new System.Drawing.Point(125, 333);
            this.xrPictureBox1.Name = "xrPictureBox1";
            this.xrPictureBox1.Size = new System.Drawing.Size(16, 25);
            this.xrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.CenterImage;
            // 
            // xrLine2
            // 
            this.xrLine2.Location = new System.Drawing.Point(475, 50);
            this.xrLine2.Name = "xrLine2";
            this.xrLine2.Size = new System.Drawing.Size(141, 9);
            // 
            // lblSoLuong
            // 
            this.lblSoLuong.BorderColor = System.Drawing.Color.Black;
            this.lblSoLuong.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoLuong.Location = new System.Drawing.Point(217, 308);
            this.lblSoLuong.Name = "lblSoLuong";
            this.lblSoLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoLuong.ParentStyleUsing.UseBackColor = false;
            this.lblSoLuong.ParentStyleUsing.UseBorderColor = false;
            this.lblSoLuong.ParentStyleUsing.UseBorders = false;
            this.lblSoLuong.ParentStyleUsing.UseFont = false;
            this.lblSoLuong.Size = new System.Drawing.Size(516, 25);
            this.lblSoLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblSoLuong.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblSoTK
            // 
            this.lblSoTK.BorderColor = System.Drawing.Color.Black;
            this.lblSoTK.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTK.Location = new System.Drawing.Point(258, 333);
            this.lblSoTK.Name = "lblSoTK";
            this.lblSoTK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTK.ParentStyleUsing.UseBackColor = false;
            this.lblSoTK.ParentStyleUsing.UseBorderColor = false;
            this.lblSoTK.ParentStyleUsing.UseBorders = false;
            this.lblSoTK.ParentStyleUsing.UseFont = false;
            this.lblSoTK.Size = new System.Drawing.Size(75, 25);
            this.lblSoTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblSoTK.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblMatHang
            // 
            this.lblMatHang.BorderColor = System.Drawing.Color.Black;
            this.lblMatHang.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMatHang.Location = new System.Drawing.Point(217, 283);
            this.lblMatHang.Name = "lblMatHang";
            this.lblMatHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblMatHang.ParentStyleUsing.UseBackColor = false;
            this.lblMatHang.ParentStyleUsing.UseBorderColor = false;
            this.lblMatHang.ParentStyleUsing.UseBorders = false;
            this.lblMatHang.ParentStyleUsing.UseFont = false;
            this.lblMatHang.Size = new System.Drawing.Size(516, 25);
            this.lblMatHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblMatHang.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel18
            // 
            this.xrLabel18.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel18.Location = new System.Drawing.Point(142, 333);
            this.xrLabel18.Multiline = true;
            this.xrLabel18.Name = "xrLabel18";
            this.xrLabel18.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel18.ParentStyleUsing.UseFont = false;
            this.xrLabel18.Size = new System.Drawing.Size(116, 25);
            this.xrLabel18.Text = "Theo TKHQ số:";
            this.xrLabel18.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel17
            // 
            this.xrLabel17.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel17.Location = new System.Drawing.Point(142, 308);
            this.xrLabel17.Multiline = true;
            this.xrLabel17.Name = "xrLabel17";
            this.xrLabel17.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel17.ParentStyleUsing.UseFont = false;
            this.xrLabel17.Size = new System.Drawing.Size(75, 25);
            this.xrLabel17.Text = "Số lượng:";
            this.xrLabel17.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrLabel15
            // 
            this.xrLabel15.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel15.Location = new System.Drawing.Point(142, 283);
            this.xrLabel15.Multiline = true;
            this.xrLabel15.Name = "xrLabel15";
            this.xrLabel15.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel15.ParentStyleUsing.UseFont = false;
            this.xrLabel15.Size = new System.Drawing.Size(75, 25);
            this.xrLabel15.Text = "Mặt hàng:";
            this.xrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // xrTable3
            // 
            this.xrTable3.Location = new System.Drawing.Point(17, 8);
            this.xrTable3.Name = "xrTable3";
            this.xrTable3.ParentStyleUsing.UseBorders = false;
            this.xrTable3.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3,
            this.xrTableRow5,
            this.xrTableRow4,
            this.xrTableRow1});
            this.xrTable3.Size = new System.Drawing.Size(275, 84);
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCuc1});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(275, 25);
            // 
            // lblCuc1
            // 
            this.lblCuc1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCuc1.Location = new System.Drawing.Point(0, 0);
            this.lblCuc1.Multiline = true;
            this.lblCuc1.Name = "lblCuc1";
            this.lblCuc1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCuc1.ParentStyleUsing.UseFont = false;
            this.lblCuc1.Size = new System.Drawing.Size(275, 25);
            this.lblCuc1.Text = "Cục Hải Quan Đà Nẵng";
            this.lblCuc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCuc1.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.lblCuc2});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Size = new System.Drawing.Size(275, 25);
            // 
            // lblCuc2
            // 
            this.lblCuc2.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCuc2.Location = new System.Drawing.Point(0, 0);
            this.lblCuc2.Multiline = true;
            this.lblCuc2.Name = "lblCuc2";
            this.lblCuc2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 2, 2, 100F);
            this.lblCuc2.ParentStyleUsing.UseFont = false;
            this.lblCuc2.Size = new System.Drawing.Size(275, 25);
            this.lblCuc2.Text = "CHI CỤC HẢI QUAN QL HÀNG XNK NGOÀI KCN";
            this.lblCuc2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblCuc2.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.cellSoCV});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(275, 17);
            // 
            // cellSoCV
            // 
            this.cellSoCV.BorderColor = System.Drawing.Color.Black;
            this.cellSoCV.CanShrink = true;
            this.cellSoCV.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cellSoCV.Location = new System.Drawing.Point(0, 0);
            this.cellSoCV.Multiline = true;
            this.cellSoCV.Name = "cellSoCV";
            this.cellSoCV.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.cellSoCV.ParentStyleUsing.UseBackColor = false;
            this.cellSoCV.ParentStyleUsing.UseBorderColor = false;
            this.cellSoCV.ParentStyleUsing.UseBorders = false;
            this.cellSoCV.ParentStyleUsing.UseFont = false;
            this.cellSoCV.Size = new System.Drawing.Size(275, 17);
            this.cellSoCV.Text = "Số:       /CV.XNK";
            this.cellSoCV.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.cellSoCV.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(275, 17);
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrTableCell1.Location = new System.Drawing.Point(0, 0);
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrTableCell1.ParentStyleUsing.UseFont = false;
            this.xrTableCell1.Size = new System.Drawing.Size(275, 17);
            this.xrTableCell1.Text = "V/v: Kiểm tra hộ.";
            this.xrTableCell1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            this.xrTableCell1.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel1
            // 
            this.xrLabel1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel1.Location = new System.Drawing.Point(367, 8);
            this.xrLabel1.Multiline = true;
            this.xrLabel1.Name = "xrLabel1";
            this.xrLabel1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel1.ParentStyleUsing.UseFont = false;
            this.xrLabel1.Size = new System.Drawing.Size(366, 42);
            this.xrLabel1.Text = "CỘNG HÒA XÃ HỘI CHỦ NGHĨA VIỆT NAM\r\nĐộc Lập - Tự Do - Hạnh Phúc";
            this.xrLabel1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblDoanhNghiep
            // 
            this.lblDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDoanhNghiep.Location = new System.Drawing.Point(83, 183);
            this.lblDoanhNghiep.Multiline = true;
            this.lblDoanhNghiep.Name = "lblDoanhNghiep";
            this.lblDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblDoanhNghiep.Size = new System.Drawing.Size(650, 42);
            this.lblDoanhNghiep.Text = "CÔNG TY VINATEX, ĐỊA CHỈ: TP ĐÀ NẴNG.";
            this.lblDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblDoanhNghiep.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblChiCuc1
            // 
            this.lblChiCuc1.BorderColor = System.Drawing.Color.Black;
            this.lblChiCuc1.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChiCuc1.Location = new System.Drawing.Point(167, 125);
            this.lblChiCuc1.Name = "lblChiCuc1";
            this.lblChiCuc1.Padding = new DevExpress.XtraPrinting.PaddingInfo(10, 2, 0, 0, 100F);
            this.lblChiCuc1.ParentStyleUsing.UseBackColor = false;
            this.lblChiCuc1.ParentStyleUsing.UseBorderColor = false;
            this.lblChiCuc1.ParentStyleUsing.UseBorders = false;
            this.lblChiCuc1.ParentStyleUsing.UseFont = false;
            this.lblChiCuc1.Size = new System.Drawing.Size(566, 25);
            this.lblChiCuc1.Text = "- CHI CỤC HẢI QUAN QL HÀNG XNK NGOÀI KCN";
            this.lblChiCuc1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.lblChiCuc1.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel3.Location = new System.Drawing.Point(92, 125);
            this.xrLabel3.Multiline = true;
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.ParentStyleUsing.UseFont = false;
            this.xrLabel3.Size = new System.Drawing.Size(75, 25);
            this.xrLabel3.Text = "Kính gửi:";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrLabel16,
            this.xrLabel12,
            this.xrLabel11,
            this.xrLabel14,
            this.xrLabel13,
            this.lblChuKy});
            this.ReportFooter.Height = 362;
            this.ReportFooter.Name = "ReportFooter";
            // 
            // xrLabel16
            // 
            this.xrLabel16.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel16.Location = new System.Drawing.Point(75, 283);
            this.xrLabel16.Multiline = true;
            this.xrLabel16.Name = "xrLabel16";
            this.xrLabel16.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel16.ParentStyleUsing.UseFont = false;
            this.xrLabel16.Size = new System.Drawing.Size(658, 25);
            this.xrLabel16.Text = "Nơi nhận.";
            this.xrLabel16.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrLabel16.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel12
            // 
            this.xrLabel12.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel12.Location = new System.Drawing.Point(150, 308);
            this.xrLabel12.Multiline = true;
            this.xrLabel12.Name = "xrLabel12";
            this.xrLabel12.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel12.ParentStyleUsing.UseFont = false;
            this.xrLabel12.Size = new System.Drawing.Size(583, 42);
            this.xrLabel12.Text = "Như trên\r\nLưu HQQLHXNKNKCN";
            this.xrLabel12.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel12.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel11
            // 
            this.xrLabel11.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel11.Location = new System.Drawing.Point(42, 142);
            this.xrLabel11.Multiline = true;
            this.xrLabel11.Name = "xrLabel11";
            this.xrLabel11.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel11.ParentStyleUsing.UseFont = false;
            this.xrLabel11.Size = new System.Drawing.Size(691, 42);
            this.xrLabel11.Text = "        Địa chỉ liện hệ: Chi Cục Hải Quan Quản Lý Hàng XNK ngoài KCN, Thành phố Đ" +
                "à Nẵng. \r\n        Điện thoại: 05113.500500, Fax : 05113.100100";
            this.xrLabel11.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel11.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel14
            // 
            this.xrLabel14.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel14.Location = new System.Drawing.Point(42, 108);
            this.xrLabel14.Multiline = true;
            this.xrLabel14.Name = "xrLabel14";
            this.xrLabel14.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel14.ParentStyleUsing.UseFont = false;
            this.xrLabel14.Size = new System.Drawing.Size(691, 25);
            this.xrLabel14.Text = "        Trân trọng.";
            this.xrLabel14.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleJustify;
            this.xrLabel14.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrLabel13
            // 
            this.xrLabel13.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xrLabel13.Location = new System.Drawing.Point(42, 0);
            this.xrLabel13.Multiline = true;
            this.xrLabel13.Name = "xrLabel13";
            this.xrLabel13.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel13.ParentStyleUsing.UseFont = false;
            this.xrLabel13.Size = new System.Drawing.Size(691, 100);
            this.xrLabel13.Text = resources.GetString("xrLabel13.Text");
            this.xrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel13.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // lblChuKy
            // 
            this.lblChuKy.BorderColor = System.Drawing.Color.Black;
            this.lblChuKy.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChuKy.Location = new System.Drawing.Point(350, 200);
            this.lblChuKy.Multiline = true;
            this.lblChuKy.Name = "lblChuKy";
            this.lblChuKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChuKy.ParentStyleUsing.UseBackColor = false;
            this.lblChuKy.ParentStyleUsing.UseBorderColor = false;
            this.lblChuKy.ParentStyleUsing.UseBorders = false;
            this.lblChuKy.ParentStyleUsing.UseFont = false;
            this.lblChuKy.Size = new System.Drawing.Size(383, 50);
            this.lblChuKy.Text = "Chi Cục Hải Quan Quản Lý Hàng XNK ngoài KCN";
            this.lblChuKy.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            this.lblChuKy.PreviewDoubleClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lbl_PreviewDoubleClick);
            // 
            // xrPictureBox2
            // 
            this.xrPictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("xrPictureBox2.Image")));
            this.xrPictureBox2.Location = new System.Drawing.Point(125, 325);
            this.xrPictureBox2.Name = "xrPictureBox2";
            this.xrPictureBox2.Size = new System.Drawing.Size(16, 25);
            // 
            // lblNgaythang
            // 
            this.lblNgaythang.BorderColor = System.Drawing.Color.Black;
            this.lblNgaythang.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgaythang.Location = new System.Drawing.Point(367, 58);
            this.lblNgaythang.Name = "lblNgaythang";
            this.lblNgaythang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 0, 0, 0, 100F);
            this.lblNgaythang.ParentStyleUsing.UseBackColor = false;
            this.lblNgaythang.ParentStyleUsing.UseBorderColor = false;
            this.lblNgaythang.ParentStyleUsing.UseBorders = false;
            this.lblNgaythang.ParentStyleUsing.UseFont = false;
            this.lblNgaythang.Size = new System.Drawing.Size(367, 25);
            this.lblNgaythang.Text = ".................,ngày...........tháng.........năm.............";
            this.lblNgaythang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // rpKTH
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.ReportHeader,
            this.ReportFooter});
            this.Margins = new System.Drawing.Printing.Margins(34, 30, 65, 100);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            ((System.ComponentModel.ISupportInitialize)(this.xrTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.XRLabel xrLabel1;
        private DevExpress.XtraReports.UI.XRLabel lblDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblChiCuc1;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
        private DevExpress.XtraReports.UI.XRLabel lblChuKy;
        private DevExpress.XtraReports.UI.XRTable xrTable3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell lblCuc1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell lblCuc2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell cellSoCV;
        private DevExpress.XtraReports.UI.XRLabel xrLabel13;
        private DevExpress.XtraReports.UI.XRLabel xrLabel14;
        private DevExpress.XtraReports.UI.XRLabel xrLabel15;
        private DevExpress.XtraReports.UI.XRLabel xrLabel18;
        private DevExpress.XtraReports.UI.XRLabel xrLabel17;
        private DevExpress.XtraReports.UI.XRLabel lblSoLuong;
        private DevExpress.XtraReports.UI.XRLabel lblSoTK;
        private DevExpress.XtraReports.UI.XRLabel lblMatHang;
        private DevExpress.XtraReports.UI.XRLine xrLine2;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox1;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox2;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox4;
        private DevExpress.XtraReports.UI.XRPictureBox xrPictureBox3;
        private DevExpress.XtraReports.UI.XRLabel xrLabel2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel9;
        private DevExpress.XtraReports.UI.XRLabel xrLabel8;
        private DevExpress.XtraReports.UI.XRLabel lblSoHD;
        private DevExpress.XtraReports.UI.XRLabel xrLabel7;
        private DevExpress.XtraReports.UI.XRLabel xrLabel11;
        private DevExpress.XtraReports.UI.XRLabel xrLabel16;
        private DevExpress.XtraReports.UI.XRLabel xrLabel12;
        private DevExpress.XtraReports.UI.XRLabel lblNgaythang;
    }
}
