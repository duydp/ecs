﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using Company.GC.BLL.KDT.GC;
using ClosedXML.Excel;

namespace Company.Interface.GC
{
    public partial class BC02HSTK_GC_TT117_ExportExcelForm : Company.Interface.BaseForm
    {
        DataTable dt = new DataTable();
        public int Maxrow;
        public int MinRow;
        public HopDong HD;
        public BC02HSTK_GC_TT117_ExportExcelForm()
        {
            InitializeComponent();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Báo cáo 02 _" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel Workbook (*.xls)| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    //Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
                    //dt = SP.BaoCaoBC02HSTK_GC_TT117_Export(this.HD.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI).Tables[0];
                    //ExportExcel(dt,sfNPL.FileName);
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgListNPL;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void ExportExcel(DataTable dt, string destinationPath)
        {
            try
            {
                XLWorkbook wb = new XLWorkbook();
                wb.Worksheets.Add(dt, "WorksheetName");
                wb.SaveAs(destinationPath);        
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void ExportPDF(DataTable dataTable, string destinationPath)
        {
            Document document = new Document();
            document.SetPageSize(iTextSharp.text.PageSize.A4.Rotate());
            PdfWriter writer = PdfWriter.GetInstance(document, new FileStream(destinationPath, FileMode.Create));
            document.Open();
            iTextSharp.text.Font font = new iTextSharp.text.Font(BaseFont.CreateFont(Application.StartupPath + @"\times.ttf", "Identity-H", false));
            PdfPTable table = new PdfPTable(dataTable.Columns.Count);
            table.WidthPercentage = 100;
            //Set columns names in the pdf file
            string ColumnName = "";
            for (int k = 0; k < dataTable.Columns.Count; k++)
            {
                ColumnName = dataTable.Columns[k].ColumnName.ToString();
                switch (ColumnName)
                {
                    case "SoToKhai":
                        ColumnName = "Số tờ khai";
                        break;
                    case "NgayDangKy":
                        ColumnName = "Ngày đăng ký";
                        break;
                    case "MaHang":
                        ColumnName = "Mã hàng hóa";
                        break;
                    case "TenHang":
                        ColumnName = "Tên hàng hóa";
                        break;
                    case "DVT":
                        ColumnName = "ĐVT";
                        break;
                    case "SoLuong":
                        ColumnName = "Số lượng";
                        break;
                    case "TongCong":
                        ColumnName = "Tổng cộng";
                        break;
                    default:
                        break;
                }
                PdfPCell cell = new PdfPCell(new Phrase(ColumnName,font));

                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                cell.BackgroundColor = new iTextSharp.text.BaseColor(51, 102, 102);
                table.AddCell(cell);
            }

            MinRow = Convert.ToInt32(txtMinRow.Value);
            Maxrow = Convert.ToInt32(txtMaxrow.Value);
            //Add values of DataTable in pdf file
            for (int i = MinRow; i < Maxrow; i++)
            {
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    PdfPCell cell = new PdfPCell(new Phrase(dataTable.Rows[i][j].ToString(),font));
                    //Align the cell in the center
                    if (j==0)
                    {
                        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                    }
                    else
                    {
                        if (j % 6 == 0)
                        {
                            cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                            cell.VerticalAlignment = PdfPCell.ALIGN_RIGHT;
                        }
                        else
                        {
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            cell.VerticalAlignment = PdfPCell.ALIGN_CENTER;
                        }
                    }
                    table.AddCell(cell);
                }
            }

            document.Add(table);
            document.Close();
        }
        private void BindData()
        {
            try
            {
                Company.GC.BLL.GC.SanPham SP = new Company.GC.BLL.GC.SanPham();
                dt = SP.BaoCaoBC02HSTK_GC_TT117(this.HD.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI).Tables[0];
                dgListNPL.Refresh();
                dgListNPL.DataSource = dt;
                dgListNPL.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExportPDF_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Báo cáo 02 - " + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".pdf";
                sfNPL.Filter = "PDF| *.pdf";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    ExportPDF(dt,sfNPL.FileName);
                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void BC02HSTK_GC_TT117_ExportExcelForm_Load(object sender, EventArgs e)
        {
            BindData();
            txtMaxrow.Value = dt.Rows.Count.ToString();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                MinRow = Convert.ToInt32(txtMinRow.Value);
                Maxrow = Convert.ToInt32(txtMaxrow.Value);

                DataTable dt2 = dt.Clone();
                for (int i = MinRow; i < Maxrow; i++)
                {
                    DataRow dr = dt2.NewRow();
                    dr.ItemArray = dt.Rows[i].ItemArray;
                    dt2.Rows.Add(dr);
                }  
                Company.Interface.Report.GC.BangKe02_HSTK_TT13 f = new Company.Interface.Report.GC.BangKe02_HSTK_TT13();                
                f.HD = this.HD;
                f.BindReport(dt2);
                f.ShowPreview();
                //if (!GlobalSettings.InTongLuyKeHSTK)
                //{
                //    Company.Interface.Report.ReportViewBC02_HSTK_TT13Form rpt = new Company.Interface.Report.ReportViewBC02_HSTK_TT13Form();
                //    rpt.HD = this.HD;
                //    rpt.Show();
                //}
                //else
                //{
                //    Company.Interface.Report.GC.BangKe02_HSTK_TT13 BC02 = new Company.Interface.Report.GC.BangKe02_HSTK_TT13();
                //    BC02.HD = HD;
                //    DataSet dataSource = new Company.GC.BLL.GC.SanPham().BaoCaoBC02HSTK_GC_TT117_New(this.HD.ID, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                //    BC02.BindReport(dataSource.Tables[0]);
                //    BC02.ShowRibbonPreview();
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
