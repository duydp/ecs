﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class IDA
    {
        public List<IDA_HANGHOA> ListHangHoa { get; set; }

        public StringBuilder BuilEdiMessagesIDA(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<IDA>(StrBuild, true, GlobalVNACC.PathConfig, "IDA");
            foreach (IDA_HANGHOA item in ListHangHoa)
            {
                item.BuildEdiMessages<IDA_HANGHOA>(StrBuild, true, GlobalVNACC.PathConfig, "IDA_HANGHOA");
            }
            return str;
        }
        public StringBuilder BuilEdiMessagesIDA01(StringBuilder StrBuild)
        {
            StringBuilder str = StrBuild;
            str = BuildEdiMessages<IDA>(StrBuild, true, GlobalVNACC.PathConfig, "IDA01");
            foreach (IDA_HANGHOA item in ListHangHoa)
            {
                item.BuildEdiMessages<IDA_HANGHOA>(StrBuild, true, GlobalVNACC.PathConfig, "IDA_HANGHOA");
            }
            return str;
        }
    }
}
