﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace LicenseCode
{
	public partial class LM_User : ICloneable
	{
		#region Properties.
		
		public int ID { set; get; }
		public string FirstName { set; get; }
		public string LastName { set; get; }
		public string UserName { set; get; }
		public string PassWord { set; get; }
		public string Department { set; get; }
		public string position { set; get; }
		public string Title { set; get; }
		public string Email { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<LM_User> ConvertToCollection(IDataReader reader)
		{
			List<LM_User> collection = new List<LM_User>();
			while (reader.Read())
			{
				LM_User entity = new LM_User();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("FirstName"))) entity.FirstName = reader.GetString(reader.GetOrdinal("FirstName"));
				if (!reader.IsDBNull(reader.GetOrdinal("LastName"))) entity.LastName = reader.GetString(reader.GetOrdinal("LastName"));
				if (!reader.IsDBNull(reader.GetOrdinal("UserName"))) entity.UserName = reader.GetString(reader.GetOrdinal("UserName"));
				if (!reader.IsDBNull(reader.GetOrdinal("PassWord"))) entity.PassWord = reader.GetString(reader.GetOrdinal("PassWord"));
				if (!reader.IsDBNull(reader.GetOrdinal("Department"))) entity.Department = reader.GetString(reader.GetOrdinal("Department"));
				if (!reader.IsDBNull(reader.GetOrdinal("position"))) entity.position = reader.GetString(reader.GetOrdinal("position"));
				if (!reader.IsDBNull(reader.GetOrdinal("Title"))) entity.Title = reader.GetString(reader.GetOrdinal("Title"));
				if (!reader.IsDBNull(reader.GetOrdinal("Email"))) entity.Email = reader.GetString(reader.GetOrdinal("Email"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<LM_User> collection, int id)
        {
            foreach (LM_User item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_LM_User VALUES(@FirstName, @LastName, @UserName, @PassWord, @Department, @position, @Title, @Email)";
            string update = "UPDATE t_LM_User SET FirstName = @FirstName, LastName = @LastName, UserName = @UserName, PassWord = @PassWord, Department = @Department, position = @position, Title = @Title, Email = @Email WHERE ID = @ID";
            string delete = "DELETE FROM t_LM_User WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FirstName", SqlDbType.NVarChar, "FirstName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LastName", SqlDbType.NVarChar, "LastName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserName", SqlDbType.VarChar, "UserName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassWord", SqlDbType.VarChar, "PassWord", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Department", SqlDbType.NVarChar, "Department", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@position", SqlDbType.NVarChar, "position", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Title", SqlDbType.NVarChar, "Title", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Email", SqlDbType.NVarChar, "Email", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FirstName", SqlDbType.NVarChar, "FirstName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LastName", SqlDbType.NVarChar, "LastName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserName", SqlDbType.VarChar, "UserName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassWord", SqlDbType.VarChar, "PassWord", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Department", SqlDbType.NVarChar, "Department", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@position", SqlDbType.NVarChar, "position", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Title", SqlDbType.NVarChar, "Title", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Email", SqlDbType.NVarChar, "Email", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_LM_User VALUES(@FirstName, @LastName, @UserName, @PassWord, @Department, @position, @Title, @Email)";
            string update = "UPDATE t_LM_User SET FirstName = @FirstName, LastName = @LastName, UserName = @UserName, PassWord = @PassWord, Department = @Department, position = @position, Title = @Title, Email = @Email WHERE ID = @ID";
            string delete = "DELETE FROM t_LM_User WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@FirstName", SqlDbType.NVarChar, "FirstName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LastName", SqlDbType.NVarChar, "LastName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@UserName", SqlDbType.VarChar, "UserName", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PassWord", SqlDbType.VarChar, "PassWord", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Department", SqlDbType.NVarChar, "Department", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@position", SqlDbType.NVarChar, "position", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Title", SqlDbType.NVarChar, "Title", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Email", SqlDbType.NVarChar, "Email", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@FirstName", SqlDbType.NVarChar, "FirstName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LastName", SqlDbType.NVarChar, "LastName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@UserName", SqlDbType.VarChar, "UserName", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PassWord", SqlDbType.VarChar, "PassWord", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Department", SqlDbType.NVarChar, "Department", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@position", SqlDbType.NVarChar, "position", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Title", SqlDbType.NVarChar, "Title", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Email", SqlDbType.NVarChar, "Email", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static LM_User Load(int id)
		{
			const string spName = "[dbo].[p_LM_User_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<LM_User> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<LM_User> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<LM_User> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_LM_User_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_User_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_LM_User_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_LM_User_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertLM_User(string firstName, string lastName, string userName, string passWord, string department, string position, string title, string email)
		{
			LM_User entity = new LM_User();	
			entity.FirstName = firstName;
			entity.LastName = lastName;
			entity.UserName = userName;
			entity.PassWord = passWord;
			entity.Department = department;
			entity.position = position;
			entity.Title = title;
			entity.Email = email;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_LM_User_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@FirstName", SqlDbType.NVarChar, FirstName);
			db.AddInParameter(dbCommand, "@LastName", SqlDbType.NVarChar, LastName);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, UserName);
			db.AddInParameter(dbCommand, "@PassWord", SqlDbType.VarChar, PassWord);
			db.AddInParameter(dbCommand, "@Department", SqlDbType.NVarChar, Department);
			db.AddInParameter(dbCommand, "@position", SqlDbType.NVarChar, position);
			db.AddInParameter(dbCommand, "@Title", SqlDbType.NVarChar, Title);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.NVarChar, Email);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<LM_User> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_User item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateLM_User(int id, string firstName, string lastName, string userName, string passWord, string department, string position, string title, string email)
		{
			LM_User entity = new LM_User();			
			entity.ID = id;
			entity.FirstName = firstName;
			entity.LastName = lastName;
			entity.UserName = userName;
			entity.PassWord = passWord;
			entity.Department = department;
			entity.position = position;
			entity.Title = title;
			entity.Email = email;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_LM_User_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@FirstName", SqlDbType.NVarChar, FirstName);
			db.AddInParameter(dbCommand, "@LastName", SqlDbType.NVarChar, LastName);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, UserName);
			db.AddInParameter(dbCommand, "@PassWord", SqlDbType.VarChar, PassWord);
			db.AddInParameter(dbCommand, "@Department", SqlDbType.NVarChar, Department);
			db.AddInParameter(dbCommand, "@position", SqlDbType.NVarChar, position);
			db.AddInParameter(dbCommand, "@Title", SqlDbType.NVarChar, Title);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.NVarChar, Email);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<LM_User> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_User item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateLM_User(int id, string firstName, string lastName, string userName, string passWord, string department, string position, string title, string email)
		{
			LM_User entity = new LM_User();			
			entity.ID = id;
			entity.FirstName = firstName;
			entity.LastName = lastName;
			entity.UserName = userName;
			entity.PassWord = passWord;
			entity.Department = department;
			entity.position = position;
			entity.Title = title;
			entity.Email = email;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_User_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@FirstName", SqlDbType.NVarChar, FirstName);
			db.AddInParameter(dbCommand, "@LastName", SqlDbType.NVarChar, LastName);
			db.AddInParameter(dbCommand, "@UserName", SqlDbType.VarChar, UserName);
			db.AddInParameter(dbCommand, "@PassWord", SqlDbType.VarChar, PassWord);
			db.AddInParameter(dbCommand, "@Department", SqlDbType.NVarChar, Department);
			db.AddInParameter(dbCommand, "@position", SqlDbType.NVarChar, position);
			db.AddInParameter(dbCommand, "@Title", SqlDbType.NVarChar, Title);
			db.AddInParameter(dbCommand, "@Email", SqlDbType.NVarChar, Email);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<LM_User> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_User item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteLM_User(int id)
		{
			LM_User entity = new LM_User();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_LM_User_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_LM_User_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<LM_User> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (LM_User item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}