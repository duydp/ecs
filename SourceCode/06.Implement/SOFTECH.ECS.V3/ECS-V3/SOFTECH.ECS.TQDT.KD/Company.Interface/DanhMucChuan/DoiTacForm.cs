﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components.DuLieuChuan;

namespace Company.Interface.DanhMucChuan
{
    public partial class DonViDoiTacForm : Company.Interface.BaseForm
    {
        DoiTacCollection dsDoiTac = new DoiTacCollection();
        public DoiTac doiTac = new DoiTac();
        public bool isBrower = false;
        public DonViDoiTacForm()
        {
            InitializeComponent();
        }

        private void PTVTForm_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            dsDoiTac = doiTac.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'", "");
            dgList.DataSource = dsDoiTac;
        }
        private void uiButton3_Click(object sender, EventArgs e)
        {
            this.Close();
            doiTac = null;
        }
        private void uiButton2_Click(object sender, EventArgs e)
        {
            try
            {
                doiTac.InsertUpdate(dsDoiTac, GlobalSettings.MA_DON_VI);
                ShowMessage(setText("Cập nhật thành công.", "Save successfully"), false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi: " + ex.Message, false);
            }
        }

        private void dgList_DeletingRecords(object sender, CancelEventArgs e)
        {
            if (ShowMessage(setText("Bạn có muốn xóa các cửa khẩu này không?","Do you want to delete ?"),true) != "Yes") e.Cancel = true;
            else
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DoiTac dt = (DoiTac)i.GetRow().DataRow;
                        if (dt.ID > 0)
                        {
                            dt.Delete();
                        }
                    }
                }
            }
        }
        private bool CheckID(string Id)
        {
            foreach (DoiTac dt in dsDoiTac)
            {
                if (dt.MaCongTy.ToUpper().Trim() == Id.Trim().ToUpper())
                    return true;
            }
            return false;
        }
        private void dgList_UpdatingCell(object sender, Janus.Windows.GridEX.UpdatingCellEventArgs e)
        {
            if (e.Column.Key == "MaCongTy")
            {
                string s = e.Value.ToString();
                if (CheckID(s))
                {
                    ShowMessage(setText("Mã đối tác này đã có.", "This value is exist"), false);
                    e.Cancel = true;
                }
                if (s.Trim().Length == 0)
                {
                    ShowMessage(setText("Mã đối tác không được rỗng.","Parter code must be filled"), false);
                    e.Cancel = true;
                }
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (isBrower)
            {
                doiTac = new DoiTac();
                doiTac = (DoiTac)e.Row.DataRow;                
                this.Close();
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            DoiTacEditForm f = new DoiTacEditForm();
            f.ShowDialog(this);
            try
            {
                dsDoiTac = doiTac.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "'", "");
                dgList.DataSource = dsDoiTac;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                dgList.Refresh();
            }
       }

        private void btnReset_Click(object sender, EventArgs e)
        {
            LoadData();
        }
       
    }
}

