﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;

namespace Company.Interface.KDT
{
    public partial class HistoryDetailDinhMucForm : Company.Interface.BaseForm
    {
        public List<KDT_SXXK_DinhMuc_Log> DMCollection = new List<KDT_SXXK_DinhMuc_Log>();
        public HistoryDetailDinhMucForm()
        {
            InitializeComponent();
        }

        private void HistoryDetailDinhMucForm_Load(object sender, EventArgs e)
        {
            BindData();
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = DMCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["MaSanPham"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH ĐỊNH MỨC KHAI BÁO.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
