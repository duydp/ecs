using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using BarcodeLib;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.Report
{
    public partial class ReportViewNoticeOfPayment : DevExpress.XtraReports.UI.XtraReport
    {
        public T_KDT_THUPHI_THONGBAO TB = new T_KDT_THUPHI_THONGBAO();
        public T_KDT_THUPHI_TOKHAI TK = new T_KDT_THUPHI_TOKHAI();
        public ReportViewNoticeOfPayment()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = true,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 55,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, Text);
            return img;
        }
        public void BindReport()
        {
            try
            {
                txtSoTiepNhan.Text = TB.SoThongBao;
                txtNgayTiepNhan.Text = TB.NgayThongBao.ToString("dd/MM/yyyy");
                picBarCode.Image = GenerateBarCode(TB.SoThongBao);

                txtSoTK.Text = TK.SoTK;
                txtNgayTK.Text = TK.NgayTK.ToString("dd/MM/yyyy");
                txtMaLH.Text = TK.MaLoaiHinh;
                txtMaLH.Text = TK.MaHQ;

                txtSoTKNP.Text = TK.SoTKNP;
                txtNgayTKNP.Text = TK.NgayTKNP.ToString("dd/MM/yyyy");
                txtLoaiHH.Text = TK.LoaiHangHoa == 1 ? "" : "";
                txtNhomHH.Text = TK.NhomLoaiHinh == "" ? "" : "";

                this.DataSource = TB.PhiCollection;
                txtSoTT.DataBindings.Add("Text",this.DataSource,"STT");
                txtNoiDung.DataBindings.Add("Text", this.DataSource, "TenKhoanMuc");
                txtDVT.DataBindings.Add("Text", this.DataSource, "TenDVT");
                txtSoLuong.DataBindings.Add("Text", this.DataSource, "SoLuongOrTrongLuong");
                txtDonGia.DataBindings.Add("Text", this.DataSource, "DonGia");
                txtThanhTien.DataBindings.Add("Text", this.DataSource, "ThanhTien");
                txtTongTien.DataBindings.Add("Text", this.DataSource, "txtTongTien");

                lblTongTien.Text = ChuyenSo(txtTongTien.Text.ToString());
                lblTongTien.Text = UppercaseFirst(lblTongTien.Text);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        static string UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }
        public static string ChuyenSo(string number)
        {
            string[] strTachPhanSauDauPhay;
            if (number.Contains(".") || number.Contains(","))
            {
                strTachPhanSauDauPhay = number.Split(',', '.');
                return (ChuyenSo(strTachPhanSauDauPhay[0]) + "phẩy " + ChuyenSo(strTachPhanSauDauPhay[1]) + "đồng");
            }

            string[] dv = { "", "mươi", "trăm", "nghìn", "triệu", "tỉ" };
            string[] cs = { "không", "một", "hai", "ba", "bốn", "năm", "sáu", "bảy", "tám", "chín" };
            string doc;
            int i, j, k, n, len, found, ddv, rd;

            len = number.Length;
            number += "ss";
            doc = "";
            found = 0;
            ddv = 0;
            rd = 0;

            i = 0;
            while (i < len)
            {
                //So chu so o hang dang duyet
                n = (len - i + 2) % 3 + 1;

                //Kiem tra so 0
                found = 0;
                for (j = 0; j < n; j++)
                {
                    if (number[i + j] != '0')
                    {
                        found = 1;
                        break;
                    }
                }

                //Duyet n chu so
                if (found == 1)
                {
                    rd = 1;
                    for (j = 0; j < n; j++)
                    {
                        ddv = 1;
                        switch (number[i + j])
                        {
                            case '0':
                                if (n - j == 3) doc += cs[0] + " ";
                                if (n - j == 2)
                                {
                                    if (number[i + j + 1] != '0') doc += "linh ";
                                    ddv = 0;
                                }
                                break;
                            case '1':
                                if (n - j == 3) doc += cs[1] + " ";
                                if (n - j == 2)
                                {
                                    doc += "mười ";
                                    ddv = 0;
                                }
                                if (n - j == 1)
                                {
                                    if (i + j == 0) k = 0;
                                    else k = i + j - 1;

                                    if (number[k] != '1' && number[k] != '0')
                                        doc += "mốt ";
                                    else
                                        doc += cs[1] + " ";
                                }
                                break;
                            case '5':
                                if ((i + j == len - 1) || (i + j + 3 == len - 1))
                                    doc += "lăm ";
                                else
                                    doc += cs[5] + " ";
                                break;
                            default:
                                doc += cs[(int)number[i + j] - 48] + " ";
                                break;
                        }

                        //Doc don vi nho
                        if (ddv == 1)
                        {
                            doc += ((n - j) != 1) ? dv[n - j - 1] + " " : dv[n - j - 1];
                        }
                    }
                }


                //Doc don vi lon
                if (len - i - n > 0)
                {
                    if ((len - i - n) % 9 == 0)
                    {
                        if (rd == 1)
                            for (k = 0; k < (len - i - n) / 9; k++)
                                doc += "tỉ ";
                        rd = 0;
                    }
                    else
                        if (found != 0) doc += dv[((len - i - n + 1) % 9) / 3 + 2] + " ";
                }

                i += n;
            }

            if (len == 1)
                if (number[0] == '0' || number[0] == '5') return cs[(int)number[0] - 48];

            return doc;
        }
    }
}
