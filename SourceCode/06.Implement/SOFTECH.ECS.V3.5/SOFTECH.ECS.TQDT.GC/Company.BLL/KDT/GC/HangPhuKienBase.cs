using System;
using System.Data;
using System.Data.SqlClient;

namespace Company.GC.BLL.KDT.GC
{
    public partial class HangPhuKien : EntityBase
    {
        #region Private members.

        protected long _ID;
        protected string _MaHang = String.Empty;
        protected string _TenHang = String.Empty;
        protected string _MaHS = String.Empty;
        protected string _ThongTinCu = String.Empty;
        protected decimal _SoLuong;
        protected double _DonGia;
        protected string _DVT_ID = String.Empty;
        protected string _NuocXX_ID = String.Empty;
        protected double _TriGia;
        protected string _NguyenTe_ID = String.Empty;
        protected int _STTHang;
        protected string _TinhTrang = String.Empty;
        protected long _Master_ID;
        protected string _NhomSP = String.Empty;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public long ID
        {
            set { this._ID = value; }
            get { return this._ID; }
        }
        public string MaHang
        {
            set { this._MaHang = value; }
            get { return this._MaHang; }
        }
        public string TenHang
        {
            set { this._TenHang = value; }
            get { return this._TenHang; }
        }
        public string MaHS
        {
            set { this._MaHS = value; }
            get { return this._MaHS; }
        }
        public string ThongTinCu
        {
            set { this._ThongTinCu = value; }
            get { return this._ThongTinCu; }
        }
        public decimal SoLuong
        {
            set { this._SoLuong = value; }
            get { return this._SoLuong; }
        }
        public double DonGia
        {
            set { this._DonGia = value; }
            get { return this._DonGia; }
        }
        public string DVT_ID
        {
            set { this._DVT_ID = value; }
            get { return this._DVT_ID; }
        }
        public string NuocXX_ID
        {
            set { this._NuocXX_ID = value; }
            get { return this._NuocXX_ID; }
        }
        public double TriGia
        {
            set { this._TriGia = value; }
            get { return this._TriGia; }
        }
        public string NguyenTe_ID
        {
            set { this._NguyenTe_ID = value; }
            get { return this._NguyenTe_ID; }
        }
        public int STTHang
        {
            set { this._STTHang = value; }
            get { return this._STTHang; }
        }
        public string TinhTrang
        {
            set { this._TinhTrang = value; }
            get { return this._TinhTrang; }
        }
        public long Master_ID
        {
            set { this._Master_ID = value; }
            get { return this._Master_ID; }
        }
        public string NhomSP
        {
            set { this._NhomSP = value; }
            get { return this._NhomSP; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_KDT_GC_HangPhuKien_Load";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            IDataReader reader = this.db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) this._ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) this._MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) this._TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) this._MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) this._ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) this._SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) this._DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) this._DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) this._NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) this._TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) this._NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) this._STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) this._TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) this._Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSP"))) this._NhomSP = reader.GetString(reader.GetOrdinal("NhomSP"));
                reader.Close();
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public HangPhuKienCollection SelectCollectionBy_Master_ID()
        {
            string spName = "p_KDT_GC_HangPhuKien_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            HangPhuKienCollection collection = new HangPhuKienCollection();
            IDataReader reader = this.db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                HangPhuKien entity = new HangPhuKien();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) entity.ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSP"))) entity.NhomSP = reader.GetString(reader.GetOrdinal("NhomSP"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_Master_ID()
        {
            string spName = "p_KDT_GC_HangPhuKien_SelectBy_Master_ID";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);

            return this.db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_KDT_GC_HangPhuKien_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_KDT_GC_HangPhuKien_SelectAll";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_GC_HangPhuKien_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_KDT_GC_HangPhuKien_SelectDynamic";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            this.db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return this.db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public HangPhuKienCollection SelectCollectionAll()
        {
            HangPhuKienCollection collection = new HangPhuKienCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                HangPhuKien entity = new HangPhuKien();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) entity.ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSP"))) entity.NhomSP = reader.GetString(reader.GetOrdinal("NhomSP"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public HangPhuKienCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            HangPhuKienCollection collection = new HangPhuKienCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                HangPhuKien entity = new HangPhuKien();

                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHS"))) entity.MaHS = reader.GetString(reader.GetOrdinal("MaHS"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThongTinCu"))) entity.ThongTinCu = reader.GetString(reader.GetOrdinal("ThongTinCu"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("DonGia"))) entity.DonGia = reader.GetDouble(reader.GetOrdinal("DonGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("DVT_ID"))) entity.DVT_ID = reader.GetString(reader.GetOrdinal("DVT_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NuocXX_ID"))) entity.NuocXX_ID = reader.GetString(reader.GetOrdinal("NuocXX_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TriGia"))) entity.TriGia = reader.GetDouble(reader.GetOrdinal("TriGia"));
                if (!reader.IsDBNull(reader.GetOrdinal("NguyenTe_ID"))) entity.NguyenTe_ID = reader.GetString(reader.GetOrdinal("NguyenTe_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("STTHang"))) entity.STTHang = reader.GetInt32(reader.GetOrdinal("STTHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TinhTrang"))) entity.TinhTrang = reader.GetString(reader.GetOrdinal("TinhTrang"));
                if (!reader.IsDBNull(reader.GetOrdinal("Master_ID"))) entity.Master_ID = reader.GetInt64(reader.GetOrdinal("Master_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("NhomSP"))) entity.NhomSP = reader.GetString(reader.GetOrdinal("NhomSP"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_HangPhuKien_Insert";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.NVarChar, this._ThongTinCu);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, this._DonGia);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, this._TriGia);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, this._TinhTrang);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@NhomSP", SqlDbType.VarChar, this._NhomSP);

            if (transaction != null)
            {
                this.db.ExecuteNonQuery(dbCommand, transaction);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
            else
            {
                this.db.ExecuteNonQuery(dbCommand);
                this._ID = (long)this.db.GetParameterValue(dbCommand, "@ID");
                return this._ID;
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(HangPhuKienCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangPhuKien item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, HangPhuKienCollection collection)
        {
            foreach (HangPhuKien item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_HangPhuKien_InsertUpdate";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);
            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.NVarChar, this._ThongTinCu);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, this._DonGia);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, this._TriGia);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, this._TinhTrang);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@NhomSP", SqlDbType.VarChar, this._NhomSP);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(HangPhuKienCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangPhuKien item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_HangPhuKien_Update";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);
            this.db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, this._MaHang);
            this.db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, this._TenHang);
            this.db.AddInParameter(dbCommand, "@MaHS", SqlDbType.VarChar, this._MaHS);
            this.db.AddInParameter(dbCommand, "@ThongTinCu", SqlDbType.NVarChar, this._ThongTinCu);
            this.db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, this._SoLuong);
            this.db.AddInParameter(dbCommand, "@DonGia", SqlDbType.Float, this._DonGia);
            this.db.AddInParameter(dbCommand, "@DVT_ID", SqlDbType.Char, this._DVT_ID);
            this.db.AddInParameter(dbCommand, "@NuocXX_ID", SqlDbType.Char, this._NuocXX_ID);
            this.db.AddInParameter(dbCommand, "@TriGia", SqlDbType.Float, this._TriGia);
            this.db.AddInParameter(dbCommand, "@NguyenTe_ID", SqlDbType.Char, this._NguyenTe_ID);
            this.db.AddInParameter(dbCommand, "@STTHang", SqlDbType.Int, this._STTHang);
            this.db.AddInParameter(dbCommand, "@TinhTrang", SqlDbType.NVarChar, this._TinhTrang);
            this.db.AddInParameter(dbCommand, "@Master_ID", SqlDbType.BigInt, this._Master_ID);
            this.db.AddInParameter(dbCommand, "@NhomSP", SqlDbType.VarChar, this._NhomSP);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(HangPhuKienCollection collection, SqlTransaction transaction)
        {
            foreach (HangPhuKien item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_KDT_GC_HangPhuKien_Delete";
            SqlCommand dbCommand = (SqlCommand)this.db.GetStoredProcCommand(spName);

            this.db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, this._ID);

            if (transaction != null)
                return this.db.ExecuteNonQuery(dbCommand, transaction);
            else
                return this.db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(HangPhuKienCollection collection, SqlTransaction transaction)
        {
            foreach (HangPhuKien item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(HangPhuKienCollection collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (HangPhuKien item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion
    }
}