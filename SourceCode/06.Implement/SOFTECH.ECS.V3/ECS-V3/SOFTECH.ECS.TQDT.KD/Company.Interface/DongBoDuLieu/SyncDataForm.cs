﻿
using System;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Janus.Windows.UI.CommandBars;
using InheritableBoolean = Janus.Windows.UI.InheritableBoolean;
#if KD_V3
using Company.Interface.SXXK;
using Company.KD.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KD.BLL;
using Company.KD.BLL.KDT;
#elif GC_V3
using Company.Interface.GC;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.GC;
#elif SXXK_V3
using Company.BLL;
using Company.BLL.KDT;
#endif
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3;
using System.Threading;
using KetQuaXuLy = Company.KDT.SHARE.QuanLyChungTu.DongBoDuLieu_V3.KetQuaXuLy;
using Company.KDT.SHARE.QuanLyChungTu;
using DevExpress.Data.Utils;


namespace Company.Interface.DongBoDuLieu
{
    public partial class SyncDataForm : BaseForm
    {
        List<ToKhaiMauDich> tokhaiSelect = new List<ToKhaiMauDich>();
        List<Detail> detailSelect = new List<Detail>();
        string msgSend = string.Empty;
#if (KD_V3) || (SXXK_V3)
        private string PhanHe = "KD";
        private void searchDaiLy()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "1 = 1";
            where += string.Format(" AND MaHaiQuan = '{0}' and MaDoanhNghiep='{1}'", GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            if (cbbLoaiToKhai.SelectedValue != null)
                where += " AND MaLoaiHinh LIKE '" + cbbLoaiToKhai.SelectedValue.ToString() + "%'";
            if (!string.IsNullOrEmpty(txtSoToKhai.Text))
            {
                where += " AND SoToKhai = " + txtSoToKhai.Value;
            }
            //if (txtNamTiepNhan.TextLength > 0)
            //{
            //    where += " AND NamDK = " + txtNamTiepNhan.Value;
            //}
            if (ccFromDate.Value < ccToDate.Value)
            {
                where += "AND (NgayDangKy BETWEEN '" + ccFromDate.Value.ToString("yyyy-MM-dd") + "' AND '" + ccToDate.Value.ToString("yyyy-MM-dd") + "')";
            }
            if (Convert.ToInt16(txtThangDK.Value) > 0)
            {
                where += " AND (month(NgayDangKy)=" + Convert.ToInt16(txtThangDK.Value) + ")";
            }
            if (GlobalSettings.MA_DON_VI != "0400101556")
            {
                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                        where += " AND TenChuHang LIKE '%" + txtTenChuHang.Text + "%'";
                }
                else
                {
                    where += " AND TenChuHang LIKE ''";
                }
            }
            else
            {
                label5.Text = "Tên khách hàng";
                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    where += " AND TenDonViDoiTac LIKE '%" + txtTenChuHang.Text + "%'";
                }
            }
            where += "AND TrangThaiXuLy = 1 AND HUONGDAN<>''";
            if (!chkSeach.Checked)
                where += "AND MSG_STATUS IS NULL";
            
            // Thực hiện tìm kiếm.            
            ToKhaiMauDichCollection tkmdCollection = new  ToKhaiMauDich().SelectCollectionDynamicDongBoDuLieu(where, "");
            if (dgList.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { dgList.DataSource = tkmdCollection; }));
            }
            else
            {
                if (tkmdCollection == null || tkmdCollection.Count == 0)
                    ShowMessage("Không tìm thấy tờ khai nào chưa được đồng bộ dữ liệu", false);
                dgList.DataSource = tkmdCollection;
            }
            
        }
#elif SXXK_V3
        private string PhanHe = "SXXK";
#elif GC_V3
        private string PhanHe = "GC";
        private List<ToKhaiChuyenTiep> ListToKhai = new List<ToKhaiChuyenTiep>();
        private void searchDaiLy()
        {
            bool isToKhaiCT = PhanHe == "GC" && cbbLoaiToKhai.SelectedValue == "CT";
            // Xây dựng điều kiện tìm kiếm.
            string where = "1 = 1";
            if (isToKhaiCT)
                where += string.Format(" AND MaHaiQuanTiepNhan = '{0}' and MaDoanhNghiep='{1}'", donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI);
            else
                where += string.Format(" AND MaHaiQuan = '{0}' and MaDoanhNghiep='{1}'", donViHaiQuanControl1.Ma, GlobalSettings.MA_DON_VI);
            if (cbbLoaiToKhai.SelectedValue != null && cbbLoaiToKhai.SelectedIndex < 2 && !string.IsNullOrEmpty(cbbLoaiToKhai.SelectedValue.ToString()))
                where += " AND MaLoaiHinh LIKE '" + cbbLoaiToKhai.SelectedValue.ToString() + "%'";
            if (txtSoToKhai.TextLength > 0)
            {
                where += " AND SoToKhai = " + txtSoToKhai.Value;
            }
            if (ccFromDate.Value < ccToDate.Value)
            {
                where += "AND (NgayDangKy BETWEEN '" + ccFromDate.Value.ToString("yyyy-MM-dd") + "' AND '" + ccToDate.Value.ToString("yyyy-MM-dd") + "')";
            }
            if (txtThangDK.Value != null && Convert.ToInt16(txtThangDK.Value) > 0)
            {
                where += " AND (month(NgayDangKy)=" + Convert.ToInt16(txtThangDK.Value) + ")";
            }
            if (GlobalSettings.MA_DON_VI != "0400101556")
            {
                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    if (txtTenChuHang.Text.Trim() != "*")
                        where += " AND TenChuHang LIKE '%" + txtTenChuHang.Text + "%'";
                }
                else
                {
                    where += " AND TenChuHang LIKE ''";
                }
            }
            else
            {
                label5.Text = "Tên khách hàng";
                if (txtTenChuHang.Text.Trim().Length > 0)
                {
                    where += " AND TenDonViDoiTac LIKE '%" + txtTenChuHang.Text + "%'";
                }
            }
            where += "AND TrangThaiXuLy = 1 AND HUONGDAN<>''";
            if (!chkSeach.Checked)
                where += "AND MSG_STATUS IS NULL";
            if (isToKhaiCT)
            {
                ToKhaiChuyenTiep tkct = new ToKhaiChuyenTiep();
                List<ToKhaiChuyenTiep> tkCollection = tkct.SelectCollectionDynamicDongBoDuLieu(where, null);

                if (dgList.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { dgList.DataSource = tkCollection;}));
                }
                else
                {
                    if (tkCollection == null || tkCollection.Count == 0)
                        ShowMessage("Không tìm thấy tờ khai nào chưa được đồng bộ dữ liệu", false);

                    dgList.DataSource = tkCollection;
                }
            }
            else
            {
                ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDich().SelectCollectionDynamicDongBoDuLieu(where, "");
                if (dgList.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { dgList.DataSource = tkmdCollection; }));
                }
                else
                {
                    if (tkmdCollection == null || tkmdCollection.Count == 0)
                        ShowMessage("Không tìm thấy tờ khai nào chưa được đồng bộ dữ liệu", false);

                    dgList.DataSource = tkmdCollection;
                }
            }
        }
#endif
        public SyncDataForm()
        {
            InitializeComponent();
        }
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {

                DateTime time = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                e.Row.Cells["MaLoaiHinh"].Text = this.LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value.ToString().Trim());
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + donViHaiQuanControl1.Ma + "/" + time.Year.ToString();
                if (GlobalSettings.IsDaiLy)
                {
#if GC_V3
                    if (cbbLoaiToKhai.SelectedIndex == 2)
                    {
                        ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)e.Row.DataRow;
                        if (tkct!=null && tkct.TrangThaiDongBo == 1)
                            e.Row.Cells["TrangThaiGui"].Text = "Đã gửi";
                        else
                            e.Row.Cells["TrangThaiGui"].Text = "Chưa gửi";
                        e.Row.Cells["MaHaiQuan"].Text = tkct.MaHaiQuanTiepNhan;
                    }
#endif
                    if (cbbLoaiToKhai.SelectedIndex != 2)
                    {
                        ToKhaiMauDich tkmd = (ToKhaiMauDich)e.Row.DataRow;
                        if (tkmd != null && tkmd.TrangThaiDongBo == 1)
                        {
                            e.Row.Cells["TrangThaiGui"].Text = "Đã gửi";
                        }
                        else
                            e.Row.Cells["TrangThaiGui"].Text = "Chưa gửi";
                    }
                }
                else
                {
                    Detail chitiet = (Detail)e.Row.DataRow;
                    if (chitiet.TrangThai == 1)
                        e.Row.Cells["TrangThaiGui"].Text = "Đã Nhận";
                    else
                        e.Row.Cells["TrangThaiGui"].Text = "Chưa Nhận";
                    e.Row.Cells["HUONGDAN"].Text = chitiet.PhanLuong;
                    e.Row.Cells["MaDoanhNghiep"].Text = chitiet.MaDaiLy;
                }
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (!this.InvokeRequired)
            {
                if (GlobalSettings.IsDaiLy)
                    searchDaiLy();
                else SearchDoanhNghiep();
            }
                
        }
        private void SearchDoanhNghiep()
        {
            if (string.IsNullOrEmpty(GlobalSettings.USERNAME_DONGBO) || string.IsNullOrEmpty(GlobalSettings.PASSWOR_DONGBO))
            {
                WSDBDLForm f = new WSDBDLForm();
                f.ShowDialog();
                if (!f.IsReady) return;
                user = f.txtMaDoanhNghiep.Text;
                password = f.txtMatKhau.Text;
            }
            else
            {
                user = GlobalSettings.USERNAME_DONGBO;
                password = GlobalSettings.PASSWOR_DONGBO;
            }
            password = Company.KDT.SHARE.Components.Helpers.GetMD5Value(password);
            ThreadPool.QueueUserWorkItem(DoSearchData);

        }
        private void SyncDataForm_Load(object sender, EventArgs e)
        {
            ccToDate.Text = DateTime.Now.AddDays(1).ToLongDateString();
            ccFromDate.Text = DateTime.Now.AddMonths(-1).ToLongDateString();
            btnNhan.Enabled = !GlobalSettings.IsDaiLy;
            btnGui.Enabled = GlobalSettings.IsDaiLy;
            if (PhanHe == "GC")
            {
                cbbLoaiToKhai.Items.Add(new Janus.Windows.EditControls.UIComboBoxItem
                {
                    Text = "Tờ khai chuyển tiếp",
                    Value = "CT"
                });
            }
            if (GlobalSettings.IsDaiLy)
            {
                chkSeach.Text = "Tìm kiếm tất cả các tờ khai đã gửi";
            }
            else
                chkSeach.Text = "Tìm kiếm tất cả các tờ khai đã nhận";
        }
        private void SaveLogError(string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.GhiChu = error;
            logError.NgayDongBo = DateTime.Now;
            logError.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            logError.SoToKhai = 0;
            logError.TrangThai = 0;
            logError.InsertUpdate();
        }
        private void SaveLogError(Detail chitiet, string error)
        {
            KetQuaXuLy logError = new KetQuaXuLy();
            logError.SoToKhai = chitiet.SoToKhai;
            logError.MaDoanhNghiep = chitiet.MaDoanhNghiep;
            logError.NgayDongBo = DateTime.Now;
            logError.GhiChu = error;
            logError.GUIDSTR = chitiet.GUIDSTR;
            logError.TrangThai = 2;
            logError.InsertUpdate();
        }
        private void btnKetQuaXuLy_Click(object sender, EventArgs e)
        {
            KetQuaXuLyForm f = new KetQuaXuLyForm();
            f.ShowDialog();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
#if KD_V3
        private void DoSendData(object obj)
        {
            SyncData syncData = new SyncData();
            syncData.SyncEventArgs += new EventHandler<SyncEventArgs>(syncData_SyncEventArgs);
            syncData.KD_SendData(tokhaiSelect, user, password);
            if (GlobalSettings.SOTOKHAI_DONGBO > 0 && GlobalSettings.MA_DON_VI != null)
            {
                GlobalSettings.ISKHAIBAO = (GlobalSettings.SOTOKHAI_DONGBO > new ToKhaiMauDich().SelectCountSoTKThongQuan(GlobalSettings.MA_DON_VI));
            }
            this.searchDaiLy();
        }
        private void DoSearchData(object obj)
        {
            SyncData syncData = new SyncData();
            syncData.SyncEventArgs += new EventHandler<SyncEventArgs>(syncData_SyncEventArgs);
            syncData.KD_SearchData(user, password, ccFromDate.Value, ccToDate.Value, chkSeach.Checked);
        }
        private void GetData(object obj)
        {

            SyncData syncData = new SyncData();
            syncData.SyncEventArgs += new EventHandler<SyncEventArgs>(syncData_SyncEventArgs);
            syncData.KD_GetData(user, password, detailSelect);
            syncData.KD_SearchData(user, password, ccFromDate.Value, ccToDate.Value, chkSeach.Checked);
        }
#elif GC_V3
        private void DoSendData(object obj)
        {
            SyncData syncData = new SyncData();
            syncData.SyncEventArgs += new EventHandler<SyncEventArgs>(syncData_SyncEventArgs);
            if (cbbLoaiToKhai.SelectedIndex == 2)
                syncData.GC_SendData_GCCT(ListToKhai, user, password);
            else
                syncData.GC_SendData(tokhaiSelect, user, password);
        if (GlobalSettings.SOTOKHAI_DONGBO > 0 && GlobalSettings.MA_DON_VI != null)
            {
                GlobalSettings.ISKHAIBAO = (GlobalSettings.SOTOKHAI_DONGBO > new ToKhaiMauDich().SelectCountSoTKThongQuan(GlobalSettings.MA_DON_VI));
            }
        this.searchDaiLy();
        }
        private void DoSearchData(object obj)
        {
            SyncData syncData = new SyncData();
            syncData.SyncEventArgs += new EventHandler<SyncEventArgs>(syncData_SyncEventArgs);
            syncData.GC_SearchData(user, password, ccFromDate.Value, ccToDate.Value, chkSeach.Checked);
        }
        private void GetData(object obj)
        {

            SyncData syncData = new SyncData();
            syncData.SyncEventArgs += new EventHandler<SyncEventArgs>(syncData_SyncEventArgs);
            syncData.GC_GetData(user, password, detailSelect);
            syncData.GC_SearchData(user, password, ccFromDate.Value, ccToDate.Value, chkSeach.Checked);
        }
#elif SXXK_V3
        private void DoSendData(object obj)
        {
            SyncData syncData = new SyncData();
            syncData.SyncEventArgs += new EventHandler<SyncEventArgs>(syncData_SyncEventArgs);
            syncData.SXXK_SendData(tokhaiSelect, user, password);
            if (GlobalSettings.SOTOKHAI_DONGBO > 0 && GlobalSettings.MA_DON_VI != null)
            {
                GlobalSettings.ISKHAIBAO = (GlobalSettings.SOTOKHAI_DONGBO > new ToKhaiMauDich().SelectCountSoTKThongQuan(GlobalSettings.MA_DON_VI));
            }
            this.searchDaiLy();
        }
        private void DoSearchData(object obj)
        {
            SyncData syncData = new SyncData();
            syncData.SyncEventArgs += new EventHandler<SyncEventArgs>(syncData_SyncEventArgs);
            syncData.SXXK_SearchData(user, password, ccFromDate.Value, ccToDate.Value, chkSeach.Checked);
        }
        private void GetData(object obj)
        {

            SyncData syncData = new SyncData();
            syncData.SyncEventArgs += new EventHandler<SyncEventArgs>(syncData_SyncEventArgs);
            syncData.SXXK_GetData(user, password, detailSelect);
            syncData.SXXK_SearchData(user, password, ccFromDate.Value, ccToDate.Value, chkSeach.Checked);
        }
#endif
        void syncData_SyncEventArgs(object sender, SyncEventArgs e)
        {
            this.Invoke(
               new EventHandler<SyncEventArgs>(SyncHandler),
               sender, e);
        }
        void SyncHandler(object sender, SyncEventArgs e)
        {
            if (e.Error == null)
            {
                lbStatusInfo.Text = e.Message;
                uiStatusBar.Panels[0].ProgressBarValue = e.Percent;
                if (e.Data != null)
                {
                    if (e.Data.GetType() == typeof(List<Detail>))
                    {
                        dgList.DataSource = e.Data;
                    }
                }
            }
            else lbStatusInfo.Text = e.Error.Message;
        }
        string user = string.Empty;
        string password = string.Empty;
        private void btnGui_Click(object sender, EventArgs e)
        {

            //bool isOveride = false;
            GridEXRow[] items = dgList.GetCheckedRows();
            tokhaiSelect = new List<ToKhaiMauDich>();
            if (items.Length == 0)
            {
                ShowMessage("Chưa chọn thông tin tờ khai", false);
                return;
            }
            foreach (GridEXRow item in items)
            {

#if GC_V3
                if (ListToKhai != null)
                    ListToKhai = new List<ToKhaiChuyenTiep>();
                if (cbbLoaiToKhai.SelectedIndex == 2)
                {
                    ToKhaiChuyenTiep tkct = (ToKhaiChuyenTiep)item.DataRow;
                    ListToKhai.Add(tkct);
                }
#endif
                if (cbbLoaiToKhai.SelectedIndex != 2)
                {
                    ToKhaiMauDich tkmd = (ToKhaiMauDich)item.DataRow;
                    tokhaiSelect.Add(tkmd);
                }

            }
            if (string.IsNullOrEmpty(GlobalSettings.USERNAME_DONGBO) || string.IsNullOrEmpty(GlobalSettings.PASSWOR_DONGBO))
            {
                WSDBDLForm f = new WSDBDLForm();
                f.ShowDialog();
                if (!f.IsReady) return;
                user = f.txtMaDoanhNghiep.Text;
                password = f.txtMatKhau.Text;
            }
            else
            {
                user = GlobalSettings.USERNAME_DONGBO;
                password = GlobalSettings.PASSWOR_DONGBO;
            }
            password = Company.KDT.SHARE.Components.Helpers.GetMD5Value(password);
            ThreadPool.QueueUserWorkItem(DoSendData);
            
        }

        private void btnNhan_Click(object sender, EventArgs e)
        {

            GridEXRow[] items = dgList.GetCheckedRows();
            if (items.Length == 0)
            {
                ShowMessage("Chưa chọn thông tin tờ khai", false);
                return;
            }
            detailSelect = new List<Detail>();
            foreach (GridEXRow item in items)
            {
                Detail detail = (Detail)item.DataRow;
                detailSelect.Add(detail);
            }
            ThreadPool.QueueUserWorkItem(GetData);
        }
    }
}
