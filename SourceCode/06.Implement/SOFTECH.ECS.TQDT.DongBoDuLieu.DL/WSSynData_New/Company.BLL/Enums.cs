using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.Components;
namespace Company.BLL
{
    //[Serializable]
    //public class TrangThaiXuLy
    //{
    //    public static readonly int CHUA_KHAI_BAO = -1;
    //    public static readonly int CHO_DUYET = 0;
    //    public static readonly int DA_DUYET = 1;
    //    public static readonly int KHONG_PHE_DUYET = 2;
    //    public static readonly int CHO_DUYET_DASUACHUA = -2;
    //}
    //public class TrangThaiPhanLuong
    //{
    //    public static readonly string LUONG_XANH = "1";
    //    public static readonly string LUONG_VANG = "2";
    //    public static readonly string LUONG_DO = "3";

    //}

    //public enum OpenFormType
    //{
    //    View,
    //    Insert,
    //    Edit,
    //    EditAll
    //}

    [Serializable]
    public class MessgaseType
    {
        public static readonly int ToKhaiNhap = 1;
        public static readonly int ToKhaiXuat = 2;
        public static readonly int ToKhaiChuyenTiepNhap = 3;
        public static readonly int ToKhaiChuyenTiepXuat = 4;
        public static readonly int DanhMucNguyenPhuLieu = 5;
        public static readonly int DanhMucSanPham = 6;
        public static readonly int HopDong = 7;
        public static readonly int PhuKien = 8;
        public static readonly int DinhMuc = 9;
        public static readonly int DinhMucCungUng = 10;
        public static readonly int ThongTin = 11;
        public static readonly int HoSoThanhKhoanSXXK = 12;
    }
    [Serializable]
    public class MessgaseFunction
    {
        public static readonly int KhaiBao = 1;
        public static readonly int HuyKhaiBao = 2;
        public static readonly int HoiTrangThai = 3;
        public static readonly int LayThongBaoHQ = 4;
        public static readonly int LayPhanHoi = 5;
        public static readonly int PhanHoi = 6;
        public static readonly int DoiMatKhau = 7;
    }

    public class LoaiPhuKien
    {
        public static readonly string BO_SUNG_DANH_MUC_NGUYEN_PHU_LIEU_NHAP_KHAU = "N01";
        public static readonly string MUA_NGUYEN_PHU_LIEU_TAI_THI_TRUONG_VIET_NAM = "N11";
    }
    //[Serializable]
    //public class LoaiToKhai
    //{
    //    public static readonly string TO_KHAI_MAU_DICH = "Tờ khai mậu dịch";
    //    public static readonly string TO_KHAI_CHUYEN_TIEP = "Tờ khai chuyển tiếp";
    //    public static readonly string DANH_MUC_NGUYEN_PHU_LIEU = "Danh mục nguyên phụ liệu";
    //    public static readonly string DANH_MUC_SAN_PHAM = "Danh mục sản phẩm";
    //    public static readonly string DINH_MUC = "Tờ khai định mức";
    //    public static readonly string HOP_DONG = "Tờ khai hợp đồng gia công";
    //    public static readonly string PHU_KIEN = "Phụ kiện hợp đồng gia công";
    //    public static readonly string DINH_MUC_HOP_DONG = "Định mức hợp đồng gia công";
    //    public static readonly string HO_SO_THANH_KHOAN = "Hồ sơ thanh khoản";
    //    public static readonly string DIEU_CHINH_THUE = "Điều chỉnh thuế";
    //}
    //[Serializable]
    //public class ChucNang
    //{
    //    public static readonly string KHAI_BAO = "Khai báo";//1
    //    public static readonly string NHAN_THONG_TIN = "Nhận thông tin";//2
    //    public static readonly string HUY_KHAI_BAO = "Hủy khai báo";       //3 
    //    public static readonly string DONG_BO_DU_LIEU = "Đồng bộ dữ liệu với hải quan";//4
    //    public static readonly string XAC_NHAN_THONG_TIN = "Xác nhận thông tin";//5
    //}
    //[Serializable]
    //public class LAY_THONG_TIN
    //{
    //    public static readonly string DINH_MUC = "LAY_TT_DM";
    //    public static readonly string NPL = "LAY_TT_NPL";
    //    public static readonly string SP = "LAY_TT_SP";
    //    public static readonly string TOKHAI = "LAY_TT_TK";
    //}

    //[Serializable]
    //public class THONG_TIN_DANG_KY
    //{
    //    public static readonly string DINH_MUC = "DANG_KY_DM";
    //    public static readonly string NPL = "DANG_KY_NPL";
    //    public static readonly string SP = "DANG_KY_SP";
    //    public static readonly string TOKHAINHAP = "DANG_KY_TKN";
    //    public static readonly string TOKHAIXUAT = "DANG_KY_TKX";
    //    public static readonly string CAPNHATTOKHAINHAP = "CAP_NHAT_TKN";
    //    public static readonly string CAPNHATTOKHAIXUAT = "CAP_NHAT_TKX";
    //    public static readonly string HOSOTHANHLY = "DANG_KY_TL_HO_SO";
    //    public static readonly string BK_TOKHAINHAP = "DANG_KY_TL_DSTKN";
    //    public static readonly string BK_TOKHAIXUAT = "DANG_KY_TL_DSTKX";
    //    public static readonly string BK_TAM_NOP_THUE = "DANG_KY_TL_CHI_TIET_NT";
    //    public static readonly string BK_CHUA_THANH_LY = "DANG_KY_TL_NPL_CHUA_TL";
    //    public static readonly string BK_TL_NKD = "DANG_KY_TL_NPL_NKD";
    //    public static readonly string BK_XUAT_GC = "DANG_KY_TL_NPL_XGC";
    //    public static readonly string BK_HUY = "DANG_KY_TL_NPL_HUY";
    //    public static readonly string BK_NOP_THUE = "DANG_KY_TL_NPL_NT";
    //    public static readonly string BK_TAI_XUAT = "DANG_KY_TL_NPL_TX";
    //    //public static readonly string  SUA_TKN= "SUA_TKNSX";
    //}

    //[Serializable]
    //public class THONG_TIN_HUY
    //{
    //    public static readonly string DINH_MUC = "HUY_DM";
    //    public static readonly string NPL = "HUY_NPL";
    //    public static readonly string SP = "HUY_SP";
    //    public static readonly string TOKHAINHAP = "HUY_TKN";
    //    public static readonly string TOKHAIXUAT = "HUY_TKX";
    //    public static readonly string HOSOTHANHLY = "HUY_TL_HO_SO";
    //    public static readonly string BK_TOKHAINHAP = "HUY_TL_DSTKN";
    //    public static readonly string BK_TOKHAIXUAT = "HUY_TL_DSTKX";
    //    public static readonly string BK_TAM_NOP_THUE = "HUY_TL_CHI_TIET_NT";
    //    public static readonly string BK_CHUA_THANH_LY = "HUY_TL_NPL_CHUA_TL";
    //    public static readonly string BK_TL_NKD = "HUY_TL_NPL_NKD";
    //    public static readonly string BK_XUAT_GC = "HUY_TL_NPL_XGC";
    //    public static readonly string BK_HUY = "HUY_TL_NPL_HUY";
    //    public static readonly string BK_NOP_THUE = "HUY_TL_NPL_NT";
    //    public static readonly string BK_TAI_XUAT = "HUY_TL_NPL_TX";
    //}

    //public enum RoleNguyenPhuLieu
    //{
    //    KhaiDienTu = 100,
    //    CapNhatDuLieu = 101,
    //    XemDuLieu = 102,
    //};
    //public enum RoleSanPham
    //{
    //    KhaiDienTu = 200,
    //    CapNhatDuLieu = 201,
    //    XemDuLieu = 202,
    //};
    //public enum RoleDinhMuc
    //{
    //    KhaiDienTu = 300,
    //    CapNhatDuLieu = 301,
    //    XemDuLieu = 302,
    //};
    //public enum RoleToKhai
    //{
    //    KhaiDienTuNhap = 400,
    //    KhaiDienTuXuat = 401,
    //    CapNhatDuLieu = 402,
    //    XemDuLieu = 403,
    //    ToKhaiSapHetHan = 404,
    //    CchiPhiXNK = 405,
    //};
    //public enum RoleThanhKhoan
    //{
    //    ThanhKhoan = 500,
    //};
    //public enum RoleSystem
    //{
    //    CreateUser = 600,
    //    DeleteUser = 601,
    //    UpdateUser = 602,
    //    CreateGroup = 603,
    //    DeleteGroup = 604,
    //    UpdateGroup = 605,
    //    Permission = 606,//phan quyen
    //    Management = 607,
    //};
    //public enum RoleNPLTon
    //{
    //    TheoDoiNPLTon = 700,
    //    ThongKeNPLTon = 701,
    //};
    //public enum RoleToKhaiHTKhac
    //{
    //    ToKhaiHTKhacNhap = 800,
    //    ToKhaiHTKhacXuat = 801,
    //};
    //public enum RoleQuanLyToKhai
    //{
    //    ThueTonKhoTKNhap = 900,
    //    ThueTonKhoTKXuat = 901,
    //};

}
