using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.BLL.SXXK.ThanhKhoan
{
    public partial class BCXuatNhapTon
    {
        #region Private members.

        protected int _LanThanhLy;
        protected string _MaDoanhNghiep = String.Empty;
        protected string _MaHaiQuan = String.Empty;
        protected string _MaNPL = String.Empty;
        protected string _TenNPL = String.Empty;
        protected int _SoToKhaiNhap;
        protected DateTime _NgayDangKyNhap = new DateTime(1900, 01, 01);
        protected DateTime _NgayHoanThanhNhap = new DateTime(1900, 01, 01);
        protected string _MaLoaiHinhNhap = String.Empty;
        protected decimal _LuongNhap;
        protected decimal _LuongTonDau;
        protected string _TenDVT_NPL = String.Empty;
        protected string _MaSP = String.Empty;
        protected string _TenSP = String.Empty;
        protected int _SoToKhaiXuat;
        protected DateTime _NgayDangKyXuat = new DateTime(1900, 01, 01);
        protected DateTime _NgayHoanThanhXuat = new DateTime(1900, 01, 01);
        protected string _MaLoaiHinhXuat = String.Empty;
        protected decimal _LuongSPXuat;
        protected string _TenDVT_SP = String.Empty;
        protected decimal _DinhMuc;
        protected decimal _LuongNPLSuDung;
        protected int _SoToKhaiTaiXuat;
        protected DateTime _NgayTaiXuat = new DateTime(1900, 01, 01);
        protected decimal _LuongNPLTaiXuat;
        protected decimal _LuongTonCuoi;
        protected decimal _ThanhKhoanTiep;
        protected decimal _ChuyenMucDichKhac;

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Properties.

        public int LanThanhLy
        {
            set { this._LanThanhLy = value; }
            get { return this._LanThanhLy; }
        }
        public string MaDoanhNghiep
        {
            set { this._MaDoanhNghiep = value; }
            get { return this._MaDoanhNghiep; }
        }
        public string MaHaiQuan
        {
            set { this._MaHaiQuan = value; }
            get { return this._MaHaiQuan; }
        }
        public string MaNPL
        {
            set { this._MaNPL = value; }
            get { return this._MaNPL; }
        }
        public string TenNPL
        {
            set { this._TenNPL = value; }
            get { return this._TenNPL; }
        }
        public int SoToKhaiNhap
        {
            set { this._SoToKhaiNhap = value; }
            get { return this._SoToKhaiNhap; }
        }
        public DateTime NgayDangKyNhap
        {
            set { this._NgayDangKyNhap = value; }
            get { return this._NgayDangKyNhap; }
        }
        public DateTime NgayHoanThanhNhap
        {
            set { this._NgayHoanThanhNhap = value; }
            get { return this._NgayHoanThanhNhap; }
        }
        public string MaLoaiHinhNhap
        {
            set { this._MaLoaiHinhNhap = value; }
            get { return this._MaLoaiHinhNhap; }
        }
        public decimal LuongNhap
        {
            set { this._LuongNhap = value; }
            get { return this._LuongNhap; }
        }
        public decimal LuongTonDau
        {
            set { this._LuongTonDau = value; }
            get { return this._LuongTonDau; }
        }
        public string TenDVT_NPL
        {
            set { this._TenDVT_NPL = value; }
            get { return this._TenDVT_NPL; }
        }
        public string MaSP
        {
            set { this._MaSP = value; }
            get { return this._MaSP; }
        }
        public string TenSP
        {
            set { this._TenSP = value; }
            get { return this._TenSP; }
        }
        public int SoToKhaiXuat
        {
            set { this._SoToKhaiXuat = value; }
            get { return this._SoToKhaiXuat; }
        }
        public DateTime NgayDangKyXuat
        {
            set { this._NgayDangKyXuat = value; }
            get { return this._NgayDangKyXuat; }
        }
        public DateTime NgayHoanThanhXuat
        {
            set { this._NgayHoanThanhXuat = value; }
            get { return this._NgayHoanThanhXuat; }
        }
        public string MaLoaiHinhXuat
        {
            set { this._MaLoaiHinhXuat = value; }
            get { return this._MaLoaiHinhXuat; }
        }
        public decimal LuongSPXuat
        {
            set { this._LuongSPXuat = value; }
            get { return this._LuongSPXuat; }
        }
        public string TenDVT_SP
        {
            set { this._TenDVT_SP = value; }
            get { return this._TenDVT_SP; }
        }
        public decimal DinhMuc
        {
            set { this._DinhMuc = value; }
            get { return this._DinhMuc; }
        }
        public decimal LuongNPLSuDung
        {
            set { this._LuongNPLSuDung = value; }
            get { return this._LuongNPLSuDung; }
        }
        public int SoToKhaiTaiXuat
        {
            set { this._SoToKhaiTaiXuat = value; }
            get { return this._SoToKhaiTaiXuat; }
        }
        public DateTime NgayTaiXuat
        {
            set { this._NgayTaiXuat = value; }
            get { return this._NgayTaiXuat; }
        }
        public decimal LuongNPLTaiXuat
        {
            set { this._LuongNPLTaiXuat = value; }
            get { return this._LuongNPLTaiXuat; }
        }
        public decimal LuongTonCuoi
        {
            set { this._LuongTonCuoi = value; }
            get { return this._LuongTonCuoi; }
        }
        public decimal ThanhKhoanTiep
        {
            set { this._ThanhKhoanTiep = value; }
            get { return this._ThanhKhoanTiep; }
        }
        public decimal ChuyenMucDichKhac
        {
            set { this._ChuyenMucDichKhac = value; }
            get { return this._ChuyenMucDichKhac; }
        }

        //---------------------------------------------------------------------------------------------

        public bool IsExist
        {
            get
            {
                return this.Load();
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public bool Load()
        {
            string spName = "p_SXXK_BCXuatNhapTon_Load";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, this._SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, this._NgayDangKyNhap);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, this._MaSP);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);

            IDataReader reader = db.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) this._LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) this._MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) this._MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) this._MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) this._TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) this._SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) this._NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhNhap"))) this._NgayHoanThanhNhap = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) this._MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) this._LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) this._LuongTonDau = reader.GetDecimal(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) this._TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) this._MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSP"))) this._TenSP = reader.GetString(reader.GetOrdinal("TenSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) this._SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) this._NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhXuat"))) this._NgayHoanThanhXuat = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) this._MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongSPXuat"))) this._LuongSPXuat = reader.GetDecimal(reader.GetOrdinal("LuongSPXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_SP"))) this._TenDVT_SP = reader.GetString(reader.GetOrdinal("TenDVT_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) this._DinhMuc = reader.GetDecimal(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) this._LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiTaiXuat"))) this._SoToKhaiTaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTaiXuat"))) this._NgayTaiXuat = reader.GetDateTime(reader.GetOrdinal("NgayTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTaiXuat"))) this._LuongNPLTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongNPLTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) this._LuongTonCuoi = reader.GetDecimal(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhKhoanTiep"))) this._ThanhKhoanTiep = reader.GetDecimal(reader.GetOrdinal("ThanhKhoanTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChuyenMucDichKhac"))) this._ChuyenMucDichKhac = reader.GetDecimal(reader.GetOrdinal("ChuyenMucDichKhac"));
                return true;
            }
            reader.Close();
            return false;
        }

        //---------------------------------------------------------------------------------------------

        public BCXuatNhapTonCollection SelectCollectionBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan()
        {
            string spName = "p_SXXK_BCXuatNhapTon_SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);

            BCXuatNhapTonCollection collection = new BCXuatNhapTonCollection();
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                BCXuatNhapTon entity = new BCXuatNhapTon();
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhNhap"))) entity.NgayHoanThanhNhap = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) entity.LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDecimal(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) entity.TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSP"))) entity.TenSP = reader.GetString(reader.GetOrdinal("TenSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhXuat"))) entity.NgayHoanThanhXuat = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongSPXuat"))) entity.LuongSPXuat = reader.GetDecimal(reader.GetOrdinal("LuongSPXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_SP"))) entity.TenDVT_SP = reader.GetString(reader.GetOrdinal("TenDVT_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetDecimal(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) entity.LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiTaiXuat"))) entity.SoToKhaiTaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTaiXuat"))) entity.NgayTaiXuat = reader.GetDateTime(reader.GetOrdinal("NgayTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTaiXuat"))) entity.LuongNPLTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongNPLTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDecimal(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhKhoanTiep"))) entity.ThanhKhoanTiep = reader.GetDecimal(reader.GetOrdinal("ThanhKhoanTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChuyenMucDichKhac"))) entity.ChuyenMucDichKhac = reader.GetDecimal(reader.GetOrdinal("ChuyenMucDichKhac"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        public static BCXuatNhapTonCollection SelectCollectionBy()
        {
            string spName = "p_SXXK_BCXuatNhapTon_SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            //db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            //db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            //db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);

            BCXuatNhapTonCollection collection = new BCXuatNhapTonCollection();
            IDataReader reader = db.ExecuteReader(dbCommand);
            while (reader.Read())
            {
                BCXuatNhapTon entity = new BCXuatNhapTon();
                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhNhap"))) entity.NgayHoanThanhNhap = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) entity.LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDecimal(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) entity.TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSP"))) entity.TenSP = reader.GetString(reader.GetOrdinal("TenSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhXuat"))) entity.NgayHoanThanhXuat = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongSPXuat"))) entity.LuongSPXuat = reader.GetDecimal(reader.GetOrdinal("LuongSPXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_SP"))) entity.TenDVT_SP = reader.GetString(reader.GetOrdinal("TenDVT_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetDecimal(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) entity.LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiTaiXuat"))) entity.SoToKhaiTaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTaiXuat"))) entity.NgayTaiXuat = reader.GetDateTime(reader.GetOrdinal("NgayTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTaiXuat"))) entity.LuongNPLTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongNPLTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDecimal(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhKhoanTiep"))) entity.ThanhKhoanTiep = reader.GetDecimal(reader.GetOrdinal("ThanhKhoanTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChuyenMucDichKhac"))) entity.ChuyenMucDichKhac = reader.GetDecimal(reader.GetOrdinal("ChuyenMucDichKhac"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan()
        {
            string spName = "p_SXXK_BCXuatNhapTon_SelectBy_MaDoanhNghiep_AND_LanThanhLy_AND_MaHaiQuan";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);

            return db.ExecuteDataSet(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        public DataSet SelectAll()
        {
            string spName = "p_SXXK_BCXuatNhapTon_SelectAll";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderAll()
        {
            string spName = "p_SXXK_BCXuatNhapTon_SelectAll";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_BCXuatNhapTon_SelectDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            string spName = "p_SXXK_BCXuatNhapTon_SelectDynamic";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public BCXuatNhapTonCollection SelectCollectionAll()
        {
            BCXuatNhapTonCollection collection = new BCXuatNhapTonCollection();

            IDataReader reader = this.SelectReaderAll();
            while (reader.Read())
            {
                BCXuatNhapTon entity = new BCXuatNhapTon();

                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhNhap"))) entity.NgayHoanThanhNhap = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) entity.LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDecimal(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) entity.TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSP"))) entity.TenSP = reader.GetString(reader.GetOrdinal("TenSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhXuat"))) entity.NgayHoanThanhXuat = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongSPXuat"))) entity.LuongSPXuat = reader.GetDecimal(reader.GetOrdinal("LuongSPXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_SP"))) entity.TenDVT_SP = reader.GetString(reader.GetOrdinal("TenDVT_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetDecimal(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) entity.LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiTaiXuat"))) entity.SoToKhaiTaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTaiXuat"))) entity.NgayTaiXuat = reader.GetDateTime(reader.GetOrdinal("NgayTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTaiXuat"))) entity.LuongNPLTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongNPLTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDecimal(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhKhoanTiep"))) entity.ThanhKhoanTiep = reader.GetDecimal(reader.GetOrdinal("ThanhKhoanTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChuyenMucDichKhac"))) entity.ChuyenMucDichKhac = reader.GetDecimal(reader.GetOrdinal("ChuyenMucDichKhac"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        //---------------------------------------------------------------------------------------------

        public BCXuatNhapTonCollection SelectCollectionDynamic(string whereCondition, string orderByExpression)
        {
            BCXuatNhapTonCollection collection = new BCXuatNhapTonCollection();

            IDataReader reader = this.SelectReaderDynamic(whereCondition, orderByExpression);
            while (reader.Read())
            {
                BCXuatNhapTon entity = new BCXuatNhapTon();

                if (!reader.IsDBNull(reader.GetOrdinal("LanThanhLy"))) entity.LanThanhLy = reader.GetInt32(reader.GetOrdinal("LanThanhLy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHaiQuan"))) entity.MaHaiQuan = reader.GetString(reader.GetOrdinal("MaHaiQuan"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNPL"))) entity.MaNPL = reader.GetString(reader.GetOrdinal("MaNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenNPL"))) entity.TenNPL = reader.GetString(reader.GetOrdinal("TenNPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiNhap"))) entity.SoToKhaiNhap = reader.GetInt32(reader.GetOrdinal("SoToKhaiNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyNhap"))) entity.NgayDangKyNhap = reader.GetDateTime(reader.GetOrdinal("NgayDangKyNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhNhap"))) entity.NgayHoanThanhNhap = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhNhap"))) entity.MaLoaiHinhNhap = reader.GetString(reader.GetOrdinal("MaLoaiHinhNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNhap"))) entity.LuongNhap = reader.GetDecimal(reader.GetOrdinal("LuongNhap"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonDau"))) entity.LuongTonDau = reader.GetDecimal(reader.GetOrdinal("LuongTonDau"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_NPL"))) entity.TenDVT_NPL = reader.GetString(reader.GetOrdinal("TenDVT_NPL"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaSP"))) entity.MaSP = reader.GetString(reader.GetOrdinal("MaSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenSP"))) entity.TenSP = reader.GetString(reader.GetOrdinal("TenSP"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiXuat"))) entity.SoToKhaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyXuat"))) entity.NgayDangKyXuat = reader.GetDateTime(reader.GetOrdinal("NgayDangKyXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayHoanThanhXuat"))) entity.NgayHoanThanhXuat = reader.GetDateTime(reader.GetOrdinal("NgayHoanThanhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinhXuat"))) entity.MaLoaiHinhXuat = reader.GetString(reader.GetOrdinal("MaLoaiHinhXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongSPXuat"))) entity.LuongSPXuat = reader.GetDecimal(reader.GetOrdinal("LuongSPXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenDVT_SP"))) entity.TenDVT_SP = reader.GetString(reader.GetOrdinal("TenDVT_SP"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMuc"))) entity.DinhMuc = reader.GetDecimal(reader.GetOrdinal("DinhMuc"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLSuDung"))) entity.LuongNPLSuDung = reader.GetDecimal(reader.GetOrdinal("LuongNPLSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhaiTaiXuat"))) entity.SoToKhaiTaiXuat = reader.GetInt32(reader.GetOrdinal("SoToKhaiTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayTaiXuat"))) entity.NgayTaiXuat = reader.GetDateTime(reader.GetOrdinal("NgayTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLTaiXuat"))) entity.LuongNPLTaiXuat = reader.GetDecimal(reader.GetOrdinal("LuongNPLTaiXuat"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongTonCuoi"))) entity.LuongTonCuoi = reader.GetDecimal(reader.GetOrdinal("LuongTonCuoi"));
                if (!reader.IsDBNull(reader.GetOrdinal("ThanhKhoanTiep"))) entity.ThanhKhoanTiep = reader.GetDecimal(reader.GetOrdinal("ThanhKhoanTiep"));
                if (!reader.IsDBNull(reader.GetOrdinal("ChuyenMucDichKhac"))) entity.ChuyenMucDichKhac = reader.GetDecimal(reader.GetOrdinal("ChuyenMucDichKhac"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public long Insert()
        {
            return this.InsertTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public long InsertTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_BCXuatNhapTon_Insert";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, this._SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, this._NgayDangKyNhap);
            db.AddInParameter(dbCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, this._NgayHoanThanhNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, this._MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, this._LuongNhap);
            db.AddInParameter(dbCommand, "@LuongTonDau", SqlDbType.Decimal, this._LuongTonDau);
            db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, this._TenDVT_NPL);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, this._MaSP);
            db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, this._TenSP);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
            db.AddInParameter(dbCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, this._NgayHoanThanhXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@LuongSPXuat", SqlDbType.Decimal, this._LuongSPXuat);
            db.AddInParameter(dbCommand, "@TenDVT_SP", SqlDbType.VarChar, this._TenDVT_SP);
            db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, this._DinhMuc);
            db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, this._LuongNPLSuDung);
            db.AddInParameter(dbCommand, "@SoToKhaiTaiXuat", SqlDbType.Int, this._SoToKhaiTaiXuat);
            db.AddInParameter(dbCommand, "@NgayTaiXuat", SqlDbType.DateTime, this._NgayTaiXuat);
            db.AddInParameter(dbCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, this._LuongNPLTaiXuat);
            db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Decimal, this._LuongTonCuoi);
            db.AddInParameter(dbCommand, "@ThanhKhoanTiep", SqlDbType.Decimal, this._ThanhKhoanTiep);
            db.AddInParameter(dbCommand, "@ChuyenMucDichKhac", SqlDbType.Decimal, this._ChuyenMucDichKhac);

            if (transaction != null)
            {
                return db.ExecuteNonQuery(dbCommand, transaction);
            }
            else
            {
                return db.ExecuteNonQuery(dbCommand);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool Insert(BCXuatNhapTonCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BCXuatNhapTon item in collection)
                    {
                        if (item.InsertTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        //---------------------------------------------------------------------------------------------		

        public void InsertTransaction(SqlTransaction transaction, BCXuatNhapTonCollection collection)
        {
            foreach (BCXuatNhapTon item in collection)
            {
                item.InsertTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.
        public int InsertUpdate()
        {
            return this.InsertUpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_BCXuatNhapTon_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, this._SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, this._NgayDangKyNhap);
            db.AddInParameter(dbCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, this._NgayHoanThanhNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, this._MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, this._LuongNhap);
            db.AddInParameter(dbCommand, "@LuongTonDau", SqlDbType.Decimal, this._LuongTonDau);
            db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, this._TenDVT_NPL);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, this._MaSP);
            db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, this._TenSP);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
            db.AddInParameter(dbCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, this._NgayHoanThanhXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@LuongSPXuat", SqlDbType.Decimal, this._LuongSPXuat);
            db.AddInParameter(dbCommand, "@TenDVT_SP", SqlDbType.VarChar, this._TenDVT_SP);
            db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, this._DinhMuc);
            db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, this._LuongNPLSuDung);
            db.AddInParameter(dbCommand, "@SoToKhaiTaiXuat", SqlDbType.Int, this._SoToKhaiTaiXuat);
            db.AddInParameter(dbCommand, "@NgayTaiXuat", SqlDbType.DateTime, this._NgayTaiXuat);
            db.AddInParameter(dbCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, this._LuongNPLTaiXuat);
            db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Decimal, this._LuongTonCuoi);
            db.AddInParameter(dbCommand, "@ThanhKhoanTiep", SqlDbType.Decimal, this._ThanhKhoanTiep);
            db.AddInParameter(dbCommand, "@ChuyenMucDichKhac", SqlDbType.Decimal, this._ChuyenMucDichKhac);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public bool InsertUpdate(BCXuatNhapTonCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BCXuatNhapTon item in collection)
                    {
                        if (item.InsertUpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public int Update()
        {
            return this.UpdateTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int UpdateTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_BCXuatNhapTon_Update";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@TenNPL", SqlDbType.NVarChar, this._TenNPL);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, this._SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, this._NgayDangKyNhap);
            db.AddInParameter(dbCommand, "@NgayHoanThanhNhap", SqlDbType.DateTime, this._NgayHoanThanhNhap);
            db.AddInParameter(dbCommand, "@MaLoaiHinhNhap", SqlDbType.Char, this._MaLoaiHinhNhap);
            db.AddInParameter(dbCommand, "@LuongNhap", SqlDbType.Decimal, this._LuongNhap);
            db.AddInParameter(dbCommand, "@LuongTonDau", SqlDbType.Decimal, this._LuongTonDau);
            db.AddInParameter(dbCommand, "@TenDVT_NPL", SqlDbType.VarChar, this._TenDVT_NPL);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, this._MaSP);
            db.AddInParameter(dbCommand, "@TenSP", SqlDbType.NVarChar, this._TenSP);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);
            db.AddInParameter(dbCommand, "@NgayHoanThanhXuat", SqlDbType.DateTime, this._NgayHoanThanhXuat);
            db.AddInParameter(dbCommand, "@MaLoaiHinhXuat", SqlDbType.Char, this._MaLoaiHinhXuat);
            db.AddInParameter(dbCommand, "@LuongSPXuat", SqlDbType.Decimal, this._LuongSPXuat);
            db.AddInParameter(dbCommand, "@TenDVT_SP", SqlDbType.VarChar, this._TenDVT_SP);
            db.AddInParameter(dbCommand, "@DinhMuc", SqlDbType.Decimal, this._DinhMuc);
            db.AddInParameter(dbCommand, "@LuongNPLSuDung", SqlDbType.Decimal, this._LuongNPLSuDung);
            db.AddInParameter(dbCommand, "@SoToKhaiTaiXuat", SqlDbType.Int, this._SoToKhaiTaiXuat);
            db.AddInParameter(dbCommand, "@NgayTaiXuat", SqlDbType.DateTime, this._NgayTaiXuat);
            db.AddInParameter(dbCommand, "@LuongNPLTaiXuat", SqlDbType.Decimal, this._LuongNPLTaiXuat);
            db.AddInParameter(dbCommand, "@LuongTonCuoi", SqlDbType.Decimal, this._LuongTonCuoi);
            db.AddInParameter(dbCommand, "@ThanhKhoanTiep", SqlDbType.Decimal, this._ThanhKhoanTiep);
            db.AddInParameter(dbCommand, "@ChuyenMucDichKhac", SqlDbType.Decimal, this._ChuyenMucDichKhac);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void UpdateCollection(BCXuatNhapTonCollection collection, SqlTransaction transaction)
        {
            foreach (BCXuatNhapTon item in collection)
            {
                item.UpdateTransaction(transaction);
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public int Delete()
        {
            return this.DeleteTransaction(null);
        }

        //---------------------------------------------------------------------------------------------

        public int DeleteTransaction(SqlTransaction transaction)
        {
            string spName = "p_SXXK_BCXuatNhapTon_Delete";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.Int, this._LanThanhLy);
            db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, this._MaDoanhNghiep);
            db.AddInParameter(dbCommand, "@MaHaiQuan", SqlDbType.Char, this._MaHaiQuan);
            db.AddInParameter(dbCommand, "@MaNPL", SqlDbType.VarChar, this._MaNPL);
            db.AddInParameter(dbCommand, "@SoToKhaiNhap", SqlDbType.Int, this._SoToKhaiNhap);
            db.AddInParameter(dbCommand, "@NgayDangKyNhap", SqlDbType.DateTime, this._NgayDangKyNhap);
            db.AddInParameter(dbCommand, "@MaSP", SqlDbType.VarChar, this._MaSP);
            db.AddInParameter(dbCommand, "@SoToKhaiXuat", SqlDbType.Int, this._SoToKhaiXuat);
            db.AddInParameter(dbCommand, "@NgayDangKyXuat", SqlDbType.DateTime, this._NgayDangKyXuat);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public void DeleteCollection(BCXuatNhapTonCollection collection, SqlTransaction transaction)
        {
            foreach (BCXuatNhapTon item in collection)
            {
                item.DeleteTransaction(transaction);
            }
        }

        //---------------------------------------------------------------------------------------------

        public bool DeleteCollection(BCXuatNhapTonCollection collection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (BCXuatNhapTon item in collection)
                    {
                        if (item.DeleteTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        #endregion
    }
}