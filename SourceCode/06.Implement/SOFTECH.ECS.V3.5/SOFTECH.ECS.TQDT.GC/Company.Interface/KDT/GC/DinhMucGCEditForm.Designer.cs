﻿namespace Company.Interface.KDT.GC
{
    partial class DinhMucGCEditForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DinhMucGCEditForm));
            Janus.Windows.GridEX.GridEXLayout cbbLenhSanXuat_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout cbbMaSP_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout cbbMaNPL_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.Common.Layouts.JanusLayoutReference dgList_DesignTimeLayout_Reference_0 = new Janus.Windows.Common.Layouts.JanusLayoutReference("GridEXLayoutData.RootTable.Columns.Column0.Image");
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.txtDinhMuc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyDinhMuc = new System.Windows.Forms.ToolStripMenuItem();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.txtTyLeHH = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTenSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearch = new Janus.Windows.EditControls.UIButton();
            this.label19 = new System.Windows.Forms.Label();
            this.txtMaSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lnkCoppy = new System.Windows.Forms.LinkLabel();
            this.cbbLenhSanXuat = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.cbbMaSP = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label24 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnAdd = new Janus.Windows.EditControls.UIButton();
            this.label16 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtDonViTinhSP = new Janus.Windows.EditControls.UIComboBox();
            this.txtMaHSSP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.uiButton1 = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnDelete = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSearchNPL = new Janus.Windows.EditControls.UIButton();
            this.label22 = new System.Windows.Forms.Label();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cbbMaNPL = new Janus.Windows.GridEX.EditControls.MultiColumnCombo();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDonViTinhNPL = new Janus.Windows.EditControls.UIComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMaHSNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSelect = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLenhSanXuat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaNPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Size = new System.Drawing.Size(1018, 618);
            // 
            // error
            // 
            this.error.ContainerControl = this;
            this.error.Icon = ((System.Drawing.Icon)(resources.GetObject("error.Icon")));
            // 
            // txtDinhMuc
            // 
            this.txtDinhMuc.BackColor = System.Drawing.Color.White;
            this.txtDinhMuc.DecimalDigits = 8;
            this.txtDinhMuc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDinhMuc.Location = new System.Drawing.Point(99, 136);
            this.txtDinhMuc.Name = "txtDinhMuc";
            this.txtDinhMuc.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtDinhMuc.Size = new System.Drawing.Size(139, 22);
            this.txtDinhMuc.TabIndex = 11;
            this.txtDinhMuc.Text = "0.00000000";
            this.txtDinhMuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtDinhMuc.Value = new decimal(new int[] {
            0,
            0,
            0,
            524288});
            this.txtDinhMuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDinhMuc.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyDinhMuc});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(123, 26);
            // 
            // copyDinhMuc
            // 
            this.copyDinhMuc.Image = ((System.Drawing.Image)(resources.GetObject("copyDinhMuc.Image")));
            this.copyDinhMuc.Name = "copyDinhMuc";
            this.copyDinhMuc.Size = new System.Drawing.Size(122, 22);
            this.copyDinhMuc.Text = "Sao chép";
            this.copyDinhMuc.Click += new System.EventHandler(this.copyDinhMuc_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // txtTyLeHH
            // 
            this.txtTyLeHH.BackColor = System.Drawing.Color.White;
            this.txtTyLeHH.DecimalDigits = 8;
            this.txtTyLeHH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyLeHH.Location = new System.Drawing.Point(349, 136);
            this.txtTyLeHH.Name = "txtTyLeHH";
            this.txtTyLeHH.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtTyLeHH.Size = new System.Drawing.Size(121, 22);
            this.txtTyLeHH.TabIndex = 12;
            this.txtTyLeHH.Text = "0.00000000";
            this.txtTyLeHH.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTyLeHH.Value = new decimal(new int[] {
            0,
            0,
            0,
            524288});
            this.txtTyLeHH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTyLeHH.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // txtTenSP
            // 
            this.txtTenSP.BackColor = System.Drawing.Color.White;
            this.txtTenSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenSP.Location = new System.Drawing.Point(121, 47);
            this.txtTenSP.MaxLength = 254;
            this.txtTenSP.Multiline = true;
            this.txtTenSP.Name = "txtTenSP";
            this.txtTenSP.ReadOnly = true;
            this.txtTenSP.Size = new System.Drawing.Size(351, 48);
            this.txtTenSP.TabIndex = 2;
            this.txtTenSP.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenNPL
            // 
            this.txtTenNPL.BackColor = System.Drawing.Color.White;
            this.txtTenNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenNPL.Location = new System.Drawing.Point(98, 47);
            this.txtTenNPL.Multiline = true;
            this.txtTenNPL.Name = "txtTenNPL";
            this.txtTenNPL.ReadOnly = true;
            this.txtTenNPL.Size = new System.Drawing.Size(372, 48);
            this.txtTenNPL.TabIndex = 8;
            this.txtTenNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.uiGroupBox6);
            this.uiGroupBox2.Controls.Add(this.lnkCoppy);
            this.uiGroupBox2.Controls.Add(this.cbbLenhSanXuat);
            this.uiGroupBox2.Controls.Add(this.cbbMaSP);
            this.uiGroupBox2.Controls.Add(this.label24);
            this.uiGroupBox2.Controls.Add(this.label17);
            this.uiGroupBox2.Controls.Add(this.btnAdd);
            this.uiGroupBox2.Controls.Add(this.label16);
            this.uiGroupBox2.Controls.Add(this.label21);
            this.uiGroupBox2.Controls.Add(this.label20);
            this.uiGroupBox2.Controls.Add(this.txtDonViTinhSP);
            this.uiGroupBox2.Controls.Add(this.txtMaHSSP);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.label15);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.txtTenSP);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Left;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(3, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(509, 264);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin sản phẩm";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.AutoScroll = true;
            this.uiGroupBox6.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox6.Controls.Add(this.btnSearch);
            this.uiGroupBox6.Controls.Add(this.label19);
            this.uiGroupBox6.Controls.Add(this.txtMaSP);
            this.uiGroupBox6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox6.Location = new System.Drawing.Point(3, 211);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(503, 50);
            this.uiGroupBox6.TabIndex = 25;
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearch
            // 
            this.btnSearch.Image = ((System.Drawing.Image)(resources.GetObject("btnSearch.Image")));
            this.btnSearch.Location = new System.Drawing.Point(369, 18);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(100, 23);
            this.btnSearch.TabIndex = 87;
            this.btnSearch.Text = "Tìm kiếm";
            this.btnSearch.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(23, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(70, 13);
            this.label19.TabIndex = 86;
            this.label19.Text = "Mã sản phẩm";
            // 
            // txtMaSP
            // 
            this.txtMaSP.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaSP.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaSP.BackColor = System.Drawing.Color.White;
            this.txtMaSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaSP.Location = new System.Drawing.Point(118, 19);
            this.txtMaSP.Name = "txtMaSP";
            this.txtMaSP.Size = new System.Drawing.Size(245, 21);
            this.txtMaSP.TabIndex = 85;
            this.txtMaSP.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaSP.TextChanged += new System.EventHandler(this.btnSearch_Click);
            // 
            // lnkCoppy
            // 
            this.lnkCoppy.AutoSize = true;
            this.lnkCoppy.Location = new System.Drawing.Point(414, 132);
            this.lnkCoppy.Name = "lnkCoppy";
            this.lnkCoppy.Size = new System.Drawing.Size(58, 14);
            this.lnkCoppy.TabIndex = 14;
            this.lnkCoppy.TabStop = true;
            this.lnkCoppy.Text = "Sao chép";
            this.lnkCoppy.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkCoppy_LinkClicked);
            // 
            // cbbLenhSanXuat
            // 
            cbbLenhSanXuat_DesignTimeLayout.LayoutString = resources.GetString("cbbLenhSanXuat_DesignTimeLayout.LayoutString");
            this.cbbLenhSanXuat.DesignTimeLayout = cbbLenhSanXuat_DesignTimeLayout;
            this.cbbLenhSanXuat.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbLenhSanXuat.Location = new System.Drawing.Point(215, 108);
            this.cbbLenhSanXuat.Name = "cbbLenhSanXuat";
            this.cbbLenhSanXuat.SelectedIndex = -1;
            this.cbbLenhSanXuat.SelectedItem = null;
            this.cbbLenhSanXuat.Size = new System.Drawing.Size(259, 21);
            this.cbbLenhSanXuat.TabIndex = 3;
            this.cbbLenhSanXuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbLenhSanXuat.ValueChanged += new System.EventHandler(this.cbbLenhSanXuat_ValueChanged);
            this.cbbLenhSanXuat.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // cbbMaSP
            // 
            cbbMaSP_DesignTimeLayout.LayoutString = resources.GetString("cbbMaSP_DesignTimeLayout.LayoutString");
            this.cbbMaSP.DesignTimeLayout = cbbMaSP_DesignTimeLayout;
            this.cbbMaSP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbMaSP.Location = new System.Drawing.Point(121, 18);
            this.cbbMaSP.Name = "cbbMaSP";
            this.cbbMaSP.SelectedIndex = -1;
            this.cbbMaSP.SelectedItem = null;
            this.cbbMaSP.Size = new System.Drawing.Size(351, 21);
            this.cbbMaSP.TabIndex = 1;
            this.cbbMaSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbMaSP.ValueChanged += new System.EventHandler(this.cbbMaSP_ValueChanged);
            this.cbbMaSP.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(286, 154);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(14, 14);
            this.label24.TabIndex = 0;
            this.label24.Text = "*";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.Red;
            this.label17.Location = new System.Drawing.Point(480, 154);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(14, 14);
            this.label17.TabIndex = 0;
            this.label17.Text = "*";
            // 
            // btnAdd
            // 
            this.btnAdd.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdd.Image = ((System.Drawing.Image)(resources.GetObject("btnAdd.Image")));
            this.btnAdd.ImageSize = new System.Drawing.Size(20, 20);
            this.btnAdd.Location = new System.Drawing.Point(123, 182);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(106, 23);
            this.btnAdd.TabIndex = 13;
            this.btnAdd.Text = "Ghi";
            this.btnAdd.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(479, 65);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(14, 14);
            this.label16.TabIndex = 0;
            this.label16.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(479, 105);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(14, 14);
            this.label21.TabIndex = 0;
            this.label21.Text = "*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(479, 25);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(14, 14);
            this.label20.TabIndex = 0;
            this.label20.Text = "*";
            // 
            // txtDonViTinhSP
            // 
            this.txtDonViTinhSP.BackColor = System.Drawing.Color.White;
            this.txtDonViTinhSP.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.txtDonViTinhSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonViTinhSP.Location = new System.Drawing.Point(123, 150);
            this.txtDonViTinhSP.Name = "txtDonViTinhSP";
            this.txtDonViTinhSP.ReadOnly = true;
            this.txtDonViTinhSP.Size = new System.Drawing.Size(159, 22);
            this.txtDonViTinhSP.TabIndex = 5;
            this.txtDonViTinhSP.ValueMember = "ID";
            this.txtDonViTinhSP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtMaHSSP
            // 
            this.txtMaHSSP.BackColor = System.Drawing.Color.White;
            this.txtMaHSSP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSSP.Location = new System.Drawing.Point(348, 150);
            this.txtMaHSSP.Name = "txtMaHSSP";
            this.txtMaHSSP.ReadOnly = true;
            this.txtMaHSSP.Size = new System.Drawing.Size(126, 22);
            this.txtMaHSSP.TabIndex = 6;
            this.txtMaHSSP.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHSSP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(24, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(78, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã sản phẩm";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(24, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 14);
            this.label2.TabIndex = 0;
            this.label2.Text = "Tên sản phẩm";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(302, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 14);
            this.label3.TabIndex = 0;
            this.label3.Text = "Mã HS";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(24, 108);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(180, 14);
            this.label15.TabIndex = 0;
            this.label15.Text = "Mã định danh của lệnh sản xuất";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 14);
            this.label4.TabIndex = 0;
            this.label4.Text = "Đơn vị tính";
            // 
            // uiButton1
            // 
            this.uiButton1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiButton1.Image = ((System.Drawing.Image)(resources.GetObject("uiButton1.Image")));
            this.uiButton1.ImageSize = new System.Drawing.Size(20, 20);
            this.uiButton1.Location = new System.Drawing.Point(9, 12);
            this.uiButton1.Name = "uiButton1";
            this.uiButton1.Size = new System.Drawing.Size(85, 23);
            this.uiButton1.TabIndex = 14;
            this.uiButton1.Text = "Sao chép";
            this.uiButton1.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.uiButton1.Click += new System.EventHandler(this.copyDinhMuc_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(928, 12);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 16;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDelete.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.ImageSize = new System.Drawing.Size(20, 20);
            this.btnDelete.Location = new System.Drawing.Point(847, 12);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(72, 23);
            this.btnDelete.TabIndex = 15;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox3.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1018, 275);
            this.uiGroupBox3.TabIndex = 37;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.AutoScroll = true;
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.uiGroupBox7);
            this.uiGroupBox4.Controls.Add(this.cbbMaNPL);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label11);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.txtDonViTinhNPL);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.txtMaHSNPL);
            this.uiGroupBox4.Controls.Add(this.label10);
            this.uiGroupBox4.Controls.Add(this.txtDinhMuc);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.txtTyLeHH);
            this.uiGroupBox4.Controls.Add(this.label5);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.txtTenNPL);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(512, 8);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(503, 264);
            this.uiGroupBox4.TabIndex = 37;
            this.uiGroupBox4.Text = "Thông tin nguyên phụ liệu";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox7.Controls.Add(this.btnSearchNPL);
            this.uiGroupBox7.Controls.Add(this.label22);
            this.uiGroupBox7.Controls.Add(this.txtMaNPL);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox7.Location = new System.Drawing.Point(3, 211);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(497, 50);
            this.uiGroupBox7.TabIndex = 286;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSearchNPL
            // 
            this.btnSearchNPL.Image = ((System.Drawing.Image)(resources.GetObject("btnSearchNPL.Image")));
            this.btnSearchNPL.Location = new System.Drawing.Point(378, 17);
            this.btnSearchNPL.Name = "btnSearchNPL";
            this.btnSearchNPL.Size = new System.Drawing.Size(89, 23);
            this.btnSearchNPL.TabIndex = 87;
            this.btnSearchNPL.Text = "Tìm kiếm";
            this.btnSearchNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSearchNPL.Click += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(22, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(42, 13);
            this.label22.TabIndex = 86;
            this.label22.Text = "Mã NPL";
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.txtMaNPL.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.txtMaNPL.BackColor = System.Drawing.Color.White;
            this.txtMaNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNPL.Location = new System.Drawing.Point(95, 18);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(266, 21);
            this.txtMaNPL.TabIndex = 85;
            this.txtMaNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNPL.TextChanged += new System.EventHandler(this.btnSearchNPL_Click);
            // 
            // cbbMaNPL
            // 
            cbbMaNPL_DesignTimeLayout.LayoutString = resources.GetString("cbbMaNPL_DesignTimeLayout.LayoutString");
            this.cbbMaNPL.DesignTimeLayout = cbbMaNPL_DesignTimeLayout;
            this.cbbMaNPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbMaNPL.Location = new System.Drawing.Point(98, 18);
            this.cbbMaNPL.Name = "cbbMaNPL";
            this.cbbMaNPL.SelectedIndex = -1;
            this.cbbMaNPL.SelectedItem = null;
            this.cbbMaNPL.Size = new System.Drawing.Size(372, 21);
            this.cbbMaNPL.TabIndex = 7;
            this.cbbMaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.cbbMaNPL.ValueChanged += new System.EventHandler(this.cbbMaNPL_ValueChanged);
            this.cbbMaNPL.TextChanged += new System.EventHandler(this.txt_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(476, 110);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(14, 14);
            this.label12.TabIndex = 0;
            this.label12.Text = "*";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Red;
            this.label11.Location = new System.Drawing.Point(244, 107);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(14, 14);
            this.label11.TabIndex = 0;
            this.label11.Text = "*";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(244, 140);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(14, 14);
            this.label18.TabIndex = 0;
            this.label18.Text = "*";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Red;
            this.label14.Location = new System.Drawing.Point(476, 65);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(14, 14);
            this.label14.TabIndex = 0;
            this.label14.Text = "*";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Red;
            this.label13.Location = new System.Drawing.Point(476, 25);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(14, 14);
            this.label13.TabIndex = 0;
            this.label13.Text = "*";
            // 
            // txtDonViTinhNPL
            // 
            this.txtDonViTinhNPL.BackColor = System.Drawing.Color.White;
            this.txtDonViTinhNPL.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.txtDonViTinhNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonViTinhNPL.Location = new System.Drawing.Point(99, 103);
            this.txtDonViTinhNPL.Name = "txtDonViTinhNPL";
            this.txtDonViTinhNPL.ReadOnly = true;
            this.txtDonViTinhNPL.Size = new System.Drawing.Size(139, 22);
            this.txtDonViTinhNPL.TabIndex = 9;
            this.txtDonViTinhNPL.ValueMember = "ID";
            this.txtDonViTinhNPL.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(22, 140);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 14);
            this.label9.TabIndex = 0;
            this.label9.Text = "Định mức";
            // 
            // txtMaHSNPL
            // 
            this.txtMaHSNPL.BackColor = System.Drawing.Color.White;
            this.txtMaHSNPL.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSNPL.Location = new System.Drawing.Point(349, 103);
            this.txtMaHSNPL.Name = "txtMaHSNPL";
            this.txtMaHSNPL.ReadOnly = true;
            this.txtMaHSNPL.Size = new System.Drawing.Size(121, 22);
            this.txtMaHSNPL.TabIndex = 10;
            this.txtMaHSNPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHSNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(265, 140);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(80, 14);
            this.label10.TabIndex = 0;
            this.label10.Text = "Tỷ lệ HH (%)";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(21, 179);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(281, 14);
            this.label23.TabIndex = 0;
            this.label23.Text = "Các dòng có đánh dấu  (*)  là bắt buộc phải nhập";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(21, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 14);
            this.label5.TabIndex = 0;
            this.label5.Text = "Mã NPL";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(22, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(54, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên NPL";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(265, 107);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 14);
            this.label7.TabIndex = 0;
            this.label7.Text = "Mã HS";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(22, 107);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 14);
            this.label8.TabIndex = 4;
            this.label8.Text = "Đơn vị tính";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox5);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 275);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1018, 343);
            this.uiGroupBox1.TabIndex = 38;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BorderStyle = Janus.Windows.GridEX.BorderStyle.Flat;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            this.dgList.ContextMenuStrip = this.contextMenuStrip1;
            dgList_DesignTimeLayout_Reference_0.Instance = ((object)(resources.GetObject("dgList_DesignTimeLayout_Reference_0.Instance")));
            dgList_DesignTimeLayout.LayoutReferences.AddRange(new Janus.Windows.Common.Layouts.JanusLayoutReference[] {
            dgList_DesignTimeLayout_Reference_0});
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(3, 8);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1012, 291);
            this.dgList.TabIndex = 341;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.SelectionChanged += new System.EventHandler(this.dgList_SelectionChanged);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.AutoScroll = true;
            this.uiGroupBox5.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox5.Controls.Add(this.btnSelect);
            this.uiGroupBox5.Controls.Add(this.btnClose);
            this.uiGroupBox5.Controls.Add(this.uiButton1);
            this.uiGroupBox5.Controls.Add(this.btnDelete);
            this.uiGroupBox5.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(3, 299);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(1012, 41);
            this.uiGroupBox5.TabIndex = 340;
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // btnSelect
            // 
            this.btnSelect.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelect.Image = ((System.Drawing.Image)(resources.GetObject("btnSelect.Image")));
            this.btnSelect.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSelect.Location = new System.Drawing.Point(100, 12);
            this.btnSelect.Name = "btnSelect";
            this.btnSelect.Size = new System.Drawing.Size(118, 23);
            this.btnSelect.TabIndex = 24;
            this.btnSelect.Text = "Chọn sản phẩm";
            this.btnSelect.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSelect.Click += new System.EventHandler(this.btnSelect_Click);
            // 
            // DinhMucGCEditForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1018, 618);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "DinhMucGCEditForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin định mức";
            this.Load += new System.EventHandler(this.DinhMucGCEditForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DinhMucGCEditForm_FormClosing);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbLenhSanXuat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            this.uiGroupBox7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cbbMaNPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ErrorProvider error;
        private System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem copyDinhMuc;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton uiButton1;
        private System.Windows.Forms.Label label17;
        private Janus.Windows.EditControls.UIButton btnAdd;
        private System.Windows.Forms.Label label16;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.EditControls.UIButton btnDelete;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.EditControls.UIComboBox txtDonViTinhSP;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSSP;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenSP;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private Janus.Windows.EditControls.UIComboBox txtDonViTinhNPL;
        private System.Windows.Forms.Label label9;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSNPL;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDinhMuc;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTyLeHH;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenNPL;
        private Janus.Windows.GridEX.GridEX dgList;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox5;
        private System.Windows.Forms.Label label23;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbMaNPL;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbMaSP;
        private Janus.Windows.GridEX.EditControls.MultiColumnCombo cbbLenhSanXuat;
        private Janus.Windows.EditControls.UIButton btnSelect;
        private System.Windows.Forms.LinkLabel lnkCoppy;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox6;
        private Janus.Windows.EditControls.UIButton btnSearch;
        private System.Windows.Forms.Label label19;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaSP;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIButton btnSearchNPL;
        private System.Windows.Forms.Label label22;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private System.Windows.Forms.Label label24;
    }
}