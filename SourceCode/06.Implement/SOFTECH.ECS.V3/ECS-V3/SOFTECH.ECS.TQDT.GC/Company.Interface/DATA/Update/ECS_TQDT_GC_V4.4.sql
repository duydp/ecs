﻿/*
Run this script on:

        192.168.72.100.ECS_TQDT_GC_V3_VERSION    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_GC_V3

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.1.0 from Red Gate Software Ltd at 09/06/2012 2:43:34 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Refreshing [dbo].[t_View_KDT_NPLCungUng]'
GO
EXEC sp_refreshview N'[dbo].[t_View_KDT_NPLCungUng]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_GC_ToKhaiChuyenTiep_LoadBy]'
GO
       
------------------------------------------------------------------------------------------------------------------------      
-- Stored procedure name: [dbo].[p_KDT_GC_ToKhaiChuyenTiep_Load]      
-- Database: ECS_TQDT_GC      
-- Author: Ngo Thanh Tung      
-- Time created: Tuesday, February 22, 2011      
------------------------------------------------------------------------------------------------------------------------      
      
ALTER PROCEDURE [dbo].[p_KDT_GC_ToKhaiChuyenTiep_LoadBy]      
 @IDHopDong bigint,        
 @SoToKhai bigint,        
 @MaLoaiHinh nchar(10),        
 @NgayDangKy DATETIME,        
 @MaDoanhNghiep varchar(20),        
 @MaHaiQuanTiepNhan char(6)       
AS      
      
SET NOCOUNT ON      
SET TRANSACTION ISOLATION LEVEL READ COMMITTED      
      
SELECT      
 [ID],      
 [IDHopDong],      
 [SoTiepNhan],      
 [NgayTiepNhan],      
 [TrangThaiXuLy],      
 [MaHaiQuanTiepNhan],      
 [SoToKhai],      
 [CanBoDangKy],      
 [NgayDangKy],      
 [MaDoanhNghiep],      
 [SoHopDongDV],      
 [NgayHDDV],      
 [NgayHetHanHDDV],      
 [NguoiChiDinhDV],      
 [MaKhachHang],      
 [TenKH],      
 [SoHDKH],      
 [NgayHDKH],      
 [NgayHetHanHDKH],      
 [NguoiChiDinhKH],      
 [MaDaiLy],      
 [MaLoaiHinh],      
 [DiaDiemXepHang],      
 [TyGiaVND],      
 [TyGiaUSD],      
 [LePhiHQ],      
 [SoBienLai],      
 [NgayBienLai],      
 [ChungTu],      
 [NguyenTe_ID],      
 [MaHaiQuanKH],      
 [ID_Relation],      
 [GUIDSTR],      
 [DeXuatKhac],      
 [LyDoSua],      
 [ActionStatus],      
 [GuidReference],      
 [NamDK],      
 [HUONGDAN],      
 [PhanLuong],      
 [Huongdan_PL],      
 [PTTT_ID],      
 [DKGH_ID],      
 [ThoiGianGiaoHang],  
 [SoKien],    
 [TrongLuong],    
 [TrongLuongNet],
 LoaiHangHoa
FROM      
 [dbo].[t_KDT_GC_ToKhaiChuyenTiep]      
WHERE      
 SoToKhai = @SoToKhai AND NgayDangKy = @NgayDangKy AND MaLoaiHinh = @MaLoaiHinh AND MaHaiQuanTiepNhan = @MaHaiQuanTiepNhan AND MaDoanhNghiep = @MaDoanhNghiep AND IDHopDong = @IDHopDong
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [dbo].[p_NPLCungUngDaDangKy]'
GO
-- =============================================  
-- Author:  <Author,,Huynh Ngoc Khanh>  
-- Create date: <Create Date,,01/09/2012>  
-- Description: <Description,,Theo doi NPL cung ung>  
-- =============================================  
CREATE PROCEDURE [dbo].[p_NPLCungUngDaDangKy]  
@HopDongID int
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 -- p_NPLTuCungUng '0400101556',23,676  
 SET NOCOUNT ON;   
  
--Declare @MaDoanhNghiep varchar(50);  
--set @MaDoanhNghiep='0100101308'  
SELECT Ma,MAX(TenNPL)AS TenNPL,SUM(LuongCung)LuongCung,MAX(DonViTinh) AS DonViTinh,STK,MAX(TKMD_ID) AS TKMD_ID ,MAX(TKCT_ID)AS TKCT_ID
FROM
(SELECT     t_KDT_GC_NguyenPhuLieu.Ma, t_KDT_GC_NguyenPhuLieu.Ten AS TenNPL, t_KDT_GC_NguyenPhuLieu.MaHS, 
                       t_View_KDT_NPLCungUng.LuongCung,
                          (SELECT     Ten
                            FROM          t_HaiQuan_DonViTinh
                            WHERE      (ID = t_KDT_GC_NguyenPhuLieu.DVT_ID)) AS DonViTinh, 
                           CASE WHEN t_View_KDT_NPLCungUng.TKMD_ID > 0 THEN
									(SELECT   
										CASE	WHEN  t_KDT_ToKhaiMauDich.SoToKhai > 0 THEN
												CONVERT(VARCHAR(7), SoToKhai) + '/' + MaLoaiHinh + '/' + CONVERT(VARCHAR(10), NamDK)
												ELSE
												N'Tờ khai chưa được cấp số (ID = '+CONVERT(VARCHAR(6),t_KDT_ToKhaiMauDich.ID)+')'
												END
									FROM    dbo.t_KDT_ToKhaiMauDich
									WHERE      ID = t_View_KDT_NPLCungUng.TKMD_ID) 
							     WHEN	t_View_KDT_NPLCungUng.TKMD_ID = 0 AND t_View_KDT_NPLCungUng.TKCT_ID > 0 THEN
									(SELECT     
										CASE	WHEN tkct.SoToKhai > 0 THEN
												CONVERT(VARCHAR(7), tkct.SoToKhai) + '/' + tkct.MaLoaiHinh + '/' + CONVERT(VARCHAR(4), tkct.NamDK)
												ELSE
												N'Tờ khai chuyển tiếp xuất chưa được cấp số (ID = '+CONVERT(VARCHAR(6),tkct.ID)+')'
												END
									FROM          dbo.t_KDT_GC_ToKhaiChuyenTiep tkct
									WHERE      tkct.ID = t_View_KDT_NPLCungUng.TKCT_ID)
                            END AS STK,
                             t_View_KDT_NPLCungUng.TKMD_ID AS TKMD_ID, t_View_KDT_NPLCungUng.TKCT_ID AS TKCT_ID
FROM         t_KDT_GC_NguyenPhuLieu INNER JOIN
                      t_View_KDT_NPLCungUng ON t_KDT_GC_NguyenPhuLieu.Ma = t_View_KDT_NPLCungUng.MaNguyenPhuLieu
WHERE     (t_KDT_GC_NguyenPhuLieu.HopDong_ID = 886 )) AS t_view_NPLCungUng
GROUP BY STK,Ma
ORDER BY Ma


END  
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

if( (select count(*) from dbo.t_HaiQuan_Version WHERE [Version] ='4.4') = 0)
	begin
		insert into dbo.t_HaiQuan_Version values('4.4', getdate(), N'')
	end 