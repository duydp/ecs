﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class ComplianceWithLaws
    {
        /// <summary>
        /// Bị xử lý về hành vi buôn lậu, vận chuyển trái phép hàng hóa qua biên giới, trốn thuế
        /// </summary>
        [XmlElement("smuggling")]
        public string Smuggling { get; set; }

        /// <summary>
        /// Bị xử phạt về hành vi trốn thuế, gian lận thuế
        /// </summary>
        [XmlElement("taxEvasion")]
        public string TaxEvasion { get; set; }

        /// <summary>
        /// Bị các cơ quan quản lý nhà nước xử phạt vi phạm trong lĩnh vực kế toán
        /// </summary>
        [XmlElement("handlingViolations")]
        public string HandlingViolations { get; set; }
    }
}
