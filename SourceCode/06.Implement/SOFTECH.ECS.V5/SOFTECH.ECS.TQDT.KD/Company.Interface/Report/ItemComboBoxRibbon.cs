﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.Interface.Report
{
    public class ItemComboBoxRibbon
    {
        string _Caption;
        public string Caption { get { return _Caption; } set { _Caption = value; } }
        object _Value;
        public object ValueItem { get { return _Value; } set { _Value = value; } }
        TypeItems _Type;
        public TypeItems Type
        { get { return _Type; } set { _Type = value; } }

        public ItemComboBoxRibbon(string caption, object value, TypeItems type)
        {
            this._Caption = caption;
            this._Value = value;
            this._Type = type;
        }
        public ItemComboBoxRibbon(string caption, TypeItems type) : this(caption, null, type) { }
        public enum TypeItems
        {
            TK,     // Tờ khai
            PL,     // Phụ lục
            Cont,   // Phụ lục container
            BangKeCont, // Bảng kê container
            TKTG,   // Tờ khai trị giá
            PLTKTG, // Phụ lục tờ khai trị giá
            All
        }





    }
}
