﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    [XmlRoot("Declaration")]
    public class SXXK_TuCungUng : DeclarationBase
    {
        [XmlElement("MaterialSupply")]
        public SXXK_MaterialSupply MaterialSupply { get; set; }
    }
}
