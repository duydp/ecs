﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe04_HSTK_TT13 : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        public bool First = false;
        public bool Last = false;
        public Report.ReportViewBC04TT74Form report = new ReportViewBC04TT74Form();

        public BangKe04_HSTK_TT13()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt)
        {
            //this.ReportHeader.Visible = First;
            //this.lblNgayKi.Visible = this.Last;
            //this.lblNgayKi1.Visible = this.Last;
            //this.lblNgayKi2.Visible = this.Last;
            this.PrintingSystem.ShowMarginsWarning = false;
            this.DataSource = dt;


            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + " - " + dr["TenSanPham"].ToString();
                }
            }

            lblMathang.Text = loaiSP;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;

            //lblNPL1.Text = dt.Columns[2].Caption;
            //lblNPL2.Text = dt.Columns[3].Caption;
            //lblNPL3.Text = dt.Columns[4].Caption;
            //lblNPL4.Text = dt.Columns[5].Caption;
            //lblNPL5.Text = dt.Columns[6].Caption;
            //lblNPL6.Text = dt.Columns[7].Caption;
            //lblNPL7.Text = dt.Columns[8].Caption;
            lblLuong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft; 
            lblTKXK.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[0].ColumnName);
            lblLuong1.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[1].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong2.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[2].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong3.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[3].ColumnName, "{0:N"+ GlobalSettings.SoThapPhan.LuongNPL +"}");
            lblLuong4.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[4].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuong5.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[5].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblHinhThucCungUng.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[6].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblGhiChu.DataBindings.Add("Text", this.DataSource, dt.TableName + "." + dt.Columns[7].ColumnName, "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblNgayKi.Text = GlobalSettings.TieudeNgay;
            //decimal[] Tong = { 0, 0, 0, 0, 0, 0, 0};

            //foreach (DataRow dr in dt.Rows)
            //{
            //    for (int i = 2; i <= 8; i++)
            //        if (dr[i].ToString() != "")
            //            Tong[i-2] += Decimal.Parse(dr[i].ToString());
            //}

            //string[] strTong = new string[7];
            //for (int j = 0; j < Tong.Length; j++)
            //{
            //    if (Tong[j] != 0)
            //    {
            //        strTong[j] = Tong[j].ToString("N" + GlobalSettings.SoThapPhan.LuongNPL);
            //    }
            //}

            //lblTong1.Text = strTong[0];
            //lblTong2.Text = strTong[1];
            //lblTong3.Text = strTong[2];
            //lblTong4.Text = strTong[3];
            //lblTong5.Text = strTong[4];
            //lblTong6.Text = strTong[5];
            //lblTong7.Text = strTong[6];

        }

        private void lblhopsoHDGC_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel label = (XRLabel)sender;
            report.Label = label;
            report.txtName.Text = label.Text;
            report.lblName.Text = label.Tag.ToString();
        }
        public void setText(XRLabel label, string text)
        {
            label.Text = text;
        }

    }
}
