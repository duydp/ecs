using System;
using System.Collections.Generic;
using System.Text;

namespace SaoChepChuyenDaDuyetMaHQKhac
{
    public partial class BaseForm
    {
        protected Janus.Windows.EditControls.UIGroupBox grbMain;
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.Common.JanusColorScheme janusColorScheme3 = new Janus.Windows.Common.JanusColorScheme();
            Janus.Windows.Common.JanusColorScheme janusColorScheme4 = new Janus.Windows.Common.JanusColorScheme();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            this.grbMain = new Janus.Windows.EditControls.UIGroupBox();
            this.vsmMain = new Janus.Windows.Common.VisualStyleManager(this.components);
            this.officeFormAdorner1 = new Janus.Windows.Ribbon.OfficeFormAdorner(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeFormAdorner1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.grbMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbMain.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.grbMain.Location = new System.Drawing.Point(0, 0);
            this.grbMain.Name = "grbMain";
            this.grbMain.Size = new System.Drawing.Size(460, 199);
            this.grbMain.TabIndex = 0;
            this.grbMain.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbMain.VisualStyleManager = this.vsmMain;
            // 
            // vsmMain
            // 
            janusColorScheme3.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme3.Name = "Office2007";
            janusColorScheme3.Office2007CustomColor = System.Drawing.Color.Empty;
            janusColorScheme3.VisualStyle = Janus.Windows.Common.VisualStyle.Office2007;
            janusColorScheme4.HighlightTextColor = System.Drawing.SystemColors.HighlightText;
            janusColorScheme4.Name = "Office2003";
            janusColorScheme4.Office2007CustomColor = System.Drawing.Color.Empty;
            this.vsmMain.ColorSchemes.Add(janusColorScheme3);
            this.vsmMain.ColorSchemes.Add(janusColorScheme4);
            this.vsmMain.DefaultColorScheme = "Office2003";
            // 
            // officeFormAdorner1
            // 
            this.officeFormAdorner1.Form = this;
            this.officeFormAdorner1.Office2007CustomColor = System.Drawing.Color.Empty;
            this.officeFormAdorner1.TitleBarFont = new System.Drawing.Font("Tahoma", 8.5F, System.Drawing.FontStyle.Bold);
            // 
            // BaseForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(460, 199);
            this.Controls.Add(this.grbMain);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BaseForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Tag = "";
            this.Text = "BaseForm";
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.officeFormAdorner1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Janus.Windows.Common.VisualStyleManager vsmMain;
        private Janus.Windows.Ribbon.OfficeFormAdorner officeFormAdorner1;

    }
}
