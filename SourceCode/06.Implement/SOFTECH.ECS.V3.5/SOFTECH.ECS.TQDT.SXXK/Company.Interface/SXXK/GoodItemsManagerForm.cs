﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
namespace Company.Interface.SXXK
{
    public partial class GoodItemsManagerForm : BaseForm
    {
        public string where = "";
        public GoodItemsManagerForm()
        {
            InitializeComponent();
        }
        private void GoodItemsManagerForm_Load(object sender, EventArgs e)
        {
            cbStatus.SelectedValue = 0;
            btnSearch_Click(null,null);
        }
        public void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = KDT_VNACCS_BaoCaoQuyetToan_NPLSP.SelectCollectionDynamic(GetSearchWhere(),"");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1=1";
                //if (!String.IsNullOrEmpty(txtSoTiepNhan.Text))
                //    where += " AND SoTN = " + txtSoTiepNhan.Text;
                //if (!String.IsNullOrEmpty(txtNamQT.Text))
                //    where += " AND NamQuyetToan = " + txtNamQT.Text;
                //if (!String.IsNullOrEmpty(cbStatus.SelectedValue.ToString()))
                //    where += " AND TrangThaiXuLy =" + cbStatus.SelectedValue;
                //if (!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Ma))
                //    where += " AND MaHQ = '" + ctrCoQuanHaiQuan.Ma + "'";
                return where;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = KDT_VNACCS_BaoCaoQuyetToan_NPLSP.SelectCollectionDynamic(GetSearchWhere(), "ID");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }
        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP();
                goodItem = (KDT_VNACCS_BaoCaoQuyetToan_NPLSP)e.Row.DataRow;
                Mau15ChiTietForm f = new Mau15ChiTietForm();
                f.IsActive = true;
                f.goodItem = goodItem;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                int LoaiHinh = Convert.ToInt32(e.Row.Cells["LoaiHinhBaoCao"].Value);
                int DVT = Convert.ToInt32(e.Row.Cells["DVTBaoCao"].Value);
                if (LoaiHinh == 1)
                {
                    e.Row.Cells["LoaiHinhBaoCao"].Text = "Gia công";
                }
                else
                {
                    e.Row.Cells["LoaiHinhBaoCao"].Text = "Sản xuất xuất khẩu";
                }

                if (DVT == 1)
                {
                    e.Row.Cells["DVTBaoCao"].Text = "Trị giá";
                }
                else
                {
                    e.Row.Cells["DVTBaoCao"].Text = "Số lượng";
                }

                string TrangThai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (TrangThai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;

                }
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (ShowMessage("Bạn có muốn xóa dòng hàng này không ?", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        KDT_VNACCS_BaoCaoQuyetToan_NPLSP goodItem = new KDT_VNACCS_BaoCaoQuyetToan_NPLSP();
                        goodItem = (KDT_VNACCS_BaoCaoQuyetToan_NPLSP)i.GetRow().DataRow;
                        goodItem.GoodItemsCollection = KDT_VNACCS_BaoCaoQuyetToan_NPLSP_Detail.SelectCollectionBy_GoodItem_ID(goodItem.ID);
                        if (!String.IsNullOrEmpty(goodItem.GuidStr))
                        {
                            ShowMessage("Báo cáo quyết toán này đã gửi lên HQ nên không được xóa !", false);
                        }
                        else
                        {
                            goodItem.DeleteFull();
                            //HoSoQuyetToan_DSHopDong.DeleteDynamic("Master_ID =" + goodItem.ID);
                        }
                    }
                }
                ShowMessage("Xóa thành công", false);
                BindData();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {

        }


    }
}
