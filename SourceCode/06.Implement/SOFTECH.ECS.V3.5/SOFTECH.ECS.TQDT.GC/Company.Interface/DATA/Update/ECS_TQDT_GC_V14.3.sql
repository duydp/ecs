/****** Object:  Table [dbo].[t_KDT_VNACC_ChuyenCuaKhau]    Script Date: 02/17/2014 10:36:22 ******/
if not exists (select * from sysobjects where name='t_KDT_VNACC_ToKhaiVanChuyen' and xtype='U')
Begin
CREATE TABLE [dbo].[t_KDT_VNACC_ToKhaiVanChuyen](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TKMD_ID] [bigint] NULL,
	[NgayDangKy] [datetime] NULL,
	[TenThongTinXuat] [nvarchar](43) NULL,
	[MaPhanLoaiXuLy] [varchar](1) NULL,
	[MaCoBaoNiemPhong] [varchar](1) NULL,
	[TenCoBaoNiemPhong] [nvarchar](35) NULL,
	[CoQuanHaiQuan] [varchar](10) NULL,
	[SoToKhaiVC] [numeric](12, 0) NULL,
	[CoBaoXuatNhapKhau] [varchar](1) NULL,
	[NgayLapToKhai] [datetime] NULL,
	[MaNguoiKhai] [varchar](5) NULL,
	[TenNguoiKhai] [varchar](50) NULL,
	[DiaChiNguoiKhai] [nvarchar](100) NULL,
	[MaNguoiVC] [varchar](13) NULL,
	[TenNguoiVC] [nvarchar](100) NULL,
	[DiaChiNguoiVC] [nvarchar](100) NULL,
	[SoHopDongVC] [varchar](11) NULL,
	[NgayHopDongVC] [datetime] NULL,
	[NgayHetHanHopDongVC] [datetime] NULL,
	[MaPhuongTienVC] [varchar](2) NULL,
	[TenPhuongTieVC] [varchar](12) NULL,
	[MaMucDichVC] [varchar](3) NULL,
	[TenMucDichVC] [nvarchar](70) NULL,
	[LoaiHinhVanTai] [varchar](2) NULL,
	[TenLoaiHinhVanTai] [nvarchar](70) NULL,
	[MaDiaDiemXepHang] [varchar](7) NULL,
	[MaViTriXepHang] [varchar](6) NULL,
	[MaCangCuaKhauGaXepHang] [varchar](6) NULL,
	[MaCangXHKhongCo_HT] [varchar](1) NULL,
	[DiaDiemXepHang] [nvarchar](35) NULL,
	[NgayDenDiaDiem_XH] [datetime] NULL,
	[MaDiaDiemDoHang] [varchar](7) NULL,
	[MaViTriDoHang] [varchar](6) NULL,
	[MaCangCuaKhauGaDoHang] [varchar](6) NULL,
	[MaCangDHKhongCo_HT] [varchar](1) NULL,
	[DiaDiemDoHang] [nvarchar](35) NULL,
	[NgayDenDiaDiem_DH] [datetime] NULL,
	[TuyenDuongVC] [varchar](35) NULL,
	[LoaiBaoLanh] [varchar](1) NULL,
	[SoTienBaoLanh] [numeric](11, 0) NULL,
	[SoLuongCot_TK] [int] NULL,
	[SoLuongContainer] [int] NULL,
	[MaNganHangBaoLanh] [varchar](11) NULL,
	[NamPhatHanhBaoLanh] [int] NULL,
	[KyHieuChungTuBaoLanh] [varchar](10) NULL,
	[SoChungTuBaoLanh] [varchar](10) NULL,
	[MaVach] [numeric](12, 0) NULL,
	[NgayPheDuyetVC] [datetime] NULL,
	[NgayDuKienBatDauVC] [datetime] NULL,
	[GioDuKienBatDauVC] [int] NULL,
	[NgayDuKienKetThucVC] [datetime] NULL,
	[GioDuKienKetThucVC] [int] NULL,
	[MaBuuChinhHQ] [varchar](7) NULL,
	[DiaChiBuuChinhHQ] [nvarchar](54) NULL,
	[TenBuuChinhHQ] [nvarchar](34) NULL,
	[GhiChu] [nvarchar](255) NULL,
	[TrangThaiXuLy] [varchar](2) NULL,
	[InputMessageID] [varchar](10) NULL,
	[MessageTag] [varchar](26) NULL,
	[IndexTag] [varchar](100) NULL,
 CONSTRAINT [PK_t_KDT_VNACC_ToKhaiVanChuyen] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
end


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Insert]
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@TenThongTinXuat nvarchar(43),
	@MaPhanLoaiXuLy varchar(1),
	@MaCoBaoNiemPhong varchar(1),
	@TenCoBaoNiemPhong nvarchar(35),
	@CoQuanHaiQuan varchar(10),
	@SoToKhaiVC numeric(12, 0),
	@CoBaoXuatNhapKhau varchar(1),
	@NgayLapToKhai datetime,
	@MaNguoiKhai varchar(5),
	@TenNguoiKhai varchar(50),
	@DiaChiNguoiKhai nvarchar(100),
	@MaNguoiVC varchar(13),
	@TenNguoiVC nvarchar(100),
	@DiaChiNguoiVC nvarchar(100),
	@SoHopDongVC varchar(11),
	@NgayHopDongVC datetime,
	@NgayHetHanHopDongVC datetime,
	@MaPhuongTienVC varchar(2),
	@TenPhuongTieVC varchar(12),
	@MaMucDichVC varchar(3),
	@TenMucDichVC nvarchar(70),
	@LoaiHinhVanTai varchar(2),
	@TenLoaiHinhVanTai nvarchar(70),
	@MaDiaDiemXepHang varchar(7),
	@MaViTriXepHang varchar(6),
	@MaCangCuaKhauGaXepHang varchar(6),
	@MaCangXHKhongCo_HT varchar(1),
	@DiaDiemXepHang nvarchar(35),
	@NgayDenDiaDiem_XH datetime,
	@MaDiaDiemDoHang varchar(7),
	@MaViTriDoHang varchar(6),
	@MaCangCuaKhauGaDoHang varchar(6),
	@MaCangDHKhongCo_HT varchar(1),
	@DiaDiemDoHang nvarchar(35),
	@NgayDenDiaDiem_DH datetime,
	@TuyenDuongVC varchar(35),
	@LoaiBaoLanh varchar(1),
	@SoTienBaoLanh numeric(11, 0),
	@SoLuongCot_TK int,
	@SoLuongContainer int,
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh int,
	@KyHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@MaVach numeric(12, 0),
	@NgayPheDuyetVC datetime,
	@NgayDuKienBatDauVC datetime,
	@GioDuKienBatDauVC int,
	@NgayDuKienKetThucVC datetime,
	@GioDuKienKetThucVC int,
	@MaBuuChinhHQ varchar(7),
	@DiaChiBuuChinhHQ nvarchar(54),
	@TenBuuChinhHQ nvarchar(34),
	@GhiChu nvarchar(255),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
(
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@TKMD_ID,
	@NgayDangKy,
	@TenThongTinXuat,
	@MaPhanLoaiXuLy,
	@MaCoBaoNiemPhong,
	@TenCoBaoNiemPhong,
	@CoQuanHaiQuan,
	@SoToKhaiVC,
	@CoBaoXuatNhapKhau,
	@NgayLapToKhai,
	@MaNguoiKhai,
	@TenNguoiKhai,
	@DiaChiNguoiKhai,
	@MaNguoiVC,
	@TenNguoiVC,
	@DiaChiNguoiVC,
	@SoHopDongVC,
	@NgayHopDongVC,
	@NgayHetHanHopDongVC,
	@MaPhuongTienVC,
	@TenPhuongTieVC,
	@MaMucDichVC,
	@TenMucDichVC,
	@LoaiHinhVanTai,
	@TenLoaiHinhVanTai,
	@MaDiaDiemXepHang,
	@MaViTriXepHang,
	@MaCangCuaKhauGaXepHang,
	@MaCangXHKhongCo_HT,
	@DiaDiemXepHang,
	@NgayDenDiaDiem_XH,
	@MaDiaDiemDoHang,
	@MaViTriDoHang,
	@MaCangCuaKhauGaDoHang,
	@MaCangDHKhongCo_HT,
	@DiaDiemDoHang,
	@NgayDenDiaDiem_DH,
	@TuyenDuongVC,
	@LoaiBaoLanh,
	@SoTienBaoLanh,
	@SoLuongCot_TK,
	@SoLuongContainer,
	@MaNganHangBaoLanh,
	@NamPhatHanhBaoLanh,
	@KyHieuChungTuBaoLanh,
	@SoChungTuBaoLanh,
	@MaVach,
	@NgayPheDuyetVC,
	@NgayDuKienBatDauVC,
	@GioDuKienBatDauVC,
	@NgayDuKienKetThucVC,
	@GioDuKienKetThucVC,
	@MaBuuChinhHQ,
	@DiaChiBuuChinhHQ,
	@TenBuuChinhHQ,
	@GhiChu,
	@TrangThaiXuLy,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Update]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@TenThongTinXuat nvarchar(43),
	@MaPhanLoaiXuLy varchar(1),
	@MaCoBaoNiemPhong varchar(1),
	@TenCoBaoNiemPhong nvarchar(35),
	@CoQuanHaiQuan varchar(10),
	@SoToKhaiVC numeric(12, 0),
	@CoBaoXuatNhapKhau varchar(1),
	@NgayLapToKhai datetime,
	@MaNguoiKhai varchar(5),
	@TenNguoiKhai varchar(50),
	@DiaChiNguoiKhai nvarchar(100),
	@MaNguoiVC varchar(13),
	@TenNguoiVC nvarchar(100),
	@DiaChiNguoiVC nvarchar(100),
	@SoHopDongVC varchar(11),
	@NgayHopDongVC datetime,
	@NgayHetHanHopDongVC datetime,
	@MaPhuongTienVC varchar(2),
	@TenPhuongTieVC varchar(12),
	@MaMucDichVC varchar(3),
	@TenMucDichVC nvarchar(70),
	@LoaiHinhVanTai varchar(2),
	@TenLoaiHinhVanTai nvarchar(70),
	@MaDiaDiemXepHang varchar(7),
	@MaViTriXepHang varchar(6),
	@MaCangCuaKhauGaXepHang varchar(6),
	@MaCangXHKhongCo_HT varchar(1),
	@DiaDiemXepHang nvarchar(35),
	@NgayDenDiaDiem_XH datetime,
	@MaDiaDiemDoHang varchar(7),
	@MaViTriDoHang varchar(6),
	@MaCangCuaKhauGaDoHang varchar(6),
	@MaCangDHKhongCo_HT varchar(1),
	@DiaDiemDoHang nvarchar(35),
	@NgayDenDiaDiem_DH datetime,
	@TuyenDuongVC varchar(35),
	@LoaiBaoLanh varchar(1),
	@SoTienBaoLanh numeric(11, 0),
	@SoLuongCot_TK int,
	@SoLuongContainer int,
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh int,
	@KyHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@MaVach numeric(12, 0),
	@NgayPheDuyetVC datetime,
	@NgayDuKienBatDauVC datetime,
	@GioDuKienBatDauVC int,
	@NgayDuKienKetThucVC datetime,
	@GioDuKienKetThucVC int,
	@MaBuuChinhHQ varchar(7),
	@DiaChiBuuChinhHQ nvarchar(54),
	@TenBuuChinhHQ nvarchar(34),
	@GhiChu nvarchar(255),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
SET
	[TKMD_ID] = @TKMD_ID,
	[NgayDangKy] = @NgayDangKy,
	[TenThongTinXuat] = @TenThongTinXuat,
	[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
	[MaCoBaoNiemPhong] = @MaCoBaoNiemPhong,
	[TenCoBaoNiemPhong] = @TenCoBaoNiemPhong,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[SoToKhaiVC] = @SoToKhaiVC,
	[CoBaoXuatNhapKhau] = @CoBaoXuatNhapKhau,
	[NgayLapToKhai] = @NgayLapToKhai,
	[MaNguoiKhai] = @MaNguoiKhai,
	[TenNguoiKhai] = @TenNguoiKhai,
	[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
	[MaNguoiVC] = @MaNguoiVC,
	[TenNguoiVC] = @TenNguoiVC,
	[DiaChiNguoiVC] = @DiaChiNguoiVC,
	[SoHopDongVC] = @SoHopDongVC,
	[NgayHopDongVC] = @NgayHopDongVC,
	[NgayHetHanHopDongVC] = @NgayHetHanHopDongVC,
	[MaPhuongTienVC] = @MaPhuongTienVC,
	[TenPhuongTieVC] = @TenPhuongTieVC,
	[MaMucDichVC] = @MaMucDichVC,
	[TenMucDichVC] = @TenMucDichVC,
	[LoaiHinhVanTai] = @LoaiHinhVanTai,
	[TenLoaiHinhVanTai] = @TenLoaiHinhVanTai,
	[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
	[MaViTriXepHang] = @MaViTriXepHang,
	[MaCangCuaKhauGaXepHang] = @MaCangCuaKhauGaXepHang,
	[MaCangXHKhongCo_HT] = @MaCangXHKhongCo_HT,
	[DiaDiemXepHang] = @DiaDiemXepHang,
	[NgayDenDiaDiem_XH] = @NgayDenDiaDiem_XH,
	[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
	[MaViTriDoHang] = @MaViTriDoHang,
	[MaCangCuaKhauGaDoHang] = @MaCangCuaKhauGaDoHang,
	[MaCangDHKhongCo_HT] = @MaCangDHKhongCo_HT,
	[DiaDiemDoHang] = @DiaDiemDoHang,
	[NgayDenDiaDiem_DH] = @NgayDenDiaDiem_DH,
	[TuyenDuongVC] = @TuyenDuongVC,
	[LoaiBaoLanh] = @LoaiBaoLanh,
	[SoTienBaoLanh] = @SoTienBaoLanh,
	[SoLuongCot_TK] = @SoLuongCot_TK,
	[SoLuongContainer] = @SoLuongContainer,
	[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
	[NamPhatHanhBaoLanh] = @NamPhatHanhBaoLanh,
	[KyHieuChungTuBaoLanh] = @KyHieuChungTuBaoLanh,
	[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
	[MaVach] = @MaVach,
	[NgayPheDuyetVC] = @NgayPheDuyetVC,
	[NgayDuKienBatDauVC] = @NgayDuKienBatDauVC,
	[GioDuKienBatDauVC] = @GioDuKienBatDauVC,
	[NgayDuKienKetThucVC] = @NgayDuKienKetThucVC,
	[GioDuKienKetThucVC] = @GioDuKienKetThucVC,
	[MaBuuChinhHQ] = @MaBuuChinhHQ,
	[DiaChiBuuChinhHQ] = @DiaChiBuuChinhHQ,
	[TenBuuChinhHQ] = @TenBuuChinhHQ,
	[GhiChu] = @GhiChu,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_InsertUpdate]
	@ID bigint,
	@TKMD_ID bigint,
	@NgayDangKy datetime,
	@TenThongTinXuat nvarchar(43),
	@MaPhanLoaiXuLy varchar(1),
	@MaCoBaoNiemPhong varchar(1),
	@TenCoBaoNiemPhong nvarchar(35),
	@CoQuanHaiQuan varchar(10),
	@SoToKhaiVC numeric(12, 0),
	@CoBaoXuatNhapKhau varchar(1),
	@NgayLapToKhai datetime,
	@MaNguoiKhai varchar(5),
	@TenNguoiKhai varchar(50),
	@DiaChiNguoiKhai nvarchar(100),
	@MaNguoiVC varchar(13),
	@TenNguoiVC nvarchar(100),
	@DiaChiNguoiVC nvarchar(100),
	@SoHopDongVC varchar(11),
	@NgayHopDongVC datetime,
	@NgayHetHanHopDongVC datetime,
	@MaPhuongTienVC varchar(2),
	@TenPhuongTieVC varchar(12),
	@MaMucDichVC varchar(3),
	@TenMucDichVC nvarchar(70),
	@LoaiHinhVanTai varchar(2),
	@TenLoaiHinhVanTai nvarchar(70),
	@MaDiaDiemXepHang varchar(7),
	@MaViTriXepHang varchar(6),
	@MaCangCuaKhauGaXepHang varchar(6),
	@MaCangXHKhongCo_HT varchar(1),
	@DiaDiemXepHang nvarchar(35),
	@NgayDenDiaDiem_XH datetime,
	@MaDiaDiemDoHang varchar(7),
	@MaViTriDoHang varchar(6),
	@MaCangCuaKhauGaDoHang varchar(6),
	@MaCangDHKhongCo_HT varchar(1),
	@DiaDiemDoHang nvarchar(35),
	@NgayDenDiaDiem_DH datetime,
	@TuyenDuongVC varchar(35),
	@LoaiBaoLanh varchar(1),
	@SoTienBaoLanh numeric(11, 0),
	@SoLuongCot_TK int,
	@SoLuongContainer int,
	@MaNganHangBaoLanh varchar(11),
	@NamPhatHanhBaoLanh int,
	@KyHieuChungTuBaoLanh varchar(10),
	@SoChungTuBaoLanh varchar(10),
	@MaVach numeric(12, 0),
	@NgayPheDuyetVC datetime,
	@NgayDuKienBatDauVC datetime,
	@GioDuKienBatDauVC int,
	@NgayDuKienKetThucVC datetime,
	@GioDuKienKetThucVC int,
	@MaBuuChinhHQ varchar(7),
	@DiaChiBuuChinhHQ nvarchar(54),
	@TenBuuChinhHQ nvarchar(34),
	@GhiChu nvarchar(255),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_ToKhaiVanChuyen] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_ToKhaiVanChuyen] 
		SET
			[TKMD_ID] = @TKMD_ID,
			[NgayDangKy] = @NgayDangKy,
			[TenThongTinXuat] = @TenThongTinXuat,
			[MaPhanLoaiXuLy] = @MaPhanLoaiXuLy,
			[MaCoBaoNiemPhong] = @MaCoBaoNiemPhong,
			[TenCoBaoNiemPhong] = @TenCoBaoNiemPhong,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[SoToKhaiVC] = @SoToKhaiVC,
			[CoBaoXuatNhapKhau] = @CoBaoXuatNhapKhau,
			[NgayLapToKhai] = @NgayLapToKhai,
			[MaNguoiKhai] = @MaNguoiKhai,
			[TenNguoiKhai] = @TenNguoiKhai,
			[DiaChiNguoiKhai] = @DiaChiNguoiKhai,
			[MaNguoiVC] = @MaNguoiVC,
			[TenNguoiVC] = @TenNguoiVC,
			[DiaChiNguoiVC] = @DiaChiNguoiVC,
			[SoHopDongVC] = @SoHopDongVC,
			[NgayHopDongVC] = @NgayHopDongVC,
			[NgayHetHanHopDongVC] = @NgayHetHanHopDongVC,
			[MaPhuongTienVC] = @MaPhuongTienVC,
			[TenPhuongTieVC] = @TenPhuongTieVC,
			[MaMucDichVC] = @MaMucDichVC,
			[TenMucDichVC] = @TenMucDichVC,
			[LoaiHinhVanTai] = @LoaiHinhVanTai,
			[TenLoaiHinhVanTai] = @TenLoaiHinhVanTai,
			[MaDiaDiemXepHang] = @MaDiaDiemXepHang,
			[MaViTriXepHang] = @MaViTriXepHang,
			[MaCangCuaKhauGaXepHang] = @MaCangCuaKhauGaXepHang,
			[MaCangXHKhongCo_HT] = @MaCangXHKhongCo_HT,
			[DiaDiemXepHang] = @DiaDiemXepHang,
			[NgayDenDiaDiem_XH] = @NgayDenDiaDiem_XH,
			[MaDiaDiemDoHang] = @MaDiaDiemDoHang,
			[MaViTriDoHang] = @MaViTriDoHang,
			[MaCangCuaKhauGaDoHang] = @MaCangCuaKhauGaDoHang,
			[MaCangDHKhongCo_HT] = @MaCangDHKhongCo_HT,
			[DiaDiemDoHang] = @DiaDiemDoHang,
			[NgayDenDiaDiem_DH] = @NgayDenDiaDiem_DH,
			[TuyenDuongVC] = @TuyenDuongVC,
			[LoaiBaoLanh] = @LoaiBaoLanh,
			[SoTienBaoLanh] = @SoTienBaoLanh,
			[SoLuongCot_TK] = @SoLuongCot_TK,
			[SoLuongContainer] = @SoLuongContainer,
			[MaNganHangBaoLanh] = @MaNganHangBaoLanh,
			[NamPhatHanhBaoLanh] = @NamPhatHanhBaoLanh,
			[KyHieuChungTuBaoLanh] = @KyHieuChungTuBaoLanh,
			[SoChungTuBaoLanh] = @SoChungTuBaoLanh,
			[MaVach] = @MaVach,
			[NgayPheDuyetVC] = @NgayPheDuyetVC,
			[NgayDuKienBatDauVC] = @NgayDuKienBatDauVC,
			[GioDuKienBatDauVC] = @GioDuKienBatDauVC,
			[NgayDuKienKetThucVC] = @NgayDuKienKetThucVC,
			[GioDuKienKetThucVC] = @GioDuKienKetThucVC,
			[MaBuuChinhHQ] = @MaBuuChinhHQ,
			[DiaChiBuuChinhHQ] = @DiaChiBuuChinhHQ,
			[TenBuuChinhHQ] = @TenBuuChinhHQ,
			[GhiChu] = @GhiChu,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
		(
			[TKMD_ID],
			[NgayDangKy],
			[TenThongTinXuat],
			[MaPhanLoaiXuLy],
			[MaCoBaoNiemPhong],
			[TenCoBaoNiemPhong],
			[CoQuanHaiQuan],
			[SoToKhaiVC],
			[CoBaoXuatNhapKhau],
			[NgayLapToKhai],
			[MaNguoiKhai],
			[TenNguoiKhai],
			[DiaChiNguoiKhai],
			[MaNguoiVC],
			[TenNguoiVC],
			[DiaChiNguoiVC],
			[SoHopDongVC],
			[NgayHopDongVC],
			[NgayHetHanHopDongVC],
			[MaPhuongTienVC],
			[TenPhuongTieVC],
			[MaMucDichVC],
			[TenMucDichVC],
			[LoaiHinhVanTai],
			[TenLoaiHinhVanTai],
			[MaDiaDiemXepHang],
			[MaViTriXepHang],
			[MaCangCuaKhauGaXepHang],
			[MaCangXHKhongCo_HT],
			[DiaDiemXepHang],
			[NgayDenDiaDiem_XH],
			[MaDiaDiemDoHang],
			[MaViTriDoHang],
			[MaCangCuaKhauGaDoHang],
			[MaCangDHKhongCo_HT],
			[DiaDiemDoHang],
			[NgayDenDiaDiem_DH],
			[TuyenDuongVC],
			[LoaiBaoLanh],
			[SoTienBaoLanh],
			[SoLuongCot_TK],
			[SoLuongContainer],
			[MaNganHangBaoLanh],
			[NamPhatHanhBaoLanh],
			[KyHieuChungTuBaoLanh],
			[SoChungTuBaoLanh],
			[MaVach],
			[NgayPheDuyetVC],
			[NgayDuKienBatDauVC],
			[GioDuKienBatDauVC],
			[NgayDuKienKetThucVC],
			[GioDuKienKetThucVC],
			[MaBuuChinhHQ],
			[DiaChiBuuChinhHQ],
			[TenBuuChinhHQ],
			[GhiChu],
			[TrangThaiXuLy],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@TKMD_ID,
			@NgayDangKy,
			@TenThongTinXuat,
			@MaPhanLoaiXuLy,
			@MaCoBaoNiemPhong,
			@TenCoBaoNiemPhong,
			@CoQuanHaiQuan,
			@SoToKhaiVC,
			@CoBaoXuatNhapKhau,
			@NgayLapToKhai,
			@MaNguoiKhai,
			@TenNguoiKhai,
			@DiaChiNguoiKhai,
			@MaNguoiVC,
			@TenNguoiVC,
			@DiaChiNguoiVC,
			@SoHopDongVC,
			@NgayHopDongVC,
			@NgayHetHanHopDongVC,
			@MaPhuongTienVC,
			@TenPhuongTieVC,
			@MaMucDichVC,
			@TenMucDichVC,
			@LoaiHinhVanTai,
			@TenLoaiHinhVanTai,
			@MaDiaDiemXepHang,
			@MaViTriXepHang,
			@MaCangCuaKhauGaXepHang,
			@MaCangXHKhongCo_HT,
			@DiaDiemXepHang,
			@NgayDenDiaDiem_XH,
			@MaDiaDiemDoHang,
			@MaViTriDoHang,
			@MaCangCuaKhauGaDoHang,
			@MaCangDHKhongCo_HT,
			@DiaDiemDoHang,
			@NgayDenDiaDiem_DH,
			@TuyenDuongVC,
			@LoaiBaoLanh,
			@SoTienBaoLanh,
			@SoLuongCot_TK,
			@SoLuongContainer,
			@MaNganHangBaoLanh,
			@NamPhatHanhBaoLanh,
			@KyHieuChungTuBaoLanh,
			@SoChungTuBaoLanh,
			@MaVach,
			@NgayPheDuyetVC,
			@NgayDuKienBatDauVC,
			@GioDuKienBatDauVC,
			@NgayDuKienKetThucVC,
			@GioDuKienKetThucVC,
			@MaBuuChinhHQ,
			@DiaChiBuuChinhHQ,
			@TenBuuChinhHQ,
			@GhiChu,
			@TrangThaiXuLy,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_ToKhaiVanChuyen] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACC_ToKhaiVanChuyen] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_ToKhaiVanChuyen_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TKMD_ID],
	[NgayDangKy],
	[TenThongTinXuat],
	[MaPhanLoaiXuLy],
	[MaCoBaoNiemPhong],
	[TenCoBaoNiemPhong],
	[CoQuanHaiQuan],
	[SoToKhaiVC],
	[CoBaoXuatNhapKhau],
	[NgayLapToKhai],
	[MaNguoiKhai],
	[TenNguoiKhai],
	[DiaChiNguoiKhai],
	[MaNguoiVC],
	[TenNguoiVC],
	[DiaChiNguoiVC],
	[SoHopDongVC],
	[NgayHopDongVC],
	[NgayHetHanHopDongVC],
	[MaPhuongTienVC],
	[TenPhuongTieVC],
	[MaMucDichVC],
	[TenMucDichVC],
	[LoaiHinhVanTai],
	[TenLoaiHinhVanTai],
	[MaDiaDiemXepHang],
	[MaViTriXepHang],
	[MaCangCuaKhauGaXepHang],
	[MaCangXHKhongCo_HT],
	[DiaDiemXepHang],
	[NgayDenDiaDiem_XH],
	[MaDiaDiemDoHang],
	[MaViTriDoHang],
	[MaCangCuaKhauGaDoHang],
	[MaCangDHKhongCo_HT],
	[DiaDiemDoHang],
	[NgayDenDiaDiem_DH],
	[TuyenDuongVC],
	[LoaiBaoLanh],
	[SoTienBaoLanh],
	[SoLuongCot_TK],
	[SoLuongContainer],
	[MaNganHangBaoLanh],
	[NamPhatHanhBaoLanh],
	[KyHieuChungTuBaoLanh],
	[SoChungTuBaoLanh],
	[MaVach],
	[NgayPheDuyetVC],
	[NgayDuKienBatDauVC],
	[GioDuKienBatDauVC],
	[NgayDuKienKetThucVC],
	[GioDuKienKetThucVC],
	[MaBuuChinhHQ],
	[DiaChiBuuChinhHQ],
	[TenBuuChinhHQ],
	[GhiChu],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACC_ToKhaiVanChuyen]	

GO




--------------------------------------------------------------------------T_KDT_VNACC_TKVC_VANDON-----------
--------------------------------------------------------------------------
if not exists (select * from sysobjects where name='t_KDT_VNACC_TKVC_VanDon' and xtype='U')
Begin



CREATE TABLE [dbo].[t_KDT_VNACC_TKVC_VanDon](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[SoTTVanDon] [varchar](1) NULL,
	[SoVanDon] [varchar](35) NULL,
	[NgayPhatHanhVD] [datetime] NULL,
	[MoTaHangHoa] [nvarchar](70) NULL,
	[MaHS] [varchar](4) NULL,
	[KyHieuVaSoHieu] [varchar](140) NULL,
	[NgayNhapKhoHQLanDau] [datetime] NULL,
	[PhanLoaiSanPhan] [varchar](1) NULL,
	[MaNuocSanXuat] [varchar](2) NULL,
	[TenNuocSanXuat] [varchar](7) NULL,
	[MaDiaDiemXuatPhatVC] [varchar](6) NULL,
	[TenDiaDiemXuatPhatVC] [varchar](35) NULL,
	[MaDiaDiemDichVC] [varchar](6) NULL,
	[TenDiaDiemDichVC] [varchar](35) NULL,
	[LoaiHangHoa] [varchar](1) NULL,
	[MaPhuongTienVC] [varchar](40) NULL,
	[TenPhuongTienVC] [varchar](40) NULL,
	[NgayHangDuKienDenDi] [datetime] NULL,
	[MaNguoiNhapKhau] [varchar](13) NULL,
	[TenNguoiNhapKhau] [nvarchar](100) NULL,
	[DiaChiNguoiNhapKhau] [nvarchar](100) NULL,
	[MaNguoiXuatKhua] [varchar](13) NULL,
	[TenNguoiXuatKhau] [nvarchar](100) NULL,
	[DiaChiNguoiXuatKhau] [nvarchar](100) NULL,
	[MaNguoiUyThac] [varchar](13) NULL,
	[TenNguoiUyThac] [nvarchar](100) NULL,
	[DiaChiNguoiUyThac] [nvarchar](100) NULL,
	[MaVanBanPhapLuat1] [varchar](2) NULL,
	[MaVanBanPhapLuat2] [varchar](2) NULL,
	[MaVanBanPhapLuat3] [varchar](2) NULL,
	[MaVanBanPhapLuat4] [varchar](2) NULL,
	[MaVanBanPhapLuat5] [varchar](2) NULL,
	[MaDVTTriGia] [varchar](3) NULL,
	[TriGia] [numeric](20, 4) NULL,
	[SoLuong] [numeric](8, 4) NULL,
	[MaDVTSoLuong] [varchar](3) NULL,
	[TongTrongLuong] [numeric](10, 4) NULL,
	[MaDVTTrongLuong] [varchar](3) NULL,
	[TheTich] [numeric](10, 4) NULL,
	[MaDVTTheTich] [varchar](3) NULL,
	[MaDanhDauDDKH1] [varchar](5) NULL,
	[MaDanhDauDDKH2] [varchar](5) NULL,
	[MaDanhDauDDKH3] [varchar](5) NULL,
	[MaDanhDauDDKH4] [varchar](5) NULL,
	[MaDanhDauDDKH5] [varchar](5) NULL,
	[SoGiayPhep] [varchar](11) NULL,
	[NgayCapPhep] [datetime] NULL,
	[NgayHetHanCapPhep] [datetime] NULL,
	[GhiChu] [nvarchar](255) NULL,

 CONSTRAINT [PK_t_KDT_VNACC_TKVC_VanDon] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

end

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_VanDon_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Insert]
	@Master_ID bigint,
	@SoTTVanDon varchar(1),
	@SoVanDon varchar(35),
	@NgayPhatHanhVD datetime,
	@MoTaHangHoa nvarchar(70),
	@MaHS varchar(4),
	@KyHieuVaSoHieu varchar(140),
	@NgayNhapKhoHQLanDau datetime,
	@PhanLoaiSanPhan varchar(1),
	@MaNuocSanXuat varchar(2),
	@TenNuocSanXuat varchar(7),
	@MaDiaDiemXuatPhatVC varchar(6),
	@TenDiaDiemXuatPhatVC varchar(35),
	@MaDiaDiemDichVC varchar(6),
	@TenDiaDiemDichVC varchar(35),
	@LoaiHangHoa varchar(1),
	@MaPhuongTienVC varchar(40),
	@TenPhuongTienVC varchar(40),
	@NgayHangDuKienDenDi datetime,
	@MaNguoiNhapKhau varchar(13),
	@TenNguoiNhapKhau nvarchar(100),
	@DiaChiNguoiNhapKhau nvarchar(100),
	@MaNguoiXuatKhua varchar(13),
	@TenNguoiXuatKhau nvarchar(100),
	@DiaChiNguoiXuatKhau nvarchar(100),
	@MaNguoiUyThac varchar(13),
	@TenNguoiUyThac nvarchar(100),
	@DiaChiNguoiUyThac nvarchar(100),
	@MaVanBanPhapLuat1 varchar(2),
	@MaVanBanPhapLuat2 varchar(2),
	@MaVanBanPhapLuat3 varchar(2),
	@MaVanBanPhapLuat4 varchar(2),
	@MaVanBanPhapLuat5 varchar(2),
	@MaDVTTriGia varchar(3),
	@TriGia numeric(20, 4),
	@SoLuong numeric(8, 4),
	@MaDVTSoLuong varchar(3),
	@TongTrongLuong numeric(10, 4),
	@MaDVTTrongLuong varchar(3),
	@TheTich numeric(10, 4),
	@MaDVTTheTich varchar(3),
	@MaDanhDauDDKH1 varchar(5),
	@MaDanhDauDDKH2 varchar(5),
	@MaDanhDauDDKH3 varchar(5),
	@MaDanhDauDDKH4 varchar(5),
	@MaDanhDauDDKH5 varchar(5),
	@SoGiayPhep varchar(11),
	@NgayCapPhep datetime,
	@NgayHetHanCapPhep datetime,
	@GhiChu nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TKVC_VanDon]
(
	[Master_ID],
	[SoTTVanDon],
	[SoVanDon],
	[NgayPhatHanhVD],
	[MoTaHangHoa],
	[MaHS],
	[KyHieuVaSoHieu],
	[NgayNhapKhoHQLanDau],
	[PhanLoaiSanPhan],
	[MaNuocSanXuat],
	[TenNuocSanXuat],
	[MaDiaDiemXuatPhatVC],
	[TenDiaDiemXuatPhatVC],
	[MaDiaDiemDichVC],
	[TenDiaDiemDichVC],
	[LoaiHangHoa],
	[MaPhuongTienVC],
	[TenPhuongTienVC],
	[NgayHangDuKienDenDi],
	[MaNguoiNhapKhau],
	[TenNguoiNhapKhau],
	[DiaChiNguoiNhapKhau],
	[MaNguoiXuatKhua],
	[TenNguoiXuatKhau],
	[DiaChiNguoiXuatKhau],
	[MaNguoiUyThac],
	[TenNguoiUyThac],
	[DiaChiNguoiUyThac],
	[MaVanBanPhapLuat1],
	[MaVanBanPhapLuat2],
	[MaVanBanPhapLuat3],
	[MaVanBanPhapLuat4],
	[MaVanBanPhapLuat5],
	[MaDVTTriGia],
	[TriGia],
	[SoLuong],
	[MaDVTSoLuong],
	[TongTrongLuong],
	[MaDVTTrongLuong],
	[TheTich],
	[MaDVTTheTich],
	[MaDanhDauDDKH1],
	[MaDanhDauDDKH2],
	[MaDanhDauDDKH3],
	[MaDanhDauDDKH4],
	[MaDanhDauDDKH5],
	[SoGiayPhep],
	[NgayCapPhep],
	[NgayHetHanCapPhep],
	[GhiChu]
)
VALUES 
(
	@Master_ID,
	@SoTTVanDon,
	@SoVanDon,
	@NgayPhatHanhVD,
	@MoTaHangHoa,
	@MaHS,
	@KyHieuVaSoHieu,
	@NgayNhapKhoHQLanDau,
	@PhanLoaiSanPhan,
	@MaNuocSanXuat,
	@TenNuocSanXuat,
	@MaDiaDiemXuatPhatVC,
	@TenDiaDiemXuatPhatVC,
	@MaDiaDiemDichVC,
	@TenDiaDiemDichVC,
	@LoaiHangHoa,
	@MaPhuongTienVC,
	@TenPhuongTienVC,
	@NgayHangDuKienDenDi,
	@MaNguoiNhapKhau,
	@TenNguoiNhapKhau,
	@DiaChiNguoiNhapKhau,
	@MaNguoiXuatKhua,
	@TenNguoiXuatKhau,
	@DiaChiNguoiXuatKhau,
	@MaNguoiUyThac,
	@TenNguoiUyThac,
	@DiaChiNguoiUyThac,
	@MaVanBanPhapLuat1,
	@MaVanBanPhapLuat2,
	@MaVanBanPhapLuat3,
	@MaVanBanPhapLuat4,
	@MaVanBanPhapLuat5,
	@MaDVTTriGia,
	@TriGia,
	@SoLuong,
	@MaDVTSoLuong,
	@TongTrongLuong,
	@MaDVTTrongLuong,
	@TheTich,
	@MaDVTTheTich,
	@MaDanhDauDDKH1,
	@MaDanhDauDDKH2,
	@MaDanhDauDDKH3,
	@MaDanhDauDDKH4,
	@MaDanhDauDDKH5,
	@SoGiayPhep,
	@NgayCapPhep,
	@NgayHetHanCapPhep,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Update]
	@ID bigint,
	@Master_ID bigint,
	@SoTTVanDon varchar(1),
	@SoVanDon varchar(35),
	@NgayPhatHanhVD datetime,
	@MoTaHangHoa nvarchar(70),
	@MaHS varchar(4),
	@KyHieuVaSoHieu varchar(140),
	@NgayNhapKhoHQLanDau datetime,
	@PhanLoaiSanPhan varchar(1),
	@MaNuocSanXuat varchar(2),
	@TenNuocSanXuat varchar(7),
	@MaDiaDiemXuatPhatVC varchar(6),
	@TenDiaDiemXuatPhatVC varchar(35),
	@MaDiaDiemDichVC varchar(6),
	@TenDiaDiemDichVC varchar(35),
	@LoaiHangHoa varchar(1),
	@MaPhuongTienVC varchar(40),
	@TenPhuongTienVC varchar(40),
	@NgayHangDuKienDenDi datetime,
	@MaNguoiNhapKhau varchar(13),
	@TenNguoiNhapKhau nvarchar(100),
	@DiaChiNguoiNhapKhau nvarchar(100),
	@MaNguoiXuatKhua varchar(13),
	@TenNguoiXuatKhau nvarchar(100),
	@DiaChiNguoiXuatKhau nvarchar(100),
	@MaNguoiUyThac varchar(13),
	@TenNguoiUyThac nvarchar(100),
	@DiaChiNguoiUyThac nvarchar(100),
	@MaVanBanPhapLuat1 varchar(2),
	@MaVanBanPhapLuat2 varchar(2),
	@MaVanBanPhapLuat3 varchar(2),
	@MaVanBanPhapLuat4 varchar(2),
	@MaVanBanPhapLuat5 varchar(2),
	@MaDVTTriGia varchar(3),
	@TriGia numeric(20, 4),
	@SoLuong numeric(8, 4),
	@MaDVTSoLuong varchar(3),
	@TongTrongLuong numeric(10, 4),
	@MaDVTTrongLuong varchar(3),
	@TheTich numeric(10, 4),
	@MaDVTTheTich varchar(3),
	@MaDanhDauDDKH1 varchar(5),
	@MaDanhDauDDKH2 varchar(5),
	@MaDanhDauDDKH3 varchar(5),
	@MaDanhDauDDKH4 varchar(5),
	@MaDanhDauDDKH5 varchar(5),
	@SoGiayPhep varchar(11),
	@NgayCapPhep datetime,
	@NgayHetHanCapPhep datetime,
	@GhiChu nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TKVC_VanDon]
SET
	[Master_ID] = @Master_ID,
	[SoTTVanDon] = @SoTTVanDon,
	[SoVanDon] = @SoVanDon,
	[NgayPhatHanhVD] = @NgayPhatHanhVD,
	[MoTaHangHoa] = @MoTaHangHoa,
	[MaHS] = @MaHS,
	[KyHieuVaSoHieu] = @KyHieuVaSoHieu,
	[NgayNhapKhoHQLanDau] = @NgayNhapKhoHQLanDau,
	[PhanLoaiSanPhan] = @PhanLoaiSanPhan,
	[MaNuocSanXuat] = @MaNuocSanXuat,
	[TenNuocSanXuat] = @TenNuocSanXuat,
	[MaDiaDiemXuatPhatVC] = @MaDiaDiemXuatPhatVC,
	[TenDiaDiemXuatPhatVC] = @TenDiaDiemXuatPhatVC,
	[MaDiaDiemDichVC] = @MaDiaDiemDichVC,
	[TenDiaDiemDichVC] = @TenDiaDiemDichVC,
	[LoaiHangHoa] = @LoaiHangHoa,
	[MaPhuongTienVC] = @MaPhuongTienVC,
	[TenPhuongTienVC] = @TenPhuongTienVC,
	[NgayHangDuKienDenDi] = @NgayHangDuKienDenDi,
	[MaNguoiNhapKhau] = @MaNguoiNhapKhau,
	[TenNguoiNhapKhau] = @TenNguoiNhapKhau,
	[DiaChiNguoiNhapKhau] = @DiaChiNguoiNhapKhau,
	[MaNguoiXuatKhua] = @MaNguoiXuatKhua,
	[TenNguoiXuatKhau] = @TenNguoiXuatKhau,
	[DiaChiNguoiXuatKhau] = @DiaChiNguoiXuatKhau,
	[MaNguoiUyThac] = @MaNguoiUyThac,
	[TenNguoiUyThac] = @TenNguoiUyThac,
	[DiaChiNguoiUyThac] = @DiaChiNguoiUyThac,
	[MaVanBanPhapLuat1] = @MaVanBanPhapLuat1,
	[MaVanBanPhapLuat2] = @MaVanBanPhapLuat2,
	[MaVanBanPhapLuat3] = @MaVanBanPhapLuat3,
	[MaVanBanPhapLuat4] = @MaVanBanPhapLuat4,
	[MaVanBanPhapLuat5] = @MaVanBanPhapLuat5,
	[MaDVTTriGia] = @MaDVTTriGia,
	[TriGia] = @TriGia,
	[SoLuong] = @SoLuong,
	[MaDVTSoLuong] = @MaDVTSoLuong,
	[TongTrongLuong] = @TongTrongLuong,
	[MaDVTTrongLuong] = @MaDVTTrongLuong,
	[TheTich] = @TheTich,
	[MaDVTTheTich] = @MaDVTTheTich,
	[MaDanhDauDDKH1] = @MaDanhDauDDKH1,
	[MaDanhDauDDKH2] = @MaDanhDauDDKH2,
	[MaDanhDauDDKH3] = @MaDanhDauDDKH3,
	[MaDanhDauDDKH4] = @MaDanhDauDDKH4,
	[MaDanhDauDDKH5] = @MaDanhDauDDKH5,
	[SoGiayPhep] = @SoGiayPhep,
	[NgayCapPhep] = @NgayCapPhep,
	[NgayHetHanCapPhep] = @NgayHetHanCapPhep,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SoTTVanDon varchar(1),
	@SoVanDon varchar(35),
	@NgayPhatHanhVD datetime,
	@MoTaHangHoa nvarchar(70),
	@MaHS varchar(4),
	@KyHieuVaSoHieu varchar(140),
	@NgayNhapKhoHQLanDau datetime,
	@PhanLoaiSanPhan varchar(1),
	@MaNuocSanXuat varchar(2),
	@TenNuocSanXuat varchar(7),
	@MaDiaDiemXuatPhatVC varchar(6),
	@TenDiaDiemXuatPhatVC varchar(35),
	@MaDiaDiemDichVC varchar(6),
	@TenDiaDiemDichVC varchar(35),
	@LoaiHangHoa varchar(1),
	@MaPhuongTienVC varchar(40),
	@TenPhuongTienVC varchar(40),
	@NgayHangDuKienDenDi datetime,
	@MaNguoiNhapKhau varchar(13),
	@TenNguoiNhapKhau nvarchar(100),
	@DiaChiNguoiNhapKhau nvarchar(100),
	@MaNguoiXuatKhua varchar(13),
	@TenNguoiXuatKhau nvarchar(100),
	@DiaChiNguoiXuatKhau nvarchar(100),
	@MaNguoiUyThac varchar(13),
	@TenNguoiUyThac nvarchar(100),
	@DiaChiNguoiUyThac nvarchar(100),
	@MaVanBanPhapLuat1 varchar(2),
	@MaVanBanPhapLuat2 varchar(2),
	@MaVanBanPhapLuat3 varchar(2),
	@MaVanBanPhapLuat4 varchar(2),
	@MaVanBanPhapLuat5 varchar(2),
	@MaDVTTriGia varchar(3),
	@TriGia numeric(20, 4),
	@SoLuong numeric(8, 4),
	@MaDVTSoLuong varchar(3),
	@TongTrongLuong numeric(10, 4),
	@MaDVTTrongLuong varchar(3),
	@TheTich numeric(10, 4),
	@MaDVTTheTich varchar(3),
	@MaDanhDauDDKH1 varchar(5),
	@MaDanhDauDDKH2 varchar(5),
	@MaDanhDauDDKH3 varchar(5),
	@MaDanhDauDDKH4 varchar(5),
	@MaDanhDauDDKH5 varchar(5),
	@SoGiayPhep varchar(11),
	@NgayCapPhep datetime,
	@NgayHetHanCapPhep datetime,
	@GhiChu nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TKVC_VanDon] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TKVC_VanDon] 
		SET
			[Master_ID] = @Master_ID,
			[SoTTVanDon] = @SoTTVanDon,
			[SoVanDon] = @SoVanDon,
			[NgayPhatHanhVD] = @NgayPhatHanhVD,
			[MoTaHangHoa] = @MoTaHangHoa,
			[MaHS] = @MaHS,
			[KyHieuVaSoHieu] = @KyHieuVaSoHieu,
			[NgayNhapKhoHQLanDau] = @NgayNhapKhoHQLanDau,
			[PhanLoaiSanPhan] = @PhanLoaiSanPhan,
			[MaNuocSanXuat] = @MaNuocSanXuat,
			[TenNuocSanXuat] = @TenNuocSanXuat,
			[MaDiaDiemXuatPhatVC] = @MaDiaDiemXuatPhatVC,
			[TenDiaDiemXuatPhatVC] = @TenDiaDiemXuatPhatVC,
			[MaDiaDiemDichVC] = @MaDiaDiemDichVC,
			[TenDiaDiemDichVC] = @TenDiaDiemDichVC,
			[LoaiHangHoa] = @LoaiHangHoa,
			[MaPhuongTienVC] = @MaPhuongTienVC,
			[TenPhuongTienVC] = @TenPhuongTienVC,
			[NgayHangDuKienDenDi] = @NgayHangDuKienDenDi,
			[MaNguoiNhapKhau] = @MaNguoiNhapKhau,
			[TenNguoiNhapKhau] = @TenNguoiNhapKhau,
			[DiaChiNguoiNhapKhau] = @DiaChiNguoiNhapKhau,
			[MaNguoiXuatKhua] = @MaNguoiXuatKhua,
			[TenNguoiXuatKhau] = @TenNguoiXuatKhau,
			[DiaChiNguoiXuatKhau] = @DiaChiNguoiXuatKhau,
			[MaNguoiUyThac] = @MaNguoiUyThac,
			[TenNguoiUyThac] = @TenNguoiUyThac,
			[DiaChiNguoiUyThac] = @DiaChiNguoiUyThac,
			[MaVanBanPhapLuat1] = @MaVanBanPhapLuat1,
			[MaVanBanPhapLuat2] = @MaVanBanPhapLuat2,
			[MaVanBanPhapLuat3] = @MaVanBanPhapLuat3,
			[MaVanBanPhapLuat4] = @MaVanBanPhapLuat4,
			[MaVanBanPhapLuat5] = @MaVanBanPhapLuat5,
			[MaDVTTriGia] = @MaDVTTriGia,
			[TriGia] = @TriGia,
			[SoLuong] = @SoLuong,
			[MaDVTSoLuong] = @MaDVTSoLuong,
			[TongTrongLuong] = @TongTrongLuong,
			[MaDVTTrongLuong] = @MaDVTTrongLuong,
			[TheTich] = @TheTich,
			[MaDVTTheTich] = @MaDVTTheTich,
			[MaDanhDauDDKH1] = @MaDanhDauDDKH1,
			[MaDanhDauDDKH2] = @MaDanhDauDDKH2,
			[MaDanhDauDDKH3] = @MaDanhDauDDKH3,
			[MaDanhDauDDKH4] = @MaDanhDauDDKH4,
			[MaDanhDauDDKH5] = @MaDanhDauDDKH5,
			[SoGiayPhep] = @SoGiayPhep,
			[NgayCapPhep] = @NgayCapPhep,
			[NgayHetHanCapPhep] = @NgayHetHanCapPhep,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TKVC_VanDon]
		(
			[Master_ID],
			[SoTTVanDon],
			[SoVanDon],
			[NgayPhatHanhVD],
			[MoTaHangHoa],
			[MaHS],
			[KyHieuVaSoHieu],
			[NgayNhapKhoHQLanDau],
			[PhanLoaiSanPhan],
			[MaNuocSanXuat],
			[TenNuocSanXuat],
			[MaDiaDiemXuatPhatVC],
			[TenDiaDiemXuatPhatVC],
			[MaDiaDiemDichVC],
			[TenDiaDiemDichVC],
			[LoaiHangHoa],
			[MaPhuongTienVC],
			[TenPhuongTienVC],
			[NgayHangDuKienDenDi],
			[MaNguoiNhapKhau],
			[TenNguoiNhapKhau],
			[DiaChiNguoiNhapKhau],
			[MaNguoiXuatKhua],
			[TenNguoiXuatKhau],
			[DiaChiNguoiXuatKhau],
			[MaNguoiUyThac],
			[TenNguoiUyThac],
			[DiaChiNguoiUyThac],
			[MaVanBanPhapLuat1],
			[MaVanBanPhapLuat2],
			[MaVanBanPhapLuat3],
			[MaVanBanPhapLuat4],
			[MaVanBanPhapLuat5],
			[MaDVTTriGia],
			[TriGia],
			[SoLuong],
			[MaDVTSoLuong],
			[TongTrongLuong],
			[MaDVTTrongLuong],
			[TheTich],
			[MaDVTTheTich],
			[MaDanhDauDDKH1],
			[MaDanhDauDDKH2],
			[MaDanhDauDDKH3],
			[MaDanhDauDDKH4],
			[MaDanhDauDDKH5],
			[SoGiayPhep],
			[NgayCapPhep],
			[NgayHetHanCapPhep],
			[GhiChu]
		)
		VALUES 
		(
			@Master_ID,
			@SoTTVanDon,
			@SoVanDon,
			@NgayPhatHanhVD,
			@MoTaHangHoa,
			@MaHS,
			@KyHieuVaSoHieu,
			@NgayNhapKhoHQLanDau,
			@PhanLoaiSanPhan,
			@MaNuocSanXuat,
			@TenNuocSanXuat,
			@MaDiaDiemXuatPhatVC,
			@TenDiaDiemXuatPhatVC,
			@MaDiaDiemDichVC,
			@TenDiaDiemDichVC,
			@LoaiHangHoa,
			@MaPhuongTienVC,
			@TenPhuongTienVC,
			@NgayHangDuKienDenDi,
			@MaNguoiNhapKhau,
			@TenNguoiNhapKhau,
			@DiaChiNguoiNhapKhau,
			@MaNguoiXuatKhua,
			@TenNguoiXuatKhau,
			@DiaChiNguoiXuatKhau,
			@MaNguoiUyThac,
			@TenNguoiUyThac,
			@DiaChiNguoiUyThac,
			@MaVanBanPhapLuat1,
			@MaVanBanPhapLuat2,
			@MaVanBanPhapLuat3,
			@MaVanBanPhapLuat4,
			@MaVanBanPhapLuat5,
			@MaDVTTriGia,
			@TriGia,
			@SoLuong,
			@MaDVTSoLuong,
			@TongTrongLuong,
			@MaDVTTrongLuong,
			@TheTich,
			@MaDVTTheTich,
			@MaDanhDauDDKH1,
			@MaDanhDauDDKH2,
			@MaDanhDauDDKH3,
			@MaDanhDauDDKH4,
			@MaDanhDauDDKH5,
			@SoGiayPhep,
			@NgayCapPhep,
			@NgayHetHanCapPhep,
			@GhiChu
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TKVC_VanDon]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TKVC_VanDon] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoTTVanDon],
	[SoVanDon],
	[NgayPhatHanhVD],
	[MoTaHangHoa],
	[MaHS],
	[KyHieuVaSoHieu],
	[NgayNhapKhoHQLanDau],
	[PhanLoaiSanPhan],
	[MaNuocSanXuat],
	[TenNuocSanXuat],
	[MaDiaDiemXuatPhatVC],
	[TenDiaDiemXuatPhatVC],
	[MaDiaDiemDichVC],
	[TenDiaDiemDichVC],
	[LoaiHangHoa],
	[MaPhuongTienVC],
	[TenPhuongTienVC],
	[NgayHangDuKienDenDi],
	[MaNguoiNhapKhau],
	[TenNguoiNhapKhau],
	[DiaChiNguoiNhapKhau],
	[MaNguoiXuatKhua],
	[TenNguoiXuatKhau],
	[DiaChiNguoiXuatKhau],
	[MaNguoiUyThac],
	[TenNguoiUyThac],
	[DiaChiNguoiUyThac],
	[MaVanBanPhapLuat1],
	[MaVanBanPhapLuat2],
	[MaVanBanPhapLuat3],
	[MaVanBanPhapLuat4],
	[MaVanBanPhapLuat5],
	[MaDVTTriGia],
	[TriGia],
	[SoLuong],
	[MaDVTSoLuong],
	[TongTrongLuong],
	[MaDVTTrongLuong],
	[TheTich],
	[MaDVTTheTich],
	[MaDanhDauDDKH1],
	[MaDanhDauDDKH2],
	[MaDanhDauDDKH3],
	[MaDanhDauDDKH4],
	[MaDanhDauDDKH5],
	[SoGiayPhep],
	[NgayCapPhep],
	[NgayHetHanCapPhep],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACC_TKVC_VanDon]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SoTTVanDon],
	[SoVanDon],
	[NgayPhatHanhVD],
	[MoTaHangHoa],
	[MaHS],
	[KyHieuVaSoHieu],
	[NgayNhapKhoHQLanDau],
	[PhanLoaiSanPhan],
	[MaNuocSanXuat],
	[TenNuocSanXuat],
	[MaDiaDiemXuatPhatVC],
	[TenDiaDiemXuatPhatVC],
	[MaDiaDiemDichVC],
	[TenDiaDiemDichVC],
	[LoaiHangHoa],
	[MaPhuongTienVC],
	[TenPhuongTienVC],
	[NgayHangDuKienDenDi],
	[MaNguoiNhapKhau],
	[TenNguoiNhapKhau],
	[DiaChiNguoiNhapKhau],
	[MaNguoiXuatKhua],
	[TenNguoiXuatKhau],
	[DiaChiNguoiXuatKhau],
	[MaNguoiUyThac],
	[TenNguoiUyThac],
	[DiaChiNguoiUyThac],
	[MaVanBanPhapLuat1],
	[MaVanBanPhapLuat2],
	[MaVanBanPhapLuat3],
	[MaVanBanPhapLuat4],
	[MaVanBanPhapLuat5],
	[MaDVTTriGia],
	[TriGia],
	[SoLuong],
	[MaDVTSoLuong],
	[TongTrongLuong],
	[MaDVTTrongLuong],
	[TheTich],
	[MaDVTTheTich],
	[MaDanhDauDDKH1],
	[MaDanhDauDDKH2],
	[MaDanhDauDDKH3],
	[MaDanhDauDDKH4],
	[MaDanhDauDDKH5],
	[SoGiayPhep],
	[NgayCapPhep],
	[NgayHetHanCapPhep],
	[GhiChu]
FROM [dbo].[t_KDT_VNACC_TKVC_VanDon] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_VanDon_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoTTVanDon],
	[SoVanDon],
	[NgayPhatHanhVD],
	[MoTaHangHoa],
	[MaHS],
	[KyHieuVaSoHieu],
	[NgayNhapKhoHQLanDau],
	[PhanLoaiSanPhan],
	[MaNuocSanXuat],
	[TenNuocSanXuat],
	[MaDiaDiemXuatPhatVC],
	[TenDiaDiemXuatPhatVC],
	[MaDiaDiemDichVC],
	[TenDiaDiemDichVC],
	[LoaiHangHoa],
	[MaPhuongTienVC],
	[TenPhuongTienVC],
	[NgayHangDuKienDenDi],
	[MaNguoiNhapKhau],
	[TenNguoiNhapKhau],
	[DiaChiNguoiNhapKhau],
	[MaNguoiXuatKhua],
	[TenNguoiXuatKhau],
	[DiaChiNguoiXuatKhau],
	[MaNguoiUyThac],
	[TenNguoiUyThac],
	[DiaChiNguoiUyThac],
	[MaVanBanPhapLuat1],
	[MaVanBanPhapLuat2],
	[MaVanBanPhapLuat3],
	[MaVanBanPhapLuat4],
	[MaVanBanPhapLuat5],
	[MaDVTTriGia],
	[TriGia],
	[SoLuong],
	[MaDVTSoLuong],
	[TongTrongLuong],
	[MaDVTTrongLuong],
	[TheTich],
	[MaDVTTheTich],
	[MaDanhDauDDKH1],
	[MaDanhDauDDKH2],
	[MaDanhDauDDKH3],
	[MaDanhDauDDKH4],
	[MaDanhDauDDKH5],
	[SoGiayPhep],
	[NgayCapPhep],
	[NgayHetHanCapPhep],
	[GhiChu]
FROM
	[dbo].[t_KDT_VNACC_TKVC_VanDon]	

GO




----------------------------------------------------------------------------T_KDT_VNACC_TKVC_Container--------------------------------

if not exists (select * from sysobjects where name='t_KDT_VNACC_TKVC_Container' and xtype='U')
Begin


CREATE TABLE [t_KDT_VNACC_TKVC_Container](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	SotieuDe varchar(3) NULL,
	SoHieuContainer varchar(17) NULL,
	SoDongHangTrenTK varchar(5) NULL,
	SoSeal1 varchar(15) NULL,
	SoSeal2 varchar(15) NULL,
	SoSeal3 varchar(15) NULL,
	SoSeal4 varchar(15) NULL,
	SoSeal5 varchar(15) NULL,
	SoSeal6 varchar(15) NULL,
	Note varchar(50)null,

 CONSTRAINT [PK_t_KDT_VNACC_TKVC_Conainer] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
End
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_Container_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Insert]
	@Master_ID bigint,
	@SotieuDe varchar(3),
	@SoHieuContainer varchar(17),
	@SoDongHangTrenTK varchar(5),
	@SoSeal1 varchar(15),
	@SoSeal2 varchar(15),
	@SoSeal3 varchar(15),
	@SoSeal4 varchar(15),
	@SoSeal5 varchar(15),
	@SoSeal6 varchar(15),
	@Note varchar(50),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TKVC_Container]
(
	[Master_ID],
	[SotieuDe],
	[SoHieuContainer],
	[SoDongHangTrenTK],
	[SoSeal1],
	[SoSeal2],
	[SoSeal3],
	[SoSeal4],
	[SoSeal5],
	[SoSeal6],
	[Note]
)
VALUES 
(
	@Master_ID,
	@SotieuDe,
	@SoHieuContainer,
	@SoDongHangTrenTK,
	@SoSeal1,
	@SoSeal2,
	@SoSeal3,
	@SoSeal4,
	@SoSeal5,
	@SoSeal6,
	@Note
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Update]
	@ID bigint,
	@Master_ID bigint,
	@SotieuDe varchar(3),
	@SoHieuContainer varchar(17),
	@SoDongHangTrenTK varchar(5),
	@SoSeal1 varchar(15),
	@SoSeal2 varchar(15),
	@SoSeal3 varchar(15),
	@SoSeal4 varchar(15),
	@SoSeal5 varchar(15),
	@SoSeal6 varchar(15),
	@Note varchar(50)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TKVC_Container]
SET
	[Master_ID] = @Master_ID,
	[SotieuDe] = @SotieuDe,
	[SoHieuContainer] = @SoHieuContainer,
	[SoDongHangTrenTK] = @SoDongHangTrenTK,
	[SoSeal1] = @SoSeal1,
	[SoSeal2] = @SoSeal2,
	[SoSeal3] = @SoSeal3,
	[SoSeal4] = @SoSeal4,
	[SoSeal5] = @SoSeal5,
	[SoSeal6] = @SoSeal6,
	[Note] = @Note
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SotieuDe varchar(3),
	@SoHieuContainer varchar(17),
	@SoDongHangTrenTK varchar(5),
	@SoSeal1 varchar(15),
	@SoSeal2 varchar(15),
	@SoSeal3 varchar(15),
	@SoSeal4 varchar(15),
	@SoSeal5 varchar(15),
	@SoSeal6 varchar(15),
	@Note varchar(50)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TKVC_Container] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TKVC_Container] 
		SET
			[Master_ID] = @Master_ID,
			[SotieuDe] = @SotieuDe,
			[SoHieuContainer] = @SoHieuContainer,
			[SoDongHangTrenTK] = @SoDongHangTrenTK,
			[SoSeal1] = @SoSeal1,
			[SoSeal2] = @SoSeal2,
			[SoSeal3] = @SoSeal3,
			[SoSeal4] = @SoSeal4,
			[SoSeal5] = @SoSeal5,
			[SoSeal6] = @SoSeal6,
			[Note] = @Note
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TKVC_Container]
		(
			[Master_ID],
			[SotieuDe],
			[SoHieuContainer],
			[SoDongHangTrenTK],
			[SoSeal1],
			[SoSeal2],
			[SoSeal3],
			[SoSeal4],
			[SoSeal5],
			[SoSeal6],
			[Note]
		)
		VALUES 
		(
			@Master_ID,
			@SotieuDe,
			@SoHieuContainer,
			@SoDongHangTrenTK,
			@SoSeal1,
			@SoSeal2,
			@SoSeal3,
			@SoSeal4,
			@SoSeal5,
			@SoSeal6,
			@Note
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TKVC_Container]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TKVC_Container] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SotieuDe],
	[SoHieuContainer],
	[SoDongHangTrenTK],
	[SoSeal1],
	[SoSeal2],
	[SoSeal3],
	[SoSeal4],
	[SoSeal5],
	[SoSeal6],
	[Note]
FROM
	[dbo].[t_KDT_VNACC_TKVC_Container]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SotieuDe],
	[SoHieuContainer],
	[SoDongHangTrenTK],
	[SoSeal1],
	[SoSeal2],
	[SoSeal3],
	[SoSeal4],
	[SoSeal5],
	[SoSeal6],
	[Note]
FROM [dbo].[t_KDT_VNACC_TKVC_Container] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_Container_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_Container_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SotieuDe],
	[SoHieuContainer],
	[SoDongHangTrenTK],
	[SoSeal1],
	[SoSeal2],
	[SoSeal3],
	[SoSeal4],
	[SoSeal5],
	[SoSeal6],
	[Note]
FROM
	[dbo].[t_KDT_VNACC_TKVC_Container]	

GO

------------------------------------------------------------------------------T_KDT_VNACC_TKVC_TKXuat-------------------------------


if not exists (select * from sysobjects where name='t_KDT_VNACC_TKVC_TKXuat' and xtype='U')
Begin

CREATE TABLE [dbo].[t_KDT_VNACC_TKVC_TKXuat](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[SoToKhaiXuat] numeric(12) NULL,
 
 CONSTRAINT [PK_t_KDT_VNACC_TKVC_TKXuat] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


End
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Insert]
	@Master_ID bigint,
	@SoToKhaiXuat numeric(12, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TKVC_TKXuat]
(
	[Master_ID],
	[SoToKhaiXuat]
)
VALUES 
(
	@Master_ID,
	@SoToKhaiXuat
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Update]
	@ID bigint,
	@Master_ID bigint,
	@SoToKhaiXuat numeric(12, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TKVC_TKXuat]
SET
	[Master_ID] = @Master_ID,
	[SoToKhaiXuat] = @SoToKhaiXuat
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SoToKhaiXuat numeric(12, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TKVC_TKXuat] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TKVC_TKXuat] 
		SET
			[Master_ID] = @Master_ID,
			[SoToKhaiXuat] = @SoToKhaiXuat
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TKVC_TKXuat]
		(
			[Master_ID],
			[SoToKhaiXuat]
		)
		VALUES 
		(
			@Master_ID,
			@SoToKhaiXuat
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TKVC_TKXuat]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TKVC_TKXuat] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoToKhaiXuat]
FROM
	[dbo].[t_KDT_VNACC_TKVC_TKXuat]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SoToKhaiXuat]
FROM [dbo].[t_KDT_VNACC_TKVC_TKXuat] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, February 18, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TKXuat_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoToKhaiXuat]
FROM
	[dbo].[t_KDT_VNACC_TKVC_TKXuat]	

GO


-----------------------------------------------------------t_VNACC_TKVC_TrungChuyen-------------------------------------------------------
if not exists (select * from sysobjects where name='t_KDT_VNACC_TKVC_TrungChuyen' and xtype='U')
Begin
CREATE TABLE [dbo].[t_KDT_VNACC_TKVC_TrungChuyen](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_ID] [bigint] NULL,
	[MaDiaDiemTrungChuyen] [varchar](7) NULL,
	[TenDiaDiemTrungChuyen] [varchar](20) NULL,
	[NgayDenDiaDiem_TC] [datetime] NULL,
	[NgayDiDiaDiem_TC] [datetime] NULL,
 CONSTRAINT [PK_t_KDT_VNACC_TKVC_TrungChuyen] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
End



-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Insert]
	@Master_ID bigint,
	@MaDiaDiemTrungChuyen varchar(7),
	@TenDiaDiemTrungChuyen varchar(20),
	@NgayDenDiaDiem_TC datetime,
	@NgayDiDiaDiem_TC datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
(
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDiDiaDiem_TC]
)
VALUES 
(
	@Master_ID,
	@MaDiaDiemTrungChuyen,
	@TenDiaDiemTrungChuyen,
	@NgayDenDiaDiem_TC,
	@NgayDiDiaDiem_TC
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaDiaDiemTrungChuyen varchar(7),
	@TenDiaDiemTrungChuyen varchar(20),
	@NgayDenDiaDiem_TC datetime,
	@NgayDiDiaDiem_TC datetime
AS

UPDATE
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
SET
	[Master_ID] = @Master_ID,
	[MaDiaDiemTrungChuyen] = @MaDiaDiemTrungChuyen,
	[TenDiaDiemTrungChuyen] = @TenDiaDiemTrungChuyen,
	[NgayDenDiaDiem_TC] = @NgayDenDiaDiem_TC,
	[NgayDiDiaDiem_TC] = @NgayDiDiaDiem_TC
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaDiaDiemTrungChuyen varchar(7),
	@TenDiaDiemTrungChuyen varchar(20),
	@NgayDenDiaDiem_TC datetime,
	@NgayDiDiaDiem_TC datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TKVC_TrungChuyen] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TKVC_TrungChuyen] 
		SET
			[Master_ID] = @Master_ID,
			[MaDiaDiemTrungChuyen] = @MaDiaDiemTrungChuyen,
			[TenDiaDiemTrungChuyen] = @TenDiaDiemTrungChuyen,
			[NgayDenDiaDiem_TC] = @NgayDenDiaDiem_TC,
			[NgayDiDiaDiem_TC] = @NgayDiDiaDiem_TC
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
		(
			[Master_ID],
			[MaDiaDiemTrungChuyen],
			[TenDiaDiemTrungChuyen],
			[NgayDenDiaDiem_TC],
			[NgayDiDiaDiem_TC]
		)
		VALUES 
		(
			@Master_ID,
			@MaDiaDiemTrungChuyen,
			@TenDiaDiemTrungChuyen,
			@NgayDenDiaDiem_TC,
			@NgayDiDiaDiem_TC
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TKVC_TrungChuyen] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDiDiaDiem_TC]
FROM
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDiDiaDiem_TC]
FROM [dbo].[t_KDT_VNACC_TKVC_TrungChuyen] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Friday, February 21, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TKVC_TrungChuyen_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaDiaDiemTrungChuyen],
	[TenDiaDiemTrungChuyen],
	[NgayDenDiaDiem_TC],
	[NgayDiDiaDiem_TC]
FROM
	[dbo].[t_KDT_VNACC_TKVC_TrungChuyen]	

GO



-----------------------------------------------------------t_VNACC_Category_Berth---------------------------------------------------------



if not exists (select * from sysobjects where name='t_VNACC_Category_Berth' and xtype='U')
Begin
CREATE TABLE [dbo].[t_VNACC_Category_Berth](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ReferenceDB] [varchar](5) NULL,
	[CodeHQ] [varchar](3) NULL,
	[Code] [varchar](6) NULL,
	[MoTa_HeThong] [varchar](255) NULL,
	[TenCang] [nvarchar](255) NULL,
	[DiaChi] [nvarchar](255) NULL,
	[Note] [varchar](255) NULL,
 CONSTRAINT [PK_t_VNACC_Category_Berth] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
End
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Insert]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Update]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Delete]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_Load]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_VNACC_Category_Berth_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Insert]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Insert]
	@ReferenceDB varchar(5),
	@CodeHQ varchar(3),
	@Code varchar(6),
	@MoTa_HeThong varchar(255),
	@TenCang nvarchar(255),
	@DiaChi nvarchar(255),
	@Note varchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_VNACC_Category_Berth]
(
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
)
VALUES 
(
	@ReferenceDB,
	@CodeHQ,
	@Code,
	@MoTa_HeThong,
	@TenCang,
	@DiaChi,
	@Note
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Update]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Update]
	@ID bigint,
	@ReferenceDB varchar(5),
	@CodeHQ varchar(3),
	@Code varchar(6),
	@MoTa_HeThong varchar(255),
	@TenCang nvarchar(255),
	@DiaChi nvarchar(255),
	@Note varchar(255)
AS

UPDATE
	[dbo].[t_VNACC_Category_Berth]
SET
	[ReferenceDB] = @ReferenceDB,
	[CodeHQ] = @CodeHQ,
	[Code] = @Code,
	[MoTa_HeThong] = @MoTa_HeThong,
	[TenCang] = @TenCang,
	[DiaChi] = @DiaChi,
	[Note] = @Note
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_InsertUpdate]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_InsertUpdate]
	@ID bigint,
	@ReferenceDB varchar(5),
	@CodeHQ varchar(3),
	@Code varchar(6),
	@MoTa_HeThong varchar(255),
	@TenCang nvarchar(255),
	@DiaChi nvarchar(255),
	@Note varchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_VNACC_Category_Berth] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_VNACC_Category_Berth] 
		SET
			[ReferenceDB] = @ReferenceDB,
			[CodeHQ] = @CodeHQ,
			[Code] = @Code,
			[MoTa_HeThong] = @MoTa_HeThong,
			[TenCang] = @TenCang,
			[DiaChi] = @DiaChi,
			[Note] = @Note
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_VNACC_Category_Berth]
		(
			[ReferenceDB],
			[CodeHQ],
			[Code],
			[MoTa_HeThong],
			[TenCang],
			[DiaChi],
			[Note]
		)
		VALUES 
		(
			@ReferenceDB,
			@CodeHQ,
			@Code,
			@MoTa_HeThong,
			@TenCang,
			@DiaChi,
			@Note
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Delete]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_VNACC_Category_Berth]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_DeleteDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_VNACC_Category_Berth] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_Load]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
FROM
	[dbo].[t_VNACC_Category_Berth]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_SelectDynamic]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
FROM [dbo].[t_VNACC_Category_Berth] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_VNACC_Category_Berth_SelectAll]
-- Database: ECS_TQDT_KD_VNACCS
-- Author: Ngo Thanh Tung
-- Time created: Thursday, February 20, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_VNACC_Category_Berth_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceDB],
	[CodeHQ],
	[Code],
	[MoTa_HeThong],
	[TenCang],
	[DiaChi],
	[Note]
FROM
	[dbo].[t_VNACC_Category_Berth]	

GO






           --Cập nhật version
     IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '14.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('14.3', GETDATE(), N'Tao bang to khai van chuyen, bang danh muc t_VNACC_Category_Berth(A305)')
END	