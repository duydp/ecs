﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class ArrivalTransportMeans
    {
        /// <summary>
        /// Tên PTVT
        /// </summary>
        [XmlElement("identification")]
        public string Identification { get; set; }
        /// <summary>
        /// Số hiệu PTVT
        /// </summary>
        [XmlElement("identity")]
        public string Identity { get; set; }
        /// <summary>
        /// Kiểu
        /// </summary>
        [XmlElement("modeAndType")]
        public int ModeAndType { get; set; }
        /// <summary>
        /// .....
        /// </summary>
        [XmlElement("registrationNationality")]
        public string RegistrationNationality { get; set; }

    }
}
