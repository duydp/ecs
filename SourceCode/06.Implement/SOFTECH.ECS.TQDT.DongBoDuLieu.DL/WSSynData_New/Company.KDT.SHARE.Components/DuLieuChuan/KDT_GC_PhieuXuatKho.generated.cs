﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components.DuLieuChuan
{
	public partial class KDT_GC_PhieuXuatKho : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public DateTime NgayXuatKho { set; get; }
		public decimal SoPhieu { set; get; }
		public string SoHopDong { set; get; }
		public string KhachHang { set; get; }
		public string Style { set; get; }
		public string PO { set; get; }
		public string DonViNhan { set; get; }
		public string XuatTaiKho { set; get; }
		public decimal SoLuong { set; get; }
		public string DVTSoLuong { set; get; }
		public string SoInvoice { set; get; }
		public DateTime NgayInvoice { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_GC_PhieuXuatKho> ConvertToCollection(IDataReader reader)
		{
			List<KDT_GC_PhieuXuatKho> collection = new List<KDT_GC_PhieuXuatKho>();
			while (reader.Read())
			{
				KDT_GC_PhieuXuatKho entity = new KDT_GC_PhieuXuatKho();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayXuatKho"))) entity.NgayXuatKho = reader.GetDateTime(reader.GetOrdinal("NgayXuatKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoPhieu"))) entity.SoPhieu = reader.GetDecimal(reader.GetOrdinal("SoPhieu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
				if (!reader.IsDBNull(reader.GetOrdinal("KhachHang"))) entity.KhachHang = reader.GetString(reader.GetOrdinal("KhachHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("Style"))) entity.Style = reader.GetString(reader.GetOrdinal("Style"));
				if (!reader.IsDBNull(reader.GetOrdinal("PO"))) entity.PO = reader.GetString(reader.GetOrdinal("PO"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViNhan"))) entity.DonViNhan = reader.GetString(reader.GetOrdinal("DonViNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("XuatTaiKho"))) entity.XuatTaiKho = reader.GetString(reader.GetOrdinal("XuatTaiKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVTSoLuong"))) entity.DVTSoLuong = reader.GetString(reader.GetOrdinal("DVTSoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoInvoice"))) entity.SoInvoice = reader.GetString(reader.GetOrdinal("SoInvoice"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayInvoice"))) entity.NgayInvoice = reader.GetDateTime(reader.GetOrdinal("NgayInvoice"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_GC_PhieuXuatKho> collection, long id)
        {
            foreach (KDT_GC_PhieuXuatKho item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_PhieuXuatKho VALUES(@TKMD_ID, @NgayXuatKho, @SoPhieu, @SoHopDong, @KhachHang, @Style, @PO, @DonViNhan, @XuatTaiKho, @SoLuong, @DVTSoLuong, @SoInvoice, @NgayInvoice)";
            string update = "UPDATE t_KDT_GC_PhieuXuatKho SET TKMD_ID = @TKMD_ID, NgayXuatKho = @NgayXuatKho, SoPhieu = @SoPhieu, SoHopDong = @SoHopDong, KhachHang = @KhachHang, Style = @Style, PO = @PO, DonViNhan = @DonViNhan, XuatTaiKho = @XuatTaiKho, SoLuong = @SoLuong, DVTSoLuong = @DVTSoLuong, SoInvoice = @SoInvoice, NgayInvoice = @NgayInvoice WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhieuXuatKho WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayXuatKho", SqlDbType.DateTime, "NgayXuatKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhieu", SqlDbType.Decimal, "SoPhieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Style", SqlDbType.NVarChar, "Style", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PO", SqlDbType.NVarChar, "PO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViNhan", SqlDbType.NVarChar, "DonViNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XuatTaiKho", SqlDbType.NVarChar, "XuatTaiKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTSoLuong", SqlDbType.VarChar, "DVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoInvoice", SqlDbType.NVarChar, "SoInvoice", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayInvoice", SqlDbType.DateTime, "NgayInvoice", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayXuatKho", SqlDbType.DateTime, "NgayXuatKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhieu", SqlDbType.Decimal, "SoPhieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Style", SqlDbType.NVarChar, "Style", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PO", SqlDbType.NVarChar, "PO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViNhan", SqlDbType.NVarChar, "DonViNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XuatTaiKho", SqlDbType.NVarChar, "XuatTaiKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTSoLuong", SqlDbType.VarChar, "DVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoInvoice", SqlDbType.NVarChar, "SoInvoice", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayInvoice", SqlDbType.DateTime, "NgayInvoice", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_PhieuXuatKho VALUES(@TKMD_ID, @NgayXuatKho, @SoPhieu, @SoHopDong, @KhachHang, @Style, @PO, @DonViNhan, @XuatTaiKho, @SoLuong, @DVTSoLuong, @SoInvoice, @NgayInvoice)";
            string update = "UPDATE t_KDT_GC_PhieuXuatKho SET TKMD_ID = @TKMD_ID, NgayXuatKho = @NgayXuatKho, SoPhieu = @SoPhieu, SoHopDong = @SoHopDong, KhachHang = @KhachHang, Style = @Style, PO = @PO, DonViNhan = @DonViNhan, XuatTaiKho = @XuatTaiKho, SoLuong = @SoLuong, DVTSoLuong = @DVTSoLuong, SoInvoice = @SoInvoice, NgayInvoice = @NgayInvoice WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_PhieuXuatKho WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayXuatKho", SqlDbType.DateTime, "NgayXuatKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoPhieu", SqlDbType.Decimal, "SoPhieu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Style", SqlDbType.NVarChar, "Style", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PO", SqlDbType.NVarChar, "PO", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViNhan", SqlDbType.NVarChar, "DonViNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XuatTaiKho", SqlDbType.NVarChar, "XuatTaiKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVTSoLuong", SqlDbType.VarChar, "DVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoInvoice", SqlDbType.NVarChar, "SoInvoice", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayInvoice", SqlDbType.DateTime, "NgayInvoice", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayXuatKho", SqlDbType.DateTime, "NgayXuatKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoPhieu", SqlDbType.Decimal, "SoPhieu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.NVarChar, "SoHopDong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhachHang", SqlDbType.NVarChar, "KhachHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Style", SqlDbType.NVarChar, "Style", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PO", SqlDbType.NVarChar, "PO", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViNhan", SqlDbType.NVarChar, "DonViNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XuatTaiKho", SqlDbType.NVarChar, "XuatTaiKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVTSoLuong", SqlDbType.VarChar, "DVTSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoInvoice", SqlDbType.NVarChar, "SoInvoice", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayInvoice", SqlDbType.DateTime, "NgayInvoice", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_GC_PhieuXuatKho Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_GC_PhieuXuatKho> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_GC_PhieuXuatKho> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_GC_PhieuXuatKho> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_GC_PhieuXuatKho(long tKMD_ID, DateTime ngayXuatKho, decimal soPhieu, string soHopDong, string khachHang, string style, string pO, string donViNhan, string xuatTaiKho, decimal soLuong, string dVTSoLuong, string soInvoice, DateTime ngayInvoice)
		{
			KDT_GC_PhieuXuatKho entity = new KDT_GC_PhieuXuatKho();	
			entity.TKMD_ID = tKMD_ID;
			entity.NgayXuatKho = ngayXuatKho;
			entity.SoPhieu = soPhieu;
			entity.SoHopDong = soHopDong;
			entity.KhachHang = khachHang;
			entity.Style = style;
			entity.PO = pO;
			entity.DonViNhan = donViNhan;
			entity.XuatTaiKho = xuatTaiKho;
			entity.SoLuong = soLuong;
			entity.DVTSoLuong = dVTSoLuong;
			entity.SoInvoice = soInvoice;
			entity.NgayInvoice = ngayInvoice;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayXuatKho", SqlDbType.DateTime, NgayXuatKho.Year <= 1753 ? DBNull.Value : (object) NgayXuatKho);
			db.AddInParameter(dbCommand, "@SoPhieu", SqlDbType.Decimal, SoPhieu);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@KhachHang", SqlDbType.NVarChar, KhachHang);
			db.AddInParameter(dbCommand, "@Style", SqlDbType.NVarChar, Style);
			db.AddInParameter(dbCommand, "@PO", SqlDbType.NVarChar, PO);
			db.AddInParameter(dbCommand, "@DonViNhan", SqlDbType.NVarChar, DonViNhan);
			db.AddInParameter(dbCommand, "@XuatTaiKho", SqlDbType.NVarChar, XuatTaiKho);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVTSoLuong", SqlDbType.VarChar, DVTSoLuong);
            //db.AddInParameter(dbCommand, "@SoInvoice", SqlDbType.NVarChar, SoInvoice);
            //db.AddInParameter(dbCommand, "@NgayInvoice", SqlDbType.DateTime, NgayInvoice.Year <= 1753 ? DBNull.Value : (object) NgayInvoice);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_GC_PhieuXuatKho> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuXuatKho item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_GC_PhieuXuatKho(long id, long tKMD_ID, DateTime ngayXuatKho, decimal soPhieu, string soHopDong, string khachHang, string style, string pO, string donViNhan, string xuatTaiKho, decimal soLuong, string dVTSoLuong, string soInvoice, DateTime ngayInvoice)
		{
			KDT_GC_PhieuXuatKho entity = new KDT_GC_PhieuXuatKho();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.NgayXuatKho = ngayXuatKho;
			entity.SoPhieu = soPhieu;
			entity.SoHopDong = soHopDong;
			entity.KhachHang = khachHang;
			entity.Style = style;
			entity.PO = pO;
			entity.DonViNhan = donViNhan;
			entity.XuatTaiKho = xuatTaiKho;
			entity.SoLuong = soLuong;
			entity.DVTSoLuong = dVTSoLuong;
			entity.SoInvoice = soInvoice;
			entity.NgayInvoice = ngayInvoice;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_PhieuXuatKho_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayXuatKho", SqlDbType.DateTime, NgayXuatKho.Year <= 1753 ? DBNull.Value : (object) NgayXuatKho);
			db.AddInParameter(dbCommand, "@SoPhieu", SqlDbType.Decimal, SoPhieu);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@KhachHang", SqlDbType.NVarChar, KhachHang);
			db.AddInParameter(dbCommand, "@Style", SqlDbType.NVarChar, Style);
			db.AddInParameter(dbCommand, "@PO", SqlDbType.NVarChar, PO);
			db.AddInParameter(dbCommand, "@DonViNhan", SqlDbType.NVarChar, DonViNhan);
			db.AddInParameter(dbCommand, "@XuatTaiKho", SqlDbType.NVarChar, XuatTaiKho);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVTSoLuong", SqlDbType.VarChar, DVTSoLuong);
			db.AddInParameter(dbCommand, "@SoInvoice", SqlDbType.NVarChar, SoInvoice);
			db.AddInParameter(dbCommand, "@NgayInvoice", SqlDbType.DateTime, NgayInvoice.Year <= 1753 ? DBNull.Value : (object) NgayInvoice);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_GC_PhieuXuatKho> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuXuatKho item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_GC_PhieuXuatKho(long id, long tKMD_ID, DateTime ngayXuatKho, decimal soPhieu, string soHopDong, string khachHang, string style, string pO, string donViNhan, string xuatTaiKho, decimal soLuong, string dVTSoLuong, string soInvoice, DateTime ngayInvoice)
		{
			KDT_GC_PhieuXuatKho entity = new KDT_GC_PhieuXuatKho();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.NgayXuatKho = ngayXuatKho;
			entity.SoPhieu = soPhieu;
			entity.SoHopDong = soHopDong;
			entity.KhachHang = khachHang;
			entity.Style = style;
			entity.PO = pO;
			entity.DonViNhan = donViNhan;
			entity.XuatTaiKho = xuatTaiKho;
			entity.SoLuong = soLuong;
			entity.DVTSoLuong = dVTSoLuong;
			entity.SoInvoice = soInvoice;
			entity.NgayInvoice = ngayInvoice;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayXuatKho", SqlDbType.DateTime, NgayXuatKho.Year <= 1753 ? DBNull.Value : (object) NgayXuatKho);
			db.AddInParameter(dbCommand, "@SoPhieu", SqlDbType.Decimal, SoPhieu);
			db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.NVarChar, SoHopDong);
			db.AddInParameter(dbCommand, "@KhachHang", SqlDbType.NVarChar, KhachHang);
			db.AddInParameter(dbCommand, "@Style", SqlDbType.NVarChar, Style);
			db.AddInParameter(dbCommand, "@PO", SqlDbType.NVarChar, PO);
			db.AddInParameter(dbCommand, "@DonViNhan", SqlDbType.NVarChar, DonViNhan);
			db.AddInParameter(dbCommand, "@XuatTaiKho", SqlDbType.NVarChar, XuatTaiKho);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DVTSoLuong", SqlDbType.VarChar, DVTSoLuong);
			db.AddInParameter(dbCommand, "@SoInvoice", SqlDbType.NVarChar, SoInvoice);
			db.AddInParameter(dbCommand, "@NgayInvoice", SqlDbType.DateTime, NgayInvoice.Year <= 1753 ? DBNull.Value : (object) NgayInvoice);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_GC_PhieuXuatKho> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuXuatKho item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_GC_PhieuXuatKho(long id)
		{
			KDT_GC_PhieuXuatKho entity = new KDT_GC_PhieuXuatKho();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_PhieuXuatKho_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_GC_PhieuXuatKho> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_GC_PhieuXuatKho item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}