﻿------------------------------------------------
Hungtq updated 18/10/2013
------------------------------------------------
private void DirectorySecurityChange(string dirPath)
{
    DriveInfo info = new DriveInfo(Application.ExecutablePath);
    if (info.DriveType != DriveType.CDRom)
    {
        DirectoryInfo info2 = new DirectoryInfo(dirPath);
        try
        {
            DirectorySecurity accessControl = info2.GetAccessControl();
            foreach (FileSystemAccessRule rule in accessControl.GetAccessRules(true, true, typeof(NTAccount)))
            {
                if ("everyone".ToUpper() == rule.IdentityReference.ToString().ToUpper())
                {
                    return;
                }
            }
            accessControl.AddAccessRule(new FileSystemAccessRule("everyone", FileSystemRights.FullControl, InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit, PropagationFlags.None, AccessControlType.Allow));
            info2.SetAccessControl(accessControl);
        }
        catch (Exception exception)
        {
            using (MessageDialogSimpleForm form = new MessageDialogSimpleForm())
            {
                form.ShowMessage(ButtonPatern.OK_ONLY, MessageKind.Error, MessageDialog.CreateExceptionMessage(exception));
            }
        }
    }
}

------------------------------------------------
Hungtq updated 05/04/2013
------------------------------------------------
1. Thiết lập ẩn/ hiện các chức năng không còn sử dụng nữa.
+ Key trong fie config:	<add key="Ep" value="GFQRz2+dO1o="/>
+ Hàm sử dụng trong form Main:	SetExpress(...);
+ Giá trị: value="GFQRz2+dO1o=" : "true"; value="FND0y8lXdJE=" : "false"
+ true: ẩn; false: hiện.