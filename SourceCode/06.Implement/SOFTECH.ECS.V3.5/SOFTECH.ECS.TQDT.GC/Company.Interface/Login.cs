﻿using System;
using System.Windows.Forms;
using Company.QuanTri;
using System.Data.SqlClient;

namespace Company.Interface
{
    public partial class Login : Form
    {
        private Company.Controls.MessageBoxControl _msgBox;

        public Login()
        {
            InitializeComponent();
        }

        private string ShowMessage(string message, bool showYesNoButton)
        {
            _msgBox = new Company.Controls.MessageBoxControl
                          {
                              ShowYesNoButton = showYesNoButton,
                              MessageString = message
                          };
            _msgBox.ShowDialog();
            var st = _msgBox.ReturnValue;
            _msgBox.Dispose();
            return st;
        }

        private static string SetText(string vnText, string enText)
        {
            return GlobalSettings.NGON_NGU == "1" ? enText : vnText;
        }

        private void UiButton2Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void UiButton3Click(object sender, EventArgs e)
        {
            try
            {
                MainForm.EcsQuanTri = ECSPrincipal.ValidateLogin(txtUser.Text.Trim(), txtMatKhau.Text.Trim());
                if (MainForm.EcsQuanTri == null)
                {
                    string msg = "Đăng nhập không thành công.";
                    if (GlobalSettings.NGON_NGU == "1")
                    {
                        msg = "Login failed.";
                    }
                    ShowMessage(msg, false);
                    MainForm.isLoginSuccess = false;
                }
                else
                {
                    //TODO: VNACCS---------------------------------------------------------------------------------------------
                    if (MainForm.isUseSettingVNACC)
                    {
                        //TODO: Kiem tra bang USER: da co bo sung cac field va store cho VNACC?
                        Company.KDT.SHARE.Components.SQL.CheckUserTableIsHaveVNACCS(GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS);

                        //TODO: Chay store cap nhat trang thai cua Terminal.
                        Company.QuanTri.User userVNACC = Company.QuanTri.User.Load_VNACC(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.ID);
                        userVNACC.DataBase = GlobalSettings.DATABASE_NAME;
                        userVNACC.Update_VNACC();

                        try
                        {
                            //TODO: Kiem tra bang USER: da co bo sung cac field va store cho VNACC?
                            Company.KDT.SHARE.Components.SQL.CheckAndCreateProcedureCheckTerminalOnline(GlobalSettings.SERVER_NAME, GlobalSettings.DATABASE_NAME, GlobalSettings.USER, GlobalSettings.PASS);

                            //TODO: Chay store cap nhat trang thai cua Terminal.
                            User.CheckAndUpdateTerminalStatus();
                        }
                        catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }

                        //TODO: Kiem tra cho phep 1 USER duoc phep truy cap duy nhat.
                        //userVNACC = Company.QuanTri.User.Load_VNACC(((Company.QuanTri.SiteIdentity)MainForm.EcsQuanTri.Identity).user.ID);
                        //if (userVNACC.isOnline && userVNACC.IP != Company.KDT.SHARE.Components.Globals.GetLocalIPAddress())
                        //{
                        //    string msg = string.Format("Tài khoản người sử dụng '{0}' này đang kết nối khai báo.\r\n\n- Máy đang kết nối: {1} - {2}\r\n\nBạn vui lòng kiểm tra lại hoặc đăng nhập với tài khoản người sử dụng khác.", userVNACC.USER_NAME, userVNACC.IP, userVNACC.HostName);
                        //    ShowMessage(msg, false);
                        //    MainForm.isLoginSuccess = false;
                        //    return;
                        //}

                        //TODO: Cap nhat IP, HostName cua PC dang nhap vao bang USER
                        userVNACC.HostName = System.Environment.MachineName;
                        userVNACC.IP = Company.KDT.SHARE.Components.Globals.GetLocalIPAddress();
                        userVNACC.DataBase = GlobalSettings.DATABASE_NAME;
                        userVNACC.Update_VNACC();


                        //TODO: Kiem tra Terminal co cung ID co dang su dung khong truoc khi dang nhap nguoi dung. Chi cho phep 1 ternimal ID duoc phep khai bao.                    
                        if (Company.QuanTri.User.CheckVNACCTerminalCountOnline(userVNACC.VNACC_TerminalID) > 1
                            || Company.QuanTri.User.CheckVNACCTerminalCountOnline(userVNACC.VNACC_TerminalID, userVNACC.USER_NAME) > 1)
                        {
                            ShowMessage("Phát hiện có 1 Terminal '" + userVNACC.VNACC_TerminalID + "' khác đang kết nối khai báo. Bạn vui lòng thiết lập lại thông tin cho Terminal.", false);

                            userVNACC.VNACC_TerminalID = string.Empty;
                            userVNACC.VNACC_TerminalPass = string.Empty;
                            userVNACC.VNACC_UserID = string.Empty;
                            userVNACC.VNACC_UserCode = string.Empty;
                            userVNACC.VNACC_UserPass = string.Empty;
                            userVNACC.Update_VNACC();
                        }

                        //Cap nhat bien toan cuc VNACC cho chuong trinh
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalID = userVNACC.VNACC_TerminalID;
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.TerminalAccessKey = userVNACC.VNACC_TerminalPass != null ? userVNACC.VNACC_TerminalPass.Trim() : "";
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_ID = userVNACC.VNACC_UserID;
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Ma = userVNACC.VNACC_UserCode;
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.NguoiSuDung_Pass = userVNACC.VNACC_UserPass != null ? userVNACC.VNACC_UserPass.Trim() : "";
                        Company.KDT.SHARE.VNACCS.GlobalVNACC.Url = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("VNACCAddress", @"https://ediconn.vnaccs.customs.gov.vn/test");
                    }

                    MainForm.isLoginSuccess = true;
                    GlobalSettings.UserLog = txtUser.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("UserLog", txtUser.Text.Trim());
                    GlobalSettings.PassLog = txtMatKhau.Text.Trim();
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("PassLog", txtMatKhau.Text.Trim());
                    GlobalSettings.RefreshKey();
                    Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MainForm.isLoginSuccess = false;
                if (ShowMessage("Lỗi kết nối CSDL. Bạn có muốn cấu hình kết nối CSDL không?", true) == "Yes")
                {
                    var f = new ThietLapThongSoKBForm();
                    f.ShowDialog();
                }
                return;
            }
            
        }


        private void LinkLabel1LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var f = new ThietLapThongSoKBForm();
            f.ShowDialog();
        }

        private void Login_Load(object sender, EventArgs e)
        {
            txtMatKhau.Text = txtUser.Text = string.Empty;
            txtUser.Focus();
            try
            {
                uiButton3.Text = SetText("Đăng nhập", "Login");
                uiButton2.Text = SetText("Thoát", "Exit");
                linkLabel1.Text = SetText("Cấu hình thông số kết nối", "Configuration of connected parameters");

                BackgroundImage = System.Drawing.Image.FromFile(GlobalSettings.NGON_NGU == "1" ? "LOGIN-GIACONG-eng.png" : "LOGIN-GIACONG.png");

                BackgroundImageLayout = ImageLayout.Stretch;
            }
            catch (Exception)
            { }
        }
    }
}