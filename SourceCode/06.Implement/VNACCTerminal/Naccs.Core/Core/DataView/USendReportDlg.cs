﻿namespace Naccs.Core.DataView
{
    using Naccs.Core.Classes;
    using Naccs.Core.Properties;
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.IO;
    using System.Text;
    using System.Windows.Forms;

    public class USendReportDlg : Form
    {
        private ColumnHeader clmDataKey;
        private ColumnHeader clmDetail;
        private ColumnHeader clmFileName;
        private ColumnHeader clmFolderName;
        private ColumnHeader clmStatus;
        private string CnWriteFormat = "\"{0}\",\"{1}\",\"{2}\",\"{3}\"";
        private IContainer components;
        private SaveFileDialog dlgSave;
        private ImageList imgReport;
        private ListView lstReport;
        private ToolStripMenuItem mnClose;
        private ToolStripMenuItem mnDelete;
        private ToolStripMenuItem mnFile;
        private ToolStripSeparator mnFileSeparator1;
        private MenuStrip mnReport;
        private ToolStripMenuItem mnSave;

        public USendReportDlg()
        {
            this.InitializeComponent();
            this.Clear();
        }

        public void Add(string path)
        {
            this.lstReport.Items.Add(Path.GetFileName(path));
            int num = this.Count - 1;
            this.lstReport.Items[num].SubItems.Add(Path.GetDirectoryName(path));
            this.lstReport.Items[num].SubItems.Add("");
            this.lstReport.Items[num].SubItems.Add("");
            this.lstReport.Items[num].SubItems.Add("");
        }

        public void Add(string path, ReportStatus status, string datakey, string detail)
        {
            this.Add(path);
            int index = this.Count - 1;
            this.SetStatus(index, status, datakey, detail);
        }

        public void Clear()
        {
            this.lstReport.Items.Clear();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        public string FilePath(int index)
        {
            string str = null;
            if ((index >= 0) && ((this.Count > 0) && (index < this.Count)))
            {
                str = Path.Combine(this.lstReport.Items[index].SubItems[this.clmFolderName.Index].Text, this.lstReport.Items[index].SubItems[this.clmFileName.Index].Text);
            }
            return str;
        }

        private int Find(string datakey)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this.lstReport.Items[i].SubItems[this.clmDataKey.Index].Text == datakey)
                {
                    return i;
                }
            }
            return -1;
        }

        private void InitializeComponent()
        {
            this.components = new Container();
            ListViewItem item = new ListViewItem("");
            ComponentResourceManager manager = new ComponentResourceManager(typeof(USendReportDlg));
            this.lstReport = new ListView();
            this.clmFileName = new ColumnHeader();
            this.clmFolderName = new ColumnHeader();
            this.clmStatus = new ColumnHeader();
            this.clmDetail = new ColumnHeader();
            this.clmDataKey = new ColumnHeader();
            this.imgReport = new ImageList(this.components);
            this.mnReport = new MenuStrip();
            this.mnFile = new ToolStripMenuItem();
            this.mnSave = new ToolStripMenuItem();
            this.mnDelete = new ToolStripMenuItem();
            this.mnFileSeparator1 = new ToolStripSeparator();
            this.mnClose = new ToolStripMenuItem();
            this.dlgSave = new SaveFileDialog();
            this.mnReport.SuspendLayout();
            base.SuspendLayout();
            this.lstReport.Columns.AddRange(new ColumnHeader[] { this.clmFileName, this.clmFolderName, this.clmStatus, this.clmDetail, this.clmDataKey });
            this.lstReport.Dock = DockStyle.Fill;
            this.lstReport.FullRowSelect = true;
            this.lstReport.Items.AddRange(new ListViewItem[] { item });
            this.lstReport.Location = new Point(0, 0x1a);
            this.lstReport.Name = "lstReport";
            this.lstReport.Size = new Size(0x2ac, 0xe8);
            this.lstReport.SmallImageList = this.imgReport;
            this.lstReport.TabIndex = 0;
            this.lstReport.UseCompatibleStateImageBehavior = false;
            this.lstReport.View = View.Details;
            this.clmFileName.Text = "T\x00ean tệp";
            this.clmFileName.Width = 120;
            this.clmFolderName.Text = "T\x00ean thư mục";
            this.clmFolderName.Width = 190;
            this.clmStatus.Text = "T\x00ecnh trạng";
            this.clmStatus.Width = 70;
            this.clmDetail.Text = "Th\x00f4ng tin chi tiết";
            this.clmDetail.Width = 300;
            this.clmDataKey.Text = "登録キー";
            this.clmDataKey.Width = 0;
            this.imgReport.ImageStream = (ImageListStreamer) manager.GetObject("imgReport.ImageStream");
            this.imgReport.TransparentColor = Color.Magenta;
            this.imgReport.Images.SetKeyName(0, "Wait");
            this.imgReport.Images.SetKeyName(1, "Sent");
            this.imgReport.Images.SetKeyName(2, "Error");
            this.imgReport.Images.SetKeyName(3, "Delete");
            this.mnReport.Items.AddRange(new ToolStripItem[] { this.mnFile });
            this.mnReport.Location = new Point(0, 0);
            this.mnReport.Name = "mnReport";
            this.mnReport.Size = new Size(0x2ac, 0x1a);
            this.mnReport.TabIndex = 1;
            this.mnReport.Text = "menuStrip1";
            this.mnFile.DropDownItems.AddRange(new ToolStripItem[] { this.mnSave, this.mnDelete, this.mnFileSeparator1, this.mnClose });
            this.mnFile.Name = "mnFile";
            this.mnFile.Size = new Size(0x52, 0x16);
            this.mnFile.Text = "Tệp tin (&F)";
            this.mnSave.Name = "mnSave";
            this.mnSave.Size = new Size(0xcb, 0x16);
            this.mnSave.Text = "Lưu kết quả (&S)";
            this.mnSave.Click += new EventHandler(this.mnSave_Click);
            this.mnDelete.Name = "mnDelete";
            this.mnDelete.Size = new Size(0xcb, 0x16);
            this.mnDelete.Text = "X\x00f3a tệp tin đ\x00e3 gửi (&D)";
            this.mnDelete.Click += new EventHandler(this.mnDelete_Click);
            this.mnFileSeparator1.Name = "mnFileSeparator1";
            this.mnFileSeparator1.Size = new Size(200, 6);
            this.mnClose.Name = "mnClose";
            this.mnClose.Size = new Size(0xcb, 0x16);
            this.mnClose.Text = "kết th\x00fac (&X)";
            this.mnClose.Click += new EventHandler(this.mnClose_Click);
            this.dlgSave.DefaultExt = "csv";
            this.dlgSave.Filter = "csv|*.csv|すべてのファイル|*.*";
            this.dlgSave.RestoreDirectory = true;
            base.AutoScaleDimensions = new SizeF(6f, 12f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x2ac, 0x102);
            base.Controls.Add(this.lstReport);
            base.Controls.Add(this.mnReport);
            base.MainMenuStrip = this.mnReport;
            base.Name = "USendReportDlg";
            base.ShowIcon = false;
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "B\x00e1o c\x00e1o kết quả xử l\x00fd";
            this.mnReport.ResumeLayout(false);
            this.mnReport.PerformLayout();
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        private void mnClose_Click(object sender, EventArgs e)
        {
            base.Close();
        }

        private void mnDelete_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < this.Count; i++)
            {
                if (this.lstReport.Items[i].ImageKey == "Sent")
                {
                    string path = this.FilePath(i);
                    try
                    {
                        File.Delete(path);
                        this.UpdateStatus(i, ReportStatus.Delete);
                    }
                    catch
                    {
                    }
                }
            }
        }

        private void mnSave_Click(object sender, EventArgs e)
        {
            if (this.dlgSave.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    StreamWriter writer = new StreamWriter(this.dlgSave.FileName, false, Encoding.GetEncoding("UTF-8"));
                    try
                    {
                        writer.Flush();
                        string str = string.Format(this.CnWriteFormat, new object[] { this.clmFileName.Text, this.clmFolderName.Text, this.clmStatus.Text, this.clmDetail.Text });
                        writer.WriteLine(str);
                        for (int i = 0; i < this.Count; i++)
                        {
                            string str2 = this.lstReport.Items[i].SubItems[this.clmFileName.Index].Text.Replace("\"", "\"\"");
                            string str3 = this.lstReport.Items[i].SubItems[this.clmFolderName.Index].Text.Replace("\"", "\"\"");
                            string str4 = this.lstReport.Items[i].SubItems[this.clmStatus.Index].Text.Replace("\"", "\"\"");
                            string str5 = this.lstReport.Items[i].SubItems[this.clmDetail.Index].Text.Replace("\"", "\"\"");
                            str = string.Format(this.CnWriteFormat, new object[] { str2, str3, str4, str5 });
                            writer.WriteLine(str);
                        }
                    }
                    finally
                    {
                        writer.Close();
                    }
                }
                catch (Exception exception)
                {
                    new MessageDialog().ShowMessage("E303", string.Format("File : {0}", this.dlgSave.FileName), string.Format("Errocode : E303\r\nFileName : {0}\r\n{1}", this.dlgSave.FileName, MessageDialog.CreateExceptionMessage(exception)));
                }
            }
        }

        public void SetStatus(string datakey, ReportStatus status, string detail)
        {
            int index = this.Find(datakey);
            this.SetStatus(index, status, datakey, detail);
        }

        public void SetStatus(int index, ReportStatus status, string datakey, string detail)
        {
            if ((index >= 0) && ((this.Count > 0) && (index < this.Count)))
            {
                if (detail != null)
                {
                    this.lstReport.Items[index].SubItems[this.clmDetail.Index].Text = detail;
                }
                if (datakey != null)
                {
                    this.lstReport.Items[index].SubItems[this.clmDataKey.Index].Text = datakey;
                }
                this.UpdateStatus(index, status);
            }
        }

        public int StatusCount(ReportStatus status)
        {
            int num = 0;
            for (int i = 0; i < this.Count; i++)
            {
                if (status == ReportStatus.None)
                {
                    if (string.IsNullOrEmpty(this.lstReport.Items[i].ImageKey))
                    {
                        num++;
                    }
                }
                else if (this.lstReport.Items[i].ImageKey == this.StatusImageKey(status))
                {
                    num++;
                }
            }
            return num;
        }

        private string StatusImageKey(ReportStatus status)
        {
            string str = null;
            if (status == ReportStatus.Wait)
            {
                return "Wait";
            }
            if (status == ReportStatus.Sent)
            {
                return "Sent";
            }
            if (status == ReportStatus.Error)
            {
                return "Error";
            }
            if (status == ReportStatus.Delete)
            {
                str = "Delete";
            }
            return str;
        }

        private string StatusString(ReportStatus status)
        {
            string str = null;
            if (status == ReportStatus.Wait)
            {
                return Resources.ResourceManager.GetString("CORE113");
            }
            if (status == ReportStatus.Sent)
            {
                return Resources.ResourceManager.GetString("CORE114");
            }
            if (status == ReportStatus.Error)
            {
                return Resources.ResourceManager.GetString("CORE115");
            }
            if (status == ReportStatus.Delete)
            {
                str = Resources.ResourceManager.GetString("CORE116");
            }
            return str;
        }

        private void UpdateStatus(int index, ReportStatus status)
        {
            this.lstReport.Items[index].ImageKey = this.StatusImageKey(status);
            this.lstReport.Items[index].SubItems[this.clmStatus.Index].Text = this.StatusString(status);
        }

        public int Count
        {
            get
            {
                return this.lstReport.Items.Count;
            }
        }

        public enum ReportStatus
        {
            None,
            Wait,
            Sent,
            Error,
            Delete
        }
    }
}

