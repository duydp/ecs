-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectDynamicNew]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectDynamicNew]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadingNew_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadingNew_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadingNew_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadingNew_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadingNew_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadingNew_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectAll]


GO

-----------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectDynamicNew]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectDynamicNew]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT   A.* 
FROM [dbo].[t_KDT_VNACCS_BillOfLadingNew] A INNER JOIN dbo.t_KDT_VNACCS_BillOfLadings_Details B ON B.BillOfLadings_ID = A.ID   
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  
 GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadingNew_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TenDoanhNghiep nvarchar(255),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BillOfLadingNew]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@MaHQ,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadingNew_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TenDoanhNghiep nvarchar(255),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BillOfLadingNew]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadingNew_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TenDoanhNghiep nvarchar(255),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BillOfLadingNew] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BillOfLadingNew] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BillOfLadingNew]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[MaHQ],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@MaHQ,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@GuidStr
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadingNew_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BillOfLadingNew]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadingNew_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BillOfLadingNew] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadingNew_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BillOfLadingNew]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[GuidStr]
FROM [dbo].[t_KDT_VNACCS_BillOfLadingNew] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadingNew_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[GuidStr]
FROM
	[dbo].[t_KDT_VNACCS_BillOfLadingNew]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectBy_BillOfLadings_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectBy_BillOfLadings_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BillOfLadings_Detail_DeleteBy_BillOfLadings_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_DeleteBy_BillOfLadings_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Insert]
	@BillOfLadings_ID bigint,
	@SoVanDonGoc nvarchar(35),
	@NgayVanDonGoc datetime,
	@MaNguoiPhatHanh nvarchar(50),
	@SoLuongVDN numeric(2, 0),
	@PhanLoaiTachVD numeric(2, 0),
	@LoaiHang numeric(1, 0),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BillOfLadings_Details]
(
	[BillOfLadings_ID],
	[SoVanDonGoc],
	[NgayVanDonGoc],
	[MaNguoiPhatHanh],
	[SoLuongVDN],
	[PhanLoaiTachVD],
	[LoaiHang]
)
VALUES 
(
	@BillOfLadings_ID,
	@SoVanDonGoc,
	@NgayVanDonGoc,
	@MaNguoiPhatHanh,
	@SoLuongVDN,
	@PhanLoaiTachVD,
	@LoaiHang
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Update]
	@ID bigint,
	@BillOfLadings_ID bigint,
	@SoVanDonGoc nvarchar(35),
	@NgayVanDonGoc datetime,
	@MaNguoiPhatHanh nvarchar(50),
	@SoLuongVDN numeric(2, 0),
	@PhanLoaiTachVD numeric(2, 0),
	@LoaiHang numeric(1, 0)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BillOfLadings_Details]
SET
	[BillOfLadings_ID] = @BillOfLadings_ID,
	[SoVanDonGoc] = @SoVanDonGoc,
	[NgayVanDonGoc] = @NgayVanDonGoc,
	[MaNguoiPhatHanh] = @MaNguoiPhatHanh,
	[SoLuongVDN] = @SoLuongVDN,
	[PhanLoaiTachVD] = @PhanLoaiTachVD,
	[LoaiHang] = @LoaiHang
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_InsertUpdate]
	@ID bigint,
	@BillOfLadings_ID bigint,
	@SoVanDonGoc nvarchar(35),
	@NgayVanDonGoc datetime,
	@MaNguoiPhatHanh nvarchar(50),
	@SoLuongVDN numeric(2, 0),
	@PhanLoaiTachVD numeric(2, 0),
	@LoaiHang numeric(1, 0)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BillOfLadings_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BillOfLadings_Details] 
		SET
			[BillOfLadings_ID] = @BillOfLadings_ID,
			[SoVanDonGoc] = @SoVanDonGoc,
			[NgayVanDonGoc] = @NgayVanDonGoc,
			[MaNguoiPhatHanh] = @MaNguoiPhatHanh,
			[SoLuongVDN] = @SoLuongVDN,
			[PhanLoaiTachVD] = @PhanLoaiTachVD,
			[LoaiHang] = @LoaiHang
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BillOfLadings_Details]
		(
			[BillOfLadings_ID],
			[SoVanDonGoc],
			[NgayVanDonGoc],
			[MaNguoiPhatHanh],
			[SoLuongVDN],
			[PhanLoaiTachVD],
			[LoaiHang]
		)
		VALUES 
		(
			@BillOfLadings_ID,
			@SoVanDonGoc,
			@NgayVanDonGoc,
			@MaNguoiPhatHanh,
			@SoLuongVDN,
			@PhanLoaiTachVD,
			@LoaiHang
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BillOfLadings_Details]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_DeleteBy_BillOfLadings_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_DeleteBy_BillOfLadings_ID]
	@BillOfLadings_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BillOfLadings_Details]
WHERE
	[BillOfLadings_ID] = @BillOfLadings_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BillOfLadings_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BillOfLadings_ID],
	[SoVanDonGoc],
	[NgayVanDonGoc],
	[MaNguoiPhatHanh],
	[SoLuongVDN],
	[PhanLoaiTachVD],
	[LoaiHang]
FROM
	[dbo].[t_KDT_VNACCS_BillOfLadings_Details]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectBy_BillOfLadings_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectBy_BillOfLadings_ID]
	@BillOfLadings_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BillOfLadings_ID],
	[SoVanDonGoc],
	[NgayVanDonGoc],
	[MaNguoiPhatHanh],
	[SoLuongVDN],
	[PhanLoaiTachVD],
	[LoaiHang]
FROM
	[dbo].[t_KDT_VNACCS_BillOfLadings_Details]
WHERE
	[BillOfLadings_ID] = @BillOfLadings_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[BillOfLadings_ID],
	[SoVanDonGoc],
	[NgayVanDonGoc],
	[MaNguoiPhatHanh],
	[SoLuongVDN],
	[PhanLoaiTachVD],
	[LoaiHang]
FROM [dbo].[t_KDT_VNACCS_BillOfLadings_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BillOfLadings_Detail_SelectAll]








AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BillOfLadings_ID],
	[SoVanDonGoc],
	[NgayVanDonGoc],
	[MaNguoiPhatHanh],
	[SoLuongVDN],
	[PhanLoaiTachVD],
	[LoaiHang]
FROM
	[dbo].[t_KDT_VNACCS_BillOfLadings_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_SelectBy_BillOfLadings_Details_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_SelectBy_BillOfLadings_Details_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_DeleteBy_BillOfLadings_Details_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_DeleteBy_BillOfLadings_Details_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_Insert]
	@BillOfLadings_Details_ID bigint,
	@STT numeric(2, 0),
	@SoVanDon nvarchar(35),
	@TenNguoiGuiHang nvarchar(255),
	@DiaChiNguoiGuiHang nvarchar(255),
	@TenNguoiNhanHang nvarchar(255),
	@DiaChiNguoiNhanHang nvarchar(255),
	@TongSoLuongContainer numeric(3, 0),
	@SoLuongHang numeric(8, 0),
	@DVTSoLuong nvarchar(4),
	@TongTrongLuongHang numeric(10, 3),
	@DVTTrongLuong nvarchar(4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BranchDetail]
(
	[BillOfLadings_Details_ID],
	[STT],
	[SoVanDon],
	[TenNguoiGuiHang],
	[DiaChiNguoiGuiHang],
	[TenNguoiNhanHang],
	[DiaChiNguoiNhanHang],
	[TongSoLuongContainer],
	[SoLuongHang],
	[DVTSoLuong],
	[TongTrongLuongHang],
	[DVTTrongLuong]
)
VALUES 
(
	@BillOfLadings_Details_ID,
	@STT,
	@SoVanDon,
	@TenNguoiGuiHang,
	@DiaChiNguoiGuiHang,
	@TenNguoiNhanHang,
	@DiaChiNguoiNhanHang,
	@TongSoLuongContainer,
	@SoLuongHang,
	@DVTSoLuong,
	@TongTrongLuongHang,
	@DVTTrongLuong
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_Update]
	@ID bigint,
	@BillOfLadings_Details_ID bigint,
	@STT numeric(2, 0),
	@SoVanDon nvarchar(35),
	@TenNguoiGuiHang nvarchar(255),
	@DiaChiNguoiGuiHang nvarchar(255),
	@TenNguoiNhanHang nvarchar(255),
	@DiaChiNguoiNhanHang nvarchar(255),
	@TongSoLuongContainer numeric(3, 0),
	@SoLuongHang numeric(8, 0),
	@DVTSoLuong nvarchar(4),
	@TongTrongLuongHang numeric(10, 3),
	@DVTTrongLuong nvarchar(4)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BranchDetail]
SET
	[BillOfLadings_Details_ID] = @BillOfLadings_Details_ID,
	[STT] = @STT,
	[SoVanDon] = @SoVanDon,
	[TenNguoiGuiHang] = @TenNguoiGuiHang,
	[DiaChiNguoiGuiHang] = @DiaChiNguoiGuiHang,
	[TenNguoiNhanHang] = @TenNguoiNhanHang,
	[DiaChiNguoiNhanHang] = @DiaChiNguoiNhanHang,
	[TongSoLuongContainer] = @TongSoLuongContainer,
	[SoLuongHang] = @SoLuongHang,
	[DVTSoLuong] = @DVTSoLuong,
	[TongTrongLuongHang] = @TongTrongLuongHang,
	[DVTTrongLuong] = @DVTTrongLuong
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_InsertUpdate]
	@ID bigint,
	@BillOfLadings_Details_ID bigint,
	@STT numeric(2, 0),
	@SoVanDon nvarchar(35),
	@TenNguoiGuiHang nvarchar(255),
	@DiaChiNguoiGuiHang nvarchar(255),
	@TenNguoiNhanHang nvarchar(255),
	@DiaChiNguoiNhanHang nvarchar(255),
	@TongSoLuongContainer numeric(3, 0),
	@SoLuongHang numeric(8, 0),
	@DVTSoLuong nvarchar(4),
	@TongTrongLuongHang numeric(10, 3),
	@DVTTrongLuong nvarchar(4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BranchDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BranchDetail] 
		SET
			[BillOfLadings_Details_ID] = @BillOfLadings_Details_ID,
			[STT] = @STT,
			[SoVanDon] = @SoVanDon,
			[TenNguoiGuiHang] = @TenNguoiGuiHang,
			[DiaChiNguoiGuiHang] = @DiaChiNguoiGuiHang,
			[TenNguoiNhanHang] = @TenNguoiNhanHang,
			[DiaChiNguoiNhanHang] = @DiaChiNguoiNhanHang,
			[TongSoLuongContainer] = @TongSoLuongContainer,
			[SoLuongHang] = @SoLuongHang,
			[DVTSoLuong] = @DVTSoLuong,
			[TongTrongLuongHang] = @TongTrongLuongHang,
			[DVTTrongLuong] = @DVTTrongLuong
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BranchDetail]
		(
			[BillOfLadings_Details_ID],
			[STT],
			[SoVanDon],
			[TenNguoiGuiHang],
			[DiaChiNguoiGuiHang],
			[TenNguoiNhanHang],
			[DiaChiNguoiNhanHang],
			[TongSoLuongContainer],
			[SoLuongHang],
			[DVTSoLuong],
			[TongTrongLuongHang],
			[DVTTrongLuong]
		)
		VALUES 
		(
			@BillOfLadings_Details_ID,
			@STT,
			@SoVanDon,
			@TenNguoiGuiHang,
			@DiaChiNguoiGuiHang,
			@TenNguoiNhanHang,
			@DiaChiNguoiNhanHang,
			@TongSoLuongContainer,
			@SoLuongHang,
			@DVTSoLuong,
			@TongTrongLuongHang,
			@DVTTrongLuong
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BranchDetail]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_DeleteBy_BillOfLadings_Details_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_DeleteBy_BillOfLadings_Details_ID]
	@BillOfLadings_Details_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BranchDetail]
WHERE
	[BillOfLadings_Details_ID] = @BillOfLadings_Details_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BranchDetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BillOfLadings_Details_ID],
	[STT],
	[SoVanDon],
	[TenNguoiGuiHang],
	[DiaChiNguoiGuiHang],
	[TenNguoiNhanHang],
	[DiaChiNguoiNhanHang],
	[TongSoLuongContainer],
	[SoLuongHang],
	[DVTSoLuong],
	[TongTrongLuongHang],
	[DVTTrongLuong]
FROM
	[dbo].[t_KDT_VNACCS_BranchDetail]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_SelectBy_BillOfLadings_Details_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_SelectBy_BillOfLadings_Details_ID]
	@BillOfLadings_Details_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BillOfLadings_Details_ID],
	[STT],
	[SoVanDon],
	[TenNguoiGuiHang],
	[DiaChiNguoiGuiHang],
	[TenNguoiNhanHang],
	[DiaChiNguoiNhanHang],
	[TongSoLuongContainer],
	[SoLuongHang],
	[DVTSoLuong],
	[TongTrongLuongHang],
	[DVTTrongLuong]
FROM
	[dbo].[t_KDT_VNACCS_BranchDetail]
WHERE
	[BillOfLadings_Details_ID] = @BillOfLadings_Details_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[BillOfLadings_Details_ID],
	[STT],
	[SoVanDon],
	[TenNguoiGuiHang],
	[DiaChiNguoiGuiHang],
	[TenNguoiNhanHang],
	[DiaChiNguoiNhanHang],
	[TongSoLuongContainer],
	[SoLuongHang],
	[DVTSoLuong],
	[TongTrongLuongHang],
	[DVTTrongLuong]
FROM [dbo].[t_KDT_VNACCS_BranchDetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_SelectAll]













AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BillOfLadings_Details_ID],
	[STT],
	[SoVanDon],
	[TenNguoiGuiHang],
	[DiaChiNguoiGuiHang],
	[TenNguoiNhanHang],
	[DiaChiNguoiNhanHang],
	[TongSoLuongContainer],
	[SoLuongHang],
	[DVTSoLuong],
	[TongTrongLuongHang],
	[DVTTrongLuong]
FROM
	[dbo].[t_KDT_VNACCS_BranchDetail]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectAll]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectBy_BranchDetail_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectBy_BranchDetail_ID]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_DeleteBy_BranchDetail_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_DeleteBy_BranchDetail_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Insert]
	@BranchDetail_ID bigint,
	@SoContainer nvarchar(35),
	@SoSeal nvarchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments]
(
	[BranchDetail_ID],
	[SoContainer],
	[SoSeal]
)
VALUES 
(
	@BranchDetail_ID,
	@SoContainer,
	@SoSeal
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Update]
	@ID bigint,
	@BranchDetail_ID bigint,
	@SoContainer nvarchar(35),
	@SoSeal nvarchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments]
SET
	[BranchDetail_ID] = @BranchDetail_ID,
	[SoContainer] = @SoContainer,
	[SoSeal] = @SoSeal
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_InsertUpdate]
	@ID bigint,
	@BranchDetail_ID bigint,
	@SoContainer nvarchar(35),
	@SoSeal nvarchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments] 
		SET
			[BranchDetail_ID] = @BranchDetail_ID,
			[SoContainer] = @SoContainer,
			[SoSeal] = @SoSeal
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments]
		(
			[BranchDetail_ID],
			[SoContainer],
			[SoSeal]
		)
		VALUES 
		(
			@BranchDetail_ID,
			@SoContainer,
			@SoSeal
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_DeleteBy_BranchDetail_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_DeleteBy_BranchDetail_ID]
	@BranchDetail_ID bigint
AS

DELETE FROM [dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments]
WHERE
	[BranchDetail_ID] = @BranchDetail_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BranchDetail_ID],
	[SoContainer],
	[SoSeal]
FROM
	[dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectBy_BranchDetail_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectBy_BranchDetail_ID]
	@BranchDetail_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BranchDetail_ID],
	[SoContainer],
	[SoSeal]
FROM
	[dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments]
WHERE
	[BranchDetail_ID] = @BranchDetail_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[BranchDetail_ID],
	[SoContainer],
	[SoSeal]
FROM [dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_BranchDetail_TransportEquipment_SelectAll]




AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[BranchDetail_ID],
	[SoContainer],
	[SoSeal]
FROM
	[dbo].[t_KDT_VNACCS_BranchDetail_TransportEquipments]	

GO

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '27.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('27.6',GETDATE(), N'CẬP NHẬT KHAI BÁO TÁCH VẬN ĐƠN')
END