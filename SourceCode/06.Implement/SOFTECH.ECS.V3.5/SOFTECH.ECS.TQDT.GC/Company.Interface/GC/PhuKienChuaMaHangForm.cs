using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT.GC;

namespace Company.Interface.GC
{
    public partial class PhuKienChuaMaHangForm : BaseForm
    {
        public HopDong HD = new HopDong();
        public PhuKienChuaMaHangForm()
        {
            InitializeComponent();
        }
        public PhuKienDangKyCollection PKDKCollection = new PhuKienDangKyCollection();
        private void PhuKienChuaMaHangForm_Load(object sender, EventArgs e)
        {
            dgPhuKien.DataSource = this.PKDKCollection;
        }
        private void dgPhuKien_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            PhuKienDangKy PKDK = (PhuKienDangKy)dgPhuKien.GetRow().DataRow;
            PhuKienGCDetailForm pkForm = new PhuKienGCDetailForm();
            pkForm.HD = this.HD;
            pkForm.PKDK = PKDK;
            pkForm.ShowDialog();
        }

        private void menuExportExcel_Click(object sender, EventArgs e)
        {
            SaveFileDialog sf = new SaveFileDialog();
            sf.FileName = "Danh sách Phụ kiện của Hợp đồng_" + HD.SoHopDong.Replace("/","-") + "_" + DateTime.Now.ToString("dd/MM/yyyy").Replace("/", "_") + ".xls";
            sf.Filter = "Excel File | *.xls ";

            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                Janus.Windows.GridEX.Export.GridEXExporter grdExport = new Janus.Windows.GridEX.Export.GridEXExporter();
                grdExport.GridEX = dgPhuKien;

                try
                {
                    System.IO.Stream str = sf.OpenFile();
                    grdExport.Export(str);
                    str.Close();
                    if (ShowMessage("Bạn có muốn mở File này không", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sf.FileName);
                    }
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi trong quá trình mở File" + ex.Message, false);
                }
            }
            else
            {
                ShowMessage("Xuất Excel không thành công", false);
            }
        }
    }
}

