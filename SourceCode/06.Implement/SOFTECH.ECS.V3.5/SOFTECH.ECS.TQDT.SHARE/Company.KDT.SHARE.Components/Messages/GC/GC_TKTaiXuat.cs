﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    //Phụ kiện hợp đồng
    [XmlRoot("Declaration")]
    public class GC_TKTaiXuat : IssueBase
    {
        /// <summary>
        /// Số tờ khai 
        /// </summary>
        [XmlElement("customsReference")]
        public string CustomsReference { get; set; }
        /// <summary>
        /// Ngày đăng ký YYYY-MM-DD HH:mm:ss
        /// </summary>
        [XmlElement("acceptance")]
        public string Acceptance { get; set; }
        /// <summary>
        /// Trạng thái
        /// </summary>
        [XmlElement("status")]
        public string Status { get; set; }
        /// <summary>
        /// Đại lý
        /// </summary>
        [XmlElement("Agent")]
        public List<Agent> Agents { get; set; }
        /// <summary>
        /// Doanh nghiệp nhập khẩu
        /// </summary>
        [XmlElement("Importer")]
        public NameBase Importer { get; set; }

        [XmlElement("ContractDocument")]
        public ContractDocument contractReference { get; set; }

        [XmlElement("DeclarationDocument")]
        public List<GC_DeclarationDocument> declarationDocument { get; set; }

        [XmlElement("MaterialReExport")]
        public GC_DeclarationDocument materialReExport { get; set; }
       

    }
}
