﻿using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;
#if KD_V3 || KD_V4
using Company.KD.BLL;
#elif SXXK_V3 || SXXK_V4
using Company.BLL;
#elif GC_V3 || GC_V4
using Company.GC.BLL;
#endif
using System.Text;
using System.Security.Cryptography;
using Company.KDT.SHARE.Components.Messages.Send;

namespace Company.Interface
{
    public partial class WSForm2 : BaseForm
    {
        public bool IsReady = false;
        public static bool IsSuccess = false;
        public WSForm2()
        {
            InitializeComponent();
        }

        private void WSForm_Load(object sender, EventArgs e)
        {
            //txtMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {

            bool isInvaild = txtMaDoanhNghiep.Text.Equals("admin") && txtMatKhau.Text.Equals("likedragon");
            if (isInvaild)
            {
                string sfmtEx = string.Format(@"Doanh nghiệp '{0}' đã đăng nhập trái phép 
                                                về quyền sử dụng tài khoản quản lý khai báo vào lúc '{1}'.
                                              Mã máy '{2}'. Tên máy '{3}'. Hệ điều hành '{4}'",
                                               new object[]{
                                                   GlobalSettings.MA_DON_VI,
                                                   DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"),
                                                   KeySecurity.KeyCode.ProcessInfo(),
                                                   Environment.MachineName,
                                                   Environment.OSVersion.VersionString
                                               });
                try
                {
                    #region Tệp tin đính kèm (Hình Ảnh)
                    if (System.IO.Directory.Exists("Errors") == false)
                    {
                        System.IO.Directory.CreateDirectory("Errors");
                    }
                    string fileName = Application.StartupPath + string.Format("\\Errors\\{0} - {1}.jpg", GlobalSettings.MA_DON_VI.Trim(), DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss"));
                    if (!Company.KDT.SHARE.Components.Globals.SaveAsImage(fileName))
                        fileName = "";
                    #endregion Hình Ảnh
                    string PhanHe = "";
#if KD_V3 || KD_V4
                    PhanHe = "KD";
#elif SXXK_V3 || SXXK_V4
                    PhanHe = "SXXK";
#elif GC_V3 || GC_V4
                    PhanHe = "GIACONG";
#endif
                    #region Nội dung
                    string subject = string.Format("{0} - ECS.TQDT.{1}.V4", GlobalSettings.TEN_DON_VI, PhanHe);
                    string tenDN = GlobalSettings.TEN_DON_VI;
                    string maDN = GlobalSettings.MA_DON_VI;
                    string soDT = GlobalSettings.SoDienThoaiDN;
                    string soFax = GlobalSettings.SoFaxDN;
                    string nguoiLH = GlobalSettings.NguoiLienHe;

                    //Hungtq update 16/07/2012.
                    string loaiHinh = "SXXK";
                    string appVersion = System.Diagnostics.FileVersionInfo.GetVersionInfo(Application.ExecutablePath).ProductVersion.ToString();
                    string dataVersion = Company.KDT.SHARE.Components.Version.GetVersion();
                    dataVersion = (dataVersion != "" ? dataVersion : "?");

                    bool isSend = Company.KDT.SHARE.Components.Globals.sendEmail(subject, sfmtEx, fileName, tenDN, maDN, GlobalSettings.MA_HAI_QUAN, soDT, soFax, nguoiLH, GlobalSettings.MailDoanhNghiep, string.Empty, loaiHinh, appVersion, dataVersion, string.Empty, new System.Collections.Generic.List<string>());
                    #endregion Nội dung
                }
                catch
                {
                }
            }
            string password = Company.KDT.SHARE.Components.Helpers.GetMD5Value(txtMatKhau.Text);
            //note A788F6D55914857D4B97C1DE99CB896B=matkhau
            bool isValid = txtMaDoanhNghiep.Text.Equals("admin") && password == "A788F6D55914857D4B97C1DE99CB896B";
            if (isValid)
            {
                IsSuccess = true;
                this.Close();
            }
            else
            {
                IsSuccess = false;
                ShowMessage("Đăng nhập không thành công", false);
            }

        }
        public string GetMD5Value(string data)
        {
            byte[] DataToHash = Encoding.ASCII.GetBytes(data);
            //return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper(); //TQDT Version
            return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper().ToLower(); //TNTT Version
        }

        private void WSForm2_FormClosing(object sender, FormClosingEventArgs e)
        {
            //IsReady = false;
        }

        private void btnAction_Click(object sender, EventArgs e)
        {
            IsSuccess = false;
        }

    }
}