﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Xml;

namespace Company.KDT.SHARE.Components
{
    /// <summary>
    /// Nội dung gửi.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [XmlRoot("Content")]
    public class MessageContent<T>
    {
        private T _declaration;

        /// <summary>
        /// Nội dung cần gửi đến hải quan
        /// </summary>
        [XmlElement("Declaration")]
        public T Declaration
        {
            get
            {
                return _declaration;
            }
            set
            {
                _declaration = value;
            }

        }        
        [XmlText()]
        public string Text { get; set; }


        /// <summary>
        /// Thông tin XML
        /// </summary>
        public Company.KDT.SHARE.Components.Messages.ThuPhiTuDongHP.Register_Information Register_Information { get; set; }

    }
}
