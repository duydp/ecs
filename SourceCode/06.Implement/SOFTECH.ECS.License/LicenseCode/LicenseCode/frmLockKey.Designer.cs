﻿namespace LicenseCode
{
    partial class frmLockKey
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLockKey));
            this.txtLyDo = new System.Windows.Forms.TextBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnLock = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.lblStatus = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtActiveCode = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblNguoiCapKey = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblProductID = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTenDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblMaDoanhNghiep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblNgayActive = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.lblNgayCapKey = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.lblNgayHetHan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.lblNgayCuoi = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label14 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtLyDo
            // 
            this.txtLyDo.Location = new System.Drawing.Point(149, 281);
            this.txtLyDo.Margin = new System.Windows.Forms.Padding(4);
            this.txtLyDo.Multiline = true;
            this.txtLyDo.Name = "txtLyDo";
            this.txtLyDo.Size = new System.Drawing.Size(554, 106);
            this.txtLyDo.TabIndex = 5;
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.AutoScroll = true;
            this.uiGroupBox7.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox7.Controls.Add(this.uiGroupBox1);
            this.uiGroupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox7.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox7.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(759, 451);
            this.uiGroupBox7.TabIndex = 12;
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // btnLock
            // 
            this.btnLock.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLock.Image = ((System.Drawing.Image)(resources.GetObject("btnLock.Image")));
            this.btnLock.ImageIndex = 0;
            this.btnLock.ImageSize = new System.Drawing.Size(20, 20);
            this.btnLock.Location = new System.Drawing.Point(365, 403);
            this.btnLock.Name = "btnLock";
            this.btnLock.Size = new System.Drawing.Size(84, 24);
            this.btnLock.TabIndex = 63;
            this.btnLock.Text = "Lock";
            this.btnLock.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnLock.Click += new System.EventHandler(this.btnLock_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.btnLock);
            this.uiGroupBox1.Controls.Add(this.txtLyDo);
            this.uiGroupBox1.Controls.Add(this.lblStatus);
            this.uiGroupBox1.Controls.Add(this.txtActiveCode);
            this.uiGroupBox1.Controls.Add(this.lblNguoiCapKey);
            this.uiGroupBox1.Controls.Add(this.lblProductID);
            this.uiGroupBox1.Controls.Add(this.lblTenDoanhNghiep);
            this.uiGroupBox1.Controls.Add(this.lblMaDoanhNghiep);
            this.uiGroupBox1.Controls.Add(this.lblNgayActive);
            this.uiGroupBox1.Controls.Add(this.lblNgayCapKey);
            this.uiGroupBox1.Controls.Add(this.lblNgayHetHan);
            this.uiGroupBox1.Controls.Add(this.lblNgayCuoi);
            this.uiGroupBox1.Controls.Add(this.label14);
            this.uiGroupBox1.Controls.Add(this.label22);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label40);
            this.uiGroupBox1.Controls.Add(this.label13);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.label15);
            this.uiGroupBox1.Controls.Add(this.label17);
            this.uiGroupBox1.Controls.Add(this.label16);
            this.uiGroupBox1.Controls.Add(this.lbl);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(759, 451);
            this.uiGroupBox1.TabIndex = 13;
            this.uiGroupBox1.Text = "Thông tin CD Key";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(558, 197);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.ReadOnly = true;
            this.lblStatus.Size = new System.Drawing.Size(145, 23);
            this.lblStatus.TabIndex = 25;
            this.lblStatus.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtActiveCode
            // 
            this.txtActiveCode.Location = new System.Drawing.Point(149, 31);
            this.txtActiveCode.Name = "txtActiveCode";
            this.txtActiveCode.Size = new System.Drawing.Size(284, 23);
            this.txtActiveCode.TabIndex = 24;
            this.txtActiveCode.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.txtActiveCode.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblNguoiCapKey
            // 
            this.lblNguoiCapKey.Location = new System.Drawing.Point(149, 239);
            this.lblNguoiCapKey.Name = "lblNguoiCapKey";
            this.lblNguoiCapKey.ReadOnly = true;
            this.lblNguoiCapKey.Size = new System.Drawing.Size(554, 23);
            this.lblNguoiCapKey.TabIndex = 23;
            this.lblNguoiCapKey.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblProductID
            // 
            this.lblProductID.Location = new System.Drawing.Point(149, 153);
            this.lblProductID.Name = "lblProductID";
            this.lblProductID.ReadOnly = true;
            this.lblProductID.Size = new System.Drawing.Size(284, 23);
            this.lblProductID.TabIndex = 28;
            this.lblProductID.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.Location = new System.Drawing.Point(149, 115);
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.ReadOnly = true;
            this.lblTenDoanhNghiep.Size = new System.Drawing.Size(284, 23);
            this.lblTenDoanhNghiep.TabIndex = 27;
            this.lblTenDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.Location = new System.Drawing.Point(149, 73);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.ReadOnly = true;
            this.lblMaDoanhNghiep.Size = new System.Drawing.Size(284, 23);
            this.lblMaDoanhNghiep.TabIndex = 26;
            this.lblMaDoanhNghiep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // lblNgayActive
            // 
            this.lblNgayActive.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.lblNgayActive.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.lblNgayActive.DropDownCalendar.Name = "";
            this.lblNgayActive.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.lblNgayActive.Location = new System.Drawing.Point(149, 193);
            this.lblNgayActive.Name = "lblNgayActive";
            this.lblNgayActive.ReadOnly = true;
            this.lblNgayActive.Size = new System.Drawing.Size(155, 23);
            this.lblNgayActive.TabIndex = 20;
            this.lblNgayActive.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // lblNgayCapKey
            // 
            this.lblNgayCapKey.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.lblNgayCapKey.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.lblNgayCapKey.DropDownCalendar.Name = "";
            this.lblNgayCapKey.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.lblNgayCapKey.Location = new System.Drawing.Point(563, 153);
            this.lblNgayCapKey.Name = "lblNgayCapKey";
            this.lblNgayCapKey.ReadOnly = true;
            this.lblNgayCapKey.Size = new System.Drawing.Size(152, 23);
            this.lblNgayCapKey.TabIndex = 19;
            this.lblNgayCapKey.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // lblNgayHetHan
            // 
            this.lblNgayHetHan.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.lblNgayHetHan.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.lblNgayHetHan.DropDownCalendar.Name = "";
            this.lblNgayHetHan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.lblNgayHetHan.Location = new System.Drawing.Point(563, 115);
            this.lblNgayHetHan.Name = "lblNgayHetHan";
            this.lblNgayHetHan.ReadOnly = true;
            this.lblNgayHetHan.Size = new System.Drawing.Size(152, 23);
            this.lblNgayHetHan.TabIndex = 22;
            this.lblNgayHetHan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // lblNgayCuoi
            // 
            this.lblNgayCuoi.CustomFormat = "dd/MM/yyyy hh:mm:ss";
            this.lblNgayCuoi.DateFormat = Janus.Windows.CalendarCombo.DateFormat.Custom;
            // 
            // 
            // 
            this.lblNgayCuoi.DropDownCalendar.Name = "";
            this.lblNgayCuoi.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.lblNgayCuoi.Location = new System.Drawing.Point(563, 73);
            this.lblNgayCuoi.Name = "lblNgayCuoi";
            this.lblNgayCuoi.ReadOnly = true;
            this.lblNgayCuoi.Size = new System.Drawing.Size(152, 23);
            this.lblNgayCuoi.TabIndex = 21;
            this.lblNgayCuoi.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Location = new System.Drawing.Point(13, 31);
            this.label14.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 16);
            this.label14.TabIndex = 12;
            this.label14.Text = "Actived Key :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Location = new System.Drawing.Point(450, 156);
            this.label22.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(92, 16);
            this.label22.TabIndex = 13;
            this.label22.Text = "Ngày cấp key :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(13, 73);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 16);
            this.label3.TabIndex = 11;
            this.label3.Text = "Mã doanh nghiệp :";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Location = new System.Drawing.Point(450, 200);
            this.label40.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(76, 16);
            this.label40.TabIndex = 9;
            this.label40.Text = "Trạng thái :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Location = new System.Drawing.Point(13, 115);
            this.label13.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 16);
            this.label13.TabIndex = 10;
            this.label13.Text = "Tên doanh nghiệp :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Location = new System.Drawing.Point(13, 239);
            this.label18.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(97, 16);
            this.label18.TabIndex = 17;
            this.label18.Text = "Người cấp key :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Location = new System.Drawing.Point(13, 197);
            this.label15.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(82, 16);
            this.label15.TabIndex = 18;
            this.label15.Text = "Ngày active :";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Location = new System.Drawing.Point(450, 76);
            this.label17.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(106, 16);
            this.label17.TabIndex = 16;
            this.label17.Text = "Đăng nhập cuối :";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Location = new System.Drawing.Point(450, 118);
            this.label16.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(92, 16);
            this.label16.TabIndex = 14;
            this.label16.Text = "Ngày hết hạn :";
            // 
            // lbl
            // 
            this.lbl.AutoSize = true;
            this.lbl.BackColor = System.Drawing.Color.Transparent;
            this.lbl.Location = new System.Drawing.Point(13, 153);
            this.lbl.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lbl.Name = "lbl";
            this.lbl.Size = new System.Drawing.Size(94, 16);
            this.lbl.TabIndex = 15;
            this.lbl.Text = "Mã sản phẩm :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(13, 281);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 16);
            this.label1.TabIndex = 17;
            this.label1.Text = "Lý do :";
            // 
            // frmLockKey
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(759, 451);
            this.Controls.Add(this.uiGroupBox7);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(118)))), ((int)(((byte)(61)))));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmLockKey";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Lock Key";
            this.Load += new System.EventHandler(this.frmLockKey_Load);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtLyDo;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox7;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIButton btnLock;
        private Janus.Windows.GridEX.EditControls.EditBox lblStatus;
        private Janus.Windows.GridEX.EditControls.EditBox txtActiveCode;
        private Janus.Windows.GridEX.EditControls.EditBox lblNguoiCapKey;
        private Janus.Windows.GridEX.EditControls.EditBox lblProductID;
        private Janus.Windows.GridEX.EditControls.EditBox lblTenDoanhNghiep;
        private Janus.Windows.GridEX.EditControls.EditBox lblMaDoanhNghiep;
        private Janus.Windows.CalendarCombo.CalendarCombo lblNgayActive;
        private Janus.Windows.CalendarCombo.CalendarCombo lblNgayCapKey;
        private Janus.Windows.CalendarCombo.CalendarCombo lblNgayHetHan;
        private Janus.Windows.CalendarCombo.CalendarCombo lblNgayCuoi;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lbl;
        private System.Windows.Forms.Label label1;
    }
}