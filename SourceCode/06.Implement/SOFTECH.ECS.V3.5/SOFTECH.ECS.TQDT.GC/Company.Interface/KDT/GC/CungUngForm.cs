﻿using System;
using Company.GC.BLL;
using Company.GC.BLL.KDT.GC;
using Company.GC.BLL.Utils;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using System.Windows.Forms;
using System.Data;
using Company.KDT.SHARE.Components;
using System.Collections.Generic;
using System.Linq;
using ToKhaiMauDich = Company.GC.BLL.KDT.ToKhaiMauDich;
using Company.Interface.GC;
using SOFTECH.ECS.TQDT.GC.KDT.GC;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.GC.BLL.KDT.SXXK;
using Company.GC.BLL.DataTransferObjectMapper;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.VNACCS;
namespace Company.Interface.KDT.GC
{
    public partial class CungUngForm : BaseForm
    {
        public KDT_GC_CungUngDangKy CungUngDK = new KDT_GC_CungUngDangKy();
        private KDT_GC_CungUng CungUng = new KDT_GC_CungUng();
        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        bool isEdit = false;
        public bool isAuto = false;
        public bool isTranfer = false;
        public DataTable tbNPLCU = new DataTable();
        public DataTable tbNPLDetail = new DataTable();
        public HopDong HD = new HopDong();
        public CungUngForm()
        {
            InitializeComponent();
        }


        private void setCungUngAuTo()
        {
            if (CungUngDK.ID == 0)
                CungUngDK.TrangThaiXuLy = -1;
            txtSoHopDong.Text = HD.SoHopDong;
            clcNgayTiepNhan.Value = CungUngDK.NgayTiepNhan;
            clcNgayHD.Value = HD.NgayKy;
            clcNgayHetHan.Value = HD.NgayHetHan;
            txtSoTiepNhan.Text = CungUngDK.SoTiepNhan.ToString();
            ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            foreach (DataRow dr in tbNPLCU.Rows)
            {
                KDT_GC_CungUng CU = new KDT_GC_CungUng();
                CU.MaNPL = dr["Ma"].ToString();
                CU.TenNPL = dr["Ten"].ToString();
                CU.LuongCungUng = Convert.ToDecimal(dr["LuongTonCuoi"].ToString());
                CU.ThanhTien = 0;
                CU.DVT_ID = dr["DVT"].ToString();
                CungUngDK.NPLCungUng_List.Add(CU);
            }
            dgList.DataSource = CungUngDK.NPLCungUng_List;
            //dgList.DataSource = KDT_GC_CungUng.SelectDynamic("Master_ID = " + CungUngDK.ID, "").Tables[0];
            dgList.Refetch();
        }
        private void CungUngForm_Load(object sender, EventArgs e)
         {
            if (isAuto)
            {
                setCungUngAuTo();
            }
            else if (isTranfer)
            {
                try
                {
                    if (CungUngDK.ID == 0)
                        CungUngDK.TrangThaiXuLy = -1;
                    txtSoHopDong.Text = HD.SoHopDong;
                    clcNgayTiepNhan.Value = CungUngDK.NgayTiepNhan;
                    clcNgayHD.Value = HD.NgayKy;
                    clcNgayHetHan.Value = HD.NgayHetHan;
                    txtSoTiepNhan.Text = CungUngDK.SoTiepNhan.ToString();
                    ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                    foreach (DataRow dr in tbNPLCU.Rows)
                    {
                        KDT_GC_CungUng CU = new KDT_GC_CungUng();
                        CU.MaNPL = dr["MaNPL"].ToString();
                        Company.GC.BLL.GC.NguyenPhuLieu NPL = Company.GC.BLL.GC.NguyenPhuLieu.LoadNew(CU.MaNPL,HD.ID);
                        CU.TenNPL = NPL.Ten;
                        CU.LuongCungUng = Convert.ToDecimal(dr["LuongCungUng"].ToString());
                        CU.ThanhTien = 0;
                        CU.DVT_ID = DonViTinh_GetName(NPL.DVT_ID);
                        CungUngDK.NPLCungUng_List.Add(CU);
                    }
                    dgList.DataSource = CungUngDK.NPLCungUng_List;
                    dgList.Refetch();
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            else
            {
                LoadData();
            }

            SetCommand();

        }
        private void LoadData()
        {
            if (CungUngDK.ID == 0)
                CungUngDK.TrangThaiXuLy = -1;
            txtSoHopDong.Text = HD.SoHopDong;
            clcNgayTiepNhan.Value = CungUngDK.NgayTiepNhan;
            clcNgayHD.Value = HD.NgayKy;
            clcNgayHetHan.Value = HD.NgayHetHan;
            txtSoTiepNhan.Text = CungUngDK.SoTiepNhan.ToString();
            //clcNgayTiepNhan.Value = CungUngDK.NgayTiepNhan;
            if (CungUngDK.MaHQ == null || CungUngDK.MaHQ.Trim() == "")
                ctrMaHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            else
                ctrMaHaiQuan.Code = CungUngDK.MaHQ;// CungUngDK.MaHQ;
//            dgList.DataSource = CungUngDK.NPLCungUng_List;
            dgList.DataSource = KDT_GC_CungUng.SelectDynamic("Master_ID = " + CungUngDK.ID, "").Tables[0];
            dgList.Refetch();
        }
        private void SetCommand()
        {
            int TrangThai = CungUngDK.TrangThaiXuLy;
            if (CungUngDK.TrangThaiXuLy == -1 || CungUngDK.TrangThaiXuLy == 2)
            {
                cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdHuyKhaiBao.Enabled = cmdHuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                if (TrangThai == 2)
                    cmdHuyKhaiBao.Enabled = cmdHuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnXoa.Enabled = true;
            }
            else
            {
                cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnXoa.Enabled = false;
                if (TrangThai == 1)
                    cmdHuyKhaiBao.Enabled = cmdHuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                else
                    cmdHuyKhaiBao.Enabled = cmdHuyKhaiBao1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            }


        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                List<KDT_GC_CungUng> ItemColl = new List<KDT_GC_CungUng>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa NPL này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_GC_CungUng)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_GC_CungUng item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.DeleteFull(item.ID);
                        CungUngDK.NPLCungUng_List.Remove(item);
                    }
                    dgList.Refetch();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }

        }

        private void cmdMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdNPLCungUng":
                    ThemNPL();
                    break;
                case "cmdSave":
                    Save();
                    break;
                case "cmdSend":
                    SendV5();
                    break;
                case "cmdFeedBack":
                    FeedBackV5();
                    break;
                case "cmdKetQuaXuLy":
                    KetQuaXuLyPK();
                    break;
                case "cmdHuyKhaiBao":
                    SendHuyV5();
                    break;
            }
        }
        private void SendHuyV5()
        {
            if (ShowMessage("Bạn có muốn khai báo hủy bảng kê tự cung ứng này ?", true) == "Yes")
            {
                //   bool isKhaiSua = false;
                if (CungUngDK.ID == 0)
                {
                    this.ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    CungUngDK = KDT_GC_CungUngDangKy.LoadFull(CungUngDK.ID);
                    CungUngDK.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    //if (BKCungUng.TrangThaiXuLy == 5)
                    //    isKhaiSua = true;
                    //else
                    //    isKhaiSua = false;
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
                sendXML.master_id = CungUngDK.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                    return;
                }
                try
                {
                    if (this.CungUngDK.NPLCungUng_List.Count > 0)
                    {
                        string returnMessage = string.Empty;
                        CungUngDK.GuidStr = Guid.NewGuid().ToString();
                        //SXXK_TuCungUng TuCungUng = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferTuCungUng(BKCungUng, GlobalSettings.TEN_DON_VI, isKhaiSua);
                        //PhuKienToKhai phukien = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferPhuKienToKhai(BKCungUng, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = CancelMessageV5(CungUngDK);
                        //BKCungUng.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(CungUngDK.ID, "Hủy khai báo bảng kê");
                            cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdSave.Enabled = cmdSave1.Enabled = cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            //btnDelete.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
                            sendXML.master_id = CungUngDK.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            CungUngDK.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }
        public static ObjectSend CancelMessageV5(KDT_GC_CungUngDangKy BK)
        {
            string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
            string sfmtDate = "yyyy-MM-dd";
            //string sfmtVnDateTime = "dd-MM-yyyy HH:mm:ss";
            //bool IsToKhaiNhap = BK.MaLoaiHinh.StartsWith("N");
            BK.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
            //bool IsHuyToKhai = (BK.ActionStatus == (short)ActionStatus.ToKhaiXinHuy);
            bool IsHuyToKhai = true;
            string issuer = string.Empty;
            issuer = DeclarationIssuer.GC_TUCUNGUNG;

            DeclarationBase declaredCancel = new DeclarationBase()
            {
                Issuer = issuer,
                Reference = BK.GuidStr,
                IssueLocation = string.Empty,
                Agents = new List<Agent>(),

                Issue = DateTime.Now.ToString(sfmtDateTime),
                Function = DeclarationFunction.HUY,
                Status = AgentsStatus.DAILY_LAM_THUTUC_HAIQUAN,
                CustomsReference = Helpers.FormatNumeric(BK.SoTiepNhan, 0),
                Acceptance = BK.NgayTiepNhan.ToString(sfmtDate),
                DeclarationOffice = BK.MaHQ,
                Importer = new NameBase()
                {
                    Name = GlobalSettings.TEN_DON_VI,
                    Identity = BK.MaDoanhNghiep
                },
                AdditionalInformations = new List<AdditionalInformation>()
            };
            declaredCancel.Agents.Add(new Agent()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = BK.MaDoanhNghiep,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            declaredCancel.AdditionalInformations.Add(
                new AdditionalInformation()
                {
                    Content = new Content() { Text = "Xin hủy bảng kê NPL cung ứng do sai số liệu" }
                }
                );
            if (IsHuyToKhai)
            {
                //List<HuyToKhai> cancelContent = (List<HuyToKhai>)HuyToKhai.SelectCollectionBy_BK_ID(BK.ID);
                //if (cancelContent.Count > 0)
                //{
                //    declaredCancel.AdditionalInformations[0].Content.Text = cancelContent[0].LyDoHuy;
                //    declaredCancel.Reference = guidHuyTK;
                //}
            }
            ObjectSend objSend = new ObjectSend(new NameBase()
            {
                Name = GlobalSettings.TEN_DON_VI,
                Identity = BK.MaDoanhNghiep
            },
            new NameBase()
            {
                Name = DonViHaiQuan.GetName(GlobalSettings.MA_HAI_QUAN),
                Identity = BK.MaHQ.Trim()
            },
            new SubjectBase()
            {
                Type = issuer,
                Function = declaredCancel.Function,
                Reference = declaredCancel.Reference //BK.GUIDSTR
            }, declaredCancel);
            return objSend;
        }
        private void ThemNPL()
        {
            NguyenPhuLieuRegistedForm NPLRegistedForm = new NguyenPhuLieuRegistedForm();
            NPLRegistedForm.isBrower = true;
            NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = CungUngDK.HopDong_ID;
            NPLRegistedForm.ShowDialog();
            if (!string.IsNullOrEmpty(NPLRegistedForm.NguyenPhuLieuSelected.Ma))
            {

                CungUng.MaNPL = NPLRegistedForm.NguyenPhuLieuSelected.Ma;
                CungUng.TenNPL = NPLRegistedForm.NguyenPhuLieuSelected.Ten;
                CungUng.MaHS = NPLRegistedForm.NguyenPhuLieuSelected.MaHS;
                CungUng.DVT_ID = VNACCS_Mapper.GetCodeVNACC(NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));

                CungUngNPLForm frm = new CungUngNPLForm();
                frm.CungUngNPL = CungUng;
                frm.CungUngDK = CungUngDK;
                frm.ShowDialog();
                //LoadData();
                CungUng = new KDT_GC_CungUng();
            }
            LoadData();
        }
        private void Save()
        {
            try
            {
                CungUngDK.NgayTiepNhan = clcNgayTiepNhan.Value;
                CungUngDK.SoHopDong = HD.SoHopDong;
                CungUngDK.HopDong_ID = HD.ID;
                CungUngDK.NgayHopDong = HD.NgayKy;
                CungUngDK.NgayHetHan = HD.NgayHetHan;
                CungUngDK.MaHQ = ctrMaHaiQuan.Code;
                CungUngDK.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                if (CungUngDK.ID == 0)
                {
                    CungUngDK.TrangThaiXuLy = -1;
                    CungUngDK.Insert();
                }
                else
                {
                    CungUngDK.Update();
                }
                if (isAuto)
                    if (CungUngDK.NPLCungUng_List.Count > 0)
                    {
                        foreach (var item in CungUngDK.NPLCungUng_List)
                        {
                            item.Master_ID = CungUngDK.ID;
                            item.InsertUpdate();
                        }
                    }
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                ShowMessage("Lưu không thành công : " + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void KetQuaXuLyPK()
        {
            ThongDiepForm form = new ThongDiepForm();
            form.ItemID = this.CungUngDK.ID;
            form.DeclarationIssuer = DeclarationIssuer.GC_TUCUNGUNG;
            form.ShowDialog(this);
        }

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            //MsgSend sendXML = new MsgSend();
            //sendXML.LoaiHS = LoaiKhaiBao.PhuKien_TK;
            //sendXML.master_id = CungUngDK.ID;
            //if (!sendXML.Load())
            //{
            //    ShowMessage("Thông tin chưa được gửi đến hải quan. Xin kiểm tra lại", false);
            //    XacNhanThongTin.Enabled = XacNhanThongTin1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
            //    cmdAdd.Enabled = cmdAddNew1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
            //    return;
            //}
            while (isFeedBack)
            {
                string reference = CungUngDK.GuidStr;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.GC_TUCUNGUNG,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = DeclarationIssuer.GC_TUCUNGUNG,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = CungUngDK.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(CungUngDK.MaHQ.Trim())),
                                                  //Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(CungUngDK.MaHQ).Trim() : CungUngDK.MaHQ
                                                  Identity = CungUngDK.MaHQ
                                              }, subjectBase, null);
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }
            }
            CungUngDK = KDT_GC_CungUngDangKy.LoadFull(CungUngDK.ID);
            LoadData();
        }

        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            feedbackContent = SingleMessage.NPLCungUngSendHandler(CungUngDK, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void SendV5()
        {
            if (ShowMessage("Bạn có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            {
                if (CungUngDK.ID == 0)
                {
                    this.ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    CungUngDK = KDT_GC_CungUngDangKy.LoadFull(CungUngDK.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
                sendXML.master_id = CungUngDK.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    if (this.CungUngDK.NPLCungUng_List.Count > 0)
                    {

                        string returnMessage = string.Empty;
                        CungUngDK.GuidStr = Guid.NewGuid().ToString();
                        GC_NPLCungUng nplCungUng = Mapper_V4.ToDataTransferTuCungUng(CungUngDK, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = new ObjectSend(
                             new NameBase()
                             {
                                 Name = GlobalSettings.TEN_DON_VI,
                                 Identity = CungUngDK.MaDoanhNghiep
                             },
                              new NameBase()
                              {
                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(CungUngDK.MaHQ)),
                                  // Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? VNACCS_Mapper.GetCodeVNACCMaHaiQuan(CungUngDK.MaHQ).Trim() : CungUngDK.MaHQ
                                  Identity = CungUngDK.MaHQ
                              },
                              new SubjectBase()
                              {

                                  Type = nplCungUng.Issuer,
                                  Function = DeclarationFunction.KHAI_BAO,
                                  Reference = CungUngDK.GuidStr,
                              },
                              nplCungUng
                            );
                        CungUngDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            sendForm.Message.XmlSaveMessage(CungUngDK.ID, MessageTitle.KhaiBaoNPLTuCungUng);
                            cmdFeedBack.Enabled = cmdFeedBack1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdNPLCungUng.Enabled = cmdNPLCungUng1.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            btnXoa.Enabled = false;
                            sendXML.LoaiHS = LoaiKhaiBao.TuCungUng;
                            sendXML.master_id = CungUngDK.ID;
                            //sendXML.msg = msgSend.
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            CungUngDK.Update();
                            FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    
                }
            }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            
            if (isAuto )
            {
                int id = dgList.CurrentRow.Position;
                CungUngNPLForm frm = new CungUngNPLForm();
                frm.CungUngNPL = CungUngDK.NPLCungUng_List[id];
                frm.CungUngDK = CungUngDK;

                //frm.HopDong_ID = CungUngDK.HopDong_ID;
                frm.ShowDialog();
            }
            else if (isTranfer)
            {
                try
                {
                    int id = dgList.CurrentRow.Position;
                    CungUngNPLForm frm = new CungUngNPLForm();
                    //CungUngDK.NPLCungUng_List.Clear();
                    CungUngDK.NPLCungUng_List[id].CungUngDetail_List.Clear();
                    for (int i = 0; i < tbNPLDetail.Rows.Count; i++)
                    {
                        KDT_GC_CungUng_Detail nplCungUng = new KDT_GC_CungUng_Detail();
                        string MaNPL = tbNPLDetail.Rows[i]["MaNPL"].ToString();
                        nplCungUng.SoLuong = Convert.ToDecimal(tbNPLDetail.Rows[i]["LuongCungUng"].ToString());
                        if (MaNPL == e.Row.Cells["MaNPL"].Value.ToString() && nplCungUng.SoLuong == Convert.ToDecimal(e.Row.Cells["LuongCungUng"].Value.ToString()))
                        {
                            nplCungUng.Master_ID = CungUngDK.ID;
                            nplCungUng.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;//tbNPLDetail.Rows[i]["TenSP"].ToString();                            
                            //nplCungUng.MaLoaiHinh = tbNPLDetail.Rows[i]["TenSP"].ToString();
                            nplCungUng.NgayDangKy = Convert.ToDateTime(tbNPLDetail.Rows[i]["NgayXuat"].ToString());
                            nplCungUng.MaSanPham = tbNPLDetail.Rows[i]["MaSP"].ToString();
                            Company.GC.BLL.GC.SanPham spCU = Company.GC.BLL.GC.SanPham.LoadNew(nplCungUng.MaSanPham, HD.ID);
                            if (spCU!=null)
                            {
                                nplCungUng.TenSanPham = spCU.Ten;
                                nplCungUng.DVT_ID = DonViTinh_GetName(spCU.DVT_ID);   
                            }
                            nplCungUng.SoToKhaiXuat = tbNPLDetail.Rows[i]["SoToKhai"].ToString();
                            nplCungUng.SoToKhaiVNACCS = tbNPLDetail.Rows[i]["SoTKVNACCS"].ToString();
                            KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(nplCungUng.SoToKhaiVNACCS);
                            if (TKMD!=null)
                            {
                                nplCungUng.MaLoaiHinh = TKMD.MaLoaiHinh;   
                            }
                            nplCungUng.SoLuong = Convert.ToDecimal(tbNPLDetail.Rows[i]["LuongCungUng"].ToString());
                            nplCungUng.LuongCungUng = Convert.ToDecimal(tbNPLDetail.Rows[i]["LuongCungUng"].ToString());
                            CungUngDK.NPLCungUng_List[id].CungUngDetail_List.Add(nplCungUng);
                        }
                    }
                    //CungUngDK.NPLCungUng_List[id].Add(CungUng);
                    frm.CungUngNPL = CungUngDK.NPLCungUng_List[id];
                    frm.CungUngDK = CungUngDK;
                    frm.ShowDialog();
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                }
            }
            else
            {
                if (e.Row.RowType == RowType.Record)
                {

                    int id = Convert.ToInt32(dgList.CurrentRow.Cells["ID"].Value);
                    CungUngNPLForm frm = new CungUngNPLForm();
                    frm.CungUngNPL = KDT_GC_CungUng.LoadFull(id);
                    frm.CungUngDK = CungUngDK;
                    //frm.HopDong_ID = CungUngDK.HopDong_ID;
                    frm.ShowDialog();
                    //LoadData();
                }
                LoadData();
            }
            
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            //if (e.Row.RowType == RowType.Record)
            //{
            //    KDT_GC_CungUng CU = new KDT_GC_CungUng();
            //    CU = (KDT_GC_CungUng)e.Row.DataRow;
            //    decimal tongluog = 0;
            //    foreach (KDT_GC_CungUng_Detail CU_DeTail in CU.CungUngDetail_List)
            //    {
            //        foreach (KDT_GC_CungUng_ChungTu item in CU_DeTail.CungUngCT_List)
            //        {
            //            tongluog += (System.Decimal)item.SoLuong;
            //        }   
            //    }
            //    e.Row.Cells["LuongCungUng"].Text = tongluog.ToString();
            //}
        }

        private void btn_CapNhatPhanBo_Click(object sender, EventArgs e)
        {
            List<NPLNhapTonThucTe> list_npl = new List<NPLNhapTonThucTe>();

            foreach (KDT_GC_CungUng npl in CungUngDK.NPLCungUng_List)
            {
                NPLNhapTonThucTe tt = new NPLNhapTonThucTe();
                tt.ID_HopDong = CungUngDK.HopDong_ID;
                tt.MaDoanhNghiep = CungUngDK.MaDoanhNghiep;
                tt.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                tt.MaHS = npl.MaHS;
                tt.MaLoaiHinh = "NPLCU";
                tt.MaNPL = npl.MaNPL;
                tt.NamDangKy = Int16.Parse(CungUngDK.NgayTiepNhan.Year.ToString());
                tt.SoToKhai = int.Parse(CungUngDK.ID.ToString());
                tt.TenNPL = npl.MaNPL;
                tt.Luong = npl.LuongCungUng;
                tt.Ton = npl.LuongCungUng;
                tt.TonTriGiaKB = 0;
                tt.TriGiaKB = 0;
                tt.XuatXu = "VN";
                tt.NgayDangKy = CungUngDK.NgayTiepNhan;
                tt.SoToKhaiVNACCS = 0;
                tt.DVT = npl.DVT_ID;
                list_npl.Add(tt);
            }
            try
            {
                if (NPLNhapTonThucTe.InsertUpdateCollection(list_npl))
                {
                    MessageBox.Show("Cập nhật thành công!", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show("Lỗi cập nhật phân bổ", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }


    }
}
