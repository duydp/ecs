using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Company.Interface.SXXK.BangKeThanhLy;
using Janus.Windows.GridEX;
using Company.Interface.Report.SXXK;

namespace Company.Interface.SXXK.BangKe
{
    public partial class BK06Form : BaseForm
    {
        public HoSoThanhLyDangKy HSTL = new HoSoThanhLyDangKy();
        public BKNPLXuatSuDungNKDCollection bkCollection = new BKNPLXuatSuDungNKDCollection();
        private string MaLoaiHinhNhap;
        private string MaLoaiHinhXuat;
        private string MaHaiQuanNhap;
        private string MaHaiQuanXuat;
        private DateTime NgayDangKy;
        private DateTime NgayDangKyXuat;
        private string DVT_ID;
        private decimal SoLuongNPL;
        public BK06Form()
        {
            InitializeComponent();
        }

        private void txtToKhaiNKD_ButtonClick(object sender, EventArgs e)
        {
            txtSoLuongXuat.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            int id = this.HSTL.getBKToKhaiNhap();
            DataSet ds = new BKToKhaiNhap().getBKToKhaiNKD(this.HSTL.BKCollection[id].ID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DSToKhaiNKDForm f = new DSToKhaiNKDForm();
                f.BangKeHSTL_ID = this.HSTL.BKCollection[id].ID;
                f.ds = ds;
                f.ShowDialog();
                if (f.SoToKhai == 0) return;
                txtToKhaiNKD.Text = Convert.ToString(f.SoToKhai);
                txtLoaiHinhNKD.Text = LoaiHinhMauDich_GetTenVT((object)f.MaLoaiHinh);
                txtNamDKNhap.Text = Convert.ToString(f.NgayDangKy.Year);
                txtMaNPL.Text = f.MaNPL;
                txtTenNPL.Text = f.TenNPL;
                this.SoLuongNPL = f.SoLuong;
                this.MaHaiQuanNhap = f.MaHaiQuan;
                this.MaLoaiHinhNhap= f.MaLoaiHinh;
                this.NgayDangKy = f.NgayDangKy;
            }
            else
            {
               // ShowMessage("Không có tờ khai nhập kinh doanh nào trong bảng kê danh sách tờ khai nhập!", false);
                MLMessages("Không có tờ khai nhập kinh doanh nào trong bảng kê danh sách tờ khai nhập!", "MSG_THK79", "", false);
            }
        }

        private void txtToKhaiXuat_ButtonClick(object sender, EventArgs e)
        {
            int id = this.HSTL.getBKToKhaiXuat();
            DataSet ds = new BKToKhaiXuat().getBKToKhaiXuatSP(this.HSTL.BKCollection[id].ID);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DSToKhaiXuatSPForm f = new DSToKhaiXuatSPForm();
                f.BangKeHSTL_ID = this.HSTL.BKCollection[id].ID;
                f.ds = ds;
                f.ShowDialog();
                if (f.SoToKhai == 0) return;
                txtToKhaiXuat.Text = Convert.ToString(f.SoToKhai);
                txtLoaiHinhXuat.Text = LoaiHinhMauDich_GetTenVT((object)f.MaLoaiHinh);
                txtNamDKXuat.Text = Convert.ToString(f.NgayDangKy.Year);
                txtMaSP.Text = f.MaSP;
                txtTenSP.Text = f.TenSP;
                this.MaHaiQuanXuat = f.MaHaiQuan;
                this.MaLoaiHinhXuat = f.MaLoaiHinh;
                this.DVT_ID = f.DVT_ID;
                this.NgayDangKyXuat = f.NgayDangKy;
            }
            else
            {
                //ShowMessage("Không có tờ khai xuất SP nào trong bảng kê danh sách tờ khai xuất!", false);
                MLMessages("Không có tờ khai xuất SP nào trong bảng kê danh sách tờ khai xuất!", "MSG_THK79", "", false);
            }
        }

        private void cmdAdd_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            Company.BLL.SXXK.DinhMuc dm = new Company.BLL.SXXK.DinhMuc();
            dm.MaHaiQuan = this.HSTL.MaHaiQuanTiepNhan;
            dm.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
            dm.MaSanPHam = txtMaSP.Text;
            dm.MaNguyenPhuLieu = txtMaNPL.Text;
            if (dm.Load())
            {
                if (Convert.ToDecimal(txtSoLuongXuat.Value) * dm.DinhMucChung > this.SoLuongNPL)
                {
                    //ShowMessage("Lượng nhập vào phải nhỏ hơn lượng NPL quy đổi trên tờ khai xuất.", false);
                    MLMessages("Lượng nhập vào phải nhỏ hơn lượng NPL quy đổi trên tờ khai xuất.", "MSG_THK81", "", false);
                    return;
                }
            }
            else
            {
 
            }
            BKNPLXuatSuDungNKD bk = new BKNPLXuatSuDungNKD();
            bk.SoToKhai = Convert.ToInt32(txtToKhaiNKD.Text);
            bk.MaLoaiHinh = this.MaLoaiHinhNhap;
            bk.NamDangKy = Convert.ToInt16(txtNamDKNhap.Text);
            bk.MaHaiQuan = this.MaHaiQuanNhap;
            bk.SoToKhaiXuat = Convert.ToInt32(txtToKhaiXuat.Text);
            bk.MaLoaiHinhXuat = this.MaLoaiHinhXuat;
            bk.NamDangKyXuat = Convert.ToInt16(txtNamDKXuat.Text);
            bk.MaHaiQuanXuat = this.MaHaiQuanXuat;
            bk.MaNPL = txtMaNPL.Text;
            bk.MaSP = txtMaSP.Text;
            bk.NgayDangKy = this.NgayDangKy;
            bk.NgayDangKyXuat = this.NgayDangKyXuat;
            bk.LuongSuDung = Convert.ToDecimal(txtSoLuongXuat.Value);
            this.bkCollection.Add(bk);
            gridEX2.Refetch();
            this.ClearData();
        }
        private void ClearData()
        {
            txtToKhaiNKD.Text = "";
            txtLoaiHinhNKD.Text = "";
            txtNamDKNhap.Text = "";
            txtMaNPL.Text = "";
            txtTenNPL.Text = "";
            txtMaSP.Text = "";
            txtTenSP.Text = "";
            txtToKhaiXuat.Text = "";
            txtLoaiHinhXuat.Text = "";
            txtNamDKXuat.Text = "";
            txtSoLuongXuat.Value = 0;
        }

        private void BK06Form_Load(object sender, EventArgs e)
        {
            gridEX2.Tables[0].Columns["LuongSuDung"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            txtSoLuongXuat.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            if (this.HSTL.TrangThaiThanhKhoan == 401)
            {
                cmdAdd.Enabled = cmdSave.Enabled = false;
                gridEX2.AllowDelete = InheritableBoolean.False;
                gridEX2.AllowEdit = InheritableBoolean.False;
            }
            gridEX2.DataSource = this.bkCollection;
        }

        private void cmdSave_Click(object sender, EventArgs e)
        {
            if (this.bkCollection.Count == 0)
            {
                ShowMessage("Bạn chưa nhập NPL nhập theo loại hình nhập kinh doanh.", false);
                return;
            }
            try
            {
                this.Cursor = Cursors.WaitCursor;
                if (this.HSTL.getBKNPLNhapKinhDoanh() < 0)
                {
                    BangKeHoSoThanhLy bk = new BangKeHoSoThanhLy();
                    bk.MaterID = this.HSTL.ID;
                    bk.STTHang = this.HSTL.BKCollection.Count + 1;
                    bk.MaBangKe = "DTLNPLNKD";
                    bk.TenBangKe = "DTLNPLNKD";
                    bk.bkNPLNKDCollection = this.bkCollection;
                    this.HSTL.BKCollection.Add(bk);
                }
                else
                {
                    this.HSTL.BKCollection[this.HSTL.getBKNPLNhapKinhDoanh()].bkNPLNKDCollection = this.bkCollection;
                }
                this.HSTL.BKCollection[this.HSTL.getBKNPLNhapKinhDoanh()].InsertUpdate_BKNPLNKD(this.bkCollection);
                this.Close();
            }
            catch (Exception ex)
            {
                ShowMessage(" " + ex.Message, false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void gridEX2_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                e.Row.Cells["SoToKhaiXuat"].Text = e.Row.Cells["SoToKhaiXuat"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinhXuat"].Value) + "/" + e.Row.Cells["NamDangKyXuat"].Text;

            }

        }

        private void lblPrint_Click(object sender, EventArgs e)
        {
            BK06NhapKinhDoanhReport report = new BK06NhapKinhDoanhReport();
            report.HSTL = this.HSTL;
            report.BindReport();
            report.ShowPreviewDialog();
        }


    }
}