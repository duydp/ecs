﻿namespace Company.Interface
{
    partial class CoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CoForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkNoCo = new Janus.Windows.EditControls.UICheckBox();
            this.lblNgayNopCo = new System.Windows.Forms.Label();
            this.ccNgayNopCO = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtNguoiKy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cbLoaiCO = new Janus.Windows.EditControls.UIComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtThongTinMoTa = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtDiaChiNguoiNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ctrMaNuocNK = new Company.Interface.Controls.NuocHControl();
            this.label3 = new System.Windows.Forms.Label();
            this.ctrMaNuocXK = new Company.Interface.Controls.NuocHControl();
            this.label2 = new System.Windows.Forms.Label();
            this.ctrNuocCapCO = new Company.Interface.Controls.NuocHControl();
            this.txtToChucCap = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtDiaChiNguoiXK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSoCO = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.ccNgayCO = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnGhi = new Janus.Windows.EditControls.UIButton();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.rfvNgayCO = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoCO = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDiaChiXK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenDiaChiNK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvThongTinMoTa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNguoiKyCO = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnChonGP = new Janus.Windows.EditControls.UIButton();
            this.grpTiepNhan = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayTiepNhan = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnKetQuaXyLy = new Janus.Windows.EditControls.UIButton();
            this.btnKhaiBao = new Janus.Windows.EditControls.UIButton();
            this.btnLayPhanHoi = new Janus.Windows.EditControls.UIButton();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayCO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoCO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaChiXK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaChiNK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvThongTinMoTa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiKyCO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).BeginInit();
            this.grpTiepNhan.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.btnKhaiBao);
            this.grbMain.Controls.Add(this.btnLayPhanHoi);
            this.grbMain.Controls.Add(this.grpTiepNhan);
            this.grbMain.Controls.Add(this.btnChonGP);
            this.grbMain.Controls.Add(this.btnXoa);
            this.grbMain.Controls.Add(this.btnGhi);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Size = new System.Drawing.Size(570, 484);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.chkNoCo);
            this.uiGroupBox2.Controls.Add(this.lblNgayNopCo);
            this.uiGroupBox2.Controls.Add(this.ccNgayNopCO);
            this.uiGroupBox2.Controls.Add(this.txtNguoiKy);
            this.uiGroupBox2.Controls.Add(this.label7);
            this.uiGroupBox2.Controls.Add(this.cbLoaiCO);
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtThongTinMoTa);
            this.uiGroupBox2.Controls.Add(this.label5);
            this.uiGroupBox2.Controls.Add(this.txtDiaChiNguoiNK);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.ctrMaNuocNK);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Controls.Add(this.ctrMaNuocXK);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.ctrNuocCapCO);
            this.uiGroupBox2.Controls.Add(this.txtToChucCap);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label10);
            this.uiGroupBox2.Controls.Add(this.txtDiaChiNguoiXK);
            this.uiGroupBox2.Controls.Add(this.label8);
            this.uiGroupBox2.Controls.Add(this.txtSoCO);
            this.uiGroupBox2.Controls.Add(this.label14);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.ccNgayCO);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(8, 82);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(554, 334);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.Text = "Thông tin chung";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // chkNoCo
            // 
            this.chkNoCo.Location = new System.Drawing.Point(170, 307);
            this.chkNoCo.Name = "chkNoCo";
            this.chkNoCo.Size = new System.Drawing.Size(104, 23);
            this.chkNoCo.TabIndex = 22;
            this.chkNoCo.Text = "Nợ CO";
            this.chkNoCo.CheckedChanged += new System.EventHandler(this.chkNoCo_CheckedChanged);
            // 
            // lblNgayNopCo
            // 
            this.lblNgayNopCo.AutoSize = true;
            this.lblNgayNopCo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayNopCo.Location = new System.Drawing.Point(280, 314);
            this.lblNgayNopCo.Name = "lblNgayNopCo";
            this.lblNgayNopCo.Size = new System.Drawing.Size(69, 13);
            this.lblNgayNopCo.TabIndex = 23;
            this.lblNgayNopCo.Text = "Thời hạn nộp";
            // 
            // ccNgayNopCO
            // 
            this.ccNgayNopCO.BackColor = System.Drawing.SystemColors.Info;
            // 
            // 
            // 
            this.ccNgayNopCO.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayNopCO.DropDownCalendar.Name = "";
            this.ccNgayNopCO.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayNopCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayNopCO.IsNullDate = true;
            this.ccNgayNopCO.Location = new System.Drawing.Point(358, 307);
            this.ccNgayNopCO.Name = "ccNgayNopCO";
            this.ccNgayNopCO.Nullable = true;
            this.ccNgayNopCO.NullButtonText = "Xóa";
            this.ccNgayNopCO.ShowNullButton = true;
            this.ccNgayNopCO.Size = new System.Drawing.Size(178, 21);
            this.ccNgayNopCO.TabIndex = 24;
            this.ccNgayNopCO.TodayButtonText = "Hôm nay";
            this.ccNgayNopCO.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayNopCO.VisualStyleManager = this.vsmMain;
            // 
            // txtNguoiKy
            // 
            this.txtNguoiKy.BackColor = System.Drawing.SystemColors.Info;
            this.txtNguoiKy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiKy.Location = new System.Drawing.Point(170, 279);
            this.txtNguoiKy.MaxLength = 255;
            this.txtNguoiKy.Name = "txtNguoiKy";
            this.txtNguoiKy.Size = new System.Drawing.Size(366, 21);
            this.txtNguoiKy.TabIndex = 21;
            this.txtNguoiKy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNguoiKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtNguoiKy.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(22, 284);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 13);
            this.label7.TabIndex = 20;
            this.label7.Text = "Người ký CO";
            // 
            // cbLoaiCO
            // 
            this.cbLoaiCO.BackColor = System.Drawing.SystemColors.Info;
            this.cbLoaiCO.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbLoaiCO.DisplayMember = "Name";
            this.cbLoaiCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbLoaiCO.Location = new System.Drawing.Point(358, 44);
            this.cbLoaiCO.Name = "cbLoaiCO";
            this.cbLoaiCO.Size = new System.Drawing.Size(178, 21);
            this.cbLoaiCO.TabIndex = 7;
            this.cbLoaiCO.ValueMember = "ID";
            this.cbLoaiCO.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbLoaiCO.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(302, 49);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Loại CO";
            // 
            // txtThongTinMoTa
            // 
            this.txtThongTinMoTa.BackColor = System.Drawing.SystemColors.Info;
            this.txtThongTinMoTa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThongTinMoTa.Location = new System.Drawing.Point(170, 237);
            this.txtThongTinMoTa.MaxLength = 255;
            this.txtThongTinMoTa.Multiline = true;
            this.txtThongTinMoTa.Name = "txtThongTinMoTa";
            this.txtThongTinMoTa.Size = new System.Drawing.Size(366, 36);
            this.txtThongTinMoTa.TabIndex = 19;
            this.txtThongTinMoTa.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtThongTinMoTa.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtThongTinMoTa.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 244);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(142, 24);
            this.label5.TabIndex = 18;
            this.label5.Text = "Thông tin mô tả chi tiết\r\n hàng hóa";
            // 
            // txtDiaChiNguoiNK
            // 
            this.txtDiaChiNguoiNK.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtDiaChiNguoiNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiNguoiNK.Location = new System.Drawing.Point(170, 197);
            this.txtDiaChiNguoiNK.MaxLength = 255;
            this.txtDiaChiNguoiNK.Multiline = true;
            this.txtDiaChiNguoiNK.Name = "txtDiaChiNguoiNK";
            this.txtDiaChiNguoiNK.Size = new System.Drawing.Size(366, 36);
            this.txtDiaChiNguoiNK.TabIndex = 17;
            this.txtDiaChiNguoiNK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiNguoiNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDiaChiNguoiNK.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(22, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 30);
            this.label4.TabIndex = 16;
            this.label4.Text = "Tên địa chỉ người nhập khẩu\r\ntrên CO";
            // 
            // ctrMaNuocNK
            // 
            this.ctrMaNuocNK.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrMaNuocNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrMaNuocNK.Location = new System.Drawing.Point(169, 127);
            this.ctrMaNuocNK.Ma = "";
            this.ctrMaNuocNK.Name = "ctrMaNuocNK";
            this.ctrMaNuocNK.ReadOnly = false;
            this.ctrMaNuocNK.Size = new System.Drawing.Size(381, 22);
            this.ctrMaNuocNK.TabIndex = 13;
            this.ctrMaNuocNK.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Mã nước nhập khẩu trên CO";
            // 
            // ctrMaNuocXK
            // 
            this.ctrMaNuocXK.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrMaNuocXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrMaNuocXK.Location = new System.Drawing.Point(169, 99);
            this.ctrMaNuocXK.Ma = "";
            this.ctrMaNuocXK.Name = "ctrMaNuocXK";
            this.ctrMaNuocXK.ReadOnly = false;
            this.ctrMaNuocXK.Size = new System.Drawing.Size(381, 22);
            this.ctrMaNuocXK.TabIndex = 11;
            this.ctrMaNuocXK.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(140, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Mã nước xuất khẩu trên CO";
            // 
            // ctrNuocCapCO
            // 
            this.ctrNuocCapCO.ErrorMessage = "\"Nước\" không được bỏ trống.";
            this.ctrNuocCapCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocCapCO.Location = new System.Drawing.Point(169, 71);
            this.ctrNuocCapCO.Ma = "";
            this.ctrNuocCapCO.Name = "ctrNuocCapCO";
            this.ctrNuocCapCO.ReadOnly = false;
            this.ctrNuocCapCO.Size = new System.Drawing.Size(381, 22);
            this.ctrNuocCapCO.TabIndex = 9;
            this.ctrNuocCapCO.VisualStyleManager = this.vsmMain;
            // 
            // txtToChucCap
            // 
            this.txtToChucCap.BackColor = System.Drawing.SystemColors.Control;
            this.txtToChucCap.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToChucCap.Location = new System.Drawing.Point(169, 44);
            this.txtToChucCap.MaxLength = 255;
            this.txtToChucCap.Name = "txtToChucCap";
            this.txtToChucCap.Size = new System.Drawing.Size(107, 21);
            this.txtToChucCap.TabIndex = 5;
            this.txtToChucCap.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtToChucCap.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtToChucCap.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(22, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Tổ chức cấp";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(22, 74);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(70, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Nước cấp CO";
            // 
            // txtDiaChiNguoiXK
            // 
            this.txtDiaChiNguoiXK.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtDiaChiNguoiXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaChiNguoiXK.Location = new System.Drawing.Point(170, 155);
            this.txtDiaChiNguoiXK.MaxLength = 255;
            this.txtDiaChiNguoiXK.Multiline = true;
            this.txtDiaChiNguoiXK.Name = "txtDiaChiNguoiXK";
            this.txtDiaChiNguoiXK.Size = new System.Drawing.Size(366, 36);
            this.txtDiaChiNguoiXK.TabIndex = 15;
            this.txtDiaChiNguoiXK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaChiNguoiXK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDiaChiNguoiXK.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(22, 162);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(140, 24);
            this.label8.TabIndex = 14;
            this.label8.Text = "Tên địa chỉ người xuất khẩu\r\n trên CO";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // txtSoCO
            // 
            this.txtSoCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoCO.Location = new System.Drawing.Point(169, 17);
            this.txtSoCO.MaxLength = 255;
            this.txtSoCO.Name = "txtSoCO";
            this.txtSoCO.Size = new System.Drawing.Size(106, 21);
            this.txtSoCO.TabIndex = 1;
            this.txtSoCO.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoCO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoCO.VisualStyleManager = this.vsmMain;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(302, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Ngày CO";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(22, 22);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(37, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Số CO";
            // 
            // ccNgayCO
            // 
            // 
            // 
            // 
            this.ccNgayCO.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayCO.DropDownCalendar.Name = "";
            this.ccNgayCO.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayCO.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayCO.IsNullDate = true;
            this.ccNgayCO.Location = new System.Drawing.Point(358, 17);
            this.ccNgayCO.Name = "ccNgayCO";
            this.ccNgayCO.Nullable = true;
            this.ccNgayCO.NullButtonText = "Xóa";
            this.ccNgayCO.ShowNullButton = true;
            this.ccNgayCO.Size = new System.Drawing.Size(178, 21);
            this.ccNgayCO.TabIndex = 3;
            this.ccNgayCO.TodayButtonText = "Hôm nay";
            this.ccNgayCO.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayCO.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(490, 422);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 2;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnGhi
            // 
            this.btnGhi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGhi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnGhi.Icon")));
            this.btnGhi.Location = new System.Drawing.Point(313, 422);
            this.btnGhi.Name = "btnGhi";
            this.btnGhi.Size = new System.Drawing.Size(77, 23);
            this.btnGhi.TabIndex = 1;
            this.btnGhi.Text = "Lưu";
            this.btnGhi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnGhi.VisualStyleManager = this.vsmMain;
            this.btnGhi.Click += new System.EventHandler(this.btnGhi_Click);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvNgayCO
            // 
            this.rfvNgayCO.ControlToValidate = this.ccNgayCO;
            this.rfvNgayCO.ErrorMessage = "\"Ngày CO\" không được bỏ trống.";
            this.rfvNgayCO.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayCO.Icon")));
            this.rfvNgayCO.Tag = "rfvNgayCO";
            // 
            // rfvSoCO
            // 
            this.rfvSoCO.ControlToValidate = this.txtSoCO;
            this.rfvSoCO.ErrorMessage = "\"Số CO\" không được để trống.";
            this.rfvSoCO.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoCO.Icon")));
            this.rfvSoCO.Tag = "rfvSoCO";
            // 
            // rfvTenDiaChiXK
            // 
            this.rfvTenDiaChiXK.ControlToValidate = this.txtDiaChiNguoiXK;
            this.rfvTenDiaChiXK.ErrorMessage = "\"Tên địa chỉ người xuất khẩu trên CO\" không được để trống.";
            this.rfvTenDiaChiXK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDiaChiXK.Icon")));
            this.rfvTenDiaChiXK.Tag = "rfvTenDiaChiXK";
            // 
            // rfvTenDiaChiNK
            // 
            this.rfvTenDiaChiNK.ControlToValidate = this.txtDiaChiNguoiNK;
            this.rfvTenDiaChiNK.ErrorMessage = "\"Tên địa chỉ người nhập khẩu trên CO\" không được để trống.";
            this.rfvTenDiaChiNK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenDiaChiNK.Icon")));
            this.rfvTenDiaChiNK.Tag = "rfvTenDiaChiNK";
            // 
            // rfvThongTinMoTa
            // 
            this.rfvThongTinMoTa.ControlToValidate = this.txtThongTinMoTa;
            this.rfvThongTinMoTa.ErrorMessage = "\"Thông tin mô tả chi tiết hàng hóa\" không được để trống.";
            this.rfvThongTinMoTa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvThongTinMoTa.Icon")));
            this.rfvThongTinMoTa.Tag = "rfvTenDiaChiNK";
            // 
            // rfvNguoiKyCO
            // 
            this.rfvNguoiKyCO.ControlToValidate = this.txtNguoiKy;
            this.rfvNguoiKyCO.ErrorMessage = "\"Người ký\" không được bỏ trống.";
            this.rfvNguoiKyCO.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiKyCO.Icon")));
            this.rfvNguoiKyCO.Tag = "rfvNguoiKyCO";
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Icon = ((System.Drawing.Icon)(resources.GetObject("btnXoa.Icon")));
            this.btnXoa.Location = new System.Drawing.Point(396, 422);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(88, 23);
            this.btnXoa.TabIndex = 3;
            this.btnXoa.Text = "Xoá CO";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnChonGP
            // 
            this.btnChonGP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnChonGP.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChonGP.Icon = ((System.Drawing.Icon)(resources.GetObject("btnChonGP.Icon")));
            this.btnChonGP.Location = new System.Drawing.Point(313, 451);
            this.btnChonGP.Name = "btnChonGP";
            this.btnChonGP.Size = new System.Drawing.Size(252, 23);
            this.btnChonGP.TabIndex = 4;
            this.btnChonGP.Text = "Chọn CO đã có";
            this.btnChonGP.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnChonGP.VisualStyleManager = this.vsmMain;
            this.btnChonGP.Click += new System.EventHandler(this.btnChonCO_Click);
            // 
            // grpTiepNhan
            // 
            this.grpTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.grpTiepNhan.Controls.Add(this.ccNgayTiepNhan);
            this.grpTiepNhan.Controls.Add(this.txtSoTiepNhan);
            this.grpTiepNhan.Controls.Add(this.label9);
            this.grpTiepNhan.Controls.Add(this.label11);
            this.grpTiepNhan.Controls.Add(this.label12);
            this.grpTiepNhan.Controls.Add(this.btnKetQuaXyLy);
            this.grpTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grpTiepNhan.Location = new System.Drawing.Point(8, 8);
            this.grpTiepNhan.Name = "grpTiepNhan";
            this.grpTiepNhan.Size = new System.Drawing.Size(554, 68);
            this.grpTiepNhan.TabIndex = 24;
            this.grpTiepNhan.Text = "Thông tin khai báo";
            this.grpTiepNhan.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grpTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayTiepNhan
            // 
            // 
            // 
            // 
            this.ccNgayTiepNhan.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTiepNhan.DropDownCalendar.Name = "";
            this.ccNgayTiepNhan.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTiepNhan.IsNullDate = true;
            this.ccNgayTiepNhan.Location = new System.Drawing.Point(407, 15);
            this.ccNgayTiepNhan.Name = "ccNgayTiepNhan";
            this.ccNgayTiepNhan.Nullable = true;
            this.ccNgayTiepNhan.NullButtonText = "Xóa";
            this.ccNgayTiepNhan.ReadOnly = true;
            this.ccNgayTiepNhan.ShowNullButton = true;
            this.ccNgayTiepNhan.Size = new System.Drawing.Size(141, 21);
            this.ccNgayTiepNhan.TabIndex = 22;
            this.ccNgayTiepNhan.TodayButtonText = "Hôm nay";
            this.ccNgayTiepNhan.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccNgayTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(121, 15);
            this.txtSoTiepNhan.MaxLength = 255;
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(142, 21);
            this.txtSoTiepNhan.TabIndex = 1;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(21, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 13);
            this.label9.TabIndex = 2;
            this.label9.Text = "Trạng thái";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(305, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Ngày tiếp nhận";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(20, 23);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(67, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Số tiếp nhận";
            // 
            // btnKetQuaXyLy
            // 
            this.btnKetQuaXyLy.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnKetQuaXyLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKetQuaXyLy.ImageIndex = 2;
            this.btnKetQuaXyLy.ImageList = this.ImageList1;
            this.btnKetQuaXyLy.Location = new System.Drawing.Point(407, 39);
            this.btnKetQuaXyLy.Name = "btnKetQuaXyLy";
            this.btnKetQuaXyLy.Size = new System.Drawing.Size(141, 23);
            this.btnKetQuaXyLy.TabIndex = 3;
            this.btnKetQuaXyLy.Text = "Kết quả xử lý";
            this.btnKetQuaXyLy.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKetQuaXyLy.Click += new System.EventHandler(this.btnKetQuaXyLy_Click);
            // 
            // btnKhaiBao
            // 
            this.btnKhaiBao.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnKhaiBao.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnKhaiBao.Icon = ((System.Drawing.Icon)(resources.GetObject("btnKhaiBao.Icon")));
            this.btnKhaiBao.Location = new System.Drawing.Point(132, 422);
            this.btnKhaiBao.Name = "btnKhaiBao";
            this.btnKhaiBao.Size = new System.Drawing.Size(88, 23);
            this.btnKhaiBao.TabIndex = 26;
            this.btnKhaiBao.Text = "Khai  báo";
            this.btnKhaiBao.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnKhaiBao.Click += new System.EventHandler(this.btnKhaiBao_Click);
            // 
            // btnLayPhanHoi
            // 
            this.btnLayPhanHoi.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLayPhanHoi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLayPhanHoi.Icon = ((System.Drawing.Icon)(resources.GetObject("btnLayPhanHoi.Icon")));
            this.btnLayPhanHoi.Location = new System.Drawing.Point(11, 422);
            this.btnLayPhanHoi.Name = "btnLayPhanHoi";
            this.btnLayPhanHoi.Size = new System.Drawing.Size(109, 23);
            this.btnLayPhanHoi.TabIndex = 25;
            this.btnLayPhanHoi.Text = "Lấy phản hồi";
            this.btnLayPhanHoi.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnLayPhanHoi.Click += new System.EventHandler(this.btnLayPhanHoi_Click);
            // 
            // CoForm
            // 
            this.AcceptButton = this.btnGhi;
            this.ClientSize = new System.Drawing.Size(570, 484);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "CoForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Thông tin CO";
            this.Load += new System.EventHandler(this.CoForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayCO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoCO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaChiXK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenDiaChiNK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvThongTinMoTa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiKyCO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grpTiepNhan)).EndInit();
            this.grpTiepNhan.ResumeLayout(false);
            this.grpTiepNhan.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoCO;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label14;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiXK;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIButton btnGhi;
        private Janus.Windows.EditControls.UIButton btnClose;
        private System.Windows.Forms.Label label10;
        private Janus.Windows.GridEX.EditControls.EditBox txtToChucCap;
        private System.Windows.Forms.Label label1;
        private Company.Interface.Controls.NuocHControl ctrMaNuocNK;
        private System.Windows.Forms.Label label3;
        private Company.Interface.Controls.NuocHControl ctrMaNuocXK;
        private System.Windows.Forms.Label label2;
        private Company.Interface.Controls.NuocHControl ctrNuocCapCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtThongTinMoTa;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaChiNguoiNK;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.EditControls.UIComboBox cbLoaiCO;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ErrorProvider epError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNgayCO;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoCO;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDiaChiXK;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenDiaChiNK;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvThongTinMoTa;
        private System.Windows.Forms.ImageList ImageList1;
        private System.Windows.Forms.Label lblNgayNopCo;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayNopCO;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiKy;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.EditControls.UICheckBox chkNoCo;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNguoiKyCO;
        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnChonGP;
        private Janus.Windows.EditControls.UIGroupBox grpTiepNhan;
        private Janus.Windows.CalendarCombo.CalendarCombo ccNgayTiepNhan;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private Janus.Windows.EditControls.UIButton btnKhaiBao;
        private Janus.Windows.EditControls.UIButton btnLayPhanHoi;
        private Janus.Windows.EditControls.UIButton btnKetQuaXyLy;
    }
}
