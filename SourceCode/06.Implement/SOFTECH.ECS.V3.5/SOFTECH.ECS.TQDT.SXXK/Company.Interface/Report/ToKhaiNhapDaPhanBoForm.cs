using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.SXXK.ThanhKhoan;
using Company.BLL.SXXK.ToKhai;

namespace Company.Interface.Report
{
    public partial class ToKhaiNhapDaPhanBoForm : BaseForm
    {
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public ToKhaiNhapDaPhanBoForm()
        {
            InitializeComponent();
        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaLoaiHinh"].Text = LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value);
            }
        }

        private void dgList1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                this.TKMD.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
                this.TKMD.MaLoaiHinh = Convert.ToString(e.Row.Cells["MaLoaiHinh"].Value);
                this.TKMD.NamDangKy = Convert.ToInt16(e.Row.Cells["NamDangKy"].Value);
                this.TKMD.MaHaiQuan = Convert.ToString(e.Row.Cells["MaHaiQuan"].Value);
                this.TKMD.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                this.Close();
            }
        }

        private void ToKhaiNhapDaPhanBoForm_Load(object sender, EventArgs e)
        {
            ccFromDate.Value = DateTime.Today.AddYears(-1);
            search();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            search();
        }

        private void search()
        {
            DataTable dt = new BCXuatNhapTon().GetToKhaiNhapDaPhanBo(ccFromDate.Value, ccToDate.Value);
            dgList1.DataSource = dt;
            dgList1.Refetch();
        }
    }
}