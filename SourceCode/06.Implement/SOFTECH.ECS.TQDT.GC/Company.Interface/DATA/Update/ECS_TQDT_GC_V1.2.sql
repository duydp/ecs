/*
Run this script on:

        ECSTEAM.ECS_TQDT_GC_VERSION    -  This database will be modified

to synchronize it with:

        ECSTEAM.ECS_TQDT_GC

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 10/18/2011 2:48:15 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Dropping constraints from [dbo].[t_KDT_Messages]'
GO
ALTER TABLE [dbo].[t_KDT_Messages] DROP CONSTRAINT [PK_t_KDT_ThongDiep]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Rebuilding [dbo].[t_KDT_Messages]'
GO
CREATE TABLE [dbo].[tmp_rg_xx_t_KDT_Messages]
(
[ID] [int] NOT NULL IDENTITY(1, 1),
[ReferenceID] [uniqueidentifier] NULL,
[ItemID] [bigint] NULL,
[MessageFrom] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageTo] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[MessageType] [int] NULL,
[MessageFunction] [int] NULL,
[MessageContent] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CreatedTime] [datetime] NULL,
[TieuDeThongBao] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[NoiDungThongBao] [nvarchar] (max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_t_KDT_Messages] ON
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
INSERT INTO [dbo].[tmp_rg_xx_t_KDT_Messages]([ID], [ReferenceID], [ItemID], [MessageFrom], [MessageTo], [MessageType], [MessageFunction], [MessageContent], [CreatedTime], [TieuDeThongBao], [NoiDungThongBao]) SELECT [ID], [ReferenceID], [ItemID], [MessageFrom], [MessageTo], [MessageType], [MessageFunction], CAST([MessageContent] AS [nvarchar] (max)) COLLATE SQL_Latin1_General_CP1_CI_AS, [CreatedTime], [TieuDeThongBao], [NoiDungThongBao] FROM [dbo].[t_KDT_Messages]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
SET IDENTITY_INSERT [dbo].[tmp_rg_xx_t_KDT_Messages] OFF
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DECLARE @idVal INT
SELECT @idVal = IDENT_CURRENT(N't_KDT_Messages')
DBCC CHECKIDENT([tmp_rg_xx_t_KDT_Messages], RESEED, @idVal)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
DROP TABLE [dbo].[t_KDT_Messages]
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
EXEC sp_rename N'[dbo].[tmp_rg_xx_t_KDT_Messages]', N't_KDT_Messages'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_t_KDT_ThongDiep] on [dbo].[t_KDT_Messages]'
GO
ALTER TABLE [dbo].[t_KDT_Messages] ADD CONSTRAINT [PK_t_KDT_ThongDiep] PRIMARY KEY CLUSTERED  ([ID])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [dbo].[p_KDT_Message_Insert]'
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_Message_Insert]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Monday, May 24, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_Message_Insert]
	@ReferenceID uniqueidentifier,
	@ItemID bigint,
	@MessageFrom varchar(50),
	@MessageTo varchar(50),
	@MessageType int,
	@MessageFunction int,
	@MessageContent NVARCHAR(max),
	@CreatedTime datetime,
	@TieuDeThongBao nvarchar(100),
	@NoiDungThongBao nvarchar(max),
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_KDT_Messages]
(
	[ReferenceID],
	[ItemID],
	[MessageFrom],
	[MessageTo],
	[MessageType],
	[MessageFunction],
	[MessageContent],
	[CreatedTime],
	[TieuDeThongBao],
	[NoiDungThongBao]
)
VALUES 
(
	@ReferenceID,
	@ItemID,
	@MessageFrom,
	@MessageTo,
	@MessageType,
	@MessageFunction,
	@MessageContent,
	@CreatedTime,
	@TieuDeThongBao,
	@NoiDungThongBao
)

SET @ID = SCOPE_IDENTITY()


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

UPDATE dbo.t_HaiQuan_Version SET [Version] = '1.2', [Date] = GETDATE()