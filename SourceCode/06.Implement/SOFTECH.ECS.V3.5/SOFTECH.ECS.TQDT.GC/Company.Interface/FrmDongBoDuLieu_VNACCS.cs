﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.Components;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.VNACCS;
using Helper.Controls;
using Company.Interface.DongBoDuLieu_New;
using Company.BLL.VNACCS;
using Company.GC.BLL.KDT;
using Company.GC.BLL.VNACC;

namespace Company.Interface
{
    public partial class FrmDongBoDuLieu_VNACCS : BaseForm
    {
        private ToKhaiMauDichCollection tkmdCollection = new ToKhaiMauDichCollection();

        private List<KDT_VNACC_ToKhaiMauDich> listTKVANCCS = new List<KDT_VNACC_ToKhaiMauDich>();

        private List<DongBoDuLieu_TrackVNACCS> ListTrack = new List<DongBoDuLieu_TrackVNACCS>();
        private List<DongBoDuLieu_TrackVNACCS> ListTrack_DB = new List<DongBoDuLieu_TrackVNACCS>();
        private List<KDT_VNACC_ToKhaiMauDich> listTKVANCCS_DB = new List<KDT_VNACC_ToKhaiMauDich>();
        private string nhomLoaiHinh = "";
        string maDoanhNghiep = "", passwordLogin = "", userLogin = "";
        public wssyndata.Service myService = new wssyndata.Service();
        private bool isAdmin = false;
        private string idDaily = "";
        string where = "";
        public FrmDongBoDuLieu_VNACCS()
        {
            InitializeComponent();
            try
            {
                this.Cursor = Cursors.WaitCursor;
                myService.Url = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("WS_SyncData");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
            finally { this.Cursor = Cursors.Default; }
        }

        private void FrmDongBoDuLieu_Load(object sender, EventArgs e)
        {
            try
            {
               
                    clcDenNgay.Value = DateTime.Today;
                    clcTuNgay.Value = clcDenNgay.Value.AddDays(-30);
                    //this.search();
              

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (LoginDB())
            {
                this.search();
            }
            else
            {
                ShowMessage("Đăng nhập không thành công", false);
                LoginDB();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Đồng bộ dữ liệu.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnTransfer_Click(object sender, EventArgs e)
        {
            try
            {
                lblPhanTram.Text = "0%";
                prbTienTrinh.Value = 0;
                txtError.Text = "";
                Janus.Windows.GridEX.GridEXRow[] listChecked = grList.GetCheckedRows();
                if (listChecked.Length > 0)
                {
                    

                    if (!isAdmin)
                    {
                        listTKVANCCS_DB.Clear();
                        
                        for (int i = 0; i < listChecked.Length; i++)
                        {
                            KDT_VNACC_ToKhaiMauDich tkmd = new KDT_VNACC_ToKhaiMauDich();
                            Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                            tkmd = KDT_VNACC_ToKhaiMauDich.LoadBySoTK(item.Cells["SoToKhai"].Value.ToString());
                            listTKVANCCS_DB.Add(tkmd);
                        }
                        if (checkTKChuaTQ.Checked)
                            System.Threading.ThreadPool.QueueUserWorkItem(DoSendCTQ);
                        else
                            System.Threading.ThreadPool.QueueUserWorkItem(DoSend);
                    }
                    else
                    {
                        ListTrack_DB.Clear();
                        
                        for (int i = 0; i < listChecked.Length; i++)
                        {
                            DongBoDuLieu_TrackVNACCS trackVnaccs = new DongBoDuLieu_TrackVNACCS();
                            Janus.Windows.GridEX.GridEXRow item = listChecked[i];
                            trackVnaccs.SoToKhai = item.Cells["SoToKhai"].Value.ToString();
                            trackVnaccs.NgayDangKy = Convert.ToDateTime(item.Cells["Ngaydangky"].Value);
                            ListTrack_DB.Add(trackVnaccs);

                        }
                        if (checkTKChuaTQ.Checked)

                            System.Threading.ThreadPool.QueueUserWorkItem(DoReceiveCTQ);
                        else
                            System.Threading.ThreadPool.QueueUserWorkItem(DoReceive);
                    }
                                     
                }
                else
                {
                    ShowMessage("Chọn tờ khai để đồng bộ ", false);
                }
               
            }
            catch (Exception ex)
            {
                Globals.ShowMessageTQDT("Đồng bộ dữ liệu...", "Lỗi trong quá trình thực hiện:\r\n" + ex.Message + "\nChi tiết:" + ex.StackTrace, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        /// <summary>
        /// Tìm kiếm dữ liệu.
        /// </summary>
        private void search()
        {
            try
            {
               

                    // Xây dựng điều kiện tìm kiếm.
                    string tungay = clcTuNgay.Value.ToString("yyyy/MM/dd");
                    string denngay = clcDenNgay.Value.ToString("yyyy/MM/dd");
                    if (checkTKChuaTQ.Checked)
                        where = " Trangthaixuly = 2 ";
                    else
                        where = " Trangthaixuly = 3 ";
                    where += string.Format(" AND  MaDonVi = '{0}'", GlobalSettings.MA_DON_VI);
                    where += string.Format(" AND (NgayDangKy BETWEEN '{0}' AND '{1}') ", tungay, denngay);
                    // Thực hiện tìm kiếm.       


                    if (!isAdmin)
                    {

                        List<KDT_VNACC_ToKhaiMauDich> lisTK = new List<KDT_VNACC_ToKhaiMauDich>();
                        List<KDT_VNACC_ToKhaiMauDich> lisTKSync = new List<KDT_VNACC_ToKhaiMauDich>();
                        listTKVANCCS = KDT_VNACC_ToKhaiMauDich.SelectCollectionDynamic(where, "");
                        List<string> listTKTrack = new List<string>();
                        listTKTrack.AddRange(myService.SelectSoToKhai(userLogin, passwordLogin, tungay, denngay, GlobalSettings.MA_DON_VI, idDaily));
                        foreach (KDT_VNACC_ToKhaiMauDich item in listTKVANCCS)
                        {
                            if (!listTKTrack.Contains(item.SoToKhai.ToString()))
                                lisTK.Add(item);
                            if (listTKTrack.Contains(item.SoToKhai.ToString()))
                                lisTKSync.Add(item);

                        }
                        if (checkTKSync.Checked)
                            grList.DataSource = lisTKSync;
                        else
                            grList.DataSource = lisTK;

                        grList.Refetch();
                    }
                    else
                    {
                        //FrmDailyDongBo frm = new FrmDailyDongBo();
                        //frm.listDaiLy.AddRange(myService.SelectUserDaiLy(GlobalSettings.MA_DON_VI));
                        //frm.ShowDialog();
                        //idDaily += frm.userDaily;
                        List<string> listTKVNACC = KDT_VNACC_ToKhaiMauDich.SelectListSoToKhai(where);
                        List<Company.Interface.wssyndata.DongBoDuLieu_TrackVNACCS> listTKTrack = new List<Company.Interface.wssyndata.DongBoDuLieu_TrackVNACCS>();
                        List<Company.Interface.wssyndata.DongBoDuLieu_TrackVNACCS> listTK = new List<Company.Interface.wssyndata.DongBoDuLieu_TrackVNACCS>();
                        List<Company.Interface.wssyndata.DongBoDuLieu_TrackVNACCS> listTKSync = new List<Company.Interface.wssyndata.DongBoDuLieu_TrackVNACCS>();
                        listTKTrack.AddRange(myService.SelectTrack_VNACCS(userLogin, passwordLogin, tungay, denngay, GlobalSettings.MA_DON_VI, idDaily));
                       // KDT_VNACC_ToKhaiMauDich tk = null;
                        foreach (Company.Interface.wssyndata.DongBoDuLieu_TrackVNACCS item in listTKTrack)
                        {
                            if (!listTKVNACC.Contains(item.SoToKhai.ToString()))
                            {
                                listTK.Add(item);
                            }
                            if (listTKVNACC.Contains(item.SoToKhai.ToString()))
                            {
                                listTKSync.Add(item);
                            }
                        }
                        grList.RootTable.Columns["SoHoaDon"].Visible = false;
                        grList.RootTable.Columns["MaLoaiHinh"].Visible = false;
                        grList.RootTable.Columns["MaPhanLoaiKiemTra"].Visible = false;
                        grList.RootTable.Columns["UserName"].Visible = true;

                        if (checkTKSync.Checked)
                        {
                            grList.DataSource = listTKSync;
                        }
                        else
                        {
                            grList.DataSource = listTK;
                        }
                        grList.Refetch();

                    }
               


            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //Lypt create date 20/01/2010


        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
           
        }
        private bool LoginDB()
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                WSDBDLForm dbdlForm = new WSDBDLForm();
                if (GlobalSettings.PASSWOR_DONGBO == null || GlobalSettings.PASSWOR_DONGBO== "")
                {
                    dbdlForm.ShowDialog(this);
                    if (!dbdlForm.IsReady) return false;
                }
            
                    passwordLogin = GlobalSettings.PASSWOR_DONGBO != null ? GlobalSettings.PASSWOR_DONGBO : dbdlForm.PASSWOR_DONGBO;
                    userLogin = GlobalSettings.USERNAME_DONGBO != null ? GlobalSettings.USERNAME_DONGBO : dbdlForm.USERNAME_DONGBO;
               


                Company.Interface.wssyndata.DaiLy userDaiLy = myService.LoginDaiLy(userLogin, passwordLogin);

                if (userDaiLy == null || (userDaiLy.MaDoanhNghiep != GlobalSettings.MA_DON_VI && !userDaiLy.USER_NAME.Equals("administrator")))
                {
                    ShowMessage("Người dùng không hợp lệ!", false);
                    userLogin = "";
                    return false;
                }
                else
                {
                    isAdmin = userDaiLy.isAdmin;
                    idDaily = userDaiLy.ID.ToString();
                    if (isAdmin)
                    {
                        FrmDailyDongBo frm = new FrmDailyDongBo();
                        frm.listDaiLy.AddRange(myService.SelectUserDaiLy(GlobalSettings.MA_DON_VI));
                        frm.ShowDialog();
                        idDaily += frm.userDaily;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }

        private void grList_LoadingRow(object sender, RowLoadEventArgs e)
        {

            if (e.Row.RowType == RowType.Record)
            {
                if (!isAdmin)
                {
                    if (e.Row.Cells["MaPhanLoaiKiemTra"].Value != null)
                    {
                        string pl = e.Row.Cells["MaPhanLoaiKiemTra"].Value.ToString();
                        if (pl == "1")
                            e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Xanh";
                        else if (pl == "2")
                            e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Vàng";
                        else if (pl == "3")
                            e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Luồng Đỏ";
                        else
                            e.Row.Cells["MaPhanLoaiKiemTra"].Text = "Chưa phân luồng";
                    }
                }
               
                if (e.Row.Cells["NgayDangKy"].Value != null)
                {
                    DateTime ngaydk = System.Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value.ToString());
                    if (ngaydk.Year == 1900)
                        e.Row.Cells["NgayDangKy"].Text = "";
                }

            }
        }

        private void FrmDongBoDuLieu_VNACCS_FormClosing(object sender, FormClosingEventArgs e)
        {
            GlobalSettings.USERNAME_DONGBO = "";
            GlobalSettings.PASSWOR_DONGBO = "";
        }

        private void DoSend(object obj)
        {
            try
            {
                SyncDataVNACCS send = new SyncDataVNACCS();
                send.SyncEventArgs += new EventHandler<SyncVNACCSEventArgs>(send_SyncEventArgs);
                send.SendToKhai(listTKVANCCS_DB, GlobalSettings.USERNAME_DONGBO, GlobalSettings.PASSWOR_DONGBO);
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { search(); }));
                }
                else
                    search();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        private void DoSendCTQ(object obj)
        {
            try
            {
                SyncDataVNACCS send = new SyncDataVNACCS();
                send.SyncEventArgs += new EventHandler<SyncVNACCSEventArgs>(send_SyncEventArgs);
                send.SendToKhaiCTQ(listTKVANCCS_DB, GlobalSettings.USERNAME_DONGBO, GlobalSettings.PASSWOR_DONGBO);
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { search(); }));
                }
                else
                    search();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void DoReceive(object obj)
        {
            try
            {
                SyncDataVNACCS receive = new SyncDataVNACCS();
                receive.SyncEventArgs += new EventHandler<SyncVNACCSEventArgs>(receive_SyncEventArgs);
                receive.ReceiveToKhai(ListTrack_DB, GlobalSettings.USERNAME_DONGBO, GlobalSettings.PASSWOR_DONGBO);
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { search(); }));
                }
                else
                    search();
            }
            catch (Exception ex)
            {
               throw ex;
            }
           
        }

        private void DoReceiveCTQ(object obj)
        {
            try
            {
                SyncDataVNACCS receive = new SyncDataVNACCS();
                receive.SyncEventArgs += new EventHandler<SyncVNACCSEventArgs>(receive_SyncEventArgs);
                receive.ReceiveToKhaiCTQ(ListTrack_DB, GlobalSettings.USERNAME_DONGBO, GlobalSettings.PASSWOR_DONGBO);
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate { search(); }));
                }
                else
                    search();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        void receive_SyncEventArgs(object sender, SyncVNACCSEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { SetLable(e); }));
            }
            else
            {
                SetLable(e);
            }
        }

        void send_SyncEventArgs(object sender, SyncVNACCSEventArgs e)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate { SetLable(e); }));
            }
            else
            {
                SetLable(e);
            }
        }
        private void SetLable(SyncVNACCSEventArgs e)
        {
            if (e.Error != null)
            {
                Exception ex = new Exception(e.Error.ToString());
                
                txtError.Text = txtError.Text+"\r\n"+e.Error.ToString();//Set lable error;
                Logger.LocalLogger.Instance().WriteMessage("Lỗi đồng bộ tờ khai", ex);
            }
            else if (!string.IsNullOrEmpty(e.Message))
            {
                txtError.Text = "";
                lblPhanTram.Text = e.Percent.ToString() + "%";// Set lable trang thai
            }
            if (e.Percent > 0)
               prbTienTrinh.Value= e.Percent;// set  Process = percent
        }
      

    }
}
