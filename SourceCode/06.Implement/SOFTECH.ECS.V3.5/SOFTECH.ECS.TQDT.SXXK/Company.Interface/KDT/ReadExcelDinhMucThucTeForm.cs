﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.KDT.GC
{
    public partial class ReadExcelDinhMucThucTeForm : Company.Interface.BaseForm
    {
        public KDT_SXXK_DinhMucThucTeDangKy dinhMucThucTeDangKy = new KDT_SXXK_DinhMucThucTeDangKy();
        public int FormatDMSD = GlobalSettings.SoThapPhan.DinhMuc;
        public bool isRead = false;
        public ReadExcelDinhMucThucTeForm()
        {
            InitializeComponent();
        }

        private void ReadExcelDinhMucThucTeForm_Load(object sender, EventArgs e)
        {

        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    MLMessages("Lỗi khi chọn file. Doanh nghiệp hãy kiểm tra lại đường dẫn hoặc đóng file trước khi đọc.", "MSG_EXC03", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("Lỗi khi đọc sheet. Doanh nghiệp hãy kiểm tra lại tên sheet trước khi chọn file.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtFilePath, errorProvider, "ĐƯỜNG DẪN FILE EXCEL", isOnlyWarning);
                isValid &= ValidateControl.ValidateChoose(cbbSheetName, errorProvider, "TÊN SHEET", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaNPLColumn, errorProvider, "MÃ NGUYÊN PHỤ LIỆU", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaSPColumn, errorProvider, "MÃ SẢN PHẨM", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtDMSDColumn, errorProvider, "ĐỊNH MỨC SỬ DỤNG", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtRow, errorProvider, "DÒNG BẮT ĐẦU");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                openFileDialog1.ShowDialog();
                txtFilePath.Text = openFileDialog1.FileName;
                cbbSheetName.DataSource = GetAllSheetName();
                cbbSheetName.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void btnRead_Click(object sender, EventArgs e)
        {
            if (!ValidateForm(false))
                return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;

            btnRead.Enabled = false;
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text);
            }
            catch (Exception ex)
            {
                ShowMessage("Lỗi liên quan đến đường dẫn tên file không tồn tại hoặc file đang mở, vui lòng kiểm tra lại.\r\n\nLỖI CHI TIẾT: " + ex.Message, false);
                this.btnRead.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }
            try
            {
                ws = wb.Worksheets[cbbSheetName.Text];
            }
            catch
            {
                this.btnRead.Enabled = true;
                this.Cursor = Cursors.Default;
                return;
            }

            WorksheetRowCollection wsrc = ws.Rows;
            char maSPColumn = Convert.ToChar(txtMaSPColumn.Text);
            int maSPCol = ConvertCharToInt(maSPColumn);

            char maNPLColumn = Convert.ToChar(txtMaNPLColumn.Text);
            int maNPLCol = ConvertCharToInt(maNPLColumn);

            char DMSDColumn = Convert.ToChar(txtDMSDColumn.Text);
            int DMSDCol = ConvertCharToInt(DMSDColumn);

            string errorTotal = "";
            string errorMaSanPham = "";
            string errorMaSanPhamExits = "";
            string errorMaNguyenPhuLieu = "";
            string errorMaNguyenPhuLieuExits = "";
            string errorDinhMuc = "";
            string errorDinhMucExits = "";
            string errorDMSD = "";
            string errorDMSDExits = "";

            string errorNPL = "";
            string errorSP = "";
            string errorSPExits = "";
            bool isCheck = false;
            string MaNPLCheck = String.Empty;
            string MaSPCheck = String.Empty;
            bool isAdd = true;
            bool isExits = false;
            List<SXXK_NguyenPhuLieu> AllNPLCollection = SXXK_NguyenPhuLieu.SelectCollectionDynamic("MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "'", "");
            List<KDT_LenhSanXuat_SP> AllSPCollection = KDT_LenhSanXuat_SP.SelectCollectionBy_LenhSanXuat_ID(dinhMucThucTeDangKy.LenhSanXuat_ID);
            List<SXXK_SanPham> SPCollectionCheck = new List<SXXK_SanPham>();
            List<SXXK_NguyenPhuLieu> NPLCollectionCheck = new List<SXXK_NguyenPhuLieu>();

            List<KDT_SXXK_DinhMucThucTe_DinhMuc> DinhMucSPCollection = new List<KDT_SXXK_DinhMucThucTe_DinhMuc>();
            List<KDT_SXXK_DinhMucThucTe_SP> SPCollection = new List<KDT_SXXK_DinhMucThucTe_SP>();
            KDT_SXXK_DinhMucThucTe_SP dinhMucThucTe_SP = new KDT_SXXK_DinhMucThucTe_SP();
            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    { 
                        try
                        {
                            dinhMucThucTe_SP.MaSanPham = Convert.ToString(wsr.Cells[maSPCol].Value).Trim();
                            if (dinhMucThucTe_SP.MaSanPham.Length == 0)
                            {
                                errorMaSanPham += "[" + (wsr.Index + 1) + "]-[" + dinhMucThucTe_SP.MaSanPham + "]\n";
                                isAdd = false;
                            }
                            else
                            {
                                //TẠO DANH SÁCH SẢN PHẨM ĐỂ KIỂM TRA DANH SÁCH SẢN PHẨM CÓ NẰM TRONG LỆNH SẢN XUẤT NÀY KHÔNG
                                if (String.IsNullOrEmpty(MaSPCheck))
                                {
                                    SXXK_SanPham SP = new SXXK_SanPham();
                                    SP.Ma = dinhMucThucTe_SP.MaSanPham;
                                    SP.STT = (wsr.Index + 1);
                                    SPCollectionCheck.Add(SP);
                                    MaSPCheck = dinhMucThucTe_SP.MaSanPham;

                                    DinhMucSPCollection = new List<KDT_SXXK_DinhMucThucTe_DinhMuc>();
                                    KDT_SXXK_DinhMucThucTe_DinhMuc dinhMuc = new KDT_SXXK_DinhMucThucTe_DinhMuc();
                                    try
                                    {
                                        dinhMuc.MaNPL = Convert.ToString(wsr.Cells[maNPLCol].Value).Trim();
                                        isExits = false;
                                        if (dinhMuc.MaNPL.Length == 0)
                                        {
                                            errorMaNguyenPhuLieu += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNPL + "]\n";
                                            isAdd = false;
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(MaNPLCheck))
                                            {
                                                SXXK_NguyenPhuLieu NPL = new SXXK_NguyenPhuLieu();
                                                NPL.Ma = dinhMuc.MaNPL;
                                                NPL.STT = (wsr.Index + 1);
                                                NPLCollectionCheck.Add(NPL);
                                                MaNPLCheck = dinhMuc.MaNPL;
                                            }
                                            else if (MaNPLCheck != dinhMuc.MaNPL)
                                            {
                                                SXXK_NguyenPhuLieu NPL = new SXXK_NguyenPhuLieu();
                                                NPL.Ma = dinhMuc.MaNPL;
                                                NPL.STT = (wsr.Index + 1);
                                                NPLCollectionCheck.Add(NPL);
                                                MaNPLCheck = dinhMuc.MaNPL;
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        errorMaNguyenPhuLieu += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNPL + "]\n";
                                        isAdd = false;
                                    }
                                    string DinhMucSuDung = String.Empty;
                                    try
                                    {
                                        DinhMucSuDung = wsr.Cells[DMSDCol].Value.ToString();
                                        if (DinhMucSuDung.Length == 0)
                                        {
                                            errorDMSD += "[" + (wsr.Index + 1) + "]-[" + DinhMucSuDung + "]\n";
                                            isAdd = false;
                                        }
                                        else
                                        {
                                            dinhMuc.DinhMucSuDung = Convert.ToDecimal(wsr.Cells[DMSDCol].Value);
                                            if (Decimal.Round(dinhMuc.DinhMucSuDung, FormatDMSD) != dinhMuc.DinhMucSuDung)
                                            {
                                                errorDMSDExits += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.DinhMucSuDung + "]\n";
                                                isAdd = false;
                                            }

                                        }
                                    }
                                    catch (Exception)
                                    {
                                        errorDMSD += "[" + (wsr.Index + 1) + "]-[" + DinhMucSuDung + "]\n";
                                        isAdd = false;
                                    }
                                    if (isAdd)
                                        DinhMucSPCollection.Add(dinhMuc);
                                }
                                else if (MaSPCheck == dinhMucThucTe_SP.MaSanPham)
                                {
                                    KDT_SXXK_DinhMucThucTe_DinhMuc dinhMuc = new KDT_SXXK_DinhMucThucTe_DinhMuc();
                                    try
                                    {
                                        dinhMuc.MaNPL = Convert.ToString(wsr.Cells[maNPLCol].Value).Trim();
                                        isExits = false;
                                        if (dinhMuc.MaNPL.Length == 0)
                                        {
                                            errorMaNguyenPhuLieu += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNPL + "]\n";
                                            isAdd = false;
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(MaNPLCheck))
                                            {
                                                SXXK_NguyenPhuLieu NPL = new SXXK_NguyenPhuLieu();
                                                NPL.Ma = dinhMuc.MaNPL;
                                                NPL.STT = (wsr.Index + 1);
                                                NPLCollectionCheck.Add(NPL);
                                                MaNPLCheck = dinhMuc.MaNPL;
                                            }
                                            else if (MaNPLCheck != dinhMuc.MaNPL)
                                            {
                                                SXXK_NguyenPhuLieu NPL = new SXXK_NguyenPhuLieu();
                                                NPL.Ma = dinhMuc.MaNPL;
                                                NPL.STT = (wsr.Index + 1);
                                                NPLCollectionCheck.Add(NPL);
                                                MaNPLCheck = dinhMuc.MaNPL;
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        errorMaNguyenPhuLieu += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNPL + "]\n";
                                        isAdd = false;
                                    }
                                    string DinhMucSuDung = String.Empty;
                                    try
                                    {
                                        DinhMucSuDung = wsr.Cells[DMSDCol].Value.ToString();
                                        if (DinhMucSuDung.Length == 0)
                                        {
                                            errorDMSD += "[" + (wsr.Index + 1) + "]-[" + DinhMucSuDung + "]\n";
                                            isAdd = false;
                                        }
                                        else
                                        {
                                            dinhMuc.DinhMucSuDung = Convert.ToDecimal(wsr.Cells[DMSDCol].Value);
                                            if (Decimal.Round(dinhMuc.DinhMucSuDung, FormatDMSD) != dinhMuc.DinhMucSuDung)
                                            {
                                                errorDMSDExits += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.DinhMucSuDung + "]\n";
                                                isAdd = false;
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        errorDMSD += "[" + (wsr.Index + 1) + "]-[" + DinhMucSuDung + "]\n";
                                        isAdd = false;
                                    }
                                    if (isAdd)
                                        DinhMucSPCollection.Add(dinhMuc);
                                }
                                else if (MaSPCheck != dinhMucThucTe_SP.MaSanPham)
                                {
                                    dinhMucThucTe_SP.MaSanPham = MaSPCheck;
                                    dinhMucThucTe_SP.DMCollection = DinhMucSPCollection;
                                    SPCollection.Add(dinhMucThucTe_SP);

                                    dinhMucThucTe_SP = new KDT_SXXK_DinhMucThucTe_SP();
                                    DinhMucSPCollection = new List<KDT_SXXK_DinhMucThucTe_DinhMuc>();

                                    dinhMucThucTe_SP.MaSanPham = Convert.ToString(wsr.Cells[maSPCol].Value).Trim();
                                    SXXK_SanPham SP = new SXXK_SanPham();
                                    SP.Ma = dinhMucThucTe_SP.MaSanPham;
                                    SP.STT = (wsr.Index + 1);
                                    SPCollectionCheck.Add(SP);
                                    MaSPCheck = dinhMucThucTe_SP.MaSanPham;
                                    KDT_SXXK_DinhMucThucTe_DinhMuc dinhMuc = new KDT_SXXK_DinhMucThucTe_DinhMuc();
                                    try
                                    {
                                        dinhMuc.MaNPL = Convert.ToString(wsr.Cells[maNPLCol].Value).Trim();
                                        isExits = false;
                                        if (dinhMuc.MaNPL.Length == 0)
                                        {
                                            errorMaNguyenPhuLieu += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNPL + "]\n";
                                            isAdd = false;
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(MaNPLCheck))
                                            {
                                                SXXK_NguyenPhuLieu NPL = new SXXK_NguyenPhuLieu();
                                                NPL.Ma = dinhMuc.MaNPL;
                                                NPL.STT = (wsr.Index + 1);
                                                NPLCollectionCheck.Add(NPL);
                                                MaNPLCheck = dinhMuc.MaNPL;
                                            }
                                            else if (MaNPLCheck != dinhMuc.MaNPL)
                                            {
                                                SXXK_NguyenPhuLieu NPL = new SXXK_NguyenPhuLieu();
                                                NPL.Ma = dinhMuc.MaNPL;
                                                NPL.STT = (wsr.Index + 1);
                                                NPLCollectionCheck.Add(NPL);
                                                MaNPLCheck = dinhMuc.MaNPL;
                                            }
                                        }
                                    }
                                    catch (Exception)
                                    {
                                        errorMaNguyenPhuLieu += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNPL + "]\n";
                                        isAdd = false;
                                    }
                                    string DinhMucSuDung = String.Empty;
                                    try
                                    {
                                        DinhMucSuDung = wsr.Cells[DMSDCol].Value.ToString();
                                        if (DinhMucSuDung.Length == 0)
                                        {
                                            errorDMSD += "[" + (wsr.Index + 1) + "]-[" + DinhMucSuDung + "]\n";
                                            isAdd = false;
                                        }
                                        else
                                        {
                                            dinhMuc.DinhMucSuDung = Convert.ToDecimal(wsr.Cells[DMSDCol].Value);
                                            if (Decimal.Round(dinhMuc.DinhMucSuDung, FormatDMSD) != dinhMuc.DinhMucSuDung)
                                            {
                                                errorDMSDExits += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.DinhMucSuDung + "]\n";
                                                isAdd = false;
                                            }

                                        }
                                    }
                                    catch (Exception)
                                    {
                                        errorDMSD += "[" + (wsr.Index + 1) + "]-[" + DinhMucSuDung + "]\n";
                                        isAdd = false;
                                    }
                                    if (isAdd)
                                        DinhMucSPCollection.Add(dinhMuc);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            errorMaSanPham += "[" + (wsr.Index + 1) + "]-[" + MaSPCheck + "]\n";
                            isAdd = false;
                        }                        
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                        return;
                    }
                }
            }
            dinhMucThucTe_SP.MaSanPham = MaSPCheck;
            dinhMucThucTe_SP.DMCollection = DinhMucSPCollection;
            SPCollection.Add(dinhMucThucTe_SP);

            String STT = String.Empty;
            String MaSPTemp = String.Empty;
            if (String.IsNullOrEmpty(errorSP))
            {
                //KIỂM TRA MÃ SP CÓ NẰM TRONG LỆNH SẢN XUẤT NÀY KHÔNG
                MaSPTemp = String.Empty;
                foreach (SXXK_SanPham item in SPCollectionCheck)
                {
                    isExits = false;
                    foreach (KDT_LenhSanXuat_SP items in AllSPCollection)
                    {
                        if (item.Ma == items.MaSanPham)
                        {
                            isExits = true;
                            foreach (KDT_SXXK_DinhMucThucTe_SP sp in SPCollection)
                            {
                                if (sp.MaSanPham == items.MaSanPham)
                                {
                                    sp.TenSanPham = items.TenSanPham;
                                    sp.MaHS = items.MaHS;
                                    sp.DVT_ID = items.DVT_ID;
                                    sp.GhiChu = items.GhiChu;
                                    sp.TrangThai = items.TrangThai;
                                    break;
                                }
                            }
                            break;
                        }
                    }
                    if (!isExits)
                    {
                        if (MaSPTemp != item.Ma)
                        {
                            errorSP += "[" + item.STT + "]-[" + item.Ma + "]\n";
                        }
                        MaSPTemp = item.Ma;
                    }
                }
            }
            String MaNPLTemp = String.Empty;
            if (String.IsNullOrEmpty(errorNPL))
            {
                //KIỂM TRA MÃ NPL ĐÃ ĐĂNG KÝ VÀO HỢP ĐỒNG CHƯA
                foreach (SXXK_NguyenPhuLieu item in NPLCollectionCheck)
                {
                    isExits = false;
                    foreach (SXXK_NguyenPhuLieu items in AllNPLCollection)
                    {
                        if (item.Ma == items.Ma)
                        {
                            item.Ten = items.Ten;
                            isExits = true;
                            foreach (KDT_SXXK_DinhMucThucTe_SP sp in SPCollection)
                            {
                                int k = 1;
                                foreach (KDT_SXXK_DinhMucThucTe_DinhMuc dinhmuc in sp.DMCollection)
                                {
                                    if (dinhmuc.MaNPL == items.Ma)
                                    {
                                        dinhmuc.STT = k;
                                        dinhmuc.MaSanPham = sp.MaSanPham;
                                        dinhmuc.TenSanPham = sp.TenSanPham;
                                        dinhmuc.DVT_SP = sp.DVT_ID;
                                        dinhmuc.MaNPL = items.Ma;
                                        dinhmuc.TenNPL = items.Ten;
                                        dinhmuc.DVT_NPL = items.DVT_ID;
                                        dinhmuc.MaHS = items.MaHS;
                                        k++;
                                    }
                                }
                            }
                            break;
                        }
                    }
                    if (!isExits)
                    {
                        if (MaNPLTemp != item.Ma)
                        {
                            errorNPL += "[" + item.STT + "]-[" + item.Ma + "]\n";
                        }
                        MaNPLTemp = item.Ma;
                    }
                }
            }
            if (!String.IsNullOrEmpty(errorMaSanPham))
                errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorMaSanPham + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorMaNguyenPhuLieu))
                errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorMaNguyenPhuLieu + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDMSD))
                errorTotal += "\n - [STT] -[ĐỊNH MỨC SỬ DỤNG] : \n" + errorDMSD + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
            if (!String.IsNullOrEmpty(errorDMSDExits))
                errorTotal += "\n - [STT] -[ĐỊNH MỨC SỬ DỤNG] : \n" + errorDMSDExits + " DOANH NGHIỆP CẤU HÌNH CHỈ ĐƯỢC NHẬP TỐI ĐA " + FormatDMSD + " SỐ THẬP PHÂN . ĐỂ CẤU HÌNH : VÀO MENU HỆ THỐNG - CẤU HÌNH THÔNG SỐ ĐỌC EXCEL\n";
            if (!String.IsNullOrEmpty(errorNPL))
                errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorNPL + " ĐÃ NHẬP CHƯA ĐƯỢC ĐĂNG KÝ CÙNG HỢP ĐỒNG\n";
            if (!String.IsNullOrEmpty(errorSP))
                errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorSP + " NÀY KHÔNG THUỘC LỆNH SẢM XUẤT NÀY\n";

            if (String.IsNullOrEmpty(errorTotal))
            {
                // Xóa định mức đã nhập trước đó
                foreach (KDT_SXXK_DinhMucThucTe_SP item in dinhMucThucTeDangKy.SPCollection)
                {
                    foreach (KDT_SXXK_DinhMucThucTe_DinhMuc itemss in item.DMCollection)
                    {
                        if (itemss.ID > 0)
                            itemss.Delete();
                    }
                    if (item.ID > 0)
                        item.Delete();
                    dinhMucThucTeDangKy.SPCollection.Remove(item);
                }
                dinhMucThucTeDangKy.SPCollection = SPCollection;
                ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG .", false);
            }
            else
            {
                ShowMessageTQDT("Thông báo từ hệ thống", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG .\r\n" + errorTotal, false);
                return;
            }
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
