﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.GC.BLL.GC;
using Company.KDT.SHARE.Components;
namespace Company.Interface.GC
{
    public partial class BaoCaoQuyetToanMMTBManagementForm : BaseForm
    {
        public string where = "";
        public BaoCaoQuyetToanMMTBManagementForm()
        {
            InitializeComponent();
        }
        public void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = KDT_VNACCS_BaoCaoQuyetToan_MMTB.SelectCollectionAll();
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }

        }

        private void BaoCaoQuyetToanMMTBManagementForm_Load(object sender, EventArgs e)
        {
            //BindData();
            ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
            ctrCoQuanHaiQuan.BackColor = Color.Transparent;
            cbStatus.SelectedValue = 0;
            txtNamQT.Text = DateTime.Now.Year.ToString();
            GetSearchWhere();
        }
        private string GetSearchWhere()
        {
            try
            {
                where = " 1=1";
                if (!String.IsNullOrEmpty(txtSoTiepNhan.Text))
                    where += " AND SoTN LIKE '%" + txtSoTiepNhan.Text +"%'";
                if (!String.IsNullOrEmpty(txtNamQT.Text))
                    where += " AND YEAR(NgayTN) = " + txtNamQT.Text;
                if (!String.IsNullOrEmpty(cbStatus.SelectedValue.ToString()))
                    where += " AND TrangThaiXuLy =" + cbStatus.SelectedValue;
                if (!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Code))
                    where += " AND MaHQ = '" + ctrCoQuanHaiQuan.Code + "'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
                return null;
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = KDT_VNACCS_BaoCaoQuyetToan_MMTB.SelectCollectionDynamic(GetSearchWhere(), "ID");
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (showMsg("MSG_DEL01", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            KDT_VNACCS_BaoCaoQuyetToan_MMTB goodItem = new KDT_VNACCS_BaoCaoQuyetToan_MMTB();
                            goodItem = (KDT_VNACCS_BaoCaoQuyetToan_MMTB)i.GetRow().DataRow;
                            goodItem.GoodItemCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail.SelectCollectionBy_GoodItems(goodItem.ID);
                            foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodItem.GoodItemCollection)
                            {
                                item.GoodItemDetailCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail.SelectCollectionBy_Contract_ID(item.ID);
                            }
                            if (goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || goodItem.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA || goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || goodItem.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                            {
                                ShowMessage("Báo cáo quyết toán này đã gửi lên HQ nên không được xóa !", false);
                            }
                            else
                            {
                                goodItem.DeleteFull();
                            }
                        }
                    }
                    ShowMessage("Xóa thành công", false);
                    //BindData();
                    btnSearch_Click(null, null);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                KDT_VNACCS_BaoCaoQuyetToan_MMTB goodItem = new KDT_VNACCS_BaoCaoQuyetToan_MMTB();
                goodItem = (KDT_VNACCS_BaoCaoQuyetToan_MMTB)e.Row.DataRow;
                goodItem.GoodItemCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail.SelectCollectionBy_GoodItems(goodItem.ID);
                foreach (KDT_VNACCS_BaoCaoQuyetToan_MMTB_HopDong_Detail item in goodItem.GoodItemCollection)
                {
                    item.GoodItemDetailCollection = KDT_VNACCS_BaoCaoQuyetToan_MMTB_Detail.SelectCollectionBy_Contract_ID(item.ID);
                }
                BaoCaoQuyetToanMMTB f = new BaoCaoQuyetToanMMTB();
                f.IsActive = true;
                f.goodItem = goodItem;
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                
            }
        }

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                string TrangThai = e.Row.Cells["TrangThaiXuLy"].Value.ToString();
                switch (TrangThai)
                {
                    case "0":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                        break;
                    case "-1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                        break;
                    case "1":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                        break;
                    case "2":
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                        break;

                }
            }
        }

        private void cbStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnSearch_Click(null,null);
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách báo cáo quyết toán MMTB_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = dgList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage("Có lỗi trong quá trình xử lý :\r\n " + ex.Message, false);
            }
        }
    }
}
