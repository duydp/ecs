﻿namespace Company.Interface.KDT.SXXK
{
    partial class BaoCaoChotTonReadExcelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaoCaoChotTonReadExcelForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.linkExcelMau = new System.Windows.Forms.LinkLabel();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.btnSelectFile = new Janus.Windows.EditControls.UIButton();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbSheetName = new Janus.Windows.EditControls.UIComboBox();
            this.txtDVTColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTKTTColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTKSSColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(923, 163);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.linkExcelMau);
            this.uiGroupBox2.Controls.Add(this.btnSave);
            this.uiGroupBox2.Controls.Add(this.btnSelectFile);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 77);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(923, 86);
            this.uiGroupBox2.TabIndex = 3;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(437, 51);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 22);
            this.btnClose.TabIndex = 3;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // linkExcelMau
            // 
            this.linkExcelMau.AutoSize = true;
            this.linkExcelMau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkExcelMau.Location = new System.Drawing.Point(12, 56);
            this.linkExcelMau.Name = "linkExcelMau";
            this.linkExcelMau.Size = new System.Drawing.Size(89, 13);
            this.linkExcelMau.TabIndex = 25;
            this.linkExcelMau.TabStop = true;
            this.linkExcelMau.Text = "Mở file excel mẫu";
            this.linkExcelMau.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkExcelMau_LinkClicked);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSave.Location = new System.Drawing.Point(352, 51);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 22);
            this.btnSave.TabIndex = 2;
            this.btnSave.Text = "Ghi";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFile.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectFile.Image")));
            this.btnSelectFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSelectFile.Location = new System.Drawing.Point(653, 18);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(93, 23);
            this.btnSelectFile.TabIndex = 1;
            this.btnSelectFile.Text = "Chọn file";
            this.btnSelectFile.VisualStyleManager = this.vsmMain;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(13, 19);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(633, 21);
            this.txtFilePath.TabIndex = 0;
            this.txtFilePath.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.cbbSheetName);
            this.uiGroupBox1.Controls.Add(this.txtDVTColumn);
            this.uiGroupBox1.Controls.Add(this.txtTKTTColumn);
            this.uiGroupBox1.Controls.Add(this.txtTKSSColumn);
            this.uiGroupBox1.Controls.Add(this.txtTenHangColumn);
            this.uiGroupBox1.Controls.Add(this.txtMaHangColumn);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.txtRow);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(923, 77);
            this.uiGroupBox1.TabIndex = 2;
            this.uiGroupBox1.Text = "Thông số cấu hình";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // cbbSheetName
            // 
            this.cbbSheetName.DisplayMember = "Name";
            this.cbbSheetName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSheetName.Location = new System.Drawing.Point(13, 42);
            this.cbbSheetName.Name = "cbbSheetName";
            this.cbbSheetName.Size = new System.Drawing.Size(151, 22);
            this.cbbSheetName.TabIndex = 32;
            this.cbbSheetName.Tag = "";
            this.cbbSheetName.ValueMember = "ID";
            this.cbbSheetName.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtDVTColumn
            // 
            this.txtDVTColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTColumn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTColumn.Location = new System.Drawing.Point(496, 42);
            this.txtDVTColumn.MaxLength = 1;
            this.txtDVTColumn.Name = "txtDVTColumn";
            this.txtDVTColumn.Size = new System.Drawing.Size(71, 21);
            this.txtDVTColumn.TabIndex = 11;
            this.txtDVTColumn.Text = "C";
            this.txtDVTColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtTKTTColumn
            // 
            this.txtTKTTColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTKTTColumn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTKTTColumn.Location = new System.Drawing.Point(743, 42);
            this.txtTKTTColumn.MaxLength = 1;
            this.txtTKTTColumn.Name = "txtTKTTColumn";
            this.txtTKTTColumn.Size = new System.Drawing.Size(75, 21);
            this.txtTKTTColumn.TabIndex = 9;
            this.txtTKTTColumn.Text = "E";
            this.txtTKTTColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTKSSColumn
            // 
            this.txtTKSSColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTKSSColumn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTKSSColumn.Location = new System.Drawing.Point(592, 42);
            this.txtTKSSColumn.MaxLength = 1;
            this.txtTKSSColumn.Name = "txtTKSSColumn";
            this.txtTKSSColumn.Size = new System.Drawing.Size(75, 21);
            this.txtTKSSColumn.TabIndex = 9;
            this.txtTKSSColumn.Text = "D";
            this.txtTKSSColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtTenHangColumn
            // 
            this.txtTenHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHangColumn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangColumn.Location = new System.Drawing.Point(385, 42);
            this.txtTenHangColumn.MaxLength = 1;
            this.txtTenHangColumn.Name = "txtTenHangColumn";
            this.txtTenHangColumn.Size = new System.Drawing.Size(75, 21);
            this.txtTenHangColumn.TabIndex = 7;
            this.txtTenHangColumn.Text = "B";
            this.txtTenHangColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHangColumn
            // 
            this.txtMaHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHangColumn.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangColumn.Location = new System.Drawing.Point(272, 42);
            this.txtMaHangColumn.MaxLength = 1;
            this.txtMaHangColumn.Name = "txtMaHangColumn";
            this.txtMaHangColumn.Size = new System.Drawing.Size(86, 21);
            this.txtMaHangColumn.TabIndex = 5;
            this.txtMaHangColumn.Text = "A";
            this.txtMaHangColumn.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên Sheet";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(269, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cột Mã hàng hóa";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(169, 23);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(53, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Dòng đầu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(382, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(93, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cột Tên hàng hóa";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(740, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(146, 13);
            this.label7.TabIndex = 8;
            this.label7.Text = "Cột Số lượng tồn kho thực tế";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(589, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(147, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cột Số lượng tồn kho sổ sách";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(493, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(47, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Cột ĐVT";
            // 
            // txtRow
            // 
            this.txtRow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRow.Location = new System.Drawing.Point(172, 43);
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(75, 21);
            this.txtRow.TabIndex = 3;
            this.txtRow.Text = "2";
            this.txtRow.Value = 2;
            this.txtRow.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtRow.VisualStyleManager = this.vsmMain;
            // 
            // BaoCaoChotTonReadExcelForm
            // 
            this.ClientSize = new System.Drawing.Size(923, 163);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "BaoCaoChotTonReadExcelForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Đọc danh sách hàng hóa từ Excel";
            this.Load += new System.EventHandler(this.BaoCaoChotTonReadExcelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnSelectFile;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.EditControls.UIComboBox cbbSheetName;
        private System.Windows.Forms.LinkLabel linkExcelMau;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtTKSSColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangColumn;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label label7;
        private Janus.Windows.GridEX.EditControls.EditBox txtTKTTColumn;
    }
}
