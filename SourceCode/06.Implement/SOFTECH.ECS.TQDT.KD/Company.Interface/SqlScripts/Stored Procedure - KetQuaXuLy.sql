USE [ECS.TQDT.KD]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_KetQuaXuLy_Delete]    Script Date: 05/17/2010 02:11:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_Delete]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_KetQuaXuLy]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_KetQuaXuLy_DeleteDynamic]    Script Date: 05/17/2010 02:11:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_DeleteDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_KetQuaXuLy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_KetQuaXuLy_Insert]    Script Date: 05/17/2010 02:11:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_Insert]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_Insert]
	@ReferenceID uniqueidentifier,
	@LoaiChungTu varchar(10),
	@LoaiThongDiep nvarchar(100),
	@NoiDung nvarchar(500),
	@Ngay datetime,
	@ID int OUTPUT
AS
INSERT INTO [dbo].[t_KDT_KetQuaXuLy]
(
	[ReferenceID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay]
)
VALUES 
(
	@ReferenceID,
	@LoaiChungTu,
	@LoaiThongDiep,
	@NoiDung,
	@Ngay
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_KetQuaXuLy_InsertUpdate]    Script Date: 05/17/2010 02:11:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_InsertUpdate]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_InsertUpdate]
	@ID int,
	@ReferenceID uniqueidentifier,
	@LoaiChungTu varchar(10),
	@LoaiThongDiep nvarchar(100),
	@NoiDung nvarchar(500),
	@Ngay datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_KetQuaXuLy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_KetQuaXuLy] 
		SET
			[ReferenceID] = @ReferenceID,
			[LoaiChungTu] = @LoaiChungTu,
			[LoaiThongDiep] = @LoaiThongDiep,
			[NoiDung] = @NoiDung,
			[Ngay] = @Ngay
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_KetQuaXuLy]
		(
			[ReferenceID],
			[LoaiChungTu],
			[LoaiThongDiep],
			[NoiDung],
			[Ngay]
		)
		VALUES 
		(
			@ReferenceID,
			@LoaiChungTu,
			@LoaiThongDiep,
			@NoiDung,
			@Ngay
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_KetQuaXuLy_Load]    Script Date: 05/17/2010 02:11:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_Load]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay]
FROM
	[dbo].[t_KDT_KetQuaXuLy]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_KetQuaXuLy_SelectAll]    Script Date: 05/17/2010 02:11:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_SelectAll]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[ReferenceID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay]
FROM
	[dbo].[t_KDT_KetQuaXuLy]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_KetQuaXuLy_SelectDynamic]    Script Date: 05/17/2010 02:11:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_SelectDynamic]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[ReferenceID],
	[LoaiChungTu],
	[LoaiThongDiep],
	[NoiDung],
	[Ngay]
FROM [dbo].[t_KDT_KetQuaXuLy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_KetQuaXuLy_Update]    Script Date: 05/17/2010 02:11:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_KetQuaXuLy_Update]
-- Database: ECS.TQDT.KD
-- Author: Ngo Thanh Tung
-- Time created: Sunday, May 16, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_KetQuaXuLy_Update]
	@ID int,
	@ReferenceID uniqueidentifier,
	@LoaiChungTu varchar(10),
	@LoaiThongDiep nvarchar(100),
	@NoiDung nvarchar(500),
	@Ngay datetime
AS

UPDATE
	[dbo].[t_KDT_KetQuaXuLy]
SET
	[ReferenceID] = @ReferenceID,
	[LoaiChungTu] = @LoaiChungTu,
	[LoaiThongDiep] = @LoaiThongDiep,
	[NoiDung] = @NoiDung,
	[Ngay] = @Ngay
WHERE
	[ID] = @ID


GO

