
/****** Object:  Table [dbo].[t_KDT_SXXK_HSTKBoSung]    Script Date: 03/13/2013 08:41:20 ******/
IF  not EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[t_KDT_SXXK_HSTKBoSung]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[t_KDT_SXXK_HSTKBoSung](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[HoSoDangKy_ID] [bigint] NOT NULL,
	[NgayKhaiBao] [datetime] NOT NULL,
	[SoLuongChungTuKemTheo] [int] NOT NULL,
	[GhiChu] [nvarchar](150) NULL,
 CONSTRAINT [PK_t_KDT_SXXK_HSTKBoSung] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_Delete]    Script Date: 03/13/2013 08:42:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HSTKBoSung_Delete]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_Delete]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_DeleteBy_HoSoDangKy_ID]    Script Date: 03/13/2013 08:42:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HSTKBoSung_DeleteBy_HoSoDangKy_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_DeleteBy_HoSoDangKy_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_DeleteDynamic]    Script Date: 03/13/2013 08:42:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HSTKBoSung_DeleteDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_DeleteDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_Insert]    Script Date: 03/13/2013 08:42:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HSTKBoSung_Insert]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_Insert]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_InsertUpdate]    Script Date: 03/13/2013 08:42:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HSTKBoSung_InsertUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_InsertUpdate]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_Load]    Script Date: 03/13/2013 08:42:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HSTKBoSung_Load]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_Load]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_SelectAll]    Script Date: 03/13/2013 08:42:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HSTKBoSung_SelectAll]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_SelectAll]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_SelectBy_HoSoDangKy_ID]    Script Date: 03/13/2013 08:42:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HSTKBoSung_SelectBy_HoSoDangKy_ID]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_SelectBy_HoSoDangKy_ID]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_SelectDynamic]    Script Date: 03/13/2013 08:42:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HSTKBoSung_SelectDynamic]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_SelectDynamic]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_Update]    Script Date: 03/13/2013 08:42:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_SXXK_HSTKBoSung_Update]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_Update]
GO


/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_Delete]    Script Date: 03/13/2013 08:42:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HSTKBoSung_Delete]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_SXXK_HSTKBoSung]
WHERE
	[ID] = @ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_DeleteBy_HoSoDangKy_ID]    Script Date: 03/13/2013 08:42:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HSTKBoSung_DeleteBy_HoSoDangKy_ID]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_DeleteBy_HoSoDangKy_ID]
	@HoSoDangKy_ID bigint
AS

DELETE FROM [dbo].[t_KDT_SXXK_HSTKBoSung]
WHERE
	[HoSoDangKy_ID] = @HoSoDangKy_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_DeleteDynamic]    Script Date: 03/13/2013 08:42:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HSTKBoSung_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_SXXK_HSTKBoSung] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_Insert]    Script Date: 03/13/2013 08:42:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HSTKBoSung_Insert]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_Insert]
	@HoSoDangKy_ID bigint,
	@NgayKhaiBao datetime,
	@SoLuongChungTuKemTheo int,
	@GhiChu nvarchar(150),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_SXXK_HSTKBoSung]
(
	[HoSoDangKy_ID],
	[NgayKhaiBao],
	[SoLuongChungTuKemTheo],
	[GhiChu]
)
VALUES 
(
	@HoSoDangKy_ID,
	@NgayKhaiBao,
	@SoLuongChungTuKemTheo,
	@GhiChu
)

SET @ID = SCOPE_IDENTITY()


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_InsertUpdate]    Script Date: 03/13/2013 08:42:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HSTKBoSung_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_InsertUpdate]
	@ID bigint,
	@HoSoDangKy_ID bigint,
	@NgayKhaiBao datetime,
	@SoLuongChungTuKemTheo int,
	@GhiChu nvarchar(150)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_SXXK_HSTKBoSung] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_SXXK_HSTKBoSung] 
		SET
			[HoSoDangKy_ID] = @HoSoDangKy_ID,
			[NgayKhaiBao] = @NgayKhaiBao,
			[SoLuongChungTuKemTheo] = @SoLuongChungTuKemTheo,
			[GhiChu] = @GhiChu
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_SXXK_HSTKBoSung]
		(
			[HoSoDangKy_ID],
			[NgayKhaiBao],
			[SoLuongChungTuKemTheo],
			[GhiChu]
		)
		VALUES 
		(
			@HoSoDangKy_ID,
			@NgayKhaiBao,
			@SoLuongChungTuKemTheo,
			@GhiChu
		)		
	END

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_Load]    Script Date: 03/13/2013 08:42:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HSTKBoSung_Load]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HoSoDangKy_ID],
	[NgayKhaiBao],
	[SoLuongChungTuKemTheo],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_HSTKBoSung]
WHERE
	[ID] = @ID

GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_SelectAll]    Script Date: 03/13/2013 08:42:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HSTKBoSung_SelectAll]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HoSoDangKy_ID],
	[NgayKhaiBao],
	[SoLuongChungTuKemTheo],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_HSTKBoSung]	


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_SelectBy_HoSoDangKy_ID]    Script Date: 03/13/2013 08:42:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HSTKBoSung_SelectBy_HoSoDangKy_ID]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_SelectBy_HoSoDangKy_ID]
	@HoSoDangKy_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[HoSoDangKy_ID],
	[NgayKhaiBao],
	[SoLuongChungTuKemTheo],
	[GhiChu]
FROM
	[dbo].[t_KDT_SXXK_HSTKBoSung]
WHERE
	[HoSoDangKy_ID] = @HoSoDangKy_ID


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_SelectDynamic]    Script Date: 03/13/2013 08:42:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HSTKBoSung_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[HoSoDangKy_ID],
	[NgayKhaiBao],
	[SoLuongChungTuKemTheo],
	[GhiChu]
FROM [dbo].[t_KDT_SXXK_HSTKBoSung] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL


GO

/****** Object:  StoredProcedure [dbo].[p_KDT_SXXK_HSTKBoSung_Update]    Script Date: 03/13/2013 08:42:37 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_SXXK_HSTKBoSung_Update]
-- Database: ECS_TQDT_SXXK_V4
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, March 12, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_SXXK_HSTKBoSung_Update]
	@ID bigint,
	@HoSoDangKy_ID bigint,
	@NgayKhaiBao datetime,
	@SoLuongChungTuKemTheo int,
	@GhiChu nvarchar(150)
AS

UPDATE
	[dbo].[t_KDT_SXXK_HSTKBoSung]
SET
	[HoSoDangKy_ID] = @HoSoDangKy_ID,
	[NgayKhaiBao] = @NgayKhaiBao,
	[SoLuongChungTuKemTheo] = @SoLuongChungTuKemTheo,
	[GhiChu] = @GhiChu
WHERE
	[ID] = @ID


GO



UPDATE dbo.t_HaiQuan_Version SET [Version] = '7.4', [Date] = GETDATE(), Notes = N''