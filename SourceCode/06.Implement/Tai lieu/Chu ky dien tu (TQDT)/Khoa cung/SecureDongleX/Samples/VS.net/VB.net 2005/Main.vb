Option Explicit On
Friend Class frmMain
   Inherits System.Windows.Forms.Form

   Dim retcode, handl As Integer
   Dim block_index As Short
   Dim uid, hid As Integer
   Dim buffer As String
   Dim seed As String
   Dim stri As String
   Dim key512(512) As Char
   Dim tempKey As String
   Dim plainText As String
   Dim data As String
   Dim bufferLength As Object
   Dim startIndex As Short

   Private Sub cmdRead_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdRead.Click
      ' Read SecureDongle X with specified UID
      retcode = SDX_Find()
      If (retcode < 0) Then
         lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
      End If

      If (retcode = 0) Then
         lstOutput.Items.Add("No SecureDongle X plugged")
         Exit Sub
      Else
         lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
      End If

      stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
      uid = Val(stri)

      retcode = SDX_Open(1, uid, hid)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      Else
         lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
      End If

      handl = retcode
      seed = InputBox("Please input Read block index (0-4)", "Input")
      block_index = Val(seed)
      buffer = New String(Chr(0), 512)

      retcode = SDX_Read(handl, block_index, buffer)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      End If
      lstOutput.Items.Add("Read: " & (buffer))

      SDX_Close(handl)
   End Sub

   Private Sub cmdTransform_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdTransform.Click
      ' Transform data using specified SecureDongle X
      retcode = SDX_Find()
      If (retcode < 0) Then
         lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
      End If

      If (retcode = 0) Then
         lstOutput.Items.Add("No SecureDongle X plugged")
         Exit Sub
      Else
         lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
      End If

      stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
      uid = Val(stri)

      retcode = SDX_Open(1, uid, hid)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      Else
         lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
      End If

      handl = retcode
      stri = InputBox("Please input data (i.e. helloworld)", "Input", "SDXTransform Test")

      retcode = SDX_Transform(handl, Len(stri), stri)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      End If
      lstOutput.Items.Add("Transform result:  " & (stri))

      SDX_Close(handl)
   End Sub

   Private Sub cmdWrite_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWrite.Click
      ' Write to SecureDongle X with specified UID
      retcode = SDX_Find()
      If (retcode < 0) Then
         lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
      End If

      If (retcode = 0) Then
         lstOutput.Items.Add("No SecureDongle X plugged")
         Exit Sub
      Else
         lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
      End If

      stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
      uid = Val(stri)

      retcode = SDX_Open(1, uid, hid)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      Else
         lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
      End If

      handl = retcode
      seed = InputBox("Please input write block index (0-4)", "Input", "0")
      block_index = Val(seed)
      stri = InputBox("Please input data to be written (i.e. helloworld)", "Input", "SDX - Sample Data")
      retcode = SDX_Write(handl, block_index, stri)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      End If
      lstOutput.Items.Add("Write: " & (stri))

      SDX_Close(handl)
   End Sub

   Private Sub cmdEncrypt_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdEncrypt.Click
      ' Encrypt using RSA and store to SecureDongle X with specified UID
      retcode = SDX_Find()
      If (retcode < 0) Then
         lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
      End If

      If (retcode = 0) Then
         lstOutput.Items.Add("No SecureDongle X plugged")
         Exit Sub
      Else
         lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
      End If

      stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
      uid = Val(stri)

      retcode = SDX_Open(1, uid, hid)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      Else
         lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
      End If

      handl = retcode
      seed = InputBox("Please enter start index (0-2559)", "Input", "0")
      startIndex = Val(seed)
      data = InputBox("Please enter data to encrypt", "Input", "SDX Test RSA Encrypt")
      bufferLength = Len(data)
      tempKey = New String(Chr(0), 512)
      retcode = SDX_RSAEncrypt(handl, startIndex, data, bufferLength, tempKey)
      key512 = tempKey.ToCharArray
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      End If
      lstOutput.Items.Add("Write success, size: " & bufferLength & ". Key to decrypt is stored inside key512 variable.")
      lstOutput.Items.Add("You can try reading the contents of the SDX")

      SDX_Close(handl)
   End Sub

   Private Sub cmdDecrypt_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdDecrypt.Click
      ' Read from SecureDongle X with specified UID and decrypt using RSA
      retcode = SDX_Find()
      If (retcode < 0) Then
         lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
      End If

      If (retcode = 0) Then
         lstOutput.Items.Add("No SecureDongle X plugged")
         Exit Sub
      Else
         lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
      End If

      stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
      uid = Val(stri)

      retcode = SDX_Open(1, uid, hid)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      Else
         lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
      End If

      handl = retcode
      seed = InputBox("Please enter start index (0-2559)", "Input", "0")
      startIndex = Val(seed)

      stri = InputBox("Please enter length of data to decrypt", "Input", bufferLength)
      bufferLength = Val(stri)

      plainText = New String(Chr(0), 512)
      retcode = SDX_RSADecrypt(handl, startIndex, plainText, bufferLength, key512)

      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      End If
      lstOutput.Items.Add("Decrypted Data: " & plainText)

      SDX_Close(handl)
   End Sub

   Private Sub cmdWriteMap_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdWriteMap.Click
      ' Write to SecureDongle X with specified UID
      ' This is a sample of simple Data Mapping to put plain text data not in order
      ' For example, in 5-byte data, byte 1 will be put in SDX memory location 10, byte 2 will be put in SDX memory location 1
      retcode = SDX_Find()
      If (retcode < 0) Then
         lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
      End If

      If (retcode = 0) Then
         lstOutput.Items.Add("No SecureDongle X plugged")
         Exit Sub
      Else
         lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
      End If

      stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
      uid = Val(stri)

      retcode = SDX_Open(1, uid, hid)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      Else
         lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
      End If

      handl = retcode
      seed = InputBox("Please input write block index(0-4)", "Input", "0")
      block_index = Val(seed)
      stri = InputBox("Please input data to be written (i.e. helloworld)", "Input", "SDX - Sample Map Encryption")

      lstOutput.Items.Add("Original data:  " & (stri))
      lstOutput.Items.Add("Length =  " & Len(stri))

      '****************** ENCRYPTION SAMPLE *******************
      Dim temp(512) As Byte
      Dim sze, i, indx As Short
      If Len(stri) > 512 Then
         lstOutput.Items.Add("Error: Data size max is 512")
         Exit Sub
      End If

      'Create a 512-byte Full Map with randomized content
      For i = 0 To 512
         If (Rnd() Mod 2) = 0 Then
            temp(i) = (Rnd() Mod 26) + 65
         Else
            temp(i) = (Rnd() Mod 26) + 97
         End If
      Next i

      'Calculate where to put the data, and put it there
      For i = 0 To Len(stri) - 1
         sze = i * (Len(stri) - 1)
         indx = sze Mod 512
         temp(indx) = Asc(Mid(stri, i + 1, 1))
      Next i
      stri = ""
      For i = 0 To 512
         stri = stri & Chr(temp(i))
      Next i
      '********************************************************

      'Write the Mapped data
      retcode = SDX_Write(handl, block_index, stri)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      End If
      lstOutput.Items.Add("Data Written:  " & (stri))

      SDX_Close(handl)
   End Sub

   Private Sub cmdReadMap_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdReadMap.Click
      ' Read from SecureDongle X with specified UID
      ' This is the sample to read the Mapped data
      retcode = SDX_Find()
      If (retcode < 0) Then
         lstOutput.Items.Add("Error Finding SecureDongle X: " & Hex(retcode))
      End If

      If (retcode = 0) Then
         lstOutput.Items.Add("No SecureDongle X plugged")
         Exit Sub
      Else
         lstOutput.Items.Add("Found SecureDongle X: " & Hex(retcode))
      End If

      stri = InputBox("Please input UID (i.e. 715400947)", "Input", "715400947")
      uid = Val(stri)

      retcode = SDX_Open(1, uid, hid)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      Else
         lstOutput.Items.Add("Succeeded Opening SecureDongle X, UID: " & (uid))
      End If

      Dim datasize As Short
      handl = retcode
      seed = InputBox("Please input Read block index (0-4)", "Input")
      block_index = Val(seed)
      seed = InputBox("Please enter size of the data to be read (1-512)", "Input")
      datasize = Val(seed)
      If datasize < 1 Or datasize > 512 Then
         MsgBox("Data size wrong")
         Exit Sub
      End If

      buffer = New String(Chr(0), 512)
      retcode = SDX_Read(handl, block_index, buffer)
      If (retcode < 0) Then
         lstOutput.Items.Add("Error: " & Hex(retcode))
         Exit Sub
      End If

      '******************* DECRYPTION **********************
      Dim buff As String
      Dim sze, i, indx As Short
      buff = ""
      For i = 0 To datasize - 1
         sze = i * (datasize - 1)
         indx = sze Mod 512
         buff = buff & Mid(buffer, indx + 1, 1)
      Next i
      '*****************************************************

      lstOutput.Items.Add("Read:  " & (buff))

      SDX_Close(handl)
   End Sub
End Class