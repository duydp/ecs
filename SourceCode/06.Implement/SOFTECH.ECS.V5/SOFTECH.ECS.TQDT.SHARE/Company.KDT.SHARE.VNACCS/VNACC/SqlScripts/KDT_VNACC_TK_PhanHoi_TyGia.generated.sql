-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Insert]
	@Master_ID bigint,
	@MaTTTyGiaTinhThue varchar(3),
	@TyGiaTinhThue numeric(13, 4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
(
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
)
VALUES 
(
	@Master_ID,
	@MaTTTyGiaTinhThue,
	@TyGiaTinhThue
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Update]
	@ID bigint,
	@Master_ID bigint,
	@MaTTTyGiaTinhThue varchar(3),
	@TyGiaTinhThue numeric(13, 4)
AS

UPDATE
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
SET
	[Master_ID] = @Master_ID,
	[MaTTTyGiaTinhThue] = @MaTTTyGiaTinhThue,
	[TyGiaTinhThue] = @TyGiaTinhThue
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@MaTTTyGiaTinhThue varchar(3),
	@TyGiaTinhThue numeric(13, 4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] 
		SET
			[Master_ID] = @Master_ID,
			[MaTTTyGiaTinhThue] = @MaTTTyGiaTinhThue,
			[TyGiaTinhThue] = @TyGiaTinhThue
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
		(
			[Master_ID],
			[MaTTTyGiaTinhThue],
			[TyGiaTinhThue]
		)
		VALUES 
		(
			@Master_ID,
			@MaTTTyGiaTinhThue,
			@TyGiaTinhThue
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
FROM [dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Thursday, October 24, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_TK_PhanHoi_TyGium_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[MaTTTyGiaTinhThue],
	[TyGiaTinhThue]
FROM
	[dbo].[t_KDT_VNACC_TK_PhanHoi_TyGia]	

GO

