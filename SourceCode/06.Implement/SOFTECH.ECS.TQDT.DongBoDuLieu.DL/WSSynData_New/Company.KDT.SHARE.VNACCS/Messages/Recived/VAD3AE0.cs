﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS.Messages.Recived
{
    public partial class VAD3AE0 : BasicVNACC
    {
        public List<VAD3AE0_HANG> HangMD { get; set; }
        public void LoadVAD3AE0(string strResult)
        {
            try
            {

                this.GetObject<VAD3AE0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD3AE0, false, VAD3AE0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAD3AE0.TongSoByte);
                while (true)
                {
                    VAD3AE0_HANG vad1agHang = new VAD3AE0_HANG();
                    vad1agHang.GetObject<VAD3AE0_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAD3AE0_HANG", false, VAD3AE0_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAD3AE0_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAD3AE0_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAD3AE0_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAD3AE0_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }
    }
}
