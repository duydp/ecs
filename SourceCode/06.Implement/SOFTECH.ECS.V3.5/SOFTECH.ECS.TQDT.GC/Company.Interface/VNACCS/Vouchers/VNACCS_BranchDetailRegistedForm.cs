﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface.VNACCS.Vouchers
{
    public partial class VNACCS_BranchDetailRegistedForm : Company.Interface.BaseForm
    {
        public KDT_VNACCS_BranchDetail branchDetail = new KDT_VNACCS_BranchDetail();
        public VNACCS_BranchDetailRegistedForm()
        {
            InitializeComponent();
        }

        private void VNACCS_BranchDetailRegistedForm_Load(object sender, EventArgs e)
        {
            btnSearch_Click(null, null);
        }
        private string GetSearchWhere()
        {
            try
            {
                string where = " BillOfLadings_Details_ID  IN (SELECT ID FROM dbo.t_KDT_VNACCS_BillOfLadings_Details WHERE ID IN (SELECT ID FROM dbo.t_KDT_VNACCS_BillOfLading WHERE TrangThaiXuLy IN (0,1))) ";
                if (!String.IsNullOrEmpty(txtSoVanDonGoc.Text.ToString()))
                    where += " AND SoVanDon LIKE '%" + txtSoVanDonGoc.Text.ToString() + "%'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return " 1=1";
            }
        }
        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                dgList.Refetch();
                dgList.DataSource = KDT_VNACCS_BranchDetail.SelectCollectionDynamic(GetSearchWhere(),"");
                dgList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    branchDetail = (KDT_VNACCS_BranchDetail)e.Row.DataRow;
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
