﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class TransportEquipments
    {
        /// <summary>
        /// Product
        /// </summary>
        [XmlElement("TransportEquipment")]
        public List<TransportEquipment> TransportEquipment { get; set; }

    }
}
