﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_ChuyenCuaKhau_HangHoaForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiVanChuyen TKVC;
        public KDT_VNACC_TKVC_VanDon VanDon = null;
        public VNACC_ChuyenCuaKhau_HangHoaForm()
        {
            InitializeComponent();
            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EDeclarationOnTransportation.OLA.ToString());
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        bool isAddNew = false;
        private void btnLuu_Click(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;


                if (VanDon == null)
                {
                    VanDon = new KDT_VNACC_TKVC_VanDon();
                    isAddNew = true;
                }
                GetVanDon();
                if (isAddNew)
                    TKVC.VanDonCollection.Add(VanDon);
                grList.DataSource = TKVC.VanDonCollection;
                grList.Refetch();
                VanDon = new KDT_VNACC_TKVC_VanDon();
                //SetHangMD();
                VanDon = null;
                txtSoLuong.Text = "0";

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }
        private void GetVanDon()
        {
            try
            {
                VanDon.SoVanDon = txtSoVanDon.Text;
                VanDon.NgayPhatHanhVD = clcNgayPhatHanhVD.Value;
                VanDon.MoTaHangHoa = txtMoTaHangHoa.Text;
                VanDon.MaHS = ctrMaHS.Code;
                VanDon.KyHieuVaSoHieu = txtKyHieuVaSoHieu.Text;
                VanDon.NgayNhapKhoHQLanDau = clcNgayNhapKhoHQLanDau.Value;
                VanDon.PhanLoaiSanPhan = txtPhanLoaiSanPhan.Text;
                VanDon.MaNuocSanXuat = ctrMaNuocSanXuat.Code;
                VanDon.MaDiaDiemXuatPhatVC = ctrMaDiaDiemXuatPhatVC.Code;
                VanDon.MaDiaDiemDichVC = ctrMaDiaDiemDichVC.Code;
                VanDon.LoaiHangHoa = ctrLoaiHangHoa.Code;
                VanDon.MaPhuongTienVC = txtMaPhuongTienVC.Text;
                VanDon.TenPhuongTienVC = txtTenPhuongTienVC.Text;
                VanDon.NgayHangDuKienDenDi = clcNgayHangDuKienDenDi.Value;
                VanDon.MaNguoiNhapKhau = txtMaNguoiNhapKhau.Text;
                VanDon.TenNguoiNhapKhau = txtTenNguoiNhapKhau.Text;
                VanDon.DiaChiNguoiNhapKhau = txtDiaChiNguoiNhapKhau.Text;
                VanDon.MaNguoiXuatKhua = txtMaNguoiXuatKhua.Text;
                VanDon.TenNguoiXuatKhau = txtTenNguoiXuatKhau.Text;
                VanDon.DiaChiNguoiXuatKhau = txtDiaChiNguoiXuatKhau.Text;
                VanDon.MaNguoiUyThac = txtMaNguoiUyThac.Text;
                VanDon.TenNguoiUyThac = txtTenNguoiUyThac.Text;
                VanDon.DiaChiNguoiUyThac = txtDiaChiNguoiUyThac.Text;
                VanDon.MaVanBanPhapLuat1 = ctrMaVanBanPhapLuat1.Code;
                VanDon.MaVanBanPhapLuat2 = ctrMaVanBanPhapLuat2.Code;
                VanDon.MaVanBanPhapLuat3 = ctrMaVanBanPhapLuat3.Code;
                VanDon.MaVanBanPhapLuat4 = ctrMaVanBanPhapLuat4.Code;
                VanDon.MaVanBanPhapLuat5 = ctrMaVanBanPhapLuat5.Code;
                VanDon.SoLuong = Convert.ToDecimal(txtSoLuong.Value);
                VanDon.MaDVTSoLuong = ctrMaDVTSoLuong.Code;
                VanDon.TongTrongLuong = Convert.ToDecimal(txtTongTrongLuong.Value);
                VanDon.MaDVTTrongLuong = ctrMaDVTTrongLuong.Code;
                VanDon.TheTich = Convert.ToDecimal(txtTheTich.Value);
                VanDon.MaDVTTheTich = ctrMaDVTTheTich.Code;
                VanDon.TriGia = Convert.ToDecimal(txtTriGia.Text);
                VanDon.MaDVTTriGia = ctrMaDVTTriGia.Code;
                VanDon.MaDanhDauDDKH1 = ctrMaDanhDauDDKH1.Code;
                VanDon.MaDanhDauDDKH2 = ctrMaDanhDauDDKH2.Code;
                VanDon.MaDanhDauDDKH3 = ctrMaDanhDauDDKH3.Code;
                VanDon.MaDanhDauDDKH4 = ctrMaDanhDauDDKH4.Code;
                VanDon.MaDanhDauDDKH5 = ctrMaDanhDauDDKH5.Code;
                VanDon.SoGiayPhep = txtSoGiayPhep.Text;
                VanDon.NgayCapPhep = clcNgayCapPhep.Value;
                VanDon.NgayHetHanCapPhep = clcNgayHetHanCapPhep.Value;
                VanDon.GhiChu = txtGhiChu.Text;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }


        }
        private void SetVanDon()
        {
            try
            {
                txtSoVanDon.Text = VanDon.SoVanDon;
                clcNgayPhatHanhVD.Value = VanDon.NgayPhatHanhVD;
                txtMoTaHangHoa.Text = VanDon.MoTaHangHoa;
                ctrMaHS.Code = VanDon.MaHS;
                txtKyHieuVaSoHieu.Text = VanDon.KyHieuVaSoHieu;
                clcNgayNhapKhoHQLanDau.Value = VanDon.NgayNhapKhoHQLanDau;
                txtPhanLoaiSanPhan.Text = VanDon.PhanLoaiSanPhan;
                ctrMaNuocSanXuat.Code = VanDon.MaNuocSanXuat;
                ctrMaDiaDiemXuatPhatVC.Code = VanDon.MaDiaDiemXuatPhatVC;
                ctrMaDiaDiemDichVC.Code = VanDon.MaDiaDiemDichVC;
                ctrLoaiHangHoa.Code = VanDon.LoaiHangHoa;
                txtMaPhuongTienVC.Text = VanDon.MaPhuongTienVC;
                txtTenPhuongTienVC.Text = VanDon.TenPhuongTienVC;
                clcNgayHangDuKienDenDi.Value = VanDon.NgayHangDuKienDenDi;
                txtMaNguoiNhapKhau.Text = VanDon.MaNguoiNhapKhau;
                txtTenNguoiNhapKhau.Text = VanDon.TenNguoiNhapKhau;
                txtDiaChiNguoiNhapKhau.Text = VanDon.DiaChiNguoiNhapKhau;
                txtMaNguoiXuatKhua.Text = VanDon.MaNguoiXuatKhua;
                txtTenNguoiXuatKhau.Text = VanDon.TenNguoiXuatKhau;
                txtDiaChiNguoiXuatKhau.Text = VanDon.DiaChiNguoiXuatKhau;
                txtMaNguoiUyThac.Text = VanDon.MaNguoiUyThac;
                txtTenNguoiUyThac.Text = VanDon.TenNguoiUyThac;
                txtDiaChiNguoiUyThac.Text = VanDon.DiaChiNguoiUyThac;
                ctrMaVanBanPhapLuat1.Code = VanDon.MaVanBanPhapLuat1;
                ctrMaVanBanPhapLuat2.Code = VanDon.MaVanBanPhapLuat2;
                ctrMaVanBanPhapLuat3.Code = VanDon.MaVanBanPhapLuat3;
                ctrMaVanBanPhapLuat4.Code = VanDon.MaVanBanPhapLuat4;
                ctrMaVanBanPhapLuat5.Code = VanDon.MaVanBanPhapLuat5;
                txtSoLuong.Value = VanDon.SoLuong;
                ctrMaDVTSoLuong.Code = VanDon.MaDVTSoLuong;
                txtTongTrongLuong.Value = VanDon.TongTrongLuong;
                ctrMaDVTTrongLuong.Code = VanDon.MaDVTTrongLuong;
                txtTheTich.Value = VanDon.TheTich;
                ctrMaDVTTheTich.Code = VanDon.MaDVTTheTich;
                txtTriGia.Value = VanDon.TriGia;
                ctrMaDVTTriGia.Code = VanDon.MaDVTTriGia;
                ctrMaDanhDauDDKH1.Code = VanDon.MaDanhDauDDKH1;
                ctrMaDanhDauDDKH2.Code = VanDon.MaDanhDauDDKH2;
                ctrMaDanhDauDDKH3.Code = VanDon.MaDanhDauDDKH3;
                ctrMaDanhDauDDKH4.Code = VanDon.MaDanhDauDDKH4;
                ctrMaDanhDauDDKH5.Code = VanDon.MaDanhDauDDKH5;
                txtSoGiayPhep.Text = VanDon.SoGiayPhep;
                clcNgayCapPhep.Value = VanDon.NgayCapPhep;
                clcNgayHetHanCapPhep.Value = VanDon.NgayHetHanCapPhep;
                txtGhiChu.Text = VanDon.GhiChu;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }


        }

        private void VNACC_ChuyenCuaKhau_HangHoaForm_Load(object sender, EventArgs e)
        {

            try
            {
                Cursor = Cursors.WaitCursor;
                this.SetIDControl();
                grList.DataSource = TKVC.VanDonCollection;
                if (TKVC.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc || TKVC.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
                {
                    btnLuu.Enabled = false;
                    btnXoa.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
            
            //ctr
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_VNACC_TKVC_VanDon> ItemColl = new List<KDT_VNACC_TKVC_VanDon>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn có muốn xóa hàng này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACC_TKVC_VanDon)i.GetRow().DataRow);

                        }

                    }
                    foreach (KDT_VNACC_TKVC_VanDon item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        TKVC.VanDonCollection.Remove(item);

                    }

                    grList.DataSource = TKVC.VanDonCollection;
                    try
                    {
                        grList.Refetch();
                    }
                    catch
                    {
                        grList.Refresh();
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                try
                {
                    Cursor = Cursors.WaitCursor;

                    VanDon = (KDT_VNACC_TKVC_VanDon)grList.CurrentRow.DataRow;
                    SetVanDon();
                    isAddNew = false;

                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                }
                finally { Cursor = Cursors.Default; }
            }
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;
                txtSoVanDon.Tag = "CG_";
                clcNgayPhatHanhVD.TagName = "DB_";
                txtMoTaHangHoa.Tag = "CM_";
                ctrMaHS.TagName = "HS_";
                txtKyHieuVaSoHieu.Tag = "MR_";
                clcNgayNhapKhoHQLanDau.TagName = "IS_";
                txtPhanLoaiSanPhan.Tag = "RM_";
                ctrMaNuocSanXuat.TagCode = "OR_";
                ctrMaDiaDiemXuatPhatVC.TagName = "LP_";
                ctrMaDiaDiemDichVC.TagName = "PA_";
                ctrLoaiHangHoa.TagName = "TM_";
                txtMaPhuongTienVC.Tag = "TE_";
                txtTenPhuongTienVC.Tag = "SN_";
                clcNgayHangDuKienDenDi.TagName = "DD_";
                txtMaNguoiNhapKhau.Tag = "IM_";
                txtTenNguoiNhapKhau.Tag = "IN_";
                txtDiaChiNguoiNhapKhau.Tag = "IA_";
                txtMaNguoiXuatKhua.Tag = "EM_";
                txtTenNguoiXuatKhau.Tag = "EN_";
                txtDiaChiNguoiXuatKhau.Tag = "EA_";
                txtMaNguoiUyThac.Tag = "KM_";
                txtTenNguoiUyThac.Tag = "KN_";
                txtDiaChiNguoiUyThac.Tag = "KA_";
                ctrMaVanBanPhapLuat1.TagName = "OA_";
                ctrMaVanBanPhapLuat2.TagName = "OA_";
                ctrMaVanBanPhapLuat3.TagName = "OA_";
                ctrMaVanBanPhapLuat4.TagName = "OA_";
                ctrMaVanBanPhapLuat5.TagName = "OA_";
                txtSoLuong.Tag = "PC_";
                ctrMaDVTSoLuong.TagName = "PU_";
                txtTongTrongLuong.Tag = "GW_";
                ctrMaDVTTrongLuong.TagName = "WU_";
                txtTheTich.Tag = "VO_";
                ctrMaDVTTheTich.TagName = "VU_";
                txtTriGia.Tag = "PR_";
                ctrMaDVTTriGia.TagName = "RT_";
                ctrMaDanhDauDDKH1.TagName = "RA_";
                ctrMaDanhDauDDKH2.TagName = "RA_";
                ctrMaDanhDauDDKH3.TagName = "RA_";
                ctrMaDanhDauDDKH4.TagName = "RA_";
                ctrMaDanhDauDDKH5.TagName = "RA_";
                txtSoGiayPhep.Tag = "PM_";
                clcNgayCapPhep.TagName = "PD_";
                clcNgayHetHanCapPhep.TagName = "EP_";
                txtGhiChu.Tag = "NT_";





            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }


    }
}
