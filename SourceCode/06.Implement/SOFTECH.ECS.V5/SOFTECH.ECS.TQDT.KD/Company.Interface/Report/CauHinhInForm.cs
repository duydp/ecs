﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KD.BLL.KDT;

namespace Company.Interface.Report
{
    public partial class CauHinhInForm : Company.Interface.BaseForm
    {
        public CauHinhInForm()
        {
            InitializeComponent();
        }
        public Label lblDonGiaNT { get { return _lblDonGiaNT; } }
        public Label lblTriGiaNT { get { return _lblTriGiaNT; } }
        public Label lblLuongHMD { get { return _lblSoLuongHMD; } }
        public Label lblFontToKhai { get { return _lblFontToKhai; } }
        public List<Label> Listlable = new List<Label>();
        private void CauHinhInForm_Load(object sender, EventArgs e)
        {
            try
            {

                if (Listlable.Count > 0)
                {
                    timer1.Interval = 500;
                    timer1.Start();
                }
                GlobalSettings.KhoiTao_GiaTriMacDinh();
                txtDinhMuc.Value = GlobalSettings.SoThapPhan.DinhMuc;
                txtLuongNPL.Value = GlobalSettings.SoThapPhan.LuongNPL;
                txtLuongSP.Value = GlobalSettings.SoThapPhan.LuongSP;
                txtTriGiaNT.Value = GlobalSettings.SoThapPhan.TriGiaNT;
                txtDonGiaNT.Value = GlobalSettings.SoThapPhan.DonGiaNT;
                try
                {
                    txtSoLuongHMD.Value = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTPSoLuongHMD");
                    txtTrongLuongTK.Value = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("SoTPTrongLuongTK");
                    
                    txtFontTenHang.Value = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang");
                    txtfontToKhai.Value = Convert.ToDouble(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontReport"));
                    chkKhongDungBKContainer.Checked = Convert.ToBoolean(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsKhongDungBangKeCont","False"));
                }
                catch (Exception ex)
                {
                    ShowMessage("Lỗi đọc thông tin cấu hình số thập phân.", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
//                     txtSoLuongHMD.Value = 0;
//                     txtTrongLuongTK.Value = 0;
//                     txtFontTenHang.Value = 0;
//                     txtfontToKhai.Value = 0;
                }

                string delay = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("TimeDelay");
                numTimeDelay.Value = Convert.ToDecimal(delay);
                chkInCV3742.Checked = Company.KDT.SHARE.Components.Globals.InCV3742;
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                HeThongPhongKhaiCollection htpkCollection = new HeThongPhongKhaiCollection();
                GlobalSettings.SoThapPhan.DinhMuc = (int)txtDinhMuc.Value;
                HeThongPhongKhai htpk = new HeThongPhongKhai();
                htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                htpk.PassWord = "";
                htpk.Role = 0;
                htpk.Key_Config = "DinhMuc";
                htpk.Value_Config = txtDinhMuc.Text;
                htpkCollection.Add(htpk);

                GlobalSettings.SoThapPhan.LuongNPL = (int)txtLuongNPL.Value;
                htpk = new HeThongPhongKhai();
                htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                htpk.PassWord = "";
                htpk.Role = 0;
                htpk.Key_Config = "LuongNPL";
                htpk.Value_Config = txtLuongNPL.Text;
                htpkCollection.Add(htpk);

                GlobalSettings.SoThapPhan.LuongSP = (int)txtLuongSP.Value;
                htpk = new HeThongPhongKhai();
                htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                htpk.PassWord = "";
                htpk.Role = 0;
                htpk.Key_Config = "LuongSP";
                htpk.Value_Config = txtLuongSP.Text;
                htpkCollection.Add(htpk);

                try
                {
                    GlobalSettings.SoThapPhan.SoLuongHMD = (int)txtSoLuongHMD.Value;
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTPSoLuongHMD", txtSoLuongHMD.Value);
                    htpk = new HeThongPhongKhai();
                    htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                    htpk.PassWord = "";
                    htpk.Role = 0;
                    htpk.Key_Config = "SoLuongTK";
                    htpk.Value_Config = txtSoLuongHMD.Text;
                    htpkCollection.Add(htpk);

                    //DATLMQ bổ sung cấu hình số thập phân trọng lượng ở ngoài tờ khai 14/03/2011
                    GlobalSettings.SoThapPhan.TrongLuongHangTK = (int)txtTrongLuongTK.Value;
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTPTrongLuongTK", txtTrongLuongTK.Value);
                    htpk = new HeThongPhongKhai();
                    htpk.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    htpk.TenDoanhNghiep = GlobalSettings.TEN_DON_VI;
                    htpk.PassWord = "";
                    htpk.Role = 0;
                    htpk.Key_Config = "TrongLuongTK";
                    htpk.Value_Config = txtTrongLuongTK.Text;
                    htpkCollection.Add(htpk);

                    //DATLMQ bổ sung Cấu hình font tên hàng hóa 18/05/2011
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontTenHang", txtFontTenHang.Text);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("FontReport", txtfontToKhai.Text);

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TimeDelay", numTimeDelay.Value.ToString());

                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("SoTPTrongLuongTK", txtTrongLuongTK.Value);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("TriGiaNT", txtTriGiaNT.Value);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("DonGiaNT", txtDonGiaNT.Value);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("IsKhongDungBangKeCont", chkKhongDungBKContainer.Checked.ToString());
                    Company.KDT.SHARE.Components.Globals.TriGiaNT = Convert.ToInt32(txtTriGiaNT.Value);
                    Company.KDT.SHARE.Components.Globals.DonGiaNT = Convert.ToInt32(txtDonGiaNT.Value);
                    Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("InCV3742", chkInCV3742.Checked.ToString());
                    Company.KDT.SHARE.Components.Globals.InCV3742 = chkInCV3742.Checked;
                    GlobalSettings.RefreshKey();

                }
                catch
                {
                    ShowMessage("Lỗi: Thiếu một số key trong file config", false);
                    return;
                }

                try
                {
                    htpk.InsertUpdate(htpkCollection);
                    ShowMessage("Lưu thông tin cấu hình thành công.", false);
                }
                catch
                {
                    ShowMessage("Có lỗi khi lưu thông tin cấu hình.", false);
                    return;
                }

                this.Close();
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            foreach (Label lable in Listlable)
            {
                lable.ForeColor = lable.ForeColor == Color.Black ? Color.Red : Color.Black;
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

     
    }
}

