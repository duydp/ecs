﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Services;

namespace WSSynData
{
    /// <summary>
    /// Summary description for Service
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class Service : System.Web.Services.WebService
    {
        #region USER AGENT - ĐẠI LÝ

        /// <summary>
        /// Kiểm tra thông tin người dùng Đại lý.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <returns></returns>
        [WebMethod]
        public Company.KDT.SHARE.Components.DaiLy LoginDaiLy(string userNameLogin, string passWordLogin)
        {
            Company.KDT.SHARE.Components.DaiLy user = Company.KDT.SHARE.Components.DaiLy.Load(userNameLogin, passWordLogin);
            return user;
        }

        /// <summary>
        /// Danh sách người dùng Đại lý
        /// </summary>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public List<Company.KDT.SHARE.Components.DaiLy> SelectUserDaiLy(string maDoanhNghiep)
        {
            return Company.KDT.SHARE.Components.DaiLy.SelectUserDaiLy(maDoanhNghiep);
        }

        [WebMethod]
        public List<Company.KDT.SHARE.Components.DaiLy> SelectUserDaiLyAll()
        {
            return (List<Company.KDT.SHARE.Components.DaiLy>)Company.KDT.SHARE.Components.DaiLy.SelectCollectionAll();

        }

        /// <summary>
        /// Tạo mới người dùng Đại lý
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="tenNguoiDung"></param>
        /// <param name="matKhau"></param>
        /// <param name="hoVaTen"></param>
        /// <param name="moTa"></param>
        /// <param name="laQuanTri"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public int InsertUpdateUserDaiLy(string userNameLogin, string passWordLogin, string tenNguoiDung, string matKhau, string hoVaTen, string moTa, bool laQuanTri, string maDoanhNghiep)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
                return Company.KDT.SHARE.Components.DaiLy.InsertUpdateUserDaiLy(tenNguoiDung, matKhau, hoVaTen, moTa, laQuanTri, maDoanhNghiep);
            else
                return 0;
        }

        /// <summary>
        /// Xóa người dùng Đại lý
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="tenNguoiDung"></param>
        /// <param name="matKhau"></param>
        /// <returns></returns>
        [WebMethod]
        public bool DeleteUserDaiLy(string userNameLogin, string passWordLogin, string tenNguoiDung, string matKhau)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
                return Company.KDT.SHARE.Components.DaiLy.DeleteUserDaiLy(tenNguoiDung, matKhau);
            else
                return false;
        }

        /// <summary>
        /// Kiểm tra tên người dùng đã tồn tại?.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [WebMethod]
        public bool CheckUserName(string userName)
        {
            Company.QuanTri.User user = new Company.QuanTri.User();

            return user.CheckUserName(userName);
        }

        /// <summary>
        /// Tạo mới cấu hình số tờ khai quy định Đại lý phải tải lên server?.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [WebMethod]
        public int InsertUpdateCauHinhDaiLy(string maDoanhNghiep, string tenDoanhNghiep, string key, string value, string ghiChu)
        {
            return Company.KDT.SHARE.Components.CauHinhDaiLy.InsertUpdateCauHinh(maDoanhNghiep, tenDoanhNghiep, key, value, ghiChu);
        }

        /// <summary>
        /// Kiểm tra cấu hình số tờ khai quy định Đại lý phải tải lên server?.
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [WebMethod]
        public string CheckCauHinhSoToKhai(string maDoanhNghiep, string key)
        {
            Company.KDT.SHARE.Components.CauHinhDaiLy cauhinh = new Company.KDT.SHARE.Components.CauHinhDaiLy();

            cauhinh = Company.KDT.SHARE.Components.CauHinhDaiLy.Load(maDoanhNghiep, key);

            if (cauhinh != null)
            {
                return string.IsNullOrEmpty(cauhinh.Value) != true ? cauhinh.Value : "";
            }

            return "";
        }

        #endregion

        #region TỜ KHAI

        /// <summary>
        /// Gửi dữ liệu một tờ khai và hàng mậu dịch, các thông tin liên quan đến tờ khai.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="TKMD"></param>
        /// <param name="overwrite"></param>
        /// <returns></returns>
        [WebMethod]
        public string Send(string userNameLogin, string passWordLogin, ref Company.BLL.KDT.ToKhaiMauDich TKMD)
        {
            string error = "";
            //Neu login thanh cong
            Company.KDT.SHARE.Components.DaiLy userDaiLy = LoginDaiLy(userNameLogin, passWordLogin);

            if (userDaiLy != null)
            {
                try
                {
                    //Insert to khai va hang mau dich
                    TKMD.InsertUpdateFullDaiLy();
                    error = "";
                }
                catch (Exception ex) { error = ex.Message; }

                //Them vao bang t_DongBoDuLieu_Track
                Company.KDT.SHARE.Components.Track newTrack = new Company.KDT.SHARE.Components.Track();
                newTrack.MaDoanhNghiep = TKMD.MaDoanhNghiep;
                newTrack.MaHaiQuan = TKMD.MaHaiQuan;
                newTrack.SoToKhai = TKMD.SoToKhai;
                newTrack.NamDangKy = TKMD.NamDK;
                newTrack.MaLoaiHinh = TKMD.MaLoaiHinh;
                newTrack.TKMD_GUIDSTR = TKMD.GUIDSTR;
                //Ghi log Error neu co.
                newTrack.GhiChu = error;
                //1: Thanh cong; 0: Khong thanh cong.
                newTrack.TrangThaiDongBoTaiLen = error.Length == 0 ? 1 : 0;
                newTrack.TrangThaiDongBoTaiXuong = 0;
                //ID user dai ly gui du lieu den Server.
                newTrack.IDUserDaiLy = userDaiLy != null ? userDaiLy.ID : 0;
                newTrack.UserName = userDaiLy != null ? userDaiLy.USER_NAME : "";
                newTrack.NgayDongBo = DateTime.Now;

                newTrack.InsertUpdateBy();
            }

            return error;
        }

        /// <summary>
        /// Gửi dữ liệu danh sách tờ khai và hàng mậu dịch, các thông tin liên quan đến tờ khai.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="TKMD"></param>
        /// <param name="overwrite"></param>
        /// <returns></returns>
        [WebMethod]
        public string Sends(string userNameLogin, string passWordLogin, ref Company.BLL.KDT.ToKhaiMauDichCollection TKMDCollection)
        {
            string error = "";
            //Neu login thanh cong
            Company.KDT.SHARE.Components.DaiLy userDaiLy = LoginDaiLy(userNameLogin, passWordLogin);

            if (userDaiLy != null)
            {
                foreach (Company.BLL.KDT.ToKhaiMauDich item in TKMDCollection)
                {
                    try
                    {
                        //Insert to khai va hang mau dich
                        item.InsertUpdateFull();
                        error = "";
                    }
                    catch (Exception ex) { error = ex.Message; }

                    //Them vao bang t_DongBoDuLieu_Track
                    Company.KDT.SHARE.Components.Track newTrack = new Company.KDT.SHARE.Components.Track();
                    newTrack.MaDoanhNghiep = item.MaDoanhNghiep;
                    newTrack.MaHaiQuan = item.MaHaiQuan;
                    newTrack.SoToKhai = item.SoToKhai;
                    newTrack.NamDangKy = item.NamDK;
                    newTrack.MaLoaiHinh = item.MaLoaiHinh;
                    newTrack.TKMD_GUIDSTR = item.GUIDSTR;
                    //Ghi log Error neu co.
                    newTrack.GhiChu = error;
                    //1: Thanh cong; 0: Khong thanh cong.
                    newTrack.TrangThaiDongBoTaiLen = error.Length == 0 ? 1 : 0;
                    newTrack.TrangThaiDongBoTaiXuong = 0;
                    //ID user dai ly gui du lieu den Server.
                    newTrack.IDUserDaiLy = userDaiLy != null ? userDaiLy.ID : 0;
                    newTrack.UserName = userDaiLy != null ? userDaiLy.USER_NAME : "";
                    newTrack.NgayDongBo = DateTime.Now;

                    newTrack.Insert();

                    //Neu co loi xay ra -> Thoat khoi vong lap
                    if (error.Length > 0) break;
                }
            }

            return error;
        }

        /// <summary>
        /// Lấy thông tin danh sách tờ khai theo mã doanh nghiệp.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public Company.BLL.KDT.ToKhaiMauDichCollection Receives(string userNameLogin, string passWordLogin, string maDoanhNghiep, DateTime tuNgay, DateTime denNgay)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();
                foreach (Company.BLL.KDT.ToKhaiMauDich item in tkmd.SelectCollectionByDaiLy(maDoanhNghiep, tuNgay, denNgay))
                {

                    tkmd.LoadHMDCollection();
                    //Load Chung tu kem lien quan cua To khai.
                    //tkmd.LoadChungTuHaiQuan();
                }
            }

            return null;
        }

        /// <summary>
        /// Lấy thông tin danh sách tờ khai theo mã doanh nghiệp.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public Company.BLL.KDT.ToKhaiMauDichCollection ReceivesByDaiLy(string userNameLogin, string passWordLogin, string maDoanhNghiep, DateTime tuNgay, DateTime denNgay, int idUserDaiLy)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();

                return tkmd.SelectCollectionByDaiLy(maDoanhNghiep, tuNgay, denNgay, idUserDaiLy);
            }

            return null;
        }

        /// <summary>
        /// Lấy thông tin tờ khai theo mã doanh nghiệp.
        /// </summary>
        /// <param name="userNameLogin"></param>
        /// <param name="passWordLogin"></param>
        /// <param name="soToKhai"></param>
        /// <param name="namDangKy"></param>
        /// <param name="maLoaiHinh"></param>
        /// <param name="maHaiQuan"></param>
        /// <param name="maDoanhNghiep"></param>
        /// <returns></returns>
        [WebMethod]
        public Company.BLL.KDT.ToKhaiMauDich Receive(string userNameLogin, string passWordLogin, int soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                Company.BLL.KDT.ToKhaiMauDich tkmd = new Company.BLL.KDT.ToKhaiMauDich();
                tkmd.Load(maHaiQuan, maDoanhNghiep, soToKhai, namDangKy, maLoaiHinh);

                //Load Hang hoa mau dich
                tkmd.LoadHMDCollection();

                //Load Chung tu kem lien quan cua To khai.
                tkmd.LoadChungTuHaiQuan();

                return tkmd;
            }

            return null;
        }

        #endregion

        #region TRACK

        [WebMethod]
        public bool InsertUpdateTrack(string userNameLogin, string passWordLogin, Company.KDT.SHARE.Components.Track log)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                log.InsertUpdateBy();

                return true;
            }

            return false;
        }

        [WebMethod]
        public Company.KDT.SHARE.Components.Track SelectTrack(string userNameLogin, string passWordLogin, long soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                return Company.KDT.SHARE.Components.Track.Load(soToKhai, namDangKy, maLoaiHinh, maHaiQuan, maDoanhNghiep);
            }

            return null;
        }

        [WebMethod]
        public List<Company.KDT.SHARE.Components.Track> SelectTrackDynamic(string userNameLogin, string passWordLogin, string whereCondition, string orderBy)
        {
            //Neu login thanh cong
            if (LoginDaiLy(userNameLogin, passWordLogin) != null)
            {
                return (List<Company.KDT.SHARE.Components.Track>)Company.KDT.SHARE.Components.Track.SelectCollectionDynamic(whereCondition, orderBy);
            }

            return null;
        }

        #endregion
    }

}
