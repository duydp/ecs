﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
#if KD_V3
using Company.KDT.SHARE.Components.DuLieuChuan;
#endif
namespace Company.Interface.Controls
{
    public partial class ToKhaiProviders : UserControl
    {
        public event EventHandler ProviderChanged;

        public ToKhaiProviders()
        {
            InitializeComponent();
        }

        private void setDataToCboNguoiKB()
        {
            DataTable dt = new Company.QuanTri.User().SelectDynamic("USER_NAME <>'' and HO_TEN<>''", "").Tables[0];
            DataRow dr = dt.NewRow();
            dr["USER_NAME"] = -1;
            dr["HO_TEN"] = "--Tất cả--";
            dt.Rows.InsertAt(dr, 0);
            cbUserKB.DataSource = dt;
            cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
            cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
        }
        private void TimToKhai_Load(object sender, EventArgs e)
        {
            txtNamTiepNhan.Text = DateTime.Now.Year.ToString();
            setDataToCboNguoiKB();

        }

        public string NamTiepNhan
        {
            get { return txtNamTiepNhan.Text.Length > 0 ? txtNamTiepNhan.Text.Trim() : ""; }
        }

        string where = string.Empty;
        public string SearchString
        {
            get
            {
                where = string.Empty;
                if (txtSoTiepNhan.TextLength > 0)
                {
                    where += " AND SoTiepNhan like '%" + txtSoTiepNhan.Text + "%'";
                }

                if (txtSoTK.TextLength > 0)
                    where += " AND SoToKhai = " + txtSoTK.Text;

                if (chkDate.Checked)
                    where += string.Format(" AND NgayDangKy >= '{0}' AND NgayDangKy <= '{1}'", dtTuNgay.Value.ToString("yyyy-MM-dd"), dtDenNgay.Value.ToString("yyyy-MM-dd"));

                if (cbUserKB.SelectedValue != null && cbUserKB.SelectedIndex != 0 && cbUserKB.SelectedItem.Value.ToString() != "")
                {
                    where += @" And ID in (select ID_DK from t_KDT_SXXK_LogKhaiBao WHERE UserNameKhaiBao = '" + cbUserKB.SelectedItem.Value.ToString() + "' AND LoaiKhaiBao = 'TK')";
                }

                if (txtSoHopDong.TextLength > 0)
                {
                    where += " AND Sohopdong like '%" + txtSoHopDong.Text + "%'";
                }
                if (txtNamTiepNhan.TextLength > 0)
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Text.Trim();
                if (txtSoHoaDon.TextLength > 0)
                    where += " AND SoHoaDonThuongMai like '%" + txtSoHoaDon.Text + "%'";

                return where;
            }
            set
            {
                where = value;
            }

        }
        private void chkDate_CheckedChanged(object sender, EventArgs e)
        {
            dtTuNgay.Enabled = dtDenNgay.Enabled = chkDate.Checked;
        }

        private void Text_Changed(object sender, EventArgs e)
        {
            if (ProviderChanged != null)
            {
                ProviderChanged(this, null);
            }
        }
        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = false;
            if (e.KeyChar == '\r')
            {
                e.Handled = true;
                if (ProviderChanged != null)
                {
                    ProviderChanged(this, null);
                }
            }
        }
    }
}
