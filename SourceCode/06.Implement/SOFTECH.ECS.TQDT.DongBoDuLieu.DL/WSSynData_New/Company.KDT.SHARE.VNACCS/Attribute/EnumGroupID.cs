﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Company.KDT.SHARE.VNACCS
{
    public partial class EnumGroupID
    {
        public static readonly string None = "None";
    }
    public enum ETrangThaiPhanHoi
    {
        Download,
        Waiting,
        Stop,
        Error,
        NewMessages
    }
    public partial class EnumNghiepVu
    {
        public static readonly string IVA = "IVA";
        public static readonly string IVA01 = "IVA01";
        public static readonly string IIV = "IIV";
        public static readonly string IDA = "IDA";
        public static readonly string IDB = "IDB";
        public static readonly string IDC = "IDC";
        public static readonly string IDD = "IDD";
        public static readonly string IID = "IID";
        public static readonly string IDA01 = "IDA01";

        public static readonly string EDA = "EDA";
        public static readonly string EDB = "EDB";
        public static readonly string EDC = "EDC";
        public static readonly string EDD = "EDD";
        public static readonly string EDA01 = "EDA01";
        public static readonly string IEX = "IEX";

        public static readonly string MIC = "MIC";
        public static readonly string MID = "MID";
        public static readonly string MIE = "MIE";
        public static readonly string MEC = "MEC";
        public static readonly string MED = "MED";
        public static readonly string MEE = "MEE";

        public static readonly string SEA = "SEA";
        public static readonly string SEB = "SEB";
        public static readonly string ISE = "ISE";
        public static readonly string SAA = "SAA";
        public static readonly string SAB = "SAB";
        public static readonly string ISA = "ISA";
        public static readonly string SFA = "SFA";
        public static readonly string SFB = "SFB";
        public static readonly string ISF = "ISF";
        public static readonly string SMA = "SMA";
        public static readonly string SMB = "SMB";
        public static readonly string ISM = "ISM";


        public static readonly string OLA = "OLA";
        public static readonly string OLB = "OLB";
        public static readonly string OLC = "OLC";
        public static readonly string TEA = "TEA";
        public static readonly string COT = "COT";
        public static readonly string TIA = "TIA";
    }

    public partial class EnumThongBao
    {
        public static string GetTenNV(string maNV)
        {
            string temp = "";
            switch (maNV) 
            {
                case "VAD0AA0": 
                    temp = VAD0AA0;
                    break;
                case "VAD4190":
                    temp = VAD4190;
                    break;
                case "VAD1AC0":
                    temp = VAD1AC0;
                    break;
                case "VAD2AC0": 
                    temp = VAD2AC0; 
                    break;
                case "VAD3AC0": 
                    temp = VAD3AC0; 
                    break;
                case "VAD1BK0":
                    temp = VAD1BK0; 
                    break;
                case "VAD2BK0":
                    temp = VAD2BK0; 
                    break;
                case "VAD3BK0": 
                    temp = VAD3BK0; 
                    break;
                case "VAD1RC0": 
                    temp = VAD1RC0; 
                    break;
                case "VAD2RC0": temp = VAD2RC0; break;
                case "VAD3RC0": temp = VAD3RC0; break;
                case "VAD2RY0": temp = VAD2RY0; break;
                case "VAD3RY0": temp = VAD3RY0; break;
                case "VAD1AG0": temp = VAD1AG0; break;
                case "VAD1BJ0": temp = VAD1BJ0; break;
                case "VAD1RG0": temp = VAD1RG0; break;
                case "VAF8010": temp = VAF8010; break;
                case "VAF8030": temp = VAF8030; break;
                case "VAD0AB0": temp = VAD0AB0; break;
                case "VAD4240": temp = VAD4240; break;
                case "VAD2AE0": temp = VAD2AE0; break;
                case "VAD3AE0": temp = VAD3AE0; break;
                case "VAD2BM0": temp = VAD2BM0; break;
                case "VAD3BM0": temp = VAD3BM0; break;
                case "VAD2RE0": temp = VAD2RE0; break;
                case "VAD3RE0": temp = VAD3RE0; break;
                case "VAD8000": temp = VAD8000; break;
                case "VADBAC0": temp = VADBAC0; break;
                case "VAD1FC0": temp = VAD1FC0; break;
                case "VAD2FC0": temp = VAD2FC0; break;
                case "VAD3FC0": temp = VAD3FC0; break;
                case "VAD2AY0": temp = VAD2AY0; break;
                case "VAD3AY0": temp = VAD3AY0; break;
                case "VAD2BZ0": temp = VAD2BZ0; break;
                case "VAD3BZ0": temp = VAD3BZ0; break;
                case "VAD1FG0": temp = VAD1FG0; break;
                case "VAD6030": temp = VAD6030; break;
                case "VAD6040": temp = VAD6040; break;
                case "VAD2FE0": temp = VAD2FE0; break;
                case "VAD3FE0": temp = VAD3FE0; break;
                case "VAD2AG0": temp = VAD2AG0; break;
                case "VAD2RG0": temp = VAD2RG0; break;
                case "VAD2BJ0": temp = VAD2BJ0; break;
                case "VAD2FG0": temp = VAD2FG0; break;
                case "VAF8020": temp = VAF8020; break;
                case "VAD0AP0": temp = VAD0AP0; break;
                case "VAD0FP0": temp = VAD0FP0; break;
                case "VAE0LA0": temp = VAE0LA0; break;
                case "VAE4000": temp = VAE4000; break;
                case "VAE1LD0": temp = VAE1LD0; break;
                case "VAE2LD0": temp = VAE2LD0; break;
                case "VAE3LD0": temp = VAE3LD0; break;
                case "VAE1RD0": temp = VAE1RD0; break;
                case "VAE2RD0": temp = VAE2RD0; break;
                case "VAE3RD0": temp = VAE3RD0; break;
                case "VAE1LF0": temp = VAE1LF0; break;
                case "VAE1RF0": temp = VAE1RF0; break;
                case "VAF8040": temp = VAF8040; break;
                case "VAE0LB0": temp = VAE0LB0; break;
                case "VAE4110": temp = VAE4110; break;
                case "VAE2LE0": temp = VAE2LE0; break;
                case "VAE3LE0": temp = VAE3LE0; break;
                case "VAE2LY0": temp = VAE2LY0; break;
                case "VAE3LY0": temp = VAE3LY0; break;
                case "VAE2RE0": temp = VAE2RE0; break;
                case "VAE3RE0": temp = VAE3RE0; break;
                case "VAE8000": temp = VAE8000; break;
                case "VAE1JD0": temp = VAE1JD0; break;
                case "VAE2JD0": temp = VAE2JD0; break;
                case "VAE3JD0": temp = VAE3JD0; break;
                case "VAE1JF0": temp = VAE1JF0; break;
                case "VAE4940": temp = VAE4940; break;
                case "VAE2JE0": temp = VAE2JE0; break;
                case "VAE3JE0": temp = VAE3JE0; break;
                case "VAE2LF0": temp = VAE2LF0; break;
                case "VAE2RF0": temp = VAE2RF0; break;
                case "VAE2JF0": temp = VAE2JF0; break;
                case "VAE0LC0": temp = VAE0LC0; break;
                case "VAE0JC0": temp = VAE0JC0; break;
                case "VAD4870": temp = VAD4870; break;
                case "VAE4740": temp = VAE4740; break;
                case "VAD4910": temp = VAD4910; break;
                case "VAE4780": temp = VAE4780; break;
                case "VAL8000": temp = VAL8000; break;
                case "VAL8010": temp = VAL8010; break;
                case "VAL8020": temp = VAL8020; break;
                case "VAL8030": temp = VAL8030; break;
                case "VAL8040": temp = VAL8040; break;
                case "VAL8050": temp = VAL8050; break;
                case "VAL8060": temp = VAL8060; break;
                case "VAL8070": temp = VAL8070; break;
                case "VAD8010": temp = VAD8010; break;
                case "VAD8020": temp = VAD8020; break;
                case "VAE8010": temp = VAE8010; break;
                case "VAE8020": temp = VAE8020; break;
                case "VAL8080": temp = VAL8080; break;
                case "VAL8090": temp = VAL8090; break;
                case "VAL8100": temp = VAL8100; break;
                case "VAD8030": temp = VAD8030; break;
                case "VAD8040": temp = VAD8040; break;
                case "VAD8050": temp = VAD8050; break;
                case "VAD8060": temp = VAD8060; break;
                case "VAE8030": temp = VAE8030; break;
                case "VAE8040": temp = VAE8040; break;
                case "VAE8050": temp = VAE8050; break;
                case "VAE8060": temp = VAE8060; break;
                case "VAD8070": temp = VAD8070; break;
                case "VAE8070": temp = VAE8070; break;
                case "VAL8110": temp = VAL8110; break;
                case "VAL8120": temp = VAL8120; break;
                case "VAL8130": temp = VAL8130; break;
                case "VAL8140": temp = VAL8140; break;
                case "VAL8150": temp = VAL8150; break;
                case "VAL8160": temp = VAL8160; break;
                case "VAHP010": temp = VAHP010; break;
                case "VAHP020": temp = VAHP020; break;
                case "VAHC010": temp = VAHC010; break;
                case "VAHP030": temp = VAHP030; break;
                case "VAHP040": temp = VAHP040; break;
                case "VAHP050": temp = VAHP050; break;
                case "VAHP060": temp = VAHP060; break;
                case "VAHM020": temp = VAHM020; break;
                case "VAHM030": temp = VAHM030; break;
                case "VAJP010": temp = VAJP010; break;
                case "VAJP020": temp = VAJP020; break;
                case "VAJC010": temp = VAJC010; break;
                case "VAJC020": temp = VAJC020; break;
                case "VAJP030": temp = VAJP030; break;
                case "VAJP040": temp = VAJP040; break;
                case "VAJP050": temp = VAJP050; break;
                case "VAJP060": temp = VAJP060; break;
                case "VAJM010": temp = VAJM010; break;
                case "VAJM020": temp = VAJM020; break;
                case "VAJM030": temp = VAJM030; break;
                case "VAGP010": temp = VAGP010; break;
                case "VAGP020": temp = VAGP020; break;
                case "VAGC010": temp = VAGC010; break;
                case "VAGP030": temp = VAGP030; break;
                case "VAGP040": temp = VAGP040; break;
                case "VAGP050": temp = VAGP050; break;
                case "VAGP060": temp = VAGP060; break;
                case "VAGM020": temp = VAGM020; break;
                case "VAGM030": temp = VAGM030; break;
                case "VAGP110": temp = VAGP110; break;
                case "VAGP120": temp = VAGP120; break;
                case "VAGC110": temp = VAGC110; break;
                case "VAGP130": temp = VAGP130; break;
                case "VAGP140": temp = VAGP140; break;
                case "VAGP150": temp = VAGP150; break;
                case "VAGP160": temp = VAGP160; break;
                case "VAGM120": temp = VAGM120; break;
                case "VAGM130": temp = VAGM130; break;
                case "VAL0870": temp = VAL0870; break;
                case "VAL0880": temp = VAL0880; break;
                case "VAL0890": temp = VAL0890; break;
                case "VAL0940": temp = VAL0940; break;
                case "VAF8050": temp = VAF8050; break;
                case "VAF8060": temp = VAF8060; break;
                case "VAF8070": temp = VAF8070; break;
                case "VAF8080": temp = VAF8080; break;
                case "VAF5110": temp = VAF5110; break;
                case "VAU0060": temp = VAU0060; break;
                case "VA27XX ": temp = VA27XX; break;
                case "VAL0460": temp = VAL0460; break;
                case "VAL0020": temp = VAL0020; break;
                case "VA28XX ": temp = VA28XX; break;
                case "VAL0040": temp = VAL0040; break;
                case "VA29XX ": temp = VA29XX; break;
                case "VAL0110": temp = VAL0110; break;
                case "VAL0120": temp = VAL0120; break;
                case "VAL0060": temp = VAL0060; break;
                case "VAQ0010": temp = VAQ0010; break;
                case "VAQ0020": temp = VAQ0020; break;
                case "VAS5010": temp = VAS5010; break;
                case "VAS5040": temp = VAS5040; break;
                case "VAS5030": temp = VAS5030; break;







            }
            return temp;

        }
        public static readonly string KetThucPhanHoi = "END_OF_MESSAGE";
        public static readonly string SendMess = "Gửi thông điệp đến hải quan";
        public static readonly string ReturnMess = "Thông điệp phản hồi từ hải quan";
        

        #region New
        public static readonly string VAD0AA0 = "Bản sao thông tin của tờ khai nhập khẩu đã đăng ký";
        public static readonly string VAD4190 = "Nhập khẩu kê khai thông tin đăng ký";
        public static readonly string VAD1AC0 = "Tờ khai hàng hóa nhập khẩu (Thông báo kết quả phân luồng)";
        public static readonly string VAD2AC0 = "Tờ khai hàng hóa nhập khẩu (Thông báo kết quả phân luồng)";
        public static readonly string VAD3AC0 = "Tờ khai hàng hóa nhập khẩu (Thông báo kết quả phân luồng)";
        public static readonly string VAD1BK0 = "Tờ khai hàng hóa nhập khẩu (đề nghị giải phóng hàng)";
        public static readonly string VAD2BK0 = "Tờ khai hàng hóa nhập khẩu (đề nghị giải phóng hàng)";
        public static readonly string VAD3BK0 = "Tờ khai hàng hóa nhập khẩu (đề nghị giải phóng hàng)";
        public static readonly string VAD1RC0 = "Tờ khai hàng hóa nhập kho ngoại quan (Thông báo kết quả phân luồng)";
        public static readonly string VAD2RC0 = "Tờ khai hàng hóa nhập kho ngoại quan (Thông báo kết quả phân luồng)";
        public static readonly string VAD3RC0 = "Tờ khai hàng hóa nhập kho ngoại quan (Thông báo kết quả phân luồng)";
        public static readonly string VAD2RY0 = "Tờ khai bổ sung hàng hóa nhập kho ngoại quan (cơ quan Hải quan thực hiện)";
        public static readonly string VAD3RY0 = "Tờ khai bổ sung hàng hóa nhập kho ngoại quan (cơ quan Hải quan thực hiện)";
        public static readonly string VAD1AG0 = "Tờ khai hàng hóa nhập khẩu (thông quan)";
        public static readonly string VAD1BJ0 = "Tờ khai hàng hóa nhập khẩu (giải phóng hàng)";
        public static readonly string VAD1RG0 = "Tờ khai hàng hóa nhập kho ngoại quan (cho phép đưa hàng vào kho)";
        public static readonly string VAF8010 = "Chứng từ ghi số thuế phải thu";
        public static readonly string VAF8030 = "Chứng từ ghi số lệ phí phải thu";
        public static readonly string VAD0AB0 = "Bản sao thông tin của tờ khai nhập khẩu điều chỉnh";
        public static readonly string VAD4240 = "Thông tin đăng ký thay đổi thành phần của tờ khai nhập khẩu";
        public static readonly string VAD2AE0 = "Tờ khai bổ sung hàng hóa nhập khẩu (người khai Hải quan thực hiện)";
        public static readonly string VAD3AE0 = "Tờ khai bổ sung hàng hóa nhập khẩu (người khai Hải quan thực hiện)";
        public static readonly string VAD2BM0 = "Tờ khai bổ sung hàng hóa nhập khẩu (trường hợp đề nghị giải phóng hàng)";
        public static readonly string VAD3BM0 = "Tờ khai bổ sung hàng hóa nhập khẩu (trường hợp đề nghị giải phóng hàng)";
        public static readonly string VAD2RE0 = "Tờ khai bổ sung hàng hóa nhập kho ngoại quan (người khai Hải quan thực hiện)";
        public static readonly string VAD3RE0 = "Tờ khai bổ sung hàng hóa nhập kho ngoại quan (người khai Hải quan thực hiện)";
        public static readonly string VAD8000 = "Bản chỉ thị sửa đổi của Hải Quan";
        public static readonly string VADBAC0 = "Khai báo nộp thuế của doanh nghiệp AEO";
        public static readonly string VAD1FC0 = "Tờ khai hàng hóa nhập khẩu trị giá thấp (Thông báo kết quả phân luồng)";
        public static readonly string VAD2FC0 = "Tờ khai hàng hóa nhập khẩu trị giá thấp (Thông báo kết quả phân luồng)";
        public static readonly string VAD3FC0 = "Tờ khai hàng hóa nhập khẩu trị giá thấp (Thông báo kết quả phân luồng)";
        public static readonly string VAD2AY0 = "Tờ khai bổ sung hàng hóa nhập khẩu (cơ quan Hải quan thực hiện)";
        public static readonly string VAD3AY0 = "Tờ khai bổ sung hàng hóa nhập khẩu (cơ quan Hải quan thực hiện)";
        public static readonly string VAD2BZ0 = "Tờ khai hàng hóa nhập khẩu (giải phóng hàng)";
        public static readonly string VAD3BZ0 = "Tờ khai hàng hóa nhập khẩu (giải phóng hàng)";
        public static readonly string VAD1FG0 = "Tờ khai hàng hóa nhập khẩu trị giá thấp (thông quan)";
        public static readonly string VAD6030 = "Công bố thông tin đăng ký phần nhập khẩu (Miễn thuế/Thuế thấp)";
        public static readonly string VAD6040 = "Thông tin đăng ký đã thay đổi thành phần của tờ khai nhập khẩu (Miễn thuế/Thuế thấp)";
        public static readonly string VAD2FE0 = "Tờ khai bổ sung hàng hóa nhập khẩu trị giá thấp";
        public static readonly string VAD3FE0 = "Tờ khai bổ sung hàng hóa nhập khẩu trị giá thấp";
        public static readonly string VAD2AG0 = "Tờ khai hàng hóa nhập khẩu (thông quan)";
        public static readonly string VAD2RG0 = "Tờ khai hàng hóa nhập kho ngoại quan (cho phép đưa hàng vào kho)";
        public static readonly string VAD2BJ0 = "Tờ khai hàng hóa nhập khẩu (giải phóng hàng)";
        public static readonly string VAD2FG0 = "Tờ khai hàng hóa nhập khẩu trị giá thấp (thông quan)";
        public static readonly string VAF8020 = "Thông báo Về việc ấn định thuế đối với hàng hóa xuất khẩu, nhập khẩu";
        public static readonly string VAD0AP0 = "Thông tin tham chiếu của tờ khai nhập khẩu";
        public static readonly string VAD0FP0 = "Thông tin tham chiếu tờ khai xuất khẩu ( thuế được miễn giải phóng mặt bằng số lượng nhỏ )";
        public static readonly string VAE0LA0 = "Bản Bản sao thông tin của tờ khai xuất khẩu đã đăng ký";
        public static readonly string VAE4000 = "Thông tin trước khi đăng ký tờ khai xuất khẩu";
        public static readonly string VAE1LD0 = "Tờ khai hàng hóa xuất khẩu (Thông báo kết quả phân luồng)";
        public static readonly string VAE2LD0 = "Tờ khai hàng hóa xuất khẩu (Thông báo kết quả phân luồng)";
        public static readonly string VAE3LD0 = "Tờ khai hàng hóa xuất khẩu (Thông báo kết quả phân luồng)";
        public static readonly string VAE1RD0 = "Tờ khai hàng hóa xuất kho ngoại quan (Thông báo kết quả phân luồng)";
        public static readonly string VAE2RD0 = "Tờ khai hàng hóa xuất kho ngoại quan (Thông báo kết quả phân luồng)";
        public static readonly string VAE3RD0 = "Tờ khai hàng hóa xuất kho ngoại quan (Thông báo kết quả phân luồng)";
        public static readonly string VAE1LF0 = "Tờ khai hàng hóa xuất khẩu (thông quan)";
        public static readonly string VAE1RF0 = "Tờ khai hàng hóa xuất kho ngoại quan (cho phép hàng hóa xuất kho)";
        public static readonly string VAF8040 = "Chứng từ ghi số lệ phí phải thu";
        public static readonly string VAE0LB0 = "Bản sao thông tin của tờ khai xuất khẩu điều chỉnh";
        public static readonly string VAE4110 = "Thông tin đăng ký tờ khai xuất khẩu chỉnh";
        public static readonly string VAE2LE0 = "Tờ khai bổ sung hàng hóa xuất khẩu (người khai Hải quan thực hiện)";
        public static readonly string VAE3LE0 = "Tờ khai bổ sung hàng hóa xuất khẩu (người khai Hải quan thực hiện)";
        public static readonly string VAE2LY0 = "Tờ khai bổ sung hàng hóa xuất khẩu (cơ quan Hải quan thực hiện)";
        public static readonly string VAE3LY0 = "Tờ khai bổ sung hàng hóa xuất khẩu (cơ quan Hải quan thực hiện)";
        public static readonly string VAE2RE0 = "Tờ khai bổ sung hàng hóa xuất kho ngoại quan (người khai hải quan thực hiện)";
        public static readonly string VAE3RE0 = "Tờ khai bổ sung hàng hóa xuất kho ngoại quan (người khai hải quan thực hiện)";
        public static readonly string VAE8000 = "Thông tin chỉ thị của Hải Quan";
        public static readonly string VAE1JD0 = "Tờ khai hàng hóa xuất khẩu trị giá thấp (Thông báo kết quả phân luồng)";
        public static readonly string VAE2JD0 = "Tờ khai hàng hóa xuất khẩu trị giá thấp (Thông báo kết quả phân luồng)";
        public static readonly string VAE3JD0 = "Tờ khai hàng hóa xuất khẩu trị giá thấp (Thông báo kết quả phân luồng)";
        public static readonly string VAE1JF0 = "Tờ khai hàng hóa xuất khẩu trị giá thấp (Thông quan)";
        public static readonly string VAE4940 = "Thông tin tờ khai xuất khẩu ( Miễn thuế/Thuế thấp )";
        public static readonly string VAE2JE0 = "Tờ khai bổ sung hàng hóa xuất khẩu trị giá thấp (Thông báo kết quả phân luồng)";
        public static readonly string VAE3JE0 = "Tờ khai bổ sung hàng hóa xuất khẩu trị giá thấp (Thông báo kết quả phân luồng)";
        public static readonly string VAE2LF0 = "Tờ khai hàng hóa xuất khẩu (Thông quan)";
        public static readonly string VAE2RF0 = "Tờ khai hàng hóa xuất kho ngoại quan(cho phép hàng hóa xuất kho)";
        public static readonly string VAE2JF0 = "Tờ khai hàng hóa xuất khẩu trị giá thấp (Thông quan)";
        public static readonly string VAE0LC0 = "Thông tin tham chiếu tờ khai xuất khẩu";
        public static readonly string VAE0JC0 = "Thông tin tham chiếu tờ khai xuất khẩu (thuế được miễn giải phóng mặt bằng số lượng nhỏ)";
        public static readonly string VAD4870 = "Thông báo yêu cầu kiểm tra thực tế hàng hóa";
        public static readonly string VAE4740 = "Thông báo yêu cầu kiểm tra thực tế hàng hóa";
        public static readonly string VAD4910 = "Thông báo hủy yêu cầu kiểm tra thực tế hàng hóa";
        public static readonly string VAE4780 = "Thông báo hủy yêu cầu kiểm tra thực tế hàng hóa";
        public static readonly string VAL8000 = "Tờ khai bổ sung sau thông quan (chưa đăng ký)";
        public static readonly string VAL8010 = "Thông tin về đăng ký các mặt hàng trên tờ khai sửa đổi";
        public static readonly string VAL8020 = "Tờ khai bổ sung sau thông quan (đã đăng ký) ";
        public static readonly string VAL8030 = "Tờ khai bổ sung sau thông quan (được chấp nhận)";
        public static readonly string VAL8040 = "Tờ khai bổ sung sau thông quan (được chấp nhận)";
        public static readonly string VAL8050 = "Tờ khai bổ sung sau thông quan (được chấp nhận)";
        public static readonly string VAL8060 = "Tờ khai bổ sung sau thông quan (thông báo không chấp nhận)";
        public static readonly string VAL8070 = "Thông tin tham chiếu về kê khai sửa đổi";
        public static readonly string VAD8010 = "Tờ khai danh mục miễn thuế nhập khẩu";
        public static readonly string VAD8020 = "Tờ khai sửa đổi danh mục miễn thuế nhập khẩu";
        public static readonly string VAE8010 = "Tờ khai danh mục miễn thuế xuất khẩu";
        public static readonly string VAE8020 = "Tờ khai sửa đổi danh mục miễn thuế xuất khẩu";
        public static readonly string VAL8080 = "Thông tin khai danh sách miễn thuế";
        public static readonly string VAL8090 = "Danh sách miễn thuế lấy ra";
        public static readonly string VAL8100 = "Chi tiết miễn thuế lấy ra";
        public static readonly string VAD8030 = "Tờ khai danh mục miễn thuế nhập khẩu (đã đăng ký)";
        public static readonly string VAD8040 = "Tờ khai danh mục miễn thuế nhập khẩu (không được đăng ký)";
        public static readonly string VAD8050 = "Tờ khai danh mục miễn thuế nhập khẩu (bị hủy)";
        public static readonly string VAD8060 = "Tờ khai danh mục miễn thuế nhập khẩu (có hướng dẫn của Hải quan)";
        public static readonly string VAE8030 = "Tờ khai danh mục miễn thuế xuất khẩu (đã đăng ký)";
        public static readonly string VAE8040 = "Tờ khai danh mục miễn thuế xuất khẩu (không được đăng ký)";
        public static readonly string VAE8050 = "Tờ khai danh mục miễn thuế xuất khẩu (bị hủy)";
        public static readonly string VAE8060 = "Tờ khai danh mục miễn thuế xuất khẩu (có hướng dẫn của Hải quan)";
        public static readonly string VAD8070 = "Tờ khai danh mục miễn thuế nhập khẩu (tạm dừng)";
        public static readonly string VAE8070 = "Tờ khai danh mục miễn thuế xuất khẩu (tạm dừng)";
        public static readonly string VAL8110 = "Tờ khai bổ sung thông tin quản lý hàng TNTX, TXTN";
        public static readonly string VAL8120 = "Dự kiến ​​nhập khẩu / thông tin quản lý tờ khai xuất khẩu";
        public static readonly string VAL8130 = "Tờ khai bổ sung thông tin quản lý hàng TNTX, TXTN (đã đăng ký)";
        public static readonly string VAL8140 = "Tờ khai bổ sung thông tin quản lý hàng TNTX, TXTN (có hướng dẫn của Hải quan)";
        public static readonly string VAL8150 = "Danh sách các thông tin quản lý nhập khẩu / xuất khẩu dự kiến";
        public static readonly string VAL8160 = "Xem chi tiết thông tin quản lý nhập khẩu / xuất khẩu dự kiến";
        public static readonly string VAHP010 = "Nhận đơn xin phép xuất khẩu / nhập khẩu vật liệu nổ công nghiệp";
        public static readonly string VAHP020 = "Thông tin xin cấp Giấy phép xuất khẩu / nhập khẩu vật liệu nổ công nghiệp";
        public static readonly string VAHC010 = "Cho phép các ứng dụng cho phép xuất khẩu / nhập khẩu vật liệu nổ công nghiệp";
        public static readonly string VAHP030 = "Không cấp giấy phép cho phép xuất khẩu / nhập khẩu vật liệu nổ công nghiệp";
        public static readonly string VAHP040 = "Không chấp thuận ứng dụng cho phép xuất khẩu / nhập khẩu vật liệu nổ công nghiệp";
        public static readonly string VAHP050 = "Hủy bỏ ứng dụng cho phép xuất khẩu / nhập khẩu vật liệu nổ công nghiệp";
        public static readonly string VAHP060 = "Thông báo chỉ đạo của ứng dụng cho phép xuất khẩu / nhập khẩu vật liệu nổ công nghiệp";
        public static readonly string VAHM020 = "Thông tin tham chiếu xin cấp Giấy phép xuất khẩu / nhập khẩu vật liệu nổ công nghiệp";
        public static readonly string VAHM030 = "Thông tin tham chiếu của Giấy phép trên Giấy phép xuất khẩu / nhập khẩu vật liệu nổ công nghiệp";
        public static readonly string VAJP010 = "Nhận đơn xin cấp giấy chứng nhận kiểm dịch động vật nhập khẩu / sản phẩm động vật";
        public static readonly string VAJP020 = "Thông tin xin cấp giấy chứng nhận kiểm dịch động vật nhập khẩu / sản phẩm động vật";
        public static readonly string VAJC010 = "Giấy chứng nhận kiểm dịch động vật nhập khẩu / sản phẩm động vật thông tin ứng dụng";
        public static readonly string VAJC020 = "Đăng ký kiểm tra / kiểm tra thông tin (ứng dụng cho giấy chứng nhận kiểm dịch động vật nhập khẩu / sản phẩm động vật)";
        public static readonly string VAJP030 = "Cho phép áp dụng đối với giấy chứng nhận kiểm dịch động vật nhập khẩu / sản phẩm động vật";
        public static readonly string VAJP040 = "Không cấp giấy phép chứng nhận kiểm dịch động vật nhập khẩu / sản phẩm động vật";
        public static readonly string VAJP050 = "Hủy đặt phòng xin cấp chứng chỉ kiểm dịch động vật nhập khẩu / sản phẩm động vật";
        public static readonly string VAJP060 = "Thông báo hướng xin cấp chứng chỉ kiểm dịch động vật nhập khẩu / sản phẩm động vật";
        public static readonly string VAJM010 = "Thông tin tham chiếu về tình trạng trên kiểm tra / thanh tra để áp dụng cho giấy chứng nhận kiểm dịch động vật nhập khẩu / sản phẩm động vật";
        public static readonly string VAJM020 = "Thông tin tham chiếu xin cấp giấy chứng nhận kiểm dịch động vật nhập khẩu / sản phẩm động vật";
        public static readonly string VAJM030 = "Thông tin tham chiếu của Giấy phép trên Giấy chứng nhận kiểm dịch động vật nhập khẩu / sản phẩm động vật";
        public static readonly string VAGP010 = "Nhận đơn xin cấp Giấy chứng nhận về sự hài lòng về chất lượng thực phẩm nhập khẩu";
        public static readonly string VAGP020 = "Thông tin xin cấp Giấy chứng nhận về sự hài lòng về chất lượng thực phẩm nhập khẩu";
        public static readonly string VAGC010 = "Giấy chứng nhận về sự hài lòng về chất lượng thực phẩm để biết thông tin ứng dụng nhập khẩu";
        public static readonly string VAGP030 = "Cho phép áp dụng đối với Giấy chứng nhận về sự hài lòng về chất lượng thực phẩm nhập khẩu";
        public static readonly string VAGP040 = "Không cấp giấy phép chứng nhận về sự hài lòng về chất lượng thực phẩm nhập khẩu";
        public static readonly string VAGP050 = "Hủy bỏ đơn xin Giấy chứng nhận về sự hài lòng về chất lượng thực phẩm nhập khẩu";
        public static readonly string VAGP060 = "Thông báo hướng áp dụng cho Giấy chứng nhận về sự hài lòng về chất lượng thực phẩm nhập khẩu";
        public static readonly string VAGM020 = "Thông tin tham chiếu xin cấp Giấy chứng nhận về sự hài lòng về chất lượng thực phẩm nhập khẩu";
        public static readonly string VAGM030 = "Thông tin tham chiếu của Giấy phép trên Giấy chứng nhận về sự hài lòng về chất lượng thực phẩm nhập khẩu";
        public static readonly string VAGP110 = "Nhận đơn xin phép nhập khẩu sản phẩm thuốc";
        public static readonly string VAGP120 = "Thông tin xin cấp Giấy phép nhập khẩu sản phẩm thuốc";
        public static readonly string VAGC110 = "Giấy phép nhập khẩu thuốc thông tin sản phẩm ứng dụng";
        public static readonly string VAGP130 = "Cho phép các ứng dụng cho phép nhập khẩu sản phẩm thuốc";
        public static readonly string VAGP140 = "Không cấp giấy phép nhập khẩu sản phẩm thuốc";
        public static readonly string VAGP150 = "Hủy bỏ ứng dụng cho phép nhập khẩu sản phẩm thuốc";
        public static readonly string VAGP160 = "Thông báo chỉ đạo của ứng dụng cho phép nhập khẩu sản phẩm thuốc";
        public static readonly string VAGM120 = "Thông tin tham chiếu xin cấp Giấy phép nhập khẩu sản phẩm thuốc";
        public static readonly string VAGM130 = "Thông tin tham chiếu của Giấy phép trên Giấy phép nhập khẩu sản phẩm thuốc";
        public static readonly string VAL0870 = "Nhận được hóa đơn / thông tin danh sách đóng gói";
        public static readonly string VAL0880 = "Thông tin thông báo về số lượng nhận được hóa đơn điện tử";
        public static readonly string VAL0890 = "Thông tin trên hóa đơn / phiếu đóng gói";
        public static readonly string VAL0940 = "Thông tin về tài liệu tham chiếu hóa đơn / phiếu đóng gói";
        public static readonly string VAF8050 = "Tham chiếu thông tin bảo mật cá nhân";
        public static readonly string VAF8060 = "Tham chiếu thông tin bảo mật độc quyền";
        public static readonly string VAF8070 = "Tài liệu tham chiếu của an ninh cá nhân cho thông tin giao thông vận tải ngoại quan";
        public static readonly string VAF8080 = "Tài liệu tham chiếu của tiền gửi cho thông tin giao thông vận tải ngoại quan";
        public static readonly string VAF5110 = "Tham chiếu thông tin thanh toán Ngân hàng";


        public static readonly string VAU0060 = "Reference information of search request information (DLG22)";
        public static readonly string VA27XX = "Thông báo số tiếp nhận file đính kèm";

        public static readonly string VAL0460 = "Thông tin về kết quả phản hồi file đính kèm";
        public static readonly string VAL0020 = "Bản sao của thông tin áp dụng chung";
        public static readonly string VA28XX = "Thông tin áp dụng chung ";
        public static readonly string VAL0040 = "Bản sao sửa đổi thông tin áp dụng chung";
        public static readonly string VA29XX = "Sửa đổi thông tin áp dụng chung";
        public static readonly string VAL0110 = "Thông tin tham khảo áp dụng chung một";
        public static readonly string VAL0120 = "Thông tin tham khảo của chung ứng dụng B";
        public static readonly string VAL0060 = "Thông tin thông báo về tình trạng áp dụng chung";
        public static readonly string VAQ0010 = "Thông tin để xác nhận thiết lập kênh thông tin liên lạc";
        public static readonly string VAQ0020 = "Tạo tin nhắn để xác nhận thiết lập kênh thông tin liên lạc";


        public static readonly string VAS5010 = "Bản sao thông tin khai báo vận chuyển";
        public static readonly string VAS5040 = "Thông báo phê duyệt khai báo vận chuyển (Luồng Xanh)";
        public static readonly string VAS5030 = "Bản sao thông tin khai báo vận chuyển (Luồng Vàng)";

        #endregion
    }

    public partial class EnumNghiepVuPhanHoi
    {
        #region OGA
        public static readonly string VAHP010 = "VAHP010";
        public static readonly string VAHP020 = "VAHP020";
        public static readonly string VAHC010 = "VAHC010";
        public static readonly string VAHP030 = "VAHP030";
        public static readonly string VAHP040 = "VAHP040";
        public static readonly string VAHP050 = "VAHP050";
        public static readonly string VAHP060 = "VAHP060";
        public static readonly string VAHM020 = "VAHM020";
        public static readonly string VAHM030 = "VAHM030";
        public static readonly string VAJP010 = "VAJP010";
        public static readonly string VAJP020 = "VAJP020";
        public static readonly string VAJC010 = "VAJC010";
        public static readonly string VAJC020 = "VAJC020";
        public static readonly string VAJP030 = "VAJP030";
        public static readonly string VAJP040 = "VAJP040";
        public static readonly string VAJP050 = "VAJP050";
        public static readonly string VAJP060 = "VAJP060";
        public static readonly string VAJM010 = "VAJM010";
        public static readonly string VAJM020 = "VAJM020";
        public static readonly string VAJM030 = "VAJM030";
        public static readonly string VAGP010 = "VAGP010";
        public static readonly string VAGP020 = "VAGP020";
        public static readonly string VAGC010 = "VAGC010";
        public static readonly string VAGP030 = "VAGP030";
        public static readonly string VAGP040 = "VAGP040";
        public static readonly string VAGP050 = "VAGP050";
        public static readonly string VAGP060 = "VAGP060";
        public static readonly string VAGM020 = "VAGM020";
        public static readonly string VAGM030 = "VAGM030";
        public static readonly string VAGP110 = "VAGP110";
        public static readonly string VAGP120 = "VAGP120";
        public static readonly string VAGC110 = "VAGC110";
        public static readonly string VAGP130 = "VAGP130";
        public static readonly string VAGP140 = "VAGP140";
        public static readonly string VAGP150 = "VAGP150";
        public static readonly string VAGP160 = "VAGP160";
        public static readonly string VAGM120 = "VAGM120";
        public static readonly string VAGM130 = "VAGM130";
        public static readonly string VAL0870 = "VAL0870";
        public static readonly string VAL0880 = "VAL0880";
        public static readonly string VAL0890 = "VAL0890";
        public static readonly string VAL0940 = "VAL0940";
        public static readonly string VAF8050 = "VAF8050";
        public static readonly string VAF8060 = "VAF8060";
        public static readonly string VAF8070 = "VAF8070";
        public static readonly string VAF8080 = "VAF8080";
        public static readonly string VAF5110 = "VAF5110";
        
        #endregion

        #region E-declaration
        public static readonly string VAD0AA0 = "VAD0AA0";
        public static readonly string VAD4190 = "VAD4190";
        public static readonly string VAD1AC0 = "VAD1AC0";
        public static readonly string VAD2AC0 = "VAD2AC0";
        public static readonly string VAD3AC0 = "VAD3AC0";
        public static readonly string VAD1BK0 = "VAD1BK0";
        public static readonly string VAD2BK0 = "VAD2BK0";
        public static readonly string VAD3BK0 = "VAD3BK0";
        public static readonly string VAD1RC0 = "VAD1RC0";
        public static readonly string VAD2RC0 = "VAD2RC0";
        public static readonly string VAD3RC0 = "VAD3RC0";
        public static readonly string VAD1AG0 = "VAD1AG0";
        public static readonly string VAD1BJ0 = "VAD1BJ0";
        public static readonly string VAD1RG0 = "VAD1RG0";
        public static readonly string VAF8010 = "VAF8010";
        public static readonly string VAF8030 = "VAF8030";
        public static readonly string VAD0AB0 = "VAD0AB0";
        public static readonly string VAD4240 = "VAD4240";
        public static readonly string VAD2AE0 = "VAD2AE0";
        public static readonly string VAD3AE0 = "VAD3AE0";
        public static readonly string VAD2BM0 = "VAD2BM0";
        public static readonly string VAD3BM0 = "VAD3BM0";
        public static readonly string VAD2RE0 = "VAD2RE0";
        public static readonly string VAD3RE0 = "VAD3RE0";
        public static readonly string VAD2AY0 = "VAD2AY0";
        public static readonly string VAD3AY0 = "VAD3AY0";
        public static readonly string VAD2BZ0 = "VAD2BZ0";
        public static readonly string VAD3BZ0 = "VAD3BZ0";
        public static readonly string VAD2RY0 = "VAD2RY0";
        public static readonly string VAD3RY0 = "VAD3RY0";
        public static readonly string VAD8000 = "VAD8000";
        public static readonly string VADBAC0 = "VADBAC0";
        public static readonly string VAD1FC0 = "VAD1FC0";
        public static readonly string VAD2FC0 = "VAD2FC0";
        public static readonly string VAD3FC0 = "VAD3FC0";
        public static readonly string VAD1FG0 = "VAD1FG0";
        public static readonly string VAD6030 = "VAD6030";
        public static readonly string VAD6040 = "VAD6040";
        public static readonly string VAD2FE0 = "VAD2FE0";
        public static readonly string VAD3FE0 = "VAD3FE0";
        public static readonly string VAD2AG0 = "VAD2AG0";
        public static readonly string VAD2RG0 = "VAD2RG0";
        public static readonly string VAD2BJ0 = "VAD2BJ0";
        public static readonly string VAD2FG0 = "VAD2FG0";
        public static readonly string VAF8020 = "VAF8020";
        public static readonly string VAD0AP0 = "VAD0AP0";
        public static readonly string VAD0FP0 = "VAD0FP0";
        public static readonly string VAE0LA0 = "VAE0LA0";
        public static readonly string VAE4000 = "VAE4000";
        public static readonly string VAE1LD0 = "VAE1LD0";
        public static readonly string VAE2LD0 = "VAE2LD0";
        public static readonly string VAE3LD0 = "VAE3LD0";
        public static readonly string VAE1RD0 = "VAE1RD0";
        public static readonly string VAE2RD0 = "VAE2RD0";
        public static readonly string VAE3RD0 = "VAE3RD0";
        public static readonly string VAE1LF0 = "VAE1LF0";
        public static readonly string VAE1RF0 = "VAE1RF0";
        public static readonly string VAF8040 = "VAF8040";
        public static readonly string VAE0LB0 = "VAE0LB0";
        public static readonly string VAE4110 = "VAE4110";
        public static readonly string VAE2LE0 = "VAE2LE0";
        public static readonly string VAE3LE0 = "VAE3LE0";
        public static readonly string VAE2RE0 = "VAE2RE0";
        public static readonly string VAE3RE0 = "VAE3RE0";
        public static readonly string VAE2LY0 = "VAE2LY0";
        public static readonly string VAE3LY0 = "VAE3LY0";
        public static readonly string VAE2RY0 = "VAE2RY0";
        public static readonly string VAE3RY0 = "VAE3RY0";
        public static readonly string VAE8000 = "VAE8000";
        public static readonly string VAE1JD0 = "VAE1JD0";
        public static readonly string VAE2JD0 = "VAE2JD0";
        public static readonly string VAE3JD0 = "VAE3JD0";
        public static readonly string VAE1JF0 = "VAE1JF0";
        public static readonly string VAE4940 = "VAE4940";
        public static readonly string VAE2JE0 = "VAE2JE0";
        public static readonly string VAE3JE0 = "VAE3JE0";
        public static readonly string VAE2LF0 = "VAE2LF0";
        public static readonly string VAE2RF0 = "VAE2RF0";
        public static readonly string VAE2JF0 = "VAE2JF0";
        public static readonly string VAE0LC0 = "VAE0LC0";
        public static readonly string VAE0JC0 = "VAE0JC0";
        public static readonly string VAD4870 = "VAD4870";
        public static readonly string VAE4740 = "VAE4740";
        public static readonly string VAD4910 = "VAD4910";
        public static readonly string VAE4780 = "VAE4780";
        public static readonly string VAL8000 = "VAL8000";
        public static readonly string VAL8010 = "VAL8010";
        public static readonly string VAL8020 = "VAL8020";
        public static readonly string VAL8030 = "VAL8030";
        public static readonly string VAL8040 = "VAL8040";
        public static readonly string VAL8050 = "VAL8050";
        public static readonly string VAL8060 = "VAL8060";
        public static readonly string VAL8070 = "VAL8070";
        public static readonly string VAD8010 = "VAD8010";
        public static readonly string VAD8020 = "VAD8020";
        public static readonly string VAE8010 = "VAE8010";
        public static readonly string VAE8020 = "VAE8020";
        public static readonly string VAL8080 = "VAL8080";
        public static readonly string VAL8090 = "VAL8090";
        public static readonly string VAL8100 = "VAL8100";
        public static readonly string VAD8030 = "VAD8030";
        public static readonly string VAD8040 = "VAD8040";
        public static readonly string VAD8050 = "VAD8050";
        public static readonly string VAD8060 = "VAD8060";
        public static readonly string VAE8030 = "VAE8030";
        public static readonly string VAE8040 = "VAE8040";
        public static readonly string VAE8050 = "VAE8050";
        public static readonly string VAE8060 = "VAE8060";
        public static readonly string VAD8070 = "VAD8070";
        public static readonly string VAE8070 = "VAE8070";
        public static readonly string VAL8110 = "VAL8110";
        public static readonly string VAL8120 = "VAL8120";
        public static readonly string VAL8130 = "VAL8130";
        public static readonly string VAL8140 = "VAL8140";
        public static readonly string VAL8150 = "VAL8150";
        public static readonly string VAL8160 = "VAL8160";

        #endregion

        #region Tờ khai vận chuyển
        public static readonly string VAS5050 = "VAS5050";
        public static readonly string VAS501 = "VAS5010";
        public static readonly string VAS5020 = "VAS5020";
        public static readonly string VAS5030 = "VAS5030";
        public static readonly string VAS5040 = "VAS5040";
        public static readonly string VAS5130 = "VAS5130";
        //public static readonly string VAS5050 = "VAGP130";
        //public static readonly string VAS5050 = "VAGP130";


        #endregion
        public static readonly string VAL0020 = "VAL0020";
        public static readonly string VAL0040 = "VAL0040";
        public static readonly string VAL2800 = "VAL2800";
        public static readonly string VAL2900 = "VAL2900";
        public static readonly string VAL2700 = "VAL2700";


        public static readonly List<string> GroupPhanHoi_ToKhai = new List<string>
        {
               VAD1AC0,VAD2AC0,VAD3AC0,VAD1BK0,VAD2BK0,VAD3BK0,VAD2AE0,VAD3AE0,VAD2BM0,VAD3BM0,VAD2AY0,VAD3AY0,VAD2BZ0,VAD3BZ0,VADBAC0,
            VAD1RC0,VAD2RC0,VAD3RC0,VAD2RE0, VAD3RE0,VAD2RY0, VAD3RY0, VAD0AA0, VAD0AB0, VAE1LD0,VAE2LD0,VAE3LD0,VAE1RD0,VAE2RD0,VAE3RD0, VAE2LE0 , VAE3LE0 ,VAE2LY0,VAE3LY0 , VAE2RE0 , VAE3RE0, VAE2RY0, VAE3RY0, VAE0LA0,  VAE0LB0, VAE0LC0, VAE4110,VAE1LF0, VAE2LF0, VAE1RF0 , VAE2RF0,VAD1AG0,VAD2AG0,VAD1BJ0,VAD2BJ0,VAD1RG0,VAD2RG0,VAE8000,VAD8000
        };

        public static readonly List<string> GroupPhanHoi_ToKhaiNhap_BanXacNhan = new List<string>
        {
            VAD1AC0,VAD2AC0,VAD3AC0,VAD1BK0,VAD2BK0,VAD3BK0,VAD2AE0,VAD3AE0,VAD2BM0,VAD3BM0,VAD2AY0,VAD3AY0,VAD2BZ0,VAD3BZ0,VADBAC0,
            VAD1RC0,VAD2RC0,VAD3RC0,VAD2RE0, VAD3RE0,VAD2RY0, VAD3RY0, VAD0AA0, VAD0AB0,
        };
        public static readonly List<string> GroupPhanHoi_ToKhaiXuat_BanXacNhan = new List<string>
        {
            // Tờ khai xuất
            VAE1LD0,VAE2LD0,VAE3LD0,VAE1RD0,VAE2RD0,VAE3RD0, VAE2LE0 , VAE3LE0 ,VAE2LY0,VAE3LY0 , VAE2RE0 , VAE3RE0, VAE2RY0, VAE3RY0, VAE0LA0,  VAE0LB0, VAE4110
        };

        public static readonly List<string> GroupPhanHoi_ToKhaiXuat_BanChapNhanThongQuan = new List<string>
        {
           // Tờ khai xuất 
           VAE1LF0, VAE2LF0, VAE1RF0 , VAE2RF0
        };
        public static readonly List<string> GroupPhanHoi_ToKhaiNhap_BanChapNhanThongQuan = new List<string>
        {
            VAD1AG0,VAD2AG0,VAD1BJ0,VAD2BJ0,VAD1RG0,VAD2RG0,
        };

        //AMA
        public static readonly List<string> GroupPhanHoi_ToKhaiBiSung_BanXacNhan = new List<string>
        {
            VAF8010, VAF8020, VAF8030, VAF8050,VAD4870,VAE4740
        };
        //AMA
        public static readonly List<string> GroupPhanHoi_ToKhaiBoSung_BanChapNhanThongQuan = new List<string>
        {
            VAL8000,VAL8020,VAL8030, VAL8040,VAL8050,VAL8060
        };

        /// <summary>
        /// CHUNG TU THANH TOAN
        /// </summary>
        public static readonly List<string> GroupPhanHoi_ChungTuThanhToan_HuongDan = new List<string> { 
            //IAS
            VAF8050, VAF8060, VAF8070, VAF8080,
            //IBA
            VAF5110
        };

        /// <summary>
        /// Da gui den HQ -> Trang thai xu ly: Khai bao chinh thuc
        /// </summary>
        public static readonly List<string> GroupPhanHoi_GiayPhep_BanXacNhan = new List<string> { 
            //SAA
            VAJP010, VAJP020 ,
            //SEA
            VAHP010, VAHP020,
            //SFA
            VAGP010, VAGP020,
            //SMA
            VAGP110, VAGP120
        };
        /// <summary>
        /// HQ duyet -> Trang thai xu ly: Thong quan
        /// </summary>
        public static readonly List<string> GroupPhanHoi_GiayPhep_BanDuyet = new List<string> 
        { 
            //SAA
            VAJP030,
            //SEA
            VAHP030,
            //SFA
            VAGP030,
            //SMA
            VAGP130
        };
        /// <summary>
        /// HQ tu choi -> Trang thai xu ly: Tu choi
        /// </summary>
        public static readonly List<string> GroupPhanHoi_GiayPhep_TuChoi = new List<string> 
        { 
            //SAA
            VAJP040, VAJP050,
            //SEA
            VAHP040, VAHP050,
            //SFA
            VAGP040, VAGP050,
            //SMA
            VAGP140, VAGP150
        };
        /// <summary>
        /// HQ gui thong tin huong dan -> Trang thai xu ly: Khong cap nhat trang thai xu ly
        /// </summary>
        public static readonly List<string> GroupPhanHoi_GiayPhep_HuongDan = new List<string> 
        { 
            //SAA
            VAJP060,
            //SEA
            VAHP060,
            //SFA
            VAGP060,
            //SMA
            VAGP160
        };
        public static readonly List<string> GroupPhanHoi_HoaDon_HuongDan = new List<string> 
        { 
            //IVA
            VAL0870,
            //IVA
           VAL0880
           
        };
        public static readonly List<string> GroupPhanHoi_TKVC_PhanHoiPheDuyet = new List<string> 
        {
            VAS5040,
            VAS5050
        };
        public static readonly List<string> GroupPhanHoi_ThongTinTKDinhKem = new List<string> 
        {
            VAL0020,VAL0040,VAL2900, VAL2800, VAL2700
        };
        public static readonly List<string> GroupPhanHoi_DMMT = new List<string> 
        {
            VAD8010, VAE8010 ,VAD8020,VAD8030,VAD8040,VAD8050,VAD8060,VAD8070
        };
    }

    public partial class EnumTrangThaiXuLyMessage
    {
        public static readonly string ChuaXuLy = "C";
        public static readonly string XuLyLoi = "E";
        public static readonly string DaXem = "X";
    }

    public partial class EnumTrangThaiXuLy
    {
        public static readonly string ChuaKhaiBao = "0";
        public static readonly string KhaiBaoTam = "1";
        public static readonly string KhaiBaoChinhThuc = "2";
        public static readonly string ThongQuan = "3";
        public static readonly string KhaiBaoSua = "4";
        public static readonly string TuChoi = "5";
        public static readonly string Huy = "6";
        //public static readonly string KhaiBaoSuaChinhThuc
    }
}

