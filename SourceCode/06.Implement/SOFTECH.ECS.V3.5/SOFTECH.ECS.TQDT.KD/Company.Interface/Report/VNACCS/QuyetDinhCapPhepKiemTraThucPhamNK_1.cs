using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;

namespace Company.Interface.Report.VNACCS
{
    public partial class QuyetDinhCapPhepKiemTraThucPhamNK_1 : DevExpress.XtraReports.UI.XtraReport
    {
        public QuyetDinhCapPhepKiemTraThucPhamNK_1()
        {
            InitializeComponent();
        }
        public void BindingReport(VAGP030 vagp)
        {
            int HangHoa = vagp.HangHoa.Count;
            if (HangHoa <= 10)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 10, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblMaNguoiKhai.Text = vagp.A01.GetValue().ToString().ToUpper();
            lblTenNguoiKhai.Text = vagp.A02.GetValue().ToString().ToUpper();
            lblMaDonViCapPhep.Text = vagp.A04.GetValue().ToString().ToUpper();
            lblTenDonViCP.Text = vagp.A05.GetValue().ToString().ToUpper();
            lblSoDonXinCapPhep.Text = vagp.A08.GetValue().ToString().ToUpper();
            lblChucNangChungTu.Text = vagp.A06.GetValue().ToString().ToUpper();
            lblLoaiGiayPhep.Text = vagp.A07.GetValue().ToString().ToUpper();
            lblKetQuaXuLy.Text = vagp.A09.GetValue().ToString().ToUpper();
            lblSoGiayPhep.Text = vagp.A10.GetValue().ToString().ToUpper();
            //lblNgayGiayPhep.Text = vagp.A11.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vagp.A11.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayGiayPhep.Text = Convert.ToDateTime(vagp.A11.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayGiayPhep.Text = "";
            }
            if (Convert.ToDateTime(vagp.A12.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHieuLucGP.Text = Convert.ToDateTime(vagp.A12.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHieuLucGP.Text = "";
            }
            if (Convert.ToDateTime(vagp.A13.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayHetHieuLucGP.Text = Convert.ToDateTime(vagp.A13.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayHetHieuLucGP.Text = "";
            }
            lblThuongNhanXK.Text = vagp.A14.GetValue().ToString().ToUpper();
            lblMaBuuChinhTNXK.Text = vagp.A20.GetValue().ToString().ToUpper();
            lblSoNhaTenDuongTNXK.Text = vagp.A15.GetValue().ToString().ToUpper();
            lblPhuongXaTNXK.Text = vagp.A16.GetValue().ToString().ToUpper();
            lblQuanHuyenTNXK.Text = vagp.A17.GetValue().ToString().ToUpper();
            lblTinhThanhTNXK.Text = vagp.A18.GetValue().ToString().ToUpper();
            lblMaQuocGiaTNXK.Text = vagp.A19.GetValue().ToString().ToUpper();
            lblSoDienThoaiTNXK.Text = vagp.A21.GetValue().ToString().ToUpper();
            lblSoFaxTNXK.Text = vagp.A22.GetValue().ToString().ToUpper();
            lblEmailTNXK.Text = vagp.A23.GetValue().ToString().ToUpper();
            lblSoHopDong.Text = vagp.A32.GetValue().ToString().ToUpper();
            lblSoVanDon.Text = vagp.A33.GetValue().ToString().ToUpper();
            lblMaBenDi.Text = vagp.A34.GetValue().ToString().ToUpper();
            lblTenBenDi.Text = vagp.A35.GetValue().ToString().ToUpper();
            lblMaThuongNhanNK.Text = vagp.A24.GetValue().ToString().ToUpper();
            lblTenThuongNhanNK.Text = vagp.A25.GetValue().ToString().ToUpper();
            lblMaBuuChinhTNNK.Text = vagp.A28.GetValue().ToString().ToUpper();
            lblSoNhaTenDuongTNNK.Text = vagp.A26.GetValue().ToString().ToUpper();
            lblMaQuocGiaTNNK.Text = vagp.A27.GetValue().ToString().ToUpper();
            lblSoDienThoaiTNNK.Text = vagp.A29.GetValue().ToString().ToUpper();
            lblSoFaxTNNK.Text = vagp.A30.GetValue().ToString().ToUpper();
            lblEmailTNNK.Text = vagp.A31.GetValue().ToString().ToUpper();
            lblMaBenDen.Text = vagp.A36.GetValue().ToString().ToUpper();
            lblTenBenDen.Text = vagp.A37.GetValue().ToString().ToUpper();
            //lblThoiGianNKDuKien.Text = vagp.A38.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vagp.A38.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiGianNKDuKien.Text = Convert.ToDateTime(vagp.A38.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiGianNKDuKien.Text = "";
            }
            lblGiaTriHangHoa.Text = vagp.A39.GetValue().ToString().ToUpper();
            lblMaDonViTienTe.Text = vagp.A40.GetValue().ToString().ToUpper();
            lblDiaDiemTapKetHH.Text = vagp.A41.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vagp.A42.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblThoiGianKiemTra.Text = Convert.ToDateTime(vagp.A42.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblThoiGianKiemTra.Text = "";
            }
            lblDiaDiemKiemTra.Text = vagp.A43.GetValue().ToString().ToUpper();
            lblGhiChu.Text = vagp.A45.GetValue().ToString().ToUpper();
            if (Convert.ToDateTime(vagp.A03.GetValue()).ToString("dd/MM/yyyy") != "01/01/1900")
            {
                lblNgayKhaiBao.Text = Convert.ToDateTime(vagp.A03.GetValue()).ToString("dd/MM/yyyy");
            }
            else
            {
                lblNgayKhaiBao.Text = "";
            }
            lblDaiDienCoQuanKT.Text = vagp.A44.GetValue().ToString().ToUpper();



        }



    }
}
