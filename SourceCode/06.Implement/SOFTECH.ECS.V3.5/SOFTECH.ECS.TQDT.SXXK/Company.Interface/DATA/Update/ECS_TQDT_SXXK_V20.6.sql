/****** PHIPH :  Table [dbo].[t_KDT_CX_NPLXinHuyDangKy]    Script Date: 11/27/2014 10:16:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_CX_NPLXinHuyDangKy](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SoTiepNhan] [bigint] NULL,
	[NgayTiepNhan] [datetime] NULL,
	[MaHaiQuan] [varchar](6) NULL,
	[MaDoanhNghiep] [varchar](20) NULL,
	[LanThanhLy] [int] NULL,
	[TrangThaiXuLy] [int] NULL,
	[GUIDSTR] [nvarchar](200) NULL,
	[NgayThongBaoHuy] [datetime] NULL,
 CONSTRAINT [PK_T_KDT_CX_NPLXinHuyDangKy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuyDangKy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuyDangKy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuyDangKy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuyDangKy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuyDangKy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuyDangKy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuyDangKy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuyDangKy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuyDangKy_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_Insert]
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200),
	@NgayThongBaoHuy datetime,
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_CX_NPLXinHuyDangKy]
(
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR],
	[NgayThongBaoHuy]
)
VALUES 
(
	@SoTiepNhan,
	@NgayTiepNhan,
	@MaHaiQuan,
	@MaDoanhNghiep,
	@LanThanhLy,
	@TrangThaiXuLy,
	@GUIDSTR,
	@NgayThongBaoHuy
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuyDangKy_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200),
	@NgayThongBaoHuy datetime
AS

UPDATE
	[dbo].[t_KDT_CX_NPLXinHuyDangKy]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[LanThanhLy] = @LanThanhLy,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[GUIDSTR] = @GUIDSTR,
	[NgayThongBaoHuy] = @NgayThongBaoHuy
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuyDangKy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_InsertUpdate]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan varchar(6),
	@MaDoanhNghiep varchar(20),
	@LanThanhLy int,
	@TrangThaiXuLy int,
	@GUIDSTR nvarchar(200),
	@NgayThongBaoHuy datetime
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_CX_NPLXinHuyDangKy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_CX_NPLXinHuyDangKy] 
		SET
			[SoTiepNhan] = @SoTiepNhan,
			[NgayTiepNhan] = @NgayTiepNhan,
			[MaHaiQuan] = @MaHaiQuan,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[LanThanhLy] = @LanThanhLy,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[GUIDSTR] = @GUIDSTR,
			[NgayThongBaoHuy] = @NgayThongBaoHuy
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_CX_NPLXinHuyDangKy]
		(
			[SoTiepNhan],
			[NgayTiepNhan],
			[MaHaiQuan],
			[MaDoanhNghiep],
			[LanThanhLy],
			[TrangThaiXuLy],
			[GUIDSTR],
			[NgayThongBaoHuy]
		)
		VALUES 
		(
			@SoTiepNhan,
			@NgayTiepNhan,
			@MaHaiQuan,
			@MaDoanhNghiep,
			@LanThanhLy,
			@TrangThaiXuLy,
			@GUIDSTR,
			@NgayThongBaoHuy
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuyDangKy_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_CX_NPLXinHuyDangKy]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuyDangKy_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_CX_NPLXinHuyDangKy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuyDangKy_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR],
	[NgayThongBaoHuy]
FROM
	[dbo].[t_KDT_CX_NPLXinHuyDangKy]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuyDangKy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR],
	[NgayThongBaoHuy]
FROM [dbo].[t_KDT_CX_NPLXinHuyDangKy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuyDangKy_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Thursday, November 27, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuyDangKy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoTiepNhan],
	[NgayTiepNhan],
	[MaHaiQuan],
	[MaDoanhNghiep],
	[LanThanhLy],
	[TrangThaiXuLy],
	[GUIDSTR],
	[NgayThongBaoHuy]
FROM
	[dbo].[t_KDT_CX_NPLXinHuyDangKy]	

GO

/****** phiph:  Table [dbo].[t_KDT_CX_NPLXinHuy]    Script Date: 11/27/2014 10:18:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[t_KDT_CX_NPLXinHuy](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Master_id] [bigint] NULL,
	[SoToKhai] [numeric](18, 0) NULL,
	[MaLoaiHinh] [varchar](6) NULL,
	[NgayDangKy] [datetime] NULL,
	[MaHaiQuan] [varchar](6) NULL,
	[MaHang] [nvarchar](80) NULL,
	[TenHang] [nvarchar](255) NULL,
	[LuongTieuHuy] [decimal](24, 6) NULL,
	[LoaiHang] [int] NULL,
	[DVT] [varchar](6) NULL,
	[Temp] [nvarchar](255) NULL,
 CONSTRAINT [PK_t_KDT_CX_NPLXinHuy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuy_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuy_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuy_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuy_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuy_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuy_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuy_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_CX_NPLXinHuy_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuy_Insert]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 26, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_Insert]
	@Master_id bigint,
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaHang nvarchar(80),
	@TenHang nvarchar(255),
	@LuongTieuHuy decimal(24, 6),
	@LoaiHang int,
	@DVT varchar(6),
	@Temp nvarchar(255),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_CX_NPLXinHuy]
(
	[Master_id],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaHang],
	[TenHang],
	[LuongTieuHuy],
	[LoaiHang],
	[DVT],
	[Temp]
)
VALUES 
(
	@Master_id,
	@SoToKhai,
	@MaLoaiHinh,
	@NgayDangKy,
	@MaHaiQuan,
	@MaHang,
	@TenHang,
	@LuongTieuHuy,
	@LoaiHang,
	@DVT,
	@Temp
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuy_Update]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 26, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_Update]
	@ID bigint,
	@Master_id bigint,
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaHang nvarchar(80),
	@TenHang nvarchar(255),
	@LuongTieuHuy decimal(24, 6),
	@LoaiHang int,
	@DVT varchar(6),
	@Temp nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_CX_NPLXinHuy]
SET
	[Master_id] = @Master_id,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaHaiQuan] = @MaHaiQuan,
	[MaHang] = @MaHang,
	[TenHang] = @TenHang,
	[LuongTieuHuy] = @LuongTieuHuy,
	[LoaiHang] = @LoaiHang,
	[DVT] = @DVT,
	[Temp] = @Temp
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuy_InsertUpdate]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 26, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_InsertUpdate]
	@ID bigint,
	@Master_id bigint,
	@SoToKhai numeric(18, 0),
	@MaLoaiHinh varchar(6),
	@NgayDangKy datetime,
	@MaHaiQuan varchar(6),
	@MaHang nvarchar(80),
	@TenHang nvarchar(255),
	@LuongTieuHuy decimal(24, 6),
	@LoaiHang int,
	@DVT varchar(6),
	@Temp nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_CX_NPLXinHuy] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_CX_NPLXinHuy] 
		SET
			[Master_id] = @Master_id,
			[SoToKhai] = @SoToKhai,
			[MaLoaiHinh] = @MaLoaiHinh,
			[NgayDangKy] = @NgayDangKy,
			[MaHaiQuan] = @MaHaiQuan,
			[MaHang] = @MaHang,
			[TenHang] = @TenHang,
			[LuongTieuHuy] = @LuongTieuHuy,
			[LoaiHang] = @LoaiHang,
			[DVT] = @DVT,
			[Temp] = @Temp
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_CX_NPLXinHuy]
		(
			[Master_id],
			[SoToKhai],
			[MaLoaiHinh],
			[NgayDangKy],
			[MaHaiQuan],
			[MaHang],
			[TenHang],
			[LuongTieuHuy],
			[LoaiHang],
			[DVT],
			[Temp]
		)
		VALUES 
		(
			@Master_id,
			@SoToKhai,
			@MaLoaiHinh,
			@NgayDangKy,
			@MaHaiQuan,
			@MaHang,
			@TenHang,
			@LuongTieuHuy,
			@LoaiHang,
			@DVT,
			@Temp
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuy_Delete]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 26, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_CX_NPLXinHuy]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuy_DeleteDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 26, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_CX_NPLXinHuy] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuy_Load]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 26, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaHang],
	[TenHang],
	[LuongTieuHuy],
	[LoaiHang],
	[DVT],
	[Temp]
FROM
	[dbo].[t_KDT_CX_NPLXinHuy]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuy_SelectDynamic]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 26, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_id],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaHang],
	[TenHang],
	[LuongTieuHuy],
	[LoaiHang],
	[DVT],
	[Temp]
FROM [dbo].[t_KDT_CX_NPLXinHuy] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_CX_NPLXinHuy_SelectAll]
-- Database: ECS_TQDT_SXXK_V4_HT_2
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, November 26, 2014
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_CX_NPLXinHuy_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_id],
	[SoToKhai],
	[MaLoaiHinh],
	[NgayDangKy],
	[MaHaiQuan],
	[MaHang],
	[TenHang],
	[LuongTieuHuy],
	[LoaiHang],
	[DVT],
	[Temp]
FROM
	[dbo].[t_KDT_CX_NPLXinHuy]	

GO

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '20.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('20.6',GETDATE(), N' Cap nhat bang ke tieu huy')
END