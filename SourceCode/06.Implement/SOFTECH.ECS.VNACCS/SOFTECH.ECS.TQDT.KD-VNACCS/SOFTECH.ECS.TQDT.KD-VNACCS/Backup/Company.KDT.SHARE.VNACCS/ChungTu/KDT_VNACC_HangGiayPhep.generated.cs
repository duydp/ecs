using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACC_HangGiayPhep : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string GiayPhepType { set; get; }
		public long GiayPhep_ID { set; get; }
		public string TenHangHoa { set; get; }
		public string MaSoHangHoa { set; get; }
		public decimal SoLuong { set; get; }
		public string DonVitinhSoLuong { set; get; }
		public decimal KhoiLuong { set; get; }
		public string DonVitinhKhoiLuong { set; get; }
		public string XuatXu { set; get; }
		public string TinhBiet { set; get; }
		public decimal Tuoi { set; get; }
		public string NoiSanXuat { set; get; }
		public string QuyCachDongGoi { set; get; }
		public decimal TongSoLuongNhapKhau { set; get; }
		public string DonViTinhTongSoLuongNhapKhau { set; get; }
		public string KichCoCaThe { set; get; }
		public decimal TrongLuongTinh { set; get; }
		public string DonViTinhTrongLuong { set; get; }
		public decimal TrongLuongCaBi { set; get; }
		public string DonViTinhTrongLuongCaBi { set; get; }
		public decimal SoLuongKiemDich { set; get; }
		public string DonViTinhSoLuongKiemDich { set; get; }
		public string LoaiBaoBi { set; get; }
		public string MucDichSuDung { set; get; }
		public string Notes { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACC_HangGiayPhep> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACC_HangGiayPhep> collection = new List<KDT_VNACC_HangGiayPhep>();
			while (reader.Read())
			{
				KDT_VNACC_HangGiayPhep entity = new KDT_VNACC_HangGiayPhep();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayPhepType"))) entity.GiayPhepType = reader.GetString(reader.GetOrdinal("GiayPhepType"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayPhep_ID"))) entity.GiayPhep_ID = reader.GetInt64(reader.GetOrdinal("GiayPhep_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenHangHoa"))) entity.TenHangHoa = reader.GetString(reader.GetOrdinal("TenHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaSoHangHoa"))) entity.MaSoHangHoa = reader.GetString(reader.GetOrdinal("MaSoHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonVitinhSoLuong"))) entity.DonVitinhSoLuong = reader.GetString(reader.GetOrdinal("DonVitinhSoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("KhoiLuong"))) entity.KhoiLuong = reader.GetDecimal(reader.GetOrdinal("KhoiLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonVitinhKhoiLuong"))) entity.DonVitinhKhoiLuong = reader.GetString(reader.GetOrdinal("DonVitinhKhoiLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("XuatXu"))) entity.XuatXu = reader.GetString(reader.GetOrdinal("XuatXu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TinhBiet"))) entity.TinhBiet = reader.GetString(reader.GetOrdinal("TinhBiet"));
				if (!reader.IsDBNull(reader.GetOrdinal("Tuoi"))) entity.Tuoi = reader.GetDecimal(reader.GetOrdinal("Tuoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiSanXuat"))) entity.NoiSanXuat = reader.GetString(reader.GetOrdinal("NoiSanXuat"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuyCachDongGoi"))) entity.QuyCachDongGoi = reader.GetString(reader.GetOrdinal("QuyCachDongGoi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongSoLuongNhapKhau"))) entity.TongSoLuongNhapKhau = reader.GetDecimal(reader.GetOrdinal("TongSoLuongNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTinhTongSoLuongNhapKhau"))) entity.DonViTinhTongSoLuongNhapKhau = reader.GetString(reader.GetOrdinal("DonViTinhTongSoLuongNhapKhau"));
				if (!reader.IsDBNull(reader.GetOrdinal("KichCoCaThe"))) entity.KichCoCaThe = reader.GetString(reader.GetOrdinal("KichCoCaThe"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongTinh"))) entity.TrongLuongTinh = reader.GetDecimal(reader.GetOrdinal("TrongLuongTinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTinhTrongLuong"))) entity.DonViTinhTrongLuong = reader.GetString(reader.GetOrdinal("DonViTinhTrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuongCaBi"))) entity.TrongLuongCaBi = reader.GetDecimal(reader.GetOrdinal("TrongLuongCaBi"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTinhTrongLuongCaBi"))) entity.DonViTinhTrongLuongCaBi = reader.GetString(reader.GetOrdinal("DonViTinhTrongLuongCaBi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuongKiemDich"))) entity.SoLuongKiemDich = reader.GetDecimal(reader.GetOrdinal("SoLuongKiemDich"));
				if (!reader.IsDBNull(reader.GetOrdinal("DonViTinhSoLuongKiemDich"))) entity.DonViTinhSoLuongKiemDich = reader.GetString(reader.GetOrdinal("DonViTinhSoLuongKiemDich"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiBaoBi"))) entity.LoaiBaoBi = reader.GetString(reader.GetOrdinal("LoaiBaoBi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MucDichSuDung"))) entity.MucDichSuDung = reader.GetString(reader.GetOrdinal("MucDichSuDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACC_HangGiayPhep> collection, long id)
        {
            foreach (KDT_VNACC_HangGiayPhep item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangGiayPhep VALUES(@GiayPhepType, @GiayPhep_ID, @TenHangHoa, @MaSoHangHoa, @SoLuong, @DonVitinhSoLuong, @KhoiLuong, @DonVitinhKhoiLuong, @XuatXu, @TinhBiet, @Tuoi, @NoiSanXuat, @QuyCachDongGoi, @TongSoLuongNhapKhau, @DonViTinhTongSoLuongNhapKhau, @KichCoCaThe, @TrongLuongTinh, @DonViTinhTrongLuong, @TrongLuongCaBi, @DonViTinhTrongLuongCaBi, @SoLuongKiemDich, @DonViTinhSoLuongKiemDich, @LoaiBaoBi, @MucDichSuDung, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_HangGiayPhep SET GiayPhepType = @GiayPhepType, GiayPhep_ID = @GiayPhep_ID, TenHangHoa = @TenHangHoa, MaSoHangHoa = @MaSoHangHoa, SoLuong = @SoLuong, DonVitinhSoLuong = @DonVitinhSoLuong, KhoiLuong = @KhoiLuong, DonVitinhKhoiLuong = @DonVitinhKhoiLuong, XuatXu = @XuatXu, TinhBiet = @TinhBiet, Tuoi = @Tuoi, NoiSanXuat = @NoiSanXuat, QuyCachDongGoi = @QuyCachDongGoi, TongSoLuongNhapKhau = @TongSoLuongNhapKhau, DonViTinhTongSoLuongNhapKhau = @DonViTinhTongSoLuongNhapKhau, KichCoCaThe = @KichCoCaThe, TrongLuongTinh = @TrongLuongTinh, DonViTinhTrongLuong = @DonViTinhTrongLuong, TrongLuongCaBi = @TrongLuongCaBi, DonViTinhTrongLuongCaBi = @DonViTinhTrongLuongCaBi, SoLuongKiemDich = @SoLuongKiemDich, DonViTinhSoLuongKiemDich = @DonViTinhSoLuongKiemDich, LoaiBaoBi = @LoaiBaoBi, MucDichSuDung = @MucDichSuDung, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangGiayPhep WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhepType", SqlDbType.VarChar, "GiayPhepType", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, "DonVitinhSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhoiLuong", SqlDbType.Decimal, "KhoiLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, "DonVitinhKhoiLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XuatXu", SqlDbType.VarChar, "XuatXu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhBiet", SqlDbType.VarChar, "TinhBiet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Tuoi", SqlDbType.Decimal, "Tuoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiSanXuat", SqlDbType.NVarChar, "NoiSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuyCachDongGoi", SqlDbType.NVarChar, "QuyCachDongGoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoLuongNhapKhau", SqlDbType.Decimal, "TongSoLuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhTongSoLuongNhapKhau", SqlDbType.VarChar, "DonViTinhTongSoLuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KichCoCaThe", SqlDbType.NVarChar, "KichCoCaThe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuongTinh", SqlDbType.Decimal, "TrongLuongTinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhTrongLuong", SqlDbType.VarChar, "DonViTinhTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuongCaBi", SqlDbType.Decimal, "TrongLuongCaBi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhTrongLuongCaBi", SqlDbType.VarChar, "DonViTinhTrongLuongCaBi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongKiemDich", SqlDbType.Decimal, "SoLuongKiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhSoLuongKiemDich", SqlDbType.VarChar, "DonViTinhSoLuongKiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiBaoBi", SqlDbType.NVarChar, "LoaiBaoBi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MucDichSuDung", SqlDbType.NVarChar, "MucDichSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhepType", SqlDbType.VarChar, "GiayPhepType", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, "DonVitinhSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhoiLuong", SqlDbType.Decimal, "KhoiLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, "DonVitinhKhoiLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XuatXu", SqlDbType.VarChar, "XuatXu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhBiet", SqlDbType.VarChar, "TinhBiet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Tuoi", SqlDbType.Decimal, "Tuoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiSanXuat", SqlDbType.NVarChar, "NoiSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuyCachDongGoi", SqlDbType.NVarChar, "QuyCachDongGoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoLuongNhapKhau", SqlDbType.Decimal, "TongSoLuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhTongSoLuongNhapKhau", SqlDbType.VarChar, "DonViTinhTongSoLuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KichCoCaThe", SqlDbType.NVarChar, "KichCoCaThe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuongTinh", SqlDbType.Decimal, "TrongLuongTinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhTrongLuong", SqlDbType.VarChar, "DonViTinhTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuongCaBi", SqlDbType.Decimal, "TrongLuongCaBi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhTrongLuongCaBi", SqlDbType.VarChar, "DonViTinhTrongLuongCaBi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongKiemDich", SqlDbType.Decimal, "SoLuongKiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhSoLuongKiemDich", SqlDbType.VarChar, "DonViTinhSoLuongKiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiBaoBi", SqlDbType.NVarChar, "LoaiBaoBi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MucDichSuDung", SqlDbType.NVarChar, "MucDichSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACC_HangGiayPhep VALUES(@GiayPhepType, @GiayPhep_ID, @TenHangHoa, @MaSoHangHoa, @SoLuong, @DonVitinhSoLuong, @KhoiLuong, @DonVitinhKhoiLuong, @XuatXu, @TinhBiet, @Tuoi, @NoiSanXuat, @QuyCachDongGoi, @TongSoLuongNhapKhau, @DonViTinhTongSoLuongNhapKhau, @KichCoCaThe, @TrongLuongTinh, @DonViTinhTrongLuong, @TrongLuongCaBi, @DonViTinhTrongLuongCaBi, @SoLuongKiemDich, @DonViTinhSoLuongKiemDich, @LoaiBaoBi, @MucDichSuDung, @Notes, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACC_HangGiayPhep SET GiayPhepType = @GiayPhepType, GiayPhep_ID = @GiayPhep_ID, TenHangHoa = @TenHangHoa, MaSoHangHoa = @MaSoHangHoa, SoLuong = @SoLuong, DonVitinhSoLuong = @DonVitinhSoLuong, KhoiLuong = @KhoiLuong, DonVitinhKhoiLuong = @DonVitinhKhoiLuong, XuatXu = @XuatXu, TinhBiet = @TinhBiet, Tuoi = @Tuoi, NoiSanXuat = @NoiSanXuat, QuyCachDongGoi = @QuyCachDongGoi, TongSoLuongNhapKhau = @TongSoLuongNhapKhau, DonViTinhTongSoLuongNhapKhau = @DonViTinhTongSoLuongNhapKhau, KichCoCaThe = @KichCoCaThe, TrongLuongTinh = @TrongLuongTinh, DonViTinhTrongLuong = @DonViTinhTrongLuong, TrongLuongCaBi = @TrongLuongCaBi, DonViTinhTrongLuongCaBi = @DonViTinhTrongLuongCaBi, SoLuongKiemDich = @SoLuongKiemDich, DonViTinhSoLuongKiemDich = @DonViTinhSoLuongKiemDich, LoaiBaoBi = @LoaiBaoBi, MucDichSuDung = @MucDichSuDung, Notes = @Notes, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACC_HangGiayPhep WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhepType", SqlDbType.VarChar, "GiayPhepType", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, "DonVitinhSoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KhoiLuong", SqlDbType.Decimal, "KhoiLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, "DonVitinhKhoiLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@XuatXu", SqlDbType.VarChar, "XuatXu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TinhBiet", SqlDbType.VarChar, "TinhBiet", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Tuoi", SqlDbType.Decimal, "Tuoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiSanXuat", SqlDbType.NVarChar, "NoiSanXuat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuyCachDongGoi", SqlDbType.NVarChar, "QuyCachDongGoi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongSoLuongNhapKhau", SqlDbType.Decimal, "TongSoLuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhTongSoLuongNhapKhau", SqlDbType.VarChar, "DonViTinhTongSoLuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KichCoCaThe", SqlDbType.NVarChar, "KichCoCaThe", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuongTinh", SqlDbType.Decimal, "TrongLuongTinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhTrongLuong", SqlDbType.VarChar, "DonViTinhTrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuongCaBi", SqlDbType.Decimal, "TrongLuongCaBi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhTrongLuongCaBi", SqlDbType.VarChar, "DonViTinhTrongLuongCaBi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuongKiemDich", SqlDbType.Decimal, "SoLuongKiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DonViTinhSoLuongKiemDich", SqlDbType.VarChar, "DonViTinhSoLuongKiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiBaoBi", SqlDbType.NVarChar, "LoaiBaoBi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MucDichSuDung", SqlDbType.NVarChar, "MucDichSuDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhepType", SqlDbType.VarChar, "GiayPhepType", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhep_ID", SqlDbType.BigInt, "GiayPhep_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenHangHoa", SqlDbType.NVarChar, "TenHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaSoHangHoa", SqlDbType.VarChar, "MaSoHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, "DonVitinhSoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KhoiLuong", SqlDbType.Decimal, "KhoiLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, "DonVitinhKhoiLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@XuatXu", SqlDbType.VarChar, "XuatXu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TinhBiet", SqlDbType.VarChar, "TinhBiet", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Tuoi", SqlDbType.Decimal, "Tuoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiSanXuat", SqlDbType.NVarChar, "NoiSanXuat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuyCachDongGoi", SqlDbType.NVarChar, "QuyCachDongGoi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongSoLuongNhapKhau", SqlDbType.Decimal, "TongSoLuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhTongSoLuongNhapKhau", SqlDbType.VarChar, "DonViTinhTongSoLuongNhapKhau", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KichCoCaThe", SqlDbType.NVarChar, "KichCoCaThe", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuongTinh", SqlDbType.Decimal, "TrongLuongTinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhTrongLuong", SqlDbType.VarChar, "DonViTinhTrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuongCaBi", SqlDbType.Decimal, "TrongLuongCaBi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhTrongLuongCaBi", SqlDbType.VarChar, "DonViTinhTrongLuongCaBi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuongKiemDich", SqlDbType.Decimal, "SoLuongKiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DonViTinhSoLuongKiemDich", SqlDbType.VarChar, "DonViTinhSoLuongKiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiBaoBi", SqlDbType.NVarChar, "LoaiBaoBi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MucDichSuDung", SqlDbType.NVarChar, "MucDichSuDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACC_HangGiayPhep Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACC_HangGiayPhep> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACC_HangGiayPhep> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACC_HangGiayPhep> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACC_HangGiayPhep(string giayPhepType, long giayPhep_ID, string tenHangHoa, string maSoHangHoa, decimal soLuong, string donVitinhSoLuong, decimal khoiLuong, string donVitinhKhoiLuong, string xuatXu, string tinhBiet, decimal tuoi, string noiSanXuat, string quyCachDongGoi, decimal tongSoLuongNhapKhau, string donViTinhTongSoLuongNhapKhau, string kichCoCaThe, decimal trongLuongTinh, string donViTinhTrongLuong, decimal trongLuongCaBi, string donViTinhTrongLuongCaBi, decimal soLuongKiemDich, string donViTinhSoLuongKiemDich, string loaiBaoBi, string mucDichSuDung, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangGiayPhep entity = new KDT_VNACC_HangGiayPhep();	
			entity.GiayPhepType = giayPhepType;
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TenHangHoa = tenHangHoa;
			entity.MaSoHangHoa = maSoHangHoa;
			entity.SoLuong = soLuong;
			entity.DonVitinhSoLuong = donVitinhSoLuong;
			entity.KhoiLuong = khoiLuong;
			entity.DonVitinhKhoiLuong = donVitinhKhoiLuong;
			entity.XuatXu = xuatXu;
			entity.TinhBiet = tinhBiet;
			entity.Tuoi = tuoi;
			entity.NoiSanXuat = noiSanXuat;
			entity.QuyCachDongGoi = quyCachDongGoi;
			entity.TongSoLuongNhapKhau = tongSoLuongNhapKhau;
			entity.DonViTinhTongSoLuongNhapKhau = donViTinhTongSoLuongNhapKhau;
			entity.KichCoCaThe = kichCoCaThe;
			entity.TrongLuongTinh = trongLuongTinh;
			entity.DonViTinhTrongLuong = donViTinhTrongLuong;
			entity.TrongLuongCaBi = trongLuongCaBi;
			entity.DonViTinhTrongLuongCaBi = donViTinhTrongLuongCaBi;
			entity.SoLuongKiemDich = soLuongKiemDich;
			entity.DonViTinhSoLuongKiemDich = donViTinhSoLuongKiemDich;
			entity.LoaiBaoBi = loaiBaoBi;
			entity.MucDichSuDung = mucDichSuDung;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@GiayPhepType", SqlDbType.VarChar, GiayPhepType);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaSoHangHoa", SqlDbType.VarChar, MaSoHangHoa);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, DonVitinhSoLuong);
			db.AddInParameter(dbCommand, "@KhoiLuong", SqlDbType.Decimal, KhoiLuong);
			db.AddInParameter(dbCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, DonVitinhKhoiLuong);
			db.AddInParameter(dbCommand, "@XuatXu", SqlDbType.VarChar, XuatXu);
			db.AddInParameter(dbCommand, "@TinhBiet", SqlDbType.VarChar, TinhBiet);
			db.AddInParameter(dbCommand, "@Tuoi", SqlDbType.Decimal, Tuoi);
			db.AddInParameter(dbCommand, "@NoiSanXuat", SqlDbType.NVarChar, NoiSanXuat);
			db.AddInParameter(dbCommand, "@QuyCachDongGoi", SqlDbType.NVarChar, QuyCachDongGoi);
			db.AddInParameter(dbCommand, "@TongSoLuongNhapKhau", SqlDbType.Decimal, TongSoLuongNhapKhau);
			db.AddInParameter(dbCommand, "@DonViTinhTongSoLuongNhapKhau", SqlDbType.VarChar, DonViTinhTongSoLuongNhapKhau);
			db.AddInParameter(dbCommand, "@KichCoCaThe", SqlDbType.NVarChar, KichCoCaThe);
			db.AddInParameter(dbCommand, "@TrongLuongTinh", SqlDbType.Decimal, TrongLuongTinh);
			db.AddInParameter(dbCommand, "@DonViTinhTrongLuong", SqlDbType.VarChar, DonViTinhTrongLuong);
			db.AddInParameter(dbCommand, "@TrongLuongCaBi", SqlDbType.Decimal, TrongLuongCaBi);
			db.AddInParameter(dbCommand, "@DonViTinhTrongLuongCaBi", SqlDbType.VarChar, DonViTinhTrongLuongCaBi);
			db.AddInParameter(dbCommand, "@SoLuongKiemDich", SqlDbType.Decimal, SoLuongKiemDich);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongKiemDich", SqlDbType.VarChar, DonViTinhSoLuongKiemDich);
			db.AddInParameter(dbCommand, "@LoaiBaoBi", SqlDbType.NVarChar, LoaiBaoBi);
			db.AddInParameter(dbCommand, "@MucDichSuDung", SqlDbType.NVarChar, MucDichSuDung);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACC_HangGiayPhep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACC_HangGiayPhep(long id, string giayPhepType, long giayPhep_ID, string tenHangHoa, string maSoHangHoa, decimal soLuong, string donVitinhSoLuong, decimal khoiLuong, string donVitinhKhoiLuong, string xuatXu, string tinhBiet, decimal tuoi, string noiSanXuat, string quyCachDongGoi, decimal tongSoLuongNhapKhau, string donViTinhTongSoLuongNhapKhau, string kichCoCaThe, decimal trongLuongTinh, string donViTinhTrongLuong, decimal trongLuongCaBi, string donViTinhTrongLuongCaBi, decimal soLuongKiemDich, string donViTinhSoLuongKiemDich, string loaiBaoBi, string mucDichSuDung, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangGiayPhep entity = new KDT_VNACC_HangGiayPhep();			
			entity.ID = id;
			entity.GiayPhepType = giayPhepType;
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TenHangHoa = tenHangHoa;
			entity.MaSoHangHoa = maSoHangHoa;
			entity.SoLuong = soLuong;
			entity.DonVitinhSoLuong = donVitinhSoLuong;
			entity.KhoiLuong = khoiLuong;
			entity.DonVitinhKhoiLuong = donVitinhKhoiLuong;
			entity.XuatXu = xuatXu;
			entity.TinhBiet = tinhBiet;
			entity.Tuoi = tuoi;
			entity.NoiSanXuat = noiSanXuat;
			entity.QuyCachDongGoi = quyCachDongGoi;
			entity.TongSoLuongNhapKhau = tongSoLuongNhapKhau;
			entity.DonViTinhTongSoLuongNhapKhau = donViTinhTongSoLuongNhapKhau;
			entity.KichCoCaThe = kichCoCaThe;
			entity.TrongLuongTinh = trongLuongTinh;
			entity.DonViTinhTrongLuong = donViTinhTrongLuong;
			entity.TrongLuongCaBi = trongLuongCaBi;
			entity.DonViTinhTrongLuongCaBi = donViTinhTrongLuongCaBi;
			entity.SoLuongKiemDich = soLuongKiemDich;
			entity.DonViTinhSoLuongKiemDich = donViTinhSoLuongKiemDich;
			entity.LoaiBaoBi = loaiBaoBi;
			entity.MucDichSuDung = mucDichSuDung;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACC_HangGiayPhep_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayPhepType", SqlDbType.VarChar, GiayPhepType);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaSoHangHoa", SqlDbType.VarChar, MaSoHangHoa);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, DonVitinhSoLuong);
			db.AddInParameter(dbCommand, "@KhoiLuong", SqlDbType.Decimal, KhoiLuong);
			db.AddInParameter(dbCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, DonVitinhKhoiLuong);
			db.AddInParameter(dbCommand, "@XuatXu", SqlDbType.VarChar, XuatXu);
			db.AddInParameter(dbCommand, "@TinhBiet", SqlDbType.VarChar, TinhBiet);
			db.AddInParameter(dbCommand, "@Tuoi", SqlDbType.Decimal, Tuoi);
			db.AddInParameter(dbCommand, "@NoiSanXuat", SqlDbType.NVarChar, NoiSanXuat);
			db.AddInParameter(dbCommand, "@QuyCachDongGoi", SqlDbType.NVarChar, QuyCachDongGoi);
			db.AddInParameter(dbCommand, "@TongSoLuongNhapKhau", SqlDbType.Decimal, TongSoLuongNhapKhau);
			db.AddInParameter(dbCommand, "@DonViTinhTongSoLuongNhapKhau", SqlDbType.VarChar, DonViTinhTongSoLuongNhapKhau);
			db.AddInParameter(dbCommand, "@KichCoCaThe", SqlDbType.NVarChar, KichCoCaThe);
			db.AddInParameter(dbCommand, "@TrongLuongTinh", SqlDbType.Decimal, TrongLuongTinh);
			db.AddInParameter(dbCommand, "@DonViTinhTrongLuong", SqlDbType.VarChar, DonViTinhTrongLuong);
			db.AddInParameter(dbCommand, "@TrongLuongCaBi", SqlDbType.Decimal, TrongLuongCaBi);
			db.AddInParameter(dbCommand, "@DonViTinhTrongLuongCaBi", SqlDbType.VarChar, DonViTinhTrongLuongCaBi);
			db.AddInParameter(dbCommand, "@SoLuongKiemDich", SqlDbType.Decimal, SoLuongKiemDich);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongKiemDich", SqlDbType.VarChar, DonViTinhSoLuongKiemDich);
			db.AddInParameter(dbCommand, "@LoaiBaoBi", SqlDbType.NVarChar, LoaiBaoBi);
			db.AddInParameter(dbCommand, "@MucDichSuDung", SqlDbType.NVarChar, MucDichSuDung);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACC_HangGiayPhep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACC_HangGiayPhep(long id, string giayPhepType, long giayPhep_ID, string tenHangHoa, string maSoHangHoa, decimal soLuong, string donVitinhSoLuong, decimal khoiLuong, string donVitinhKhoiLuong, string xuatXu, string tinhBiet, decimal tuoi, string noiSanXuat, string quyCachDongGoi, decimal tongSoLuongNhapKhau, string donViTinhTongSoLuongNhapKhau, string kichCoCaThe, decimal trongLuongTinh, string donViTinhTrongLuong, decimal trongLuongCaBi, string donViTinhTrongLuongCaBi, decimal soLuongKiemDich, string donViTinhSoLuongKiemDich, string loaiBaoBi, string mucDichSuDung, string notes, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACC_HangGiayPhep entity = new KDT_VNACC_HangGiayPhep();			
			entity.ID = id;
			entity.GiayPhepType = giayPhepType;
			entity.GiayPhep_ID = giayPhep_ID;
			entity.TenHangHoa = tenHangHoa;
			entity.MaSoHangHoa = maSoHangHoa;
			entity.SoLuong = soLuong;
			entity.DonVitinhSoLuong = donVitinhSoLuong;
			entity.KhoiLuong = khoiLuong;
			entity.DonVitinhKhoiLuong = donVitinhKhoiLuong;
			entity.XuatXu = xuatXu;
			entity.TinhBiet = tinhBiet;
			entity.Tuoi = tuoi;
			entity.NoiSanXuat = noiSanXuat;
			entity.QuyCachDongGoi = quyCachDongGoi;
			entity.TongSoLuongNhapKhau = tongSoLuongNhapKhau;
			entity.DonViTinhTongSoLuongNhapKhau = donViTinhTongSoLuongNhapKhau;
			entity.KichCoCaThe = kichCoCaThe;
			entity.TrongLuongTinh = trongLuongTinh;
			entity.DonViTinhTrongLuong = donViTinhTrongLuong;
			entity.TrongLuongCaBi = trongLuongCaBi;
			entity.DonViTinhTrongLuongCaBi = donViTinhTrongLuongCaBi;
			entity.SoLuongKiemDich = soLuongKiemDich;
			entity.DonViTinhSoLuongKiemDich = donViTinhSoLuongKiemDich;
			entity.LoaiBaoBi = loaiBaoBi;
			entity.MucDichSuDung = mucDichSuDung;
			entity.Notes = notes;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GiayPhepType", SqlDbType.VarChar, GiayPhepType);
			db.AddInParameter(dbCommand, "@GiayPhep_ID", SqlDbType.BigInt, GiayPhep_ID);
			db.AddInParameter(dbCommand, "@TenHangHoa", SqlDbType.NVarChar, TenHangHoa);
			db.AddInParameter(dbCommand, "@MaSoHangHoa", SqlDbType.VarChar, MaSoHangHoa);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@DonVitinhSoLuong", SqlDbType.VarChar, DonVitinhSoLuong);
			db.AddInParameter(dbCommand, "@KhoiLuong", SqlDbType.Decimal, KhoiLuong);
			db.AddInParameter(dbCommand, "@DonVitinhKhoiLuong", SqlDbType.VarChar, DonVitinhKhoiLuong);
			db.AddInParameter(dbCommand, "@XuatXu", SqlDbType.VarChar, XuatXu);
			db.AddInParameter(dbCommand, "@TinhBiet", SqlDbType.VarChar, TinhBiet);
			db.AddInParameter(dbCommand, "@Tuoi", SqlDbType.Decimal, Tuoi);
			db.AddInParameter(dbCommand, "@NoiSanXuat", SqlDbType.NVarChar, NoiSanXuat);
			db.AddInParameter(dbCommand, "@QuyCachDongGoi", SqlDbType.NVarChar, QuyCachDongGoi);
			db.AddInParameter(dbCommand, "@TongSoLuongNhapKhau", SqlDbType.Decimal, TongSoLuongNhapKhau);
			db.AddInParameter(dbCommand, "@DonViTinhTongSoLuongNhapKhau", SqlDbType.VarChar, DonViTinhTongSoLuongNhapKhau);
			db.AddInParameter(dbCommand, "@KichCoCaThe", SqlDbType.NVarChar, KichCoCaThe);
			db.AddInParameter(dbCommand, "@TrongLuongTinh", SqlDbType.Decimal, TrongLuongTinh);
			db.AddInParameter(dbCommand, "@DonViTinhTrongLuong", SqlDbType.VarChar, DonViTinhTrongLuong);
			db.AddInParameter(dbCommand, "@TrongLuongCaBi", SqlDbType.Decimal, TrongLuongCaBi);
			db.AddInParameter(dbCommand, "@DonViTinhTrongLuongCaBi", SqlDbType.VarChar, DonViTinhTrongLuongCaBi);
			db.AddInParameter(dbCommand, "@SoLuongKiemDich", SqlDbType.Decimal, SoLuongKiemDich);
			db.AddInParameter(dbCommand, "@DonViTinhSoLuongKiemDich", SqlDbType.VarChar, DonViTinhSoLuongKiemDich);
			db.AddInParameter(dbCommand, "@LoaiBaoBi", SqlDbType.NVarChar, LoaiBaoBi);
			db.AddInParameter(dbCommand, "@MucDichSuDung", SqlDbType.NVarChar, MucDichSuDung);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACC_HangGiayPhep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACC_HangGiayPhep(long id)
		{
			KDT_VNACC_HangGiayPhep entity = new KDT_VNACC_HangGiayPhep();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACC_HangGiayPhep_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACC_HangGiayPhep> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACC_HangGiayPhep item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}