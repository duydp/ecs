﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS;

namespace Company.KDT.SHARE.VNACCS.Messages.Recived
{
   public partial class VAD8020 :BasicVNACC
    {
       public List<VAD8020_HANG> HangHoa { get; set; }
       public void LoadVAD8020(string strResult)
       {
           try
           {
               this.GetObject<VAD8020>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAD8020, false, VAD8020.TongSoByte);
               strResult = HelperVNACCS.SubStringByBytes(strResult, VAD8020.TongSoByte);
               while (true)
               {
                   VAD8020_HANG ivaHang = new VAD8020_HANG();
                   ivaHang.GetObject<VAD8020_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAD8020_HANG", false, VAD8020_HANG.TongSoByte);
                   if (this.HangHoa == null) this.HangHoa = new List<VAD8020_HANG>();
                   this.HangHoa.Add(ivaHang);
                   if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAD8020_HANG.TongSoByte)
                   {
                       if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAD8020_HANG.TongSoByte > 4)
                       {
                           strResult = HelperVNACCS.SubStringByBytes(strResult, VAD8020_HANG.TongSoByte);
                           continue;
                       }
                   }
                   break;
               }
           }
           catch (System.Exception ex)
           {
               throw ex;
           }



       }
    }
}
