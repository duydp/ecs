namespace Company.Interface.Report.SXXK
{
    partial class BangKeToKhaiNhapDuaVaoThanhKhoan_TT38
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.xrTable_DuLieu = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_DT_SoToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_DT_TenHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_DT_DVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_DT_LuongNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_DT_LuongDuaVaoThanhKhoan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_DT_LuongTon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_DT_ThueKhongThu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_DT_HopDongNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_DT_SoChungTuThanhToan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_DT_TriGiaThanhToan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_DT_GhiChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.TopMargin = new DevExpress.XtraReports.UI.TopMarginBand();
            this.BottomMargin = new DevExpress.XtraReports.UI.BottomMarginBand();
            this.ReportHeader = new DevExpress.XtraReports.UI.ReportHeaderBand();
            this.xrTable_TenCot = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell_SoToKhai = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_TenHang = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_DVT = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_LuongNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_LuongThanhKhoan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_LuongTon = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_ThueKhongThu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_HopDongNhapKhau = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_SoChungTuThanhToan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_TriGiaThanhToan = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell_GhiChu = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrLabelTenBangKe = new DevExpress.XtraReports.UI.XRLabel();
            this.GroupHeader1 = new DevExpress.XtraReports.UI.GroupHeaderBand();
            this.xrTable_SoThuTuCot = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.xrTableCell1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell10 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableCell11 = new DevExpress.XtraReports.UI.XRTableCell();
            this.ReportFooter = new DevExpress.XtraReports.UI.ReportFooterBand();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_DuLieu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_TenCot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_SoThuTuCot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_DuLieu});
            this.Detail.HeightF = 23.95834F;
            this.Detail.Name = "Detail";
            this.Detail.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // xrTable_DuLieu
            // 
            this.xrTable_DuLieu.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_DuLieu.Font = new System.Drawing.Font("Times New Roman", 9.75F);
            this.xrTable_DuLieu.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable_DuLieu.Name = "xrTable_DuLieu";
            this.xrTable_DuLieu.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow3});
            this.xrTable_DuLieu.SizeF = new System.Drawing.SizeF(1019F, 23.95834F);
            this.xrTable_DuLieu.StylePriority.UseBorders = false;
            this.xrTable_DuLieu.StylePriority.UseFont = false;
            this.xrTable_DuLieu.StylePriority.UseTextAlignment = false;
            this.xrTable_DuLieu.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_DT_SoToKhai,
            this.xrTableCell_DT_TenHang,
            this.xrTableCell_DT_DVT,
            this.xrTableCell_DT_LuongNhapKhau,
            this.xrTableCell_DT_LuongDuaVaoThanhKhoan,
            this.xrTableCell_DT_LuongTon,
            this.xrTableCell_DT_ThueKhongThu,
            this.xrTableCell_DT_HopDongNhapKhau,
            this.xrTableCell_DT_SoChungTuThanhToan,
            this.xrTableCell_DT_TriGiaThanhToan,
            this.xrTableCell_DT_GhiChu});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Weight = 1;
            // 
            // xrTableCell_DT_SoToKhai
            // 
            this.xrTableCell_DT_SoToKhai.Name = "xrTableCell_DT_SoToKhai";
            this.xrTableCell_DT_SoToKhai.Text = "(1)";
            this.xrTableCell_DT_SoToKhai.Weight = 0.33449870650191926;
            // 
            // xrTableCell_DT_TenHang
            // 
            this.xrTableCell_DT_TenHang.Name = "xrTableCell_DT_TenHang";
            this.xrTableCell_DT_TenHang.Text = "(2)";
            this.xrTableCell_DT_TenHang.Weight = 0.54744577185094534;
            // 
            // xrTableCell_DT_DVT
            // 
            this.xrTableCell_DT_DVT.Name = "xrTableCell_DT_DVT";
            this.xrTableCell_DT_DVT.Text = "(3)";
            this.xrTableCell_DT_DVT.Weight = 0.11805552164713543;
            // 
            // xrTableCell_DT_LuongNhapKhau
            // 
            this.xrTableCell_DT_LuongNhapKhau.Name = "xrTableCell_DT_LuongNhapKhau";
            this.xrTableCell_DT_LuongNhapKhau.Text = "(4)";
            this.xrTableCell_DT_LuongNhapKhau.Weight = 0.20070390655747189;
            // 
            // xrTableCell_DT_LuongDuaVaoThanhKhoan
            // 
            this.xrTableCell_DT_LuongDuaVaoThanhKhoan.Name = "xrTableCell_DT_LuongDuaVaoThanhKhoan";
            this.xrTableCell_DT_LuongDuaVaoThanhKhoan.Text = "(5)";
            this.xrTableCell_DT_LuongDuaVaoThanhKhoan.Weight = 0.30086699770602038;
            // 
            // xrTableCell_DT_LuongTon
            // 
            this.xrTableCell_DT_LuongTon.Name = "xrTableCell_DT_LuongTon";
            this.xrTableCell_DT_LuongTon.Text = "(6)";
            this.xrTableCell_DT_LuongTon.Weight = 0.24612213394319779;
            // 
            // xrTableCell_DT_ThueKhongThu
            // 
            this.xrTableCell_DT_ThueKhongThu.Name = "xrTableCell_DT_ThueKhongThu";
            this.xrTableCell_DT_ThueKhongThu.Text = "(7)";
            this.xrTableCell_DT_ThueKhongThu.Weight = 0.25757835265893658;
            // 
            // xrTableCell_DT_HopDongNhapKhau
            // 
            this.xrTableCell_DT_HopDongNhapKhau.Name = "xrTableCell_DT_HopDongNhapKhau";
            this.xrTableCell_DT_HopDongNhapKhau.Text = "(8)";
            this.xrTableCell_DT_HopDongNhapKhau.Weight = 0.20001891256275617;
            // 
            // xrTableCell_DT_SoChungTuThanhToan
            // 
            this.xrTableCell_DT_SoChungTuThanhToan.Name = "xrTableCell_DT_SoChungTuThanhToan";
            this.xrTableCell_DT_SoChungTuThanhToan.Text = "(9)";
            this.xrTableCell_DT_SoChungTuThanhToan.Weight = 0.23981174392874893;
            // 
            // xrTableCell_DT_TriGiaThanhToan
            // 
            this.xrTableCell_DT_TriGiaThanhToan.Name = "xrTableCell_DT_TriGiaThanhToan";
            this.xrTableCell_DT_TriGiaThanhToan.Text = "(10)";
            this.xrTableCell_DT_TriGiaThanhToan.Weight = 0.20873879639510434;
            // 
            // xrTableCell_DT_GhiChu
            // 
            this.xrTableCell_DT_GhiChu.Name = "xrTableCell_DT_GhiChu";
            this.xrTableCell_DT_GhiChu.Text = "(11)";
            this.xrTableCell_DT_GhiChu.Weight = 0.34615915624776383;
            // 
            // TopMargin
            // 
            this.TopMargin.HeightF = 21F;
            this.TopMargin.Name = "TopMargin";
            this.TopMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // BottomMargin
            // 
            this.BottomMargin.HeightF = 0F;
            this.BottomMargin.Name = "BottomMargin";
            this.BottomMargin.Padding = new DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100F);
            this.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft;
            // 
            // ReportHeader
            // 
            this.ReportHeader.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_TenCot,
            this.xrLabelTenBangKe});
            this.ReportHeader.HeightF = 137.5F;
            this.ReportHeader.Name = "ReportHeader";
            // 
            // xrTable_TenCot
            // 
            this.xrTable_TenCot.Borders = ((DevExpress.XtraPrinting.BorderSide)((((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Top)
                        | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_TenCot.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTable_TenCot.LocationFloat = new DevExpress.Utils.PointFloat(0F, 85.41666F);
            this.xrTable_TenCot.Name = "xrTable_TenCot";
            this.xrTable_TenCot.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1});
            this.xrTable_TenCot.SizeF = new System.Drawing.SizeF(1019F, 52.08334F);
            this.xrTable_TenCot.StylePriority.UseBorders = false;
            this.xrTable_TenCot.StylePriority.UseFont = false;
            this.xrTable_TenCot.StylePriority.UseTextAlignment = false;
            this.xrTable_TenCot.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell_SoToKhai,
            this.xrTableCell_TenHang,
            this.xrTableCell_DVT,
            this.xrTableCell_LuongNhapKhau,
            this.xrTableCell_LuongThanhKhoan,
            this.xrTableCell_LuongTon,
            this.xrTableCell_ThueKhongThu,
            this.xrTableCell_HopDongNhapKhau,
            this.xrTableCell_SoChungTuThanhToan,
            this.xrTableCell_TriGiaThanhToan,
            this.xrTableCell_GhiChu});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Weight = 1;
            // 
            // xrTableCell_SoToKhai
            // 
            this.xrTableCell_SoToKhai.Name = "xrTableCell_SoToKhai";
            this.xrTableCell_SoToKhai.Text = "Số Tờ Khai";
            this.xrTableCell_SoToKhai.Weight = 0.33449870650191926;
            // 
            // xrTableCell_TenHang
            // 
            this.xrTableCell_TenHang.Name = "xrTableCell_TenHang";
            this.xrTableCell_TenHang.Text = "Tên hàng (Dòng/Tên/Mã NPL)";
            this.xrTableCell_TenHang.Weight = 0.54744577185094534;
            // 
            // xrTableCell_DVT
            // 
            this.xrTableCell_DVT.Name = "xrTableCell_DVT";
            this.xrTableCell_DVT.Text = "DVT";
            this.xrTableCell_DVT.Weight = 0.11805552164713543;
            // 
            // xrTableCell_LuongNhapKhau
            // 
            this.xrTableCell_LuongNhapKhau.Name = "xrTableCell_LuongNhapKhau";
            this.xrTableCell_LuongNhapKhau.Text = "Lượng NK";
            this.xrTableCell_LuongNhapKhau.Weight = 0.20070390655747189;
            // 
            // xrTableCell_LuongThanhKhoan
            // 
            this.xrTableCell_LuongThanhKhoan.Name = "xrTableCell_LuongThanhKhoan";
            this.xrTableCell_LuongThanhKhoan.Text = "Lượng đưa vào thanh khoản";
            this.xrTableCell_LuongThanhKhoan.Weight = 0.30086699770602038;
            // 
            // xrTableCell_LuongTon
            // 
            this.xrTableCell_LuongTon.Name = "xrTableCell_LuongTon";
            this.xrTableCell_LuongTon.Text = "Lượng tồn";
            this.xrTableCell_LuongTon.Weight = 0.24612213394319779;
            // 
            // xrTableCell_ThueKhongThu
            // 
            this.xrTableCell_ThueKhongThu.Name = "xrTableCell_ThueKhongThu";
            this.xrTableCell_ThueKhongThu.Text = "Số thuế đề nghị không thu";
            this.xrTableCell_ThueKhongThu.Weight = 0.25757835265893658;
            // 
            // xrTableCell_HopDongNhapKhau
            // 
            this.xrTableCell_HopDongNhapKhau.Name = "xrTableCell_HopDongNhapKhau";
            this.xrTableCell_HopDongNhapKhau.Text = "Hợp đồng NK";
            this.xrTableCell_HopDongNhapKhau.Weight = 0.20001891256275617;
            // 
            // xrTableCell_SoChungTuThanhToan
            // 
            this.xrTableCell_SoChungTuThanhToan.Name = "xrTableCell_SoChungTuThanhToan";
            this.xrTableCell_SoChungTuThanhToan.Text = "Số chứng từ thanh toán";
            this.xrTableCell_SoChungTuThanhToan.Weight = 0.23981174392874893;
            // 
            // xrTableCell_TriGiaThanhToan
            // 
            this.xrTableCell_TriGiaThanhToan.Name = "xrTableCell_TriGiaThanhToan";
            this.xrTableCell_TriGiaThanhToan.Text = "Trị giá thanh toán (USD)";
            this.xrTableCell_TriGiaThanhToan.Weight = 0.20873879639510434;
            // 
            // xrTableCell_GhiChu
            // 
            this.xrTableCell_GhiChu.Name = "xrTableCell_GhiChu";
            this.xrTableCell_GhiChu.Text = "Ghi chú";
            this.xrTableCell_GhiChu.Weight = 0.34615915624776383;
            // 
            // xrLabelTenBangKe
            // 
            this.xrLabelTenBangKe.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabelTenBangKe.LocationFloat = new DevExpress.Utils.PointFloat(128.4362F, 25.33332F);
            this.xrLabelTenBangKe.Name = "xrLabelTenBangKe";
            this.xrLabelTenBangKe.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabelTenBangKe.SizeF = new System.Drawing.SizeF(772.985F, 23F);
            this.xrLabelTenBangKe.StylePriority.UseFont = false;
            this.xrLabelTenBangKe.StylePriority.UseTextAlignment = false;
            this.xrLabelTenBangKe.Text = "BẢNG KÊ TỜ KHAI NHẬP KHẨU ĐƯA VÀO THANH KHOẢN";
            this.xrLabelTenBangKe.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // GroupHeader1
            // 
            this.GroupHeader1.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.xrTable_SoThuTuCot});
            this.GroupHeader1.HeightF = 23.95834F;
            this.GroupHeader1.Name = "GroupHeader1";
            // 
            // xrTable_SoThuTuCot
            // 
            this.xrTable_SoThuTuCot.Borders = ((DevExpress.XtraPrinting.BorderSide)(((DevExpress.XtraPrinting.BorderSide.Left | DevExpress.XtraPrinting.BorderSide.Right)
                        | DevExpress.XtraPrinting.BorderSide.Bottom)));
            this.xrTable_SoThuTuCot.Font = new System.Drawing.Font("Times New Roman", 9.75F, System.Drawing.FontStyle.Bold);
            this.xrTable_SoThuTuCot.LocationFloat = new DevExpress.Utils.PointFloat(0F, 0F);
            this.xrTable_SoThuTuCot.Name = "xrTable_SoThuTuCot";
            this.xrTable_SoThuTuCot.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow2});
            this.xrTable_SoThuTuCot.SizeF = new System.Drawing.SizeF(1019F, 23.95834F);
            this.xrTable_SoThuTuCot.StylePriority.UseBorders = false;
            this.xrTable_SoThuTuCot.StylePriority.UseFont = false;
            this.xrTable_SoThuTuCot.StylePriority.UseTextAlignment = false;
            this.xrTable_SoThuTuCot.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.xrTableCell1,
            this.xrTableCell2,
            this.xrTableCell3,
            this.xrTableCell4,
            this.xrTableCell5,
            this.xrTableCell6,
            this.xrTableCell7,
            this.xrTableCell8,
            this.xrTableCell9,
            this.xrTableCell10,
            this.xrTableCell11});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Weight = 1;
            // 
            // xrTableCell1
            // 
            this.xrTableCell1.Name = "xrTableCell1";
            this.xrTableCell1.Text = "(1)";
            this.xrTableCell1.Weight = 0.33449870650191926;
            // 
            // xrTableCell2
            // 
            this.xrTableCell2.Name = "xrTableCell2";
            this.xrTableCell2.Text = "(2)";
            this.xrTableCell2.Weight = 0.54744577185094534;
            // 
            // xrTableCell3
            // 
            this.xrTableCell3.Name = "xrTableCell3";
            this.xrTableCell3.Text = "(3)";
            this.xrTableCell3.Weight = 0.11805552164713543;
            // 
            // xrTableCell4
            // 
            this.xrTableCell4.Name = "xrTableCell4";
            this.xrTableCell4.Text = "(4)";
            this.xrTableCell4.Weight = 0.20070390655747189;
            // 
            // xrTableCell5
            // 
            this.xrTableCell5.Name = "xrTableCell5";
            this.xrTableCell5.Text = "(5)";
            this.xrTableCell5.Weight = 0.30086699770602038;
            // 
            // xrTableCell6
            // 
            this.xrTableCell6.Name = "xrTableCell6";
            this.xrTableCell6.Text = "(6)";
            this.xrTableCell6.Weight = 0.24612213394319779;
            // 
            // xrTableCell7
            // 
            this.xrTableCell7.Name = "xrTableCell7";
            this.xrTableCell7.Text = "(7)";
            this.xrTableCell7.Weight = 0.25757835265893658;
            // 
            // xrTableCell8
            // 
            this.xrTableCell8.Name = "xrTableCell8";
            this.xrTableCell8.Text = "(8)";
            this.xrTableCell8.Weight = 0.20001891256275617;
            // 
            // xrTableCell9
            // 
            this.xrTableCell9.Name = "xrTableCell9";
            this.xrTableCell9.Text = "(9)";
            this.xrTableCell9.Weight = 0.23981174392874893;
            // 
            // xrTableCell10
            // 
            this.xrTableCell10.Name = "xrTableCell10";
            this.xrTableCell10.Text = "(10)";
            this.xrTableCell10.Weight = 0.20873879639510434;
            // 
            // xrTableCell11
            // 
            this.xrTableCell11.Name = "xrTableCell11";
            this.xrTableCell11.Text = "(11)";
            this.xrTableCell11.Weight = 0.34615915624776383;
            // 
            // ReportFooter
            // 
            this.ReportFooter.Name = "ReportFooter";
            // 
            // BangKeToKhaiNhapDuaVaoThanhKhoan_TT38
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail,
            this.TopMargin,
            this.BottomMargin,
            this.ReportHeader,
            this.GroupHeader1,
            this.ReportFooter});
            this.Landscape = true;
            this.Margins = new System.Drawing.Printing.Margins(38, 43, 21, 0);
            this.PageHeight = 850;
            this.PageWidth = 1100;
            this.Version = "10.2";
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_DuLieu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_TenCot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable_SoThuTuCot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.TopMarginBand TopMargin;
        private DevExpress.XtraReports.UI.BottomMarginBand BottomMargin;
        private DevExpress.XtraReports.UI.ReportHeaderBand ReportHeader;
        private DevExpress.XtraReports.UI.GroupHeaderBand GroupHeader1;
        private DevExpress.XtraReports.UI.XRLabel xrLabelTenBangKe;
        private DevExpress.XtraReports.UI.XRTable xrTable_TenCot;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_SoToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_SoChungTuThanhToan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_GhiChu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_TenHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DVT;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_LuongNhapKhau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_LuongThanhKhoan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_LuongTon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_ThueKhongThu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_HopDongNhapKhau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_TriGiaThanhToan;
        private DevExpress.XtraReports.UI.XRTable xrTable_SoThuTuCot;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell1;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell2;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell4;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell5;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell6;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell7;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell8;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell9;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell10;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell11;
        private DevExpress.XtraReports.UI.XRTable xrTable_DuLieu;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DT_SoToKhai;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DT_TenHang;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DT_DVT;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DT_LuongNhapKhau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DT_LuongDuaVaoThanhKhoan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DT_LuongTon;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DT_ThueKhongThu;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DT_HopDongNhapKhau;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DT_SoChungTuThanhToan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DT_TriGiaThanhToan;
        private DevExpress.XtraReports.UI.XRTableCell xrTableCell_DT_GhiChu;
        private DevExpress.XtraReports.UI.ReportFooterBand ReportFooter;
    }
}
