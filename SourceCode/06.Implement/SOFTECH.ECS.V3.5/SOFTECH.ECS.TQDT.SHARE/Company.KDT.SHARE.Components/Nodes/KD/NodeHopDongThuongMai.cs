﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components.Nodes.KD
{
    public class NodeHopDongThuongMai
    {
        /// <summary>
        /// Khai bao.
        /// </summary>
        public const string Declaration = "Declaration";
        /// <summary>
        /// Loại chứng từ (=315)
        /// </summary>
        public const string issuer = "issuer";
        /// <summary>
        /// Số tham chiếu chứng từ || Số TK || Số hợp đồng
        /// </summary>
        public const string reference = "reference";
        /// <summary>
        /// Ngày gửi chứng từ || Ngày TK || Ngàyhợp đồng
        /// </summary>
        public const string issue = "issue";
        /// <summary>
        /// Chức năng của chứng từ (=8)
        /// </summary>
        public const string function = "function";
        /// <summary>
        /// Nơi khai báo
        /// </summary>
        public const string issueLocation = "issueLocation";
        /// <summary>
        /// Trạng thái của chứng từ || trạng thái đại lý
        /// </summary>
        public const string status = "status";
        /// <summary>
        /// Số đăng ký chứng từ
        /// </summary>
        public const string customsReference = "customsReference";
        /// <summary>
        /// Ngày đăng ký chứng từ 
        /// </summary>
        public const string acceptance = "acceptance";
        /// <summary>
        /// Hải quan tiếp nhận chứng từ || Mã hải quan 
        /// </summary>
        public const string declarationOffice = "declarationOffice";
        /// <summary>
        /// Người khai hải quan
        /// </summary>
        public const string Agent = "Agent";
        /// <summary>
        /// Tên người khai hải quan || Tên doanh nghiệp || Tên người mua || Tên người bán
        /// </summary>
        public const string name = "name";
        /// <summary>
        /// Mã người khai hải quan || Mã doanh nghiệp || Mã người mua || Mã người bán
        /// </summary>
        public const string identity = "identity";
        /// <summary>
        /// Doanh nghiệp XNK
        /// </summary>
        public const string Importer = "Importer";
        /// <summary>
        /// 
        /// </summary>
        public const string importer = "importer";
        /// <summary>
        /// 
        /// </summary>
        public const string Identity = "Identity";
        /// <summary>
        /// Thông tin tham chiếu đến Tờ khai
        /// </summary>
        public const string DeclarationDocument = "DeclarationDocument";
        /// <summary>
        /// Mã LH
        /// </summary>
        public const string natureOfTransaction = "natureOfTransaction";        
        /// <summary>
        /// Trị giá
        /// </summary>
        public const string statisticalValue = "statisticalValue";
        /// <summary>
        /// Đồng tiền thanh toán
        /// </summary>
        public const string CurrencyExchange = "CurrencyExchange";
        /// <summary>
        /// Thông tin hàng hoá
        /// </summary>
        public const string Commodity = "Commodity";
        /// <summary>
        /// Tên hàng
        /// </summary>
        public const string description = "description";
        /// <summary>
        /// Mã tham chiếu hàng hóa
        /// </summary>
        public const string identification = "identification";
        /// <summary>
        /// Mã HS
        /// </summary>
        public const string tariffClassification = "tariffClassification";
        /// <summary>
        /// Quantity
        /// </summary>
        public const string GoodsMeasure = "GoodsMeasure";
        /// <summary>
        /// Số lượng
        /// </summary>
        public const string quantity = "quantity";
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public const string measureUnit = "measureUnit";
        /// <summary>
        /// Đồng tiền thanh toán
        /// </summary>
        public const string currencyType = "currencyType";        
        /// <summary>
        /// Xuất xứ
        /// </summary>
        public const string Origin = "Origin";
        /// <summary>
        /// Nước Xuất xứ
        /// </summary>
        public const string originCountry = "originCountry";        
        /// <summary>
        /// Người bán
        /// </summary>
        public const string Seller = "Seller";
        /// <summary>
        /// Người mua
        /// </summary>
        public const string Buyer = "Buyer";
        /// <summary>
        /// Phương thức thanh toán
        /// </summary>
        public const string Payment = "Payment";
        /// <summary>
        /// Phương thức thanh toán
        /// </summary>
        public const string method = "method";        
        /// <summary>
        /// Điều kiện giao hàng
        /// </summary>
        public const string TradeTerm = "TradeTerm";
        /// <summary>
        /// Điều kiện giao hàng
        /// </summary>
        public const string condition = "condition";
        /// <summary>
        /// Đơn giá
        /// </summary>
        public const string unitPrice = "unitPrice";        
        /// <summary>
        /// Tổng trị giá
        /// </summary>
        public const string totalValue = "totalValue";
        /// <summary>
        /// Địa điểm giao hàng
        /// </summary>
        public const string DeliveryDestination = "DeliveryDestination";
        /// <summary>
        /// Địa điểm giao hàng
        /// </summary>
        public const string line = "line";
        /// <summary>
        /// Thông tin về hợp đồng
        /// </summary>
        public const string ContractDocuments = "ContractDocuments";
        /// <summary>
        /// Thông tin về hợp đồng
        /// </summary>
        public const string ContractDocument = "ContractDocument";
        /// <summary>
        /// Thời hạn thanh toán
        /// </summary>
        public const string expire = "expire";
        /// <summary>
        /// Ghi chú khác về hàng hóa || Ghi chú khác
        /// </summary>
        public const string AdditionalInformation = "AdditionalInformation";
        /// <summary>
        /// Ghi chú khác về hàng hóa || Ghi chú khác
        /// </summary>
        public const string content = "content";
        /// <summary>
        /// Thông tin hàng hóa trên hợp đồng
        /// </summary>
        public const string ContractItem = "ContractItem";
        

    }
}
