using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACCS_Mapper : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string CodeV4 { set; get; }
		public string NameV4 { set; get; }
		public string CodeV5 { set; get; }
		public string NameV5 { set; get; }
		public string LoaiMapper { set; get; }
		public string Notes { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<VNACCS_Mapper> ConvertToCollection(IDataReader reader)
		{
			List<VNACCS_Mapper> collection = new List<VNACCS_Mapper>();
			while (reader.Read())
			{
				VNACCS_Mapper entity = new VNACCS_Mapper();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("CodeV4"))) entity.CodeV4 = reader.GetString(reader.GetOrdinal("CodeV4"));
				if (!reader.IsDBNull(reader.GetOrdinal("NameV4"))) entity.NameV4 = reader.GetString(reader.GetOrdinal("NameV4"));
				if (!reader.IsDBNull(reader.GetOrdinal("CodeV5"))) entity.CodeV5 = reader.GetString(reader.GetOrdinal("CodeV5"));
				if (!reader.IsDBNull(reader.GetOrdinal("NameV5"))) entity.NameV5 = reader.GetString(reader.GetOrdinal("NameV5"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiMapper"))) entity.LoaiMapper = reader.GetString(reader.GetOrdinal("LoaiMapper"));
				if (!reader.IsDBNull(reader.GetOrdinal("Notes"))) entity.Notes = reader.GetString(reader.GetOrdinal("Notes"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<VNACCS_Mapper> collection, long id)
        {
            foreach (VNACCS_Mapper item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_VNACCS_Mapper VALUES(@CodeV4, @NameV4, @CodeV5, @NameV5, @LoaiMapper, @Notes)";
            string update = "UPDATE t_VNACCS_Mapper SET CodeV4 = @CodeV4, NameV4 = @NameV4, CodeV5 = @CodeV5, NameV5 = @NameV5, LoaiMapper = @LoaiMapper, Notes = @Notes WHERE ID = @ID";
            string delete = "DELETE FROM t_VNACCS_Mapper WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CodeV4", SqlDbType.VarChar, "CodeV4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NameV4", SqlDbType.NVarChar, "NameV4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CodeV5", SqlDbType.VarChar, "CodeV5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NameV5", SqlDbType.NVarChar, "NameV5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiMapper", SqlDbType.VarChar, "LoaiMapper", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CodeV4", SqlDbType.VarChar, "CodeV4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NameV4", SqlDbType.NVarChar, "NameV4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CodeV5", SqlDbType.VarChar, "CodeV5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NameV5", SqlDbType.NVarChar, "NameV5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiMapper", SqlDbType.VarChar, "LoaiMapper", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_VNACCS_Mapper VALUES(@CodeV4, @NameV4, @CodeV5, @NameV5, @LoaiMapper, @Notes)";
            string update = "UPDATE t_VNACCS_Mapper SET CodeV4 = @CodeV4, NameV4 = @NameV4, CodeV5 = @CodeV5, NameV5 = @NameV5, LoaiMapper = @LoaiMapper, Notes = @Notes WHERE ID = @ID";
            string delete = "DELETE FROM t_VNACCS_Mapper WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CodeV4", SqlDbType.VarChar, "CodeV4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NameV4", SqlDbType.NVarChar, "NameV4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CodeV5", SqlDbType.VarChar, "CodeV5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NameV5", SqlDbType.NVarChar, "NameV5", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiMapper", SqlDbType.VarChar, "LoaiMapper", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CodeV4", SqlDbType.VarChar, "CodeV4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NameV4", SqlDbType.NVarChar, "NameV4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CodeV5", SqlDbType.VarChar, "CodeV5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NameV5", SqlDbType.NVarChar, "NameV5", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiMapper", SqlDbType.VarChar, "LoaiMapper", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Notes", SqlDbType.NVarChar, "Notes", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static VNACCS_Mapper Load(long id)
		{
			const string spName = "[dbo].[p_VNACCS_Mapper_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<VNACCS_Mapper> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<VNACCS_Mapper> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<VNACCS_Mapper> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_VNACCS_Mapper_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACCS_Mapper_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_VNACCS_Mapper_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_VNACCS_Mapper_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertVNACCS_Mapper(string codeV4, string nameV4, string codeV5, string nameV5, string loaiMapper, string notes)
		{
			VNACCS_Mapper entity = new VNACCS_Mapper();	
			entity.CodeV4 = codeV4;
			entity.NameV4 = nameV4;
			entity.CodeV5 = codeV5;
			entity.NameV5 = nameV5;
			entity.LoaiMapper = loaiMapper;
			entity.Notes = notes;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_VNACCS_Mapper_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@CodeV4", SqlDbType.VarChar, CodeV4);
			db.AddInParameter(dbCommand, "@NameV4", SqlDbType.NVarChar, NameV4);
			db.AddInParameter(dbCommand, "@CodeV5", SqlDbType.VarChar, CodeV5);
			db.AddInParameter(dbCommand, "@NameV5", SqlDbType.NVarChar, NameV5);
			db.AddInParameter(dbCommand, "@LoaiMapper", SqlDbType.VarChar, LoaiMapper);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<VNACCS_Mapper> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_Mapper item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateVNACCS_Mapper(long id, string codeV4, string nameV4, string codeV5, string nameV5, string loaiMapper, string notes)
		{
			VNACCS_Mapper entity = new VNACCS_Mapper();			
			entity.ID = id;
			entity.CodeV4 = codeV4;
			entity.NameV4 = nameV4;
			entity.CodeV5 = codeV5;
			entity.NameV5 = nameV5;
			entity.LoaiMapper = loaiMapper;
			entity.Notes = notes;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_VNACCS_Mapper_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@CodeV4", SqlDbType.VarChar, CodeV4);
			db.AddInParameter(dbCommand, "@NameV4", SqlDbType.NVarChar, NameV4);
			db.AddInParameter(dbCommand, "@CodeV5", SqlDbType.VarChar, CodeV5);
			db.AddInParameter(dbCommand, "@NameV5", SqlDbType.NVarChar, NameV5);
			db.AddInParameter(dbCommand, "@LoaiMapper", SqlDbType.VarChar, LoaiMapper);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<VNACCS_Mapper> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_Mapper item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateVNACCS_Mapper(long id, string codeV4, string nameV4, string codeV5, string nameV5, string loaiMapper, string notes)
		{
			VNACCS_Mapper entity = new VNACCS_Mapper();			
			entity.ID = id;
			entity.CodeV4 = codeV4;
			entity.NameV4 = nameV4;
			entity.CodeV5 = codeV5;
			entity.NameV5 = nameV5;
			entity.LoaiMapper = loaiMapper;
			entity.Notes = notes;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACCS_Mapper_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@CodeV4", SqlDbType.VarChar, CodeV4);
			db.AddInParameter(dbCommand, "@NameV4", SqlDbType.NVarChar, NameV4);
			db.AddInParameter(dbCommand, "@CodeV5", SqlDbType.VarChar, CodeV5);
			db.AddInParameter(dbCommand, "@NameV5", SqlDbType.NVarChar, NameV5);
			db.AddInParameter(dbCommand, "@LoaiMapper", SqlDbType.VarChar, LoaiMapper);
			db.AddInParameter(dbCommand, "@Notes", SqlDbType.NVarChar, Notes);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<VNACCS_Mapper> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_Mapper item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteVNACCS_Mapper(long id)
		{
			VNACCS_Mapper entity = new VNACCS_Mapper();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_VNACCS_Mapper_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_VNACCS_Mapper_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<VNACCS_Mapper> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (VNACCS_Mapper item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}