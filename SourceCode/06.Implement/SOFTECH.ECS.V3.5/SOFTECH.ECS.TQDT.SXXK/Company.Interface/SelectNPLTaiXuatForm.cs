using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.KDT.SXXK;
using Janus.Windows.GridEX;
using Company.BLL.SXXK;

namespace Company.Interface
{
    public partial class SelectNPLTaiXuatForm : BaseForm
    {
        public Company.BLL.SXXK.PhanBoToKhaiXuat pbToKhaiXuat = new Company.BLL.SXXK.PhanBoToKhaiXuat();
        public IList<Company.BLL.SXXK.PhanBoToKhaiXuat> pbToKhaiXuatCollection = new List<Company.BLL.SXXK.PhanBoToKhaiXuat>();
        public Company.BLL.SXXK.ToKhai.ToKhaiMauDich TKMD;

        private int SoToKhai;
        private string MaLoaiHinh;
        private DateTime NgayDangKy;
        private string TenNPL;
        private string DVT_ID;
        private decimal SoLuong;
        public bool isPhanBo = false;
        public SelectNPLTaiXuatForm()
        {
            InitializeComponent();
        }

        private void dgList1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
                this.MaLoaiHinh = e.Row.Cells["MaLoaiHinh"].Text;
                this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                this.TenNPL = e.Row.Cells["TenHang"].Text;
                this.SoLuong = Convert.ToDecimal(e.Row.Cells["Ton"].Value);
                this.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                txtSoToKhai.Text = e.Row.Cells["SoToKhai"].Text;
                txtMaNPL.Text = e.Row.Cells["MaNPL"].Text;
                txtLuong.Text = this.SoLuong.ToString();
                btnAdd.Enabled = true;
            }
        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (cvError.IsValid)
            {
                if (checkExit(this.SoToKhai, this.MaLoaiHinh, Convert.ToInt16(this.NgayDangKy.Year), this.MaHaiQuan.Trim(), txtMaNPL.Text))
                {
                   // ShowMessage("Nguyên phụ liệu của tờ khai này đã có, bạn hãy chọn nguyên phụ liệu khác.", false);
                    MLMessages("Nguyên phụ liệu của tờ khai này đã có, bạn hãy chọn nguyên phụ liệu khác.", "MSG_PUB24", "", false);
                    txtSoToKhai.Text = "";
                    txtLuong.Value = 0;
                    txtMaNPL.Text = "";
                    return;
                }
                if (this.SoLuong < Convert.ToDecimal(txtLuong.Value))
                {
                   // ShowMessage("Lượng tái xuất phải nhỏ hơn hoặc bằng lượng nguyên phụ liệu tồn của tờ khai.", false);
                    MLMessages("Lượng tái xuất phải nhỏ hơn hoặc bằng lượng nguyên phụ liệu tồn của tờ khai.", "MSG_PUB25", "", false);
                    txtLuong.Value = 0;
                    txtLuong.Focus();
                    return;
                }
                PhanBoToKhaiNhap pbTKNhap = new PhanBoToKhaiNhap();
                pbTKNhap.LuongPhanBo = Convert.ToDouble(txtLuong.Text);
                pbTKNhap.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                pbTKNhap.MaHaiQuanNhap = GlobalSettings.MA_HAI_QUAN;
                pbTKNhap.MaNPL = txtMaNPL.Text.Trim();
                pbTKNhap.NamDangKyNhap =(short) this.NgayDangKy.Year;
                pbTKNhap.SoToKhaiNhap = this.SoToKhai;
                pbTKNhap.MaLoaiHinhNhap = this.MaLoaiHinh;
                double TongLuongCungUng=0;
                foreach (PhanBoToKhaiNhap pbTKN in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
                {
                    if (pbTKN.MaNPL.Trim().ToUpper() == cbhang.SelectedValue.ToString())
                    {
                        TongLuongCungUng += pbTKN.LuongPhanBo;
                    }
                }
                if ((TongLuongCungUng + pbTKNhap.LuongPhanBo) > Convert.ToDouble(txtLuongTaiXuat.Text))
                {
                    // ShowMessage("Tổng lượng chọn trong các tờ khai nhập chưa bằng lượng tái xuất.", false);
                    MLMessages("Tổng lượng chọn trong các tờ khai nhập chưa bằng lượng tái xuất.", "MSG_PUB29", "", false);
                    return;
                }
                pbToKhaiXuat.PhanBoToKhaiNhapCollection.Add(pbTKNhap);
                dgLispbTKX.Refetch();
                txtSoToKhai.Text = "";
                txtMaNPL.Text = "";
                txtLuong.Value = 0;
            }
        }
        private bool checkExit(int soToKhai, string maLoaiHinh, short namDangKy, string maHaiQuan, string maNPL)
        {
            foreach (PhanBoToKhaiNhap nplTT in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
            {
                if (nplTT.SoToKhaiNhap == soToKhai && nplTT.MaLoaiHinhNhap == maLoaiHinh && nplTT.NamDangKyNhap == namDangKy && nplTT.MaHaiQuanNhap == maHaiQuan && nplTT.MaNPL == maNPL)
                    return true;
            }
            return false;
        }
        private void SelectPhanBoToKhaiXuatOfMaNPL(string MaNPL)
        {
            foreach (Company.BLL.SXXK.PhanBoToKhaiXuat pbTKX in pbToKhaiXuatCollection)
            {
                if (pbTKX.MaSP.Trim().ToUpper() == MaNPL.Trim().ToUpper())
                {
                    pbToKhaiXuat = pbTKX;
                    txtLuongTaiXuat.Text = pbToKhaiXuat.SoLuongXuat.ToString();
                    return;
                }
            }
        }
        private void BK07Form_Load(object sender, EventArgs e)
        {
            txtLuongTaiXuat.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            txtLuong.DecimalDigits = GlobalSettings.SoThapPhan.LuongNPL;
            dgListTon.Tables[0].Columns["Ton"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["Ton"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongTonDau"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongPhanBo"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            dgLispbTKX.Tables[0].Columns["LuongTonCuoi"].FormatString = "N" + GlobalSettings.SoThapPhan.LuongNPL;
            
            if (TKMD.HMDCollection.Count == 0)
            {
                TKMD.LoadHMDCollection();
            }
            cbhang.Items.Clear();
            foreach (Company.BLL.SXXK.ToKhai.HangMauDich HMD in TKMD.HMDCollection)
            {
                cbhang.Items.Add(HMD.TenHang, HMD.MaPhu);
            }
            cbhang.SelectedIndex = 0;
            SelectPhanBoToKhaiXuatOfMaNPL(cbhang.SelectedValue.ToString());
            BindData();
            if (GlobalSettings.NGON_NGU == "0")
            {
                this.Text = "Phân bổ cho tờ khai tái xuất số : " + TKMD.SoToKhai + "/" + TKMD.MaLoaiHinh + "/" + TKMD.NamDangKy;
            }
            else
            {
                this.Text = "Allocate to re-export delcaration number : " + TKMD.SoToKhai + "/" + TKMD.MaLoaiHinh + "/" + TKMD.NamDangKy;
            }
        }
        private void BindData()
        {
            DataTable NPLNhapTonThucTeCollection = Company.BLL.SXXK.NPLNhapTonThucTe.SelectBy_MaHaiQuan_AND_MaDoanhNghiepAndMaNPLAndTonHonO(GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI, cbhang.SelectedValue.ToString(), TKMD.NgayDangKy, GlobalSettings.SoThapPhan.LuongNPL).Tables[0];
            foreach (Company.BLL.SXXK.PhanBoToKhaiNhap pbTKN in pbToKhaiXuat.PhanBoToKhaiNhapCollection)
            {
                DataRow rowTon = NPLNhapTonThucTeCollection.Select("SoToKhai=" + pbTKN.SoToKhaiNhap + " and MaLoaiHinh='" + pbTKN.MaLoaiHinhNhap + "' and NamDangKy=" + pbTKN.NamDangKyNhap + " and MaHaiQuan='" + pbTKN.MaHaiQuanNhap + "' and MaNPL='" + pbTKN.MaNPL.Trim()+ "'")[0];
                rowTon["Ton"] = (Convert.ToDouble(rowTon["Ton"]) - pbTKN.LuongPhanBo);                
            }
            dgListTon.DataSource =NPLNhapTonThucTeCollection;
            dgLispbTKX.DataSource = pbToKhaiXuat.PhanBoToKhaiNhapCollection;
        }
        private bool KiemTraLuongTaiXuat(string maNPL, IList<PhanBoToKhaiNhap> PhanBoToKhaiNhapCollection,double LuongTaiXuat)
        {
            double TongTaixuat=0;
            foreach (PhanBoToKhaiNhap pbTKN in PhanBoToKhaiNhapCollection)
            {
                TongTaixuat += pbTKN.LuongPhanBo;
            }
            if (TongTaixuat != LuongTaiXuat)
                return false;
            return true; 
        }
        private void cmdSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (pbToKhaiXuat.PhanBoToKhaiNhapCollection.Count == 0)
                {
                    throw new Exception("Bạn chưa chọn phân bổ tờ khai nhập cho mặt hàng này.");
                }
                foreach(Company.BLL.SXXK.PhanBoToKhaiXuat pbToKhaiX in pbToKhaiXuatCollection)
                {
                    if(!KiemTraLuongTaiXuat(pbToKhaiX.MaSP,pbToKhaiX.PhanBoToKhaiNhapCollection,Convert.ToDouble(pbToKhaiX.SoLuongXuat)))
                    {
                        //ShowMessage("Lượng tái xuất được chọn trong các tờ khai nhập chưa bằng lượng tái xuất.",false);
                        MLMessages("Lượng tái xuất được chọn trong các tờ khai nhập chưa bằng lượng tái xuất.", "MSG_PUB25", "", false);
                        return;
                    }
                }
                this.Cursor = Cursors.WaitCursor;
                Company.BLL.SXXK.PhanBoToKhaiXuat pbTKX = new PhanBoToKhaiXuat();
                pbTKX.SoToKhaiXuat = this.TKMD.SoToKhai;
                pbTKX.MaLoaiHinhXuat = this.TKMD.MaLoaiHinh;
                pbTKX.MaHaiQuanXuat = TKMD.MaHaiQuan.Trim();
                pbTKX.NamDangKyXuat = (short)TKMD.NgayDangKy.Year;
                pbTKX.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                pbTKX.PhanBoChoToKhaiTaiXuat(pbToKhaiXuatCollection, GlobalSettings.SoThapPhan.LuongNPL);
                //ShowMessage("Thực hiện phân bổ thành công.", false);
                MLMessages("Thực hiện phân bổ thành công.", "MSG_PUB30", "", false);
                isPhanBo = true;
                this.Close();
            }
            catch (Exception ex)
            {
                //ShowMessage(" " + ex.Message, false);
                MLMessages(" Lỗi " + ex.Message, "MSG_PUB29", "", false);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }

        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["SoToKhai"].Text = e.Row.Cells["SoToKhai"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value) + "/" + e.Row.Cells["NamDangKy"].Text;
                Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                NPL.Ma = e.Row.Cells["MaNPL"].Text.Trim();
                NPL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                NPL.Load();
                e.Row.Cells["TenHang"].Text = NPL.Ten;
            }
        }

        private void dgList2_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            e.Row.Cells["SoToKhaiNhap"].Text = e.Row.Cells["SoToKhaiNhap"].Text + "/" + LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinhNhap"].Value) + "/" + e.Row.Cells["NamDangKyNhap"].Text;
        }

        private void dgList2_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            txtSoToKhai.Text = e.Row.Cells["SoToKhai"].Text;
            this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
            this.MaLoaiHinh = Convert.ToString(e.Row.Cells["MaLoaiHinh"].Value);
            this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
            this.TenNPL = e.Row.Cells["TenNPL"].Text;
            this.DVT_ID = e.Row.Cells["DVT_ID"].Text;
            this.MaHaiQuan = e.Row.Cells["MaHaiQuan"].Text;
            txtMaNPL.Text = e.Row.Cells["MaNPL"].Text;
            txtLuong.Value = Convert.ToDecimal(e.Row.Cells["LuongNopThue"].Value);
            btnAdd.Enabled = false;
            this.SoLuong = Convert.ToDecimal(e.Row.Cells["Ton"].Text);

        }

      
        private void cbhang_SelectedIndexChanged(object sender, EventArgs e)
        {            
            SelectPhanBoToKhaiXuatOfMaNPL(cbhang.SelectedValue.ToString());
            BindData();
        }

    }
}