using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;
using BarcodeLib;
namespace Company.Interface.Report.VNACCS
{
    public partial class ReportViewVNACCSToKhaiXuatKhau2 : DevExpress.XtraReports.UI.XtraReport
    {
        int count;
        public ReportViewVNACCSToKhaiXuatKhau2()
        {
            InitializeComponent();
        }
        public Image GenerateBarCode(string Text)
        {
            BarcodeLib.Barcode barcode = new BarcodeLib.Barcode()
            {
                IncludeLabel = false,
                Alignment = AlignmentPositions.CENTER,
                Width = 300,
                Height = 30,
                RotateFlipType = RotateFlipType.RotateNoneFlipNone,
                BackColor = Color.White,
                ForeColor = Color.Black,
            };

            Image img = barcode.Encode(TYPE.CODE128B, Text);
            return img;
        }
        public void BindReport(KDT_VNACC_ToKhaiMauDich tkmd)
        {
            try
            {
                //lblSoTrang.Text = "2/" + Vae.K69.GetValue().ToString();
                pictureBox1.Image = GenerateBarCode(tkmd.SoToKhai.ToString());
                count = tkmd.HangCollection.Count;
                lblTenThongTinXuat.Text = "Tờ khai hàng hóa xuất khẩu (thông quan)";
                int HangHoa = tkmd.HangCollection.Count;
                if (HangHoa < 2)
                {
                    lblSoTrang.Text = "2/" + System.Convert.ToDecimal(3).ToString();
                }
                else
                {
                    lblSoTrang.Text = "2/" + System.Convert.ToDecimal(2 + Math.Round((decimal)HangHoa / 2, 0, MidpointRounding.AwayFromZero)).ToString();
                }
                lblSotokhai.Text = tkmd.SoToKhai.ToString();
                lblSotokhaidautien.Text = String.IsNullOrEmpty(tkmd.SoToKhaiDauTien) ? "" : tkmd.SoToKhaiDauTien.ToString();
                lblSonhanhtokhaichianho.Text = tkmd.SoNhanhToKhai.ToString();
                lblTongsotokhaichianho.Text = tkmd.TongSoTKChiaNho.ToString();
                lblSotokhaitamnhaptaixuat.Text = tkmd.SoToKhaiTNTX.ToString();
                lblMaphanloaikiemtra.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiKiemTra) ? "" : tkmd.MaPhanLoaiKiemTra.ToString();
                lblMaloaihinh.Text = String.IsNullOrEmpty(tkmd.MaLoaiHinh) ? "" : tkmd.MaLoaiHinh.ToString();
                lblMaphanloaihanghoa.Text = String.IsNullOrEmpty(tkmd.MaPhanLoaiHH) ? "" : tkmd.MaPhanLoaiHH.ToString();
                lblMahieuphuongthucvanchuyen.Text = String.IsNullOrEmpty(tkmd.MaPhuongThucVT) ? "" : tkmd.MaPhuongThucVT.ToString();
                lblMasothuedaidien.Text = String.IsNullOrEmpty(tkmd.MaSoThueDaiDien) ? "" : tkmd.MaSoThueDaiDien.ToString();
                lblTencoquanhaiquantiepnhantokhai.Text = String.IsNullOrEmpty(tkmd.TenCoQuanHaiQuan) ? "" : tkmd.TenCoQuanHaiQuan.ToString();
                lblNhomxulyhoso.Text = String.IsNullOrEmpty(tkmd.NhomXuLyHS) ? "" : tkmd.NhomXuLyHS.ToString();
                if
                 (tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgaydangky.Text = tkmd.NgayDangKy.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblNgaydangky.Text = "";
                }

                if (tkmd.NgayDangKy.Hour.ToString() != "" && tkmd.NgayDangKy.Hour.ToString() != "0")
                {
                    lblGiodangky.Text = tkmd.NgayDangKy.Hour.ToString() + ":" + tkmd.NgayDangKy.Minute.ToString() + ":" + tkmd.NgayDangKy.Second.ToString();
                }
                else
                {
                    lblGiodangky.Text = "";
                }
                if
                    (tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblNgaythaydoidangky.Text = tkmd.NgayThayDoiDangKy.ToString("dd/MM/yyyy");

                }
                else
                {
                    lblNgaythaydoidangky.Text = "";
                }

                if (tkmd.NgayThayDoiDangKy.Hour.ToString() != "" && tkmd.NgayThayDoiDangKy.Hour.ToString() != "0")
                {
                    lblGiothaydoidangky.Text = tkmd.NgayThayDoiDangKy.Hour.ToString() + ":" + tkmd.NgayThayDoiDangKy.Minute.ToString() + ":" + tkmd.NgayThayDoiDangKy.Second.ToString();
                }
                else
                {
                    lblGiothaydoidangky.Text = "";
                }
                if
                    (tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/1900" && tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy") != "01/01/0001")
                {
                    lblThoihantainhapxuat.Text = tkmd.ThoiHanTaiNhapTaiXuat.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblThoihantainhapxuat.Text = "";
                }

                lblBieuthitruonghophethan.Text = String.IsNullOrEmpty(tkmd.BieuThiTruongHopHetHan) ? "" : tkmd.BieuThiTruongHopHetHan.ToString();
                //lblNgaydukienden.Text = Convert.ToDateTime(Vae.ADT.GetValue()).ToString("dd/MM/yyyy");
                lblTendiachixephang.Text = String.IsNullOrEmpty(tkmd.TenDiaDiemXepHang) ? "" : tkmd.TenDiaDiemXepHang;
                lblDiachixephang.Text = String.IsNullOrEmpty(tkmd.MaDiaDiemXepHang) ? "" : tkmd.MaDiaDiemXepHang.ToString();
                if (tkmd.ChiThiHQCollection.Count > 0)
                {
                    lblPhanloai.Text = String.IsNullOrEmpty(tkmd.ChiThiHQCollection[0].LoaiThongTin) ? "" : tkmd.ChiThiHQCollection[0].LoaiThongTin.ToString();
                }
                else
                {
                    lblPhanloai.Text = String.Empty;
                }
                //lblMaxephang1.Text = tkmd.ContainerTKMD.MaDiaDiem1.ToString();

                for (int i = 0; i < tkmd.ContainerTKMD.SoContainerCollection.Count; i++)
                {
                    switch (i)
                    {
                        case 0:
                            lblMaxephang1.Text = String.IsNullOrEmpty(tkmd.ContainerTKMD.MaDiaDiem1) ? "" : tkmd.ContainerTKMD.MaDiaDiem1.ToString();
                            break;
                        case 1:
                            lblMaxephang2.Text = String.IsNullOrEmpty(tkmd.ContainerTKMD.MaDiaDiem2) ? "" : tkmd.ContainerTKMD.MaDiaDiem2.ToString();
                            break;
                        case 2:
                            lblMaxephang3.Text = String.IsNullOrEmpty(tkmd.ContainerTKMD.MaDiaDiem3) ? "" : tkmd.ContainerTKMD.MaDiaDiem3.ToString();
                            break;
                        case 3:
                            lblMaxephang4.Text = String.IsNullOrEmpty(tkmd.ContainerTKMD.MaDiaDiem4) ? "" : tkmd.ContainerTKMD.MaDiaDiem4.ToString();

                            break;
                        case 4:
                            lblMaxephang5.Text = String.IsNullOrEmpty(tkmd.ContainerTKMD.MaDiaDiem5) ? "" : tkmd.ContainerTKMD.MaDiaDiem5.ToString();
                            break;

                    }

                }

                BindReportContainer(tkmd);
                BindReportChiThiHQ(tkmd);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }           

        }
        public void BindReportChiThiHQ(KDT_VNACC_ToKhaiMauDich tkmd)
        {
            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Date", typeof(string));
                dt.Columns.Add("Ten", typeof(string));
                dt.Columns.Add("NoiDung", typeof(string));
                dt.Columns.Add("STT", typeof(string));
                int STT = 0;

                for (int i = 0; i < tkmd.ChiThiHQCollection.Count; i++)
                {
                    STT++;
                    DataRow dr = dt.NewRow();
                    if
                      (Convert.ToDateTime(tkmd.ChiThiHQCollection[i].Ngay).ToString("dd/MM/yyyy") != "01/01/1900" && Convert.ToDateTime(tkmd.ChiThiHQCollection[i].Ngay).ToString("dd/MM/yyyy") != "01/01/0001")
                    {
                        dr["Date"] = Convert.ToDateTime(tkmd.ChiThiHQCollection[i].Ngay).ToString("dd/MM/yyyy");
                    }


                    dr["Ten"] = String.IsNullOrEmpty(tkmd.ChiThiHQCollection[i].Ten) ? "" : tkmd.ChiThiHQCollection[i].Ten.ToString();
                    dr["STT"] = STT.ToString();
                    dr["NoiDung"] = String.IsNullOrEmpty(tkmd.ChiThiHQCollection[i].NoiDung) ? "" : tkmd.ChiThiHQCollection[i].NoiDung.ToString();
                    dt.Rows.Add(dr);
                }

                while (dt.Rows.Count < 10)
                {

                    STT++;
                    DataRow dr = dt.NewRow();
                    dr["Date"] = "";
                    dr["Ten"] = "";
                    dr["NoiDung"] = "";
                    dr["STT"] = STT.ToString();
                    dt.Rows.Add(dr);
                }
                DetailReport1.DataSource = dt;
                lblNgaychithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "Date");
                lblTenchithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "Ten");
                lblNoidungchithihaiquan1.DataBindings.Add("Text", DetailReport1.DataSource, "NoiDung");
                lblSTTChiThiHQ.DataBindings.Add("Text", DetailReport1.DataSource, "STT");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }

        }

        public void BindReportContainer(KDT_VNACC_ToKhaiMauDich tkmd)
        {

            try
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("STT1", typeof(string));
                dt.Columns.Add("SoCont1", typeof(string));
                dt.Columns.Add("STT2", typeof(string));
                dt.Columns.Add("SoCont2", typeof(string));
                dt.Columns.Add("STT3", typeof(string));
                dt.Columns.Add("SoCont3", typeof(string));
                dt.Columns.Add("STT4", typeof(string));
                dt.Columns.Add("SoCont4", typeof(string));
                dt.Columns.Add("STT5", typeof(string));
                dt.Columns.Add("SoCont5", typeof(string));
                for (int i = 1; i < 11; i++)
                {
                    DataRow dr = dt.NewRow();
                    int r = 1;
                    for (int j = (i - 1) * 5; j < i * 5; j++)
                    {

                        dr["STT" + r] = (j + 1).ToString();
                        try
                        {
                            dr["SoCont" + r] = String.IsNullOrEmpty(tkmd.ContainerTKMD.SoContainerCollection[r].SoContainer) ? "" : tkmd.ContainerTKMD.SoContainerCollection[r].SoContainer.ToString();
                        }
                        catch (Exception ex)
                        {

                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }

                        r = r + 1;

                    }
                    dt.Rows.Add(dr);

                }
                for (int i = 0; i < tkmd.ContainerTKMD.SoContainerCollection.Count; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["STT" + i +1] = i+1;
                    dr["SoCont" + i + 1] = String.IsNullOrEmpty(tkmd.ContainerTKMD.SoContainerCollection[i].SoContainer) ? "" : tkmd.ContainerTKMD.SoContainerCollection[i].SoContainer.ToString();
                    dt.Rows.Add(dr);
                }
                DetailReport.DataSource = dt;


                lblSTT1.DataBindings.Add("Text", DetailReport.DataSource, "STT1");
                lblSocontainer1.DataBindings.Add("Text", DetailReport.DataSource, "SoCont1");
                lblSTT2.DataBindings.Add("Text", DetailReport.DataSource, "STT2");
                lblSocontainer2.DataBindings.Add("Text", DetailReport.DataSource, "SoCont2");
                lblSTT3.DataBindings.Add("Text", DetailReport.DataSource, "STT3");
                lblSocontainer3.DataBindings.Add("Text", DetailReport.DataSource, "SoCont3");
                lblSTT4.DataBindings.Add("Text", DetailReport.DataSource, "STT4");
                lblSocontainer4.DataBindings.Add("Text", DetailReport.DataSource, "SoCont4");
                lblSTT5.DataBindings.Add("Text", DetailReport.DataSource, "STT5");
                lblSocontainer5.DataBindings.Add("Text", DetailReport.DataSource, "SoCont5");

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }




        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            XRLabel lbl = (XRLabel)sender;
            if (lbl.Text.Trim() == "0" || lbl.Text.Trim() == "0,00" || lbl.Text.Trim() == "0,0000")
                lbl.Text = "";
        }

        private void lblSoTrang_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            int pages;
            if (count % 2 >= 1)
            {
                pages = count / 2 + 1;
            }
            else
            {
                pages = count;
            }
            lblSoTrang.Text = 2 + "/" + (pages + 2);
        }
    }
}
