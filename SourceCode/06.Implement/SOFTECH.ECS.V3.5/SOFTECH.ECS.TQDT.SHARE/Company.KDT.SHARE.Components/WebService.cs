﻿//#define DEBUG
using System;
using System.Net;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Xml;
using System.IO;
using System.Windows.Forms;
using System.Globalization;
using System.ServiceModel;
using System.Text.RegularExpressions;
using System.ServiceModel.Description;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Data;
using Company.KDT.SHARE.VNACCS;


namespace Company.KDT.SHARE.Components
{

    [ServiceContractAttribute(Namespace = "http://softech.vn/", ConfigurationName = "ISignMessage")]
    public interface ISignMessage
    {
        [OperationContractAttribute(Action = "http://softech.vn/ClientSend", ReplyAction = "*")]
        string ClientSend(string message);

        [OperationContractAttribute(Action = "http://softech.vn/ServerSend", ReplyAction = "*")]
        string ServerSend(string message);

        [OperationContractAttribute(Action = "http://softech.vn/ClientSendFile", ReplyAction = "*")]
        string ClientSendFile(string message);

        [OperationContractAttribute(Action = "http://softech.vn/ServerSendFile", ReplyAction = "*")]
        string ServerSendFile(string message);

        [OperationContractAttribute(Action = "http://softech.vn/GetNewFileClientSend", ReplyAction = "*")]
        DataTable GetNewFileClientSend(string From);

        [OperationContractAttribute(Action = "http://softech.vn/GetNewMessageClientSend", ReplyAction = "*")]
        DataTable GetNewMessageClientSend(string From);


        [OperationContractAttribute(Action = "http://softech.vn/IsExist", ReplyAction = "*")]
        bool IsExist(string username, string password);

        [OperationContractAttribute(Action = "http://softech.vn/GetDaiLy", ReplyAction = "*")]
        DataTable GetDaiLy(string username, string password);

        [OperationContractAttribute(Action = "http://softech.vn/SearchCustommer", ReplyAction = "*")]
        DataTable SearchCustommer(string where,string username, string password);

        [OperationContractAttribute(Action = "http://softech.vn/GetAllCustommer", ReplyAction = "*")]
        DataTable GetAllCustommer(string username, string password);


        [OperationContractAttribute(Action = "http://softech.vn/ChangePass", ReplyAction = "*")]
        string ChangePass(string username, string password, string accountDL, string NewPass, bool ChangePassAdmin);
    }
    [ServiceContractAttribute(Namespace = "http://cis.customs.gov.vn/", ConfigurationName = "IHaiQuanService")]
    public interface IHaiQuanContract
    {
        [OperationContractAttribute(Action = "http://cis.customs.gov.vn/Send", ReplyAction = "*")]
        string Send(string MessageXML, string UserId, string Password);
        [OperationContractAttribute(Action = "http://cis.customs.gov.vn/TestWebservie", ReplyAction = "*")]
        string TestWebservie();
    }
    /// <summary>
    /// KhanhHN bo sung webservice dong bo du lieu
    /// </summary>
    [ServiceContractAttribute(Namespace = "http://tempuri.org/", ConfigurationName = "ISyncData")]
    public interface ISyncData
    {
        #region Đồng bộ dữ liệu giữa đại lý và doanh nghiệp
        [OperationContractAttribute(Action = "http://tempuri.org/UpdateTrack", ReplyAction = "*")]
        string UpdateTrack(string userNameLogin, string passWordLogin, long soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep, string msg);


        [OperationContractAttribute(Action = "http://tempuri.org/InsertUpdateUserDaiLy", ReplyAction = "*")]
        int InsertUpdateUserDaiLy(string userNameLogin, string passWordLogin, string tenNguoiDung, string matKhau, string hoVaTen, string moTa, string maDoanhNghiep);

        [OperationContractAttribute(Action = "http://tempuri.org/CheckUserName", ReplyAction = "*")]
        bool CheckUserName(string userName);

        [OperationContractAttribute(Action = "http://tempuri.org/DeleteUserDaiLy", ReplyAction = "*")]
        bool DeleteUserDaiLy(string userNameLogin, string passWordLogin, string tenNguoiDung, string matKhau);

        [OperationContractAttribute(Action = "http://tempuri.org/SelectTrackDynamic", ReplyAction = "*")]
        string SelectTrackDynamic(string userNameLogin, string passWordLogin, string whereCondition, string orderBy);

        [OperationContractAttribute(Action = "http://tempuri.org/SelectUserDaiLyAll", ReplyAction = "*")]
        string SelectUserDaiLyAll(string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/SelectUserDaiLy", ReplyAction = "*")]
        string SelectUserDaiLy(string maDoanhNghiep, string userNameLogin, string passWordLogin);


        [OperationContractAttribute(Action = "http://tempuri.org/SelectTrack", ReplyAction = "*")]
        string SelectTrack(string userNameLogin, string passWordLogin, long soToKhai, int namDangKy, string maLoaiHinh, string maHaiQuan, string maDoanhNghiep);

        [OperationContractAttribute(Action = "http://tempuri.org/LoginDaiLy", ReplyAction = "*")]
        string LoginDaiLy(string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/SendDaiLy", ReplyAction = "*")]
        string SendDaiLy(string message, string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/CheckDaiLy", ReplyAction = "*")]
        string CheckDaiLy(string maDaiLy, string maDoanhNghiep);

        [OperationContractAttribute(Action = "http://tempuri.org/SendDoanhNghiep", ReplyAction = "*")]
        string SendDoanhNghiep(string guiStr, string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/ReceivesViews", ReplyAction = "*")]
        string ReceivesViews(string userNameLogin, string passWordLogin, string messages);

        [OperationContractAttribute(Action = "http://tempuri.org/CheckCauHinhDL", ReplyAction = "*")]
        string CheckCauHinhDL(string MaDoanhNghiep);

        [OperationContractAttribute(Action = "http://tempuri.org/InsertUpdateCauHinhDaiLy", ReplyAction = "*")]
        bool InsertUpdateCauHinhDaiLy(string userNameLogin, string passWordLogin, string maDoanhNghiep, string tenDoanhNghiep, int value, string ghiChu, int isUse);

        [OperationContractAttribute(Action = "http://tempuri.org/SendGC", ReplyAction = "*")]
        string SendGC(string message, string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/ReceivesGC", ReplyAction = "*")]
        string ReceivesGC(string Type, string userNameLogin, string passWordLogin);

        [OperationContractAttribute(Action = "http://tempuri.org/LoginDaiLy_V3", ReplyAction = "*")]
        string LoginDaiLy_V3(string userNameLogin, string passWordLogin);
        #endregion
        #region UPDATE CATEGORY VNACCS
        //[OperationContractAttribute(Action = "http://tempuri.org/UpdateCategoryOnline", ReplyAction = "*")]
        //System.Data.DataSet UpdateCategoryVNACCS(string password, DateTime dateLastUpdated, ECategory CategoryType);
        #endregion
        [OperationContractAttribute(Action = "http://tempuri.org/GetDanhMucOnline", ReplyAction = "*")]
        System.Data.DataSet GetDanhMucOnline(string passWordLogin, DateTime dateLastUpdated, Company.KDT.SHARE.Components.EDanhMucHaiQuan type);

        [OperationContractAttribute(Action = "http://tempuri.org/LayDanhMucLinhVuc", ReplyAction = "*")]
        System.Data.DataSet LayDanhMucLinhVuc(string passWordLogin, DateTime dateLastUpdated);

        [OperationContractAttribute(Action = "http://tempuri.org/XoaDanhMucLinhVuc", ReplyAction = "*")]
        bool XoaDanhMucLinhVuc(string passWordLogin, string maLinhVuc);

        [OperationContractAttribute(Action = "http://tempuri.org/LuuDanhMucLinhVuc", ReplyAction = "*")]
        bool LuuDanhMucLinhVuc(string passWordLogin, System.Data.DataTable data);

        [OperationContractAttribute(Action = "http://tempuri.org/LayDanhMucThuVien", ReplyAction = "*")]
        System.Data.DataSet LayDanhMucThuVien(string passWordLogin, DateTime dateLastUpdated);

        [OperationContractAttribute(Action = "http://tempuri.org/LayDanhMucCauHoiDoanhNghiep", ReplyAction = "*")]
        System.Data.DataSet LayDanhMucCauHoiDoanhNghiep(string passWordLogin, DateTime dateLastUpdated);

        [OperationContractAttribute(Action = "http://tempuri.org/TaoSuaCauHoi", ReplyAction = "*")]
        string TaoSuaCauHoi(string passWordLogin, ref Company.KDT.SHARE.Components.Feedback.Components.ThuVienCauHoi cauHoi);

        [OperationContractAttribute(Action = "http://tempuri.org/XoaCauHoi", ReplyAction = "*")]
        string XoaCauHoi(string passWordLogin, string maCauHoi);
    }

    public class WebService
    {

        #region Load Config Webservice

        public static string LoadConfigure(string key)
        {
            System.Configuration.Configuration config = System.Configuration.ConfigurationManager.OpenExeConfiguration(System.Configuration.ConfigurationUserLevel.None);

            return config.AppSettings.Settings[key].Value.ToString();
        }

        static WebService()
        {
            //Hook a callback to verify the remote certificate
            ServicePointManager.ServerCertificateValidationCallback =
                delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) { return true; };

        }
        private static T CreateService<T>(string url)
        {
            T service;
            try
            {

                if (!Uri.IsWellFormedUriString(url, UriKind.Absolute))
                    url = string.Format("http://{0}", url);
                BasicHttpBinding binding = new BasicHttpBinding();
                EndpointAddress address = new EndpointAddress(url);
                string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
                string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
                if (!string.IsNullOrEmpty(host + port))
                {
                    WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                    proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                    binding.ProxyAddress = proxy.Address;
                }
                binding.UseDefaultWebProxy = true;
                binding.SendTimeout = new TimeSpan(0, Globals.TimeConnect, 0);
                binding.MaxReceivedMessageSize = int.MaxValue;
                binding.MaxBufferSize = int.MaxValue;
                binding.TextEncoding = System.Text.Encoding.UTF8;
                binding.ReaderQuotas = System.Xml.XmlDictionaryReaderQuotas.Max;
                binding.OpenTimeout = new TimeSpan(0, Globals.TimeConnect, 0);
                //binding.Security.Message.ClientCredentialType.
                if (address.Uri.Scheme == "https")
                {
                    //Dùng https
                    binding.Security.Mode = BasicHttpSecurityMode.Transport;
                    binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Windows;
                }

                /*
                 * Mở theo cách add service(Source gen). LanNT
                 * cis = new Company.KDT.SHARE.Components.CisWS.CISServiceSoapClient(binding, address);
                 */
                /*
                 * Mở theo Channel
                 */
              
                    ChannelFactory<T> factory = new ChannelFactory<T>(binding, address);

                    //client.ClientCredentials.UserName.UserName = "shoaib";
                    //client.ClientCredentials.UserName.Password = "shaikh";
                    service = factory.CreateChannel();

                
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Khởi tạo service kết nối", ex);
                throw new Exception("Mở kênh kết nối với Hải quan gặp lỗi.\r\n" + ex.Message + "\r\nURL = " + url);
            }
            return service;
        }
        public static Company.KDT.SHARE.Components.WS_HQBD_KHO.CFSService CreateServiceV5_BD(string url)
        {
            Company.KDT.SHARE.Components.WS_HQBD_KHO.CFSService service = new Company.KDT.SHARE.Components.WS_HQBD_KHO.CFSService();
            service.Url = url;
            //service.Timeout = Globals.TimeConnect;
            //string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
            //string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
            //string host = "115.75.32.68";
            string host = "103.153.218.7";
            string port = "80";
            if (!string.IsNullOrEmpty(host + port))
            {
                WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                //proxy.Address = host+":80";
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                service.Proxy = proxy;
            }
            //service.

            return service;

        }
        public static Company.KDT.SHARE.Components.WS_HQĐN_KHO.CFSService CreateServiceV5_ĐN(string url)
        {
            Company.KDT.SHARE.Components.WS_HQĐN_KHO.CFSService service = new Company.KDT.SHARE.Components.WS_HQĐN_KHO.CFSService();
            service.Url = url;
            //service.Timeout = Globals.TimeConnect;
            //string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
            //string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
            string host = "113.161.145.108";
            string port = "80";
            if (!string.IsNullOrEmpty(host + port))
            {
                WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                //proxy.Address = host+":80";
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                service.Proxy = proxy;
            }
            //service.

            return service;

        }
        public static Company.KDT.SHARE.Components.WS_QLTP_HP.KDTService CreateServiceV5_QLTP_HP(string url)
        {
            Company.KDT.SHARE.Components.WS_QLTP_HP.KDTService service = new Company.KDT.SHARE.Components.WS_QLTP_HP.KDTService();
            service.Url = url;
            string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
            string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
            //string host = "113.160.97.58";
            //string port = "8220";
            if (!string.IsNullOrEmpty(host + port))
            {
                WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                //proxy.Address = host+":80";
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                service.Proxy = proxy;
            }
            return service;

        }
        public static Company.KDT.SHARE.Components.WSKDTServiceAcc.Service CreateServiceV5(string url)
        {
            Company.KDT.SHARE.Components.WSKDTServiceAcc.Service service = new Company.KDT.SHARE.Components.WSKDTServiceAcc.Service();
            service.Url = url;
            //service.Timeout = Globals.TimeConnect;
            string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
            string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
            //string host = "103.248.160.15";
            //string port = "8088";
            if (!string.IsNullOrEmpty(host + port))
            {
                WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                service.Proxy = proxy;
            }
            //service.

            return service;
 
        }
        public static Company.KDT.SHARE.Components.WSKDTServiceAcc.Service CreateKDTService(string url)
        {
            Company.KDT.SHARE.Components.WSKDTServiceAcc.Service service = new Company.KDT.SHARE.Components.WSKDTServiceAcc.Service();
            service.Url = url;
            //service.Timeout = Globals.TimeConnect;
            string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
            string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
            //string host = "103.248.160.15";
            //string port = "8088";
            if (!string.IsNullOrEmpty(host + port))
            {
                WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                service.Proxy = proxy;
            }
            //service.

            return service;

        }
        public static Company.KDT.SHARE.Components.WSKTX_V3.CISService CreateServiceKTX(string url)
        {
            Company.KDT.SHARE.Components.WSKTX_V3.CISService service = new Company.KDT.SHARE.Components.WSKTX_V3.CISService();
            service.Url = url;
            //service.Timeout = Globals.TimeConnect;
            string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
            string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
            //string host = "103.248.160.22";
            //string port = "8088";
            if (!string.IsNullOrEmpty(host + port))
            {
                WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                service.Proxy = proxy;
            }
            //service.

            return service;

        }

        public static Company.KDT.SHARE.Components.WS.KDTService CreateServiceV4(string url)
        {
            Company.KDT.SHARE.Components.WS.KDTService service = new Company.KDT.SHARE.Components.WS.KDTService();
            service.Url = url;
            //service.Timeout = Globals.TimeConnect;
            string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
            string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
            //string host = "103.248.160.22";
            //string port = "8088";
            if (!string.IsNullOrEmpty(host + port))
            {
                WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                service.Proxy = proxy;
            }
            //service.

            return service;

        }
        public static Company.KDT.SHARE.Components.WSKDTServiceCKS.Service CreateServiceCKS(string url)
        {
            Company.KDT.SHARE.Components.WSKDTServiceCKS.Service service = new Company.KDT.SHARE.Components.WSKDTServiceCKS.Service();
            service.Url = url;
            //service.Timeout = Globals.TimeConnect;
            string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
            string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
            //string host = "103.248.160.15";
            //string port = "8088";
            if (!string.IsNullOrEmpty(host + port))
            {
                WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                service.Proxy = proxy;
            }
            //service.

            return service;

        }

        public static Company.KDT.SHARE.Components.WSHaiQuan.CISService CreateCISService(string url)
        {
            Company.KDT.SHARE.Components.WSHaiQuan.CISService service = new Company.KDT.SHARE.Components.WSHaiQuan.CISService();
            service.Url = url;
            //service.Timeout = Globals.TimeConnect;
            string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
            string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
            //string host = "103.248.160.22";
            //string port = "8088";
            if (!string.IsNullOrEmpty(host + port))
            {
                WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                service.Proxy = proxy;
            }
            //service.

            return service;

        }
        //public static Company.KDT.SHARE.Components.WSHaiQuan_New.Service_New CreateServiceV5Test(string url)
        //{
        //    Company.KDT.SHARE.Components.WSHaiQuan_New.Service_New service = new Company.KDT.SHARE.Components.WSHaiQuan_New.Service_New();
        //    service.Url = url;
        //    //service.Timeout = Globals.TimeConnect;
        //    //string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
        //    //string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
        //    string host = "103.248.160.15";
        //    string port = "8088";
        //    if (!string.IsNullOrEmpty(host + port))
        //    {
        //        WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
        //        proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
        //        service.Proxy = proxy;
        //    }
        //    //service.

        //    return service;

        //}
        public static ISignMessage SignMessage()
        {
            ISignMessage service = null;
            try
            {
                string url;
                url = LoadConfigure("WS_SIGNMESSAGE");
                url = "https://ecs.softech.cloud/SignRemote/SignMessage.asmx";
                service = CreateService<ISignMessage>(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }
        public static WSKTX_V3.CISService HQServiceKTX(bool isDaiLy, string MaDoanhNghiep)
        {
            WSKTX_V3.CISService service = null;
            string url;
            try
            {
                if (isDaiLy)
                {
                    string configURL = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "DIA_CHI_HQ").Value_Config.ToString() + "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "TEN_DICH_VU").Value_Config.ToString();
                    if (configURL.Contains("http"))
                        url = configURL;
                    else
                        url = string.Format("http://{0}", configURL);
                }
                else
                {
                    url = LoadConfigure("WS_URL");
                }
                service = CreateServiceKTX(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }

        public static WS.KDTService HQServiceV4(bool isDaiLy, string MaDoanhNghiep)
        {
            WS.KDTService service = null;
            string url;
            try
            {
                if (isDaiLy)
                {
                    string configURL = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "DIA_CHI_HQ").Value_Config.ToString() + "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "TEN_DICH_VU").Value_Config.ToString();
                    if (configURL.Contains("http"))
                        url = configURL;
                    else
                        url = string.Format("http://{0}", configURL);
                }
                else
                {
                    url = LoadConfigure("WS_URL");
                }
                service = CreateServiceV4(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }
        public static WSKDTServiceCKS.Service ServiceCKS(bool isDaiLy, string MaDoanhNghiep)
        {
            WSKDTServiceCKS.Service service = null;
            string url;
            try
            {
                if (isDaiLy)
                {
                    string configURL = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "DIA_CHI_HQ").Value_Config.ToString() + "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "TEN_DICH_VU").Value_Config.ToString();
                    if (configURL.Contains("http"))
                        url = configURL;
                    else
                        url = string.Format("http://{0}", configURL);
                }
                else
                {
                    url = LoadConfigure("WS_URL");
                }
                service = CreateServiceCKS(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }
        public static WSHaiQuan.CISService CISService(bool isDaiLy, string MaDoanhNghiep)
        {
            WSHaiQuan.CISService service = null;
            string url;
            try
            {
                if (isDaiLy)
                {
                    string configURL = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "DIA_CHI_HQ").Value_Config.ToString() + "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "TEN_DICH_VU").Value_Config.ToString();
                    if (configURL.Contains("http"))
                        url = configURL;
                    else
                        url = string.Format("http://{0}", configURL);
                }
                else
                {
                    url = LoadConfigure("WS_URL");
                }
                service = CreateCISService(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }
        public static IHaiQuanContract HQService(bool isDaiLy,string MaDoanhNghiep)
        {
            IHaiQuanContract service = null;
            string url;
            try
            {
                if (isDaiLy)
                {
                    string configURL = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "DIA_CHI_HQ").Value_Config.ToString() + "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "TEN_DICH_VU").Value_Config.ToString();
                    if (configURL.Contains("http"))
                        url = configURL;
                    else
                        url = string.Format("http://{0}",configURL);
                }
                else
                {
                    url = LoadConfigure("WS_URL");
                }
                service = CreateService<IHaiQuanContract>(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }
        public static WS_HQBD_KHO.CFSService HQServiceV5_BD(bool isDaiLy, string MaDoanhNghiep)
        {
            WS_HQBD_KHO.CFSService service = null;
            string url;
            try
            {
                //if (isDaiLy)
                //{
                //    string configURL = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "DIA_CHI_HQ").Value_Config.ToString() + "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "TEN_DICH_VU").Value_Config.ToString();
                //    if (configURL.Contains("http"))
                //        url = configURL;
                //    else
                //        url = string.Format("http://{0}", configURL);
                //}
                //else
                //{
                //    url = LoadConfigure("WS_URL");
                //}
                url = "http://cfs.hqbd.gov.vn/CFS/services/KhoCFS.asmx";

                service = CreateServiceV5_BD(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }
        public static WS_HQĐN_KHO.CFSService HQServiceV5_ĐN(bool isDaiLy, string MaDoanhNghiep)
        {
            WS_HQĐN_KHO.CFSService service = null;
            string url;
            try
            {
                //if (isDaiLy)
                //{
                //    string configURL = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "DIA_CHI_HQ").Value_Config.ToString() + "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "TEN_DICH_VU").Value_Config.ToString();
                //    if (configURL.Contains("http"))
                //        url = configURL;
                //    else
                //        url = string.Format("http://{0}", configURL);
                //}
                //else
                //{
                //    url = LoadConfigure("WS_URL");
                //}
                url = "http://113.161.145.108:80/cfsservices/KhoCFS.asmx";

                service = CreateServiceV5_ĐN(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }
        public static WS_QLTP_HP.KDTService HQServiceV5_QLTP_HP(bool isDaiLy, string MaDoanhNghiep)
        {
            WS_QLTP_HP.KDTService service = null;
            string url;
            try
            {
                url = "http://113.160.97.58:8220/KDTService.asmx";

                service = CreateServiceV5_QLTP_HP(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }
        public static WSKDTServiceAcc.Service HQServiceV5(bool isDaiLy, string MaDoanhNghiep)
        {
            WSKDTServiceAcc.Service service = null;
            string url;
            try
            {
                if (isDaiLy)
                {
                    string configURL = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "DIA_CHI_HQ").Value_Config.ToString() + "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "TEN_DICH_VU").Value_Config.ToString();
                    if (configURL.Contains("http"))
                        url = configURL;
                    else
                        url = string.Format("http://{0}", configURL);
                }
                else
                {
                    url = LoadConfigure("WS_URL");
                }
                service = CreateServiceV5(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }
        public static WSKDTServiceAcc.Service KDTService(bool isDaiLy, string MaDoanhNghiep)
        {
            WSKDTServiceAcc.Service service = null;
            string url;
            try
            {
                if (isDaiLy)
                {
                    string configURL = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "DIA_CHI_HQ").Value_Config.ToString() + "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "TEN_DICH_VU").Value_Config.ToString();
                    if (configURL.Contains("http"))
                        url = configURL;
                    else
                        url = string.Format("http://{0}", configURL);
                }
                else
                {
                    url = LoadConfigure("WS_URL");
                }

                service = CreateKDTService(url);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw;
            }
            return service;
        }
        //public static WSHaiQuan_New.Service_New HQServiceV5Test(bool isDaiLy, string MaDoanhNghiep)
        //{
        //    WSHaiQuan_New.Service_New service = null;
        //    string url;
        //    try
        //    {
        //        if (isDaiLy)
        //        {
        //            string configURL = Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "DIA_CHI_HQ").Value_Config.ToString() + "/" + Company.KDT.SHARE.Components.HeThongPhongKhai.Load(MaDoanhNghiep, "TEN_DICH_VU").Value_Config.ToString();
        //            if (configURL.Contains("http"))
        //                url = configURL;
        //            else
        //                url = string.Format("http://{0}", configURL);
        //        }
        //        else
        //        {
        //            url = "http://103.248.160.15:8088/KDTServiceAcc/CISService.asmx";
        //            //url = LoadConfigure("WS_URL");
        //        }

        //        service = CreateServiceV5Test(url);
        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.LocalLogger.Instance().WriteMessage(ex);
        //        throw;
        //    }
        //    return service;
        //}


        public static ISyncData SyncService()
        {
            ISyncData service = null;
            string url;
            try
            {
                url = "http://localhost:1516/Service.asmx";
                //url = LoadConfigure("WS_SyncData");
                service = CreateService<ISyncData>(url);

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw (ex);
            }
            return service;
        }
        public static WSSyncData.Service SyncDataService()
        {
            WSSyncData.Service service = null;
            string url;
            try
            {
                url = "http://ecs.softech.cloud/wssyndata/Service.asmx";
                //url = LoadConfigure("WS_SyncData");
                service = CreateServiceSyncData(url);

            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                throw (ex);
            }
            return service;
        }
        public static Company.KDT.SHARE.Components.WSSyncData.Service CreateServiceSyncData(string url)
        {
            Company.KDT.SHARE.Components.WSSyncData.Service service = new Company.KDT.SHARE.Components.WSSyncData.Service();
            service.Url = url;
            //service.Timeout = Globals.TimeConnect;
            string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");
            string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");
            //string host = "103.248.160.15";
            //string port = "8088";
            if (!string.IsNullOrEmpty(host + port))
            {
                WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
                service.Proxy = proxy;
            }
            //service.

            return service;

        }
        public static Company.KDT.SHARE.Components.WS.KDTService GetWS()
        {
            Company.KDT.SHARE.Components.WS.KDTService kdt = new Company.KDT.SHARE.Components.WS.KDTService();
            try
            {
                string url = LoadConfigure("WS_URL");
                kdt.Url = url;

                string host = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Host");

                string port = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("Port");

                if (Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("IsUseProxy").Equals("True") || !string.IsNullOrEmpty(host + port))
                {
                    WebProxy proxy = new WebProxy(host, Convert.ToInt32(port));
                    kdt.Proxy = proxy;

                }
                else
                {
                    kdt.Proxy = System.Net.WebRequest.GetSystemWebProxy();
                    kdt.Proxy.Credentials = CredentialCache.DefaultCredentials;
                    kdt.Credentials = CredentialCache.DefaultCredentials;
                }

                kdt.Timeout = 300000;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage("Khởi tạo service kết nối", ex);
            }

            return kdt;
        }
        #endregion
    }
}
