using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;

namespace Company.Interface.Report.GC.ThongTu38
{
    public partial class BangKe15_HSTK_TT38 : DevExpress.XtraReports.UI.XtraReport
    {
        DataSet ds = new DataSet();
        public BangKe15_HSTK_TT38()
        {
            InitializeComponent();
        }


        public void bindReport() 
        {
            DataTable dataTable = ds.Tables[0];
            dataTable.TableName = "t_GC_NguyenPhuLieu";
            this.DataSource = dataTable;
            lblSoTT.DataBindings.Add("Text", this.DataSource,"");
            lblTaiKhoan.DataBindings.Add("Text", this.DataSource,"");
            lblNguyenPhuLieu.DataBindings.Add("Text", this.DataSource,"Ten");
            lblTonDauKy.DataBindings.Add("Text",this.DataSource,"SoLuongDangKy");
            lblNhapTrongKy.DataBindings.Add("Text",this.DataSource,"SoLuongDaNhap");
            lblXuatTrongKy.DataBindings.Add("Text",this.DataSource,"SoLuongDaDung");
            lblTonCuoiKy.DataBindings.Add("Text", this.DataSource, "");
        }
    }
}
