------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_ToKhaiMauDich_Update]
-- Database: ECS_SXXKTQ
-- Author: Ngo Thanh Tung
-- Time created: Thursday, January 07, 2010
------------------------------------------------------------------------------------------------------------------------

ALTER PROCEDURE [dbo].[p_KDT_ToKhaiMauDich_Update]
	@ID bigint,
	@SoTiepNhan bigint,
	@NgayTiepNhan datetime,
	@MaHaiQuan char(6),
	@SoToKhai int,
	@MaLoaiHinh char(5),
	@NgayDangKy datetime,
	@MaDoanhNghiep varchar(14),
	@TenDoanhNghiep nvarchar(255),
	@MaDaiLyTTHQ varchar(14),
	@TenDaiLyTTHQ nvarchar(255),
	@TenDonViDoiTac nvarchar(255),
	@ChiTietDonViDoiTac nvarchar(255),
	@SoGiayPhep nvarchar(50),
	@NgayGiayPhep datetime,
	@NgayHetHanGiayPhep datetime,
	@SoHopDong nvarchar(500),
	@NgayHopDong datetime,
	@NgayHetHanHopDong datetime,
	@SoHoaDonThuongMai nvarchar(50),
	@NgayHoaDonThuongMai datetime,
	@PTVT_ID varchar(3),
	@SoHieuPTVT nvarchar(50),
	@NgayDenPTVT datetime,
	@QuocTichPTVT_ID char(3),
	@LoaiVanDon nvarchar(50),
	@SoVanDon nvarchar(50),
	@NgayVanDon datetime,
	@NuocXK_ID char(3),
	@NuocNK_ID char(3),
	@DiaDiemXepHang nvarchar(255),
	@CuaKhau_ID char(6),
	@DKGH_ID varchar(7),
	@NguyenTe_ID char(3),
	@TyGiaTinhThue money,
	@TyGiaUSD money,
	@PTTT_ID varchar(10),
	@SoHang smallint,
	@SoLuongPLTK smallint,
	@TenChuHang nvarchar(30),
	@ChucVu nvarchar(20),
	@SoContainer20 numeric(15, 0),
	@SoContainer40 numeric(15, 0),
	@SoKien numeric(15, 0),
	@TrongLuong numeric(18, 5),
	@TongTriGiaKhaiBao money,
	@TongTriGiaTinhThue money,
	@LoaiToKhaiGiaCong varchar(3),
	@LePhiHaiQuan money,
	@PhiBaoHiem money,
	@PhiVanChuyen money,
	@PhiXepDoHang money,
	@PhiKhac money,
	@CanBoDangKy varchar(100),
	@QuanLyMay bit,
	@TrangThaiXuLy int,
	@LoaiHangHoa char(1),
	@GiayTo nvarchar(40),
	@PhanLuong varchar(2),
	@MaDonViUT varchar(14),
	@TenDonViUT nvarchar(50),
	@TrongLuongNet float,
	@SoTienKhoan money,
	@GUIDSTR nvarchar(500),
	@DeXuatKhac nvarchar(500),
	@LyDoSua nvarchar(500),
	@ActionStatus smallint,
	@GuidReference nvarchar(500),
	@NamDK int,
	@HUONGDAN nvarchar(max)
AS
UPDATE
	[dbo].[t_KDT_ToKhaiMauDich]
SET
	[SoTiepNhan] = @SoTiepNhan,
	[NgayTiepNhan] = @NgayTiepNhan,
	[MaHaiQuan] = @MaHaiQuan,
	[SoToKhai] = @SoToKhai,
	[MaLoaiHinh] = @MaLoaiHinh,
	[NgayDangKy] = @NgayDangKy,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[MaDaiLyTTHQ] = @MaDaiLyTTHQ,
	[TenDaiLyTTHQ] = @TenDaiLyTTHQ,
	[TenDonViDoiTac] = @TenDonViDoiTac,
	[ChiTietDonViDoiTac] = @ChiTietDonViDoiTac,
	[SoGiayPhep] = @SoGiayPhep,
	[NgayGiayPhep] = @NgayGiayPhep,
	[NgayHetHanGiayPhep] = @NgayHetHanGiayPhep,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[NgayHetHanHopDong] = @NgayHetHanHopDong,
	[SoHoaDonThuongMai] = @SoHoaDonThuongMai,
	[NgayHoaDonThuongMai] = @NgayHoaDonThuongMai,
	[PTVT_ID] = @PTVT_ID,
	[SoHieuPTVT] = @SoHieuPTVT,
	[NgayDenPTVT] = @NgayDenPTVT,
	[QuocTichPTVT_ID] = @QuocTichPTVT_ID,
	[LoaiVanDon] = @LoaiVanDon,
	[SoVanDon] = @SoVanDon,
	[NgayVanDon] = @NgayVanDon,
	[NuocXK_ID] = @NuocXK_ID,
	[NuocNK_ID] = @NuocNK_ID,
	[DiaDiemXepHang] = @DiaDiemXepHang,
	[CuaKhau_ID] = @CuaKhau_ID,
	[DKGH_ID] = @DKGH_ID,
	[NguyenTe_ID] = @NguyenTe_ID,
	[TyGiaTinhThue] = @TyGiaTinhThue,
	[TyGiaUSD] = @TyGiaUSD,
	[PTTT_ID] = @PTTT_ID,
	[SoHang] = @SoHang,
	[SoLuongPLTK] = @SoLuongPLTK,
	[TenChuHang] = @TenChuHang,
	[ChucVu] = @ChucVu,
	[SoContainer20] = @SoContainer20,
	[SoContainer40] = @SoContainer40,
	[SoKien] = @SoKien,
	[TrongLuong] = @TrongLuong,
	[TongTriGiaKhaiBao] = @TongTriGiaKhaiBao,
	[TongTriGiaTinhThue] = @TongTriGiaTinhThue,
	[LoaiToKhaiGiaCong] = @LoaiToKhaiGiaCong,
	[LePhiHaiQuan] = @LePhiHaiQuan,
	[PhiBaoHiem] = @PhiBaoHiem,
	[PhiVanChuyen] = @PhiVanChuyen,
	[PhiXepDoHang] = @PhiXepDoHang,
	[PhiKhac] = @PhiKhac,
	[CanBoDangKy] = @CanBoDangKy,
	[QuanLyMay] = @QuanLyMay,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[LoaiHangHoa] = @LoaiHangHoa,
	[GiayTo] = @GiayTo,
	[PhanLuong] = @PhanLuong,
	[MaDonViUT] = @MaDonViUT,
	[TenDonViUT] = @TenDonViUT,
	[TrongLuongNet] = @TrongLuongNet,
	[SoTienKhoan] = @SoTienKhoan,
	[GUIDSTR] = @GUIDSTR,
	[DeXuatKhac] = @DeXuatKhac,
	[LyDoSua] = @LyDoSua,
	[ActionStatus] = @ActionStatus,
	[GuidReference] = @GuidReference,
	[NamDK] = @NamDK,
	[HUONGDAN] = @HUONGDAN
WHERE
	[ID] = @ID


Go
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '18.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('18.9',GETDATE(), N' Cap nhat danh muc V5 khai tu xa')
END	
