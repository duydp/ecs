IF OBJECT_ID('p_KDT_SXXK_DinhMuc_SelectDistinctDynamicLSX') IS NOT NULL
DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectDistinctDynamicLSX]
GO
IF OBJECT_ID('p_KDT_SXXK_DinhMuc_SelectDynamicSP') IS NOT NULL
DROP PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectDynamicSP]
GO
-----------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_SelectDistinctDynamicLSX]    
-- Database: ECS_TQDT_SXXK_V4_TAT    
-----------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectDistinctDynamicLSX]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT     
 DISTINCT      
 [MaDinhDanhLenhSX] ,  
 Master_ID  
FROM [dbo].[t_KDT_SXXK_DinhMuc]     
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END      
EXEC sp_executesql @SQL    
GO

-----------------------------------------------    
-- Stored procedure name: [dbo].[p_KDT_SXXK_DinhMuc_SelectDynamicSP]    
-- Database: ECS_TQDT_SXXK_V4_TAT    
-----------------------------------------------    
    
CREATE PROCEDURE [dbo].[p_KDT_SXXK_DinhMuc_SelectDynamicSP]    
 @WhereCondition NVARCHAR(500),    
 @OrderByExpression NVARCHAR(250) = NULL    
AS    
    
SET NOCOUNT ON    
SET TRANSACTION ISOLATION LEVEL READ COMMITTED    
    
DECLARE @SQL NVARCHAR(MAX)    
    
SET @SQL =     
'SELECT     
 DISTINCT    
 [MaSanPham],       
 [MaDinhDanhLenhSX],  
  Master_ID  
FROM [dbo].[t_KDT_SXXK_DinhMuc]    
WHERE ' + @WhereCondition    
    
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0    
BEGIN    
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression    
END      
EXEC sp_executesql @SQL 

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '41.3') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('41.3',GETDATE(), N'CẬP NHẬT PROCEDURE CẬP NHẬT LỆNH SẢN XUẤT TỪ ĐỊNH MỨC ĐÃ NHẬP TRƯỚC ĐÓ')
END
