﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.GC.BLL.KDT;
using Company.Interface.Report.SXXK;
using DevExpress.XtraPrinting;
using DevExpress.XtraReports.UI;
using Company.GC.BLL.KDT.SXXK;
using System.Collections;
using Janus.Windows.EditControls;
using Company.GC.BLL.KDT.GC;
using Company.Interface.Report.GC;

namespace Company.Interface.Report
{

    public partial class ReportViewBC02_NVLCUForm : BaseForm
    {
        public long TKMD_ID;
        private BangKe02_NVLCU BC02 = new BangKe02_NVLCU();
        public DataTable dtBangNguyenPhuLieu = new DataTable();
        public HopDong HD = new HopDong();
        public ToKhaiMauDich TKMD = new ToKhaiMauDich();
        public ToKhaiMauDichCollection TKMDCol = new ToKhaiMauDichCollection();
        public Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
        public string _Sotokhai = "";
        public string _MaLoaiHinh = "";
        public string _NgayToKhai = "";
        public ReportViewBC02_NVLCUForm()
        {
            InitializeComponent();
        }

        private void ReportViewForm_Load(object sender, EventArgs e)
        {
            dtBangNguyenPhuLieu = this.CreatSchemaDataSetNewRP01(TKMD_ID);
            this.BC02.HD = this.HD;
            this.BC02._MaLoaiHinh = this._MaLoaiHinh;
            this.BC02._Sotokhai = this._Sotokhai;
            this.BC02._NgayToKhai = this._NgayToKhai;
            this.BC02.Source = dtBangNguyenPhuLieu;
            this.BC02.BindReport();
            printControl1.PrintingSystem = this.BC02.PrintingSystem;
            this.BC02.CreateDocument();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.Print, new object[] { true });
            }
            catch 
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }                
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                printControl1.ExecCommand(PrintingSystemCommand.ExportXls, new object[] { true });
            }
            catch
            {
                showMsg("MSG_PRI03");
                //ShowMessage("Lỗi khi in!", false);
            }   
        }


        public DataTable CreatSchemaDataSetNewRP01(long TKMD_ID)
        {
            DataTable dttemp = new DataTable("dtBangNguyenPhuLieu" + TKMD_ID.ToString());
            DataColumn[] dcCol = new DataColumn[9];

            dcCol[0] = new DataColumn("STT", typeof(string));
            dcCol[0].Caption = "STT";
            dcCol[1] = new DataColumn("Ten", typeof(string));
            dcCol[1].Caption = "Ten";
            dcCol[2] = new DataColumn("Ma", typeof(string));
            dcCol[2].Caption = "Ma";
            dcCol[3] = new DataColumn("DVT", typeof(string));
            dcCol[3].Caption = "DVT";
            dcCol[4] = new DataColumn("SoLuong", typeof(decimal));
            dcCol[4].Caption = "SoLuong";
            dcCol[5] = new DataColumn("DonGia", typeof(decimal));
            dcCol[5].Caption = "DonGia";
            dcCol[6] = new DataColumn("TriGia", typeof(decimal));
            dcCol[6].Caption = "TriGia";
            dcCol[7] = new DataColumn("HinhThuc", typeof(string));
            dcCol[7].Caption = "HinhThuc";
            dcCol[8] = new DataColumn("GhiChu", typeof(string));
            dcCol[8].Caption = "GhiChu";

            dttemp.Columns.AddRange(dcCol);

            Company.GC.BLL.GC.NguyenPhuLieu NPL = new Company.GC.BLL.GC.NguyenPhuLieu();
            DataSet HangCungUng = this.TKMD.GetHangCungUng(TKMD_ID);

            int sothutu = 1;
            foreach (DataRow dr in HangCungUng.Tables[0].Rows)
            {
                DataRow drData = dttemp.NewRow();
                drData[0] = sothutu;
                drData[1] = NPL.GetTenByMa(dr.ItemArray[1].ToString(),this.HD.ID);
                drData[2] = dr.ItemArray[1].ToString();
                drData[3] = DonViTinh_GetName(NPL.GetDVTByMa(dr.ItemArray[1].ToString(), this.HD.ID));
                drData[4] = Decimal.Parse(dr.ItemArray[2].ToString());
                drData[5] = Decimal.Parse(dr.ItemArray[3].ToString());
                drData[6] = Decimal.Parse(dr.ItemArray[4].ToString());
                drData[7] = dr.ItemArray[5].ToString();
                sothutu++;
                dttemp.Rows.Add(drData);
            }

            return dttemp;
        }
   
    }
}