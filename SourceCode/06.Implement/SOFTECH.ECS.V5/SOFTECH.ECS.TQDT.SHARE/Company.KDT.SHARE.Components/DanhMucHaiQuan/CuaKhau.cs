﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
    public partial class CuaKhau
    {
        public static List<CuaKhau> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<CuaKhau> collection = new List<CuaKhau>();

            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                CuaKhau entity = new CuaKhau();
                entity.ID = dataSet.Tables[0].Rows[i]["ID"].ToString();
                entity.Ten = dataSet.Tables[0].Rows[i]["Ten"].ToString();
                entity.MaCuc = dataSet.Tables[0].Rows[i]["MaCuc"] != DBNull.Value ? dataSet.Tables[0].Rows[i]["MaCuc"].ToString() : "";
                entity.DateCreated = dataSet.Tables[0].Rows[i]["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateCreated"]) : DateTime.Today;
                entity.DateModified = dataSet.Tables[0].Rows[i]["DateModified"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateModified"]) : DateTime.Today;
                collection.Add(entity);
            }

            return collection;
        }
    }
}
