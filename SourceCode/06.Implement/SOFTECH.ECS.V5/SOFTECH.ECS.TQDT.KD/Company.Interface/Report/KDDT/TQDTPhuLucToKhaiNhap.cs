﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KD.BLL.KDT;
using Company.KDT.SHARE.Components.DuLieuChuan;
using System.Collections.Generic;

namespace Company.Interface.Report.KDTD
{
    public partial class TQDTPhuLucToKhaiNhap : DevExpress.XtraReports.UI.XtraReport
    {
        
        public List<HangMauDich> HMDCollection = new List<HangMauDich>();
        public int SoToKhai;
        public DateTime NgayDangKy;
        public ToKhaiMauDich TKMD;
        public Company.Interface.Report.ReportViewTKNTQDTForm report;
        public bool MienThue1 = false;
        public bool MienThue2 = false;
        public bool inMaHang = false;
        public int soDongHang;
        public bool isCuaKhau = false;

        public TQDTPhuLucToKhaiNhap()
        {
            InitializeComponent();
        }
        public void BindReport(string pls)
        {
            xrLabel3.Text = this.TKMD.MaLoaiHinh;//.Substring(1, 2);
            this.PrintingSystem.ShowMarginsWarning = false;
            double tongTriGiaNT = 0;
            double tongThueXNK = 0;
            double tongThueGTGT = 0;
            double tongTriGiaThuKhac = 0;
            float sizeFont = float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang", "8")) == 0 ? 8 : float.Parse(Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("FontTenHang", "8"));
            //lblMaHaiQuan.Text = GlobalSettings.MA_HAI_QUAN;
            //lblChiCucHQ.Text = GlobalSettings.TEN_CUC_HAI_QUAN;
            //lblChiCucHQCK.Text = GlobalSettings.TEN_HAI_QUAN;
            if (isCuaKhau)
            {
                string chicuc = Company.KDT.SHARE.Components.DuLieuChuan.NhomCuaKhau.GetDonVi(this.TKMD.CuaKhau_ID);
                if (chicuc == string.Empty)
                    chicuc = this.TKMD.CuaKhau_ID + "-" + CuaKhau.GetName(this.TKMD.CuaKhau_ID);
                lblChiCucHQCK.Text = chicuc;
            }
            else
                lblChiCucHQCK.Text = "";
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;
            if (TKMD.NgayDangKy > new DateTime(1900, 1, 1))
                lblNgayDangKy.Text = TKMD.NgayDangKy.ToString("dd/MM/yyyy hh:mm:ss");
            if (TKMD.SoToKhai != 0)
                lblSoToKhai.Text = TKMD.SoToKhai + "";
            //lblThongBaoMienThue.Text = GlobalSettings.DonViBaoCao;
            // lblMienThue1.Text = GlobalSettings.TieuDeInDinhMuc;
            //lblThongBaoMienThue.Visible = MienThue2;
            // lblMienThue1.Visible = MienThue1;
            xrLabel1.Text = pls;
            // KhanhHN - Biến phân biệt tờ khai có cả 2 loại thuế GTGT và TTDB
            bool TypeTax = false;
            foreach (HangMauDich hmd in this.HMDCollection)
            {
                if(hmd.ThueSuatGTGT >0 && hmd.ThueSuatTTDB>0)
                {
                    TypeTax = true;
                    break;
                }
            }
            for (int i = 0; i < this.HMDCollection.Count; i++)
            {
                XRControl control = new XRControl();
                HangMauDich hmd = this.HMDCollection[i];

                //STT Hang. Hungtq Update 18/12/2010
                control = this.xrTable1.Rows[i + 3].Controls["STT" + (i + 1)];
                //control.Text = ((i + 1) + ((Convert.ToInt32(pls) - 1) * 9)).ToString(); //Comment by Hungtq
                control.Text = ((i + 1) + ((Convert.ToInt32(pls) - 1) * soDongHang)).ToString();

                control = this.xrTable1.Rows[i + 3].Controls["TenHang" + (i + 1)];//2
                //if(control !=null )
                if (!inMaHang)
                    control.Text = hmd.TenHang;
                else
                {
                    if (!string.IsNullOrEmpty(hmd.MaPhu))
                        control.Text = hmd.TenHang + "/" + hmd.MaPhu;
                    else
                        control.Text = hmd.TenHang;
                }
                control.WordWrap = true;
                control.Font = new Font("Times New Roman", sizeFont);

                control = this.xrTable1.Rows[i + 3].Controls["MaHS" + (i + 1)];
                control.Text = hmd.MaHS;

                control = this.xrTable1.Rows[i + 3].Controls["XuatXu" + (i + 1)];
                control.Text = hmd.NuocXX_ID;

                control = this.xrTable1.Rows[i + 3].Controls["Luong" + (i + 1)];
                control.Text = hmd.SoLuong.ToString("N"+ GlobalSettings.SoThapPhan.SoLuongHMD);

                control = this.xrTable1.Rows[i + 3].Controls["DVT" + (i + 1)];
                control.Text = DonViTinh.GetName((object)hmd.DVT_ID);

                if (hmd.FOC)
                {
                    control = this.xrTable1.Rows[i + 3].Controls["DonGiaNT" + (i + 1)];
                    control.Text = hmd.DonGiaKB.ToString("N" + +GlobalSettings.SoThapPhan.DonGiaNT);

                    control = this.xrTable1.Rows[i + 3].Controls["TriGiaNT" + (i + 1)];
                    control.Text = "F.O.C";

                    //STT Thue hang hoa. Hungtq Update 18/12/2010
                    control = this.xrTable2.Rows[i + 2].Controls["STT2" + (i + 1)];
                    //control.Text = ((i + 1) + ((Convert.ToInt32(pls) - 1) * 9)).ToString(); //Comment by HungTQ
                    control.Text = ((i + 1) + ((Convert.ToInt32(pls) - 1) * soDongHang)).ToString();

                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTT" + (i + 1)];
                    control.Text = "";


                    control = this.xrTable2.Rows[i + 2].Controls["ThueSuatXNK" + (i + 1)];
                    control.Text = "";


                    control = this.xrTable2.Rows[i + 2].Controls["TienThueXNK" + (i + 1)];
                    control.Text = "";


                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                    double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK + hmd.ThueTTDB;
                    control.Text = "";


                    control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                    control.Text = "";


                    control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                    control.Text = "";



                    control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];

                    control.Text = "";

                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];

                    control.Text = "";
                }
                else
                {
                    control = this.xrTable1.Rows[i + 3].Controls["DonGiaNT" + (i + 1)];
                    control.Text = hmd.DonGiaKB.ToString("N" + GlobalSettings.SoThapPhan.DonGiaNT);

                    control = this.xrTable1.Rows[i + 3].Controls["TriGiaNT" + (i + 1)];
                    control.Text = hmd.TriGiaKB.ToString("N" + GlobalSettings.SoThapPhan.TriGiaNT);

                    //STT Thue hang hoa. Hungtq Update 18/12/2010
                    control = this.xrTable2.Rows[i + 2].Controls["STT2" + (i + 1)];
                    //control.Text = ((i + 1) + ((Convert.ToInt32(pls) - 1) * 9)).ToString(); //Comment by Hungtq
                    control.Text = ((i + 1) + ((Convert.ToInt32(pls) - 1) * soDongHang)).ToString();

                    control = this.xrTable2.Rows[i + 2].Controls["TriGiaTT" + (i + 1)];
                    control.Text = hmd.TriGiaTT.ToString("N0");
                    //if (hmd.MienThue == 1)
                    //    control.Text = "";

                    if (MienThue1) control.Text = "";

                    control = this.xrTable2.Rows[i + 2].Controls["ThueSuatXNK" + (i + 1)];
                    if (hmd.ThueTuyetDoi)
                    {
                        control.Text = "";
                    }
                    //else if (hmd.MienThue == 1)
                    //    control.Text = "";
                    else
                    {
                        if (hmd.ThueSuatXNKGiam == 0)
                            control.Text = hmd.ThueSuatXNK.ToString("N0");
                        else
                            control.Text = hmd.ThueSuatXNKGiam.ToString();
                    }
                    if (MienThue1) control.Text = "";
                    control = this.xrTable2.Rows[i + 2].Controls["TienThueXNK" + (i + 1)];
                    control.Text = hmd.ThueXNK.ToString("N0");
                    if (MienThue1) control.Text = "";
                    //if (hmd.MienThue == 1) control.Text = "";
                    if (hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0 && !TypeTax)
                    {
                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                        double TriGiaTTGTGT = hmd.TriGiaTT + hmd.ThueXNK;
                        control.Text = TriGiaTTGTGT.ToString("N0");
                        if (MienThue2) control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                        if (hmd.ThueSuatXNKGiam == 0)
                            control.Text = hmd.ThueSuatGTGT.ToString("N0");
                        else
                            control.Text = hmd.ThueSuatVATGiam.ToString();
                        if (MienThue2) control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                        control.Text = hmd.ThueGTGT.ToString("N0");
                        if (MienThue2) control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];
                        if (Convert.ToDouble(hmd.TyLeThuKhac) > 0)
                            control.Text = hmd.TyLeThuKhac.ToString("N2");
                        else
                            control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];
                        if (Convert.ToDouble(hmd.TriGiaThuKhac) > 0)
                            control.Text = hmd.TriGiaThuKhac.ToString("N0");
                        else
                            control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";
                    }
                    else if (hmd.ThueGTGT == 0 && hmd.ThueTTDB > 0 && !TypeTax)
                    {
                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                        double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        control.Text = TriGiaTTTTDB.ToString("N0");
                        if (MienThue2) control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                        if (hmd.ThueSuatXNKGiam == 0)
                            control.Text = hmd.ThueSuatTTDB.ToString("N0");
                        else
                            control.Text = hmd.ThueSuatTTDBGiam.ToString();
                        if (MienThue2) control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                        control.Text = hmd.ThueTTDB.ToString("N0");
                        if (MienThue2) control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];

                        control.Text = hmd.TyLeThuKhac.ToString("N2");
                        //if (hmd.MienThue == 1) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];

                        control.Text = hmd.TriGiaThuKhac.ToString("N0");
                        //if (hmd.MienThue == 1) control.Text = "";
                    }
                    else if (TypeTax)
                    {
                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaTTGTGT" + (i + 1)];
                        double TriGiaTTTTDB = hmd.TriGiaTT + hmd.ThueXNK;
                        control.Text = TriGiaTTTTDB.ToString("N0");
                        if (MienThue2) control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";

                        control = this.xrTable2.Rows[i + 2].Controls["ThueSuatGTGT" + (i + 1)];
                        if (hmd.ThueSuatXNKGiam == 0)
                            control.Text = hmd.ThueSuatTTDB.ToString("N0");
                        else
                            control.Text = hmd.ThueSuatTTDBGiam.ToString();
                        //if (hmd.MienThue == 1) control.Text = "";
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TienThueGTGT" + (i + 1)];
                        control.Text = hmd.ThueTTDB.ToString("N0");
                        //if (hmd.MienThue == 1) control.Text = "";
                        if (MienThue2) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TyLeThuKhac" + (i + 1)];

                        if (hmd.ThueSuatXNKGiam == 0)
                            control.Text = hmd.ThueSuatGTGT.ToString("N0");
                        else
                            control.Text = hmd.ThueSuatVATGiam.ToString();
                        if (MienThue2) control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";
                        control = this.xrTable2.Rows[i + 2].Controls["TriGiaThuKhac" + (i + 1)];

                        control.Text = hmd.ThueGTGT.ToString("N0");
                        if (MienThue2) control.Text = "";
                        //if (hmd.MienThue == 1) control.Text = "";
                    }
                }

                if (!hmd.FOC)
                {
                    tongTriGiaNT += Math.Round(hmd.TriGiaKB, GlobalSettings.SoThapPhan.TriGiaNT, MidpointRounding.AwayFromZero);
                    tongThueXNK += hmd.ThueXNK;
                    if  (TypeTax)
                    {
                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.ThueGTGT;
                       
                    }
                    else if (hmd.ThueTTDB > 0 && hmd.ThueGTGT == 0)
                    {

                        tongThueGTGT += hmd.ThueTTDB;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                    else if(hmd.ThueGTGT > 0 && hmd.ThueTTDB == 0)
                    {
                        tongThueGTGT += hmd.ThueGTGT;
                        tongTriGiaThuKhac += hmd.TriGiaThuKhac;
                    }
                }
                if (hmd.MienThue == 1)
                { 

                }
            }
            lblTongbangso.Text = this.TinhTongThueHMD().ToString();
            string s = Company.KD.BLL.Utils.VNCurrency.ToString(this.TinhTongThueHMD()).Trim();
            s = s[0].ToString().ToUpper() + s.Substring(1);
            lblTongThueXNKChu.Text = s.Replace("  ", " ");

            lblTongTriGiaNT.Text = tongTriGiaNT.ToString("N" + GlobalSettings.SoThapPhan.TriGiaNT);
            lblTongTienThueXNK.Text = tongThueXNK.ToString("N0");
            lblTongTienThueGTGT.Text = tongThueGTGT.ToString("N0");
            if (tongTriGiaThuKhac > 0)
                lblTongTriGiaThuKhac.Text = tongTriGiaThuKhac.ToString("N0");
            else
                lblTongTriGiaThuKhac.Text = "";
            if (MienThue1) lblTongTienThueXNK.Text = "";
            if (MienThue2) lblTongTienThueGTGT.Text = "";
        }
        public double TinhTongThueHMD()
        {
            double tong = 0;
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
            {
                if (!hmd.FOC)
                    tong += hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac;
            }
            return tong;
        }
        public void setVisibleImage(bool t)
        {
            //xrPictureBox1.Visible = t;
        }
        private bool IsHaveTax()
        {
            foreach (HangMauDich hmd in this.TKMD.HMDCollection)
                if ((hmd.ThueXNK + hmd.ThueTTDB + hmd.ThueGTGT + hmd.TriGiaThuKhac) > 0) return true;
            return false;
        }
        public void ShowMienThue(bool t)
        {
            // lblThongBaoMienThue.Visible = t;
            xrTable2.Visible = !t;
            lblTongTienThueXNK.Visible = lblTongTienThueGTGT.Visible = lblTongTriGiaThuKhac.Visible = !t;
        }
        private void TenHang1_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        public void setNhomHang(XRControl cell, string tenHang)
        {
            cell.Text = tenHang;
        }

        private void TenHang1_PreviewClick_1(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang2_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang3_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang4_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang5_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang6_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang7_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang8_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }

        private void TenHang9_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRControl cell = (XRControl)sender;
            report.Cell = cell;
            report.txtTenNhomHang.Text = cell.Text;
            report.label3.Text = cell.Tag.ToString();
        }
    }
}
