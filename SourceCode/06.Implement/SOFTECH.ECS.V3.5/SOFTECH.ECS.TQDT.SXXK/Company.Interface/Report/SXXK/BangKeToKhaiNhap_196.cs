﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.BLL.SXXK.ThanhKhoan;
using Company.BLL.SXXK.ToKhai;

namespace Company.Interface.Report.SXXK
{
    public partial class BangKeToKhaiNhap_196 : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        public int SoHoSo;
        public bool isHoanThue = false;

        public BangKeToKhaiNhap_196()
        {
            InitializeComponent();
        }
        public void BindReport(string where)
        {
            this.PrintingSystem.ShowMarginsWarning = false;
            DataTable dt = new Company.BLL.KDT.SXXK.BKToKhaiNhap().getBKToKhaiNhapForReport(LanThanhLy, GlobalSettings.SoThapPhan.MauBC01, GlobalSettings.MA_DON_VI,GlobalSettings.MA_HAI_QUAN, where).Tables[0];
            if (GlobalSettings.SoThapPhan.MauBC01 == 1)
                xrLabel6.Text = "Tổng số thuế nhập khẩu không thu (VNĐ)";
            dt.TableName = "BKToKhaiNhap";
            this.DataSource = dt;
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            if (SoHoSo > 0)
            {
                lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
                lblSHSTK.Width = 200;
            }
            lblSTT.DataBindings.Add("Text", this.DataSource, dt.TableName + ".STT");
            lblSoToKhai.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoToKhai");
            lblNgayDangKy.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayDangKy", "{0:dd/MM/yy}");
            lblNgayThucXuat.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHoanThanh", "{0:dd/MM/yy}");
            lblSoHopDong.DataBindings.Add("Text", this.DataSource, dt.TableName + ".SoHopDong");
            lblNgayHopDong.DataBindings.Add("Text", this.DataSource, dt.TableName + ".NgayHopDong", "{0:dd/MM/yy}");
            lblTienThueHoan.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueNK", "{0:N0}");
            lblTongThueNK.DataBindings.Add("Text", this.DataSource, dt.TableName + ".ThueNK", "{0:N0}");
            xrLabel20.Text = xrLabel19.Text = ".........., ngày ... tháng ... năm......";

        }

        private void lblGhiChu_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            int soToKhai = Convert.ToInt32(GetCurrentColumnValue("SoToKhaiNhap"));
            string maLH = GetCurrentColumnValue("MaLoaiHinh").ToString();
            int namDangKy = Convert.ToDateTime(GetCurrentColumnValue("NgayDangKy")).Year;
            if (ToKhaiMauDich.CheckThanhKhoanHet(soToKhai, maLH, namDangKy, this.LanThanhLy) == 0) lblGhiChu.Text = "Thanh khoản hết";
            else lblGhiChu.Text = "";
            //DN
            xrLabel19.Text = GlobalSettings.TieudeNgay;
            //HQ
            //xrLabel20.Text = GlobalSettings.TieudeNgay;

        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

            int soToKhai = Convert.ToInt32(GetCurrentColumnValue("SoToKhaiNhap"));
            string maLH = GetCurrentColumnValue("MaLoaiHinh").ToString();
            int namDangKy = Convert.ToDateTime(GetCurrentColumnValue("NgayDangKy")).Year;

            if (ToKhaiMauDich.CheckDaTungThanhKhoan(soToKhai, maLH, namDangKy, this.LanThanhLy))
                this.xrTableRow1.ForeColor = Color.Blue;
            else
                this.xrTableRow1.ForeColor = Color.Black;
        }
    }
}
