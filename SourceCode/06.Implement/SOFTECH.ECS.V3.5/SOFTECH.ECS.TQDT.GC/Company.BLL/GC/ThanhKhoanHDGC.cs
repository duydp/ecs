using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Company.GC.BLL.GC
{
	public partial class ThanhKhoanHDGC 
	{


        public  DataTable GetLuongNPLTaiXuat_TT13 (long IDHOPDONG)
        {
            string strName = string.Format(@"SELECT MaPhu, SUM(TAIXUAT.SoLuong) AS SoLuongTaiXuat --, SUM(SoLuong)
                                FROM 
                                (
                                SELECT MaPhu AS MaPhu,SoLuong AS SoLuong FROM t_KDT_HangMauDich 
                                WHERE TKMD_ID IN 
                                (SELECT ID FROM t_kdt_tokhaimaudich 
                                 WHERE TrangThaiXuLy = 1 AND   IDHopDong = {0} AND LoaiHangHoa = 'N' AND MaLoaiHinh LIKE 'X%')
                                UNION ALL

                                SELECT MaHang AS MaPhu, SoLuong AS SoLuong 
                                FROM t_KDT_GC_HangChuyenTiep WHERE Master_ID IN 
                                (SELECT ID FROM t_kdt_gc_Tokhaichuyentiep 
                                 WHERE TrangThaiXuLy = 1 AND   IDHopDong = {0} AND ((LoaiHangHoa = 'N' AND MaLoaiHinh LIKE 'X%') OR (MaLoaiHinh Like 'PHPLX')))
                                ) AS TAIXUAT
                                GROUP BY MaPhu", IDHOPDONG);
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(strName);
            return this.db.ExecuteDataSet(dbCommand).Tables[0];
        }

            


        public DataSet getDuLieuThanhKhoan(long  dk)
        {
            string strName = "select * from t_GC_ThanhKhoanHDGC where OldHD_ID=" + dk;
            SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(strName);           
            return this.db.ExecuteDataSet(dbCommand);
        }

        public int DeleteHDTK(long HopDong_ID)
            {
                string strName = "Delete from t_GC_ThanhKhoanHDGC where OldHD_ID=@OldHD_ID ";
                SqlCommand dbCommand = (SqlCommand)this.db.GetSqlStringCommand(strName);
                db.AddInParameter(dbCommand, "@OldHD_ID", DbType.Int64, HopDong_ID);               
                return  this.db.ExecuteNonQuery(dbCommand);
            }
        public static string GetSoHD(object id)
        {
            SqlDatabase db1 = (SqlDatabase)DatabaseFactory.CreateDatabase();
            string query = string.Format("SELECT * FROM t_KDT_GC_HopDong WHERE ID = {0} ORDER BY ID", id.ToString());
            DbCommand dbCommand = db1.GetSqlStringCommand(query);
            IDataReader reader = db1.ExecuteReader(dbCommand);
            if (reader.Read())
            {
                string s = reader["SoHopDong"].ToString();
                reader.Close();
                return s;
            }
            reader.Close();
            return id.ToString();
        }

        public bool UpdateGrid(ThanhKhoanHDGCCollection  collection)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (ThanhKhoanHDGC  item in collection)
                    {
                        ThanhKhoanHDGC tk = new ThanhKhoanHDGC();
                        tk.HopDong_ID = item.HopDong_ID;
                        tk.STT = item.STT;
                        tk.Ten = item.Ten;
                        tk.TongLuongCU = item.TongLuongCU ;
                        tk.TongLuongNK = item.TongLuongNK;
                        tk.TongLuongXK = item.TongLuongXK;
                        tk.OldHD_ID = item.OldHD_ID;
                        tk.KetLuanXLCL = item.KetLuanXLCL;
                        tk.DVT = item.DVT;
                        tk.ChenhLech = item.ChenhLech;                        
                        
                        if (tk.UpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

        public bool UpdateDataSetFromGrid(DataSet  ds)
        {
            bool ret;
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    bool ret01 = true;
                    foreach (DataRow dr in ds.Tables[0].Rows )
                    {
                        ThanhKhoanHDGC tk = new ThanhKhoanHDGC();
                        tk.HopDong_ID = Convert.ToInt64( dr["HopDong_ID"]);
                        tk.STT = Convert.ToInt32(dr["STT"]);
                        tk.Ten = dr["Ten"].ToString();
                        tk.TongLuongCU = Convert.ToDecimal(dr["TongLuongCU"]);
                        tk.TongLuongNK = Convert.ToDecimal(dr["TongLuongNK"]);
                        tk.TongLuongXK = Convert.ToDecimal(dr["TongLuongXK"]);
                        tk.OldHD_ID = Convert.ToInt64(dr["OldHD_ID"]);
                        tk.KetLuanXLCL = dr["KetLuanXLCL"].ToString();
                        tk.DVT = dr["DVT"].ToString();
                        tk.ChenhLech = Convert.ToDecimal(dr["ChenhLech"]);

                        if (tk.UpdateTransaction(transaction) <= 0)
                        {
                            ret01 = false;
                            break;
                        }
                    }
                    if (ret01)
                    {
                        transaction.Commit();
                        ret = true;
                    }
                    else
                    {
                        transaction.Rollback();
                        ret = false;
                    }
                }
                catch
                {
                    ret = false;
                    transaction.Rollback();
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }

	}	
}