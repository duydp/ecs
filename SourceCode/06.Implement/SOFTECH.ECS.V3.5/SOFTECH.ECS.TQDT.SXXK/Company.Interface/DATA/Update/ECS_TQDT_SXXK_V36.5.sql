-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_SelectAll]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Insert]
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@NgayBatDauBC datetime,
	@NgayKetthucBC datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TenDoanhNghiep nvarchar(255),
	@TenKho nvarchar(255),
	@MaKho nvarchar(17),
	@SoTKChungTu nvarchar(30),
	@Loai numeric(2, 0),
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQTiepNhanHD nvarchar(6),
	@NgayHetHanHD datetime,
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseImport]
(
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetthucBC],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[TenKho],
	[MaKho],
	[SoTKChungTu],
	[Loai],
	[SoHopDong],
	[NgayHopDong],
	[MaHQTiepNhanHD],
	[NgayHetHanHD],
	[GhiChuKhac],
	[GuidStr]
)
VALUES 
(
	@TrangThaiXuLy,
	@SoTN,
	@NgayTN,
	@NgayBatDauBC,
	@NgayKetthucBC,
	@MaHQ,
	@MaDoanhNghiep,
	@TenDoanhNghiep,
	@TenKho,
	@MaKho,
	@SoTKChungTu,
	@Loai,
	@SoHopDong,
	@NgayHopDong,
	@MaHQTiepNhanHD,
	@NgayHetHanHD,
	@GhiChuKhac,
	@GuidStr
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Update]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@NgayBatDauBC datetime,
	@NgayKetthucBC datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TenDoanhNghiep nvarchar(255),
	@TenKho nvarchar(255),
	@MaKho nvarchar(17),
	@SoTKChungTu nvarchar(30),
	@Loai numeric(2, 0),
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQTiepNhanHD nvarchar(6),
	@NgayHetHanHD datetime,
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max)
AS

UPDATE
	[dbo].[T_KDT_VNACCS_WarehouseImport]
SET
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[SoTN] = @SoTN,
	[NgayTN] = @NgayTN,
	[NgayBatDauBC] = @NgayBatDauBC,
	[NgayKetthucBC] = @NgayKetthucBC,
	[MaHQ] = @MaHQ,
	[MaDoanhNghiep] = @MaDoanhNghiep,
	[TenDoanhNghiep] = @TenDoanhNghiep,
	[TenKho] = @TenKho,
	[MaKho] = @MaKho,
	[SoTKChungTu] = @SoTKChungTu,
	[Loai] = @Loai,
	[SoHopDong] = @SoHopDong,
	[NgayHopDong] = @NgayHopDong,
	[MaHQTiepNhanHD] = @MaHQTiepNhanHD,
	[NgayHetHanHD] = @NgayHetHanHD,
	[GhiChuKhac] = @GhiChuKhac,
	[GuidStr] = @GuidStr
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_InsertUpdate]
	@ID bigint,
	@TrangThaiXuLy int,
	@SoTN bigint,
	@NgayTN datetime,
	@NgayBatDauBC datetime,
	@NgayKetthucBC datetime,
	@MaHQ nvarchar(6),
	@MaDoanhNghiep nvarchar(17),
	@TenDoanhNghiep nvarchar(255),
	@TenKho nvarchar(255),
	@MaKho nvarchar(17),
	@SoTKChungTu nvarchar(30),
	@Loai numeric(2, 0),
	@SoHopDong nvarchar(80),
	@NgayHopDong datetime,
	@MaHQTiepNhanHD nvarchar(6),
	@NgayHetHanHD datetime,
	@GhiChuKhac nvarchar(2000),
	@GuidStr varchar(max)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_VNACCS_WarehouseImport] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_VNACCS_WarehouseImport] 
		SET
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[SoTN] = @SoTN,
			[NgayTN] = @NgayTN,
			[NgayBatDauBC] = @NgayBatDauBC,
			[NgayKetthucBC] = @NgayKetthucBC,
			[MaHQ] = @MaHQ,
			[MaDoanhNghiep] = @MaDoanhNghiep,
			[TenDoanhNghiep] = @TenDoanhNghiep,
			[TenKho] = @TenKho,
			[MaKho] = @MaKho,
			[SoTKChungTu] = @SoTKChungTu,
			[Loai] = @Loai,
			[SoHopDong] = @SoHopDong,
			[NgayHopDong] = @NgayHopDong,
			[MaHQTiepNhanHD] = @MaHQTiepNhanHD,
			[NgayHetHanHD] = @NgayHetHanHD,
			[GhiChuKhac] = @GhiChuKhac,
			[GuidStr] = @GuidStr
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseImport]
		(
			[TrangThaiXuLy],
			[SoTN],
			[NgayTN],
			[NgayBatDauBC],
			[NgayKetthucBC],
			[MaHQ],
			[MaDoanhNghiep],
			[TenDoanhNghiep],
			[TenKho],
			[MaKho],
			[SoTKChungTu],
			[Loai],
			[SoHopDong],
			[NgayHopDong],
			[MaHQTiepNhanHD],
			[NgayHetHanHD],
			[GhiChuKhac],
			[GuidStr]
		)
		VALUES 
		(
			@TrangThaiXuLy,
			@SoTN,
			@NgayTN,
			@NgayBatDauBC,
			@NgayKetthucBC,
			@MaHQ,
			@MaDoanhNghiep,
			@TenDoanhNghiep,
			@TenKho,
			@MaKho,
			@SoTKChungTu,
			@Loai,
			@SoHopDong,
			@NgayHopDong,
			@MaHQTiepNhanHD,
			@NgayHetHanHD,
			@GhiChuKhac,
			@GuidStr
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_VNACCS_WarehouseImport]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_VNACCS_WarehouseImport] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetthucBC],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[TenKho],
	[MaKho],
	[SoTKChungTu],
	[Loai],
	[SoHopDong],
	[NgayHopDong],
	[MaHQTiepNhanHD],
	[NgayHetHanHD],
	[GhiChuKhac],
	[GuidStr]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseImport]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetthucBC],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[TenKho],
	[MaKho],
	[SoTKChungTu],
	[Loai],
	[SoHopDong],
	[NgayHopDong],
	[MaHQTiepNhanHD],
	[NgayHetHanHD],
	[GhiChuKhac],
	[GuidStr]
FROM [dbo].[T_KDT_VNACCS_WarehouseImport] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_SelectAll]



















AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[TrangThaiXuLy],
	[SoTN],
	[NgayTN],
	[NgayBatDauBC],
	[NgayKetthucBC],
	[MaHQ],
	[MaDoanhNghiep],
	[TenDoanhNghiep],
	[TenKho],
	[MaKho],
	[SoTKChungTu],
	[Loai],
	[SoHopDong],
	[NgayHopDong],
	[MaHQTiepNhanHD],
	[NgayHetHanHD],
	[GhiChuKhac],
	[GuidStr]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseImport]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectBy_WarehouseImport_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectBy_WarehouseImport_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_DeleteBy_WarehouseImport_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_DeleteBy_WarehouseImport_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Insert]
	@WarehouseImport_ID bigint,
	@STT numeric(5, 0),
	@SoPhieuNhap nvarchar(50),
	@NgayPhieuNhap datetime,
	@TenNguoiGiaoHang nvarchar(255),
	@MaNguoiGiaoHang nvarchar(17),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseImport_Details]
(
	[WarehouseImport_ID],
	[STT],
	[SoPhieuNhap],
	[NgayPhieuNhap],
	[TenNguoiGiaoHang],
	[MaNguoiGiaoHang]
)
VALUES 
(
	@WarehouseImport_ID,
	@STT,
	@SoPhieuNhap,
	@NgayPhieuNhap,
	@TenNguoiGiaoHang,
	@MaNguoiGiaoHang
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Update]
	@ID bigint,
	@WarehouseImport_ID bigint,
	@STT numeric(5, 0),
	@SoPhieuNhap nvarchar(50),
	@NgayPhieuNhap datetime,
	@TenNguoiGiaoHang nvarchar(255),
	@MaNguoiGiaoHang nvarchar(17)
AS

UPDATE
	[dbo].[T_KDT_VNACCS_WarehouseImport_Details]
SET
	[WarehouseImport_ID] = @WarehouseImport_ID,
	[STT] = @STT,
	[SoPhieuNhap] = @SoPhieuNhap,
	[NgayPhieuNhap] = @NgayPhieuNhap,
	[TenNguoiGiaoHang] = @TenNguoiGiaoHang,
	[MaNguoiGiaoHang] = @MaNguoiGiaoHang
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_InsertUpdate]
	@ID bigint,
	@WarehouseImport_ID bigint,
	@STT numeric(5, 0),
	@SoPhieuNhap nvarchar(50),
	@NgayPhieuNhap datetime,
	@TenNguoiGiaoHang nvarchar(255),
	@MaNguoiGiaoHang nvarchar(17)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_VNACCS_WarehouseImport_Details] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_VNACCS_WarehouseImport_Details] 
		SET
			[WarehouseImport_ID] = @WarehouseImport_ID,
			[STT] = @STT,
			[SoPhieuNhap] = @SoPhieuNhap,
			[NgayPhieuNhap] = @NgayPhieuNhap,
			[TenNguoiGiaoHang] = @TenNguoiGiaoHang,
			[MaNguoiGiaoHang] = @MaNguoiGiaoHang
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseImport_Details]
		(
			[WarehouseImport_ID],
			[STT],
			[SoPhieuNhap],
			[NgayPhieuNhap],
			[TenNguoiGiaoHang],
			[MaNguoiGiaoHang]
		)
		VALUES 
		(
			@WarehouseImport_ID,
			@STT,
			@SoPhieuNhap,
			@NgayPhieuNhap,
			@TenNguoiGiaoHang,
			@MaNguoiGiaoHang
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_VNACCS_WarehouseImport_Details]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_DeleteBy_WarehouseImport_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_DeleteBy_WarehouseImport_ID]
	@WarehouseImport_ID bigint
AS

DELETE FROM [dbo].[T_KDT_VNACCS_WarehouseImport_Details]
WHERE
	[WarehouseImport_ID] = @WarehouseImport_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_VNACCS_WarehouseImport_Details] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseImport_ID],
	[STT],
	[SoPhieuNhap],
	[NgayPhieuNhap],
	[TenNguoiGiaoHang],
	[MaNguoiGiaoHang]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseImport_Details]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectBy_WarehouseImport_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectBy_WarehouseImport_ID]
	@WarehouseImport_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseImport_ID],
	[STT],
	[SoPhieuNhap],
	[NgayPhieuNhap],
	[TenNguoiGiaoHang],
	[MaNguoiGiaoHang]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseImport_Details]
WHERE
	[WarehouseImport_ID] = @WarehouseImport_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[WarehouseImport_ID],
	[STT],
	[SoPhieuNhap],
	[NgayPhieuNhap],
	[TenNguoiGiaoHang],
	[MaNguoiGiaoHang]
FROM [dbo].[T_KDT_VNACCS_WarehouseImport_Details] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_Detail_SelectAll]







AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseImport_ID],
	[STT],
	[SoPhieuNhap],
	[NgayPhieuNhap],
	[TenNguoiGiaoHang],
	[MaNguoiGiaoHang]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseImport_Details]	

GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Insert]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Update]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Delete]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Load]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectAll]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectBy_WarehouseImport_Details_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectBy_WarehouseImport_Details_ID]

IF OBJECT_ID(N'[dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_DeleteBy_WarehouseImport_Details_ID]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_DeleteBy_WarehouseImport_Details_ID]


GO


-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Insert]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Insert]
	@WarehouseImport_Details_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@LoaiHangHoa numeric(2, 0),
	@MaDinhDanhSX nvarchar(50),
	@NguonNhap numeric(3, 0),
	@SoLuongDuKienNhap numeric(18, 4),
	@SoLuongThucNhap numeric(18, 4),
	@DVT nvarchar(4),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails]
(
	[WarehouseImport_Details_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[LoaiHangHoa],
	[MaDinhDanhSX],
	[NguonNhap],
	[SoLuongDuKienNhap],
	[SoLuongThucNhap],
	[DVT]
)
VALUES 
(
	@WarehouseImport_Details_ID,
	@STT,
	@TenHangHoa,
	@MaHangHoa,
	@LoaiHangHoa,
	@MaDinhDanhSX,
	@NguonNhap,
	@SoLuongDuKienNhap,
	@SoLuongThucNhap,
	@DVT
)

SET @ID = SCOPE_IDENTITY()

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Update]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Update]
	@ID bigint,
	@WarehouseImport_Details_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@LoaiHangHoa numeric(2, 0),
	@MaDinhDanhSX nvarchar(50),
	@NguonNhap numeric(3, 0),
	@SoLuongDuKienNhap numeric(18, 4),
	@SoLuongThucNhap numeric(18, 4),
	@DVT nvarchar(4)
AS

UPDATE
	[dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails]
SET
	[WarehouseImport_Details_ID] = @WarehouseImport_Details_ID,
	[STT] = @STT,
	[TenHangHoa] = @TenHangHoa,
	[MaHangHoa] = @MaHangHoa,
	[LoaiHangHoa] = @LoaiHangHoa,
	[MaDinhDanhSX] = @MaDinhDanhSX,
	[NguonNhap] = @NguonNhap,
	[SoLuongDuKienNhap] = @SoLuongDuKienNhap,
	[SoLuongThucNhap] = @SoLuongThucNhap,
	[DVT] = @DVT
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_InsertUpdate]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_InsertUpdate]
	@ID bigint,
	@WarehouseImport_Details_ID bigint,
	@STT numeric(5, 0),
	@TenHangHoa nvarchar(255),
	@MaHangHoa nvarchar(50),
	@LoaiHangHoa numeric(2, 0),
	@MaDinhDanhSX nvarchar(50),
	@NguonNhap numeric(3, 0),
	@SoLuongDuKienNhap numeric(18, 4),
	@SoLuongThucNhap numeric(18, 4),
	@DVT nvarchar(4)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails] 
		SET
			[WarehouseImport_Details_ID] = @WarehouseImport_Details_ID,
			[STT] = @STT,
			[TenHangHoa] = @TenHangHoa,
			[MaHangHoa] = @MaHangHoa,
			[LoaiHangHoa] = @LoaiHangHoa,
			[MaDinhDanhSX] = @MaDinhDanhSX,
			[NguonNhap] = @NguonNhap,
			[SoLuongDuKienNhap] = @SoLuongDuKienNhap,
			[SoLuongThucNhap] = @SoLuongThucNhap,
			[DVT] = @DVT
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails]
		(
			[WarehouseImport_Details_ID],
			[STT],
			[TenHangHoa],
			[MaHangHoa],
			[LoaiHangHoa],
			[MaDinhDanhSX],
			[NguonNhap],
			[SoLuongDuKienNhap],
			[SoLuongThucNhap],
			[DVT]
		)
		VALUES 
		(
			@WarehouseImport_Details_ID,
			@STT,
			@TenHangHoa,
			@MaHangHoa,
			@LoaiHangHoa,
			@MaDinhDanhSX,
			@NguonNhap,
			@SoLuongDuKienNhap,
			@SoLuongThucNhap,
			@DVT
		)		
	END
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Delete]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails]
WHERE
	[ID] = @ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_DeleteBy_WarehouseImport_Details_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_DeleteBy_WarehouseImport_Details_ID]
	@WarehouseImport_Details_ID bigint
AS

DELETE FROM [dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails]
WHERE
	[WarehouseImport_Details_ID] = @WarehouseImport_Details_ID

GO

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_DeleteDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Load]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseImport_Details_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[LoaiHangHoa],
	[MaDinhDanhSX],
	[NguonNhap],
	[SoLuongDuKienNhap],
	[SoLuongThucNhap],
	[DVT]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails]
WHERE
	[ID] = @ID
GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectBy_WarehouseImport_Details_ID]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectBy_WarehouseImport_Details_ID]
	@WarehouseImport_Details_ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseImport_Details_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[LoaiHangHoa],
	[MaDinhDanhSX],
	[NguonNhap],
	[SoLuongDuKienNhap],
	[SoLuongThucNhap],
	[DVT]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails]
WHERE
	[WarehouseImport_Details_ID] = @WarehouseImport_Details_ID

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectDynamic]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[WarehouseImport_Details_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[LoaiHangHoa],
	[MaDinhDanhSX],
	[NguonNhap],
	[SoLuongDuKienNhap],
	[SoLuongThucNhap],
	[DVT]
FROM [dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

-----------------------------------------------
-- Stored procedure name: [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectAll]
-- Database: ECS_TQDT_GC_V4_TAT
-----------------------------------------------

CREATE PROCEDURE [dbo].[p_T_KDT_VNACCS_WarehouseImport_GoodsDetail_SelectAll]











AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[WarehouseImport_Details_ID],
	[STT],
	[TenHangHoa],
	[MaHangHoa],
	[LoaiHangHoa],
	[MaDinhDanhSX],
	[NguonNhap],
	[SoLuongDuKienNhap],
	[SoLuongThucNhap],
	[DVT]
FROM
	[dbo].[T_KDT_VNACCS_WarehouseImport_GoodsDetails]	

GO

GO
IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '36.5') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('36.5',GETDATE(), N'CẬP NHẬT MÃ ĐỊNH DANH HÀNG HÓA TT MỚI')
END