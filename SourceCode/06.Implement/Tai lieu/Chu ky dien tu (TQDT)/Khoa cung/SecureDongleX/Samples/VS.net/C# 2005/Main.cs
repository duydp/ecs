using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace sample
{
   public partial class frmMain : Form
   {
      //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-==-=-=-
      [DllImport("SDX.dll")]
      static extern int SDX_Find();
      [DllImport("SDX.dll")]
      static extern int SDX_Open(int mode, Int32 uid, ref Int32 hid);
      [DllImport("SDX.dll")]
      static extern void SDX_Close(int handle);
      [DllImport("SDX.dll")]
      static extern int SDX_Read(int handle, int block_index, byte[] buffer512);
      [DllImport("SDX.dll")]
      static extern int SDX_Write(int handle, int block_index, String buffer512);
      [DllImport("SDX.dll")]
      static extern int SDX_GetVersion(int handle);
      [DllImport("SDX.dll")]      
      static extern int SDX_Transform(int handle, int len, byte[] bufferData);
      [DllImport("SDX.dll")]
      static extern int SDX_RSAEncrypt(int handle, int startIndex, String bufferData, ref int len, byte[] key512);
      [DllImport("SDX.dll")]
      static extern int SDX_RSADecrypt(int handle, int startIndex, byte[] bufferData, ref int len, byte[] key512);

      const int HID_MODE = -1;
      //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
      int ret = 0;
      Int32 handle = 0;
      Int32 hid = 0;
      Int32 uid = 0;
      String seed, Writebuff, error;
      byte[] key512 = new byte[512];
      //=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--=-=-=-=-=-=-=
      public frmMain()
      {
         InitializeComponent();
      }

      private void btnWrite_Click(object sender, EventArgs e)
      {
         // Write to SecureDongle X with specified UID
         ret = SDX_Find();
         if (ret < 0)
         {
            error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }

         if (ret == 0)
         {
            lstOutput.Items.Add("No SecureDongle X plugged");
            return;
         }

         lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
         ret = SDX_Open(1, uid, ref hid);
         if (ret < 0)
         {
            error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         handle = ret;
         error = String.Format("Open SecureDongle X: {0:X}", handle);
         lstOutput.Items.Add(error);

         // Write 'SDX - Sample Data' to block 0
         Writebuff = "SDX - Sample Data";
         ret = SDX_Write(handle, 0, Writebuff);
         if (ret < 0)
         {
            error = String.Format("Error Writing data: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         lstOutput.Items.Add("Success Writing 'SDX - Sample Data' to Block 0");

         SDX_Close(handle);
      }

      private void btnRead_Click(object sender, EventArgs e)
      {
         // Read SecureDongle X with specified UID
         ret = SDX_Find();
         if (ret < 0)
         {
            error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }

         if (ret == 0)
         {
            lstOutput.Items.Add("No SecureDongle X plugged");
            return;
         }

         lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
         ret = SDX_Open(1, uid, ref hid);
         if (ret < 0)
         {
            error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         handle = ret;
         error = String.Format("Open SecureDongle X: {0:X}", handle);
         lstOutput.Items.Add(error);

         // Read from Block 0      
         byte[] retbuff = new byte[512];
         ret = SDX_Read(handle, 0, retbuff);
         if (ret < 0)
         {
            error = String.Format("Error Reading data: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         string sReturn = System.Text.ASCIIEncoding.ASCII.GetString(retbuff);
         lstOutput.Items.Add("Success Read. Data: " + sReturn);

         SDX_Close(handle);
      }

      private void btnTransform_Click(object sender, EventArgs e)
      {
         // Transform data using specified SecureDongle X
         ret = SDX_Find();
         if (ret < 0)
         {
            error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }

         if (ret == 0)
         {
            lstOutput.Items.Add("No SecureDongle X plugged");
            return;
         }

         lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
         ret = SDX_Open(1, uid, ref hid);
         if (ret < 0)
         {
            error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         handle = ret;
         error = String.Format("Open SecureDongle X: {0:X}", handle);
         lstOutput.Items.Add(error);

         // Transform data
         byte[] retbuff = new byte[512];
         retbuff = ASCIIEncoding.UTF8.GetBytes("SDXTransform Test");
                  
         ret = SDX_Transform(handle, retbuff.Length, retbuff);
         if (ret < 0)
         {
            error = String.Format("Error Transforming: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         string sReturn = System.Text.ASCIIEncoding.ASCII.GetString(retbuff);
         lstOutput.Items.Add("Transform result: " + sReturn);
        
         SDX_Close(handle);
      }

      private void btnEncrypt_Click(object sender, EventArgs e)
      {
         // Encrypt using RSA and store to SecureDongle X with specified UID
         int startIndex, bufferLength;
         string text;

         ret = SDX_Find();
         if (ret < 0)
         {
            error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }

         if (ret == 0)
         {
            lstOutput.Items.Add("No SecureDongle X plugged");
            return;
         }

         lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
         ret = SDX_Open(1, uid, ref hid);
         if (ret < 0)
         {
            error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         handle = ret;
         error = String.Format("Open SecureDongle X: {0:X}", handle);
         lstOutput.Items.Add(error);

         // Encrypt and write to SDX index 100
         text = "SDX Test RSA Encrypt";
         bufferLength = text.Length;
         startIndex = 100;

         ret = SDX_RSAEncrypt(handle, startIndex, text, ref bufferLength, key512);
         if (ret < 0)
         {
            error = String.Format("Error Encrypting data: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         lstOutput.Items.Add("Write success, size: " + bufferLength.ToString() + ". Key to decrypt is stored inside key512 variable.");
         lstOutput.Items.Add("You can try reading the contents of the SDX");

         SDX_Close(handle);
      }

      private void btnDecrypt_Click(object sender, EventArgs e)
      {
         // Read from SecureDongle X with specified UID and decrypt using RSA
         int startIndex, bufferLength;
         byte[] plainText = new byte[512];

         ret = SDX_Find();
         if (ret < 0)
         {
            error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }

         if (ret == 0)
         {
            lstOutput.Items.Add("No SecureDongle X plugged");
            return;
         }

         lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
         ret = SDX_Open(1, uid, ref hid);
         if (ret < 0)
         {
            error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         handle = ret;
         error = String.Format("Open SecureDongle X: {0:X}", handle);
         lstOutput.Items.Add(error);

         // Decrypt with size 256, starting from index 100
         bufferLength = 256;
         startIndex = 100;

         ret = SDX_RSADecrypt(handle, startIndex, plainText, ref bufferLength, key512);
         if (ret < 0)
         {
            error = String.Format("Error Decrypting data: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         string sReturn = System.Text.ASCIIEncoding.ASCII.GetString(plainText);
         lstOutput.Items.Add("Decrypted Data: " + sReturn);

         SDX_Close(handle);
      }

      private void btnWriteMap_Click(object sender, EventArgs e)
      {
         // Write to SecureDongle X with specified UID
         // This is a sample of simple Data Mapping to put plain text data not in order
         // For example, in 5-byte data, byte 1 will be put in SDX memory location 10, byte 2 will be put in SDX memory location 1
         ret = SDX_Find();
         if (ret < 0)
         {
            error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }

         if (ret == 0)
         {
            lstOutput.Items.Add("No SecureDongle X plugged");
            return;
         }

         lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
         ret = SDX_Open(1, uid, ref hid);
         if (ret < 0)
         {
            error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         handle = ret;
         error = String.Format("Open SecureDongle X: {0:X}", handle);
         lstOutput.Items.Add(error);

         // Write encrypted 'SDX - Sample Map Encryption' to block 0
         Writebuff = "SDX - Sample Map Encryption";
         lstOutput.Items.Add("Original data: " + Writebuff);
         lstOutput.Items.Add("Length = " + Writebuff.Length.ToString());

         //****************** ENCRYPTION SAMPLE *******************
         byte[] temp = new byte[512];
         int i, sze, indx, tmp;
         Random rand = new Random();

         if (Writebuff.Length > 512) 
         {
            lstOutput.Items.Add("Error: Data size max is 512");            
            return;
         }
        
         // Create a 512-byte Full Map with randomized content
         for (i=0; i<512; i++) {
            if ((rand.Next(2) % 2) == 0)
            {
               tmp = (rand.Next(26) % 26) + 65;
               temp[i] = (byte)tmp;
            } else {
               tmp = (rand.Next(26) % 26) + 97;
               temp[i] = (byte)tmp;
            }
         }
        
         // Calculate where to put the data, and put it there
         for (i=0; i<Writebuff.Length;i++)
         {
            sze = i * (Writebuff.Length - 1);
            indx = sze % 512;
            temp[indx] = (byte)Writebuff[i];
         }
         String stri = Encoding.GetEncoding(1252).GetString(temp);
         
         //********************************************************
        
         ret = SDX_Write(handle, 0, stri);
         if (ret < 0)
         {
            error = String.Format("Error Writing data: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         lstOutput.Items.Add("Success Writing 'SDX - Sample Map Encryption' to Block 0");

         SDX_Close(handle);
      }

      private void btnReadMap_Click(object sender, EventArgs e)
      {
         // Read from SecureDongle X with specified UID
         // This is the sample to read the Mapped data
         // Don't forget to set the Length of data to be read first
         ret = SDX_Find();
         if (ret < 0)
         {
            error = String.Format("Error Finding SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }

         if (ret == 0)
         {
            lstOutput.Items.Add("No SecureDongle X plugged");
            return;
         }

         lstOutput.Items.Add("Found SecureDongle X: " + ret.ToString());

			uid = 715400947;
         ret = SDX_Open(1, uid, ref hid);
         if (ret < 0)
         {
            error = String.Format("Error Opening SecureDongle X: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
         handle = ret;
         error = String.Format("Open SecureDongle X: {0:X}", handle);
         lstOutput.Items.Add(error);
         short datasize = 27;

         // Read from Block 0      
         byte[] retbuff = new byte[512];
         ret = SDX_Read(handle, 0, retbuff);
         if (ret < 0)
         {
            error = String.Format("Error Reading data: {0:X}", ret);
            lstOutput.Items.Add(error);
            return;
         }
               
         //******************* DECRYPTION **********************
         string buff = "";
         int i, sze, indx;
         i = 0;
         for (i=0; i<datasize; i++) {
           sze = i * (datasize - 1);
           indx = sze % 512;
           buff = buff + (char)retbuff[indx];         
         }
         //*****************************************************
        
         lstOutput.Items.Add("Success Read. Data: " + buff);

         SDX_Close(handle);
      }
   }
}