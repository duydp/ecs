/*
Run this script on:

        192.168.72.100.ECS_TQDT_SXXK_V4_HOATHO_THANHKHOAN    -  This database will be modified

to synchronize it with:

        192.168.72.100.ECS_TQDT_SXXK_V4

You are recommended to back up your database before running this script

Script created by SQL Compare version 8.2.0 from Red Gate Software Ltd at 04/24/2013 9:06:47 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy]'
GO
ALTER TABLE [dbo].[t_KDT_SXXK_HoSoThanhLyDangKy] ALTER COLUMN [GuidStr] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '8.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('8.6',GETDATE(), N'Cap nhat t_kdt_sxxk_HoSoThanhLy')
END