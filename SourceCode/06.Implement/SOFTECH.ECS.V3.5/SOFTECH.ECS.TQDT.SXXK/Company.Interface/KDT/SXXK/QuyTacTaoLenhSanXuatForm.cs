﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;

namespace Company.Interface.KDT.GC
{
    public partial class QuyTacTaoLenhSanXuatForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_LenhSanXuat_QuyTac Config = new KDT_LenhSanXuat_QuyTac();
        public bool isAdd = true;
        public QuyTacTaoLenhSanXuatForm()
        {
            InitializeComponent();
        }
        private void BindDataConfig()
        {
            try
            {
                grList.Refetch();
                grList.DataSource = KDT_LenhSanXuat_QuyTac.SelectCollectionAll();
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == Janus.Windows.GridEX.RowType.Record)
                {
                    Config = new KDT_LenhSanXuat_QuyTac();
                    Config = (KDT_LenhSanXuat_QuyTac)e.Row.DataRow;
                    SetConfigPrefix();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateNull(txtTienTo, errorProvider, "Tiền tố", isOnlyWarning);
                isValid &= ValidateControl.ValidateZero(txtDoDaiSo, errorProvider, "Độ dài số");
                isValid &= ValidateControl.ValidateNull(txtPhanSo, errorProvider, "Phần số");
                isValid &= ValidateControl.ValidateNull(cbbTinhTrang, errorProvider, "Tình trạng", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbbLoaiHinh, errorProvider, "Loại hình");
                isValid &= ValidateControl.ValidateNull(clcYear, errorProvider, "Năm");
                isValid &= ValidateControl.ValidateNull(txtSoPO, errorProvider, "Số hợp đồng");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void GetConfigPrefix()
        {
            try
            {
                Config.TienTo = txtTienTo.Text.ToString().Trim();
                Config.TinhTrang = cbbTinhTrang.SelectedValue.ToString();
                Config.LoaiHinh = cbbLoaiHinh.SelectedValue.ToString();
                Config.GiaTriPhanSo = Convert.ToInt64(txtPhanSo.Text.ToString());
                Config.DoDaiSo = Convert.ToInt64(txtDoDaiSo.Text.ToString());
                Config.Nam = clcYear.Value;
                Config.SoDonHang = txtSoPO.Text;
                Config.HienThi = txtHienThi.Text.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetConfigPrefix()
        {
            try
            {
                txtTienTo.Text = Config.TienTo.ToString();
                cbbTinhTrang.SelectedValue = Config.TinhTrang.ToString();
                cbbLoaiHinh.SelectedValue = Config.LoaiHinh.ToString();
                txtPhanSo.Text = Config.GiaTriPhanSo.ToString();
                txtDoDaiSo.Text = Config.DoDaiSo.ToString();
                clcYear.Value = Config.Nam;
                txtSoPO.Text = Config.SoDonHang.ToString();
                txtHienThi.Text = Config.HienThi.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetConfigPrefix();
                if (isAdd)
                {
                    //Kiểm tra tồn tại Cấu hình chứng từ
                    foreach (KDT_LenhSanXuat_QuyTac item in KDT_LenhSanXuat_QuyTac.SelectCollectionAll())
                    {
                        if (item.TinhTrang == cbbTinhTrang.SelectedValue.ToString())
                        {
                            errorProvider.SetError(cbbTinhTrang, "Tình trạng : ''" + cbbTinhTrang.Text.ToString() + "'' này đã được Cấu hình");
                            return;
                        }
                    }
                    Config.Insert();
                }
                else
                {
                    Config.Update();
                }
                BindDataConfig();
                Config = new KDT_LenhSanXuat_QuyTac();
                txtTienTo.Text = String.Empty;
                txtHienThi.Text = String.Empty;
                isAdd = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtTienTo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoPO.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbTinhTrang_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoPO.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbLoaiHinh_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoPO.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtPhanSo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                txtPhanSo.MaxLength = Convert.ToInt32(txtDoDaiSo.Text.ToString()) - 1;
                if (Convert.ToInt32(txtDoDaiSo.Text.ToString()) != txtPhanSo.Text.Length + 1)
                {
                    //ShowMessage("Giá trị phần số vượt quá hoặc nhỏ hơn Độ dài số ", false);
                    errorProvider.SetError(txtPhanSo, setText("Giá trị phần số vượt quá hoặc nhỏ hơn Độ dài số ", "This value must be greater than 0 "));
                    return;
                }
                txtPhanSo.MaxLength = Convert.ToInt32(txtDoDaiSo.Text.ToString());
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoPO.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtDoDaiSo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                txtPhanSo.MaxLength = Convert.ToInt32(txtDoDaiSo.Text.ToString()) - 1;
                if (Convert.ToInt32(txtDoDaiSo.Text.ToString()) != txtPhanSo.Text.Length + 1)
                {
                    errorProvider.SetError(txtDoDaiSo, setText("Giá trị Độ dài số vượt quá hoặc nhỏ hơn Giá trị Phần số ", "This value must be greater than 0 "));
                    return;
                }
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoPO.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void grList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string TinhTrang = e.Row.Cells["TinhTrang"].Value.ToString();
                    switch (TinhTrang)
                    {
                        case "M":
                            e.Row.Cells["TinhTrang"].Text = "Tạo mới";
                            break;
                        case "Đ":
                            e.Row.Cells["TinhTrang"].Text = "Đang sản xuất";
                            break;
                        case "H":
                            e.Row.Cells["TinhTrang"].Text = "Hoàn thành";
                            break;

                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void QuyTacTaoLenhSanXuatForm_Load(object sender, EventArgs e)
        {
            clcYear.ReadOnly = false;
            txtSoPO.ReadOnly = false;
            BindDataConfig();
        }

        private void txtSoHopDong_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) + "-" + clcYear.Value.Year + "-" + txtSoPO.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void clcYear_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                string Prefix = "";
                Prefix = txtTienTo.Text.ToString().Trim() + cbbTinhTrang.SelectedValue.ToString() + cbbLoaiHinh.SelectedValue.ToString() + Convert.ToInt64(txtPhanSo.Text.ToString() + 1) +"-"  + clcYear.Value.Year + "-"+ txtSoPO.Text.ToString();
                txtHienThi.Text = Prefix;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
