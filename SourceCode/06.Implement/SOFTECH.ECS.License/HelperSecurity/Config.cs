﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using System.IO;

namespace KeySecurity
{
    [XmlRoot("Root")]
    public class Config
    {
        public string ActiveUrl { get; set; }
        public string ProductKeyUrl { get; set; }
        public string ProductID { get; set; }
        public string LicenseFile { get; set; }
        public static Config Install;


        public Config()
        {

        }
        static Config()
        {
            string configName = "Active.cfg";
            if (File.Exists(configName))
            {
                string content = KeyCode.ReadFile(configName);
                Install = KeyCode.Deserialize<Config>(content);
            }
            else
            {
                Install = new Config()
                {
                    ActiveUrl = @"http://localhost:1661/{0}.asmx",
                    ProductKeyUrl = string.Empty,
                    ProductID = "ECS_TQDT_SXXK",
                    LicenseFile = "Softech.lic"
                };
                string content = KeyCode.Serializer(Install);
                KeyCode.WriteToFile(content, configName);
            }
        }
    }
}
