using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Collections.Generic;

namespace Company.BLL.KDT.SXXK
{
	public partial class BangKeHoSoThanhLy 
	{
        public BKNPLChuaThanhLyCollection bkNPLCTLCollection = new BKNPLChuaThanhLyCollection();
        public BKNPLNopThueTieuThuNoiDiaCollection bkNPLNTCollection = new BKNPLNopThueTieuThuNoiDiaCollection();
        public BKNPLTaiXuatCollection bkNPLTXCollection = new BKNPLTaiXuatCollection();
        public List<KDT_SXXK_BKNPLTuCungUng_Detail> bkNPLTCUCollection = new List<KDT_SXXK_BKNPLTuCungUng_Detail>();      
        // public BKNPLTuCungUngCollection bkNPLTCUCollection = new BKNPLTuCungUngCollection();
        public BKNPLTamNopThueCollection bkNPLTNTCollection = new BKNPLTamNopThueCollection();
        public BKNPLXinHuyCollection bkNPLXHCollection = new BKNPLXinHuyCollection();
        public BKNPLXuatGiaCongCollection bkNPLXGCCollection = new BKNPLXuatGiaCongCollection();
        public BKNPLXuatSuDungNKDCollection bkNPLNKDCollection = new BKNPLXuatSuDungNKDCollection();
        public BKToKhaiNhapCollection bkTKNCollection = new BKToKhaiNhapCollection();
        public BKToKhaiXuatCollection bkTKXColletion = new BKToKhaiXuatCollection();
       
        public void LoadChiTietBangKe()
        {
            switch (this.MaBangKe.Trim())
            {
                case "DTLNPLCHUATL":
                    this.bkNPLCTLCollection = BKNPLChuaThanhLy.SelectCollectionBy_BangKeHoSoThanhLy_ID(this.ID);
                    break;
                case "DTLCHITIETNT":
                    this.bkNPLTNTCollection = BKNPLTamNopThue.SelectCollectionBy_BangKeHoSoThanhLy_ID(this.ID);
                    break;
                case "DTLNPLNKD":
                    this.bkNPLNKDCollection = BKNPLXuatSuDungNKD.SelectCollectionBy_BangKeHoSoThanhLy_ID(this.ID);
                    break;
                case "DTLNPLNT":
                    this.bkNPLNTCollection= BKNPLNopThueTieuThuNoiDia.SelectCollectionBy_BangKeHoSoThanhLy_ID(this.ID);
                    break;
                case "DTLNPLTX":
                    this.bkNPLTXCollection = BKNPLTaiXuat.SelectCollectionBy_BangKeHoSoThanhLy_ID(this.ID);
                    break;
                case "DTLNPLXGC":
                    this.bkNPLXGCCollection = BKNPLXuatGiaCong.SelectCollectionBy_BangKeHoSoThanhLy_ID(this.ID);
                    break;
                case "DTLNPLXH":
                    this.bkNPLXHCollection = BKNPLXinHuy.SelectCollectionBy_BangKeHoSoThanhLy_ID(this.ID);
                    break; ;
                case "DTLTKN":
                    //this.bkTKNCollection = BKToKhaiNhap.SelectCollectionBy_BangKeHoSoThanhLy_ID(this.ID);
                    this.bkTKNCollection = BKToKhaiNhap.SelectCollectionBy_BangKeHoSoThanhLy_ID_NgayHoanThanh(this.ID);
                    break;
                case "DTLTKX":
                    //this.bkTKXColletion = BKToKhaiXuat.SelectCollectionBy_BangKeHoSoThanhLy_ID(this.ID);
                    this.bkTKXColletion = BKToKhaiXuat.SelectCollectionBy_BangKeHoSoThanhLy_ID_NgayHoanThanh(this.ID);
                    break;
                case"DTLNPLTCU":
                    this.bkNPLTCUCollection = KDT_SXXK_BKNPLTuCungUng_Detail.SelectCollectionDynamic("BangKeHoSoThanhLy_ID = " +this.ID, "");
                    //this.bkNPLTCUCollection = BKNPLTuCungUng.SelectCollectionBy_BangKeHoSoThanhLy_ID(this.ID);
                    break;
                default:
                    this.bkNPLTCUCollection = KDT_SXXK_BKNPLTuCungUng_Detail.SelectCollectionDynamic("BangKeHoSoThanhLy_ID = " + this.ID, "");
                    //this.bkNPLTCUCollection = BKNPLTuCungUng.SelectCollectionBy_BangKeHoSoThanhLy_ID(this.ID);
                    break;

            }
        }

        public bool InsertUpdateFull(BKNPLChuaThanhLyCollection bkDetailCollection, SqlTransaction transaction)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                if (this._ID == 0)
                    this._ID = this.InsertTransaction(transaction);
                else
                    this.UpdateTransaction(transaction);

                foreach (BKNPLChuaThanhLy bkDetail in bkDetailCollection)
                {
                    if (bkDetail.ID == 0)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    else
                    {
                        bkDetail.UpdateTransaction(transaction);
                    }
                    if (bkDetail.DVT_QuyDoi != null)
                    {
                        bkDetail.DVT_QuyDoi.Master_ID = bkDetail.ID;
                        bkDetail.DVT_QuyDoi.Type = "NPL_CTL";
                        bkDetail.DVT_QuyDoi.InsertUpdate();
                    }


                }
                ret = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ret;
        }
        public bool InsertUpdateFull(BKNPLNopThueTieuThuNoiDiaCollection bkDetailCollection, SqlTransaction transaction)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                if (this._ID == 0)
                    this._ID = this.InsertTransaction(transaction);
                else
                {
                    this.UpdateTransaction(transaction);

                }
                foreach (BKNPLNopThueTieuThuNoiDia bkDetail in bkDetailCollection)
                {
                    bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                    bkDetail.ID = bkDetail.InsertTransaction(transaction);
                }

                ret = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ret;
        }
        public bool InsertUpdateFull(BKNPLTaiXuatCollection bkDetailCollection, SqlTransaction transaction)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                if (this._ID == 0)
                    this._ID = this.InsertTransaction(transaction);
                else
                    this.UpdateTransaction(transaction);

                foreach (BKNPLTaiXuat bkDetail in bkDetailCollection)
                {
                    if (bkDetail.ID == 0)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    else
                    {
                        bkDetail.UpdateTransaction(transaction);
                    }
                }
                ret = true;
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            return ret;
        }
        public bool InsertUpdateFull(BKNPLTamNopThueCollection bkDetailCollection,SqlTransaction transaction)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                if (this._ID == 0)
                    this._ID = this.InsertTransaction(transaction);
                else
                    this.UpdateTransaction(transaction);

                foreach (BKNPLTamNopThue bkDetail in bkDetailCollection)
                {
                    if (bkDetail.ID == 0)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    else
                    {
                        bkDetail.UpdateTransaction(transaction);
                    }
                    if (bkDetail.DVT_QuyDoi != null)
                    {
                        bkDetail.DVT_QuyDoi.Master_ID = bkDetail.ID;
                        bkDetail.DVT_QuyDoi.Type = "NPL_XNT";
                        bkDetail.DVT_QuyDoi.InsertUpdate();
                    }
                }
                ret = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            return ret;
        }
        public bool InsertUpdateFull(BKNPLTuCungUngCollection bkDetailCollection, SqlTransaction transaction)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                if (this._ID == 0)
                    this._ID = this.InsertTransaction(transaction);
                else
                    this.UpdateTransaction(transaction);

                foreach (BKNPLTuCungUng bkDetail in bkDetailCollection)
                {
                    if (bkDetail.ID == 0)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    else
                    {
                        bkDetail.UpdateTransaction(transaction);
                    }
                }
                ret = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ret;
        }
        public bool InsertUpdateFull(List<KDT_SXXK_BKNPLTuCungUng_Detail> bkDetailCollection, SqlTransaction transaction)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                if (this._ID == 0)
                    this._ID = this.InsertTransaction(transaction);
                else
                    this.UpdateTransaction(transaction);

                foreach (KDT_SXXK_BKNPLTuCungUng_Detail bkDetail in bkDetailCollection)
                {
                    if (bkDetail.ID == 0)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertUpdate(transaction);
                    }
                    else
                    {
                        bkDetail.Update(transaction);
                    }
                }
                ret = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ret;
        }
        public bool InsertUpdateFull(BKNPLXinHuyCollection bkDetailCollection, SqlTransaction transaction)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                if (this._ID == 0)
                    this._ID = this.InsertTransaction(transaction);
                else
                    this.UpdateTransaction(transaction);

                foreach (BKNPLXinHuy bkDetail in bkDetailCollection)
                {
                    if (bkDetail.ID == 0)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    else
                    {
                        bkDetail.UpdateTransaction(transaction);
                    }
                }
                
                ret = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
                
            return ret;
        }
        public bool InsertUpdateFull(BKNPLXuatGiaCongCollection bkDetailCollection, SqlTransaction transaction)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                if (this._ID == 0)
                    this._ID = this.InsertTransaction(transaction);
                else
                    this.UpdateTransaction(transaction);

                foreach (BKNPLXuatGiaCong bkDetail in bkDetailCollection)
                {
                    if (bkDetail.ID == 0)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    else
                    {
                        bkDetail.UpdateTransaction(transaction);
                    }
                }
                ret = true;
            }
            catch (Exception ex)
            {
                
                throw new Exception(ex.Message);
            }
            return ret;
        }
        public bool InsertUpdateFull(BKNPLXuatSuDungNKDCollection bkDetailCollection, SqlTransaction transaction)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                if (this._ID == 0)
                    this._ID = this.InsertTransaction(transaction);
                else
                    this.UpdateTransaction(transaction);

                foreach (BKNPLXuatSuDungNKD bkDetail in bkDetailCollection)
                {
                    if (bkDetail.ID == 0)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    else
                    {
                        bkDetail.UpdateTransaction(transaction);
                    }
                }
                ret = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ret;
        }
        public bool InsertUpdateFull(BKToKhaiNhapCollection bkDetailCollection, SqlTransaction transaction)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                if (this._ID == 0)
                    this._ID = this.InsertTransaction(transaction);
                else
                    this.UpdateTransaction(transaction);

                foreach (BKToKhaiNhap bkDetail in bkDetailCollection)
                {
                    if (bkDetail.ID == 0)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    else
                    {
                        bkDetail.UpdateTransaction(transaction);
                    }
                }
                ret = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ret;
        }
        public bool InsertUpdateFull(BKToKhaiXuatCollection bkDetailCollection, SqlTransaction transaction)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            try
            {
                if (this._ID == 0)
                    this._ID = this.InsertTransaction(transaction);
                else
                    this.UpdateTransaction(transaction);

                foreach (BKToKhaiXuat bkDetail in bkDetailCollection)
                {
                    if (bkDetail.ID == 0)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    else
                    {
                        bkDetail.UpdateTransaction(transaction);
                    }
                }
                ret = true;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return ret;
        }
        public bool InsertUpdateFull(SqlTransaction transaction)
        {
            switch (this.MaBangKe.Trim())
            {
                case "DTLNPLCHUATL":
                    return this.InsertUpdateFull(this.bkNPLCTLCollection, transaction);
                case "DTLCHITIETNT":
                    return this.InsertUpdateFull(this.bkNPLTNTCollection, transaction);
                case "DTLNPLNKD":
                    return this.InsertUpdateFull(this.bkNPLNKDCollection, transaction);
                case "DTLNPLNT":
                    return this.InsertUpdateFull(this.bkNPLNTCollection, transaction);
                case "DTLNPLTX":
                    return this.InsertUpdateFull(this.bkNPLTXCollection, transaction);
                case "DTLNPLXGC":
                    return this.InsertUpdateFull(this.bkNPLXGCCollection, transaction);
                case "DTLNPLXH":
                    return this.InsertUpdateFull(this.bkNPLXHCollection, transaction);
                case "DTLTKN":
                    return this.InsertUpdateFull(this.bkTKNCollection, transaction);
                case "DTLTKX":
                    return this.InsertUpdateFull(this.bkTKXColletion, transaction);
                default:
                    return this.InsertUpdateFull(this.bkNPLTCUCollection, transaction);

            }

        }
        
        public bool InsertUpdate_BKNPLCHUATL(BKNPLChuaThanhLyCollection bkDetailCollection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.UpdateTransaction(transaction);
                        //new BKNPLChuaThanhLy().DeleteTransactionBy_BangKeHoSoThanhLy_ID(transaction ,this.ID);
                    }
                    int i = 1;
                    foreach (BKNPLChuaThanhLy bkDetail in bkDetailCollection)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.STTHang = i;
                        bkDetail.InsertUpdateTransaction(transaction);
                        if(bkDetail.DVT_QuyDoi != null)
                        {
                            bkDetail.DVT_QuyDoi.Master_ID = bkDetail.ID;
                            bkDetail.DVT_QuyDoi.Type = "BK_NPLCTL";
                            bkDetail.DVT_QuyDoi.InsertUpdate();
                        }
                        i++;
                    }
                    ret = true;
                    transaction.Commit();
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdate_BKNPLNT(BKNPLNopThueTieuThuNoiDiaCollection bkDetailCollection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.UpdateTransaction(transaction);
                        new BKNPLNopThueTieuThuNoiDia().DeleteTransactionBy_BangKeHoSoThanhLy_ID(transaction,this.ID);
                    }
                    foreach (BKNPLNopThueTieuThuNoiDia bkDetail in bkDetailCollection)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    ret = true;
                    transaction.Commit();
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdate_BKNPLTX(BKNPLTaiXuatCollection bkDetailCollection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.UpdateTransaction(transaction);
                        new BKNPLTaiXuat().DeleteTransactionBy_BangKeHoSoThanhLy_ID(transaction, this.ID);
                    }
                    foreach (BKNPLTaiXuat bkDetail in bkDetailCollection)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                        if (bkDetail.DVT_QuyDoi != null)
                        {
                            bkDetail.DVT_QuyDoi.Master_ID = bkDetail.ID;
                            bkDetail.DVT_QuyDoi.Type = "BK_NPLTX";
                            bkDetail.DVT_QuyDoi.InsertUpdate();
                        }
                    }
                    ret = true;
                    transaction.Commit();
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdate_BKNPLTNT(BKNPLTamNopThueCollection bkDetailCollection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.UpdateTransaction(transaction);
                        new BKNPLTamNopThue().DeleteTransactionBy_BangKeHoSoThanhLy_ID(transaction, this.ID);
                    }
                    foreach (BKNPLTamNopThue bkDetail in bkDetailCollection)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                        if (bkDetail.DVT_QuyDoi != null)
                        {
                            bkDetail.DVT_QuyDoi.Master_ID = bkDetail.ID;
                            bkDetail.DVT_QuyDoi.Type = "BK_NPLXNT";
                            bkDetail.DVT_QuyDoi.InsertUpdate();
                        }
                    }
                    ret = true;
                    transaction.Commit();
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdate_BKNPLTCU(BKNPLTuCungUngCollection bkDetailCollection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.UpdateTransaction(transaction);
                        new BKNPLTuCungUng().DeleteTransactionBy_BangKeHoSoThanhLy_ID(transaction, this.ID);
                    }
                    foreach (BKNPLTuCungUng bkDetail in bkDetailCollection)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    ret = true;
                    transaction.Commit();
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdate_BKNPLTCU_NEW(List<KDT_SXXK_BKNPLTuCungUng_Detail> bkDetailCollection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.UpdateTransaction(transaction);
                        new BKNPLTuCungUng().DeleteTransactionBy_BangKeHoSoThanhLy_ID(transaction, this.ID);
                    }
                    foreach (KDT_SXXK_BKNPLTuCungUng_Detail bkDetail in bkDetailCollection)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertUpdate(transaction);
                    }
                    ret = true;
                    transaction.Commit();
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdate_BKNPLXH(BKNPLXinHuyCollection bkDetailCollection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.UpdateTransaction(transaction);
                        new BKNPLXinHuy().DeleteTransactionBy_BangKeHoSoThanhLy_ID(transaction, this.ID);
                    }
                    foreach (BKNPLXinHuy bkDetail in bkDetailCollection)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    ret = true;
                    transaction.Commit();
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdate_BKNPLXGC(BKNPLXuatGiaCongCollection bkDetailCollection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.UpdateTransaction(transaction);
                        new BKNPLXuatGiaCong().DeleteTransactionBy_BangKeHoSoThanhLy_ID(transaction, this.ID);
                    }
                    foreach (BKNPLXuatGiaCong bkDetail in bkDetailCollection)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    ret = true;
                    transaction.Commit();
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdate_BKNPLNKD(BKNPLXuatSuDungNKDCollection bkDetailCollection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.UpdateTransaction(transaction);
                        new BKNPLXuatSuDungNKD().DeleteTransactionBy_BangKeHoSoThanhLy_ID(transaction, this.ID);
                    }
                    foreach (BKNPLXuatSuDungNKD bkDetail in bkDetailCollection)
                    {
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    ret = true;
                    transaction.Commit();
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdate_BKTKX(BKToKhaiXuatCollection bkDetailCollection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.UpdateTransaction(transaction);
                    }
                    int i = 1;
                    foreach (BKToKhaiXuat bkDetail in bkDetailCollection)
                    {
                        bkDetail.STTHang = i;
                        i++;
                        if (bkDetail.ID == 0)
                        {
                            bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                            
                            bkDetail.ID = bkDetail.InsertTransaction(transaction);
                        }
                        else
                        {
                            bkDetail.UpdateTransaction(transaction);
                        }
                    }
                    ret = true;
                    transaction.Commit();
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
        public bool InsertUpdate_BKTKN(BKToKhaiNhapCollection bkDetailCollection)
        {
            bool ret;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                SqlTransaction transaction = connection.BeginTransaction();
                try
                {
                    if (this._ID == 0)
                        this._ID = this.InsertTransaction(transaction);
                    else
                    {
                        this.UpdateTransaction(transaction);
                        //new BKToKhaiNhap().DeleteTransactionBy_BangKeHoSoThanhLy_ID(transaction, this.ID);
                    }
                    int i = 1;
                    foreach (BKToKhaiNhap bkDetail in bkDetailCollection)
                    {
                        bkDetail.STTHang = i;
                        i++;
                        bkDetail.BangKeHoSoThanhLy_ID = this._ID;
                        if (bkDetail.ID > 0)
                            bkDetail.UpdateTransaction(transaction);
                        else 
                            bkDetail.ID = bkDetail.InsertTransaction(transaction);
                    }
                    ret = true;
                    transaction.Commit();
                }

                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw new Exception(ex.Message);
                }
                finally
                {
                    connection.Close();
                }
            }
            return ret;
        }
	}	
}