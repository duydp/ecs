﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class GC_DinhMucSP:DeclarationBase
    {
        /// <summary>
        /// Mo ta hop dong
        /// </summary>
        [XmlElement("ContractReference")]
        public AdditionalDocument ContractReference { get; set; }
        /// <summary>
        /// Mô tả sản phẩm
        /// </summary>
        [XmlElement("ProductionNorm")]
        public ProductionNorm ProductionNorm {get;set;}

        
    }
}
