﻿using System;
using System.Drawing;
using Company.KD.BLL.DuLieuChuan;
using Company.KD.BLL.KDT;
using Company.Interface.SXXK;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.KD.BLL;
using Company.KD.BLL.Utils;
using System.Data;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;

namespace Company.Interface
{
    public partial class ListDeNghiChuyenCuaKhauTKMDForm : BaseForm
    {
        //-----------------------------------------------------------------------------------------
        public bool isKhaiBoSung = false;
        public ToKhaiMauDich TKMD;
        //-----------------------------------------------------------------------------------------

        public ListDeNghiChuyenCuaKhauTKMDForm()
        {
            InitializeComponent();
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
        }


        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

   

        private void SelectHangTriGiaForm_Load(object sender, EventArgs e)
        {
            dgList.DataSource = TKMD.listChuyenCuaKhau;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
            //if (TKMD.TrangThaiXuLy != Company.KDT.SHARE.Components.TrangThaiXuLy.CHUA_KHAI_BAO)
            //{
            //    btnTaoMoi.Enabled = false;
            //    btnXoa.Enabled = false;
            //}
        }

  
      

        private void btnXoa_Click(object sender, EventArgs e)
        {
            List<DeNghiChuyenCuaKhau> listHopDong = new List<DeNghiChuyenCuaKhau>();
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (dgList.GetRows().Length < 0) return;
            if (items.Count <= 0) return;
            {
                if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                {
                    return;
                }
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        DeNghiChuyenCuaKhau hd = (DeNghiChuyenCuaKhau)i.GetRow().DataRow;
                        listHopDong.Add(hd);
                    }
                }
            }

            foreach (DeNghiChuyenCuaKhau hd in listHopDong)
            {
                hd.Delete();
                TKMD.listChuyenCuaKhau.Remove(hd);
            }

            dgList.DataSource = TKMD.listChuyenCuaKhau;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

      
        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            ChuyenCuaKhauForm f = new ChuyenCuaKhauForm();
            f.TKMD = TKMD;
            f.isKhaiBoSung = isKhaiBoSung;
            f.ShowDialog();
            dgList.DataSource = TKMD.listChuyenCuaKhau;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            ChuyenCuaKhauForm f = new ChuyenCuaKhauForm();
            f.TKMD = TKMD;
            f.deNghiChuyen = (DeNghiChuyenCuaKhau)e.Row.DataRow;
            f.isKhaiBoSung = isKhaiBoSung;
            f.ShowDialog();
            dgList.DataSource = TKMD.listChuyenCuaKhau;
            try { dgList.Refetch(); }
            catch { dgList.Refresh(); }
        }

     


    }
}
