IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectDynamicCartReport]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectDynamicCartReport]
IF OBJECT_ID(N'[dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectDynamicBCQT]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectDynamicBCQT]
IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectDynamic]

GO   
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectDynamic]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectDynamic]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT ID ,
       SoTiepNhan ,
       NgayTiepNhan ,
       MaDoanhNghiep ,
       MaHQ ,
       LoaiDoiTuong ,
       LoaiTTHH ,
       SoDinhDanh ,
	   CASE WHEN SoDinhDanh IS NOT NULL THEN ( SELECT TOP 1 SoToKhai FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE ID =  (SELECT TOP 1 TKMD_ID FROM dbo.t_KDT_VNACC_TK_SoVanDon WHERE SoDinhDanh=CSDD.SoDinhDanh)) ELSE NULL END AS SoToKhai,
	   CASE WHEN SoDinhDanh IS NOT NULL THEN ( SELECT TOP 1 NgayDangKy FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE ID =  (SELECT TOP 1 TKMD_ID FROM dbo.t_KDT_VNACC_TK_SoVanDon WHERE SoDinhDanh=CSDD.SoDinhDanh)) ELSE NULL END AS NgayDangKy,
	   CASE WHEN SoDinhDanh IS NOT NULL THEN ( SELECT TOP 1 MaLoaiHinh FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE ID =  (SELECT TOP 1 TKMD_ID FROM dbo.t_KDT_VNACC_TK_SoVanDon WHERE SoDinhDanh=CSDD.SoDinhDanh)) ELSE NULL END AS MaLoaiHinh,
	   CASE WHEN SoDinhDanh IS NOT NULL THEN ( SELECT TOP 1 MaPhanLoaiKiemTra FROM dbo.t_KDT_VNACC_ToKhaiMauDich WHERE ID =  (SELECT TOP 1 TKMD_ID FROM dbo.t_KDT_VNACC_TK_SoVanDon WHERE SoDinhDanh=CSDD.SoDinhDanh)) ELSE NULL END AS MaPhanLoaiKiemTra,
       NgayCap ,
       GuidStr ,
       TrangThaiXuLy ,
       CodeContent FROM dbo.t_KDT_VNACCS_CapSoDinhDanh CSDD   
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  
GO 
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectDynamic]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  
CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_HANGHOA_SelectDynamicBCQT]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT   
 [ID],
 CASE WHEN [PHIEUXNKHO_ID] <>0 THEN (SELECT TOP 1 SOCT FROM dbo.T_KHOKETOAN_PHIEUXNKHO WHERE ID = PHIEUXNKHO_ID) ELSE NULL END AS SOCT,
 CASE WHEN [PHIEUXNKHO_ID] <>0 THEN (SELECT TOP 1 NGAYCT FROM dbo.T_KHOKETOAN_PHIEUXNKHO WHERE ID = PHIEUXNKHO_ID) ELSE NULL END AS NGAYCT,
 CASE WHEN [PHIEUXNKHO_ID] <>0 THEN (SELECT TOP 1 LOAICHUNGTU FROM dbo.T_KHOKETOAN_PHIEUXNKHO WHERE ID = PHIEUXNKHO_ID) ELSE NULL END AS LOAICHUNGTU,
 [STT],  
 [LOAIHANG],  
 [MAHANG],  
 [TENHANG],  
 [DVT],  
 [MAHANGMAP],  
 [TENHANGMAP],  
 [DVTMAP],  
 [SOLUONG],  
 [DONGIA],  
 [DONGIANT],  
 [TRIGIANT],  
 [THANHTIEN],  
 [MAKHO],  
 [TAIKHOANNO],  
 [TAIKHOANCO],  
 [TYLEQD],  
 [SOLUONGQD],  
 [THUEXNK],  
 [THUETTDB],  
 [THUEKHAC],  
 [TRIGIANTSAUPB],  
 [GHICHU]  
FROM [dbo].[T_KHOKETOAN_PHIEUXNKHO_HANGHOA]    
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO
-----------------------------------------------  
-- Stored procedure name: [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectDynamicCartReport]  
-- Database: ECS_TQDT_GC_V4_TAT  
-----------------------------------------------  
CREATE PROCEDURE [dbo].[p_T_KHOKETOAN_PHIEUXNKHO_SelectDynamicCartReport]  
 @WhereCondition NVARCHAR(500),  
 @OrderByExpression NVARCHAR(250) = NULL  
AS  
  
SET NOCOUNT ON  
SET TRANSACTION ISOLATION LEVEL READ COMMITTED  
  
DECLARE @SQL NVARCHAR(MAX)  
  
SET @SQL =   
'SELECT 
 ROW_NUMBER() OVER (ORDER BY NGAYCT) AS STT,
 NGAYCT,
 CASE WHEN PXNK.LOAICHUNGTU=''N'' THEN SOCT ELSE NULL END AS SOCTNHAP,
 CASE WHEN PXNK.LOAICHUNGTU=''X'' THEN SOCT ELSE NULL END AS SOCTXUAT,
 PXNK.GHICHU,
 CASE WHEN PXNK.LOAICHUNGTU=''N'' THEN HH.SOLUONGQD ELSE 0 END AS LUONGNHAP,
 CASE WHEN PXNK.LOAICHUNGTU=''X'' THEN HH.SOLUONGQD ELSE 0 END AS LUONGXUAT
 FROM dbo.T_KHOKETOAN_PHIEUXNKHO PXNK INNER JOIN dbo.T_KHOKETOAN_PHIEUXNKHO_HANGHOA HH ON HH.PHIEUXNKHO_ID = PXNK.ID   
WHERE ' + @WhereCondition  
  
IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0  
BEGIN  
 SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression  
END  
  
EXEC sp_executesql @SQL  

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '39.7') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('39.7',GETDATE(), N'CẬP NHẬT PHIẾU XUẤT NHẬP KHO VÀ THẺ KHO ')
END