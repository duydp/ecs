﻿using System;
using System.Xml;
using System.Windows.Forms;
#if KD_V3 || KD_V4
using Company.KD.BLL.SXXK;
using Company.KD.BLL.SXXK.ToKhai;
#elif SXXK_V3 || SXXK_V4
#elif GV_V3
using Company.GC.BLL.SXXK;
using Company.GC.BLL.SXXK.ToKhai;
#elif GC_V2
//using Resources = SOFTECH.ECS.TQDT.GC.Properties.Resources;
#endif
using System.Threading;
using System.Data;
using System.Configuration;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Drawing;


namespace Company.Interface.ProdInfo
{
    public partial class frmRegisteOnline : BaseForm
    {

#if KD_V3 || KD_V4
        string PhanHe = "KINH DOANH VÀ ĐẦU TƯ V4";
        string AsemblyName = "SOFTECH.ECS.TQDT.KD";
        string imageName = "KD";
#elif KD_V4
        string PhanHe = "KINH DOANH VÀ ĐẦU TƯ V5";
        string AsemblyName = "SOFTECH.ECS.TQDT.KD";
        string imageName = "KD"
#elif SXXK_V3
        string PhanHe = "SẢN XUẤT - XUẤT KHẨU V3";
        string AsemblyName = "SOFTECH.ECS.TQDT.SXXK";
        string imageName = "SXXK";
#elif SXXK_V4
        string PhanHe = "SẢN XUẤT - XUẤT KHẨU V5";
        string AsemblyName = "SOFTECH.ECS.TQDT.SXXK";
        string imageName = "SXXK";
#elif GC_V4
        string PhanHe = "GIA CÔNG V5";
        string AsemblyName = "SOFTECH.ECS.TQDT.GC";
        string imageName = "GC";
#elif GC_V3
        string PhanHe = "GIA CÔNG V3";
        string AsemblyName = "SOFTECH.ECS.TQDT.GC";
        string imageName = "GC";
#elif KD_V2
        string PhanHe = "KINH DOANH VÀ ĐẦU TƯ V2";
        string AsemblyName = "SOFTECH.ECS.TQDT.KD";
        string imageName = "KD";
#elif GC_V2
        string PhanHe = "GIA CÔNG V2";
        string AsemblyName = "SOFTECH.ECS.TQDT.GC";
        string imageName = "GC";
#elif SXXK_V2
        string PhanHe = "SẢN XUẤT - XUẤT KHẨU V2";
        string AsemblyName = "SOFTECH.ECS.TQDT.SXXK";
        string imageName = "SXXK";
#endif
        public frmRegisteOnline()
        {
            InitializeComponent();
        }
        public bool sendmail()
        {
            bool isSend;
            try
            {
                string tenDN = txtTenDoanhNghiep.Text.Trim();
                string maDn = txtMaDoanhNghiep.Text.Trim();
                string haiQuan = ctrDonViHQ.Ma + "-" + ctrDonViHQ.Ten;
                string dienthoai = txtDienThoai.Text.Trim();
                string nguoiLH = txtNguoiLH.Text.Trim();
                string diachi = txtDiaChiDoanhNghiep.Text.Trim();
                string email = txtEmail.Text.Trim();
                string subj = "Đăng ký dùng thử chương trình TQDT " + PhanHe;
                isSend = sendEmail(subj, tenDN, maDn, haiQuan,
                    dienthoai, nguoiLH, diachi, email);
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
            return isSend;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                containerValidator1.Validate();

                if (containerValidator1.IsValid)
                {
                    if (!CheckEmail(txtEmail.Text))
                    {
                        ShowMessage("Email không hợp lệ", false);
                        return;
                    }
                    errorProvider1.Clear();
                    string notice;
                    if (!sendmail())
                    {

                        notice = "Không thể gửi thông tin đăng ký, xin kiểm tra lại kết nối mạng (hoặc có thể bị chặn bởi firewall hoặc AntiVirust)";
                        notice += "\r\n\r\n-----------------------------------------------------------------";
                        notice += "\r\nNếu không thể gửi thông tin lại, bạn có thể gửi mail liên lạc đến địa chỉ: ecs@softech.vn";
                        notice += "\r\nHoặc qua số điện thoại : (0236) 3.840.888";
                        this.ShowMessageTQDT(notice, false);
                    }
                    else
                    {
                        notice = " Bạn đã gửi thông tin đăng ký đến chúng tôi.\r\n Tối đa trong vòng 24h tới chúng tôi sẽ gửi CD key kích hoạt đến Email đăng ký của bạn. Xin vui lòng kiểm tra hộp thư.";
                        notice += "\r\n Cảm ơn bạn đã quan tâm sử dụng chương trình.";
                        notice += "\r\n\r\n-----------------------------------------------------------------";
                        notice += "\r\n Mọi chi tiết xin liên hệ: CÔNG TY CP SOFTECH - 38 YÊN BÁI - TP.ĐÀ NẴNG.\r\n Số điện thoại: (0236) 3.840.888";
                        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ismail", "1");
                        this.ShowMessageTQDT(notice, false);
                        this.Close();
                    }
                }
            }
            catch (System.Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Application.Exit();
            }
        }

        private void frmRegisteOnline_Load(object sender, EventArgs e)
        {
            #region Load image from  this form's resource
           
            // Gets a reference to the same assembly that 
            // contains the type that is creating the ResourceManager.
            System.Reflection.Assembly myAssembly;
            myAssembly = this.GetType().Assembly;

            // Gets a reference to a different assembly.
            System.Reflection.Assembly myOtherAssembly;
            myOtherAssembly = System.Reflection.Assembly.Load(AsemblyName);

            // Creates the ResourceManager.
            System.Resources.ResourceManager myManager = new
               System.Resources.ResourceManager("Company.Interface.ProdInfo.frmRegisteOnline",
               myAssembly);

            // Retrieves String and Image resources.
            System.Drawing.Image myImage;
            myImage = (System.Drawing.Image)myManager.GetObject(imageName);
            #endregion
            pictureBox1.Image = myImage;
            this.Text = "Đăng ký thông tin với nhà cung cấp";
        }
        public bool sendEmail(string subject, string tenDN, string maDN, string maHQ, string soDT, string nguoiLH, string diachi, string email)
        {
            try
            {
                string toEmail = Company.KDT.SHARE.Components.Globals.ReadNodeXmlAppSettings("ToMail");
                if (!CheckEmail(toEmail))
                    toEmail = "ecs@softech.vn";
                string fromEmail = "hotrotqdt@gmail.com";
                string content = "<font color=#1F497D><b>ĐĂNG KÝ SỬ DỤNG THỬ PHẦN MỀM "+PhanHe+"</b><br><b>Tên doanh nghiệp:</b> " + tenDN + "<br>";
                content += string.IsNullOrEmpty(maDN) ? string.Empty : "<b>Mã doanh nghiệp: </b>" + maDN + "<br>";
                content += string.IsNullOrEmpty(diachi) ? string.Empty : "<b>Địa chỉ doanh nghiệp: </b>" + diachi + "<br>";
                content += string.IsNullOrEmpty(email) ? string.Empty : "<b>Email nhận key: </b>" + email + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Mã Hải quan: </b>" + maHQ + "<br>";
                content += string.IsNullOrEmpty(soDT) ? string.Empty : "<b>Số điện thoại: </b>" + soDT + "<br>";
                content += string.IsNullOrEmpty(maHQ) ? string.Empty : "<b>Người liên hệ: </b>" + nguoiLH + "<br>";
                MailMessage messageObj = new MailMessage();
                messageObj.Subject = subject;
                messageObj.Body = content;
                messageObj.IsBodyHtml = true;
                messageObj.Priority = MailPriority.Normal;
                
                #region Send Mail

                SmtpClient client = new SmtpClient("smtp.gmail.com", 25);
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(fromEmail, "hotrotqdt");
                //client.SendCompleted += new SendCompletedEventHandler(client_SendCompleted);
                messageObj.From = new MailAddress(fromEmail);
                messageObj.To.Add(toEmail);
                #endregion Send Mail
                client.Timeout = 15000;
                this.Cursor = Cursors.WaitCursor;
                client.Send(messageObj);
                this.Cursor = Cursors.Default;
                return true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return false;
            }
        }

        //static void client_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        //{
        //    string notice;
        //    String token = (string)e.UserState;
        //    if (e.Error != null)
        //    {
        //        notice = "Không thể gửi thông tin đăng ký, xin kiểm tra lại kết nối mạng (hoặc có thể bị chặn bởi firewall hoặc AntiVirust)";
        //        notice += "\r\n\r\n-----------------------------------------------------------------";
        //        notice += "\r\nNếu không thể gửi thông tin lại, bạn có thể gửi mail liên lạc đến địa chỉ: ecs@softech.vn";
        //        notice += "\r\nHoặc qua số điện thoại : (0511) 3840888";
                
        //        ShowMessageTQDT(notice, false);
        //    }
        //    else
        //    {
        //        notice = " Bạn đã gửi thông tin đăng ký đến chúng tôi.\r\n Tối đa trong vòng 24h tới chúng tôi sẽ gửi CD key kích hoạt đến Email đăng ký của bạn. Xin vui lòng kiểm tra hộp thư.";
        //        notice += "\r\n Cảm ơn bạn đã quan tâm sử dụng chương trình.";
        //        notice += "\r\n\r\n-----------------------------------------------------------------";
        //        notice += "\r\n Mọi chi tiết xin liên hệ: CÔNG TY CP SOFTECH - 15 QUANG TRUNG - TP.ĐÀ NẴNG.\r\n Số điện thoại: (0511) 3840888";
        //        Company.KDT.SHARE.Components.Globals.SaveNodeXmlAppSettings("ismail", "1");
        //        ShowMessageTQDT(notice, false);
        //        Application.Exit();

        //        //  LoggerWrapper.Logger.Write("Message Delivered.", LogCategory.Information);
        //    }
        //}
        private static bool CheckEmail(string str)
        {
            Regex re = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            if (re.IsMatch(str))
                return true;
            else
                return false;
        }
        public string ShowMessageTQDT(string messageContent, bool showYesNoButton)
        {
            string messageHQ = "Thông báo ";
            Company.Controls.KDTMessageBoxControl _KDTMsgBox = new Company.Controls.KDTMessageBoxControl();
            _KDTMsgBox.ShowYesNoButton = showYesNoButton;
            _KDTMsgBox.HQMessageString = messageHQ;
            _KDTMsgBox.MessageString = messageContent;
            _KDTMsgBox.ShowDialog();
            string st = _KDTMsgBox.ReturnValue;
            _KDTMsgBox.Dispose();
            return st;
        }
    }
}