namespace Company.Interface.GC
{
    partial class ReadExcelFormNPLPhanBo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadExcelFormNPLPhanBo));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSheet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNamDK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtmaLH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtsoTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSelectFile = new Janus.Windows.EditControls.UIButton();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.rfvFilePath = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvSoTK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSheet = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoLuong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaLH = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvNamDK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaNPL = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoTK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaLH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNamDK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaNPL)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(659, 175);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(190, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cột Số TK";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(286, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cột Mã LH";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(382, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cột Năm ĐK";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(466, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Cột Mã NPL";
            // 
            // txtRow
            // 
            this.txtRow.DecimalDigits = 0;
            this.txtRow.Location = new System.Drawing.Point(92, 46);
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(79, 21);
            this.txtRow.TabIndex = 3;
            this.txtRow.Text = "2";
            this.txtRow.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.txtRow.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên Sheet";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(564, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(68, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Cột số lượng";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(89, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Dòng bắt đầu";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtSheet);
            this.uiGroupBox1.Controls.Add(this.txtMaNPL);
            this.uiGroupBox1.Controls.Add(this.txtSoLuong);
            this.uiGroupBox1.Controls.Add(this.txtNamDK);
            this.uiGroupBox1.Controls.Add(this.txtmaLH);
            this.uiGroupBox1.Controls.Add(this.txtsoTK);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.txtRow);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(659, 78);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông số cấu hình";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtSheet
            // 
            this.txtSheet.Location = new System.Drawing.Point(13, 46);
            this.txtSheet.Name = "txtSheet";
            this.txtSheet.Size = new System.Drawing.Size(61, 21);
            this.txtSheet.TabIndex = 1;
            this.txtSheet.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNPL
            // 
            this.txtMaNPL.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaNPL.Location = new System.Drawing.Point(469, 46);
            this.txtMaNPL.Name = "txtMaNPL";
            this.txtMaNPL.Size = new System.Drawing.Size(79, 21);
            this.txtMaNPL.TabIndex = 11;
            this.txtMaNPL.Text = "D";
            this.txtMaNPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaNPL.TextChanged += new System.EventHandler(this.editBox8_TextChanged);
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuong.Location = new System.Drawing.Point(567, 46);
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Size = new System.Drawing.Size(75, 21);
            this.txtSoLuong.TabIndex = 13;
            this.txtSoLuong.Text = "E";
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNamDK
            // 
            this.txtNamDK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNamDK.Location = new System.Drawing.Point(385, 46);
            this.txtNamDK.Name = "txtNamDK";
            this.txtNamDK.Size = new System.Drawing.Size(63, 21);
            this.txtNamDK.TabIndex = 9;
            this.txtNamDK.Text = "C";
            this.txtNamDK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtmaLH
            // 
            this.txtmaLH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtmaLH.Location = new System.Drawing.Point(289, 46);
            this.txtmaLH.Name = "txtmaLH";
            this.txtmaLH.Size = new System.Drawing.Size(75, 21);
            this.txtmaLH.TabIndex = 7;
            this.txtmaLH.Text = "B";
            this.txtmaLH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtsoTK
            // 
            this.txtsoTK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtsoTK.Location = new System.Drawing.Point(193, 46);
            this.txtsoTK.Name = "txtsoTK";
            this.txtsoTK.Size = new System.Drawing.Size(75, 21);
            this.txtsoTK.TabIndex = 5;
            this.txtsoTK.Text = "A";
            this.txtsoTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.AutoScroll = true;
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnSelectFile);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 78);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(659, 53);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFile.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectFile.Image")));
            this.btnSelectFile.Location = new System.Drawing.Point(567, 18);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(75, 23);
            this.btnSelectFile.TabIndex = 0;
            this.btnSelectFile.Text = "Browe";
            this.btnSelectFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(13, 19);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(535, 21);
            this.txtFilePath.TabIndex = 1;
            this.txtFilePath.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.Location = new System.Drawing.Point(333, 14);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(87, 23);
            this.btnClose.TabIndex = 4;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(240, 14);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(87, 23);
            this.btnSave.TabIndex = 3;
            this.btnSave.Text = "Ghi";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // rfvFilePath
            // 
            this.rfvFilePath.ControlToValidate = this.txtFilePath;
            this.rfvFilePath.ErrorMessage = "\"Tên đường dẫn file\" không được rỗng";
            this.rfvFilePath.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvFilePath.Icon")));
            this.rfvFilePath.Tag = "rfvFilePath";
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            this.error.Icon = ((System.Drawing.Icon)(resources.GetObject("error.Icon")));
            // 
            // rfvSoTK
            // 
            this.rfvSoTK.ControlToValidate = this.txtsoTK;
            this.rfvSoTK.ErrorMessage = "\"Cột số TK\" bắt buộc phải nhập.";
            this.rfvSoTK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoTK.Icon")));
            this.rfvSoTK.Tag = "rfvMa";
            // 
            // rfvTenSheet
            // 
            this.rfvTenSheet.ControlToValidate = this.txtSheet;
            this.rfvTenSheet.ErrorMessage = "\"Tên sheet\" bắt buộc phải nhập.";
            this.rfvTenSheet.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSheet.Icon")));
            this.rfvTenSheet.Tag = "rfvTenSheet";
            // 
            // rfvSoLuong
            // 
            this.rfvSoLuong.ControlToValidate = this.txtSoLuong;
            this.rfvSoLuong.ErrorMessage = "\"Cột số lượng\" bắt buộc phải nhập.";
            this.rfvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoLuong.Icon")));
            this.rfvSoLuong.Tag = "rfvSoLuong";
            // 
            // rfvMaLH
            // 
            this.rfvMaLH.ControlToValidate = this.txtmaLH;
            this.rfvMaLH.ErrorMessage = "\"Cột Mã LH\" bắt buộc phải nhập.";
            this.rfvMaLH.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaLH.Icon")));
            this.rfvMaLH.Tag = "rfvMa";
            // 
            // rfvNamDK
            // 
            this.rfvNamDK.ControlToValidate = this.txtNamDK;
            this.rfvNamDK.ErrorMessage = "\"Cột Năm ĐK\" bắt buộc phải nhập.";
            this.rfvNamDK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNamDK.Icon")));
            this.rfvNamDK.Tag = "rfvMa";
            // 
            // rfvMaNPL
            // 
            this.rfvMaNPL.ControlToValidate = this.txtMaNPL;
            this.rfvMaNPL.ErrorMessage = "\"Cột Mã NPL\" bắt buộc phải nhập.";
            this.rfvMaNPL.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaNPL.Icon")));
            this.rfvMaNPL.Tag = "rfvMa";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.btnSave);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 131);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(659, 44);
            this.uiGroupBox3.TabIndex = 5;
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // ReadExcelFormNPLPhanBo
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(659, 175);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReadExcelFormNPLPhanBo";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đọc danh sách phân bổ chuyển tiếp";
            this.Load += new System.EventHandler(this.ReadExcelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoTK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaLH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNamDK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaNPL)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnSelectFile;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtNamDK;
        private Janus.Windows.GridEX.EditControls.EditBox txtmaLH;
        private Janus.Windows.GridEX.EditControls.EditBox txtsoTK;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNPL;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuong;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.GridEX.EditControls.EditBox txtSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvFilePath;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoTK;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoLuong;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaLH;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvNamDK;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaNPL;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
    }
}