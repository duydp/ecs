﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.GC.BLL.KDT.GC;
using Company.Interface.KDT.GC;
using Company.Interface.GC;
using Company.KDT.SHARE.Components;
using Company.GC.BLL.GC;

namespace Company.Interface.KDT
{
    public partial class BaoCaoQuyetToan_HangHoaForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public KDT_VNACCS_BaoCaoQuyetToan_TT39 BCQT = new KDT_VNACCS_BaoCaoQuyetToan_TT39();
        public List<HopDong> HDCollection = new List<HopDong>();
        public KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail HopDong = new KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail();
        public List<KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail> HopDongCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail>();
        public KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail HangHoa = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();
        public List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> HangHoaCollection = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail>();        
        public bool isAdd = true;
        public bool isExits = false;
        public string LoaiHangHoa = "";
        public NguyenPhuLieuRegistedForm NPLRegistedForm;
        public SanPhamRegistedForm SPRegistedForm;
        List<GC_NguyenPhuLieu> NPLCollection = new List<GC_NguyenPhuLieu>();
        List<GC_SanPham> SPCollection = new List<GC_SanPham>();
        public String Caption;
        public bool IsChange;
        public BaoCaoQuyetToan_HangHoaForm()
        {
            InitializeComponent();
            ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
        }
        private void BindHopDong()
        {
            try
            {
                dgListHopDong.Refresh();
                dgListHopDong.DataSource = BCQT.HopDongCollection;
                dgListHopDong.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);                
            }
        }
        private void BindDataHangHoa()
        {
            try
            {
                dgListTotal.Refresh();
                dgListTotal.DataSource = HopDong.HangHoaCollection;
                dgListTotal.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (BCQT.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || BCQT.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || BCQT.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void BaoCaoQuyetToan_HangHoaForm_Load(object sender, EventArgs e)
        {
            Caption = this.Text;
            if (this.OpenType==Company.KDT.SHARE.Components.OpenFormType.View)
            {
                btnAdd.Enabled = false;
                btnAddHD.Enabled = false;
                btnDelete.Enabled = false;
                btnDeleteHD.Enabled = false;
                btnImportExcel.Enabled = false;
            }
            else
            {
                btnAdd.Enabled = true;
                btnAddHD.Enabled = true;
                btnDelete.Enabled = true;
                btnDeleteHD.Enabled = true;
                btnImportExcel.Enabled = true;
            }
            BindHopDong();
        }

        private void dgListHopDong_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListHopDong.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        HopDong = (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail)i.GetRow().DataRow;
                        BindDataHangHoa();
                        switch (LoaiHangHoa)
                        {
                            case "NPL":
                                NPLCollection = new List<GC_NguyenPhuLieu>();
                                NPLCollection = GC_NguyenPhuLieu.SelectCollectionDynamic("HopDong_ID = " + HopDong.ID_HopDong + "", "");
                                cbbMaHangHoa.DataSource = NPLCollection;
                                cbbMaHangHoa.DisplayMember = "Ma";
                                cbbMaHangHoa.ValueMember = "Ma";
                                break;
                            case "SP":
                                SPCollection = new List<GC_SanPham>();
                                SPCollection = GC_SanPham.SelectCollectionDynamic("HopDong_ID = " + HopDong.ID_HopDong + "", "");
                                cbbMaHangHoa.DataSource = SPCollection;
                                cbbMaHangHoa.DisplayMember = "Ma";
                                cbbMaHangHoa.ValueMember = "Ma";
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnAddHD_Click(object sender, EventArgs e)
        {
            try
            {                
                HopDongManageForm f = new HopDongManageForm();
                f.isQuyetToan = true;
                f.isTaoHoSo = true;
                f.IsBrowseForm = true;
                f.ShowDialog();
                HDCollection = f.list_HopDong;
                if (HDCollection.Count > 0)
                {
                    foreach (HopDong items in HDCollection)
                    {
                        foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in BCQT.HopDongCollection)
                        {
                            if (items.ID ==item.ID_HopDong)
                            {
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            HopDong = new KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail();
                            HopDong.ID_HopDong = items.ID;
                            HopDong.SoHopDong = items.SoHopDong;
                            HopDong.NgayHopDong = items.NgayKy;
                            HopDong.NgayHetHan = items.NgayGiaHan.Year == 1900 ? items.NgayHetHan : items.NgayGiaHan;
                            HopDong.MaHQ = items.MaHaiQuan;
                            BCQT.HopDongCollection.Add(HopDong);
                            int k = 1;
                            foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in BCQT.HopDongCollection)
                            {
                                item.STT = k;
                                k++;
                            }
                            this.SetChange(true);
                        }
                    }
                    BindHopDong();
                }
                else if (f.HopDongSelected != null)
                {
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in BCQT.HopDongCollection)
                    {
                        if (f.HopDongSelected.ID == item.ID_HopDong)
                        {
                            isExits = true;
                            break;
                        }
                    }
                    if (!isExits)
                    {
                        HopDong = new KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail();
                        HopDong.ID_HopDong = f.HopDongSelected.ID;
                        HopDong.SoHopDong = f.HopDongSelected.SoHopDong;
                        HopDong.NgayHopDong = f.HopDongSelected.NgayKy;
                        HopDong.NgayHetHan = f.HopDongSelected.NgayGiaHan.Year == 1900 ? f.HopDongSelected.NgayHetHan : f.HopDongSelected.NgayGiaHan;
                        HopDong.MaHQ = f.HopDongSelected.MaHaiQuan;
                        BCQT.HopDongCollection.Add(HopDong);
                        int k = 1;
                        foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in BCQT.HopDongCollection)
                        {
                            item.STT = k;
                            k++;
                        }
                        this.SetChange(true);
                    }
                    BindHopDong();
                }
                else
                {
                    ShowMessage("Doanh nghiệp chưa chọn Hợp đồng để đưa vào báo cáo quyết toán .", false);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDeleteHD_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListHopDong.SelectedItems;
                List<KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail> ItemColl = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail>();
                if (dgListHopDong.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Bạn muốn xóa Hợp đồng này khỏi Báo cáo quyết toán không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.DeleteFull();
                        BCQT.HopDongCollection.Remove(item);
                    }
                    int k = 1;
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail item in HopDongCollection)
                    {
                        item.STT = k;
                        k++;
                    }
                    BindHopDong();
                    HopDong = new KDT_VNACCS_BaoCaoQuyetToan_TT39_HopDong_Detail();
                    BindDataHangHoa();
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                isValid &= ValidateControl.ValidateChoose(cbbMaHangHoa, errorProvider, " Mã hàng hóa ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTenHangHoa, errorProvider, " Tên hàng hóa ", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(ctrDVTLuong1, errorProvider, " Đơn vị tính ");

                //isValid &= ValidateControl.ValidateNull(txtTonDauKy, errorProvider, " Tồn đầu kỳ ");
                //isValid &= ValidateControl.ValidateNull(txtNhapTrongKy, errorProvider, " Nhập trong kỳ ", isOnlyWarning);
                //isValid &= ValidateControl.ValidateNull(txtTaiXuat, errorProvider, " Tái xuất ");
                //isValid &= ValidateControl.ValidateNull(txtChuyenMDSD, errorProvider, " Chuyển mục đích sử dụng ");
                //isValid &= ValidateControl.ValidateNull(txtXuatKhac, errorProvider, " Xuất khác ");
                //isValid &= ValidateControl.ValidateNull(txtXuatTrongKy, errorProvider, " Xuất trong kỳ ");
                //isValid &= ValidateControl.ValidateNull(txtTonCuoiKy, errorProvider, " Tồn cuối kỳ ");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            return isValid;
        }
        private void GetHangHoa()
        {
            try
            {
                if (HangHoa==null)
                    HangHoa = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();
                HangHoa.MaHangHoa = cbbMaHangHoa.Value.ToString();
                HangHoa.TenHangHoa = txtTenHangHoa.Text.ToString();
                HangHoa.DVT = ctrDVTLuong1.Code.ToString();
                HangHoa.LuongTonDauKy = Convert.ToDecimal(txtTonDauKy.Value.ToString());
                HangHoa.LuongNhapTrongKy = Convert.ToDecimal(txtNhapTrongKy.Value.ToString());
                HangHoa.LuongTaiXuat = Convert.ToDecimal(txtTaiXuat.Value.ToString());
                HangHoa.LuongChuyenMucDichSD = Convert.ToDecimal(txtChuyenMDSD.Value.ToString());
                HangHoa.LuongXuatKhac = Convert.ToDecimal(txtXuatKhac.Value.ToString());
                HangHoa.LuongXuatKhau = Convert.ToDecimal(txtXuatTrongKy.Value.ToString());
                HangHoa.LuongTonCuoiKy = Convert.ToDecimal(txtTonCuoiKy.Value.ToString());
                HangHoa.GhiChu = txtGhiChu.Text.ToString();
                if (isAdd)
                {
                    HopDong.HangHoaCollection.Add(HangHoa);
                    int k = 1;
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in HopDong.HangHoaCollection)
                    {
                        item.STT = k;
                        k++;
                    }
                }
                HangHoa = new KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail();
                isAdd = true;

                cbbMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                cbbMaHangHoa.Value = String.Empty;
                cbbMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                txtTenHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenHangHoa.Text = String.Empty;
                txtTenHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrDVTLuong1.Code = String.Empty;
                ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtTonDauKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtTonDauKy.Value = String.Empty;
                txtTonDauKy.TextChanged += new EventHandler(txt_TextChanged);

                txtNhapTrongKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtNhapTrongKy.Value = String.Empty;
                txtNhapTrongKy.TextChanged += new EventHandler(txt_TextChanged);

                txtTaiXuat.TextChanged -= new EventHandler(txt_TextChanged);
                txtTaiXuat.Value = String.Empty;
                txtTaiXuat.TextChanged += new EventHandler(txt_TextChanged);

                txtChuyenMDSD.TextChanged -= new EventHandler(txt_TextChanged);
                txtChuyenMDSD.Value = String.Empty;
                txtChuyenMDSD.TextChanged += new EventHandler(txt_TextChanged);

                txtXuatKhac.TextChanged -= new EventHandler(txt_TextChanged);
                txtXuatKhac.Value = String.Empty;
                txtXuatKhac.TextChanged += new EventHandler(txt_TextChanged);

                txtXuatTrongKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtXuatTrongKy.Value = String.Empty;
                txtXuatTrongKy.TextChanged += new EventHandler(txt_TextChanged);

                txtTonCuoiKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtTonCuoiKy.Value = String.Empty;
                txtTonCuoiKy.TextChanged += new EventHandler(txt_TextChanged);

                txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtGhiChu.Text = String.Empty;
                txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetHangHoa()
        {
            try
            {
                cbbMaHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                cbbMaHangHoa.Value = HangHoa.MaHangHoa;
                cbbMaHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                txtTenHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                txtTenHangHoa.Text = HangHoa.TenHangHoa;
                txtTenHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                ctrDVTLuong1.Code = HangHoa.DVT;
                ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                txtTonDauKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtTonDauKy.Value = HangHoa.LuongTonDauKy;
                txtTonDauKy.TextChanged += new EventHandler(txt_TextChanged);

                txtNhapTrongKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtNhapTrongKy.Value = HangHoa.LuongNhapTrongKy;
                txtNhapTrongKy.TextChanged += new EventHandler(txt_TextChanged);

                txtTaiXuat.TextChanged -= new EventHandler(txt_TextChanged);
                txtTaiXuat.Value = HangHoa.LuongTaiXuat;
                txtTaiXuat.TextChanged += new EventHandler(txt_TextChanged);

                txtChuyenMDSD.TextChanged -= new EventHandler(txt_TextChanged);
                txtChuyenMDSD.Value = HangHoa.LuongChuyenMucDichSD;
                txtChuyenMDSD.TextChanged += new EventHandler(txt_TextChanged);

                txtXuatKhac.TextChanged -= new EventHandler(txt_TextChanged);
                txtXuatKhac.Value = HangHoa.LuongXuatKhac;
                txtXuatKhac.TextChanged += new EventHandler(txt_TextChanged);

                txtXuatTrongKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtXuatTrongKy.Value = HangHoa.LuongXuatKhau;
                txtXuatTrongKy.TextChanged += new EventHandler(txt_TextChanged);

                txtTonCuoiKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtTonCuoiKy.Value = HangHoa.LuongTonCuoiKy;
                txtTonCuoiKy.TextChanged += new EventHandler(txt_TextChanged);

                txtGhiChu.TextChanged -= new EventHandler(txt_TextChanged);
                txtGhiChu.Text = HangHoa.GhiChu;
                txtGhiChu.TextChanged += new EventHandler(txt_TextChanged);

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                if ( HopDong == null)
                {
                    ShowMessage("Doanh nghiệp chưa chọn Hợp đồng để đưa vào Báo cáo quyết toán",false);
                    return;
                }
                GetHangHoa();
                BindDataHangHoa();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgListTotal.SelectedItems;
                List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail> ItemColl = new List<KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail>();
                if (dgListTotal.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessage("Doanh nghiệp muốn xóa dòng hàng này không ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in ItemColl)
                    {
                        if (item.ID > 0)
                            item.Delete();
                        HopDong.HangHoaCollection.Remove(item);
                    }
                    int k = 1;
                    foreach (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail item in HopDong.HangHoaCollection)
                    {
                        item.STT = k;
                        k++;
                    }
                    BindDataHangHoa();
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dgListTotal_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgListTotal.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            HangHoa = (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail)i.GetRow().DataRow;
                            SetHangHoa();
                            isAdd = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtTonDauKy_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
                txtTonCuoiKy.TextChanged += new EventHandler(txt_TextChanged);
                //this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtNhapTrongKy_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
                txtTonCuoiKy.TextChanged += new EventHandler(txt_TextChanged);
                //this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtTaiXuat_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
                txtTonCuoiKy.TextChanged += new EventHandler(txt_TextChanged);
                //this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtChuyenMDSD_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
                txtTonCuoiKy.TextChanged += new EventHandler(txt_TextChanged);
                //this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtXuatTrongKy_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
                txtTonCuoiKy.TextChanged += new EventHandler(txt_TextChanged);
                //this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtXuatKhac_TextChanged(object sender, EventArgs e)
        {
            try
            {
                txtTonCuoiKy.TextChanged -= new EventHandler(txt_TextChanged);
                txtTonCuoiKy.Value = Convert.ToDecimal(txtTonDauKy.Value) + Convert.ToDecimal(txtNhapTrongKy.Value) - Convert.ToDecimal(txtTaiXuat.Value) - Convert.ToDecimal(txtChuyenMDSD.Value) - Convert.ToDecimal(txtXuatKhac.Value) - Convert.ToDecimal(txtXuatTrongKy.Value);
                txtTonCuoiKy.TextChanged += new EventHandler(txt_TextChanged);
                //this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMaHangHoa_ButtonClick(object sender, EventArgs e)
        {
            //switch (this.LoaiHangHoa)
            //{
            //    case "NPL":
            //        this.NPLRegistedForm = new NguyenPhuLieuRegistedForm();
            //        this.NPLRegistedForm.isBrower = true;
            //        this.NPLRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        this.NPLRegistedForm.NguyenPhuLieuSelected.HopDong_ID = HopDong.ID_HopDong;
            //        this.NPLRegistedForm.ShowDialog();
            //        if (!string.IsNullOrEmpty(this.NPLRegistedForm.NguyenPhuLieuSelected.Ma))
            //        {
            //            txtMaHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ma;
            //            txtTenHangHoa.Text = this.NPLRegistedForm.NguyenPhuLieuSelected.Ten;
            //            ctrDVTLuong1.Code = this.DVT_VNACC(this.NPLRegistedForm.NguyenPhuLieuSelected.DVT_ID.PadRight(3));
            //        }
            //        break;
            //    case "SP":
            //        this.SPRegistedForm = new SanPhamRegistedForm();
            //        this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
            //        this.SPRegistedForm.SanPhamSelected.HopDong_ID = HopDong.ID_HopDong;
            //        this.SPRegistedForm.ShowDialog();
            //        if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
            //        {
            //            txtMaHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
            //            txtTenHangHoa.Text = this.SPRegistedForm.SanPhamSelected.Ten;
            //            ctrDVTLuong1.Code = this.DVT_VNACC(this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3));
            //        }
            //        break;
            //}
        }

        private void txtMaHangHoa_Leave(object sender, EventArgs e)
        {
            //bool isNotFound = false;
            //if (this.LoaiHangHoa == "NPL")
            //{

            //    NguyenPhuLieu npl = new NguyenPhuLieu();
            //    npl.HopDong_ID = HopDong.ID_HopDong;
            //    npl.Ma = txtMaHangHoa.Text.Trim();
            //    npl = npl.Load(npl.Ma, npl.HopDong_ID);
            //    if (npl.DVT_ID != null)
            //    {
            //        txtMaHangHoa.Text = npl.Ma;
            //        txtTenHangHoa.Text = npl.Ten;
            //        ctrDVTLuong1.Code = this.DVT_VNACC(npl.DVT_ID.PadRight(3));
            //    }
            //    else
            //    {
            //        isNotFound = true;
            //    }
            //}
            //else if (this.LoaiHangHoa == "SP")
            //{
            //    SanPham sp = new SanPham();
            //    sp.HopDong_ID = HopDong.ID_HopDong;
            //    sp.Ma = txtMaHangHoa.Text.Trim();
            //    if (sp.Load())
            //    {
            //        txtMaHangHoa.Text = sp.Ma;
            //        txtTenHangHoa.Text = sp.Ten;
            //        ctrDVTLuong1.Code = this.DVT_VNACC(sp.DVT_ID.PadRight(3));
            //    }
            //    else
            //    {
            //        isNotFound = true;
            //    }
            //}
            //if (isNotFound)
            //{
            //    txtTenHangHoa.Text  = ctrDVTLuong1.Code = string.Empty;
            //    return;
            //}
        }

        private void dgListTotal_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType==RowType.Record)
            {
                HangHoa = (KDT_VNACCS_BaoCaoQuyetToan_TT39_Detail)e.Row.DataRow;
                SetHangHoa();
                isAdd = false; 
            }
        }

        private void dgListTotal_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void btnImportExcel_Click(object sender, EventArgs e)
        {
            if ( HopDong== null)
            {
                ShowMessage("Doanh nghiệp chưa chọn Hợp đồng để đưa vào Báo cáo quyết toán .",false);
                return;
            }
            ReadExcelFormNPL f = new ReadExcelFormNPL();
            f.HopDong = HopDong;
            f.BCQT = BCQT;
            f.LoaiHangHoa = LoaiHangHoa;
            f.ShowDialog(this);
            this.SetChange(f.ImportSuccess);
            BindHopDong();
            BindDataHangHoa();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgListHopDong.RootTable.Columns["SoHopDong"], ConditionOperator.Contains, txtSoHopDong.Text);
                dgListHopDong.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgListTotal.RootTable.Columns["MaHangHoa"], ConditionOperator.Contains, txtMaHang.Text);
                dgListTotal.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void cbbMaHangHoa_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (cbbMaHangHoa.Value != null)
                {
                    switch (LoaiHangHoa)
                    {
                        case "NPL":
                            foreach (GC_NguyenPhuLieu item in NPLCollection)
                            {
                                if (item.Ma == cbbMaHangHoa.Value.ToString())
                                {
                                    txtTenHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                                    txtTenHangHoa.Text = item.Ten;
                                    txtTenHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                                    ctrDVTLuong1.Code = this.DVT_VNACC(item.DVT_ID.PadRight(3));
                                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                                    break;
                                }
                            }
                            break;
                        case "SP":
                            foreach (GC_SanPham item in SPCollection)
                            {
                                if (item.Ma == cbbMaHangHoa.Value.ToString())
                                {
                                    txtTenHangHoa.TextChanged -= new EventHandler(txt_TextChanged);
                                    txtTenHangHoa.Text = item.Ten;
                                    txtTenHangHoa.TextChanged += new EventHandler(txt_TextChanged);

                                    ctrDVTLuong1.EditValueChanged -= new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);
                                    ctrDVTLuong1.Code = this.DVT_VNACC(item.DVT_ID.PadRight(3));
                                    ctrDVTLuong1.EditValueChanged += new Company.KDT.SHARE.VNACCS.Controls.ucCategory.EditValueChangedHandle(txt_TextChanged);

                                    break;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}
