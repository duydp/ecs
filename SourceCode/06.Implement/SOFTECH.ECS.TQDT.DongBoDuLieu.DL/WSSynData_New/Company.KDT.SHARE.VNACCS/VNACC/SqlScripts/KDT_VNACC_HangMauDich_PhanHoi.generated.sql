-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Insert]
	@Master_ID bigint,
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(12, 0),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(18, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(17, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(16, 0),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(12, 0),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(12, 0),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(16, 4),
	@TienBaoHiem_KhoanTien numeric(16, 4),
	@DieuKhoanMienGiam varchar(60),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]
(
	[Master_ID],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
)
VALUES 
(
	@Master_ID,
	@SoDong,
	@MaPhanLoaiTaiXacNhanGia,
	@TenNoiXuatXu,
	@SoLuongTinhThue,
	@MaDVTDanhThue,
	@DonGiaTinhThue,
	@DV_SL_TrongDonGiaTinhThue,
	@MaTTDonGiaTinhThue,
	@TriGiaTinhThueS,
	@MaTTTriGiaTinhThueS,
	@MaTTSoTienMienGiam,
	@MaPhanLoaiThueSuatThue,
	@ThueSuatThue,
	@PhanLoaiThueSuatThue,
	@SoTienThue,
	@MaTTSoTienThueXuatKhau,
	@TienLePhi_DonGia,
	@TienBaoHiem_DonGia,
	@TienLePhi_SoLuong,
	@TienLePhi_MaDVSoLuong,
	@TienBaoHiem_SoLuong,
	@TienBaoHiem_MaDVSoLuong,
	@TienLePhi_KhoanTien,
	@TienBaoHiem_KhoanTien,
	@DieuKhoanMienGiam
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Update]
	@ID bigint,
	@Master_ID bigint,
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(12, 0),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(18, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(17, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(16, 0),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(12, 0),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(12, 0),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(16, 4),
	@TienBaoHiem_KhoanTien numeric(16, 4),
	@DieuKhoanMienGiam varchar(60)
AS

UPDATE
	[dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]
SET
	[Master_ID] = @Master_ID,
	[SoDong] = @SoDong,
	[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
	[TenNoiXuatXu] = @TenNoiXuatXu,
	[SoLuongTinhThue] = @SoLuongTinhThue,
	[MaDVTDanhThue] = @MaDVTDanhThue,
	[DonGiaTinhThue] = @DonGiaTinhThue,
	[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
	[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
	[TriGiaTinhThueS] = @TriGiaTinhThueS,
	[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
	[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
	[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
	[ThueSuatThue] = @ThueSuatThue,
	[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
	[SoTienThue] = @SoTienThue,
	[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
	[TienLePhi_DonGia] = @TienLePhi_DonGia,
	[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
	[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
	[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
	[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
	[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
	[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
	[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
	[DieuKhoanMienGiam] = @DieuKhoanMienGiam
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_InsertUpdate]
	@ID bigint,
	@Master_ID bigint,
	@SoDong varchar(2),
	@MaPhanLoaiTaiXacNhanGia varchar(1),
	@TenNoiXuatXu varchar(7),
	@SoLuongTinhThue numeric(12, 0),
	@MaDVTDanhThue varchar(4),
	@DonGiaTinhThue numeric(18, 4),
	@DV_SL_TrongDonGiaTinhThue varchar(4),
	@MaTTDonGiaTinhThue varchar(3),
	@TriGiaTinhThueS numeric(17, 4),
	@MaTTTriGiaTinhThueS varchar(3),
	@MaTTSoTienMienGiam varchar(3),
	@MaPhanLoaiThueSuatThue varchar(1),
	@ThueSuatThue varchar(30),
	@PhanLoaiThueSuatThue varchar(1),
	@SoTienThue numeric(16, 0),
	@MaTTSoTienThueXuatKhau varchar(3),
	@TienLePhi_DonGia varchar(21),
	@TienBaoHiem_DonGia varchar(21),
	@TienLePhi_SoLuong numeric(12, 0),
	@TienLePhi_MaDVSoLuong varchar(4),
	@TienBaoHiem_SoLuong numeric(12, 0),
	@TienBaoHiem_MaDVSoLuong varchar(4),
	@TienLePhi_KhoanTien numeric(16, 4),
	@TienBaoHiem_KhoanTien numeric(16, 4),
	@DieuKhoanMienGiam varchar(60)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACC_HangMauDich_PhanHoi] 
		SET
			[Master_ID] = @Master_ID,
			[SoDong] = @SoDong,
			[MaPhanLoaiTaiXacNhanGia] = @MaPhanLoaiTaiXacNhanGia,
			[TenNoiXuatXu] = @TenNoiXuatXu,
			[SoLuongTinhThue] = @SoLuongTinhThue,
			[MaDVTDanhThue] = @MaDVTDanhThue,
			[DonGiaTinhThue] = @DonGiaTinhThue,
			[DV_SL_TrongDonGiaTinhThue] = @DV_SL_TrongDonGiaTinhThue,
			[MaTTDonGiaTinhThue] = @MaTTDonGiaTinhThue,
			[TriGiaTinhThueS] = @TriGiaTinhThueS,
			[MaTTTriGiaTinhThueS] = @MaTTTriGiaTinhThueS,
			[MaTTSoTienMienGiam] = @MaTTSoTienMienGiam,
			[MaPhanLoaiThueSuatThue] = @MaPhanLoaiThueSuatThue,
			[ThueSuatThue] = @ThueSuatThue,
			[PhanLoaiThueSuatThue] = @PhanLoaiThueSuatThue,
			[SoTienThue] = @SoTienThue,
			[MaTTSoTienThueXuatKhau] = @MaTTSoTienThueXuatKhau,
			[TienLePhi_DonGia] = @TienLePhi_DonGia,
			[TienBaoHiem_DonGia] = @TienBaoHiem_DonGia,
			[TienLePhi_SoLuong] = @TienLePhi_SoLuong,
			[TienLePhi_MaDVSoLuong] = @TienLePhi_MaDVSoLuong,
			[TienBaoHiem_SoLuong] = @TienBaoHiem_SoLuong,
			[TienBaoHiem_MaDVSoLuong] = @TienBaoHiem_MaDVSoLuong,
			[TienLePhi_KhoanTien] = @TienLePhi_KhoanTien,
			[TienBaoHiem_KhoanTien] = @TienBaoHiem_KhoanTien,
			[DieuKhoanMienGiam] = @DieuKhoanMienGiam
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]
		(
			[Master_ID],
			[SoDong],
			[MaPhanLoaiTaiXacNhanGia],
			[TenNoiXuatXu],
			[SoLuongTinhThue],
			[MaDVTDanhThue],
			[DonGiaTinhThue],
			[DV_SL_TrongDonGiaTinhThue],
			[MaTTDonGiaTinhThue],
			[TriGiaTinhThueS],
			[MaTTTriGiaTinhThueS],
			[MaTTSoTienMienGiam],
			[MaPhanLoaiThueSuatThue],
			[ThueSuatThue],
			[PhanLoaiThueSuatThue],
			[SoTienThue],
			[MaTTSoTienThueXuatKhau],
			[TienLePhi_DonGia],
			[TienBaoHiem_DonGia],
			[TienLePhi_SoLuong],
			[TienLePhi_MaDVSoLuong],
			[TienBaoHiem_SoLuong],
			[TienBaoHiem_MaDVSoLuong],
			[TienLePhi_KhoanTien],
			[TienBaoHiem_KhoanTien],
			[DieuKhoanMienGiam]
		)
		VALUES 
		(
			@Master_ID,
			@SoDong,
			@MaPhanLoaiTaiXacNhanGia,
			@TenNoiXuatXu,
			@SoLuongTinhThue,
			@MaDVTDanhThue,
			@DonGiaTinhThue,
			@DV_SL_TrongDonGiaTinhThue,
			@MaTTDonGiaTinhThue,
			@TriGiaTinhThueS,
			@MaTTTriGiaTinhThueS,
			@MaTTSoTienMienGiam,
			@MaPhanLoaiThueSuatThue,
			@ThueSuatThue,
			@PhanLoaiThueSuatThue,
			@SoTienThue,
			@MaTTSoTienThueXuatKhau,
			@TienLePhi_DonGia,
			@TienBaoHiem_DonGia,
			@TienLePhi_SoLuong,
			@TienLePhi_MaDVSoLuong,
			@TienBaoHiem_SoLuong,
			@TienBaoHiem_MaDVSoLuong,
			@TienLePhi_KhoanTien,
			@TienBaoHiem_KhoanTien,
			@DieuKhoanMienGiam
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[Master_ID],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
FROM [dbo].[t_KDT_VNACC_HangMauDich_PhanHoi] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Saturday, October 05, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACC_HangMauDich_PhanHoi_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[Master_ID],
	[SoDong],
	[MaPhanLoaiTaiXacNhanGia],
	[TenNoiXuatXu],
	[SoLuongTinhThue],
	[MaDVTDanhThue],
	[DonGiaTinhThue],
	[DV_SL_TrongDonGiaTinhThue],
	[MaTTDonGiaTinhThue],
	[TriGiaTinhThueS],
	[MaTTTriGiaTinhThueS],
	[MaTTSoTienMienGiam],
	[MaPhanLoaiThueSuatThue],
	[ThueSuatThue],
	[PhanLoaiThueSuatThue],
	[SoTienThue],
	[MaTTSoTienThueXuatKhau],
	[TienLePhi_DonGia],
	[TienBaoHiem_DonGia],
	[TienLePhi_SoLuong],
	[TienLePhi_MaDVSoLuong],
	[TienBaoHiem_SoLuong],
	[TienBaoHiem_MaDVSoLuong],
	[TienLePhi_KhoanTien],
	[TienBaoHiem_KhoanTien],
	[DieuKhoanMienGiam]
FROM
	[dbo].[t_KDT_VNACC_HangMauDich_PhanHoi]	

GO

