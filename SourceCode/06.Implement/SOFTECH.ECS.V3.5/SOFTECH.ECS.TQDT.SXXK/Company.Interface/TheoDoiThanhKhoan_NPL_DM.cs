﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.KDT.SXXK;

namespace Company.Interface
{
    public partial class TheoDoiThanhKhoan_NPL_DM : BaseForm
    {
        public int id = 0;
        public TheoDoiThanhKhoan_NPL_DM()
        {
            InitializeComponent();
        }
        public TheoDoiThanhKhoan_NPL_DM(int Id) 
        {
            InitializeComponent();
            id = Id;
        }
        
        private void TheoDoiThanhKhoan_NPL_DM_Load(object sender, EventArgs e)
        {
            //tb = DinhMucThanhKhoan.SelectDynamic(string.Format("ID = {0}",id)," LanThanhLy desc").Tables[0];
            dgList.DataSource = DinhMucThanhKhoan.SelectDynamic(string.Format("ID = {0}", id), " LanThanhLy desc").Tables[0];
        }
    }
}