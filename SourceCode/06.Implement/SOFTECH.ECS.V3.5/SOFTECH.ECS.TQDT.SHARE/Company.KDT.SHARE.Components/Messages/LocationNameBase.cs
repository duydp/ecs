﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class LocationNameBase
    {
       /// <summary>
       /// Tên khu vực, quốc gia
       /// </summary>
        [XmlElement("name")]
        public string Name { get; set; }

        /// <summary>
        /// Mã địa điểm dỡ hàng (Khu vực chịu sự giám sát Hải quan): 
        /// </summary>
        [XmlElement("customsCode")]
        public string CustomsCode { get; set; }

        /// <summary>
        /// Tên địa điểm xếp hàng (Khu vực chịu sự giám sát Hải quan) 
        /// </summary>
        [XmlElement("customsName")]
        public string CustomsName { get; set; }

        /// <summary>
        /// Mã vị trí dỡ hàng (Nơi dỡ hàng): 
        /// </summary>
        [XmlElement("code")]
        public string Code { get; set; }

        /// <summary>
        /// Mã cảng/cửa khẩu/ga dỡ hàng (Mã cảng dỡ hàng)
        /// </summary>
        [XmlElement("portCode")]
        public string PortCode { get; set; }

    }
}
