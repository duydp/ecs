namespace Company.Interface
{
    partial class ImportFromExcelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportFromExcelForm));
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.chkTTDM = new Janus.Windows.EditControls.UICheckBox();
            this.chkDMMoiKhai = new Janus.Windows.EditControls.UICheckBox();
            this.chkSanPham = new Janus.Windows.EditControls.UICheckBox();
            this.chkOverwrite = new Janus.Windows.EditControls.UICheckBox();
            this.chkNPL = new Janus.Windows.EditControls.UICheckBox();
            this.chkHMD = new Janus.Windows.EditControls.UICheckBox();
            this.chkToKhaiMauDich = new Janus.Windows.EditControls.UICheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDN = new Janus.Windows.GridEX.EditControls.EditBox();
            this.donViHaiQuanControl1 = new Company.Interface.Controls.DonViHaiQuanControl();
            this.ccToDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.ccFromDate = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.btnImport = new Janus.Windows.EditControls.UIButton();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnReadFile = new Janus.Windows.EditControls.UIButton();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.ccToDate);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.label4);
            this.grbMain.Controls.Add(this.label3);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.ccFromDate);
            this.grbMain.Controls.Add(this.btnImport);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(562, 332);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(49, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Hải quan";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.chkTTDM);
            this.uiGroupBox1.Controls.Add(this.chkDMMoiKhai);
            this.uiGroupBox1.Controls.Add(this.chkSanPham);
            this.uiGroupBox1.Controls.Add(this.chkOverwrite);
            this.uiGroupBox1.Controls.Add(this.chkNPL);
            this.uiGroupBox1.Controls.Add(this.chkHMD);
            this.uiGroupBox1.Controls.Add(this.chkToKhaiMauDich);
            this.uiGroupBox1.Location = new System.Drawing.Point(13, 187);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(536, 96);
            this.uiGroupBox1.TabIndex = 2;
            this.uiGroupBox1.Text = "Chọn dữ liệu nhập";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // chkTTDM
            // 
            this.chkTTDM.Location = new System.Drawing.Point(31, 59);
            this.chkTTDM.Name = "chkTTDM";
            this.chkTTDM.Size = new System.Drawing.Size(137, 23);
            this.chkTTDM.TabIndex = 11;
            this.chkTTDM.Text = "Thông tin định mức";
            this.chkTTDM.VisualStyleManager = this.vsmMain;
            // 
            // chkDMMoiKhai
            // 
            this.chkDMMoiKhai.Location = new System.Drawing.Point(314, 30);
            this.chkDMMoiKhai.Name = "chkDMMoiKhai";
            this.chkDMMoiKhai.Size = new System.Drawing.Size(88, 23);
            this.chkDMMoiKhai.TabIndex = 9;
            this.chkDMMoiKhai.Text = "Định mức ";
            this.chkDMMoiKhai.VisualStyleManager = this.vsmMain;
            // 
            // chkSanPham
            // 
            this.chkSanPham.Checked = true;
            this.chkSanPham.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkSanPham.Location = new System.Drawing.Point(186, 30);
            this.chkSanPham.Name = "chkSanPham";
            this.chkSanPham.Size = new System.Drawing.Size(128, 23);
            this.chkSanPham.TabIndex = 8;
            this.chkSanPham.Text = "Sản phẩm ";
            this.chkSanPham.VisualStyleManager = this.vsmMain;
            // 
            // chkOverwrite
            // 
            this.chkOverwrite.BackColor = System.Drawing.Color.Transparent;
            this.chkOverwrite.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkOverwrite.Location = new System.Drawing.Point(442, 59);
            this.chkOverwrite.Name = "chkOverwrite";
            this.chkOverwrite.Size = new System.Drawing.Size(59, 23);
            this.chkOverwrite.TabIndex = 10;
            this.chkOverwrite.Text = "Ghi đè";
            this.chkOverwrite.VisualStyleManager = this.vsmMain;
            // 
            // chkNPL
            // 
            this.chkNPL.Checked = true;
            this.chkNPL.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkNPL.Location = new System.Drawing.Point(31, 30);
            this.chkNPL.Name = "chkNPL";
            this.chkNPL.Size = new System.Drawing.Size(169, 23);
            this.chkNPL.TabIndex = 7;
            this.chkNPL.Text = "Nguyên phụ liệu ";
            this.chkNPL.VisualStyleManager = this.vsmMain;
            // 
            // chkHMD
            // 
            this.chkHMD.Location = new System.Drawing.Point(312, 59);
            this.chkHMD.Name = "chkHMD";
            this.chkHMD.Size = new System.Drawing.Size(124, 23);
            this.chkHMD.TabIndex = 6;
            this.chkHMD.Text = "Hàng mậu dịch";
            this.chkHMD.VisualStyleManager = this.vsmMain;
            // 
            // chkToKhaiMauDich
            // 
            this.chkToKhaiMauDich.Location = new System.Drawing.Point(186, 59);
            this.chkToKhaiMauDich.Name = "chkToKhaiMauDich";
            this.chkToKhaiMauDich.Size = new System.Drawing.Size(141, 23);
            this.chkToKhaiMauDich.TabIndex = 3;
            this.chkToKhaiMauDich.Text = "Tờ khai mậu dịch ";
            this.chkToKhaiMauDich.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(49, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Doanh nghiệp";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.txtTenDN);
            this.uiGroupBox2.Controls.Add(this.txtMaDN);
            this.uiGroupBox2.Controls.Add(this.label1);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.donViHaiQuanControl1);
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(537, 88);
            this.uiGroupBox2.TabIndex = 4;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDN
            // 
            this.txtTenDN.Location = new System.Drawing.Point(236, 48);
            this.txtTenDN.Name = "txtTenDN";
            this.txtTenDN.Size = new System.Drawing.Size(215, 21);
            this.txtTenDN.TabIndex = 5;
            this.txtTenDN.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDN
            // 
            this.txtMaDN.Location = new System.Drawing.Point(130, 48);
            this.txtMaDN.Name = "txtMaDN";
            this.txtMaDN.Size = new System.Drawing.Size(100, 21);
            this.txtMaDN.TabIndex = 4;
            this.txtMaDN.VisualStyleManager = this.vsmMain;
            // 
            // donViHaiQuanControl1
            // 
            this.donViHaiQuanControl1.BackColor = System.Drawing.Color.Transparent;
            this.donViHaiQuanControl1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.donViHaiQuanControl1.Location = new System.Drawing.Point(130, 20);
            this.donViHaiQuanControl1.Ma = "";
            this.donViHaiQuanControl1.MaCuc = "";
            this.donViHaiQuanControl1.Name = "donViHaiQuanControl1";
            this.donViHaiQuanControl1.ReadOnly = false;
            this.donViHaiQuanControl1.Size = new System.Drawing.Size(336, 22);
            this.donViHaiQuanControl1.TabIndex = 0;
            this.donViHaiQuanControl1.VisualStyleManager = this.vsmMain;
            // 
            // ccToDate
            // 
            // 
            // 
            // 
            this.ccToDate.DropDownCalendar.Name = "";
            this.ccToDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccToDate.Location = new System.Drawing.Point(317, 376);
            this.ccToDate.Name = "ccToDate";
            this.ccToDate.Size = new System.Drawing.Size(100, 21);
            this.ccToDate.TabIndex = 9;
            this.ccToDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccToDate.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(240, 381);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "đến ngày";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(17, 381);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(47, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Từ ngày";
            // 
            // ccFromDate
            // 
            // 
            // 
            // 
            this.ccFromDate.DropDownCalendar.Name = "";
            this.ccFromDate.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccFromDate.Enabled = false;
            this.ccFromDate.Location = new System.Drawing.Point(96, 376);
            this.ccFromDate.Name = "ccFromDate";
            this.ccFromDate.Size = new System.Drawing.Size(100, 21);
            this.ccFromDate.TabIndex = 6;
            this.ccFromDate.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2003;
            this.ccFromDate.VisualStyleManager = this.vsmMain;
            // 
            // btnImport
            // 
            this.btnImport.Icon = ((System.Drawing.Icon)(resources.GetObject("btnImport.Icon")));
            this.btnImport.Location = new System.Drawing.Point(172, 307);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(100, 23);
            this.btnImport.TabIndex = 5;
            this.btnImport.Text = "Nhập dữ liệu";
            this.btnImport.VisualStyleManager = this.vsmMain;
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this.grbMain;
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaDN;
            this.rfvMa.ErrorMessage = "\"Cột mã Doanh nghiệp\" bắt buộc phải nhập.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Excel 97 - 2003 files (*.xls)|*.xls";
            // 
            // btnClose
            // 
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(287, 307);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(98, 23);
            this.btnClose.TabIndex = 6;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnReadFile);
            this.uiGroupBox3.Controls.Add(this.txtFilePath);
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(13, 116);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(537, 53);
            this.uiGroupBox3.TabIndex = 214;
            this.uiGroupBox3.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // btnReadFile
            // 
            this.btnReadFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReadFile.Icon = ((System.Drawing.Icon)(resources.GetObject("btnReadFile.Icon")));
            this.btnReadFile.Location = new System.Drawing.Point(453, 19);
            this.btnReadFile.Name = "btnReadFile";
            this.btnReadFile.Size = new System.Drawing.Size(71, 23);
            this.btnReadFile.TabIndex = 216;
            this.btnReadFile.Text = "Đọc file";
            this.btnReadFile.VisualStyleManager = this.vsmMain;
            this.btnReadFile.Click += new System.EventHandler(this.btnReadFile_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtFilePath.Location = new System.Drawing.Point(13, 19);
            this.txtFilePath.Multiline = true;
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.Size = new System.Drawing.Size(434, 23);
            this.txtFilePath.TabIndex = 188;
            this.txtFilePath.VisualStyleManager = this.vsmMain;
            this.txtFilePath.ButtonClick += new System.EventHandler(this.txtFilePath_ButtonClick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // ImportFromExcelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(562, 332);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ImportFromExcelForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Nhập từ file Excel";
            this.Load += new System.EventHandler(this.ExportToExcelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UICheckBox chkHMD;
        private Janus.Windows.EditControls.UICheckBox chkToKhaiMauDich;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDN;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDN;
        private Janus.Windows.EditControls.UIButton btnImport;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private Company.Interface.Controls.DonViHaiQuanControl donViHaiQuanControl1;
        private Janus.Windows.EditControls.UICheckBox chkDMMoiKhai;
        private Janus.Windows.EditControls.UICheckBox chkSanPham;
        private Janus.Windows.EditControls.UICheckBox chkNPL;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.CalendarCombo.CalendarCombo ccToDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.CalendarCombo.CalendarCombo ccFromDate;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIButton btnReadFile;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private Janus.Windows.EditControls.UICheckBox chkOverwrite;
        private Janus.Windows.EditControls.UICheckBox chkTTDM;
    }
}

