﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.Interface.Feedback
{
    public enum EnumPhanLoai
    {
        /// <summary>
        /// Tôi có ý kiến thắc mắc chưa được giải đáp
        /// </summary>
        ThacMac,
        /// <summary>
        /// Tôi muốn đóng góp ý kiến xây dựng chương trình
        /// </summary>
        GopY,
        /// <summary>
        /// Tôi muốn thông báo lỗi của chương trình/ hệ thống
        /// </summary>
        Loi,
    }
}
