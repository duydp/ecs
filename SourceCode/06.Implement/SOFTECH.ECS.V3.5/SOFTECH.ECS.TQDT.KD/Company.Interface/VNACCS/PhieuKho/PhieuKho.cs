﻿using System;
using System.Windows.Forms;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Company.Interface.Report;
using System.IO;
//using Company.BLL;
#if SXXK_V4
using Company.BLL.KDT;
using Company.BLL.KDT.SXXK;
#endif
#if GC_V4
using Company.GC.BLL.KDT;
using Company.GC.BLL.KDT.SXXK;
#endif
using DuLieuChuan = Company.KDT.SHARE.Components.DuLieuChuan;
//using Company.BLL.KDT.SXXK;
using System.Net.Mail;
using Company.Interface.DanhMucChuan;
using Company.KDT.SHARE.QuanLyChungTu;
using System.Collections.Generic;
using System.Xml;
using System.Globalization;
using Infragistics.Excel;
using Company.KDT.SHARE.Components.AnDinhThue;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.VNACC.PhieuKho;
//using Company.BLL.DataTransferObjectMapper;
using Company.KDT.SHARE.Components.Messages.PhieuKho;
using System.Data;
#if KD_V4
using Company.KD.BLL.KDT.SXXK;
#endif
using Company.Interface.VNACCS;

namespace Company.Interface.VNACCS.PhieuKho
{
    public partial class PhieuKho : BaseFormHaveGuidPanel
    {

        private static string sfmtDate = "yyyy-MM-dd";
        private static string sfmtDateTime = "yyyy-MM-dd HH:mm:ss";
        private static string sfmtYear = "yyyy";

        private FeedBackContent feedbackContent = null;
        private string msgInfor = string.Empty;
        KDT_VNACC_ToKhaiMauDich TKMD;
        //List
        List<KDT_VNACC_ToKhaiMauDich> TKMDCollection = new List<KDT_VNACC_ToKhaiMauDich>();
        KDT_KhoCFS_DangKy khoCFS = new KDT_KhoCFS_DangKy();
        List<KDT_KhoCFS_Detail> listTK = new List<KDT_KhoCFS_Detail>();
        
        public long TKMD_ID = 0;
        public long khoCFS_ID = 0;
        public String Caption;
        public bool IsChange;
        public PhieuKho()
        {
            InitializeComponent();
            
        }

        private DataTable makeTB() 
        {
            try
            {
                DataTable tb = new DataTable("KHO");
                DataColumn[] col1 = new DataColumn[3];
                col1[0] = new DataColumn("TKMD_ID", Type.GetType("System.String"));
                col1[1] = new DataColumn("SoToKhai", Type.GetType("System.String"));
                col1[2] = new DataColumn("MaLoaiHinh", Type.GetType("System.String"));
                tb.Columns.AddRange(col1);
                return tb;
            }
            catch (Exception ex)
            {
                return null;
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }

        private void loadTK(long id) 
        {
            try
            {
                listTK = KDT_KhoCFS_Detail.SelectCollectionAll().FindAll(delegate(KDT_KhoCFS_Detail tk)
                {
                    return tk.Master_ID == id;
                });
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }            
        }
        private void SetCommandStatus()
        {
            if (khoCFS.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendV5.Enabled = cmdSendV51.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                clcNgayTN.Value = khoCFS.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (khoCFS.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || khoCFS.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {

                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSendV5.Enabled = cmdSendV51.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = khoCFS.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (khoCFS.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || khoCFS.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO)
            {
                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendV5.Enabled = cmdSendV51.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = khoCFS.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo hủy", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (khoCFS.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {

                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSendV5.Enabled = cmdSendV51.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAdd.Enabled = cmdAdd1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                clcNgayTN.Value = khoCFS.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Cancel");
                this.OpenType = OpenFormType.View;
            }

        }
        private void PhieuKho_Load(object sender, EventArgs e)
        {
            try
            {
                cbbCoQuanHQ.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                Caption = this.Text;
                if (khoCFS_ID != 0)
                {
                    khoCFS = KDT_KhoCFS_DangKy.LoadFull(khoCFS_ID);
                    ReLoad();
                    if (khoCFS.TrangThaiXuLy == 1)
                    {
                        lblTrangThai.Text = "Đã duyệt";
                        clcNgayTN.Value = khoCFS.NgayTiepNhan;
                    }
                    else if (khoCFS.TrangThaiXuLy == -1)
                    {
                        lblTrangThai.Text = "Không phê duyệt";
                    }
                    else
                        lblTrangThai.Text = "Chưa khai báo";
                }
                else
                {
                    lblTrangThai.Text = "Chưa khai báo";
                    khoCFS.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                }
                SetCommandStatus();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }            
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (khoCFS.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || khoCFS.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET || khoCFS.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        
        private void cmbToolBar_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key) 
            {
                // Khai báo PK.
                case "cmdSendV5":
                    this.SendV5();
                    break;
                case "cmdAdd":
                    this.btnThemToKhai_Click(null,null);
                    break;
                // Khai báo PK.
                case "cmdSave":
                    this.Save(true);
                    break;
                case "cmdFeedback":
                    this.FeedBackV5();
                    break;
                case "cmdResult":
                    this.KetQuaXuLy();
                    break;
                case "cmdCancel":
                    this.CancelV5();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_KhoCFS_DangKy", "", Convert.ToInt32(khoCFS.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void KetQuaXuLy()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                //form.ItemID = this.phieukhoDK.ID;
                form.ItemID = this.khoCFS.ID;
                form.KhoCFS = true;
                form.DeclarationIssuer = DeclarationIssuer.TTHangGuiKho;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }        
        }

        
        private void Save(bool saveList)
        {

            try
            {
                
                khoCFS.MaKhoCFS = cbbMaKhoCFS.Code.ToString();
                khoCFS.MaHQ = cbbCoQuanHQ.Code.ToString();//.Substring(0,4);
                if (khoCFS.DetailCollection.Count==0)
                {
                    ShowMessageTQDT("Doanh nghiệp phải Thêm tờ khai trước khi lưu .", false);
                    return;
                }
                khoCFS.InsertUpdateFull();
                khoCFS_ID = khoCFS.ID;
                ShowMessage("Lưu thành công.", false);
            }
            catch (Exception ex)
            {
                //ShowMessage("Lỗi : " + ex, false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SendV5()
        {
            if (ShowMessage("Doanh nghiệp có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            {

                //if (phieukhoDK.ID == 0)
                if(khoCFS_ID == 0)
                {
                    this.ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    //phieukhoDK = KDT_PhieuKhoDangKy.Load(phieukhoDK.ID);
                    
                    khoCFS = KDT_KhoCFS_DangKy.LoadFull(khoCFS.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.PhieuNhapKho;
                //sendXML.master_id = phieukhoDK.ID;
                sendXML.master_id = khoCFS.ID;
                if (sendXML.Load())
                {
                    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                    cmdSendV5.Enabled = cmdSendV51.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    return;
                }
                try
                {
                    //if (this.phieukhoDK.ID!=0)
                    if (this.khoCFS.ID != 0)
                    {

                        string returnMessage = string.Empty;
                        //phieukhoDK.GuidStr = Guid.NewGuid().ToString();
                        khoCFS.GuidStr = Guid.NewGuid().ToString();
                        PhieuNhapKho pnk = new PhieuNhapKho();
                        //pnk = Mapper_V4.ToDaTaTransferPhieuNhapKho(TKMD,phieukhoDK);
                        pnk = ToDaTaTransferPhieuKhoCFS(khoCFS, GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = new ObjectSend(
                            new MessageHeader()
                            {
                                ProcedureType = "2",
                                Reference = new Reference()
                                {
                                    Version = "3.00",
                                    MessageID = khoCFS.GuidStr,
                                },
                                SendApplication = new SendApplication()
                                {
                                    Name = "Softech_ECS",
                                    Version = "5.00",
                                    CompanyName = "Công ty cổ phần SOFTECH Đà Nẵng",
                                    CompanyIdentity = "0401430088",
                                    CreateMessageIssue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                },
                                From = new NameBase()
                                {
                                    Name = GlobalSettings.TEN_DON_VI,
                                    Identity = GlobalSettings.MA_DON_VI,
                                },
                                To = new NameBase()
                                {
                                    //Name = TKMD.TenCoQuanHaiQuan,
                                    //Identity = TKMD.CoQuanHaiQuan,
                                    Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(khoCFS.MaKhoCFS.Substring(0,4))),
                                    Identity = khoCFS.MaKhoCFS.Substring(0,4)
                                },
                                Subject = new Subject()
                                {
                                    Type = DeclarationIssuer.TTHangGuiKho,
                                    Function = DeclarationFunction.KHAIBAO_HANGGUIKHO,
                                    Reference = khoCFS.GuidStr,
                                    SendApplication = "SOFTECH_ECS",
                                    ReceiveApplication = "CFS_Service",
                                }
                            },
                            new NameBase()
                            {
                                Name = GlobalSettings.TEN_DON_VI,
                                Identity = GlobalSettings.MA_DON_VI,
                            },
                            new NameBase()
                            {
                                    //Name = TKMD.TenCoQuanHaiQuan,
                                    //Identity = TKMD.CoQuanHaiQuan,
                                Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(khoCFS.MaKhoCFS.Substring(0, 4))),
                                Identity = khoCFS.MaKhoCFS.Substring(0, 4)
                            },
                            new Subject()
                            {
                                    Type = DeclarationIssuer.TTHangGuiKho,
                                    Function = DeclarationFunction.KHAIBAO_HANGGUIKHO,
                                    //Reference = phieukhoDK.GuidStr,
                                    Reference = khoCFS.GuidStr,
                                    SendApplication = "SOFTECH_ECS",
                                    ReceiveApplication = "CFS_Service",
                            },
                            pnk
                            );
                        //phieukhoDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        khoCFS.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend, 1,khoCFS.MaHQ);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            //sendForm.Message.XmlSaveMessage(phieukhoDK.ID, MessageTitle.PhieuNhapKho);
                            sendForm.Message.XmlSaveMessage(khoCFS.ID, MessageTitle.PhieuNhapKho);
                            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                            
                            sendXML.LoaiHS = LoaiKhaiBao.PhieuNhapKho;
                            
                            sendXML.master_id = khoCFS.ID;
                            sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                            sendXML.func = Convert.ToInt32(pnk.Function);
                            sendXML.InsertUpdate();

                            //phieukhoDK.Update();
                            khoCFS.Update();
                            
                            //FeedBackV5();
                            SetCommandStatus();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                    PhieuKho_Load(null,null);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        private void CancelV5()
        {
            if (ShowMessage("Bạn có muốn khai báo phụ kiện này đến Hải quan?", true) == "Yes")
            {

                //if (phieukhoDK.ID == 0)
                if (khoCFS_ID == 0)
                {
                    this.ShowMessage("Doanh nghiệp hãy lưu thông tin trước khi khai báo", false);
                    return;
                }
                else
                {
                    //phieukhoDK = KDT_PhieuKhoDangKy.Load(phieukhoDK.ID);

                    khoCFS = KDT_KhoCFS_DangKy.LoadFull(khoCFS.ID);
                }
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.PhieuNhapKho;
                //sendXML.master_id = phieukhoDK.ID;
                sendXML.master_id = khoCFS.ID;
                //if (sendXML.Load())
                //{
                //    ShowMessage("Thông tin đã được gửi đến hải quan. Nhấn [Lấy phản hồi] để lấy thông tin", false);
                //    cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                //    NhanDuLieu.Enabled = NhanDuLieu1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                //    return;
                //}
                try
                {
                    //if (this.phieukhoDK.ID!=0)
                    if (this.khoCFS.ID != 0)
                    {

                        string returnMessage = string.Empty;
                        //phieukhoDK.GuidStr = Guid.NewGuid().ToString();
                        //khoCFS.GuidStr = Guid.NewGuid().ToString();
                         PhieuNhapKho pnk = new PhieuNhapKho();
                        //pnk = Mapper_V4.ToDaTaTransferPhieuNhapKho(TKMD,phieukhoDK);
                         pnk = ToDaTaTransferCancelPhieuKhoCFS(khoCFS, GlobalSettings.MA_DON_VI, GlobalSettings.TEN_DON_VI);

                        ObjectSend msgSend = new ObjectSend(
                            new MessageHeader()
                            {
                                ProcedureType = "2",
                                Reference = new Reference()
                                {
                                    Version = "3.00",
                                    MessageID = khoCFS.GuidStr,
                                },
                                SendApplication = new SendApplication()
                                {
                                    Name = "Softech_ECS",
                                    Version = "5.00",
                                    CompanyName = "Công ty cổ phần SOFTECH Đà Nẵng",
                                    CompanyIdentity = "0401430088",
                                    CreateMessageIssue = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                },
                                From = new NameBase()
                                {
                                    Name = GlobalSettings.TEN_DON_VI,
                                    Identity = GlobalSettings.MA_DON_VI,
                                },
                                To = new NameBase()
                                {
                                    //Name = TKMD.TenCoQuanHaiQuan,
                                    //Identity = TKMD.CoQuanHaiQuan,
                                    Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(khoCFS.MaKhoCFS.Substring(0, 4))),
                                    Identity = khoCFS.MaKhoCFS.Substring(0, 4)
                                },
                                Subject = new Subject()
                                {
                                    Type = DeclarationIssuer.TTHangGuiKho,
                                    Function = DeclarationFunction.KHAIBAO_HANGGUIKHO,
                                    Reference = khoCFS.GuidStr,
                                    SendApplication = "SOFTECH_ECS",
                                    ReceiveApplication = "CFS_Service",
                                }
                            },
                            new NameBase()
                            {
                                Name = GlobalSettings.TEN_DON_VI,
                                Identity = GlobalSettings.MA_DON_VI,
                            },
                            new NameBase()
                            {
                                //Name = TKMD.TenCoQuanHaiQuan,
                                //Identity = TKMD.CoQuanHaiQuan,
                                Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(khoCFS.MaKhoCFS.Substring(0, 4))),
                                Identity = khoCFS.MaKhoCFS.Substring(0, 4)
                            },
                            new Subject()
                            {
                                Type = DeclarationIssuer.TTHangGuiKho,
                                Function = DeclarationFunction.KHAIBAO_HUYHANGGUIKHO,
                                //Reference = phieukhoDK.GuidStr,
                                Reference = khoCFS.GuidStr,
                                SendApplication = "SOFTECH_ECS",
                                ReceiveApplication = "CFS_Service",
                            },
                            pnk
                            );
                        //phieukhoDK.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                        if (khoCFS.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
                        {
                            khoCFS.TrangThaiXuLy = TrangThaiXuLy.HUY_KHAI_BAO;
                        }
                        SendMessageForm sendForm = new SendMessageForm();
                        sendForm.Send += SendMessage;
                        bool isSend = sendForm.DoSend(msgSend, 1,khoCFS.MaHQ);
                        if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            //sendForm.Message.XmlSaveMessage(phieukhoDK.ID, MessageTitle.PhieuNhapKho);
                            sendForm.Message.XmlSaveMessage(khoCFS.ID, MessageTitle.PhieuNhapKho);
                            cmdFeedback.Enabled = cmdFeedback1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                            cmdSave.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                            sendXML.LoaiHS = LoaiKhaiBao.PhieuNhapKho;

                            sendXML.master_id = khoCFS.ID;
                            sendXML.msg = msgSend.ToString();
                            sendXML.func = 1;
                            sendXML.InsertUpdate();

                            //phieukhoDK.Update();
                            khoCFS.TrangThaiXuLy = TrangThaiXuLy.DA_HUY;
                            khoCFS.Update();
                            SetCommandStatus();
                            //FeedBackV5();
                        }
                        else if (!string.IsNullOrEmpty(msgInfor))
                            ShowMessageTQDT(msgInfor, false);
                    }
                    PhieuKho_Load(null, null);
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    //
                }
            }
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            //feedbackContent = SingleMessage.PhieuKhoSendHandler(phieukhoDK, ref msgInfor, e);
            feedbackContent = SingleMessage.PhieuKhoSendHandler(khoCFS, ref msgInfor, e);
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            while (isFeedBack)
            {
                //string reference = phieukhoDK.GuidStr;
                string reference = khoCFS.GuidStr;

                SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = DeclarationIssuer.LayTTHangGuiKho,
                    Reference = reference,
                    Function = DeclarationFunction.LAY_TT_HANGGUIKHO,
                    Type = DeclarationIssuer.LayTTHangGuiKho,

                };
                
                //subjectBase.Type = DeclarationIssuer.PhieuNhapKho;
                

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = GlobalSettings.MA_DON_VI
                                            },
                                              new NameBase()
                                              {
                                                  //Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(phieukhoDK.MaHQ.Trim())),
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(khoCFS.MaKhoCFS.Substring(0,4))),
                                                  //Identity = phieukhoDK.MaHQ
                                                  Identity = khoCFS.MaKhoCFS.Substring(0, 4)
                                              }, subjectBase, null);
                //if (phieukhoDK.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                if (khoCFS.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
                {
                    //msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(TKMD.CoQuanHaiQuan));
                    //msgSend.To.Identity = TKMD.CoQuanHaiQuan;
                    msgSend.To.Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(VNACCS_Mapper.GetCodeV4MaHaiQuan(khoCFS.MaKhoCFS.Substring(0, 4)));
                    msgSend.To.Identity = khoCFS.MaKhoCFS.Substring(0, 4);
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                isFeedBack = sendForm.DoSend(msgSend,1,khoCFS.MaHQ);
                if (isFeedBack)
                {
                    isFeedBack = ShowMessageTQDT(msgInfor + "\r\nBạn có muốn nhận phản hồi tiếp không?", true) == "Yes";
                }

            }
            PhieuKho_Load(null,null);
        }
        private bool CheckTK(long TKMD_ID, KDT_KhoCFS_DangKy khoCFS) 
        {
            try
            {
                foreach (var item in khoCFS.DetailCollection)
                {
                    if (TKMD_ID == item.TKMD_ID)
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        private void btnThemToKhai_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.cbbMaKhoCFS.Code == null)
                {
                    ShowMessageTQDT("Bạn chưa chọn Mã kho CFS để khai báo .", false);
                }
                else
                {
                    VNACC_ToKhaiManager vNACCToKhaiManager = new VNACC_ToKhaiManager()
                    {
                        IsShowChonToKhai = true
                    };
                    vNACCToKhaiManager.ShowDialog(this);
                    if (vNACCToKhaiManager.DialogResult == DialogResult.OK)
                    {
                        this.TKMD = vNACCToKhaiManager.TKMDDuocChon;
                        KDT_KhoCFS_Detail kDTKhoCFSDetail = new KDT_KhoCFS_Detail();
                        if (this.CheckTK(this.TKMD.ID, this.khoCFS))
                        {
                            base.ShowMessageTQDT("Tờ khai này đã có trong danh sách tờ khai .", false);
                        }
                        else
                        {
                            kDTKhoCFSDetail.TKMD_ID = this.TKMD.ID;
                            kDTKhoCFSDetail.SoToKhai = this.TKMD.SoToKhai;
                            kDTKhoCFSDetail.NgayDangKy = this.TKMD.NgayDangKy;
                            kDTKhoCFSDetail.MaLoaiHinh = this.TKMD.MaLoaiHinh;
                            this.TKMDCollection.Add(this.TKMD);
                            this.khoCFS.DetailCollection.Add(kDTKhoCFSDetail);
                            this.SetChange(true);
                        }
                    }
                    try
                    {
                        this.grList.Refetch();
                        this.grList.DataSource = this.khoCFS.DetailCollection;
                        this.grList.Refresh();
                    }
                    catch (Exception ex)
                    {
                        Logger.LocalLogger.Instance().WriteMessage(ex);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void ReLoad() 
        {
            if (khoCFS.ID != 0) 
            {
                cbbMaKhoCFS.Code = khoCFS.MaKhoCFS;
                cbbCoQuanHQ.Code = khoCFS.MaHQ;//.Substring(0, 4);
                khoCFS.DetailCollection = KDT_KhoCFS_Detail.SelectCollectionDynamic("Master_ID = " + khoCFS.ID, "");
            }
            try
            {
                //DataTable dtb = this.makeTB();
                //for (int i = 0; i < khoCFS.ListTK.Count; i++)
                //{
                //    DataRow dr = dtb.NewRow();
                //    dr["TKMD_ID"] = khoCFS.ListTK[i].TKMD_ID;
                //    dr["SoToKhai"] = khoCFS.ListTK[i].SoToKhai;
                //    dr["MaLoaiHinh"] = khoCFS.ListTK[i].MaLoaiHinh;
                //    dtb.Rows.Add(dr);
                //}

                grList.Refetch();
                grList.DataSource = khoCFS.DetailCollection;
                grList.Refresh();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
            
        }

        private void grList_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }

        private void grList_MouseClick(object sender, MouseEventArgs e)
        {

        }

        public static PhieuNhapKho ToDaTaTransferPhieuKhoCFS(KDT_KhoCFS_DangKy khoCFS, string maDV, string tenVD)
        {

            PhieuNhapKho pnk = new PhieuNhapKho()
            {
                Issuer = DeclarationIssuer.TTHangGuiKho,
                Function = DeclarationFunction.KHAIBAO_HANGGUIKHO,
                Reference = khoCFS.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                DeclarationOffice = khoCFS.MaHQ,
                Status = "1",
                CustomsReference = "",
                Acceptance = DateTime.Now.ToString(sfmtDate),
                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Name = tenVD,
                    Identity = maDV
                },
                ReferencePNK = new ReferencePNK()
                {
                    Indentity = maDV,
                    DeclarationOffice = khoCFS.MaKhoCFS.Substring(0, 4),
                    Warehouse = khoCFS.MaKhoCFS,
                    //Test mã kho
                    //Warehouse = "43ND001",
                    DeclarationList = new List<DeclarationList>()
                }
            };
            foreach (KDT_KhoCFS_Detail Detail in khoCFS.ListTK)
            {
                KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(Detail.TKMD_ID);
                TKMD.TyGiaCollection = KDT_VNACC_TK_PhanHoi_TyGia.SelectCollectionDynamic("Master_ID = " + TKMD.ID, "ID");
                TKMD.HangCollection = KDT_VNACC_HangMauDich.SelectCollectionDynamic("TKMD_ID = " + TKMD.ID, "ID");
                pnk.ReferencePNK.DeclarationList.Add(new DeclarationList()
                {
                    Reference = TKMD.SoToKhai.ToString(),
                    Acceptance = TKMD.NgayDangKy.ToString(sfmtDate),
                    //Acceptance = DateTime.Now.ToString(sfmtDate),
                    DeclarationOffice = TKMD.CoQuanHaiQuan,
                    DeclarationHeadOffice = "",
                    NatureOfTransaction = TKMD.MaLoaiHinh,
                    Identification = TKMD.HangCollection[0].MaSoHang.Substring(0, 4),
                    Identificationname = TKMD.HangCollection[0].TenHang,
                    PackagingQuanlity = float.Parse(TKMD.SoLuong.ToString()).ToString(),
                    PackagingUnit = TKMD.MaDVTSoLuong,
                    TotalGrossMass = TKMD.TrongLuong.ToString(),
                    TotalGrossMassUnit = TKMD.MaDVTTrongLuong,
                    TotalcustomsValue = TKMD.TongTriGiaHD.ToString(),
                    //TotalcustomsValue = "8.08",
                    CurrencyType = TKMD.MaTTHoaDon,
                    RateVND = TKMD.TyGiaCollection[0].TyGiaTinhThue.ToString(),
                    RateUSD = "",
                    QueueDestination = TKMD.MaDiaDiemXepHang,
                    DeliveryDestination = khoCFS.MaKhoCFS,
                    DeliveryDestinationDate = TKMD.NgayDen.ToString(sfmtDate),
                    InvoiceNo = TKMD.SoHoaDon,
                    InvoiceDate = TKMD.NgayPhatHanhHD.ToString(sfmtDate),
                    //DeliveryDestinationDate = DateTime.Now.ToString(sfmtDate),
                    PluongInformation = TKMD.MaPhanLoaiKiemTra,
                });
            }
            //foreach (KDT_KhoCFS_Detail tk in khoCFS.ListTK)
            //{
            //    KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(tk.TKMD_ID);
            //    KDT_VNACC_TK_PhanHoi_TyGia tygia = KDT_VNACC_TK_PhanHoi_TyGia.SelectCollectionAll().Find(delegate(KDT_VNACC_TK_PhanHoi_TyGia t)
            //    {
            //        return t.Master_ID == TKMD.ID;
            //    });
            //    KDT_VNACC_HangMauDich hmd = KDT_VNACC_HangMauDich.SelectCollectionAll().Find(delegate(KDT_VNACC_HangMauDich h)
            //    {
            //        return h.TKMD_ID == TKMD.ID;
            //    });
            //    pnk.ReferencePNK.DeclarationList.Add(new DeclarationList()
            //    {
            //        Reference = TKMD.SoToKhai.ToString(),
            //        Acceptance = TKMD.NgayDangKy.ToString(sfmtDate),
            //        //Acceptance = DateTime.Now.ToString(sfmtDate),
            //        DeclarationOffice = TKMD.CoQuanHaiQuan,
            //        DeclarationHeadOffice = "",
            //        NatureOfTransaction = TKMD.MaLoaiHinh,
            //        Identification = hmd.MaSoHang.Substring(0, 4),
            //        Identificationname = hmd.TenHang,
            //        PackagingQuanlity = float.Parse(TKMD.SoLuong.ToString()).ToString(),
            //        PackagingUnit = TKMD.MaDVTSoLuong,
            //        TotalGrossMass = TKMD.TrongLuong.ToString(),
            //        TotalGrossMassUnit = TKMD.MaDVTTrongLuong,
            //        TotalcustomsValue = TKMD.TongTriGiaHD.ToString(),
            //        //TotalcustomsValue = "8.08",
            //        CurrencyType = TKMD.MaTTHoaDon,
            //        RateVND = tygia.TyGiaTinhThue.ToString(),
            //        RateUSD = "",
            //        QueueDestination = TKMD.MaDiaDiemXepHang,
            //        DeliveryDestination = khoCFS.MaKhoCFS,
            //        DeliveryDestinationDate = TKMD.NgayDen.ToString(sfmtDate),
            //        InvoiceNo = TKMD.SoHoaDon,
            //        InvoiceDate = TKMD.NgayPhatHanhHD.ToString(sfmtDate),
            //        //DeliveryDestinationDate = DateTime.Now.ToString(sfmtDate),
            //        PluongInformation = TKMD.MaPhanLoaiKiemTra,
            //    });
            //}

            pnk.Agents.Add(new Agent()
            {
                Name = tenVD,
                Identity = maDV,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            return pnk;
        }
        public static PhieuNhapKho ToDaTaTransferCancelPhieuKhoCFS(KDT_KhoCFS_DangKy khoCFS, string maDV, string tenVD)
        {

            PhieuNhapKho pnk = new PhieuNhapKho()
            {
                Issuer = DeclarationIssuer.TTHangGuiKho,
                Function = DeclarationFunction.KHAIBAO_HUYHANGGUIKHO,
                Reference = khoCFS.GuidStr,
                Issue = DateTime.Now.ToString(sfmtDateTime),
                IssueLocation = "",
                DeclarationOffice = khoCFS.MaHQ,
                Status = "1",
                CustomsReference = "",
                Acceptance = DateTime.Now.ToString(sfmtDate),
                Agents = new List<Agent>(),
                Importer = new NameBase()
                {
                    Name = tenVD,
                    Identity = maDV
                },
                ReferencePNK = new ReferencePNK()
                {
                    Indentity = maDV,
                    DeclarationOffice = khoCFS.MaKhoCFS.Substring(0, 4),
                    Warehouse = khoCFS.MaKhoCFS,
                    DeclarationList = new List<DeclarationList>()
                }
            };

            pnk.Agents.Add(new Agent()
            {
                Name = tenVD,
                Identity = maDV,
                Status = AgentsStatus.NGUOIKHAI_HAIQUAN
            });
            return pnk;
        }

        private void linkHDSD_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("https://www.youtube.com/watch?v=gSX-Av4Bt2c");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void cbbMaKhoCFS_Leave(object sender, EventArgs e)
        {
            try
            {
                cbbCoQuanHQ.Code = cbbMaKhoCFS.Code.ToString().Substring(0, 4);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", "DOANH NGHIỆP CÓ MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
                {
                    GridEXSelectedItemCollection items = grList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            KDT_KhoCFS_Detail Detail = new KDT_KhoCFS_Detail();
                            Detail = (KDT_KhoCFS_Detail)i.GetRow().DataRow;
                            if (Detail.TKMD_ID > 0)
                            {
                                Detail.Delete();
                            }
                            this.khoCFS.DetailCollection.Remove(Detail);
                        }
                    }
                    ShowMessageTQDT(" Thông báo từ hệ thống ", "XÓA THÀNH CÔNG", false);
                    ReLoad();
                    this.SetChange(true);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                try
                {
                    Cursor = Cursors.WaitCursor;

                    int id = Convert.ToInt32(e.Row.Cells["TKMD_ID"].Value);
                    KDT_VNACC_ToKhaiMauDich TKMD = KDT_VNACC_ToKhaiMauDich.Load(id);
                    //TKMD = TKMD.LoadToKhai(id);
                    TKMD.LoadFull();
                    string loaiHinh = "";
                    DataSet ds = VNACC_Category_Common.SelectDynamic("Code ='" + TKMD.MaLoaiHinh + "'", "ID");
                    DataRow dr = ds.Tables[0].Rows[0];
                    loaiHinh = dr["ReferenceDB"].ToString();

                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E002.ToString())
                    {
                        VNACC_ToKhaiMauDichXuatForm f = new VNACC_ToKhaiMauDichXuatForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                    }
                    if (loaiHinh == Company.KDT.SHARE.VNACCS.ECategory.E001.ToString())
                    {
                        VNACC_ToKhaiMauDichNhapForm f = new VNACC_ToKhaiMauDichNhapForm();
                        f.TKMD = new KDT_VNACC_ToKhaiMauDich();
                        f.TKMD = TKMD;
                        f.ShowDialog();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
                }
                finally { Cursor = Cursors.Default; }
            }
        }

        private void PhieuKho_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Khai báo hàng vào Kho CFS có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    Save(true);
                }
            }
        }
     }
}

