using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

/// <summary>
/// Summary description for Verify
/// </summary>
public class Verify
{
    private string productid;
    private string key;
    private ActivateResults objResults;

    private Entity.Serial objSerial;
    private Entity.License objLicense;

	public Verify(string Serial, string ProductID, string Key)
	{
        this.productid = ProductID;
        this.key = Key;
        this.objSerial = Entity.Serial.Load(Serial);

	}

    public ActivateResults doVerify()
    {
        objResults = new ActivateResults();
        objResults.Value = false;
        if (this.validSerial())
        {
            if (this.checkUsingExpired())
            {
                int value = this.checkSufficientQuantity();
                if (value > 0)
                {
                    objResults.Value = true;
                    if (value == 1)
                    {
                        objResults.Information = this.Activate(true);
                        objResults.Message = MsgActivate.reActivate;
                    }
                    else if (value == 2)
                    {
                        objResults.Information = this.Activate(false);
                        objResults.Message = MsgActivate.newActivate;
                    }
                }
                else
                {
                    objResults.Message = MsgActivate.sufficientQuantity;
                }
            }
            else
            {
                objResults.Message = MsgActivate.usingExpired;
            }
        }
        else
        {
            objResults.Message = MsgActivate.inAccurate;
        }

        return objResults;
    }

    // Kiểm tra đã activate hết chưa
    private int checkSufficientQuantity()
    {
        DataTable dt = Entity.SerialLog.SelectBy_SerialActivating(objSerial.Code).Tables[0];
        int value = 0;

        if (this.checkKeyActivated(dt))
        {
            value = 1;
        }
        else
        {
            if (dt.Rows.Count < objLicense.NumberClient)
            {
                value = 2;
            }
        }
        return value;
    }

    // Kiểm tra hạn sử dụng
    private bool checkUsingExpired()
    {
        bool value = false;
        if (objSerial.CreateDate.AddMonths(objLicense.Term) > DateTime.Now)
        {
            value = true;
        }
        return value;
    }

    // Kiểm tra Serial đã activate key này chưa
    private bool checkKeyActivated(DataTable dt)
    {
        bool reActivate = false;
        for (int i = 0; i < dt.Rows.Count; i++)
        {
            if (dt.Rows[i]["KeyActivate"].ToString() == this.key)
            {
                reActivate = true;
                break;
            }
        }
        return reActivate;
    }

    // Xác thực Serial
    private bool validSerial()
    {
        bool value = false;
        if (objSerial != null && objSerial.ProductID == this.productid)
        {
            objLicense = Entity.License.Load(this.objSerial.LicenseID);
            if (objLicense != null)
            {
                value = true;
            }
        }
        return value;
    }

    // Quá trình xử lý actvate
    private SerialInfo Activate(bool reActivate)
    {
        SerialInfo objSerialInfo = new SerialInfo();
        Entity.SerialLog objSerialLog;

        try
        {
            objResults.Value = true;
            objResults.Message = "Cài đặt mới";
            if (reActivate)
            {
                objSerialLog = Entity.SerialLog.LoadBy_KeyActivating(objSerial.Code, key);
                objSerialLog.Descriptions = "Cài đặt lại";
                objSerialLog.Activating = false;
                objSerialLog.Update();
            }

            objSerialLog = new Entity.SerialLog();
            objSerialLog.SerialCode = objSerial.Code;
            objSerialLog.KeyActivate = key;
            objSerialLog.CreateDate = DateTime.Now;
            objSerialLog.Activating = true;
            objSerialLog.Insert();

            objSerialInfo.LicenseName = objLicense.Name;
            objSerialInfo.NumberClient = objLicense.NumberClient;
            objSerialInfo.DateExpires = objSerial.CreateDate.AddMonths(objLicense.Term).ToString("dd/MM/yyyy");
        }
        catch
        {
            
        }
        return objSerialInfo;
    }
}

public class MsgActivate
{
    public static string inAccurate = "Thông tin Serial không chính xác";
    public static string doseNotExist = "License không tồn tại";
    public static string usingExpired = "Serial đã hết hạn sử dụng";
    public static string sufficientQuantity = "Serial này đã sử dụng hết số máy";
    public static string reActivate = "Kích hoạt lại máy đã được đăng ký";
    public static string newActivate = "Kích hoạt mới";
    public static string errServer = "Máy chủ bị lỗi";
}
