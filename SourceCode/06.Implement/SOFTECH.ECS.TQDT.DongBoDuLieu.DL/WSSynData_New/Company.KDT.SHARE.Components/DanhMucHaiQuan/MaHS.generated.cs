using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
	public partial class MaHS : ICloneable
	{
		#region Properties.
		
		public string MaHang { set; get; }
		public string MoTa_VN { set; get; }
		public string DVT { set; get; }
		public string MoTa_EN { set; get; }
		public string MaTuongUng { set; get; }
		public double ThueNKUD { set; get; }
		public double ThueNKUDDB { set; get; }
		public double ThueVAT { set; get; }
		public double ThueTTDB { set; get; }
		public double ThueXK { set; get; }
		public double ThueMT { set; get; }
		public string GhiChu { set; get; }
		public string ChinhSachMatHang { set; get; }
		public string QuanLyGia { set; get; }
		public string ThuocNhom { set; get; }
		public string GiayPhepNKKTD { set; get; }
		public string GiayPhepNK { set; get; }
		public string KiemDich { set; get; }
		public string PhanTichNguyCoGayHai { set; get; }
		public string VSATTP { set; get; }
		public string HangHoaNhom2KTCL { set; get; }
		public string CamNKQSD { set; get; }
		public string CamNK { set; get; }
		public string CamXK { set; get; }
		public string HanNgachThueQuan { set; get; }
		public string QuanLyHoaChat { set; get; }
		public string HangTieuDungTT07 { set; get; }
		public string ChinhSachKhac { set; get; }
		public DateTime DateCreated { set; get; }
		public DateTime DateModified { set; get; }
		public string Nhom { set; get; }
		public string PN1 { set; get; }
		public string PN2 { set; get; }
		public string Pn3 { set; get; }
		public string Mo_ta { set; get; }
		public string HS10SO { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<MaHS> ConvertToCollection(IDataReader reader)
		{
			IList<MaHS> collection = new List<MaHS>();
			while (reader.Read())
			{
				MaHS entity = new MaHS();
				if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MoTa_VN"))) entity.MoTa_VN = reader.GetString(reader.GetOrdinal("MoTa_VN"));
				if (!reader.IsDBNull(reader.GetOrdinal("DVT"))) entity.DVT = reader.GetString(reader.GetOrdinal("DVT"));
				if (!reader.IsDBNull(reader.GetOrdinal("MoTa_EN"))) entity.MoTa_EN = reader.GetString(reader.GetOrdinal("MoTa_EN"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTuongUng"))) entity.MaTuongUng = reader.GetString(reader.GetOrdinal("MaTuongUng"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueNKUD"))) entity.ThueNKUD = reader.GetDouble(reader.GetOrdinal("ThueNKUD"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueNKUDDB"))) entity.ThueNKUDDB = reader.GetDouble(reader.GetOrdinal("ThueNKUDDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueVAT"))) entity.ThueVAT = reader.GetDouble(reader.GetOrdinal("ThueVAT"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueTTDB"))) entity.ThueTTDB = reader.GetDouble(reader.GetOrdinal("ThueTTDB"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueXK"))) entity.ThueXK = reader.GetDouble(reader.GetOrdinal("ThueXK"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThueMT"))) entity.ThueMT = reader.GetDouble(reader.GetOrdinal("ThueMT"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChinhSachMatHang"))) entity.ChinhSachMatHang = reader.GetString(reader.GetOrdinal("ChinhSachMatHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuanLyGia"))) entity.QuanLyGia = reader.GetString(reader.GetOrdinal("QuanLyGia"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThuocNhom"))) entity.ThuocNhom = reader.GetString(reader.GetOrdinal("ThuocNhom"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayPhepNKKTD"))) entity.GiayPhepNKKTD = reader.GetString(reader.GetOrdinal("GiayPhepNKKTD"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiayPhepNK"))) entity.GiayPhepNK = reader.GetString(reader.GetOrdinal("GiayPhepNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("KiemDich"))) entity.KiemDich = reader.GetString(reader.GetOrdinal("KiemDich"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanTichNguyCoGayHai"))) entity.PhanTichNguyCoGayHai = reader.GetString(reader.GetOrdinal("PhanTichNguyCoGayHai"));
				if (!reader.IsDBNull(reader.GetOrdinal("VSATTP"))) entity.VSATTP = reader.GetString(reader.GetOrdinal("VSATTP"));
				if (!reader.IsDBNull(reader.GetOrdinal("HangHoaNhom2KTCL"))) entity.HangHoaNhom2KTCL = reader.GetString(reader.GetOrdinal("HangHoaNhom2KTCL"));
				if (!reader.IsDBNull(reader.GetOrdinal("CamNKQSD"))) entity.CamNKQSD = reader.GetString(reader.GetOrdinal("CamNKQSD"));
				if (!reader.IsDBNull(reader.GetOrdinal("CamNK"))) entity.CamNK = reader.GetString(reader.GetOrdinal("CamNK"));
				if (!reader.IsDBNull(reader.GetOrdinal("CamXK"))) entity.CamXK = reader.GetString(reader.GetOrdinal("CamXK"));
				if (!reader.IsDBNull(reader.GetOrdinal("HanNgachThueQuan"))) entity.HanNgachThueQuan = reader.GetString(reader.GetOrdinal("HanNgachThueQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("QuanLyHoaChat"))) entity.QuanLyHoaChat = reader.GetString(reader.GetOrdinal("QuanLyHoaChat"));
				if (!reader.IsDBNull(reader.GetOrdinal("HangTieuDungTT07"))) entity.HangTieuDungTT07 = reader.GetString(reader.GetOrdinal("HangTieuDungTT07"));
				if (!reader.IsDBNull(reader.GetOrdinal("ChinhSachKhac"))) entity.ChinhSachKhac = reader.GetString(reader.GetOrdinal("ChinhSachKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateCreated"))) entity.DateCreated = reader.GetDateTime(reader.GetOrdinal("DateCreated"));
				if (!reader.IsDBNull(reader.GetOrdinal("DateModified"))) entity.DateModified = reader.GetDateTime(reader.GetOrdinal("DateModified"));
				if (!reader.IsDBNull(reader.GetOrdinal("Nhom"))) entity.Nhom = reader.GetString(reader.GetOrdinal("Nhom"));
				if (!reader.IsDBNull(reader.GetOrdinal("PN1"))) entity.PN1 = reader.GetString(reader.GetOrdinal("PN1"));
				if (!reader.IsDBNull(reader.GetOrdinal("PN2"))) entity.PN2 = reader.GetString(reader.GetOrdinal("PN2"));
				if (!reader.IsDBNull(reader.GetOrdinal("Pn3"))) entity.Pn3 = reader.GetString(reader.GetOrdinal("Pn3"));
				if (!reader.IsDBNull(reader.GetOrdinal("Mo_ta"))) entity.Mo_ta = reader.GetString(reader.GetOrdinal("Mo_ta"));
				if (!reader.IsDBNull(reader.GetOrdinal("HS10SO"))) entity.HS10SO = reader.GetString(reader.GetOrdinal("HS10SO"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<MaHS> collection, string maHang)
        {
            foreach (MaHS item in collection)
            {
                if (item.MaHang == maHang)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_HaiQuan_MaHS VALUES(@MaHang, @MoTa_VN, @DVT, @MoTa_EN, @MaTuongUng, @ThueNKUD, @ThueNKUDDB, @ThueVAT, @ThueTTDB, @ThueXK, @ThueMT, @GhiChu, @ChinhSachMatHang, @QuanLyGia, @ThuocNhom, @GiayPhepNKKTD, @GiayPhepNK, @KiemDich, @PhanTichNguyCoGayHai, @VSATTP, @HangHoaNhom2KTCL, @CamNKQSD, @CamNK, @CamXK, @HanNgachThueQuan, @QuanLyHoaChat, @HangTieuDungTT07, @ChinhSachKhac, @DateCreated, @DateModified, @Nhom, @PN1, @PN2, @Pn3, @Mo_ta, @HS10SO)";
            string update = "UPDATE t_HaiQuan_MaHS SET MoTa_VN = @MoTa_VN, DVT = @DVT, MoTa_EN = @MoTa_EN, MaTuongUng = @MaTuongUng, ThueNKUD = @ThueNKUD, ThueNKUDDB = @ThueNKUDDB, ThueVAT = @ThueVAT, ThueTTDB = @ThueTTDB, ThueXK = @ThueXK, ThueMT = @ThueMT, GhiChu = @GhiChu, ChinhSachMatHang = @ChinhSachMatHang, QuanLyGia = @QuanLyGia, ThuocNhom = @ThuocNhom, GiayPhepNKKTD = @GiayPhepNKKTD, GiayPhepNK = @GiayPhepNK, KiemDich = @KiemDich, PhanTichNguyCoGayHai = @PhanTichNguyCoGayHai, VSATTP = @VSATTP, HangHoaNhom2KTCL = @HangHoaNhom2KTCL, CamNKQSD = @CamNKQSD, CamNK = @CamNK, CamXK = @CamXK, HanNgachThueQuan = @HanNgachThueQuan, QuanLyHoaChat = @QuanLyHoaChat, HangTieuDungTT07 = @HangTieuDungTT07, ChinhSachKhac = @ChinhSachKhac, DateCreated = @DateCreated, DateModified = @DateModified, Nhom = @Nhom, PN1 = @PN1, PN2 = @PN2, Pn3 = @Pn3, Mo_ta = @Mo_ta, HS10SO = @HS10SO WHERE MaHang = @MaHang";
            string delete = "DELETE FROM t_HaiQuan_MaHS WHERE MaHang = @MaHang";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTa_VN", SqlDbType.NVarChar, "MoTa_VN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTa_EN", SqlDbType.NVarChar, "MoTa_EN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTuongUng", SqlDbType.NVarChar, "MaTuongUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueNKUD", SqlDbType.Float, "ThueNKUD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueNKUDDB", SqlDbType.Float, "ThueNKUDDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueVAT", SqlDbType.Float, "ThueVAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueTTDB", SqlDbType.Float, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueXK", SqlDbType.Float, "ThueXK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueMT", SqlDbType.Float, "ThueMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChinhSachMatHang", SqlDbType.NVarChar, "ChinhSachMatHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanLyGia", SqlDbType.NVarChar, "QuanLyGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThuocNhom", SqlDbType.NVarChar, "ThuocNhom", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhepNKKTD", SqlDbType.NVarChar, "GiayPhepNKKTD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhepNK", SqlDbType.NVarChar, "GiayPhepNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiemDich", SqlDbType.NVarChar, "KiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanTichNguyCoGayHai", SqlDbType.NVarChar, "PhanTichNguyCoGayHai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@VSATTP", SqlDbType.NVarChar, "VSATTP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HangHoaNhom2KTCL", SqlDbType.NVarChar, "HangHoaNhom2KTCL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CamNKQSD", SqlDbType.NVarChar, "CamNKQSD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CamNK", SqlDbType.NVarChar, "CamNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CamXK", SqlDbType.NVarChar, "CamXK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HanNgachThueQuan", SqlDbType.NVarChar, "HanNgachThueQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanLyHoaChat", SqlDbType.NVarChar, "QuanLyHoaChat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HangTieuDungTT07", SqlDbType.NVarChar, "HangTieuDungTT07", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChinhSachKhac", SqlDbType.NVarChar, "ChinhSachKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Nhom", SqlDbType.NVarChar, "Nhom", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PN1", SqlDbType.NVarChar, "PN1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PN2", SqlDbType.NVarChar, "PN2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Pn3", SqlDbType.NVarChar, "Pn3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Mo_ta", SqlDbType.NVarChar, "Mo_ta", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HS10SO", SqlDbType.NVarChar, "HS10SO", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTa_VN", SqlDbType.NVarChar, "MoTa_VN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTa_EN", SqlDbType.NVarChar, "MoTa_EN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTuongUng", SqlDbType.NVarChar, "MaTuongUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueNKUD", SqlDbType.Float, "ThueNKUD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueNKUDDB", SqlDbType.Float, "ThueNKUDDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueVAT", SqlDbType.Float, "ThueVAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueTTDB", SqlDbType.Float, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueXK", SqlDbType.Float, "ThueXK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueMT", SqlDbType.Float, "ThueMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChinhSachMatHang", SqlDbType.NVarChar, "ChinhSachMatHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanLyGia", SqlDbType.NVarChar, "QuanLyGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThuocNhom", SqlDbType.NVarChar, "ThuocNhom", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhepNKKTD", SqlDbType.NVarChar, "GiayPhepNKKTD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhepNK", SqlDbType.NVarChar, "GiayPhepNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiemDich", SqlDbType.NVarChar, "KiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanTichNguyCoGayHai", SqlDbType.NVarChar, "PhanTichNguyCoGayHai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@VSATTP", SqlDbType.NVarChar, "VSATTP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HangHoaNhom2KTCL", SqlDbType.NVarChar, "HangHoaNhom2KTCL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CamNKQSD", SqlDbType.NVarChar, "CamNKQSD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CamNK", SqlDbType.NVarChar, "CamNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CamXK", SqlDbType.NVarChar, "CamXK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HanNgachThueQuan", SqlDbType.NVarChar, "HanNgachThueQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanLyHoaChat", SqlDbType.NVarChar, "QuanLyHoaChat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HangTieuDungTT07", SqlDbType.NVarChar, "HangTieuDungTT07", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChinhSachKhac", SqlDbType.NVarChar, "ChinhSachKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Nhom", SqlDbType.NVarChar, "Nhom", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PN1", SqlDbType.NVarChar, "PN1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PN2", SqlDbType.NVarChar, "PN2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Pn3", SqlDbType.NVarChar, "Pn3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Mo_ta", SqlDbType.NVarChar, "Mo_ta", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HS10SO", SqlDbType.NVarChar, "HS10SO", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_HaiQuan_MaHS VALUES(@MaHang, @MoTa_VN, @DVT, @MoTa_EN, @MaTuongUng, @ThueNKUD, @ThueNKUDDB, @ThueVAT, @ThueTTDB, @ThueXK, @ThueMT, @GhiChu, @ChinhSachMatHang, @QuanLyGia, @ThuocNhom, @GiayPhepNKKTD, @GiayPhepNK, @KiemDich, @PhanTichNguyCoGayHai, @VSATTP, @HangHoaNhom2KTCL, @CamNKQSD, @CamNK, @CamXK, @HanNgachThueQuan, @QuanLyHoaChat, @HangTieuDungTT07, @ChinhSachKhac, @DateCreated, @DateModified, @Nhom, @PN1, @PN2, @Pn3, @Mo_ta, @HS10SO)";
            string update = "UPDATE t_HaiQuan_MaHS SET MoTa_VN = @MoTa_VN, DVT = @DVT, MoTa_EN = @MoTa_EN, MaTuongUng = @MaTuongUng, ThueNKUD = @ThueNKUD, ThueNKUDDB = @ThueNKUDDB, ThueVAT = @ThueVAT, ThueTTDB = @ThueTTDB, ThueXK = @ThueXK, ThueMT = @ThueMT, GhiChu = @GhiChu, ChinhSachMatHang = @ChinhSachMatHang, QuanLyGia = @QuanLyGia, ThuocNhom = @ThuocNhom, GiayPhepNKKTD = @GiayPhepNKKTD, GiayPhepNK = @GiayPhepNK, KiemDich = @KiemDich, PhanTichNguyCoGayHai = @PhanTichNguyCoGayHai, VSATTP = @VSATTP, HangHoaNhom2KTCL = @HangHoaNhom2KTCL, CamNKQSD = @CamNKQSD, CamNK = @CamNK, CamXK = @CamXK, HanNgachThueQuan = @HanNgachThueQuan, QuanLyHoaChat = @QuanLyHoaChat, HangTieuDungTT07 = @HangTieuDungTT07, ChinhSachKhac = @ChinhSachKhac, DateCreated = @DateCreated, DateModified = @DateModified, Nhom = @Nhom, PN1 = @PN1, PN2 = @PN2, Pn3 = @Pn3, Mo_ta = @Mo_ta, HS10SO = @HS10SO WHERE MaHang = @MaHang";
            string delete = "DELETE FROM t_HaiQuan_MaHS WHERE MaHang = @MaHang";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTa_VN", SqlDbType.NVarChar, "MoTa_VN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTa_EN", SqlDbType.NVarChar, "MoTa_EN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTuongUng", SqlDbType.NVarChar, "MaTuongUng", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueNKUD", SqlDbType.Float, "ThueNKUD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueNKUDDB", SqlDbType.Float, "ThueNKUDDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueVAT", SqlDbType.Float, "ThueVAT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueTTDB", SqlDbType.Float, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueXK", SqlDbType.Float, "ThueXK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThueMT", SqlDbType.Float, "ThueMT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChinhSachMatHang", SqlDbType.NVarChar, "ChinhSachMatHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanLyGia", SqlDbType.NVarChar, "QuanLyGia", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThuocNhom", SqlDbType.NVarChar, "ThuocNhom", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhepNKKTD", SqlDbType.NVarChar, "GiayPhepNKKTD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiayPhepNK", SqlDbType.NVarChar, "GiayPhepNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KiemDich", SqlDbType.NVarChar, "KiemDich", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanTichNguyCoGayHai", SqlDbType.NVarChar, "PhanTichNguyCoGayHai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@VSATTP", SqlDbType.NVarChar, "VSATTP", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HangHoaNhom2KTCL", SqlDbType.NVarChar, "HangHoaNhom2KTCL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CamNKQSD", SqlDbType.NVarChar, "CamNKQSD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CamNK", SqlDbType.NVarChar, "CamNK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CamXK", SqlDbType.NVarChar, "CamXK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HanNgachThueQuan", SqlDbType.NVarChar, "HanNgachThueQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@QuanLyHoaChat", SqlDbType.NVarChar, "QuanLyHoaChat", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HangTieuDungTT07", SqlDbType.NVarChar, "HangTieuDungTT07", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ChinhSachKhac", SqlDbType.NVarChar, "ChinhSachKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Nhom", SqlDbType.NVarChar, "Nhom", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PN1", SqlDbType.NVarChar, "PN1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PN2", SqlDbType.NVarChar, "PN2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Pn3", SqlDbType.NVarChar, "Pn3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Mo_ta", SqlDbType.NVarChar, "Mo_ta", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@HS10SO", SqlDbType.NVarChar, "HS10SO", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTa_VN", SqlDbType.NVarChar, "MoTa_VN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DVT", SqlDbType.NVarChar, "DVT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTa_EN", SqlDbType.NVarChar, "MoTa_EN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTuongUng", SqlDbType.NVarChar, "MaTuongUng", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueNKUD", SqlDbType.Float, "ThueNKUD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueNKUDDB", SqlDbType.Float, "ThueNKUDDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueVAT", SqlDbType.Float, "ThueVAT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueTTDB", SqlDbType.Float, "ThueTTDB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueXK", SqlDbType.Float, "ThueXK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThueMT", SqlDbType.Float, "ThueMT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChinhSachMatHang", SqlDbType.NVarChar, "ChinhSachMatHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanLyGia", SqlDbType.NVarChar, "QuanLyGia", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThuocNhom", SqlDbType.NVarChar, "ThuocNhom", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhepNKKTD", SqlDbType.NVarChar, "GiayPhepNKKTD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiayPhepNK", SqlDbType.NVarChar, "GiayPhepNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KiemDich", SqlDbType.NVarChar, "KiemDich", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanTichNguyCoGayHai", SqlDbType.NVarChar, "PhanTichNguyCoGayHai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@VSATTP", SqlDbType.NVarChar, "VSATTP", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HangHoaNhom2KTCL", SqlDbType.NVarChar, "HangHoaNhom2KTCL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CamNKQSD", SqlDbType.NVarChar, "CamNKQSD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CamNK", SqlDbType.NVarChar, "CamNK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CamXK", SqlDbType.NVarChar, "CamXK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HanNgachThueQuan", SqlDbType.NVarChar, "HanNgachThueQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@QuanLyHoaChat", SqlDbType.NVarChar, "QuanLyHoaChat", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HangTieuDungTT07", SqlDbType.NVarChar, "HangTieuDungTT07", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ChinhSachKhac", SqlDbType.NVarChar, "ChinhSachKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateCreated", SqlDbType.DateTime, "DateCreated", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DateModified", SqlDbType.DateTime, "DateModified", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Nhom", SqlDbType.NVarChar, "Nhom", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PN1", SqlDbType.NVarChar, "PN1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PN2", SqlDbType.NVarChar, "PN2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Pn3", SqlDbType.NVarChar, "Pn3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Mo_ta", SqlDbType.NVarChar, "Mo_ta", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@HS10SO", SqlDbType.NVarChar, "HS10SO", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static MaHS Load(string maHang)
		{
			const string spName = "[dbo].[p_HaiQuan_MaHS_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, maHang);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<MaHS> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<MaHS> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<MaHS> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_HaiQuan_MaHS_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HaiQuan_MaHS_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_HaiQuan_MaHS_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HaiQuan_MaHS_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertMaHS(string moTa_VN, string dVT, string moTa_EN, string maTuongUng, double thueNKUD, double thueNKUDDB, double thueVAT, double thueTTDB, double thueXK, double thueMT, string ghiChu, string chinhSachMatHang, string quanLyGia, string thuocNhom, string giayPhepNKKTD, string giayPhepNK, string kiemDich, string phanTichNguyCoGayHai, string vSATTP, string hangHoaNhom2KTCL, string camNKQSD, string camNK, string camXK, string hanNgachThueQuan, string quanLyHoaChat, string hangTieuDungTT07, string chinhSachKhac, DateTime dateCreated, DateTime dateModified, string nhom, string pN1, string pN2, string pn3, string mo_ta, string hS10SO)
		{
			MaHS entity = new MaHS();	
			entity.MoTa_VN = moTa_VN;
			entity.DVT = dVT;
			entity.MoTa_EN = moTa_EN;
			entity.MaTuongUng = maTuongUng;
			entity.ThueNKUD = thueNKUD;
			entity.ThueNKUDDB = thueNKUDDB;
			entity.ThueVAT = thueVAT;
			entity.ThueTTDB = thueTTDB;
			entity.ThueXK = thueXK;
			entity.ThueMT = thueMT;
			entity.GhiChu = ghiChu;
			entity.ChinhSachMatHang = chinhSachMatHang;
			entity.QuanLyGia = quanLyGia;
			entity.ThuocNhom = thuocNhom;
			entity.GiayPhepNKKTD = giayPhepNKKTD;
			entity.GiayPhepNK = giayPhepNK;
			entity.KiemDich = kiemDich;
			entity.PhanTichNguyCoGayHai = phanTichNguyCoGayHai;
			entity.VSATTP = vSATTP;
			entity.HangHoaNhom2KTCL = hangHoaNhom2KTCL;
			entity.CamNKQSD = camNKQSD;
			entity.CamNK = camNK;
			entity.CamXK = camXK;
			entity.HanNgachThueQuan = hanNgachThueQuan;
			entity.QuanLyHoaChat = quanLyHoaChat;
			entity.HangTieuDungTT07 = hangTieuDungTT07;
			entity.ChinhSachKhac = chinhSachKhac;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			entity.Nhom = nhom;
			entity.PN1 = pN1;
			entity.PN2 = pN2;
			entity.Pn3 = pn3;
			entity.Mo_ta = mo_ta;
			entity.HS10SO = hS10SO;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_HaiQuan_MaHS_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@MoTa_VN", SqlDbType.NVarChar, MoTa_VN);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@MoTa_EN", SqlDbType.NVarChar, MoTa_EN);
			db.AddInParameter(dbCommand, "@MaTuongUng", SqlDbType.NVarChar, MaTuongUng);
			db.AddInParameter(dbCommand, "@ThueNKUD", SqlDbType.Float, ThueNKUD);
			db.AddInParameter(dbCommand, "@ThueNKUDDB", SqlDbType.Float, ThueNKUDDB);
			db.AddInParameter(dbCommand, "@ThueVAT", SqlDbType.Float, ThueVAT);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueXK", SqlDbType.Float, ThueXK);
			db.AddInParameter(dbCommand, "@ThueMT", SqlDbType.Float, ThueMT);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@ChinhSachMatHang", SqlDbType.NVarChar, ChinhSachMatHang);
			db.AddInParameter(dbCommand, "@QuanLyGia", SqlDbType.NVarChar, QuanLyGia);
			db.AddInParameter(dbCommand, "@ThuocNhom", SqlDbType.NVarChar, ThuocNhom);
			db.AddInParameter(dbCommand, "@GiayPhepNKKTD", SqlDbType.NVarChar, GiayPhepNKKTD);
			db.AddInParameter(dbCommand, "@GiayPhepNK", SqlDbType.NVarChar, GiayPhepNK);
			db.AddInParameter(dbCommand, "@KiemDich", SqlDbType.NVarChar, KiemDich);
			db.AddInParameter(dbCommand, "@PhanTichNguyCoGayHai", SqlDbType.NVarChar, PhanTichNguyCoGayHai);
			db.AddInParameter(dbCommand, "@VSATTP", SqlDbType.NVarChar, VSATTP);
			db.AddInParameter(dbCommand, "@HangHoaNhom2KTCL", SqlDbType.NVarChar, HangHoaNhom2KTCL);
			db.AddInParameter(dbCommand, "@CamNKQSD", SqlDbType.NVarChar, CamNKQSD);
			db.AddInParameter(dbCommand, "@CamNK", SqlDbType.NVarChar, CamNK);
			db.AddInParameter(dbCommand, "@CamXK", SqlDbType.NVarChar, CamXK);
			db.AddInParameter(dbCommand, "@HanNgachThueQuan", SqlDbType.NVarChar, HanNgachThueQuan);
			db.AddInParameter(dbCommand, "@QuanLyHoaChat", SqlDbType.NVarChar, QuanLyHoaChat);
			db.AddInParameter(dbCommand, "@HangTieuDungTT07", SqlDbType.NVarChar, HangTieuDungTT07);
			db.AddInParameter(dbCommand, "@ChinhSachKhac", SqlDbType.NVarChar, ChinhSachKhac);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			db.AddInParameter(dbCommand, "@Nhom", SqlDbType.NVarChar, Nhom);
			db.AddInParameter(dbCommand, "@PN1", SqlDbType.NVarChar, PN1);
			db.AddInParameter(dbCommand, "@PN2", SqlDbType.NVarChar, PN2);
			db.AddInParameter(dbCommand, "@Pn3", SqlDbType.NVarChar, Pn3);
			db.AddInParameter(dbCommand, "@Mo_ta", SqlDbType.NVarChar, Mo_ta);
			db.AddInParameter(dbCommand, "@HS10SO", SqlDbType.NVarChar, HS10SO);
			
			if (transaction != null)
			{
				return db.ExecuteNonQuery(dbCommand, transaction);
			}
            else
			{
				return db.ExecuteNonQuery(dbCommand);
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<MaHS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MaHS item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateMaHS(string maHang, string moTa_VN, string dVT, string moTa_EN, string maTuongUng, double thueNKUD, double thueNKUDDB, double thueVAT, double thueTTDB, double thueXK, double thueMT, string ghiChu, string chinhSachMatHang, string quanLyGia, string thuocNhom, string giayPhepNKKTD, string giayPhepNK, string kiemDich, string phanTichNguyCoGayHai, string vSATTP, string hangHoaNhom2KTCL, string camNKQSD, string camNK, string camXK, string hanNgachThueQuan, string quanLyHoaChat, string hangTieuDungTT07, string chinhSachKhac, DateTime dateCreated, DateTime dateModified, string nhom, string pN1, string pN2, string pn3, string mo_ta, string hS10SO)
		{
			MaHS entity = new MaHS();			
			entity.MaHang = maHang;
			entity.MoTa_VN = moTa_VN;
			entity.DVT = dVT;
			entity.MoTa_EN = moTa_EN;
			entity.MaTuongUng = maTuongUng;
			entity.ThueNKUD = thueNKUD;
			entity.ThueNKUDDB = thueNKUDDB;
			entity.ThueVAT = thueVAT;
			entity.ThueTTDB = thueTTDB;
			entity.ThueXK = thueXK;
			entity.ThueMT = thueMT;
			entity.GhiChu = ghiChu;
			entity.ChinhSachMatHang = chinhSachMatHang;
			entity.QuanLyGia = quanLyGia;
			entity.ThuocNhom = thuocNhom;
			entity.GiayPhepNKKTD = giayPhepNKKTD;
			entity.GiayPhepNK = giayPhepNK;
			entity.KiemDich = kiemDich;
			entity.PhanTichNguyCoGayHai = phanTichNguyCoGayHai;
			entity.VSATTP = vSATTP;
			entity.HangHoaNhom2KTCL = hangHoaNhom2KTCL;
			entity.CamNKQSD = camNKQSD;
			entity.CamNK = camNK;
			entity.CamXK = camXK;
			entity.HanNgachThueQuan = hanNgachThueQuan;
			entity.QuanLyHoaChat = quanLyHoaChat;
			entity.HangTieuDungTT07 = hangTieuDungTT07;
			entity.ChinhSachKhac = chinhSachKhac;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			entity.Nhom = nhom;
			entity.PN1 = pN1;
			entity.PN2 = pN2;
			entity.Pn3 = pn3;
			entity.Mo_ta = mo_ta;
			entity.HS10SO = hS10SO;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_HaiQuan_MaHS_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@MoTa_VN", SqlDbType.NVarChar, MoTa_VN);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@MoTa_EN", SqlDbType.NVarChar, MoTa_EN);
			db.AddInParameter(dbCommand, "@MaTuongUng", SqlDbType.NVarChar, MaTuongUng);
			db.AddInParameter(dbCommand, "@ThueNKUD", SqlDbType.Float, ThueNKUD);
			db.AddInParameter(dbCommand, "@ThueNKUDDB", SqlDbType.Float, ThueNKUDDB);
			db.AddInParameter(dbCommand, "@ThueVAT", SqlDbType.Float, ThueVAT);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueXK", SqlDbType.Float, ThueXK);
			db.AddInParameter(dbCommand, "@ThueMT", SqlDbType.Float, ThueMT);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@ChinhSachMatHang", SqlDbType.NVarChar, ChinhSachMatHang);
			db.AddInParameter(dbCommand, "@QuanLyGia", SqlDbType.NVarChar, QuanLyGia);
			db.AddInParameter(dbCommand, "@ThuocNhom", SqlDbType.NVarChar, ThuocNhom);
			db.AddInParameter(dbCommand, "@GiayPhepNKKTD", SqlDbType.NVarChar, GiayPhepNKKTD);
			db.AddInParameter(dbCommand, "@GiayPhepNK", SqlDbType.NVarChar, GiayPhepNK);
			db.AddInParameter(dbCommand, "@KiemDich", SqlDbType.NVarChar, KiemDich);
			db.AddInParameter(dbCommand, "@PhanTichNguyCoGayHai", SqlDbType.NVarChar, PhanTichNguyCoGayHai);
			db.AddInParameter(dbCommand, "@VSATTP", SqlDbType.NVarChar, VSATTP);
			db.AddInParameter(dbCommand, "@HangHoaNhom2KTCL", SqlDbType.NVarChar, HangHoaNhom2KTCL);
			db.AddInParameter(dbCommand, "@CamNKQSD", SqlDbType.NVarChar, CamNKQSD);
			db.AddInParameter(dbCommand, "@CamNK", SqlDbType.NVarChar, CamNK);
			db.AddInParameter(dbCommand, "@CamXK", SqlDbType.NVarChar, CamXK);
			db.AddInParameter(dbCommand, "@HanNgachThueQuan", SqlDbType.NVarChar, HanNgachThueQuan);
			db.AddInParameter(dbCommand, "@QuanLyHoaChat", SqlDbType.NVarChar, QuanLyHoaChat);
			db.AddInParameter(dbCommand, "@HangTieuDungTT07", SqlDbType.NVarChar, HangTieuDungTT07);
			db.AddInParameter(dbCommand, "@ChinhSachKhac", SqlDbType.NVarChar, ChinhSachKhac);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			db.AddInParameter(dbCommand, "@Nhom", SqlDbType.NVarChar, Nhom);
			db.AddInParameter(dbCommand, "@PN1", SqlDbType.NVarChar, PN1);
			db.AddInParameter(dbCommand, "@PN2", SqlDbType.NVarChar, PN2);
			db.AddInParameter(dbCommand, "@Pn3", SqlDbType.NVarChar, Pn3);
			db.AddInParameter(dbCommand, "@Mo_ta", SqlDbType.NVarChar, Mo_ta);
			db.AddInParameter(dbCommand, "@HS10SO", SqlDbType.NVarChar, HS10SO);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<MaHS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MaHS item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateMaHS(string maHang, string moTa_VN, string dVT, string moTa_EN, string maTuongUng, double thueNKUD, double thueNKUDDB, double thueVAT, double thueTTDB, double thueXK, double thueMT, string ghiChu, string chinhSachMatHang, string quanLyGia, string thuocNhom, string giayPhepNKKTD, string giayPhepNK, string kiemDich, string phanTichNguyCoGayHai, string vSATTP, string hangHoaNhom2KTCL, string camNKQSD, string camNK, string camXK, string hanNgachThueQuan, string quanLyHoaChat, string hangTieuDungTT07, string chinhSachKhac, DateTime dateCreated, DateTime dateModified, string nhom, string pN1, string pN2, string pn3, string mo_ta, string hS10SO)
		{
			MaHS entity = new MaHS();			
			entity.MaHang = maHang;
			entity.MoTa_VN = moTa_VN;
			entity.DVT = dVT;
			entity.MoTa_EN = moTa_EN;
			entity.MaTuongUng = maTuongUng;
			entity.ThueNKUD = thueNKUD;
			entity.ThueNKUDDB = thueNKUDDB;
			entity.ThueVAT = thueVAT;
			entity.ThueTTDB = thueTTDB;
			entity.ThueXK = thueXK;
			entity.ThueMT = thueMT;
			entity.GhiChu = ghiChu;
			entity.ChinhSachMatHang = chinhSachMatHang;
			entity.QuanLyGia = quanLyGia;
			entity.ThuocNhom = thuocNhom;
			entity.GiayPhepNKKTD = giayPhepNKKTD;
			entity.GiayPhepNK = giayPhepNK;
			entity.KiemDich = kiemDich;
			entity.PhanTichNguyCoGayHai = phanTichNguyCoGayHai;
			entity.VSATTP = vSATTP;
			entity.HangHoaNhom2KTCL = hangHoaNhom2KTCL;
			entity.CamNKQSD = camNKQSD;
			entity.CamNK = camNK;
			entity.CamXK = camXK;
			entity.HanNgachThueQuan = hanNgachThueQuan;
			entity.QuanLyHoaChat = quanLyHoaChat;
			entity.HangTieuDungTT07 = hangTieuDungTT07;
			entity.ChinhSachKhac = chinhSachKhac;
			entity.DateCreated = dateCreated;
			entity.DateModified = dateModified;
			entity.Nhom = nhom;
			entity.PN1 = pN1;
			entity.PN2 = pN2;
			entity.Pn3 = pn3;
			entity.Mo_ta = mo_ta;
			entity.HS10SO = hS10SO;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HaiQuan_MaHS_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			db.AddInParameter(dbCommand, "@MoTa_VN", SqlDbType.NVarChar, MoTa_VN);
			db.AddInParameter(dbCommand, "@DVT", SqlDbType.NVarChar, DVT);
			db.AddInParameter(dbCommand, "@MoTa_EN", SqlDbType.NVarChar, MoTa_EN);
			db.AddInParameter(dbCommand, "@MaTuongUng", SqlDbType.NVarChar, MaTuongUng);
			db.AddInParameter(dbCommand, "@ThueNKUD", SqlDbType.Float, ThueNKUD);
			db.AddInParameter(dbCommand, "@ThueNKUDDB", SqlDbType.Float, ThueNKUDDB);
			db.AddInParameter(dbCommand, "@ThueVAT", SqlDbType.Float, ThueVAT);
			db.AddInParameter(dbCommand, "@ThueTTDB", SqlDbType.Float, ThueTTDB);
			db.AddInParameter(dbCommand, "@ThueXK", SqlDbType.Float, ThueXK);
			db.AddInParameter(dbCommand, "@ThueMT", SqlDbType.Float, ThueMT);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@ChinhSachMatHang", SqlDbType.NVarChar, ChinhSachMatHang);
			db.AddInParameter(dbCommand, "@QuanLyGia", SqlDbType.NVarChar, QuanLyGia);
			db.AddInParameter(dbCommand, "@ThuocNhom", SqlDbType.NVarChar, ThuocNhom);
			db.AddInParameter(dbCommand, "@GiayPhepNKKTD", SqlDbType.NVarChar, GiayPhepNKKTD);
			db.AddInParameter(dbCommand, "@GiayPhepNK", SqlDbType.NVarChar, GiayPhepNK);
			db.AddInParameter(dbCommand, "@KiemDich", SqlDbType.NVarChar, KiemDich);
			db.AddInParameter(dbCommand, "@PhanTichNguyCoGayHai", SqlDbType.NVarChar, PhanTichNguyCoGayHai);
			db.AddInParameter(dbCommand, "@VSATTP", SqlDbType.NVarChar, VSATTP);
			db.AddInParameter(dbCommand, "@HangHoaNhom2KTCL", SqlDbType.NVarChar, HangHoaNhom2KTCL);
			db.AddInParameter(dbCommand, "@CamNKQSD", SqlDbType.NVarChar, CamNKQSD);
			db.AddInParameter(dbCommand, "@CamNK", SqlDbType.NVarChar, CamNK);
			db.AddInParameter(dbCommand, "@CamXK", SqlDbType.NVarChar, CamXK);
			db.AddInParameter(dbCommand, "@HanNgachThueQuan", SqlDbType.NVarChar, HanNgachThueQuan);
			db.AddInParameter(dbCommand, "@QuanLyHoaChat", SqlDbType.NVarChar, QuanLyHoaChat);
			db.AddInParameter(dbCommand, "@HangTieuDungTT07", SqlDbType.NVarChar, HangTieuDungTT07);
			db.AddInParameter(dbCommand, "@ChinhSachKhac", SqlDbType.NVarChar, ChinhSachKhac);
			db.AddInParameter(dbCommand, "@DateCreated", SqlDbType.DateTime, DateCreated.Year <= 1753 ? DBNull.Value : (object) DateCreated);
			db.AddInParameter(dbCommand, "@DateModified", SqlDbType.DateTime, DateModified.Year <= 1753 ? DBNull.Value : (object) DateModified);
			db.AddInParameter(dbCommand, "@Nhom", SqlDbType.NVarChar, Nhom);
			db.AddInParameter(dbCommand, "@PN1", SqlDbType.NVarChar, PN1);
			db.AddInParameter(dbCommand, "@PN2", SqlDbType.NVarChar, PN2);
			db.AddInParameter(dbCommand, "@Pn3", SqlDbType.NVarChar, Pn3);
			db.AddInParameter(dbCommand, "@Mo_ta", SqlDbType.NVarChar, Mo_ta);
			db.AddInParameter(dbCommand, "@HS10SO", SqlDbType.NVarChar, HS10SO);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<MaHS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MaHS item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteMaHS(string maHang)
		{
			MaHS entity = new MaHS();
			entity.MaHang = maHang;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HaiQuan_MaHS_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_HaiQuan_MaHS_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<MaHS> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (MaHS item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}