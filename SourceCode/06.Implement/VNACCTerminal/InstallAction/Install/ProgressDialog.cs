﻿namespace Install
{
    using System;
    using System.ComponentModel;
    using System.Drawing;
    using System.Windows.Forms;

    public class ProgressDialog : Form
    {
        private IContainer components;
        private Label fileNameLabel;
        private Label messageLabel;
        private ProgressBar progressBar;

        public ProgressDialog()
        {
            this.InitializeComponent();
            base.ShowInTaskbar = false;
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && (this.components != null))
            {
                this.components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.messageLabel = new Label();
            this.progressBar = new ProgressBar();
            this.fileNameLabel = new Label();
            base.SuspendLayout();
            this.messageLabel.Location = new Point(12, 9);
            this.messageLabel.Name = "messageLabel";
            this.messageLabel.Size = new Size(330, 12);
            this.messageLabel.TabIndex = 0;
            this.messageLabel.Text = "Message";
            this.messageLabel.TextAlign = ContentAlignment.TopCenter;
            this.progressBar.Location = new Point(12, 0x27);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new Size(330, 15);
            this.progressBar.TabIndex = 1;
            this.fileNameLabel.Location = new Point(12, 0x18);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new Size(330, 12);
            this.fileNameLabel.TabIndex = 2;
            this.fileNameLabel.Text = "Path";
            this.fileNameLabel.TextAlign = ContentAlignment.TopCenter;
            base.AutoScaleDimensions = new SizeF(6f, 12f);
            base.AutoScaleMode = AutoScaleMode.Font;
            base.ClientSize = new Size(0x162, 0x60);
            base.ControlBox = false;
            base.Controls.Add(this.fileNameLabel);
            base.Controls.Add(this.progressBar);
            base.Controls.Add(this.messageLabel);
            base.FormBorderStyle = FormBorderStyle.FixedDialog;
            base.Location = new Point(20, 20);
            base.MaximizeBox = false;
            base.MinimizeBox = false;
            base.Name = "ProgressDialog";
            base.StartPosition = FormStartPosition.CenterScreen;
            this.Text = "Private Terminal Software";
            base.TopMost = true;
            base.ResumeLayout(false);
        }

        protected override System.Windows.Forms.CreateParams CreateParams
        {
            get
            {
                System.Windows.Forms.CreateParams createParams = base.CreateParams;
                createParams.ClassStyle |= 0x200;
                return createParams;
            }
        }

        public string FileName
        {
            get
            {
                return this.fileNameLabel.Text;
            }
            set
            {
                this.fileNameLabel.Text = value;
            }
        }

        public long Maximum
        {
            get
            {
                return (long) this.progressBar.Maximum;
            }
            set
            {
                if (value > 0x7fffffffL)
                {
                    this.progressBar.Maximum = 0x7fffffff;
                }
                else
                {
                    this.progressBar.Maximum = (int) value;
                }
            }
        }

        public string Message
        {
            get
            {
                return this.messageLabel.Text;
            }
            set
            {
                this.messageLabel.Text = value;
            }
        }

        public long Minimum
        {
            get
            {
                return (long) this.progressBar.Minimum;
            }
            set
            {
                if (value > 0x7fffffffL)
                {
                    this.progressBar.Minimum = 0x7fffffff;
                }
                else
                {
                    this.progressBar.Minimum = (int) value;
                }
            }
        }

        public long Value
        {
            get
            {
                return (long) this.progressBar.Value;
            }
            set
            {
                if (value > 0x7fffffffL)
                {
                    this.progressBar.Value = 0;
                }
                else
                {
                    this.progressBar.Value = (int) value;
                }
            }
        }
    }
}

