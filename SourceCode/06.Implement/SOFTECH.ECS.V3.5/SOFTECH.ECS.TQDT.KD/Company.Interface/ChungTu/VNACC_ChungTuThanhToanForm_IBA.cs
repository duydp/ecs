﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.VNACCS.ClassVNACC;
using Company.KDT.SHARE.VNACCS.Maper;
using Company.KDT.SHARE.VNACCS.LogMessages;

namespace Company.Interface
{
    public partial class VNACC_ChungTuThanhToanForm_IBA : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ChungTuThanhToan CTTT = new KDT_VNACC_ChungTuThanhToan();
        public KDT_VNACC_ChungTuThanhToan_ChiTiet CTTT_ChiTiet = new KDT_VNACC_ChungTuThanhToan_ChiTiet();
        private ELoaiThongTin LoaiChungTuThanhToan = ELoaiThongTin.IBA;

        public VNACC_ChungTuThanhToanForm_IBA()
        {
            InitializeComponent();

            this.Load += new EventHandler(VNACC_ChungTuThanhToanForm_IBA_Load);
            cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmbMain_CommandClick);

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EPayment.IBA.ToString());
        }

        public VNACC_ChungTuThanhToanForm_IBA(long id)
        {
            InitializeComponent();

            this.Load += new EventHandler(VNACC_ChungTuThanhToanForm_IBA_Load);
            cmbMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(cmbMain_CommandClick);

            base.SetHandler(this);
            base.docGuide = Company.KDT.SHARE.VNACCS.HelperVNACCS.ReadGuideFile(EPayment.IBA.ToString());

            CTTT = KDT_VNACC_ChungTuThanhToan.Load(id);
            CTTT.LoadChungTuThanhToan_ChiTiet();
            SetCTTT();
        }

        void VNACC_ChungTuThanhToanForm_IBA_Load(object sender, EventArgs e)
        {
            cmdLuu.Visible = cmdLuu1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            cmdLayPhanHoi.Visible = cmdLayPhanHoi.Visible = Janus.Windows.UI.InheritableBoolean.False;
            try
            {
                Cursor = Cursors.WaitCursor;

                SetIDControl();

                SetMaxLengthControl();

                ValidateForm(true);

                SetAutoRemoveUnicodeAndUpperCaseControl();

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void cmbMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdLuu":
                    this.SaveCTTT();
                    break;

                case "cmdKhaiBao":
                    GetCTTT();
                    break;
                case "cmdLayPhanHoi":
                    break;
            }
        }

        private void SaveCTTT()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                //if (!ValidateForm(false))
                //    return;

                GetCTTT();

                CTTT.InsertUpdateFull();

                Helper.Controls.MessageBoxControlV.ShowMessage(Company.KDT.SHARE.Components.ThongBao.APPLICATION_SAVE_DATA_SUCCESS_0Param, false);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetCTTT()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                CTTT.LoaiChungTu = LoaiChungTuThanhToan.ToString();
                CTTT.MaNganHangTraThay = txtMaNganHangTraThay.Text;
                CTTT.TenNganHangTraThay = txtTenNganHangTraThay.Text;
                CTTT.NamPhatHanh = Convert.ToInt32(txtNamPhatHanh.Value);
                CTTT.KiHieuChungTuPhatHanhHanMuc = txtKiHieuChungTuPhatHanhHanMuc.Text;
                CTTT.SoHieuPhatHanhHanMuc = txtSoHieuPhatHanhHanMuc.Text;
                CTTT.CoBaoVoHieu = txtCoBaoVohieu.Text;
                CTTT.MaDonViSuDungHanMuc = txtMaDonViSuDungHanMuc.Text;
                CTTT.TenDonViSuDungHanMuc = txtTenDonViSuDungHanMuc.Text;
                CTTT.NgayBatDauHieuLuc = dtNgayBatDauHieuLuc.Value;
                CTTT.NgayHetHieuLuc = dtNgayHetHieuLuc.Value;
                CTTT.SoTienHanMucDangKi = Convert.ToDecimal(txtSoTienHanMucDangKi.Value);
                CTTT.SoDuHanMuc = Convert.ToDecimal(txtSoDuHanMuc.Value);
                CTTT.SoThuTuHanMuc = txtSoThuTuHanMuc.Text;
                CTTT.MaTienTe = ucMaTienTe.Code;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void SetCTTT()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNganHangTraThay.Text = CTTT.MaNganHangTraThay;
                txtTenNganHangTraThay.Text = CTTT.TenNganHangTraThay;
                txtNamPhatHanh.Value = CTTT.NamPhatHanh;
                txtKiHieuChungTuPhatHanhHanMuc.Text = CTTT.KiHieuChungTuPhatHanhHanMuc;
                txtSoHieuPhatHanhHanMuc.Text = CTTT.SoHieuPhatHanhHanMuc;
                txtCoBaoVohieu.Text = CTTT.CoBaoVoHieu;
                txtMaDonViSuDungHanMuc.Text = CTTT.MaDonViSuDungHanMuc;
                txtTenDonViSuDungHanMuc.Text = CTTT.TenDonViSuDungHanMuc;
                dtNgayBatDauHieuLuc.Value = CTTT.NgayBatDauHieuLuc;
                dtNgayHetHieuLuc.Value = CTTT.NgayHetHieuLuc;
                txtSoTienHanMucDangKi.Value = CTTT.SoTienDangKyBaoLanh;
                txtSoDuHanMuc.Value = CTTT.SoDuHanMuc;
                txtSoThuTuHanMuc.Text = CTTT.SoThuTuHanMuc;
                ucMaTienTe.Code = CTTT.MaTienTe;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void grdHang_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                GridEXSelectedItemCollection items = grdHang.SelectedItems;
                if (grdHang.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                CTTT_ChiTiet = (KDT_VNACC_ChungTuThanhToan_ChiTiet)items[0].GetRow().DataRow;
                SetHang(CTTT_ChiTiet);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void GetHang(KDT_VNACC_ChungTuThanhToan_ChiTiet hang)
        {
            hang.SoThuTuGiaoDich = Convert.ToInt32(txtSoThuTuGiaoDich.Value);
            hang.NgayTaoDuLieu = dtNgayTaoDuLieu.Value;
            hang.ThoiGianTaoDuLieu = dtThoiGianTaoDuLieu.Value;
            hang.SoTienTruLui = Convert.ToDecimal(txtSoTienTruLui.Value);
            hang.SoTienTangLen = Convert.ToDecimal(txtSoTienTangLen.Value);
            hang.SoToKhai = Convert.ToInt32(txtSoToKhaiChiTiet.Value);
            hang.NgayDangKyToKhai = dtNgayDangKyToKhai.Value;
            hang.MaLoaiHinh = txtMaLoaiHinhChiTiet.Text;
            hang.CoQuanHaiQuan = txtCoQuanhaiQuanChiTiet.Text;
            hang.TenCucHaiQuan = txtTenCucHaiQuanChiTiet.Text;
        }

        private void SetHang(KDT_VNACC_ChungTuThanhToan_ChiTiet hang)
        {
            txtSoThuTuGiaoDich.Value = hang.SoThuTuGiaoDich;
            dtNgayTaoDuLieu.Value = hang.NgayTaoDuLieu;
            dtThoiGianTaoDuLieu.Value = hang.ThoiGianTaoDuLieu;
            txtSoTienTruLui.Value = hang.SoTienTruLui;
            txtSoTienTangLen.Value = hang.SoTienTangLen;
            txtSoToKhaiChiTiet.Value = hang.SoToKhai;
            dtNgayDangKyToKhai.Value = hang.NgayDangKyToKhai;
            txtMaLoaiHinhChiTiet.Text = hang.MaLoaiHinh;
            txtCoQuanhaiQuanChiTiet.Text = hang.CoQuanHaiQuan;
            txtTenCucHaiQuanChiTiet.Text = hang.TenCucHaiQuan;

        }

        private void ucCategory_OnEnter(object sender, EventArgs e)
        {
            base.ShowGuide(sender, e);
        }

        private bool SetIDControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNganHangTraThay.Tag = "BRC"; //Mã ngân hàng trả thay
                txtMaDonViSuDungHanMuc.Tag = "UBP"; //Mã đơn vị sử dụng hạn mức
                txtNamPhatHanh.Tag = "RYA"; //Năm phát hành
                txtKiHieuChungTuPhatHanhHanMuc.Tag = "BPS"; //Kí hiệu chứng từ phát hành hạn mức
                txtSoHieuPhatHanhHanMuc.Tag = "BPN"; //Số hiệu phát hành hạn mức
                ucMaTienTe.TagName = "CCC"; //Mã tiền tệ

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                isValid &= ValidateControl.ValidateNull(txtMaNganHangTraThay, errorProvider, "Mã ngân hàng trả thay", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtNamPhatHanh, errorProvider, "Năm phát hành", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtKiHieuChungTuPhatHanhHanMuc, errorProvider, "Kí hiệu chứng từ phát hành hạn mức", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtSoHieuPhatHanhHanMuc, errorProvider, "Số hiệu phát hành hạn mức", isOnlyWarning);
                ucMaTienTe.IsOnlyWarning = isOnlyWarning;
                ucMaTienTe.SetValidate = !isOnlyWarning;
                isValid &= ucMaTienTe.IsValidate; //"Mã tiền tệ");
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private bool SetMaxLengthControl()
        {
            bool isValid = true;

            try
            {
                Cursor = Cursors.WaitCursor;

                txtMaNganHangTraThay.MaxLength = 11;
                txtMaDonViSuDungHanMuc.MaxLength = 13;
                txtNamPhatHanh.MaxLength = 4;
                txtKiHieuChungTuPhatHanhHanMuc.MaxLength = 10;
                txtSoHieuPhatHanhHanMuc.MaxLength = 10;
                //txtMaTienTe.MaxLength = 3;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }

        private void SetAutoRemoveUnicodeAndUpperCaseControl()
        {
            txtMaNganHangTraThay.TextChanged += new EventHandler(SetTextChanged_Handler); //Số tờ khai đầu tiên
            txtMaDonViSuDungHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã loại hình
            txtNamPhatHanh.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã phân loại hàng hóa
            txtKiHieuChungTuPhatHanhHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler); //Mã hiệu phương thức vận chuyển
            txtSoHieuPhatHanhHanMuc.TextChanged += new EventHandler(SetTextChanged_Handler); //Phân loại cá nhân/tổ chức
            ucMaTienTe.TextChanged += new EventHandler(SetTextChanged_Handler); //Cơ quan Hải quan

            txtMaNganHangTraThay.CharacterCasing = CharacterCasing.Upper; //Số tờ khai đầu tiên
            txtMaDonViSuDungHanMuc.CharacterCasing = CharacterCasing.Upper; //Mã loại hình
            txtNamPhatHanh.CharacterCasing = CharacterCasing.Upper; //Mã phân loại hàng hóa
            txtKiHieuChungTuPhatHanhHanMuc.CharacterCasing = CharacterCasing.Upper; //Mã hiệu phương thức vận chuyển
            txtSoHieuPhatHanhHanMuc.CharacterCasing = CharacterCasing.Upper; //Phân loại cá nhân/tổ chức
            ucMaTienTe.IsUpperCase = true; //Cơ quan Hải quan
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region Send VNACCS

        /// <summary>
        /// Khai báo thông tin
        /// </summary>
        /// <param name="KhaiBaoSua"></param>
        private void GetIBA()
        {
            try
            {
                CTTT.InputMessageID = HelperVNACCS.NewInputMSGID();
                MessagesSend msg;
                IBA IBA;
                IBA = VNACCMaperFromObject.IBAMapper(CTTT);
                if (IBA == null)
                {
                    this.ShowMessage("Lỗi khi tạo messages !", false);
                    return;
                }
                msg = MessagesSend.Load<IBA>(IBA, CTTT.InputMessageID);
                MsgLog.SaveMessages(msg, CTTT.ID, EnumThongBao.SendMess, "");
                SendmsgVNACCFrm f = new SendmsgVNACCFrm(msg);
                f.isSend = true;
                f.isRep = false;
                f.inputMSGID = CTTT.InputMessageID;
                f.ShowDialog(this);
                if (f.DialogResult == DialogResult.Cancel)
                {
                    this.ShowMessage(f.msgFeedBack, false);
                }
                else if (f.DialogResult == DialogResult.No)
                {
                }
                else
                {
                    CTTT.InputMessageID = msg.Header.InputMessagesID.GetValue().ToString();
                    if (f.feedback.Error == null || f.feedback.Error.Count == 0)
                    {
                        if (f.feedback.ResponseData != null && f.feedback.ResponseData.Body != null)
                        {
                            CapNhatThongTin(f.feedback.ResponseData);
                            f.msgFeedBack += Environment.NewLine + "Thông tin đã được cập nhật theo phản hồi hải quan. Vui lòng kiểm tra lại";
                        }
                    }
                    this.ShowMessageTQDT(f.msgFeedBack, false);
                }
            }
            catch (Exception ex)
            {
                this.ShowMessage(ex.Message, false);
            }
        }

        #endregion

        #region Cập nhật thông tin

        private void CapNhatThongTin(ReturnMessages msgResult)
        {
            ProcessMessages.GetDataResult_ChungTuThanhToan_IBA(msgResult, "", CTTT);
            CTTT.InsertUpdateFull();
            SetCTTT();
            //setCommandStatus();
        }

        private void TuDongCapNhatThongTin()
        {
            if (CTTT != null && CTTT.SoHieuPhatHanhHanMuc != "" && CTTT.ID > 0)
            {
                List<MsgLog> listLog = new List<MsgLog>();
                IList<MsgPhanBo> listPB = MsgPhanBo.SelectCollectionDynamic(string.Format("SoTiepNhan = '{0}' And MessagesInputID = '{1}'", CTTT.SoHieuPhatHanhHanMuc.ToString(), CTTT.InputMessageID), null);
                foreach (MsgPhanBo msgPb in listPB)
                {
                    MsgLog log = MsgLog.Load(msgPb.Master_ID);
                    if (log == null)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = "Không tìm thấy log";
                        msgPb.InsertUpdate();
                    }
                    try
                    {
                        ReturnMessages msgReturn = new ReturnMessages(log.Log_Messages);
                        CapNhatThongTin(msgReturn);
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.DaXem; //Đã cập nhật thông tin
                    }
                    catch (System.Exception ex)
                    {
                        msgPb.TrangThai = EnumTrangThaiXuLyMessage.XuLyLoi; // Lỗi
                        msgPb.GhiChu = ex.Message;
                        msgPb.InsertUpdate();
                    }
                }
            }
        }

        #endregion

    }
}
