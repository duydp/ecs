﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
   public class CX_Huy_NguyenLieuSanPham:DeclarationBase
    {
       /// <summary>
       /// ngày thông báo hủy, bắt buộc, an..10
       /// </summary>
       [XmlElement("time")]
       public string Time { get; set; }
       /// <summary>
       /// Thông tin về hàng hóa xin tiêu hủy
       /// </summary>
       [XmlElement("CustomsGoodsItem")]
       public List<CustomsGoodsItem> CustomsGoodsItemL { get; set; }
    }
}
