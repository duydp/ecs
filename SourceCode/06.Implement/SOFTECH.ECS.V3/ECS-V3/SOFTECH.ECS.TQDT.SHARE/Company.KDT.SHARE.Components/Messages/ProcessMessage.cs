﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Company.KDT.SHARE.Components
{
    public abstract class ProcessMessage:IMessage
    {
        public virtual MessageInfo Send(string pass)
        {
            MessageInfo ret = new MessageInfo();
            
            return ret;
        }
        public virtual MessageInfo Request(string pass)
        {
            MessageInfo ret = new MessageInfo();

            return ret;
        }
        public virtual void Process(MessageInfo messageInfo)
        {

        }
        
    }
}
