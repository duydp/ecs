﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class SXXK_GoodsMeasure : DeclarationBase
    {
        /// <summary>
        ///  tỉ lệ chuyển đổi
        /// </summary>
        [XmlElement("conversionRate")]
        public string ConversionRate { get; set; }
        /// <summary>
        /// Số lượng(double) n..14,3
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }
        /// <summary>
        /// Khối lượng- trọng lượng n..14,3
        /// </summary>
        [XmlElement("grossMass")]
        public string GrossMass { get; set; }
        /// <summary>
        /// Mã đơn vị tính an..3
        /// </summary>
        [XmlElement("measureUnit")]
        public List<string> MeasureUnits { get; set; }
        [XmlElement("tariff")]
        public string Tariff { get; set; }

        [XmlElement("GoodsMeasure")]
        public GoodsMeasure GoodsMeasure { get; set; }

        [XmlElement("registeredMeasureUnit")]
        public string RegisteredMeasureUnit { get; set; }


        [XmlElement("MethodOfProcess")]
        public string MethodOfProcess { get; set; }

        [XmlElement("unitPrice")]
        public string UnitPrice { get; set; }

        [XmlElement("quantity1")]
        public string Quantity1 { get; set; }

        [XmlElement("quantity2")]
        public string Quantity2 { get; set; }

        [XmlElement("quantity3")]
        public string Quantity3 { get; set; }

        [XmlElement("quantity4")]
        public string Quantity4 { get; set; }

        [XmlElement("quantity5")]
        public string Quantity5 { get; set; }

        [XmlElement("quantity6")]
        public string Quantity6 { get; set; }

        [XmlElement("quantity7")]
        public string Quantity7 { get; set; }

        [XmlElement("content")]
        public string Content { get; set; }

        [XmlElement("tax")]
        public string Tax { get; set; }


    }
}
