﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Company.KDT.SHARE.VNACCS.VNACC.PhieuKho;
using Janus.Windows.GridEX;
using Company.KDT.SHARE.Components;

namespace Company.Interface.VNACCS.PhieuKho
{
    public partial class PhieuKhoManager : BaseForm
    {
        public PhieuKhoManager()
        {
            InitializeComponent();
        }

        private void btnTimKiem_Click(object sender, EventArgs e)
        {
            try
            {
                //DataTable tb = KDT_KhoCFS_DangKy.SelectDynamic(GetSearchWhere(), "").Tables[0];
                if (!String.IsNullOrEmpty(txtSoTK.Text))
                {
                    grList.Refresh();
                    grList.DataSource = KDT_KhoCFS_DangKy.SelectCollectionDynamicByTK(GetSearchWhere(), "");
                    grList.Refetch();
                }
                else
                {
                    grList.Refresh();
                    grList.DataSource = KDT_KhoCFS_DangKy.SelectCollectionDynamic(GetSearchWhere(), "");
                    grList.Refetch();
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }

        }
        private string GetSearchWhere()
        {
            try
            {
                string where = " 1=1";
                if (!String.IsNullOrEmpty(txtSoTiepNhan.Text))
                    where += " AND SoTiepNhan = " + txtSoTiepNhan.Text;
                if (!String.IsNullOrEmpty(txtNamTiepNhan.Text))
                    where += " AND YEAR(NgayTiepNhan) = " + txtNamTiepNhan.Text;
                if (cbStatus.SelectedValue != null)
                    where += " AND TrangThaiXuLy =" + cbStatus.SelectedValue;
                if (cbbMaKhoCFS.Code != null)
                {   
                    if(!String.IsNullOrEmpty(cbbMaKhoCFS.Code.ToString()))
                        where += " AND MaKhoCFS = '" + cbbMaKhoCFS.Code + "'";
                }
                if (!String.IsNullOrEmpty(txtSoTK.Text))
                    where += " AND SoToKhai =" + txtSoTK.Text;
                if (ctrCoQuanHaiQuan.Code != null)
                    if(!String.IsNullOrEmpty(ctrCoQuanHaiQuan.Code.ToString()))
                        where += " AND MaHQ = '" + ctrCoQuanHaiQuan.Code + "'";
                return where;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
                return null;
            }
        }
        private void grList_MouseDoubleClick(object sender, MouseEventArgs e)
        {

            //PhieuKho pk = new PhieuKho();
            //pk.khoCFS_ID= Int64.Parse(grList.CurrentRow.Cells[0].Value.ToString());
            //pk.Show();
        }

        private void PhieuKhoManager_Load(object sender, EventArgs e)
        {
            try
            {
                //cbStatus.SelectedIndex = 0;
                //ctrCoQuanHaiQuan.Code = GlobalSettings.MA_HAI_QUAN_VNACCS;
                //ctrCoQuanHaiQuan.BackColor = Color.Transparent;
                txtNamTiepNhan.Text = DateTime.Now.Year.ToString();
                btnTimKiem_Click(null,null);
                //DataTable tb = KDT_KhoCFS_DangKy.SelectDynamic("TrangThaiXuLy = 0", "").Tables[0];
                //grList.DataSource = tb;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }            
        }

        private void cbbTrangThai_SelectedValueChanged(object sender, EventArgs e)
        {
            try
            {
                btnTimKiem_Click(sender, e);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }
        public void ExportExcel(GridEX gridList)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách phiếu kho " + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = gridList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void grList_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            try
            {
                PhieuKho pk = new PhieuKho();
                pk.khoCFS_ID = Int64.Parse(grList.CurrentRow.Cells[0].Value.ToString());
                pk.WindowState = FormWindowState.Normal;
                pk.StartPosition = FormStartPosition.CenterParent;
                pk.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "Danh sách phiếu kho CFS_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "-") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (ShowMessage("Bạn có muốn xuất kèm theo danh sách tờ khai không", true) == "No")
                {
                    if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        gridEXExporter1.GridEX = grList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();

                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }
                else
                {
                    if (sfNPL.ShowDialog(this) == DialogResult.Yes || sfNPL.FileName != "")
                    {
                        Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                        grList.Tables[0].Columns.Add(new GridEXColumn("TKMD_ID", ColumnType.Text, EditType.NoEdit) { Caption = "TKMD_ID" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("Master_ID", ColumnType.Text, EditType.NoEdit) { Caption = "Số tham chiếu" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("SoToKhai", ColumnType.Text, EditType.NoEdit) { Caption = "Số tờ khai" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("NgayDangKy", ColumnType.Text, EditType.NoEdit) { Caption = "Ngày đăng ký" });
                        grList.Tables[0].Columns.Add(new GridEXColumn("MaLoaiHinh", ColumnType.Text, EditType.NoEdit) { Caption = "Mã loại hình" });

                        grList.DataSource = KDT_KhoCFS_DangKy.SelectDynamicFull(GetSearchWhere(), "").Tables[0];
                        gridEXExporter1.GridEX = grList;
                        System.IO.Stream str = sfNPL.OpenFile();
                        gridEXExporter1.Export(str);
                        str.Close();
                        grList.Tables[0].Columns.Remove("TKMD_ID");
                        grList.Tables[0].Columns.Remove("Master_ID");
                        grList.Tables[0].Columns.Remove("SoToKhai");
                        grList.Tables[0].Columns.Remove("NgayDangKy");
                        grList.Tables[0].Columns.Remove("MaLoaiHinh");
                        grList.Refresh();
                        btnTimKiem_Click(null, null);
                        if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                        {
                            System.Diagnostics.Process.Start(sfNPL.FileName);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //ShowMessage("Có lỗi trong quá trình xử lý :\r\n " + ex.Message, false);
            }
        }

        private void cbbMaKhoCFS_Leave(object sender, EventArgs e)
        {
            try
            {
                ctrCoQuanHaiQuan.Code = cbbMaKhoCFS.Code.ToString().Substring(0, 4);
                btnTimKiem_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void ctrCoQuanHaiQuan_Leave(object sender, EventArgs e)
        {
            btnTimKiem_Click(null,null);
        }

        private void grList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {

                if (e.Row.RowType == RowType.Record)
                {
                    if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_DUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã duyệt";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHUA_KHAI_BAO)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chưa khai báo";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHO_DUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ duyệt";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.CHO_HUY)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Chờ hủy";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_HUY)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã hủy";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.KHONG_PHE_DUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Không phê duyệt";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.SUA_KHAI_BAO)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo sửa";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.HUY_KHAI_BAO)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đã khai báo hủy";
                    else if (Convert.ToInt64(e.Row.Cells["TrangThaiXuLy"].Text) == TrangThaiXuLy.SUATKDADUYET)
                        e.Row.Cells["TrangThaiXuLy"].Text = "Đang sửa";
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = grList.SelectedItems;
                List<KDT_KhoCFS_DangKy> ItemColl = new List<KDT_KhoCFS_DangKy>();
                if (grList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessageTQDT(" Thông báo từ hệ thống ", "Doanh nghiệp có muốn xóa Danh sách Phiếu kho này không?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((KDT_KhoCFS_DangKy)i.GetRow().DataRow);
                        }

                    }
                    foreach (KDT_KhoCFS_DangKy item in ItemColl)
                    {
                        if (item.ID > 0)
                        {
                            if (item.TrangThaiXuLy ==TrangThaiXuLy.CHO_DUYET || item.TrangThaiXuLy ==TrangThaiXuLy.DA_DUYET)
                            {
                                ShowMessageTQDT(" Thông báo từ hệ thống ", "Phiếu kho đã được khai báo đến HQ .Doanh nghiệp không thể xóa phiếu kho này", true);
                            }
                            else
                            {
                                item.DeleteFull(item.ID);
                            }
                        }
                    }
                }
                btnTimKiem_Click(null,null);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                //
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
