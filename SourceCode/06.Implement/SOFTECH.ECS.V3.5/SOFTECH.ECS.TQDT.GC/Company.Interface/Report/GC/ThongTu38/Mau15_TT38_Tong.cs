﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.Interface;
using System.Data;
using Logger;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class Mau15_TT38_Tong : DevExpress.XtraReports.UI.XtraReport
    {
        public bool isLuong = false;
        public HopDong HD = new HopDong();
        public int NamDangKy;
        public DateTime NgayDauKy;
        public DateTime NgayCuoiKy;
        string MaHang = "";
        int STT = 0;
        decimal SoLuong_G_F = 0;
        decimal TriGia_G_F = 0;
        decimal TriGiaNhapTrongKy_G_F = 0;
        decimal TriGiaXuatTrongKy_G_F = 0;
        decimal TriGiaTonCuoiKy_G_F = 0;
        DateTime date = new DateTime(2015, 01, 01);
        public DataTable tb_Mau15 = new DataTable();
        public DataTable dt = new DataTable();
        public Mau15_TT38_Tong()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dtTable, bool isDaQuyetToan)
        {
            dt = dtTable.DefaultView.ToTable(true,"SoTaiKhoan","Ten", "Ma","DVT_ID", "LuongTonDau", "LuongNhap_TrongKy", "LuongXuat_TrongKy","LuongTonCuoi", "TriGiaTonDau", "TriGiaNhap_TrongKy", "TriGiaXuat_TrongKy", "TriGiaTonCuoi");
            DetailReport.DataSource = dt;
            if (isLuong)
                lblT_SoTien.Text = "Số lượng";
            lblTonDauKy_T.Text = "Tồn Đầu Kỳ (" + NgayDauKy.ToString("dd/MM/yyyy") + ")";
            lbTenToChuc.Text = GlobalSettings.TEN_DON_VI;

            lbMaSo.Text = GlobalSettings.MA_DON_VI;
            lbDiaChi.Text = GlobalSettings.DIA_CHI;
            //lbNam.Text = NamDangKy.ToString();
            this.GroupHeader3.Visible = false;
            //this.GroupHeader3.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
            //    new DevExpress.XtraReports.UI.GroupField("SoHopDong")
                
            //});
            this.GroupHeader2.Visible = true;
            this.GroupHeader2.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("SoTaiKhoan"),
                
                
            });
            this.GroupHeader1.Visible = false;
            this.GroupHeader1.GroupFields.AddRange(new DevExpress.XtraReports.UI.GroupField[] {
                new DevExpress.XtraReports.UI.GroupField("STT"),
                new DevExpress.XtraReports.UI.GroupField("MaNPL"),
                
            });
            this.GroupFooter1.Visible = false;
            //this.Detail1.Visible = false;
            //txtHopDong.DataBindings.Add("Text", DetailReport.DataSource, "SoHopDong");
            txtSoTaiKhoan_G_H_2.DataBindings.Add("Text", DetailReport.DataSource, "SoTaiKhoan");

            //lbNam.DataBindings.Add("Text",DetailReport.DataSource,"NamDangKy");
            if(isDaQuyetToan)
                txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "ID");
            else
                txtSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
            //txtTaiKhoan.DataBindings.Add("Text", DetailReport.DataSource, "SoTaiKhoan");
            txtTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "Ten");
            txtMaNPL.DataBindings.Add("Text", DetailReport.DataSource, "Ma");
            lbl_MaHang_G_H.DataBindings.Add("Text", DetailReport.DataSource, "Ma");
            txt_MaNPL_G_F.DataBindings.Add("Text", DetailReport.DataSource, "Ma");
            //txtDVT_ID.DataBindings.Add("Text", DetailReport.DataSource, "DVT_ID");
            
            if (isLuong)
            {
                txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(4,true));
                //txtTriGia.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(4,true));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongNhap_TrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongXuat_TrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonCuoi", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
            }
            else
            {
                //txtTriGia.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(4,true));
                txtTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
                txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaNhap_TrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
                txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaXuat_TrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));
                txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTonCuoi", Company.KDT.SHARE.Components.Globals.FormatNumber(4, true));

            }
            //txtTriGia.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //txtNhapTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaNhap_TrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //txtXuatTrongKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaXuat_TrongKy", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //txtTonCuoiKy.DataBindings.Add("Text", DetailReport.DataSource, "TriGiaTonCuoi", Company.KDT.SHARE.Components.Globals.FormatNumber(4));
            //txtGhiChu.DataBindings.Add("Text", DetailReport.DataSource,"GhiChu");
        }

        private void txt_SoTaiKhoan_G_H_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if(txt_SoTaiKhoan_G_H)
        }

        private void txtSTT_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //STT++;
            //txtSTT_G_F.Text = STT.ToString();
        }

        private void txt_MaNPL_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void txt_MaNPL_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void lbl_STT_G_H_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txtTenTaiKhoan_AfterPrint(object sender, EventArgs e)
        {
            if (txtSoTaiKhoan_G_H_2.Text == "152")
                txtTenTaiKhoan.Text = "Nguyên liệu , vật liệu nhập khẩu";
            else
            {
                txtTenTaiKhoan.Text = "Thanh Phẩm , vật liệu nhập khẩu";
                STT = 0;
            }
        }

        private void txtTenTaiKhoan_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (txtSoTaiKhoan_G_H_2.Text == "152")
                txtTenTaiKhoan.Text = "Nguyên liệu , vật liệu nhập khẩu";
            else
            {
                txtTenTaiKhoan.Text = "Thành phẩm được sản xuất từ nguồn nhập khẩu";
                STT = 0;
            }
        }

        private void txtNhapTrongKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //    if (Convert.ToDateTime(txtNgayDangKy.Text) < date)
            //    {
            //        txtNhapTrongKy.Text = "0";
            //    }
            //    decimal c = Convert.ToDecimal(txtNhapTrongKy.Text);
            //    txtNhapTrongKy.Text = c.ToString("N0");
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);
            //    //throw;
            //}
        }
        private void lable_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void txtNgayDangKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //    DateTime d = Convert.ToDateTime(txtNgayDangKy.Text);
            //    txtNgayDangKy.Text = d.ToString("dd/MM/yyyy");
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);
            //    //throw;
            //}

        }

        private void txtSoLuong_G_F_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{


            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}

        }

        private void txtSoLuong_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //    if(SoLuong_G_F==0)
            //        txtSoLuong_G_F.Text = "0";
            //    else
            //        txtSoLuong_G_F.Text = SoLuong_G_F.ToString("N4");
            //    SoLuong_G_F = 0;
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtTonDauKy_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    SoLuong_G_F += Convert.ToDecimal(txtTonDauKy.Text);
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtTriGia_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    TriGia_G_F += Convert.ToDecimal(txtTriGia.Text);
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }
        private void txtTriGiaTonDauKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //    if (TriGia_G_F==0)
            //        txtTriGiaTonDauKy.Text = "0";
            //    else
            //        txtTriGiaTonDauKy.Text = TriGia_G_F.ToString("N0");
            //    TriGia_G_F = 0;
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }
        private void txtTriGiaTonDauKy_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txtTriGiaNhapTrongKy_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    TriGiaNhapTrongKy_G_F += Convert.ToDecimal(txtTriGiaNhapTrongKy.Text);
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtTriGiaNhapTrongKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //    if (TriGiaNhapTrongKy_G_F==0)
            //        txtTriGiaNhapTrongKy.Text = "0";
            //    else
            //        txtTriGiaNhapTrongKy.Text = TriGiaNhapTrongKy_G_F.ToString("N0");
            //    TriGiaNhapTrongKy_G_F = 0;
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtTriGiaXuatTrongKy_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //    if (TriGiaXuatTrongKy_G_F==0)
            //        txtTriGiaXuatTrongKy_G_F.Text = "0";
            //    else
            //        txtTriGiaXuatTrongKy_G_F.Text = TriGiaXuatTrongKy_G_F.ToString("N0");
            //        //txtTriGiaXuatTrongKy_G_F.Text = TriGiaXuatTrongKy_G_F.ToString();
            //    TriGiaXuatTrongKy_G_F = 0;
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtTriGiaXuatTrongKy_G_F_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    txtTriGiaXuatTrongKy_G_F.Text = TriGiaXuatTrongKy_G_F.ToString("N4");
            //    TriGiaXuatTrongKy_G_F = 0;
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtTriGiaTonCuoiKy_G_F_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //    if (TriGiaTonCuoiKy_G_F==0)
            //        txtTriGiaTonCuoiKy_G_F.Text = "0";
            //    else
            //        txtTriGiaTonCuoiKy_G_F.Text = TriGiaTonCuoiKy_G_F.ToString("N0");
            //    TriGiaTonCuoiKy_G_F = 0;
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtTriGiaTonCuoiKy_G_F_AfterPrint(object sender, EventArgs e)
        {

        }

        private void txtTonCuoiKy_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    TriGiaTonCuoiKy_G_F += Convert.ToDecimal(txtTonCuoiKy.Text);
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtNhapTrongKy_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    TriGiaNhapTrongKy_G_F += Convert.ToDecimal(txtNhapTrongKy.Text);
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);
            //}
        }

        private void txtXuatTrongKy_AfterPrint(object sender, EventArgs e)
        {
            //try
            //{
            //    TriGiaXuatTrongKy_G_F += Convert.ToDecimal(txtXuatTrongKy.Text);
            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);

            //}
        }

        private void txtDonGia_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //try
            //{
            //    if (string.IsNullOrEmpty(txtDonGia.Text)) 
            //    {
            //        decimal c = Convert.ToDecimal(txtDonGia.Text);
            //        if (c == 0)
            //        {
            //            txtDonGia.Text = "0";
            //        }
            //        else
            //            txtDonGia.Text = c.ToString("N6");
            //    }

            //}
            //catch (Exception ex)
            //{
            //    LocalLogger.Instance().WriteMessage(ex);
            //    //throw;
            //}

        }

        private void txtTonDauKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (Convert.ToDateTime(txtNgayDangKy.Text) > date) 
            //{
            //    txtTonDauKy.Text = "0";
            //}
        }

        private void txtTriGia_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (Convert.ToDateTime(txtNgayDangKy.Text) > date)
            //{
            //    txtTriGia.Text = "0";
            //}
        }

        private void txtNhapTrongKy_BeforePrint_1(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void txtXuatTrongKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (Convert.ToDateTime(txtNgayDangKy.Text) < date)
            //{
            //    txtXuatTrongKy.Text = "0";
            //}
        }

        private void txtTonCuoiKy_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (Convert.ToDateTime(txtNgayDangKy.Text) < date)
            //{
            //    txtTonCuoiKy.Text = "0";
            //}
        }

        private void txtHopDong_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
           // txtHopDong.Text = "Số hợp đồng: " + txtHopDong.Text;
        }

        private void txtMaLoaiHinh_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            //if (txtMaLoaiHinh.Text.Contains("V")) 
            //{
            //    try
            //    {
            //        txtMaLoaiHinh.Text = txtMaLoaiHinh.Text.Substring(2, 3);
            //    }
            //    catch (Exception ex)
            //    {

            //        //throw;
            //    }

            //}
        }

    }
}
