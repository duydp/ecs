using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.Components.Common
{
	public partial class GiamSatTieuHuyBoSung : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long GSTH_ID { set; get; }
		public string SoThamChieuPLHD { set; get; }
		public string SoDangKyPLHD { set; get; }
		public DateTime NgayDangKyPLHD { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<GiamSatTieuHuyBoSung> ConvertToCollection(IDataReader reader)
		{
			IList<GiamSatTieuHuyBoSung> collection = new List<GiamSatTieuHuyBoSung>();
			while (reader.Read())
			{
				GiamSatTieuHuyBoSung entity = new GiamSatTieuHuyBoSung();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("GSTH_ID"))) entity.GSTH_ID = reader.GetInt64(reader.GetOrdinal("GSTH_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoThamChieuPLHD"))) entity.SoThamChieuPLHD = reader.GetString(reader.GetOrdinal("SoThamChieuPLHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDangKyPLHD"))) entity.SoDangKyPLHD = reader.GetString(reader.GetOrdinal("SoDangKyPLHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKyPLHD"))) entity.NgayDangKyPLHD = reader.GetDateTime(reader.GetOrdinal("NgayDangKyPLHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<GiamSatTieuHuyBoSung> collection, long id)
        {
            foreach (GiamSatTieuHuyBoSung item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_GiamSatTieuHuyBoSung VALUES(@GSTH_ID, @SoThamChieuPLHD, @SoDangKyPLHD, @NgayDangKyPLHD, @GhiChu)";
            string update = "UPDATE t_KDT_GC_GiamSatTieuHuyBoSung SET GSTH_ID = @GSTH_ID, SoThamChieuPLHD = @SoThamChieuPLHD, SoDangKyPLHD = @SoDangKyPLHD, NgayDangKyPLHD = @NgayDangKyPLHD, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_GiamSatTieuHuyBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GSTH_ID", SqlDbType.BigInt, "GSTH_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThamChieuPLHD", SqlDbType.VarChar, "SoThamChieuPLHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKyPLHD", SqlDbType.VarChar, "SoDangKyPLHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyPLHD", SqlDbType.DateTime, "NgayDangKyPLHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GSTH_ID", SqlDbType.BigInt, "GSTH_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThamChieuPLHD", SqlDbType.VarChar, "SoThamChieuPLHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKyPLHD", SqlDbType.VarChar, "SoDangKyPLHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyPLHD", SqlDbType.DateTime, "NgayDangKyPLHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_GiamSatTieuHuyBoSung VALUES(@GSTH_ID, @SoThamChieuPLHD, @SoDangKyPLHD, @NgayDangKyPLHD, @GhiChu)";
            string update = "UPDATE t_KDT_GC_GiamSatTieuHuyBoSung SET GSTH_ID = @GSTH_ID, SoThamChieuPLHD = @SoThamChieuPLHD, SoDangKyPLHD = @SoDangKyPLHD, NgayDangKyPLHD = @NgayDangKyPLHD, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_GiamSatTieuHuyBoSung WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GSTH_ID", SqlDbType.BigInt, "GSTH_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoThamChieuPLHD", SqlDbType.VarChar, "SoThamChieuPLHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDangKyPLHD", SqlDbType.VarChar, "SoDangKyPLHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKyPLHD", SqlDbType.DateTime, "NgayDangKyPLHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GSTH_ID", SqlDbType.BigInt, "GSTH_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoThamChieuPLHD", SqlDbType.VarChar, "SoThamChieuPLHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDangKyPLHD", SqlDbType.VarChar, "SoDangKyPLHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKyPLHD", SqlDbType.DateTime, "NgayDangKyPLHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static GiamSatTieuHuyBoSung Load(long id)
		{
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<GiamSatTieuHuyBoSung> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<GiamSatTieuHuyBoSung> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<GiamSatTieuHuyBoSung> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<GiamSatTieuHuyBoSung> SelectCollectionBy_GSTH_ID(long gSTH_ID)
		{
            IDataReader reader = SelectReaderBy_GSTH_ID(gSTH_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_GSTH_ID(long gSTH_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectBy_GSTH_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GSTH_ID", SqlDbType.BigInt, gSTH_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_GSTH_ID(long gSTH_ID)
		{
			const string spName = "p_KDT_GC_GiamSatTieuHuyBoSung_SelectBy_GSTH_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GSTH_ID", SqlDbType.BigInt, gSTH_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertGiamSatTieuHuyBoSung(long gSTH_ID, string soThamChieuPLHD, string soDangKyPLHD, DateTime ngayDangKyPLHD, string ghiChu)
		{
			GiamSatTieuHuyBoSung entity = new GiamSatTieuHuyBoSung();	
			entity.GSTH_ID = gSTH_ID;
			entity.SoThamChieuPLHD = soThamChieuPLHD;
			entity.SoDangKyPLHD = soDangKyPLHD;
			entity.NgayDangKyPLHD = ngayDangKyPLHD;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@GSTH_ID", SqlDbType.BigInt, GSTH_ID);
			db.AddInParameter(dbCommand, "@SoThamChieuPLHD", SqlDbType.VarChar, SoThamChieuPLHD);
			db.AddInParameter(dbCommand, "@SoDangKyPLHD", SqlDbType.VarChar, SoDangKyPLHD);
			db.AddInParameter(dbCommand, "@NgayDangKyPLHD", SqlDbType.DateTime, NgayDangKyPLHD.Year <= 1753 ? DBNull.Value : (object) NgayDangKyPLHD);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<GiamSatTieuHuyBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiamSatTieuHuyBoSung item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateGiamSatTieuHuyBoSung(long id, long gSTH_ID, string soThamChieuPLHD, string soDangKyPLHD, DateTime ngayDangKyPLHD, string ghiChu)
		{
			GiamSatTieuHuyBoSung entity = new GiamSatTieuHuyBoSung();			
			entity.ID = id;
			entity.GSTH_ID = gSTH_ID;
			entity.SoThamChieuPLHD = soThamChieuPLHD;
			entity.SoDangKyPLHD = soDangKyPLHD;
			entity.NgayDangKyPLHD = ngayDangKyPLHD;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_GC_GiamSatTieuHuyBoSung_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GSTH_ID", SqlDbType.BigInt, GSTH_ID);
			db.AddInParameter(dbCommand, "@SoThamChieuPLHD", SqlDbType.VarChar, SoThamChieuPLHD);
			db.AddInParameter(dbCommand, "@SoDangKyPLHD", SqlDbType.VarChar, SoDangKyPLHD);
			db.AddInParameter(dbCommand, "@NgayDangKyPLHD", SqlDbType.DateTime, NgayDangKyPLHD.Year <= 1753 ? DBNull.Value : (object) NgayDangKyPLHD);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<GiamSatTieuHuyBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiamSatTieuHuyBoSung item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateGiamSatTieuHuyBoSung(long id, long gSTH_ID, string soThamChieuPLHD, string soDangKyPLHD, DateTime ngayDangKyPLHD, string ghiChu)
		{
			GiamSatTieuHuyBoSung entity = new GiamSatTieuHuyBoSung();			
			entity.ID = id;
			entity.GSTH_ID = gSTH_ID;
			entity.SoThamChieuPLHD = soThamChieuPLHD;
			entity.SoDangKyPLHD = soDangKyPLHD;
			entity.NgayDangKyPLHD = ngayDangKyPLHD;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@GSTH_ID", SqlDbType.BigInt, GSTH_ID);
			db.AddInParameter(dbCommand, "@SoThamChieuPLHD", SqlDbType.VarChar, SoThamChieuPLHD);
			db.AddInParameter(dbCommand, "@SoDangKyPLHD", SqlDbType.VarChar, SoDangKyPLHD);
			db.AddInParameter(dbCommand, "@NgayDangKyPLHD", SqlDbType.DateTime, NgayDangKyPLHD.Year <= 1753 ? DBNull.Value : (object) NgayDangKyPLHD);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<GiamSatTieuHuyBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiamSatTieuHuyBoSung item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteGiamSatTieuHuyBoSung(long id)
		{
			GiamSatTieuHuyBoSung entity = new GiamSatTieuHuyBoSung();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_GSTH_ID(long gSTH_ID)
		{
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_DeleteBy_GSTH_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@GSTH_ID", SqlDbType.BigInt, gSTH_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_GC_GiamSatTieuHuyBoSung_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<GiamSatTieuHuyBoSung> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (GiamSatTieuHuyBoSung item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}