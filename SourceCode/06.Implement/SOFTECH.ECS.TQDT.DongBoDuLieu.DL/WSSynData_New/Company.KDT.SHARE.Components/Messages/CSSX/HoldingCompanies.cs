﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components.Messages.CSSX
{
   public class HoldingCompanies
    {
        /// <summary>
        /// Số lượng thành viên
        /// </summary>
        [XmlElement("quantity")]
        public string Quantity { get; set; }

        /// <summary>
        /// Công ty mẹ nhập khẩu, cung ứng nguyên liệu, vật tư để gia công, sản xuất xuất khẩu cho các đơn vị thành viên trực thuộc
        /// </summary>
       [XmlElement("HoldingCompany")]
       public List<HoldingCompany> HoldingCompany { get; set; }
    }
}
