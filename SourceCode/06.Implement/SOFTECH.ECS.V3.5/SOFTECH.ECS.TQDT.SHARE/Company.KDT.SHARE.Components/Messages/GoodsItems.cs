﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Company.KDT.SHARE.Components.Messages.GoodsItem;

namespace Company.KDT.SHARE.Components.Messages
{
  public  class GoodsItems
    {
        /// <summary>
        /// Thông tin về danh sách NPL trong báo cáo
        /// </summary>
      [XmlElement("GoodsItem")]
      public List<Company.KDT.SHARE.Components.GoodsItem> GoodsItem { get; set; }

      /// <summary>
      /// Chi tiết hàng
      /// </summary>
      [XmlElement("Commodity")]
      public Commodity Commodity { get; set; }

      /// <summary>
      /// Thông tin lượng
      /// </summary>
      [XmlElement("GoodsMeasure")]
      public GoodsMeasure GoodsMeasure { get; set; }

      /// <summary>
      /// Thông tin lượng
      /// </summary>
      [XmlElement("QuantityForward")]
      public QuantityForward QuantityForward { get; set; }

      /// <summary>
      /// AttachedFile
      /// </summary>
      [XmlElement("AttachedFile")]
      public Company.KDT.SHARE.Components.AttachedFile AttachedFile { get; set; }
    }
}
