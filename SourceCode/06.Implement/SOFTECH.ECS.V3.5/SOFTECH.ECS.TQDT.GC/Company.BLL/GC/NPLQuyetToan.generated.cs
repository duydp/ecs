﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
    public partial class NPLQuyetToan : ICloneable
    {
        #region Properties.

        public long ID { set; get; }
        public int HopDong_ID { set; get; }
        public string SoHopDong { set; get; }
        public decimal SoToKhai { set; get; }
        public string MaLoaiHinh { set; get; }
        public DateTime NgayDangKy { set; get; }
        public string MaHang { set; get; }
        public string TenHang { set; get; }
        public decimal SoLuong { set; get; }
        public string MaNguyenPhuLieu { set; get; }
        public decimal DinhMucSuDung { set; get; }
        public decimal TyLeHaoHut { set; get; }
        public decimal LuongNPLChuaHH { set; get; }
        public decimal LuongNPLCoHH { set; get; }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Methods
        protected static List<NPLQuyetToan> ConvertToCollection(IDataReader reader)
        {
            List<NPLQuyetToan> collection = new List<NPLQuyetToan>();
            while (reader.Read())
            {
                NPLQuyetToan entity = new NPLQuyetToan();
                if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("HopDong_ID"))) entity.HopDong_ID = reader.GetInt32(reader.GetOrdinal("HopDong_ID"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoHopDong"))) entity.SoHopDong = reader.GetString(reader.GetOrdinal("SoHopDong"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
                if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaHang"))) entity.MaHang = reader.GetString(reader.GetOrdinal("MaHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenHang"))) entity.TenHang = reader.GetString(reader.GetOrdinal("TenHang"));
                if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
                if (!reader.IsDBNull(reader.GetOrdinal("MaNguyenPhuLieu"))) entity.MaNguyenPhuLieu = reader.GetString(reader.GetOrdinal("MaNguyenPhuLieu"));
                if (!reader.IsDBNull(reader.GetOrdinal("DinhMucSuDung"))) entity.DinhMucSuDung = reader.GetDecimal(reader.GetOrdinal("DinhMucSuDung"));
                if (!reader.IsDBNull(reader.GetOrdinal("TyLeHaoHut"))) entity.TyLeHaoHut = reader.GetDecimal(reader.GetOrdinal("TyLeHaoHut"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLChuaHH"))) entity.LuongNPLChuaHH = reader.GetDecimal(reader.GetOrdinal("LuongNPLChuaHH"));
                if (!reader.IsDBNull(reader.GetOrdinal("LuongNPLCoHH"))) entity.LuongNPLCoHH = reader.GetDecimal(reader.GetOrdinal("LuongNPLCoHH"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static bool Find(List<NPLQuyetToan> collection, long id)
        {
            foreach (NPLQuyetToan item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }

        public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_NPLQuyetToan VALUES(@HopDong_ID, @SoHopDong, @SoToKhai, @MaLoaiHinh, @NgayDangKy, @MaHang, @TenHang, @SoLuong, @MaNguyenPhuLieu, @DinhMucSuDung, @TyLeHaoHut, @LuongNPLChuaHH, @LuongNPLCoHH)";
            string update = "UPDATE t_KDT_GC_NPLQuyetToan SET HopDong_ID = @HopDong_ID, SoHopDong = @SoHopDong, SoToKhai = @SoToKhai, MaLoaiHinh = @MaLoaiHinh, NgayDangKy = @NgayDangKy, MaHang = @MaHang, TenHang = @TenHang, SoLuong = @SoLuong, MaNguyenPhuLieu = @MaNguyenPhuLieu, DinhMucSuDung = @DinhMucSuDung, TyLeHaoHut = @TyLeHaoHut, LuongNPLChuaHH = @LuongNPLChuaHH, LuongNPLCoHH = @LuongNPLCoHH WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_NPLQuyetToan WHERE ID = @ID";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.Int, "HopDong_ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@LuongNPLChuaHH", SqlDbType.Decimal, "LuongNPLChuaHH", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@LuongNPLCoHH", SqlDbType.Decimal, "LuongNPLCoHH", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.Int, "HopDong_ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@LuongNPLChuaHH", SqlDbType.Decimal, "LuongNPLChuaHH", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@LuongNPLCoHH", SqlDbType.Decimal, "LuongNPLCoHH", DataRowVersion.Current);

            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

        public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_NPLQuyetToan VALUES(@HopDong_ID, @SoHopDong, @SoToKhai, @MaLoaiHinh, @NgayDangKy, @MaHang, @TenHang, @SoLuong, @MaNguyenPhuLieu, @DinhMucSuDung, @TyLeHaoHut, @LuongNPLChuaHH, @LuongNPLCoHH)";
            string update = "UPDATE t_KDT_GC_NPLQuyetToan SET HopDong_ID = @HopDong_ID, SoHopDong = @SoHopDong, SoToKhai = @SoToKhai, MaLoaiHinh = @MaLoaiHinh, NgayDangKy = @NgayDangKy, MaHang = @MaHang, TenHang = @TenHang, SoLuong = @SoLuong, MaNguyenPhuLieu = @MaNguyenPhuLieu, DinhMucSuDung = @DinhMucSuDung, TyLeHaoHut = @TyLeHaoHut, LuongNPLChuaHH = @LuongNPLChuaHH, LuongNPLCoHH = @LuongNPLCoHH WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_NPLQuyetToan WHERE ID = @ID";

            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
            db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@HopDong_ID", SqlDbType.Int, "HopDong_ID", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@LuongNPLChuaHH", SqlDbType.Decimal, "LuongNPLChuaHH", DataRowVersion.Current);
            db.AddInParameter(InsertCommand, "@LuongNPLCoHH", SqlDbType.Decimal, "LuongNPLCoHH", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
            db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@HopDong_ID", SqlDbType.Int, "HopDong_ID", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoHopDong", SqlDbType.VarChar, "SoHopDong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaHang", SqlDbType.VarChar, "MaHang", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TenHang", SqlDbType.NVarChar, "TenHang", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, "MaNguyenPhuLieu", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@DinhMucSuDung", SqlDbType.Decimal, "DinhMucSuDung", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@TyLeHaoHut", SqlDbType.Decimal, "TyLeHaoHut", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@LuongNPLChuaHH", SqlDbType.Decimal, "LuongNPLChuaHH", DataRowVersion.Current);
            db.AddInParameter(UpdateCommand, "@LuongNPLCoHH", SqlDbType.Decimal, "LuongNPLCoHH", DataRowVersion.Current);

            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
            db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Select methods.

        public static NPLQuyetToan Load(long id)
        {
            const string spName = "[dbo].[p_NPLQuyetToan_Load]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
            List<NPLQuyetToan> collection = ConvertToCollection(reader);
            if (collection.Count > 0)
            {
                return collection[0];
            }
            return null;
        }

        //---------------------------------------------------------------------------------------------


        public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_NPLQuyetToan_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);


            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_NPLQuyetToan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteDataSet(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_NPLQuyetToan_SelectAll]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
        {
            const string spName = "[dbo].[p_NPLQuyetToan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);

            return db.ExecuteReader(dbCommand);
        }

        //---------------------------------------------------------------------------------------------

        // Select by foreign key return collection		

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert methods.

        public static long InsertNPLQuyetToan(int hopDong_ID, string soHopDong, decimal soToKhai, string maLoaiHinh, DateTime ngayDangKy, string maHang, string tenHang, decimal soLuong, string maNguyenPhuLieu, decimal dinhMucSuDung, decimal tyLeHaoHut, decimal luongNPLChuaHH, decimal luongNPLCoHH)
        {
            NPLQuyetToan entity = new NPLQuyetToan();
            entity.HopDong_ID = hopDong_ID;
            entity.SoHopDong = soHopDong;
            entity.SoToKhai = soToKhai;
            entity.MaLoaiHinh = maLoaiHinh;
            entity.NgayDangKy = ngayDangKy;
            entity.MaHang = maHang;
            entity.TenHang = tenHang;
            entity.SoLuong = soLuong;
            entity.MaNguyenPhuLieu = maNguyenPhuLieu;
            entity.DinhMucSuDung = dinhMucSuDung;
            entity.TyLeHaoHut = tyLeHaoHut;
            entity.LuongNPLChuaHH = luongNPLChuaHH;
            entity.LuongNPLCoHH = luongNPLCoHH;
            return entity.Insert();
        }

        public long Insert()
        {
            return this.Insert(null);
        }

        //---------------------------------------------------------------------------------------------

        public long Insert(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_NPLQuyetToan_Insert]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.Int, HopDong_ID);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object)NgayDangKy);
            db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNguyenPhuLieu);
            db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
            db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
            db.AddInParameter(dbCommand, "@LuongNPLChuaHH", SqlDbType.Decimal, LuongNPLChuaHH);
            db.AddInParameter(dbCommand, "@LuongNPLCoHH", SqlDbType.Decimal, LuongNPLCoHH);

            if (transaction != null)
            {
                db.ExecuteNonQuery(dbCommand, transaction);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
            else
            {
                db.ExecuteNonQuery(dbCommand);
                ID = (long)db.GetParameterValue(dbCommand, "@ID");
                return ID;
            }
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Insert / Update methods.

        public static int InsertUpdateNPLQuyetToan(long id, int hopDong_ID, string soHopDong, decimal soToKhai, string maLoaiHinh, DateTime ngayDangKy, string maHang, string tenHang, decimal soLuong, string maNguyenPhuLieu, decimal dinhMucSuDung, decimal tyLeHaoHut, decimal luongNPLChuaHH, decimal luongNPLCoHH)
        {
            NPLQuyetToan entity = new NPLQuyetToan();
            entity.ID = id;
            entity.HopDong_ID = hopDong_ID;
            entity.SoHopDong = soHopDong;
            entity.SoToKhai = soToKhai;
            entity.MaLoaiHinh = maLoaiHinh;
            entity.NgayDangKy = ngayDangKy;
            entity.MaHang = maHang;
            entity.TenHang = tenHang;
            entity.SoLuong = soLuong;
            entity.MaNguyenPhuLieu = maNguyenPhuLieu;
            entity.DinhMucSuDung = dinhMucSuDung;
            entity.TyLeHaoHut = tyLeHaoHut;
            entity.LuongNPLChuaHH = luongNPLChuaHH;
            entity.LuongNPLCoHH = luongNPLCoHH;
            return entity.InsertUpdate();
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate()
        {
            return this.InsertUpdate(null);
        }

        //---------------------------------------------------------------------------------------------

        public int InsertUpdate(SqlTransaction transaction)
        {
            const string spName = "p_NPLQuyetToan_InsertUpdate";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.Int, HopDong_ID);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object)NgayDangKy);
            db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNguyenPhuLieu);
            db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
            db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
            db.AddInParameter(dbCommand, "@LuongNPLChuaHH", SqlDbType.Decimal, LuongNPLChuaHH);
            db.AddInParameter(dbCommand, "@LuongNPLCoHH", SqlDbType.Decimal, LuongNPLCoHH);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        #endregion

        //---------------------------------------------------------------------------------------------

        #region Update methods.

        public static int UpdateNPLQuyetToan(long id, int hopDong_ID, string soHopDong, decimal soToKhai, string maLoaiHinh, DateTime ngayDangKy, string maHang, string tenHang, decimal soLuong, string maNguyenPhuLieu, decimal dinhMucSuDung, decimal tyLeHaoHut, decimal luongNPLChuaHH, decimal luongNPLCoHH)
        {
            NPLQuyetToan entity = new NPLQuyetToan();
            entity.ID = id;
            entity.HopDong_ID = hopDong_ID;
            entity.SoHopDong = soHopDong;
            entity.SoToKhai = soToKhai;
            entity.MaLoaiHinh = maLoaiHinh;
            entity.NgayDangKy = ngayDangKy;
            entity.MaHang = maHang;
            entity.TenHang = tenHang;
            entity.SoLuong = soLuong;
            entity.MaNguyenPhuLieu = maNguyenPhuLieu;
            entity.DinhMucSuDung = dinhMucSuDung;
            entity.TyLeHaoHut = tyLeHaoHut;
            entity.LuongNPLChuaHH = luongNPLChuaHH;
            entity.LuongNPLCoHH = luongNPLCoHH;
            return entity.Update();
        }

        //---------------------------------------------------------------------------------------------

        public int Update()
        {
            return this.Update(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Update(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_NPLQuyetToan_Update]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
            db.AddInParameter(dbCommand, "@HopDong_ID", SqlDbType.Int, HopDong_ID);
            db.AddInParameter(dbCommand, "@SoHopDong", SqlDbType.VarChar, SoHopDong);
            db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
            db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
            db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object)NgayDangKy);
            db.AddInParameter(dbCommand, "@MaHang", SqlDbType.VarChar, MaHang);
            db.AddInParameter(dbCommand, "@TenHang", SqlDbType.NVarChar, TenHang);
            db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
            db.AddInParameter(dbCommand, "@MaNguyenPhuLieu", SqlDbType.VarChar, MaNguyenPhuLieu);
            db.AddInParameter(dbCommand, "@DinhMucSuDung", SqlDbType.Decimal, DinhMucSuDung);
            db.AddInParameter(dbCommand, "@TyLeHaoHut", SqlDbType.Decimal, TyLeHaoHut);
            db.AddInParameter(dbCommand, "@LuongNPLChuaHH", SqlDbType.Decimal, LuongNPLChuaHH);
            db.AddInParameter(dbCommand, "@LuongNPLCoHH", SqlDbType.Decimal, LuongNPLCoHH);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }


        #endregion

        //---------------------------------------------------------------------------------------------

        #region Delete methods.

        public static int DeleteNPLQuyetToan(long id)
        {
            NPLQuyetToan entity = new NPLQuyetToan();
            entity.ID = id;

            return entity.Delete();
        }

        public int Delete()
        {
            return this.Delete(null);
        }

        //---------------------------------------------------------------------------------------------

        public int Delete(SqlTransaction transaction)
        {
            const string spName = "[dbo].[p_NPLQuyetToan_Delete]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);

            if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
        }

        //---------------------------------------------------------------------------------------------


        public static int DeleteDynamic(string whereCondition)
        {
            const string spName = "[dbo].[p_NPLQuyetToan_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);

            db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);

            return db.ExecuteNonQuery(dbCommand);
        }
        //---------------------------------------------------------------------------------------------

        #endregion


        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
    }
}