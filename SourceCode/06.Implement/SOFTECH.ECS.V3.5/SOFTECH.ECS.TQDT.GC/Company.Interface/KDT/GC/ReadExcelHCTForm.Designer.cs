namespace Company.Interface.KDT.GC
{
    partial class ReadExcelHCTForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadExcelHCTForm));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSheet = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtXuatXuColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuongColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDonGiaColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSelectFile = new Janus.Windows.EditControls.UIButton();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.uiCheckBox1 = new Janus.Windows.EditControls.UICheckBox();
            this.rfvFilePath = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSheet = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDonGia = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoLuong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.requiredFieldValidator4 = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.lblLinkExcel = new System.Windows.Forms.LinkLabel();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDonGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.requiredFieldValidator4)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.lblLinkExcel);
            this.grbMain.Controls.Add(this.uiCheckBox1);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Controls.Add(this.btnClose);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(537, 178);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(174, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 13);
            this.label2.TabIndex = 188;
            this.label2.Text = "Cột mã hàng";
            // 
            // txtRow
            // 
            this.txtRow.DecimalDigits = 0;
            this.txtRow.Location = new System.Drawing.Point(87, 40);
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(79, 21);
            this.txtRow.TabIndex = 196;
            this.txtRow.Text = "2";
            this.txtRow.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.txtRow.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 199;
            this.label6.Text = "Tên Sheet";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(258, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 201;
            this.label7.Text = "Cột số lượng";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(348, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 13);
            this.label8.TabIndex = 202;
            this.label8.Text = "Cột đơn giá";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(436, 24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 13);
            this.label9.TabIndex = 203;
            this.label9.Text = "Cột xuất xứ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(84, 24);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 13);
            this.label11.TabIndex = 208;
            this.label11.Text = "Dòng bắt đầu";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtSheet);
            this.uiGroupBox1.Controls.Add(this.txtXuatXuColumn);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongColumn);
            this.uiGroupBox1.Controls.Add(this.txtDonGiaColumn);
            this.uiGroupBox1.Controls.Add(this.txtMaHangColumn);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label9);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label11);
            this.uiGroupBox1.Controls.Add(this.txtRow);
            this.uiGroupBox1.Controls.Add(this.label8);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(515, 74);
            this.uiGroupBox1.TabIndex = 212;
            this.uiGroupBox1.Text = "Thông số cấu hình";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtSheet
            // 
            this.txtSheet.Location = new System.Drawing.Point(13, 40);
            this.txtSheet.Name = "txtSheet";
            this.txtSheet.Size = new System.Drawing.Size(61, 21);
            this.txtSheet.TabIndex = 217;
            this.txtSheet.VisualStyleManager = this.vsmMain;
            // 
            // txtXuatXuColumn
            // 
            this.txtXuatXuColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtXuatXuColumn.Location = new System.Drawing.Point(436, 40);
            this.txtXuatXuColumn.Name = "txtXuatXuColumn";
            this.txtXuatXuColumn.Size = new System.Drawing.Size(75, 21);
            this.txtXuatXuColumn.TabIndex = 216;
            this.txtXuatXuColumn.Text = "D";
            this.txtXuatXuColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtSoLuongColumn
            // 
            this.txtSoLuongColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuongColumn.Location = new System.Drawing.Point(261, 40);
            this.txtSoLuongColumn.Name = "txtSoLuongColumn";
            this.txtSoLuongColumn.Size = new System.Drawing.Size(79, 21);
            this.txtSoLuongColumn.TabIndex = 214;
            this.txtSoLuongColumn.Text = "B";
            this.txtSoLuongColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtDonGiaColumn
            // 
            this.txtDonGiaColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonGiaColumn.Location = new System.Drawing.Point(351, 40);
            this.txtDonGiaColumn.Name = "txtDonGiaColumn";
            this.txtDonGiaColumn.Size = new System.Drawing.Size(75, 21);
            this.txtDonGiaColumn.TabIndex = 212;
            this.txtDonGiaColumn.Text = "C";
            this.txtDonGiaColumn.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHangColumn
            // 
            this.txtMaHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHangColumn.Location = new System.Drawing.Point(177, 40);
            this.txtMaHangColumn.Name = "txtMaHangColumn";
            this.txtMaHangColumn.Size = new System.Drawing.Size(75, 21);
            this.txtMaHangColumn.TabIndex = 209;
            this.txtMaHangColumn.Text = "A";
            this.txtMaHangColumn.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnSelectFile);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(11, 94);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(516, 51);
            this.uiGroupBox2.TabIndex = 213;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFile.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSelectFile.Icon")));
            this.btnSelectFile.Location = new System.Drawing.Point(435, 18);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(75, 23);
            this.btnSelectFile.TabIndex = 189;
            this.btnSelectFile.Text = "Browe";
            this.btnSelectFile.VisualStyleManager = this.vsmMain;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // txtFilePath
            // 
            this.txtFilePath.Location = new System.Drawing.Point(13, 20);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(416, 21);
            this.txtFilePath.TabIndex = 188;
            this.txtFilePath.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(459, 153);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(68, 23);
            this.btnClose.TabIndex = 214;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(385, 153);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(68, 23);
            this.btnSave.TabIndex = 215;
            this.btnSave.Text = "Ghi";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // uiCheckBox1
            // 
            this.uiCheckBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uiCheckBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiCheckBox1.Checked = true;
            this.uiCheckBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.uiCheckBox1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiCheckBox1.Location = new System.Drawing.Point(160, 154);
            this.uiCheckBox1.Name = "uiCheckBox1";
            this.uiCheckBox1.Size = new System.Drawing.Size(104, 23);
            this.uiCheckBox1.TabIndex = 216;
            this.uiCheckBox1.Text = "Font Unicode";
            this.uiCheckBox1.VisualStyleManager = this.vsmMain;
            // 
            // rfvFilePath
            // 
            this.rfvFilePath.ControlToValidate = this.txtFilePath;
            this.rfvFilePath.ErrorMessage = "\"Tên đường dẫn file\" không được rỗng";
            this.rfvFilePath.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvFilePath.Icon")));
            this.rfvFilePath.Tag = "rfvFilePath";
            // 
            // cvError
            // 
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaHangColumn;
            this.rfvMa.ErrorMessage = "\"Cột mã hàng\" bắt buộc phải nhập.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            this.rfvMa.Tag = "rfvMa";
            // 
            // rfvTenSheet
            // 
            this.rfvTenSheet.ControlToValidate = this.txtSheet;
            this.rfvTenSheet.ErrorMessage = "\"Tên sheet\" bắt buộc phải nhập.";
            this.rfvTenSheet.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSheet.Icon")));
            this.rfvTenSheet.Tag = "rfvTenSheet";
            // 
            // rfvDonGia
            // 
            this.rfvDonGia.ControlToValidate = this.txtDonGiaColumn;
            this.rfvDonGia.ErrorMessage = "\"Cột đơn giá\" bắt buộc phải nhập.";
            this.rfvDonGia.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDonGia.Icon")));
            this.rfvDonGia.Tag = "rfvDonGia";
            // 
            // rfvSoLuong
            // 
            this.rfvSoLuong.ControlToValidate = this.txtSoLuongColumn;
            this.rfvSoLuong.ErrorMessage = "\"Cột số lượng\" bắt buộc phải nhập.";
            this.rfvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoLuong.Icon")));
            this.rfvSoLuong.Tag = "rfvSoLuong";
            // 
            // requiredFieldValidator4
            // 
            this.requiredFieldValidator4.ControlToValidate = this.txtXuatXuColumn;
            this.requiredFieldValidator4.ErrorMessage = "\"Cột xuất xứ\" bắt buộc phải nhập.";
            this.requiredFieldValidator4.Icon = ((System.Drawing.Icon)(resources.GetObject("requiredFieldValidator4.Icon")));
            this.requiredFieldValidator4.Tag = "requiredFieldValidator4";
            // 
            // lblLinkExcel
            // 
            this.lblLinkExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblLinkExcel.AutoSize = true;
            this.lblLinkExcel.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkExcel.Location = new System.Drawing.Point(12, 158);
            this.lblLinkExcel.Name = "lblLinkExcel";
            this.lblLinkExcel.Size = new System.Drawing.Size(106, 13);
            this.lblLinkExcel.TabIndex = 218;
            this.lblLinkExcel.TabStop = true;
            this.lblLinkExcel.Text = "Mở tệp tin mẫu Excel";
            this.lblLinkExcel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLinkExcel_LinkClicked);
            // 
            // ReadExcelHCTForm
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(537, 178);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReadExcelHCTForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đọc danh sách hàng hóa";
            this.Load += new System.EventHandler(this.ReadExcelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            this.grbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDonGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.requiredFieldValidator4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnSelectFile;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtXuatXuColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuongColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonGiaColumn;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UICheckBox uiCheckBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvFilePath;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDonGia;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoLuong;
        private Company.Controls.CustomValidation.RequiredFieldValidator requiredFieldValidator4;
        private System.Windows.Forms.LinkLabel lblLinkExcel;
    }
}