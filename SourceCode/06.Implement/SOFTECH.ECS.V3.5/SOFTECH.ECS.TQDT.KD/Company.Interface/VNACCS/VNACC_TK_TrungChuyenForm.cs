﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;
using Janus.Windows.GridEX;

namespace Company.Interface
{
    public partial class VNACC_TK_TrungChuyenForm : BaseFormHaveGuidPanel
    {
        public KDT_VNACC_ToKhaiMauDich TKMD;
        DataTable dt = new DataTable();
        DataSet dsMaDiaDiem = new DataSet();
        public String Caption;
        public bool IsChange;
        public VNACC_TK_TrungChuyenForm()
        {
            InitializeComponent();
        }
        private void SetChange(bool Status)
        {
            if (Status)
            {
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ChuaKhaiBao || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoTam || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoSua)
                {
                    this.IsChange = true;
                    this.Text = Caption + " * ";
                }
            }
            else
            {
                this.IsChange = false;
                this.Text = Caption;
            }
        }
        private void txt_TextChanged(object sender, EventArgs e)
        {
            this.SetChange(true);
        }
        private void VNACC_TK_TrungChuyenForm_Load(object sender, EventArgs e)
        {
            try
            {
                Caption = this.Text;
                LoadData();
                if (TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.KhaiBaoChinhThuc || TKMD.TrangThaiXuLy == EnumTrangThaiXuLy.ThongQuan)
                {
                    btnSave.Enabled = false;
                    btnXoa.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }
        private void LoadData()
        {
            try
            {
                dsMaDiaDiem = VNACC_Category_Cargo.SelectAll();
                grList.DropDowns["drpMaDiaDiem"].DataSource = dsMaDiaDiem.Tables[0];
                dt = Company.KDT.SHARE.Components.GenericListToDataTable.ConvertTo(TKMD.TrungChuyenCollection);
                grList.DataSource = dt;
                grList.RecordAdded += new EventHandler(txt_TextChanged);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                int sott = 1;
                TKMD.TrungChuyenCollection.Clear();
                if (dt.Rows.Count == 0) return;
                foreach (DataRow item in dt.Rows)
                {
                    KDT_VNACC_TK_TrungChuyen tc = new KDT_VNACC_TK_TrungChuyen();
                    if (item["ID"].ToString().Length != 0)
                    {
                        tc.ID = Convert.ToInt32(item["ID"].ToString());
                        tc.TKMD_ID = Convert.ToInt32(item["TKMD_ID"].ToString());
                    }
                    tc.SoTT = sott;
                    tc.MaDiaDiem = item["MaDiaDiem"].ToString();
                    tc.NgayDen = Convert.ToDateTime(item["NgayDen"].ToString());
                    tc.NgayKhoiHanh = Convert.ToDateTime(item["NgayKhoiHanh"].ToString());
                    TKMD.TrungChuyenCollection.Add(tc);
                    sott++;
                }
                ShowMessage("Lưu thành công ", false);
                this.SetChange(true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;

            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void grList_DeletingRecord(object sender, RowActionCancelEventArgs e)
        {
            btnXoa_Click(null, null);
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                int ID = Convert.ToInt32(grList.CurrentRow.Cells["ID"].Value.ToString());
                if (ShowMessage("Bạn có muốn xóa thông tin trung chuyển này không?", true) == "Yes")
                {
                    if (ID > 0)
                        KDT_VNACC_TK_TrungChuyen.DeleteDynamic("ID=" + ID);
                    grList.CurrentRow.Delete();
                    grList.Refetch();
                    this.SetChange(true);
                }
                this.Cursor = Cursors.Default;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                this.Cursor = Cursors.Default;

            }
        }

        private void VNACC_TK_TrungChuyenForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (IsChange)
            {
                if (ShowMessage("Dữ liệu Trung chuyển có thay đổi . Doanh nghiệp có muốn Lưu thay đổi không ? ", true) == "Yes")
                {
                    this.btnSave_Click(null,null);
                }
            }
        }


    }
}
