using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_TKTriGiaThap : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public string MaLoaiHinh { set; get; }
		public decimal SoToKhai { set; get; }
		public string PhanLoaiBaoCaoSua { set; get; }
		public string PhanLoaiToChuc { set; get; }
		public string CoQuanHaiQuan { set; get; }
		public string NhomXuLyHS { set; get; }
		public string MaDonVi { set; get; }
		public string TenDonVi { set; get; }
		public string MaBuuChinhDonVi { set; get; }
		public string DiaChiDonVi { set; get; }
		public string SoDienThoaiDonVi { set; get; }
		public string MaDoiTac { set; get; }
		public string TenDoiTac { set; get; }
		public string MaBuuChinhDoiTac { set; get; }
		public string DiaChiDoiTac1 { set; get; }
		public string DiaChiDoiTac2 { set; get; }
		public string DiaChiDoiTac3 { set; get; }
		public string DiaChiDoiTac4 { set; get; }
		public string MaNuocDoiTac { set; get; }
		public string SoHouseAWB { set; get; }
		public string SoMasterAWB { set; get; }
		public decimal SoLuong { set; get; }
		public decimal TrongLuong { set; get; }
		public string MaDDLuuKho { set; get; }
		public string TenMayBayChoHang { set; get; }
		public DateTime NgayHangDen { set; get; }
		public string MaCangDoHang { set; get; }
		public string MaDDNhanHangCuoiCung { set; get; }
		public string MaDiaDiemXepHang { set; get; }
		public decimal TongTriGiaTinhThue { set; get; }
		public string MaTTTongTriGiaTinhThue { set; get; }
		public decimal GiaKhaiBao { set; get; }
		public string PhanLoaiGiaHD { set; get; }
		public string MaDieuKienGiaHD { set; get; }
		public string MaTTHoaDon { set; get; }
		public decimal TongTriGiaHD { set; get; }
		public string MaPhanLoaiPhiVC { set; get; }
		public string MaTTPhiVC { set; get; }
		public decimal PhiVanChuyen { set; get; }
		public string MaPhanLoaiPhiBH { set; get; }
		public string MaTTPhiBH { set; get; }
		public decimal PhiBaoHiem { set; get; }
		public string MoTaHangHoa { set; get; }
		public string MaNuocXuatXu { set; get; }
		public decimal TriGiaTinhThue { set; get; }
		public string GhiChu { set; get; }
		public string SoQuanLyNoiBoDN { set; get; }
		public string TrangThaiXuLy { set; get; }
		public string InputMessageID { set; get; }
		public string MessageTag { set; get; }
		public string IndexTag { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_TKTriGiaThap> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_TKTriGiaThap> collection = new List<KDT_VNACCS_TKTriGiaThap>();
			while (reader.Read())
			{
				KDT_VNACCS_TKTriGiaThap entity = new KDT_VNACCS_TKTriGiaThap();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaLoaiHinh"))) entity.MaLoaiHinh = reader.GetString(reader.GetOrdinal("MaLoaiHinh"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoToKhai"))) entity.SoToKhai = reader.GetDecimal(reader.GetOrdinal("SoToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiBaoCaoSua"))) entity.PhanLoaiBaoCaoSua = reader.GetString(reader.GetOrdinal("PhanLoaiBaoCaoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiToChuc"))) entity.PhanLoaiToChuc = reader.GetString(reader.GetOrdinal("PhanLoaiToChuc"));
				if (!reader.IsDBNull(reader.GetOrdinal("CoQuanHaiQuan"))) entity.CoQuanHaiQuan = reader.GetString(reader.GetOrdinal("CoQuanHaiQuan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NhomXuLyHS"))) entity.NhomXuLyHS = reader.GetString(reader.GetOrdinal("NhomXuLyHS"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDonVi"))) entity.MaDonVi = reader.GetString(reader.GetOrdinal("MaDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDonVi"))) entity.TenDonVi = reader.GetString(reader.GetOrdinal("TenDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhDonVi"))) entity.MaBuuChinhDonVi = reader.GetString(reader.GetOrdinal("MaBuuChinhDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDonVi"))) entity.DiaChiDonVi = reader.GetString(reader.GetOrdinal("DiaChiDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDienThoaiDonVi"))) entity.SoDienThoaiDonVi = reader.GetString(reader.GetOrdinal("SoDienThoaiDonVi"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoiTac"))) entity.MaDoiTac = reader.GetString(reader.GetOrdinal("MaDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenDoiTac"))) entity.TenDoiTac = reader.GetString(reader.GetOrdinal("TenDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaBuuChinhDoiTac"))) entity.MaBuuChinhDoiTac = reader.GetString(reader.GetOrdinal("MaBuuChinhDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac1"))) entity.DiaChiDoiTac1 = reader.GetString(reader.GetOrdinal("DiaChiDoiTac1"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac2"))) entity.DiaChiDoiTac2 = reader.GetString(reader.GetOrdinal("DiaChiDoiTac2"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac3"))) entity.DiaChiDoiTac3 = reader.GetString(reader.GetOrdinal("DiaChiDoiTac3"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaChiDoiTac4"))) entity.DiaChiDoiTac4 = reader.GetString(reader.GetOrdinal("DiaChiDoiTac4"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNuocDoiTac"))) entity.MaNuocDoiTac = reader.GetString(reader.GetOrdinal("MaNuocDoiTac"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHouseAWB"))) entity.SoHouseAWB = reader.GetString(reader.GetOrdinal("SoHouseAWB"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoMasterAWB"))) entity.SoMasterAWB = reader.GetString(reader.GetOrdinal("SoMasterAWB"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoLuong"))) entity.SoLuong = reader.GetDecimal(reader.GetOrdinal("SoLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrongLuong"))) entity.TrongLuong = reader.GetDecimal(reader.GetOrdinal("TrongLuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDDLuuKho"))) entity.MaDDLuuKho = reader.GetString(reader.GetOrdinal("MaDDLuuKho"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenMayBayChoHang"))) entity.TenMayBayChoHang = reader.GetString(reader.GetOrdinal("TenMayBayChoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayHangDen"))) entity.NgayHangDen = reader.GetDateTime(reader.GetOrdinal("NgayHangDen"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCangDoHang"))) entity.MaCangDoHang = reader.GetString(reader.GetOrdinal("MaCangDoHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDDNhanHangCuoiCung"))) entity.MaDDNhanHangCuoiCung = reader.GetString(reader.GetOrdinal("MaDDNhanHangCuoiCung"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDiaDiemXepHang"))) entity.MaDiaDiemXepHang = reader.GetString(reader.GetOrdinal("MaDiaDiemXepHang"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaTinhThue"))) entity.TongTriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TongTriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTTongTriGiaTinhThue"))) entity.MaTTTongTriGiaTinhThue = reader.GetString(reader.GetOrdinal("MaTTTongTriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("GiaKhaiBao"))) entity.GiaKhaiBao = reader.GetDecimal(reader.GetOrdinal("GiaKhaiBao"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhanLoaiGiaHD"))) entity.PhanLoaiGiaHD = reader.GetString(reader.GetOrdinal("PhanLoaiGiaHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDieuKienGiaHD"))) entity.MaDieuKienGiaHD = reader.GetString(reader.GetOrdinal("MaDieuKienGiaHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTHoaDon"))) entity.MaTTHoaDon = reader.GetString(reader.GetOrdinal("MaTTHoaDon"));
				if (!reader.IsDBNull(reader.GetOrdinal("TongTriGiaHD"))) entity.TongTriGiaHD = reader.GetDecimal(reader.GetOrdinal("TongTriGiaHD"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiPhiVC"))) entity.MaPhanLoaiPhiVC = reader.GetString(reader.GetOrdinal("MaPhanLoaiPhiVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTPhiVC"))) entity.MaTTPhiVC = reader.GetString(reader.GetOrdinal("MaTTPhiVC"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiVanChuyen"))) entity.PhiVanChuyen = reader.GetDecimal(reader.GetOrdinal("PhiVanChuyen"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaPhanLoaiPhiBH"))) entity.MaPhanLoaiPhiBH = reader.GetString(reader.GetOrdinal("MaPhanLoaiPhiBH"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaTTPhiBH"))) entity.MaTTPhiBH = reader.GetString(reader.GetOrdinal("MaTTPhiBH"));
				if (!reader.IsDBNull(reader.GetOrdinal("PhiBaoHiem"))) entity.PhiBaoHiem = reader.GetDecimal(reader.GetOrdinal("PhiBaoHiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("MoTaHangHoa"))) entity.MoTaHangHoa = reader.GetString(reader.GetOrdinal("MoTaHangHoa"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaNuocXuatXu"))) entity.MaNuocXuatXu = reader.GetString(reader.GetOrdinal("MaNuocXuatXu"));
				if (!reader.IsDBNull(reader.GetOrdinal("TriGiaTinhThue"))) entity.TriGiaTinhThue = reader.GetDecimal(reader.GetOrdinal("TriGiaTinhThue"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoQuanLyNoiBoDN"))) entity.SoQuanLyNoiBoDN = reader.GetString(reader.GetOrdinal("SoQuanLyNoiBoDN"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetString(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("InputMessageID"))) entity.InputMessageID = reader.GetString(reader.GetOrdinal("InputMessageID"));
				if (!reader.IsDBNull(reader.GetOrdinal("MessageTag"))) entity.MessageTag = reader.GetString(reader.GetOrdinal("MessageTag"));
				if (!reader.IsDBNull(reader.GetOrdinal("IndexTag"))) entity.IndexTag = reader.GetString(reader.GetOrdinal("IndexTag"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_TKTriGiaThap> collection, long id)
        {
            foreach (KDT_VNACCS_TKTriGiaThap item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TKTriGiaThap VALUES(@MaLoaiHinh, @SoToKhai, @PhanLoaiBaoCaoSua, @PhanLoaiToChuc, @CoQuanHaiQuan, @NhomXuLyHS, @MaDonVi, @TenDonVi, @MaBuuChinhDonVi, @DiaChiDonVi, @SoDienThoaiDonVi, @MaDoiTac, @TenDoiTac, @MaBuuChinhDoiTac, @DiaChiDoiTac1, @DiaChiDoiTac2, @DiaChiDoiTac3, @DiaChiDoiTac4, @MaNuocDoiTac, @SoHouseAWB, @SoMasterAWB, @SoLuong, @TrongLuong, @MaDDLuuKho, @TenMayBayChoHang, @NgayHangDen, @MaCangDoHang, @MaDDNhanHangCuoiCung, @MaDiaDiemXepHang, @TongTriGiaTinhThue, @MaTTTongTriGiaTinhThue, @GiaKhaiBao, @PhanLoaiGiaHD, @MaDieuKienGiaHD, @MaTTHoaDon, @TongTriGiaHD, @MaPhanLoaiPhiVC, @MaTTPhiVC, @PhiVanChuyen, @MaPhanLoaiPhiBH, @MaTTPhiBH, @PhiBaoHiem, @MoTaHangHoa, @MaNuocXuatXu, @TriGiaTinhThue, @GhiChu, @SoQuanLyNoiBoDN, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_TKTriGiaThap SET MaLoaiHinh = @MaLoaiHinh, SoToKhai = @SoToKhai, PhanLoaiBaoCaoSua = @PhanLoaiBaoCaoSua, PhanLoaiToChuc = @PhanLoaiToChuc, CoQuanHaiQuan = @CoQuanHaiQuan, NhomXuLyHS = @NhomXuLyHS, MaDonVi = @MaDonVi, TenDonVi = @TenDonVi, MaBuuChinhDonVi = @MaBuuChinhDonVi, DiaChiDonVi = @DiaChiDonVi, SoDienThoaiDonVi = @SoDienThoaiDonVi, MaDoiTac = @MaDoiTac, TenDoiTac = @TenDoiTac, MaBuuChinhDoiTac = @MaBuuChinhDoiTac, DiaChiDoiTac1 = @DiaChiDoiTac1, DiaChiDoiTac2 = @DiaChiDoiTac2, DiaChiDoiTac3 = @DiaChiDoiTac3, DiaChiDoiTac4 = @DiaChiDoiTac4, MaNuocDoiTac = @MaNuocDoiTac, SoHouseAWB = @SoHouseAWB, SoMasterAWB = @SoMasterAWB, SoLuong = @SoLuong, TrongLuong = @TrongLuong, MaDDLuuKho = @MaDDLuuKho, TenMayBayChoHang = @TenMayBayChoHang, NgayHangDen = @NgayHangDen, MaCangDoHang = @MaCangDoHang, MaDDNhanHangCuoiCung = @MaDDNhanHangCuoiCung, MaDiaDiemXepHang = @MaDiaDiemXepHang, TongTriGiaTinhThue = @TongTriGiaTinhThue, MaTTTongTriGiaTinhThue = @MaTTTongTriGiaTinhThue, GiaKhaiBao = @GiaKhaiBao, PhanLoaiGiaHD = @PhanLoaiGiaHD, MaDieuKienGiaHD = @MaDieuKienGiaHD, MaTTHoaDon = @MaTTHoaDon, TongTriGiaHD = @TongTriGiaHD, MaPhanLoaiPhiVC = @MaPhanLoaiPhiVC, MaTTPhiVC = @MaTTPhiVC, PhiVanChuyen = @PhiVanChuyen, MaPhanLoaiPhiBH = @MaPhanLoaiPhiBH, MaTTPhiBH = @MaTTPhiBH, PhiBaoHiem = @PhiBaoHiem, MoTaHangHoa = @MoTaHangHoa, MaNuocXuatXu = @MaNuocXuatXu, TriGiaTinhThue = @TriGiaTinhThue, GhiChu = @GhiChu, SoQuanLyNoiBoDN = @SoQuanLyNoiBoDN, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TKTriGiaThap WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiBaoCaoSua", SqlDbType.VarChar, "PhanLoaiBaoCaoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, "PhanLoaiToChuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHS", SqlDbType.VarChar, "NhomXuLyHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonVi", SqlDbType.VarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, "MaBuuChinhDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDonVi", SqlDbType.NVarChar, "DiaChiDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, "SoDienThoaiDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoiTac", SqlDbType.VarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, "MaBuuChinhDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, "DiaChiDoiTac1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, "DiaChiDoiTac2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, "DiaChiDoiTac3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, "DiaChiDoiTac4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocDoiTac", SqlDbType.VarChar, "MaNuocDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHouseAWB", SqlDbType.VarChar, "SoHouseAWB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoMasterAWB", SqlDbType.VarChar, "SoMasterAWB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDDLuuKho", SqlDbType.VarChar, "MaDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenMayBayChoHang", SqlDbType.NVarChar, "TenMayBayChoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHangDen", SqlDbType.DateTime, "NgayHangDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangDoHang", SqlDbType.VarChar, "MaCangDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDDNhanHangCuoiCung", SqlDbType.VarChar, "MaDDNhanHangCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaTinhThue", SqlDbType.Decimal, "TongTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTongTriGiaTinhThue", SqlDbType.VarChar, "MaTTTongTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaKhaiBao", SqlDbType.Decimal, "GiaKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, "PhanLoaiGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, "MaDieuKienGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTHoaDon", SqlDbType.VarChar, "MaTTHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaHD", SqlDbType.Decimal, "TongTriGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, "MaPhanLoaiPhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTPhiVC", SqlDbType.VarChar, "MaTTPhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, "MaPhanLoaiPhiBH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTPhiBH", SqlDbType.VarChar, "MaTTPhiBH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTaHangHoa", SqlDbType.NVarChar, "MoTaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocXuatXu", SqlDbType.VarChar, "MaNuocXuatXu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, "SoQuanLyNoiBoDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.NChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiBaoCaoSua", SqlDbType.VarChar, "PhanLoaiBaoCaoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, "PhanLoaiToChuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHS", SqlDbType.VarChar, "NhomXuLyHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonVi", SqlDbType.VarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, "MaBuuChinhDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDonVi", SqlDbType.NVarChar, "DiaChiDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, "SoDienThoaiDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoiTac", SqlDbType.VarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, "MaBuuChinhDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, "DiaChiDoiTac1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, "DiaChiDoiTac2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, "DiaChiDoiTac3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, "DiaChiDoiTac4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocDoiTac", SqlDbType.VarChar, "MaNuocDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHouseAWB", SqlDbType.VarChar, "SoHouseAWB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoMasterAWB", SqlDbType.VarChar, "SoMasterAWB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDDLuuKho", SqlDbType.VarChar, "MaDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenMayBayChoHang", SqlDbType.NVarChar, "TenMayBayChoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHangDen", SqlDbType.DateTime, "NgayHangDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangDoHang", SqlDbType.VarChar, "MaCangDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDDNhanHangCuoiCung", SqlDbType.VarChar, "MaDDNhanHangCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaTinhThue", SqlDbType.Decimal, "TongTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTongTriGiaTinhThue", SqlDbType.VarChar, "MaTTTongTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaKhaiBao", SqlDbType.Decimal, "GiaKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, "PhanLoaiGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, "MaDieuKienGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTHoaDon", SqlDbType.VarChar, "MaTTHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaHD", SqlDbType.Decimal, "TongTriGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, "MaPhanLoaiPhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTPhiVC", SqlDbType.VarChar, "MaTTPhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, "MaPhanLoaiPhiBH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTPhiBH", SqlDbType.VarChar, "MaTTPhiBH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTaHangHoa", SqlDbType.NVarChar, "MoTaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocXuatXu", SqlDbType.VarChar, "MaNuocXuatXu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, "SoQuanLyNoiBoDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.NChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_TKTriGiaThap VALUES(@MaLoaiHinh, @SoToKhai, @PhanLoaiBaoCaoSua, @PhanLoaiToChuc, @CoQuanHaiQuan, @NhomXuLyHS, @MaDonVi, @TenDonVi, @MaBuuChinhDonVi, @DiaChiDonVi, @SoDienThoaiDonVi, @MaDoiTac, @TenDoiTac, @MaBuuChinhDoiTac, @DiaChiDoiTac1, @DiaChiDoiTac2, @DiaChiDoiTac3, @DiaChiDoiTac4, @MaNuocDoiTac, @SoHouseAWB, @SoMasterAWB, @SoLuong, @TrongLuong, @MaDDLuuKho, @TenMayBayChoHang, @NgayHangDen, @MaCangDoHang, @MaDDNhanHangCuoiCung, @MaDiaDiemXepHang, @TongTriGiaTinhThue, @MaTTTongTriGiaTinhThue, @GiaKhaiBao, @PhanLoaiGiaHD, @MaDieuKienGiaHD, @MaTTHoaDon, @TongTriGiaHD, @MaPhanLoaiPhiVC, @MaTTPhiVC, @PhiVanChuyen, @MaPhanLoaiPhiBH, @MaTTPhiBH, @PhiBaoHiem, @MoTaHangHoa, @MaNuocXuatXu, @TriGiaTinhThue, @GhiChu, @SoQuanLyNoiBoDN, @TrangThaiXuLy, @InputMessageID, @MessageTag, @IndexTag)";
            string update = "UPDATE t_KDT_VNACCS_TKTriGiaThap SET MaLoaiHinh = @MaLoaiHinh, SoToKhai = @SoToKhai, PhanLoaiBaoCaoSua = @PhanLoaiBaoCaoSua, PhanLoaiToChuc = @PhanLoaiToChuc, CoQuanHaiQuan = @CoQuanHaiQuan, NhomXuLyHS = @NhomXuLyHS, MaDonVi = @MaDonVi, TenDonVi = @TenDonVi, MaBuuChinhDonVi = @MaBuuChinhDonVi, DiaChiDonVi = @DiaChiDonVi, SoDienThoaiDonVi = @SoDienThoaiDonVi, MaDoiTac = @MaDoiTac, TenDoiTac = @TenDoiTac, MaBuuChinhDoiTac = @MaBuuChinhDoiTac, DiaChiDoiTac1 = @DiaChiDoiTac1, DiaChiDoiTac2 = @DiaChiDoiTac2, DiaChiDoiTac3 = @DiaChiDoiTac3, DiaChiDoiTac4 = @DiaChiDoiTac4, MaNuocDoiTac = @MaNuocDoiTac, SoHouseAWB = @SoHouseAWB, SoMasterAWB = @SoMasterAWB, SoLuong = @SoLuong, TrongLuong = @TrongLuong, MaDDLuuKho = @MaDDLuuKho, TenMayBayChoHang = @TenMayBayChoHang, NgayHangDen = @NgayHangDen, MaCangDoHang = @MaCangDoHang, MaDDNhanHangCuoiCung = @MaDDNhanHangCuoiCung, MaDiaDiemXepHang = @MaDiaDiemXepHang, TongTriGiaTinhThue = @TongTriGiaTinhThue, MaTTTongTriGiaTinhThue = @MaTTTongTriGiaTinhThue, GiaKhaiBao = @GiaKhaiBao, PhanLoaiGiaHD = @PhanLoaiGiaHD, MaDieuKienGiaHD = @MaDieuKienGiaHD, MaTTHoaDon = @MaTTHoaDon, TongTriGiaHD = @TongTriGiaHD, MaPhanLoaiPhiVC = @MaPhanLoaiPhiVC, MaTTPhiVC = @MaTTPhiVC, PhiVanChuyen = @PhiVanChuyen, MaPhanLoaiPhiBH = @MaPhanLoaiPhiBH, MaTTPhiBH = @MaTTPhiBH, PhiBaoHiem = @PhiBaoHiem, MoTaHangHoa = @MoTaHangHoa, MaNuocXuatXu = @MaNuocXuatXu, TriGiaTinhThue = @TriGiaTinhThue, GhiChu = @GhiChu, SoQuanLyNoiBoDN = @SoQuanLyNoiBoDN, TrangThaiXuLy = @TrangThaiXuLy, InputMessageID = @InputMessageID, MessageTag = @MessageTag, IndexTag = @IndexTag WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_TKTriGiaThap WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiBaoCaoSua", SqlDbType.VarChar, "PhanLoaiBaoCaoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, "PhanLoaiToChuc", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NhomXuLyHS", SqlDbType.VarChar, "NhomXuLyHS", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDonVi", SqlDbType.VarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, "MaBuuChinhDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDonVi", SqlDbType.NVarChar, "DiaChiDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, "SoDienThoaiDonVi", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoiTac", SqlDbType.VarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, "MaBuuChinhDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, "DiaChiDoiTac1", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, "DiaChiDoiTac2", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, "DiaChiDoiTac3", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, "DiaChiDoiTac4", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocDoiTac", SqlDbType.VarChar, "MaNuocDoiTac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHouseAWB", SqlDbType.VarChar, "SoHouseAWB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoMasterAWB", SqlDbType.VarChar, "SoMasterAWB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDDLuuKho", SqlDbType.VarChar, "MaDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenMayBayChoHang", SqlDbType.NVarChar, "TenMayBayChoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayHangDen", SqlDbType.DateTime, "NgayHangDen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCangDoHang", SqlDbType.VarChar, "MaCangDoHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDDNhanHangCuoiCung", SqlDbType.VarChar, "MaDDNhanHangCuoiCung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaTinhThue", SqlDbType.Decimal, "TongTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTTongTriGiaTinhThue", SqlDbType.VarChar, "MaTTTongTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GiaKhaiBao", SqlDbType.Decimal, "GiaKhaiBao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, "PhanLoaiGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, "MaDieuKienGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTHoaDon", SqlDbType.VarChar, "MaTTHoaDon", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TongTriGiaHD", SqlDbType.Decimal, "TongTriGiaHD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, "MaPhanLoaiPhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTPhiVC", SqlDbType.VarChar, "MaTTPhiVC", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, "MaPhanLoaiPhiBH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaTTPhiBH", SqlDbType.VarChar, "MaTTPhiBH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MoTaHangHoa", SqlDbType.NVarChar, "MoTaHangHoa", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaNuocXuatXu", SqlDbType.VarChar, "MaNuocXuatXu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, "SoQuanLyNoiBoDN", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.NChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaLoaiHinh", SqlDbType.VarChar, "MaLoaiHinh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoToKhai", SqlDbType.Decimal, "SoToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiBaoCaoSua", SqlDbType.VarChar, "PhanLoaiBaoCaoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, "PhanLoaiToChuc", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, "CoQuanHaiQuan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NhomXuLyHS", SqlDbType.VarChar, "NhomXuLyHS", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDonVi", SqlDbType.VarChar, "MaDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDonVi", SqlDbType.NVarChar, "TenDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, "MaBuuChinhDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDonVi", SqlDbType.NVarChar, "DiaChiDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, "SoDienThoaiDonVi", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoiTac", SqlDbType.VarChar, "MaDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenDoiTac", SqlDbType.NVarChar, "TenDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, "MaBuuChinhDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, "DiaChiDoiTac1", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, "DiaChiDoiTac2", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, "DiaChiDoiTac3", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, "DiaChiDoiTac4", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocDoiTac", SqlDbType.VarChar, "MaNuocDoiTac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHouseAWB", SqlDbType.VarChar, "SoHouseAWB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoMasterAWB", SqlDbType.VarChar, "SoMasterAWB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoLuong", SqlDbType.Decimal, "SoLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrongLuong", SqlDbType.Decimal, "TrongLuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDDLuuKho", SqlDbType.VarChar, "MaDDLuuKho", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenMayBayChoHang", SqlDbType.NVarChar, "TenMayBayChoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayHangDen", SqlDbType.DateTime, "NgayHangDen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCangDoHang", SqlDbType.VarChar, "MaCangDoHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDDNhanHangCuoiCung", SqlDbType.VarChar, "MaDDNhanHangCuoiCung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, "MaDiaDiemXepHang", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaTinhThue", SqlDbType.Decimal, "TongTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTTongTriGiaTinhThue", SqlDbType.VarChar, "MaTTTongTriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GiaKhaiBao", SqlDbType.Decimal, "GiaKhaiBao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, "PhanLoaiGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, "MaDieuKienGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTHoaDon", SqlDbType.VarChar, "MaTTHoaDon", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TongTriGiaHD", SqlDbType.Decimal, "TongTriGiaHD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, "MaPhanLoaiPhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTPhiVC", SqlDbType.VarChar, "MaTTPhiVC", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiVanChuyen", SqlDbType.Decimal, "PhiVanChuyen", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, "MaPhanLoaiPhiBH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaTTPhiBH", SqlDbType.VarChar, "MaTTPhiBH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PhiBaoHiem", SqlDbType.Decimal, "PhiBaoHiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MoTaHangHoa", SqlDbType.NVarChar, "MoTaHangHoa", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaNuocXuatXu", SqlDbType.VarChar, "MaNuocXuatXu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TriGiaTinhThue", SqlDbType.Decimal, "TriGiaTinhThue", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, "SoQuanLyNoiBoDN", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.NChar, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@InputMessageID", SqlDbType.VarChar, "InputMessageID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MessageTag", SqlDbType.VarChar, "MessageTag", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@IndexTag", SqlDbType.VarChar, "IndexTag", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_TKTriGiaThap Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TKTriGiaThap_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_TKTriGiaThap> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_TKTriGiaThap> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_TKTriGiaThap> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_TKTriGiaThap_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_TKTriGiaThap(string maLoaiHinh, decimal soToKhai, string phanLoaiBaoCaoSua, string phanLoaiToChuc, string coQuanHaiQuan, string nhomXuLyHS, string maDonVi, string tenDonVi, string maBuuChinhDonVi, string diaChiDonVi, string soDienThoaiDonVi, string maDoiTac, string tenDoiTac, string maBuuChinhDoiTac, string diaChiDoiTac1, string diaChiDoiTac2, string diaChiDoiTac3, string diaChiDoiTac4, string maNuocDoiTac, string soHouseAWB, string soMasterAWB, decimal soLuong, decimal trongLuong, string maDDLuuKho, string tenMayBayChoHang, DateTime ngayHangDen, string maCangDoHang, string maDDNhanHangCuoiCung, string maDiaDiemXepHang, decimal tongTriGiaTinhThue, string maTTTongTriGiaTinhThue, decimal giaKhaiBao, string phanLoaiGiaHD, string maDieuKienGiaHD, string maTTHoaDon, decimal tongTriGiaHD, string maPhanLoaiPhiVC, string maTTPhiVC, decimal phiVanChuyen, string maPhanLoaiPhiBH, string maTTPhiBH, decimal phiBaoHiem, string moTaHangHoa, string maNuocXuatXu, decimal triGiaTinhThue, string ghiChu, string soQuanLyNoiBoDN, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TKTriGiaThap entity = new KDT_VNACCS_TKTriGiaThap();	
			entity.MaLoaiHinh = maLoaiHinh;
			entity.SoToKhai = soToKhai;
			entity.PhanLoaiBaoCaoSua = phanLoaiBaoCaoSua;
			entity.PhanLoaiToChuc = phanLoaiToChuc;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHS = nhomXuLyHS;
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.MaBuuChinhDonVi = maBuuChinhDonVi;
			entity.DiaChiDonVi = diaChiDonVi;
			entity.SoDienThoaiDonVi = soDienThoaiDonVi;
			entity.MaDoiTac = maDoiTac;
			entity.TenDoiTac = tenDoiTac;
			entity.MaBuuChinhDoiTac = maBuuChinhDoiTac;
			entity.DiaChiDoiTac1 = diaChiDoiTac1;
			entity.DiaChiDoiTac2 = diaChiDoiTac2;
			entity.DiaChiDoiTac3 = diaChiDoiTac3;
			entity.DiaChiDoiTac4 = diaChiDoiTac4;
			entity.MaNuocDoiTac = maNuocDoiTac;
			entity.SoHouseAWB = soHouseAWB;
			entity.SoMasterAWB = soMasterAWB;
			entity.SoLuong = soLuong;
			entity.TrongLuong = trongLuong;
			entity.MaDDLuuKho = maDDLuuKho;
			entity.TenMayBayChoHang = tenMayBayChoHang;
			entity.NgayHangDen = ngayHangDen;
			entity.MaCangDoHang = maCangDoHang;
			entity.MaDDNhanHangCuoiCung = maDDNhanHangCuoiCung;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TongTriGiaTinhThue = tongTriGiaTinhThue;
			entity.MaTTTongTriGiaTinhThue = maTTTongTriGiaTinhThue;
			entity.GiaKhaiBao = giaKhaiBao;
			entity.PhanLoaiGiaHD = phanLoaiGiaHD;
			entity.MaDieuKienGiaHD = maDieuKienGiaHD;
			entity.MaTTHoaDon = maTTHoaDon;
			entity.TongTriGiaHD = tongTriGiaHD;
			entity.MaPhanLoaiPhiVC = maPhanLoaiPhiVC;
			entity.MaTTPhiVC = maTTPhiVC;
			entity.PhiVanChuyen = phiVanChuyen;
			entity.MaPhanLoaiPhiBH = maPhanLoaiPhiBH;
			entity.MaTTPhiBH = maTTPhiBH;
			entity.PhiBaoHiem = phiBaoHiem;
			entity.MoTaHangHoa = moTaHangHoa;
			entity.MaNuocXuatXu = maNuocXuatXu;
			entity.TriGiaTinhThue = triGiaTinhThue;
			entity.GhiChu = ghiChu;
			entity.SoQuanLyNoiBoDN = soQuanLyNoiBoDN;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_TKTriGiaThap_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@PhanLoaiBaoCaoSua", SqlDbType.VarChar, PhanLoaiBaoCaoSua);
			db.AddInParameter(dbCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, PhanLoaiToChuc);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHS", SqlDbType.VarChar, NhomXuLyHS);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.VarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, MaBuuChinhDonVi);
			db.AddInParameter(dbCommand, "@DiaChiDonVi", SqlDbType.NVarChar, DiaChiDonVi);
			db.AddInParameter(dbCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, SoDienThoaiDonVi);
			db.AddInParameter(dbCommand, "@MaDoiTac", SqlDbType.VarChar, MaDoiTac);
			db.AddInParameter(dbCommand, "@TenDoiTac", SqlDbType.NVarChar, TenDoiTac);
			db.AddInParameter(dbCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, MaBuuChinhDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, DiaChiDoiTac1);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, DiaChiDoiTac2);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, DiaChiDoiTac3);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, DiaChiDoiTac4);
			db.AddInParameter(dbCommand, "@MaNuocDoiTac", SqlDbType.VarChar, MaNuocDoiTac);
			db.AddInParameter(dbCommand, "@SoHouseAWB", SqlDbType.VarChar, SoHouseAWB);
			db.AddInParameter(dbCommand, "@SoMasterAWB", SqlDbType.VarChar, SoMasterAWB);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@MaDDLuuKho", SqlDbType.VarChar, MaDDLuuKho);
			db.AddInParameter(dbCommand, "@TenMayBayChoHang", SqlDbType.NVarChar, TenMayBayChoHang);
			db.AddInParameter(dbCommand, "@NgayHangDen", SqlDbType.DateTime, NgayHangDen.Year <= 1753 ? DBNull.Value : (object) NgayHangDen);
			db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
			db.AddInParameter(dbCommand, "@MaDDNhanHangCuoiCung", SqlDbType.VarChar, MaDDNhanHangCuoiCung);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Decimal, TongTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTTongTriGiaTinhThue", SqlDbType.VarChar, MaTTTongTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@GiaKhaiBao", SqlDbType.Decimal, GiaKhaiBao);
			db.AddInParameter(dbCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, PhanLoaiGiaHD);
			db.AddInParameter(dbCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, MaDieuKienGiaHD);
			db.AddInParameter(dbCommand, "@MaTTHoaDon", SqlDbType.VarChar, MaTTHoaDon);
			db.AddInParameter(dbCommand, "@TongTriGiaHD", SqlDbType.Decimal, TongTriGiaHD);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, MaPhanLoaiPhiVC);
			db.AddInParameter(dbCommand, "@MaTTPhiVC", SqlDbType.VarChar, MaTTPhiVC);
			db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Decimal, PhiVanChuyen);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, MaPhanLoaiPhiBH);
			db.AddInParameter(dbCommand, "@MaTTPhiBH", SqlDbType.VarChar, MaTTPhiBH);
			db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Decimal, PhiBaoHiem);
			db.AddInParameter(dbCommand, "@MoTaHangHoa", SqlDbType.NVarChar, MoTaHangHoa);
			db.AddInParameter(dbCommand, "@MaNuocXuatXu", SqlDbType.VarChar, MaNuocXuatXu);
			db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Decimal, TriGiaTinhThue);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, SoQuanLyNoiBoDN);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.NChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_TKTriGiaThap> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TKTriGiaThap item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_TKTriGiaThap(long id, string maLoaiHinh, decimal soToKhai, string phanLoaiBaoCaoSua, string phanLoaiToChuc, string coQuanHaiQuan, string nhomXuLyHS, string maDonVi, string tenDonVi, string maBuuChinhDonVi, string diaChiDonVi, string soDienThoaiDonVi, string maDoiTac, string tenDoiTac, string maBuuChinhDoiTac, string diaChiDoiTac1, string diaChiDoiTac2, string diaChiDoiTac3, string diaChiDoiTac4, string maNuocDoiTac, string soHouseAWB, string soMasterAWB, decimal soLuong, decimal trongLuong, string maDDLuuKho, string tenMayBayChoHang, DateTime ngayHangDen, string maCangDoHang, string maDDNhanHangCuoiCung, string maDiaDiemXepHang, decimal tongTriGiaTinhThue, string maTTTongTriGiaTinhThue, decimal giaKhaiBao, string phanLoaiGiaHD, string maDieuKienGiaHD, string maTTHoaDon, decimal tongTriGiaHD, string maPhanLoaiPhiVC, string maTTPhiVC, decimal phiVanChuyen, string maPhanLoaiPhiBH, string maTTPhiBH, decimal phiBaoHiem, string moTaHangHoa, string maNuocXuatXu, decimal triGiaTinhThue, string ghiChu, string soQuanLyNoiBoDN, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TKTriGiaThap entity = new KDT_VNACCS_TKTriGiaThap();			
			entity.ID = id;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.SoToKhai = soToKhai;
			entity.PhanLoaiBaoCaoSua = phanLoaiBaoCaoSua;
			entity.PhanLoaiToChuc = phanLoaiToChuc;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHS = nhomXuLyHS;
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.MaBuuChinhDonVi = maBuuChinhDonVi;
			entity.DiaChiDonVi = diaChiDonVi;
			entity.SoDienThoaiDonVi = soDienThoaiDonVi;
			entity.MaDoiTac = maDoiTac;
			entity.TenDoiTac = tenDoiTac;
			entity.MaBuuChinhDoiTac = maBuuChinhDoiTac;
			entity.DiaChiDoiTac1 = diaChiDoiTac1;
			entity.DiaChiDoiTac2 = diaChiDoiTac2;
			entity.DiaChiDoiTac3 = diaChiDoiTac3;
			entity.DiaChiDoiTac4 = diaChiDoiTac4;
			entity.MaNuocDoiTac = maNuocDoiTac;
			entity.SoHouseAWB = soHouseAWB;
			entity.SoMasterAWB = soMasterAWB;
			entity.SoLuong = soLuong;
			entity.TrongLuong = trongLuong;
			entity.MaDDLuuKho = maDDLuuKho;
			entity.TenMayBayChoHang = tenMayBayChoHang;
			entity.NgayHangDen = ngayHangDen;
			entity.MaCangDoHang = maCangDoHang;
			entity.MaDDNhanHangCuoiCung = maDDNhanHangCuoiCung;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TongTriGiaTinhThue = tongTriGiaTinhThue;
			entity.MaTTTongTriGiaTinhThue = maTTTongTriGiaTinhThue;
			entity.GiaKhaiBao = giaKhaiBao;
			entity.PhanLoaiGiaHD = phanLoaiGiaHD;
			entity.MaDieuKienGiaHD = maDieuKienGiaHD;
			entity.MaTTHoaDon = maTTHoaDon;
			entity.TongTriGiaHD = tongTriGiaHD;
			entity.MaPhanLoaiPhiVC = maPhanLoaiPhiVC;
			entity.MaTTPhiVC = maTTPhiVC;
			entity.PhiVanChuyen = phiVanChuyen;
			entity.MaPhanLoaiPhiBH = maPhanLoaiPhiBH;
			entity.MaTTPhiBH = maTTPhiBH;
			entity.PhiBaoHiem = phiBaoHiem;
			entity.MoTaHangHoa = moTaHangHoa;
			entity.MaNuocXuatXu = maNuocXuatXu;
			entity.TriGiaTinhThue = triGiaTinhThue;
			entity.GhiChu = ghiChu;
			entity.SoQuanLyNoiBoDN = soQuanLyNoiBoDN;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_TKTriGiaThap_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@PhanLoaiBaoCaoSua", SqlDbType.VarChar, PhanLoaiBaoCaoSua);
			db.AddInParameter(dbCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, PhanLoaiToChuc);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHS", SqlDbType.VarChar, NhomXuLyHS);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.VarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, MaBuuChinhDonVi);
			db.AddInParameter(dbCommand, "@DiaChiDonVi", SqlDbType.NVarChar, DiaChiDonVi);
			db.AddInParameter(dbCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, SoDienThoaiDonVi);
			db.AddInParameter(dbCommand, "@MaDoiTac", SqlDbType.VarChar, MaDoiTac);
			db.AddInParameter(dbCommand, "@TenDoiTac", SqlDbType.NVarChar, TenDoiTac);
			db.AddInParameter(dbCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, MaBuuChinhDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, DiaChiDoiTac1);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, DiaChiDoiTac2);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, DiaChiDoiTac3);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, DiaChiDoiTac4);
			db.AddInParameter(dbCommand, "@MaNuocDoiTac", SqlDbType.VarChar, MaNuocDoiTac);
			db.AddInParameter(dbCommand, "@SoHouseAWB", SqlDbType.VarChar, SoHouseAWB);
			db.AddInParameter(dbCommand, "@SoMasterAWB", SqlDbType.VarChar, SoMasterAWB);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@MaDDLuuKho", SqlDbType.VarChar, MaDDLuuKho);
			db.AddInParameter(dbCommand, "@TenMayBayChoHang", SqlDbType.NVarChar, TenMayBayChoHang);
			db.AddInParameter(dbCommand, "@NgayHangDen", SqlDbType.DateTime, NgayHangDen.Year <= 1753 ? DBNull.Value : (object) NgayHangDen);
			db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
			db.AddInParameter(dbCommand, "@MaDDNhanHangCuoiCung", SqlDbType.VarChar, MaDDNhanHangCuoiCung);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Decimal, TongTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTTongTriGiaTinhThue", SqlDbType.VarChar, MaTTTongTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@GiaKhaiBao", SqlDbType.Decimal, GiaKhaiBao);
			db.AddInParameter(dbCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, PhanLoaiGiaHD);
			db.AddInParameter(dbCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, MaDieuKienGiaHD);
			db.AddInParameter(dbCommand, "@MaTTHoaDon", SqlDbType.VarChar, MaTTHoaDon);
			db.AddInParameter(dbCommand, "@TongTriGiaHD", SqlDbType.Decimal, TongTriGiaHD);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, MaPhanLoaiPhiVC);
			db.AddInParameter(dbCommand, "@MaTTPhiVC", SqlDbType.VarChar, MaTTPhiVC);
			db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Decimal, PhiVanChuyen);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, MaPhanLoaiPhiBH);
			db.AddInParameter(dbCommand, "@MaTTPhiBH", SqlDbType.VarChar, MaTTPhiBH);
			db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Decimal, PhiBaoHiem);
			db.AddInParameter(dbCommand, "@MoTaHangHoa", SqlDbType.NVarChar, MoTaHangHoa);
			db.AddInParameter(dbCommand, "@MaNuocXuatXu", SqlDbType.VarChar, MaNuocXuatXu);
			db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Decimal, TriGiaTinhThue);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, SoQuanLyNoiBoDN);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.NChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_TKTriGiaThap> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TKTriGiaThap item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_TKTriGiaThap(long id, string maLoaiHinh, decimal soToKhai, string phanLoaiBaoCaoSua, string phanLoaiToChuc, string coQuanHaiQuan, string nhomXuLyHS, string maDonVi, string tenDonVi, string maBuuChinhDonVi, string diaChiDonVi, string soDienThoaiDonVi, string maDoiTac, string tenDoiTac, string maBuuChinhDoiTac, string diaChiDoiTac1, string diaChiDoiTac2, string diaChiDoiTac3, string diaChiDoiTac4, string maNuocDoiTac, string soHouseAWB, string soMasterAWB, decimal soLuong, decimal trongLuong, string maDDLuuKho, string tenMayBayChoHang, DateTime ngayHangDen, string maCangDoHang, string maDDNhanHangCuoiCung, string maDiaDiemXepHang, decimal tongTriGiaTinhThue, string maTTTongTriGiaTinhThue, decimal giaKhaiBao, string phanLoaiGiaHD, string maDieuKienGiaHD, string maTTHoaDon, decimal tongTriGiaHD, string maPhanLoaiPhiVC, string maTTPhiVC, decimal phiVanChuyen, string maPhanLoaiPhiBH, string maTTPhiBH, decimal phiBaoHiem, string moTaHangHoa, string maNuocXuatXu, decimal triGiaTinhThue, string ghiChu, string soQuanLyNoiBoDN, string trangThaiXuLy, string inputMessageID, string messageTag, string indexTag)
		{
			KDT_VNACCS_TKTriGiaThap entity = new KDT_VNACCS_TKTriGiaThap();			
			entity.ID = id;
			entity.MaLoaiHinh = maLoaiHinh;
			entity.SoToKhai = soToKhai;
			entity.PhanLoaiBaoCaoSua = phanLoaiBaoCaoSua;
			entity.PhanLoaiToChuc = phanLoaiToChuc;
			entity.CoQuanHaiQuan = coQuanHaiQuan;
			entity.NhomXuLyHS = nhomXuLyHS;
			entity.MaDonVi = maDonVi;
			entity.TenDonVi = tenDonVi;
			entity.MaBuuChinhDonVi = maBuuChinhDonVi;
			entity.DiaChiDonVi = diaChiDonVi;
			entity.SoDienThoaiDonVi = soDienThoaiDonVi;
			entity.MaDoiTac = maDoiTac;
			entity.TenDoiTac = tenDoiTac;
			entity.MaBuuChinhDoiTac = maBuuChinhDoiTac;
			entity.DiaChiDoiTac1 = diaChiDoiTac1;
			entity.DiaChiDoiTac2 = diaChiDoiTac2;
			entity.DiaChiDoiTac3 = diaChiDoiTac3;
			entity.DiaChiDoiTac4 = diaChiDoiTac4;
			entity.MaNuocDoiTac = maNuocDoiTac;
			entity.SoHouseAWB = soHouseAWB;
			entity.SoMasterAWB = soMasterAWB;
			entity.SoLuong = soLuong;
			entity.TrongLuong = trongLuong;
			entity.MaDDLuuKho = maDDLuuKho;
			entity.TenMayBayChoHang = tenMayBayChoHang;
			entity.NgayHangDen = ngayHangDen;
			entity.MaCangDoHang = maCangDoHang;
			entity.MaDDNhanHangCuoiCung = maDDNhanHangCuoiCung;
			entity.MaDiaDiemXepHang = maDiaDiemXepHang;
			entity.TongTriGiaTinhThue = tongTriGiaTinhThue;
			entity.MaTTTongTriGiaTinhThue = maTTTongTriGiaTinhThue;
			entity.GiaKhaiBao = giaKhaiBao;
			entity.PhanLoaiGiaHD = phanLoaiGiaHD;
			entity.MaDieuKienGiaHD = maDieuKienGiaHD;
			entity.MaTTHoaDon = maTTHoaDon;
			entity.TongTriGiaHD = tongTriGiaHD;
			entity.MaPhanLoaiPhiVC = maPhanLoaiPhiVC;
			entity.MaTTPhiVC = maTTPhiVC;
			entity.PhiVanChuyen = phiVanChuyen;
			entity.MaPhanLoaiPhiBH = maPhanLoaiPhiBH;
			entity.MaTTPhiBH = maTTPhiBH;
			entity.PhiBaoHiem = phiBaoHiem;
			entity.MoTaHangHoa = moTaHangHoa;
			entity.MaNuocXuatXu = maNuocXuatXu;
			entity.TriGiaTinhThue = triGiaTinhThue;
			entity.GhiChu = ghiChu;
			entity.SoQuanLyNoiBoDN = soQuanLyNoiBoDN;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.InputMessageID = inputMessageID;
			entity.MessageTag = messageTag;
			entity.IndexTag = indexTag;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TKTriGiaThap_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@MaLoaiHinh", SqlDbType.VarChar, MaLoaiHinh);
			db.AddInParameter(dbCommand, "@SoToKhai", SqlDbType.Decimal, SoToKhai);
			db.AddInParameter(dbCommand, "@PhanLoaiBaoCaoSua", SqlDbType.VarChar, PhanLoaiBaoCaoSua);
			db.AddInParameter(dbCommand, "@PhanLoaiToChuc", SqlDbType.VarChar, PhanLoaiToChuc);
			db.AddInParameter(dbCommand, "@CoQuanHaiQuan", SqlDbType.VarChar, CoQuanHaiQuan);
			db.AddInParameter(dbCommand, "@NhomXuLyHS", SqlDbType.VarChar, NhomXuLyHS);
			db.AddInParameter(dbCommand, "@MaDonVi", SqlDbType.VarChar, MaDonVi);
			db.AddInParameter(dbCommand, "@TenDonVi", SqlDbType.NVarChar, TenDonVi);
			db.AddInParameter(dbCommand, "@MaBuuChinhDonVi", SqlDbType.VarChar, MaBuuChinhDonVi);
			db.AddInParameter(dbCommand, "@DiaChiDonVi", SqlDbType.NVarChar, DiaChiDonVi);
			db.AddInParameter(dbCommand, "@SoDienThoaiDonVi", SqlDbType.VarChar, SoDienThoaiDonVi);
			db.AddInParameter(dbCommand, "@MaDoiTac", SqlDbType.VarChar, MaDoiTac);
			db.AddInParameter(dbCommand, "@TenDoiTac", SqlDbType.NVarChar, TenDoiTac);
			db.AddInParameter(dbCommand, "@MaBuuChinhDoiTac", SqlDbType.VarChar, MaBuuChinhDoiTac);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac1", SqlDbType.NVarChar, DiaChiDoiTac1);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac2", SqlDbType.NVarChar, DiaChiDoiTac2);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac3", SqlDbType.NVarChar, DiaChiDoiTac3);
			db.AddInParameter(dbCommand, "@DiaChiDoiTac4", SqlDbType.NVarChar, DiaChiDoiTac4);
			db.AddInParameter(dbCommand, "@MaNuocDoiTac", SqlDbType.VarChar, MaNuocDoiTac);
			db.AddInParameter(dbCommand, "@SoHouseAWB", SqlDbType.VarChar, SoHouseAWB);
			db.AddInParameter(dbCommand, "@SoMasterAWB", SqlDbType.VarChar, SoMasterAWB);
			db.AddInParameter(dbCommand, "@SoLuong", SqlDbType.Decimal, SoLuong);
			db.AddInParameter(dbCommand, "@TrongLuong", SqlDbType.Decimal, TrongLuong);
			db.AddInParameter(dbCommand, "@MaDDLuuKho", SqlDbType.VarChar, MaDDLuuKho);
			db.AddInParameter(dbCommand, "@TenMayBayChoHang", SqlDbType.NVarChar, TenMayBayChoHang);
			db.AddInParameter(dbCommand, "@NgayHangDen", SqlDbType.DateTime, NgayHangDen.Year <= 1753 ? DBNull.Value : (object) NgayHangDen);
			db.AddInParameter(dbCommand, "@MaCangDoHang", SqlDbType.VarChar, MaCangDoHang);
			db.AddInParameter(dbCommand, "@MaDDNhanHangCuoiCung", SqlDbType.VarChar, MaDDNhanHangCuoiCung);
			db.AddInParameter(dbCommand, "@MaDiaDiemXepHang", SqlDbType.VarChar, MaDiaDiemXepHang);
			db.AddInParameter(dbCommand, "@TongTriGiaTinhThue", SqlDbType.Decimal, TongTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@MaTTTongTriGiaTinhThue", SqlDbType.VarChar, MaTTTongTriGiaTinhThue);
			db.AddInParameter(dbCommand, "@GiaKhaiBao", SqlDbType.Decimal, GiaKhaiBao);
			db.AddInParameter(dbCommand, "@PhanLoaiGiaHD", SqlDbType.VarChar, PhanLoaiGiaHD);
			db.AddInParameter(dbCommand, "@MaDieuKienGiaHD", SqlDbType.VarChar, MaDieuKienGiaHD);
			db.AddInParameter(dbCommand, "@MaTTHoaDon", SqlDbType.VarChar, MaTTHoaDon);
			db.AddInParameter(dbCommand, "@TongTriGiaHD", SqlDbType.Decimal, TongTriGiaHD);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiVC", SqlDbType.VarChar, MaPhanLoaiPhiVC);
			db.AddInParameter(dbCommand, "@MaTTPhiVC", SqlDbType.VarChar, MaTTPhiVC);
			db.AddInParameter(dbCommand, "@PhiVanChuyen", SqlDbType.Decimal, PhiVanChuyen);
			db.AddInParameter(dbCommand, "@MaPhanLoaiPhiBH", SqlDbType.VarChar, MaPhanLoaiPhiBH);
			db.AddInParameter(dbCommand, "@MaTTPhiBH", SqlDbType.VarChar, MaTTPhiBH);
			db.AddInParameter(dbCommand, "@PhiBaoHiem", SqlDbType.Decimal, PhiBaoHiem);
			db.AddInParameter(dbCommand, "@MoTaHangHoa", SqlDbType.NVarChar, MoTaHangHoa);
			db.AddInParameter(dbCommand, "@MaNuocXuatXu", SqlDbType.VarChar, MaNuocXuatXu);
			db.AddInParameter(dbCommand, "@TriGiaTinhThue", SqlDbType.Decimal, TriGiaTinhThue);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			db.AddInParameter(dbCommand, "@SoQuanLyNoiBoDN", SqlDbType.VarChar, SoQuanLyNoiBoDN);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.NChar, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@InputMessageID", SqlDbType.VarChar, InputMessageID);
			db.AddInParameter(dbCommand, "@MessageTag", SqlDbType.VarChar, MessageTag);
			db.AddInParameter(dbCommand, "@IndexTag", SqlDbType.VarChar, IndexTag);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_TKTriGiaThap> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TKTriGiaThap item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_TKTriGiaThap(long id)
		{
			KDT_VNACCS_TKTriGiaThap entity = new KDT_VNACCS_TKTriGiaThap();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TKTriGiaThap_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_TKTriGiaThap_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_TKTriGiaThap> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_TKTriGiaThap item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}