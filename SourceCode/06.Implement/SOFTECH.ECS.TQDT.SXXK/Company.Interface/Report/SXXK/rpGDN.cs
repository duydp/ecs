﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using System.Data;
using Company.BLL.DuLieuChuan;

namespace Company.Interface.Report.SXXK
{
    public partial class rpGDN : DevExpress.XtraReports.UI.XtraReport
    {
        ToKhaiMauDich TKMD = null;
        public rpGDN(ToKhaiMauDich tkmd)
        {
            InitializeComponent();
            TKMD = tkmd;
            lbl1.Text = GlobalSettings.TEN_DON_VI.ToUpper();
            lbl6.Text = tkmd.SoHopDong;
            lbl7.Text = tkmd.DKGH_ID;
            lbl14.Text = tkmd.TenDonViDoiTac;
            lbl15.Text = tkmd.SoHopDong;
            lbl16.Text = tkmd.NgayHopDong.ToString("dd/MM/yyyy");
            lbl17.Text = tkmd.SoHoaDonThuongMai;
            lbl18.Text = tkmd.NgayHoaDonThuongMai.ToString("dd/MM/yyyy");

            lbl19.Text = tkmd.SoToKhai.ToString();

            lbl22.Text = tkmd.SoVanDon;
            lbl24.Text = tkmd.NgayVanDon.ToString("dd/MM/yyyy");
            this.dispReport();
        }

        private void dispReport()
        {
            this.DataSource = this.createData();
            lblSTT.DataBindings.Add("Text", null, "STT");
            lblTenNPL.DataBindings.Add("Text", null, "TenNPL");
            lblSoLuong.DataBindings.Add("Text", null, "SoLuong");
            lblDVT.DataBindings.Add("Text", null, "DVT");
            lblSokien.DataBindings.Add("Text", null, "SoKien");
            lblTrongLuong.DataBindings.Add("Text", null, "TrongLuong");
            lblThanhTien.DataBindings.Add("Text", null, "ThanhTien");
            lblGhiChu.DataBindings.Add("Text", null, "GhiChu");

        }

        private DataTable createData()
        {
            DataTable dt = new DataTable();
            TKMD.LoadHMDCollection();
            HangMauDichCollection hmdColl = TKMD.HMDCollection;
            dt.Columns.Add("STT", Type.GetType("System.String"));
            dt.Columns.Add("TenNPL", Type.GetType("System.String"));
            dt.Columns.Add("SoLuong", Type.GetType("System.String"));
            dt.Columns.Add("DVT", Type.GetType("System.String"));
            dt.Columns.Add("SoKien", Type.GetType("System.String"));
            dt.Columns.Add("TrongLuong", Type.GetType("System.String"));
            dt.Columns.Add("ThanhTien", Type.GetType("System.String"));
            dt.Columns.Add("GhiChu", Type.GetType("System.String"));
            foreach (HangMauDich hmd in hmdColl)
            {
                DataRow dr = dt.NewRow();
                dr["STT"] = "01";
                dr["TenNPL"] = hmd.TenHang;
                dr["SoLuong"] = hmd.SoLuong.ToString("N"+GlobalSettings.SoThapPhan.LuongNPL);
                dr["DVT"] = DonViTinh.GetName(hmd.DVT_ID);
                dr["SoKien"] = TKMD.SoKien;
                dr["TrongLuong"] = "";
                dr["ThanhTien"] = hmd.TriGiaKB.ToString("N2"); ;
                dr["GhiChu"] = "Hàng rời";

                dt.Rows.Add(dr);
            }

            return dt;
        }

        private void lbl_PreviewDoubleClick(object sender, PreviewMouseEventArgs e)
        {
            if (sender.GetType().ToString() == "DevExpress.XtraReports.UI.XRLabel")
            {
                XRLabel lbl = (XRLabel)sender;
                DoiNoiDung obj = new DoiNoiDung(lbl.Text);
                obj.ShowDialog();
                if (obj.NoiDungMoi != "")
                {
                    lbl.Text = obj.NoiDungMoi.Replace(@"\n", "\n");
                    this.CreateDocument();
                }
            }
            else if (sender.GetType().ToString() == "DevExpress.XtraReports.UI.XRTableCell")
            {
                XRTableCell cell = (XRTableCell)sender;
                DoiNoiDung obj = new DoiNoiDung(cell.Text);
                obj.ShowDialog();
                if (obj.NoiDungMoi != "")
                {
                    cell.Text = obj.NoiDungMoi.Replace(@"\n", "\n");
                    this.CreateDocument();
                }
            }
        }

        public void dispBackColor(bool val)
        {
            if (val)
            {
                lbl1.BackColor = Color.Silver;
                //lbl2.BackColor = Color.Silver;
                lbl3.BackColor = Color.Silver;
                lbl4.BackColor = Color.Silver;
                lbl5.BackColor = Color.Silver;
                lbl6.BackColor = Color.Silver;
                lbl7.BackColor = Color.Silver;
                lbl8.BackColor = Color.Silver;
                lbl9.BackColor = Color.Silver;
                lbl10.BackColor = Color.Silver;
                lbl11.BackColor = Color.Silver;
                lbl12.BackColor = Color.Silver;
                lbl13.BackColor = Color.Silver;
                lbl14.BackColor = Color.Silver;
                lbl15.BackColor = Color.Silver;
                lbl16.BackColor = Color.Silver;
                lbl17.BackColor = Color.Silver;
                lbl18.BackColor = Color.Silver;
                lbl19.BackColor = Color.Silver;
                lbl20.BackColor = Color.Silver;
                lbl21.BackColor = Color.Silver;
                lbl22.BackColor = Color.Silver;
                lbl23.BackColor = Color.Silver;
                lbl24.BackColor = Color.Silver;
                lbl25.BackColor = Color.Silver;
                lbl26.BackColor = Color.Silver;
            }
            else
            {
                lbl1.BackColor = Color.Transparent;
                //lbl2.BackColor = Color.Transparent;
                lbl3.BackColor = Color.Transparent;
                lbl4.BackColor = Color.Transparent;
                lbl5.BackColor = Color.Transparent;
                lbl6.BackColor = Color.Transparent;
                lbl7.BackColor = Color.Transparent;
                lbl8.BackColor = Color.Transparent;
                lbl9.BackColor = Color.Transparent;
                lbl10.BackColor = Color.Transparent;
                lbl11.BackColor = Color.Transparent;
                lbl12.BackColor = Color.Transparent;
                lbl13.BackColor = Color.Transparent;
                lbl14.BackColor = Color.Transparent;
                lbl15.BackColor = Color.Transparent;
                lbl16.BackColor = Color.Transparent;
                lbl17.BackColor = Color.Transparent;
                lbl18.BackColor = Color.Transparent;
                lbl19.BackColor = Color.Transparent;
                lbl20.BackColor = Color.Transparent;
                lbl21.BackColor = Color.Transparent;
                lbl22.BackColor = Color.Transparent;
                lbl23.BackColor = Color.Transparent;
                lbl24.BackColor = Color.Transparent;
                lbl25.BackColor = Color.Transparent;
                lbl26.BackColor = Color.Transparent;
            }
            this.CreateDocument();
        }
    }
}
