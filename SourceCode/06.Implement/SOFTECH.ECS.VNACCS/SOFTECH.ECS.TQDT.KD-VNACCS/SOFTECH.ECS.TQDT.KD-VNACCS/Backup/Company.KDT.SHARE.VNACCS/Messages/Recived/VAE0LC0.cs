using System;
using System.Collections.Generic;
using System.Text;
using Company.KDT.SHARE.VNACCS;
using System.Reflection;
using Company.KDT.SHARE.VNACCS.ClassVNACC;

namespace Company.KDT.SHARE.VNACCS
{

    public partial class VAE0LC0 : BasicVNACC
    {
        public List<VAE0LC0_HANG> HangMD { get; set; }
        public void LoadVAE0LC0(string strResult)
        {
            try
            {

                this.GetObject<VAE0LC0>(strResult, true, GlobalVNACC.PathConfig, EnumNghiepVuPhanHoi.VAE0LC0, false, VAE0LC0.TongSoByte);
                strResult = HelperVNACCS.SubStringByBytes(strResult, VAE0LC0.TongSoByte);
                while (true)
                {
                    VAE0LC0_HANG vad1agHang = new VAE0LC0_HANG();
                    vad1agHang.GetObject<VAE0LC0_HANG>(strResult, true, GlobalVNACC.PathConfig, "VAE0LC0_HANG", false, VAE0LC0_HANG.TongSoByte);
                    if (this.HangMD == null) this.HangMD = new List<VAE0LC0_HANG>();
                    this.HangMD.Add(vad1agHang);
                    if (UTF8Encoding.UTF8.GetBytes(strResult).Length > VAE0LC0_HANG.TongSoByte)
                    {
                        if (UTF8Encoding.UTF8.GetBytes(strResult).Length - VAE0LC0_HANG.TongSoByte > 4)
                        {
                            strResult = HelperVNACCS.SubStringByBytes(strResult, VAE0LC0_HANG.TongSoByte);
                            continue;
                        }
                    }
                    break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }

        }


    }

}
