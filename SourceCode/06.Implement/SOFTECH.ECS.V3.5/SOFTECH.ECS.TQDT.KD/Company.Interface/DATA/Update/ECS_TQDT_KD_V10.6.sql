IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_UserCode') = 0
BEGIN
	ALTER TABLE dbo.[User]
	ADD VNACC_UserCode [varchar](50) NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_UserID') = 0
BEGIN
	ALTER TABLE dbo.[User]
	ADD VNACC_UserID [varchar](3) NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_UserPass') = 0
BEGIN
	ALTER TABLE dbo.[User]
	ADD VNACC_UserPass [nvarchar](200) NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_TerminalID') = 0
BEGIN
	ALTER TABLE dbo.[User]
	ADD VNACC_TerminalID [varchar](50) NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_TerminalPass') = 0
BEGIN
	ALTER TABLE dbo.[User]
	ADD VNACC_TerminalPass [nvarchar](200) NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_CA') = 0
BEGIN
	ALTER TABLE dbo.[User]
	ADD VNACC_CA [nvarchar](250) NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'VNACC_CAPublicKey') = 0
BEGIN
	ALTER TABLE dbo.[User]
	ADD VNACC_CAPublicKey [nvarchar](250) NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'isOnline') = 0
BEGIN
	ALTER TABLE dbo.[User]
	ADD isOnline BIT
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'HostName') = 0
BEGIN
	ALTER TABLE dbo.[User]
	ADD HostName VARCHAR(100) NULL
END	
GO

IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'IP') = 0
BEGIN
	ALTER TABLE dbo.[User]
	ADD IP VARCHAR(20) NULL
END	
GO


IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 'User' AND column_name = 'DataBase') = 0
BEGIN
	ALTER TABLE dbo.[User]
	ADD [DataBase] VARCHAR(200) NULL
END	
GO


/****** Object:  StoredProcedure [dbo].[CheckAndUpdateTerminalStatus]    Script Date: 11/13/2013 14:23:06 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CheckAndUpdateTerminalStatus]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[CheckAndUpdateTerminalStatus]
GO


/****** Object:  StoredProcedure [dbo].[CheckAndUpdateTerminalStatus]    Script Date: 11/13/2013 14:23:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


                                CREATE PROC [dbo].[CheckAndUpdateTerminalStatus]
                                AS
                                	
                                DECLARE @user VARCHAR(100), @database VARCHAR(200)

                                DECLARE cur CURSOR FAST_FORWARD READ_ONLY FOR
                                SELECT [USER_NAME], [DataBase] FROM [User]

                                OPEN cur

                                FETCH NEXT FROM cur INTO @user, @database

                                WHILE @@FETCH_STATUS = 0
                                BEGIN

                                if(SELECT COUNT(*) from sys.dm_exec_connections c INNER JOIN sys.dm_exec_sessions s ON c.session_id = s.session_id
                                LEFT JOIN sys.sysprocesses pr ON pr.spid = c.session_id
                                where client_net_address is not null 
                                and (client_net_address = (select IP from [User] where [user_name]  = @user) OR client_net_address = '<local machine>') 
                                AND ISNULL(DB_NAME(pr.dbid), N'') = ( SELECT [DataBase] FROM [User] WHERE [DataBase] = @database and [DataBase] is not null)
                                and s.program_name = '.Net SqlClient Data Provider') > 0
                                    begin
                                        update [User] set isOnline = 1 where [user_name]  = @user
                                    end
                                else
                                    begin
                                        update [User] set isOnline = 0 where [user_name]  = @user
                                    END
                                	
                                FETCH NEXT FROM cur INTO @user, @database

                                END

                                CLOSE cur
                                DEALLOCATE cur
                                
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '10.6') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('10.6', GETDATE(), N'Bo sung thong tin cho USER (VNACCS)')
END	