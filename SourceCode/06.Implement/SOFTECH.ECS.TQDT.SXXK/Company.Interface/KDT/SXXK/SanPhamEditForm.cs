﻿using System;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.BLL.Utils;
using Company.BLL.DuLieuChuan;
using System.Data;
using System.Collections.Generic;
using Company.Controls.CustomValidation;

namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamEditForm : BaseForm
    {        
        public SanPham SPDetail;
        public SanPhamCollection SPCollection;
        public SanPhamDangKy spDangKy;
        private string MoTa;
        public SanPhamEditForm()
        {
            InitializeComponent();
            
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if(this.OpenType == OpenFormType.Insert)
                this.SPDetail = null;
            this.Close();
        }
        private bool checkSPExit(string maSP)
        {
            foreach (SanPham sp in SPCollection)
            {
                if (sp.Ma.Trim().ToUpper() == maSP.Trim().ToUpper()) return true;
            }
            return false;
        }
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            // Kiểm tra tính hợp lệ của mã HS.
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                error.SetIconPadding(txtMaHS, -8);
                //error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                }
                else
                {
                    error.SetError(txtMaHS, "HS code is invalid");
                }
                return;
            }

            if (!cvError.IsValid) return;
            if (checkSPExit(txtMa.Text.Trim()))
            {
                //ShowMessage("Sản phẩm này đã được khai báo trên lưới.", false);
                MLMessages("Sản phẩm này đã được khai báo trên lưới.", "MSG_PUB10", "", false);
                return;
            }
            //if (SanPham.checkNPLExit(txtMa.Text.Trim(),spDangKy.ID))
            //{
            //    //ShowMessage("Sản phẩm này đã được khai báo trong danh sách khác.", false);
            //    MLMessages("Sản phẩm này đã được khai báo trong danh sách khác.", "MSG_PUB10", "", false);
            //    return;
            //}
            if (this.OpenType == OpenFormType.Insert)
            {
                // Kiểm tra xem SP(SXXK) đã tồn tại hay chưa?
                BLL.SXXK.SanPham spSXXK = new BLL.SXXK.SanPham();
                spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                spSXXK.MaHaiQuan = this.MaHaiQuan.Trim().Trim();
                spSXXK.Ma = txtMa.Text.Trim();
                if (spSXXK.Load())
                {
                   // ShowMessage("Sản phẩm này đã được đăng ký.", false);
                    MLMessages("Sản phẩm này đã được đăng ký.", "MSG_PUB06", "", false);
                    return;
                }

                this.SPDetail = new SanPham();
                this.SPDetail.Ma = txtMa.Text.Trim();
                this.SPDetail.Ten = txtTen.Text.Trim();
                this.SPDetail.MaHS = txtMaHS.Text.Trim();
                this.SPDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            }
            else if (this.OpenType == OpenFormType.Edit)
            {
                // Kiểm tra xem SP(SXXK) đã tồn tại hay chưa?
                if (txtMa.Text.Trim().ToUpper() != SPDetail.Ma.Trim().ToUpper())
                {
                    BLL.SXXK.SanPham spSXXK = new BLL.SXXK.SanPham();
                    spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                    spSXXK.MaHaiQuan = this.MaHaiQuan.Trim().Trim();
                    spSXXK.Ma = txtMa.Text.Trim();
                    if (spSXXK.Load())
                    {
                        //ShowMessage("Sản phẩm này đã được đăng ký.", false);
                        MLMessages("Sản phẩm này đã được đăng ký.", "MSG_PUB06", "", false);
                        return;
                    }
                }
                //if (this.SPDetail.ID > 0)
                {
                    string MaCu = SPDetail.Ma;
                    string maSP = txtMa.Text.Trim();
                    string tenSP = txtTen.Text.Trim();
                    string mahs = txtMaHS.Text.Trim();
                    string dvt = cbDonViTinh.SelectedValue.ToString();
                    if (maSP != SPDetail.Ma || tenSP != SPDetail.Ten || SPDetail.MaHS != mahs || SPDetail.DVT_ID != dvt)
                    {
                        this.SPDetail.Ma = txtMa.Text.Trim();
                        this.SPDetail.Ten = txtTen.Text.Trim();
                        this.SPDetail.MaHS = txtMaHS.Text.Trim();
                        this.SPDetail.DVT_ID = cbDonViTinh.SelectedValue.ToString();                       
                        if (SPDetail.ID > 0)
                        {
                            this.SPDetail.Update();
                            BLL.SXXK.SanPham spSXXK = new BLL.SXXK.SanPham();
                            spSXXK.MaDoanhNghiep = this.MaDoanhNghiep;
                            spSXXK.MaHaiQuan = this.MaHaiQuan.Trim().Trim();
                            spSXXK.Ma = MaCu;
                            spSXXK.Delete();

                            spSXXK.Ma = SPDetail.Ma;
                            spSXXK.MaHS = SPDetail.MaHS;
                            spSXXK.Ten = SPDetail.Ten;
                            spSXXK.DVT_ID = SPDetail.DVT_ID;
                            spSXXK.Insert();

                            //DATLMQ bổ sung lưu Log sửa đổi ngày 06/06/2011
                            try
                            {
                                string where = "1 = 1";
                                where += string.Format(" AND ID_DK = {0} AND LoaiKhaiBao = '{1}'", SPDetail.Master_ID, Company.KDT.SHARE.Components.LoaiKhaiBao.SanPham);
                                List<Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao> listLog = Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.SelectCollectionDynamic(where, "");
                                if (listLog.Count > 0)
                                {
                                    long idLog = listLog[0].IDLog;
                                    string loaiKhaiBao = listLog[0].LoaiKhaiBao;
                                    long idDK = listLog[0].ID_DK;
                                    string guidstr = listLog[0].GUIDSTR_DK;
                                    string userKhaiBao = listLog[0].UserNameKhaiBao;
                                    DateTime ngayKhaiBao = listLog[0].NgayKhaiBao;
                                    string userSuaDoi = GlobalSettings.UserLog;
                                    DateTime ngaySuaDoi = DateTime.Now;
                                    string ghiChu = listLog[0].GhiChu;
                                    bool isDelete = listLog[0].IsDelete;
                                    Company.KDT.SHARE.QuanLyChungTu.LogKhaiBao.UpdateLogKhaiBao(idLog, loaiKhaiBao, idDK, guidstr, userKhaiBao, ngayKhaiBao,
                                                                                                userSuaDoi, ngaySuaDoi, ghiChu, isDelete);
                                }
                            }
                            catch (Exception ex)
                            {
                                ShowMessage("Có lỗi: Không thể cập nhật user sửa đổi.\r\nChi tiết lỗi: " + ex.Message, false);
                                return;
                            }
                        }

                    }
                }
            }
            this.Close();
        }

        private void SanPhamEditForm_Load(object sender, EventArgs e)
        {
           
            System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
            DataTable dt = MaHS.SelectAll();
            foreach (DataRow dr in dt.Rows)
                col.Add(dr["HS10So"].ToString());
            txtMaHS.AutoCompleteCustomSource = col;
            txtMa.Focus();
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            cbDonViTinh.DataSource = this._DonViTinh;
            cbDonViTinh.DisplayMember = "Ten";
            cbDonViTinh.ValueMember = "ID";
            cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            
            if (this.OpenType == OpenFormType.Insert)
            {
                cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
            }
            else if (this.OpenType == OpenFormType.Edit)
            {
                if (this.SPDetail != null)
                {
                    txtMa.Text = this.SPDetail.Ma;
                    txtTen.Text = this.SPDetail.Ten;
                    txtMaHS.Text = this.SPDetail.MaHS;
                    cbDonViTinh.SelectedValue = this.SPDetail.DVT_ID;
                }
            }
        }

        private void txtMaHS_Leave(object sender, EventArgs e)
        {
            if (!MaHS.Validate(txtMaHS.Text, 10))
            {
                error.SetIconPadding(txtMaHS, -8);
               // error.SetError(txtMaHS, "Mã HS không hợp lệ.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaHS, "Mã số HS không hợp lệ.");
                }
                else
                {
                    error.SetError(txtMaHS, "HS code is invalid");
                }
            }
            else
            {
                error.SetError(txtMaHS, string.Empty);
            }
            this.MoTa = MaHS.CheckExist(txtMaHS.Text);
            if (this.MoTa == "")
            {
                error.SetIconPadding(txtMaHS, -8);
                //error.SetError(txtMaHS, "Mã HS không có trong danh mục mã HS.");
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtMaHS, "Mã HS không có trong danh mục mã HS.");
                }
                else
                {
                    error.SetError(txtMaHS, "This code does not exist in HS list.");
                }
            }
            else
            {
                error.SetError(txtMaHS, string.Empty);
            }
        }

        private void txtTen_TextChanged(object sender, EventArgs e)
        {

        }

    }
}