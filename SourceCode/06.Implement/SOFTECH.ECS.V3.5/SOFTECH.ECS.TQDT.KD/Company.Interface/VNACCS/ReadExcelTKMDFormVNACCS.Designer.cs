namespace Company.Interface
{
    partial class ReadExcelTKMDFormVNACCS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ReadExcelTKMDFormVNACCS));
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRow = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblXuatXu = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.grbThuKhac = new Janus.Windows.EditControls.UIGroupBox();
            this.txtThueSuatVAT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueTV = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueVAT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueSuatTTDB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueTTDB = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueXNK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueSuatBVMT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTriGiaTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.cbbSheetName = new Janus.Windows.EditControls.UIComboBox();
            this.txtTenHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHSColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtThueSuatTV = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDonGiaTinhThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtXuatXuColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHangColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTriGiaTinhThueS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDonGiaColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtThueSuat = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label49 = new System.Windows.Forms.Label();
            this.txtSoLuongColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDVTColumn = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbbSheetTK = new Janus.Windows.EditControls.UIComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtRowIndex = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtMaHQ = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaLH = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNgayDK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDiaDiemDohang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDiaDiemDichVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTongTriGiaHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTongSoTienLePhi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDiaDiemDoHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDDLuuKho = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNgayPhatHanhHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNgayCapPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaTTHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNgayHangDen = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNgayHoanThanhKiemTraBP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHoaDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhiVanChuyen = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhuongThucTT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDieuKienGiaHD = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenPTVC = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaNuocDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaPhuongThucVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhiBaoHiem = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNgayThayDoiDangKy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoLuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNguoiNopThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTriGiaTinhThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtNgayKhaiBaoNopThue = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtHopDong_So = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoTK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.rfvDVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvFilePath = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.txtFilePath = new Janus.Windows.GridEX.EditControls.EditBox();
            this.rfvTenSP = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTenSheet = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvDonGia = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvSoLuong = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTSXNK = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnSelectFile = new Janus.Windows.EditControls.UIButton();
            this.lblLinkExcelTemplate = new System.Windows.Forms.LinkLabel();
            this.label59 = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtGhiChu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.txtMaPhanLoaiKiemTra = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoVanDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.txtSoGP = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label64 = new System.Windows.Forms.Label();
            this.txtSoLuongCont = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label65 = new System.Windows.Forms.Label();
            this.txtNgayThongQuan = new Janus.Windows.GridEX.EditControls.EditBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbThuKhac)).BeginInit();
            this.grbThuKhac.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDonGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTSXNK)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Size = new System.Drawing.Size(1498, 645);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.Filter = "Excel 97-2003|*.xls";
            this.openFileDialog1.RestoreDirectory = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(26, 104);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 14);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cột mã hàng";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(26, 164);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 14);
            this.label3.TabIndex = 6;
            this.label3.Text = "Cột tên hàng";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(26, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 14);
            this.label4.TabIndex = 8;
            this.label4.Text = "Cột mã HS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(307, 42);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 14);
            this.label5.TabIndex = 12;
            this.label5.Text = "Cột ĐVT";
            // 
            // txtRow
            // 
            this.txtRow.DecimalDigits = 0;
            this.txtRow.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRow.Location = new System.Drawing.Point(119, 68);
            this.txtRow.MaxLength = 6;
            this.txtRow.Name = "txtRow";
            this.txtRow.Size = new System.Drawing.Size(151, 22);
            this.txtRow.TabIndex = 2;
            this.txtRow.Text = "2";
            this.txtRow.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.txtRow.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(26, 42);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 14);
            this.label6.TabIndex = 0;
            this.label6.Text = "Tên Sheet";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(307, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 14);
            this.label7.TabIndex = 14;
            this.label7.Text = "Cột số lượng";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(307, 104);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(70, 14);
            this.label8.TabIndex = 16;
            this.label8.Text = "Cột đơn giá";
            // 
            // lblXuatXu
            // 
            this.lblXuatXu.AutoSize = true;
            this.lblXuatXu.BackColor = System.Drawing.Color.Transparent;
            this.lblXuatXu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblXuatXu.Location = new System.Drawing.Point(26, 196);
            this.lblXuatXu.Name = "lblXuatXu";
            this.lblXuatXu.Size = new System.Drawing.Size(72, 14);
            this.lblXuatXu.TabIndex = 10;
            this.lblXuatXu.Text = "Cột xuất xứ";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(26, 72);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 14);
            this.label11.TabIndex = 2;
            this.label11.Text = "Dòng bắt đầu";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.grbThuKhac);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1498, 498);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Thông số cấu hình";
            // 
            // grbThuKhac
            // 
            this.grbThuKhac.Controls.Add(this.txtThueSuatVAT);
            this.grbThuKhac.Controls.Add(this.txtMaPhanLoaiKiemTra);
            this.grbThuKhac.Controls.Add(this.txtNgayThongQuan);
            this.grbThuKhac.Controls.Add(this.txtSoToKhai);
            this.grbThuKhac.Controls.Add(this.txtSoLuongCont);
            this.grbThuKhac.Controls.Add(this.txtThueTV);
            this.grbThuKhac.Controls.Add(this.txtSoGP);
            this.grbThuKhac.Controls.Add(this.txtThueVAT);
            this.grbThuKhac.Controls.Add(this.txtThueSuatTTDB);
            this.grbThuKhac.Controls.Add(this.txtThueTTDB);
            this.grbThuKhac.Controls.Add(this.label62);
            this.grbThuKhac.Controls.Add(this.label60);
            this.grbThuKhac.Controls.Add(this.txtThueXNK);
            this.grbThuKhac.Controls.Add(this.txtThueSuatBVMT);
            this.grbThuKhac.Controls.Add(this.txtTriGiaTT);
            this.grbThuKhac.Controls.Add(this.cbbSheetName);
            this.grbThuKhac.Controls.Add(this.txtTenHangColumn);
            this.grbThuKhac.Controls.Add(this.txtMaHSColumn);
            this.grbThuKhac.Controls.Add(this.txtThueSuatTV);
            this.grbThuKhac.Controls.Add(this.txtDonGiaTinhThue);
            this.grbThuKhac.Controls.Add(this.txtXuatXuColumn);
            this.grbThuKhac.Controls.Add(this.txtSoVanDon);
            this.grbThuKhac.Controls.Add(this.txtGhiChu);
            this.grbThuKhac.Controls.Add(this.txtMaHangColumn);
            this.grbThuKhac.Controls.Add(this.txtTriGiaTinhThueS);
            this.grbThuKhac.Controls.Add(this.txtDonGiaColumn);
            this.grbThuKhac.Controls.Add(this.label61);
            this.grbThuKhac.Controls.Add(this.label65);
            this.grbThuKhac.Controls.Add(this.label6);
            this.grbThuKhac.Controls.Add(this.label59);
            this.grbThuKhac.Controls.Add(this.label7);
            this.grbThuKhac.Controls.Add(this.label64);
            this.grbThuKhac.Controls.Add(this.label63);
            this.grbThuKhac.Controls.Add(this.label58);
            this.grbThuKhac.Controls.Add(this.label57);
            this.grbThuKhac.Controls.Add(this.label56);
            this.grbThuKhac.Controls.Add(this.label55);
            this.grbThuKhac.Controls.Add(this.label54);
            this.grbThuKhac.Controls.Add(this.label53);
            this.grbThuKhac.Controls.Add(this.label52);
            this.grbThuKhac.Controls.Add(this.label51);
            this.grbThuKhac.Controls.Add(this.label15);
            this.grbThuKhac.Controls.Add(this.txtThueSuat);
            this.grbThuKhac.Controls.Add(this.label49);
            this.grbThuKhac.Controls.Add(this.label8);
            this.grbThuKhac.Controls.Add(this.txtRow);
            this.grbThuKhac.Controls.Add(this.txtSoLuongColumn);
            this.grbThuKhac.Controls.Add(this.label5);
            this.grbThuKhac.Controls.Add(this.lblXuatXu);
            this.grbThuKhac.Controls.Add(this.txtDVTColumn);
            this.grbThuKhac.Controls.Add(this.label4);
            this.grbThuKhac.Controls.Add(this.label2);
            this.grbThuKhac.Controls.Add(this.label50);
            this.grbThuKhac.Controls.Add(this.label1);
            this.grbThuKhac.Controls.Add(this.label3);
            this.grbThuKhac.Controls.Add(this.label11);
            this.grbThuKhac.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grbThuKhac.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbThuKhac.Location = new System.Drawing.Point(3, 263);
            this.grbThuKhac.Name = "grbThuKhac";
            this.grbThuKhac.Size = new System.Drawing.Size(1492, 232);
            this.grbThuKhac.TabIndex = 33;
            this.grbThuKhac.Text = "Cấu hình thông tin hàng hoá";
            this.grbThuKhac.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // txtThueSuatVAT
            // 
            this.txtThueSuatVAT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueSuatVAT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatVAT.Location = new System.Drawing.Point(725, 100);
            this.txtThueSuatVAT.MaxLength = 1;
            this.txtThueSuatVAT.Name = "txtThueSuatVAT";
            this.txtThueSuatVAT.Size = new System.Drawing.Size(149, 22);
            this.txtThueSuatVAT.TabIndex = 10;
            this.txtThueSuatVAT.Text = "AS";
            this.txtThueSuatVAT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueTV
            // 
            this.txtThueTV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueTV.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueTV.Location = new System.Drawing.Point(977, 100);
            this.txtThueTV.MaxLength = 1;
            this.txtThueTV.Name = "txtThueTV";
            this.txtThueTV.Size = new System.Drawing.Size(149, 22);
            this.txtThueTV.TabIndex = 10;
            this.txtThueTV.Text = "AY";
            this.txtThueTV.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueVAT
            // 
            this.txtThueVAT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueVAT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueVAT.Location = new System.Drawing.Point(977, 68);
            this.txtThueVAT.MaxLength = 1;
            this.txtThueVAT.Name = "txtThueVAT";
            this.txtThueVAT.Size = new System.Drawing.Size(149, 22);
            this.txtThueVAT.TabIndex = 10;
            this.txtThueVAT.Text = "AX";
            this.txtThueVAT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuatTTDB
            // 
            this.txtThueSuatTTDB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueSuatTTDB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatTTDB.Location = new System.Drawing.Point(725, 68);
            this.txtThueSuatTTDB.MaxLength = 1;
            this.txtThueSuatTTDB.Name = "txtThueSuatTTDB";
            this.txtThueSuatTTDB.Size = new System.Drawing.Size(149, 22);
            this.txtThueSuatTTDB.TabIndex = 10;
            this.txtThueSuatTTDB.Text = "AR";
            this.txtThueSuatTTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueTTDB
            // 
            this.txtThueTTDB.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueTTDB.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueTTDB.Location = new System.Drawing.Point(977, 38);
            this.txtThueTTDB.MaxLength = 1;
            this.txtThueTTDB.Name = "txtThueTTDB";
            this.txtThueTTDB.Size = new System.Drawing.Size(149, 22);
            this.txtThueTTDB.TabIndex = 10;
            this.txtThueTTDB.Text = "AW";
            this.txtThueTTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueXNK
            // 
            this.txtThueXNK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueXNK.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueXNK.Location = new System.Drawing.Point(725, 192);
            this.txtThueXNK.MaxLength = 1;
            this.txtThueXNK.Name = "txtThueXNK";
            this.txtThueXNK.Size = new System.Drawing.Size(149, 22);
            this.txtThueXNK.TabIndex = 10;
            this.txtThueXNK.Text = "AV";
            this.txtThueXNK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuatBVMT
            // 
            this.txtThueSuatBVMT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueSuatBVMT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatBVMT.Location = new System.Drawing.Point(725, 160);
            this.txtThueSuatBVMT.MaxLength = 1;
            this.txtThueSuatBVMT.Name = "txtThueSuatBVMT";
            this.txtThueSuatBVMT.Size = new System.Drawing.Size(149, 22);
            this.txtThueSuatBVMT.TabIndex = 10;
            this.txtThueSuatBVMT.Text = "AU";
            this.txtThueSuatBVMT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaTT
            // 
            this.txtTriGiaTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTriGiaTT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTT.Location = new System.Drawing.Point(436, 160);
            this.txtTriGiaTT.MaxLength = 1;
            this.txtTriGiaTT.Name = "txtTriGiaTT";
            this.txtTriGiaTT.Size = new System.Drawing.Size(149, 22);
            this.txtTriGiaTT.TabIndex = 10;
            this.txtTriGiaTT.Text = "AO";
            this.txtTriGiaTT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // cbbSheetName
            // 
            this.cbbSheetName.DisplayMember = "Name";
            this.cbbSheetName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSheetName.Location = new System.Drawing.Point(119, 38);
            this.cbbSheetName.Name = "cbbSheetName";
            this.cbbSheetName.Size = new System.Drawing.Size(151, 22);
            this.cbbSheetName.TabIndex = 31;
            this.cbbSheetName.Tag = "PhuongThucThanhToan";
            this.cbbSheetName.ValueMember = "ID";
            this.cbbSheetName.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // txtTenHangColumn
            // 
            this.txtTenHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenHangColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHangColumn.Location = new System.Drawing.Point(119, 160);
            this.txtTenHangColumn.MaxLength = 1;
            this.txtTenHangColumn.Name = "txtTenHangColumn";
            this.txtTenHangColumn.Size = new System.Drawing.Size(152, 22);
            this.txtTenHangColumn.TabIndex = 4;
            this.txtTenHangColumn.Text = "AI";
            this.txtTenHangColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaHSColumn
            // 
            this.txtMaHSColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHSColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHSColumn.Location = new System.Drawing.Point(119, 128);
            this.txtMaHSColumn.MaxLength = 1;
            this.txtMaHSColumn.Name = "txtMaHSColumn";
            this.txtMaHSColumn.Size = new System.Drawing.Size(152, 22);
            this.txtMaHSColumn.TabIndex = 5;
            this.txtMaHSColumn.Text = "AH";
            this.txtMaHSColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtThueSuatTV
            // 
            this.txtThueSuatTV.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueSuatTV.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuatTV.Location = new System.Drawing.Point(725, 128);
            this.txtThueSuatTV.MaxLength = 1;
            this.txtThueSuatTV.Name = "txtThueSuatTV";
            this.txtThueSuatTV.Size = new System.Drawing.Size(149, 22);
            this.txtThueSuatTV.TabIndex = 8;
            this.txtThueSuatTV.Text = "AT";
            this.txtThueSuatTV.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtThueSuatTV.TextChanged += new System.EventHandler(this.txtXuatXuColumn_TextChanged);
            // 
            // txtDonGiaTinhThue
            // 
            this.txtDonGiaTinhThue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaTinhThue.Location = new System.Drawing.Point(436, 128);
            this.txtDonGiaTinhThue.MaxLength = 1;
            this.txtDonGiaTinhThue.Name = "txtDonGiaTinhThue";
            this.txtDonGiaTinhThue.Size = new System.Drawing.Size(149, 22);
            this.txtDonGiaTinhThue.TabIndex = 8;
            this.txtDonGiaTinhThue.Text = "AN";
            this.txtDonGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDonGiaTinhThue.TextChanged += new System.EventHandler(this.txtXuatXuColumn_TextChanged);
            // 
            // txtXuatXuColumn
            // 
            this.txtXuatXuColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtXuatXuColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtXuatXuColumn.Location = new System.Drawing.Point(119, 192);
            this.txtXuatXuColumn.MaxLength = 1;
            this.txtXuatXuColumn.Name = "txtXuatXuColumn";
            this.txtXuatXuColumn.Size = new System.Drawing.Size(152, 22);
            this.txtXuatXuColumn.TabIndex = 8;
            this.txtXuatXuColumn.Text = "AJ";
            this.txtXuatXuColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtXuatXuColumn.TextChanged += new System.EventHandler(this.txtXuatXuColumn_TextChanged);
            // 
            // txtMaHangColumn
            // 
            this.txtMaHangColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHangColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHangColumn.Location = new System.Drawing.Point(119, 100);
            this.txtMaHangColumn.MaxLength = 1;
            this.txtMaHangColumn.Name = "txtMaHangColumn";
            this.txtMaHangColumn.Size = new System.Drawing.Size(151, 22);
            this.txtMaHangColumn.TabIndex = 3;
            this.txtMaHangColumn.Text = "AG";
            this.txtMaHangColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaTinhThueS
            // 
            this.txtTriGiaTinhThueS.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTriGiaTinhThueS.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThueS.Location = new System.Drawing.Point(436, 192);
            this.txtTriGiaTinhThueS.MaxLength = 1;
            this.txtTriGiaTinhThueS.Name = "txtTriGiaTinhThueS";
            this.txtTriGiaTinhThueS.Size = new System.Drawing.Size(149, 22);
            this.txtTriGiaTinhThueS.TabIndex = 9;
            this.txtTriGiaTinhThueS.Text = "AP";
            this.txtTriGiaTinhThueS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDonGiaColumn
            // 
            this.txtDonGiaColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDonGiaColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDonGiaColumn.Location = new System.Drawing.Point(436, 100);
            this.txtDonGiaColumn.MaxLength = 1;
            this.txtDonGiaColumn.Name = "txtDonGiaColumn";
            this.txtDonGiaColumn.Size = new System.Drawing.Size(149, 22);
            this.txtDonGiaColumn.TabIndex = 9;
            this.txtDonGiaColumn.Text = "AM";
            this.txtDonGiaColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(894, 104);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(56, 14);
            this.label58.TabIndex = 20;
            this.label58.Text = "Thuế TV";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(894, 72);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(64, 14);
            this.label57.TabIndex = 20;
            this.label57.Text = "Thuế VAT";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.BackColor = System.Drawing.Color.Transparent;
            this.label56.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(894, 42);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(71, 14);
            this.label56.TabIndex = 20;
            this.label56.Text = "Thuế TTĐB";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.BackColor = System.Drawing.Color.Transparent;
            this.label55.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(600, 196);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(62, 14);
            this.label55.TabIndex = 20;
            this.label55.Text = "Thuế XNK";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.BackColor = System.Drawing.Color.Transparent;
            this.label54.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(600, 164);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(119, 14);
            this.label54.TabIndex = 20;
            this.label54.Text = "Cột thuế suât BVMT";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.BackColor = System.Drawing.Color.Transparent;
            this.label53.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(600, 132);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(103, 14);
            this.label53.TabIndex = 20;
            this.label53.Text = "Cột thuế suât TV";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.BackColor = System.Drawing.Color.Transparent;
            this.label52.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(600, 104);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(111, 14);
            this.label52.TabIndex = 20;
            this.label52.Text = "Cột thuế suât VAT";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.BackColor = System.Drawing.Color.Transparent;
            this.label51.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(600, 72);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(118, 14);
            this.label51.TabIndex = 20;
            this.label51.Text = "Cột thuế suât TTĐB";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(600, 42);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(109, 14);
            this.label15.TabIndex = 20;
            this.label15.Text = "Cột thuế suât XNK";
            // 
            // txtThueSuat
            // 
            this.txtThueSuat.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtThueSuat.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtThueSuat.Location = new System.Drawing.Point(725, 38);
            this.txtThueSuat.MaxLength = 1;
            this.txtThueSuat.Name = "txtThueSuat";
            this.txtThueSuat.Size = new System.Drawing.Size(149, 22);
            this.txtThueSuat.TabIndex = 12;
            this.txtThueSuat.Text = "AQ";
            this.txtThueSuat.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.BackColor = System.Drawing.Color.Transparent;
            this.label49.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(307, 132);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(125, 14);
            this.label49.TabIndex = 16;
            this.label49.Text = "Cột đơn giá tính thuế";
            // 
            // txtSoLuongColumn
            // 
            this.txtSoLuongColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuongColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongColumn.Location = new System.Drawing.Point(436, 68);
            this.txtSoLuongColumn.MaxLength = 1;
            this.txtSoLuongColumn.Name = "txtSoLuongColumn";
            this.txtSoLuongColumn.Size = new System.Drawing.Size(149, 22);
            this.txtSoLuongColumn.TabIndex = 6;
            this.txtSoLuongColumn.Text = "AL";
            this.txtSoLuongColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDVTColumn
            // 
            this.txtDVTColumn.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDVTColumn.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDVTColumn.Location = new System.Drawing.Point(436, 38);
            this.txtDVTColumn.MaxLength = 1;
            this.txtDVTColumn.Name = "txtDVTColumn";
            this.txtDVTColumn.Size = new System.Drawing.Size(149, 22);
            this.txtDVTColumn.TabIndex = 13;
            this.txtDVTColumn.Text = "AK";
            this.txtDVTColumn.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDVTColumn.TextChanged += new System.EventHandler(this.editBox8_TextChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.BackColor = System.Drawing.Color.Transparent;
            this.label50.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(307, 196);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(115, 14);
            this.label50.TabIndex = 18;
            this.label50.Text = "Cột trị giá tính thuế";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(307, 164);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 14);
            this.label1.TabIndex = 18;
            this.label1.Text = "Cột trị giá";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.cbbSheetTK);
            this.uiGroupBox4.Controls.Add(this.label20);
            this.uiGroupBox4.Controls.Add(this.txtRowIndex);
            this.uiGroupBox4.Controls.Add(this.label47);
            this.uiGroupBox4.Controls.Add(this.label41);
            this.uiGroupBox4.Controls.Add(this.label35);
            this.uiGroupBox4.Controls.Add(this.label29);
            this.uiGroupBox4.Controls.Add(this.label48);
            this.uiGroupBox4.Controls.Add(this.label16);
            this.uiGroupBox4.Controls.Add(this.label21);
            this.uiGroupBox4.Controls.Add(this.txtMaHQ);
            this.uiGroupBox4.Controls.Add(this.txtMaLH);
            this.uiGroupBox4.Controls.Add(this.txtNgayDK);
            this.uiGroupBox4.Controls.Add(this.txtTenDonVi);
            this.uiGroupBox4.Controls.Add(this.txtTenDiaDiemDohang);
            this.uiGroupBox4.Controls.Add(this.txtDiaDiemDichVC);
            this.uiGroupBox4.Controls.Add(this.txtTongTriGiaHD);
            this.uiGroupBox4.Controls.Add(this.txtTongSoTienLePhi);
            this.uiGroupBox4.Controls.Add(this.txtMaDiaDiemDoHang);
            this.uiGroupBox4.Controls.Add(this.txtMaDDLuuKho);
            this.uiGroupBox4.Controls.Add(this.txtNgayPhatHanhHD);
            this.uiGroupBox4.Controls.Add(this.txtNgayCapPhep);
            this.uiGroupBox4.Controls.Add(this.txtMaTTHoaDon);
            this.uiGroupBox4.Controls.Add(this.txtMaDiaDiemXepHang);
            this.uiGroupBox4.Controls.Add(this.txtNgayHangDen);
            this.uiGroupBox4.Controls.Add(this.txtNgayHoanThanhKiemTraBP);
            this.uiGroupBox4.Controls.Add(this.txtSoHoaDon);
            this.uiGroupBox4.Controls.Add(this.txtPhiVanChuyen);
            this.uiGroupBox4.Controls.Add(this.txtPhuongThucTT);
            this.uiGroupBox4.Controls.Add(this.txtMaDieuKienGiaHD);
            this.uiGroupBox4.Controls.Add(this.txtTenPTVC);
            this.uiGroupBox4.Controls.Add(this.txtMaNuocDoiTac);
            this.uiGroupBox4.Controls.Add(this.txtMaPhuongThucVT);
            this.uiGroupBox4.Controls.Add(this.txtPhiBaoHiem);
            this.uiGroupBox4.Controls.Add(this.txtNgayThayDoiDangKy);
            this.uiGroupBox4.Controls.Add(this.txtSoLuong);
            this.uiGroupBox4.Controls.Add(this.txtTenDiaDiemXepHang);
            this.uiGroupBox4.Controls.Add(this.txtTrongLuong);
            this.uiGroupBox4.Controls.Add(this.txtNguoiNopThue);
            this.uiGroupBox4.Controls.Add(this.txtTriGiaTinhThue);
            this.uiGroupBox4.Controls.Add(this.txtNgayKhaiBaoNopThue);
            this.uiGroupBox4.Controls.Add(this.txtHopDong_So);
            this.uiGroupBox4.Controls.Add(this.txtTenDoiTac);
            this.uiGroupBox4.Controls.Add(this.txtMaUyThac);
            this.uiGroupBox4.Controls.Add(this.txtMDonVi);
            this.uiGroupBox4.Controls.Add(this.txtSoTK);
            this.uiGroupBox4.Controls.Add(this.label46);
            this.uiGroupBox4.Controls.Add(this.label40);
            this.uiGroupBox4.Controls.Add(this.label34);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.label14);
            this.uiGroupBox4.Controls.Add(this.label24);
            this.uiGroupBox4.Controls.Add(this.label45);
            this.uiGroupBox4.Controls.Add(this.label39);
            this.uiGroupBox4.Controls.Add(this.label33);
            this.uiGroupBox4.Controls.Add(this.label22);
            this.uiGroupBox4.Controls.Add(this.label13);
            this.uiGroupBox4.Controls.Add(this.label25);
            this.uiGroupBox4.Controls.Add(this.label44);
            this.uiGroupBox4.Controls.Add(this.label38);
            this.uiGroupBox4.Controls.Add(this.label32);
            this.uiGroupBox4.Controls.Add(this.label19);
            this.uiGroupBox4.Controls.Add(this.label12);
            this.uiGroupBox4.Controls.Add(this.label26);
            this.uiGroupBox4.Controls.Add(this.label43);
            this.uiGroupBox4.Controls.Add(this.label42);
            this.uiGroupBox4.Controls.Add(this.label37);
            this.uiGroupBox4.Controls.Add(this.label36);
            this.uiGroupBox4.Controls.Add(this.label31);
            this.uiGroupBox4.Controls.Add(this.label30);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Controls.Add(this.label17);
            this.uiGroupBox4.Controls.Add(this.label10);
            this.uiGroupBox4.Controls.Add(this.label9);
            this.uiGroupBox4.Controls.Add(this.label27);
            this.uiGroupBox4.Controls.Add(this.label28);
            this.uiGroupBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(3, 18);
            this.uiGroupBox4.Margin = new System.Windows.Forms.Padding(0);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(1492, 245);
            this.uiGroupBox4.TabIndex = 32;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // cbbSheetTK
            // 
            this.cbbSheetTK.DisplayMember = "Name";
            this.cbbSheetTK.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbbSheetTK.Location = new System.Drawing.Point(120, 22);
            this.cbbSheetTK.Name = "cbbSheetTK";
            this.cbbSheetTK.Size = new System.Drawing.Size(151, 22);
            this.cbbSheetTK.TabIndex = 31;
            this.cbbSheetTK.Tag = "PhuongThucThanhToan";
            this.cbbSheetTK.ValueMember = "ID";
            this.cbbSheetTK.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(27, 26);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(66, 14);
            this.label20.TabIndex = 0;
            this.label20.Text = "Tên Sheet";
            // 
            // txtRowIndex
            // 
            this.txtRowIndex.DecimalDigits = 0;
            this.txtRowIndex.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRowIndex.Location = new System.Drawing.Point(120, 55);
            this.txtRowIndex.MaxLength = 6;
            this.txtRowIndex.Name = "txtRowIndex";
            this.txtRowIndex.Size = new System.Drawing.Size(90, 22);
            this.txtRowIndex.TabIndex = 2;
            this.txtRowIndex.Text = "2";
            this.txtRowIndex.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.txtRowIndex.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.BackColor = System.Drawing.Color.Transparent;
            this.label47.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(1256, 59);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(127, 14);
            this.label47.TabIndex = 2;
            this.label47.Text = "Tên địa điểm dỡ hàng";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.BackColor = System.Drawing.Color.Transparent;
            this.label41.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(995, 59);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(72, 14);
            this.label41.TabIndex = 2;
            this.label41.Text = "Tổng TGHD";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(781, 59);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 14);
            this.label35.TabIndex = 2;
            this.label35.Text = "Số lệ phí";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(524, 59);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(120, 14);
            this.label29.TabIndex = 2;
            this.label29.Text = "Mã địa điểm dỡ hàng";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.BackColor = System.Drawing.Color.Transparent;
            this.label48.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(278, 27);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(131, 14);
            this.label48.TabIndex = 2;
            this.label48.Text = "Mã địa điểm đích VCBT";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(278, 59);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 14);
            this.label16.TabIndex = 2;
            this.label16.Text = "Mã uỷ thác";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(27, 59);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(82, 14);
            this.label21.TabIndex = 2;
            this.label21.Text = "Dòng bắt đầu";
            // 
            // txtMaHQ
            // 
            this.txtMaHQ.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaHQ.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHQ.Location = new System.Drawing.Point(120, 147);
            this.txtMaHQ.MaxLength = 1;
            this.txtMaHQ.Name = "txtMaHQ";
            this.txtMaHQ.Size = new System.Drawing.Size(90, 22);
            this.txtMaHQ.TabIndex = 3;
            this.txtMaHQ.Text = "C";
            this.txtMaHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaLH
            // 
            this.txtMaLH.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaLH.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaLH.Location = new System.Drawing.Point(120, 115);
            this.txtMaLH.MaxLength = 1;
            this.txtMaLH.Name = "txtMaLH";
            this.txtMaLH.Size = new System.Drawing.Size(90, 22);
            this.txtMaLH.TabIndex = 3;
            this.txtMaLH.Text = "B";
            this.txtMaLH.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNgayDK
            // 
            this.txtNgayDK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayDK.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayDK.Location = new System.Drawing.Point(120, 178);
            this.txtNgayDK.MaxLength = 1;
            this.txtNgayDK.Name = "txtNgayDK";
            this.txtNgayDK.Size = new System.Drawing.Size(90, 22);
            this.txtNgayDK.TabIndex = 3;
            this.txtNgayDK.Text = "D";
            this.txtNgayDK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenDonVi
            // 
            this.txtTenDonVi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenDonVi.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVi.Location = new System.Drawing.Point(1388, 84);
            this.txtTenDonVi.MaxLength = 1;
            this.txtTenDonVi.Name = "txtTenDonVi";
            this.txtTenDonVi.Size = new System.Drawing.Size(87, 22);
            this.txtTenDonVi.TabIndex = 3;
            this.txtTenDonVi.Text = "AI";
            this.txtTenDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenDiaDiemDohang
            // 
            this.txtTenDiaDiemDohang.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenDiaDiemDohang.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDiaDiemDohang.Location = new System.Drawing.Point(1388, 55);
            this.txtTenDiaDiemDohang.MaxLength = 1;
            this.txtTenDiaDiemDohang.Name = "txtTenDiaDiemDohang";
            this.txtTenDiaDiemDohang.Size = new System.Drawing.Size(87, 22);
            this.txtTenDiaDiemDohang.TabIndex = 3;
            this.txtTenDiaDiemDohang.Text = "AH";
            this.txtTenDiaDiemDohang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtDiaDiemDichVC
            // 
            this.txtDiaDiemDichVC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDiaDiemDichVC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemDichVC.Location = new System.Drawing.Point(415, 23);
            this.txtDiaDiemDichVC.MaxLength = 1;
            this.txtDiaDiemDichVC.Name = "txtDiaDiemDichVC";
            this.txtDiaDiemDichVC.Size = new System.Drawing.Size(97, 22);
            this.txtDiaDiemDichVC.TabIndex = 3;
            this.txtDiaDiemDichVC.Text = "AN";
            this.txtDiaDiemDichVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongTriGiaHD
            // 
            this.txtTongTriGiaHD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTongTriGiaHD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongTriGiaHD.Location = new System.Drawing.Point(1135, 55);
            this.txtTongTriGiaHD.MaxLength = 1;
            this.txtTongTriGiaHD.Name = "txtTongTriGiaHD";
            this.txtTongTriGiaHD.Size = new System.Drawing.Size(114, 22);
            this.txtTongTriGiaHD.TabIndex = 3;
            this.txtTongTriGiaHD.Text = "Z";
            this.txtTongTriGiaHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTongSoTienLePhi
            // 
            this.txtTongSoTienLePhi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTongSoTienLePhi.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTienLePhi.Location = new System.Drawing.Point(870, 55);
            this.txtTongSoTienLePhi.MaxLength = 1;
            this.txtTongSoTienLePhi.Name = "txtTongSoTienLePhi";
            this.txtTongSoTienLePhi.Size = new System.Drawing.Size(107, 22);
            this.txtTongSoTienLePhi.TabIndex = 3;
            this.txtTongSoTienLePhi.Text = "S";
            this.txtTongSoTienLePhi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDiaDiemDoHang
            // 
            this.txtMaDiaDiemDoHang.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDiaDiemDoHang.Location = new System.Drawing.Point(650, 55);
            this.txtMaDiaDiemDoHang.MaxLength = 1;
            this.txtMaDiaDiemDoHang.Name = "txtMaDiaDiemDoHang";
            this.txtMaDiaDiemDoHang.Size = new System.Drawing.Size(113, 22);
            this.txtMaDiaDiemDoHang.TabIndex = 3;
            this.txtMaDiaDiemDoHang.Text = "M";
            this.txtMaDiaDiemDoHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDDLuuKho
            // 
            this.txtMaDDLuuKho.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaDDLuuKho.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDDLuuKho.Location = new System.Drawing.Point(1388, 210);
            this.txtMaDDLuuKho.MaxLength = 1;
            this.txtMaDDLuuKho.Name = "txtMaDDLuuKho";
            this.txtMaDDLuuKho.Size = new System.Drawing.Size(87, 22);
            this.txtMaDDLuuKho.TabIndex = 3;
            this.txtMaDDLuuKho.Text = "AM";
            this.txtMaDDLuuKho.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNgayPhatHanhHD
            // 
            this.txtNgayPhatHanhHD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayPhatHanhHD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayPhatHanhHD.Location = new System.Drawing.Point(1135, 210);
            this.txtNgayPhatHanhHD.MaxLength = 1;
            this.txtNgayPhatHanhHD.Name = "txtNgayPhatHanhHD";
            this.txtNgayPhatHanhHD.Size = new System.Drawing.Size(114, 22);
            this.txtNgayPhatHanhHD.TabIndex = 3;
            this.txtNgayPhatHanhHD.Text = "AE";
            this.txtNgayPhatHanhHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNgayCapPhep
            // 
            this.txtNgayCapPhep.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayCapPhep.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayCapPhep.Location = new System.Drawing.Point(870, 210);
            this.txtNgayCapPhep.MaxLength = 1;
            this.txtNgayCapPhep.Name = "txtNgayCapPhep";
            this.txtNgayCapPhep.Size = new System.Drawing.Size(107, 22);
            this.txtNgayCapPhep.TabIndex = 3;
            this.txtNgayCapPhep.Text = "X";
            this.txtNgayCapPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaTTHoaDon
            // 
            this.txtMaTTHoaDon.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaTTHoaDon.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaTTHoaDon.Location = new System.Drawing.Point(650, 210);
            this.txtMaTTHoaDon.MaxLength = 1;
            this.txtMaTTHoaDon.Name = "txtMaTTHoaDon";
            this.txtMaTTHoaDon.Size = new System.Drawing.Size(113, 22);
            this.txtMaTTHoaDon.TabIndex = 3;
            this.txtMaTTHoaDon.Text = "R";
            this.txtMaTTHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDiaDiemXepHang
            // 
            this.txtMaDiaDiemXepHang.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDiaDiemXepHang.Location = new System.Drawing.Point(415, 210);
            this.txtMaDiaDiemXepHang.MaxLength = 1;
            this.txtMaDiaDiemXepHang.Name = "txtMaDiaDiemXepHang";
            this.txtMaDiaDiemXepHang.Size = new System.Drawing.Size(97, 22);
            this.txtMaDiaDiemXepHang.TabIndex = 3;
            this.txtMaDiaDiemXepHang.Text = "L";
            this.txtMaDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNgayHangDen
            // 
            this.txtNgayHangDen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayHangDen.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayHangDen.Location = new System.Drawing.Point(415, 178);
            this.txtNgayHangDen.MaxLength = 1;
            this.txtNgayHangDen.Name = "txtNgayHangDen";
            this.txtNgayHangDen.Size = new System.Drawing.Size(97, 22);
            this.txtNgayHangDen.TabIndex = 3;
            this.txtNgayHangDen.Text = "J";
            this.txtNgayHangDen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNgayHoanThanhKiemTraBP
            // 
            this.txtNgayHoanThanhKiemTraBP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayHoanThanhKiemTraBP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayHoanThanhKiemTraBP.Location = new System.Drawing.Point(1388, 178);
            this.txtNgayHoanThanhKiemTraBP.MaxLength = 1;
            this.txtNgayHoanThanhKiemTraBP.Name = "txtNgayHoanThanhKiemTraBP";
            this.txtNgayHoanThanhKiemTraBP.Size = new System.Drawing.Size(87, 22);
            this.txtNgayHoanThanhKiemTraBP.TabIndex = 3;
            this.txtNgayHoanThanhKiemTraBP.Text = "AL";
            this.txtNgayHoanThanhKiemTraBP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHoaDon
            // 
            this.txtSoHoaDon.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoHoaDon.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHoaDon.Location = new System.Drawing.Point(1135, 178);
            this.txtSoHoaDon.MaxLength = 1;
            this.txtSoHoaDon.Name = "txtSoHoaDon";
            this.txtSoHoaDon.Size = new System.Drawing.Size(114, 22);
            this.txtSoHoaDon.TabIndex = 3;
            this.txtSoHoaDon.Text = "AD";
            this.txtSoHoaDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhiVanChuyen
            // 
            this.txtPhiVanChuyen.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPhiVanChuyen.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiVanChuyen.Location = new System.Drawing.Point(870, 178);
            this.txtPhiVanChuyen.MaxLength = 1;
            this.txtPhiVanChuyen.Name = "txtPhiVanChuyen";
            this.txtPhiVanChuyen.Size = new System.Drawing.Size(107, 22);
            this.txtPhiVanChuyen.TabIndex = 3;
            this.txtPhiVanChuyen.Text = "W";
            this.txtPhiVanChuyen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhuongThucTT
            // 
            this.txtPhuongThucTT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPhuongThucTT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhuongThucTT.Location = new System.Drawing.Point(650, 178);
            this.txtPhuongThucTT.MaxLength = 1;
            this.txtPhuongThucTT.Name = "txtPhuongThucTT";
            this.txtPhuongThucTT.Size = new System.Drawing.Size(113, 22);
            this.txtPhuongThucTT.TabIndex = 3;
            this.txtPhuongThucTT.Text = "Q";
            this.txtPhuongThucTT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaDieuKienGiaHD
            // 
            this.txtMaDieuKienGiaHD.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaDieuKienGiaHD.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDieuKienGiaHD.Location = new System.Drawing.Point(650, 147);
            this.txtMaDieuKienGiaHD.MaxLength = 1;
            this.txtMaDieuKienGiaHD.Name = "txtMaDieuKienGiaHD";
            this.txtMaDieuKienGiaHD.Size = new System.Drawing.Size(113, 22);
            this.txtMaDieuKienGiaHD.TabIndex = 3;
            this.txtMaDieuKienGiaHD.Text = "P";
            this.txtMaDieuKienGiaHD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenPTVC
            // 
            this.txtTenPTVC.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenPTVC.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenPTVC.Location = new System.Drawing.Point(415, 147);
            this.txtTenPTVC.MaxLength = 1;
            this.txtTenPTVC.Name = "txtTenPTVC";
            this.txtTenPTVC.Size = new System.Drawing.Size(97, 22);
            this.txtTenPTVC.TabIndex = 3;
            this.txtTenPTVC.Text = "I";
            this.txtTenPTVC.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaNuocDoiTac
            // 
            this.txtMaNuocDoiTac.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaNuocDoiTac.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaNuocDoiTac.Location = new System.Drawing.Point(650, 115);
            this.txtMaNuocDoiTac.MaxLength = 1;
            this.txtMaNuocDoiTac.Name = "txtMaNuocDoiTac";
            this.txtMaNuocDoiTac.Size = new System.Drawing.Size(113, 22);
            this.txtMaNuocDoiTac.TabIndex = 3;
            this.txtMaNuocDoiTac.Text = "O";
            this.txtMaNuocDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaPhuongThucVT
            // 
            this.txtMaPhuongThucVT.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaPhuongThucVT.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPhuongThucVT.Location = new System.Drawing.Point(415, 115);
            this.txtMaPhuongThucVT.MaxLength = 1;
            this.txtMaPhuongThucVT.Name = "txtMaPhuongThucVT";
            this.txtMaPhuongThucVT.Size = new System.Drawing.Size(97, 22);
            this.txtMaPhuongThucVT.TabIndex = 3;
            this.txtMaPhuongThucVT.Text = "H";
            this.txtMaPhuongThucVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtPhiBaoHiem
            // 
            this.txtPhiBaoHiem.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtPhiBaoHiem.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPhiBaoHiem.Location = new System.Drawing.Point(870, 147);
            this.txtPhiBaoHiem.MaxLength = 1;
            this.txtPhiBaoHiem.Name = "txtPhiBaoHiem";
            this.txtPhiBaoHiem.Size = new System.Drawing.Size(107, 22);
            this.txtPhiBaoHiem.TabIndex = 3;
            this.txtPhiBaoHiem.Text = "V";
            this.txtPhiBaoHiem.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNgayThayDoiDangKy
            // 
            this.txtNgayThayDoiDangKy.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayThayDoiDangKy.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayThayDoiDangKy.Location = new System.Drawing.Point(1388, 147);
            this.txtNgayThayDoiDangKy.MaxLength = 1;
            this.txtNgayThayDoiDangKy.Name = "txtNgayThayDoiDangKy";
            this.txtNgayThayDoiDangKy.Size = new System.Drawing.Size(87, 22);
            this.txtNgayThayDoiDangKy.TabIndex = 3;
            this.txtNgayThayDoiDangKy.Text = "AK";
            this.txtNgayThayDoiDangKy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoLuong
            // 
            this.txtSoLuong.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuong.Location = new System.Drawing.Point(1135, 147);
            this.txtSoLuong.MaxLength = 1;
            this.txtSoLuong.Name = "txtSoLuong";
            this.txtSoLuong.Size = new System.Drawing.Size(114, 22);
            this.txtSoLuong.TabIndex = 3;
            this.txtSoLuong.Text = "AC";
            this.txtSoLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenDiaDiemXepHang
            // 
            this.txtTenDiaDiemXepHang.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDiaDiemXepHang.Location = new System.Drawing.Point(1388, 115);
            this.txtTenDiaDiemXepHang.MaxLength = 1;
            this.txtTenDiaDiemXepHang.Name = "txtTenDiaDiemXepHang";
            this.txtTenDiaDiemXepHang.Size = new System.Drawing.Size(87, 22);
            this.txtTenDiaDiemXepHang.TabIndex = 3;
            this.txtTenDiaDiemXepHang.Text = "AJ";
            this.txtTenDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTrongLuong.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTrongLuong.Location = new System.Drawing.Point(1135, 115);
            this.txtTrongLuong.MaxLength = 1;
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.Size = new System.Drawing.Size(114, 22);
            this.txtTrongLuong.TabIndex = 3;
            this.txtTrongLuong.Text = "AB";
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNguoiNopThue
            // 
            this.txtNguoiNopThue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNguoiNopThue.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNguoiNopThue.Location = new System.Drawing.Point(870, 115);
            this.txtNguoiNopThue.MaxLength = 1;
            this.txtNguoiNopThue.Name = "txtNguoiNopThue";
            this.txtNguoiNopThue.Size = new System.Drawing.Size(107, 22);
            this.txtNguoiNopThue.TabIndex = 3;
            this.txtNguoiNopThue.Text = "U";
            this.txtNguoiNopThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTriGiaTinhThue
            // 
            this.txtTriGiaTinhThue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTriGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaTinhThue.Location = new System.Drawing.Point(1135, 84);
            this.txtTriGiaTinhThue.MaxLength = 1;
            this.txtTriGiaTinhThue.Name = "txtTriGiaTinhThue";
            this.txtTriGiaTinhThue.Size = new System.Drawing.Size(114, 22);
            this.txtTriGiaTinhThue.TabIndex = 3;
            this.txtTriGiaTinhThue.Text = "AA";
            this.txtTriGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtNgayKhaiBaoNopThue
            // 
            this.txtNgayKhaiBaoNopThue.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayKhaiBaoNopThue.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayKhaiBaoNopThue.Location = new System.Drawing.Point(870, 84);
            this.txtNgayKhaiBaoNopThue.MaxLength = 1;
            this.txtNgayKhaiBaoNopThue.Name = "txtNgayKhaiBaoNopThue";
            this.txtNgayKhaiBaoNopThue.Size = new System.Drawing.Size(107, 22);
            this.txtNgayKhaiBaoNopThue.TabIndex = 3;
            this.txtNgayKhaiBaoNopThue.Text = "T";
            this.txtNgayKhaiBaoNopThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtHopDong_So
            // 
            this.txtHopDong_So.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtHopDong_So.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHopDong_So.Location = new System.Drawing.Point(650, 84);
            this.txtHopDong_So.MaxLength = 1;
            this.txtHopDong_So.Name = "txtHopDong_So";
            this.txtHopDong_So.Size = new System.Drawing.Size(113, 22);
            this.txtHopDong_So.TabIndex = 3;
            this.txtHopDong_So.Text = "N";
            this.txtHopDong_So.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtHopDong_So.TextChanged += new System.EventHandler(this.txtHopDong_So_TextChanged);
            // 
            // txtTenDoiTac
            // 
            this.txtTenDoiTac.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtTenDoiTac.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDoiTac.Location = new System.Drawing.Point(415, 84);
            this.txtTenDoiTac.MaxLength = 1;
            this.txtTenDoiTac.Name = "txtTenDoiTac";
            this.txtTenDoiTac.Size = new System.Drawing.Size(97, 22);
            this.txtTenDoiTac.TabIndex = 3;
            this.txtTenDoiTac.Text = "G";
            this.txtTenDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMaUyThac
            // 
            this.txtMaUyThac.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaUyThac.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaUyThac.Location = new System.Drawing.Point(415, 55);
            this.txtMaUyThac.MaxLength = 1;
            this.txtMaUyThac.Name = "txtMaUyThac";
            this.txtMaUyThac.Size = new System.Drawing.Size(97, 22);
            this.txtMaUyThac.TabIndex = 3;
            this.txtMaUyThac.Text = "F";
            this.txtMaUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtMDonVi
            // 
            this.txtMDonVi.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMDonVi.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMDonVi.Location = new System.Drawing.Point(120, 210);
            this.txtMDonVi.MaxLength = 1;
            this.txtMDonVi.Name = "txtMDonVi";
            this.txtMDonVi.Size = new System.Drawing.Size(90, 22);
            this.txtMDonVi.TabIndex = 3;
            this.txtMDonVi.Text = "E";
            this.txtMDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoTK
            // 
            this.txtSoTK.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoTK.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTK.Location = new System.Drawing.Point(120, 84);
            this.txtSoTK.MaxLength = 1;
            this.txtSoTK.Name = "txtSoTK";
            this.txtSoTK.Size = new System.Drawing.Size(90, 22);
            this.txtSoTK.TabIndex = 3;
            this.txtSoTK.Text = "A";
            this.txtSoTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.BackColor = System.Drawing.Color.Transparent;
            this.label46.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(1256, 119);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(133, 14);
            this.label46.TabIndex = 6;
            this.label46.Text = "Tên địa điểm xếp hàng";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.BackColor = System.Drawing.Color.Transparent;
            this.label40.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(995, 119);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(75, 14);
            this.label40.TabIndex = 6;
            this.label40.Text = "Trọng lượng";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(781, 119);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(86, 14);
            this.label34.TabIndex = 6;
            this.label34.Text = "Người đăng ký";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(524, 119);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(58, 14);
            this.label23.TabIndex = 6;
            this.label23.Text = "Mã nước ";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.Color.Transparent;
            this.label14.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(278, 119);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(57, 14);
            this.label14.TabIndex = 6;
            this.label14.Text = "Mã PTVT";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(27, 119);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(40, 14);
            this.label24.TabIndex = 6;
            this.label24.Text = "Mã LH";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.BackColor = System.Drawing.Color.Transparent;
            this.label45.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(1256, 88);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(66, 14);
            this.label45.TabIndex = 4;
            this.label45.Text = "Tên đơn vị";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.BackColor = System.Drawing.Color.Transparent;
            this.label39.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(995, 88);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(60, 14);
            this.label39.TabIndex = 4;
            this.label39.Text = "Trị giá TT";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(781, 88);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(83, 14);
            this.label33.TabIndex = 4;
            this.label33.Text = "Ngày khai báo";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(524, 88);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(61, 14);
            this.label22.TabIndex = 4;
            this.label22.Text = "Hợp đồng";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.Transparent;
            this.label13.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(278, 88);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(74, 14);
            this.label13.TabIndex = 4;
            this.label13.Text = "Tên  đối tác";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(27, 88);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(40, 14);
            this.label25.TabIndex = 4;
            this.label25.Text = "Số TK";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(1256, 151);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(82, 14);
            this.label44.TabIndex = 8;
            this.label44.Text = "Ngày thay đổi";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(995, 151);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(56, 14);
            this.label38.TabIndex = 8;
            this.label38.Text = "Số lượng";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(781, 151);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(77, 14);
            this.label32.TabIndex = 8;
            this.label32.Text = "Phí bảo hiểm";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.BackColor = System.Drawing.Color.Transparent;
            this.label19.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(524, 151);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(94, 14);
            this.label19.TabIndex = 8;
            this.label19.Text = "Mã điều kiện giá";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(278, 151);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(63, 14);
            this.label12.TabIndex = 8;
            this.label12.Text = "Tên PTVC";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(27, 151);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 14);
            this.label26.TabIndex = 8;
            this.label26.Text = "Mã HQ";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(1256, 214);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(116, 14);
            this.label43.TabIndex = 30;
            this.label43.Text = "Mã địa điểm lưu kho";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(1256, 182);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(101, 14);
            this.label42.TabIndex = 14;
            this.label42.Text = "Ngày hoàn thành";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(995, 214);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(131, 14);
            this.label37.TabIndex = 30;
            this.label37.Text = "Ngày phát hàng HĐTM";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.BackColor = System.Drawing.Color.Transparent;
            this.label36.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(995, 182);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(91, 14);
            this.label36.TabIndex = 14;
            this.label36.Text = "Số hoá đơn TM";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(781, 214);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(89, 14);
            this.label31.TabIndex = 30;
            this.label31.Text = "Ngày cấp phép";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(781, 182);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(90, 14);
            this.label30.TabIndex = 14;
            this.label30.Text = "Phí vận chuyển";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(524, 214);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(83, 14);
            this.label18.TabIndex = 30;
            this.label18.Text = "Mã tiền tệ HĐ";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(524, 182);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(38, 14);
            this.label17.TabIndex = 14;
            this.label17.Text = "PTTT";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(278, 214);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(126, 14);
            this.label10.TabIndex = 30;
            this.label10.Text = "Mã địa điểm xếp hàng";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(278, 182);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(90, 14);
            this.label9.TabIndex = 14;
            this.label9.Text = "Ngày hàng đến";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(27, 214);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(49, 14);
            this.label27.TabIndex = 30;
            this.label27.Text = "Tên ĐV";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(27, 182);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(53, 14);
            this.label28.TabIndex = 14;
            this.label28.Text = "Ngày ĐK";
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(492, 28);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(109, 23);
            this.btnClose.TabIndex = 34;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSave.Location = new System.Drawing.Point(379, 28);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(107, 23);
            this.btnSave.TabIndex = 33;
            this.btnSave.Text = "Ghi";
            this.btnSave.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // rfvDVT
            // 
            this.rfvDVT.ControlToValidate = this.txtDVTColumn;
            this.rfvDVT.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvDVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDVT.Icon")));
            this.rfvDVT.Tag = "rfvDVT";
            // 
            // rfvFilePath
            // 
            this.rfvFilePath.ControlToValidate = this.txtFilePath;
            this.rfvFilePath.ErrorMessage = "Chưa chọn file Excel cần đọc";
            this.rfvFilePath.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvFilePath.Icon")));
            this.rfvFilePath.Tag = "rfvFilePath";
            // 
            // txtFilePath
            // 
            this.txtFilePath.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFilePath.Location = new System.Drawing.Point(13, 19);
            this.txtFilePath.Name = "txtFilePath";
            this.txtFilePath.ReadOnly = true;
            this.txtFilePath.Size = new System.Drawing.Size(572, 22);
            this.txtFilePath.TabIndex = 27;
            this.txtFilePath.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // rfvTenSP
            // 
            this.rfvTenSP.ControlToValidate = this.txtTenHangColumn;
            this.rfvTenSP.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvTenSP.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSP.Icon")));
            this.rfvTenSP.Tag = "rfvTenSP";
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtMaHSColumn;
            this.rfvMaHS.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // cvError
            // 
            this.cvError.HostingForm = this;
            // 
            // error
            // 
            this.error.ContainerControl = this;
            this.error.Icon = ((System.Drawing.Icon)(resources.GetObject("error.Icon")));
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaHangColumn;
            this.rfvMa.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            this.rfvMa.Tag = "rfvMa";
            // 
            // rfvTenSheet
            // 
            this.rfvTenSheet.ControlToValidate = this.cbbSheetName;
            this.rfvTenSheet.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvTenSheet.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTenSheet.Icon")));
            this.rfvTenSheet.Tag = "rfvTenSheet";
            // 
            // rfvDonGia
            // 
            this.rfvDonGia.ControlToValidate = this.txtDonGiaColumn;
            this.rfvDonGia.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvDonGia.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvDonGia.Icon")));
            this.rfvDonGia.Tag = "rfvDonGia";
            // 
            // rfvSoLuong
            // 
            this.rfvSoLuong.ControlToValidate = this.txtSoLuongColumn;
            this.rfvSoLuong.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoLuong.Icon")));
            this.rfvSoLuong.Tag = "rfvSoLuong";
            // 
            // rfvTSXNK
            // 
            this.rfvTSXNK.ControlToValidate = this.txtThueSuat;
            this.rfvTSXNK.ErrorMessage = "Thông tin này  không được để trống";
            this.rfvTSXNK.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTSXNK.Icon")));
            this.rfvTSXNK.Tag = "rfvTSXNK";
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.AutoScroll = true;
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.btnClose);
            this.uiGroupBox3.Controls.Add(this.btnSave);
            this.uiGroupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox3.Location = new System.Drawing.Point(0, 583);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(1498, 62);
            this.uiGroupBox3.TabIndex = 279;
            this.uiGroupBox3.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btnSelectFile);
            this.uiGroupBox2.Controls.Add(this.txtFilePath);
            this.uiGroupBox2.Controls.Add(this.lblLinkExcelTemplate);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 498);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(1498, 85);
            this.uiGroupBox2.TabIndex = 280;
            this.uiGroupBox2.Text = "Đường dẫn file Excel cần nhập";
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            // 
            // btnSelectFile
            // 
            this.btnSelectFile.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelectFile.Image = ((System.Drawing.Image)(resources.GetObject("btnSelectFile.Image")));
            this.btnSelectFile.ImageSize = new System.Drawing.Size(20, 20);
            this.btnSelectFile.Location = new System.Drawing.Point(591, 18);
            this.btnSelectFile.Name = "btnSelectFile";
            this.btnSelectFile.Size = new System.Drawing.Size(96, 22);
            this.btnSelectFile.TabIndex = 28;
            this.btnSelectFile.Text = "Chọn file";
            this.btnSelectFile.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnSelectFile.Click += new System.EventHandler(this.btnSelectFile_Click);
            // 
            // lblLinkExcelTemplate
            // 
            this.lblLinkExcelTemplate.AutoSize = true;
            this.lblLinkExcelTemplate.BackColor = System.Drawing.Color.Transparent;
            this.lblLinkExcelTemplate.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLinkExcelTemplate.Location = new System.Drawing.Point(10, 50);
            this.lblLinkExcelTemplate.Name = "lblLinkExcelTemplate";
            this.lblLinkExcelTemplate.Size = new System.Drawing.Size(123, 14);
            this.lblLinkExcelTemplate.TabIndex = 29;
            this.lblLinkExcelTemplate.TabStop = true;
            this.lblLinkExcelTemplate.Text = "Mở tệp tin mẫu Excel";
            this.lblLinkExcelTemplate.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblLinkExcelTemplate_LinkClicked);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.BackColor = System.Drawing.Color.Transparent;
            this.label59.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(894, 136);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(62, 14);
            this.label59.TabIndex = 20;
            this.label59.Text = "Số tờ khai";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(977, 132);
            this.txtSoToKhai.MaxLength = 1;
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.Size = new System.Drawing.Size(149, 22);
            this.txtSoToKhai.TabIndex = 10;
            this.txtSoToKhai.Text = "A";
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtGhiChu
            // 
            this.txtGhiChu.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtGhiChu.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGhiChu.Location = new System.Drawing.Point(977, 193);
            this.txtGhiChu.MaxLength = 1;
            this.txtGhiChu.Name = "txtGhiChu";
            this.txtGhiChu.Size = new System.Drawing.Size(149, 22);
            this.txtGhiChu.TabIndex = 3;
            this.txtGhiChu.Text = "X";
            this.txtGhiChu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.BackColor = System.Drawing.Color.Transparent;
            this.label60.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(894, 197);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(48, 14);
            this.label60.TabIndex = 2;
            this.label60.Text = "Ghi chú";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.BackColor = System.Drawing.Color.Transparent;
            this.label61.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(894, 168);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(68, 14);
            this.label61.TabIndex = 20;
            this.label61.Text = "Phân luồng";
            // 
            // txtMaPhanLoaiKiemTra
            // 
            this.txtMaPhanLoaiKiemTra.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtMaPhanLoaiKiemTra.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaPhanLoaiKiemTra.Location = new System.Drawing.Point(977, 164);
            this.txtMaPhanLoaiKiemTra.MaxLength = 1;
            this.txtMaPhanLoaiKiemTra.Name = "txtMaPhanLoaiKiemTra";
            this.txtMaPhanLoaiKiemTra.Size = new System.Drawing.Size(149, 22);
            this.txtMaPhanLoaiKiemTra.TabIndex = 10;
            this.txtMaPhanLoaiKiemTra.Text = "U";
            this.txtMaPhanLoaiKiemTra.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoVanDon
            // 
            this.txtSoVanDon.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoVanDon.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanDon.Location = new System.Drawing.Point(1234, 38);
            this.txtSoVanDon.MaxLength = 1;
            this.txtSoVanDon.Name = "txtSoVanDon";
            this.txtSoVanDon.Size = new System.Drawing.Size(149, 22);
            this.txtSoVanDon.TabIndex = 3;
            this.txtSoVanDon.Text = "J";
            this.txtSoVanDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(1151, 42);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(69, 14);
            this.label62.TabIndex = 2;
            this.label62.Text = "Số vận đơn";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.BackColor = System.Drawing.Color.Transparent;
            this.label63.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(1151, 71);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(78, 14);
            this.label63.TabIndex = 20;
            this.label63.Text = "Số giấy phép";
            // 
            // txtSoGP
            // 
            this.txtSoGP.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoGP.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoGP.Location = new System.Drawing.Point(1234, 67);
            this.txtSoGP.MaxLength = 1;
            this.txtSoGP.Name = "txtSoGP";
            this.txtSoGP.Size = new System.Drawing.Size(149, 22);
            this.txtSoGP.TabIndex = 10;
            this.txtSoGP.Text = "K";
            this.txtSoGP.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.BackColor = System.Drawing.Color.Transparent;
            this.label64.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(1151, 103);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(77, 14);
            this.label64.TabIndex = 20;
            this.label64.Text = "Số Container";
            // 
            // txtSoLuongCont
            // 
            this.txtSoLuongCont.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtSoLuongCont.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoLuongCont.Location = new System.Drawing.Point(1234, 99);
            this.txtSoLuongCont.MaxLength = 1;
            this.txtSoLuongCont.Name = "txtSoLuongCont";
            this.txtSoLuongCont.Size = new System.Drawing.Size(149, 22);
            this.txtSoLuongCont.TabIndex = 10;
            this.txtSoLuongCont.Text = "AD";
            this.txtSoLuongCont.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.BackColor = System.Drawing.Color.Transparent;
            this.label65.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(1151, 136);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(55, 14);
            this.label65.TabIndex = 20;
            this.label65.Text = "Ngày TQ";
            // 
            // txtNgayThongQuan
            // 
            this.txtNgayThongQuan.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNgayThongQuan.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNgayThongQuan.Location = new System.Drawing.Point(1234, 132);
            this.txtNgayThongQuan.MaxLength = 1;
            this.txtNgayThongQuan.Name = "txtNgayThongQuan";
            this.txtNgayThongQuan.Size = new System.Drawing.Size(149, 22);
            this.txtNgayThongQuan.TabIndex = 10;
            this.txtNgayThongQuan.Text = "AE";
            this.txtNgayThongQuan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // ReadExcelTKMDFormVNACCS
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(1498, 645);
            this.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReadExcelTKMDFormVNACCS";
            this.helpProvider1.SetShowHelp(this, true);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Đọc danh sách hàng hóa từ Excel";
            this.Load += new System.EventHandler(this.ReadExcelForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbThuKhac)).EndInit();
            this.grbThuKhac.ResumeLayout(false);
            this.grbThuKhac.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvFilePath)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTenSheet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvDonGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTSXNK)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRow;
        private System.Windows.Forms.Label lblXuatXu;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label11;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHSColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenHangColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHangColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtXuatXuColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtDVTColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuongColumn;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonGiaColumn;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDVT;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvFilePath;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSP;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaHS;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private System.Windows.Forms.ErrorProvider error;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTenSheet;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvDonGia;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvSoLuong;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTSXNK;
        private Janus.Windows.GridEX.EditControls.EditBox txtTriGiaTT;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueSuat;
        private System.Windows.Forms.Label label15;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private Janus.Windows.EditControls.UIGroupBox grbThuKhac;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIButton btnSelectFile;
        private Janus.Windows.GridEX.EditControls.EditBox txtFilePath;
        private System.Windows.Forms.LinkLabel lblLinkExcelTemplate;
        private Janus.Windows.EditControls.UIComboBox cbbSheetName;
        public Company.KDT.SHARE.VNACCS.Controls.ucCategory ctrMaMienGiam;
        private Janus.Windows.EditControls.UIComboBox cbbSheetTK;
        private System.Windows.Forms.Label label20;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtRowIndex;
        private System.Windows.Forms.Label label21;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTK;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaHQ;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaLH;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayDK;
        private Janus.Windows.GridEX.EditControls.EditBox txtMDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaUyThac;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDoiTac;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayHangDen;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenPTVC;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaPhuongThucVT;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDiaDiemDoHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDiaDiemXepHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtHopDong_So;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaNuocDoiTac;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhuongThucTT;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDieuKienGiaHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtTongSoTienLePhi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaTTHoaDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayKhaiBaoNopThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtNguoiNopThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayCapPhep;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhiVanChuyen;
        private Janus.Windows.GridEX.EditControls.EditBox txtPhiBaoHiem;
        private Janus.Windows.GridEX.EditControls.EditBox txtTongTriGiaHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtTrongLuong;
        private Janus.Windows.GridEX.EditControls.EditBox txtTriGiaTinhThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayPhatHanhHD;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHoaDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuong;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemDohang;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDonVi;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaDDLuuKho;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayHoanThanhKiemTraBP;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayThayDoiDangKy;
        private Janus.Windows.GridEX.EditControls.EditBox txtTenDiaDiemXepHang;
        private Janus.Windows.GridEX.EditControls.EditBox txtDiaDiemDichVC;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label48;
        private Janus.Windows.GridEX.EditControls.EditBox txtDonGiaTinhThue;
        private Janus.Windows.GridEX.EditControls.EditBox txtTriGiaTinhThueS;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueSuatTTDB;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueSuatVAT;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueSuatBVMT;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueSuatTV;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueXNK;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueTV;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueVAT;
        private Janus.Windows.GridEX.EditControls.EditBox txtThueTTDB;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label56;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoToKhai;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private Janus.Windows.GridEX.EditControls.EditBox txtGhiChu;
        private Janus.Windows.GridEX.EditControls.EditBox txtMaPhanLoaiKiemTra;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoVanDon;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoLuongCont;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoGP;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private Janus.Windows.GridEX.EditControls.EditBox txtNgayThongQuan;
        private System.Windows.Forms.Label label65;
    }
}