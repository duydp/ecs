/****** Script for SelectTopNRows command from SSMS  ******/
IF (SELECT COUNT(*) FROM dbo.t_HaiQuan_CuaKhau WHERE ID = 'D02S') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_CuaKhau
			( ID, Ten, MaCuc )
	VALUES  ( 'D02S', -- ID - varchar(6)
			  N'Chi Cục HQ Chuyển Phát Nhanh', -- Ten - nvarchar(50)
			  '02'  -- MaCuc - char(2)
			  )
END			  
go

if( (select count(*) from dbo.t_HaiQuan_Version) > 0)
	begin
		UPDATE    dbo.t_HaiQuan_Version SET [Version] ='2.2', Date = getdate()
	end
else
	begin
		insert into dbo.t_HaiQuan_Version values('2.2', getdate(), null)
	end 