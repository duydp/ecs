﻿namespace Company.Interface.KDT.GC
{
    partial class CungUngForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CungUngForm));
            this.btnXoa = new Janus.Windows.EditControls.UIButton();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.error = new System.Windows.Forms.ErrorProvider(this.components);
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btn_CapNhatPhanBo = new Janus.Windows.EditControls.UIButton();
            this.cmdMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.uiCommandBar1 = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.cmdNPLCungUng1 = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLCungUng");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedBack1 = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedBack");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdHuyKhaiBao1 = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyKhaiBao");
            this.cmdNPLCungUng = new Janus.Windows.UI.CommandBars.UICommand("cmdNPLCungUng");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.cmdFeedBack = new Janus.Windows.UI.CommandBars.UICommand("cmdFeedBack");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdTQDT = new Janus.Windows.UI.CommandBars.UICommand("cmdTQDT");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdHuyKhaiBao = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyKhaiBao");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.label3 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.clcNgayHD = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.ctrMaHaiQuan = new Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.clcNgayHetHan = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            this.clcNgayTiepNhan = new Company.KDT.SHARE.VNACCS.Controls.ucCalendar();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.dgList);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Location = new System.Drawing.Point(0, 32);
            this.grbMain.Size = new System.Drawing.Size(796, 418);
            // 
            // btnXoa
            // 
            this.btnXoa.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnXoa.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnXoa.Image = ((System.Drawing.Image)(resources.GetObject("btnXoa.Image")));
            this.btnXoa.ImageSize = new System.Drawing.Size(20, 20);
            this.btnXoa.Location = new System.Drawing.Point(632, 10);
            this.btnXoa.Name = "btnXoa";
            this.btnXoa.Size = new System.Drawing.Size(75, 23);
            this.btnXoa.TabIndex = 3;
            this.btnXoa.Text = "Xoá";
            this.btnXoa.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnXoa.VisualStyleManager = this.vsmMain;
            this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageSize = new System.Drawing.Size(20, 20);
            this.btnClose.Location = new System.Drawing.Point(715, 10);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 1;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgList.ColumnAutoResize = true;
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.Hierarchical = true;
            this.dgList.Location = new System.Drawing.Point(0, 105);
            this.dgList.Name = "dgList";
            this.dgList.RecordNavigator = true;
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.Size = new System.Drawing.Size(796, 273);
            this.dgList.TabIndex = 16;
            this.dgList.TotalRow = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.TotalRowPosition = Janus.Windows.GridEX.TotalRowPosition.BottomFixed;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // error
            // 
            this.error.ContainerControl = this;
            this.error.Icon = ((System.Drawing.Icon)(resources.GetObject("error.Icon")));
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.btn_CapNhatPhanBo);
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.btnXoa);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 378);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(796, 40);
            this.uiGroupBox2.TabIndex = 30;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btn_CapNhatPhanBo
            // 
            this.btn_CapNhatPhanBo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_CapNhatPhanBo.Image = ((System.Drawing.Image)(resources.GetObject("btn_CapNhatPhanBo.Image")));
            this.btn_CapNhatPhanBo.ImageSize = new System.Drawing.Size(20, 20);
            this.btn_CapNhatPhanBo.Location = new System.Drawing.Point(3, 10);
            this.btn_CapNhatPhanBo.Name = "btn_CapNhatPhanBo";
            this.btn_CapNhatPhanBo.Size = new System.Drawing.Size(139, 23);
            this.btn_CapNhatPhanBo.TabIndex = 4;
            this.btn_CapNhatPhanBo.Text = "Cập nhật phân bổ";
            this.btn_CapNhatPhanBo.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btn_CapNhatPhanBo.VisualStyleManager = this.vsmMain;
            this.btn_CapNhatPhanBo.Click += new System.EventHandler(this.btn_CapNhatPhanBo_Click);
            // 
            // cmdMain
            // 
            this.cmdMain.BottomRebar = this.BottomRebar1;
            this.cmdMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.cmdMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNPLCungUng,
            this.cmdSend,
            this.cmdFeedBack,
            this.cmdKetQuaXuLy,
            this.cmdTQDT,
            this.cmdSave,
            this.cmdHuyKhaiBao});
            this.cmdMain.ContainerControl = this;
            this.cmdMain.Id = new System.Guid("db3f14b4-f472-45b2-95e5-5df0dc360a22");
            this.cmdMain.LeftRebar = this.LeftRebar1;
            this.cmdMain.LockCommandBars = true;
            this.cmdMain.RightRebar = this.RightRebar1;
            this.cmdMain.TopRebar = this.TopRebar1;
            this.cmdMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmdMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmdMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmdMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 439);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(775, 0);
            // 
            // uiCommandBar1
            // 
            this.uiCommandBar1.CommandManager = this.cmdMain;
            this.uiCommandBar1.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdNPLCungUng1,
            this.cmdSave1,
            this.cmdSend1,
            this.cmdFeedBack1,
            this.cmdKetQuaXuLy1,
            this.cmdHuyKhaiBao1});
            this.uiCommandBar1.Key = "CommandBar1";
            this.uiCommandBar1.Location = new System.Drawing.Point(0, 0);
            this.uiCommandBar1.Name = "uiCommandBar1";
            this.uiCommandBar1.RowIndex = 0;
            this.uiCommandBar1.Size = new System.Drawing.Size(614, 32);
            this.uiCommandBar1.Text = "CommandBar1";
            // 
            // cmdNPLCungUng1
            // 
            this.cmdNPLCungUng1.Image = ((System.Drawing.Image)(resources.GetObject("cmdNPLCungUng1.Image")));
            this.cmdNPLCungUng1.Key = "cmdNPLCungUng";
            this.cmdNPLCungUng1.Name = "cmdNPLCungUng1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave1.Image")));
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend1.Image")));
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // cmdFeedBack1
            // 
            this.cmdFeedBack1.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedBack1.Image")));
            this.cmdFeedBack1.Key = "cmdFeedBack";
            this.cmdFeedBack1.Name = "cmdFeedBack1";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.Image = ((System.Drawing.Image)(resources.GetObject("cmdKetQuaXuLy1.Image")));
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // cmdHuyKhaiBao1
            // 
            this.cmdHuyKhaiBao1.Image = ((System.Drawing.Image)(resources.GetObject("cmdHuyKhaiBao1.Image")));
            this.cmdHuyKhaiBao1.Key = "cmdHuyKhaiBao";
            this.cmdHuyKhaiBao1.Name = "cmdHuyKhaiBao1";
            // 
            // cmdNPLCungUng
            // 
            this.cmdNPLCungUng.Image = ((System.Drawing.Image)(resources.GetObject("cmdNPLCungUng.Image")));
            this.cmdNPLCungUng.Key = "cmdNPLCungUng";
            this.cmdNPLCungUng.Name = "cmdNPLCungUng";
            this.cmdNPLCungUng.Text = "Thêm NPL";
            // 
            // cmdSend
            // 
            this.cmdSend.Image = ((System.Drawing.Image)(resources.GetObject("cmdSend.Image")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Khai báo";
            // 
            // cmdFeedBack
            // 
            this.cmdFeedBack.Image = ((System.Drawing.Image)(resources.GetObject("cmdFeedBack.Image")));
            this.cmdFeedBack.Key = "cmdFeedBack";
            this.cmdFeedBack.Name = "cmdFeedBack";
            this.cmdFeedBack.Text = "Nhận phản hồi";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý";
            // 
            // cmdTQDT
            // 
            this.cmdTQDT.Key = "cmdTQDT";
            this.cmdTQDT.Name = "cmdTQDT";
            this.cmdTQDT.Text = "Thông quan điện tử";
            // 
            // cmdSave
            // 
            this.cmdSave.Image = ((System.Drawing.Image)(resources.GetObject("cmdSave.Image")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Text = "Lưu thông tin";
            // 
            // cmdHuyKhaiBao
            // 
            this.cmdHuyKhaiBao.Image = ((System.Drawing.Image)(resources.GetObject("cmdHuyKhaiBao.Image")));
            this.cmdHuyKhaiBao.Key = "cmdHuyKhaiBao";
            this.cmdHuyKhaiBao.Name = "cmdHuyKhaiBao";
            this.cmdHuyKhaiBao.Text = "Khai báo hủy";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmdMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 28);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 411);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmdMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(775, 28);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 411);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.uiCommandBar1});
            this.TopRebar1.CommandManager = this.cmdMain;
            this.TopRebar1.Controls.Add(this.uiCommandBar1);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(796, 32);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(320, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Số tiếp nhận";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(550, 65);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 27;
            this.label6.Text = "Ngày hết hạn";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Location = new System.Drawing.Point(320, 65);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 27;
            this.label7.Text = "Ngày hợp đồng";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(12, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 27;
            this.label5.Text = "Hợp đồng ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(550, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 13);
            this.label4.TabIndex = 27;
            this.label4.Text = "Ngày tiếp nhận";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(12, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Mã hải quan";
            // 
            // clcNgayHD
            // 
            this.clcNgayHD.Location = new System.Drawing.Point(407, 61);
            this.clcNgayHD.Name = "clcNgayHD";
            this.clcNgayHD.ReadOnly = false;
            this.clcNgayHD.Size = new System.Drawing.Size(130, 21);
            this.clcNgayHD.TabIndex = 28;
            this.clcNgayHD.TagName = "";
            this.clcNgayHD.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayHD.WhereCondition = "";
            // 
            // ctrMaHaiQuan
            // 
            this.ctrMaHaiQuan.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.ctrMaHaiQuan.Appearance.Options.UseBackColor = true;
            this.ctrMaHaiQuan.CategoryType = Company.KDT.SHARE.VNACCS.ECategory.A038;
            this.ctrMaHaiQuan.Code = "";
            this.ctrMaHaiQuan.Cursor = System.Windows.Forms.Cursors.Default;
            this.ctrMaHaiQuan.ImportType = Company.KDT.SHARE.VNACCS.EImportExport.I;
            this.ctrMaHaiQuan.IsOnlyWarning = false;
            this.ctrMaHaiQuan.IsValidate = true;
            this.ctrMaHaiQuan.Location = new System.Drawing.Point(83, 25);
            this.ctrMaHaiQuan.Name = "ctrMaHaiQuan";
            this.ctrMaHaiQuan.Name_VN = "";
            this.ctrMaHaiQuan.SetOnlyWarning = false;
            this.ctrMaHaiQuan.SetValidate = false;
            this.ctrMaHaiQuan.ShowColumnCode = true;
            this.ctrMaHaiQuan.ShowColumnName = true;
            this.ctrMaHaiQuan.Size = new System.Drawing.Size(238, 21);
            this.ctrMaHaiQuan.TabIndex = 29;
            this.ctrMaHaiQuan.TagCode = "";
            this.ctrMaHaiQuan.TagName = "";
            this.ctrMaHaiQuan.Where = null;
            this.ctrMaHaiQuan.WhereCondition = "";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.AutoScroll = true;
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.txtSoHopDong);
            this.uiGroupBox1.Controls.Add(this.ctrMaHaiQuan);
            this.uiGroupBox1.Controls.Add(this.clcNgayHetHan);
            this.uiGroupBox1.Controls.Add(this.clcNgayTiepNhan);
            this.uiGroupBox1.Controls.Add(this.clcNgayHD);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label7);
            this.uiGroupBox1.Controls.Add(this.label6);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(796, 105);
            this.uiGroupBox1.TabIndex = 29;
            this.uiGroupBox1.Text = "Thông tin chung";
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Location = new System.Drawing.Point(407, 25);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.Size = new System.Drawing.Size(130, 21);
            this.txtSoTiepNhan.TabIndex = 30;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.Location = new System.Drawing.Point(83, 61);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(231, 21);
            this.txtSoHopDong.TabIndex = 30;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // clcNgayHetHan
            // 
            this.clcNgayHetHan.Location = new System.Drawing.Point(633, 61);
            this.clcNgayHetHan.Name = "clcNgayHetHan";
            this.clcNgayHetHan.ReadOnly = false;
            this.clcNgayHetHan.Size = new System.Drawing.Size(130, 21);
            this.clcNgayHetHan.TabIndex = 28;
            this.clcNgayHetHan.TagName = "";
            this.clcNgayHetHan.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayHetHan.WhereCondition = "";
            // 
            // clcNgayTiepNhan
            // 
            this.clcNgayTiepNhan.Location = new System.Drawing.Point(633, 25);
            this.clcNgayTiepNhan.Name = "clcNgayTiepNhan";
            this.clcNgayTiepNhan.ReadOnly = false;
            this.clcNgayTiepNhan.Size = new System.Drawing.Size(130, 21);
            this.clcNgayTiepNhan.TabIndex = 28;
            this.clcNgayTiepNhan.TagName = "";
            this.clcNgayTiepNhan.Value = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.clcNgayTiepNhan.WhereCondition = "";
            // 
            // CungUngForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 450);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "CungUngForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Nguyên phụ liệu cung ứng";
            this.Load += new System.EventHandler(this.CungUngForm_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.error)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmdMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiCommandBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Janus.Windows.EditControls.UIButton btnXoa;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.GridEX.GridEX dgList;
        private System.Windows.Forms.ErrorProvider error;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.UI.CommandBars.UIRebar LeftRebar1;
        private Janus.Windows.UI.CommandBars.UICommandManager cmdMain;
        private Janus.Windows.UI.CommandBars.UIRebar BottomRebar1;
        private Janus.Windows.UI.CommandBars.UICommandBar uiCommandBar1;
        private Janus.Windows.UI.CommandBars.UIRebar RightRebar1;
        private Janus.Windows.UI.CommandBars.UIRebar TopRebar1;
        private Janus.Windows.UI.CommandBars.UICommand cmdNPLCungUng1;
        private Janus.Windows.UI.CommandBars.UICommand cmdNPLCungUng;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedBack;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy;
        private Janus.Windows.UI.CommandBars.UICommand cmdTQDT;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave1;
        private Janus.Windows.UI.CommandBars.UICommand cmdSave;
        private Janus.Windows.UI.CommandBars.UICommand cmdSend1;
        private Janus.Windows.UI.CommandBars.UICommand cmdFeedBack1;
        private Janus.Windows.UI.CommandBars.UICommand cmdKetQuaXuLy1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuyKhaiBao1;
        private Janus.Windows.UI.CommandBars.UICommand cmdHuyKhaiBao;
        private Janus.Windows.EditControls.UIButton btn_CapNhatPhanBo;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Company.KDT.SHARE.VNACCS.Controls.ucCategoryAllowEmpty ctrMaHaiQuan;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHD;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoHopDong;
        private Janus.Windows.GridEX.EditControls.EditBox txtSoTiepNhan;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayHetHan;
        private Company.KDT.SHARE.VNACCS.Controls.ucCalendar clcNgayTiepNhan;
    }
}