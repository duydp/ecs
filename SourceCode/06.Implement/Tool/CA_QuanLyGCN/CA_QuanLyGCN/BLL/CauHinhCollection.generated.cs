﻿using System;
using System.Text;
using System.Collections;
using System.ComponentModel;

namespace CA_QuanLyGCN
{
    public class CauHinhCollections : CollectionBase
    {
        #region Constructor

        public CauHinhCollections() { }
        #endregion Constructor

        #region Indexer Methods

        public CauHinh this[int index]
        {
            get { return (CauHinh)List[index]; }
            set { List[index] = value; }
        }

        #endregion Indexer Methods

        #region Private Methods

        public int Add(CauHinh value)
        {
            return List.Add(value);
        }

        public void Insert(int index, CauHinh value)
        {
            List.Insert(index, value);
        }

        public void Remove(CauHinh value)
        {
            List.Remove(value);
        }

        public bool Contains(string id)
        {
            for (int i = 0; i < List.Count; i++)
            {
                CauHinh CauHinhEntity = (CauHinh)List[i];
                if (CauHinhEntity.Key.Equals(id) && (CauHinhEntity.EntityState != EntityState.Deleted))
                {
                    return true;
                }
            }
            return false;
        }

        public CauHinh SearchById(string id)
        {
            for (int i = 0; i < List.Count; i++)
            {
                CauHinh CauHinhEntity = (CauHinh)List[i];
                if ((CauHinhEntity.Key.Equals(id)) && (CauHinhEntity.EntityState != EntityState.Deleted))
                {
                    return CauHinhEntity;
                }
            }
            return null;
        }
/*
        public CauHinh SearchByTen(string name)
        {
            for (int i = 0; i < List.Count; i++)
            {
                CauHinh CauHinhEntity = (CauHinh)List[i];
                if ((CauHinhEntity.Name.ToLower().Trim().Equals(name.ToLower().Trim())) && (CauHinhEntity.EntityState != EntityState.Deleted))
                {
                    return CauHinhEntity;
                }
            }
            return null;
        }
*/
        public void Save()
        {
            for (int i = 0; i < List.Count; i++)
            {
                CauHinh CauHinhEntity = (CauHinh)List[i];
                switch (CauHinhEntity.EntityState)
                {
                    case EntityState.Deleted:
                        CauHinhEntity.Delete();
                        break;
                    case EntityState.Added:
                        CauHinhEntity.Insert();
                        break;
                    case EntityState.Updated:
                        CauHinhEntity.Update();
                        break;
                }
                CauHinhEntity.EntityState = EntityState.Unchanged;
            }

        }

        #endregion Private Methods
						
		#region Clone Method

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        public CauHinhCollections Clone()
        {
            CauHinhCollections _tmpCollections = new CauHinhCollections();

            foreach (CauHinh item in this)
            {
                _tmpCollections.Add((CauHinh)item.Clone());
            }

            return _tmpCollections;
        }

        #endregion

    }
}

