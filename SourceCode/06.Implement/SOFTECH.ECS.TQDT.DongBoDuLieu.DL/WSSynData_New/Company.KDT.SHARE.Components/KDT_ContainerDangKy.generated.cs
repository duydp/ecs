using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.Components
{
	public partial class KDT_ContainerDangKy : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public long TKMD_ID { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string MaHQ { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string GuidStr { set; get; }
		public string DeXuatKhac { set; get; }
		public string LyDoSua { set; get; }
		public DateTime ThoiGianXuatDL { set; get; }
		public int LuongTK { set; get; }
		public int TrangThaiToKhai { set; get; }
		public string CustomsStatus { set; get; }
		public string Note { set; get; }
		public string CargoPiece { set; get; }
		public string PieceUnitCode { set; get; }
		public string CargoWeight { set; get; }
		public string WeightUnitCode { set; get; }
		public string LocationControl { set; get; }
		public string CargoCtrlNo { set; get; }
		public int TransportationCode { set; get; }
		public DateTime ArrivalDeparture { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_ContainerDangKy> ConvertToCollection(IDataReader reader)
		{
			List<KDT_ContainerDangKy> collection = new List<KDT_ContainerDangKy>();
			while (reader.Read())
			{
				KDT_ContainerDangKy entity = new KDT_ContainerDangKy();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQ"))) entity.MaHQ = reader.GetString(reader.GetOrdinal("MaHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("DeXuatKhac"))) entity.DeXuatKhac = reader.GetString(reader.GetOrdinal("DeXuatKhac"));
				if (!reader.IsDBNull(reader.GetOrdinal("LyDoSua"))) entity.LyDoSua = reader.GetString(reader.GetOrdinal("LyDoSua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThoiGianXuatDL"))) entity.ThoiGianXuatDL = reader.GetDateTime(reader.GetOrdinal("ThoiGianXuatDL"));
				if (!reader.IsDBNull(reader.GetOrdinal("LuongTK"))) entity.LuongTK = reader.GetInt32(reader.GetOrdinal("LuongTK"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiToKhai"))) entity.TrangThaiToKhai = reader.GetInt32(reader.GetOrdinal("TrangThaiToKhai"));
				if (!reader.IsDBNull(reader.GetOrdinal("CustomsStatus"))) entity.CustomsStatus = reader.GetString(reader.GetOrdinal("CustomsStatus"));
				if (!reader.IsDBNull(reader.GetOrdinal("Note"))) entity.Note = reader.GetString(reader.GetOrdinal("Note"));
				if (!reader.IsDBNull(reader.GetOrdinal("CargoPiece"))) entity.CargoPiece = reader.GetString(reader.GetOrdinal("CargoPiece"));
				if (!reader.IsDBNull(reader.GetOrdinal("PieceUnitCode"))) entity.PieceUnitCode = reader.GetString(reader.GetOrdinal("PieceUnitCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("CargoWeight"))) entity.CargoWeight = reader.GetString(reader.GetOrdinal("CargoWeight"));
				if (!reader.IsDBNull(reader.GetOrdinal("WeightUnitCode"))) entity.WeightUnitCode = reader.GetString(reader.GetOrdinal("WeightUnitCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("LocationControl"))) entity.LocationControl = reader.GetString(reader.GetOrdinal("LocationControl"));
				if (!reader.IsDBNull(reader.GetOrdinal("CargoCtrlNo"))) entity.CargoCtrlNo = reader.GetString(reader.GetOrdinal("CargoCtrlNo"));
				if (!reader.IsDBNull(reader.GetOrdinal("TransportationCode"))) entity.TransportationCode = reader.GetInt32(reader.GetOrdinal("TransportationCode"));
				if (!reader.IsDBNull(reader.GetOrdinal("ArrivalDeparture"))) entity.ArrivalDeparture = reader.GetDateTime(reader.GetOrdinal("ArrivalDeparture"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_ContainerDangKy> collection, long id)
        {
            foreach (KDT_ContainerDangKy item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_ContainerDangKy VALUES(@SoTiepNhan, @NgayTiepNhan, @TKMD_ID, @TrangThaiXuLy, @MaHQ, @MaDoanhNghiep, @GuidStr, @DeXuatKhac, @LyDoSua, @ThoiGianXuatDL, @LuongTK, @TrangThaiToKhai, @CustomsStatus, @Note, @CargoPiece, @PieceUnitCode, @CargoWeight, @WeightUnitCode, @LocationControl, @CargoCtrlNo, @TransportationCode, @ArrivalDeparture)";
            string update = "UPDATE t_KDT_ContainerDangKy SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TKMD_ID = @TKMD_ID, TrangThaiXuLy = @TrangThaiXuLy, MaHQ = @MaHQ, MaDoanhNghiep = @MaDoanhNghiep, GuidStr = @GuidStr, DeXuatKhac = @DeXuatKhac, LyDoSua = @LyDoSua, ThoiGianXuatDL = @ThoiGianXuatDL, LuongTK = @LuongTK, TrangThaiToKhai = @TrangThaiToKhai, CustomsStatus = @CustomsStatus, Note = @Note, CargoPiece = @CargoPiece, PieceUnitCode = @PieceUnitCode, CargoWeight = @CargoWeight, WeightUnitCode = @WeightUnitCode, LocationControl = @LocationControl, CargoCtrlNo = @CargoCtrlNo, TransportationCode = @TransportationCode, ArrivalDeparture = @ArrivalDeparture WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ContainerDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.VarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianXuatDL", SqlDbType.DateTime, "ThoiGianXuatDL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTK", SqlDbType.Int, "LuongTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiToKhai", SqlDbType.Int, "TrangThaiToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsStatus", SqlDbType.NVarChar, "CustomsStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Note", SqlDbType.NVarChar, "Note", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CargoPiece", SqlDbType.VarChar, "CargoPiece", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PieceUnitCode", SqlDbType.VarChar, "PieceUnitCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CargoWeight", SqlDbType.VarChar, "CargoWeight", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@WeightUnitCode", SqlDbType.NVarChar, "WeightUnitCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LocationControl", SqlDbType.NVarChar, "LocationControl", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CargoCtrlNo", SqlDbType.NVarChar, "CargoCtrlNo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TransportationCode", SqlDbType.Int, "TransportationCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDeparture", SqlDbType.DateTime, "ArrivalDeparture", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.VarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianXuatDL", SqlDbType.DateTime, "ThoiGianXuatDL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTK", SqlDbType.Int, "LuongTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiToKhai", SqlDbType.Int, "TrangThaiToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsStatus", SqlDbType.NVarChar, "CustomsStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Note", SqlDbType.NVarChar, "Note", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CargoPiece", SqlDbType.VarChar, "CargoPiece", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PieceUnitCode", SqlDbType.VarChar, "PieceUnitCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CargoWeight", SqlDbType.VarChar, "CargoWeight", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@WeightUnitCode", SqlDbType.NVarChar, "WeightUnitCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LocationControl", SqlDbType.NVarChar, "LocationControl", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CargoCtrlNo", SqlDbType.NVarChar, "CargoCtrlNo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TransportationCode", SqlDbType.Int, "TransportationCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDeparture", SqlDbType.DateTime, "ArrivalDeparture", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_ContainerDangKy VALUES(@SoTiepNhan, @NgayTiepNhan, @TKMD_ID, @TrangThaiXuLy, @MaHQ, @MaDoanhNghiep, @GuidStr, @DeXuatKhac, @LyDoSua, @ThoiGianXuatDL, @LuongTK, @TrangThaiToKhai, @CustomsStatus, @Note, @CargoPiece, @PieceUnitCode, @CargoWeight, @WeightUnitCode, @LocationControl, @CargoCtrlNo, @TransportationCode, @ArrivalDeparture)";
            string update = "UPDATE t_KDT_ContainerDangKy SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, TKMD_ID = @TKMD_ID, TrangThaiXuLy = @TrangThaiXuLy, MaHQ = @MaHQ, MaDoanhNghiep = @MaDoanhNghiep, GuidStr = @GuidStr, DeXuatKhac = @DeXuatKhac, LyDoSua = @LyDoSua, ThoiGianXuatDL = @ThoiGianXuatDL, LuongTK = @LuongTK, TrangThaiToKhai = @TrangThaiToKhai, CustomsStatus = @CustomsStatus, Note = @Note, CargoPiece = @CargoPiece, PieceUnitCode = @PieceUnitCode, CargoWeight = @CargoWeight, WeightUnitCode = @WeightUnitCode, LocationControl = @LocationControl, CargoCtrlNo = @CargoCtrlNo, TransportationCode = @TransportationCode, ArrivalDeparture = @ArrivalDeparture WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ContainerDangKy WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.VarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThoiGianXuatDL", SqlDbType.DateTime, "ThoiGianXuatDL", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LuongTK", SqlDbType.Int, "LuongTK", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiToKhai", SqlDbType.Int, "TrangThaiToKhai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CustomsStatus", SqlDbType.NVarChar, "CustomsStatus", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@Note", SqlDbType.NVarChar, "Note", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CargoPiece", SqlDbType.VarChar, "CargoPiece", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@PieceUnitCode", SqlDbType.VarChar, "PieceUnitCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CargoWeight", SqlDbType.VarChar, "CargoWeight", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@WeightUnitCode", SqlDbType.NVarChar, "WeightUnitCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LocationControl", SqlDbType.NVarChar, "LocationControl", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CargoCtrlNo", SqlDbType.NVarChar, "CargoCtrlNo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TransportationCode", SqlDbType.Int, "TransportationCode", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ArrivalDeparture", SqlDbType.DateTime, "ArrivalDeparture", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.VarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.VarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DeXuatKhac", SqlDbType.NVarChar, "DeXuatKhac", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LyDoSua", SqlDbType.NVarChar, "LyDoSua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThoiGianXuatDL", SqlDbType.DateTime, "ThoiGianXuatDL", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LuongTK", SqlDbType.Int, "LuongTK", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiToKhai", SqlDbType.Int, "TrangThaiToKhai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CustomsStatus", SqlDbType.NVarChar, "CustomsStatus", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@Note", SqlDbType.NVarChar, "Note", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CargoPiece", SqlDbType.VarChar, "CargoPiece", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@PieceUnitCode", SqlDbType.VarChar, "PieceUnitCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CargoWeight", SqlDbType.VarChar, "CargoWeight", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@WeightUnitCode", SqlDbType.NVarChar, "WeightUnitCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LocationControl", SqlDbType.NVarChar, "LocationControl", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CargoCtrlNo", SqlDbType.NVarChar, "CargoCtrlNo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TransportationCode", SqlDbType.Int, "TransportationCode", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ArrivalDeparture", SqlDbType.DateTime, "ArrivalDeparture", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_ContainerDangKy Load(long id)
		{
			const string spName = "[dbo].[p_KDT_ContainerDangKy_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_ContainerDangKy> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_ContainerDangKy> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_ContainerDangKy> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_ContainerDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ContainerDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_ContainerDangKy_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ContainerDangKy_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_ContainerDangKy(long soTiepNhan, DateTime ngayTiepNhan, long tKMD_ID, int trangThaiXuLy, string maHQ, string maDoanhNghiep, string guidStr, string deXuatKhac, string lyDoSua, DateTime thoiGianXuatDL, int luongTK, int trangThaiToKhai, string customsStatus, string note, string cargoPiece, string pieceUnitCode, string cargoWeight, string weightUnitCode, string locationControl, string cargoCtrlNo, int transportationCode, DateTime arrivalDeparture)
		{
			KDT_ContainerDangKy entity = new KDT_ContainerDangKy();	
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TKMD_ID = tKMD_ID;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GuidStr = guidStr;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ThoiGianXuatDL = thoiGianXuatDL;
			entity.LuongTK = luongTK;
			entity.TrangThaiToKhai = trangThaiToKhai;
			entity.CustomsStatus = customsStatus;
			entity.Note = note;
			entity.CargoPiece = cargoPiece;
			entity.PieceUnitCode = pieceUnitCode;
			entity.CargoWeight = cargoWeight;
			entity.WeightUnitCode = weightUnitCode;
			entity.LocationControl = locationControl;
			entity.CargoCtrlNo = cargoCtrlNo;
			entity.TransportationCode = transportationCode;
			entity.ArrivalDeparture = arrivalDeparture;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_ContainerDangKy_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.VarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ThoiGianXuatDL", SqlDbType.DateTime, ThoiGianXuatDL.Year <= 1753 ? DBNull.Value : (object) ThoiGianXuatDL);
			db.AddInParameter(dbCommand, "@LuongTK", SqlDbType.Int, LuongTK);
			db.AddInParameter(dbCommand, "@TrangThaiToKhai", SqlDbType.Int, TrangThaiToKhai);
			db.AddInParameter(dbCommand, "@CustomsStatus", SqlDbType.NVarChar, CustomsStatus);
			db.AddInParameter(dbCommand, "@Note", SqlDbType.NVarChar, Note);
			db.AddInParameter(dbCommand, "@CargoPiece", SqlDbType.VarChar, CargoPiece);
			db.AddInParameter(dbCommand, "@PieceUnitCode", SqlDbType.VarChar, PieceUnitCode);
			db.AddInParameter(dbCommand, "@CargoWeight", SqlDbType.VarChar, CargoWeight);
			db.AddInParameter(dbCommand, "@WeightUnitCode", SqlDbType.NVarChar, WeightUnitCode);
			db.AddInParameter(dbCommand, "@LocationControl", SqlDbType.NVarChar, LocationControl);
			db.AddInParameter(dbCommand, "@CargoCtrlNo", SqlDbType.NVarChar, CargoCtrlNo);
			db.AddInParameter(dbCommand, "@TransportationCode", SqlDbType.Int, TransportationCode);
			db.AddInParameter(dbCommand, "@ArrivalDeparture", SqlDbType.DateTime, ArrivalDeparture.Year <= 1753 ? DBNull.Value : (object) ArrivalDeparture);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_ContainerDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_ContainerDangKy item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_ContainerDangKy(long id, long soTiepNhan, DateTime ngayTiepNhan, long tKMD_ID, int trangThaiXuLy, string maHQ, string maDoanhNghiep, string guidStr, string deXuatKhac, string lyDoSua, DateTime thoiGianXuatDL, int luongTK, int trangThaiToKhai, string customsStatus, string note, string cargoPiece, string pieceUnitCode, string cargoWeight, string weightUnitCode, string locationControl, string cargoCtrlNo, int transportationCode, DateTime arrivalDeparture)
		{
			KDT_ContainerDangKy entity = new KDT_ContainerDangKy();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TKMD_ID = tKMD_ID;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GuidStr = guidStr;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ThoiGianXuatDL = thoiGianXuatDL;
			entity.LuongTK = luongTK;
			entity.TrangThaiToKhai = trangThaiToKhai;
			entity.CustomsStatus = customsStatus;
			entity.Note = note;
			entity.CargoPiece = cargoPiece;
			entity.PieceUnitCode = pieceUnitCode;
			entity.CargoWeight = cargoWeight;
			entity.WeightUnitCode = weightUnitCode;
			entity.LocationControl = locationControl;
			entity.CargoCtrlNo = cargoCtrlNo;
			entity.TransportationCode = transportationCode;
			entity.ArrivalDeparture = arrivalDeparture;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_ContainerDangKy_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.VarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ThoiGianXuatDL", SqlDbType.DateTime, ThoiGianXuatDL.Year <= 1753 ? DBNull.Value : (object) ThoiGianXuatDL);
			db.AddInParameter(dbCommand, "@LuongTK", SqlDbType.Int, LuongTK);
			db.AddInParameter(dbCommand, "@TrangThaiToKhai", SqlDbType.Int, TrangThaiToKhai);
			db.AddInParameter(dbCommand, "@CustomsStatus", SqlDbType.NVarChar, CustomsStatus);
			db.AddInParameter(dbCommand, "@Note", SqlDbType.NVarChar, Note);
			db.AddInParameter(dbCommand, "@CargoPiece", SqlDbType.VarChar, CargoPiece);
			db.AddInParameter(dbCommand, "@PieceUnitCode", SqlDbType.VarChar, PieceUnitCode);
			db.AddInParameter(dbCommand, "@CargoWeight", SqlDbType.VarChar, CargoWeight);
			db.AddInParameter(dbCommand, "@WeightUnitCode", SqlDbType.NVarChar, WeightUnitCode);
			db.AddInParameter(dbCommand, "@LocationControl", SqlDbType.NVarChar, LocationControl);
			db.AddInParameter(dbCommand, "@CargoCtrlNo", SqlDbType.NVarChar, CargoCtrlNo);
			db.AddInParameter(dbCommand, "@TransportationCode", SqlDbType.Int, TransportationCode);
			db.AddInParameter(dbCommand, "@ArrivalDeparture", SqlDbType.DateTime, ArrivalDeparture.Year <= 1753 ? DBNull.Value : (object) ArrivalDeparture);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_ContainerDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_ContainerDangKy item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_ContainerDangKy(long id, long soTiepNhan, DateTime ngayTiepNhan, long tKMD_ID, int trangThaiXuLy, string maHQ, string maDoanhNghiep, string guidStr, string deXuatKhac, string lyDoSua, DateTime thoiGianXuatDL, int luongTK, int trangThaiToKhai, string customsStatus, string note, string cargoPiece, string pieceUnitCode, string cargoWeight, string weightUnitCode, string locationControl, string cargoCtrlNo, int transportationCode, DateTime arrivalDeparture)
		{
			KDT_ContainerDangKy entity = new KDT_ContainerDangKy();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.TKMD_ID = tKMD_ID;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.MaHQ = maHQ;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.GuidStr = guidStr;
			entity.DeXuatKhac = deXuatKhac;
			entity.LyDoSua = lyDoSua;
			entity.ThoiGianXuatDL = thoiGianXuatDL;
			entity.LuongTK = luongTK;
			entity.TrangThaiToKhai = trangThaiToKhai;
			entity.CustomsStatus = customsStatus;
			entity.Note = note;
			entity.CargoPiece = cargoPiece;
			entity.PieceUnitCode = pieceUnitCode;
			entity.CargoWeight = cargoWeight;
			entity.WeightUnitCode = weightUnitCode;
			entity.LocationControl = locationControl;
			entity.CargoCtrlNo = cargoCtrlNo;
			entity.TransportationCode = transportationCode;
			entity.ArrivalDeparture = arrivalDeparture;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ContainerDangKy_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.VarChar, MaHQ);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@DeXuatKhac", SqlDbType.NVarChar, DeXuatKhac);
			db.AddInParameter(dbCommand, "@LyDoSua", SqlDbType.NVarChar, LyDoSua);
			db.AddInParameter(dbCommand, "@ThoiGianXuatDL", SqlDbType.DateTime, ThoiGianXuatDL.Year <= 1753 ? DBNull.Value : (object) ThoiGianXuatDL);
			db.AddInParameter(dbCommand, "@LuongTK", SqlDbType.Int, LuongTK);
			db.AddInParameter(dbCommand, "@TrangThaiToKhai", SqlDbType.Int, TrangThaiToKhai);
			db.AddInParameter(dbCommand, "@CustomsStatus", SqlDbType.NVarChar, CustomsStatus);
			db.AddInParameter(dbCommand, "@Note", SqlDbType.NVarChar, Note);
			db.AddInParameter(dbCommand, "@CargoPiece", SqlDbType.VarChar, CargoPiece);
			db.AddInParameter(dbCommand, "@PieceUnitCode", SqlDbType.VarChar, PieceUnitCode);
			db.AddInParameter(dbCommand, "@CargoWeight", SqlDbType.VarChar, CargoWeight);
			db.AddInParameter(dbCommand, "@WeightUnitCode", SqlDbType.NVarChar, WeightUnitCode);
			db.AddInParameter(dbCommand, "@LocationControl", SqlDbType.NVarChar, LocationControl);
			db.AddInParameter(dbCommand, "@CargoCtrlNo", SqlDbType.NVarChar, CargoCtrlNo);
			db.AddInParameter(dbCommand, "@TransportationCode", SqlDbType.Int, TransportationCode);
			db.AddInParameter(dbCommand, "@ArrivalDeparture", SqlDbType.DateTime, ArrivalDeparture.Year <= 1753 ? DBNull.Value : (object) ArrivalDeparture);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_ContainerDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_ContainerDangKy item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_ContainerDangKy(long id)
		{
			KDT_ContainerDangKy entity = new KDT_ContainerDangKy();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ContainerDangKy_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_ContainerDangKy_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_ContainerDangKy> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_ContainerDangKy item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}