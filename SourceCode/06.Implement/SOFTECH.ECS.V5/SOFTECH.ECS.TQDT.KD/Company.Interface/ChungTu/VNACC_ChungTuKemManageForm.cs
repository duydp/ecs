﻿using System;
using System.Drawing;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using System.Data;
using System.Collections.Generic;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface
{
    public partial class VNACC_ChungTuKemManageForm : BaseForm
    {
        private List<KDT_VNACC_ChungTuDinhKem> ChungTuDinhKemColecctions = new List<KDT_VNACC_ChungTuDinhKem>();
        public ELoaiThongTin LoaiChungTu = ELoaiThongTin.MSB;

        public VNACC_ChungTuKemManageForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void VNACC_ChungTuKemManageForm_Load(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                BindData();

                //Fill ValueList
                this.dgList.RootTable.Columns["CoQuanHaiQuan"].HasValueList = true;
                Janus.Windows.GridEX.GridEXValueListItemCollection valueList = this.dgList.RootTable.Columns["CoQuanHaiQuan"].ValueList;
                System.Data.DataView view = VNACC_Category_CustomsOffice.SelectAll().Tables[0].DefaultView;
                view.Sort = "CustomsCode ASC";
                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    valueList.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["CustomsCode"].ToString(), row["CustomsOfficeNameInVietnamese"].ToString()));
                }

                //Nhom xu ly ho so
                this.dgList.RootTable.Columns["NhomXuLyHoSoID"].HasValueList = true;
                Janus.Windows.GridEX.GridEXValueListItemCollection vlNhomXuLyHoSo = this.dgList.RootTable.Columns["NhomXuLyHoSoID"].ValueList;
                view = VNACC_Category_CustomsSubSection.SelectAll().Tables[0].DefaultView;
                view.Sort = "CustomsSubSectionCode ASC";
                for (int i = 0; i < view.Count; i++)
                {
                    System.Data.DataRowView row = view[i];
                    vlNhomXuLyHoSo.Add(new Janus.Windows.GridEX.GridEXValueListItem(row["ID"].ToString(), row["Name"].ToString()));
                }

                //Loai chung tu
                this.dgList.RootTable.Columns["LoaiChungTu"].HasValueList = true;
                Janus.Windows.GridEX.GridEXValueListItemCollection vlLoaiChungTu = this.dgList.RootTable.Columns["LoaiChungTu"].ValueList;
                vlLoaiChungTu.Add(new Janus.Windows.GridEX.GridEXValueListItem(ELoaiThongTin.MSB.ToString(), ELoaiThongTin.MSB.ToString()));
                vlLoaiChungTu.Add(new Janus.Windows.GridEX.GridEXValueListItem(ELoaiThongTin.HYS.ToString(), ELoaiThongTin.HYS.ToString()));
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void BindData()
        {
            //string query = string.Format("LoaiChungTu = '{0}'", LoaiChungTu.ToString());
            ChungTuDinhKemColecctions = KDT_VNACC_ChungTuDinhKem.SelectCollectionAll();

            dgList.DataSource = ChungTuDinhKemColecctions;
            try { dgList.Refetch(); }
            catch { }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                List<KDT_VNACC_ChungTuDinhKem> listChungTu = new List<KDT_VNACC_ChungTuDinhKem>();
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                {
                    if (ShowMessage("Bạn có muốn xóa không ?", true) != "Yes")
                    {
                        return;
                    }
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            KDT_VNACC_ChungTuDinhKem gp = (KDT_VNACC_ChungTuDinhKem)i.GetRow().DataRow;
                            listChungTu.Add(gp);
                        }
                    }
                }

                foreach (KDT_VNACC_ChungTuDinhKem item in listChungTu)
                {
                    if (item.ID > 0)
                    {
                        //Load chung tu kem chi tiet (Neu co)
                        item.LoadListChungTuDinhKem_ChiTiet();

                        //Xoa chung tu dinh kem chi tiet
                        for (int k = 0; k < item.ChungTuDinhKemChiTietCollection.Count; k++)
                        {
                            item.ChungTuDinhKemChiTietCollection[k].Delete();
                        }

                        //Xoa chung tu dinh kem trong DB
                        item.Delete();
                    }
                }

                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void btnTaoMoi_Click(object sender, EventArgs e)
        {
            //VNACC_ChungTuDinhKemForm ctForm = new VNACC_ChungTuDinhKemForm();
            //ctForm.ShowDialog(this);

            //dgList.DataSource = ctForm.ListCTDK;
            //try { dgList.Refetch(); }
            //catch { }
        }

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                KDT_VNACC_ChungTuDinhKem chungTu = (KDT_VNACC_ChungTuDinhKem)e.Row.DataRow;

                VNACC_ChungTuDinhKemForm f = null;
                if (chungTu.LoaiChungTu == ELoaiThongTin.MSB.ToString())
                    f = new VNACC_ChungTuDinhKemForm(ELoaiThongTin.MSB);
                else if (chungTu.LoaiChungTu == ELoaiThongTin.HYS.ToString())
                    f = new VNACC_ChungTuDinhKemForm(ELoaiThongTin.HYS);

                f.isAddNew = false;
                f.ChungTuKem = chungTu;

                f.ShowDialog(this);

                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            else
            {
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        KDT_VNACC_ChungTuDinhKem gp = (KDT_VNACC_ChungTuDinhKem)i.GetRow().DataRow;
                        if (gp.SoTiepNhan > 0)
                            btnXoa.Enabled = false;
                        else
                            btnXoa.Enabled = true;
                    }
                }
            }
        }

    }
}
