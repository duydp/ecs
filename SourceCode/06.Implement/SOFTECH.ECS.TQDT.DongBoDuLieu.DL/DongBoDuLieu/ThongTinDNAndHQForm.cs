﻿using System;
using System.Windows.Forms;
using Company.BLL.SXXK;
using Company.BLL.SXXK.ToKhai;
using Company.BLL.SXXK.ThanhKhoan;
using System.Threading;
using System.Data;
using System.Configuration;
using Company.KDT.SHARE.Components;

namespace DongBoDuLieu
{
    public partial class ThongTinDNAndHQForm : BaseForm
    {
        public ThongTinDNAndHQForm()
        {
            InitializeComponent();
        }

        private void SendForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.Cursor = Cursors.WaitCursor;

                txtMaDN.Text = WebService.LoadConfigure("MaDoanhNghiep");// GlobalSettings.MA_DON_VI;
                txtTenDN.Text = WebService.LoadConfigure("TenDoanhNghiep");// GlobalSettings.TEN_DON_VI;
                txtMaCuc.Text = WebService.LoadConfigure("MaHaiQuan"); // GlobalSettings.MA_CUC_HAI_QUAN;
                chChecked.Checked = bool.Parse(WebService.LoadConfigure("CHUNGTUDINHKEM"));
                chkSoTK.Checked = bool.Parse(WebService.LoadConfigure("SuDungSoTK"));
                txtSoTK.Value = Int32.Parse(WebService.LoadConfigure("SoTK"));
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                MLMessages("Có lỗi trong quá trình xử lý", "", "", false);
            }
            finally { this.Cursor = Cursors.Default; }
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            containerValidator1.Validate();
            if (!containerValidator1.IsValid)
                return;
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings.Remove("MaHaiQuan");
            config.AppSettings.Settings.Add("MaHaiQuan", txtMaCuc.Text);

            config.AppSettings.Settings.Remove("MaDoanhNghiep");
            config.AppSettings.Settings.Add("MaDoanhNghiep", txtMaDN.Text);

            config.AppSettings.Settings.Remove("TenDoanhNghiep");
            config.AppSettings.Settings.Add("TenDoanhNghiep", txtTenDN.Text);
            config.AppSettings.Settings.Remove("CHUNGTUDINHKEM");
            config.AppSettings.Settings.Add("CHUNGTUDINHKEM", chChecked.Checked.ToString());

            config.AppSettings.Settings.Remove("SuDungSoTK");
            config.AppSettings.Settings.Add("SuDungSoTK", chkSoTK.Checked.ToString());
            config.AppSettings.Settings.Remove("SoTK");
            config.AppSettings.Settings.Add("SoTK", txtSoTK.Value.ToString());

            config.Save(ConfigurationSaveMode.Full);
            ConfigurationManager.RefreshSection("appSettings");
            this.Close();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}