-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_VNACCS_TEA_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_VNACCS_TEA_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_Insert]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Insert]
	@SoDanhMucMienThue numeric(12, 0),
	@PhanLoaiXuatNhapKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@DiaChiCuaNguoiKhai nvarchar(100),
	@SDTCuaNguoiKhai varchar(20),
	@ThoiHanMienThue datetime,
	@TenDuAnDauTu nvarchar(70),
	@DiaDiemXayDungDuAn nvarchar(100),
	@MucTieuDuAn nvarchar(100),
	@MaMienGiam varchar(5),
	@PhamViDangKyDMMT nvarchar(100),
	@NgayDuKienXNK datetime,
	@GP_GCNDauTuSo varchar(20),
	@NgayChungNhan datetime,
	@CapBoi nvarchar(100),
	@GhiChu nvarchar(255),
	@CamKetSuDung nvarchar(140),
	@MaNguoiKhai varchar(13),
	@KhaiBaoTuNgay datetime,
	@KhaiBaoDenNgay datetime,
	@MaSoQuanLyDSMT varchar(14),
	@PhanLoaiCapPhep varchar(1),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100),
	@ID bigint OUTPUT
AS

INSERT INTO [dbo].[t_KDT_VNACCS_TEA]
(
	[SoDanhMucMienThue],
	[PhanLoaiXuatNhapKhau],
	[CoQuanHaiQuan],
	[DiaChiCuaNguoiKhai],
	[SDTCuaNguoiKhai],
	[ThoiHanMienThue],
	[TenDuAnDauTu],
	[DiaDiemXayDungDuAn],
	[MucTieuDuAn],
	[MaMienGiam],
	[PhamViDangKyDMMT],
	[NgayDuKienXNK],
	[GP_GCNDauTuSo],
	[NgayChungNhan],
	[CapBoi],
	[GhiChu],
	[CamKetSuDung],
	[MaNguoiKhai],
	[KhaiBaoTuNgay],
	[KhaiBaoDenNgay],
	[MaSoQuanLyDSMT],
	[PhanLoaiCapPhep],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
)
VALUES 
(
	@SoDanhMucMienThue,
	@PhanLoaiXuatNhapKhau,
	@CoQuanHaiQuan,
	@DiaChiCuaNguoiKhai,
	@SDTCuaNguoiKhai,
	@ThoiHanMienThue,
	@TenDuAnDauTu,
	@DiaDiemXayDungDuAn,
	@MucTieuDuAn,
	@MaMienGiam,
	@PhamViDangKyDMMT,
	@NgayDuKienXNK,
	@GP_GCNDauTuSo,
	@NgayChungNhan,
	@CapBoi,
	@GhiChu,
	@CamKetSuDung,
	@MaNguoiKhai,
	@KhaiBaoTuNgay,
	@KhaiBaoDenNgay,
	@MaSoQuanLyDSMT,
	@PhanLoaiCapPhep,
	@TrangThaiXuLy,
	@InputMessageID,
	@MessageTag,
	@IndexTag
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_Update]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Update]
	@ID bigint,
	@SoDanhMucMienThue numeric(12, 0),
	@PhanLoaiXuatNhapKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@DiaChiCuaNguoiKhai nvarchar(100),
	@SDTCuaNguoiKhai varchar(20),
	@ThoiHanMienThue datetime,
	@TenDuAnDauTu nvarchar(70),
	@DiaDiemXayDungDuAn nvarchar(100),
	@MucTieuDuAn nvarchar(100),
	@MaMienGiam varchar(5),
	@PhamViDangKyDMMT nvarchar(100),
	@NgayDuKienXNK datetime,
	@GP_GCNDauTuSo varchar(20),
	@NgayChungNhan datetime,
	@CapBoi nvarchar(100),
	@GhiChu nvarchar(255),
	@CamKetSuDung nvarchar(140),
	@MaNguoiKhai varchar(13),
	@KhaiBaoTuNgay datetime,
	@KhaiBaoDenNgay datetime,
	@MaSoQuanLyDSMT varchar(14),
	@PhanLoaiCapPhep varchar(1),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS

UPDATE
	[dbo].[t_KDT_VNACCS_TEA]
SET
	[SoDanhMucMienThue] = @SoDanhMucMienThue,
	[PhanLoaiXuatNhapKhau] = @PhanLoaiXuatNhapKhau,
	[CoQuanHaiQuan] = @CoQuanHaiQuan,
	[DiaChiCuaNguoiKhai] = @DiaChiCuaNguoiKhai,
	[SDTCuaNguoiKhai] = @SDTCuaNguoiKhai,
	[ThoiHanMienThue] = @ThoiHanMienThue,
	[TenDuAnDauTu] = @TenDuAnDauTu,
	[DiaDiemXayDungDuAn] = @DiaDiemXayDungDuAn,
	[MucTieuDuAn] = @MucTieuDuAn,
	[MaMienGiam] = @MaMienGiam,
	[PhamViDangKyDMMT] = @PhamViDangKyDMMT,
	[NgayDuKienXNK] = @NgayDuKienXNK,
	[GP_GCNDauTuSo] = @GP_GCNDauTuSo,
	[NgayChungNhan] = @NgayChungNhan,
	[CapBoi] = @CapBoi,
	[GhiChu] = @GhiChu,
	[CamKetSuDung] = @CamKetSuDung,
	[MaNguoiKhai] = @MaNguoiKhai,
	[KhaiBaoTuNgay] = @KhaiBaoTuNgay,
	[KhaiBaoDenNgay] = @KhaiBaoDenNgay,
	[MaSoQuanLyDSMT] = @MaSoQuanLyDSMT,
	[PhanLoaiCapPhep] = @PhanLoaiCapPhep,
	[TrangThaiXuLy] = @TrangThaiXuLy,
	[InputMessageID] = @InputMessageID,
	[MessageTag] = @MessageTag,
	[IndexTag] = @IndexTag
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_InsertUpdate]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_InsertUpdate]
	@ID bigint,
	@SoDanhMucMienThue numeric(12, 0),
	@PhanLoaiXuatNhapKhau varchar(1),
	@CoQuanHaiQuan varchar(6),
	@DiaChiCuaNguoiKhai nvarchar(100),
	@SDTCuaNguoiKhai varchar(20),
	@ThoiHanMienThue datetime,
	@TenDuAnDauTu nvarchar(70),
	@DiaDiemXayDungDuAn nvarchar(100),
	@MucTieuDuAn nvarchar(100),
	@MaMienGiam varchar(5),
	@PhamViDangKyDMMT nvarchar(100),
	@NgayDuKienXNK datetime,
	@GP_GCNDauTuSo varchar(20),
	@NgayChungNhan datetime,
	@CapBoi nvarchar(100),
	@GhiChu nvarchar(255),
	@CamKetSuDung nvarchar(140),
	@MaNguoiKhai varchar(13),
	@KhaiBaoTuNgay datetime,
	@KhaiBaoDenNgay datetime,
	@MaSoQuanLyDSMT varchar(14),
	@PhanLoaiCapPhep varchar(1),
	@TrangThaiXuLy varchar(2),
	@InputMessageID varchar(10),
	@MessageTag varchar(26),
	@IndexTag varchar(100)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_VNACCS_TEA] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_VNACCS_TEA] 
		SET
			[SoDanhMucMienThue] = @SoDanhMucMienThue,
			[PhanLoaiXuatNhapKhau] = @PhanLoaiXuatNhapKhau,
			[CoQuanHaiQuan] = @CoQuanHaiQuan,
			[DiaChiCuaNguoiKhai] = @DiaChiCuaNguoiKhai,
			[SDTCuaNguoiKhai] = @SDTCuaNguoiKhai,
			[ThoiHanMienThue] = @ThoiHanMienThue,
			[TenDuAnDauTu] = @TenDuAnDauTu,
			[DiaDiemXayDungDuAn] = @DiaDiemXayDungDuAn,
			[MucTieuDuAn] = @MucTieuDuAn,
			[MaMienGiam] = @MaMienGiam,
			[PhamViDangKyDMMT] = @PhamViDangKyDMMT,
			[NgayDuKienXNK] = @NgayDuKienXNK,
			[GP_GCNDauTuSo] = @GP_GCNDauTuSo,
			[NgayChungNhan] = @NgayChungNhan,
			[CapBoi] = @CapBoi,
			[GhiChu] = @GhiChu,
			[CamKetSuDung] = @CamKetSuDung,
			[MaNguoiKhai] = @MaNguoiKhai,
			[KhaiBaoTuNgay] = @KhaiBaoTuNgay,
			[KhaiBaoDenNgay] = @KhaiBaoDenNgay,
			[MaSoQuanLyDSMT] = @MaSoQuanLyDSMT,
			[PhanLoaiCapPhep] = @PhanLoaiCapPhep,
			[TrangThaiXuLy] = @TrangThaiXuLy,
			[InputMessageID] = @InputMessageID,
			[MessageTag] = @MessageTag,
			[IndexTag] = @IndexTag
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_VNACCS_TEA]
		(
			[SoDanhMucMienThue],
			[PhanLoaiXuatNhapKhau],
			[CoQuanHaiQuan],
			[DiaChiCuaNguoiKhai],
			[SDTCuaNguoiKhai],
			[ThoiHanMienThue],
			[TenDuAnDauTu],
			[DiaDiemXayDungDuAn],
			[MucTieuDuAn],
			[MaMienGiam],
			[PhamViDangKyDMMT],
			[NgayDuKienXNK],
			[GP_GCNDauTuSo],
			[NgayChungNhan],
			[CapBoi],
			[GhiChu],
			[CamKetSuDung],
			[MaNguoiKhai],
			[KhaiBaoTuNgay],
			[KhaiBaoDenNgay],
			[MaSoQuanLyDSMT],
			[PhanLoaiCapPhep],
			[TrangThaiXuLy],
			[InputMessageID],
			[MessageTag],
			[IndexTag]
		)
		VALUES 
		(
			@SoDanhMucMienThue,
			@PhanLoaiXuatNhapKhau,
			@CoQuanHaiQuan,
			@DiaChiCuaNguoiKhai,
			@SDTCuaNguoiKhai,
			@ThoiHanMienThue,
			@TenDuAnDauTu,
			@DiaDiemXayDungDuAn,
			@MucTieuDuAn,
			@MaMienGiam,
			@PhamViDangKyDMMT,
			@NgayDuKienXNK,
			@GP_GCNDauTuSo,
			@NgayChungNhan,
			@CapBoi,
			@GhiChu,
			@CamKetSuDung,
			@MaNguoiKhai,
			@KhaiBaoTuNgay,
			@KhaiBaoDenNgay,
			@MaSoQuanLyDSMT,
			@PhanLoaiCapPhep,
			@TrangThaiXuLy,
			@InputMessageID,
			@MessageTag,
			@IndexTag
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_Delete]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Delete]
	@ID bigint
AS

DELETE FROM 
	[dbo].[t_KDT_VNACCS_TEA]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_DeleteDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_VNACCS_TEA] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_Load]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_Load]
	@ID bigint
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoDanhMucMienThue],
	[PhanLoaiXuatNhapKhau],
	[CoQuanHaiQuan],
	[DiaChiCuaNguoiKhai],
	[SDTCuaNguoiKhai],
	[ThoiHanMienThue],
	[TenDuAnDauTu],
	[DiaDiemXayDungDuAn],
	[MucTieuDuAn],
	[MaMienGiam],
	[PhamViDangKyDMMT],
	[NgayDuKienXNK],
	[GP_GCNDauTuSo],
	[NgayChungNhan],
	[CapBoi],
	[GhiChu],
	[CamKetSuDung],
	[MaNguoiKhai],
	[KhaiBaoTuNgay],
	[KhaiBaoDenNgay],
	[MaSoQuanLyDSMT],
	[PhanLoaiCapPhep],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEA]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_SelectDynamic]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[SoDanhMucMienThue],
	[PhanLoaiXuatNhapKhau],
	[CoQuanHaiQuan],
	[DiaChiCuaNguoiKhai],
	[SDTCuaNguoiKhai],
	[ThoiHanMienThue],
	[TenDuAnDauTu],
	[DiaDiemXayDungDuAn],
	[MucTieuDuAn],
	[MaMienGiam],
	[PhamViDangKyDMMT],
	[NgayDuKienXNK],
	[GP_GCNDauTuSo],
	[NgayChungNhan],
	[CapBoi],
	[GhiChu],
	[CamKetSuDung],
	[MaNguoiKhai],
	[KhaiBaoTuNgay],
	[KhaiBaoDenNgay],
	[MaSoQuanLyDSMT],
	[PhanLoaiCapPhep],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM [dbo].[t_KDT_VNACCS_TEA] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_VNACCS_TEA_SelectAll]
-- Database: ECS_TQDT_KD_V5
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, October 01, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_VNACCS_TEA_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[SoDanhMucMienThue],
	[PhanLoaiXuatNhapKhau],
	[CoQuanHaiQuan],
	[DiaChiCuaNguoiKhai],
	[SDTCuaNguoiKhai],
	[ThoiHanMienThue],
	[TenDuAnDauTu],
	[DiaDiemXayDungDuAn],
	[MucTieuDuAn],
	[MaMienGiam],
	[PhamViDangKyDMMT],
	[NgayDuKienXNK],
	[GP_GCNDauTuSo],
	[NgayChungNhan],
	[CapBoi],
	[GhiChu],
	[CamKetSuDung],
	[MaNguoiKhai],
	[KhaiBaoTuNgay],
	[KhaiBaoDenNgay],
	[MaSoQuanLyDSMT],
	[PhanLoaiCapPhep],
	[TrangThaiXuLy],
	[InputMessageID],
	[MessageTag],
	[IndexTag]
FROM
	[dbo].[t_KDT_VNACCS_TEA]	

GO

