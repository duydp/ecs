﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
namespace Company.KDT.SHARE.Components
{
    public class CustomsValue
    {
        /// <summary>
        /// tổng giá trị sản phẩm
        /// </summary>
        [XmlElement("totalProductValue")]
        public string TotalProductValue { get; set; }
        /// <summary>
        /// giá trị tiền công
        /// </summary>
        [XmlElement("totalPaymentValue")]
        public string TotalPaymentValue { get; set; }
        /// <summary>
        /// đơn giá
        /// </summary>
        [XmlElement("unitPrice")]
        public string unitPrice { set; get; }
    }
}
