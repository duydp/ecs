﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Company.KDT.SHARE.Components.DanhMucHaiQuan
{
    public partial class NguyenTe
    {
        public static List<NguyenTe> ConvertDataSetToCollection(DataSet dataSet)
        {
            List<NguyenTe> collection = new List<NguyenTe>();

            for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
            {
                NguyenTe entity = new NguyenTe();
                entity.ID = dataSet.Tables[0].Rows[i]["ID"].ToString();
                entity.Ten = dataSet.Tables[0].Rows[i]["Ten"].ToString();
                entity.TyGiaTinhThue = dataSet.Tables[0].Rows[i]["TyGiaTinhThue"] != DBNull.Value ? Convert.ToDecimal(dataSet.Tables[0].Rows[i]["TyGiaTinhThue"]) : 0;
                entity.DateCreated = dataSet.Tables[0].Rows[i]["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateCreated"]) : DateTime.Today;
                entity.DateModified = dataSet.Tables[0].Rows[i]["DateModified"] != DBNull.Value ? Convert.ToDateTime(dataSet.Tables[0].Rows[i]["DateModified"]) : DateTime.Today;
                collection.Add(entity);
            }

            return collection;
        }
    }
}
