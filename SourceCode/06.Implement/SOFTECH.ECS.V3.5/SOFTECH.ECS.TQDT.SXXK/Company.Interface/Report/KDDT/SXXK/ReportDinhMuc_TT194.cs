﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.KDT.SXXK;
using System.Data;

namespace Company.Interface.Report.SXXK
{
    public partial class ReportDinhMuc_TT194 : DevExpress.XtraReports.UI.XtraReport
    {
        public delegate void GetObjectInfo(string hodong, string tokhai);
        public string MaSP; 
        public string TenSP;
        public string DVT;
        public string HopDong;
        public DinhMucDangKy DMDangKy;
        private int STT = 0;
        public bool inNKvaND;
        public GetObjectInfo GetText;
        
        public ReportDinhMuc_TT194()
        {
            InitializeComponent();
        }
               
        public void BindReport()
        {
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (HopDong != null && HopDong != "")
            {
                lblHopDong.Text = this.HopDong;
                lblHopDong.Visible = true;
                lblHopDongLable.Visible = true;
            }
            else
            {
                lblHopDong.Visible = false;
                lblHopDongLable.Visible = false;
                lblSanPham.Location = lblHopDongLable.Location;
            }
            if(this.DMDangKy.SoTiepNhan >0)
                lblSoTN.Text = "STNĐM: " + this.DMDangKy.SoTiepNhan;
            lblSanPham.Text = "Mã sản phẩm : " + this.MaSP + "        - Tên sản phẩm : " + TenSP + "  - Đơn vị tính sp : " + DVT;
            this.DataSource = DMDangKy.getDinhMucOfSanPham(this.MaSP,GlobalSettings.MA_HAI_QUAN,GlobalSettings.MA_DON_VI);
            lblMaNPL.DataBindings.Add("Text", this.DataSource, "MaNguyenPhuLieu");            
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT_ID");
            lblDM.DataBindings.Add("Text", this.DataSource, "DinhMucSuDung","{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTyLeHH.DataBindings.Add("Text", this.DataSource, "TyLeHaoHut","{0:n0}");
            lblDMChung.DataBindings.Add("Text", this.DataSource, "DinhMucChung","{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, "TenNPL");
            lblNgay.Text = lblNgay2.Text = Properties.Settings.Default.TieudeNgay;
            if (!inNKvaND)
                lblNguonCung.DataBindings.Add("Text", this.DataSource, "FromVietNam");
            else
                lblNguonCung.Text = "Nhập khẩu và mua tại VN";
        }
        public void BindReportDinhMucDaDangKy()
        {
            lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
            lblDiaChi.Text = GlobalSettings.DIA_CHI;
            lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
            if (HopDong != null && HopDong != "")
                lblHopDong.Text = this.HopDong;
            else
            {
                lblHopDong.Visible = false;
                lblHopDongLable.Visible = false;
                lblSanPham.Location = lblHopDongLable.Location;
            }
            lblSanPham.Text = "Mã sản phẩm : " + this.MaSP + "        - Tên sản phẩm : " + TenSP + "  - Đơn vị tính sp : " + DVT;
            DataSet ds = BLL.SXXK.DinhMuc.getDinhMucOfSanPham(this.MaSP, GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
            ds.Tables[0].Columns.Add("STTHang");
            int i=1;
            foreach (DataRow row in ds.Tables[0].Rows)
                row["STTHang"] = i++;
            this.DataSource = ds.Tables[0];
            lblMaNPL.DataBindings.Add("Text", this.DataSource, "MaNguyenPhuLieu");
            lblSTT.DataBindings.Add("Text", this.DataSource, "STTHang");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT_ID");
            lblDM.DataBindings.Add("Text", this.DataSource, "DinhMucSuDung", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTyLeHH.DataBindings.Add("Text", this.DataSource, "TyLeHaoHut", "{0:n0}");
            lblDMChung.DataBindings.Add("Text", this.DataSource, "DinhMucChung", "{0:n" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, "TenNPL");
            //lblNgay.Text = "............., ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            //lblNgay2.Text = "............, ngày " + DateTime.Now.Day + " tháng " + DateTime.Now.Month + " năm " + DateTime.Now.Year;
            lblNgay.Text = lblNgay2.Text = Properties.Settings.Default.TieudeNgay;
            if (!inNKvaND)
                lblNguonCung.DataBindings.Add("Text", this.DataSource, "FromVietNam");
            else
                lblNguonCung.Text = "Nhập khẩu và mua tại VN";
        }
        private void lblTenNPL_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            
        }

        private void ReportHeader_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
        }

        private void lblTyLeHH_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
           
        }

        private void ReportDinhMuc_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT = 0;
        }

        private void lblSTT_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            this.STT++;
            lblSTT.Text = this.STT + "";
        }    

    }
}
