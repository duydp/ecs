using System;
using System.Data;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.SXXK;
using Company.BLL.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.Interface.Report.SXXK;

namespace Company.Interface.SXXK
{
    public partial class SanPhamRegistedForm : BaseForm
    {
        public SanPham SanPhamSelected = new SanPham();
        public SanPhamCollection SPCollection = new SanPhamCollection();
        public SanPhamCollection SanPhamsCollectionSelected = new SanPhamCollection();
        public bool events = false;
        private DataSet dsRegistedList;
        public SanPhamRegistedForm()
        {
            InitializeComponent();

        }

        public void BindData()
        {


        }

        //-----------------------------------------------------------------------------------------

        private void khoitao_DuLieuChuan()
        {
            // Đơn vị tính.
            this._DonViTinh = DonViTinh.SelectAll().Tables[0];
            if (this.CalledForm == "DinhMucSendForm")
            {
                //ctrDonViHaiQuan.Enabled = false;
            }
            lblHint.Visible = this.CalledForm != string.Empty;
        }

        //-----------------------------------------------------------------------------------------

        private void setDataToComboUserKB()
        {
            DataTable dt = new Company.QuanTri.User().SelectAll().Tables[0];
            DataRow dr = dt.NewRow();
            dr["USER_NAME"] = -1;
            dr["HO_TEN"] = "--Tất cả--";
            dt.Rows.InsertAt(dr, 0);
            cbUserKB.DataSource = dt;
            cbUserKB.DisplayMember = dt.Columns["HO_TEN"].ToString();
            cbUserKB.ValueMember = dt.Columns["USER_NAME"].ToString();
        }

        //-----------------------------------------------------------------------------------------

        private void SanPhamRegistedForm_Load(object sender, EventArgs e)
        {
            lblTongSP.Text = "0";
            btnExportExcel.Visible = true;

            this.khoitao_DuLieuChuan();
            setDataToComboUserKB();
            cbUserKB.SelectedIndex = 0;
            // Sản phẩm đã đăng ký.
            dgList.Tables[0].Columns["MaDoanhNghiep"].Visible = GlobalSettings.DAI_LY_TTHQ;
            this.search();
            // Doanh nghiệp / Đại lý TTHQ.
            if (MainForm.versionHD == 0)
            {
                if (!MainForm.EcsQuanTri.HasPermission(Convert.ToInt64(RoleSanPham.CapNhatDuLieu)))
                {
                    dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.False;
                    uiButton3.Visible = false;
                    btnDelete.Visible = false;
                }
            }
        }

        //-----------------------------------------------------------------------------------------

        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
                e.Row.Cells["DVT_ID"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
        }

        //-----------------------------------------------------------------------------------------



        //-----------------------------------------------------------------------------------------

        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            if (lblHint.Visible)
            {
                if (e.Row.RowType == RowType.Record)
                {
                    string maSP = e.Row.Cells["Ma"].Text;
                    this.SanPhamSelected.Ma = maSP;
                    this.SanPhamSelected.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                    this.SanPhamSelected.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    this.SanPhamSelected.Ten = e.Row.Cells["Ten"].Text;
                    this.SanPhamSelected.MaHS = e.Row.Cells["MaHS"].Text;
                    this.SanPhamSelected.DVT_ID = e.Row.Cells["DVT_ID"].Text;
                    this.SanPhamSelected.Load();
                    this.Hide();
                }
            }
            else
            {

            }
            events = false;
        }

        private SanPhamCollection MuiltiSellect()
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {

                    this.SanPhamsCollectionSelected.Add((SanPham)i.GetRow().DataRow);

                }
            }
            events = true;
            return SanPhamsCollectionSelected;
        }

        private void ctrDonViHaiQuan_ValueChanged(object sender, EventArgs e)
        {
            this.BindData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void filterEditor1_Click(object sender, EventArgs e)
        {

        }

        private void uiButton1_Click(object sender, EventArgs e)
        {
            ImportSPForm f = new ImportSPForm();
            f.SPCollection = this.SPCollection;
            f.ShowDialog();
            BindData();
            dgList.Refetch();
        }

        private void uiButton2_Click(object sender, EventArgs e)
        {
            //this.MuiltiSellect();
            //this.Close();
        }

        private void uiButton1_Click_1(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            this.BindData();
            dgList.Refetch();
            this.Cursor = Cursors.Default;
        }

        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // if (ShowMessage("Bạn có muốn xóa các sản phẩm này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa các sản phẩm này không?", "MSG_DEL01", "", true) == "Yes")
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPham hmd = (SanPham)i.GetRow().DataRow;
                        hmd.Delete();
                    }
                }
            }
            else
            {
                e.Cancel = true;
            }
        }

        private void uiButton3_Click(object sender, EventArgs e)
        {
            SanPhamEditForm f = new SanPhamEditForm();
            f.ShowDialog();
            search();
        }

        private void uiButton4_Click(object sender, EventArgs e)
        {
            SelectSanPhamForm f = new SelectSanPhamForm();
            f.ShowDialog();
            ReportSanPham f2 = new ReportSanPham();
            if (f.spCollectionSelect.Count > 0)
            {
                f2.SPCollection = f.spCollectionSelect;
                f2.BindReportDinhMucDaDangKy();
                f2.ShowPreview();
            }

        }

        private void uiButton2_Click_1(object sender, EventArgs e)
        {
            ImportSPForm f = new ImportSPForm();
            f.SPCollection = this.SPCollection;
            f.ShowDialog();
            this.search();
        }

        private void grbMain_Click(object sender, EventArgs e)
        {

        }

        private void uiGroupBox4_Click(object sender, EventArgs e)
        {

        }
        private void search()
        {
            // Xây dựng điều kiện tìm kiếm.
            string where = "MaDoanhNghiep = '" + GlobalSettings.MA_DON_VI + "' AND MaHaiQuan = '" + GlobalSettings.MA_HAI_QUAN + "'";
            if (txtMaSP.Text.Trim().Length > 0)
            {
                where += " AND Ma Like '%" + txtMaSP.Text.Trim() + "%'";
            }

            if (txtTenSP.Text.Trim().Length > 0)
            {
                where += " AND Ten Like '%" + txtTenSP.Text.Trim() + "%'";
            }

            this.SPCollection = new SanPham().SelectCollectionDynamic(where, " Ma");
            dgList.DataSource = this.SPCollection;

            lblTongSP.Text = SPCollection.Count.ToString();
        }

        private DataTable dtSP;
        private void searchNguoiKhaiBao(string userName)
        {
            if (userName.Equals("-1"))
            {
                search();
            }
            else
            {
                dtSP = new SanPham().GetSPFromUserName(userName).Tables[0];
                dgList.DataSource = dtSP;
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            //search();
            searchNguoiKhaiBao(cbUserKB.SelectedItem.Value.ToString());
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            GridEXSelectedItemCollection items = dgList.SelectedItems;
            if (items.Count <= 0) return;
            //if (ShowMessage("Bạn có muốn xóa các sản phẩm này không?", true) == "Yes")
            if (MLMessages("Bạn có muốn xóa các sản phẩm này không?", "MSG_DEL01", "", true) == "Yes")
            {

                foreach (GridEXSelectedItem i in items)
                {
                    if (i.RowType == RowType.Record)
                    {
                        SanPham hmd = (SanPham)i.GetRow().DataRow;
                        hmd.Delete();
                    }
                }
                this.search();
            }

        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "SanPhamDaDangKy_" + DateTime.Today.ToString("dd/MM/yyyy").Replace("/", "") + ".xls";
                sfNPL.Filter = "Excel files| *.xls";
                sfNPL.ShowDialog();

                if (sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }

        //-----------------------------------------------------------------------------------------
    }
}