﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using System.Data;
using Company.GC.BLL.KDT.GC;


namespace Company.Interface.Report.GC
{
    public partial class BangKe05New : DevExpress.XtraReports.UI.XtraReport
    {
        public HopDong HD = new HopDong();
        public bool First = false;
        public bool Last = false;
        public Report.ReportViewBC05NewForm report = new ReportViewBC05NewForm();
        public BangKe05New()
        {
            InitializeComponent();
        }
        public void BindReport(DataTable dt)
        {

            ReportHeader.Visible = this.First;
            ReportFooter1.Visible = this.Last;
            this.DataSource = dt;
            lblAddNhan.Text = GlobalSettings.DIA_CHI;
            lblbennhan.Text = GlobalSettings.TEN_DON_VI;
            lblChiCucHQ.Text = GlobalSettings.TEN_HAI_QUAN;

            decimal soluongSP = new Company.GC.BLL.GC.SanPham().GetTongSoSanPhamInHopDong(this.HD.ID);
            lblSoLuong.Text = soluongSP.ToString("N" + GlobalSettings.SoThapPhan.LuongSP);
            DataSet ds = new Company.GC.BLL.GC.NhomSanPham().GetTenNhomSanPham(this.HD.ID);
            string loaiSP = "";
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (loaiSP == "")
                {
                    loaiSP = dr["TenSanPham"].ToString();

                }
                else
                {
                    loaiSP = loaiSP + "," + dr["TenSanPham"].ToString();
                }
            }
            lblMathang.Text = loaiSP;

            lblDateFirst.Text = this.HD.NgayKy.ToString("dd/MM/yyyy");
            lbllimittimefirst.Text = this.HD.NgayHetHan.ToString("dd/MM/yyyy");
            lblbenthue.Text = this.HD.DonViDoiTac;
            lblAddThue.Text = this.HD.DiaChiDoiTac;
            lblhopsoHDGC.Text = this.HD.SoHopDong;
            //
            lblSTT.DataBindings.Add("Text", this.DataSource, "STT");
            lblTenNPL.DataBindings.Add("Text", this.DataSource, "TenNPL");
            lblMaNPL.DataBindings.Add("Text", this.DataSource, "MaNPL");
            lblDVT.DataBindings.Add("Text", this.DataSource, "DVT");
            lblSP1.TextAlignment = lblSP2.TextAlignment = lblSP3.TextAlignment = lblSP4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            if (dt.Columns["LuongSD0"].Caption!= "")
                lblSP1.Text = "Mã hàng: " + dt.Columns["DinhMuc0"].Caption + "\r\n" + "Số lượng: " + dt.Columns["LuongSD0"].Caption;

            if (dt.Columns["LuongSD1"].Caption != "")
                lblSP2.Text = "Mã hàng: " + dt.Columns["DinhMuc1"].Caption + "\r\n" + "Số lượng: " + dt.Columns["LuongSD1"].Caption;

            if (dt.Columns["LuongSD2"].Caption != "")
                lblSP3.Text = "Mã hàng: " + dt.Columns["DinhMuc2"].Caption + "\r\n" + "Số lượng: " + dt.Columns["LuongSD2"].Caption;

            if (dt.Columns["LuongSD3"].Caption != "")
                lblSP4.Text = "Mã hàng: " + dt.Columns["DinhMuc3"].Caption + "\r\n" + "Số lượng: " + dt.Columns["LuongSD3"].Caption;

            lblLuongSD1.DataBindings.Add("Text", this.DataSource, "LuongSD0", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD2.DataBindings.Add("Text", this.DataSource, "LuongSD1", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD3.DataBindings.Add("Text", this.DataSource, "LuongSD2", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");
            lblLuongSD4.DataBindings.Add("Text", this.DataSource, "LuongSD3", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");

            lblDinhMuc1.DataBindings.Add("Text", this.DataSource, "DinhMuc0", "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc2.DataBindings.Add("Text", this.DataSource, "DinhMuc1", "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc3.DataBindings.Add("Text", this.DataSource, "DinhMuc2", "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            lblDinhMuc4.DataBindings.Add("Text", this.DataSource, "DinhMuc3", "{0:N" + GlobalSettings.SoThapPhan.DinhMuc + "}");
            xrLabel19.Text = GlobalSettings.TieudeNgay;

            lblTongLuong.DataBindings.Add("Text", this.DataSource, "TongNPL", "{0:N" + GlobalSettings.SoThapPhan.LuongNPL + "}");



        }
        private void lblDinhMuc1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc0") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc0")) == 0) lblDinhMuc1.Text = "-";
        }
        private void lblDinhMuc2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc1") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc1")) == 0) lblDinhMuc2.Text = "-";
        }
        private void lblDinhMuc3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc2") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc2")) == 0) lblDinhMuc3.Text = "-";
        }
        private void lblDinhMuc4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc3") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc3")) == 0) lblDinhMuc4.Text = "-";
        }

        private void lblLuongSD1_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc0") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc0")) == 0) lblLuongSD1.Text = "-";
        }
        private void lblLuongSD2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc1") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc1")) == 0) lblLuongSD2.Text = "-";
        }
        private void lblLuongSD3_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc2") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc2")) == 0) lblLuongSD3.Text = "-";
        }
        private void lblLuongSD4_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            if (GetCurrentColumnValue("DinhMuc3") != DBNull.Value && Convert.ToDecimal(GetCurrentColumnValue("DinhMuc3")) == 0) lblLuongSD4.Text = "-";
        }

        private void lblhopsoHDGC_PreviewClick(object sender, PreviewMouseEventArgs e)
        {
            XRLabel label = (XRLabel)sender;
            report.Label = label;
            report.txtName.Text = label.Text;
            report.lblName.Text = label.Tag.ToString();
        }
        public void setText(XRLabel label, string text)
        {
            label.Text = text;
        }
    }
}
