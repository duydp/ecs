
/****** Object:  StoredProcedure [dbo].[p_SXXK_BCXuatNhapTon_TT196]    Script Date: 06/13/2013 09:11:13 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_BCXuatNhapTon_TT196]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_BCXuatNhapTon_TT196]
GO


/****** Object:  StoredProcedure [dbo].[p_SXXK_BCXuatNhapTon_TT196]    Script Date: 06/13/2013 09:11:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_SXXK_BCXuatNhapTon_TT196]
-- Database: ECS_TQ_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Wednesday, April 21, 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_BCXuatNhapTon_TT196]
		@LanThanhLy bigint
		,@NamThanhLy int
		,@MaDoanhNghiep varchar(50)
		,@SapXepNgayDangKyTKN bit
AS
BEGIN
	
SET NOCOUNT ON
--SET TRANSACTION ISOLATION LEVEL READ COMMITTED

if OBJECT_ID('tempdb..#BCNPLXuatNhapTon_TT196') is not null drop table #BCNPLXuatNhapTon_TT196

CREATE TABLE #BCNPLXuatNhapTon_TT196
(
	STT BIGINT
	,TenNPL NVARCHAR(1000)
	,DVT NVARCHAR(30)
	,SoToKhai NVARCHAR(50)
	,LuongNhap DECIMAL(18,8)
	,LuongTonDau DECIMAL(18,8)
	,LuongSuDung DECIMAL(18,8)
	,LuongTaiXuat DECIMAL(18,8)
	,LuongTonCuoi DECIMAL(18,8)
	,XuLy NVARCHAR(255)
	,MaNPL NVARCHAR(50)
	,STK BIGINT
	,MaLoaiHinh NVARCHAR(20)
	,NamDangKy bigint
)
IF(@SapXepNgayDangKyTKN = 0)
BEGIN -- Sắp xếp theo mã NPL
	
INSERT INTO #BCNPLXuatNhapTon_TT196
(
	STT,
	TenNPL,
	DVT,
	SoToKhai,
	LuongNhap,
	LuongTonDau,
	LuongSuDung,
	LuongTaiXuat,
	LuongTonCuoi,
	XuLy,
	MaNPL ,
	STK,
	MaLoaiHinh, 
	NamDangKy
)


SELECT 
	  (ROW_NUMBER () OVER( ORDER BY MaNPL,NgayDangKyNhap,SoToKhaiNhap)) AS STT
      ,(MIN([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL
      ,MAX(TenDVT_NPL) AS DVT
      ,(Convert(Nvarchar(10),SoToKhaiNhap) + ';' + MaLoaiHinhNhap + ';' + CONVERT(NVARCHAR(20),NgayDangKyNhap,103)) AS SoToKhai
      ,Max([LuongNhap]) AS LuongNhap
      ,MAX([LuongTonDau]) AS LuongTonDau
      ,SUM([LuongNPLSuDung]) AS LuongSuDung 
      ,SUM([LuongNPLTaiXuat]) AS LuongTaiXuat
      ,MIN([LuongTonCuoi]) AS LuongTonCuoi
	  ,'' AS  XuLy
	  ,MaNPL AS MaNPL
	  ,SoToKhaiNhap AS STK
	  ,MaLoaiHinhNhap AS MaLoaiHinh
	  ,YEAR(NgayDangKyNhap) AS NamDangKy
	  
  FROM [t_KDT_SXXK_BCXuatNhapTon]
  where LanThanhLy = @LanThanhLy and NamThanhLy = @NamThanhLy and MaDoanhNghiep = @MaDoanhNghiep
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap 
ORDER BY MaNPL,NgayDangKyNhap,SoToKhaiNhap

END
ELSE
	BEGIN -- sắp xếp theo ngày đăng ký
			
INSERT INTO #BCNPLXuatNhapTon_TT196
(
	STT,
	TenNPL,
	DVT,
	SoToKhai,
	LuongNhap,
	LuongTonDau,
	LuongSuDung,
	LuongTaiXuat,
	LuongTonCuoi,
	XuLy,
	MaNPL ,
	STK,
	MaLoaiHinh, 
	NamDangKy
)


SELECT 
	  (ROW_NUMBER () OVER( ORDER BY NgayDangKyNhap,SoToKhaiNhap, MaNPL)) AS STT
      ,(MIN([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL
      ,MAX(TenDVT_NPL) AS DVT
      ,(Convert(Nvarchar(10),SoToKhaiNhap) + ';' + MaLoaiHinhNhap + ';' + CONVERT(NVARCHAR(20),NgayDangKyNhap,103)) AS SoToKhai
      ,Max([LuongNhap]) AS LuongNhap
      ,MAX([LuongTonDau]) AS LuongTonDau
      ,SUM([LuongNPLSuDung]) AS LuongSuDung 
      ,SUM([LuongNPLTaiXuat]) AS LuongTaiXuat
      ,MIN([LuongTonCuoi]) AS LuongTonCuoi
	  ,'' AS  XuLy
	  ,MaNPL AS MaNPL
	  ,SoToKhaiNhap AS STK
	  ,MaLoaiHinhNhap AS MaLoaiHinh
	  ,YEAR(NgayDangKyNhap) AS NamDangKy
	  
  FROM [t_KDT_SXXK_BCXuatNhapTon]
  where LanThanhLy = @LanThanhLy and NamThanhLy = @NamThanhLy and MaDoanhNghiep = @MaDoanhNghiep
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap 
ORDER BY NgayDangKyNhap,SoToKhaiNhap,MaNPL

	END


UPDATE #BCNPLXuatNhapTon_TT196 
SET XuLy = (SELECT TOP 1 ThanhKhoanTiep FROM [t_KDT_SXXK_BCXuatNhapTon] bc
            WHERE bc.MaNPL = #BCNPLXuatNhapTon_TT196.MaNPL AND bc.MaLoaiHinhNhap = #BCNPLXuatNhapTon_TT196.MaLoaiHinh AND bc.SoToKhaiNhap = #BCNPLXuatNhapTon_TT196.STK AND
				  Year(bc.NgayDangKyNhap) = #BCNPLXuatNhapTon_TT196.NamDangKy AND bc.LuongTonCuoi = #BCNPLXuatNhapTon_TT196.LuongTonCuoi)

SELECT STT,TenNPL,DVT,SoToKhai,LuongNhap,LuongTonDau,LuongSuDung,LuongSuDung,LuongTaiXuat,LuongTonCuoi,XuLy
  FROM #BCNPLXuatNhapTon_TT196 				  
	
DROP TABLE #BCNPLXuatNhapTon_TT196

END


GO

/****** Object:  StoredProcedure [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]    Script Date: 06/14/2013 15:59:24 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_SXXK_BCThueXNK_TT196]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_SXXK_BCThueXNK_TT196]
GO


/****** Object:  StoredProcedure [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]    Script Date: 06/14/2013 15:59:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_CTTT_ChiPhiKhac_DeleteBy_IDCT]
-- Database: ECS_TQDT_SXXK
-- Author: Ngo Thanh Tung
-- Time created: Friday, October 07, 2011
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_SXXK_BCThueXNK_TT196]
		 @LanThanhLy bigint
		,@NamThanhLy int
		,@MaDoanhNghiep varchar(50)
		,@SapXepNgayDangKyTKN bit
AS

BEGIN
	
	
	if OBJECT_ID('tempdb..#BCThueXNK_TT196') is not null drop table #BCThueXNK_TT196

CREATE TABLE #BCThueXNK_TT196
(
	STT BIGINT
	,SoToKhai NVARCHAR(255)
	,TenNPL NVARCHAR(1000)
	,DVT NVARCHAR(30)
	,LuongNhap DECIMAL(18,5)
	,TienThueHoan DECIMAL(18,8)
	,ThuePhaiThu DECIMAL(18,8)
	,GhiChu NVARCHAR(255)
	,ThueNKNop DECIMAL(18,0)
	,LuongNPLSuDung DECIMAL(18,5)
)
IF(@SapXepNgayDangKyTKN = 1)
	
	BEGIN -- Sắp xếp theo ngày đăng ký
		
INSERT INTO #BCThueXNK_TT196
(
	STT,
	SoToKhai,
	TenNPL,
	DVT,
	LuongNhap,
	TienThueHoan,
	ThuePhaiThu,
	GhiChu,
	ThueNKNop,
	LuongNPLSuDung
	
)

SELECT 
	  (ROW_NUMBER () OVER( ORDER BY NgayDangKyNhap,SoToKhaiNhap, MaNPL)) AS STT
      ,(Convert(Nvarchar(10),SoToKhaiNhap) + ';' + MaLoaiHinhNhap + ';' + CONVERT(NVARCHAR(25),NgayDangKyNhap,103)) AS SoToKhai
      ,(MIN([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL
      ,MAX(TenDVT_NPL) AS DVT
      ,Max([LuongNhap]) AS LuongNhap
      ,0 AS TienThueHoan
      ,0 AS ThuePhaiThu
      ,'' as GhiChu
      ,MAX(ThueNKNop) AS ThueNKNop
	  ,SUM(LuongNPLSuDung) AS LuongSuDung
	  
  FROM t_KDT_SXXK_BCThueXNK
  where LanThanhLy = @LanThanhLy and NamThanhLy = @NamThanhLy and MaDoanhNghiep = @MaDoanhNghiep
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap 
ORDER BY NgayDangKyNhap,SoToKhaiNhap, MaNPL

		END
ELSE -- Sắp xếp theo mã NPL
	BEGIN
		
		INSERT INTO #BCThueXNK_TT196
(
	STT,
	SoToKhai,
	TenNPL,
	DVT,
	LuongNhap,
	TienThueHoan,
	ThuePhaiThu,
	GhiChu,
	ThueNKNop,
	LuongNPLSuDung
	
)

SELECT 
	  (ROW_NUMBER () OVER( ORDER BY  MaNPL ,NgayDangKyNhap,SoToKhaiNhap)) AS STT
      ,(Convert(Nvarchar(10),SoToKhaiNhap) + ';' + MaLoaiHinhNhap + ';' + CONVERT(NVARCHAR(25),NgayDangKyNhap,103)) AS SoToKhai
      ,(MIN([TenNPL]) + ' / ' + UPPER([MaNPL])) AS TenNPL
      ,MAX(TenDVT_NPL) AS DVT
      ,Max([LuongNhap]) AS LuongNhap
      ,0 AS TienThueHoan
      ,0 AS ThuePhaiThu
      ,'' as GhiChu
      ,MAX(ThueNKNop) AS ThueNKNop
	  ,SUM(LuongNPLSuDung) AS LuongSuDung
	  
  FROM t_KDT_SXXK_BCThueXNK
  where LanThanhLy = @LanThanhLy and NamThanhLy = @NamThanhLy and MaDoanhNghiep = @MaDoanhNghiep
GROUP BY MaNPL,SoToKhaiNhap ,NgayDangKyNhap, MaLoaiHinhNhap 
ORDER BY  MaNPL,NgayDangKyNhap,SoToKhaiNhap
		
	END
UPDATE #BCThueXNK_TT196
SET
	TienThueHoan = ROUND((LuongNPLSuDung/LuongNhap) * ThueNKNop,0)
UPDATE #BCThueXNK_TT196
SET	
	ThuePhaiThu =  ThueNKNop - TienThueHoan

SELECT * FROM #BCThueXNK_TT196

DROP TABLE #BCThueXNK_TT196
	
END

GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '9.2') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('9.2',GETDATE(), N'Cập nhật Báo cáo Thanh khoản')
END