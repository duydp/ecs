using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.QuanLyChungTu
{
	public partial class ChungThuGiamDinh : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long TKMD_ID { set; get; }
		public DateTime NgayDangKy { set; get; }
		public long SoTiepNhan { set; get; }
		public int TrangThai { set; get; }
		public int LoaiKB { set; get; }
		public string GuidStr { set; get; }
		public string DiaDiem { set; get; }
		public string MaCoQuanGD { set; get; }
		public string TenCoQuanGD { set; get; }
		public string CanBoGD { set; get; }
		public string NoiDung { set; get; }
		public string KetQua { set; get; }
		public string ThongTinKhac { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static IList<ChungThuGiamDinh> ConvertToCollection(IDataReader reader)
		{
			IList<ChungThuGiamDinh> collection = new List<ChungThuGiamDinh>();
			while (reader.Read())
			{
				ChungThuGiamDinh entity = new ChungThuGiamDinh();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("TKMD_ID"))) entity.TKMD_ID = reader.GetInt64(reader.GetOrdinal("TKMD_ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayDangKy"))) entity.NgayDangKy = reader.GetDateTime(reader.GetOrdinal("NgayDangKy"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThai"))) entity.TrangThai = reader.GetInt32(reader.GetOrdinal("TrangThai"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiKB"))) entity.LoaiKB = reader.GetInt32(reader.GetOrdinal("LoaiKB"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("DiaDiem"))) entity.DiaDiem = reader.GetString(reader.GetOrdinal("DiaDiem"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaCoQuanGD"))) entity.MaCoQuanGD = reader.GetString(reader.GetOrdinal("MaCoQuanGD"));
				if (!reader.IsDBNull(reader.GetOrdinal("TenCoQuanGD"))) entity.TenCoQuanGD = reader.GetString(reader.GetOrdinal("TenCoQuanGD"));
				if (!reader.IsDBNull(reader.GetOrdinal("CanBoGD"))) entity.CanBoGD = reader.GetString(reader.GetOrdinal("CanBoGD"));
				if (!reader.IsDBNull(reader.GetOrdinal("NoiDung"))) entity.NoiDung = reader.GetString(reader.GetOrdinal("NoiDung"));
				if (!reader.IsDBNull(reader.GetOrdinal("KetQua"))) entity.KetQua = reader.GetString(reader.GetOrdinal("KetQua"));
				if (!reader.IsDBNull(reader.GetOrdinal("ThongTinKhac"))) entity.ThongTinKhac = reader.GetString(reader.GetOrdinal("ThongTinKhac"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(IList<ChungThuGiamDinh> collection, long id)
        {
            foreach (ChungThuGiamDinh item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_ChungThuGiamDinh VALUES(@TKMD_ID, @NgayDangKy, @SoTiepNhan, @TrangThai, @LoaiKB, @GuidStr, @DiaDiem, @MaCoQuanGD, @TenCoQuanGD, @CanBoGD, @NoiDung, @KetQua, @ThongTinKhac)";
            string update = "UPDATE t_KDT_ChungThuGiamDinh SET TKMD_ID = @TKMD_ID, NgayDangKy = @NgayDangKy, SoTiepNhan = @SoTiepNhan, TrangThai = @TrangThai, LoaiKB = @LoaiKB, GuidStr = @GuidStr, DiaDiem = @DiaDiem, MaCoQuanGD = @MaCoQuanGD, TenCoQuanGD = @TenCoQuanGD, CanBoGD = @CanBoGD, NoiDung = @NoiDung, KetQua = @KetQua, ThongTinKhac = @ThongTinKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ChungThuGiamDinh WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKB", SqlDbType.Int, "LoaiKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.NVarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiem", SqlDbType.NVarChar, "DiaDiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCoQuanGD", SqlDbType.NVarChar, "MaCoQuanGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanGD", SqlDbType.NVarChar, "TenCoQuanGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CanBoGD", SqlDbType.NVarChar, "CanBoGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDung", SqlDbType.NVarChar, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQua", SqlDbType.NVarChar, "KetQua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKB", SqlDbType.Int, "LoaiKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.NVarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiem", SqlDbType.NVarChar, "DiaDiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCoQuanGD", SqlDbType.NVarChar, "MaCoQuanGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanGD", SqlDbType.NVarChar, "TenCoQuanGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CanBoGD", SqlDbType.NVarChar, "CanBoGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDung", SqlDbType.NVarChar, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQua", SqlDbType.NVarChar, "KetQua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_ChungThuGiamDinh VALUES(@TKMD_ID, @NgayDangKy, @SoTiepNhan, @TrangThai, @LoaiKB, @GuidStr, @DiaDiem, @MaCoQuanGD, @TenCoQuanGD, @CanBoGD, @NoiDung, @KetQua, @ThongTinKhac)";
            string update = "UPDATE t_KDT_ChungThuGiamDinh SET TKMD_ID = @TKMD_ID, NgayDangKy = @NgayDangKy, SoTiepNhan = @SoTiepNhan, TrangThai = @TrangThai, LoaiKB = @LoaiKB, GuidStr = @GuidStr, DiaDiem = @DiaDiem, MaCoQuanGD = @MaCoQuanGD, TenCoQuanGD = @TenCoQuanGD, CanBoGD = @CanBoGD, NoiDung = @NoiDung, KetQua = @KetQua, ThongTinKhac = @ThongTinKhac WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_ChungThuGiamDinh WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiKB", SqlDbType.Int, "LoaiKB", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.NVarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@DiaDiem", SqlDbType.NVarChar, "DiaDiem", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaCoQuanGD", SqlDbType.NVarChar, "MaCoQuanGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TenCoQuanGD", SqlDbType.NVarChar, "TenCoQuanGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CanBoGD", SqlDbType.NVarChar, "CanBoGD", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NoiDung", SqlDbType.NVarChar, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@KetQua", SqlDbType.NVarChar, "KetQua", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TKMD_ID", SqlDbType.BigInt, "TKMD_ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayDangKy", SqlDbType.DateTime, "NgayDangKy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThai", SqlDbType.Int, "TrangThai", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiKB", SqlDbType.Int, "LoaiKB", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.NVarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@DiaDiem", SqlDbType.NVarChar, "DiaDiem", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaCoQuanGD", SqlDbType.NVarChar, "MaCoQuanGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TenCoQuanGD", SqlDbType.NVarChar, "TenCoQuanGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CanBoGD", SqlDbType.NVarChar, "CanBoGD", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NoiDung", SqlDbType.NVarChar, "NoiDung", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@KetQua", SqlDbType.NVarChar, "KetQua", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@ThongTinKhac", SqlDbType.NVarChar, "ThongTinKhac", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static ChungThuGiamDinh Load(long id)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			IList<ChungThuGiamDinh> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static IList<ChungThuGiamDinh> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static IList<ChungThuGiamDinh> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IList<ChungThuGiamDinh> SelectCollectionBy_TKMD_ID(long tKMD_ID)
		{
            IDataReader reader = SelectReaderBy_TKMD_ID(tKMD_ID);
			return ConvertToCollection(reader);	
		}		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_SelectBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteDataSet(dbCommand);
		}
		//---------------------------------------------------------------------------------------------

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		public static IDataReader SelectReaderBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "p_KDT_ChungThuGiamDinh_SelectBy_TKMD_ID";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
			
            return db.ExecuteReader(dbCommand);
		}		
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertChungThuGiamDinh(long tKMD_ID, DateTime ngayDangKy, long soTiepNhan, int trangThai, int loaiKB, string guidStr, string diaDiem, string maCoQuanGD, string tenCoQuanGD, string canBoGD, string noiDung, string ketQua, string thongTinKhac)
		{
			ChungThuGiamDinh entity = new ChungThuGiamDinh();	
			entity.TKMD_ID = tKMD_ID;
			entity.NgayDangKy = ngayDangKy;
			entity.SoTiepNhan = soTiepNhan;
			entity.TrangThai = trangThai;
			entity.LoaiKB = loaiKB;
			entity.GuidStr = guidStr;
			entity.DiaDiem = diaDiem;
			entity.MaCoQuanGD = maCoQuanGD;
			entity.TenCoQuanGD = tenCoQuanGD;
			entity.CanBoGD = canBoGD;
			entity.NoiDung = noiDung;
			entity.KetQua = ketQua;
			entity.ThongTinKhac = thongTinKhac;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
			db.AddInParameter(dbCommand, "@DiaDiem", SqlDbType.NVarChar, DiaDiem);
			db.AddInParameter(dbCommand, "@MaCoQuanGD", SqlDbType.NVarChar, MaCoQuanGD);
			db.AddInParameter(dbCommand, "@TenCoQuanGD", SqlDbType.NVarChar, TenCoQuanGD);
			db.AddInParameter(dbCommand, "@CanBoGD", SqlDbType.NVarChar, CanBoGD);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
			db.AddInParameter(dbCommand, "@KetQua", SqlDbType.NVarChar, KetQua);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(IList<ChungThuGiamDinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungThuGiamDinh item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateChungThuGiamDinh(long id, long tKMD_ID, DateTime ngayDangKy, long soTiepNhan, int trangThai, int loaiKB, string guidStr, string diaDiem, string maCoQuanGD, string tenCoQuanGD, string canBoGD, string noiDung, string ketQua, string thongTinKhac)
		{
			ChungThuGiamDinh entity = new ChungThuGiamDinh();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.NgayDangKy = ngayDangKy;
			entity.SoTiepNhan = soTiepNhan;
			entity.TrangThai = trangThai;
			entity.LoaiKB = loaiKB;
			entity.GuidStr = guidStr;
			entity.DiaDiem = diaDiem;
			entity.MaCoQuanGD = maCoQuanGD;
			entity.TenCoQuanGD = tenCoQuanGD;
			entity.CanBoGD = canBoGD;
			entity.NoiDung = noiDung;
			entity.KetQua = ketQua;
			entity.ThongTinKhac = thongTinKhac;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_ChungThuGiamDinh_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
			db.AddInParameter(dbCommand, "@DiaDiem", SqlDbType.NVarChar, DiaDiem);
			db.AddInParameter(dbCommand, "@MaCoQuanGD", SqlDbType.NVarChar, MaCoQuanGD);
			db.AddInParameter(dbCommand, "@TenCoQuanGD", SqlDbType.NVarChar, TenCoQuanGD);
			db.AddInParameter(dbCommand, "@CanBoGD", SqlDbType.NVarChar, CanBoGD);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
			db.AddInParameter(dbCommand, "@KetQua", SqlDbType.NVarChar, KetQua);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(IList<ChungThuGiamDinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungThuGiamDinh item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateChungThuGiamDinh(long id, long tKMD_ID, DateTime ngayDangKy, long soTiepNhan, int trangThai, int loaiKB, string guidStr, string diaDiem, string maCoQuanGD, string tenCoQuanGD, string canBoGD, string noiDung, string ketQua, string thongTinKhac)
		{
			ChungThuGiamDinh entity = new ChungThuGiamDinh();			
			entity.ID = id;
			entity.TKMD_ID = tKMD_ID;
			entity.NgayDangKy = ngayDangKy;
			entity.SoTiepNhan = soTiepNhan;
			entity.TrangThai = trangThai;
			entity.LoaiKB = loaiKB;
			entity.GuidStr = guidStr;
			entity.DiaDiem = diaDiem;
			entity.MaCoQuanGD = maCoQuanGD;
			entity.TenCoQuanGD = tenCoQuanGD;
			entity.CanBoGD = canBoGD;
			entity.NoiDung = noiDung;
			entity.KetQua = ketQua;
			entity.ThongTinKhac = thongTinKhac;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, TKMD_ID);
			db.AddInParameter(dbCommand, "@NgayDangKy", SqlDbType.DateTime, NgayDangKy.Year <= 1753 ? DBNull.Value : (object) NgayDangKy);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@TrangThai", SqlDbType.Int, TrangThai);
			db.AddInParameter(dbCommand, "@LoaiKB", SqlDbType.Int, LoaiKB);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.NVarChar, GuidStr);
			db.AddInParameter(dbCommand, "@DiaDiem", SqlDbType.NVarChar, DiaDiem);
			db.AddInParameter(dbCommand, "@MaCoQuanGD", SqlDbType.NVarChar, MaCoQuanGD);
			db.AddInParameter(dbCommand, "@TenCoQuanGD", SqlDbType.NVarChar, TenCoQuanGD);
			db.AddInParameter(dbCommand, "@CanBoGD", SqlDbType.NVarChar, CanBoGD);
			db.AddInParameter(dbCommand, "@NoiDung", SqlDbType.NVarChar, NoiDung);
			db.AddInParameter(dbCommand, "@KetQua", SqlDbType.NVarChar, KetQua);
			db.AddInParameter(dbCommand, "@ThongTinKhac", SqlDbType.NVarChar, ThongTinKhac);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(IList<ChungThuGiamDinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungThuGiamDinh item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteChungThuGiamDinh(long id)
		{
			ChungThuGiamDinh entity = new ChungThuGiamDinh();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		public static int DeleteBy_TKMD_ID(long tKMD_ID)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_DeleteBy_TKMD_ID]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@TKMD_ID", SqlDbType.BigInt, tKMD_ID);
						
            return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
			
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_ChungThuGiamDinh_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(IList<ChungThuGiamDinh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (ChungThuGiamDinh item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}