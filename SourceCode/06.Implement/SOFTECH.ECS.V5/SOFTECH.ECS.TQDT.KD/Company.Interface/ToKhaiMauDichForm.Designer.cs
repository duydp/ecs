﻿using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using Company.Controls.CustomValidation;
using Janus.Windows.CalendarCombo;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;
using Janus.Windows.UI.CommandBars;
using Janus.Windows.UI.Dock;
using CuaKhauControl = Company.Interface.Controls.CuaKhauControl;
using DonViHaiQuanControl = Company.Interface.Controls.DonViHaiQuanControl;
using LoaiHinhMauDichControl01 = Company.Interface.Controls.LoaiHinhMauDichVControl;
using NguyenTeControl = Company.Interface.Controls.NguyenTeControl;
using NuocControl = Company.Interface.Controls.NuocControl;

namespace Company.Interface
{
    partial class ToKhaiMauDichForm
    {
        private Panel pnlToKhaiMauDich;
        private NumericEditBox txtTrongLuong;
        private NumericEditBox txtSoKien;
        private NumericEditBox txtTongSoContainer;
        private EditBox txtTenChuHang;
        private Label label29;
        private Label label31;
        private Label label21;
        private Label label22;
        private UIGroupBox grbNguyenTe;
        private Label label9;
        private Label label30;
        private UIGroupBox grbNuocXK;
        private UIGroupBox uiGroupBox6;
        private EditBox txtTenDaiLy;
        private EditBox txtMaDaiLy;
        private Label label7;
        private Label label8;
        private UIGroupBox uiGroupBox5;
        private EditBox txtMaDonViUyThac;
        private Label label5;
        private Label label6;
        private EditBox txtTenDonViUyThac;
        private UIGroupBox grbNguoiXK;
        private EditBox txtTenDonViDoiTac;
        private UIGroupBox uiGroupBox7;
        private CalendarCombo ccNgayHHHopDong;
        private EditBox txtSoHopDong;
        private Label label13;
        private Label label14;
        private Label label34;
        private CalendarCombo ccNgayHopDong;
        private UIGroupBox uiGroupBox18;
        private UIGroupBox uiGroupBox16;
        private UIGroupBox grbDiaDiemDoHang;
        private EditBox txtDiaDiemXepHang;
        private CalendarCombo ccNgayVanTaiDon;
        private EditBox txtSoVanTaiDon;
        private Label label19;
        private Label label20;
        private UIGroupBox uiGroupBox11;
        private UIComboBox cbPTVT;
        private Label label17;
        private Label lblSoHieuPTVT;
        private EditBox txtSoHieuPTVT;
        private Label lblNgayDenPTVT;
        private EditBox txtSoHoaDonThuongMai;
        private Label label15;
        private Label label16;
        private CalendarCombo ccNgayHHGiayPhep;
        private EditBox txtSoGiayPhep;
        private Label label11;
        private Label label12;
        private Label label33;
        private CalendarCombo ccNgayGiayPhep;
        private UIGroupBox grbHoaDonThuongMai;
        private UIGroupBox grbHopDong;
        private UIGroupBox grbDiaDiemXepHang;
        private UIGroupBox grbVanTaiDon;
        private UIGroupBox gbGiayPhep;
        private NumericEditBox txtTyGiaTinhThue;
        private NumericEditBox txtTyGiaUSD;
        private DataSet ds;
        private DataTable dtHangMauDich;
        private DataColumn dataColumn1;
        private DataColumn dataColumn2;
        private DataColumn dataColumn3;
        private DataColumn dataColumn4;
        private DataColumn dataColumn5;
        private DataColumn dataColumn6;
        private DataColumn dataColumn7;
        private DataColumn dataColumn8;
        private DataColumn dataColumn9;
        private DataTable dtLoaiHinhMauDich;
        private DataColumn dataColumn10;
        private DataColumn dataColumn11;
        private DataTable dtPTTT;
        private DataColumn dataColumn12;
        private DataColumn dataColumn13;
        private DataTable dtCuaKhau;
        private DataColumn dataColumn14;
        private DataColumn dataColumn15;
        private DataColumn dataColumn16;
        private DataTable dtNguyenTe;
        private DataColumn dataColumn17;
        private DataColumn dataColumn18;
        private DataTable dtCompanyNuoc;
        private DataColumn dataColumn19;
        private DataColumn dataColumn20;
        private ErrorProvider epError;
        private ContainerValidator cvError;
        private ListValidationSummary lvsError;
        private RequiredFieldValidator rfvSoHieuPTVT;
        private Label label1;
        private Label label2;
        private NumericEditBox txtLePhiHQ;
        private NumericEditBox txtPhiVanChuyen;
        private NumericEditBox txtPhiBaoHiem;
        private Label label4;
        private UIGroupBox uiGroupBox2;
        private DataTable dtDonViHaiQuan;
        private DataColumn dataColumn21;
        private DataColumn dataColumn22;
        private Label label3;
        private UIGroupBox uiGroupBox1;
        private UIPanelManager uiPanelManager1;
        private UIPanelGroup uiPanel0;
        private UIPanel uiPanel1;
        private UIPanelInnerContainer uiPanel1Container;
        private NumericEditBox txtSoLuongPLTK;
        private UIGroupBox grbNguoiNK;
        private EditBox txtTenDonVi;
        private EditBox txtMaDonVi;
        private CalendarCombo ccNgayDen;
        private CalendarCombo ccNgayHDTM;
        private RequiredFieldValidator rfvNguoiXuatKhau;
        private RangeValidator rvTyGiaTT;
        private RangeValidator rvTyGiaUSD;
        private UICommandManager cmMain;
        private UICommand cmdSave;
        private UIRebar TopRebar1;
        private UICommand cmdSave1;
        private UICommandBar cmbToolBar;
        private UIRebar BottomRebar1;
        private UIRebar LeftRebar1;
        private UIRebar RightRebar1;
        private IContainer components;


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Janus.Windows.GridEX.GridEXLayout dglistTKTK23_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridEX1_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout dgToKhaiTriGia_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            Janus.Windows.GridEX.GridEXLayout gridCO_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiMauDichForm));
            this.pnlToKhaiMauDich = new System.Windows.Forms.Panel();
            this.uiDamBaoNghiaVuNT = new Janus.Windows.EditControls.UIGroupBox();
            this.ccDamBaoNopThue_NgayKetThuc = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccDamBaoNopThue_NgayBatDau = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtDamBaoNopThue_HinhThuc = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chkDamBaoNopThue_isValue = new Janus.Windows.EditControls.UICheckBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtDamBaoNopThue_TriGia = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.uiAnHanThue = new Janus.Windows.EditControls.UIGroupBox();
            this.txtAnHanThue_LyDo = new Janus.Windows.GridEX.EditControls.EditBox();
            this.chkAnHangThue = new Janus.Windows.EditControls.UICheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.txtAnHanThue_ThoiGian = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.btnNoiDungDieuChinhTKForm = new Janus.Windows.EditControls.UIButton();
            this.label25 = new System.Windows.Forms.Label();
            this.txtTrongLuongTinh = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtPhiNganHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label24 = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtChungTu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbNguoiNK = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDonVi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtPhiBaoHiem = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.grbDiaDiemXepHang = new Janus.Windows.EditControls.UIGroupBox();
            this.txtDiaDiemXepHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPhiVanChuyen = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtLePhiHQ = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTrongLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtSoKien = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTongSoContainer = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtLyDoSua = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtDeXuatKhac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtChucVu = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtTenChuHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.grbNguyenTe = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNguyenTe = new Company.Interface.Controls.NguyenTeControl();
            this.txtTyGiaTinhThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.txtTyGiaUSD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.grbNuocXK = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrNuocXuatKhau = new Company.Interface.Controls.NuocControl();
            this.uiGroupBox6 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDaiLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaDaiLy = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.uiGroupBox5 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtMaDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtTenDonViUyThac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.grbNguoiXK = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTenDonViDoiTac = new Janus.Windows.GridEX.EditControls.EditBox();
            this.uiGroupBox7 = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrLoaiHinhMauDich = new Company.Interface.Controls.LoaiHinhMauDichVControl();
            this.grbHopDong = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHHHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHopDong = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.ccNgayHopDong = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.uiGroupBox18 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbPTTT = new Janus.Windows.EditControls.UIComboBox();
            this.uiGroupBox16 = new Janus.Windows.EditControls.UIGroupBox();
            this.cbDKGH = new Janus.Windows.EditControls.UIComboBox();
            this.grbDiaDiemDoHang = new Janus.Windows.EditControls.UIGroupBox();
            this.ctrCuaKhau = new Company.Interface.Controls.CuaKhauControl();
            this.grbVanTaiDon = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayVanTaiDon = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoVanTaiDon = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.uiGroupBox11 = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayDen = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.cbPTVT = new Janus.Windows.EditControls.UIComboBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lblSoHieuPTVT = new System.Windows.Forms.Label();
            this.txtSoHieuPTVT = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblNgayDenPTVT = new System.Windows.Forms.Label();
            this.gbGiayPhep = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHHGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoGiayPhep = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.ccNgayGiayPhep = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.grbHoaDonThuongMai = new Janus.Windows.EditControls.UIGroupBox();
            this.ccNgayHDTM = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.txtSoHoaDonThuongMai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.ds = new System.Data.DataSet();
            this.dtHangMauDich = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dtLoaiHinhMauDich = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dtPTTT = new System.Data.DataTable();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dtCuaKhau = new System.Data.DataTable();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dtNguyenTe = new System.Data.DataTable();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dtCompanyNuoc = new System.Data.DataTable();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dtDonViHaiQuan = new System.Data.DataTable();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvSoHieuPTVT = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.iconTrungToKhai = new System.Windows.Forms.PictureBox();
            this.txtHDPL = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.ccNgayDK = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.ccNgayTN = new Janus.Windows.CalendarCombo.CalendarCombo();
            this.label32 = new System.Windows.Forms.Label();
            this.lblPhanLuong = new System.Windows.Forms.Label();
            this.txtSoTiepNhan = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblSoTiepNhan = new System.Windows.Forms.Label();
            this.txtSoToKhai = new Janus.Windows.GridEX.EditControls.EditBox();
            this.lblTrangThai = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.ctrDonViHaiQuan = new Company.Interface.Controls.DonViHaiQuanControl();
            this.txtSoLuongPLTK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label44 = new System.Windows.Forms.Label();
            this.uiPanelManager1 = new Janus.Windows.UI.Dock.UIPanelManager(this.components);
            this.uiPanel4 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel4Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dglistTKTK23 = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.uiPanel0 = new Janus.Windows.UI.Dock.UIPanelGroup();
            this.uiPanel1 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel1Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.uiPanel2 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel2Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.gridEX1 = new Janus.Windows.GridEX.GridEX();
            this.TKGT = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel3Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.dgToKhaiTriGia = new Janus.Windows.GridEX.GridEX();
            this.uiPanel6 = new Janus.Windows.UI.Dock.UIPanel();
            this.uiPanel6Container = new Janus.Windows.UI.Dock.UIPanelInnerContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.gridCO = new Janus.Windows.GridEX.GridEX();
            this.rfvNguoiXuatKhau = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rvTyGiaTT = new Company.Controls.CustomValidation.RangeValidator();
            this.rvTyGiaUSD = new Company.Controls.CustomValidation.RangeValidator();
            this.cmMain = new Janus.Windows.UI.CommandBars.UICommandManager(this.components);
            this.BottomRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.cmbToolBar = new Janus.Windows.UI.CommandBars.UICommandBar();
            this.ThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("ThemHang");
            this.cmdSave1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdToKhaiTriGia1 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGia");
            this.ChungTuDinhKem1 = new Janus.Windows.UI.CommandBars.UICommand("ChungTuDinhKem");
            this.Separator2 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.TruyenDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("TruyenDuLieu");
            this.cmdBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSung");
            this.TienIch1 = new Janus.Windows.UI.CommandBars.UICommand("TienIch");
            this.cmdSave = new Janus.Windows.UI.CommandBars.UICommand("cmdSave");
            this.cmdThemHang = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdReadExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.cmdTinhLaiThue = new Janus.Windows.UI.CommandBars.UICommand("cmdTinhLaiThue");
            this.cmdPrint = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdInToKhaiTQDT_TT151 = new Janus.Windows.UI.CommandBars.UICommand("cmdInToKhaiTQDT_TT15");
            this.cmdInTKTCTT1961 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKTCTT196");
            this.cmdInTKTGPP11 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKTGPP1");
            this.cmdInTKDTSuaDoiBoSung1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKDTSuaDoiBoSung");
            this.cmdContainer1961 = new Janus.Windows.UI.CommandBars.UICommand("cmdContainer196");
            this.cmdInBangKeHD_ToKhai1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInBangKeHD_ToKhai");
            this.cmdInAnDinhThue1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInAnDinhThue");
            this.cmdInChungTuGiay1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInChungTuGiay");
            this.cmdInKiemTraHangHoa1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInKiemTraHangHoa");
            this.cmdSend = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.ChungTuKemTheo = new Janus.Windows.UI.CommandBars.UICommand("ChungTuKemTheo");
            this.NhanDuLieu = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.XacNhan = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.ThemHang2 = new Janus.Windows.UI.CommandBars.UICommand("ThemHang");
            this.cmdThemHang1 = new Janus.Windows.UI.CommandBars.UICommand("cmdThemHang");
            this.cmdReadExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.ChungTuKemTheo1 = new Janus.Windows.UI.CommandBars.UICommand("ChungTuKemTheo");
            this.FileDinhKem = new Janus.Windows.UI.CommandBars.UICommand("FileDinhKem");
            this.ToKhaiTriGia = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiTriGia");
            this.KhaiDienTu = new Janus.Windows.UI.CommandBars.UICommand("KhaiDienTu");
            this.cmdSend1 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.NhanDuLieu1 = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.XacNhan1 = new Janus.Windows.UI.CommandBars.UICommand("XacNhan");
            this.TienIch = new Janus.Windows.UI.CommandBars.UICommand("TienIch");
            this.cmdTinhLaiThue1 = new Janus.Windows.UI.CommandBars.UICommand("cmdTinhLaiThue");
            this.cmdPrint1 = new Janus.Windows.UI.CommandBars.UICommand("cmdPrint");
            this.cmdExportExcel1 = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.Huy = new Janus.Windows.UI.CommandBars.UICommand("Huy");
            this.cmdExportExcel = new Janus.Windows.UI.CommandBars.UICommand("cmdExportExcel");
            this.cmdToKhaiTriGia = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTriGia");
            this.ToKhaiTriGia1 = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiTriGia");
            this.cmdToKhaiTGPP21 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTGPP2");
            this.cmdToKhaiTGPP31 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTGPP3");
            this.cmdToKhaiTGPP2 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTGPP2");
            this.cmdToKhaiTGPP3 = new Janus.Windows.UI.CommandBars.UICommand("cmdToKhaiTGPP3");
            this.ToKhai = new Janus.Windows.UI.CommandBars.UICommand("ToKhai");
            this.InPhieuTN = new Janus.Windows.UI.CommandBars.UICommand("InPhieuTN");
            this.TruyenDuLieu = new Janus.Windows.UI.CommandBars.UICommand("TruyenDuLieu");
            this.cmdSend2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSend");
            this.NhanDuLieu2 = new Janus.Windows.UI.CommandBars.UICommand("NhanDuLieu");
            this.Separator1 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdSuaTK2 = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaTK");
            this.cmdHuyTK2 = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyTK");
            this.Separator3 = new Janus.Windows.UI.CommandBars.UICommand("Separator");
            this.cmdKetQuaXuLy1 = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.ChungTuDinhKem = new Janus.Windows.UI.CommandBars.UICommand("ChungTuDinhKem");
            this.VanDon2 = new Janus.Windows.UI.CommandBars.UICommand("VanDon");
            this.GiayPhep1 = new Janus.Windows.UI.CommandBars.UICommand("GiayPhep");
            this.HopDong1 = new Janus.Windows.UI.CommandBars.UICommand("HopDong");
            this.HoaDon1 = new Janus.Windows.UI.CommandBars.UICommand("HoaDon");
            this.CO1 = new Janus.Windows.UI.CommandBars.UICommand("CO");
            this.cmdChuyenCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenCuaKhau");
            this.cmdChungTuKem1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuKem");
            this.cmdChungTuNo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuNo");
            this.GiayKiemTra1 = new Janus.Windows.UI.CommandBars.UICommand("GiayKiemTra");
            this.ChungThuGiamDinh1 = new Janus.Windows.UI.CommandBars.UICommand("ChungThuGiamDinh");
            this.cmdChunTuTruocDo1 = new Janus.Windows.UI.CommandBars.UICommand("cmdChunTuTruocDo");
            this.cmdBaoLanhThue1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBaoLanhThue");
            this.cmdVanDon = new Janus.Windows.UI.CommandBars.UICommand("VanDon");
            this.cmdHopDong = new Janus.Windows.UI.CommandBars.UICommand("HopDong");
            this.cmdGiayPhep = new Janus.Windows.UI.CommandBars.UICommand("GiayPhep");
            this.cmdHoaDon = new Janus.Windows.UI.CommandBars.UICommand("HoaDon");
            this.cmdCO = new Janus.Windows.UI.CommandBars.UICommand("CO");
            this.ToKhaiA4 = new Janus.Windows.UI.CommandBars.UICommand("ToKhaiA4");
            this.cmdChuyenCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdChuyenCuaKhau");
            this.cmdBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSung");
            this.cmdBoSungCO1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungCO");
            this.cmdBoSungGiayPhep1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungGiayPhep");
            this.cmdBoSungHopDong1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungHopDong");
            this.cmdBoSungHoaDon1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungHoaDon");
            this.cmdBoSungChuyenCuaKhau1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungChuyenCuaKhau");
            this.cmdBoSungChungTuDinhKemDangAnh1 = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungChungTuDinhKemDangAnh");
            this.cmdGiayNopTien1 = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayNopTien");
            this.BSChungThuGiamDinh1 = new Janus.Windows.UI.CommandBars.UICommand("BSChungThuGiamDinh");
            this.cmdBoSungCO = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungCO");
            this.cmdBoSungGiayPhep = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungGiayPhep");
            this.cmdBoSungHopDong = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungHopDong");
            this.cmdBoSungHoaDon = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungHoaDon");
            this.cmdBoSungChuyenCuaKhau = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungChuyenCuaKhau");
            this.cmdHuyTK = new Janus.Windows.UI.CommandBars.UICommand("cmdHuyTK");
            this.cmdSuaTK = new Janus.Windows.UI.CommandBars.UICommand("cmdSuaTK");
            this.cmdXacNhanThongTin = new Janus.Windows.UI.CommandBars.UICommand("cmdXacNhanThongTin");
            this.cmdInTKTQ = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKTQ");
            this.cmdContainer = new Janus.Windows.UI.CommandBars.UICommand("cmdContainer");
            this.cmdChungTuKem = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuKem");
            this.cmdKetQuaXuLy = new Janus.Windows.UI.CommandBars.UICommand("cmdKetQuaXuLy");
            this.cmdLayPhanHoiSoTiepNhan = new Janus.Windows.UI.CommandBars.UICommand("cmdLayPhanHoiSoTiepNhan");
            this.cmdBoSungChungTuDinhKem = new Janus.Windows.UI.CommandBars.UICommand("cmdBoSungChungTuDinhKemDangAnh");
            this.cmdInTKSuaDoiBoSung = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKDTSuaDoiBoSung");
            this.cmdInAnDinhThue = new Janus.Windows.UI.CommandBars.UICommand("cmdInAnDinhThue");
            this.cmdChungTuNo = new Janus.Windows.UI.CommandBars.UICommand("cmdChungTuNo");
            this.cmdInToKhaiTQDT_TT15 = new Janus.Windows.UI.CommandBars.UICommand("cmdInToKhaiTQDT_TT15");
            this.cmdInPhuLucTKNK = new Janus.Windows.UI.CommandBars.UICommand("cmdInPhuLucTKNK");
            this.cmdGiayNopTien = new Janus.Windows.UI.CommandBars.UICommand("cmdGiayNopTien");
            this.cmdInTKTGPP1 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKTGPP1");
            this.GiayKiemTra = new Janus.Windows.UI.CommandBars.UICommand("GiayKiemTra");
            this.ChungThuGiamDinh = new Janus.Windows.UI.CommandBars.UICommand("ChungThuGiamDinh");
            this.BSChungThuGiamDinh = new Janus.Windows.UI.CommandBars.UICommand("BSChungThuGiamDinh");
            this.cmdInChungTuGiay = new Janus.Windows.UI.CommandBars.UICommand("cmdInChungTuGiay");
            this.cmdInKiemTraHangHoa = new Janus.Windows.UI.CommandBars.UICommand("cmdInKiemTraHangHoa");
            this.cmdContainer196 = new Janus.Windows.UI.CommandBars.UICommand("cmdContainer196");
            this.cmdInTKTCTT196 = new Janus.Windows.UI.CommandBars.UICommand("cmdInTKTCTT196");
            this.cmdChunTuTruocDo = new Janus.Windows.UI.CommandBars.UICommand("cmdChunTuTruocDo");
            this.cmdInBangKeHD_ToKhai = new Janus.Windows.UI.CommandBars.UICommand("cmdInBangKeHD_ToKhai");
            this.cmdBaoLanhThue = new Janus.Windows.UI.CommandBars.UICommand("cmdBaoLanhThue");
            this.LeftRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.RightRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.TopRebar1 = new Janus.Windows.UI.CommandBars.UIRebar();
            this.ilLarge = new System.Windows.Forms.ImageList(this.components);
            this.cmdReadExcel2 = new Janus.Windows.UI.CommandBars.UICommand("cmdReadExcel");
            this.ChungTuKemTheo2 = new Janus.Windows.UI.CommandBars.UICommand("ChungTuKemTheo");
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.uiPanel3 = new Janus.Windows.UI.Dock.UIPanel();
            this.rangeNH = new Company.Controls.CustomValidation.RangeValidator();
            this.rangeVC = new Company.Controls.CustomValidation.RangeValidator();
            this.rangePHQ = new Company.Controls.CustomValidation.RangeValidator();
            this.rangeBH = new Company.Controls.CustomValidation.RangeValidator();
            this.VanDon1 = new Janus.Windows.UI.CommandBars.UICommand("VanDon");
            this.gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter(this.components);
            this.rfvNgayDen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).BeginInit();
            this.pnlToKhaiMauDich.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiDamBaoNghiaVuNT)).BeginInit();
            this.uiDamBaoNghiaVuNT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiAnHanThue)).BeginInit();
            this.uiAnHanThue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNK)).BeginInit();
            this.grbNguoiNK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemXepHang)).BeginInit();
            this.grbDiaDiemXepHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguyenTe)).BeginInit();
            this.grbNguyenTe.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNuocXK)).BeginInit();
            this.grbNuocXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).BeginInit();
            this.uiGroupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).BeginInit();
            this.uiGroupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiXK)).BeginInit();
            this.grbNguoiXK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).BeginInit();
            this.uiGroupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDong)).BeginInit();
            this.grbHopDong.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).BeginInit();
            this.uiGroupBox18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).BeginInit();
            this.uiGroupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemDoHang)).BeginInit();
            this.grbDiaDiemDoHang.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbVanTaiDon)).BeginInit();
            this.grbVanTaiDon.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).BeginInit();
            this.uiGroupBox11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).BeginInit();
            this.gbGiayPhep.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHoaDonThuongMai)).BeginInit();
            this.grbHoaDonThuongMai.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuPTVT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconTrungToKhai)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel4)).BeginInit();
            this.uiPanel4.SuspendLayout();
            this.uiPanel4Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dglistTKTK23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).BeginInit();
            this.uiPanel0.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).BeginInit();
            this.uiPanel1.SuspendLayout();
            this.uiPanel1Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).BeginInit();
            this.uiPanel2.SuspendLayout();
            this.uiPanel2Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TKGT)).BeginInit();
            this.TKGT.SuspendLayout();
            this.uiPanel3Container.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhaiTriGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel6)).BeginInit();
            this.uiPanel6.SuspendLayout();
            this.uiPanel6Container.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridCO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiXuatKhau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).BeginInit();
            this.TopRebar1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeNH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeVC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangePHQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeBH)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Location = new System.Drawing.Point(3, 35);
            this.grbMain.Size = new System.Drawing.Size(1178, 700);
            // 
            // pnlToKhaiMauDich
            // 
            this.pnlToKhaiMauDich.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlToKhaiMauDich.AutoScroll = true;
            this.pnlToKhaiMauDich.AutoScrollMargin = new System.Drawing.Size(4, 4);
            this.pnlToKhaiMauDich.BackColor = System.Drawing.Color.Transparent;
            this.pnlToKhaiMauDich.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlToKhaiMauDich.Controls.Add(this.uiDamBaoNghiaVuNT);
            this.pnlToKhaiMauDich.Controls.Add(this.uiAnHanThue);
            this.pnlToKhaiMauDich.Controls.Add(this.btnNoiDungDieuChinhTKForm);
            this.pnlToKhaiMauDich.Controls.Add(this.label25);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTrongLuongTinh);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiNganHang);
            this.pnlToKhaiMauDich.Controls.Add(this.label24);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox4);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguoiNK);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiBaoHiem);
            this.pnlToKhaiMauDich.Controls.Add(this.grbDiaDiemXepHang);
            this.pnlToKhaiMauDich.Controls.Add(this.label4);
            this.pnlToKhaiMauDich.Controls.Add(this.txtPhiVanChuyen);
            this.pnlToKhaiMauDich.Controls.Add(this.label2);
            this.pnlToKhaiMauDich.Controls.Add(this.txtLePhiHQ);
            this.pnlToKhaiMauDich.Controls.Add(this.label1);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTrongLuong);
            this.pnlToKhaiMauDich.Controls.Add(this.txtSoKien);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTongSoContainer);
            this.pnlToKhaiMauDich.Controls.Add(this.txtLyDoSua);
            this.pnlToKhaiMauDich.Controls.Add(this.txtDeXuatKhac);
            this.pnlToKhaiMauDich.Controls.Add(this.label28);
            this.pnlToKhaiMauDich.Controls.Add(this.txtChucVu);
            this.pnlToKhaiMauDich.Controls.Add(this.txtTenChuHang);
            this.pnlToKhaiMauDich.Controls.Add(this.label26);
            this.pnlToKhaiMauDich.Controls.Add(this.label35);
            this.pnlToKhaiMauDich.Controls.Add(this.label29);
            this.pnlToKhaiMauDich.Controls.Add(this.label31);
            this.pnlToKhaiMauDich.Controls.Add(this.label21);
            this.pnlToKhaiMauDich.Controls.Add(this.label22);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguyenTe);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNuocXK);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox6);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox5);
            this.pnlToKhaiMauDich.Controls.Add(this.grbNguoiXK);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox7);
            this.pnlToKhaiMauDich.Controls.Add(this.grbHopDong);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox18);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox16);
            this.pnlToKhaiMauDich.Controls.Add(this.grbDiaDiemDoHang);
            this.pnlToKhaiMauDich.Controls.Add(this.grbVanTaiDon);
            this.pnlToKhaiMauDich.Controls.Add(this.uiGroupBox11);
            this.pnlToKhaiMauDich.Controls.Add(this.gbGiayPhep);
            this.pnlToKhaiMauDich.Controls.Add(this.grbHoaDonThuongMai);
            this.pnlToKhaiMauDich.Location = new System.Drawing.Point(0, 111);
            this.pnlToKhaiMauDich.Name = "pnlToKhaiMauDich";
            this.pnlToKhaiMauDich.Size = new System.Drawing.Size(1178, 589);
            this.pnlToKhaiMauDich.TabIndex = 10;
            // 
            // uiDamBaoNghiaVuNT
            // 
            this.uiDamBaoNghiaVuNT.AccessibleRole = System.Windows.Forms.AccessibleRole.CheckButton;
            this.uiDamBaoNghiaVuNT.Controls.Add(this.ccDamBaoNopThue_NgayKetThuc);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.ccDamBaoNopThue_NgayBatDau);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.txtDamBaoNopThue_HinhThuc);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.chkDamBaoNopThue_isValue);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.label38);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.label41);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.label40);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.label39);
            this.uiDamBaoNghiaVuNT.Controls.Add(this.txtDamBaoNopThue_TriGia);
            this.uiDamBaoNghiaVuNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiDamBaoNghiaVuNT.Location = new System.Drawing.Point(205, 595);
            this.uiDamBaoNghiaVuNT.Name = "uiDamBaoNghiaVuNT";
            this.uiDamBaoNghiaVuNT.Size = new System.Drawing.Size(343, 130);
            this.uiDamBaoNghiaVuNT.TabIndex = 38;
            this.uiDamBaoNghiaVuNT.Text = "Doanh nghiệp được đảm bảo nghĩa vụ nộp thuế";
            this.uiDamBaoNghiaVuNT.TextOffset = 7;
            this.uiDamBaoNghiaVuNT.UseCompatibleTextRendering = true;
            this.uiDamBaoNghiaVuNT.VisualStyleManager = this.vsmMain;
            // 
            // ccDamBaoNopThue_NgayKetThuc
            // 
            // 
            // 
            // 
            this.ccDamBaoNopThue_NgayKetThuc.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccDamBaoNopThue_NgayKetThuc.DropDownCalendar.Name = "";
            this.ccDamBaoNopThue_NgayKetThuc.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccDamBaoNopThue_NgayKetThuc.Enabled = false;
            this.ccDamBaoNopThue_NgayKetThuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccDamBaoNopThue_NgayKetThuc.IsNullDate = true;
            this.ccDamBaoNopThue_NgayKetThuc.Location = new System.Drawing.Point(205, 78);
            this.ccDamBaoNopThue_NgayKetThuc.Name = "ccDamBaoNopThue_NgayKetThuc";
            this.ccDamBaoNopThue_NgayKetThuc.Nullable = true;
            this.ccDamBaoNopThue_NgayKetThuc.NullButtonText = "Xóa";
            this.ccDamBaoNopThue_NgayKetThuc.ShowNullButton = true;
            this.ccDamBaoNopThue_NgayKetThuc.Size = new System.Drawing.Size(130, 21);
            this.ccDamBaoNopThue_NgayKetThuc.TabIndex = 40;
            this.ccDamBaoNopThue_NgayKetThuc.TodayButtonText = "Hôm nay";
            this.ccDamBaoNopThue_NgayKetThuc.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            // 
            // ccDamBaoNopThue_NgayBatDau
            // 
            // 
            // 
            // 
            this.ccDamBaoNopThue_NgayBatDau.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccDamBaoNopThue_NgayBatDau.DropDownCalendar.Name = "";
            this.ccDamBaoNopThue_NgayBatDau.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccDamBaoNopThue_NgayBatDau.Enabled = false;
            this.ccDamBaoNopThue_NgayBatDau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccDamBaoNopThue_NgayBatDau.IsNullDate = true;
            this.ccDamBaoNopThue_NgayBatDau.Location = new System.Drawing.Point(205, 38);
            this.ccDamBaoNopThue_NgayBatDau.Name = "ccDamBaoNopThue_NgayBatDau";
            this.ccDamBaoNopThue_NgayBatDau.Nullable = true;
            this.ccDamBaoNopThue_NgayBatDau.NullButtonText = "Xóa";
            this.ccDamBaoNopThue_NgayBatDau.ShowNullButton = true;
            this.ccDamBaoNopThue_NgayBatDau.Size = new System.Drawing.Size(131, 21);
            this.ccDamBaoNopThue_NgayBatDau.TabIndex = 40;
            this.ccDamBaoNopThue_NgayBatDau.TodayButtonText = "Hôm nay";
            this.ccDamBaoNopThue_NgayBatDau.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccDamBaoNopThue_NgayBatDau.VisualStyleManager = this.vsmMain;
            // 
            // txtDamBaoNopThue_HinhThuc
            // 
            this.txtDamBaoNopThue_HinhThuc.Enabled = false;
            this.txtDamBaoNopThue_HinhThuc.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDamBaoNopThue_HinhThuc.Location = new System.Drawing.Point(12, 78);
            this.txtDamBaoNopThue_HinhThuc.MaxLength = 255;
            this.txtDamBaoNopThue_HinhThuc.Multiline = true;
            this.txtDamBaoNopThue_HinhThuc.Name = "txtDamBaoNopThue_HinhThuc";
            this.txtDamBaoNopThue_HinhThuc.Size = new System.Drawing.Size(168, 48);
            this.txtDamBaoNopThue_HinhThuc.TabIndex = 3;
            this.txtDamBaoNopThue_HinhThuc.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDamBaoNopThue_HinhThuc.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // chkDamBaoNopThue_isValue
            // 
            this.chkDamBaoNopThue_isValue.BackColor = System.Drawing.Color.Transparent;
            this.chkDamBaoNopThue_isValue.Location = new System.Drawing.Point(0, -5);
            this.chkDamBaoNopThue_isValue.Name = "chkDamBaoNopThue_isValue";
            this.chkDamBaoNopThue_isValue.Size = new System.Drawing.Size(294, 23);
            this.chkDamBaoNopThue_isValue.TabIndex = 39;
            this.chkDamBaoNopThue_isValue.CheckedChanged += new System.EventHandler(this.chkDamBaoNopThue_isValue_CheckedChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(9, 62);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(113, 13);
            this.label38.TabIndex = 17;
            this.label38.Text = "Hình thức đảm bảo";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(201, 62);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(137, 13);
            this.label41.TabIndex = 17;
            this.label41.Text = "Ngày kết thúc đảm bảo";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(201, 22);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(133, 13);
            this.label40.TabIndex = 17;
            this.label40.Text = "Ngày bắt đầu đảm bảo";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(9, 22);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(94, 13);
            this.label39.TabIndex = 17;
            this.label39.Text = "Trị giá đảm bảo";
            // 
            // txtDamBaoNopThue_TriGia
            // 
            this.txtDamBaoNopThue_TriGia.DecimalDigits = 4;
            this.txtDamBaoNopThue_TriGia.Enabled = false;
            this.txtDamBaoNopThue_TriGia.Location = new System.Drawing.Point(12, 38);
            this.txtDamBaoNopThue_TriGia.MaxLength = 10;
            this.txtDamBaoNopThue_TriGia.Name = "txtDamBaoNopThue_TriGia";
            this.txtDamBaoNopThue_TriGia.Size = new System.Drawing.Size(168, 21);
            this.txtDamBaoNopThue_TriGia.TabIndex = 20;
            this.txtDamBaoNopThue_TriGia.Tag = "SoContainer20";
            this.txtDamBaoNopThue_TriGia.Text = "0.0000";
            this.txtDamBaoNopThue_TriGia.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtDamBaoNopThue_TriGia.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtDamBaoNopThue_TriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // uiAnHanThue
            // 
            this.uiAnHanThue.AccessibleRole = System.Windows.Forms.AccessibleRole.CheckButton;
            this.uiAnHanThue.Controls.Add(this.txtAnHanThue_LyDo);
            this.uiAnHanThue.Controls.Add(this.chkAnHangThue);
            this.uiAnHanThue.Controls.Add(this.label37);
            this.uiAnHanThue.Controls.Add(this.label36);
            this.uiAnHanThue.Controls.Add(this.txtAnHanThue_ThoiGian);
            this.uiAnHanThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiAnHanThue.Location = new System.Drawing.Point(8, 595);
            this.uiAnHanThue.Name = "uiAnHanThue";
            this.uiAnHanThue.Size = new System.Drawing.Size(188, 130);
            this.uiAnHanThue.TabIndex = 38;
            this.uiAnHanThue.Text = "Tờ khai được ân hạn thuế";
            this.uiAnHanThue.TextOffset = 7;
            this.uiAnHanThue.UseCompatibleTextRendering = true;
            this.uiAnHanThue.VisualStyleManager = this.vsmMain;
            // 
            // txtAnHanThue_LyDo
            // 
            this.txtAnHanThue_LyDo.Enabled = false;
            this.txtAnHanThue_LyDo.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAnHanThue_LyDo.Location = new System.Drawing.Point(12, 78);
            this.txtAnHanThue_LyDo.MaxLength = 255;
            this.txtAnHanThue_LyDo.Multiline = true;
            this.txtAnHanThue_LyDo.Name = "txtAnHanThue_LyDo";
            this.txtAnHanThue_LyDo.Size = new System.Drawing.Size(168, 48);
            this.txtAnHanThue_LyDo.TabIndex = 3;
            this.txtAnHanThue_LyDo.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtAnHanThue_LyDo.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // chkAnHangThue
            // 
            this.chkAnHangThue.BackColor = System.Drawing.Color.Transparent;
            this.chkAnHangThue.Location = new System.Drawing.Point(0, -5);
            this.chkAnHangThue.Name = "chkAnHangThue";
            this.chkAnHangThue.Size = new System.Drawing.Size(173, 23);
            this.chkAnHangThue.TabIndex = 39;
            this.chkAnHangThue.CheckedChanged += new System.EventHandler(this.chkAnHangThue_CheckedChanged);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(9, 62);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(109, 13);
            this.label37.TabIndex = 17;
            this.label37.Text = "Lý do được ân hạn";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(9, 22);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(140, 13);
            this.label36.TabIndex = 17;
            this.label36.Text = "Thời gian ân hạn (Ngày)";
            // 
            // txtAnHanThue_ThoiGian
            // 
            this.txtAnHanThue_ThoiGian.DecimalDigits = 0;
            this.txtAnHanThue_ThoiGian.Enabled = false;
            this.txtAnHanThue_ThoiGian.Location = new System.Drawing.Point(12, 38);
            this.txtAnHanThue_ThoiGian.MaxLength = 10;
            this.txtAnHanThue_ThoiGian.Name = "txtAnHanThue_ThoiGian";
            this.txtAnHanThue_ThoiGian.Size = new System.Drawing.Size(168, 21);
            this.txtAnHanThue_ThoiGian.TabIndex = 20;
            this.txtAnHanThue_ThoiGian.Tag = "SoContainer20";
            this.txtAnHanThue_ThoiGian.Text = "0";
            this.txtAnHanThue_ThoiGian.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtAnHanThue_ThoiGian.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtAnHanThue_ThoiGian.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // btnNoiDungDieuChinhTKForm
            // 
            this.btnNoiDungDieuChinhTKForm.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoiDungDieuChinhTKForm.Location = new System.Drawing.Point(627, 567);
            this.btnNoiDungDieuChinhTKForm.Name = "btnNoiDungDieuChinhTKForm";
            this.btnNoiDungDieuChinhTKForm.Size = new System.Drawing.Size(75, 23);
            this.btnNoiDungDieuChinhTKForm.TabIndex = 37;
            this.btnNoiDungDieuChinhTKForm.Text = "...";
            this.btnNoiDungDieuChinhTKForm.VisualStyleManager = this.vsmMain;
            this.btnNoiDungDieuChinhTKForm.Click += new System.EventHandler(this.btnNoiDungDieuChinhTKForm_Click_1);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(521, 415);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(100, 13);
            this.label25.TabIndex = 27;
            this.label25.Text = "Trọng lượng tịnh";
            // 
            // txtTrongLuongTinh
            // 
            this.txtTrongLuongTinh.DecimalDigits = 4;
            this.txtTrongLuongTinh.Location = new System.Drawing.Point(525, 431);
            this.txtTrongLuongTinh.MaxLength = 10;
            this.txtTrongLuongTinh.Name = "txtTrongLuongTinh";
            this.txtTrongLuongTinh.Size = new System.Drawing.Size(96, 21);
            this.txtTrongLuongTinh.TabIndex = 28;
            this.txtTrongLuongTinh.Tag = "TrongLuongTinh";
            this.txtTrongLuongTinh.Text = "0.0000";
            this.txtTrongLuongTinh.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuongTinh.Value = 0;
            this.txtTrongLuongTinh.ValueType = Janus.Windows.GridEX.NumericEditValueType.Double;
            this.txtTrongLuongTinh.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTrongLuongTinh.VisualStyleManager = this.vsmMain;
            // 
            // txtPhiNganHang
            // 
            this.txtPhiNganHang.DecimalDigits = 4;
            this.txtPhiNganHang.Location = new System.Drawing.Point(525, 480);
            this.txtPhiNganHang.MaxLength = 12;
            this.txtPhiNganHang.Name = "txtPhiNganHang";
            this.txtPhiNganHang.Size = new System.Drawing.Size(96, 21);
            this.txtPhiNganHang.TabIndex = 36;
            this.txtPhiNganHang.Tag = "PhiKhac";
            this.txtPhiNganHang.Text = "0.0000";
            this.txtPhiNganHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiNganHang.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtPhiNganHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtPhiNganHang.VisualStyleManager = this.vsmMain;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(525, 463);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(54, 13);
            this.label24.TabIndex = 35;
            this.label24.Text = "Phí khác";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.Controls.Add(this.txtChungTu);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(556, 303);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(190, 104);
            this.uiGroupBox4.TabIndex = 16;
            this.uiGroupBox4.Text = "Chứng từ";
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // txtChungTu
            // 
            this.txtChungTu.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtChungTu.ButtonText = "...";
            this.txtChungTu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChungTu.Location = new System.Drawing.Point(8, 20);
            this.txtChungTu.Multiline = true;
            this.txtChungTu.Name = "txtChungTu";
            this.txtChungTu.ReadOnly = true;
            this.txtChungTu.Size = new System.Drawing.Size(176, 75);
            this.txtChungTu.TabIndex = 1;
            this.txtChungTu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChungTu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtChungTu.VisualStyleManager = this.vsmMain;
            this.txtChungTu.ButtonClick += new System.EventHandler(this.txtChungTu_ButtonClick);
            // 
            // grbNguoiNK
            // 
            this.grbNguoiNK.Controls.Add(this.txtTenDonVi);
            this.grbNguoiNK.Controls.Add(this.txtMaDonVi);
            this.grbNguoiNK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiNK.Location = new System.Drawing.Point(12, 8);
            this.grbNguoiNK.Name = "grbNguoiNK";
            this.grbNguoiNK.Size = new System.Drawing.Size(184, 104);
            this.grbNguoiNK.TabIndex = 0;
            this.grbNguoiNK.Text = "Người nhập khẩu";
            this.grbNguoiNK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbNguoiNK.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDonVi
            // 
            this.txtTenDonVi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtTenDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonVi.Location = new System.Drawing.Point(8, 48);
            this.txtTenDonVi.Multiline = true;
            this.txtTenDonVi.Name = "txtTenDonVi";
            this.txtTenDonVi.ReadOnly = true;
            this.txtTenDonVi.Size = new System.Drawing.Size(168, 48);
            this.txtTenDonVi.TabIndex = 1;
            this.txtTenDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDonVi
            // 
            this.txtMaDonVi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtMaDonVi.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonVi.Location = new System.Drawing.Point(8, 20);
            this.txtMaDonVi.Name = "txtMaDonVi";
            this.txtMaDonVi.ReadOnly = true;
            this.txtMaDonVi.Size = new System.Drawing.Size(168, 21);
            this.txtMaDonVi.TabIndex = 0;
            this.txtMaDonVi.Text = "0400100457";
            this.txtMaDonVi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonVi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDonVi.VisualStyleManager = this.vsmMain;
            // 
            // txtPhiBaoHiem
            // 
            this.txtPhiBaoHiem.DecimalDigits = 4;
            this.txtPhiBaoHiem.Location = new System.Drawing.Point(312, 479);
            this.txtPhiBaoHiem.MaxLength = 12;
            this.txtPhiBaoHiem.Name = "txtPhiBaoHiem";
            this.txtPhiBaoHiem.Size = new System.Drawing.Size(96, 21);
            this.txtPhiBaoHiem.TabIndex = 32;
            this.txtPhiBaoHiem.Tag = "LePhiBaoHiem";
            this.txtPhiBaoHiem.Text = "0.0000";
            this.txtPhiBaoHiem.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiBaoHiem.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtPhiBaoHiem.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtPhiBaoHiem.VisualStyleManager = this.vsmMain;
            // 
            // grbDiaDiemXepHang
            // 
            this.grbDiaDiemXepHang.Controls.Add(this.txtDiaDiemXepHang);
            this.grbDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDiaDiemXepHang.Location = new System.Drawing.Point(556, 220);
            this.grbDiaDiemXepHang.Name = "grbDiaDiemXepHang";
            this.grbDiaDiemXepHang.Size = new System.Drawing.Size(190, 80);
            this.grbDiaDiemXepHang.TabIndex = 11;
            this.grbDiaDiemXepHang.Tag = "CangXepHang";
            this.grbDiaDiemXepHang.Text = "Địa điểm xếp hàng";
            this.grbDiaDiemXepHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbDiaDiemXepHang.VisualStyleManager = this.vsmMain;
            // 
            // txtDiaDiemXepHang
            // 
            this.txtDiaDiemXepHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiaDiemXepHang.Location = new System.Drawing.Point(8, 24);
            this.txtDiaDiemXepHang.MaxLength = 255;
            this.txtDiaDiemXepHang.Multiline = true;
            this.txtDiaDiemXepHang.Name = "txtDiaDiemXepHang";
            this.txtDiaDiemXepHang.Size = new System.Drawing.Size(176, 48);
            this.txtDiaDiemXepHang.TabIndex = 0;
            this.txtDiaDiemXepHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDiaDiemXepHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDiaDiemXepHang.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(312, 463);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 31;
            this.label4.Text = "Phí bảo hiểm";
            // 
            // txtPhiVanChuyen
            // 
            this.txtPhiVanChuyen.DecimalDigits = 4;
            this.txtPhiVanChuyen.Location = new System.Drawing.Point(420, 479);
            this.txtPhiVanChuyen.MaxLength = 20;
            this.txtPhiVanChuyen.Name = "txtPhiVanChuyen";
            this.txtPhiVanChuyen.Size = new System.Drawing.Size(92, 21);
            this.txtPhiVanChuyen.TabIndex = 34;
            this.txtPhiVanChuyen.Tag = "PhiVanChuyen";
            this.txtPhiVanChuyen.Text = "0.0000";
            this.txtPhiVanChuyen.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtPhiVanChuyen.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtPhiVanChuyen.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtPhiVanChuyen.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(420, 463);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 13);
            this.label2.TabIndex = 33;
            this.label2.Text = "Phí vận chuyển";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // txtLePhiHQ
            // 
            this.txtLePhiHQ.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtLePhiHQ.DecimalDigits = 0;
            this.txtLePhiHQ.Location = new System.Drawing.Point(205, 479);
            this.txtLePhiHQ.MaxLength = 12;
            this.txtLePhiHQ.Name = "txtLePhiHQ";
            this.txtLePhiHQ.Size = new System.Drawing.Size(96, 21);
            this.txtLePhiHQ.TabIndex = 30;
            this.txtLePhiHQ.Tag = "LePhiHQ";
            this.txtLePhiHQ.Text = "0";
            this.txtLePhiHQ.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtLePhiHQ.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtLePhiHQ.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLePhiHQ.VisualStyleManager = this.vsmMain;
            this.txtLePhiHQ.Click += new System.EventHandler(this.txtLePhiHQ_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(205, 463);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Lệ phí HQ (vnđ)";
            // 
            // txtTrongLuong
            // 
            this.txtTrongLuong.DecimalDigits = 4;
            this.txtTrongLuong.Location = new System.Drawing.Point(420, 431);
            this.txtTrongLuong.MaxLength = 10;
            this.txtTrongLuong.Name = "txtTrongLuong";
            this.txtTrongLuong.Size = new System.Drawing.Size(92, 21);
            this.txtTrongLuong.TabIndex = 26;
            this.txtTrongLuong.Tag = "TrongLuong";
            this.txtTrongLuong.Text = "0.0000";
            this.txtTrongLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTrongLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            262144});
            this.txtTrongLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTrongLuong.VisualStyleManager = this.vsmMain;
            // 
            // txtSoKien
            // 
            this.txtSoKien.DecimalDigits = 0;
            this.txtSoKien.Location = new System.Drawing.Point(312, 431);
            this.txtSoKien.MaxLength = 10;
            this.txtSoKien.Name = "txtSoKien";
            this.txtSoKien.Size = new System.Drawing.Size(99, 21);
            this.txtSoKien.TabIndex = 24;
            this.txtSoKien.Tag = "SoKienHang";
            this.txtSoKien.Text = "0";
            this.txtSoKien.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtSoKien.Value = 0;
            this.txtSoKien.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoKien.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoKien.VisualStyleManager = this.vsmMain;
            // 
            // txtTongSoContainer
            // 
            this.txtTongSoContainer.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtTongSoContainer.DecimalDigits = 0;
            this.txtTongSoContainer.Location = new System.Drawing.Point(205, 431);
            this.txtTongSoContainer.MaxLength = 10;
            this.txtTongSoContainer.Name = "txtTongSoContainer";
            this.txtTongSoContainer.Size = new System.Drawing.Size(96, 21);
            this.txtTongSoContainer.TabIndex = 20;
            this.txtTongSoContainer.Tag = "SoContainer20";
            this.txtTongSoContainer.Text = "0";
            this.txtTongSoContainer.TextAlignment = Janus.Windows.GridEX.TextAlignment.Far;
            this.txtTongSoContainer.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoContainer.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTongSoContainer.VisualStyleManager = this.vsmMain;
            this.txtTongSoContainer.Click += new System.EventHandler(this.txtSoContainer20_Click);
            // 
            // txtLyDoSua
            // 
            this.txtLyDoSua.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLyDoSua.Location = new System.Drawing.Point(312, 521);
            this.txtLyDoSua.Multiline = true;
            this.txtLyDoSua.Name = "txtLyDoSua";
            this.txtLyDoSua.Size = new System.Drawing.Size(309, 70);
            this.txtLyDoSua.TabIndex = 18;
            this.txtLyDoSua.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLyDoSua.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtLyDoSua.VisualStyleManager = this.vsmMain;
            // 
            // txtDeXuatKhac
            // 
            this.txtDeXuatKhac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDeXuatKhac.Location = new System.Drawing.Point(9, 520);
            this.txtDeXuatKhac.Multiline = true;
            this.txtDeXuatKhac.Name = "txtDeXuatKhac";
            this.txtDeXuatKhac.Size = new System.Drawing.Size(291, 70);
            this.txtDeXuatKhac.TabIndex = 18;
            this.txtDeXuatKhac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtDeXuatKhac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtDeXuatKhac.VisualStyleManager = this.vsmMain;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(312, 505);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(61, 13);
            this.label28.TabIndex = 17;
            this.label28.Text = "Lý do sửa";
            // 
            // txtChucVu
            // 
            this.txtChucVu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChucVu.Location = new System.Drawing.Point(9, 479);
            this.txtChucVu.MaxLength = 30;
            this.txtChucVu.Name = "txtChucVu";
            this.txtChucVu.Size = new System.Drawing.Size(187, 21);
            this.txtChucVu.TabIndex = 18;
            this.txtChucVu.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtChucVu.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            // 
            // txtTenChuHang
            // 
            this.txtTenChuHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenChuHang.Location = new System.Drawing.Point(9, 431);
            this.txtTenChuHang.MaxLength = 30;
            this.txtTenChuHang.Name = "txtTenChuHang";
            this.txtTenChuHang.Size = new System.Drawing.Size(187, 21);
            this.txtTenChuHang.TabIndex = 18;
            this.txtTenChuHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenChuHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenChuHang.VisualStyleManager = this.vsmMain;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(9, 504);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(82, 13);
            this.label26.TabIndex = 17;
            this.label26.Text = "Đề xuất khác";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(9, 463);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(134, 13);
            this.label35.TabIndex = 17;
            this.label35.Text = "Chức vụ người đại diện";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(9, 415);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(132, 13);
            this.label29.TabIndex = 17;
            this.label29.Text = "Đại diện doanh nghiệp";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(205, 415);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(78, 13);
            this.label31.TabIndex = 19;
            this.label31.Text = "Số container";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(312, 415);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 13);
            this.label21.TabIndex = 23;
            this.label21.Text = "Số kiện hàng";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(417, 415);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(75, 13);
            this.label22.TabIndex = 25;
            this.label22.Text = "Trọng lượng";
            // 
            // grbNguyenTe
            // 
            this.grbNguyenTe.Controls.Add(this.ctrNguyenTe);
            this.grbNguyenTe.Controls.Add(this.txtTyGiaTinhThue);
            this.grbNguyenTe.Controls.Add(this.label9);
            this.grbNguyenTe.Controls.Add(this.label30);
            this.grbNguyenTe.Controls.Add(this.txtTyGiaUSD);
            this.grbNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguyenTe.Location = new System.Drawing.Point(380, 303);
            this.grbNguyenTe.Name = "grbNguyenTe";
            this.grbNguyenTe.Size = new System.Drawing.Size(168, 104);
            this.grbNguyenTe.TabIndex = 15;
            this.grbNguyenTe.Text = "Đồng tiền thanh toán";
            this.grbNguyenTe.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbNguyenTe.VisualStyleManager = this.vsmMain;
            // 
            // ctrNguyenTe
            // 
            this.ctrNguyenTe.BackColor = System.Drawing.Color.Transparent;
            this.ctrNguyenTe.ErrorMessage = "\"Nguyên tệ\" không được bỏ trống.";
            this.ctrNguyenTe.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNguyenTe.Location = new System.Drawing.Point(8, 20);
            this.ctrNguyenTe.Ma = "";
            this.ctrNguyenTe.Name = "ctrNguyenTe";
            this.ctrNguyenTe.ReadOnly = false;
            this.ctrNguyenTe.Size = new System.Drawing.Size(166, 22);
            this.ctrNguyenTe.TabIndex = 0;
            this.ctrNguyenTe.Tag = "DongTienThanhToan";
            this.ctrNguyenTe.VisualStyleManager = this.vsmMain;
            this.ctrNguyenTe.ValueChanged += new Company.Interface.Controls.NguyenTeControl.ValueChangedEventHandler(this.ctrNguyenTe_ValueChanged);
            // 
            // txtTyGiaTinhThue
            // 
            this.txtTyGiaTinhThue.DecimalDigits = 2;
            this.txtTyGiaTinhThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaTinhThue.Location = new System.Drawing.Point(80, 47);
            this.txtTyGiaTinhThue.Name = "txtTyGiaTinhThue";
            this.txtTyGiaTinhThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaTinhThue.Size = new System.Drawing.Size(81, 21);
            this.txtTyGiaTinhThue.TabIndex = 2;
            this.txtTyGiaTinhThue.Tag = "TyGiaTinhThue";
            this.txtTyGiaTinhThue.Text = "131.00";
            this.txtTyGiaTinhThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaTinhThue.Value = new decimal(new int[] {
            13100,
            0,
            0,
            131072});
            this.txtTyGiaTinhThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTyGiaTinhThue.VisualStyleManager = this.vsmMain;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(59, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Tỷ giá USD";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(8, 55);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(51, 13);
            this.label30.TabIndex = 1;
            this.label30.Text = "Tỷ giá TT";
            // 
            // txtTyGiaUSD
            // 
            this.txtTyGiaUSD.DecimalDigits = 0;
            this.txtTyGiaUSD.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTyGiaUSD.Location = new System.Drawing.Point(80, 74);
            this.txtTyGiaUSD.Name = "txtTyGiaUSD";
            this.txtTyGiaUSD.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTyGiaUSD.Size = new System.Drawing.Size(81, 21);
            this.txtTyGiaUSD.TabIndex = 4;
            this.txtTyGiaUSD.Text = "131";
            this.txtTyGiaUSD.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTyGiaUSD.Value = new decimal(new int[] {
            131,
            0,
            0,
            0});
            this.txtTyGiaUSD.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTyGiaUSD.VisualStyleManager = this.vsmMain;
            // 
            // grbNuocXK
            // 
            this.grbNuocXK.Controls.Add(this.ctrNuocXuatKhau);
            this.grbNuocXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNuocXK.Location = new System.Drawing.Point(204, 220);
            this.grbNuocXK.Name = "grbNuocXK";
            this.grbNuocXK.Size = new System.Drawing.Size(168, 80);
            this.grbNuocXK.TabIndex = 9;
            this.grbNuocXK.Text = "Nước xuất khẩu";
            this.grbNuocXK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbNuocXK.VisualStyleManager = this.vsmMain;
            // 
            // ctrNuocXuatKhau
            // 
            this.ctrNuocXuatKhau.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXuatKhau.ErrorMessage = "\"Nước xuất khẩu\" bắt buộc phải chọn.";
            this.ctrNuocXuatKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXuatKhau.Location = new System.Drawing.Point(7, 24);
            this.ctrNuocXuatKhau.Ma = "";
            this.ctrNuocXuatKhau.Name = "ctrNuocXuatKhau";
            this.ctrNuocXuatKhau.ReadOnly = false;
            this.ctrNuocXuatKhau.Size = new System.Drawing.Size(169, 50);
            this.ctrNuocXuatKhau.TabIndex = 0;
            this.ctrNuocXuatKhau.Tag = "NuocXK";
            this.ctrNuocXuatKhau.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox6
            // 
            this.uiGroupBox6.Controls.Add(this.txtTenDaiLy);
            this.uiGroupBox6.Controls.Add(this.txtMaDaiLy);
            this.uiGroupBox6.Controls.Add(this.label7);
            this.uiGroupBox6.Controls.Add(this.label8);
            this.uiGroupBox6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox6.Location = new System.Drawing.Point(12, 303);
            this.uiGroupBox6.Name = "uiGroupBox6";
            this.uiGroupBox6.Size = new System.Drawing.Size(184, 104);
            this.uiGroupBox6.TabIndex = 12;
            this.uiGroupBox6.Text = "Đại lý làm TTHQ";
            this.uiGroupBox6.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox6.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDaiLy
            // 
            this.txtTenDaiLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDaiLy.Location = new System.Drawing.Point(40, 47);
            this.txtTenDaiLy.MaxLength = 255;
            this.txtTenDaiLy.Multiline = true;
            this.txtTenDaiLy.Name = "txtTenDaiLy";
            this.txtTenDaiLy.Size = new System.Drawing.Size(136, 48);
            this.txtTenDaiLy.TabIndex = 3;
            this.txtTenDaiLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDaiLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDaiLy.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDaiLy
            // 
            this.txtMaDaiLy.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDaiLy.Location = new System.Drawing.Point(40, 20);
            this.txtMaDaiLy.MaxLength = 14;
            this.txtMaDaiLy.Name = "txtMaDaiLy";
            this.txtMaDaiLy.Size = new System.Drawing.Size(136, 21);
            this.txtMaDaiLy.TabIndex = 1;
            this.txtMaDaiLy.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDaiLy.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDaiLy.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(21, 13);
            this.label7.TabIndex = 0;
            this.label7.Tag = "Ma";
            this.label7.Text = "Mã";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(8, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(25, 13);
            this.label8.TabIndex = 2;
            this.label8.Tag = "Ten";
            this.label8.Text = "Tên";
            // 
            // uiGroupBox5
            // 
            this.uiGroupBox5.Controls.Add(this.txtMaDonViUyThac);
            this.uiGroupBox5.Controls.Add(this.label5);
            this.uiGroupBox5.Controls.Add(this.label6);
            this.uiGroupBox5.Controls.Add(this.txtTenDonViUyThac);
            this.uiGroupBox5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox5.Location = new System.Drawing.Point(12, 220);
            this.uiGroupBox5.Name = "uiGroupBox5";
            this.uiGroupBox5.Size = new System.Drawing.Size(184, 80);
            this.uiGroupBox5.TabIndex = 8;
            this.uiGroupBox5.Text = "Người ủy thác";
            this.uiGroupBox5.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox5.VisualStyleManager = this.vsmMain;
            // 
            // txtMaDonViUyThac
            // 
            this.txtMaDonViUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaDonViUyThac.Location = new System.Drawing.Point(40, 20);
            this.txtMaDonViUyThac.MaxLength = 15;
            this.txtMaDonViUyThac.Name = "txtMaDonViUyThac";
            this.txtMaDonViUyThac.Size = new System.Drawing.Size(136, 21);
            this.txtMaDonViUyThac.TabIndex = 1;
            this.txtMaDonViUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaDonViUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMaDonViUyThac.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Mã";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 2;
            this.label6.Tag = "TenDVUT";
            this.label6.Text = "Tên";
            // 
            // txtTenDonViUyThac
            // 
            this.txtTenDonViUyThac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonViUyThac.Location = new System.Drawing.Point(40, 47);
            this.txtTenDonViUyThac.MaxLength = 55;
            this.txtTenDonViUyThac.Name = "txtTenDonViUyThac";
            this.txtTenDonViUyThac.Size = new System.Drawing.Size(136, 21);
            this.txtTenDonViUyThac.TabIndex = 3;
            this.txtTenDonViUyThac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonViUyThac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDonViUyThac.VisualStyleManager = this.vsmMain;
            // 
            // grbNguoiXK
            // 
            this.grbNguoiXK.Controls.Add(this.txtTenDonViDoiTac);
            this.grbNguoiXK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbNguoiXK.Location = new System.Drawing.Point(12, 114);
            this.grbNguoiXK.Name = "grbNguoiXK";
            this.grbNguoiXK.Size = new System.Drawing.Size(184, 104);
            this.grbNguoiXK.TabIndex = 4;
            this.grbNguoiXK.Tag = "NguoiXuatKhau";
            this.grbNguoiXK.Text = "Người xuất khẩu";
            this.grbNguoiXK.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbNguoiXK.VisualStyleManager = this.vsmMain;
            // 
            // txtTenDonViDoiTac
            // 
            this.txtTenDonViDoiTac.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtTenDonViDoiTac.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenDonViDoiTac.Location = new System.Drawing.Point(8, 20);
            this.txtTenDonViDoiTac.MaxLength = 500;
            this.txtTenDonViDoiTac.Multiline = true;
            this.txtTenDonViDoiTac.Name = "txtTenDonViDoiTac";
            this.txtTenDonViDoiTac.Size = new System.Drawing.Size(168, 78);
            this.txtTenDonViDoiTac.TabIndex = 0;
            this.txtTenDonViDoiTac.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenDonViDoiTac.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtTenDonViDoiTac.VisualStyleManager = this.vsmMain;
            this.txtTenDonViDoiTac.ButtonClick += new System.EventHandler(this.txtTenDonViDoiTac_ButtonClick);
            // 
            // uiGroupBox7
            // 
            this.uiGroupBox7.Controls.Add(this.ctrLoaiHinhMauDich);
            this.uiGroupBox7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox7.Location = new System.Drawing.Point(204, 8);
            this.uiGroupBox7.Name = "uiGroupBox7";
            this.uiGroupBox7.Size = new System.Drawing.Size(168, 104);
            this.uiGroupBox7.TabIndex = 1;
            this.uiGroupBox7.Text = "Loại hình mậu dịch";
            this.uiGroupBox7.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox7.VisualStyleManager = this.vsmMain;
            // 
            // ctrLoaiHinhMauDich
            // 
            this.ctrLoaiHinhMauDich.BackColor = System.Drawing.Color.Transparent;
            this.ctrLoaiHinhMauDich.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrLoaiHinhMauDich.Location = new System.Drawing.Point(7, 24);
            this.ctrLoaiHinhMauDich.Ma = "";
            this.ctrLoaiHinhMauDich.Name = "ctrLoaiHinhMauDich";
            this.ctrLoaiHinhMauDich.Nhom = "";
            this.ctrLoaiHinhMauDich.ReadOnly = false;
            this.ctrLoaiHinhMauDich.Size = new System.Drawing.Size(169, 50);
            this.ctrLoaiHinhMauDich.TabIndex = 0;
            this.ctrLoaiHinhMauDich.VisualStyleManager = this.vsmMain;
            this.ctrLoaiHinhMauDich.Leave += new System.EventHandler(this.ctrLoaiHinhMauDich_Leave);
            // 
            // grbHopDong
            // 
            this.grbHopDong.Controls.Add(this.ccNgayHHHopDong);
            this.grbHopDong.Controls.Add(this.txtSoHopDong);
            this.grbHopDong.Controls.Add(this.label13);
            this.grbHopDong.Controls.Add(this.label14);
            this.grbHopDong.Controls.Add(this.label34);
            this.grbHopDong.Controls.Add(this.ccNgayHopDong);
            this.grbHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHopDong.Location = new System.Drawing.Point(556, 8);
            this.grbHopDong.Name = "grbHopDong";
            this.grbHopDong.Size = new System.Drawing.Size(190, 104);
            this.grbHopDong.TabIndex = 3;
            this.grbHopDong.Text = "Hợp đồng";
            this.grbHopDong.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbHopDong.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayHHHopDong
            // 
            // 
            // 
            // 
            this.ccNgayHHHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHHopDong.DropDownCalendar.Name = "";
            this.ccNgayHHHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHHHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHHopDong.IsNullDate = true;
            this.ccNgayHHHopDong.Location = new System.Drawing.Point(88, 77);
            this.ccNgayHHHopDong.Name = "ccNgayHHHopDong";
            this.ccNgayHHHopDong.Nullable = true;
            this.ccNgayHHHopDong.NullButtonText = "Xóa";
            this.ccNgayHHHopDong.ShowNullButton = true;
            this.ccNgayHHHopDong.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHHHopDong.TabIndex = 5;
            this.ccNgayHHHopDong.TodayButtonText = "Hôm nay";
            this.ccNgayHHHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHHHopDong.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHopDong
            // 
            this.txtSoHopDong.ButtonText = "...";
            this.txtSoHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHopDong.Location = new System.Drawing.Point(88, 20);
            this.txtSoHopDong.Name = "txtSoHopDong";
            this.txtSoHopDong.Size = new System.Drawing.Size(96, 21);
            this.txtSoHopDong.TabIndex = 1;
            this.txtSoHopDong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHopDong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHopDong.VisualStyleManager = this.vsmMain;
            this.txtSoHopDong.ButtonClick += new System.EventHandler(this.txtSoHopDong_ButtonClick);
            this.txtSoHopDong.Leave += new System.EventHandler(this.txtSoHopDong_Leave);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(8, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 13);
            this.label13.TabIndex = 0;
            this.label13.Tag = "SoHD";
            this.label13.Text = "Số";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(8, 56);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 13);
            this.label14.TabIndex = 2;
            this.label14.Tag = "NgayHD";
            this.label14.Text = "Ngày";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(8, 85);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(49, 13);
            this.label34.TabIndex = 4;
            this.label34.Tag = "NgayHetHanHD";
            this.label34.Text = "Ngày HH";
            // 
            // ccNgayHopDong
            // 
            // 
            // 
            // 
            this.ccNgayHopDong.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHopDong.DropDownCalendar.Name = "";
            this.ccNgayHopDong.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHopDong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHopDong.IsNullDate = true;
            this.ccNgayHopDong.Location = new System.Drawing.Point(88, 48);
            this.ccNgayHopDong.Name = "ccNgayHopDong";
            this.ccNgayHopDong.Nullable = true;
            this.ccNgayHopDong.NullButtonText = "Xóa";
            this.ccNgayHopDong.ShowNullButton = true;
            this.ccNgayHopDong.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHopDong.TabIndex = 3;
            this.ccNgayHopDong.TodayButtonText = "Hôm nay";
            this.ccNgayHopDong.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHopDong.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox18
            // 
            this.uiGroupBox18.Controls.Add(this.cbPTTT);
            this.uiGroupBox18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox18.Location = new System.Drawing.Point(204, 358);
            this.uiGroupBox18.Name = "uiGroupBox18";
            this.uiGroupBox18.Size = new System.Drawing.Size(168, 49);
            this.uiGroupBox18.TabIndex = 14;
            this.uiGroupBox18.Text = "Phương thức thanh toán";
            this.uiGroupBox18.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox18.VisualStyleManager = this.vsmMain;
            // 
            // cbPTTT
            // 
            this.cbPTTT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTTT.DisplayMember = "Name";
            this.cbPTTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTTT.Location = new System.Drawing.Point(7, 19);
            this.cbPTTT.Name = "cbPTTT";
            this.cbPTTT.Size = new System.Drawing.Size(152, 21);
            this.cbPTTT.TabIndex = 0;
            this.cbPTTT.Tag = "PhuongThucThanhToan";
            this.cbPTTT.ValueMember = "ID";
            this.cbPTTT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbPTTT.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox16
            // 
            this.uiGroupBox16.Controls.Add(this.cbDKGH);
            this.uiGroupBox16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox16.Location = new System.Drawing.Point(204, 303);
            this.uiGroupBox16.Name = "uiGroupBox16";
            this.uiGroupBox16.Size = new System.Drawing.Size(168, 48);
            this.uiGroupBox16.TabIndex = 13;
            this.uiGroupBox16.Text = "Điều kiện giao hàng";
            this.uiGroupBox16.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox16.VisualStyleManager = this.vsmMain;
            // 
            // cbDKGH
            // 
            this.cbDKGH.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDKGH.DisplayMember = "ID";
            this.cbDKGH.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDKGH.Location = new System.Drawing.Point(7, 19);
            this.cbDKGH.Name = "cbDKGH";
            this.cbDKGH.Size = new System.Drawing.Size(152, 21);
            this.cbDKGH.TabIndex = 0;
            this.cbDKGH.Tag = "DieuKienGiaoHang";
            this.cbDKGH.ValueMember = "ID";
            this.cbDKGH.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbDKGH.VisualStyleManager = this.vsmMain;
            // 
            // grbDiaDiemDoHang
            // 
            this.grbDiaDiemDoHang.Controls.Add(this.ctrCuaKhau);
            this.grbDiaDiemDoHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbDiaDiemDoHang.Location = new System.Drawing.Point(380, 220);
            this.grbDiaDiemDoHang.Name = "grbDiaDiemDoHang";
            this.grbDiaDiemDoHang.Size = new System.Drawing.Size(168, 80);
            this.grbDiaDiemDoHang.TabIndex = 10;
            this.grbDiaDiemDoHang.Tag = "CangDoHang";
            this.grbDiaDiemDoHang.Text = "Địa điểm dỡ hàng";
            this.grbDiaDiemDoHang.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbDiaDiemDoHang.VisualStyleManager = this.vsmMain;
            // 
            // ctrCuaKhau
            // 
            this.ctrCuaKhau.BackColor = System.Drawing.Color.Transparent;
            this.ctrCuaKhau.ErrorMessage = "\"Địa điểm dỡ hàng\" bắt buộc phải chọn.";
            this.ctrCuaKhau.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrCuaKhau.Location = new System.Drawing.Point(8, 24);
            this.ctrCuaKhau.Ma = "";
            this.ctrCuaKhau.Name = "ctrCuaKhau";
            this.ctrCuaKhau.ReadOnly = false;
            this.ctrCuaKhau.Size = new System.Drawing.Size(151, 50);
            this.ctrCuaKhau.TabIndex = 0;
            this.ctrCuaKhau.VisualStyleManager = this.vsmMain;
            // 
            // grbVanTaiDon
            // 
            this.grbVanTaiDon.Controls.Add(this.ccNgayVanTaiDon);
            this.grbVanTaiDon.Controls.Add(this.txtSoVanTaiDon);
            this.grbVanTaiDon.Controls.Add(this.label19);
            this.grbVanTaiDon.Controls.Add(this.label20);
            this.grbVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbVanTaiDon.Location = new System.Drawing.Point(556, 114);
            this.grbVanTaiDon.Name = "grbVanTaiDon";
            this.grbVanTaiDon.Size = new System.Drawing.Size(190, 104);
            this.grbVanTaiDon.TabIndex = 7;
            this.grbVanTaiDon.Text = "Vận tải đơn";
            this.grbVanTaiDon.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbVanTaiDon.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayVanTaiDon
            // 
            // 
            // 
            // 
            this.ccNgayVanTaiDon.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayVanTaiDon.DropDownCalendar.Name = "";
            this.ccNgayVanTaiDon.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayVanTaiDon.IsNullDate = true;
            this.ccNgayVanTaiDon.Location = new System.Drawing.Point(88, 47);
            this.ccNgayVanTaiDon.Name = "ccNgayVanTaiDon";
            this.ccNgayVanTaiDon.Nullable = true;
            this.ccNgayVanTaiDon.NullButtonText = "Xóa";
            this.ccNgayVanTaiDon.ShowNullButton = true;
            this.ccNgayVanTaiDon.Size = new System.Drawing.Size(96, 21);
            this.ccNgayVanTaiDon.TabIndex = 3;
            this.ccNgayVanTaiDon.TodayButtonText = "Hôm nay";
            this.ccNgayVanTaiDon.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayVanTaiDon.VisualStyleManager = this.vsmMain;
            // 
            // txtSoVanTaiDon
            // 
            this.txtSoVanTaiDon.ButtonStyle = Janus.Windows.GridEX.EditControls.EditButtonStyle.Ellipsis;
            this.txtSoVanTaiDon.ButtonText = "...";
            this.txtSoVanTaiDon.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoVanTaiDon.Location = new System.Drawing.Point(88, 20);
            this.txtSoVanTaiDon.Name = "txtSoVanTaiDon";
            this.txtSoVanTaiDon.Size = new System.Drawing.Size(96, 21);
            this.txtSoVanTaiDon.TabIndex = 1;
            this.txtSoVanTaiDon.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoVanTaiDon.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoVanTaiDon.VisualStyleManager = this.vsmMain;
            this.txtSoVanTaiDon.ButtonClick += new System.EventHandler(this.txtSoVanTaiDon_ButtonClick);
            this.txtSoVanTaiDon.Leave += new System.EventHandler(this.txtSoVanTaiDon_Leave);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(8, 28);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(19, 13);
            this.label19.TabIndex = 0;
            this.label19.Tag = "SoVanDon";
            this.label19.Text = "Số";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(8, 55);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 2;
            this.label20.Tag = "NgayVanDon";
            this.label20.Text = "Ngày";
            // 
            // uiGroupBox11
            // 
            this.uiGroupBox11.Controls.Add(this.ccNgayDen);
            this.uiGroupBox11.Controls.Add(this.cbPTVT);
            this.uiGroupBox11.Controls.Add(this.label17);
            this.uiGroupBox11.Controls.Add(this.lblSoHieuPTVT);
            this.uiGroupBox11.Controls.Add(this.txtSoHieuPTVT);
            this.uiGroupBox11.Controls.Add(this.lblNgayDenPTVT);
            this.uiGroupBox11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox11.Location = new System.Drawing.Point(204, 114);
            this.uiGroupBox11.Name = "uiGroupBox11";
            this.uiGroupBox11.Size = new System.Drawing.Size(168, 104);
            this.uiGroupBox11.TabIndex = 5;
            this.uiGroupBox11.Text = "Phương tiện vận tải";
            this.uiGroupBox11.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox11.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayDen
            // 
            // 
            // 
            // 
            this.ccNgayDen.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDen.DropDownCalendar.Name = "";
            this.ccNgayDen.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDen.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDen.IsNullDate = true;
            this.ccNgayDen.Location = new System.Drawing.Point(65, 75);
            this.ccNgayDen.Name = "ccNgayDen";
            this.ccNgayDen.NullButtonText = "Xóa";
            this.ccNgayDen.ShowNullButton = true;
            this.ccNgayDen.Size = new System.Drawing.Size(96, 21);
            this.ccNgayDen.TabIndex = 5;
            this.ccNgayDen.TodayButtonText = "Hôm nay";
            this.ccNgayDen.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDen.VisualStyleManager = this.vsmMain;
            // 
            // cbPTVT
            // 
            this.cbPTVT.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbPTVT.DisplayMember = "Ten";
            this.cbPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPTVT.Location = new System.Drawing.Point(65, 20);
            this.cbPTVT.Name = "cbPTVT";
            this.cbPTVT.Size = new System.Drawing.Size(96, 21);
            this.cbPTVT.TabIndex = 1;
            this.cbPTVT.ValueMember = "ID";
            this.cbPTVT.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.cbPTVT.VisualStyleManager = this.vsmMain;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(3, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(31, 13);
            this.label17.TabIndex = 0;
            this.label17.Tag = "LoaiPTVT";
            this.label17.Text = "PTVT";
            // 
            // lblSoHieuPTVT
            // 
            this.lblSoHieuPTVT.AutoSize = true;
            this.lblSoHieuPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoHieuPTVT.Location = new System.Drawing.Point(6, 52);
            this.lblSoHieuPTVT.Name = "lblSoHieuPTVT";
            this.lblSoHieuPTVT.Size = new System.Drawing.Size(42, 13);
            this.lblSoHieuPTVT.TabIndex = 2;
            this.lblSoHieuPTVT.Tag = "SoHieuPTVT";
            this.lblSoHieuPTVT.Text = "Số hiệu";
            // 
            // txtSoHieuPTVT
            // 
            this.txtSoHieuPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHieuPTVT.Location = new System.Drawing.Point(65, 47);
            this.txtSoHieuPTVT.Name = "txtSoHieuPTVT";
            this.txtSoHieuPTVT.Size = new System.Drawing.Size(96, 21);
            this.txtSoHieuPTVT.TabIndex = 3;
            this.txtSoHieuPTVT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHieuPTVT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHieuPTVT.VisualStyleManager = this.vsmMain;
            // 
            // lblNgayDenPTVT
            // 
            this.lblNgayDenPTVT.AutoSize = true;
            this.lblNgayDenPTVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNgayDenPTVT.Location = new System.Drawing.Point(3, 79);
            this.lblNgayDenPTVT.Name = "lblNgayDenPTVT";
            this.lblNgayDenPTVT.Size = new System.Drawing.Size(53, 13);
            this.lblNgayDenPTVT.TabIndex = 4;
            this.lblNgayDenPTVT.Tag = "NgayDenPTVT";
            this.lblNgayDenPTVT.Text = "Ngày đến";
            // 
            // gbGiayPhep
            // 
            this.gbGiayPhep.Controls.Add(this.ccNgayHHGiayPhep);
            this.gbGiayPhep.Controls.Add(this.txtSoGiayPhep);
            this.gbGiayPhep.Controls.Add(this.label11);
            this.gbGiayPhep.Controls.Add(this.label12);
            this.gbGiayPhep.Controls.Add(this.label33);
            this.gbGiayPhep.Controls.Add(this.ccNgayGiayPhep);
            this.gbGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGiayPhep.Location = new System.Drawing.Point(380, 8);
            this.gbGiayPhep.Name = "gbGiayPhep";
            this.gbGiayPhep.Size = new System.Drawing.Size(168, 104);
            this.gbGiayPhep.TabIndex = 2;
            this.gbGiayPhep.Text = "Giấy phép";
            this.gbGiayPhep.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.gbGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayHHGiayPhep
            // 
            // 
            // 
            // 
            this.ccNgayHHGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHHGiayPhep.DropDownCalendar.Name = "";
            this.ccNgayHHGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHHGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHHGiayPhep.IsNullDate = true;
            this.ccNgayHHGiayPhep.Location = new System.Drawing.Point(64, 77);
            this.ccNgayHHGiayPhep.Name = "ccNgayHHGiayPhep";
            this.ccNgayHHGiayPhep.Nullable = true;
            this.ccNgayHHGiayPhep.NullButtonText = "Xóa";
            this.ccNgayHHGiayPhep.ShowNullButton = true;
            this.ccNgayHHGiayPhep.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHHGiayPhep.TabIndex = 5;
            this.ccNgayHHGiayPhep.TodayButtonText = "Hôm nay";
            this.ccNgayHHGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHHGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // txtSoGiayPhep
            // 
            this.txtSoGiayPhep.ButtonText = "...";
            this.txtSoGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoGiayPhep.Location = new System.Drawing.Point(64, 20);
            this.txtSoGiayPhep.Name = "txtSoGiayPhep";
            this.txtSoGiayPhep.Size = new System.Drawing.Size(96, 21);
            this.txtSoGiayPhep.TabIndex = 1;
            this.txtSoGiayPhep.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoGiayPhep.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoGiayPhep.VisualStyleManager = this.vsmMain;
            this.txtSoGiayPhep.ButtonClick += new System.EventHandler(this.txtSoGiayPhep_ButtonClick);
            this.txtSoGiayPhep.Leave += new System.EventHandler(this.txtSoGiayPhep_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 28);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(19, 13);
            this.label11.TabIndex = 0;
            this.label11.Tag = "SoGiayPhep";
            this.label11.Text = "Số";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(6, 56);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 13);
            this.label12.TabIndex = 2;
            this.label12.Tag = "NgayGiayPhep";
            this.label12.Text = "Ngày";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(8, 85);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(49, 13);
            this.label33.TabIndex = 4;
            this.label33.Tag = "NgayHHGiay Phep";
            this.label33.Text = "Ngày HH";
            // 
            // ccNgayGiayPhep
            // 
            // 
            // 
            // 
            this.ccNgayGiayPhep.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayGiayPhep.DropDownCalendar.Name = "";
            this.ccNgayGiayPhep.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayGiayPhep.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayGiayPhep.IsNullDate = true;
            this.ccNgayGiayPhep.Location = new System.Drawing.Point(64, 48);
            this.ccNgayGiayPhep.Name = "ccNgayGiayPhep";
            this.ccNgayGiayPhep.Nullable = true;
            this.ccNgayGiayPhep.NullButtonText = "Xóa";
            this.ccNgayGiayPhep.ShowNullButton = true;
            this.ccNgayGiayPhep.Size = new System.Drawing.Size(96, 21);
            this.ccNgayGiayPhep.TabIndex = 3;
            this.ccNgayGiayPhep.TodayButtonText = "Hôm nay";
            this.ccNgayGiayPhep.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayGiayPhep.VisualStyleManager = this.vsmMain;
            // 
            // grbHoaDonThuongMai
            // 
            this.grbHoaDonThuongMai.Controls.Add(this.ccNgayHDTM);
            this.grbHoaDonThuongMai.Controls.Add(this.txtSoHoaDonThuongMai);
            this.grbHoaDonThuongMai.Controls.Add(this.label15);
            this.grbHoaDonThuongMai.Controls.Add(this.label16);
            this.grbHoaDonThuongMai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbHoaDonThuongMai.Location = new System.Drawing.Point(380, 114);
            this.grbHoaDonThuongMai.Name = "grbHoaDonThuongMai";
            this.grbHoaDonThuongMai.Size = new System.Drawing.Size(168, 104);
            this.grbHoaDonThuongMai.TabIndex = 6;
            this.grbHoaDonThuongMai.Text = "Hóa đơn thương mại";
            this.grbHoaDonThuongMai.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.grbHoaDonThuongMai.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayHDTM
            // 
            // 
            // 
            // 
            this.ccNgayHDTM.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayHDTM.DropDownCalendar.Name = "";
            this.ccNgayHDTM.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHDTM.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayHDTM.IsNullDate = true;
            this.ccNgayHDTM.Location = new System.Drawing.Point(63, 48);
            this.ccNgayHDTM.Name = "ccNgayHDTM";
            this.ccNgayHDTM.Nullable = true;
            this.ccNgayHDTM.NullButtonText = "Xóa";
            this.ccNgayHDTM.ShowNullButton = true;
            this.ccNgayHDTM.Size = new System.Drawing.Size(96, 21);
            this.ccNgayHDTM.TabIndex = 3;
            this.ccNgayHDTM.TodayButtonText = "Hôm nay";
            this.ccNgayHDTM.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayHDTM.VisualStyleManager = this.vsmMain;
            // 
            // txtSoHoaDonThuongMai
            // 
            this.txtSoHoaDonThuongMai.ButtonText = "...";
            this.txtSoHoaDonThuongMai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoHoaDonThuongMai.Location = new System.Drawing.Point(63, 20);
            this.txtSoHoaDonThuongMai.Name = "txtSoHoaDonThuongMai";
            this.txtSoHoaDonThuongMai.Size = new System.Drawing.Size(96, 21);
            this.txtSoHoaDonThuongMai.TabIndex = 1;
            this.txtSoHoaDonThuongMai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoHoaDonThuongMai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoHoaDonThuongMai.VisualStyleManager = this.vsmMain;
            this.txtSoHoaDonThuongMai.ButtonClick += new System.EventHandler(this.txtSoHoaDonThuongMai_ButtonClick);
            this.txtSoHoaDonThuongMai.Leave += new System.EventHandler(this.txtSoHoaDonThuongMai_Leave);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(19, 13);
            this.label15.TabIndex = 0;
            this.label15.Tag = "SoHDTM";
            this.label15.Text = "Số";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 56);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 13);
            this.label16.TabIndex = 2;
            this.label16.Tag = "NgayHDTM";
            this.label16.Text = "Ngày";
            // 
            // ds
            // 
            this.ds.DataSetName = "NewDataSet";
            this.ds.Locale = new System.Globalization.CultureInfo("en-US");
            this.ds.Tables.AddRange(new System.Data.DataTable[] {
            this.dtHangMauDich,
            this.dtLoaiHinhMauDich,
            this.dtPTTT,
            this.dtCuaKhau,
            this.dtNguyenTe,
            this.dtCompanyNuoc,
            this.dtDonViHaiQuan});
            // 
            // dtHangMauDich
            // 
            this.dtHangMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
            this.dtHangMauDich.TableName = "HangMauDich";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "hmd_SoThuTuHang";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "hmd_MaHS";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "hmd_MaPhu";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "hmd_TenHang";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "nuoc_Ten";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "donvitinh_Ten";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "hmd_Luong";
            this.dataColumn7.DataType = typeof(decimal);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "hmd_DonGiaKB";
            this.dataColumn8.DataType = typeof(decimal);
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "hmd_TriGiaKB";
            this.dataColumn9.DataType = typeof(decimal);
            // 
            // dtLoaiHinhMauDich
            // 
            this.dtLoaiHinhMauDich.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11});
            this.dtLoaiHinhMauDich.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "loaihinh_Ma"}, true)});
            this.dtLoaiHinhMauDich.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn10};
            this.dtLoaiHinhMauDich.TableName = "LoaiHinhMauDich";
            // 
            // dataColumn10
            // 
            this.dataColumn10.AllowDBNull = false;
            this.dataColumn10.ColumnName = "loaihinh_Ma";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "loaihinh_Ten";
            // 
            // dtPTTT
            // 
            this.dtPTTT.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn12,
            this.dataColumn13});
            this.dtPTTT.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "pttt_Ma"}, true)});
            this.dtPTTT.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn12};
            this.dtPTTT.TableName = "PhuongThucThanhToan";
            // 
            // dataColumn12
            // 
            this.dataColumn12.AllowDBNull = false;
            this.dataColumn12.ColumnName = "pttt_Ma";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "pttt_GhiChu";
            // 
            // dtCuaKhau
            // 
            this.dtCuaKhau.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16});
            this.dtCuaKhau.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "cuakhau_Ma"}, true)});
            this.dtCuaKhau.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn14};
            this.dtCuaKhau.TableName = "CuaKhau";
            // 
            // dataColumn14
            // 
            this.dataColumn14.AllowDBNull = false;
            this.dataColumn14.ColumnName = "cuakhau_Ma";
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "cuakhau_Ten";
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "cuc_ma";
            // 
            // dtNguyenTe
            // 
            this.dtNguyenTe.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn17,
            this.dataColumn18});
            this.dtNguyenTe.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nguyente_Ma"}, true)});
            this.dtNguyenTe.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn17};
            this.dtNguyenTe.TableName = "NguyenTe";
            // 
            // dataColumn17
            // 
            this.dataColumn17.AllowDBNull = false;
            this.dataColumn17.ColumnName = "nguyente_Ma";
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "nguyente_Ten";
            // 
            // dtCompanyNuoc
            // 
            this.dtCompanyNuoc.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn19,
            this.dataColumn20});
            this.dtCompanyNuoc.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "nuoc_Ma"}, true)});
            this.dtCompanyNuoc.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn19};
            this.dtCompanyNuoc.TableName = "CompanyNuoc";
            // 
            // dataColumn19
            // 
            this.dataColumn19.AllowDBNull = false;
            this.dataColumn19.ColumnName = "nuoc_Ma";
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "nuoc_Ten";
            // 
            // dtDonViHaiQuan
            // 
            this.dtDonViHaiQuan.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn21,
            this.dataColumn22});
            this.dtDonViHaiQuan.TableName = "DonViHaiQuan";
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "donvihaiquan_Ma";
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "donvihaiquan_Ten";
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // rfvSoHieuPTVT
            // 
            this.rfvSoHieuPTVT.ControlToValidate = this.txtSoHieuPTVT;
            this.rfvSoHieuPTVT.ErrorMessage = "\"Số hiệu phương tiện vận tải\" không được để trống.";
            this.rfvSoHieuPTVT.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvSoHieuPTVT.Icon")));
            this.rfvSoHieuPTVT.Tag = "rfvSoHieuPTVT";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(754, 139);
            this.uiGroupBox2.TabIndex = 258;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(99, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Chi cục Hải Quan";
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(217)))), ((int)(((byte)(232)))), ((int)(((byte)(226)))));
            this.uiGroupBox1.Controls.Add(this.iconTrungToKhai);
            this.uiGroupBox1.Controls.Add(this.txtHDPL);
            this.uiGroupBox1.Controls.Add(this.label43);
            this.uiGroupBox1.Controls.Add(this.label42);
            this.uiGroupBox1.Controls.Add(this.ccNgayDK);
            this.uiGroupBox1.Controls.Add(this.ccNgayTN);
            this.uiGroupBox1.Controls.Add(this.label32);
            this.uiGroupBox1.Controls.Add(this.pnlToKhaiMauDich);
            this.uiGroupBox1.Controls.Add(this.lblPhanLuong);
            this.uiGroupBox1.Controls.Add(this.txtSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.lblSoTiepNhan);
            this.uiGroupBox1.Controls.Add(this.txtSoToKhai);
            this.uiGroupBox1.Controls.Add(this.lblTrangThai);
            this.uiGroupBox1.Controls.Add(this.label27);
            this.uiGroupBox1.Controls.Add(this.label18);
            this.uiGroupBox1.Controls.Add(this.label10);
            this.uiGroupBox1.Controls.Add(this.ctrDonViHaiQuan);
            this.uiGroupBox1.Controls.Add(this.txtSoLuongPLTK);
            this.uiGroupBox1.Controls.Add(this.label44);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(3, 35);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(1178, 700);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // iconTrungToKhai
            // 
            this.iconTrungToKhai.Image = global::Company.Interface.Properties.Resources.Oxygen_Icons_org_Oxygen_Status_dialog_warning;
            this.iconTrungToKhai.Location = new System.Drawing.Point(681, 0);
            this.iconTrungToKhai.Name = "iconTrungToKhai";
            this.iconTrungToKhai.Size = new System.Drawing.Size(32, 32);
            this.iconTrungToKhai.TabIndex = 39;
            this.iconTrungToKhai.TabStop = false;
            this.toolTip1.SetToolTip(this.iconTrungToKhai, "Trùng tờ khai");
            this.iconTrungToKhai.Visible = false;
            // 
            // txtHDPL
            // 
            this.txtHDPL.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHDPL.Location = new System.Drawing.Point(381, 86);
            this.txtHDPL.Name = "txtHDPL";
            this.txtHDPL.ReadOnly = true;
            this.txtHDPL.Size = new System.Drawing.Size(366, 21);
            this.txtHDPL.TabIndex = 18;
            this.txtHDPL.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtHDPL.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtHDPL.VisualStyleManager = this.vsmMain;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.BackColor = System.Drawing.Color.Transparent;
            this.label43.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(234, 91);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(139, 13);
            this.label43.TabIndex = 17;
            this.label43.Text = "Hướng dẫn phân luồng :";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.BackColor = System.Drawing.Color.Transparent;
            this.label42.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(235, 64);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(83, 13);
            this.label42.TabIndex = 16;
            this.label42.Text = "Ngày đăng ký";
            // 
            // ccNgayDK
            // 
            // 
            // 
            // 
            this.ccNgayDK.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayDK.DropDownCalendar.Name = "";
            this.ccNgayDK.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayDK.IsNullDate = true;
            this.ccNgayDK.Location = new System.Drawing.Point(381, 59);
            this.ccNgayDK.Name = "ccNgayDK";
            this.ccNgayDK.NullButtonText = "Xóa";
            this.ccNgayDK.ShowNullButton = true;
            this.ccNgayDK.Size = new System.Drawing.Size(94, 21);
            this.ccNgayDK.TabIndex = 15;
            this.ccNgayDK.TodayButtonText = "Hôm nay";
            this.ccNgayDK.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayDK.VisualStyleManager = this.vsmMain;
            // 
            // ccNgayTN
            // 
            // 
            // 
            // 
            this.ccNgayTN.DropDownCalendar.FirstMonth = new System.DateTime(2006, 9, 1, 0, 0, 0, 0);
            this.ccNgayTN.DropDownCalendar.Name = "";
            this.ccNgayTN.DropDownCalendar.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTN.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ccNgayTN.IsNullDate = true;
            this.ccNgayTN.Location = new System.Drawing.Point(109, 59);
            this.ccNgayTN.Name = "ccNgayTN";
            this.ccNgayTN.NullButtonText = "Xóa";
            this.ccNgayTN.ShowNullButton = true;
            this.ccNgayTN.Size = new System.Drawing.Size(96, 21);
            this.ccNgayTN.TabIndex = 6;
            this.ccNgayTN.TodayButtonText = "Hôm nay";
            this.ccNgayTN.VisualStyle = Janus.Windows.CalendarCombo.VisualStyle.Office2007;
            this.ccNgayTN.VisualStyleManager = this.vsmMain;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(3, 64);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(91, 13);
            this.label32.TabIndex = 14;
            this.label32.Text = "Ngày tiếp nhận";
            // 
            // lblPhanLuong
            // 
            this.lblPhanLuong.AutoSize = true;
            this.lblPhanLuong.BackColor = System.Drawing.Color.Transparent;
            this.lblPhanLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPhanLuong.ForeColor = System.Drawing.Color.Red;
            this.lblPhanLuong.Location = new System.Drawing.Point(106, 91);
            this.lblPhanLuong.Name = "lblPhanLuong";
            this.lblPhanLuong.Size = new System.Drawing.Size(101, 13);
            this.lblPhanLuong.TabIndex = 12;
            this.lblPhanLuong.Text = "Chưa phân luồng";
            // 
            // txtSoTiepNhan
            // 
            this.txtSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoTiepNhan.Location = new System.Drawing.Point(109, 32);
            this.txtSoTiepNhan.Name = "txtSoTiepNhan";
            this.txtSoTiepNhan.ReadOnly = true;
            this.txtSoTiepNhan.Size = new System.Drawing.Size(96, 21);
            this.txtSoTiepNhan.TabIndex = 5;
            this.txtSoTiepNhan.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoTiepNhan.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoTiepNhan.VisualStyleManager = this.vsmMain;
            // 
            // lblSoTiepNhan
            // 
            this.lblSoTiepNhan.AutoSize = true;
            this.lblSoTiepNhan.BackColor = System.Drawing.Color.Transparent;
            this.lblSoTiepNhan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSoTiepNhan.Location = new System.Drawing.Point(3, 37);
            this.lblSoTiepNhan.Name = "lblSoTiepNhan";
            this.lblSoTiepNhan.Size = new System.Drawing.Size(77, 13);
            this.lblSoTiepNhan.TabIndex = 4;
            this.lblSoTiepNhan.Text = "Số tiếp nhận";
            // 
            // txtSoToKhai
            // 
            this.txtSoToKhai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSoToKhai.Location = new System.Drawing.Point(381, 31);
            this.txtSoToKhai.Name = "txtSoToKhai";
            this.txtSoToKhai.ReadOnly = true;
            this.txtSoToKhai.Size = new System.Drawing.Size(94, 21);
            this.txtSoToKhai.TabIndex = 3;
            this.txtSoToKhai.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoToKhai.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoToKhai.VisualStyleManager = this.vsmMain;
            // 
            // lblTrangThai
            // 
            this.lblTrangThai.AutoSize = true;
            this.lblTrangThai.BackColor = System.Drawing.Color.Transparent;
            this.lblTrangThai.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTrangThai.ForeColor = System.Drawing.Color.Red;
            this.lblTrangThai.Location = new System.Drawing.Point(614, 64);
            this.lblTrangThai.Name = "lblTrangThai";
            this.lblTrangThai.Size = new System.Drawing.Size(133, 13);
            this.lblTrangThai.TabIndex = 7;
            this.lblTrangThai.Text = "Chưa gửi đến Hải quan";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(3, 91);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(75, 13);
            this.label27.TabIndex = 6;
            this.label27.Text = "Phân luồng :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(506, 64);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(102, 13);
            this.label18.TabIndex = 6;
            this.label18.Text = "Trạng thái xử lý :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(235, 36);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Số tờ khai          ";
            // 
            // ctrDonViHaiQuan
            // 
            this.ctrDonViHaiQuan.BackColor = System.Drawing.Color.Transparent;
            this.ctrDonViHaiQuan.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrDonViHaiQuan.Location = new System.Drawing.Point(109, 4);
            this.ctrDonViHaiQuan.Ma = "";
            this.ctrDonViHaiQuan.MaCuc = "";
            this.ctrDonViHaiQuan.Name = "ctrDonViHaiQuan";
            this.ctrDonViHaiQuan.ReadOnly = false;
            this.ctrDonViHaiQuan.Size = new System.Drawing.Size(366, 22);
            this.ctrDonViHaiQuan.TabIndex = 1;
            this.ctrDonViHaiQuan.VisualStyleManager = this.vsmMain;
            // 
            // txtSoLuongPLTK
            // 
            this.txtSoLuongPLTK.Location = new System.Drawing.Point(617, 31);
            this.txtSoLuongPLTK.MaxLength = 3;
            this.txtSoLuongPLTK.Name = "txtSoLuongPLTK";
            this.txtSoLuongPLTK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSoLuongPLTK.Size = new System.Drawing.Size(50, 21);
            this.txtSoLuongPLTK.TabIndex = 9;
            this.txtSoLuongPLTK.Text = "0";
            this.txtSoLuongPLTK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtSoLuongPLTK.Value = 0;
            this.txtSoLuongPLTK.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoLuongPLTK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtSoLuongPLTK.VisualStyleManager = this.vsmMain;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.BackColor = System.Drawing.Color.Transparent;
            this.label44.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(506, 36);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(86, 13);
            this.label44.TabIndex = 8;
            this.label44.Text = "Số lượng PLTK";
            // 
            // uiPanelManager1
            // 
            this.uiPanelManager1.BackColorGradientAutoHideStrip = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(241)))), ((int)(((byte)(228)))));
            this.uiPanelManager1.ContainerControl = this;
            this.uiPanelManager1.DefaultPanelSettings.CaptionStyle = Janus.Windows.UI.Dock.PanelCaptionStyle.Dark;
            this.uiPanelManager1.DefaultPanelSettings.CloseButtonVisible = false;
            this.uiPanelManager1.Tag = null;
            this.uiPanel4.Id = new System.Guid("da85d0f8-1ab6-4104-b70d-3669dabcdd6d");
            this.uiPanelManager1.Panels.Add(this.uiPanel4);
            this.uiPanel0.Id = new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2");
            this.uiPanel0.StaticGroup = true;
            this.uiPanel1.Id = new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8");
            this.uiPanel0.Panels.Add(this.uiPanel1);
            this.uiPanelManager1.Panels.Add(this.uiPanel0);
            this.uiPanel2.Id = new System.Guid("146f6615-6820-40ed-9958-87376ff81f1b");
            this.uiPanelManager1.Panels.Add(this.uiPanel2);
            this.TKGT.Id = new System.Guid("2ba2b962-92cf-4c3c-b6ee-00a6c9ea8fc9");
            this.uiPanelManager1.Panels.Add(this.TKGT);
            this.uiPanel6.Id = new System.Guid("25f67436-f7da-4828-bc56-30b18658aaa4");
            this.uiPanelManager1.Panels.Add(this.uiPanel6);
            // 
            // Design Time Panel Info:
            // 
            this.uiPanelManager1.BeginPanelInfo();
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("da85d0f8-1ab6-4104-b70d-3669dabcdd6d"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(785, 152), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, Janus.Windows.UI.Dock.PanelDockStyle.Bottom, true, new System.Drawing.Size(1162, 210), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), 174, true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("146f6615-6820-40ed-9958-87376ff81f1b"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(754, 200), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("2ba2b962-92cf-4c3c-b6ee-00a6c9ea8fc9"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(801, 200), true);
            this.uiPanelManager1.AddDockPanelInfo(new System.Guid("25f67436-f7da-4828-bc56-30b18658aaa4"), Janus.Windows.UI.Dock.PanelDockStyle.Bottom, new System.Drawing.Size(801, 200), true);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("381b9853-bd5e-4a28-bf1d-78e8b43a2ec2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, true, new System.Drawing.Point(22, 29), new System.Drawing.Size(56, 56), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("b850d037-76b5-4d54-94fd-9e085d6057e8"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("146f6615-6820-40ed-9958-87376ff81f1b"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("2ba2b962-92cf-4c3c-b6ee-00a6c9ea8fc9"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("2ff543b8-c802-4621-9f05-feb4b6aff3b2"), Janus.Windows.UI.Dock.PanelGroupStyle.HorizontalTiles, true, new System.Drawing.Point(279, 267), new System.Drawing.Size(200, 200), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("da85d0f8-1ab6-4104-b70d-3669dabcdd6d"), new System.Drawing.Point(514, 494), new System.Drawing.Size(200, 200), false);
            this.uiPanelManager1.AddFloatingPanelInfo(new System.Guid("25f67436-f7da-4828-bc56-30b18658aaa4"), new System.Drawing.Point(-1, -1), new System.Drawing.Size(-1, -1), false);
            this.uiPanelManager1.EndPanelInfo();
            // 
            // uiPanel4
            // 
            this.uiPanel4.AutoHide = true;
            this.uiPanel4.FloatingLocation = new System.Drawing.Point(514, 494);
            this.uiPanel4.InnerContainer = this.uiPanel4Container;
            this.uiPanel4.Location = new System.Drawing.Point(3, 392);
            this.uiPanel4.Name = "uiPanel4";
            this.uiPanel4.Size = new System.Drawing.Size(785, 152);
            this.uiPanel4.TabIndex = 4;
            this.uiPanel4.Text = "Tờ khai trị giá PP23";
            // 
            // uiPanel4Container
            // 
            this.uiPanel4Container.Controls.Add(this.dglistTKTK23);
            this.uiPanel4Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel4Container.Name = "uiPanel4Container";
            this.uiPanel4Container.Size = new System.Drawing.Size(783, 124);
            this.uiPanel4Container.TabIndex = 0;
            // 
            // dglistTKTK23
            // 
            this.dglistTKTK23.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dglistTKTK23.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dglistTKTK23.AlternatingColors = true;
            this.dglistTKTK23.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dglistTKTK23_DesignTimeLayout.LayoutString = resources.GetString("dglistTKTK23_DesignTimeLayout.LayoutString");
            this.dglistTKTK23.DesignTimeLayout = dglistTKTK23_DesignTimeLayout;
            this.dglistTKTK23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dglistTKTK23.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dglistTKTK23.FrozenColumns = 3;
            this.dglistTKTK23.GroupByBoxVisible = false;
            this.dglistTKTK23.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dglistTKTK23.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dglistTKTK23.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dglistTKTK23.ImageList = this.ImageList1;
            this.dglistTKTK23.Location = new System.Drawing.Point(0, 0);
            this.dglistTKTK23.Name = "dglistTKTK23";
            this.dglistTKTK23.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dglistTKTK23.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dglistTKTK23.Size = new System.Drawing.Size(783, 124);
            this.dglistTKTK23.TabIndex = 0;
            this.dglistTKTK23.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dglistTKTK23.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dglistTKTK23_RowDoubleClick);
            this.dglistTKTK23.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dglistTKTK23_DeletingRecords);
            this.dglistTKTK23.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dglistTKTK23_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "printer.png");
            this.ImageList1.Images.SetKeyName(1, "page_edit.png");
            this.ImageList1.Images.SetKeyName(2, "page_red.png");
            this.ImageList1.Images.SetKeyName(3, "GiayPhep1.Icon.ico");
            this.ImageList1.Images.SetKeyName(4, "Yes.png");
            // 
            // uiPanel0
            // 
            this.uiPanel0.AutoHide = true;
            this.uiPanel0.CaptionFormatStyle.FontBold = Janus.Windows.UI.TriState.True;
            this.uiPanel0.CloseButtonVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel0.FloatingLocation = new System.Drawing.Point(22, 29);
            this.uiPanel0.FloatingSize = new System.Drawing.Size(56, 56);
            this.uiPanel0.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiPanel0.Location = new System.Drawing.Point(3, 524);
            this.uiPanel0.Name = "uiPanel0";
            this.uiPanel0.Size = new System.Drawing.Size(1162, 210);
            this.uiPanel0.TabIndex = 1;
            this.uiPanel0.Text = "Thông tin hàng hóa";
            // 
            // uiPanel1
            // 
            this.uiPanel1.CaptionVisible = Janus.Windows.UI.InheritableBoolean.False;
            this.uiPanel1.InnerContainer = this.uiPanel1Container;
            this.uiPanel1.Location = new System.Drawing.Point(0, 26);
            this.uiPanel1.Name = "uiPanel1";
            this.uiPanel1.Size = new System.Drawing.Size(1162, 184);
            this.uiPanel1.TabIndex = 4;
            this.uiPanel1.Text = "Panel 1";
            // 
            // uiPanel1Container
            // 
            this.uiPanel1Container.Controls.Add(this.dgList);
            this.uiPanel1Container.Location = new System.Drawing.Point(1, 1);
            this.uiPanel1Container.Name = "uiPanel1Container";
            this.uiPanel1Container.Size = new System.Drawing.Size(1160, 182);
            this.uiPanel1Container.TabIndex = 0;
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(0, 0);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaderContent = Janus.Windows.GridEX.RowHeaderContent.RowIndex;
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(1160, 182);
            this.dgList.TabIndex = 182;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // uiPanel2
            // 
            this.uiPanel2.AutoHide = true;
            this.uiPanel2.InnerContainer = this.uiPanel2Container;
            this.uiPanel2.Location = new System.Drawing.Point(3, 165);
            this.uiPanel2.Name = "uiPanel2";
            this.uiPanel2.Size = new System.Drawing.Size(754, 200);
            this.uiPanel2.TabIndex = 4;
            this.uiPanel2.Text = "Thông tin chứng từ";
            // 
            // uiPanel2Container
            // 
            this.uiPanel2Container.Controls.Add(this.gridEX1);
            this.uiPanel2Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel2Container.Name = "uiPanel2Container";
            this.uiPanel2Container.Size = new System.Drawing.Size(752, 172);
            this.uiPanel2Container.TabIndex = 0;
            // 
            // gridEX1
            // 
            this.gridEX1.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX1.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridEX1.AlternatingColors = true;
            this.gridEX1.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.gridEX1.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.gridEX1.ColumnAutoResize = true;
            gridEX1_DesignTimeLayout.LayoutString = resources.GetString("gridEX1_DesignTimeLayout.LayoutString");
            this.gridEX1.DesignTimeLayout = gridEX1_DesignTimeLayout;
            this.gridEX1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridEX1.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridEX1.GroupByBoxVisible = false;
            this.gridEX1.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX1.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridEX1.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridEX1.ImageList = this.ImageList1;
            this.gridEX1.Location = new System.Drawing.Point(0, 0);
            this.gridEX1.Name = "gridEX1";
            this.gridEX1.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridEX1.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridEX1.Size = new System.Drawing.Size(752, 172);
            this.gridEX1.TabIndex = 2;
            this.gridEX1.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridEX1.VisualStyleManager = this.vsmMain;
            this.gridEX1.DeletingRecord += new Janus.Windows.GridEX.RowActionCancelEventHandler(this.gridEX1_DeletingRecord);
            this.gridEX1.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.gridEX1_RowDoubleClick);
            // 
            // TKGT
            // 
            this.TKGT.AutoHide = true;
            this.TKGT.InnerContainer = this.uiPanel3Container;
            this.TKGT.Location = new System.Drawing.Point(3, 187);
            this.TKGT.Name = "TKGT";
            this.TKGT.Size = new System.Drawing.Size(801, 200);
            this.TKGT.TabIndex = 4;
            this.TKGT.Text = "Tờ khai trị giá";
            // 
            // uiPanel3Container
            // 
            this.uiPanel3Container.Controls.Add(this.dgToKhaiTriGia);
            this.uiPanel3Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel3Container.Name = "uiPanel3Container";
            this.uiPanel3Container.Size = new System.Drawing.Size(799, 172);
            this.uiPanel3Container.TabIndex = 0;
            // 
            // dgToKhaiTriGia
            // 
            this.dgToKhaiTriGia.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgToKhaiTriGia.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgToKhaiTriGia.AlternatingColors = true;
            this.dgToKhaiTriGia.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.dgToKhaiTriGia.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.dgToKhaiTriGia.ColumnAutoResize = true;
            dgToKhaiTriGia_DesignTimeLayout.LayoutString = resources.GetString("dgToKhaiTriGia_DesignTimeLayout.LayoutString");
            this.dgToKhaiTriGia.DesignTimeLayout = dgToKhaiTriGia_DesignTimeLayout;
            this.dgToKhaiTriGia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgToKhaiTriGia.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgToKhaiTriGia.GroupByBoxVisible = false;
            this.dgToKhaiTriGia.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiTriGia.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgToKhaiTriGia.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgToKhaiTriGia.ImageList = this.ImageList1;
            this.dgToKhaiTriGia.Location = new System.Drawing.Point(0, 0);
            this.dgToKhaiTriGia.Name = "dgToKhaiTriGia";
            this.dgToKhaiTriGia.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgToKhaiTriGia.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgToKhaiTriGia.Size = new System.Drawing.Size(799, 172);
            this.dgToKhaiTriGia.TabIndex = 3;
            this.dgToKhaiTriGia.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.dgToKhaiTriGia.VisualStyleManager = this.vsmMain;
            this.dgToKhaiTriGia.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgToKhaiTriGia_RowDoubleClick);
            this.dgToKhaiTriGia.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgToKhaiTriGia_DeletingRecords);
            // 
            // uiPanel6
            // 
            this.uiPanel6.AutoHide = true;
            this.uiPanel6.InnerContainer = this.uiPanel6Container;
            this.uiPanel6.Location = new System.Drawing.Point(3, -13);
            this.uiPanel6.Name = "uiPanel6";
            this.uiPanel6.Size = new System.Drawing.Size(801, 200);
            this.uiPanel6.TabIndex = 4;
            this.uiPanel6.Text = "Thông tin CO";
            // 
            // uiPanel6Container
            // 
            this.uiPanel6Container.Controls.Add(this.groupBox1);
            this.uiPanel6Container.Location = new System.Drawing.Point(1, 27);
            this.uiPanel6Container.Name = "uiPanel6Container";
            this.uiPanel6Container.Size = new System.Drawing.Size(799, 172);
            this.uiPanel6Container.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.gridCO);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(799, 172);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // gridCO
            // 
            this.gridCO.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridCO.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.gridCO.AlternatingColors = true;
            this.gridCO.BorderStyle = Janus.Windows.GridEX.BorderStyle.None;
            this.gridCO.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            this.gridCO.ColumnAutoResize = true;
            gridCO_DesignTimeLayout.LayoutString = resources.GetString("gridCO_DesignTimeLayout.LayoutString");
            this.gridCO.DesignTimeLayout = gridCO_DesignTimeLayout;
            this.gridCO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridCO.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.gridCO.GroupByBoxVisible = false;
            this.gridCO.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridCO.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.gridCO.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.gridCO.ImageList = this.ImageList1;
            this.gridCO.Location = new System.Drawing.Point(3, 16);
            this.gridCO.Name = "gridCO";
            this.gridCO.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.gridCO.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.gridCO.Size = new System.Drawing.Size(793, 153);
            this.gridCO.TabIndex = 4;
            this.gridCO.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.gridCO.VisualStyleManager = this.vsmMain;
            // 
            // rfvNguoiXuatKhau
            // 
            this.rfvNguoiXuatKhau.ControlToValidate = this.txtTenDonViDoiTac;
            this.rfvNguoiXuatKhau.ErrorMessage = "\"Người xuất khẩu\" không được để trống.";
            this.rfvNguoiXuatKhau.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNguoiXuatKhau.Icon")));
            this.rfvNguoiXuatKhau.Tag = "rfvNguoiXuatKhau";
            // 
            // rvTyGiaTT
            // 
            this.rvTyGiaTT.ControlToValidate = this.txtTyGiaTinhThue;
            this.rvTyGiaTT.ErrorMessage = "\"Tỷ giá tính thuế\" không hợp lệ.";
            this.rvTyGiaTT.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaTT.Icon")));
            this.rvTyGiaTT.MaximumValue = "999999";
            this.rvTyGiaTT.MinimumValue = "1";
            this.rvTyGiaTT.Tag = "rvTyGiaTT";
            this.rvTyGiaTT.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rvTyGiaUSD
            // 
            this.rvTyGiaUSD.ControlToValidate = this.txtTyGiaUSD;
            this.rvTyGiaUSD.ErrorMessage = "\"Tỷ giá USD\" không hợp lệ.";
            this.rvTyGiaUSD.Icon = ((System.Drawing.Icon)(resources.GetObject("rvTyGiaUSD.Icon")));
            this.rvTyGiaUSD.MaximumValue = "999999";
            this.rvTyGiaUSD.MinimumValue = "1";
            this.rvTyGiaUSD.Tag = "rvTyGiaUSD";
            this.rvTyGiaUSD.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // cmMain
            // 
            this.cmMain.BottomRebar = this.BottomRebar1;
            this.cmMain.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.cmMain.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSave,
            this.cmdThemHang,
            this.cmdReadExcel,
            this.cmdTinhLaiThue,
            this.cmdPrint,
            this.cmdSend,
            this.ChungTuKemTheo,
            this.NhanDuLieu,
            this.XacNhan,
            this.ThemHang2,
            this.FileDinhKem,
            this.ToKhaiTriGia,
            this.KhaiDienTu,
            this.TienIch,
            this.Huy,
            this.cmdExportExcel,
            this.cmdToKhaiTriGia,
            this.cmdToKhaiTGPP2,
            this.cmdToKhaiTGPP3,
            this.ToKhai,
            this.InPhieuTN,
            this.TruyenDuLieu,
            this.ChungTuDinhKem,
            this.cmdVanDon,
            this.cmdHopDong,
            this.cmdGiayPhep,
            this.cmdHoaDon,
            this.cmdCO,
            this.ToKhaiA4,
            this.cmdChuyenCuaKhau,
            this.cmdBoSung,
            this.cmdBoSungCO,
            this.cmdBoSungGiayPhep,
            this.cmdBoSungHopDong,
            this.cmdBoSungHoaDon,
            this.cmdBoSungChuyenCuaKhau,
            this.cmdHuyTK,
            this.cmdSuaTK,
            this.cmdXacNhanThongTin,
            this.cmdInTKTQ,
            this.cmdContainer,
            this.cmdChungTuKem,
            this.cmdKetQuaXuLy,
            this.cmdLayPhanHoiSoTiepNhan,
            this.cmdBoSungChungTuDinhKem,
            this.cmdInTKSuaDoiBoSung,
            this.cmdInAnDinhThue,
            this.cmdChungTuNo,
            this.cmdInToKhaiTQDT_TT15,
            this.cmdInPhuLucTKNK,
            this.cmdGiayNopTien,
            this.cmdInTKTGPP1,
            this.GiayKiemTra,
            this.ChungThuGiamDinh,
            this.BSChungThuGiamDinh,
            this.cmdInChungTuGiay,
            this.cmdInKiemTraHangHoa,
            this.cmdContainer196,
            this.cmdInTKTCTT196,
            this.cmdChunTuTruocDo,
            this.cmdInBangKeHD_ToKhai,
            this.cmdBaoLanhThue});
            this.cmMain.ContainerControl = this;
            this.cmMain.Id = new System.Guid("4877a3e1-a31b-4952-8e28-564d209e4869");
            this.cmMain.ImageList = this.ImageList1;
            this.cmMain.LeftRebar = this.LeftRebar1;
            this.cmMain.RightRebar = this.RightRebar1;
            this.cmMain.Tag = null;
            this.cmMain.TopRebar = this.TopRebar1;
            this.cmMain.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            this.cmMain.VisualStyleManager = this.vsmMain;
            this.cmMain.CommandClick += new Janus.Windows.UI.CommandBars.CommandEventHandler(this.cmMain_CommandClick);
            // 
            // BottomRebar1
            // 
            this.BottomRebar1.CommandManager = this.cmMain;
            this.BottomRebar1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.BottomRebar1.Location = new System.Drawing.Point(0, 0);
            this.BottomRebar1.Name = "BottomRebar1";
            this.BottomRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // cmbToolBar
            // 
            this.cmbToolBar.CommandManager = this.cmMain;
            this.cmbToolBar.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ThemHang1,
            this.cmdSave1,
            this.cmdToKhaiTriGia1,
            this.ChungTuDinhKem1,
            this.Separator2,
            this.TruyenDuLieu1,
            this.cmdBoSung1,
            this.TienIch1});
            this.cmbToolBar.FullRow = true;
            this.cmbToolBar.Key = "cmdToolBar";
            this.cmbToolBar.Location = new System.Drawing.Point(0, 0);
            this.cmbToolBar.LockCommandBar = Janus.Windows.UI.InheritableBoolean.True;
            this.cmbToolBar.MergeRowOrder = 1;
            this.cmbToolBar.Name = "cmbToolBar";
            this.cmbToolBar.RowIndex = 0;
            this.cmbToolBar.Size = new System.Drawing.Size(1184, 32);
            this.cmbToolBar.Text = "cmdToolBar";
            this.cmbToolBar.View = Janus.Windows.UI.CommandBars.View.LargeIcons;
            // 
            // ThemHang1
            // 
            this.ThemHang1.Key = "ThemHang";
            this.ThemHang1.Name = "ThemHang1";
            // 
            // cmdSave1
            // 
            this.cmdSave1.Key = "cmdSave";
            this.cmdSave1.Name = "cmdSave1";
            this.cmdSave1.Text = "Lưu";
            // 
            // cmdToKhaiTriGia1
            // 
            this.cmdToKhaiTriGia1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdToKhaiTriGia1.Icon")));
            this.cmdToKhaiTriGia1.Key = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia1.Name = "cmdToKhaiTriGia1";
            // 
            // ChungTuDinhKem1
            // 
            this.ChungTuDinhKem1.Icon = ((System.Drawing.Icon)(resources.GetObject("ChungTuDinhKem1.Icon")));
            this.ChungTuDinhKem1.Key = "ChungTuDinhKem";
            this.ChungTuDinhKem1.Name = "ChungTuDinhKem1";
            // 
            // Separator2
            // 
            this.Separator2.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator2.Key = "Separator";
            this.Separator2.Name = "Separator2";
            // 
            // TruyenDuLieu1
            // 
            this.TruyenDuLieu1.Key = "TruyenDuLieu";
            this.TruyenDuLieu1.Name = "TruyenDuLieu1";
            this.TruyenDuLieu1.Text = "Khai báo";
            // 
            // cmdBoSung1
            // 
            this.cmdBoSung1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdBoSung1.Icon")));
            this.cmdBoSung1.Key = "cmdBoSung";
            this.cmdBoSung1.Name = "cmdBoSung1";
            // 
            // TienIch1
            // 
            this.TienIch1.Icon = ((System.Drawing.Icon)(resources.GetObject("TienIch1.Icon")));
            this.TienIch1.Key = "TienIch";
            this.TienIch1.Name = "TienIch1";
            // 
            // cmdSave
            // 
            this.cmdSave.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSave.Icon")));
            this.cmdSave.Key = "cmdSave";
            this.cmdSave.MergeOrder = 3;
            this.cmdSave.MergeType = Janus.Windows.UI.CommandBars.CommandMergeType.MergeItems;
            this.cmdSave.Name = "cmdSave";
            this.cmdSave.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.cmdSave.Text = "Lưu thông tin";
            // 
            // cmdThemHang
            // 
            this.cmdThemHang.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdThemHang.Icon")));
            this.cmdThemHang.Key = "cmdThemHang";
            this.cmdThemHang.Name = "cmdThemHang";
            this.cmdThemHang.Text = "Thêm hàng";
            // 
            // cmdReadExcel
            // 
            this.cmdReadExcel.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdReadExcel.Icon")));
            this.cmdReadExcel.Key = "cmdReadExcel";
            this.cmdReadExcel.Name = "cmdReadExcel";
            this.cmdReadExcel.Text = "Thêm hàng từ Excel";
            // 
            // cmdTinhLaiThue
            // 
            this.cmdTinhLaiThue.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdTinhLaiThue.Icon")));
            this.cmdTinhLaiThue.Key = "cmdTinhLaiThue";
            this.cmdTinhLaiThue.Name = "cmdTinhLaiThue";
            this.cmdTinhLaiThue.Text = "Tính lại thuế";
            // 
            // cmdPrint
            // 
            this.cmdPrint.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdInToKhaiTQDT_TT151,
            this.cmdInTKTCTT1961,
            this.cmdInTKTGPP11,
            this.cmdInTKDTSuaDoiBoSung1,
            this.cmdContainer1961,
            this.cmdInBangKeHD_ToKhai1,
            this.cmdInAnDinhThue1,
            this.cmdInChungTuGiay1,
            this.cmdInKiemTraHangHoa1});
            this.cmdPrint.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdPrint.Icon")));
            this.cmdPrint.Key = "cmdPrint";
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Shortcut = System.Windows.Forms.Shortcut.CtrlP;
            this.cmdPrint.Text = "In";
            // 
            // cmdInToKhaiTQDT_TT151
            // 
            this.cmdInToKhaiTQDT_TT151.ImageIndex = 0;
            this.cmdInToKhaiTQDT_TT151.Key = "cmdInToKhaiTQDT_TT15";
            this.cmdInToKhaiTQDT_TT151.Name = "cmdInToKhaiTQDT_TT151";
            this.cmdInToKhaiTQDT_TT151.Text = "In tờ khai thông quan (Thông tư 196)";
            // 
            // cmdInTKTCTT1961
            // 
            this.cmdInTKTCTT1961.Key = "cmdInTKTCTT196";
            this.cmdInTKTCTT1961.Name = "cmdInTKTCTT1961";
            this.cmdInTKTCTT1961.Text = "In tờ khai tại chỗ (Thông tư 196)";
            // 
            // cmdInTKTGPP11
            // 
            this.cmdInTKTGPP11.Key = "cmdInTKTGPP1";
            this.cmdInTKTGPP11.Name = "cmdInTKTGPP11";
            // 
            // cmdInTKDTSuaDoiBoSung1
            // 
            this.cmdInTKDTSuaDoiBoSung1.ImageIndex = 0;
            this.cmdInTKDTSuaDoiBoSung1.Key = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKDTSuaDoiBoSung1.Name = "cmdInTKDTSuaDoiBoSung1";
            this.cmdInTKDTSuaDoiBoSung1.Text = "In tờ khai sửa đổi bổ sung";
            // 
            // cmdContainer1961
            // 
            this.cmdContainer1961.ImageIndex = 0;
            this.cmdContainer1961.Key = "cmdContainer196";
            this.cmdContainer1961.Name = "cmdContainer1961";
            this.cmdContainer1961.Text = "In bảng kê container";
            // 
            // cmdInBangKeHD_ToKhai1
            // 
            this.cmdInBangKeHD_ToKhai1.Key = "cmdInBangKeHD_ToKhai";
            this.cmdInBangKeHD_ToKhai1.Name = "cmdInBangKeHD_ToKhai1";
            this.cmdInBangKeHD_ToKhai1.Text = "In bảng kê hợp đồng/ đơn hàng";
            // 
            // cmdInAnDinhThue1
            // 
            this.cmdInAnDinhThue1.ImageIndex = 0;
            this.cmdInAnDinhThue1.Key = "cmdInAnDinhThue";
            this.cmdInAnDinhThue1.Name = "cmdInAnDinhThue1";
            this.cmdInAnDinhThue1.Text = "In ấn định thuế";
            // 
            // cmdInChungTuGiay1
            // 
            this.cmdInChungTuGiay1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdInChungTuGiay1.Icon")));
            this.cmdInChungTuGiay1.Key = "cmdInChungTuGiay";
            this.cmdInChungTuGiay1.Name = "cmdInChungTuGiay1";
            // 
            // cmdInKiemTraHangHoa1
            // 
            this.cmdInKiemTraHangHoa1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdInKiemTraHangHoa1.Icon")));
            this.cmdInKiemTraHangHoa1.Key = "cmdInKiemTraHangHoa";
            this.cmdInKiemTraHangHoa1.Name = "cmdInKiemTraHangHoa1";
            // 
            // cmdSend
            // 
            this.cmdSend.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdSend.Icon")));
            this.cmdSend.Key = "cmdSend";
            this.cmdSend.Name = "cmdSend";
            this.cmdSend.Text = "Khai báo";
            this.cmdSend.ToolTipText = "Khai báo (Ctrl + K)";
            // 
            // ChungTuKemTheo
            // 
            this.ChungTuKemTheo.Icon = ((System.Drawing.Icon)(resources.GetObject("ChungTuKemTheo.Icon")));
            this.ChungTuKemTheo.Key = "ChungTuKemTheo";
            this.ChungTuKemTheo.Name = "ChungTuKemTheo";
            this.ChungTuKemTheo.Text = "Thêm chứng từ";
            // 
            // NhanDuLieu
            // 
            this.NhanDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("NhanDuLieu.Icon")));
            this.NhanDuLieu.Key = "NhanDuLieu";
            this.NhanDuLieu.Name = "NhanDuLieu";
            this.NhanDuLieu.Text = "Nhận dữ liệu";
            // 
            // XacNhan
            // 
            this.XacNhan.Icon = ((System.Drawing.Icon)(resources.GetObject("XacNhan.Icon")));
            this.XacNhan.Key = "XacNhan";
            this.XacNhan.Name = "XacNhan";
            this.XacNhan.Text = "Xác nhận";
            // 
            // ThemHang2
            // 
            this.ThemHang2.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdThemHang1,
            this.cmdReadExcel1,
            this.ChungTuKemTheo1});
            this.ThemHang2.Icon = ((System.Drawing.Icon)(resources.GetObject("ThemHang2.Icon")));
            this.ThemHang2.Key = "ThemHang";
            this.ThemHang2.Name = "ThemHang2";
            this.ThemHang2.Text = "Thêm hàng";
            // 
            // cmdThemHang1
            // 
            this.cmdThemHang1.Key = "cmdThemHang";
            this.cmdThemHang1.Name = "cmdThemHang1";
            // 
            // cmdReadExcel1
            // 
            this.cmdReadExcel1.Key = "cmdReadExcel";
            this.cmdReadExcel1.Name = "cmdReadExcel1";
            // 
            // ChungTuKemTheo1
            // 
            this.ChungTuKemTheo1.Key = "ChungTuKemTheo";
            this.ChungTuKemTheo1.Name = "ChungTuKemTheo1";
            this.ChungTuKemTheo1.Visible = Janus.Windows.UI.InheritableBoolean.False;
            // 
            // FileDinhKem
            // 
            this.FileDinhKem.Icon = ((System.Drawing.Icon)(resources.GetObject("FileDinhKem.Icon")));
            this.FileDinhKem.Key = "FileDinhKem";
            this.FileDinhKem.Name = "FileDinhKem";
            this.FileDinhKem.Text = "File đính kèm";
            // 
            // ToKhaiTriGia
            // 
            this.ToKhaiTriGia.Key = "ToKhaiTriGia";
            this.ToKhaiTriGia.Name = "ToKhaiTriGia";
            this.ToKhaiTriGia.Text = "Tờ khai trị giá PP1";
            // 
            // KhaiDienTu
            // 
            this.KhaiDienTu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend1,
            this.NhanDuLieu1,
            this.XacNhan1});
            this.KhaiDienTu.Icon = ((System.Drawing.Icon)(resources.GetObject("KhaiDienTu.Icon")));
            this.KhaiDienTu.Key = "KhaiDienTu";
            this.KhaiDienTu.Name = "KhaiDienTu";
            this.KhaiDienTu.Text = "Khai điện tử";
            // 
            // cmdSend1
            // 
            this.cmdSend1.Key = "cmdSend";
            this.cmdSend1.Name = "cmdSend1";
            // 
            // NhanDuLieu1
            // 
            this.NhanDuLieu1.Key = "NhanDuLieu";
            this.NhanDuLieu1.Name = "NhanDuLieu1";
            // 
            // XacNhan1
            // 
            this.XacNhan1.Key = "XacNhan";
            this.XacNhan1.Name = "XacNhan1";
            // 
            // TienIch
            // 
            this.TienIch.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdTinhLaiThue1,
            this.cmdPrint1,
            this.cmdExportExcel1});
            this.TienIch.Key = "TienIch";
            this.TienIch.Name = "TienIch";
            this.TienIch.Text = "Tiện ích";
            // 
            // cmdTinhLaiThue1
            // 
            this.cmdTinhLaiThue1.Key = "cmdTinhLaiThue";
            this.cmdTinhLaiThue1.Name = "cmdTinhLaiThue1";
            // 
            // cmdPrint1
            // 
            this.cmdPrint1.Key = "cmdPrint";
            this.cmdPrint1.Name = "cmdPrint1";
            // 
            // cmdExportExcel1
            // 
            this.cmdExportExcel1.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdExportExcel1.Icon")));
            this.cmdExportExcel1.Key = "cmdExportExcel";
            this.cmdExportExcel1.Name = "cmdExportExcel1";
            // 
            // Huy
            // 
            this.Huy.Icon = ((System.Drawing.Icon)(resources.GetObject("Huy.Icon")));
            this.Huy.Key = "Huy";
            this.Huy.Name = "Huy";
            this.Huy.Text = "Hủy khai báo";
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.Key = "cmdExportExcel";
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Text = "Xuất Excel";
            // 
            // cmdToKhaiTriGia
            // 
            this.cmdToKhaiTriGia.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.ToKhaiTriGia1,
            this.cmdToKhaiTGPP21,
            this.cmdToKhaiTGPP31});
            this.cmdToKhaiTriGia.Key = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia.Name = "cmdToKhaiTriGia";
            this.cmdToKhaiTriGia.Text = "Tờ khai trị giá";
            // 
            // ToKhaiTriGia1
            // 
            this.ToKhaiTriGia1.Key = "ToKhaiTriGia";
            this.ToKhaiTriGia1.Name = "ToKhaiTriGia1";
            // 
            // cmdToKhaiTGPP21
            // 
            this.cmdToKhaiTGPP21.Key = "cmdToKhaiTGPP2";
            this.cmdToKhaiTGPP21.Name = "cmdToKhaiTGPP21";
            this.cmdToKhaiTGPP21.Text = "Tờ khai trị giá PP2";
            // 
            // cmdToKhaiTGPP31
            // 
            this.cmdToKhaiTGPP31.Key = "cmdToKhaiTGPP3";
            this.cmdToKhaiTGPP31.Name = "cmdToKhaiTGPP31";
            // 
            // cmdToKhaiTGPP2
            // 
            this.cmdToKhaiTGPP2.Key = "cmdToKhaiTGPP2";
            this.cmdToKhaiTGPP2.Name = "cmdToKhaiTGPP2";
            this.cmdToKhaiTGPP2.Text = "Tờ khai trị giá PP23";
            // 
            // cmdToKhaiTGPP3
            // 
            this.cmdToKhaiTGPP3.Key = "cmdToKhaiTGPP3";
            this.cmdToKhaiTGPP3.Name = "cmdToKhaiTGPP3";
            this.cmdToKhaiTGPP3.Text = "Tờ khai trị giá PP3";
            // 
            // ToKhai
            // 
            this.ToKhai.Key = "ToKhai";
            this.ToKhai.Name = "ToKhai";
            this.ToKhai.Text = "Tờ khai";
            // 
            // InPhieuTN
            // 
            this.InPhieuTN.Key = "InPhieuTN";
            this.InPhieuTN.Name = "InPhieuTN";
            this.InPhieuTN.Text = "Phiếu tiếp nhận";
            // 
            // TruyenDuLieu
            // 
            this.TruyenDuLieu.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdSend2,
            this.NhanDuLieu2,
            this.Separator1,
            this.cmdSuaTK2,
            this.cmdHuyTK2,
            this.Separator3,
            this.cmdKetQuaXuLy1});
            this.TruyenDuLieu.Icon = ((System.Drawing.Icon)(resources.GetObject("TruyenDuLieu.Icon")));
            this.TruyenDuLieu.Key = "TruyenDuLieu";
            this.TruyenDuLieu.Name = "TruyenDuLieu";
            this.TruyenDuLieu.Text = "Truyền dữ liệu từ xa";
            // 
            // cmdSend2
            // 
            this.cmdSend2.Key = "cmdSend";
            this.cmdSend2.Name = "cmdSend2";
            this.cmdSend2.Text = "1. Khai báo";
            // 
            // NhanDuLieu2
            // 
            this.NhanDuLieu2.Key = "NhanDuLieu";
            this.NhanDuLieu2.Name = "NhanDuLieu2";
            this.NhanDuLieu2.Text = "2. Lấy phản hồi";
            // 
            // Separator1
            // 
            this.Separator1.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator1.Key = "Separator";
            this.Separator1.Name = "Separator1";
            // 
            // cmdSuaTK2
            // 
            this.cmdSuaTK2.Image = ((System.Drawing.Image)(resources.GetObject("cmdSuaTK2.Image")));
            this.cmdSuaTK2.Key = "cmdSuaTK";
            this.cmdSuaTK2.Name = "cmdSuaTK2";
            // 
            // cmdHuyTK2
            // 
            this.cmdHuyTK2.Image = ((System.Drawing.Image)(resources.GetObject("cmdHuyTK2.Image")));
            this.cmdHuyTK2.Key = "cmdHuyTK";
            this.cmdHuyTK2.Name = "cmdHuyTK2";
            // 
            // Separator3
            // 
            this.Separator3.CommandType = Janus.Windows.UI.CommandBars.CommandType.Separator;
            this.Separator3.Key = "Separator";
            this.Separator3.Name = "Separator3";
            // 
            // cmdKetQuaXuLy1
            // 
            this.cmdKetQuaXuLy1.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy1.Name = "cmdKetQuaXuLy1";
            // 
            // ChungTuDinhKem
            // 
            this.ChungTuDinhKem.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.VanDon2,
            this.GiayPhep1,
            this.HopDong1,
            this.HoaDon1,
            this.CO1,
            this.cmdChuyenCuaKhau1,
            this.cmdChungTuKem1,
            this.cmdChungTuNo1,
            this.GiayKiemTra1,
            this.ChungThuGiamDinh1,
            this.cmdChunTuTruocDo1,
            this.cmdBaoLanhThue1});
            this.ChungTuDinhKem.Icon = ((System.Drawing.Icon)(resources.GetObject("ChungTuDinhKem.Icon")));
            this.ChungTuDinhKem.Key = "ChungTuDinhKem";
            this.ChungTuDinhKem.Name = "ChungTuDinhKem";
            this.ChungTuDinhKem.Text = "Chứng từ đính kèm";
            // 
            // VanDon2
            // 
            this.VanDon2.Key = "VanDon";
            this.VanDon2.Name = "VanDon2";
            // 
            // GiayPhep1
            // 
            this.GiayPhep1.Key = "GiayPhep";
            this.GiayPhep1.Name = "GiayPhep1";
            // 
            // HopDong1
            // 
            this.HopDong1.Key = "HopDong";
            this.HopDong1.Name = "HopDong1";
            // 
            // HoaDon1
            // 
            this.HoaDon1.Key = "HoaDon";
            this.HoaDon1.Name = "HoaDon1";
            // 
            // CO1
            // 
            this.CO1.Key = "CO";
            this.CO1.Name = "CO1";
            // 
            // cmdChuyenCuaKhau1
            // 
            this.cmdChuyenCuaKhau1.Key = "cmdChuyenCuaKhau";
            this.cmdChuyenCuaKhau1.Name = "cmdChuyenCuaKhau1";
            // 
            // cmdChungTuKem1
            // 
            this.cmdChungTuKem1.Key = "cmdChungTuKem";
            this.cmdChungTuKem1.Name = "cmdChungTuKem1";
            // 
            // cmdChungTuNo1
            // 
            this.cmdChungTuNo1.Key = "cmdChungTuNo";
            this.cmdChungTuNo1.Name = "cmdChungTuNo1";
            // 
            // GiayKiemTra1
            // 
            this.GiayKiemTra1.Key = "GiayKiemTra";
            this.GiayKiemTra1.Name = "GiayKiemTra1";
            // 
            // ChungThuGiamDinh1
            // 
            this.ChungThuGiamDinh1.Key = "ChungThuGiamDinh";
            this.ChungThuGiamDinh1.Name = "ChungThuGiamDinh1";
            // 
            // cmdChunTuTruocDo1
            // 
            this.cmdChunTuTruocDo1.Key = "cmdChunTuTruocDo";
            this.cmdChunTuTruocDo1.Name = "cmdChunTuTruocDo1";
            // 
            // cmdBaoLanhThue1
            // 
            this.cmdBaoLanhThue1.Key = "cmdBaoLanhThue";
            this.cmdBaoLanhThue1.Name = "cmdBaoLanhThue1";
            // 
            // cmdVanDon
            // 
            this.cmdVanDon.Key = "VanDon";
            this.cmdVanDon.Name = "cmdVanDon";
            this.cmdVanDon.Text = "Vận đơn";
            // 
            // cmdHopDong
            // 
            this.cmdHopDong.Key = "HopDong";
            this.cmdHopDong.Name = "cmdHopDong";
            this.cmdHopDong.Text = "Hợp Đồng";
            // 
            // cmdGiayPhep
            // 
            this.cmdGiayPhep.Key = "GiayPhep";
            this.cmdGiayPhep.Name = "cmdGiayPhep";
            this.cmdGiayPhep.Text = "Giấy phép";
            // 
            // cmdHoaDon
            // 
            this.cmdHoaDon.Key = "HoaDon";
            this.cmdHoaDon.Name = "cmdHoaDon";
            this.cmdHoaDon.Text = "Hóa đơn thương mại";
            // 
            // cmdCO
            // 
            this.cmdCO.Key = "CO";
            this.cmdCO.Name = "cmdCO";
            this.cmdCO.Text = "CO";
            // 
            // ToKhaiA4
            // 
            this.ToKhaiA4.Key = "ToKhaiA4";
            this.ToKhaiA4.Name = "ToKhaiA4";
            this.ToKhaiA4.Text = "Tờ khai A4";
            // 
            // cmdChuyenCuaKhau
            // 
            this.cmdChuyenCuaKhau.Key = "cmdChuyenCuaKhau";
            this.cmdChuyenCuaKhau.Name = "cmdChuyenCuaKhau";
            this.cmdChuyenCuaKhau.Text = "Đề nghị chuyển cửa khẩu";
            // 
            // cmdBoSung
            // 
            this.cmdBoSung.Commands.AddRange(new Janus.Windows.UI.CommandBars.UICommand[] {
            this.cmdBoSungCO1,
            this.cmdBoSungGiayPhep1,
            this.cmdBoSungHopDong1,
            this.cmdBoSungHoaDon1,
            this.cmdBoSungChuyenCuaKhau1,
            this.cmdBoSungChungTuDinhKemDangAnh1,
            this.cmdGiayNopTien1,
            this.BSChungThuGiamDinh1});
            this.cmdBoSung.Key = "cmdBoSung";
            this.cmdBoSung.Name = "cmdBoSung";
            this.cmdBoSung.Shortcut = System.Windows.Forms.Shortcut.ShiftIns;
            this.cmdBoSung.Text = "Bổ sung chứng từ";
            // 
            // cmdBoSungCO1
            // 
            this.cmdBoSungCO1.Key = "cmdBoSungCO";
            this.cmdBoSungCO1.Name = "cmdBoSungCO1";
            // 
            // cmdBoSungGiayPhep1
            // 
            this.cmdBoSungGiayPhep1.Key = "cmdBoSungGiayPhep";
            this.cmdBoSungGiayPhep1.Name = "cmdBoSungGiayPhep1";
            // 
            // cmdBoSungHopDong1
            // 
            this.cmdBoSungHopDong1.Key = "cmdBoSungHopDong";
            this.cmdBoSungHopDong1.Name = "cmdBoSungHopDong1";
            // 
            // cmdBoSungHoaDon1
            // 
            this.cmdBoSungHoaDon1.Key = "cmdBoSungHoaDon";
            this.cmdBoSungHoaDon1.Name = "cmdBoSungHoaDon1";
            // 
            // cmdBoSungChuyenCuaKhau1
            // 
            this.cmdBoSungChuyenCuaKhau1.Key = "cmdBoSungChuyenCuaKhau";
            this.cmdBoSungChuyenCuaKhau1.Name = "cmdBoSungChuyenCuaKhau1";
            // 
            // cmdBoSungChungTuDinhKemDangAnh1
            // 
            this.cmdBoSungChungTuDinhKemDangAnh1.Key = "cmdBoSungChungTuDinhKemDangAnh";
            this.cmdBoSungChungTuDinhKemDangAnh1.Name = "cmdBoSungChungTuDinhKemDangAnh1";
            // 
            // cmdGiayNopTien1
            // 
            this.cmdGiayNopTien1.Key = "cmdGiayNopTien";
            this.cmdGiayNopTien1.Name = "cmdGiayNopTien1";
            // 
            // BSChungThuGiamDinh1
            // 
            this.BSChungThuGiamDinh1.Key = "BSChungThuGiamDinh";
            this.BSChungThuGiamDinh1.Name = "BSChungThuGiamDinh1";
            // 
            // cmdBoSungCO
            // 
            this.cmdBoSungCO.Key = "cmdBoSungCO";
            this.cmdBoSungCO.Name = "cmdBoSungCO";
            this.cmdBoSungCO.Text = "Bổ sung CO";
            // 
            // cmdBoSungGiayPhep
            // 
            this.cmdBoSungGiayPhep.Key = "cmdBoSungGiayPhep";
            this.cmdBoSungGiayPhep.Name = "cmdBoSungGiayPhep";
            this.cmdBoSungGiayPhep.Text = "Bổ sung giấy phép";
            // 
            // cmdBoSungHopDong
            // 
            this.cmdBoSungHopDong.Key = "cmdBoSungHopDong";
            this.cmdBoSungHopDong.Name = "cmdBoSungHopDong";
            this.cmdBoSungHopDong.Text = "Bổ sung hợp đồng";
            // 
            // cmdBoSungHoaDon
            // 
            this.cmdBoSungHoaDon.Key = "cmdBoSungHoaDon";
            this.cmdBoSungHoaDon.Name = "cmdBoSungHoaDon";
            this.cmdBoSungHoaDon.Text = "Bổ sung hóa đơn thương mại";
            // 
            // cmdBoSungChuyenCuaKhau
            // 
            this.cmdBoSungChuyenCuaKhau.Key = "cmdBoSungChuyenCuaKhau";
            this.cmdBoSungChuyenCuaKhau.Name = "cmdBoSungChuyenCuaKhau";
            this.cmdBoSungChuyenCuaKhau.Text = "Bổ sung đề nghị chuyển cửa khẩu";
            // 
            // cmdHuyTK
            // 
            this.cmdHuyTK.Key = "cmdHuyTK";
            this.cmdHuyTK.Name = "cmdHuyTK";
            this.cmdHuyTK.Text = "Hủy tờ khai";
            this.cmdHuyTK.Visible = Janus.Windows.UI.InheritableBoolean.True;
            // 
            // cmdSuaTK
            // 
            this.cmdSuaTK.Key = "cmdSuaTK";
            this.cmdSuaTK.Name = "cmdSuaTK";
            this.cmdSuaTK.Text = "Sửa tờ khai";
            // 
            // cmdXacNhanThongTin
            // 
            this.cmdXacNhanThongTin.Key = "cmdXacNhanThongTin";
            this.cmdXacNhanThongTin.Name = "cmdXacNhanThongTin";
            this.cmdXacNhanThongTin.Shortcut = System.Windows.Forms.Shortcut.Ctrl7;
            this.cmdXacNhanThongTin.Text = "Xác nhận thông tin";
            // 
            // cmdInTKTQ
            // 
            this.cmdInTKTQ.Key = "cmdInTKTQ";
            this.cmdInTKTQ.Name = "cmdInTKTQ";
            this.cmdInTKTQ.Text = "In tờ khai thông quan";
            // 
            // cmdContainer
            // 
            this.cmdContainer.Key = "cmdContainer";
            this.cmdContainer.Name = "cmdContainer";
            this.cmdContainer.Text = "In bảng kê Container";
            // 
            // cmdChungTuKem
            // 
            this.cmdChungTuKem.Key = "cmdChungTuKem";
            this.cmdChungTuKem.Name = "cmdChungTuKem";
            this.cmdChungTuKem.Text = "Chứng từ dạng ảnh";
            // 
            // cmdKetQuaXuLy
            // 
            this.cmdKetQuaXuLy.Icon = ((System.Drawing.Icon)(resources.GetObject("cmdKetQuaXuLy.Icon")));
            this.cmdKetQuaXuLy.Key = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Name = "cmdKetQuaXuLy";
            this.cmdKetQuaXuLy.Text = "Kết quả xử lý thông tin";
            // 
            // cmdLayPhanHoiSoTiepNhan
            // 
            this.cmdLayPhanHoiSoTiepNhan.Key = "cmdLayPhanHoiSoTiepNhan";
            this.cmdLayPhanHoiSoTiepNhan.Name = "cmdLayPhanHoiSoTiepNhan";
            this.cmdLayPhanHoiSoTiepNhan.Text = "Lấy phản hồi số tiếp nhận";
            // 
            // cmdBoSungChungTuDinhKem
            // 
            this.cmdBoSungChungTuDinhKem.Key = "cmdBoSungChungTuDinhKemDangAnh";
            this.cmdBoSungChungTuDinhKem.Name = "cmdBoSungChungTuDinhKem";
            this.cmdBoSungChungTuDinhKem.Text = "Bổ sung chứng từ đính kèm dạng ảnh";
            // 
            // cmdInTKSuaDoiBoSung
            // 
            this.cmdInTKSuaDoiBoSung.Key = "cmdInTKDTSuaDoiBoSung";
            this.cmdInTKSuaDoiBoSung.Name = "cmdInTKSuaDoiBoSung";
            this.cmdInTKSuaDoiBoSung.Text = "In tờ khai sửa đổi bổ sung";
            // 
            // cmdInAnDinhThue
            // 
            this.cmdInAnDinhThue.Key = "cmdInAnDinhThue";
            this.cmdInAnDinhThue.Name = "cmdInAnDinhThue";
            this.cmdInAnDinhThue.Text = "In ấn định thuế tờ khai";
            // 
            // cmdChungTuNo
            // 
            this.cmdChungTuNo.Key = "cmdChungTuNo";
            this.cmdChungTuNo.Name = "cmdChungTuNo";
            this.cmdChungTuNo.Text = "Chứng từ khác";
            // 
            // cmdInToKhaiTQDT_TT15
            // 
            this.cmdInToKhaiTQDT_TT15.Key = "cmdInToKhaiTQDT_TT15";
            this.cmdInToKhaiTQDT_TT15.Name = "cmdInToKhaiTQDT_TT15";
            this.cmdInToKhaiTQDT_TT15.Text = "In tờ khai thông quan (Thông tư 196)";
            // 
            // cmdInPhuLucTKNK
            // 
            this.cmdInPhuLucTKNK.Key = "cmdInPhuLucTKNK";
            this.cmdInPhuLucTKNK.Name = "cmdInPhuLucTKNK";
            this.cmdInPhuLucTKNK.Text = "In phụ lục tờ khai (Thông tư 196)";
            // 
            // cmdGiayNopTien
            // 
            this.cmdGiayNopTien.Key = "cmdGiayNopTien";
            this.cmdGiayNopTien.Name = "cmdGiayNopTien";
            this.cmdGiayNopTien.Text = "Bổ sung giấy nộp tiền";
            // 
            // cmdInTKTGPP1
            // 
            this.cmdInTKTGPP1.ImageIndex = 0;
            this.cmdInTKTGPP1.Key = "cmdInTKTGPP1";
            this.cmdInTKTGPP1.Name = "cmdInTKTGPP1";
            this.cmdInTKTGPP1.Text = "In tờ khai trị giá (Phương pháp 1)";
            // 
            // GiayKiemTra
            // 
            this.GiayKiemTra.Key = "GiayKiemTra";
            this.GiayKiemTra.Name = "GiayKiemTra";
            this.GiayKiemTra.Text = "Giấy kiểm tra chất lượng";
            // 
            // ChungThuGiamDinh
            // 
            this.ChungThuGiamDinh.Key = "ChungThuGiamDinh";
            this.ChungThuGiamDinh.Name = "ChungThuGiamDinh";
            this.ChungThuGiamDinh.Text = "Chứng thư giám định";
            // 
            // BSChungThuGiamDinh
            // 
            this.BSChungThuGiamDinh.Key = "BSChungThuGiamDinh";
            this.BSChungThuGiamDinh.Name = "BSChungThuGiamDinh";
            this.BSChungThuGiamDinh.Text = "Bổ sung chứng thư giám định";
            // 
            // cmdInChungTuGiay
            // 
            this.cmdInChungTuGiay.Key = "cmdInChungTuGiay";
            this.cmdInChungTuGiay.Name = "cmdInChungTuGiay";
            this.cmdInChungTuGiay.Text = "Phiếu ghi kết quả kiểm tra chứng từ giấy";
            // 
            // cmdInKiemTraHangHoa
            // 
            this.cmdInKiemTraHangHoa.Key = "cmdInKiemTraHangHoa";
            this.cmdInKiemTraHangHoa.Name = "cmdInKiemTraHangHoa";
            this.cmdInKiemTraHangHoa.Text = "Phiếu ghi kết quả kiểm tra hàng hóa";
            // 
            // cmdContainer196
            // 
            this.cmdContainer196.Key = "cmdContainer196";
            this.cmdContainer196.Name = "cmdContainer196";
            this.cmdContainer196.Text = "In bảng kê Container";
            // 
            // cmdInTKTCTT196
            // 
            this.cmdInTKTCTT196.ImageIndex = 0;
            this.cmdInTKTCTT196.Key = "cmdInTKTCTT196";
            this.cmdInTKTCTT196.Name = "cmdInTKTCTT196";
            this.cmdInTKTCTT196.Text = "In tờ khai tại chỗ (Thông tư 196)";
            // 
            // cmdChunTuTruocDo
            // 
            this.cmdChunTuTruocDo.Key = "cmdChunTuTruocDo";
            this.cmdChunTuTruocDo.Name = "cmdChunTuTruocDo";
            this.cmdChunTuTruocDo.Text = "Chứng từ trước đó";
            // 
            // cmdInBangKeHD_ToKhai
            // 
            this.cmdInBangKeHD_ToKhai.ImageIndex = 0;
            this.cmdInBangKeHD_ToKhai.Key = "cmdInBangKeHD_ToKhai";
            this.cmdInBangKeHD_ToKhai.Name = "cmdInBangKeHD_ToKhai";
            this.cmdInBangKeHD_ToKhai.Text = "In bảng kê hợp đồng/ đơn hàng kèm theo tờ khai";
            // 
            // cmdBaoLanhThue
            // 
            this.cmdBaoLanhThue.Key = "cmdBaoLanhThue";
            this.cmdBaoLanhThue.Name = "cmdBaoLanhThue";
            this.cmdBaoLanhThue.Text = "Bảo lãnh thuế";
            // 
            // LeftRebar1
            // 
            this.LeftRebar1.CommandManager = this.cmMain;
            this.LeftRebar1.Dock = System.Windows.Forms.DockStyle.Left;
            this.LeftRebar1.Location = new System.Drawing.Point(0, 0);
            this.LeftRebar1.Name = "LeftRebar1";
            this.LeftRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // RightRebar1
            // 
            this.RightRebar1.CommandManager = this.cmMain;
            this.RightRebar1.Dock = System.Windows.Forms.DockStyle.Right;
            this.RightRebar1.Location = new System.Drawing.Point(0, 0);
            this.RightRebar1.Name = "RightRebar1";
            this.RightRebar1.Size = new System.Drawing.Size(0, 0);
            // 
            // TopRebar1
            // 
            this.TopRebar1.CommandBars.AddRange(new Janus.Windows.UI.CommandBars.UICommandBar[] {
            this.cmbToolBar});
            this.TopRebar1.CommandManager = this.cmMain;
            this.TopRebar1.Controls.Add(this.cmbToolBar);
            this.TopRebar1.Dock = System.Windows.Forms.DockStyle.Top;
            this.TopRebar1.Location = new System.Drawing.Point(0, 0);
            this.TopRebar1.Name = "TopRebar1";
            this.TopRebar1.Size = new System.Drawing.Size(1184, 32);
            // 
            // ilLarge
            // 
            this.ilLarge.ColorDepth = System.Windows.Forms.ColorDepth.Depth32Bit;
            this.ilLarge.ImageSize = new System.Drawing.Size(24, 24);
            this.ilLarge.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // cmdReadExcel2
            // 
            this.cmdReadExcel2.Key = "cmdReadExcel";
            this.cmdReadExcel2.Name = "cmdReadExcel2";
            // 
            // ChungTuKemTheo2
            // 
            this.ChungTuKemTheo2.Key = "ChungTuKemTheo";
            this.ChungTuKemTheo2.Name = "ChungTuKemTheo2";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.Filter = "Excel file|*.xls";
            // 
            // uiPanel3
            // 
            this.uiPanel3.Location = new System.Drawing.Point(3, 3);
            this.uiPanel3.Name = "uiPanel3";
            this.uiPanel3.Size = new System.Drawing.Size(200, 256);
            this.uiPanel3.TabIndex = 4;
            this.uiPanel3.Text = "Panel 3";
            // 
            // rangeNH
            // 
            this.rangeNH.ControlToValidate = this.txtPhiNganHang;
            this.rangeNH.ErrorMessage = "Giá trị này không hợp lệ.";
            this.rangeNH.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeNH.Icon")));
            this.rangeNH.MaximumValue = "999999";
            this.rangeNH.MinimumValue = "0";
            this.rangeNH.Tag = "rangeNH";
            this.rangeNH.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rangeVC
            // 
            this.rangeVC.ControlToValidate = this.txtPhiVanChuyen;
            this.rangeVC.ErrorMessage = "Giá trị này không hợp lệ.";
            this.rangeVC.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeVC.Icon")));
            this.rangeVC.MaximumValue = "9999999999";
            this.rangeVC.MinimumValue = "0";
            this.rangeVC.Tag = "rangeVC";
            this.rangeVC.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rangePHQ
            // 
            this.rangePHQ.ControlToValidate = this.txtLePhiHQ;
            this.rangePHQ.ErrorMessage = "Giá trị này không hợp lệ.";
            this.rangePHQ.Icon = ((System.Drawing.Icon)(resources.GetObject("rangePHQ.Icon")));
            this.rangePHQ.MaximumValue = "999999999";
            this.rangePHQ.MinimumValue = "0";
            this.rangePHQ.Tag = "rangePHQ";
            this.rangePHQ.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rangeBH
            // 
            this.rangeBH.ControlToValidate = this.txtPhiBaoHiem;
            this.rangeBH.ErrorMessage = "Giá trị này không hợp lệ.";
            this.rangeBH.Icon = ((System.Drawing.Icon)(resources.GetObject("rangeBH.Icon")));
            this.rangeBH.MaximumValue = "9999999999";
            this.rangeBH.MinimumValue = "0";
            this.rangeBH.Tag = "rangeBH";
            this.rangeBH.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // VanDon1
            // 
            this.VanDon1.Key = "VanDon";
            this.VanDon1.Name = "VanDon1";
            // 
            // gridEXExporter1
            // 
            this.gridEXExporter1.GridEX = this.dgList;
            // 
            // rfvNgayDen
            // 
            this.rfvNgayDen.ControlToValidate = this.ccNgayDen;
            this.rfvNgayDen.ErrorMessage = "\"Ngày đến\" không được để trống.";
            this.rfvNgayDen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvNgayDen.Icon")));
            this.rfvNgayDen.Tag = "rfvNgayDen";
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 200;
            // 
            // timer1
            // 
            this.timer1.Interval = 500;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // ToKhaiMauDichForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.ClientSize = new System.Drawing.Size(1184, 756);
            this.Controls.Add(this.uiGroupBox1);
            this.Controls.Add(this.TopRebar1);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.Name = "ToKhaiMauDichForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.Text = "Tờ khai nhập khẩu";
            this.Load += new System.EventHandler(this.ToKhaiNhapKhauSXXK_Form_Load);
            this.Controls.SetChildIndex(this.TopRebar1, 0);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider)).EndInit();
            this.pnlToKhaiMauDich.ResumeLayout(false);
            this.pnlToKhaiMauDich.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiDamBaoNghiaVuNT)).EndInit();
            this.uiDamBaoNghiaVuNT.ResumeLayout(false);
            this.uiDamBaoNghiaVuNT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiAnHanThue)).EndInit();
            this.uiAnHanThue.ResumeLayout(false);
            this.uiAnHanThue.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiNK)).EndInit();
            this.grbNguoiNK.ResumeLayout(false);
            this.grbNguoiNK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemXepHang)).EndInit();
            this.grbDiaDiemXepHang.ResumeLayout(false);
            this.grbDiaDiemXepHang.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguyenTe)).EndInit();
            this.grbNguyenTe.ResumeLayout(false);
            this.grbNguyenTe.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNuocXK)).EndInit();
            this.grbNuocXK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox6)).EndInit();
            this.uiGroupBox6.ResumeLayout(false);
            this.uiGroupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox5)).EndInit();
            this.uiGroupBox5.ResumeLayout(false);
            this.uiGroupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbNguoiXK)).EndInit();
            this.grbNguoiXK.ResumeLayout(false);
            this.grbNguoiXK.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox7)).EndInit();
            this.uiGroupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbHopDong)).EndInit();
            this.grbHopDong.ResumeLayout(false);
            this.grbHopDong.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox18)).EndInit();
            this.uiGroupBox18.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox16)).EndInit();
            this.uiGroupBox16.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbDiaDiemDoHang)).EndInit();
            this.grbDiaDiemDoHang.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grbVanTaiDon)).EndInit();
            this.grbVanTaiDon.ResumeLayout(false);
            this.grbVanTaiDon.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox11)).EndInit();
            this.uiGroupBox11.ResumeLayout(false);
            this.uiGroupBox11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gbGiayPhep)).EndInit();
            this.gbGiayPhep.ResumeLayout(false);
            this.gbGiayPhep.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbHoaDonThuongMai)).EndInit();
            this.grbHoaDonThuongMai.ResumeLayout(false);
            this.grbHoaDonThuongMai.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtHangMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtLoaiHinhMauDich)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtPTTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCuaKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtNguyenTe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCompanyNuoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDonViHaiQuan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvSoHieuPTVT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconTrungToKhai)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanelManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel4)).EndInit();
            this.uiPanel4.ResumeLayout(false);
            this.uiPanel4Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dglistTKTK23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel0)).EndInit();
            this.uiPanel0.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel1)).EndInit();
            this.uiPanel1.ResumeLayout(false);
            this.uiPanel1Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel2)).EndInit();
            this.uiPanel2.ResumeLayout(false);
            this.uiPanel2Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridEX1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TKGT)).EndInit();
            this.TKGT.ResumeLayout(false);
            this.uiPanel3Container.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgToKhaiTriGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel6)).EndInit();
            this.uiPanel6.ResumeLayout(false);
            this.uiPanel6Container.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridCO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNguoiXuatKhau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaTT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rvTyGiaUSD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BottomRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbToolBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeftRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RightRebar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TopRebar1)).EndInit();
            this.TopRebar1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiPanel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeNH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeVC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangePHQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rangeBH)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvNgayDen)).EndInit();
            this.ResumeLayout(false);

        }



        #endregion

        private LoaiHinhMauDichControl01 ctrLoaiHinhMauDich;
        private NguyenTeControl ctrNguyenTe;
        private NuocControl ctrNuocXuatKhau;
        private UIComboBox cbPTTT;
        private UIComboBox cbDKGH;
        private CuaKhauControl ctrCuaKhau;
        private DonViHaiQuanControl ctrDonViHaiQuan;
        private ImageList ImageList1;
        private UICommand cmdThemHang;
        private UICommand cmdTinhLaiThue;
        internal ImageList ilLarge;
        private UICommand cmdPrint;
        private UICommand cmdSend;
        private UIPanel uiPanel2;
        private UIPanelInnerContainer uiPanel2Container;
        private UICommand ChungTuKemTheo;
        private UIGroupBox uiGroupBox4;
        private EditBox txtChungTu;
        private Label label18;
        private Label label10;
        private Label lblTrangThai;
        private EditBox txtSoToKhai;
        private GridEX gridEX1;
        private GridEX dgList;
        private UICommand cmdReadExcel;
        private UICommand NhanDuLieu;
        private UICommand ThemHang2;
        private UICommand cmdThemHang1;
        private UICommand cmdReadExcel1;
        private UICommand ChungTuKemTheo1;
        private UICommand ThemHang1;
        private UICommand cmdReadExcel2;
        private UICommand ChungTuKemTheo2;
        private EditBox txtSoTiepNhan;
        private Label lblSoTiepNhan;
        private UICommand XacNhan;
        private UICommand FileDinhKem;
        private NumericEditBox txtPhiNganHang;
        private Label label24;
        private UICommand Separator2;
        private Label label25;
        private NumericEditBox txtTrongLuongTinh;
        private UICommand ToKhaiTriGia;
        private UIPanel TKGT;
        private UIPanelInnerContainer uiPanel3Container;
        private GridEX dgToKhaiTriGia;
        private UICommand KhaiDienTu;
        private UICommand cmdSend1;
        private UICommand NhanDuLieu1;
        private UICommand XacNhan1;
        private UICommand TienIch;
        private UICommand cmdTinhLaiThue1;
        private UICommand cmdPrint1;
        private UICommand TienIch1;
        private UICommand Huy;
        private UICommand cmdExportExcel1;
        private UICommand cmdExportExcel;
        private SaveFileDialog saveFileDialog1;
        private UICommand cmdToKhaiTriGia;
        private UICommand ToKhaiTriGia1;
        private UICommand cmdToKhaiTriGia1;
        private UICommand cmdToKhaiTGPP21;
        private UICommand cmdToKhaiTGPP31;
        private UICommand cmdToKhaiTGPP2;
        private UICommand cmdToKhaiTGPP3;
        private UIPanel uiPanel3;
        private UIPanel uiPanel4;
        private UIPanelInnerContainer uiPanel4Container;
        private GridEX dglistTKTK23;
        private RangeValidator rangeNH;
        private RangeValidator rangeVC;
        private RangeValidator rangePHQ;
        private RangeValidator rangeBH;
        private UICommand ToKhai;
        private UICommand InPhieuTN;
        private UICommand TruyenDuLieu;
        private UICommand cmdSend2;
        private UICommand NhanDuLieu2;
        private UICommand TruyenDuLieu1;
        private UICommand ChungTuDinhKem;
        private UICommand ChungTuDinhKem1;
        private UICommand cmdVanDon;
        private UICommand cmdHopDong;
        private UICommand VanDon1;
        private UICommand HopDong1;
        private UICommand GiayPhep1;
        private UICommand HoaDon1;
        private UICommand CO1;
        private UICommand cmdGiayPhep;
        private UICommand cmdHoaDon;
        private UICommand cmdCO;
        private UICommand ToKhaiA4;
        private Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1;
        private UICommand VanDon2;
        private UICommand cmdChuyenCuaKhau1;
        private UICommand cmdChuyenCuaKhau;
        private UICommand cmdBoSung;
        private UICommand cmdBoSung1;
        private UICommand cmdBoSungCO;
        private UICommand cmdBoSungCO1;
        private UICommand cmdBoSungGiayPhep1;
        private UICommand cmdBoSungHopDong1;
        private UICommand cmdBoSungHoaDon1;
        private UICommand cmdBoSungChuyenCuaKhau1;
        private UICommand cmdBoSungGiayPhep;
        private UICommand cmdBoSungHopDong;
        private UICommand cmdBoSungHoaDon;
        private UICommand cmdBoSungChuyenCuaKhau;
        private UICommand cmdHuyTK;
        private UICommand cmdSuaTK;
        private UIPanel uiPanel6;
        private UIPanelInnerContainer uiPanel6Container;
        private GroupBox groupBox1;
        private GridEX gridCO;
        private UICommand cmdXacNhanThongTin;
        private Label lblPhanLuong;
        private Label label27;
        private UICommand cmdInTKTQ;
        private UICommand cmdContainer;
        private UICommand cmdChungTuKem1;
        private UICommand cmdChungTuKem;
        private UICommand cmdKetQuaXuLy1;
        private UICommand cmdKetQuaXuLy;
        private UICommand cmdLayPhanHoiSoTiepNhan;
        private UICommand cmdBoSungChungTuDinhKemDangAnh1;
        private UICommand cmdBoSungChungTuDinhKem;
        private EditBox txtDeXuatKhac;
        private Label label26;
        private EditBox txtLyDoSua;
        private Label label28;
        private UICommand cmdSuaTK2;
        private UICommand cmdInTKSuaDoiBoSung;
        private UICommand cmdInTKDTSuaDoiBoSung1;
        private RequiredFieldValidator rfvNgayDen;
        private UIButton btnNoiDungDieuChinhTKForm;
        private UICommand cmdInAnDinhThue1;
        private UICommand cmdInAnDinhThue;
        private UICommand Separator1;
        private UICommand cmdHuyTK2;
        private UICommand Separator3;
        private UICommand cmdChungTuNo1;
        private UICommand cmdChungTuNo;
        private UICommand cmdInToKhaiTQDT_TT151;
        private UICommand cmdInToKhaiTQDT_TT15;
        private UICommand cmdInPhuLucTKNK;
        private UICommand cmdGiayNopTien1;
        private UICommand cmdGiayNopTien;
        private EditBox txtChucVu;
        private Label label35;
        private UICommand cmdInTKTGPP11;
        private UICommand cmdInTKTGPP1;
        private UIGroupBox uiAnHanThue;
        private UICheckBox chkAnHangThue;
        private EditBox txtAnHanThue_LyDo;
        private Label label37;
        private Label label36;
        private NumericEditBox txtAnHanThue_ThoiGian;
        private UIGroupBox uiDamBaoNghiaVuNT;
        private CalendarCombo ccDamBaoNopThue_NgayBatDau;
        private EditBox txtDamBaoNopThue_HinhThuc;
        private UICheckBox chkDamBaoNopThue_isValue;
        private Label label38;
        private Label label40;
        private Label label39;
        private NumericEditBox txtDamBaoNopThue_TriGia;
        private CalendarCombo ccDamBaoNopThue_NgayKetThuc;
        private Label label41;
        private UICommand GiayKiemTra1;
        private UICommand GiayKiemTra;
        private UICommand ChungThuGiamDinh1;
        private UICommand ChungThuGiamDinh;
        private UICommand BSChungThuGiamDinh1;
        private UICommand BSChungThuGiamDinh;
        private UICommand cmdInChungTuGiay1;
        private UICommand cmdInKiemTraHangHoa1;
        private UICommand cmdInChungTuGiay;
        private UICommand cmdInKiemTraHangHoa;
        private UICommand BSGiayKiemTra1;
        private UICommand BSGiayKiemTra;
        private UICommand cmdContainer1961;
        private UICommand cmdContainer196;
        private UICommand cmdInTKTCTT1961;
        private UICommand cmdInTKTCTT196;
        private Company.KDT.SHARE.Components.Controls.TKMDVersionControlH tkmdVersionControl1;
        private CalendarCombo ccNgayTN;
        private Label label32;
        private Label label42;
        private CalendarCombo ccNgayDK;
        private Label label43;
        private EditBox txtHDPL;
        private UICommand cmdChunTuTruocDo1;
        private UICommand cmdChunTuTruocDo;
        private Label label44;
        private PictureBox iconTrungToKhai;
        private ToolTip toolTip1;
        private Timer timer1;
        private UICommand cmdInBangKeHD_ToKhai1;
        private UICommand cmdInBangKeHD_ToKhai;
        private UICommand cmdBaoLanhThue1;
        private UICommand cmdBaoLanhThue;
    }
}
