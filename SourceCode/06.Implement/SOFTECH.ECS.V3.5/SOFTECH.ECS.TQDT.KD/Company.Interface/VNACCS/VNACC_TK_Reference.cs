﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Company.KDT.SHARE.VNACCS;

namespace Company.Interface.VNACCS
{
    public partial class VNACC_TK_Reference : BaseForm
    {
        public VNACC_TK_Reference()
        {
            InitializeComponent();
        }
        public decimal SoToKhai { get; set; }
        public string LoaiVanDon { get; set; }
        public string SoVanDon { get; set; }
        public decimal SoHoaDon { get; set; }
        public string NghiepVu { get; set; }
        private void btnDong_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void VNACC_TK_Reference_Load(object sender, EventArgs e)
        {
            try
            {
                if (NghiepVu == EnumNghiepVu.EDB)
                {
                    txtSoVanDon.Enabled = cmbLoaiVanDon.Enabled = false;
                    this.Text = "Nghiệp vụ tham chiếu tờ khai xuất (Tờ khai tạm)";
                }
                else if (NghiepVu == EnumNghiepVu.IID || NghiepVu == EnumNghiepVu.IEX)
                {
                    txtSoHoaDon.Enabled = txtSoVanDon.Enabled = cmbLoaiVanDon.Enabled = false;
                    this.Text = "Nghiệp vụ tham chiếu tờ khai nhập";
                }
                else if (NghiepVu == EnumNghiepVu.EDD)
                {
                    txtSoHoaDon.Enabled = txtSoVanDon.Enabled = cmbLoaiVanDon.Enabled = false;
                    this.Text = "Nghiệp vụ tham chiếu tờ khai xuất";
                }
                else
                {
                    this.Text = "Nghiệp vụ tham chiếu tờ khai nhập (Tờ khai tạm)";
                }

                txtSoToKhai.Text = SoToKhai.ToString();
                txtSoVanDon.Text = SoVanDon;
                txtSoHoaDon.Text = SoHoaDon.ToString();
                cmbLoaiVanDon.SelectedValue = LoaiVanDon;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        private void btnDongY_Click(object sender, EventArgs e)
        {
            try
            {
                try
                {
                    SoToKhai = System.Convert.ToDecimal(txtSoToKhai.Text.Trim());
                }
                catch
                {
                    throw new Exception("Số tờ khai phải là kiểu số");
                }
                LoaiVanDon = cmbLoaiVanDon.SelectedValue == null ?  "" : cmbLoaiVanDon.SelectedValue.ToString().Trim();
                SoVanDon = txtSoVanDon.Text.Trim();
                try
                {
                    SoHoaDon = System.Convert.ToDecimal(txtSoHoaDon.Text.Trim());
                }
                catch
                {
                    throw new Exception("Số tiếp nhận hóa đơn phải là kiểu số");
                }
                this.DialogResult = DialogResult.OK;
            }
            catch (System.Exception ex)
            {
                this.ShowMessage("Thông tin nhập không đúng" + ex.Message, false);
            }
       
            
        }
        
    }
}
