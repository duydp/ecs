using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.KDT.SHARE.VNACCS;
using System.Data;
using Company.KDT.SHARE.VNACCS.Messages.Recived;

namespace Company.Interface.Report.VNACCS
{
    public partial class BanXacNhanKhaiBaoDMMienThueNK_2 : DevExpress.XtraReports.UI.XtraReport
    {
        public BanXacNhanKhaiBaoDMMienThueNK_2()
        {
            InitializeComponent();
        }
        public void BindingReport(VAD8010 vad8010,string maNV)
        {
            lblTenThongTinXuat.Text = EnumThongBao.GetTenNV(maNV);
            int HangHoa = vad8010.HangHoa.Count;
            if (HangHoa <= 8)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa / 8, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblGhiChuNK.Text = vad8010.A32.GetValue().ToString().ToUpper();
            lblCamKet.Text = vad8010.A33.GetValue().ToString().ToUpper();
            lblGhiChuHQ.Text = vad8010.A34.GetValue().ToString().ToUpper();
            DataTable dt = ConvertListToTable(vad8010.B01, 15);
            BindingReportHang(dt,false);
        }
        public void BindingReport(VAD8020 vad8010, string maNV)
        {
            lblTenThongTinXuat.Text = EnumThongBao.GetTenNV(maNV);
            int HangHoa = vad8010.HangHoa.Count;
            if (HangHoa <= 8)
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3).ToString();
            }
            else
            {
                lblTongSoTrang.Text = System.Convert.ToDecimal(3 + Math.Round((decimal)HangHoa / 8, 0, MidpointRounding.AwayFromZero)).ToString();
            }
            lblGhiChuNK.Text = vad8010.A32.GetValue().ToString().ToUpper();
            lblCamKet.Text = vad8010.A33.GetValue().ToString().ToUpper();
            lblGhiChuHQ.Text = vad8010.A34.GetValue().ToString().ToUpper();
            DataTable dt = ConvertListToTable(vad8010.B01, 15);
            BindingReportHang(dt,true);
        }
        private DataTable ConvertListToTable( GroupAttribute group, int loop)
        {
            int STT = 0;
            DataTable gr = new DataTable();
            foreach (PropertiesAttribute attribute in group.listAttribute)
            {
                gr.Columns.Add(attribute.GroupID, attribute.OfType == typeof(int) ? typeof(decimal) : attribute.OfType );
            }
            gr.Columns.Add("STT", typeof(string));
            for (int i = 0; i < loop; i++)
            {
                STT++;
                DataRow dr = gr.NewRow();                       
                foreach (PropertiesAttribute attribute in group.listAttribute)
                {
                    
                    if (attribute.OfType == typeof(DateTime))
                    {
                        if (System.Convert.ToDateTime(attribute.GetValueCollection(i)).Year > 1900)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else if (attribute.OfType == typeof(decimal) || attribute.OfType == typeof(int))
                    {
                        if (System.Convert.ToDecimal(attribute.GetValueCollection(i)) != 0)
                            dr[attribute.GroupID] = attribute.GetValueCollection(i);
                    }
                    else
                        dr[attribute.GroupID] = attribute.GetValueCollection(i);
                   
                }
                dr["STT"] = STT.ToString().ToUpper();
                gr.Rows.Add(dr);
            }
            return gr;
        }
        public void BindingReportHang(DataTable dt,bool isVAD802)
        {
            DetailReport.DataSource = dt;
            if (isVAD802)
            {
                lblSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                lblMaNguoiXNK.DataBindings.Add("Text", DetailReport.DataSource, "VAD8020_B01");
                lblTenNguoiXNK.DataBindings.Add("Text", DetailReport.DataSource, "VAD8020_C01");
            }
            else
            {
                lblSTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                lblMaNguoiXNK.DataBindings.Add("Text", DetailReport.DataSource, "VAD8010_B01");
                lblTenNguoiXNK.DataBindings.Add("Text", DetailReport.DataSource, "VAD8010_C01");
            }
            
        }

    }
}
