﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Company.Interface.GC.WareHouse
{
    public partial class WareHouseCalculateSampleDataForm : Company.Interface.BaseFormHaveGuidPanel
    {
        public WareHouseCalculateSampleDataForm()
        {
            InitializeComponent();            
        }
        public decimal LuongSPXuat = 0;
        public decimal LuongNPL = 0;
        public decimal DinhMuc = 0;
        public decimal TLHH = 0;
        public decimal DinhMucChung = 0;
        private void rdTinhDinhMuc_CheckedChanged(object sender, EventArgs e)
        {
            if (rdTinhDinhMuc.Checked ==true)
            {
                txtDinhMuc.ReadOnly = true;
            }
            else
            {
                txtDinhMuc.ReadOnly = false;
            }
        }

        private void rdTinhLuongSPXuat_CheckedChanged(object sender, EventArgs e)
        {
            if (rdTinhLuongSPXuat.Checked ==true)
            {
                txtLuongSP.ReadOnly = true;
            }
            else
            {
                txtLuongSP.ReadOnly = false;
            }
        }

        private void rdTinhLuongNPL_CheckedChanged(object sender, EventArgs e)
        {
            if (rdTinhLuongNPL.Checked == true)
            {
                txtLuongNPL.ReadOnly = true;
            }
            else
            {
                txtLuongNPL.ReadOnly = false;
            }
        }

        private void txtLuongNPL_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdTinhDinhMuc.Checked == true)
                {
                    LuongNPL = Convert.ToDecimal(txtLuongNPL.Text.ToString());
                    LuongSPXuat = Convert.ToDecimal(txtLuongSP.Text.ToString());
                    TLHH = Convert.ToDecimal(txtTyLeHH.Text.ToString());
                    DinhMuc = LuongSPXuat / (LuongNPL * (1 + TLHH / 100));
                    DinhMucChung = DinhMuc + DinhMuc * TLHH / 100;
                    txtDinhMuc.Text = DinhMuc.ToString();
                    txtDinhMucChung.Text = DinhMucChung.ToString();
                }
                else if (rdTinhLuongSPXuat.Checked == true)
                {
                    LuongNPL = Convert.ToDecimal(txtLuongNPL.Text.ToString());
                    TLHH = Convert.ToDecimal(txtTyLeHH.Text.ToString());
                    DinhMuc = Convert.ToDecimal(txtDinhMuc.Text.ToString());
                    DinhMucChung = DinhMuc + DinhMuc * TLHH / 100;
                    txtDinhMucChung.Text = DinhMucChung.ToString();
                    txtLuongSP.Text = (LuongNPL / DinhMucChung).ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtLuongSP_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdTinhDinhMuc.Checked == true)
                {
                    LuongNPL = Convert.ToDecimal(txtLuongNPL.Text.ToString());
                    LuongSPXuat = Convert.ToDecimal(txtLuongSP.Text.ToString());
                    TLHH = Convert.ToDecimal(txtTyLeHH.Text.ToString());
                    DinhMuc = LuongSPXuat / (LuongNPL * (1 + TLHH / 100));
                    DinhMucChung = DinhMuc + DinhMuc * TLHH / 100;
                    txtDinhMuc.Text = DinhMuc.ToString();
                    txtDinhMucChung.Text = DinhMucChung.ToString();
                }
                else if (rdTinhLuongNPL.Checked == true)
                {
                    LuongSPXuat = Convert.ToDecimal(txtLuongSP.Text.ToString());
                    TLHH = Convert.ToDecimal(txtTyLeHH.Text.ToString());
                    DinhMuc = Convert.ToDecimal(txtDinhMuc.Text.ToString());
                    DinhMucChung = DinhMuc + DinhMuc * TLHH / 100;
                    LuongNPL = LuongSPXuat * DinhMucChung;
                    txtLuongNPL.Text = LuongNPL.ToString();
                    txtDinhMucChung.Text = DinhMucChung.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtDinhMuc_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdTinhLuongNPL.Checked == true)
                {
                    LuongSPXuat = Convert.ToDecimal(txtLuongSP.Text.ToString());
                    TLHH = Convert.ToDecimal(txtTyLeHH.Text.ToString());
                    DinhMuc = Convert.ToDecimal(txtDinhMuc.Text.ToString());
                    DinhMucChung = DinhMuc + DinhMuc * TLHH / 100;
                    LuongNPL = LuongSPXuat * DinhMucChung;
                    txtLuongNPL.Text = LuongNPL.ToString();
                    txtDinhMucChung.Text = DinhMucChung.ToString();
                }
                else if (rdTinhLuongSPXuat.Checked == true)
                {
                    LuongNPL = Convert.ToDecimal(txtLuongNPL.Text.ToString());
                    TLHH = Convert.ToDecimal(txtTyLeHH.Text.ToString());
                    DinhMuc = Convert.ToDecimal(txtDinhMuc.Text.ToString());
                    DinhMucChung = DinhMuc + DinhMuc * TLHH / 100;
                    txtDinhMucChung.Text = DinhMucChung.ToString();
                    txtLuongSP.Text = (LuongNPL / DinhMucChung).ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtTyLeHH_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdTinhLuongNPL.Checked == true)
                {
                    LuongSPXuat = Convert.ToDecimal(txtLuongSP.Text.ToString());
                    TLHH = Convert.ToDecimal(txtTyLeHH.Text.ToString());
                    DinhMuc = Convert.ToDecimal(txtDinhMuc.Text.ToString());
                    DinhMucChung = DinhMuc + DinhMuc * TLHH / 100;
                    LuongNPL = LuongSPXuat * DinhMucChung;
                    txtLuongNPL.Text = LuongNPL.ToString();
                    txtDinhMucChung.Text = DinhMucChung.ToString();
                }
                else if (rdTinhLuongSPXuat.Checked == true)
                {
                    LuongNPL = Convert.ToDecimal(txtLuongNPL.Text.ToString());
                    TLHH = Convert.ToDecimal(txtTyLeHH.Text.ToString());
                    DinhMuc = Convert.ToDecimal(txtDinhMuc.Text.ToString());
                    DinhMucChung = DinhMuc + DinhMuc * TLHH / 100;
                    txtDinhMucChung.Text = DinhMucChung.ToString();
                    txtLuongSP.Text = (LuongNPL / DinhMucChung).ToString();
                }
                else
                {
                    LuongNPL = Convert.ToDecimal(txtLuongNPL.Text.ToString());
                    LuongSPXuat = Convert.ToDecimal(txtLuongSP.Text.ToString());
                    TLHH = Convert.ToDecimal(txtTyLeHH.Text.ToString());
                    DinhMuc = LuongSPXuat / (LuongNPL * (1 + TLHH / 100));
                    DinhMucChung = DinhMuc + DinhMuc * TLHH / 100;
                    txtDinhMuc.Text = DinhMuc.ToString();
                    txtDinhMucChung.Text = DinhMucChung.ToString();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
