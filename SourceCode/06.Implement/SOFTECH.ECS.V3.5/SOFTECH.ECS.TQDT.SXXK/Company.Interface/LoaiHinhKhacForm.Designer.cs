﻿using System.ComponentModel;
using System.Windows.Forms;
using Company.Interface.Controls;
using Janus.Windows.EditControls;
using Janus.Windows.GridEX;
using Janus.Windows.GridEX.EditControls;

namespace Company.Interface
{
    partial class LoaiHinhKhacForm  
    {
        private UIGroupBox uiGroupBox1;
        private Label label16;
        private Label label17;
        private Label label19;
        private Label label2;
        private Label label3;
        private Label label4;
        private Label label6;
        private Label label7;
        private Label label8;
        private Label label27;
        private UIGroupBox uiGroupBox2;
        private UIGroupBox grbThue;
        private Label label1;
        private Label label5;
        private Label label10;
        private Label label11;
        private Label label12;
        private Label label13;
        private Label label14;
        private Label label15;
        private UIGroupBox uiGroupBox4;
        private UIButton btnAddNew;
        private NumericEditBox txtDGNT;
        private EditBox txtMaHang;
        private EditBox txtTenHang;
        private EditBox txtMaHS;
        private NumericEditBox txtTienThue_GTGT;
        private NumericEditBox txtTienThue_NK;
        private NumericEditBox txtTGTT_TTDB;
        private NumericEditBox txtTGTT_GTGT;
        private NumericEditBox txtTGTT_NK;
        private NumericEditBox txtTS_GTGT;
        private NumericEditBox txtTS_NK;
        private NumericEditBox txtTienThue_TTDB;
        private NumericEditBox txtTS_TTDB;
        
        private Label label18;
        private ToolTip toolTip1;
        private Label lblTongTGKB;
        private Label lblTyGiaTT;
        private Label lblDVT;
        private Label lblNguyenTe_DGNT;
        private Label lblNguyenTe_TGNT;
        private ErrorProvider epError;
        private NumericEditBox txtCLG;
        private Label label9;
        private NumericEditBox txtTL_CLG;
        private NumericEditBox txtTien_CLG;
        private NuocHControl ctrNuocXX;
        private IContainer components;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoaiHinhKhacForm));
            Janus.Windows.GridEX.GridEXLayout dgList_DesignTimeLayout = new Janus.Windows.GridEX.GridEXLayout();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.chkMienThue = new Janus.Windows.EditControls.UICheckBox();
            this.dgList = new Janus.Windows.GridEX.GridEX();
            this.ImageList1 = new System.Windows.Forms.ImageList(this.components);
            this.lblTyGiaTT = new System.Windows.Forms.Label();
            this.lblTongTGKB = new System.Windows.Forms.Label();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtTriGiaKB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtLuong = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.ctrNuocXX = new Company.Interface.Controls.NuocHControl();
            this.lblDVT = new System.Windows.Forms.Label();
            this.lblNguyenTe_TGNT = new System.Windows.Forms.Label();
            this.lblNguyenTe_DGNT = new System.Windows.Forms.Label();
            this.txtDGNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGNT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.cbDonViTinh = new Janus.Windows.EditControls.UIComboBox();
            this.txtMaHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtTenHang = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtMaHS = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.grbThue = new Janus.Windows.EditControls.UIGroupBox();
            this.txtTongSoTienThue = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTienThue_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThue_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTien_CLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTienThue_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtCLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTGTT_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTL_CLG = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_TTDB = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_GTGT = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtTS_NK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.btnAddNew = new Janus.Windows.EditControls.UIButton();
            this.epError = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.lvsError = new Company.Controls.CustomValidation.ListValidationSummary();
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvTen = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMaHS = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.revMaHS = new Company.Controls.CustomValidation.RegularExpressionValidator();
            this.cvSoLuong = new Company.Controls.CustomValidation.CompareValidator();
            this.cvDonGia = new Company.Controls.CustomValidation.CompareValidator();
            this.rv1 = new Company.Controls.CustomValidation.RangeValidator();
            this.rv2 = new Company.Controls.CustomValidation.RangeValidator();
            this.rv3 = new Company.Controls.CustomValidation.RangeValidator();
            this.rv4 = new Company.Controls.CustomValidation.RangeValidator();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbThue)).BeginInit();
            this.grbThue.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.revMaHS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvDonGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rv1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rv2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rv3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rv4)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(709, 435);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox1.Controls.Add(this.btnClose);
            this.uiGroupBox1.Controls.Add(this.chkMienThue);
            this.uiGroupBox1.Controls.Add(this.dgList);
            this.uiGroupBox1.Controls.Add(this.lblTyGiaTT);
            this.uiGroupBox1.Controls.Add(this.lblTongTGKB);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox4);
            this.uiGroupBox1.Controls.Add(this.uiGroupBox2);
            this.uiGroupBox1.Controls.Add(this.grbThue);
            this.uiGroupBox1.Controls.Add(this.btnAddNew);
            this.uiGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox1.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox1.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(709, 435);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            this.uiGroupBox1.Click += new System.EventHandler(this.uiGroupBox1_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.ImageHorizontalAlignment = Janus.Windows.EditControls.ImageHorizontalAlignment.Near;
            this.btnClose.Location = new System.Drawing.Point(622, 275);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 5;
            this.btnClose.Text = "Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnClose.VisualStyleManager = this.vsmMain;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // chkMienThue
            // 
            this.chkMienThue.BackColor = System.Drawing.Color.Transparent;
            this.chkMienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkMienThue.ForeColor = System.Drawing.Color.Red;
            this.chkMienThue.Location = new System.Drawing.Point(579, 3);
            this.chkMienThue.Name = "chkMienThue";
            this.chkMienThue.Size = new System.Drawing.Size(78, 23);
            this.chkMienThue.TabIndex = 2;
            this.chkMienThue.Text = "Miễn thuế";
            this.chkMienThue.VisualStyleManager = this.vsmMain;
            this.chkMienThue.CheckedChanged += new System.EventHandler(this.chkMienThue_CheckedChanged);
            // 
            // dgList
            // 
            this.dgList.AllowDelete = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.AllowEdit = Janus.Windows.GridEX.InheritableBoolean.False;
            this.dgList.AlternatingColors = true;
            this.dgList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.dgList.BuiltInTextsData = "<LocalizableData ID=\"LocalizableStrings\" Collection=\"true\"><GroupByBoxInfo>Hiển t" +
                "hị theo nhóm.</GroupByBoxInfo></LocalizableData>";
            dgList_DesignTimeLayout.LayoutString = resources.GetString("dgList_DesignTimeLayout.LayoutString");
            this.dgList.DesignTimeLayout = dgList_DesignTimeLayout;
            this.dgList.EditorsControlStyle.ButtonAppearance = Janus.Windows.GridEX.ButtonAppearance.Regular;
            this.dgList.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.dgList.FrozenColumns = 3;
            this.dgList.GroupByBoxVisible = false;
            this.dgList.GroupRowFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.FontBold = Janus.Windows.GridEX.TriState.True;
            this.dgList.HeaderFormatStyle.TextAlignment = Janus.Windows.GridEX.TextAlignment.Center;
            this.dgList.ImageList = this.ImageList1;
            this.dgList.Location = new System.Drawing.Point(12, 304);
            this.dgList.Name = "dgList";
            this.dgList.RowHeaders = Janus.Windows.GridEX.InheritableBoolean.True;
            this.dgList.SelectionMode = Janus.Windows.GridEX.SelectionMode.MultipleSelectionSameTable;
            this.dgList.Size = new System.Drawing.Size(685, 106);
            this.dgList.TabIndex = 6;
            this.dgList.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.dgList.VisualStyleManager = this.vsmMain;
            this.dgList.RowDoubleClick += new Janus.Windows.GridEX.RowActionEventHandler(this.dgList_RowDoubleClick);
            this.dgList.DeletingRecords += new System.ComponentModel.CancelEventHandler(this.dgList_DeletingRecords);
            this.dgList.LoadingRow += new Janus.Windows.GridEX.RowLoadEventHandler(this.dgList_LoadingRow);
            // 
            // ImageList1
            // 
            this.ImageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ImageList1.ImageStream")));
            this.ImageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.ImageList1.Images.SetKeyName(0, "");
            this.ImageList1.Images.SetKeyName(1, "");
            this.ImageList1.Images.SetKeyName(2, "");
            this.ImageList1.Images.SetKeyName(3, "");
            // 
            // lblTyGiaTT
            // 
            this.lblTyGiaTT.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTyGiaTT.AutoSize = true;
            this.lblTyGiaTT.BackColor = System.Drawing.Color.Transparent;
            this.lblTyGiaTT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTyGiaTT.Location = new System.Drawing.Point(14, 416);
            this.lblTyGiaTT.Name = "lblTyGiaTT";
            this.lblTyGiaTT.Size = new System.Drawing.Size(98, 13);
            this.lblTyGiaTT.TabIndex = 7;
            this.lblTyGiaTT.Text = "Tỷ giá tính thuế:";
            // 
            // lblTongTGKB
            // 
            this.lblTongTGKB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblTongTGKB.AutoSize = true;
            this.lblTongTGKB.BackColor = System.Drawing.Color.Transparent;
            this.lblTongTGKB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTongTGKB.Location = new System.Drawing.Point(353, 416);
            this.lblTongTGKB.Name = "lblTongTGKB";
            this.lblTongTGKB.Size = new System.Drawing.Size(125, 13);
            this.lblTongTGKB.TabIndex = 8;
            this.lblTongTGKB.Text = "Tổng trị giá khai báo:";
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.label30);
            this.uiGroupBox4.Controls.Add(this.label29);
            this.uiGroupBox4.Controls.Add(this.label24);
            this.uiGroupBox4.Controls.Add(this.label23);
            this.uiGroupBox4.Controls.Add(this.txtTriGiaKB);
            this.uiGroupBox4.Controls.Add(this.txtLuong);
            this.uiGroupBox4.Controls.Add(this.ctrNuocXX);
            this.uiGroupBox4.Controls.Add(this.lblDVT);
            this.uiGroupBox4.Controls.Add(this.lblNguyenTe_TGNT);
            this.uiGroupBox4.Controls.Add(this.lblNguyenTe_DGNT);
            this.uiGroupBox4.Controls.Add(this.txtDGNT);
            this.uiGroupBox4.Controls.Add(this.txtTGNT);
            this.uiGroupBox4.Controls.Add(this.label7);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Controls.Add(this.label6);
            this.uiGroupBox4.Controls.Add(this.label18);
            this.uiGroupBox4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox4.Location = new System.Drawing.Point(12, 144);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(361, 154);
            this.uiGroupBox4.TabIndex = 1;
            this.uiGroupBox4.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.ForeColor = System.Drawing.Color.Red;
            this.label30.Location = new System.Drawing.Point(263, 48);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(13, 13);
            this.label30.TabIndex = 17;
            this.label30.Text = "*";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.ForeColor = System.Drawing.Color.Red;
            this.label29.Location = new System.Drawing.Point(263, 21);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(13, 13);
            this.label29.TabIndex = 16;
            this.label29.Text = "*";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.BackColor = System.Drawing.Color.Transparent;
            this.label24.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(279, 101);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 13);
            this.label24.TabIndex = 11;
            this.label24.Text = "(VND)";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.BackColor = System.Drawing.Color.Transparent;
            this.label23.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(5, 101);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(79, 13);
            this.label23.TabIndex = 9;
            this.label23.Text = "Trị giá khai báo";
            // 
            // txtTriGiaKB
            // 
            this.txtTriGiaKB.BackColor = System.Drawing.Color.White;
            this.txtTriGiaKB.DecimalDigits = 3;
            this.txtTriGiaKB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTriGiaKB.Location = new System.Drawing.Point(133, 96);
            this.txtTriGiaKB.Name = "txtTriGiaKB";
            this.txtTriGiaKB.ReadOnly = true;
            this.txtTriGiaKB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTriGiaKB.Size = new System.Drawing.Size(128, 21);
            this.txtTriGiaKB.TabIndex = 10;
            this.txtTriGiaKB.TabStop = false;
            this.txtTriGiaKB.Text = "0.000";
            this.txtTriGiaKB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtTriGiaKB, "Trị giá khai báo");
            this.txtTriGiaKB.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTriGiaKB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTriGiaKB.VisualStyleManager = this.vsmMain;
            // 
            // txtLuong
            // 
            this.txtLuong.DecimalDigits = 3;
            this.txtLuong.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLuong.Location = new System.Drawing.Point(133, 16);
            this.txtLuong.MaxLength = 15;
            this.txtLuong.Name = "txtLuong";
            this.txtLuong.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtLuong.Size = new System.Drawing.Size(128, 21);
            this.txtLuong.TabIndex = 1;
            this.txtLuong.Text = "0.000";
            this.txtLuong.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtLuong.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtLuong.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtLuong.VisualStyleManager = this.vsmMain;
            this.txtLuong.Leave += new System.EventHandler(this.txtLuong_Leave);
            // 
            // ctrNuocXX
            // 
            this.ctrNuocXX.BackColor = System.Drawing.Color.Transparent;
            this.ctrNuocXX.ErrorMessage = "\"Nước xuất xứ\" không được bỏ trống.";
            this.ctrNuocXX.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ctrNuocXX.Location = new System.Drawing.Point(133, 126);
            this.ctrNuocXX.Ma = "";
            this.ctrNuocXX.Name = "ctrNuocXX";
            this.ctrNuocXX.ReadOnly = false;
            this.ctrNuocXX.Size = new System.Drawing.Size(222, 22);
            this.ctrNuocXX.TabIndex = 15;
            this.ctrNuocXX.VisualStyleManager = this.vsmMain;
            // 
            // lblDVT
            // 
            this.lblDVT.AutoSize = true;
            this.lblDVT.BackColor = System.Drawing.Color.Transparent;
            this.lblDVT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDVT.Location = new System.Drawing.Point(263, 18);
            this.lblDVT.Name = "lblDVT";
            this.lblDVT.Size = new System.Drawing.Size(0, 13);
            this.lblDVT.TabIndex = 2;
            // 
            // lblNguyenTe_TGNT
            // 
            this.lblNguyenTe_TGNT.AutoSize = true;
            this.lblNguyenTe_TGNT.BackColor = System.Drawing.Color.Transparent;
            this.lblNguyenTe_TGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguyenTe_TGNT.Location = new System.Drawing.Point(279, 74);
            this.lblNguyenTe_TGNT.Name = "lblNguyenTe_TGNT";
            this.lblNguyenTe_TGNT.Size = new System.Drawing.Size(73, 13);
            this.lblNguyenTe_TGNT.TabIndex = 8;
            this.lblNguyenTe_TGNT.Text = "Mã nguyên tệ";
            // 
            // lblNguyenTe_DGNT
            // 
            this.lblNguyenTe_DGNT.AutoSize = true;
            this.lblNguyenTe_DGNT.BackColor = System.Drawing.Color.Transparent;
            this.lblNguyenTe_DGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNguyenTe_DGNT.Location = new System.Drawing.Point(279, 48);
            this.lblNguyenTe_DGNT.Name = "lblNguyenTe_DGNT";
            this.lblNguyenTe_DGNT.Size = new System.Drawing.Size(73, 13);
            this.lblNguyenTe_DGNT.TabIndex = 5;
            this.lblNguyenTe_DGNT.Text = "Mã nguyên tệ";
            // 
            // txtDGNT
            // 
            this.txtDGNT.DecimalDigits = 20;
            this.txtDGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDGNT.FormatMask = Janus.Windows.GridEX.NumericEditFormatMask.General;
            this.txtDGNT.Location = new System.Drawing.Point(133, 43);
            this.txtDGNT.MaxLength = 25;
            this.txtDGNT.Name = "txtDGNT";
            this.txtDGNT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtDGNT.Size = new System.Drawing.Size(128, 21);
            this.txtDGNT.TabIndex = 4;
            this.txtDGNT.Text = "0";
            this.txtDGNT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtDGNT, "Đơn giá khai báo");
            this.txtDGNT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtDGNT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtDGNT.VisualStyleManager = this.vsmMain;
            this.txtDGNT.Leave += new System.EventHandler(this.txtDGNT_Leave);
            // 
            // txtTGNT
            // 
            this.txtTGNT.BackColor = System.Drawing.Color.White;
            this.txtTGNT.DecimalDigits = 3;
            this.txtTGNT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGNT.Location = new System.Drawing.Point(133, 69);
            this.txtTGNT.Name = "txtTGNT";
            this.txtTGNT.ReadOnly = true;
            this.txtTGNT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGNT.Size = new System.Drawing.Size(128, 21);
            this.txtTGNT.TabIndex = 7;
            this.txtTGNT.TabStop = false;
            this.txtTGNT.Text = "0.000";
            this.txtTGNT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.toolTip1.SetToolTip(this.txtTGNT, "Trị giá khai báo");
            this.txtTGNT.Value = new decimal(new int[] {
            0,
            0,
            0,
            196608});
            this.txtTGNT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTGNT.VisualStyleManager = this.vsmMain;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 50);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Đơn giá nguyên tệ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(5, 77);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "Trị giá nguyên tệ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(5, 26);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(49, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Số lượng";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.BackColor = System.Drawing.Color.Transparent;
            this.label18.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(5, 129);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(73, 13);
            this.label18.TabIndex = 14;
            this.label18.Text = "Nước xuất xứ";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label28);
            this.uiGroupBox2.Controls.Add(this.label26);
            this.uiGroupBox2.Controls.Add(this.label25);
            this.uiGroupBox2.Controls.Add(this.label22);
            this.uiGroupBox2.Controls.Add(this.cbDonViTinh);
            this.uiGroupBox2.Controls.Add(this.txtMaHang);
            this.uiGroupBox2.Controls.Add(this.label21);
            this.uiGroupBox2.Controls.Add(this.label4);
            this.uiGroupBox2.Controls.Add(this.label2);
            this.uiGroupBox2.Controls.Add(this.label27);
            this.uiGroupBox2.Controls.Add(this.txtTenHang);
            this.uiGroupBox2.Controls.Add(this.txtMaHS);
            this.uiGroupBox2.Controls.Add(this.label3);
            this.uiGroupBox2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 8);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(361, 130);
            this.uiGroupBox2.TabIndex = 0;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.ForeColor = System.Drawing.Color.Red;
            this.label28.Location = new System.Drawing.Point(343, 106);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(13, 13);
            this.label28.TabIndex = 12;
            this.label28.Text = "*";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(343, 79);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(13, 13);
            this.label26.TabIndex = 11;
            this.label26.Text = "*";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.Color.Transparent;
            this.label25.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(343, 53);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(13, 13);
            this.label25.TabIndex = 10;
            this.label25.Text = "*";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.BackColor = System.Drawing.Color.Transparent;
            this.label22.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(343, 26);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 13);
            this.label22.TabIndex = 9;
            this.label22.Text = "*";
            // 
            // cbDonViTinh
            // 
            this.cbDonViTinh.BackColor = System.Drawing.Color.White;
            this.cbDonViTinh.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            this.cbDonViTinh.DisplayMember = "Ten";
            this.cbDonViTinh.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDonViTinh.Location = new System.Drawing.Point(133, 101);
            this.cbDonViTinh.Name = "cbDonViTinh";
            this.cbDonViTinh.Size = new System.Drawing.Size(208, 21);
            this.cbDonViTinh.TabIndex = 8;
            this.cbDonViTinh.TabStop = false;
            this.cbDonViTinh.ValueMember = "ID";
            this.cbDonViTinh.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.cbDonViTinh.VisualStyleManager = this.vsmMain;
            // 
            // txtMaHang
            // 
            this.txtMaHang.BackColor = System.Drawing.Color.White;
            this.txtMaHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHang.Location = new System.Drawing.Point(133, 21);
            this.txtMaHang.Name = "txtMaHang";
            this.txtMaHang.Size = new System.Drawing.Size(208, 21);
            this.txtMaHang.TabIndex = 1;
            this.txtMaHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaHang.VisualStyleManager = this.vsmMain;
            this.txtMaHang.ButtonClick += new System.EventHandler(this.txtMaHang_ButtonClick);
            this.txtMaHang.Leave += new System.EventHandler(this.txtMaHang_Leave);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.Color.Transparent;
            this.label21.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(229, 79);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(85, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "(gồm 10 chữ số)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Mã HS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Tên hàng";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.BackColor = System.Drawing.Color.Transparent;
            this.label27.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(8, 29);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(94, 13);
            this.label27.TabIndex = 0;
            this.label27.Text = "Mã hàng (Mã phụ)";
            // 
            // txtTenHang
            // 
            this.txtTenHang.BackColor = System.Drawing.Color.White;
            this.txtTenHang.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTenHang.Location = new System.Drawing.Point(133, 48);
            this.txtTenHang.Name = "txtTenHang";
            this.txtTenHang.Size = new System.Drawing.Size(208, 21);
            this.txtTenHang.TabIndex = 3;
            this.txtTenHang.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTenHang.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTenHang.VisualStyleManager = this.vsmMain;
            this.txtTenHang.TextChanged += new System.EventHandler(this.txtTGTT_NK_Leave);
            // 
            // txtMaHS
            // 
            this.txtMaHS.BackColor = System.Drawing.Color.White;
            this.txtMaHS.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMaHS.Location = new System.Drawing.Point(133, 74);
            this.txtMaHS.MaxLength = 10;
            this.txtMaHS.Name = "txtMaHS";
            this.txtMaHS.Size = new System.Drawing.Size(89, 21);
            this.txtMaHS.TabIndex = 5;
            this.txtMaHS.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMaHS.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtMaHS.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(8, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Đơn vị tính";
            // 
            // grbThue
            // 
            this.grbThue.BackColor = System.Drawing.Color.Transparent;
            this.grbThue.Controls.Add(this.txtTongSoTienThue);
            this.grbThue.Controls.Add(this.label20);
            this.grbThue.Controls.Add(this.txtTienThue_TTDB);
            this.grbThue.Controls.Add(this.txtTienThue_GTGT);
            this.grbThue.Controls.Add(this.txtTien_CLG);
            this.grbThue.Controls.Add(this.txtTienThue_NK);
            this.grbThue.Controls.Add(this.txtTGTT_TTDB);
            this.grbThue.Controls.Add(this.txtTGTT_GTGT);
            this.grbThue.Controls.Add(this.txtCLG);
            this.grbThue.Controls.Add(this.txtTGTT_NK);
            this.grbThue.Controls.Add(this.txtTL_CLG);
            this.grbThue.Controls.Add(this.txtTS_TTDB);
            this.grbThue.Controls.Add(this.txtTS_GTGT);
            this.grbThue.Controls.Add(this.txtTS_NK);
            this.grbThue.Controls.Add(this.label16);
            this.grbThue.Controls.Add(this.label17);
            this.grbThue.Controls.Add(this.label19);
            this.grbThue.Controls.Add(this.label1);
            this.grbThue.Controls.Add(this.label5);
            this.grbThue.Controls.Add(this.label9);
            this.grbThue.Controls.Add(this.label10);
            this.grbThue.Controls.Add(this.label11);
            this.grbThue.Controls.Add(this.label12);
            this.grbThue.Controls.Add(this.label13);
            this.grbThue.Controls.Add(this.label14);
            this.grbThue.Controls.Add(this.label15);
            this.grbThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grbThue.Location = new System.Drawing.Point(393, 8);
            this.grbThue.Name = "grbThue";
            this.grbThue.Size = new System.Drawing.Size(312, 253);
            this.grbThue.TabIndex = 3;
            this.grbThue.Text = "Phần thuế (VND)";
            this.grbThue.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2003;
            this.grbThue.VisualStyleManager = this.vsmMain;
            // 
            // txtTongSoTienThue
            // 
            this.txtTongSoTienThue.BackColor = System.Drawing.Color.White;
            this.txtTongSoTienThue.DecimalDigits = 0;
            this.txtTongSoTienThue.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTongSoTienThue.Location = new System.Drawing.Point(128, 211);
            this.txtTongSoTienThue.Name = "txtTongSoTienThue";
            this.txtTongSoTienThue.ReadOnly = true;
            this.txtTongSoTienThue.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTongSoTienThue.Size = new System.Drawing.Size(176, 21);
            this.txtTongSoTienThue.TabIndex = 25;
            this.txtTongSoTienThue.TabStop = false;
            this.txtTongSoTienThue.Text = "0";
            this.txtTongSoTienThue.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTongSoTienThue.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTongSoTienThue.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTongSoTienThue.VisualStyleManager = this.vsmMain;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.BackColor = System.Drawing.Color.Transparent;
            this.label20.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(4, 215);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(91, 13);
            this.label20.TabIndex = 24;
            this.label20.Text = "Tổng số tiền thuế";
            // 
            // txtTienThue_TTDB
            // 
            this.txtTienThue_TTDB.BackColor = System.Drawing.Color.White;
            this.txtTienThue_TTDB.DecimalDigits = 0;
            this.txtTienThue_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_TTDB.Location = new System.Drawing.Point(192, 87);
            this.txtTienThue_TTDB.Name = "txtTienThue_TTDB";
            this.txtTienThue_TTDB.ReadOnly = true;
            this.txtTienThue_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_TTDB.Size = new System.Drawing.Size(112, 21);
            this.txtTienThue_TTDB.TabIndex = 11;
            this.txtTienThue_TTDB.TabStop = false;
            this.txtTienThue_TTDB.Text = "0";
            this.txtTienThue_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTienThue_TTDB.VisualStyleManager = this.vsmMain;
            // 
            // txtTienThue_GTGT
            // 
            this.txtTienThue_GTGT.BackColor = System.Drawing.Color.White;
            this.txtTienThue_GTGT.DecimalDigits = 0;
            this.txtTienThue_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_GTGT.Location = new System.Drawing.Point(192, 133);
            this.txtTienThue_GTGT.Name = "txtTienThue_GTGT";
            this.txtTienThue_GTGT.ReadOnly = true;
            this.txtTienThue_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_GTGT.Size = new System.Drawing.Size(112, 21);
            this.txtTienThue_GTGT.TabIndex = 17;
            this.txtTienThue_GTGT.TabStop = false;
            this.txtTienThue_GTGT.Text = "0";
            this.txtTienThue_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTienThue_GTGT.VisualStyleManager = this.vsmMain;
            // 
            // txtTien_CLG
            // 
            this.txtTien_CLG.BackColor = System.Drawing.Color.White;
            this.txtTien_CLG.DecimalDigits = 0;
            this.txtTien_CLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTien_CLG.Location = new System.Drawing.Point(192, 179);
            this.txtTien_CLG.Name = "txtTien_CLG";
            this.txtTien_CLG.ReadOnly = true;
            this.txtTien_CLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTien_CLG.Size = new System.Drawing.Size(112, 21);
            this.txtTien_CLG.TabIndex = 23;
            this.txtTien_CLG.TabStop = false;
            this.txtTien_CLG.Text = "0";
            this.txtTien_CLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTien_CLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTien_CLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTien_CLG.VisualStyleManager = this.vsmMain;
            // 
            // txtTienThue_NK
            // 
            this.txtTienThue_NK.BackColor = System.Drawing.Color.White;
            this.txtTienThue_NK.DecimalDigits = 0;
            this.txtTienThue_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTienThue_NK.Location = new System.Drawing.Point(192, 40);
            this.txtTienThue_NK.Name = "txtTienThue_NK";
            this.txtTienThue_NK.ReadOnly = true;
            this.txtTienThue_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTienThue_NK.Size = new System.Drawing.Size(112, 21);
            this.txtTienThue_NK.TabIndex = 5;
            this.txtTienThue_NK.TabStop = false;
            this.txtTienThue_NK.Text = "0";
            this.txtTienThue_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTienThue_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTienThue_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTienThue_NK.VisualStyleManager = this.vsmMain;
            // 
            // txtTGTT_TTDB
            // 
            this.txtTGTT_TTDB.BackColor = System.Drawing.Color.White;
            this.txtTGTT_TTDB.DecimalDigits = 0;
            this.txtTGTT_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_TTDB.Location = new System.Drawing.Point(8, 87);
            this.txtTGTT_TTDB.Name = "txtTGTT_TTDB";
            this.txtTGTT_TTDB.ReadOnly = true;
            this.txtTGTT_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_TTDB.Size = new System.Drawing.Size(112, 21);
            this.txtTGTT_TTDB.TabIndex = 7;
            this.txtTGTT_TTDB.TabStop = false;
            this.txtTGTT_TTDB.Text = "0";
            this.txtTGTT_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTGTT_TTDB.VisualStyleManager = this.vsmMain;
            // 
            // txtTGTT_GTGT
            // 
            this.txtTGTT_GTGT.BackColor = System.Drawing.Color.White;
            this.txtTGTT_GTGT.DecimalDigits = 0;
            this.txtTGTT_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_GTGT.Location = new System.Drawing.Point(8, 133);
            this.txtTGTT_GTGT.Name = "txtTGTT_GTGT";
            this.txtTGTT_GTGT.ReadOnly = true;
            this.txtTGTT_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_GTGT.Size = new System.Drawing.Size(112, 21);
            this.txtTGTT_GTGT.TabIndex = 13;
            this.txtTGTT_GTGT.TabStop = false;
            this.txtTGTT_GTGT.Text = "0";
            this.txtTGTT_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTGTT_GTGT.VisualStyleManager = this.vsmMain;
            // 
            // txtCLG
            // 
            this.txtCLG.BackColor = System.Drawing.Color.White;
            this.txtCLG.DecimalDigits = 0;
            this.txtCLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCLG.Location = new System.Drawing.Point(8, 179);
            this.txtCLG.Name = "txtCLG";
            this.txtCLG.ReadOnly = true;
            this.txtCLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtCLG.Size = new System.Drawing.Size(112, 21);
            this.txtCLG.TabIndex = 19;
            this.txtCLG.TabStop = false;
            this.txtCLG.Text = "0";
            this.txtCLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtCLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtCLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtCLG.VisualStyleManager = this.vsmMain;
            // 
            // txtTGTT_NK
            // 
            this.txtTGTT_NK.BackColor = System.Drawing.Color.White;
            this.txtTGTT_NK.DecimalDigits = 0;
            this.txtTGTT_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTGTT_NK.Location = new System.Drawing.Point(8, 40);
            this.txtTGTT_NK.Name = "txtTGTT_NK";
            this.txtTGTT_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTGTT_NK.Size = new System.Drawing.Size(112, 21);
            this.txtTGTT_NK.TabIndex = 1;
            this.txtTGTT_NK.TabStop = false;
            this.txtTGTT_NK.Text = "0";
            this.txtTGTT_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTGTT_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTGTT_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTGTT_NK.VisualStyleManager = this.vsmMain;
            this.txtTGTT_NK.TextChanged += new System.EventHandler(this.txtTGTT_NK_TextChanged);
            this.txtTGTT_NK.Leave += new System.EventHandler(this.txtTGTT_NK_Leave);
            // 
            // txtTL_CLG
            // 
            this.txtTL_CLG.DecimalDigits = 0;
            this.txtTL_CLG.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTL_CLG.Location = new System.Drawing.Point(128, 179);
            this.txtTL_CLG.MaxLength = 5;
            this.txtTL_CLG.Name = "txtTL_CLG";
            this.txtTL_CLG.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTL_CLG.Size = new System.Drawing.Size(56, 21);
            this.txtTL_CLG.TabIndex = 21;
            this.txtTL_CLG.Text = "0";
            this.txtTL_CLG.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTL_CLG.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTL_CLG.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTL_CLG.VisualStyleManager = this.vsmMain;
            this.txtTL_CLG.Leave += new System.EventHandler(this.txtTS_CLG_Leave);
            // 
            // txtTS_TTDB
            // 
            this.txtTS_TTDB.DecimalDigits = 0;
            this.txtTS_TTDB.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_TTDB.Location = new System.Drawing.Point(128, 87);
            this.txtTS_TTDB.MaxLength = 5;
            this.txtTS_TTDB.Name = "txtTS_TTDB";
            this.txtTS_TTDB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_TTDB.Size = new System.Drawing.Size(56, 21);
            this.txtTS_TTDB.TabIndex = 9;
            this.txtTS_TTDB.Text = "0";
            this.txtTS_TTDB.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_TTDB.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_TTDB.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTS_TTDB.VisualStyleManager = this.vsmMain;
            this.txtTS_TTDB.Leave += new System.EventHandler(this.txtTS_TTDB_Leave);
            // 
            // txtTS_GTGT
            // 
            this.txtTS_GTGT.DecimalDigits = 0;
            this.txtTS_GTGT.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_GTGT.Location = new System.Drawing.Point(128, 133);
            this.txtTS_GTGT.MaxLength = 5;
            this.txtTS_GTGT.Name = "txtTS_GTGT";
            this.txtTS_GTGT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_GTGT.Size = new System.Drawing.Size(56, 21);
            this.txtTS_GTGT.TabIndex = 15;
            this.txtTS_GTGT.Text = "0";
            this.txtTS_GTGT.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_GTGT.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_GTGT.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTS_GTGT.VisualStyleManager = this.vsmMain;
            this.txtTS_GTGT.Leave += new System.EventHandler(this.txtTS_GTGT_Leave);
            // 
            // txtTS_NK
            // 
            this.txtTS_NK.DecimalDigits = 0;
            this.txtTS_NK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTS_NK.Location = new System.Drawing.Point(128, 40);
            this.txtTS_NK.MaxLength = 5;
            this.txtTS_NK.Name = "txtTS_NK";
            this.txtTS_NK.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtTS_NK.Size = new System.Drawing.Size(56, 21);
            this.txtTS_NK.TabIndex = 3;
            this.txtTS_NK.Text = "0";
            this.txtTS_NK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtTS_NK.Value = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.txtTS_NK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2003;
            this.txtTS_NK.VisualStyleManager = this.vsmMain;
            this.txtTS_NK.Leave += new System.EventHandler(this.txtTS_NK_Leave);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(8, 24);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(98, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Trị giá tính thuế NK";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(192, 24);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(68, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Tiền thuế NK";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(123, 24);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "TS (%)";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Trị giá tính thuế TTĐB";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(8, 117);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Trị giá tính thuế GTGT";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 163);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Chênh lệch giá";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(124, 71);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(41, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "TS (%)";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(124, 117);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 14;
            this.label11.Text = "TS (%)";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(126, 163);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(52, 13);
            this.label12.TabIndex = 20;
            this.label12.Text = "Tỷ lệ (%)";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(192, 71);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 13);
            this.label13.TabIndex = 10;
            this.label13.Text = "Tiền thuế TTĐB";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(192, 117);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(81, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Tiền thuế GTGT";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(192, 163);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(98, 13);
            this.label15.TabIndex = 22;
            this.label15.Text = "Tiền chênh lệch giá";
            // 
            // btnAddNew
            // 
            this.btnAddNew.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddNew.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNew.Icon = ((System.Drawing.Icon)(resources.GetObject("btnAddNew.Icon")));
            this.btnAddNew.Location = new System.Drawing.Point(389, 275);
            this.btnAddNew.Name = "btnAddNew";
            this.btnAddNew.Size = new System.Drawing.Size(75, 23);
            this.btnAddNew.TabIndex = 4;
            this.btnAddNew.Text = "Thêm";
            this.btnAddNew.VisualStyle = Janus.Windows.UI.VisualStyle.Office2003;
            this.btnAddNew.VisualStyleManager = this.vsmMain;
            this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);
            // 
            // epError
            // 
            this.epError.ContainerControl = this;
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.lvsError.SetErrorCaption(this.cvError, "Thông báo");
            this.lvsError.SetErrorMessage(this.cvError, "Có một số lỗi sau:");
            this.cvError.HostingForm = this;
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtMaHang;
            this.rfvMa.ErrorMessage = "\"Mã hàng\" không được để trống.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            this.rfvMa.Tag = "rfvMa";
            // 
            // rfvTen
            // 
            this.rfvTen.ControlToValidate = this.txtTenHang;
            this.rfvTen.ErrorMessage = "\"Tên hàng\" không được để trống.";
            this.rfvTen.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvTen.Icon")));
            this.rfvTen.Tag = "rfvTen";
            // 
            // rfvMaHS
            // 
            this.rfvMaHS.ControlToValidate = this.txtMaHS;
            this.rfvMaHS.ErrorMessage = "\"Mã HS\" không được để trống.";
            this.rfvMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMaHS.Icon")));
            this.rfvMaHS.Tag = "rfvMaHS";
            // 
            // revMaHS
            // 
            this.revMaHS.ControlToValidate = this.txtMaHS;
            this.revMaHS.ErrorMessage = "\"Mã HS\" không hợp lệ.";
            this.revMaHS.Icon = ((System.Drawing.Icon)(resources.GetObject("revMaHS.Icon")));
            this.revMaHS.Tag = "revMaHS";
            this.revMaHS.ValidationExpression = "\\d{10}";
            // 
            // cvSoLuong
            // 
            this.cvSoLuong.ControlToValidate = this.txtLuong;
            this.cvSoLuong.ErrorMessage = "\"Số lượng\" không hợp lệ.";
            this.cvSoLuong.Icon = ((System.Drawing.Icon)(resources.GetObject("cvSoLuong.Icon")));
            this.cvSoLuong.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvSoLuong.Tag = "cvSoLuong";
            this.cvSoLuong.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvSoLuong.ValueToCompare = "0";
            // 
            // cvDonGia
            // 
            this.cvDonGia.ControlToValidate = this.txtDGNT;
            this.cvDonGia.ErrorMessage = "\"Đơn giá\" không hợp lệ.";
            this.cvDonGia.Icon = ((System.Drawing.Icon)(resources.GetObject("cvDonGia.Icon")));
            this.cvDonGia.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.GreaterThan;
            this.cvDonGia.Tag = "txtDGNT";
            this.cvDonGia.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            this.cvDonGia.ValueToCompare = "0";
            // 
            // rv1
            // 
            this.rv1.ControlToValidate = this.txtTS_NK;
            this.rv1.ErrorMessage = "Giá trị không hợp lệ";
            this.rv1.Icon = ((System.Drawing.Icon)(resources.GetObject("rv1.Icon")));
            this.rv1.MaximumValue = "9999";
            this.rv1.MinimumValue = "0";
            this.rv1.Tag = "rv1";
            this.rv1.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rv2
            // 
            this.rv2.ControlToValidate = this.txtTS_TTDB;
            this.rv2.ErrorMessage = "Giá trị không hợp lệ";
            this.rv2.Icon = ((System.Drawing.Icon)(resources.GetObject("rv2.Icon")));
            this.rv2.MaximumValue = "9999";
            this.rv2.MinimumValue = "0";
            this.rv2.Tag = "rv2";
            this.rv2.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rv3
            // 
            this.rv3.ControlToValidate = this.txtTS_GTGT;
            this.rv3.ErrorMessage = "Giá trị không hợp lệ";
            this.rv3.Icon = ((System.Drawing.Icon)(resources.GetObject("rv3.Icon")));
            this.rv3.MaximumValue = "9999";
            this.rv3.MinimumValue = "0";
            this.rv3.Tag = "rv3";
            this.rv3.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // rv4
            // 
            this.rv4.ControlToValidate = this.txtTL_CLG;
            this.rv4.ErrorMessage = "Giá trị không hợp lệ";
            this.rv4.Icon = ((System.Drawing.Icon)(resources.GetObject("rv4.Icon")));
            this.rv4.MaximumValue = "9999";
            this.rv4.MinimumValue = "0";
            this.rv4.Tag = "rv4";
            this.rv4.Type = Company.Controls.CustomValidation.ValidationDataType.Currency;
            // 
            // LoaiHinhKhacForm
            // 
            this.AcceptButton = this.btnAddNew;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(709, 435);
            this.Controls.Add(this.uiGroupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoaiHinhKhacForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Thông tin hàng hóa";
            this.Load += new System.EventHandler(this.HangMauDichForm_Load);
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox1, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.uiGroupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grbThue)).EndInit();
            this.grbThue.ResumeLayout(false);
            this.grbThue.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.epError)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvTen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.revMaHS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvSoLuong)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cvDonGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rv1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rv2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rv3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rv4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private UIComboBox cbDonViTinh;
        private GridEX dgList;
        private ImageList ImageList1;
        private NumericEditBox txtTongSoTienThue;
        private Label label20;
        private UICheckBox chkMienThue;
        private UIButton btnClose;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Company.Controls.CustomValidation.ListValidationSummary lvsError;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvTen;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMaHS;
        private Company.Controls.CustomValidation.RegularExpressionValidator revMaHS;
        private Label label21;
        private NumericEditBox txtLuong;
        private Company.Controls.CustomValidation.CompareValidator cvSoLuong;
        private Company.Controls.CustomValidation.CompareValidator cvDonGia;
        // private NumericEditBox txtTriGiaKB;
        private NumericEditBox txtTGNT;
        private Label label23;
        private NumericEditBox txtTriGiaKB;
        private Label label24;
        private Label label30;
        private Label label29;
        private Label label28;
        private Label label26;
        private Label label25;
        private Label label22;
        private Company.Controls.CustomValidation.RangeValidator rv1;
        private Company.Controls.CustomValidation.RangeValidator rv2;
        private Company.Controls.CustomValidation.RangeValidator rv3;
        private Company.Controls.CustomValidation.RangeValidator rv4;
    }
}
