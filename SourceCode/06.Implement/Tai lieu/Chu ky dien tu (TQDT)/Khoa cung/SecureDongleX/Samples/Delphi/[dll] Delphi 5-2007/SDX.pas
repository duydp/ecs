unit SDX;

interface

type

// ====================================================================

TSDX_Find = function(): integer; stdcall;

// Find SecureDongle X attached to the computer
// Return value:
//	<0	Error code
//	=0	No SecureDongle X attached
//	>0	The number of attached SecureDongle X(s)

// ====================================================================

TSDX_Open = function(mode: integer; uid: cardinal; var hid: cardinal): integer; stdcall;

// Open specified SecureDongle X
// Input:
// mode -- This parameter indicates the way to open SDX
// mode = 0,  open the first found SecureDongle X
// mode > 0,  open the SDX according to the UID. The mode value is the SDX number,
//            for example: uid=12345678, mode=2, this will open the second SDX with UID 12345678
// mode = -1, open the SDX according to the HID, and *hid can not be 0
// We will define a Constant below for ease of use:
// HID_MODE = -1

// uid -- UserID,You need to specify the SDX UID and this UID is generated with SDX_GenUID
// hid -- Hardware ID,Open SDX with HID of *hid
// The SDX HID will be returned to *hid regardless of how the SDX was opened.
// Return value:
//	>=0	Success. The opened SDX handle is returned.
//	< 0	Error code. Please refer to latter section

// ====================================================================

TSDX_Close = procedure(handle: integer); stdcall;

// Close specified SecureDongle X
// Input:
// handle -- SDX  handle. It is the handle returned from SDX_Open
// Return value:
// N/A

// ====================================================================

TSDX_Read = function(handle: integer; block_index: integer; buffer512: pchar): integer; stdcall;

// Read SecureDongle X content
// Input:
// handle      -- SDX handle. It is the handle returned from SDX_Open.
// block_index -- Block index. Specify the block to read. The value range is 0-4.
// buffer512   -- Read buffer. The buffer must be at least 512 bytes to accommodate the 512 bytes block size.
// Return value:
// Error code. Please refer to latter section

// ====================================================================

TSDX_Write = function(handle: integer; block_index: integer; buffer512: pchar): integer; stdcall;

// Write to SecureDongle X
// Input:
// handle      -- SDX handle. It is the handle returned from SDX_Open
// block_index -- Block index. Specify the block to read. The value range is 0-4.
// buffer512   -- Read buffer. The buffer must be at least 512 bytes to accommodate the 512 bytes block size.
// Return value:
// Error code. Please refer to latter section

// ====================================================================

TSDX_Transform = function(handle: integer; bufferLength: integer; buffer: pchar): integer; stdcall;

// Data Hashing function
// Input:
// handle -- SDX handle. It is the handle returned from SDX_Open
// len    -- Length of buffer to transform. Maximum 64 characters
// buffer -- Content of data that user want to transform.
// SDX will return the result of transform to *buffer.
// Return value:
// Error code. Please refer to latter section

// ====================================================================

TSDX_GetVersion = function(handle: integer): integer; stdcall;

// Get SecureDongle X hardware version
// Input:
// handle -- SDX handle. It is the handle returned from SDX_Open
// Return value:
//	> 0	Success. The hardware version is returned.
//	< 0	Error code. Please refer to latter section

// ====================================================================

TSDX_RSAEncrypt = function(handle: integer; startIndex: integer; bufferData: pchar; bufferLength: pinteger; key512: pchar): integer; stdcall;

// Encrypt with RSA and write to SecureDongle X
// Input:
// handle     -- SDX handle. It is the handle returned from SDX_Open
// startIndex -- Start index. Specify the start index to write cipher text into SDX. The value range is 0-2559.
// bufferData -- Plaintext that will be encrypted.
// len        -- Length of Data buffer to encrypt. On success, SDX will write the length of cipher text to len.
// Key512     -- Decryption key. The key must 512 Byte. On success, SDX will write the decryption key to Key512.
// Return value:
// Error code. Please refer to latter section

// ====================================================================

TSDX_RSADecrypt = function(handle: integer; startIndex: integer; bufferData: pchar; bufferLength: pinteger; key512: pchar): integer; stdcall;

// Decrypt with RSA and write to SecureDongle X
// Input:
// handle     -- SDX handle. It is the handle returned from SDX_Open
// startIndex -- Start index. Specify the start index to read cipher text into SDX. The value range is 0-2559.
// bufferData -- If success, SDX will write bufferData with plaintext.
// len        -- Length of cipher text to decrypt. If success SDX will write the length of plaintext to len.
// Key512     -- Key that is used to decrypt. The key size must 512 Bytes.
// Return value:
// Error code. Please refer to latter section


var
  SDX_Find       : TSDX_Find;
  SDX_Open       : TSDX_Open;
  SDX_Close      : TSDX_Close;
  SDX_Read       : TSDX_Read;
  SDX_Write      : TSDX_Write;
  SDX_GetVersion : TSDX_GetVersion;
  SDX_Transform  : TSDX_Transform;
  SDX_RSAEncrypt : TSDX_RSAEncrypt;
  SDX_RSADecrypt : TSDX_RSADecrypt;

const
  HID_MODE  : integer = -1;

  // Error codes ========================================================
  SDXERR_SUCCESS									              : integer = 0;         // Success
  SDXERR_NO_SUCH_DEVICE							            : integer = $A0100001; // Specified SDX is not found (parameter error)
  SDXERR_NOT_OPENED_DEVICE						          : integer = $A0100002; // Need to call SDX_Open first to open the SDX, then call this function (operation error)
  SDXERR_WRONG_UID								              : integer = $A0100003; // Wrong UID(parameter error)
  SDXERR_WRONG_INDEX								            : integer = $A0100004; // Block index error (parameter error)
  SDXERR_TOO_LONG_SEED							            : integer = $A0100005; // Seed character string is longer than 64 bytes when calling GenUID (parameter error)
  SDXERR_WRITE_PROTECT							            : integer = $A0100006; // Tried to write to write-protected dongle(operation error)
  SDXERR_WRONG_START_INDEX						          : integer = $A0100007; // Start index wrong (parameter error)
  SDXERR_INVALID_LEN								            : integer = $A0100008; // Invalid length (parameter error)
  SDXERR_TOO_LONG_ENCRYPTION_DATA					      : integer = $A0100009; // Chipertext is too long (cryptography error)
  SDXERR_GENERATE_KEY								            : integer = $A010000A; // Generate key error (cryptography error)
  SDXERR_INVALID_KEY								            : integer = $A010000B; // Invalid key (cryptography error)
  SDXERR_FAILED_ENCRYPTION						          : integer = $A010000C; // Failed to encrypt string (cryptography error)
  SDXERR_FAILED_WRITE_KEY							          : integer = $A010000D; // Failed to write key (cryptography error)
  SDXERR_FAILED_DECRYPTION						          : integer = $A010000E; // Failed to decrypt string (Cryptography error)
  SDXERR_OPEN_DEVICE								            : integer = $A010000F; // Open device error (Windows error)
  SDXERR_READ_REPORT								            : integer = $A0100010; // Read record error(Windows error)
  SDXERR_WRITE_REPORT								            : integer = $A0100011; // Write record error(Windows error)
  SDXERR_SETUP_DI_GET_DEVICE_INTERFACE_DETAIL   : integer = $A0100012; // Internal error (Windows error)
  SDXERR_GET_ATTRIBUTES							            : integer = $A0100013; // Internal error (Windows error)
  SDXERR_GET_PREPARSED_DATA						          : integer = $A0100014; // Internal error (Windows error)
  SDXERR_GETCAPS									              : integer = $A0100015; // Internal error (Windows error)
  SDXERR_FREE_PREPARSED_DATA						        : integer = $A0100016; // Internal error (Windows error)
  SDXERR_FLUSH_QUEUE								            : integer = $A0100017; // Internal error (Windows error)
  SDXERR_SETUP_DI_CLASS_DEVS						        : integer = $A0100018; // Internal error (Windows error)
  SDXERR_GET_SERIAL								              : integer = $A0100019; // Internal error (Windows error)
  SDXERR_GET_PRODUCT_STRING						          : integer = $A010001A; // Internal error (Windows error)
  SDXERR_TOO_LONG_DEVICE_DETAIL					        : integer = $A010001B; // Internal error
  SDXERR_UNKNOWN_DEVICE							            : integer = $A0100020; // Unknown device(hardware error)
  SDXERR_VERIFY									                : integer = $A0100021; // Verification error(hardware error)
  SDXERR_UNKNOWN_ERROR							            : integer = $A010FFFF; // Unknown error(hardware error)

implementation

end.
