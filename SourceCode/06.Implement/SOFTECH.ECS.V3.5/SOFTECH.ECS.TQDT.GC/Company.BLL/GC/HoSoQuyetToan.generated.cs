﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.GC.BLL.GC
{
	public partial class HoSoQuyetToan : ICloneable
	{
		#region Properties.
		
		public int ID { set; get; }
		public string SoHoSo { set; get; }
		public DateTime NgayTao { set; get; }
		public int NamQT { set; get; }
		public string GhiChu { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<HoSoQuyetToan> ConvertToCollection(IDataReader reader)
		{
			List<HoSoQuyetToan> collection = new List<HoSoQuyetToan>();
			while (reader.Read())
			{
				HoSoQuyetToan entity = new HoSoQuyetToan();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt32(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoHoSo"))) entity.SoHoSo = reader.GetString(reader.GetOrdinal("SoHoSo"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTao"))) entity.NgayTao = reader.GetDateTime(reader.GetOrdinal("NgayTao"));
				if (!reader.IsDBNull(reader.GetOrdinal("NamQT"))) entity.NamQT = reader.GetInt32(reader.GetOrdinal("NamQT"));
				if (!reader.IsDBNull(reader.GetOrdinal("GhiChu"))) entity.GhiChu = reader.GetString(reader.GetOrdinal("GhiChu"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<HoSoQuyetToan> collection, int id)
        {
            foreach (HoSoQuyetToan item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_GC_HoSoQuyetToan VALUES(@SoHoSo, @NgayTao, @NamQT, @GhiChu)";
            string update = "UPDATE t_KDT_GC_HoSoQuyetToan SET SoHoSo = @SoHoSo, NgayTao = @NgayTao, NamQT = @NamQT, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_HoSoQuyetToan WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHoSo", SqlDbType.VarChar, "SoHoSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTao", SqlDbType.DateTime, "NgayTao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamQT", SqlDbType.Int, "NamQT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHoSo", SqlDbType.VarChar, "SoHoSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTao", SqlDbType.DateTime, "NgayTao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamQT", SqlDbType.Int, "NamQT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_GC_HoSoQuyetToan VALUES(@SoHoSo, @NgayTao, @NamQT, @GhiChu)";
            string update = "UPDATE t_KDT_GC_HoSoQuyetToan SET SoHoSo = @SoHoSo, NgayTao = @NgayTao, NamQT = @NamQT, GhiChu = @GhiChu WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_GC_HoSoQuyetToan WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoHoSo", SqlDbType.VarChar, "SoHoSo", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTao", SqlDbType.DateTime, "NgayTao", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NamQT", SqlDbType.Int, "NamQT", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoHoSo", SqlDbType.VarChar, "SoHoSo", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTao", SqlDbType.DateTime, "NgayTao", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NamQT", SqlDbType.Int, "NamQT", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GhiChu", SqlDbType.NVarChar, "GhiChu", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.Int, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static HoSoQuyetToan Load(int id)
		{
			const string spName = "[dbo].[p_HoSoQuyetToan_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<HoSoQuyetToan> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_HoSoQuyetToan_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HoSoQuyetToan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_HoSoQuyetToan_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_HoSoQuyetToan_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static int InsertHoSoQuyetToan(string soHoSo, DateTime ngayTao, int namQT, string ghiChu)
		{
			HoSoQuyetToan entity = new HoSoQuyetToan();	
			entity.SoHoSo = soHoSo;
			entity.NgayTao = ngayTao;
			entity.NamQT = namQT;
			entity.GhiChu = ghiChu;
			return entity.Insert();
		}
		
		public int Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public int Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_HoSoQuyetToan_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.Int, 4);
			db.AddInParameter(dbCommand, "@SoHoSo", SqlDbType.VarChar, SoHoSo);
			db.AddInParameter(dbCommand, "@NgayTao", SqlDbType.DateTime, NgayTao.Year <= 1753 ? DBNull.Value : (object) NgayTao);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.Int, NamQT);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (int) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateHoSoQuyetToan(int id, string soHoSo, DateTime ngayTao, int namQT, string ghiChu)
		{
			HoSoQuyetToan entity = new HoSoQuyetToan();			
			entity.ID = id;
			entity.SoHoSo = soHoSo;
			entity.NgayTao = ngayTao;
			entity.NamQT = namQT;
			entity.GhiChu = ghiChu;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_HoSoQuyetToan_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@SoHoSo", SqlDbType.VarChar, SoHoSo);
			db.AddInParameter(dbCommand, "@NgayTao", SqlDbType.DateTime, NgayTao.Year <= 1753 ? DBNull.Value : (object) NgayTao);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.Int, NamQT);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateHoSoQuyetToan(int id, string soHoSo, DateTime ngayTao, int namQT, string ghiChu)
		{
			HoSoQuyetToan entity = new HoSoQuyetToan();			
			entity.ID = id;
			entity.SoHoSo = soHoSo;
			entity.NgayTao = ngayTao;
			entity.NamQT = namQT;
			entity.GhiChu = ghiChu;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HoSoQuyetToan_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			db.AddInParameter(dbCommand, "@SoHoSo", SqlDbType.VarChar, SoHoSo);
			db.AddInParameter(dbCommand, "@NgayTao", SqlDbType.DateTime, NgayTao.Year <= 1753 ? DBNull.Value : (object) NgayTao);
			db.AddInParameter(dbCommand, "@NamQT", SqlDbType.Int, NamQT);
			db.AddInParameter(dbCommand, "@GhiChu", SqlDbType.NVarChar, GhiChu);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteHoSoQuyetToan(int id)
		{
			HoSoQuyetToan entity = new HoSoQuyetToan();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_HoSoQuyetToan_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.Int, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_HoSoQuyetToan_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}