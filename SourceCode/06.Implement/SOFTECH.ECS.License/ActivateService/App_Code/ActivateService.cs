using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Text;
using System.Security.Cryptography;
using System.Xml;
using Components;


/// <summary>
/// Summary description for ActivateService
/// </summary>
[WebService(Namespace = "http://microsoft.com/webservices/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class ActivateService : System.Web.Services.WebService {

    public ActivateService () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    /**
     * ActivateSoftware dung de xac thuc cac thong so phan mem can kich hoat
     * 
     * Tham so truyen vao:
     * Serial: so CD key.
     * ProductID: Ma san pham.
     * Key: khoa nhan biet chinh may dang kich hoat.
     * */

    [WebMethod]
    public ActivateResults Activate(string Serial, string ProductID, string Key)
    {
        Verify obj = new Verify(Serial, ProductID, Key);
        return obj.doVerify(); 
    }
    private string GetMD5Value(string data)
    {

        byte[] DataToHash = Encoding.ASCII.GetBytes(data);
        return BitConverter.ToString(((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(DataToHash)).Replace("-", "").ToUpper();


    }
    [WebMethod]
    public void Activates(string xml,string pass)
    {
        if (pass.Trim().ToUpper() == GetMD5Value("ECSHCM12345ABCD").Trim().ToUpper())
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            XmlNode node=doc.SelectSingleNode("Root");
            DoanhNghiep dn = new DoanhNghiep();
            dn.MaDoanhNghiep = node.SelectSingleNode("MaDoanhNghiep").InnerText;
            dn.TenDoanhNghiep = node.SelectSingleNode("MaDoanhNghiep").InnerText;
            dn.DiaChi = node.SelectSingleNode("MaDoanhNghiep").InnerText;
            dn.GhiChu = node.SelectSingleNode("MaDoanhNghiep").InnerText;
            dn.LoaiHinh = node.SelectSingleNode("MaDoanhNghiep").InnerText;
            dn.Insert();
        }
    }
}

