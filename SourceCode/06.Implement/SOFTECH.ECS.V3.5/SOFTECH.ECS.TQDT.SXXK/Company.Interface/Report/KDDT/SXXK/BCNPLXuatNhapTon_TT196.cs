using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using DevExpress.XtraReports.UI;
using Company.BLL.SXXK.ThanhKhoan;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Logger;

namespace Company.Interface.Report.SXXK
{
    public partial class BCNPLXuatNhapTon_TT196 : DevExpress.XtraReports.UI.XtraReport
    {
        public int LanThanhLy;
        private int STT = 0;
        private int STTBanLuu = 0;
        public int SoHoSo;
        public bool isHoanThue = false;
        private int SoToKhaiNhap = 0;
        private int SoToKhaiXuat = 0;
        private decimal temp = 0;

        private string MaNPLTemp = "";
        private string MaNPLBanLuu = "";

        private string MaSPTemp = "";
        private string MaSPBanLuu = "";

        private decimal _TongLuongNhap = 0;
        private decimal _TongLuongTonDau = 0;
        private decimal _TongLuongNPLSuDung = 0;
        private decimal _TongLuongTonCuoi = 0;
        private decimal _TongLuongNPLTaiXuat = 0;
        private float fontReport = 6;
        private string DATETIME_FORMAT = "dd/MM/yyyy";

        public int NamThanhLy;
        public string MaDoanhNghiep;
        DataTable dataSource = new DataTable();

        public BCNPLXuatNhapTon_TT196()
        {
            InitializeComponent();

            fontReport = GlobalSettings.FontBCXNT;
        }
        public void BindingReport()
        {
            try
            {
                DetailReport.Font = new Font("Times New Roman", GlobalSettings.FontBCXNT);

                lblTenDoanhNghiep.Text = GlobalSettings.TEN_DON_VI;
                lblMaDoanhNghiep.Text = GlobalSettings.MA_DON_VI;
                lblDiaChi.Text = GlobalSettings.DIA_CHI;
                lblSHSTK.Text = "Số " + SoHoSo + (LanThanhLy.ToString() != "" ? " - LẦN " + LanThanhLy : "");
                if (!string.IsNullOrEmpty(GlobalSettings.NoiDungSoHoSoThanhKhoan))
                {
                    lblSHSTK.Text = lblSHSTK.Text + GlobalSettings.NoiDungSoHoSoThanhKhoan;
                }
                lblSHSTK.Width = 300;

                dataSource = GetDataSource();
                DetailReport.DataSource = dataSource;
                lblSoTT.DataBindings.Add("Text", DetailReport.DataSource, "STT");
                lblTenNPL.DataBindings.Add("Text", DetailReport.DataSource, "TenNPL");
                lblDVT.DataBindings.Add("Text", DetailReport.DataSource, "DVT");
                lblSoToKhaiNhapKhau.DataBindings.Add("Text", DetailReport.DataSource, "SoToKhai");
                //lblLuongTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                lblLuongTonDauKy.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonDau", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                //lblLuongNPLSuDung.DataBindings.Add("Text", DetailReport.DataSource, "LuongSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                lblLuongNPLSuDung.DataBindings.Add("Text", DetailReport.DataSource, "LuongSuDung", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                lblLuongNPLTaiXuat.DataBindings.Add("Text", DetailReport.DataSource, "LuongTaiXuat", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                lblLuongTonCuoi.DataBindings.Add("Text", DetailReport.DataSource, "LuongTonCuoi", Company.KDT.SHARE.Components.Globals.FormatNumber(GlobalSettings.SoThapPhan_BaoCaoThanhKhoan));
                lblXuLy.DataBindings.Add("Text", DetailReport.DataSource, "XuLy");
                //HQ
                lblNgayThangHQ.Text = Properties.Settings.Default.TieudeNgay;
                //DN
                lblNgayThangDN.Text = GlobalSettings.TieudeNgay;

            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
            }
            
        }
        private DataTable GetDataSource()
        {
            try
            {

                string spName = "[p_SXXK_BCXuatNhapTon_TT22]";
                //string spName = "[p_KDT_SXXK_BCXuatNhapTon_GetBCXNTByLanThanhLy]";
                SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
                SqlCommand dbCommand = (SqlCommand)db.GetStoredProcCommand(spName);
                db.AddInParameter(dbCommand, "@LanThanhLy", SqlDbType.BigInt, LanThanhLy);
                db.AddInParameter(dbCommand, "@NamThanhLy", SqlDbType.Int, NamThanhLy);
                db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.VarChar, MaDoanhNghiep);
                db.AddInParameter(dbCommand, "@SapXepNgayDangKyTKN", SqlDbType.VarChar, GlobalSettings.SoThapPhan.SapXepTheoTK == 0 ? 0 : 1);
                return db.ExecuteDataSet(dbCommand).Tables[0];
            }
            catch (System.Exception ex)
            {
                LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
    }
}

     