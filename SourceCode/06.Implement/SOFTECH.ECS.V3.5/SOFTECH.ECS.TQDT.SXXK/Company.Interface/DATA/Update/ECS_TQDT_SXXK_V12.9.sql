/*Hungtq*/
IF (SELECT COUNT(*) FROM information_schema.COLUMNS WHERE table_name = 't_KDT_NoiDungDieuChinhTKDetail' AND column_name = 'NguoiTao') = 0
BEGIN
	ALTER TABLE dbo.t_KDT_NoiDungDieuChinhTKDetail
	ADD NguoiTao nvarchar(255) null
END	
GO

-- Drop Existing Procedures

IF OBJECT_ID(N'[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Insert]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Insert]

IF OBJECT_ID(N'[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Update]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Update]

IF OBJECT_ID(N'[dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate]

IF OBJECT_ID(N'[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Delete]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Delete]

IF OBJECT_ID(N'[dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_NoiDungDieuChinhTKDetail_Load]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Load]

IF OBJECT_ID(N'[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectDynamic]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectDynamic]

IF OBJECT_ID(N'[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectAll]') IS NOT NULL
	DROP PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectAll]


GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Insert]
-- Database: ECS_TQDT_KD_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, November 26, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Insert]
	@NoiDungTKChinh nvarchar(max),
	@NoiDungTKSua nvarchar(max),
	@Id_DieuChinh int,
	@NguoiTao nvarchar(255),
	@ID int OUTPUT
AS

INSERT INTO [dbo].[t_KDT_NoiDungDieuChinhTKDetail]
(
	[NoiDungTKChinh],
	[NoiDungTKSua],
	[Id_DieuChinh],
	[NguoiTao]
)
VALUES 
(
	@NoiDungTKChinh,
	@NoiDungTKSua,
	@Id_DieuChinh,
	@NguoiTao
)

SET @ID = SCOPE_IDENTITY()

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Update]
-- Database: ECS_TQDT_KD_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, November 26, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Update]
	@ID int,
	@NoiDungTKChinh nvarchar(max),
	@NoiDungTKSua nvarchar(max),
	@Id_DieuChinh int,
	@NguoiTao nvarchar(255)
AS

UPDATE
	[dbo].[t_KDT_NoiDungDieuChinhTKDetail]
SET
	[NoiDungTKChinh] = @NoiDungTKChinh,
	[NoiDungTKSua] = @NoiDungTKSua,
	[Id_DieuChinh] = @Id_DieuChinh,
	[NguoiTao] = @NguoiTao
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate]
-- Database: ECS_TQDT_KD_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, November 26, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate]
	@ID int,
	@NoiDungTKChinh nvarchar(max),
	@NoiDungTKSua nvarchar(max),
	@Id_DieuChinh int,
	@NguoiTao nvarchar(255)
AS
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_NoiDungDieuChinhTKDetail] WHERE [ID] = @ID)
	BEGIN
		UPDATE
			[dbo].[t_KDT_NoiDungDieuChinhTKDetail] 
		SET
			[NoiDungTKChinh] = @NoiDungTKChinh,
			[NoiDungTKSua] = @NoiDungTKSua,
			[Id_DieuChinh] = @Id_DieuChinh,
			[NguoiTao] = @NguoiTao
		WHERE
			[ID] = @ID
	END
ELSE
	BEGIN
		
		INSERT INTO [dbo].[t_KDT_NoiDungDieuChinhTKDetail]
		(
			[NoiDungTKChinh],
			[NoiDungTKSua],
			[Id_DieuChinh],
			[NguoiTao]
		)
		VALUES 
		(
			@NoiDungTKChinh,
			@NoiDungTKSua,
			@Id_DieuChinh,
			@NguoiTao
		)		
	END
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Delete]
-- Database: ECS_TQDT_KD_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, November 26, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Delete]
	@ID int
AS

DELETE FROM 
	[dbo].[t_KDT_NoiDungDieuChinhTKDetail]
WHERE
	[ID] = @ID

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]
-- Database: ECS_TQDT_KD_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, November 26, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_DeleteDynamic]
	@WhereCondition NVARCHAR(500)
AS

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 'DELETE FROM [dbo].[t_KDT_NoiDungDieuChinhTKDetail] WHERE ' + @WhereCondition

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Load]
-- Database: ECS_TQDT_KD_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, November 26, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_Load]
	@ID int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[NoiDungTKChinh],
	[NoiDungTKSua],
	[Id_DieuChinh],
	[NguoiTao]
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTKDetail]
WHERE
	[ID] = @ID
GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectDynamic]
-- Database: ECS_TQDT_KD_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, November 26, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectDynamic]
	@WhereCondition NVARCHAR(500),
	@OrderByExpression NVARCHAR(250) = NULL
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

DECLARE @SQL NVARCHAR(MAX)

SET @SQL = 
'SELECT 
	[ID],
	[NoiDungTKChinh],
	[NoiDungTKSua],
	[Id_DieuChinh],
	[NguoiTao]
FROM [dbo].[t_KDT_NoiDungDieuChinhTKDetail] 
WHERE ' + @WhereCondition

IF @OrderByExpression IS NOT NULL AND LEN(@OrderByExpression) > 0
BEGIN
	SET @SQL = @SQL + ' ORDER BY ' + @OrderByExpression
END

EXEC sp_executesql @SQL

GO

------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectAll]
-- Database: ECS_TQDT_KD_V4_UPGRADE
-- Author: Ngo Thanh Tung
-- Time created: Tuesday, November 26, 2013
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectAll]
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[NoiDungTKChinh],
	[NoiDungTKSua],
	[Id_DieuChinh],
	[NguoiTao]
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTKDetail]	

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh]    Script Date: 11/26/2013 10:00:41 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh]    Script Date: 11/26/2013 10:00:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


------------------------------------------------------------------------------------------------------------------------
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh]
-- Database: ECS.TQDT.SXXK
-- Author: Ngo Thanh Tung
-- Time created: 01 Tháng Chín 2010
------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_SelectBy_Id_DieuChinh]
	@Id_DieuChinh int
AS

SET NOCOUNT ON
SET TRANSACTION ISOLATION LEVEL READ COMMITTED

SELECT
	[ID],
	[NoiDungTKChinh],
	[NoiDungTKSua],
	[Id_DieuChinh],
	[NguoiTao]
FROM
	[dbo].[t_KDT_NoiDungDieuChinhTKDetail]
WHERE
	[Id_DieuChinh] = @Id_DieuChinh

GO


/****** Object:  StoredProcedure [dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate_ByThongTin]    Script Date: 11/26/2013 11:32:04 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate_ByThongTin]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate_ByThongTin]
GO

/****** Object:  StoredProcedure [dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate_ByThongTin]    Script Date: 11/26/2013 11:32:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
  
------------------------------------------------------------------------------------------------------------------------  
-- Stored procedure name: [dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate]  
-- Database: ECS_TQDT_KD_V4_UPGRADE  
-- Author: Ngo Thanh Tung  
-- Time created: Tuesday, November 26, 2013  
------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[p_KDT_NoiDungDieuChinhTKDetail_InsertUpdate_ByThongTin]  
 @ID int,  
 @NoiDungTKChinh nvarchar(max),  
 @NoiDungTKSua nvarchar(max),  
 @Id_DieuChinh int,  
 @NguoiTao nvarchar(255)  
AS  
IF EXISTS(SELECT [ID] FROM [dbo].[t_KDT_NoiDungDieuChinhTKDetail] WHERE Id_DieuChinh = @Id_DieuChinh and NoiDungTKChinh = @NoiDungTKChinh)  
 BEGIN  
  UPDATE  
   [dbo].[t_KDT_NoiDungDieuChinhTKDetail]   
  SET  
   [NoiDungTKChinh] = @NoiDungTKChinh,  
   [NoiDungTKSua] = @NoiDungTKSua,  
   [Id_DieuChinh] = @Id_DieuChinh,  
   [NguoiTao] = @NguoiTao  
  WHERE  
   [ID] = @ID  
 END  
ELSE  
 BEGIN  
    
  INSERT INTO [dbo].[t_KDT_NoiDungDieuChinhTKDetail]  
  (  
   [NoiDungTKChinh],  
   [NoiDungTKSua],  
   [Id_DieuChinh],  
   [NguoiTao]  
  )  
  VALUES   
  (  
   @NoiDungTKChinh,  
   @NoiDungTKSua,  
   @Id_DieuChinh,  
   @NguoiTao  
  )    
 END  
GO

IF (SELECT COUNT(*) FROM t_HaiQuan_Version WHERE [Version] = '12.9') = 0
BEGIN
	INSERT INTO dbo.t_HaiQuan_Version VALUES('12.9', GETDATE(), N'Bo sung cot Nguoi tao vao bang noi dung dieu chinh TK chi tiet, cap nhat store.')
END	
