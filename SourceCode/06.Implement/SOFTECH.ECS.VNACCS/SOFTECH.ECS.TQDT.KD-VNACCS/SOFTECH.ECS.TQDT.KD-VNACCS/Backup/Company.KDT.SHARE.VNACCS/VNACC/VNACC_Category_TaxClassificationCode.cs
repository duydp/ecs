using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;


namespace Company.KDT.SHARE.VNACCS
{
	public partial class VNACC_Category_TaxClassificationCode 
	{
        protected static List<VNACC_Category_TaxClassificationCode> ConvertToCollectionMinimize(IDataReader reader)
        {
            List<VNACC_Category_TaxClassificationCode> collection = new List<VNACC_Category_TaxClassificationCode>();
            while (reader.Read())
            {
                VNACC_Category_TaxClassificationCode entity = new VNACC_Category_TaxClassificationCode();
                if (!reader.IsDBNull(reader.GetOrdinal("TableID"))) entity.TableID = reader.GetString(reader.GetOrdinal("TableID"));
                if (!reader.IsDBNull(reader.GetOrdinal("TaxCode"))) entity.TaxCode = reader.GetString(reader.GetOrdinal("TaxCode"));
                if (!reader.IsDBNull(reader.GetOrdinal("TaxName"))) entity.TaxName = reader.GetString(reader.GetOrdinal("TaxName"));
                collection.Add(entity);
            }
            reader.Close();
            return collection;
        }

        public static List<VNACC_Category_TaxClassificationCode> SelectCollectionAllMinimize()
        {
            const string spName = "SELECT [TableID], [TaxCode], TaxName FROM [dbo].[t_VNACC_Category_TaxClassificationCode]";
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand)db.GetSqlStringCommand(spName);

            IDataReader reader = db.ExecuteReader(dbCommand);
            return ConvertToCollectionMinimize(reader);
        }

        public static List<VNACC_Category_TaxClassificationCode> SelectCollectionBy(List<VNACC_Category_TaxClassificationCode> collections, string tableID)
        {
            return collections.FindAll(delegate(VNACC_Category_TaxClassificationCode o)
            {
                return o.TableID.Contains(tableID);
            });
        }	
	}	
}