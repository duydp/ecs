﻿using System.ComponentModel;
using System.Windows.Forms;
using Company.Interface;
using Company.BLL;
using System;

namespace Company.Interface
{
	public class ChangePasswordHQForm : BaseForm
	{
		private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
		private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
		private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
		private Janus.Windows.GridEX.EditControls.EditBox txtUser;
		private Janus.Windows.GridEX.EditControls.EditBox txtMatKhau;
		private IContainer components = null;
		private Company.Controls.CustomValidation.RequiredFieldValidator rfvMa;
        private Company.Controls.CustomValidation.ContainerValidator cvError;
        private Janus.Windows.GridEX.EditControls.EditBox txtMatkhauMoi;
        private Label label3;
        private Janus.Windows.GridEX.EditControls.EditBox txtNhapLaiMK;
        private Label label4;
        private Janus.Windows.EditControls.UIButton btnClose;
        private Janus.Windows.EditControls.UIButton btnUpdate;
        private Company.Controls.CustomValidation.CompareValidator compareValidator1;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMatKhauCu;
        private Company.Controls.CustomValidation.RequiredFieldValidator rfvMatKhauMoi;
        private ErrorProvider errorProvider1;

		public bool IsLogin;
		public ChangePasswordHQForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
            txtUser.Text = GlobalSettings.MA_DON_VI;
			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePasswordHQForm));
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.btnClose = new Janus.Windows.EditControls.UIButton();
            this.btnUpdate = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtNhapLaiMK = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMatkhauMoi = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMatKhau = new Janus.Windows.GridEX.EditControls.EditBox();
            this.txtUser = new Janus.Windows.GridEX.EditControls.EditBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rfvMa = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.cvError = new Company.Controls.CustomValidation.ContainerValidator();
            this.compareValidator1 = new Company.Controls.CustomValidation.CompareValidator();
            this.rfvMatKhauCu = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.rfvMatKhauMoi = new Company.Controls.CustomValidation.RequiredFieldValidator();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.compareValidator1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMatKhauCu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMatKhauMoi)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Size = new System.Drawing.Size(311, 166);
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.uiGroupBox2.BackgroundStyle = Janus.Windows.EditControls.BackgroundStyle.Panel;
            this.uiGroupBox2.Controls.Add(this.btnClose);
            this.uiGroupBox2.Controls.Add(this.btnUpdate);
            this.uiGroupBox2.Controls.Add(this.uiGroupBox1);
            this.uiGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.uiGroupBox2.FrameStyle = Janus.Windows.EditControls.FrameStyle.None;
            this.uiGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(311, 166);
            this.uiGroupBox2.TabIndex = 22;
            this.uiGroupBox2.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // btnClose
            // 
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.Icon = ((System.Drawing.Icon)(resources.GetObject("btnClose.Icon")));
            this.btnClose.Location = new System.Drawing.Point(190, 136);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(64, 23);
            this.btnClose.TabIndex = 26;
            this.btnClose.Text = "&Đóng";
            this.btnClose.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnClose.VisualStyleManager = this.vsmMain;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.Icon = ((System.Drawing.Icon)(resources.GetObject("btnUpdate.Icon")));
            this.btnUpdate.Location = new System.Drawing.Point(126, 136);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(58, 23);
            this.btnUpdate.TabIndex = 25;
            this.btnUpdate.Text = "&Ghi";
            this.btnUpdate.VisualStyle = Janus.Windows.UI.VisualStyle.Office2007;
            this.btnUpdate.VisualStyleManager = this.vsmMain;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtNhapLaiMK);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtMatkhauMoi);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.txtMatKhau);
            this.uiGroupBox1.Controls.Add(this.txtUser);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Location = new System.Drawing.Point(5, 4);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(299, 127);
            this.uiGroupBox1.TabIndex = 23;
            this.uiGroupBox1.VisualStyle = Janus.Windows.UI.Dock.PanelVisualStyle.Office2007;
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtNhapLaiMK
            // 
            this.txtNhapLaiMK.Location = new System.Drawing.Point(121, 96);
            this.txtNhapLaiMK.Name = "txtNhapLaiMK";
            this.txtNhapLaiMK.PasswordChar = '*';
            this.txtNhapLaiMK.Size = new System.Drawing.Size(168, 21);
            this.txtNhapLaiMK.TabIndex = 24;
            this.txtNhapLaiMK.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtNhapLaiMK.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtNhapLaiMK.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(9, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 23;
            this.label4.Text = "Nhập lại mật khẩu mới";
            // 
            // txtMatkhauMoi
            // 
            this.txtMatkhauMoi.Location = new System.Drawing.Point(121, 69);
            this.txtMatkhauMoi.Name = "txtMatkhauMoi";
            this.txtMatkhauMoi.PasswordChar = '*';
            this.txtMatkhauMoi.Size = new System.Drawing.Size(168, 21);
            this.txtMatkhauMoi.TabIndex = 22;
            this.txtMatkhauMoi.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMatkhauMoi.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMatkhauMoi.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(9, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(70, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Mật khẩu mới";
            // 
            // txtMatKhau
            // 
            this.txtMatKhau.Location = new System.Drawing.Point(121, 42);
            this.txtMatKhau.Name = "txtMatKhau";
            this.txtMatKhau.PasswordChar = '*';
            this.txtMatKhau.Size = new System.Drawing.Size(168, 21);
            this.txtMatKhau.TabIndex = 19;
            this.txtMatKhau.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtMatKhau.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtMatKhau.VisualStyleManager = this.vsmMain;
            // 
            // txtUser
            // 
            this.txtUser.Location = new System.Drawing.Point(121, 15);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(168, 21);
            this.txtUser.TabIndex = 18;
            this.txtUser.TextAlignment = Janus.Windows.GridEX.TextAlignment.Near;
            this.txtUser.VisualStyle = Janus.Windows.GridEX.VisualStyle.Office2007;
            this.txtUser.VisualStyleManager = this.vsmMain;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(9, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "Tên đăng nhập";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(9, 47);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "Mật khẩu cũ";
            // 
            // rfvMa
            // 
            this.rfvMa.ControlToValidate = this.txtUser;
            this.rfvMa.ErrorMessage = "\"Tên đăng nhập\" bắt buộc phải nhập.";
            this.rfvMa.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMa.Icon")));
            // 
            // cvError
            // 
            this.cvError.ContainerToValidate = this;
            this.cvError.HostingForm = this;
            // 
            // compareValidator1
            // 
            this.compareValidator1.ControlToCompare = this.txtMatkhauMoi;
            this.compareValidator1.ControlToValidate = this.txtNhapLaiMK;
            this.compareValidator1.ErrorMessage = "\"Mật khẩu không giống nhau\"";
            this.compareValidator1.Icon = ((System.Drawing.Icon)(resources.GetObject("compareValidator1.Icon")));
            this.compareValidator1.Operator = Company.Controls.CustomValidation.ValidationCompareOperator.Equal;
            // 
            // rfvMatKhauCu
            // 
            this.rfvMatKhauCu.ControlToValidate = this.txtMatKhau;
            this.rfvMatKhauCu.ErrorMessage = "\"Mật khẩu cũ\" bắt buộc phải nhập.";
            this.rfvMatKhauCu.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMatKhauCu.Icon")));
            // 
            // rfvMatKhauMoi
            // 
            this.rfvMatKhauMoi.ControlToValidate = this.txtMatkhauMoi;
            this.rfvMatKhauMoi.ErrorMessage = "\"Mật khẩu mới\" bắt buộc phải nhập.";
            this.rfvMatKhauMoi.Icon = ((System.Drawing.Icon)(resources.GetObject("rfvMatKhauMoi.Icon")));
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // ChangePasswordHQForm
            // 
            this.AcceptButton = this.btnUpdate;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 14);
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(311, 166);
            this.Controls.Add(this.uiGroupBox2);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ChangePasswordHQForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Đổi mật khẩu kết nối với hải quan";
            this.Controls.SetChildIndex(this.grbMain, 0);
            this.Controls.SetChildIndex(this.uiGroupBox2, 0);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMa)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.compareValidator1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMatKhauCu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rfvMatKhauMoi)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		
        private void btnUpdate_Click(object sender, System.EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid)
                return;
            if (txtMatkhauMoi.Text.Trim().Length < 6)
            {
                errorProvider1.SetIconPadding(txtMatkhauMoi, -8);
                errorProvider1.SetError(txtMatkhauMoi, "Mật khẩu phải có 6 ký tự trở lên.");
                return;
            }          
            try
            {
                this.Cursor = Cursors.WaitCursor;
                WebServiceConnection.DoiMatKhau(txtUser.Text.Trim(), txtMatKhau.Text.Trim(), txtMatkhauMoi.Text.Trim(), GlobalSettings.MA_HAI_QUAN, GlobalSettings.MA_DON_VI);
                GlobalSettings.PassWordDT = "";
                this.Cursor = Cursors.Default;
                ShowMessage("Đổi mật khẩu thành công.", false);
                this.Close();
            }
            catch (Exception ex)
            {
                this.Cursor = Cursors.Default;
                ShowMessage("Lỗi : " + ex.Message,false);
            }
        }
	}
}