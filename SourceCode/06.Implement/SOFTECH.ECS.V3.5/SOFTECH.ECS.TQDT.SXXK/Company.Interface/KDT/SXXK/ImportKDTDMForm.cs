using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL;
using Janus.Windows.GridEX;
using Company.BLL.KDT.SXXK;
using GemBox.Spreadsheet;
using Company.Interface.CauHinh;
using Company.KDT.SHARE.Components;
namespace Company.Interface.KDT.SXXK
{
    public partial class ImportKDTDMForm : BaseForm
    {
        public DinhMucDangKy dmDangKy;
        public int FormatDMSD = GlobalSettings.SoThapPhan.DinhMuc;
        public int FormatTLHH = GlobalSettings.SoThapPhan.TLHH;
        public ImportKDTDMForm()
        {
            InitializeComponent();

        }

        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        private void ImportDMForm_Load(object sender, EventArgs e)
        {
            //GlobalSettings.KhoiTao_GiaTriMacDinh();
            //txtMaSPColumn.Text = GlobalSettings.DinhMuc.MaSP;
            //txtMaNPLColumn.Text = GlobalSettings.DinhMuc.MaNPL;
            //txtDMSDColumn.Text = GlobalSettings.DinhMuc.DinhMucSuDung;
            //txtTLHHColumn.Text = GlobalSettings.DinhMuc.TyLeHH;
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.ReadDefaultDM(cbbSheetName, txtRow, txtMaSPColumn, txtMaNPLColumn, txtDMSDColumn, txtTLHHColumn, null, txtMaDD, chkOverwrite);
                FormatDMSD = f.FormatDMSD;
                FormatTLHH = f.FormatTLHH;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SaveDefault()
        {
            try
            {
                FrmCauHinhThongSoReadExcel f = new FrmCauHinhThongSoReadExcel();
                f.SaveConfigDM(cbbSheetName.Text, txtRow.Text, txtMaSPColumn.Text, txtMaNPLColumn.Text, txtDMSDColumn.Text, txtTLHHColumn.Text, String.Empty, txtMaDD.Text, chkOverwrite.Checked);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void uiButton1_Click(object sender, EventArgs e)
        {
            try
            {
                cvError.Validate();
                if (!cvError.IsValid) return;
                int beginRow = Convert.ToInt32(txtRow.Value) - 1;
                if (beginRow < 0)
                {
                    error.SetError(txtRow, "DÒNG BẮT ĐẦU PHẢI LỚN HƠN 0");
                    error.SetIconPadding(txtRow, 8);
                    return;

                }
                this.Cursor = Cursors.WaitCursor;
                Workbook wb = new Workbook();
                Worksheet ws = null;
                try
                {
                    wb = Workbook.Load(txtFilePath.Text, true);
                }
                catch (Exception ex)
                {
                    MLMessages("LỖI KHI ĐỌC FILE. DOANH NGHIỆP HÃY KIỂM TRA LẠI ĐƯỜNG DẪN HOẶC ĐÓNG FILE TRƯỚC KHI ĐỌC.", "MSG_EXC03", "", false);
                    return;
                }
                try
                {
                    ws = wb.Worksheets[cbbSheetName.Text];
                }
                catch
                {
                    MLMessages("KHÔNG TỒN TẠI SHEET \"" + cbbSheetName.Text + "\"", "MSG_EXC01", cbbSheetName.Text, false);
                    return;
                }

                WorksheetRowCollection wsrc = ws.Rows;
                char maSPColumn = Convert.ToChar(txtMaSPColumn.Text);
                int maSPCol = ConvertCharToInt(maSPColumn);

                char maNPLColumn = Convert.ToChar(txtMaNPLColumn.Text);
                int maNPLCol = ConvertCharToInt(maNPLColumn);

                char DMSDColumn = Convert.ToChar(txtDMSDColumn.Text);
                int DMSDCol = ConvertCharToInt(DMSDColumn);

                char TLHHColumn = Convert.ToChar(txtTLHHColumn.Text);
                int TLHHCol = ConvertCharToInt(TLHHColumn);

                char MaDinhDanhColumn = Convert.ToChar(txtMaDD.Text);
                int MaDinhDanhCol = ConvertCharToInt(MaDinhDanhColumn);

                string errorTotal = "";
                string errorMaSanPham = "";
                string errorMaSanPhamSpecialChar = "";
                string errorMaSanPhamExits = "";
                string errorMaNguyenPhuLieu = "";
                string errorMaNguyenPhuLieuSpecialChar = "";
                string errorMaNguyenPhuLieuExits = "";
                string errorDMSD = "";
                string errorDMSDExits = "";
                string errorTLHH = "";
                string errorTLHHExits = "";
                string errorMaDinhDanh = "";
                string errorDinhMuc = "";
                string errorDinhMucExits = "";

                string errorNPL = "";
                string errorSP = "";
                string errorSPExits = "";
                DinhMucCollection DMCollectionAdd = new DinhMucCollection();
                bool isCheck = false;
                string MaNPLCheck = String.Empty;
                string MaSPCheck = String.Empty;
                NguyenPhuLieuCollection AllNPLCollection = NguyenPhuLieu.SelectCollectionAll();
                SanPhamCollection AllSPCollection = SanPham.SelectCollectionAll();
                DinhMucCollection AllDMCollection = DinhMuc.SelectCollectionAll();
                DinhMucDangKyCollection AllDMDKCollection = DinhMucDangKy.SelectCollectionDynamicAll("MaHaiQuan='" + GlobalSettings.MA_HAI_QUAN + "' AND MaDoanhNghiep='" + GlobalSettings.MA_DON_VI + "' AND TrangThaiXuLy NOT IN (10) ", "");
                List<Company.BLL.SXXK.SanPham> SPCollectionCheck = new List<Company.BLL.SXXK.SanPham>();
                List<Company.BLL.SXXK.NguyenPhuLieu> NPLCollectionCheck = new List<Company.BLL.SXXK.NguyenPhuLieu>();
                bool isAdd = true;
                bool isExits = false;
                foreach (WorksheetRow wsr in wsrc)
                {
                    if (wsr.Index >= beginRow)
                    {
                        try
                        {
                            DinhMuc dinhMuc = new DinhMuc();
                            if (wsr.Cells[maNPLCol].Value == null || Convert.ToString(wsr.Cells[maNPLCol].Value) == "") continue;
                            try
                            {
                                dinhMuc.MaSanPham = Convert.ToString(wsr.Cells[maSPCol].Value).Trim();
                                if (dinhMuc.MaSanPham.Length == 0)
                                {
                                    errorMaSanPham += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaSanPham + "]\n";
                                    isAdd = false;
                                }
                                else if (Helpers.ValidateSpecialChar(dinhMuc.MaSanPham))
                                {
                                    errorMaSanPhamSpecialChar += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaSanPham + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    //TẠO DANH SÁCH SẢN PHẨM ĐỂ KIỂM TRA ĐÃ ĐĂNG KÝ HAY CHƯA
                                    if (String.IsNullOrEmpty(MaSPCheck))
                                    {
                                        Company.BLL.SXXK.SanPham SP = new Company.BLL.SXXK.SanPham();
                                        SP.Ma = dinhMuc.MaSanPham;
                                        SP.STT = (wsr.Index + 1);
                                        SPCollectionCheck.Add(SP);
                                        MaSPCheck = dinhMuc.MaSanPham;
                                    }
                                    else if (MaSPCheck != dinhMuc.MaSanPham)
                                    {
                                        Company.BLL.SXXK.SanPham SP = new Company.BLL.SXXK.SanPham();
                                        SP.Ma = dinhMuc.MaSanPham;
                                        SP.STT = (wsr.Index + 1);
                                        SPCollectionCheck.Add(SP);
                                        MaSPCheck = dinhMuc.MaSanPham;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorMaSanPham += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaSanPham + "]\n";
                                isAdd = false;
                            }
                            try
                            {
                                dinhMuc.MaNguyenPhuLieu = Convert.ToString(wsr.Cells[maNPLCol].Value).Trim();
                                isExits = false;
                                if (dinhMuc.MaNguyenPhuLieu.Length == 0)
                                {
                                    errorMaNguyenPhuLieu += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNguyenPhuLieu + "]\n";
                                    isAdd = false;
                                }
                                else if (Helpers.ValidateSpecialChar(dinhMuc.MaNguyenPhuLieu))
                                {
                                    errorMaNguyenPhuLieuSpecialChar += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNguyenPhuLieu + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(MaNPLCheck))
                                    {
                                        Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                        NPL.Ma = dinhMuc.MaNguyenPhuLieu;
                                        NPL.STT = (wsr.Index + 1);
                                        NPLCollectionCheck.Add(NPL);
                                        MaNPLCheck = dinhMuc.MaNguyenPhuLieu;
                                    }
                                    else if (MaNPLCheck != dinhMuc.MaNguyenPhuLieu)
                                    {
                                        Company.BLL.SXXK.NguyenPhuLieu NPL = new Company.BLL.SXXK.NguyenPhuLieu();
                                        NPL.Ma = dinhMuc.MaNguyenPhuLieu;
                                        NPL.STT = (wsr.Index + 1);
                                        NPLCollectionCheck.Add(NPL);
                                        MaNPLCheck = dinhMuc.MaNguyenPhuLieu;
                                    }
                                }
                            }
                            catch (Exception)
                            {
                                errorMaNguyenPhuLieu += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.MaNguyenPhuLieu + "]\n";
                                isAdd = false;
                            }
                            string DinhMucSuDung = String.Empty;
                            try
                            {
                                DinhMucSuDung = wsr.Cells[DMSDCol].Value.ToString();
                                if (DinhMucSuDung.Length == 0)
                                {
                                    errorDMSD += "[" + (wsr.Index + 1) + "]-[" + DinhMucSuDung + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    dinhMuc.DinhMucSuDung = Convert.ToDecimal(wsr.Cells[DMSDCol].Value);
                                    if (Decimal.Round(dinhMuc.DinhMucSuDung, FormatDMSD) != dinhMuc.DinhMucSuDung)
                                    {
                                        errorDMSDExits += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.DinhMucSuDung + "]\n";
                                        isAdd = false;
                                    }

                                }
                            }
                            catch (Exception)
                            {
                                errorDMSD += "[" + (wsr.Index + 1) + "]-[" + DinhMucSuDung + "]\n";
                                isAdd = false;
                            }
                            String TLHH = String.Empty;
                            try
                            {
                                TLHH = wsr.Cells[TLHHCol].Value.ToString();
                                if (TLHH.Length == 0)
                                {
                                    errorTLHH += "[" + (wsr.Index + 1) + "]-[" + TLHH + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    dinhMuc.TyLeHaoHut = Convert.ToDecimal(wsr.Cells[TLHHCol].Value);
                                    if (Decimal.Round(dinhMuc.TyLeHaoHut, FormatTLHH) != dinhMuc.TyLeHaoHut)
                                    {
                                        errorTLHHExits += "[" + (wsr.Index + 1) + "]-[" + dinhMuc.TyLeHaoHut + "]\n";
                                        isAdd = false;
                                    }

                                }
                            }
                            catch (Exception)
                            {
                                errorTLHH += "[" + (wsr.Index + 1) + "]-[" + TLHH + "]\n";
                                isAdd = false;
                            }
                            String MaDinhDanh = String.Empty;
                            try
                            {
                                MaDinhDanh = wsr.Cells[MaDinhDanhCol].Value.ToString();
                                if (MaDinhDanh.Length == 0)
                                {
                                    errorMaDinhDanh += "[" + (wsr.Index + 1) + "]-[" + MaDinhDanh + "]\n";
                                    isAdd = false;
                                }
                                else
                                {
                                    dinhMuc.MaDinhDanhLenhSX = Convert.ToString(MaDinhDanh);
                                }
                            }
                            catch (Exception)
                            {
                                errorMaDinhDanh += "[" + (wsr.Index + 1) + "]-[" + MaDinhDanh + "]\n";
                                isAdd = false;
                            }
                            if (isAdd)
                                DMCollectionAdd.Add(dinhMuc);
                        }
                        catch (Exception ex)
                        {
                            this.SaveDefault();
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                            return;
                        }
                    }
                }
                //KIỂM TRA ĐỊNH MỨC ĐÃ ĐĂNG KÝ
                isExits = false;
                foreach (Company.BLL.SXXK.SanPham item in SPCollectionCheck)
                {
                    foreach (DinhMuc items in AllDMCollection)
                    {
                        if (item.Ma == items.MaSanPham)
                        {
                            isExits = true;
                            break;
                        }
                    }
                    if (isExits)
                    {
                        errorDinhMucExits += "[" + item.STT + "]-[" + item.Ma + "]\n";
                    }

                }
                //NẾU CHƯA ĐĂNG KÝ THÌ KIỂM TRA ĐỊNH MỨC TRÊN LƯỚI

                if (String.IsNullOrEmpty(errorDinhMucExits))
                {
                    String MaSP = String.Empty;
                    String MaNPL = String.Empty;
                    foreach (DinhMuc dinhMuc in DMCollectionAdd)
                    {
                        //NẾU KHÔNG GHI ĐÈ
                        isExits = false;
                        if (!chkOverwrite.Checked)
                        {
                            foreach (DinhMuc item in dmDangKy.DMCollection)
                            {
                                if (dinhMuc.MaSanPham.Trim().ToUpper() == item.MaSanPham.Trim().ToUpper() && dinhMuc.MaNguyenPhuLieu.Trim().ToUpper() == item.MaNguyenPhuLieu.Trim().ToUpper())
                                {
                                    isExits = true;
                                    break;
                                }
                            }
                            if (isExits)
                            {
                                if (MaSP != dinhMuc.MaSanPham && MaNPL != dinhMuc.MaNguyenPhuLieu)
                                {
                                    errorSPExits += "[" + dinhMuc.STT + "]-[" + dinhMuc.MaSanPham + "]\n";
                                }
                                MaSP = dinhMuc.MaSanPham;
                                MaNPL = dinhMuc.MaNguyenPhuLieu;
                                isAdd = false;
                            }
                        }
                        else
                        {
                            foreach (DinhMuc item in dmDangKy.DMCollection)
                            {
                                if (dinhMuc.MaSanPham.Trim().ToUpper() == item.MaSanPham.Trim().ToUpper() && dinhMuc.MaNguyenPhuLieu.Trim().ToUpper() == item.MaNguyenPhuLieu.Trim().ToUpper())
                                {
                                    dmDangKy.DMCollection.Remove(item);
                                    item.Delete();
                                    dmDangKy.DMCollection.Add(dinhMuc);
                                    break;
                                }
                            }
                        }
                    }
                    if (String.IsNullOrEmpty(errorSP))
                    {
                        //KIỂM TRA MÃ SP ĐÃ ĐƯỢC ĐĂNG KÝ HAY CHƯA
                        String MaSPTemp = String.Empty;
                        String MaNPLTemp = String.Empty;
                        foreach (Company.BLL.SXXK.SanPham item in SPCollectionCheck)
                        {
                            isExits = false;
                            foreach (SanPham items in AllSPCollection)
                            {
                                if (item.Ma == items.Ma)
                                {
                                    isExits = true;
                                    break;
                                }
                            }
                            if (!isExits)
                            {
                                if (MaSPTemp != item.Ma)
                                {
                                    errorSP += "[" + item.STT + "]-[" + item.Ma + "]\n";
                                }
                                MaSPTemp = item.Ma;
                            }
                        }
                        //KIỂM TRA MÃ NPL ĐÃ ĐƯỢC ĐĂNG KÝ HAY CHƯA
                        //foreach (Company.BLL.SXXK.NguyenPhuLieu item in NPLCollectionCheck)
                        //{
                        //    isExits = false;
                        //    foreach (NguyenPhuLieu items in AllNPLCollection)
                        //    {
                        //        if (item.Ma == items.Ma)
                        //        {
                        //            isExits = true;
                        //            break;
                        //        }
                        //    }
                        //    if (!isExits)
                        //    {
                        //        if (MaNPLTemp != item.Ma)
                        //        {
                        //            errorNPL += "[" + item.STT + "]-[" + item.Ma + "]\n";
                        //        }
                        //        MaNPLTemp = item.Ma;
                        //    }
                        //}
                    }
                }
                else
                {
                    //LẤY THÔNG TIN ĐỊNH MỨC ĐÃ NHẬP LIỆU TRƯỚC ĐÓ
                    errorDinhMucExits = String.Empty;
                    isExits = false;
                    String STT = String.Empty;
                    String MaSPTemp = String.Empty;
                    foreach (Company.BLL.SXXK.SanPham items in SPCollectionCheck)
                    {
                        foreach (DinhMucDangKy item in AllDMDKCollection)
                        {
                            if (chkOverwrite.Checked)
                            {
                                if (item.MaSanPham == items.Ma && item.ID != dmDangKy.ID)
                                {

                                    STT = items.STT.ToString();
                                    if (MaSPTemp != items.Ma)
                                    {
                                        errorDinhMuc += "[" + STT + "]-[" + item.MaSanPham + "]-[" + item.SoTiepNhan + "]-[" + item.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(item.TrangThaiXuLy).ToUpper() + "]\n";
                                    }
                                    MaSPTemp = items.Ma;
                                    break;
                                }
                            }
                            else
                            {
                                if (item.MaSanPham == items.Ma)
                                {
                                    STT = items.STT.ToString();
                                    if (MaSPTemp != items.Ma)
                                    {
                                        errorDinhMuc += "[" + STT + "]-[" + item.MaSanPham + "]-[" + item.SoTiepNhan + "]-[" + item.NgayTiepNhan.ToString("dd/MM/yyyy") + "]-[" + Globals.GetStatus(item.TrangThaiXuLy).ToUpper() + "]\n";
                                    }
                                    MaSPTemp = items.Ma;
                                    break;
                                }
                            }
                        }
                    }
                }
                if (!String.IsNullOrEmpty(errorMaSanPham))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorMaSanPham + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorMaNguyenPhuLieuSpecialChar))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorMaNguyenPhuLieuSpecialChar + " CHỨA CÁC KÝ TỰ ĐẶC BIỆT HOẶC UNICODE HOẶC XUỐNG DÒNG ";
                if (!String.IsNullOrEmpty(errorMaSanPhamExits))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorMaSanPhamExits + " CHƯA ĐƯỢC ĐĂNG KÝ\n";
                if (!String.IsNullOrEmpty(errorSPExits))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorSPExits + " ĐÃ ĐƯỢC NHẬP ĐỊNH MỨC TRÊN LƯỚI\n";
                if (!String.IsNullOrEmpty(errorMaNguyenPhuLieu))
                    errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorMaNguyenPhuLieu + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorMaNguyenPhuLieuSpecialChar))
                    errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorMaNguyenPhuLieuSpecialChar + " CHỨA CÁC KÝ TỰ ĐẶC BIỆT HOẶC UNICODE HOẶC XUỐNG DÒNG ";
                if (!String.IsNullOrEmpty(errorMaNguyenPhuLieuExits))
                    errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorMaNguyenPhuLieuExits + " CHƯA ĐƯỢC ĐĂNG KÝ\n";
                if (!String.IsNullOrEmpty(errorDMSD))
                    errorTotal += "\n - [STT] -[ĐỊNH MỨC SỬ DỤNG] : \n" + errorDMSD + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorDMSDExits))
                    errorTotal += "\n - [STT] -[ĐỊNH MỨC SỬ DỤNG] : \n" + errorDMSDExits + "DOANH NGHIỆP CẤU HÌNH CHỈ ĐƯỢC NHẬP TỐI ĐA " + FormatDMSD + " SỐ THẬP PHÂN . ĐỂ CẤU HÌNH : VÀO MENU HỆ THỐNG - CẤU HÌNH THÔNG SỐ ĐỌC EXCEL\n";
                if (!String.IsNullOrEmpty(errorTLHH))
                    errorTotal += "\n - [STT] -[TỶ LỆ HAO HỤT] : \n" + errorTLHH + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorTLHHExits))
                    errorTotal += "\n - [STT] -[TỶ LỆ HAO HỤT] : \n" + errorTLHHExits + " DOANH NGHIỆP CẤU HÌNH CHỈ ĐƯỢC NHẬP TỐI ĐA " + FormatTLHH + " SỐ THẬP PHÂN .ĐỂ CẤU HÌNH : VÀO MENU HỆ THỐNG - CẤU HÌNH THÔNG SỐ ĐỌC EXCEL\n";
                if (!String.IsNullOrEmpty(errorMaDinhDanh))
                    errorTotal += "\n - [STT] -[MÃ ĐỊNH DANH CỦA LỆNH SẢN XUẤT] : \n" + errorMaDinhDanh + " KHÔNG ĐƯỢC ĐỂ TRỐNG\n";
                if (!String.IsNullOrEmpty(errorDinhMuc))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM]-[SỐ TIẾP NHẬN]-[NGÀY TIẾP NHẬN]-[TRẠNG THÁI XỬ LÝ] : \n" + errorDinhMuc + " ĐÃ ĐƯỢC NHẬP LIỆU ĐỊNH MỨC TRƯỚC ĐÓ .\n";
                if (!String.IsNullOrEmpty(errorDinhMucExits))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorDinhMucExits + " ĐÃ ĐƯỢC KHAI BÁO ĐỊNH MỨC\n";
                if (!String.IsNullOrEmpty(errorNPL))
                    errorTotal += "\n - [STT] -[MÃ NGUYÊN PHỤ LIỆU] : \n" + errorNPL + " CHƯA ĐƯỢC ĐĂNG KÝ\n";
                if (!String.IsNullOrEmpty(errorSP))
                    errorTotal += "\n - [STT] -[MÃ SẢN PHẨM] : \n" + errorSP + " CHƯA ĐƯỢC ĐĂNG KÝ\n";

                if (String.IsNullOrEmpty(errorTotal))
                {
                    foreach (DinhMuc DM in DMCollectionAdd)
                    {
                        //LOẠI BỎ NHỮNG ĐỊNH MỨC ĐÃ NHẬP VỚI ĐỊNH MỨC SỬ DỤNG BẰNG 0
                        if (DM.DinhMucSuDung != 0)
                        {
                            dmDangKy.DMCollection.Add(DM);
                        }
                    }
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "NHẬP HÀNG TỪ EXCEL THÀNH CÔNG .", false);
                }
                else
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "NHẬP TỪ EXCEL KHÔNG THÀNH CÔNG .\r\n" + errorTotal, false);
                    return;
                }
                this.SaveDefault();
                this.Close();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
                this.Cursor = Cursors.Default;
            }
        }
        private void txtFilePath_ButtonClick(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
            txtFilePath.Text = openFileDialog1.FileName;
            cbbSheetName.DataSource = GetAllSheetName();
            cbbSheetName.SelectedIndex = 0;
        }
        private List<String> GetAllSheetName()
        {
            try
            {
                Workbook wb = new Workbook();
                try
                {
                    wb = Workbook.Load(txtFilePath.Text);
                }
                catch (Exception ex)
                {
                    MLMessages("LỖI KHI CHỌN FILE. DOANH NGHIỆP HÃY KIỂM TRA LẠI ĐƯỜNG DẪN HOẶC ĐÓNG FILE TRƯỚC KHI ĐỌC.", "MSG_EXC03", "", false);
                    Logger.LocalLogger.Instance().WriteMessage(ex);
                    return null;
                }
                List<String> Collection = new List<string>();
                foreach (Worksheet worksheet in wb.Worksheets)
                {
                    Collection.Add(worksheet.Name);
                }
                return Collection;
            }
            catch (Exception ex)
            {
                MLMessages("LỖI KHI ĐỌC SHEET. DOANH NGHIỆP HÃY KIỂM TRA LẠI TÊN SHEET TRƯỚC KHI CHỌN FILE.", "MSG_EXC03", "", false);
                Logger.LocalLogger.Instance().WriteMessage(ex);
                return null;
            }
        }
        private void dgList_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {

        }
        private void chIsFromVietnam_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void linkExcelMau_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                Company.KDT.SHARE.Components.Globals.CreateExcelTemplate_SXXK("DM");
            }
            catch (Exception ex) { Logger.LocalLogger.Instance().WriteMessage(ex); }
        }
    }
}