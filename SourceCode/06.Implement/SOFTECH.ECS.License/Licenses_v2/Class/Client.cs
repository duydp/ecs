﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;

namespace Activation.Server
{
    public partial class Client
    {
       	public static List<Client> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
            List<Client> collection = new List<Client>();

			SqlDataReader reader = (SqlDataReader) SelectReaderDynamic(whereCondition, orderByExpression);
			while (reader.Read())
			{
                Client entity = new Client();

                if (!reader.IsDBNull(reader.GetOrdinal("MaMay"))) entity.MaMay = reader.GetString(reader.GetOrdinal("MaMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("TenMay"))) entity.TenMay = reader.GetString(reader.GetOrdinal("TenMay"));
                if (!reader.IsDBNull(reader.GetOrdinal("CustomersID"))) entity.CustomersID = reader.GetString(reader.GetOrdinal("CustomersID"));
                if (!reader.IsDBNull(reader.GetOrdinal("OsVersion"))) entity.OsVersion = reader.GetString(reader.GetOrdinal("OsVersion"));

				collection.Add(entity);
			}
			
			reader.Close();
			return collection;			
		}
		
    }
}
