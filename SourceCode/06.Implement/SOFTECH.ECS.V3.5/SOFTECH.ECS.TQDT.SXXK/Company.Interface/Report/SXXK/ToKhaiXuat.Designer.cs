namespace Company.Interface.Report.SXXK
{
    partial class ToKhaiXuat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ToKhaiXuat));
            this.Detail = new DevExpress.XtraReports.UI.DetailBand();
            this.lblBanLuuHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoKienTrongLuong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenChungTu4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenChungTu3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenChungTu6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenChungTu5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanSao1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh2 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh4 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh6 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoBanChinh1 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoTiepNhan = new DevExpress.XtraReports.UI.XRLabel();
            this.xrTable1 = new DevExpress.XtraReports.UI.XRTable();
            this.xrTableRow1 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT1 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow2 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT2 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow3 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT3 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow4 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT4 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow5 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT5 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow6 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT6 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow7 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT7 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow8 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT8 = new DevExpress.XtraReports.UI.XRTableCell();
            this.xrTableRow9 = new DevExpress.XtraReports.UI.XRTableRow();
            this.TenHang9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.MaHS9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.Luong9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DVT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.DonGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.TriGiaNT9 = new DevExpress.XtraReports.UI.XRTableCell();
            this.lblMaDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel3 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoToKhai = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTongTriGiaNT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblPTTT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTyGiaTT = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgoaiTe = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDKGH = new DevExpress.XtraReports.UI.XRLabel();
            this.lblDiaDiemDoHang = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaNuoc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenNuoc = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHopDong = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayHHGiayPhep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDaiLyTTHQ = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNguoiUyThac = new DevExpress.XtraReports.UI.XRLabel();
            this.lblTenDoiTac = new DevExpress.XtraReports.UI.XRLabel();
            this.xrLabel5 = new DevExpress.XtraReports.UI.XRLabel();
            this.lblMaDoanhNghiep = new DevExpress.XtraReports.UI.XRLabel();
            this.lblSoPLTK = new DevExpress.XtraReports.UI.XRLabel();
            this.lblChiCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblCucHaiQuan = new DevExpress.XtraReports.UI.XRLabel();
            this.lblNgayDangKy = new DevExpress.XtraReports.UI.XRLabel();
            this.ptbImage = new DevExpress.XtraReports.UI.XRPictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            // 
            // Detail
            // 
            this.Detail.Controls.AddRange(new DevExpress.XtraReports.UI.XRControl[] {
            this.lblBanLuuHaiQuan,
            this.lblSoKienTrongLuong,
            this.lblTenChungTu4,
            this.lblTenChungTu3,
            this.lblTenChungTu6,
            this.lblTenChungTu5,
            this.lblSoBanSao6,
            this.lblSoBanSao5,
            this.lblSoBanSao4,
            this.lblSoBanSao3,
            this.lblSoBanSao2,
            this.lblSoBanSao1,
            this.lblSoBanChinh2,
            this.lblSoBanChinh3,
            this.lblSoBanChinh4,
            this.lblSoBanChinh5,
            this.lblSoBanChinh6,
            this.lblSoBanChinh1,
            this.lblSoTiepNhan,
            this.xrTable1,
            this.lblMaDiaDiemDoHang,
            this.xrLabel3,
            this.lblSoToKhai,
            this.lblMaDaiLyTTHQ,
            this.lblMaNguoiUyThac,
            this.lblTongTriGiaNT,
            this.lblPTTT,
            this.lblTyGiaTT,
            this.lblNgoaiTe,
            this.lblDKGH,
            this.lblDiaDiemDoHang,
            this.lblMaNuoc,
            this.lblTenNuoc,
            this.lblSoHopDong,
            this.lblNgayHHHopDong,
            this.lblNgayHopDong,
            this.lblSoGiayPhep,
            this.lblNgayGiayPhep,
            this.lblNgayHHGiayPhep,
            this.lblTenDaiLyTTHQ,
            this.lblTenDoanhNghiep,
            this.lblNguoiUyThac,
            this.lblTenDoiTac,
            this.xrLabel5,
            this.lblMaDoanhNghiep,
            this.lblSoPLTK,
            this.lblChiCucHaiQuan,
            this.lblCucHaiQuan,
            this.lblNgayDangKy,
            this.ptbImage});
            this.Detail.Height = 1118;
            this.Detail.Name = "Detail";
            // 
            // lblBanLuuHaiQuan
            // 
            this.lblBanLuuHaiQuan.BackColor = System.Drawing.Color.LavenderBlush;
            this.lblBanLuuHaiQuan.Font = new System.Drawing.Font("Times New Roman", 11F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.lblBanLuuHaiQuan.Location = new System.Drawing.Point(267, 38);
            this.lblBanLuuHaiQuan.Name = "lblBanLuuHaiQuan";
            this.lblBanLuuHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblBanLuuHaiQuan.ParentStyleUsing.UseBackColor = false;
            this.lblBanLuuHaiQuan.ParentStyleUsing.UseFont = false;
            this.lblBanLuuHaiQuan.Size = new System.Drawing.Size(283, 22);
            this.lblBanLuuHaiQuan.Text = "Bản lưu người khai Hải quan";
            this.lblBanLuuHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoKienTrongLuong
            // 
            this.lblSoKienTrongLuong.Location = new System.Drawing.Point(33, 883);
            this.lblSoKienTrongLuong.Name = "lblSoKienTrongLuong";
            this.lblSoKienTrongLuong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoKienTrongLuong.Size = new System.Drawing.Size(559, 25);
            this.lblSoKienTrongLuong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTenChungTu4
            // 
            this.lblTenChungTu4.Location = new System.Drawing.Point(50, 995);
            this.lblTenChungTu4.Name = "lblTenChungTu4";
            this.lblTenChungTu4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenChungTu4.Size = new System.Drawing.Size(108, 16);
            // 
            // lblTenChungTu3
            // 
            this.lblTenChungTu3.Location = new System.Drawing.Point(50, 975);
            this.lblTenChungTu3.Name = "lblTenChungTu3";
            this.lblTenChungTu3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenChungTu3.Size = new System.Drawing.Size(108, 17);
            // 
            // lblTenChungTu6
            // 
            this.lblTenChungTu6.Location = new System.Drawing.Point(50, 1035);
            this.lblTenChungTu6.Name = "lblTenChungTu6";
            this.lblTenChungTu6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenChungTu6.Size = new System.Drawing.Size(108, 17);
            // 
            // lblTenChungTu5
            // 
            this.lblTenChungTu5.Location = new System.Drawing.Point(50, 1015);
            this.lblTenChungTu5.Name = "lblTenChungTu5";
            this.lblTenChungTu5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenChungTu5.Size = new System.Drawing.Size(108, 17);
            // 
            // lblSoBanSao6
            // 
            this.lblSoBanSao6.Location = new System.Drawing.Point(308, 1035);
            this.lblSoBanSao6.Name = "lblSoBanSao6";
            this.lblSoBanSao6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao6.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanSao5
            // 
            this.lblSoBanSao5.Location = new System.Drawing.Point(308, 1015);
            this.lblSoBanSao5.Name = "lblSoBanSao5";
            this.lblSoBanSao5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao5.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanSao4
            // 
            this.lblSoBanSao4.Location = new System.Drawing.Point(308, 995);
            this.lblSoBanSao4.Name = "lblSoBanSao4";
            this.lblSoBanSao4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao4.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanSao3
            // 
            this.lblSoBanSao3.Location = new System.Drawing.Point(308, 975);
            this.lblSoBanSao3.Name = "lblSoBanSao3";
            this.lblSoBanSao3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao3.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanSao2
            // 
            this.lblSoBanSao2.Location = new System.Drawing.Point(308, 955);
            this.lblSoBanSao2.Name = "lblSoBanSao2";
            this.lblSoBanSao2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao2.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanSao1
            // 
            this.lblSoBanSao1.Location = new System.Drawing.Point(200, 935);
            this.lblSoBanSao1.Name = "lblSoBanSao1";
            this.lblSoBanSao1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanSao1.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanSao1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanChinh2
            // 
            this.lblSoBanChinh2.Location = new System.Drawing.Point(200, 955);
            this.lblSoBanChinh2.Name = "lblSoBanChinh2";
            this.lblSoBanChinh2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh2.Size = new System.Drawing.Size(58, 14);
            this.lblSoBanChinh2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanChinh3
            // 
            this.lblSoBanChinh3.Location = new System.Drawing.Point(200, 975);
            this.lblSoBanChinh3.Name = "lblSoBanChinh3";
            this.lblSoBanChinh3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh3.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanChinh3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanChinh4
            // 
            this.lblSoBanChinh4.Location = new System.Drawing.Point(200, 995);
            this.lblSoBanChinh4.Name = "lblSoBanChinh4";
            this.lblSoBanChinh4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh4.Size = new System.Drawing.Size(58, 20);
            this.lblSoBanChinh4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanChinh5
            // 
            this.lblSoBanChinh5.Location = new System.Drawing.Point(200, 1017);
            this.lblSoBanChinh5.Name = "lblSoBanChinh5";
            this.lblSoBanChinh5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh5.Size = new System.Drawing.Size(58, 15);
            this.lblSoBanChinh5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanChinh6
            // 
            this.lblSoBanChinh6.Location = new System.Drawing.Point(200, 1035);
            this.lblSoBanChinh6.Name = "lblSoBanChinh6";
            this.lblSoBanChinh6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh6.Size = new System.Drawing.Size(58, 19);
            this.lblSoBanChinh6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoBanChinh1
            // 
            this.lblSoBanChinh1.Location = new System.Drawing.Point(308, 935);
            this.lblSoBanChinh1.Name = "lblSoBanChinh1";
            this.lblSoBanChinh1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoBanChinh1.Size = new System.Drawing.Size(58, 17);
            this.lblSoBanChinh1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblSoTiepNhan
            // 
            this.lblSoTiepNhan.Font = new System.Drawing.Font("Times New Roman", 10F, System.Drawing.FontStyle.Bold);
            this.lblSoTiepNhan.Location = new System.Drawing.Point(650, 8);
            this.lblSoTiepNhan.Name = "lblSoTiepNhan";
            this.lblSoTiepNhan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoTiepNhan.ParentStyleUsing.UseFont = false;
            this.lblSoTiepNhan.Size = new System.Drawing.Size(142, 25);
            // 
            // xrTable1
            // 
            this.xrTable1.Location = new System.Drawing.Point(63, 567);
            this.xrTable1.Name = "xrTable1";
            this.xrTable1.Rows.AddRange(new DevExpress.XtraReports.UI.XRTableRow[] {
            this.xrTableRow1,
            this.xrTableRow2,
            this.xrTableRow3,
            this.xrTableRow4,
            this.xrTableRow5,
            this.xrTableRow6,
            this.xrTableRow7,
            this.xrTableRow8,
            this.xrTableRow9});
            this.xrTable1.Size = new System.Drawing.Size(725, 315);
            // 
            // xrTableRow1
            // 
            this.xrTableRow1.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang1,
            this.MaHS1,
            this.Luong1,
            this.DVT1,
            this.DonGiaNT1,
            this.TriGiaNT1});
            this.xrTableRow1.Name = "xrTableRow1";
            this.xrTableRow1.Size = new System.Drawing.Size(725, 35);
            // 
            // TenHang1
            // 
            this.TenHang1.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TenHang1.Location = new System.Drawing.Point(0, 0);
            this.TenHang1.Multiline = true;
            this.TenHang1.Name = "TenHang1";
            this.TenHang1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang1.ParentStyleUsing.UseFont = false;
            this.TenHang1.Size = new System.Drawing.Size(250, 35);
            this.TenHang1.Tag = "Tên hàng 1";
            this.TenHang1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang1.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS1
            // 
            this.MaHS1.Location = new System.Drawing.Point(250, 0);
            this.MaHS1.Name = "MaHS1";
            this.MaHS1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS1.Size = new System.Drawing.Size(105, 35);
            this.MaHS1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong1
            // 
            this.Luong1.Location = new System.Drawing.Point(355, 0);
            this.Luong1.Multiline = true;
            this.Luong1.Name = "Luong1";
            this.Luong1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong1.Size = new System.Drawing.Size(92, 35);
            this.Luong1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT1
            // 
            this.DVT1.Location = new System.Drawing.Point(447, 0);
            this.DVT1.Multiline = true;
            this.DVT1.Name = "DVT1";
            this.DVT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT1.Size = new System.Drawing.Size(84, 35);
            this.DVT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT1
            // 
            this.DonGiaNT1.Location = new System.Drawing.Point(531, 0);
            this.DonGiaNT1.Name = "DonGiaNT1";
            this.DonGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT1.Size = new System.Drawing.Size(85, 35);
            this.DonGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT1
            // 
            this.TriGiaNT1.Location = new System.Drawing.Point(616, 0);
            this.TriGiaNT1.Name = "TriGiaNT1";
            this.TriGiaNT1.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT1.Size = new System.Drawing.Size(109, 35);
            this.TriGiaNT1.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow2
            // 
            this.xrTableRow2.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang2,
            this.MaHS2,
            this.Luong2,
            this.DVT2,
            this.DonGiaNT2,
            this.TriGiaNT2});
            this.xrTableRow2.Name = "xrTableRow2";
            this.xrTableRow2.Size = new System.Drawing.Size(725, 35);
            // 
            // TenHang2
            // 
            this.TenHang2.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TenHang2.Location = new System.Drawing.Point(0, 0);
            this.TenHang2.Multiline = true;
            this.TenHang2.Name = "TenHang2";
            this.TenHang2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang2.ParentStyleUsing.UseFont = false;
            this.TenHang2.Size = new System.Drawing.Size(250, 35);
            this.TenHang2.Tag = "Tên hàng 2";
            this.TenHang2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang2.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS2
            // 
            this.MaHS2.Location = new System.Drawing.Point(250, 0);
            this.MaHS2.Name = "MaHS2";
            this.MaHS2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS2.Size = new System.Drawing.Size(105, 35);
            this.MaHS2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong2
            // 
            this.Luong2.Location = new System.Drawing.Point(355, 0);
            this.Luong2.Multiline = true;
            this.Luong2.Name = "Luong2";
            this.Luong2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong2.Size = new System.Drawing.Size(92, 35);
            this.Luong2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT2
            // 
            this.DVT2.Location = new System.Drawing.Point(447, 0);
            this.DVT2.Multiline = true;
            this.DVT2.Name = "DVT2";
            this.DVT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT2.Size = new System.Drawing.Size(84, 35);
            this.DVT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT2
            // 
            this.DonGiaNT2.Location = new System.Drawing.Point(531, 0);
            this.DonGiaNT2.Name = "DonGiaNT2";
            this.DonGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT2.Size = new System.Drawing.Size(85, 35);
            this.DonGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT2
            // 
            this.TriGiaNT2.Location = new System.Drawing.Point(616, 0);
            this.TriGiaNT2.Name = "TriGiaNT2";
            this.TriGiaNT2.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT2.Size = new System.Drawing.Size(109, 35);
            this.TriGiaNT2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow3
            // 
            this.xrTableRow3.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang3,
            this.MaHS3,
            this.Luong3,
            this.DVT3,
            this.DonGiaNT3,
            this.TriGiaNT3});
            this.xrTableRow3.Name = "xrTableRow3";
            this.xrTableRow3.Size = new System.Drawing.Size(725, 35);
            // 
            // TenHang3
            // 
            this.TenHang3.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TenHang3.Location = new System.Drawing.Point(0, 0);
            this.TenHang3.Multiline = true;
            this.TenHang3.Name = "TenHang3";
            this.TenHang3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang3.ParentStyleUsing.UseFont = false;
            this.TenHang3.Size = new System.Drawing.Size(250, 35);
            this.TenHang3.Tag = "Tên hàng 3";
            this.TenHang3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS3
            // 
            this.MaHS3.Location = new System.Drawing.Point(250, 0);
            this.MaHS3.Name = "MaHS3";
            this.MaHS3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS3.Size = new System.Drawing.Size(105, 35);
            this.MaHS3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong3
            // 
            this.Luong3.Location = new System.Drawing.Point(355, 0);
            this.Luong3.Multiline = true;
            this.Luong3.Name = "Luong3";
            this.Luong3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong3.Size = new System.Drawing.Size(92, 35);
            this.Luong3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT3
            // 
            this.DVT3.Location = new System.Drawing.Point(447, 0);
            this.DVT3.Multiline = true;
            this.DVT3.Name = "DVT3";
            this.DVT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT3.Size = new System.Drawing.Size(84, 35);
            this.DVT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT3
            // 
            this.DonGiaNT3.Location = new System.Drawing.Point(531, 0);
            this.DonGiaNT3.Name = "DonGiaNT3";
            this.DonGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT3.Size = new System.Drawing.Size(85, 35);
            this.DonGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT3
            // 
            this.TriGiaNT3.Location = new System.Drawing.Point(616, 0);
            this.TriGiaNT3.Name = "TriGiaNT3";
            this.TriGiaNT3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT3.Size = new System.Drawing.Size(109, 35);
            this.TriGiaNT3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow4
            // 
            this.xrTableRow4.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang4,
            this.MaHS4,
            this.Luong4,
            this.DVT4,
            this.DonGiaNT4,
            this.TriGiaNT4});
            this.xrTableRow4.Name = "xrTableRow4";
            this.xrTableRow4.Size = new System.Drawing.Size(725, 35);
            // 
            // TenHang4
            // 
            this.TenHang4.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TenHang4.Location = new System.Drawing.Point(0, 0);
            this.TenHang4.Multiline = true;
            this.TenHang4.Name = "TenHang4";
            this.TenHang4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang4.ParentStyleUsing.UseFont = false;
            this.TenHang4.Size = new System.Drawing.Size(250, 35);
            this.TenHang4.Tag = "Tên hàng 4";
            this.TenHang4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang4.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS4
            // 
            this.MaHS4.Location = new System.Drawing.Point(250, 0);
            this.MaHS4.Name = "MaHS4";
            this.MaHS4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS4.Size = new System.Drawing.Size(105, 35);
            this.MaHS4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong4
            // 
            this.Luong4.Location = new System.Drawing.Point(355, 0);
            this.Luong4.Multiline = true;
            this.Luong4.Name = "Luong4";
            this.Luong4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong4.Size = new System.Drawing.Size(92, 35);
            this.Luong4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT4
            // 
            this.DVT4.Location = new System.Drawing.Point(447, 0);
            this.DVT4.Multiline = true;
            this.DVT4.Name = "DVT4";
            this.DVT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT4.Size = new System.Drawing.Size(84, 35);
            this.DVT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT4
            // 
            this.DonGiaNT4.Location = new System.Drawing.Point(531, 0);
            this.DonGiaNT4.Name = "DonGiaNT4";
            this.DonGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT4.Size = new System.Drawing.Size(85, 35);
            this.DonGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT4
            // 
            this.TriGiaNT4.Location = new System.Drawing.Point(616, 0);
            this.TriGiaNT4.Name = "TriGiaNT4";
            this.TriGiaNT4.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT4.Size = new System.Drawing.Size(109, 35);
            this.TriGiaNT4.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow5
            // 
            this.xrTableRow5.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang5,
            this.MaHS5,
            this.Luong5,
            this.DVT5,
            this.DonGiaNT5,
            this.TriGiaNT5});
            this.xrTableRow5.Name = "xrTableRow5";
            this.xrTableRow5.Size = new System.Drawing.Size(725, 35);
            // 
            // TenHang5
            // 
            this.TenHang5.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TenHang5.Location = new System.Drawing.Point(0, 0);
            this.TenHang5.Multiline = true;
            this.TenHang5.Name = "TenHang5";
            this.TenHang5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang5.ParentStyleUsing.UseFont = false;
            this.TenHang5.Size = new System.Drawing.Size(250, 35);
            this.TenHang5.Tag = "Tên hàng 5";
            this.TenHang5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang5.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS5
            // 
            this.MaHS5.Location = new System.Drawing.Point(250, 0);
            this.MaHS5.Name = "MaHS5";
            this.MaHS5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS5.Size = new System.Drawing.Size(105, 35);
            this.MaHS5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong5
            // 
            this.Luong5.Location = new System.Drawing.Point(355, 0);
            this.Luong5.Multiline = true;
            this.Luong5.Name = "Luong5";
            this.Luong5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong5.Size = new System.Drawing.Size(92, 35);
            this.Luong5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT5
            // 
            this.DVT5.Location = new System.Drawing.Point(447, 0);
            this.DVT5.Multiline = true;
            this.DVT5.Name = "DVT5";
            this.DVT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT5.Size = new System.Drawing.Size(84, 35);
            this.DVT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT5
            // 
            this.DonGiaNT5.Location = new System.Drawing.Point(531, 0);
            this.DonGiaNT5.Name = "DonGiaNT5";
            this.DonGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT5.Size = new System.Drawing.Size(85, 35);
            this.DonGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT5
            // 
            this.TriGiaNT5.Location = new System.Drawing.Point(616, 0);
            this.TriGiaNT5.Name = "TriGiaNT5";
            this.TriGiaNT5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT5.Size = new System.Drawing.Size(109, 35);
            this.TriGiaNT5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow6
            // 
            this.xrTableRow6.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang6,
            this.MaHS6,
            this.Luong6,
            this.DVT6,
            this.DonGiaNT6,
            this.TriGiaNT6});
            this.xrTableRow6.Name = "xrTableRow6";
            this.xrTableRow6.Size = new System.Drawing.Size(725, 35);
            // 
            // TenHang6
            // 
            this.TenHang6.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TenHang6.Location = new System.Drawing.Point(0, 0);
            this.TenHang6.Multiline = true;
            this.TenHang6.Name = "TenHang6";
            this.TenHang6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang6.ParentStyleUsing.UseFont = false;
            this.TenHang6.Size = new System.Drawing.Size(250, 35);
            this.TenHang6.Tag = "Tên hàng 6";
            this.TenHang6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang6.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS6
            // 
            this.MaHS6.Location = new System.Drawing.Point(250, 0);
            this.MaHS6.Name = "MaHS6";
            this.MaHS6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS6.Size = new System.Drawing.Size(105, 35);
            this.MaHS6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong6
            // 
            this.Luong6.Location = new System.Drawing.Point(355, 0);
            this.Luong6.Multiline = true;
            this.Luong6.Name = "Luong6";
            this.Luong6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong6.Size = new System.Drawing.Size(92, 35);
            this.Luong6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT6
            // 
            this.DVT6.Location = new System.Drawing.Point(447, 0);
            this.DVT6.Multiline = true;
            this.DVT6.Name = "DVT6";
            this.DVT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT6.Size = new System.Drawing.Size(84, 35);
            this.DVT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT6
            // 
            this.DonGiaNT6.Location = new System.Drawing.Point(531, 0);
            this.DonGiaNT6.Name = "DonGiaNT6";
            this.DonGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT6.Size = new System.Drawing.Size(85, 35);
            this.DonGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT6
            // 
            this.TriGiaNT6.Location = new System.Drawing.Point(616, 0);
            this.TriGiaNT6.Name = "TriGiaNT6";
            this.TriGiaNT6.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT6.Size = new System.Drawing.Size(109, 35);
            this.TriGiaNT6.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow7
            // 
            this.xrTableRow7.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang7,
            this.MaHS7,
            this.Luong7,
            this.DVT7,
            this.DonGiaNT7,
            this.TriGiaNT7});
            this.xrTableRow7.Name = "xrTableRow7";
            this.xrTableRow7.Size = new System.Drawing.Size(725, 35);
            // 
            // TenHang7
            // 
            this.TenHang7.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TenHang7.Location = new System.Drawing.Point(0, 0);
            this.TenHang7.Multiline = true;
            this.TenHang7.Name = "TenHang7";
            this.TenHang7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang7.ParentStyleUsing.UseFont = false;
            this.TenHang7.Size = new System.Drawing.Size(250, 35);
            this.TenHang7.Tag = "Tên hàng 7";
            this.TenHang7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang7.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS7
            // 
            this.MaHS7.Location = new System.Drawing.Point(250, 0);
            this.MaHS7.Name = "MaHS7";
            this.MaHS7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS7.Size = new System.Drawing.Size(105, 35);
            this.MaHS7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong7
            // 
            this.Luong7.Location = new System.Drawing.Point(355, 0);
            this.Luong7.Multiline = true;
            this.Luong7.Name = "Luong7";
            this.Luong7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong7.Size = new System.Drawing.Size(92, 35);
            this.Luong7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT7
            // 
            this.DVT7.Location = new System.Drawing.Point(447, 0);
            this.DVT7.Multiline = true;
            this.DVT7.Name = "DVT7";
            this.DVT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT7.Size = new System.Drawing.Size(84, 35);
            this.DVT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT7
            // 
            this.DonGiaNT7.Location = new System.Drawing.Point(531, 0);
            this.DonGiaNT7.Name = "DonGiaNT7";
            this.DonGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT7.Size = new System.Drawing.Size(85, 35);
            this.DonGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT7
            // 
            this.TriGiaNT7.Location = new System.Drawing.Point(616, 0);
            this.TriGiaNT7.Name = "TriGiaNT7";
            this.TriGiaNT7.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT7.Size = new System.Drawing.Size(109, 35);
            this.TriGiaNT7.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow8
            // 
            this.xrTableRow8.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang8,
            this.MaHS8,
            this.Luong8,
            this.DVT8,
            this.DonGiaNT8,
            this.TriGiaNT8});
            this.xrTableRow8.Name = "xrTableRow8";
            this.xrTableRow8.Size = new System.Drawing.Size(725, 35);
            // 
            // TenHang8
            // 
            this.TenHang8.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TenHang8.Location = new System.Drawing.Point(0, 0);
            this.TenHang8.Multiline = true;
            this.TenHang8.Name = "TenHang8";
            this.TenHang8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang8.ParentStyleUsing.UseFont = false;
            this.TenHang8.Size = new System.Drawing.Size(250, 35);
            this.TenHang8.Tag = "Tên hàng 8";
            this.TenHang8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang8.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS8
            // 
            this.MaHS8.Location = new System.Drawing.Point(250, 0);
            this.MaHS8.Name = "MaHS8";
            this.MaHS8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS8.Size = new System.Drawing.Size(105, 35);
            this.MaHS8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong8
            // 
            this.Luong8.Location = new System.Drawing.Point(355, 0);
            this.Luong8.Multiline = true;
            this.Luong8.Name = "Luong8";
            this.Luong8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong8.Size = new System.Drawing.Size(92, 35);
            this.Luong8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT8
            // 
            this.DVT8.Location = new System.Drawing.Point(447, 0);
            this.DVT8.Multiline = true;
            this.DVT8.Name = "DVT8";
            this.DVT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT8.Size = new System.Drawing.Size(84, 35);
            this.DVT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT8
            // 
            this.DonGiaNT8.Location = new System.Drawing.Point(531, 0);
            this.DonGiaNT8.Name = "DonGiaNT8";
            this.DonGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT8.Size = new System.Drawing.Size(85, 35);
            this.DonGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT8
            // 
            this.TriGiaNT8.Location = new System.Drawing.Point(616, 0);
            this.TriGiaNT8.Name = "TriGiaNT8";
            this.TriGiaNT8.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT8.Size = new System.Drawing.Size(109, 35);
            this.TriGiaNT8.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // xrTableRow9
            // 
            this.xrTableRow9.Cells.AddRange(new DevExpress.XtraReports.UI.XRTableCell[] {
            this.TenHang9,
            this.MaHS9,
            this.Luong9,
            this.DVT9,
            this.DonGiaNT9,
            this.TriGiaNT9});
            this.xrTableRow9.Name = "xrTableRow9";
            this.xrTableRow9.Size = new System.Drawing.Size(725, 35);
            // 
            // TenHang9
            // 
            this.TenHang9.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.TenHang9.Location = new System.Drawing.Point(0, 0);
            this.TenHang9.Multiline = true;
            this.TenHang9.Name = "TenHang9";
            this.TenHang9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TenHang9.ParentStyleUsing.UseFont = false;
            this.TenHang9.Size = new System.Drawing.Size(250, 35);
            this.TenHang9.Tag = "Tên hàng 9";
            this.TenHang9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.TenHang9.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // MaHS9
            // 
            this.MaHS9.Location = new System.Drawing.Point(250, 0);
            this.MaHS9.Name = "MaHS9";
            this.MaHS9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.MaHS9.Size = new System.Drawing.Size(105, 35);
            this.MaHS9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // Luong9
            // 
            this.Luong9.Location = new System.Drawing.Point(355, 0);
            this.Luong9.Multiline = true;
            this.Luong9.Name = "Luong9";
            this.Luong9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.Luong9.Size = new System.Drawing.Size(92, 35);
            this.Luong9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // DVT9
            // 
            this.DVT9.Location = new System.Drawing.Point(447, 0);
            this.DVT9.Multiline = true;
            this.DVT9.Name = "DVT9";
            this.DVT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DVT9.Size = new System.Drawing.Size(84, 35);
            this.DVT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // DonGiaNT9
            // 
            this.DonGiaNT9.Location = new System.Drawing.Point(531, 0);
            this.DonGiaNT9.Name = "DonGiaNT9";
            this.DonGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.DonGiaNT9.Size = new System.Drawing.Size(85, 35);
            this.DonGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // TriGiaNT9
            // 
            this.TriGiaNT9.Location = new System.Drawing.Point(616, 0);
            this.TriGiaNT9.Name = "TriGiaNT9";
            this.TriGiaNT9.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.TriGiaNT9.Size = new System.Drawing.Size(109, 35);
            this.TriGiaNT9.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblMaDiaDiemDoHang
            // 
            this.lblMaDiaDiemDoHang.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDiaDiemDoHang.Location = new System.Drawing.Point(475, 408);
            this.lblMaDiaDiemDoHang.Multiline = true;
            this.lblMaDiaDiemDoHang.Name = "lblMaDiaDiemDoHang";
            this.lblMaDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(4, 2, 0, 0, 100F);
            this.lblMaDiaDiemDoHang.ParentStyleUsing.UseFont = false;
            this.lblMaDiaDiemDoHang.Size = new System.Drawing.Size(133, 23);
            // 
            // xrLabel3
            // 
            this.xrLabel3.Font = new System.Drawing.Font("Times New Roman", 8.5F);
            this.xrLabel3.Location = new System.Drawing.Point(450, 70);
            this.xrLabel3.Name = "xrLabel3";
            this.xrLabel3.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel3.ParentStyleUsing.UseFont = false;
            this.xrLabel3.Size = new System.Drawing.Size(91, 16);
            this.xrLabel3.Tag = "Mã loại hình";
            this.xrLabel3.Text = "SXXK";
            this.xrLabel3.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            this.xrLabel3.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // lblSoToKhai
            // 
            this.lblSoToKhai.Font = new System.Drawing.Font("Times New Roman", 9F);
            this.lblSoToKhai.Location = new System.Drawing.Point(358, 70);
            this.lblSoToKhai.Name = "lblSoToKhai";
            this.lblSoToKhai.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoToKhai.ParentStyleUsing.UseFont = false;
            this.lblSoToKhai.Size = new System.Drawing.Size(58, 16);
            this.lblSoToKhai.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaDaiLyTTHQ
            // 
            this.lblMaDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDaiLyTTHQ.Location = new System.Drawing.Point(150, 433);
            this.lblMaDaiLyTTHQ.Name = "lblMaDaiLyTTHQ";
            this.lblMaDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblMaDaiLyTTHQ.ParentStyleUsing.UseFont = false;
            this.lblMaDaiLyTTHQ.Size = new System.Drawing.Size(266, 24);
            this.lblMaDaiLyTTHQ.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblMaNguoiUyThac
            // 
            this.lblMaNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaNguoiUyThac.Location = new System.Drawing.Point(158, 350);
            this.lblMaNguoiUyThac.Name = "lblMaNguoiUyThac";
            this.lblMaNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 2, 0, 0, 100F);
            this.lblMaNguoiUyThac.ParentStyleUsing.UseFont = false;
            this.lblMaNguoiUyThac.Size = new System.Drawing.Size(259, 24);
            this.lblMaNguoiUyThac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTongTriGiaNT
            // 
            this.lblTongTriGiaNT.Location = new System.Drawing.Point(683, 883);
            this.lblTongTriGiaNT.Name = "lblTongTriGiaNT";
            this.lblTongTriGiaNT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTongTriGiaNT.Size = new System.Drawing.Size(105, 25);
            this.lblTongTriGiaNT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleRight;
            // 
            // lblPTTT
            // 
            this.lblPTTT.Location = new System.Drawing.Point(617, 458);
            this.lblPTTT.Multiline = true;
            this.lblPTTT.Name = "lblPTTT";
            this.lblPTTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblPTTT.Size = new System.Drawing.Size(166, 59);
            this.lblPTTT.Tag = "PTTT";
            this.lblPTTT.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblPTTT.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.lblPTTT_PreviewClick);
            // 
            // lblTyGiaTT
            // 
            this.lblTyGiaTT.Location = new System.Drawing.Point(483, 492);
            this.lblTyGiaTT.Name = "lblTyGiaTT";
            this.lblTyGiaTT.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTyGiaTT.Size = new System.Drawing.Size(125, 23);
            // 
            // lblNgoaiTe
            // 
            this.lblNgoaiTe.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblNgoaiTe.Location = new System.Drawing.Point(515, 435);
            this.lblNgoaiTe.Multiline = true;
            this.lblNgoaiTe.Name = "lblNgoaiTe";
            this.lblNgoaiTe.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgoaiTe.ParentStyleUsing.UseFont = false;
            this.lblNgoaiTe.Size = new System.Drawing.Size(91, 23);
            // 
            // lblDKGH
            // 
            this.lblDKGH.Location = new System.Drawing.Point(617, 375);
            this.lblDKGH.Multiline = true;
            this.lblDKGH.Name = "lblDKGH";
            this.lblDKGH.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDKGH.Size = new System.Drawing.Size(166, 50);
            this.lblDKGH.Tag = "DKGH";
            this.lblDKGH.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.lblDKGH.PreviewClick += new DevExpress.XtraReports.UI.PreviewMouseEventHandler(this.TenHang1_PreviewClick);
            // 
            // lblDiaDiemDoHang
            // 
            this.lblDiaDiemDoHang.Location = new System.Drawing.Point(425, 367);
            this.lblDiaDiemDoHang.Multiline = true;
            this.lblDiaDiemDoHang.Name = "lblDiaDiemDoHang";
            this.lblDiaDiemDoHang.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblDiaDiemDoHang.Size = new System.Drawing.Size(183, 41);
            this.lblDiaDiemDoHang.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblMaNuoc
            // 
            this.lblMaNuoc.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaNuoc.Location = new System.Drawing.Point(725, 325);
            this.lblMaNuoc.Name = "lblMaNuoc";
            this.lblMaNuoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(6, 2, 0, 0, 100F);
            this.lblMaNuoc.ParentStyleUsing.UseFont = false;
            this.lblMaNuoc.Size = new System.Drawing.Size(67, 23);
            // 
            // lblTenNuoc
            // 
            this.lblTenNuoc.Location = new System.Drawing.Point(617, 283);
            this.lblTenNuoc.Name = "lblTenNuoc";
            this.lblTenNuoc.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenNuoc.Size = new System.Drawing.Size(175, 41);
            this.lblTenNuoc.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblSoHopDong
            // 
            this.lblSoHopDong.Font = new System.Drawing.Font("Times New Roman", 8F);
            this.lblSoHopDong.Location = new System.Drawing.Point(492, 267);
            this.lblSoHopDong.Multiline = true;
            this.lblSoHopDong.Name = "lblSoHopDong";
            this.lblSoHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoHopDong.ParentStyleUsing.UseFont = false;
            this.lblSoHopDong.Size = new System.Drawing.Size(116, 43);
            this.lblSoHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNgayHHHopDong
            // 
            this.lblNgayHHHopDong.Location = new System.Drawing.Point(500, 325);
            this.lblNgayHHHopDong.Name = "lblNgayHHHopDong";
            this.lblNgayHHHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHHopDong.Size = new System.Drawing.Size(66, 17);
            this.lblNgayHHHopDong.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayHopDong
            // 
            this.lblNgayHopDong.Location = new System.Drawing.Point(467, 308);
            this.lblNgayHopDong.Name = "lblNgayHopDong";
            this.lblNgayHopDong.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHopDong.Size = new System.Drawing.Size(83, 16);
            // 
            // lblSoGiayPhep
            // 
            this.lblSoGiayPhep.Location = new System.Drawing.Point(658, 200);
            this.lblSoGiayPhep.Name = "lblSoGiayPhep";
            this.lblSoGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoGiayPhep.ParentStyleUsing.UseFont = false;
            this.lblSoGiayPhep.Size = new System.Drawing.Size(134, 27);
            // 
            // lblNgayGiayPhep
            // 
            this.lblNgayGiayPhep.Location = new System.Drawing.Point(658, 225);
            this.lblNgayGiayPhep.Name = "lblNgayGiayPhep";
            this.lblNgayGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayGiayPhep.Size = new System.Drawing.Size(91, 17);
            // 
            // lblNgayHHGiayPhep
            // 
            this.lblNgayHHGiayPhep.Location = new System.Drawing.Point(700, 242);
            this.lblNgayHHGiayPhep.Name = "lblNgayHHGiayPhep";
            this.lblNgayHHGiayPhep.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayHHGiayPhep.Size = new System.Drawing.Size(75, 17);
            this.lblNgayHHGiayPhep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblTenDaiLyTTHQ
            // 
            this.lblTenDaiLyTTHQ.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblTenDaiLyTTHQ.Location = new System.Drawing.Point(33, 475);
            this.lblTenDaiLyTTHQ.Name = "lblTenDaiLyTTHQ";
            this.lblTenDaiLyTTHQ.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblTenDaiLyTTHQ.ParentStyleUsing.UseFont = false;
            this.lblTenDaiLyTTHQ.Size = new System.Drawing.Size(384, 41);
            // 
            // lblTenDoanhNghiep
            // 
            this.lblTenDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoanhNghiep.Location = new System.Drawing.Point(33, 200);
            this.lblTenDoanhNghiep.Multiline = true;
            this.lblTenDoanhNghiep.Name = "lblTenDoanhNghiep";
            this.lblTenDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblTenDoanhNghiep.Size = new System.Drawing.Size(384, 58);
            this.lblTenDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // lblNguoiUyThac
            // 
            this.lblNguoiUyThac.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.lblNguoiUyThac.Location = new System.Drawing.Point(33, 375);
            this.lblNguoiUyThac.Name = "lblNguoiUyThac";
            this.lblNguoiUyThac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblNguoiUyThac.ParentStyleUsing.UseFont = false;
            this.lblNguoiUyThac.Size = new System.Drawing.Size(384, 58);
            this.lblNguoiUyThac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblTenDoiTac
            // 
            this.lblTenDoiTac.Font = new System.Drawing.Font("Times New Roman", 10F);
            this.lblTenDoiTac.Location = new System.Drawing.Point(33, 292);
            this.lblTenDoiTac.Multiline = true;
            this.lblTenDoiTac.Name = "lblTenDoiTac";
            this.lblTenDoiTac.Padding = new DevExpress.XtraPrinting.PaddingInfo(1, 2, 0, 0, 100F);
            this.lblTenDoiTac.ParentStyleUsing.UseFont = false;
            this.lblTenDoiTac.Size = new System.Drawing.Size(384, 50);
            this.lblTenDoiTac.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleCenter;
            // 
            // xrLabel5
            // 
            this.xrLabel5.Font = new System.Drawing.Font("Times New Roman", 14F, System.Drawing.FontStyle.Bold);
            this.xrLabel5.Location = new System.Drawing.Point(488, 230);
            this.xrLabel5.Name = "xrLabel5";
            this.xrLabel5.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.xrLabel5.ParentStyleUsing.UseFont = false;
            this.xrLabel5.Size = new System.Drawing.Size(17, 16);
            this.xrLabel5.Text = "×";
            this.xrLabel5.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            this.xrLabel5.Visible = false;
            // 
            // lblMaDoanhNghiep
            // 
            this.lblMaDoanhNghiep.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold);
            this.lblMaDoanhNghiep.Location = new System.Drawing.Point(158, 175);
            this.lblMaDoanhNghiep.Name = "lblMaDoanhNghiep";
            this.lblMaDoanhNghiep.Padding = new DevExpress.XtraPrinting.PaddingInfo(3, 2, 0, 0, 100F);
            this.lblMaDoanhNghiep.ParentStyleUsing.UseFont = false;
            this.lblMaDoanhNghiep.Size = new System.Drawing.Size(275, 24);
            this.lblMaDoanhNghiep.TextAlignment = DevExpress.XtraPrinting.TextAlignment.MiddleLeft;
            // 
            // lblSoPLTK
            // 
            this.lblSoPLTK.Location = new System.Drawing.Point(433, 125);
            this.lblSoPLTK.Name = "lblSoPLTK";
            this.lblSoPLTK.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblSoPLTK.Size = new System.Drawing.Size(33, 16);
            this.lblSoPLTK.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblChiCucHaiQuan
            // 
            this.lblChiCucHaiQuan.Location = new System.Drawing.Point(133, 117);
            this.lblChiCucHaiQuan.Multiline = true;
            this.lblChiCucHaiQuan.Name = "lblChiCucHaiQuan";
            this.lblChiCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblChiCucHaiQuan.Size = new System.Drawing.Size(134, 18);
            this.lblChiCucHaiQuan.Text = "\r\n";
            this.lblChiCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblCucHaiQuan
            // 
            this.lblCucHaiQuan.Location = new System.Drawing.Point(108, 92);
            this.lblCucHaiQuan.Name = "lblCucHaiQuan";
            this.lblCucHaiQuan.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblCucHaiQuan.Size = new System.Drawing.Size(150, 16);
            this.lblCucHaiQuan.Text = "THÀNH PHỐ ĐÀ NẴNG";
            this.lblCucHaiQuan.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter;
            // 
            // lblNgayDangKy
            // 
            this.lblNgayDangKy.Location = new System.Drawing.Point(375, 100);
            this.lblNgayDangKy.Name = "lblNgayDangKy";
            this.lblNgayDangKy.Padding = new DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100F);
            this.lblNgayDangKy.Size = new System.Drawing.Size(75, 16);
            // 
            // ptbImage
            // 
            this.ptbImage.Image = ((System.Drawing.Image)(resources.GetObject("ptbImage.Image")));
            this.ptbImage.Location = new System.Drawing.Point(0, 0);
            this.ptbImage.Name = "ptbImage";
            this.ptbImage.Size = new System.Drawing.Size(811, 1110);
            this.ptbImage.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage;
            // 
            // ToKhaiXuat
            // 
            this.Bands.AddRange(new DevExpress.XtraReports.UI.Band[] {
            this.Detail});
            this.Margins = new System.Drawing.Printing.Margins(0, 6, 9, 19);
            this.PageHeight = 1169;
            this.PageWidth = 827;
            this.PaperKind = System.Drawing.Printing.PaperKind.A4;
            ((System.ComponentModel.ISupportInitialize)(this.xrTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();

        }

        #endregion

        private DevExpress.XtraReports.UI.DetailBand Detail;
        private DevExpress.XtraReports.UI.XRLabel lblCucHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblNgayDangKy;
        private DevExpress.XtraReports.UI.XRLabel lblChiCucHaiQuan;
        private DevExpress.XtraReports.UI.XRLabel lblMaDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblSoPLTK;
        private DevExpress.XtraReports.UI.XRLabel xrLabel5;
        private DevExpress.XtraReports.UI.XRLabel lblNguoiUyThac;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoiTac;
        private DevExpress.XtraReports.UI.XRLabel lblTenDoanhNghiep;
        private DevExpress.XtraReports.UI.XRLabel lblTenDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRLabel lblSoHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHopDong;
        private DevExpress.XtraReports.UI.XRLabel lblSoGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblNgayHHGiayPhep;
        private DevExpress.XtraReports.UI.XRLabel lblMaNuoc;
        private DevExpress.XtraReports.UI.XRLabel lblTenNuoc;
        private DevExpress.XtraReports.UI.XRLabel lblDiaDiemDoHang;
        private DevExpress.XtraReports.UI.XRLabel lblNgoaiTe;
        private DevExpress.XtraReports.UI.XRLabel lblDKGH;
        private DevExpress.XtraReports.UI.XRLabel lblPTTT;
        private DevExpress.XtraReports.UI.XRLabel lblTyGiaTT;
        private DevExpress.XtraReports.UI.XRLabel lblTongTriGiaNT;
        private DevExpress.XtraReports.UI.XRLabel lblMaDaiLyTTHQ;
        private DevExpress.XtraReports.UI.XRLabel lblMaNguoiUyThac;
        private DevExpress.XtraReports.UI.XRLabel xrLabel3;
        private DevExpress.XtraReports.UI.XRLabel lblSoToKhai;
        private DevExpress.XtraReports.UI.XRLabel lblMaDiaDiemDoHang;
        public DevExpress.XtraReports.UI.XRPictureBox ptbImage;
        private DevExpress.XtraReports.UI.XRTable xrTable1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow1;
        private DevExpress.XtraReports.UI.XRTableCell TenHang1;
        private DevExpress.XtraReports.UI.XRTableCell MaHS1;
        private DevExpress.XtraReports.UI.XRTableCell Luong1;
        private DevExpress.XtraReports.UI.XRTableCell DVT1;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT1;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT1;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow2;
        private DevExpress.XtraReports.UI.XRTableCell TenHang2;
        private DevExpress.XtraReports.UI.XRTableCell MaHS2;
        private DevExpress.XtraReports.UI.XRTableCell Luong2;
        private DevExpress.XtraReports.UI.XRTableCell DVT2;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT2;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT2;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow3;
        private DevExpress.XtraReports.UI.XRTableCell TenHang3;
        private DevExpress.XtraReports.UI.XRTableCell MaHS3;
        private DevExpress.XtraReports.UI.XRTableCell Luong3;
        private DevExpress.XtraReports.UI.XRTableCell DVT3;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT3;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT3;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow4;
        private DevExpress.XtraReports.UI.XRTableCell TenHang4;
        private DevExpress.XtraReports.UI.XRTableCell MaHS4;
        private DevExpress.XtraReports.UI.XRTableCell Luong4;
        private DevExpress.XtraReports.UI.XRTableCell DVT4;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT4;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT4;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow5;
        private DevExpress.XtraReports.UI.XRTableCell TenHang5;
        private DevExpress.XtraReports.UI.XRTableCell MaHS5;
        private DevExpress.XtraReports.UI.XRTableCell Luong5;
        private DevExpress.XtraReports.UI.XRTableCell DVT5;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT5;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT5;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow6;
        private DevExpress.XtraReports.UI.XRTableCell TenHang6;
        private DevExpress.XtraReports.UI.XRTableCell MaHS6;
        private DevExpress.XtraReports.UI.XRTableCell Luong6;
        private DevExpress.XtraReports.UI.XRTableCell DVT6;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT6;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT6;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow7;
        private DevExpress.XtraReports.UI.XRTableCell TenHang7;
        private DevExpress.XtraReports.UI.XRTableCell MaHS7;
        private DevExpress.XtraReports.UI.XRTableCell Luong7;
        private DevExpress.XtraReports.UI.XRTableCell DVT7;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT7;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT7;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow8;
        private DevExpress.XtraReports.UI.XRTableCell TenHang8;
        private DevExpress.XtraReports.UI.XRTableCell MaHS8;
        private DevExpress.XtraReports.UI.XRTableCell Luong8;
        private DevExpress.XtraReports.UI.XRTableCell DVT8;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT8;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT8;
        private DevExpress.XtraReports.UI.XRTableRow xrTableRow9;
        private DevExpress.XtraReports.UI.XRTableCell TenHang9;
        private DevExpress.XtraReports.UI.XRTableCell MaHS9;
        private DevExpress.XtraReports.UI.XRTableCell Luong9;
        private DevExpress.XtraReports.UI.XRTableCell DVT9;
        private DevExpress.XtraReports.UI.XRTableCell DonGiaNT9;
        private DevExpress.XtraReports.UI.XRTableCell TriGiaNT9;
        private DevExpress.XtraReports.UI.XRLabel lblSoTiepNhan;
        private DevExpress.XtraReports.UI.XRLabel lblTenChungTu6;
        private DevExpress.XtraReports.UI.XRLabel lblTenChungTu5;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao6;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao5;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao4;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao3;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao2;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanSao1;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh2;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh3;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh4;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh5;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh6;
        private DevExpress.XtraReports.UI.XRLabel lblSoBanChinh1;
        private DevExpress.XtraReports.UI.XRLabel lblTenChungTu4;
        private DevExpress.XtraReports.UI.XRLabel lblTenChungTu3;
        private DevExpress.XtraReports.UI.XRLabel lblSoKienTrongLuong;
        private DevExpress.XtraReports.UI.XRLabel lblBanLuuHaiQuan;
    }
}
