﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace Company.KDT.SHARE.VNACCS
{
	public partial class KDT_VNACCS_CapSoDinhDanh : ICloneable
	{
		#region Properties.
		
		public long ID { set; get; }
		public long SoTiepNhan { set; get; }
		public DateTime NgayTiepNhan { set; get; }
		public string MaDoanhNghiep { set; get; }
		public string MaHQ { set; get; }
		public int LoaiDoiTuong { set; get; }
		public int LoaiTTHH { set; get; }
		public string SoDinhDanh { set; get; }
		public DateTime NgayCap { set; get; }
		public string GuidStr { set; get; }
		public int TrangThaiXuLy { set; get; }
		public string CodeContent { set; get; }
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Methods
		protected static List<KDT_VNACCS_CapSoDinhDanh> ConvertToCollection(IDataReader reader)
		{
			List<KDT_VNACCS_CapSoDinhDanh> collection = new List<KDT_VNACCS_CapSoDinhDanh>();
			while (reader.Read())
			{
				KDT_VNACCS_CapSoDinhDanh entity = new KDT_VNACCS_CapSoDinhDanh();
				if (!reader.IsDBNull(reader.GetOrdinal("ID"))) entity.ID = reader.GetInt64(reader.GetOrdinal("ID"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoTiepNhan"))) entity.SoTiepNhan = reader.GetInt64(reader.GetOrdinal("SoTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayTiepNhan"))) entity.NgayTiepNhan = reader.GetDateTime(reader.GetOrdinal("NgayTiepNhan"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaDoanhNghiep"))) entity.MaDoanhNghiep = reader.GetString(reader.GetOrdinal("MaDoanhNghiep"));
				if (!reader.IsDBNull(reader.GetOrdinal("MaHQ"))) entity.MaHQ = reader.GetString(reader.GetOrdinal("MaHQ"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiDoiTuong"))) entity.LoaiDoiTuong = reader.GetInt32(reader.GetOrdinal("LoaiDoiTuong"));
				if (!reader.IsDBNull(reader.GetOrdinal("LoaiTTHH"))) entity.LoaiTTHH = reader.GetInt32(reader.GetOrdinal("LoaiTTHH"));
				if (!reader.IsDBNull(reader.GetOrdinal("SoDinhDanh"))) entity.SoDinhDanh = reader.GetString(reader.GetOrdinal("SoDinhDanh"));
				if (!reader.IsDBNull(reader.GetOrdinal("NgayCap"))) entity.NgayCap = reader.GetDateTime(reader.GetOrdinal("NgayCap"));
				if (!reader.IsDBNull(reader.GetOrdinal("GuidStr"))) entity.GuidStr = reader.GetString(reader.GetOrdinal("GuidStr"));
				if (!reader.IsDBNull(reader.GetOrdinal("TrangThaiXuLy"))) entity.TrangThaiXuLy = reader.GetInt32(reader.GetOrdinal("TrangThaiXuLy"));
				if (!reader.IsDBNull(reader.GetOrdinal("CodeContent"))) entity.CodeContent = reader.GetString(reader.GetOrdinal("CodeContent"));
				collection.Add(entity);
			}
			reader.Close();
			return collection;
		}
		
		public static bool Find(List<KDT_VNACCS_CapSoDinhDanh> collection, long id)
        {
            foreach (KDT_VNACCS_CapSoDinhDanh item in collection)
            {
                if (item.ID == id)
                {
                    return true;
                }
            }

            return false;
        }
		
		public static void UpdateDataSet(DataSet ds)
        {
            string insert = "Insert INTO t_KDT_VNACCS_CapSoDinhDanh VALUES(@SoTiepNhan, @NgayTiepNhan, @MaDoanhNghiep, @MaHQ, @LoaiDoiTuong, @LoaiTTHH, @SoDinhDanh, @NgayCap, @GuidStr, @TrangThaiXuLy, @CodeContent)";
            string update = "UPDATE t_KDT_VNACCS_CapSoDinhDanh SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, MaDoanhNghiep = @MaDoanhNghiep, MaHQ = @MaHQ, LoaiDoiTuong = @LoaiDoiTuong, LoaiTTHH = @LoaiTTHH, SoDinhDanh = @SoDinhDanh, NgayCap = @NgayCap, GuidStr = @GuidStr, TrangThaiXuLy = @TrangThaiXuLy, CodeContent = @CodeContent WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_CapSoDinhDanh WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiDoiTuong", SqlDbType.Int, "LoaiDoiTuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiTTHH", SqlDbType.Int, "LoaiTTHH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDinhDanh", SqlDbType.NVarChar, "SoDinhDanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CodeContent", SqlDbType.VarChar, "CodeContent", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiDoiTuong", SqlDbType.Int, "LoaiDoiTuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiTTHH", SqlDbType.Int, "LoaiTTHH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDinhDanh", SqlDbType.NVarChar, "SoDinhDanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CodeContent", SqlDbType.VarChar, "CodeContent", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, ds.Tables[0].TableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }
				
		public static void UpdateDataSet(DataSet ds, string tableName)
        {
            string insert = "Insert INTO t_KDT_VNACCS_CapSoDinhDanh VALUES(@SoTiepNhan, @NgayTiepNhan, @MaDoanhNghiep, @MaHQ, @LoaiDoiTuong, @LoaiTTHH, @SoDinhDanh, @NgayCap, @GuidStr, @TrangThaiXuLy, @CodeContent)";
            string update = "UPDATE t_KDT_VNACCS_CapSoDinhDanh SET SoTiepNhan = @SoTiepNhan, NgayTiepNhan = @NgayTiepNhan, MaDoanhNghiep = @MaDoanhNghiep, MaHQ = @MaHQ, LoaiDoiTuong = @LoaiDoiTuong, LoaiTTHH = @LoaiTTHH, SoDinhDanh = @SoDinhDanh, NgayCap = @NgayCap, GuidStr = @GuidStr, TrangThaiXuLy = @TrangThaiXuLy, CodeContent = @CodeContent WHERE ID = @ID";
            string delete = "DELETE FROM t_KDT_VNACCS_CapSoDinhDanh WHERE ID = @ID";

			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();

            System.Data.Common.DbCommand InsertCommand = db.GetSqlStringCommand(insert);
			db.AddInParameter(InsertCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiDoiTuong", SqlDbType.Int, "LoaiDoiTuong", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@LoaiTTHH", SqlDbType.Int, "LoaiTTHH", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@SoDinhDanh", SqlDbType.NVarChar, "SoDinhDanh", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(InsertCommand, "@CodeContent", SqlDbType.VarChar, "CodeContent", DataRowVersion.Current);

            System.Data.Common.DbCommand UpdateCommand = db.GetSqlStringCommand(update);
			db.AddInParameter(UpdateCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoTiepNhan", SqlDbType.BigInt, "SoTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayTiepNhan", SqlDbType.DateTime, "NgayTiepNhan", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, "MaDoanhNghiep", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@MaHQ", SqlDbType.NVarChar, "MaHQ", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiDoiTuong", SqlDbType.Int, "LoaiDoiTuong", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@LoaiTTHH", SqlDbType.Int, "LoaiTTHH", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@SoDinhDanh", SqlDbType.NVarChar, "SoDinhDanh", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@NgayCap", SqlDbType.DateTime, "NgayCap", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@GuidStr", SqlDbType.VarChar, "GuidStr", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@TrangThaiXuLy", SqlDbType.Int, "TrangThaiXuLy", DataRowVersion.Current);
			db.AddInParameter(UpdateCommand, "@CodeContent", SqlDbType.VarChar, "CodeContent", DataRowVersion.Current);
			
            System.Data.Common.DbCommand DeleteCommand = db.GetSqlStringCommand(delete);
			db.AddInParameter(DeleteCommand, "@ID", SqlDbType.BigInt, "ID", DataRowVersion.Current);

            db.UpdateDataSet(ds, tableName, InsertCommand, UpdateCommand, DeleteCommand, UpdateBehavior.Standard);
        }

		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Select methods.
		
		public static KDT_VNACCS_CapSoDinhDanh Load(long id)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_CapSoDinhDanh_Load]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, id);
            IDataReader reader = db.ExecuteReader(dbCommand);
			List<KDT_VNACCS_CapSoDinhDanh> collection = ConvertToCollection(reader);	
			if (collection.Count > 0)
			{
				return collection[0];
			}
			return null;
		}		
		
		//---------------------------------------------------------------------------------------------
		public static List<KDT_VNACCS_CapSoDinhDanh> SelectCollectionAll()
		{
			IDataReader reader = SelectReaderAll();
			return ConvertToCollection(reader);			
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public static List<KDT_VNACCS_CapSoDinhDanh> SelectCollectionDynamic(string whereCondition, string orderByExpression)
		{
			IDataReader reader = SelectReaderDynamic(whereCondition, orderByExpression);
			return ConvertToCollection(reader);		
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		

		public static DataSet SelectAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			
            return db.ExecuteDataSet(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static DataSet SelectDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteDataSet(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
				
		public static IDataReader SelectReaderAll()
        {
            const string spName = "[dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectAll]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
            return db.ExecuteReader(dbCommand);
        }
		
		//---------------------------------------------------------------------------------------------
		
		public static IDataReader SelectReaderDynamic(string whereCondition, string orderByExpression)
		{
            const string spName = "[dbo].[p_KDT_VNACCS_CapSoDinhDanh_SelectDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            db.AddInParameter(dbCommand, "@OrderByExpression", SqlDbType.NVarChar, orderByExpression);
            
            return db.ExecuteReader(dbCommand);        				
		}
		
		//---------------------------------------------------------------------------------------------
		
		// Select by foreign key return collection		
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert methods.
		
		public static long InsertKDT_VNACCS_CapSoDinhDanh(long soTiepNhan, DateTime ngayTiepNhan, string maDoanhNghiep, string maHQ, int loaiDoiTuong, int loaiTTHH, string soDinhDanh, DateTime ngayCap, string guidStr, int trangThaiXuLy, string codeContent)
		{
			KDT_VNACCS_CapSoDinhDanh entity = new KDT_VNACCS_CapSoDinhDanh();	
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaHQ = maHQ;
			entity.LoaiDoiTuong = loaiDoiTuong;
			entity.LoaiTTHH = loaiTTHH;
			entity.SoDinhDanh = soDinhDanh;
			entity.NgayCap = ngayCap;
			entity.GuidStr = guidStr;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.CodeContent = codeContent;
			return entity.Insert();
		}
		
		public long Insert()
		{
			return this.Insert(null);
		}		
		
		//---------------------------------------------------------------------------------------------
		
		public long Insert(SqlTransaction transaction)
		{			
			const string spName = "[dbo].[p_KDT_VNACCS_CapSoDinhDanh_Insert]";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddOutParameter(dbCommand, "@ID", SqlDbType.BigInt, 8);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@LoaiDoiTuong", SqlDbType.Int, LoaiDoiTuong);
			db.AddInParameter(dbCommand, "@LoaiTTHH", SqlDbType.Int, LoaiTTHH);
			db.AddInParameter(dbCommand, "@SoDinhDanh", SqlDbType.NVarChar, SoDinhDanh);
			db.AddInParameter(dbCommand, "@NgayCap", SqlDbType.DateTime, NgayCap.Year <= 1753 ? DBNull.Value : (object) NgayCap);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@CodeContent", SqlDbType.VarChar, CodeContent);
			
			if (transaction != null)
			{
				db.ExecuteNonQuery(dbCommand, transaction);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}
            else
			{
				db.ExecuteNonQuery(dbCommand);
				ID = (long) db.GetParameterValue(dbCommand, "@ID");
				return ID;
			}			
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool InsertCollection(List<KDT_VNACCS_CapSoDinhDanh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using(SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_CapSoDinhDanh item in collection)
						{
							if (item.Insert(transaction) <= 0)
							{							
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Insert / Update methods.
		
		public static int InsertUpdateKDT_VNACCS_CapSoDinhDanh(long id, long soTiepNhan, DateTime ngayTiepNhan, string maDoanhNghiep, string maHQ, int loaiDoiTuong, int loaiTTHH, string soDinhDanh, DateTime ngayCap, string guidStr, int trangThaiXuLy, string codeContent)
		{
			KDT_VNACCS_CapSoDinhDanh entity = new KDT_VNACCS_CapSoDinhDanh();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaHQ = maHQ;
			entity.LoaiDoiTuong = loaiDoiTuong;
			entity.LoaiTTHH = loaiTTHH;
			entity.SoDinhDanh = soDinhDanh;
			entity.NgayCap = ngayCap;
			entity.GuidStr = guidStr;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.CodeContent = codeContent;
			return entity.InsertUpdate();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int InsertUpdate()
		{
			return this.InsertUpdate(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int InsertUpdate(SqlTransaction transaction)
		{			
			const string spName = "p_KDT_VNACCS_CapSoDinhDanh_InsertUpdate";		
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@LoaiDoiTuong", SqlDbType.Int, LoaiDoiTuong);
			db.AddInParameter(dbCommand, "@LoaiTTHH", SqlDbType.Int, LoaiTTHH);
			db.AddInParameter(dbCommand, "@SoDinhDanh", SqlDbType.NVarChar, SoDinhDanh);
			db.AddInParameter(dbCommand, "@NgayCap", SqlDbType.DateTime, NgayCap.Year <= 1753 ? DBNull.Value : (object) NgayCap);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@CodeContent", SqlDbType.VarChar, CodeContent);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);			
		}
		
		//---------------------------------------------------------------------------------------------
		public static bool InsertUpdateCollection(List<KDT_VNACCS_CapSoDinhDanh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_CapSoDinhDanh item in collection)
						{
							if (item.InsertUpdate(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at InsertUpdateCollection method: " + ex.Message);
						
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}	
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Update methods.
		
		public static int UpdateKDT_VNACCS_CapSoDinhDanh(long id, long soTiepNhan, DateTime ngayTiepNhan, string maDoanhNghiep, string maHQ, int loaiDoiTuong, int loaiTTHH, string soDinhDanh, DateTime ngayCap, string guidStr, int trangThaiXuLy, string codeContent)
		{
			KDT_VNACCS_CapSoDinhDanh entity = new KDT_VNACCS_CapSoDinhDanh();			
			entity.ID = id;
			entity.SoTiepNhan = soTiepNhan;
			entity.NgayTiepNhan = ngayTiepNhan;
			entity.MaDoanhNghiep = maDoanhNghiep;
			entity.MaHQ = maHQ;
			entity.LoaiDoiTuong = loaiDoiTuong;
			entity.LoaiTTHH = loaiTTHH;
			entity.SoDinhDanh = soDinhDanh;
			entity.NgayCap = ngayCap;
			entity.GuidStr = guidStr;
			entity.TrangThaiXuLy = trangThaiXuLy;
			entity.CodeContent = codeContent;
			return entity.Update();
		}
		
		//---------------------------------------------------------------------------------------------
		
		public int Update()
		{
			return this.Update(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Update(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_CapSoDinhDanh_Update]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			db.AddInParameter(dbCommand, "@SoTiepNhan", SqlDbType.BigInt, SoTiepNhan);
			db.AddInParameter(dbCommand, "@NgayTiepNhan", SqlDbType.DateTime, NgayTiepNhan.Year <= 1753 ? DBNull.Value : (object) NgayTiepNhan);
			db.AddInParameter(dbCommand, "@MaDoanhNghiep", SqlDbType.NVarChar, MaDoanhNghiep);
			db.AddInParameter(dbCommand, "@MaHQ", SqlDbType.NVarChar, MaHQ);
			db.AddInParameter(dbCommand, "@LoaiDoiTuong", SqlDbType.Int, LoaiDoiTuong);
			db.AddInParameter(dbCommand, "@LoaiTTHH", SqlDbType.Int, LoaiTTHH);
			db.AddInParameter(dbCommand, "@SoDinhDanh", SqlDbType.NVarChar, SoDinhDanh);
			db.AddInParameter(dbCommand, "@NgayCap", SqlDbType.DateTime, NgayCap.Year <= 1753 ? DBNull.Value : (object) NgayCap);
			db.AddInParameter(dbCommand, "@GuidStr", SqlDbType.VarChar, GuidStr);
			db.AddInParameter(dbCommand, "@TrangThaiXuLy", SqlDbType.Int, TrangThaiXuLy);
			db.AddInParameter(dbCommand, "@CodeContent", SqlDbType.VarChar, CodeContent);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
				
		//---------------------------------------------------------------------------------------------
		public static bool UpdateCollection(List<KDT_VNACCS_CapSoDinhDanh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_CapSoDinhDanh item in collection)
						{
							if (item.Update(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at UpdateCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		
		#endregion
		
		//---------------------------------------------------------------------------------------------
		
		#region Delete methods.
		
		public static int DeleteKDT_VNACCS_CapSoDinhDanh(long id)
		{
			KDT_VNACCS_CapSoDinhDanh entity = new KDT_VNACCS_CapSoDinhDanh();
			entity.ID = id;
			
			return entity.Delete();
		}
		
		public int Delete()
		{
			return this.Delete(null);
		}
		
		//---------------------------------------------------------------------------------------------

		public int Delete(SqlTransaction transaction)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_CapSoDinhDanh_Delete]";		
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
			SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);
			
			db.AddInParameter(dbCommand, "@ID", SqlDbType.BigInt, ID);
			
			if (transaction != null)
                return db.ExecuteNonQuery(dbCommand, transaction);
            else
                return db.ExecuteNonQuery(dbCommand);
		}
		
		//---------------------------------------------------------------------------------------------
		
		
		public static int DeleteDynamic(string whereCondition)
		{
			const string spName = "[dbo].[p_KDT_VNACCS_CapSoDinhDanh_DeleteDynamic]";
            SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            SqlCommand dbCommand = (SqlCommand) db.GetStoredProcCommand(spName);

			db.AddInParameter(dbCommand, "@WhereCondition", SqlDbType.NVarChar, whereCondition);
            
            return db.ExecuteNonQuery(dbCommand);   
		}
		//---------------------------------------------------------------------------------------------
		
		public static bool DeleteCollection(List<KDT_VNACCS_CapSoDinhDanh> collection)
        {
            bool ret;
			SqlDatabase db = (SqlDatabase) DatabaseFactory.CreateDatabase();
            using (SqlConnection connection = (SqlConnection)db.CreateConnection())
            {
                connection.Open();
                using (SqlTransaction transaction = connection.BeginTransaction())
				{
					try
					{
						bool ret01 = true;
						foreach (KDT_VNACCS_CapSoDinhDanh item in collection)
						{
							if (item.Delete(transaction) <= 0)
							{
								ret01 = false;
								break;
							}
						}
						if (ret01)
						{
							transaction.Commit();
							ret = true;
						}
						else
						{
							transaction.Rollback();
							ret = false;                    	
						}
					}
					catch (Exception ex)
					{
						transaction.Rollback();
						throw new Exception("Error at DeleteCollection method: " + ex.Message);
					}
					finally 
					{
						connection.Close();
					}
				}
            }
            return ret;		
		}
		#endregion
		
		
        #region ICloneable Members

        public object Clone()
        {
            return base.MemberwiseClone();
        }

        #endregion
	}	
}