using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Janus.Windows.GridEX;

namespace Company.Interface.SXXK.BangKeThanhLy
{
    public partial class DSToKhaiNhapChuaNPLForm : BaseForm
    {
        public int SoToKhai = 0;
        public decimal SoToKhaiVNACCS = 0;
        public string MaLoaiHinh = "";
        public DateTime NgayDangKy = new DateTime(1900, 1, 1);
        public long BangKeHSTL_ID;
        public DataSet ds;
        public DSToKhaiNhapChuaNPLForm()
        {
            InitializeComponent();
        }

        private void dgList1_LoadingRow(object sender, Janus.Windows.GridEX.RowLoadEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                e.Row.Cells["MaLoaiHinh"].Text = LoaiHinhMauDich_GetTenVT(e.Row.Cells["MaLoaiHinh"].Value);
            }
        }

        private void DSToKhaiNhapChuaNPLForm_Load(object sender, EventArgs e)
        {
            dgList1.DataSource = ds.Tables[0];
        }

        private void dgList1_RowDoubleClick(object sender, Janus.Windows.GridEX.RowActionEventArgs e)
        {
            if (e.Row.RowType == RowType.Record)
            {
                this.SoToKhai = Convert.ToInt32(e.Row.Cells["SoToKhai"].Value);
                this.SoToKhaiVNACCS = Convert.ToDecimal(e.Row.Cells["SoToKhaiVNACCS"].Value);
                this.MaLoaiHinh = Convert.ToString(e.Row.Cells["MaLoaiHinh"].Value);
                this.NgayDangKy = Convert.ToDateTime(e.Row.Cells["NgayDangKy"].Value);
                this.MaHaiQuan = Convert.ToString(e.Row.Cells["MaHaiQuan"].Value);
                this.Close();
            }
        }
    }
}