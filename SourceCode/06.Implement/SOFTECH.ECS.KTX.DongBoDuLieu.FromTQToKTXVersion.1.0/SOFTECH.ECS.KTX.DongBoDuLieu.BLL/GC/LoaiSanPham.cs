using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.SqlClient;
namespace DongBoDuLieu.BLL.GC
{
    public class LoaiSP
    {
        public static DataSet GetDataSetLoaiSanPham(Company.GC.BLL.KDT.GC.HopDong HD, string matKhau)
        {
            
            StringBuilder queryLoaiSP = new StringBuilder();
            queryLoaiSP.Append("SELECT ");
            queryLoaiSP.Append("LSP.So_HD			AS SoHopDong, ");
            queryLoaiSP.Append("LSP.Ma_HQHD		AS MaHaiQuan, ");
            queryLoaiSP.Append("LSP.DVGC			AS MaDoanhNghiep, ");
            queryLoaiSP.Append("LSP.Ngay_Ky		AS NgayKy, ");
            queryLoaiSP.Append("LSP.Ma_SPGC		AS Ma, ");
            queryLoaiSP.Append("LSP.So_Luong		AS SoLuong, ");
            queryLoaiSP.Append("LSP.Gia_GC		AS Gia ");
            queryLoaiSP.Append("FROM DLOAISPGC LSP ");
            queryLoaiSP.Append("WHERE LSP.Ma_HQHD = '" + HD.MaHaiQuan + "' AND DVGC = '" + HD.MaDoanhNghiep + "' ");
            queryLoaiSP.Append(" and So_HD='" + HD.SoHopDong + "'");
            DataSet dsLoaiSP = WebServiceDB.getDataBySQL(queryLoaiSP.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsLoaiSP;

            //TODO: LoaiSanPham
            /*
            StringBuilder queryLoaiSP = new StringBuilder();
            queryLoaiSP.Append("SELECT a.* FROM OPENROWSET('SQLOLEDB', 'Server=10.4.230.4;database=SLXNK;uid=tchq;pwd=DD9A3CAD249673C809EFA291C83CC7CDD97E3919', ' ");
            queryLoaiSP.Append("SELECT ");
            queryLoaiSP.Append("LSP.So_HD			AS SoHopDong, ");
            queryLoaiSP.Append("LSP.Ma_HQHD		AS MaHaiQuan, ");
            queryLoaiSP.Append("LSP.DVGC			AS MaDoanhNghiep, ");
            queryLoaiSP.Append("LSP.Ngay_Ky		AS NgayKy, ");
            queryLoaiSP.Append("LSP.Ma_SPGC		AS Ma, ");
            queryLoaiSP.Append("LSP.So_Luong		AS SoLuong, ");
            queryLoaiSP.Append("LSP.Gia_GC		AS Gia ");
            queryLoaiSP.Append("FROM DLOAISPGC LSP ");
            queryLoaiSP.Append("WHERE LSP.Ma_HQHD = ''" + HD.MaHaiQuan + "'' AND DVGC = ''" + HD.MaDoanhNghiep + "'' ");
            queryLoaiSP.Append(" and So_HD=''" + HD.SoHopDong + "''");
            queryLoaiSP.Append(" ') AS a;");
            DataSet dsLoaiSP = WebServiceDB.getDataBySQL(queryLoaiSP.ToString(), HD.MaHaiQuan, "SLXNK", HD.MaDoanhNghiep, matKhau);
            return dsLoaiSP;
            */
        }

        public static DataSet GetLoaiSPAtTMP(long Id_HopDong)
        {
            string sql = "select * from t_GC_NhomSanPham where HopDong_ID=" + Id_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase("MSSQL");
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }

        public static DataSet GetLoaiSPAtDN(long Id_HopDong)
        {
            string sql = "select * from t_GC_NhomSanPham where HopDong_ID="+Id_HopDong;
            SqlDatabase db = (SqlDatabase)DatabaseFactory.CreateDatabase();
            SqlCommand command = (SqlCommand)db.GetSqlStringCommand(sql);
            return db.ExecuteDataSet(command);
        }
    }
}
