﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Company.KDT.SHARE.Components
{
    public class CommercialInvoice : IssueBase
    {
        /// <summary>
        /// Tên Người Nhập
        /// </summary>
        [XmlElement("Seller")]
        public NameBase Seller { get; set; }
        /// <summary>
        /// Tên người xuất
        /// </summary>
        [XmlElement("Buyer")]
        public NameBase Buyer { get; set; }
        /// <summary>
        /// ...
        /// </summary>
        [XmlElement("AdditionalDocument")]
        public AdditionalDocument AdditionalDocument { get; set; }
        /// <summary>
        /// Phương thức thanh toán
        /// </summary>
        [XmlElement("Payment")]
        public Payment Payment { get; set; }
        /// <summary>
        /// Nguyên tệ
        /// </summary>
        [XmlElement("CurrencyExchange")]
        public CurrencyExchange CurrencyExchange { get; set; }
        /// <summary>
        /// Điều kiện giao hàng
        /// </summary>
        [XmlElement("TradeTerm")]
        public TradeTerm TradeTerm { get; set; }
        /// <summary>
        /// ....
        /// </summary>
        [XmlElement("CommercialInvoiceItem")]
        public List<CommercialInvoiceItem> CommercialInvoiceItems { get; set; }

    }
}
