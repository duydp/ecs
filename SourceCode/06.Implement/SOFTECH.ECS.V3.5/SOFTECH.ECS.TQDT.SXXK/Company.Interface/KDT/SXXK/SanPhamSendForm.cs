﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Company.BLL;
using Company.BLL.KDT.SXXK;
using Company.KDT.SHARE.Components.DuLieuChuan;
using Janus.Windows.GridEX;
using Company.BLL.KDT;
using System.IO;
using Company.KDT.SHARE.Components;
using Company.KDT.SHARE.Components.Messages.Send;
using System.Data;
using Company.Interface.SXXK;
using Company.Interface.KDT.GC;
using Company.KDT.SHARE.QuanLyChungTu;
using Company.QuanTri;

namespace Company.Interface.KDT.SXXK
{
    public partial class SanPhamSendForm : BaseFormHaveGuidPanel
    {
        public  SanPhamDangKy spDangKy = new SanPhamDangKy();
        public SanPham SP = new SanPham();
        private string xmlCurrent = "";
        private string msgInfor = string.Empty;
        private FeedBackContent feedbackContent = null;
        public bool isAdd = true;
        public SanPhamCollection SPCollection = new SanPhamCollection();
        SanPhamRegistedForm SPRegistedForm = new SanPhamRegistedForm();
        public bool isEdit = false;
        public bool isCancel = false;
        public LogHistory Log = new LogHistory();
        //-----------------------------------------------------------------------------------------
        public SanPhamSendForm()
        {
            InitializeComponent();
        }

        #region Private methods.

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Khởi tạo dữ liệu chuẩn.
        /// </summary>
        private void khoitao_DuLieuChuan()
        {
            try
            {
                this._DonViTinh = DonViTinh.SelectAll().Tables[0];
                System.Windows.Forms.AutoCompleteStringCollection col = new System.Windows.Forms.AutoCompleteStringCollection();
                DataTable dt = MaHS.SelectAll();
                foreach (DataRow dr in dt.Rows)
                    col.Add(dr["HS10So"].ToString());
                txtMaHS.AutoCompleteCustomSource = col;
                this._DonViTinh = Company.KDT.SHARE.Components.Globals.GlobalDanhMucChuanHQ.Tables["DonViTinh"];
                cbDonViTinh.DataSource = this._DonViTinh;
                cbDonViTinh.DisplayMember = "Ten";
                cbDonViTinh.ValueMember = "ID";
                cbDonViTinh.SelectedValue = GlobalSettings.DVT_MAC_DINH;
                SPCollection = SanPham.SelectCollectionAll();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        private void SanPhamSendForm_Load(object sender, EventArgs e)
        {
            try
            {
                this.khoitao_DuLieuChuan();
                if (spDangKy.ID == 0)
                {
                    if (isEdit)
                    {
                        spDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else if (isCancel)
                    {
                        spDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    }
                    else
                    {
                        spDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                }
                else
                {
                    spDangKy.LoadSPCollection();
                }
                SetCommandNew();
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Thêm mới sản phẩm
        /// </summary>
        private void Add()
        {
            try
            {
                SanPhamEditForm f = new SanPhamEditForm();
                f.OpenType = OpenFormType.Insert;
                f.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                f.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                f.SPCollection = spDangKy.SPCollection;
                f.spDangKy = spDangKy;
                f.ShowDialog(this);
                if (f.SPDetail != null)
                {
                    this.spDangKy.SPCollection.Add(f.SPDetail);
                }
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);

            }
        }

        //-----------------------------------------------------------------------------------------
        /// <summary>
        /// Lưu thông tin.
        /// </summary>
        private void Save()
        {
            try
            {

                if (this.spDangKy.SPCollection.Count == 0)
                {
                    MLMessages("DANH SÁCH SẢN PHẨM RỖNG.\nKHÔNG THỂ CẬP NHẬT DỮ LIỆU.", "MSG_SAV11", "", false);

                    return;
                }
                // Master.
                this.spDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                if (this.spDangKy.ID == 0)
                {
                    this.spDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY;
                    this.spDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                    if (isEdit)
                    {
                        this.spDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    }
                    else if (isCancel)
                    {
                        this.spDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                    }
                    else
                    {
                        this.spDangKy.TrangThaiXuLy = TrangThaiXuLy.CHUA_KHAI_BAO;
                    }
                    this.spDangKy.NgayTiepNhan = DateTime.Now;
                    spDangKy.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text.ToString());
                }
                else
                {
                    spDangKy.NgayTiepNhan = dtpNgayTN.Value;
                    spDangKy.SoTiepNhan = Convert.ToInt64(txtSoTiepNhan.Text.ToString());
                }
                int sttHang = 1;
                foreach (SanPham spD in this.spDangKy.SPCollection)
                {
                    spD.STTHang = sttHang++;
                }
                this.spDangKy.InsertUpdateFull();
                MLMessages("LƯU THÀNH CÔNG!", "MSG_SAV02", "", false);
                Log.LogSanPham(spDangKy, MessageTitle.LuuThongTin, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
            finally
            {
            }
        }


        //-----------------------------------------------------------------------------------------
        private void updateRowOnGrid(string maNPL)
        {
            try
            {
                GridEXRow[] jrows = dgList.GetRows();
                foreach (GridEXRow row in jrows)
                {
                    if (row.Cells["Ma"].Value.ToString().Equals(maNPL))
                    {
                        row.BeginEdit();
                        row.Cells["IsExistOnServer"].Value = 1;
                        row.EndEdit();
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        private void updateRowsOnGrid(IEnumerable<string> collection)
        {
            try
            {
                GridEXRow[] jrows = dgList.GetRows();
                foreach (GridEXRow row in jrows)
                {
                    row.BeginEdit();
                    row.Cells["IsExistOnServer"].Value = 0;
                    row.EndEdit();
                }

                foreach (string s in collection)
                {
                    this.updateRowOnGrid(s);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        #endregion


        //-----------------------------------------------------------------------------------------
        private void dgList_LoadingRow(object sender, RowLoadEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    e.Row.Cells["DVT_Ten"].Text = this.DonViTinh_GetName(e.Row.Cells["DVT_ID"].Value.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        private void dgList_RowDoubleClick(object sender, RowActionEventArgs e)
        {
            try
            {
                if (e.Row.RowType == RowType.Record)
                {
                    SP = new SanPham();
                    SP = (SanPham)e.Row.DataRow;
                    SetData();
                    isAdd = false;
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        //-----------------------------------------------------------------------------------------
        private void cmMain_CommandClick(object sender, Janus.Windows.UI.CommandBars.CommandEventArgs e)
        {
            switch (e.Command.Key)
            {
                case "cmdSave":
                    this.Save();
                    break;
                case "cmdAdd":
                    this.Add();
                    break;
                case "cmdSend":
                     this.SendV5(false);
                    break;
                case "cmdCancel":
                    if (ShowMessage("DOANH NGHIỆP CÓ CHẮC CHẮN MUỐN KHAI BÁO HỦY SẢN PHẨM NÀY ĐẾN HQ KHÔNG ? ", true) == "Yes")
                    {
                        spDangKy.TrangThaiXuLy = TrangThaiXuLy.HUYTKDADUYET;
                        spDangKy.Update();
                        SetCommandNew();
                        this.SendV5(true);
                    }
                    break;
                case "cmdAddExcel":
                    this.AddSanPhamFromExcel();
                    break;
                case "cmdEdit":
                    this.SendV5(false);
                    break;
                case "XacNhan":
                        this.FeedBackV5();
                    break;
                case "InPhieuTN":
                    this.inPhieuTN();
                    break;
                case "cmdSuaSanPham":
                    this.SuaSanPham();
                    break;
                case "cmdResult":
                    this.Result();
                    break;
                case "cmdUpdateGuidString":
                    this.UpdateGUIDSTR();
                    break;
                case "cmdUpdateResult":
                    this.UpdateResult();
                    break;
                case "cmdHistory":
                    this.History();
                    break;
            }
        }
        private void History()
        {
            try
            {
                HistoryForm f = new HistoryForm();
                f.ID = spDangKy.ID;
                f.loaiKhaiBao = LoaiKhaiBao.SanPham;
                f.LogKhaiBaoCollection = LogKhaiBao.SelectCollectionDynamic(String.Format("ID_DK = {0} AND LoaiKhaiBao = '{1}'", spDangKy.ID, LoaiKhaiBao.SanPham), "");
                f.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void UpdateResult()
        {
            try
            {
                if (ShowMessageTQDT("Doanh nghiệp có chắc chắc muốn cập nhật lại thông tin đã khai báo từ HQ không ?", true) == "Yes")
                {
                    ProcessMessage f = new ProcessMessage();
                    f.ProcessMessageSP(spDangKy);
                    spDangKy.InsertUpdateFull();
                    if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET || spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET || spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET_DASUACHUA)
                    {
                        spDangKy.TransgferDataToSXXK();
                    }
                    Log.LogSanPham(spDangKy, MessageTitle.CapNhatKetQuaXuLy, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    ShowMessage("Cập nhật thông tin thành công .", false);
                    SetCommandNew();
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void UpdateGUIDSTR()
        {
            try
            {
                frmUpdateGUIDSTR frm = new frmUpdateGUIDSTR("t_KDT_SXXK_SanPhamDangKy", "", Convert.ToInt32(spDangKy.ID));
                frm.ShowDialog();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        public void Result()
        {
            try
            {
                ThongDiepForm form = new ThongDiepForm();
                form.ItemID = spDangKy.ID;
                form.DeclarationIssuer = Company.KDT.SHARE.Components.Globals.LaDNCX ? DeclarationIssuer.DNCX_HANG_RA :DeclarationIssuer.SXXK_SP;
                form.ShowDialog(this);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        // duydp
        private void SuaSanPham() 
        {
            try
            {
                string msg = "";
                msg += "-------------THÔNG TIN SẢN PHẨM ĐÃ KHAI BÁO-------------";
                msg += "\nSỐ TIẾP NHẬN : " + spDangKy.SoTiepNhan.ToString();
                msg += "\nNGÀY TIẾP NHẬN : " + spDangKy.NgayTiepNhan.ToString("dd/MM/yy hh:mm:ss");
                msg += "\nHẢI QUAN TIẾP NHẬN : " + spDangKy.MaHaiQuan.ToString();
                msg += "\n--------------------THÔNG TIN XÁC NHẬN--------------------";
                msg += "\nBẠN CÓ MUỐN CHUYỂN SANG TRẠNG THÁI KHAI BÁO SỬA KHÔNG ?";
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG",msg, true) == "Yes")
                {
                    spDangKy.TrangThaiXuLy = TrangThaiXuLy.SUATKDADUYET;
                    spDangKy.InsertUpdate();
                    spDangKy.DeleteFull(spDangKy.MaDoanhNghiep, spDangKy.MaHaiQuan);
                    SetCommandStatus();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void inPhieuTN()
        {
            try
            {
                if (this.spDangKy.SoTiepNhan == 0) return;
                Company.Interface.Report.SXXK.PhieuTN phieuTN = new Company.Interface.Report.SXXK.PhieuTN();
                phieuTN.phieu = "SẢN PHẨM";
                phieuTN.soTN = this.spDangKy.SoTiepNhan.ToString();
                phieuTN.ngayTN = this.spDangKy.NgayTiepNhan.ToString("dd/MM/yyyy");
                phieuTN.maHaiQuan = this.spDangKy.MaHaiQuan.ToString();
                phieuTN.BindReport();
                phieuTN.ShowPreview();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetCommandStatus()
        {
            if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_HUY)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET ? setText("Chờ duyệt", "Wait for approval") : setText("Chờ hủy", "Wait for cancel");
                this.OpenType = OpenFormType.View;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY || spDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdAddExcel.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lblTrangThai.Text = spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY ? setText("Đã hủy", "Canceled") : setText("Không phê duyệt", "Not approved");
                this.OpenType = OpenFormType.View;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = "Đang hủy";
                this.OpenType = OpenFormType.Edit;
            }
            else
            {
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                cmdAdd.Enabled = cmdAdd.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdAddExcel.Enabled = cmdAddExcel.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Not declared") : setText("Đang sửa", "Editing");
                this.OpenType = OpenFormType.Edit;
            }

        }
        private void AddSanPhamFromExcel()
        {
            try
            {
                SanPhamReadExcelForm f = new SanPhamReadExcelForm();
                f.SPCollection = this.spDangKy.SPCollection;
                f.spDangKy = spDangKy;
                f.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }



        private void dgList_DeletingRecords(object sender, System.ComponentModel.CancelEventArgs e)
        {
            btnDelete_Click(null,null);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void XoaSanPham(GridEXSelectedItemCollection items)
        {
            if (items.Count == 0) return;
            string msgWarning = string.Empty;
            List<SanPham> sanphams = new List<SanPham>();
            List<GridEXSelectedItem> itemRemove = new List<GridEXSelectedItem>();
            foreach (GridEXSelectedItem i in items)
            {
                if (i.RowType == RowType.Record)
                {
                    SanPham sp = (SanPham)i.GetRow().DataRow;
                    sanphams.Add(sp);
                }
            }
            if (ShowMessage(" DOANH NGHIỆP CÓ MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
            {
                foreach (SanPham item in sanphams)
                {
                    if (item.ID > 0)
                    {
                        try
                        {
                            // XÓA SP ĐÃ ĐĂNG KÝ
                            Company.BLL.SXXK.SanPham SP = new Company.BLL.SXXK.SanPham();
                            SP.Ma = item.Ma;
                            SP.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            SP.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            SP.Delete();
                        }
                        catch (Exception ex)
                        {
                            Logger.LocalLogger.Instance().WriteMessage(ex);
                        }
                        item.Delete();
                    }
                    sanphams.Remove(item);
                }
                foreach (SanPham item in sanphams)
                {
                    spDangKy.SPCollection.Remove(item);
                }
                ShowMessage("XÓA THÀNH CÔNG",false);
                BindData();
            }
        }
        private void Delete()
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                XoaSanPham(items);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXSelectedItemCollection items = dgList.SelectedItems;
                SanPhamCollection ItemCollDelete = new SanPhamCollection();
                List<SanPham> ItemColl = new List<SanPham>();
                if (dgList.GetRows().Length < 0) return;
                if (items.Count <= 0) return;
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "DOANH NGHIỆP MUỐN XÓA DÒNG HÀNG NÀY KHÔNG ?", true) == "Yes")
                {
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            ItemColl.Add((SanPham)i.GetRow().DataRow);
                        }

                    }
                    foreach (SanPham item in ItemColl)
                    {
                        if (item.ID > 0)
                        {                            
                            try
                            {
                                Company.BLL.SXXK.SanPham SP = new Company.BLL.SXXK.SanPham();
                                SP.Ma = item.Ma;
                                SP.MaHaiQuan = spDangKy.MaHaiQuan;
                                SP.MaDoanhNghiep = spDangKy.MaDoanhNghiep;
                                SP.Delete();
                            }
                            catch (Exception ex)
                            {
                                Logger.LocalLogger.Instance().WriteMessage(ex);
                            }
                            item.Delete();
                        }
                        ItemCollDelete.Add(item);
                        spDangKy.SPCollection.Remove(item);
                    }
                    int k = 1;
                    foreach (SanPham item in spDangKy.SPCollection)
                    {
                        item.STTHang = k;
                        k++;
                    }
                    BindData();
                    Log.LogSanPham(spDangKy, ItemCollDelete, MessageTitle.XoaSanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        //--------------------------------------------------------------------------------
        #region Send VNACCS
        private void SendV5(bool isCancel)
        {
            if (spDangKy.ID == 0)
            {
                this.ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "BẠN HÃY LƯU THÔNG TIN TRƯỚC KHI KHAI BÁO", false);
                return;
            }
            if (this.spDangKy.SPCollection.Count == 0)
            {
                ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "DANH SÁCH SẢN PHẨM RỖNG.\nKHÔNG THỂ THỰC HIỆN KHAI BÁO THÔNG TIN ĐẾN HẢI QUAN.", false);
                return;
            }
            this.spDangKy.MaHaiQuan = GlobalSettings.MA_HAI_QUAN.Trim();
            this.spDangKy.MaDoanhNghiep = GlobalSettings.MA_DON_VI.Trim();
            this.spDangKy.MaDaiLy = GlobalSettings.MA_DAI_LY.Trim();
            try
            {
                MsgSend sendXML = new MsgSend();
                sendXML.LoaiHS = LoaiKhaiBao.SanPham;
                sendXML.master_id = spDangKy.ID;
                if (sendXML.Load())
                {
                    if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI)
                    {
                        ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "THÔNG TIN ĐÃ ĐƯỢC GỬI ĐẾN HẢI QUAN. NHẤN [LẤY PHẢN HỒI] ĐỂ LẤY THÔNG TIN", false);
                        cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                        XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                        return;
                    }
                }
                spDangKy.GUIDSTR = Guid.NewGuid().ToString();
                Company.KDT.SHARE.Components.SXXK_SanPham sanpham = new Company.KDT.SHARE.Components.SXXK_SanPham();
                if (Company.KDT.SHARE.Components.Globals.LaDNCX)
                {
                    sanpham = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferObject_DNCX_HangDuaRa(spDangKy, "", isCancel, GlobalSettings.TEN_DON_VI);
                }
                else
                {
 
                    sanpham = Company.BLL.DataTransferObjectMapper.Mapper_V4.ToDataTransferObject_SXXK_SP(spDangKy, isCancel, GlobalSettings.TEN_DON_VI);
                }
                ObjectSend msgSend = new ObjectSend(new NameBase()
                               {
                                   Name = GlobalSettings.TEN_DON_VI,
                                   Identity = spDangKy.MaDoanhNghiep,
                               },
                                new NameBase()
                                 {
                                     Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(spDangKy.MaHaiQuan),
                                     Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(spDangKy.MaHaiQuan).Trim() : spDangKy.MaHaiQuan
                                 },
                                  new SubjectBase()
                                {
                                    Type = Company.KDT.SHARE.Components.Globals.LaDNCX ? DeclarationIssuer.DNCX_HANG_RA : DeclarationIssuer.SXXK_SP,
                                    Function = sanpham.Function,
                                    Reference = spDangKy.GUIDSTR,
                                },
                                sanpham);

                switch (spDangKy.TrangThaiXuLy)
                {
                    case -1:
                        Log.LogSanPham(spDangKy, MessageTitle.KhaiBaoSanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        break;
                    case -5:
                        Log.LogSanPham(spDangKy, MessageTitle.KhaiBaoHuySanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        break;
                    case 5:
                        Log.LogSanPham(spDangKy, MessageTitle.KhaiBaoSuaSanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                        break;
                    default:
                        break;
                }
                SendMessageForm sendForm = new SendMessageForm();
                sendForm.Send += SendMessage;
                bool isSend = sendForm.DoSend(msgSend);
                if (isSend && feedbackContent.Function != DeclarationFunction.KHONG_CHAP_NHAN)
                {
                    sendForm.Message.XmlSaveMessage(spDangKy.ID, MessageTitle.KhaiBaoSanPham);
                    cmdSend.Enabled = cmdAdd.Enabled = cmdAddExcel.Enabled = cmdSave.Enabled = cmdSave1.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                    XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                    sendXML = new MsgSend();
                    sendXML.LoaiHS = LoaiKhaiBao.SanPham;
                    sendXML.func = 1;
                    sendXML.master_id = spDangKy.ID;
                    sendXML.msg = Helpers.Serializer(msgSend.Content, false, false);
                    sendXML.InsertUpdate();
                    if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN)
                    {
                        if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                        {
                            Log.LogSanPham(spDangKy, MessageTitle.KhaiBaoHQHuySanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            spDangKy.DeleteFull(spDangKy.MaDoanhNghiep, spDangKy.MaHaiQuan);
                        }
                        else
                        {
                            Log.LogSanPham(spDangKy, MessageTitle.KhaiBaoHQDaDuyetSanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            spDangKy.TransgferDataToSXXK();
                        }
                        ShowMessageTQDT(msgInfor, false);
                        SetCommandNew();
                    }
                    else if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                    {
                        FeedBackV5();
                        SetCommandNew();
                    }
                }
                else if (!string.IsNullOrEmpty(msgInfor))
                {
                    Log.LogSanPham(spDangKy, MessageTitle.KhaiBaoHQTuChoiSanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                    sendForm.Message.XmlSaveMessage(spDangKy.ID, MessageTitle.KhaiBaoSanPham);
                    ShowMessageTQDT(msgInfor, false);
                    SetCommandNew();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void FeedBackV5()
        {
            bool isFeedBack = true;
            int count = Company.KDT.SHARE.Components.Globals.CountSend;
            MsgSend sendXML = new MsgSend();
            sendXML.LoaiHS = LoaiKhaiBao.SanPham;
            sendXML.master_id = spDangKy.ID;
            if (!sendXML.Load())
            {
                if (ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN HOẶC HỆ THỐNG HQ BỊ QUÁ TẢI KHÔNG NHẬN ĐƯỢC PHẢN HỒI . NẾU TRƯỜNG HỢP HỆ THỐNG HQ BỊ QUÁ TẢI BẠN CHỌN YES ĐỂ TẠO MESSAGE NHẬN PHẢN HỒI . CHỌN NO ĐỂ BỎ QUA", true) == "Yes")
                {
                    sendXML.msg = String.Empty;
                    sendXML.func = Convert.ToInt32(DeclarationFunction.KHAI_BAO);
                    sendXML.InsertUpdate();
                }
                else
                {
                    ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "THÔNG TIN CHƯA ĐƯỢC GỬI ĐẾN HẢI QUAN. XIN KIỂM TRA LẠI", false);
                    return;
                }
            }
            while (isFeedBack)
            {
                string reference = spDangKy.GUIDSTR;

               SubjectBase subjectBase = new SubjectBase()
                {
                    Issuer = Company.KDT.SHARE.Components.Globals.LaDNCX ? DeclarationIssuer.DNCX_HANG_RA : DeclarationIssuer.SXXK_SP,
                    Reference = reference,
                    Function = DeclarationFunction.HOI_TRANG_THAI,
                    Type = Company.KDT.SHARE.Components.Globals.LaDNCX ? DeclarationIssuer.DNCX_HANG_RA : DeclarationIssuer.SXXK_SP,

                };

                ObjectSend msgSend = new ObjectSend(
                                            new NameBase()
                                            {
                                                Name = GlobalSettings.TEN_DON_VI,
                                                Identity = spDangKy.MaDoanhNghiep
                                            },
                                              new NameBase()
                                              {
                                                  Name = Company.KDT.SHARE.Components.DuLieuChuan.DonViHaiQuan.GetName(spDangKy.MaHaiQuan.Trim()),
                                                  Identity = Company.KDT.SHARE.Components.Globals.IsKhaiVNACCS ? Company.KDT.SHARE.Components.DuLieuChuan.VNACCS_Mapper.GetCodeVNACCMaHaiQuan(spDangKy.MaHaiQuan).Trim() : spDangKy.MaHaiQuan
                                              }, subjectBase, null);
                while (isFeedBack)
                {
                    SendMessageForm dlgSendForm = new SendMessageForm();
                    dlgSendForm.Send += SendMessage;
                    isFeedBack = dlgSendForm.DoSend(msgSend);
                    if (isFeedBack)
                    {
                        if (feedbackContent.Function == DeclarationFunction.CHUA_XU_LY)
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG?", true) == "Yes";
                        }
                        else if (feedbackContent.Function == DeclarationFunction.CAP_SO_TIEP_NHAN || feedbackContent.Function == DeclarationFunction.THONG_QUAN && count > 0)
                        {
                            if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
                            {
                                Log.LogSanPham(spDangKy, MessageTitle.KhaiBaoHQHuySanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                                spDangKy.DeleteFull(spDangKy.MaDoanhNghiep, spDangKy.MaHaiQuan);
                            }
                            else
                            {
                                Log.LogSanPham(spDangKy, MessageTitle.KhaiBaoHQDaDuyetSanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                                spDangKy.TransgferDataToSXXK();
                            }
                            isFeedBack = false;
                            SetCommandNew();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else if (feedbackContent.Function == DeclarationFunction.KHONG_CHAP_NHAN)
                        {
                            Log.LogSanPham(spDangKy, MessageTitle.KhaiBaoHQTuChoiSanPham, ((SiteIdentity)MainForm.EcsQuanTri.Identity).user.USER_NAME);
                            spDangKy.Update();
                            isFeedBack = false;
                            SetCommandNew();
                            ShowMessageTQDT(msgInfor, false);
                        }
                        else
                        {
                            isFeedBack = ShowMessageTQDT(msgInfor + "\rDOANH NGHIỆP CÓ MUỐN NHẬN PHẢN HỒI TIẾP KHÔNG?", true) == "Yes";
                        }
                    }
                }
            }
        }
        void SendMessage(object sender, SendEventArgs e)
        {
            this.Invoke(
                new EventHandler<SendEventArgs>(SendHandler),
                sender, e);
        }
        /// <summary>
        /// Xử lý message trả về từ hải quan.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void SendHandler(object sender, SendEventArgs e)
        {
            {
                feedbackContent = SingleMessage.SanPhamSendHandler(spDangKy, ref msgInfor, e);
            }
        }

        #endregion
        private void GetData()
        {
            try
            {
                if (SP == null)
                    SP = new SanPham();
                SP.Ma = txtMa.Text.ToString().Trim();
                SP.Ten = txtTen.Text.ToString().Trim();
                SP.MaHS = txtMaHS.Text.ToString();
                SP.DVT_ID = cbDonViTinh.SelectedValue.ToString();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetData()
        {
            try
            {
                txtMa.Text = SP.Ma;
                txtTen.Text = SP.Ten;
                txtMaHS.Text = SP.MaHS;
                cbDonViTinh.SelectedValue = SP.DVT_ID;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private bool ValidateForm(bool isOnlyWarning)
        {
            bool isValid = true;
            try
            {
                Cursor = Cursors.WaitCursor;
                isValid &= ValidateControl.ValidateNull(txtMa, errorProvider, "Mã Sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateSpecialChar(txtMa, errorProvider, "Mã Sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtTen, errorProvider, "Tên Sản phẩm", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(txtMaHS, errorProvider, "Mã số hàng hóa", isOnlyWarning);
                isValid &= ValidateControl.ValidateNull(cbDonViTinh, errorProvider, "Đơn vị tính", isOnlyWarning);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                Helper.Controls.MessageBoxControlV.ShowMessage(string.Format(Company.KDT.SHARE.Components.ThongBao.APPLICATION_ERROR_01Param, ex.Message), false);
            }
            finally { Cursor = Cursors.Default; }

            return isValid;
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm(false))
                    return;
                GetData();
                if (isAdd)
                {
                    bool isExits = false;
                    // KIỂM TRA NPL ĐÃ NHẬP TRƯỚC ĐÓ
                    foreach (SanPham item in spDangKy.SPCollection)
                    {
                        if (item.Ma == SP.Ma)
                        {
                            errorProvider.SetError(txtMa, "SẢN PHẨM NÀY ĐÃ ĐƯỢC NHẬP Ở DANH SÁCH PHÍA DƯỚI .");
                            isExits = true;
                            return;
                        }
                    }
                    // KIỂM TRA NPL ĐÃ ĐĂNG KÝ TRƯỚC ĐÓ
                    if (!isEdit || !isCancel)
                    {
                        foreach (SanPham item in SPCollection)
                        {
                            if (item.Ma == SP.Ma && item.Master_ID != spDangKy.ID)
                            {
                                SanPhamDangKy SPExits = new SanPhamDangKy();
                                SPExits.ID = item.Master_ID;
                                SPExits.Load();
                                string Error = "";
                                Error += "\nID : " + SPExits.ID;
                                Error += "\nSỐ TIẾP NHẬN : " + SPExits.SoTiepNhan.ToString();
                                Error += "\nNGÀY TIẾP NHẬN : " + SPExits.NgayTiepNhan.ToString("dd/MM/yyyy");
                                switch (SPExits.TrangThaiXuLy)
                                {
                                    case -4:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ KHAI BÁO HỦY";
                                        break;
                                    case -3:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ KHAI BÁO";
                                        break;
                                    case -2:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : CHỜ DUYỆT KHAI BÁO SỬA";
                                        break;
                                    case -1:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : CHƯA KHAI BÁO";
                                        break;
                                    case 0:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : CHỜ DUYỆT";
                                        break;
                                    case 1:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ DUYỆT";
                                        break;
                                    case 2:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : KHÔNG PHÊ DUYỆT";
                                        break;
                                    case 4:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ KHAI BÁO SỬA";
                                        break;
                                    case 5:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐANG SỬA";
                                        break;
                                    case 10:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : ĐÃ  HỦY";
                                        break;
                                    case 11:
                                        Error += "\nTRẠNG THÁI XỬ LÝ : CHỜ HỦY";
                                        break;
                                    default:
                                        break;
                                }
                                ShowMessageTQDT("THÔNG BÁO TỪ HỆ THỐNG", "SẢN PHẨM NÀY ĐÃ ĐƯỢC ĐĂNG KÝ TRƯỚC ĐÓ VỚI THÔNG TIN " + Error, false);
                                //errorProvider.SetError(txtMa, "Sản phẩm này đã được đăng ký\n" + Error);
                                isExits = true;
                                return;
                            }
                        }
                        if (!isExits)
                        {
                            spDangKy.SPCollection.Add(SP);
                        }
                    }
                    else
                    {
                        // KIỂM TRA MÃ SP NÀY ĐÃ ĐĂNG KÝ HAY CHƯA . CHỈ NHỮNG SP ĐÃ ĐĂNG KÝ MỚI ĐƯỢC PHÉP KHAI BÁO SỬA HOẶC HỦY
                        isExits = false;
                        foreach (SanPham item in SPCollection)
                        {
                            if (item.Ma == SP.Ma)
                            {
                                errorProvider.SetError(txtMa, "SẢN PHẨM NÀY CHƯA ĐƯỢC ĐĂNG KÝ CHỈ NHỮNG SP ĐÃ ĐĂNG KÝ MỚI ĐƯỢC PHÉP KHAI BÁO SỬA HOẶC HỦY .");
                                isExits = true;
                                return;
                            }
                        }
                        if (!isExits)
                        {
                            spDangKy.SPCollection.Add(SP);   
                        }
                    }
                }
                BindData();
                SP = new SanPham();
                txtMa.Text = String.Empty;
                txtTen.Text = String.Empty;
                txtMaHS.Text = String.Empty;
                isAdd = true;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void BindData()
        {
            try
            {
                dgList.Refresh();
                dgList.DataSource = this.spDangKy.SPCollection;
                dgList.Refetch();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
        private void SetCommandNew()
        {
            if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_DUYET)
            {
                btnAdd.Enabled = false;
                btnAddExcel.Enabled = false;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = spDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã duyệt", "Approved");
                this.OpenType = OpenFormType.View;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO || spDangKy.TrangThaiXuLy == TrangThaiXuLy.KHONG_PHE_DUYET)
            {
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel1.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = true;
                btnAddExcel.Enabled = true;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                dtpNgayTN.Value = DateTime.Now;
                lblTrangThai.Text = spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHUA_KHAI_BAO ? setText("Chưa khai báo", "Wait for approval") : setText("Không phê duyệt", "Wait for cancel");
                this.OpenType = OpenFormType.Edit;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_HUY)
            {
                btnAdd.Enabled = false;
                btnAddExcel.Enabled = false;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = spDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đã hủy", "Canceled");
                this.OpenType = OpenFormType.View;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI || spDangKy.TrangThaiXuLy == TrangThaiXuLy.HUY_KHAI_BAO || spDangKy.TrangThaiXuLy == TrangThaiXuLy.SUA_KHAI_BAO)
            {
                btnAdd.Enabled = false;
                btnAddExcel.Enabled = false;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;

                lblTrangThai.Text = spDangKy.TrangThaiXuLy == TrangThaiXuLy.DA_KHAI_BAO_NHUNG_CHUA_CO_PHAN_HOI ? setText("Đã khai báo", "Not declared") : setText("Đã khai báo Sửa / Hủy", "Not declared");
                this.OpenType = OpenFormType.View;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.SUATKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = true;
                btnAddExcel.Enabled = true;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = spDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang sửa", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.HUYTKDADUYET)
            {
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;

                btnAdd.Enabled = true;
                btnAddExcel.Enabled = true;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                btnDelete.Enabled = true;

                txtSoTiepNhan.Text = spDangKy.SoTiepNhan.ToString();
                dtpNgayTN.Value = spDangKy.NgayTiepNhan;
                lblTrangThai.Text = setText("Đang hủy", "Not declared");
                this.OpenType = OpenFormType.Edit;
            }
            else if (spDangKy.TrangThaiXuLy == TrangThaiXuLy.CHO_DUYET)
            {
                btnAdd.Enabled = false;
                btnAddExcel.Enabled = false;
                cmdSave.Enabled = cmdSave1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSend.Enabled = cmdSend1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdEdit.Enabled = cmdEdit1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdCancel.Enabled = cmdCancel1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                InPhieuTN.Enabled = InPhieuTN1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                cmdSuaSanPham.Enabled = cmdSuaSanPham1.Enabled = Janus.Windows.UI.InheritableBoolean.False;
                btnDelete.Enabled = false;

                XacNhan.Enabled = XacNhan1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdUpdateGuidString.Enabled = cmdUpdateGuidString1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                cmdResult.Enabled = cmdResult1.Enabled = Janus.Windows.UI.InheritableBoolean.True;
                lblTrangThai.Text = setText("Chờ duyệt", "Wait approved");
                this.OpenType = OpenFormType.View;
            }


        }
        private void btnAddExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SanPhamReadExcelForm f = new SanPhamReadExcelForm();
                f.SPCollection = this.spDangKy.SPCollection;
                f.isEdit = isEdit;
                f.isCancel = isCancel;
                f.ShowDialog(this);
                BindData();
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void dgList_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                if (this.OpenType == OpenFormType.View)
                {
                    GridEXSelectedItemCollection items = dgList.SelectedItems;
                    foreach (GridEXSelectedItem i in items)
                    {
                        if (i.RowType == RowType.Record)
                        {
                            SP = new SanPham();
                            SP = (SanPham)i.GetRow().DataRow;
                            SetData();
                            isAdd = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            } 
        }

        private void dgList_FormattingRow(object sender, RowLoadEventArgs e)
        {

        }

        private void btnExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog sfNPL = new SaveFileDialog();
                sfNPL.FileName = "DANH SÁCH SẢN PHẨM KHAI BÁO.xls";
                sfNPL.Filter = "Excel files| *.xls";
                if (sfNPL.ShowDialog(this) != DialogResult.Abort || sfNPL.FileName != "")
                {
                    Janus.Windows.GridEX.Export.GridEXExporter gridEXExporter1 = new Janus.Windows.GridEX.Export.GridEXExporter();
                    gridEXExporter1.GridEX = dgList;
                    System.IO.Stream str = sfNPL.OpenFile();
                    gridEXExporter1.Export(str);
                    str.Close();

                    if (ShowMessage("Bạn có muốn mở file này không?", true) == "Yes")
                    {
                        System.Diagnostics.Process.Start(sfNPL.FileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMa_ButtonClick(object sender, EventArgs e)
        {
            try
            {
                this.SPRegistedForm = new SanPhamRegistedForm();
                this.SPRegistedForm.CalledForm = "HangMauDichForm";
                this.SPRegistedForm.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                this.SPRegistedForm.ShowDialog();
                bool isExits = false;
                if (!string.IsNullOrEmpty(this.SPRegistedForm.SanPhamSelected.Ma))
                {
                    if (spDangKy.SPCollection.Count >=1)
                    {
                        isExits = false;
                        foreach (SanPham item in spDangKy.SPCollection)
                        {
                            if (item.Ma == this.SPRegistedForm.SanPhamSelected.Ma)
                            {
                                isExits = true;
                                break;
                            }
                        }
                        if (!isExits)
                        {
                            txtMa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                            txtTen.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                            txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                            cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                        }
                    }
                    else
                    {
                        txtMa.Text = this.SPRegistedForm.SanPhamSelected.Ma;
                        txtTen.Text = this.SPRegistedForm.SanPhamSelected.Ten;
                        txtMaHS.Text = this.SPRegistedForm.SanPhamSelected.MaHS;
                        cbDonViTinh.SelectedValue = this.SPRegistedForm.SanPhamSelected.DVT_ID.PadRight(3);
                    }
                }
                else if (this.SPRegistedForm.SanPhamsCollectionSelected.Count >=1)
                {
                    SanPhamCollection SPSelectCollection = new SanPhamCollection();
                    isExits = false;
                    foreach (Company.BLL.SXXK.SanPham item in SPRegistedForm.SanPhamsCollectionSelected)
                    {
                        if (spDangKy.SPCollection.Count >=1)
                        {
                            foreach (SanPham ite in spDangKy.SPCollection)
                            {
                                isExits = false;
                                if (ite.Ma == item.Ma)
                                {
                                    isExits = true;
                                    break;
                                }
                            }
                            if (!isExits)
                            {
                                SP = new SanPham();
                                SP.Ma = item.Ma;
                                SP.Ten = item.Ten;
                                SP.MaHS = item.MaHS;
                                SP.DVT_ID = item.DVT_ID;
                                SPSelectCollection.Add(SP);  
                            }
                        }
                        else
                        {
                            SP = new SanPham();
                            SP.Ma = item.Ma;
                            SP.Ten = item.Ten;
                            SP.MaHS = item.MaHS;
                            SP.DVT_ID = item.DVT_ID;
                            spDangKy.SPCollection.Add(SP);
                        }
                    }
                    foreach (SanPham item in SPSelectCollection)
                    {
                        spDangKy.SPCollection.Add(item);
                    }
                    BindData();
                }
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMa_Leave(object sender, EventArgs e)
        {
            try
            {
                //Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                //sp.Ma = txtMa.Text;
                //sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                //sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //if (sp.Load())
                //{
                //    txtMa.Text = sp.Ma;
                //    cbDonViTinh.SelectedValue = sp.DVT_ID.PadRight(3);
                //    txtTen.Text = sp.Ten;
                //    txtMaHS.Text = sp.MaHS;
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void txtMa_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //Company.BLL.SXXK.SanPham sp = new Company.BLL.SXXK.SanPham();
                //sp.Ma = txtMa.Text;
                //sp.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                //sp.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                //if (sp.Load())
                //{
                //    txtMa.Text = sp.Ma;
                //    cbDonViTinh.SelectedValue = sp.DVT_ID.PadRight(3);
                //    txtTen.Text = sp.Ten;
                //    txtMaHS.Text = sp.MaHS;
                //}
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GridEXFilterCondition filter = new GridEXFilterCondition(dgList.RootTable.Columns["Ma"], ConditionOperator.Contains, txtMaSP.Text);
                dgList.RootTable.FilterCondition = filter;
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
            }
        }
    }
}