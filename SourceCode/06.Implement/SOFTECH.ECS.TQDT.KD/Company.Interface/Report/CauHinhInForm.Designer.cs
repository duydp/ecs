﻿namespace Company.Interface.Report
{
    partial class CauHinhInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CauHinhInForm));
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem1 = new Janus.Windows.EditControls.UIComboBoxItem();
            Janus.Windows.EditControls.UIComboBoxItem uiComboBoxItem2 = new Janus.Windows.EditControls.UIComboBoxItem();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDinhMuc = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSave = new Janus.Windows.EditControls.UIButton();
            this.uiGroupBox1 = new Janus.Windows.EditControls.UIGroupBox();
            this.txtSoLuongHMD = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTrongLuongTK = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLuongSP = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLuongNPL = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.txtFontTenHang = new Janus.Windows.GridEX.EditControls.NumericEditBox();
            this.label6 = new System.Windows.Forms.Label();
            this.uiGroupBox2 = new Janus.Windows.EditControls.UIGroupBox();
            this.uiGroupBox3 = new Janus.Windows.EditControls.UIGroupBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.numTimeDelay = new System.Windows.Forms.NumericUpDown();
            this.uiGroupBox4 = new Janus.Windows.EditControls.UIGroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cbHienThiHoaDonTKX = new Janus.Windows.EditControls.UIComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).BeginInit();
            this.grbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).BeginInit();
            this.uiGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).BeginInit();
            this.uiGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).BeginInit();
            this.uiGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeDelay)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).BeginInit();
            this.uiGroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbMain
            // 
            this.grbMain.Controls.Add(this.uiGroupBox4);
            this.grbMain.Controls.Add(this.uiGroupBox3);
            this.grbMain.Controls.Add(this.uiGroupBox2);
            this.grbMain.Controls.Add(this.uiGroupBox1);
            this.grbMain.Controls.Add(this.btnSave);
            this.grbMain.Size = new System.Drawing.Size(356, 389);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(45, 33);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Định mức";
            // 
            // txtDinhMuc
            // 
            this.txtDinhMuc.Location = new System.Drawing.Point(136, 28);
            this.txtDinhMuc.Name = "txtDinhMuc";
            this.txtDinhMuc.Size = new System.Drawing.Size(121, 21);
            this.txtDinhMuc.TabIndex = 1;
            this.txtDinhMuc.Text = "0";
            this.txtDinhMuc.Value = 0;
            this.txtDinhMuc.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtDinhMuc.VisualStyleManager = this.vsmMain;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Location = new System.Drawing.Point(45, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Lượng NPL         ";
            // 
            // btnSave
            // 
            this.btnSave.Icon = ((System.Drawing.Icon)(resources.GetObject("btnSave.Icon")));
            this.btnSave.Location = new System.Drawing.Point(148, 359);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(65, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "&Lưu";
            this.btnSave.VisualStyleManager = this.vsmMain;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // uiGroupBox1
            // 
            this.uiGroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox1.Controls.Add(this.txtSoLuongHMD);
            this.uiGroupBox1.Controls.Add(this.label4);
            this.uiGroupBox1.Controls.Add(this.txtTrongLuongTK);
            this.uiGroupBox1.Controls.Add(this.label5);
            this.uiGroupBox1.Controls.Add(this.label2);
            this.uiGroupBox1.Controls.Add(this.label1);
            this.uiGroupBox1.Controls.Add(this.txtDinhMuc);
            this.uiGroupBox1.Controls.Add(this.txtLuongSP);
            this.uiGroupBox1.Controls.Add(this.label3);
            this.uiGroupBox1.Controls.Add(this.txtLuongNPL);
            this.uiGroupBox1.Location = new System.Drawing.Point(12, 11);
            this.uiGroupBox1.Name = "uiGroupBox1";
            this.uiGroupBox1.Size = new System.Drawing.Size(325, 173);
            this.uiGroupBox1.TabIndex = 0;
            this.uiGroupBox1.Text = "Số thập phân";
            this.uiGroupBox1.VisualStyleManager = this.vsmMain;
            // 
            // txtSoLuongHMD
            // 
            this.txtSoLuongHMD.Location = new System.Drawing.Point(136, 140);
            this.txtSoLuongHMD.Name = "txtSoLuongHMD";
            this.txtSoLuongHMD.Size = new System.Drawing.Size(121, 21);
            this.txtSoLuongHMD.TabIndex = 5;
            this.txtSoLuongHMD.Text = "0";
            this.txtSoLuongHMD.Value = 0;
            this.txtSoLuongHMD.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtSoLuongHMD.VisualStyleManager = this.vsmMain;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Location = new System.Drawing.Point(45, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Số lượng HMD";
            // 
            // txtTrongLuongTK
            // 
            this.txtTrongLuongTK.Location = new System.Drawing.Point(136, 110);
            this.txtTrongLuongTK.Name = "txtTrongLuongTK";
            this.txtTrongLuongTK.Size = new System.Drawing.Size(121, 21);
            this.txtTrongLuongTK.TabIndex = 4;
            this.txtTrongLuongTK.Text = "0";
            this.txtTrongLuongTK.Value = 0;
            this.txtTrongLuongTK.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtTrongLuongTK.VisualStyleManager = this.vsmMain;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Location = new System.Drawing.Point(45, 115);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Trọng lượng TK";
            // 
            // txtLuongSP
            // 
            this.txtLuongSP.Location = new System.Drawing.Point(136, 82);
            this.txtLuongSP.Name = "txtLuongSP";
            this.txtLuongSP.Size = new System.Drawing.Size(121, 21);
            this.txtLuongSP.TabIndex = 3;
            this.txtLuongSP.Text = "0";
            this.txtLuongSP.Value = 0;
            this.txtLuongSP.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtLuongSP.VisualStyleManager = this.vsmMain;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(45, 87);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Lượng SP";
            // 
            // txtLuongNPL
            // 
            this.txtLuongNPL.Location = new System.Drawing.Point(136, 55);
            this.txtLuongNPL.Name = "txtLuongNPL";
            this.txtLuongNPL.Size = new System.Drawing.Size(121, 21);
            this.txtLuongNPL.TabIndex = 2;
            this.txtLuongNPL.Text = "0";
            this.txtLuongNPL.Value = 0;
            this.txtLuongNPL.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtLuongNPL.VisualStyleManager = this.vsmMain;
            // 
            // txtFontTenHang
            // 
            this.txtFontTenHang.Location = new System.Drawing.Point(136, 20);
            this.txtFontTenHang.Name = "txtFontTenHang";
            this.txtFontTenHang.Size = new System.Drawing.Size(118, 21);
            this.txtFontTenHang.TabIndex = 0;
            this.txtFontTenHang.Text = "0";
            this.txtFontTenHang.Value = 0;
            this.txtFontTenHang.ValueType = Janus.Windows.GridEX.NumericEditValueType.Int32;
            this.txtFontTenHang.VisualStyleManager = this.vsmMain;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Location = new System.Drawing.Point(45, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(77, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Font Tên hàng";
            // 
            // uiGroupBox2
            // 
            this.uiGroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox2.Controls.Add(this.label6);
            this.uiGroupBox2.Controls.Add(this.txtFontTenHang);
            this.uiGroupBox2.Location = new System.Drawing.Point(12, 190);
            this.uiGroupBox2.Name = "uiGroupBox2";
            this.uiGroupBox2.Size = new System.Drawing.Size(325, 49);
            this.uiGroupBox2.TabIndex = 1;
            this.uiGroupBox2.Text = "Font";
            this.uiGroupBox2.VisualStyleManager = this.vsmMain;
            // 
            // uiGroupBox3
            // 
            this.uiGroupBox3.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox3.Controls.Add(this.label35);
            this.uiGroupBox3.Controls.Add(this.label34);
            this.uiGroupBox3.Controls.Add(this.numTimeDelay);
            this.uiGroupBox3.Location = new System.Drawing.Point(12, 245);
            this.uiGroupBox3.Name = "uiGroupBox3";
            this.uiGroupBox3.Size = new System.Drawing.Size(325, 49);
            this.uiGroupBox3.TabIndex = 2;
            this.uiGroupBox3.Text = "Khai báo";
            this.uiGroupBox3.VisualStyleManager = this.vsmMain;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.Transparent;
            this.label35.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(260, 33);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(35, 13);
            this.label35.TabIndex = 41;
            this.label35.Text = "(giây)";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label34
            // 
            this.label34.BackColor = System.Drawing.Color.Transparent;
            this.label34.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(25, 12);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(100, 32);
            this.label34.TabIndex = 40;
            this.label34.Text = "Thời gian chờ gửi và nhận thông tin";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // numTimeDelay
            // 
            this.numTimeDelay.Location = new System.Drawing.Point(136, 17);
            this.numTimeDelay.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numTimeDelay.Name = "numTimeDelay";
            this.numTimeDelay.Size = new System.Drawing.Size(118, 21);
            this.numTimeDelay.TabIndex = 0;
            this.numTimeDelay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // uiGroupBox4
            // 
            this.uiGroupBox4.BackColor = System.Drawing.Color.Transparent;
            this.uiGroupBox4.Controls.Add(this.cbHienThiHoaDonTKX);
            this.uiGroupBox4.Controls.Add(this.label8);
            this.uiGroupBox4.Location = new System.Drawing.Point(12, 300);
            this.uiGroupBox4.Name = "uiGroupBox4";
            this.uiGroupBox4.Size = new System.Drawing.Size(325, 49);
            this.uiGroupBox4.TabIndex = 3;
            this.uiGroupBox4.Text = "Tờ khai";
            this.uiGroupBox4.VisualStyleManager = this.vsmMain;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(25, 12);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 32);
            this.label8.TabIndex = 0;
            this.label8.Text = "Cho phép nhập Hóa đơn TK Xuất";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbHienThiHoaDonTKX
            // 
            this.cbHienThiHoaDonTKX.ComboStyle = Janus.Windows.EditControls.ComboStyle.DropDownList;
            uiComboBoxItem1.FormatStyle.Alpha = 0;
            uiComboBoxItem1.IsSeparator = false;
            uiComboBoxItem1.Text = "Không";
            uiComboBoxItem1.Value = 0;
            uiComboBoxItem2.FormatStyle.Alpha = 0;
            uiComboBoxItem2.IsSeparator = false;
            uiComboBoxItem2.Text = "Có";
            uiComboBoxItem2.Value = 1;
            this.cbHienThiHoaDonTKX.Items.AddRange(new Janus.Windows.EditControls.UIComboBoxItem[] {
            uiComboBoxItem1,
            uiComboBoxItem2});
            this.cbHienThiHoaDonTKX.Location = new System.Drawing.Point(136, 17);
            this.cbHienThiHoaDonTKX.Name = "cbHienThiHoaDonTKX";
            this.cbHienThiHoaDonTKX.Size = new System.Drawing.Size(118, 21);
            this.cbHienThiHoaDonTKX.TabIndex = 1;
            this.cbHienThiHoaDonTKX.VisualStyleManager = this.vsmMain;
            // 
            // CauHinhInForm
            // 
            this.ClientSize = new System.Drawing.Size(356, 389);
            this.helpProvider1.SetHelpKeyword(this, "BaseForm.htm");
            this.helpProvider1.SetHelpNavigator(this, System.Windows.Forms.HelpNavigator.Topic);
            this.MaximizeBox = false;
            this.Name = "CauHinhInForm";
            this.helpProvider1.SetShowHelp(this, true);
            this.Text = "Cấu hình hệ thống";
            this.Load += new System.EventHandler(this.CauHinhInForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grbMain)).EndInit();
            this.grbMain.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox1)).EndInit();
            this.uiGroupBox1.ResumeLayout(false);
            this.uiGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox2)).EndInit();
            this.uiGroupBox2.ResumeLayout(false);
            this.uiGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox3)).EndInit();
            this.uiGroupBox3.ResumeLayout(false);
            this.uiGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeDelay)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.uiGroupBox4)).EndInit();
            this.uiGroupBox4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtDinhMuc;
        private System.Windows.Forms.Label label1;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox1;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongSP;
        private System.Windows.Forms.Label label3;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtLuongNPL;
        private Janus.Windows.EditControls.UIButton btnSave;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtTrongLuongTK;
        private System.Windows.Forms.Label label5;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtSoLuongHMD;
        private System.Windows.Forms.Label label4;
        private Janus.Windows.GridEX.EditControls.NumericEditBox txtFontTenHang;
        private System.Windows.Forms.Label label6;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox2;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox3;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.NumericUpDown numTimeDelay;
        private Janus.Windows.EditControls.UIGroupBox uiGroupBox4;
        private System.Windows.Forms.Label label8;
        private Janus.Windows.EditControls.UIComboBox cbHienThiHoaDonTKX;
    }
}
