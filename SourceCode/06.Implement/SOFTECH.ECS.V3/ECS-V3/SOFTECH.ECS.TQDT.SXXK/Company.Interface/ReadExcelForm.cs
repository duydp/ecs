﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Infragistics.Excel;
using Company.BLL.KDT;
using GemBox.Spreadsheet;
using Company.BLL.SXXK;
//using Company.BLL.SXXK;
namespace Company.Interface
{
    public partial class ReadExcelForm : BaseForm
    {
        public List<HangMauDich> hmdCollection = new List<HangMauDich>();
        public Decimal TiGiaTT;
        public ToKhaiMauDich TKMD;            
        public ReadExcelForm()
        {
            InitializeComponent();
        }

        private void editBox8_TextChanged(object sender, EventArgs e)
        {

        }
        //Hien thi Dialog box 
        private void btnSelectFile_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog(this);
            txtFilePath.Text = openFileDialog1.FileName;
        }
        private int ConvertCharToInt(char ch)
        {
            return ch - 'A';
        }
        
        private int checkNPLExit(string maNPL)
        {
            for (int i = 0; i < this.hmdCollection.Count; i++)
            {
                if (this.hmdCollection[i].MaPhu.ToUpper() == maNPL.ToUpper()) return i;
            }
            return -1;
        }
      
        private void txtTriGiaColumn_TextChanged(object sender, EventArgs e)
        {

        }

      

        private void btnSave_Click(object sender, EventArgs e)
        {
            cvError.Validate();
            if (!cvError.IsValid) return;
            int beginRow = Convert.ToInt32(txtRow.Value) - 1;
            if (beginRow < 0)
            {
                
                if (GlobalSettings.NGON_NGU == "0")
                {
                    error.SetError(txtRow, "Dòng bắt đầu phải lớn hơn 0");
                }
                else
                {
                    error.SetError(txtRow, "Begin row must be greater than zero ");
                }
                error.SetIconPadding(txtRow, 8);
                return;

            }
            Workbook wb = new Workbook();
            Worksheet ws = null;
            try
            {
                wb = Workbook.Load(txtFilePath.Text,true);
            }
            catch (Exception ex)
            {
                Logger.LocalLogger.Instance().WriteMessage(ex);
                ShowMessage(ex.Message, false);
                return;
            }
            try
            {
                ws = wb.Worksheets[txtSheet.Text];
            }
            catch
            {
               // ShowMessage("Không tồn tại sheet \"" + txtSheet.Text + "\"", false);
                MLMessages("Không tồn tại sheet \"" + txtSheet.Text + "\"", "MSG_EXC01", txtSheet.Text, false);
                return;
            }
            //editing :
            WorksheetRowCollection wsrc = ws.Rows;
            char maHangColumn = Convert.ToChar(txtMaHangColumn.Text.Trim ());
            int maHangCol = ConvertCharToInt(maHangColumn);

            char tenHangColumn = Convert.ToChar(txtTenHangColumn.Text.Trim());
            int tenHangCol = ConvertCharToInt(tenHangColumn);

            char maHSColumn = Convert.ToChar(txtMaHSColumn.Text.Trim());
            int maHSCol = ConvertCharToInt(maHSColumn);

            char dVTColumn = Convert.ToChar(txtDVTColumn.Text.Trim());
            int dVTCol = ConvertCharToInt(dVTColumn);

            char soLuongColumn = Convert.ToChar(txtSoLuongColumn.Text.Trim());
            int soLuongCol = ConvertCharToInt(soLuongColumn);

            char donGiaColumn = Convert.ToChar(txtDonGiaColumn.Text.Trim());
            int donGiaCol = ConvertCharToInt(donGiaColumn);

            char tsXNKColumn = Convert.ToChar(txtTSXNK.Text.Trim());
            int tsXNKCol = ConvertCharToInt(tsXNKColumn);

            char xuatXuColumn = Convert.ToChar(txtXuatXuColumn.Text.Trim());
            int xuatXuCol = ConvertCharToInt(xuatXuColumn);

            char triGiaTTColumn = Convert.ToChar(txtTGTT.Text);
            int triGiaTTCol = ConvertCharToInt(triGiaTTColumn);

            foreach (WorksheetRow wsr in wsrc)
            {
                if (wsr.Index >= beginRow)
                {
                    try
                    {
                        //NguyenPhuLieu npl = new NguyenPhuLieu();                       
                        HangMauDich hmd = new HangMauDich();
                        hmd.MaHS = Convert.ToString(wsr.Cells[maHSCol].Value).Trim();
                        hmd.MaPhu = Convert.ToString(wsr.Cells[maHangCol].Value).Trim();
                        if (hmd.MaPhu.Trim() == "") continue;
                        hmd.TenHang = Convert.ToString(wsr.Cells[tenHangCol].Value).Trim();
                        hmd.SoLuong = Convert.ToDecimal(wsr.Cells[soLuongCol].Value);
                        hmd.DonGiaKB = Convert.ToDecimal(wsr.Cells[donGiaCol].Value);
                        hmd.ThueSuatXNK  = Convert.ToDecimal(wsr.Cells[tsXNKCol].Value);
                        hmd.NuocXX_ID = Convert.ToString(wsr.Cells[xuatXuCol].Value).Trim();

                        try
                        {
                            string nuocxx = this.Nuoc_GetName(hmd.NuocXX_ID);
                        }
                        catch
                        {
                            ShowMessage("Mã nước : " + hmd.NuocXX_ID + "tại dòng thứ :" + (wsr.Index+1) + " không tồn tại trong hệ thống.Hãy kiểm tra lại",false);
                            return;
                        }
                        
                        if (TKMD.LoaiHangHoa == "N")
                        {
                            NguyenPhuLieu NPL = new NguyenPhuLieu();
                            NPL.Ma = hmd.MaPhu;
                            NPL.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            NPL.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            if (!NPL.Load())
                            {
                                MLMessages("Nguyên phụ liệu có mã " + NPL.Ma + " chưa không có trong hệ thống.Hãy kiểm tra lại.", "MSG_NPL10", "" + NPL.Ma, false);
                                return;
                            }
                            else
                            {
                                hmd.DVT_ID = NPL.DVT_ID;
                            }
                        }
                        else
                        {
                            SanPham SP = new SanPham();
                            SP.Ma = hmd.MaPhu;
                            SP.MaDoanhNghiep = GlobalSettings.MA_DON_VI;
                            SP.MaHaiQuan = GlobalSettings.MA_HAI_QUAN;
                            if (!SP.Load())
                            {
                                MLMessages("Sản phẩm có mã " + SP.Ma + " chưa không có trong hệ thống.Hãy kiểm tra lại.", "MSG_SP11", "" + SP.Ma, false);
                                return;
                            }
                            else
                            {
                                hmd.DVT_ID = SP.DVT_ID;
                            }
                        }
                        hmd.DonGiaTT = hmd.DonGiaKB * TiGiaTT;
                        hmd.TriGiaKB = hmd.DonGiaKB * hmd.SoLuong;
                        hmd.TriGiaTT = Convert.ToDecimal(wsr.Cells[triGiaTTCol].Value);
                        hmd.ThueXNK = hmd.TriGiaTT * hmd.ThueSuatXNK / 100;
                        if (checkNPLExit(hmd.MaPhu) >= 0)
                        {//tiep tuc
                            //if (ShowMessage("Sản phẩm có mã \"" + hmd.MaPhu + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế sản phẩm trên lưới bằng sản phẩm này?", true) == "Yes")
                            if (MLMessages("Hàng có mã \"" + hmd.MaPhu + "\" thuộc dòng " + (wsr.Index + 1) + " trong file Excel đã có trên lưới. Bạn có muốn thay thế sản phẩm trên lưới bằng sản phẩm này?", "MSG_EXC09", "", true) == "Yes")
                                this.hmdCollection[checkNPLExit(hmd.MaPhu)] = hmd;

                        }
                        else this.hmdCollection.Add(hmd);
                        this.SaveDefault();

                       
                    }
                    catch
                    {
                       // if (ShowMessage("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\nBạn có muốn tiếp tục không?", true) != "Yes")
                        if (MLMessages("Dữ liệu ở dòng " + (wsr.Index + 1) + " không hợp lệ.\n Bạn có muốn tiếp tục không?", "MSG_EXC06", "", true) != "Yes")
                        {
                            this.hmdCollection.Clear();
                            return;
                        }
                    }
                }
            }
            this.Close();

        }
        private void SaveDefault()
        {
            DocExcel docExcel = new DocExcel();
            docExcel.insertThongSoHangTK(txtSheet.Text.Trim(),
                                                                  txtRow.Text.Trim(),
                                                                  txtMaHangColumn.Text.Trim(),
                                                                  txtTenHangColumn.Text.Trim(),
                                                                  txtMaHSColumn.Text.Trim(),
                                                                  txtDVTColumn.Text.Trim(),
                                                                  txtSoLuongColumn.Text.Trim(),
                                                                  txtDonGiaColumn.Text.Trim(),
                                                                  txtTSXNK.Text.Trim(),
                                                                  txtXuatXuColumn.Text.Trim());

        }
        private void ReadDefault()
        {
            DocExcel docExcel = new DocExcel();
            docExcel.ReadDefault(txtSheet,
                                                                  txtRow,
                                                                  txtMaHangColumn,
                                                                  txtTenHangColumn,
                                                                  txtMaHSColumn,
                                                                  txtDVTColumn,
                                                                  txtSoLuongColumn,
                                                                  txtDonGiaColumn,
                                                                  txtTSXNK,
                                                                  txtXuatXuColumn);
        }

        private void ReadExcelForm_Load(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
            this.ReadDefault();
        }
    }
}